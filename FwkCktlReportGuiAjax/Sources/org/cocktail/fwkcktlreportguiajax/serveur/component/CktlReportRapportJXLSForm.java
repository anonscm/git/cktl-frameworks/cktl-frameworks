/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.metier.EORapportJxls;
import org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class CktlReportRapportJXLSForm extends ACktlReportRapportExtFormComponent {
	private static final long serialVersionUID = 1L;

	public CktlReportRapportJXLSForm(WOContext arg0) {
		super(arg0);
	}

	public EORapportJxls getRapportExt() {
		return (EORapportJxls) valueForBinding(BINDING_rapportExt);
	}

	public Boolean unTypeExportChecked() {
		return (getRapportExt().getTypeExports().contains(unTypeFormatExports.toTypeExport()));
	}

	public void setUnTypeExportChecked(Boolean val) {
		if (val.booleanValue()) {
			if (!getRapportExt().getTypeExports().contains(unTypeFormatExports.toTypeExport())) {
				EORapportJxlsTex r = getRapportExt().createToRapportJxlsTexesRelationship();
				r.setToTypeExportRelationship(unTypeFormatExports.toTypeExport());
			}
		}
		else {
			if (getRapportExt().getTypeExports().contains(unTypeFormatExports.toTypeExport())) {
				EOQualifier qual = new EOKeyValueQualifier(EORapportJxlsTex.TO_TYPE_EXPORT_KEY, EOQualifier.QualifierOperatorEqual, unTypeFormatExports.toTypeExport());
				NSArray<EORapportJxlsTex> res = getRapportExt().toRapportJxlsTexes(qual, false);
				if (res.count() > 0) {
					getRapportExt().deleteToRapportJxlsTexesRelationship((EORapportJxlsTex) res.objectAtIndex(0));
				}
			}
		}
	}

	public Boolean isDeleteDisabled() {
		return Boolean.FALSE;
	}

	public WOActionResults onDelete() {
		try {
			getRapportExt().deleteAllToRapportJxlsTexesRelationships();
			getRapportExt().toRapport().deleteToRapportJxlssRelationship(getRapportExt());
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public String getSqlStrLight() {
		return StringCtrl.cut(getRapportExt().rjlSql(), 50);
	}

	public String getModalWindowId() {
		return getComponentId() + "_win";
	}

	public WOActionResults fermer() {
		CktlAjaxWindow.close(context(), getModalWindowId());
		if (!MyStringCtrl.isEmpty(updateContainerID())) {
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		}
		return null;
	}

}
