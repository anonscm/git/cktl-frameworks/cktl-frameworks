/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlreport.server.metier.EOCategorie;
import org.cocktail.fwkcktlreportguiajax.serveur.controleur.PubRapportSelectAndExecCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;

/**
 * Composant qui affiche la liste des rapports accessibles à l'utilisateur. Utilisez plutot ce composant que RapportList car il gère les fetch dans la
 * base.
 * 
 * @binding appUser Obligatoire.
 * @binding showFavoritesTab
 * @binding showHistoryTab
 * @binding showPlanificationTab
 * @binding showCategoriesBrowser
 * @binding refreshData Boolean. Indique au composant qu'il doit rafraichir ses données au prochain update.
 * @binding rapportsDg DisplayGroup des rapports.
 * @binding rapportsPlanificationDg DisplayGroup des abonnements aux rapports.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class PubRapportSelectList extends AFwkCktlReportGuiAjaxComponent {

	public static final String TAB_TOUS = "Tous";
	public static final String TAB_FAVORITES = "Favoris";
	public static final String TAB_HISTORIQUE = "Historique";
	public static final String TAB_RECHERCHER = "Rechercher";
	public static final String TAB_PLANIFICATION = "Abonnements";

	private static final long serialVersionUID = 1L;
	private static final String BDG_showAllTab = "showAllTab";
	private static final String BDG_showFavoritesTab = "showFavoritesTab";
	private static final String BDG_showHistoryTab = "showHistoryTab";
	private static final String BDG_showCategoriesBrowser = "showCategoriesBrowser";
	private static final String BDG_showPlanificationTab = "showPlanificationTab";
	private static final String BDG_refreshData = "refreshData";
	private static final String BDG_rapportsDg = "rapportsDg";
	private static final String BDG_rapportsPretsDg = "rapportsPretsDg";

	public static final Boolean DEFAULT_showAllTab = Boolean.TRUE;
	public static final Boolean DEFAULT_showFavoritesTab = Boolean.TRUE;
	private static final Boolean DEFAULT_showHistoryTab = Boolean.TRUE;
	private static final Boolean DEFAULT_showPlanificationTab = Boolean.TRUE;
	private static final Boolean DEFAULT_showCategoriesBrowser = Boolean.TRUE;
	private static final Boolean DEFAULT_showRechercherTab = Boolean.TRUE;
	private static final String BDG_showRechercherTab = "showRechercher";

	private static ArrayList<String> rapportListTabsList = new ArrayList<String>();
	static {
		rapportListTabsList.add(TAB_TOUS);
		rapportListTabsList.add(TAB_FAVORITES);
		rapportListTabsList.add(TAB_HISTORIQUE);
		rapportListTabsList.add(TAB_RECHERCHER);
	}

	private static Map<String, String> tabListTitle = new HashMap<String, String>();
	static {
		tabListTitle.put(TAB_TOUS, "Rapports disponibles");
		tabListTitle.put(TAB_FAVORITES, "Vos rapports favoris");
		tabListTitle.put(TAB_HISTORIQUE, "Historique d'exécution des rapports");
		tabListTitle.put(TAB_RECHERCHER, "Résultat de la recherche");
		tabListTitle.put(TAB_PLANIFICATION, "Abonnements aux rapports");
	}

	private PubRapportSelectAndExecCtrl ctrl;
	private EOCategorie selectedCategorie;

	private String selectedTab;
	private String filtre;

	public PubRapportSelectList(WOContext context) {
		super(context);
		//ctrl = new PubRapportSelectAndExecCtrl(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (wantRefreshData().booleanValue()) {
			refreshData();
		}
		super.appendToResponse(response, context);

	}

	public Boolean showFavoritesTab() {
		return booleanValueForBinding(BDG_showFavoritesTab, DEFAULT_showFavoritesTab);
	}

	public Boolean showHistoryTab() {
		return booleanValueForBinding(BDG_showHistoryTab, DEFAULT_showHistoryTab);
	}

	public Boolean showPlanificationTab() {
		return booleanValueForBinding(BDG_showPlanificationTab, DEFAULT_showPlanificationTab);
	}

	public Boolean showAllTab() {
		return booleanValueForBinding(BDG_showAllTab, DEFAULT_showAllTab);
	}

	public Boolean showRechercherTab() {
		return booleanValueForBinding(BDG_showRechercherTab, DEFAULT_showRechercherTab);
	}

	public Boolean showCategoriesBrowser() {
		return (TAB_TOUS.equals(getSelectedTab()) && booleanValueForBinding(BDG_showCategoriesBrowser, DEFAULT_showCategoriesBrowser));
	}

	public Boolean showRechercher() {
		return (TAB_RECHERCHER.equals(getSelectedTab()) && booleanValueForBinding(BDG_showRechercherTab, DEFAULT_showRechercherTab));
	}

	public Boolean wantRefreshData() {
		return booleanValueForBinding(BDG_refreshData, Boolean.FALSE);
	}

	public WODisplayGroup getRapportsDg() {
		return (WODisplayGroup) valueForBinding(BDG_rapportsDg);
	}

	public WODisplayGroup getRapportsPretsDg() {
		return (WODisplayGroup) valueForBinding(BDG_rapportsPretsDg);
	}

	private void refreshData() {
		ctrl.refreshData();
		setValueForBinding(Boolean.FALSE, BDG_refreshData);
	}

	public String tbvContainerId() {
		return getComponentId() + "_liste";
	}

	public EOCategorie getSelectedCategorie() {
		return selectedCategorie;
	}

	public void setSelectedCategorie(EOCategorie selectedCategorie) {
		this.selectedCategorie = selectedCategorie;
		refreshData();
	}

	public String jsUpdateAfterListUpdate() {
		if (updateContainerID() != null) {
			return "if ($('" + updateContainerID() + "')) {" + updateContainerID() + "Update();};";
		}
		return "";
	}

	public String getActionsContainerId() {
		return getComponentId() + "actions";
	}

	public WOActionResults showFavorites() {
		setSelectedTab(TAB_FAVORITES);
		refreshData();
		return null;
	}

	public WOActionResults showHistoriques() {
		setSelectedTab(TAB_HISTORIQUE);
		refreshData();
		return null;
	}

	public WOActionResults showSrch() {
		setSelectedTab(TAB_RECHERCHER);
		refreshData();
		return null;
	}

	public WOActionResults showPlanification() {
		setSelectedTab(TAB_PLANIFICATION);
		refreshData();
		return null;
	}

	public WOActionResults showTous() {
		setSelectedTab(TAB_TOUS);
		refreshData();
		return null;
	}

	public String getSelectedTab() {
		if (selectedTab == null) {
			selectedTab = TAB_TOUS;
		}
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public String getTbvHeight() {
		if (TAB_TOUS.equals(getSelectedTab())) {
			return "400";
		}
		return "550";
	}

	/**
	 * @return the filtre
	 */
	public String getFiltre() {
		return filtre;
	}

	/**
	 * @param filtre the filtre to set
	 */
	public void setFiltre(String filtre) {
		this.filtre = filtre;
	}

	public String getSrchContainerId() {
		return getComponentId() + "filtrecont";
	}

	public WOActionResults filtrer() {
		refreshData();
		return null;
	}

	public Boolean showRapportList() {
		return Boolean.valueOf(rapportListTabsList.contains(getSelectedTab()));
	}

	public Boolean showRapportPlanificationList() {
		return Boolean.valueOf(TAB_PLANIFICATION.equals(getSelectedTab()));
	}

	public String getRapportListLibelle() {
		return tabListTitle.get(getSelectedTab());
	}

	public String getContainerActionTousClass() {
		String res = "FwkCktlReportGuiAjax_PubRapportSelectAction";
		if (TAB_TOUS.equals(getSelectedTab())) {
			res = res + "_selected";
		}
		return res;
	}

	public String getContainerActionFavoritesClass() {
		String res = "FwkCktlReportGuiAjax_PubRapportSelectAction";
		if (TAB_FAVORITES.equals(getSelectedTab())) {
			res = res + "_selected";
		}
		return res;
	}

	public String getContainerActionHistoriqueClass() {
		String res = "FwkCktlReportGuiAjax_PubRapportSelectAction";
		if (TAB_HISTORIQUE.equals(getSelectedTab())) {
			res = res + "_selected";
		}
		return res;
	}

	public String getContainerActionSrchClass() {
		String res = "FwkCktlReportGuiAjax_PubRapportSelectAction";
		if (TAB_RECHERCHER.equals(getSelectedTab())) {
			res = res + "_selected";
		}
		return res;
	}

	public String getContainerActionPlanificationClass() {
		String res = "FwkCktlReportGuiAjax_PubRapportSelectAction";
		if (TAB_PLANIFICATION.equals(getSelectedTab())) {
			res = res + "_selected";
		}
		return res;
	}

}