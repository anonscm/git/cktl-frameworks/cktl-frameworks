/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo;
import org.cocktail.fwkcktlreport.server.metier.EOTypeAbo;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Affiche (pour une liste) un rapport pret .
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapportPretOpt extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BDG_rapportPretAbo = "rapportPretAbo";
	private static final String BINDING_isReadOnly = "isReadOnly";
	public EOTypeAbo unTypeAbo;
	public String unFrequence;
	private EOPersRapportPretAbo rapportPretAbo;

	private NSArray<String> frequences = new NSArray<String>(new String[] {
			"Jamais", "Tous les jours", "Toutes les semaines", "Tous les mois"
	});

	public RapportPretOpt(WOContext arg0) {
		super(arg0);
	}

	public EOPersRapportPretAbo getRapportPretAbo() {
		//return (EOPersRapportPret) valueForBinding(BDG_rapportPret);
		if (rapportPretAbo == null && hasBinding(BDG_rapportPretAbo)) {
			rapportPretAbo = (EOPersRapportPretAbo) valueForBinding(BDG_rapportPretAbo);
		}
		else if (rapportPretAbo == null) {
			rapportPretAbo = (EOPersRapportPretAbo) valueForBinding("value");
		} else if (rapportPretAbo.globalID() != ((EOPersRapportPretAbo) valueForBinding(BDG_rapportPretAbo)).globalID()) {
			rapportPretAbo = (EOPersRapportPretAbo) valueForBinding(BDG_rapportPretAbo);
		}
		return rapportPretAbo;
	}

	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_isReadOnly, Boolean.TRUE);
	}

	public Boolean isDisabled() {
		return isReadOnly();
	}

	public NSArray<EOTypeAbo> getTypeAbos() {
		return EOTypeAbo.fetchAll(edc(), new NSArray(EOTypeAbo.SORT_LIBELLE_ASC));
	}

	public NSArray<String> getFrequences() {
		return frequences;
	}

	public WOActionResults supprimer() {
		try {
			EOPersRapportPretAbo.supprimer(getRapportPretAbo());
			edc().saveChanges();
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

}
