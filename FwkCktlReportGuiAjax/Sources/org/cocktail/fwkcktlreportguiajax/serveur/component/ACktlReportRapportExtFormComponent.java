/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.FwkCktlReportParamManager;
import org.cocktail.fwkcktlreport.server.files.CktlReportFilesUtils;
import org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.reporting.server.CktlAbstractReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class ACktlReportRapportExtFormComponent extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_rapportExt = "rapportExt";
	private NSArray<String> avalailableDirectories;
	public EOTypeFormatExport unTypeFormatExports;

	public ACktlReportRapportExtFormComponent(WOContext context) {
		super(context);
	}

	public abstract IRapportExt getRapportExt();

	public NSArray<String> avalailableDirectories() {
		if (avalailableDirectories == null) {
			NSMutableArray<String> res = new NSMutableArray<String>();
			res.add((String) getAppUser().getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION));
			res.add((String) getAppUser().getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION));
			avalailableDirectories = res.immutableClone();
		}
		return avalailableDirectories;
	}

	public Boolean isFoundInLocal() {
		return CktlReportFilesUtils.isRapportExtFileFoundInLocal(getRapportExt(), getAppUser());
	}

	public Boolean isFoundInGlobal() {
		if (getRapportExt().hasTemporaryGlobalID()) {
			return Boolean.FALSE;
		}
		return CktlReportFilesUtils.isRapportExtFileFoundInGlobal(getRapportExt(), getAppUser());
	}

	public String onClikBeforeDelete() {
		return "confirm('Voulez-vous réellement supprimer ce modèle " + (getRapportExt().getSrcFile() != null ? getRapportExt().getSrcFile() : "") + " ?')";
	}

	public WOActionResults downloadFileFromGlobal() {
		String fileName = getRapportExt().getSrcFile();
		String filePath = CktlReportFilesUtils.globalPathOfFile(getAppUser(), getRapportExt(), fileName);
		NSData data = CktlReportFilesUtils.loadFileFromPath(filePath);
		return CktlAbstractReporter.downloadFile(data, fileName, CktlDataResponse.MIME_ANY);
	}

	public WOActionResults downloadFileFromLocal() {
		String fileName = getRapportExt().getSrcFile();
		String filePath = CktlReportFilesUtils.localPathOfFile(getAppUser(), getRapportExt(), fileName);
		NSData data = CktlReportFilesUtils.loadFileFromPath(filePath);
		return CktlAbstractReporter.downloadFile(data, fileName, CktlDataResponse.MIME_ANY);
	}
}
