/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.metier.EOCategorie;
import org.cocktail.fwkcktlreportguiajax.serveur.controleur.CategoriesBrowserCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Affiche un browser de Catégories en tenant compte des droits de l'utilisateur.
 * 
 * @binding appUser Obligatoire
 * @binding updateContainerID Id du container à rafraichir apres un changement de selection
 * @binding selection Obligatoire. La catégorie sélectionnée.
 * @binding showChemin Facultatif. Affiche ou non le chemin sélectionné sous le browser.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CategoriesBrowser extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BDG_selection = "selection";
	private static final String BDG_showChemin = "showChemin";
	private static final String POPUP_SIZE = "8";
	private CategoriesBrowserCtrl ctrl;

	public EOCategorie unCat1;
	public EOCategorie unCat2;
	public EOCategorie unCat3;

	private EOCategorie cat1;
	private EOCategorie cat2;
	private EOCategorie cat3;

	public CategoriesBrowser(WOContext context) {
		super(context);
		ctrl = new CategoriesBrowserCtrl(this);
	}

	public String container1Id() {
		return getComponentId() + "_1a";
	}

	public String container2Id() {
		return getComponentId() + "_2a";
	}

	public String container3Id() {
		return getComponentId() + "_3a";
	}

	public String containercheminId() {
		return getComponentId() + "_chemin";
	}

	public String popup1Id() {
		return getComponentId() + "_popup1";
	}

	public String popup2Id() {
		return getComponentId() + "_popup2";
	}

	public String popup3Id() {
		return getComponentId() + "_popup3";
	}

	public NSArray<EOCategorie> getCategoriesNiveau1() {
		return ctrl.getCategoriesNiveau1();
	}

	public NSArray<EOCategorie> getCategoriesNiveau2() {
		return ctrl.getCategoriesNiveau2();
	}

	public NSArray<EOCategorie> getCategoriesNiveau3() {
		return ctrl.getCategoriesNiveau3();
	}

	private String JSUpdateContainerIDUpdate() {
		if (hasBinding(BINDING_updateContainerID)) {
			return stringValueForBinding(BINDING_updateContainerID, null) + "Update();";
		}
		return "";
	}

	private String JSUpdateChemin() {
		if (showChemin().booleanValue()) {
			return containercheminId() + "Update();";
		}
		return "";
	}

	public String onCompleteCat1() {
		return "function(){" + container2Id() + "Update();" + JSUpdateChemin() + JSUpdateContainerIDUpdate() + "}";
	}

	public String onCompleteCat2() {
		return "function(){" + container3Id() + "Update();" + JSUpdateChemin() + JSUpdateContainerIDUpdate() + "}";
	}

	public String onCompleteCat3() {
		return "function(){" + JSUpdateChemin() + JSUpdateContainerIDUpdate() + "}";
	}

	public EOCategorie getSelection() {
		return (EOCategorie) valueForBinding(BDG_selection);
	}

	public void setSelection(EOCategorie value) {
		setValueForBinding(value, BDG_selection);
	}

	public EOCategorie getCat1() {
		return cat1;
	}

	public void setCat1(EOCategorie cat1) {
		setSelection(cat1);
		this.cat1 = cat1;
		ctrl.setCategoriesNiveau3(null);
		ctrl.updateCategoriesNiveau2();
	}

	public EOCategorie getCat2() {
		return cat2;
	}

	public void setCat2(EOCategorie cat2) {
		this.cat2 = cat2;
		if (cat2 == null) {
			setSelection(cat1);
		}
		else {
			setSelection(cat2);
		}
		ctrl.updateCategoriesNiveau3();
	}

	public EOCategorie getCat3() {
		return cat3;
	}

	public void setCat3(EOCategorie cat3) {
		this.cat3 = cat3;
		if (cat3 == null) {
			setSelection(cat2);
		}
		else {
			setSelection(cat3);
		}
	}

	public String popupSize() {
		return POPUP_SIZE;
	}

	public Boolean showPopup2() {
		return Boolean.valueOf(ctrl.getCategoriesNiveau2() != null && ctrl.getCategoriesNiveau2().count() > 0);
	}

	public Boolean showPopup3() {
		return Boolean.valueOf(ctrl.getCategoriesNiveau3() != null && ctrl.getCategoriesNiveau3().count() > 0);
	}

	public String categorieChemin() {
		EOCategorie sel = getSelection();
		return (sel != null ? sel.displayFullPath() : "Tous");
	}

	public String noSelectionString() {
		return null;
	}

	public Boolean showChemin() {
		return booleanValueForBinding(BDG_showChemin, Boolean.FALSE);
	}

}