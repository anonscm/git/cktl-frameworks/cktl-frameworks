/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlreportguiajax.serveur.CktlReportMailMessage;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * @binding updateContainerID
 * @binding cktlMailMessage
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportSendRapportDialog extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_windowId = "windowId";
	public static final String BINDING_callbackOnAnnuler = "callbackOnAnnuler";
	public static final String BINDING_callbackOnValider = "callbackOnValider";
	public static final String BINDING_CktlMailMessage = "cktlMailMessage";

	public CktlReportSendRapportDialog(WOContext context) {
		super(context);
	}

	public String modalWindowId() {
		return (String) valueForBinding(BINDING_windowId);
	}

	public String getContainerWindowId() {
		return getComponentId() + "_winContainer";
	}

	public String containerDestinataireId() {
		return getComponentId() + "_destContainer";
	}

	public String containerActionId() {
		return getComponentId() + "_actions";
	}

	public String containerSubjectId() {
		return getComponentId() + "_subject";
	}

	public String containerMessageId() {
		return getComponentId() + "_msg";
	}

	public String subjectId() {
		return getComponentId() + "_subject";
	}

	public CktlAjaxDestinatairesListeSelector.DestinataireListe getDestinatairesForRapport() {
		return cktlMailMessage().getDestinataires();
	}

	public WOActionResults annuler() {
		CktlAjaxWindow.close(context(), modalWindowId());
		if (hasBinding(BINDING_callbackOnAnnuler)) {
			return performParentAction((String) valueForBinding(BINDING_callbackOnAnnuler));
		}
		return null;
	}

	public WOActionResults valider() {
		WOActionResults res = null;
		try {
			validerEmail();

			CktlAjaxWindow.close(context(), modalWindowId());
			if (hasBinding(BINDING_callbackOnAnnuler)) {
				res = performParentAction((String) valueForBinding(BINDING_callbackOnAnnuler));
			}
			CktlAjaxWindow.close(context(), modalWindowId());
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}

		return res;
	}

	public Boolean isValiderDisabled() {
		return Boolean.FALSE;
	}

	public CktlReportMailMessage cktlMailMessage() {
		return (CktlReportMailMessage) valueForBinding(BINDING_CktlMailMessage);
	}

	public void validerEmail() throws Exception {
		cktlMailMessage().prepare();
		cktlMailMessage().checkMailReady();
	}

	public String getJsValiderOnComplete() {
		return (updateContainerID() != null ? getJsForUpdateContainerUpdate() : null);
	}
}