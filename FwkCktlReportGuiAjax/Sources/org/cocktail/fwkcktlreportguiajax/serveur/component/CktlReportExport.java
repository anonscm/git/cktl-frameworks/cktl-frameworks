/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.serialize.CktlReportPack;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.reporting.server.CktlAbstractReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSData;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportExport extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_rapport = "rapport";
	private CktlReportPack rapportPack;

	public CktlReportExport(WOContext context) {
		super(context);
	}

	public WOActionResults exporterRapport() {
		try {
			rapportPack = CktlReportPack.createPack(getAppUser(), getRapport());

		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults saveFile() {
		try {
			NSData zipPack = rapportPack.toZip();
			return CktlAbstractReporter.downloadFile(zipPack, getRapport().rapStrIdBasic() + "." + CktlReportPack.PACK_FILE_EXTENSION, CktlDataResponse.MIME_ANY);
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public EORapport getRapport() {
		return (EORapport) valueForBinding(BINDING_rapport);
	}

	public String getModalWindowId2() {
		return getComponentId() + "_winexport";
	}

	public WOActionResults fermer() {
		rapportPack = null;
		CktlAjaxWindow.close(context(), getModalWindowId2());
		return null;
	}

	public Boolean isPackReady() {
		return Boolean.valueOf(rapportPack != null);
	}
}