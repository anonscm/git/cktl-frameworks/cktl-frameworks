/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.CktlReportSqlCtrl;
import org.cocktail.fwkcktlreport.server.metier.EOCritere;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class RapCritPopup extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public final static String BINDING_rapportCritere = "rapportCritere";
	public final static String BINDING_value = "value";

	private CktlReportSqlCtrl critSqlCtrl;
	private NSArray listForPopupCache = null;
	public NSDictionary unPopupItem;

	public RapCritPopup(WOContext context) {
		super(context);
	}

	public String popup1Id() {
		return getOwnerComponent().getComponentId() + "popup";
	}

	/**
	 * @return La liste (de paires cle/valeurs) sous forme de NSDictionary contenue dans critMetadata si une telle liste est detectee. La liste doit
	 *         etre de la forme cle1:valeur1,cle2:valeur2,cle3,cle4:valeur4. Si une valeur est specifiee, la cle est utilisee comme valeur.
	 */
	private NSArray listForPopupList() {
		NSMutableArray result = new NSMutableArray();
		if (!getOwnerComponent().getRapportCritere().toCritere().isMetadataSql().booleanValue()) {
			String list = StringCtrl.normalize(getOwnerComponent().getRapportCritere().toCritere().critMetadata());
			NSArray res = NSArray.componentsSeparatedByString(list, ",");
			for (int i = 0; i < res.count(); i++) {
				String tmp = StringCtrl.normalize((String) res.objectAtIndex(i));
				NSArray paire = NSArray.componentsSeparatedByString(tmp, ":");
				String cle = null;
				String valeur = null;
				if (paire.count() > 0) {
					cle = (String) paire.objectAtIndex(0);
					if (paire.count() > 1) {
						valeur = (String) paire.objectAtIndex(1);
					}
					else {
						valeur = cle;
					}
					NSMutableDictionary dico = new NSMutableDictionary();
					dico.takeValueForKey(cle, EOCritere.POPUP_LIST_CLE);
					dico.takeValueForKey(valeur, EOCritere.POPUP_LIST_VALEUR);
					result.addObject(dico);
				}
			}
		}
		return result.immutableClone();
	}

	/**
	 * @return Une liste sous forme de NSArray de NSDictionary contenant deux paires avec comme clés (CLE,VALEUR).
	 */
	public NSArray<NSDictionary<String, Object>> listForPopup() {
		if (listForPopupCache == null || (critSqlCtrl != null && critSqlCtrl.containsParams())) {
			updateListForPopupCache();
		}
		return listForPopupCache;
	}

	private void updateListForPopupCache() {
		NSArray res = null;
		if (getOwnerComponent().getRapportCritere().toCritere().isMetadataSql().booleanValue()) {
			res = listForPopupSql();
		}
		else {
			res = listForPopupList();
		}
		listForPopupCache = res;
	}

	private NSArray listForPopupSql() {
		NSArray res = null;
		String sql = getOwnerComponent().getMetadataSql();
		if (sql != null) {
			critSqlCtrl = new CktlReportSqlCtrl(sql);
			res = critSqlCtrl.rawRowsForSQL(getOwnerComponent().edc(), EOCritere.POPUP_KEYS, getAppUser().getCktlReportRapportCriteresGlobal());

		}
		return res;
	}

	public Object convertParamValueToDisplayValue(Object value) {
		EOQualifier qual = new EOKeyValueQualifier(EOCritere.POPUP_LIST_CLE, EOQualifier.QualifierOperatorEqual, value);
		NSArray res = EOQualifier.filteredArrayWithQualifier(listForPopup(), qual);
		if (res.count() > 0) {
			return res.objectAtIndex(0);
		}
		return value;
	}

	public Object convertDisplayValueToParamValueTo(Object value) {
		if (value == null) {
			return null;
		}
		return ((NSDictionary) value).valueForKey(EOCritere.POPUP_LIST_CLE);
	}

	public String getSql() {
		return getOwnerComponent().getRapportCritere().toCritere().critMetadata();
	}

	/**
	 * Verifier si les criteres obligatoires sont remplis (= les parametres détectés dans la requête).
	 * 
	 * @return True ou false selon le cas...
	 */
	public Boolean isDisabled() {
		Boolean res = Boolean.FALSE;
		if (!MyStringCtrl.isEmpty(getSql())) {
			CktlReportSqlCtrl critSqlCtrl = new CktlReportSqlCtrl(getSql());
			res = (critSqlCtrl.getParams().count() > 0 && NSArrayCtrl.intersectionOfNSArray(critSqlCtrl.getParams(), getOwnerComponent().getAppUser().getCktlReportRapportCriteresGlobal().allKeys()).count() != critSqlCtrl.getParams().count());
		}
		return res;
	}

	public RapCritPopup getOwnerComponent() {
		return this;
	}

	public EORapportCritere getRapportCritere() {
		return (EORapportCritere) valueForBinding(BINDING_rapportCritere);
	}

	public String getMetadataSql() {
		String sql = StringCtrl.normalize(getRapportCritere().toCritere().critMetadata());
		sql = sql.toUpperCase();
		if (sql.startsWith("SELECT ")) {
			return sql;
		}
		return null;
	}

	/**
	 * @return L'objet selectionne.
	 */
	public Object getValue() {
		//	On recupere la valeur eventuellement (pre)selectionnee 
		Object res = getAppUser().getCktlReportRapportCriteresGlobal().get(getRapportCritere().getCle());
		if (res == null && listForPopup().count() > 0) {
			setValue(listForPopup().objectAtIndex(0));
		}
		return convertParamValueToDisplayValue(getAppUser().getCktlReportRapportCriteresGlobal().get(getRapportCritere().getCle()));
	}

	/**
	 * Affecte l'objet selectionne
	 * 
	 * @param value
	 */
	public void setValue(Object value) {
		getAppUser().getCktlReportRapportCriteresGlobal().put(getRapportCritere().getCle(), convertDisplayValueToParamValueTo(value));
	}

}