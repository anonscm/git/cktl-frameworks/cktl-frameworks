/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.metier.EORapport;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;

/**
 * @binding appUser Obligatoire.
 * @binding rapportsDg WODisplayGroup. Obligatoire
 * @binding updateContainerID
 * @binding width
 * @binding height
 * @deprecated Utilisez RapportTbv à la place.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapportSelectionList extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BDG_rapportsDg = "rapportsDg";
	public static final String BDG_width = "width";
	public static final String BDG_height = "height";
	public static final String DEFAULT_width = "300px";
	public static final String DEFAULT_height = "300px";
	private static final String CLASS_EVEN = "even";
	private static final String CLASS_ODD = "odd";

	public EORapport unRapport;
	private boolean isEven = true;

	public RapportSelectionList(WOContext context) {
		super(context);
	}

	public WODisplayGroup rapportsDg() {
		return (WODisplayGroup) valueForBinding(BDG_rapportsDg);
	}

	public String getStyle() {
		return "width: " + stringValueForBinding(BDG_width, DEFAULT_width) + ";" +
				"height: " + stringValueForBinding(BDG_height, DEFAULT_height) + ";";
	}

	public Object selection() {
		return rapportsDg().selectedObject();
	}

	public void setSelection(Object select) {
		rapportsDg().selectObject(select);
	}

	public String onSelectionChangeJS() {
		return containerSelId() + "Update()";
		//return (updateContainerID() != null ? updateContainerID() + "Update()" : null);
	}

	public String getClassLi() {
		isEven = !isEven;
		if (isEven) {
			return CLASS_EVEN;
		}
		return CLASS_ODD;

	}

	public String containerSelId() {
		return getComponentId() + "sel";
	}
}