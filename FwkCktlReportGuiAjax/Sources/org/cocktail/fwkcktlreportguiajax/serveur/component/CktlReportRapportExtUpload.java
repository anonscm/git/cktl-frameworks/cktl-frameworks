/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.files.CktlReportFilesUtils;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateContainer;

/**
 * Composant d'upload d'un fichier pour un rapport.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportRapportExtUpload extends AFwkCktlReportGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	private NSData dataUploaded;
	private String filePath;
	public static final String BINDING_rapportExt = "rapportExt";

	//public NSMutableArray<NSData> foSecondaireUpdloads = new NSMutableArray<NSData>();
	public NSMutableDictionary<String, NSData> foSecondaireUpdloads = new NSMutableDictionary<String, NSData>();
	public String foSecondaireUploadFilePathItem;

	private NSData foSecondaireNewUploadData;
	private String foSecondaireNewUploadFilePath;
	private IRapportExt rapportExtCache;
	private String erreurSaisieMessage;

	public CktlReportRapportExtUpload(WOContext arg0) {
		super(arg0);
		//mySession().registerUpdateContainerForEachResponse(videContainerId());
	}

	public IRapportExt getRapportExt() {
		if (rapportExtCache == null || !rapportExtCache.equals(valueForBinding(BINDING_rapportExt))) {
			rapportExtCache = (IRapportExt) valueForBinding(BINDING_rapportExt);
			cleanAll();
		}
		return rapportExtCache;
	}

	/**
	 * Si un fichier avec le même nom est déjà présent, on l'archive en .bak
	 * 
	 * @return
	 */
	public WOActionResults valider() {
		try {
			if (getFilePath() != null && getDataUploaded() != null && getDataUploaded().length() > 0) {
				CktlReportFilesUtils.saveFileInLocalForRapportExt(getAppUser(), getFilePath(), getDataUploaded(), getRapportExt());
			}
			//Fichiers secondaires
			for (int i = 0; i < foSecondaireUpdloads.allKeys().count(); i++) {
				String f = foSecondaireUpdloads.allKeys().objectAtIndex(i);
				CktlReportFilesUtils.saveFileInLocal(getAppUser(), getRapportExt(), f, foSecondaireUpdloads.objectForKey(f));
			}

			if (getRapportExt().getSrcFile() == null) {
				getRapportExt().setSrcFile(getFilePath());
			}

			fermer2();

		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage("Une erreur est survenue lors de l'enregistrement du fichier : " + e.getLocalizedMessage());
			AjaxUpdateContainer.updateContainerWithID(videContainerId(), context());
		}
		return null;
	}

	public WOActionResults fermer2() {
		cleanAll();
		if (!MyStringCtrl.isEmpty(updateContainerID())) {
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		}
		CktlAjaxWindow.close(context(), getModalWindowId2());
		return null;
	}

	//	/**
	//	 * @return Le répertoire ou placer le fichier.
	//	 */
	//	public String getDirectory() {
	//		return CktlReportFilesUtils.getLocalDirectory(getAppUser());
	//
	//		//return (String) getAppUser().getCktlReportParameters().get(CktlReportParameters.CKTLREPORT_LOCAL_REPORTS_LOCATION);
	//	}

	public NSData getDataUploaded() {
		return dataUploaded;
	}

	public void setDataUploaded(NSData dataUploaded) {
		this.dataUploaded = dataUploaded;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getModalWindowId2() {
		return getComponentId() + "_winfu";
	}

	public Boolean isFilePresent() {
		return Boolean.valueOf(isFilePresentGlobal().booleanValue() || isFilePresentLocal().booleanValue());
	}

	public Boolean isFilePresentLocal() {
		return Boolean.valueOf(getRapportExt().getSrcFile() != null && CktlReportFilesUtils.isRapportExtFileFoundInLocal(getRapportExt(), getAppUser()));
	}

	public Boolean isFilePresentGlobal() {
		return Boolean.valueOf(getRapportExt().getSrcFile() != null && CktlReportFilesUtils.isRapportExtFileFoundInGlobal(getRapportExt(), getAppUser()));
	}

	public String fileUploadSecondaireNewId() {
		return getComponentId() + "_foSecondaireNewUpdloads";
	}

	public NSData getFoSecondaireNewUploadData() {
		return foSecondaireNewUploadData;
	}

	public void setFoSecondaireNewUploadData(NSData foSecondaireNewUploadData) {
		this.foSecondaireNewUploadData = foSecondaireNewUploadData;
		if (foSecondaireNewUploadData != null) {
			this.foSecondaireUpdloads.takeValueForKey(foSecondaireNewUploadData, getFoSecondaireNewUploadFilePath());
		}
		cleanFileUploadSec();
	}

	public String getFoSecondaireNewUploadFilePath() {
		return foSecondaireNewUploadFilePath;
	}

	public void setFoSecondaireNewUploadFilePath(String foSecondaireNewUploadFilePath) {
		this.foSecondaireNewUploadFilePath = foSecondaireNewUploadFilePath;
	}

	public NSArray<String> foSecondaireUpdloadsFilePath() {
		return foSecondaireUpdloads.allKeys();

	}

	public String getOnFuFinishedJs() {
		return getAucFileContainerId() + "Update";
	}

	public String getOnFu2FinishedJs() {
		return getComponentId() + "_secFiles" + "Update";
	}

	public String getAucSecFilesContainerId() {
		return getComponentId() + "_secFiles";
	}

	public void cleanFileUploadSec() {
		foSecondaireNewUploadData = null;
		foSecondaireNewUploadFilePath = null;
	}

	public void cleanFileUploadMain() {
		dataUploaded = null;
		filePath = null;
	}

	public void cleanAll() {
		cleanFileUploadMain();
		cleanFileUploadSec();
	}

	public WOActionResults supprimerSecFile() {
		if (foSecondaireUploadFilePathItem != null) {
			foSecondaireUpdloads.removeObjectForKey(foSecondaireUploadFilePathItem);
		}
		return null;
	}

	public String getAucFileContainerId() {
		return getComponentId() + "_File";
	}

	public WOActionResults supprimerFile() {
		if (dataUploaded != null) {
			dataUploaded = null;
		}
		if (filePath != null) {
			filePath = null;
		}

		return null;
	}

	public Boolean isFileUploaded() {
		return Boolean.valueOf(dataUploaded != null && filePath != null);
	}

	//
	public String jsFermer() {
		String cmd = "function() {" + updateContainerID() + "Update();" + "parent.Windows.close('" + getModalWindowId2() + "_win" + "', null);}";
		return cmd;
	}

	public String jsRefresh() {
		String cmd = "function() {" + videContainerId() + "Update();}";
		return cmd;
	}

	public String videContainerId() {
		return getComponentId() + "_vide";
	}

	public String getLocalDirectoryPath() {
		return CktlReportFilesUtils.localReportDirectoryPath(getAppUser(), getRapportExt().toRapport());
	}

	public String getErreurSaisieMessage() {
		return erreurSaisieMessage;
	}

	public void setErreurSaisieMessage(String erreurSaisieMessage) {
		this.erreurSaisieMessage = erreurSaisieMessage;
	}

}