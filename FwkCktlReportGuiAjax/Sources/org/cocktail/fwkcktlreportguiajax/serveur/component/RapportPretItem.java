/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import java.util.Iterator;

import org.cocktail.fwkcktlreport.server.CktlReportRapportCriteres;
import org.cocktail.fwkcktlreport.server.metier.EOCritere;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;
import org.cocktail.fwkcktlreport.server.metier.EOTypeAbo;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Affiche (pour une liste) un rapport pret avec ses abonnements éventuels
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapportPretItem extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BDG_rapportPret = "rapportPret";
	private static final String BINDING_isReadOnly = "isReadOnly";
	public IRapportExt unRapportExt;
	private IRapportExt selectedRapportExt;
	public EOTypeExport unTypeExport;
	private EOTypeExport selectedTypeExport;
	public EOTypeAbo unTypeAbo;
	public String unFrequence;

	private NSArray<String> frequences = new NSArray<String>(new String[] {
			"Jamais", "Tous les jours", "Toutes les semaines", "Tous les mois"
	});

	public RapportPretItem(WOContext arg0) {
		super(arg0);
	}

	public EOPersRapportPret getRapportPret() {
		//return (EOPersRapportPret) valueForBinding(BDG_rapportPret);
		if (hasBinding(BDG_rapportPret)) {
			return (EOPersRapportPret) valueForBinding(BDG_rapportPret);
		}
		else {
			return (EOPersRapportPret) valueForBinding("value");
		}
	}

	public String getClassTypeExport() {
		if (getRapportPret().toTypeExport() != null) {
			return "FwkCktlReportGuiAjax_export_" + getRapportPret().toTypeExport().texStrId().toLowerCase() + "_small";
		}
		return null;
	}

	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_isReadOnly, Boolean.TRUE);
	}

	public Boolean isDisabled() {
		return isReadOnly();
	}

	/**
	 * @return the selectedRapportExt
	 */
	public IRapportExt getSelectedRapportExt() {
		if (selectedRapportExt == null && getRapportPret().toRapport().getRapportExts().count() > 0) {
			setSelectedRapportExt(getRapportPret().toRapport().getRapportExts().objectAtIndex(0));
		}
		return selectedRapportExt;
	}

	/**
	 * @param selectedRapportExt the selectedRapportExt to set
	 */
	public void setSelectedRapportExt(IRapportExt selectedRapportExt) {
		this.selectedRapportExt = selectedRapportExt;
		if (selectedRapportExt != null) {
			getRapportPret().setToTypeFormatRapportRelationship(selectedRapportExt.toTypeFormatRapport());
		}
		else {
			getRapportPret().setToTypeFormatRapportRelationship(null);
		}
	}

	/**
	 * @return the selectedTypeExport
	 */
	public EOTypeExport getSelectedTypeExport() {
		if (selectedTypeExport == null && getSelectedRapportExt() != null && getSelectedRapportExt().toTypeFormatRapport().toTypeFormatExports().count() > 0) {
			setSelectedTypeExport(((EOTypeFormatExport) getSelectedRapportExt().toTypeFormatRapport().toTypeFormatExports().objectAtIndex(0)).toTypeExport());
		}
		return selectedTypeExport;
	}

	/**
	 * @param selectedTypeExport the selectedTypeExport to set
	 */
	public void setSelectedTypeExport(EOTypeExport selectedTypeExport) {
		this.selectedTypeExport = selectedTypeExport;
		getRapportPret().setToTypeExportRelationship(selectedTypeExport);
	}

	public NSArray<EOTypeAbo> getTypeAbos() {
		return EOTypeAbo.fetchAll(edc(), new NSArray(EOTypeAbo.SORT_LIBELLE_ASC));
	}

	public NSArray<String> getFrequences() {
		return frequences;
	}

	public WOActionResults valider() {
		try {
			if (edc().hasChanges()) {

				// TODO : voir pourquoi !
				setSelectedTypeExport(unTypeExport);

				// On recup les criteres du rapport 
				CktlReportRapportCriteres criteres = CktlReportRapportCriteres.createFrom(
						null,
						super.getAppUser().
								getCktlReportRapportCriteresGlobal());

				//On ne conserve que les criteres propres au rapport
				criteres.cleanCriteres(getRapportPret().toRapport());

				NSArray cles = criteres.allKeys();
				for (int i = 0; i < cles.count(); i++) {
					StringBuffer buffer = new StringBuffer(128);
					Object key = cles.get(i);
					Object object = criteres.objectForKey(key);
					if (object instanceof String) {
						buffer.append('"');
						buffer.append((String) object);
						buffer.append('"');
					}
					else if (object instanceof Boolean) {
						buffer.append(((Boolean) object).booleanValue() ? "true" : "false");
					}
					else {
						buffer.append(object);
					}

					EOPersRapportPretCrit crits = new EOPersRapportPretCrit();

					// On rattache le bon EORapportCritere
					EOCritere eoCrit = EOCritere.fetchByKeyValue(edc(), "critCle", cles.get(i).toString());

					NSDictionary keys = EOUtilities.primaryKeyForObject(edc(), getRapportPret().toRapport());
					Object rapId = keys.objectForKey("rapId");

					keys = EOUtilities.primaryKeyForObject(edc(), eoCrit);
					Object critId = keys.objectForKey("critId");

					NSMutableArray lesQualifiers = new NSMutableArray();
					lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("critId =%@", new NSArray(critId)));
					lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("rapId =%@", new NSArray(rapId)));

					EORapportCritere rCrit = EORapportCritere.fetchByQualifier(edc(), new EOAndQualifier(lesQualifiers));

					// On construit le EOPersRapportPretCrit et on l'insert
					crits.setToRapportCritereRelationship(rCrit);
					crits.setPrpcCritValeur(new String(buffer));

					edc().insertObject(crits);
					getRapportPret().addToToPersRapportPretCritsRelationship(crits);
				}
				edc().saveChanges();
				setInfoSaisieMessage("Le rapport a été préconfiguré");
			}
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public Boolean isNotHasChanged() {
		return !edc().hasChanges();
	}

	/**
	 * @return Une chaine avec les critères représentés sous forme cle=valeur séparés par des virgules.
	 */
	public String getLesCriteres() {
		String res = "";
		if (getRapportPret() != null) {
			NSArray<EOPersRapportPretCrit> crits = getRapportPret().toPersRapportPretCrits().immutableClone();
			Iterator<EOPersRapportPretCrit> it = crits.iterator();
			while (it.hasNext()) {
				EOPersRapportPretCrit eoPersRapportPretCrit = (EOPersRapportPretCrit) it.next();
				if (res.length() > 0) {
					res += ", ";
				}
				res += eoPersRapportPretCrit.toRapportCritere().rcrLibelleToDisplay() + "=" + eoPersRapportPretCrit.prpcCritValeur();
			}
		}
		return res;
	}

}
