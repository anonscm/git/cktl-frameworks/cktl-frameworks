/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.CktlReportSqlCtrl;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSDictionary;

public class RapCritSelectDialog extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public final static String BINDING_rapportCritere = "rapportCritere";
	public final static String BINDING_value = "value";

	public RapCritSelectDialog(WOContext context) {
		super(context);
	}

	//private CktlReportSqlCtrl critSqlCtrl;

	private NSDictionary selection;
	private Object value;

	private WODisplayGroup displayGroup;

	private CktlReportSqlCtrl critSqlCtrl;

	public String id() {
		return this.getComponentId() + "select";
	}

	public Object convertParamValueToDisplayValue(Object value) {
		return value;
	}

	public Object convertDisplayValueToParamValueTo(Object value) {
		return value;
	}

	public String getTitle() {
		return "Rechercher et sélectionner...";
	}

	public NSDictionary getSelection() {
		return selection;
	}

	public void setSelection(NSDictionary selection) {
		this.selection = selection;
	}

	public String getKeyForValue() {
		return critSqlCtrl().getFields().objectAtIndex(0).toUpperCase();
	}

	public WODisplayGroup getDisplayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			//displayGroup.setNumberOfObjectsPerBatch(20);
		}
		return displayGroup;
	}

	public String getSql() {
		return this.getRapportCritere().toCritere().critMetadata();
	}

	/**
	 * Verifier si les criteres obligatoires sont remplis (= les parametres détectés dans la requête).
	 * 
	 * @return True ou false selon le cas...
	 */
	public Boolean isDisabled() {
		Boolean res = Boolean.FALSE;
		if (!MyStringCtrl.isEmpty(getSql())) {
			res = (critSqlCtrl().getParams().count() > 0 && NSArrayCtrl.intersectionOfNSArray(critSqlCtrl.getParams(), this.getAppUser().getCktlReportRapportCriteresGlobal().allKeys()).count() != critSqlCtrl.getParams().count());
		}
		else {
			res = Boolean.TRUE;
		}
		return res;
	}

	public CktlReportSqlCtrl critSqlCtrl() {
		if (critSqlCtrl == null) {
			critSqlCtrl = new CktlReportSqlCtrl(getSql());
		}
		return critSqlCtrl;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
		this.getAppUser().getCktlReportRapportCriteresGlobal().put(this.getRapportCritere().getCle(), value);
	}

	public EORapportCritere getRapportCritere() {
		return (EORapportCritere) valueForBinding(BINDING_rapportCritere);
	}

	public String getMetadataSql() {
		String sql = StringCtrl.normalize(getRapportCritere().toCritere().critMetadata());
		sql = sql.toUpperCase();
		if (sql.startsWith("SELECT ")) {
			return sql;
		}
		return null;
	}
}