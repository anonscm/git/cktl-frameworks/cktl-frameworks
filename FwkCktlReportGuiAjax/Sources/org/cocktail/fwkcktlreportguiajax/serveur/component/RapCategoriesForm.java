/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.metier.EOCategorie;
import org.cocktail.fwkcktlreport.server.metier.EORapport;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Affectation des categories a un rapport.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapCategoriesForm extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BDG_rapport = "rapport";
	public static final String BDG_wantReset = "wantReset";

	private WODisplayGroup categoriesNonAssocieesDg;
	private WODisplayGroup categoriesAssocieesDg;
	private EORapport rapportCache;

	public RapCategoriesForm(WOContext arg0) {
		super(arg0);
	}

	public String getContainerCategoriesDispoId() {
		return getComponentId() + "_containerCategoriesDispo";
	}

	public String getContainerActionsId() {
		return getComponentId() + "_containerActions";
	}

	public String getContainerCategoriesAffecteesId() {
		return getComponentId() + "_containerCategoriesAffectees";
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		getRapport();
		super.appendToResponse(response, context);

	}

	public WOActionResults onAdd() {
		try {
			if (categoriesNonAssocieesDg.selectedObject() != null) {
				getRapport().ajouteCategorie((EOCategorie) categoriesNonAssocieesDg.selectedObject());
				resetCategoriesAssocieesDg();
			}
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return postAndUpdate();
	}

	public WOActionResults onDelete() {
		try {
			if (categoriesAssocieesDg.selectedObject() != null) {
				//verifier les droits
				if (!getAppUser().getCategoriesAdministrees().contains(categoriesAssocieesDg.selectedObject())) {
					throw new Exception("Vous n'avez pas de droits d'administration sur la catégorie " + ((EOCategorie) categoriesAssocieesDg.selectedObject()).catStrId());
				}
				getRapport().enleveCategorie((EOCategorie) categoriesAssocieesDg.selectedObject());
				resetCategoriesAssocieesDg();
			}
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return postAndUpdate();
	}

	public EORapport getRapport() {
		if (wantReset() || rapportCache == null || !rapportCache.equals((EORapport) valueForBinding(BDG_rapport))) {
			rapportCache = (EORapport) valueForBinding(BDG_rapport);
			resetComp();
			if (wantReset() && canSetValueForBinding(BDG_wantReset)) {
				setValueForBinding(Boolean.FALSE, BDG_wantReset);
			}
		}
		return rapportCache;
	}

	private void resetComp() {
		categoriesNonAssocieesDg = new WODisplayGroup();
		categoriesAssocieesDg = new WODisplayGroup();
		if (rapportCache != null) {
			categoriesNonAssocieesDg.setObjectArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(getAppUser().getCategoriesAdministrees(), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOCategorie.SORT_CAT_STR_ID_ASC
			})));
			resetCategoriesAssocieesDg();
		}
	}

	private void resetCategoriesAssocieesDg() {
		categoriesAssocieesDg.setObjectArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(rapportCache.categoriesAssociees(), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EOCategorie.SORT_CAT_STR_ID_ASC
		})));
	}

	public WODisplayGroup getCategoriesNonAssocieesDg() {
		return categoriesNonAssocieesDg;
	}

	public WODisplayGroup getCategoriesAssocieesDg() {
		return categoriesAssocieesDg;
	}

	public Boolean wantReset() {
		return booleanValueForBinding(BDG_wantReset, Boolean.FALSE);
	}

}
