/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import java.io.File;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlreport.server.CktlReportRapportInstallCtrl;
import org.cocktail.fwkcktlreport.server.files.CktlReportFileAction;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.serialize.CktlReportPack;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;

/**
 * Gère l'import d'un fichier import/export de report.
 * 
 * @binding actionOnReportImported action à executer une fois le report importé.
 * @binding appUser
 * @binding updateContainerID
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportImport extends AFwkCktlReportGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	public static final String BINDING_ACTION_ON_REPORT_IMPORTED = "actionOnReportImported";
	private NSData foNewUploadData;
	private String foNewUploadFilePath;
	private String erreur;
	private String actionPresentOption = CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_NOTHING;

	public CktlReportImport(WOContext context) {
		super(context);
	}

	public NSData getFoNewUploadData() {
		return foNewUploadData;
	}

	public void setFoNewUploadData(NSData foNewUploadData) {
		this.foNewUploadData = foNewUploadData;
		analyseData(foNewUploadData);
	}

	public String getFoNewUploadFilePath() {
		return foNewUploadFilePath;
	}

	public void setFoNewUploadFilePath(String foNewUploadFilePath) {
		this.foNewUploadFilePath = foNewUploadFilePath;
	}

	public void cleanFileUploadMain() {
		foNewUploadData = null;
		foNewUploadFilePath = null;
	}

	public WOActionResults fermer() {
		cleanFileUploadMain();
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		CktlAjaxWindow.close(context(), getModalWindowId2());
		return null;
	}

	public String getModalWindowId2() {
		return getComponentId() + "_winfu";
	}

	private void analyseData(NSData data) {
		if (edc().hasChanges()) {
			edc().revert();
		}
		erreur = null;
		if (data != null) {
			NSMutableArray<EORapport> touchedRapports = new NSMutableArray<EORapport>();
			NSMutableArray<CktlReportFileAction> fileActions = new NSMutableArray<CktlReportFileAction>();
			try {
				String fileName = (new File(foNewUploadFilePath)).getName();
				CktlReportPack pack = CktlReportPack.loadPack(getAppUser(), fileName, data);
				EORapport rapport = pack.install(edc(), actionPresentOption, getAppUser(), fileActions, touchedRapports);
				//System.out.println(pack.toString());

				//				InputStream is = data.stream();
				//				String xml = ERXFileUtilities.stringFromInputStream(is);
				//
				//				EORapport rap = CktlReportXmlFactory.load(xml, edc(), getAppUser());
				//
				//				CktlReportRapportInstallCtrl.saveRapport(edc(), rap, actionPresentOption, getAppUser(), fileActions, touchedRapports);
				//
				if (edc().hasChanges()) {
					edc().saveChanges();
					try {
						//gerer les actions sur les repertoires associes
						for (int i = 0; i < fileActions.count(); i++) {
							CktlReportFileAction fa = fileActions.objectAtIndex(i);
							fa.performAction();
						}
					} catch (Exception e) {
						throw new Exception("Erreur lors des opérations sur le système de fichier, les modèles ne sont pas cohérents avec la base de données. " + e.getMessage(), e);
					}
					edc().invalidateObjectsWithGlobalIDs(org.cocktail.fwkcktlreport.server.temp.CktlEOUtilities.globalIdsForObjects(edc(), touchedRapports.immutableClone()));
					if (hasBinding(BINDING_ACTION_ON_REPORT_IMPORTED)) {
						performParentAction((String) valueForBinding(BINDING_ACTION_ON_REPORT_IMPORTED));
					}
					setInfoSaisieMessage("Rapport " + rapport.rapStrId() + " importé.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				erreur = e.getLocalizedMessage();
				setErreurSaisieMessage("Erreur lors de l'import : " + e.getLocalizedMessage());
				edc().revert();
				fileActions.clear();
			} finally {
				cleanFileUploadMain();
				touchedRapports.clear();
				//AjaxUpdateContainer.updateContainerWithID(getMainContainerId(), context());
			}
		}

	}

	public String getOnFuFinishedJs() {
		return getContainerResId() + "Update";
	}

	public String getContainerResId() {
		return getComponentId() + "_res";
	}

	public Boolean hasErreur() {
		return Boolean.valueOf(erreur != null);
	}

	public Boolean renameChecked() {
		return CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_RENAME.equals(actionPresentOption);
	}

	public void setRenameChecked(Boolean renameChecked) {
		actionPresentOption = CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_RENAME;
	}

	public Boolean replaceChecked() {
		return CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_REPLACE.equals(actionPresentOption);
	}

	public void setReplaceChecked(Boolean renameChecked) {
		actionPresentOption = CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_REPLACE;
	}

	public Boolean nothingChecked() {
		return CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_NOTHING.equals(actionPresentOption);
	}

	public void setNothingChecked(Boolean renameChecked) {
		actionPresentOption = CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_NOTHING;
	}

	public String getRenameStr() {
		return CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_RENAME;
	}

	public String getReplaceStr() {
		return CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_REPLACE;
	}

	public String getNothingStr() {
		return CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_NOTHING;
	}

	public String getOptionsContainerId() {
		return getComponentId() + "_options";
	}

	public String getErreur() {
		String res = erreur;
		erreur = null;
		return res;
	}

	public String getRenameButtonId() {
		return getComponentId() + "_rename";
	}

	public String getReplaceButtonId() {
		return getComponentId() + "_replace";
	}

	public String getNothingButtonId() {
		return getComponentId() + "_nothing";
	}
}