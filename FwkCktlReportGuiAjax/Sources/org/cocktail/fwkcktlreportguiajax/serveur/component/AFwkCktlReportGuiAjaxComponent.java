/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSPathUtilities;

import er.ajax.AjaxUpdateContainer;
import er.ajax.CktlAjaxUtils;
import er.extensions.eof.ERXEC;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class AFwkCktlReportGuiAjaxComponent extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;

	/** Obligatoire. PersId de l'utilisateur en cours. */
	public final static String BINDING_utilisateurPersId = "utilisateurPersId";

	public final static String BINDING_appUser = "appUser";

	/**
	 * Facultatif. EditingContext fourni par le composant parent. (Si non specifie, le defaultEditingContext de la session est utilise).
	 */
	public static final String BINDING_editingContext = "editingContext";

	/** L'exercice en cours (en String du genre 2009). */
	public static final String BINDING_currentExercice = "currentExercice";

	private String containerErreurMsgId;
	private String erreurSaisieMessage;
	private String infoSaisieMessage;

	private FwkCktlReportApplicationUser appUser;

	public AFwkCktlReportGuiAjaxComponent(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		if (hasBinding(BINDING_cssFilename)) {
			String filename = (String) valueForBinding(BINDING_cssFilename);
			String fwkName = null;
			if (hasBinding(BINDING_cssFwkName)) {
				fwkName = (String) valueForBinding(BINDING_cssFwkName);
				if (!("app".equalsIgnoreCase(fwkName))) {
					fwkName = NSPathUtilities.stringByDeletingPathExtension(fwkName);
					fwkName = NSPathUtilities.stringByAppendingPathExtension(fwkName, "framework");
				}
			}
			CktlAjaxUtils.addStylesheetResourceInHead(context, response, fwkName, filename);
		}
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlReportGuiAjax", "css/FwkCktlReportGuiAjax.css");
	}

	/**
	 * @return Le binding <i>editingContext</i> s'il est renseigne, sinon l'editingContext de la session. Cette methode peut etre surchargee pour
	 *         renvoyer un nestedEditingContext.
	 */
	public ERXEC edc() {
		if (hasBinding(BINDING_editingContext)) {
			return (ERXEC) valueForBinding(BINDING_editingContext);
		}
		return (ERXEC) mySession().defaultEditingContext();
	}

	public FwkCktlReportApplicationUser getAppUser() {
		if (appUser == null) {
			if (hasBinding(BINDING_appUser)) {
				setappUser((FwkCktlReportApplicationUser) valueForBinding(BINDING_appUser));
			}
		}
		return appUser;
	}

	public void setappUser(FwkCktlReportApplicationUser appUser) {
		this.appUser = appUser;
	}

	//	public Integer getUtilisateurPersId() {
	//		if (hasBinding(BINDING_utilisateurPersId)) {
	//			return (Integer) valueForBinding(BINDING_utilisateurPersId);
	//		}
	//		if (getAppUser() != null) {
	//			return getAppUser().getPersId();
	//		}
	//		return null;
	//	}

	public String getContainerErreurMsgId() {
		if (containerErreurMsgId == null) {
			containerErreurMsgId = getComponentId() + "_containerErreurMsg";
		}
		return containerErreurMsgId;
	}

	public String getErreurSaisieMessage() {
		String tmp = erreurSaisieMessage;
		return tmp;
	}

	public void setErreurSaisieMessage(String erreurSaisieMessage) {
		this.erreurSaisieMessage = erreurSaisieMessage;
		if (!MyStringCtrl.isEmpty(erreurSaisieMessage)) {
			mySession().addSimpleErrorMessage("Erreur", erreurSaisieMessage);
		}
	}

	public String getCurrentExercice() {
		return (String) valueForBinding(BINDING_currentExercice);
	}

	public String getInfoSaisieMessage() {
		return infoSaisieMessage;
	}

	public void setInfoSaisieMessage(String infoSaisieMessage) {
		this.infoSaisieMessage = infoSaisieMessage;
		if (!MyStringCtrl.isEmpty(infoSaisieMessage)) {
			mySession().addSimpleInfoMessage("Info", infoSaisieMessage);
		}
	}

	public String jsUpdateUpdateContainerID() {
		if (updateContainerID() != null) {
			return "if ($('" + updateContainerID() + "')) {" + updateContainerID() + "Update();}";
		}
		return null;
	}

	/**
	 * Ne fait rien de particulier sauf la mise à jour du container updateContainerID si défini.
	 * 
	 * @return
	 */
	public WOActionResults postAndUpdate() {
		if (!MyStringCtrl.isEmpty(updateContainerID())) {
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		}
		return null;
	}

}
