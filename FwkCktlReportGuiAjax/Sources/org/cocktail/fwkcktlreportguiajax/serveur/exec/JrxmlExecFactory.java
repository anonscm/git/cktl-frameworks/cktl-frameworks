/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.exec;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktlreport.server.CktlReportRapportCriteres;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;
import org.cocktail.fwkcktlreport.server.files.CktlReportFilesUtils;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.ICktlReportingTaskListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import com.webobjects.foundation.NSArray;

public class JrxmlExecFactory extends AExecFactory {

	private JrxmlReporter reporter;
	private JrxmlReporterAjaxProgress reporterProgress;

	public JrxmlReporter lanceGeneration(FwkCktlReportApplicationUser appUser, EORapport rapport, EOTypeExport typeExport, CktlReportRapportCriteres criteres, Map<String, Object> params, NSArray<ICktlReportingTaskListener> listeners) throws Throwable {
		String reportFileName = CktlReportFilesUtils.pathOfRapportExtFile(rapport.toRapportJrxml(), appUser);

		Connection connection = getJDBCConnection();
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.putAll(params);
		aMap.putAll(criteres.preparedMapForReport());
		int formatExp = 0;
		if ("PDF".equals(typeExport.texStrId())) {
			formatExp = CktlAbstractReporter.EXPORT_FORMAT_PDF;
		}
		if ("XLS".equals(typeExport.texStrId())) {
			formatExp = CktlAbstractReporter.EXPORT_FORMAT_XLS;
		}
		setReporter(new JrxmlReporter());
		setReporterProgress(new JrxmlReporterAjaxProgress(100));
		if (listeners != null) {
			Iterator<ICktlReportingTaskListener> iter = listeners.iterator();
			while (iter.hasNext()) {
				ICktlReportingTaskListener iCktlReportingTaskListener = (ICktlReportingTaskListener) iter.next();
				getReporter().addListener(iCktlReportingTaskListener);
			}
		}
		getReporter().printWithThread("Construction de " + rapport.rapLibelle(), connection, null, reportFileName, aMap, formatExp, false, true, getReporterProgress());
		return getReporter();

	}

	public JrxmlReporter getReporter() {
		return reporter;
	}

	public void setReporter(JrxmlReporter reporter) {
		this.reporter = reporter;
	}

	public JrxmlReporterAjaxProgress getReporterProgress() {

		return reporterProgress;
	}

	public void setReporterProgress(JrxmlReporterAjaxProgress reporterProgress) {
		this.reporterProgress = reporterProgress;
	}

}
