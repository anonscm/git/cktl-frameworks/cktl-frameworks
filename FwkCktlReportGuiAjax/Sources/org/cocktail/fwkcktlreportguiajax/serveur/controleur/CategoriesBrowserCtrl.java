/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.controleur;

import org.cocktail.fwkcktlreport.server.metier.EOCategorie;
import org.cocktail.fwkcktlreportguiajax.serveur.component.CategoriesBrowser;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CategoriesBrowserCtrl {

	private static final NSArray<EOSortOrdering> SORT_CAT_LIBELLE = new NSArray<EOSortOrdering>(new EOSortOrdering[] {
			EOCategorie.SORT_CAT_LIBELLE_ASC
	});
	private CategoriesBrowser component;
	private NSArray<EOCategorie> allCategoriesAutorisees;
	private NSArray<EOCategorie> categoriesNiveau1;
	private NSArray<EOCategorie> categoriesNiveau2;
	private NSArray<EOCategorie> categoriesNiveau3;

	public CategoriesBrowserCtrl(CategoriesBrowser comp) {
		component = comp;
	}

	public NSArray<EOCategorie> getAllCategoriesAutorisees() {
		if (allCategoriesAutorisees == null) {
			NSMutableArray<EOCategorie> res = new NSMutableArray<EOCategorie>();
			NSArray<EOCategorie> allCategoriesAutoriseesTmp = component.getAppUser().getCategoriesAutorisees();
			//On ajoute les categories parentes pour la navigation si celles si ne sont pas présentes.
			for (int i = 0; i < allCategoriesAutoriseesTmp.count(); i++) {
				EOCategorie cat = (EOCategorie) allCategoriesAutoriseesTmp.objectAtIndex(i);
				while (cat.catNiveau().intValue() > 0) {
					if (!res.containsObject(cat)) {
						res.addObject(cat);
					}
					cat = cat.toCategoriePere();
				}
			}
			allCategoriesAutorisees = res.immutableClone();
		}
		return allCategoriesAutorisees;
	}

	public NSArray<EOCategorie> getCategoriesNiveau1() {
		if (categoriesNiveau1 == null) {
			categoriesNiveau1 = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(getAllCategoriesAutorisees(), EOCategorie.QUAL_NIVEAU1), SORT_CAT_LIBELLE);
			if (categoriesNiveau1.count() > 0) {
				component.setCat1((EOCategorie) categoriesNiveau1.objectAtIndex(0));
			}
		}
		return categoriesNiveau1;
	}

	public NSArray<EOCategorie> getCategoriesNiveau2() {
		return categoriesNiveau2;
	}

	public NSArray<EOCategorie> getCategoriesNiveau3() {
		return categoriesNiveau3;
	}

	public void setCategoriesNiveau2(NSArray<EOCategorie> categoriesNiveau2) {
		this.categoriesNiveau2 = categoriesNiveau2;
	}

	public void setCategoriesNiveau3(NSArray<EOCategorie> categoriesNiveau3) {
		this.categoriesNiveau3 = categoriesNiveau3;
	}

	public void updateCategoriesNiveau2() {
		NSArray<EOCategorie> res = null;
		EOCategorie cat = component.getCat1();
		if (cat != null) {
			EOQualifier qual = new EOKeyValueQualifier(EOCategorie.TO_CATEGORIE_PERE_KEY, EOQualifier.QualifierOperatorEqual, cat);
			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(getAllCategoriesAutorisees(), qual), SORT_CAT_LIBELLE);
		}
		setCategoriesNiveau2(res);
		if (categoriesNiveau2.count() > 0) {
			component.setCat2((EOCategorie) categoriesNiveau2.objectAtIndex(0));
		}
	}

	public void updateCategoriesNiveau3() {
		NSArray<EOCategorie> res = null;
		EOCategorie cat = component.getCat2();
		if (cat != null) {
			EOQualifier qual = new EOKeyValueQualifier(EOCategorie.TO_CATEGORIE_PERE_KEY, EOQualifier.QualifierOperatorEqual, cat);
			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(getAllCategoriesAutorisees(), qual), SORT_CAT_LIBELLE);
		}
		setCategoriesNiveau3(res);
	}

}
