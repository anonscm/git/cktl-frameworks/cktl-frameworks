/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.controleur;

import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreportguiajax.serveur.component.PubRapportSelectAndExec;
import org.cocktail.fwkcktlreportguiajax.serveur.component.PubRapportSelectList;

import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;

public class PubRapportSelectAndExecCtrl {
	private static NSArray SORT_RAPPORT_LIBELLE_ASC = new NSArray(new Object[] {
			EORapport.SORT_RAP_LIBELLE_ASC
	});

	private PubRapportSelectAndExec component;

	public PubRapportSelectAndExecCtrl(PubRapportSelectAndExec pubRapportSelectList) {
		component = pubRapportSelectList;
	}

	public WODisplayGroup getRapportsDg() {
		return component.getRapportsDg();
	}

	public WODisplayGroup getRapportsPretsDg() {
		return component.getRapportsPretsDg();
	}

	public void refreshData() {
		if (PubRapportSelectList.TAB_PLANIFICATION.equals(component.getSelectedTab())) {
			getRapportsPretsDg().setObjectArray(component.getAppUser().getAllRapportPrets());
		}
		else {
			NSArray<EORapport> res = NSArray.emptyArray();
			if (PubRapportSelectList.TAB_TOUS.equals(component.getSelectedTab())) {
				//Filtrer l'ensemble des rapports en fonction des categories autorisees
				//res = EORapport.fetchRapportsAutorisesPourUtilisateur(component.edc(), component.getAppUser().getPersId(), component.getSelectedCategorie(), SORT_RAPPORT_LIBELLE_ASC);
				res = component.getAppUser().getRapportAutorisesPourCategorie(component.getSelectedCategorie());
			}
			else if (PubRapportSelectList.TAB_FAVORITES.equals(component.getSelectedTab())) {
				//    EORapport.fetchAllRapportsFavorisPourUtilisateur(component.edc(), component.getAppUser(), SORT_RAPPORT_LIBELLE_ASC)
				res = component.getAppUser().getRapportsFavoris();
			}
			else if (PubRapportSelectList.TAB_HISTORIQUE.equals(component.getSelectedTab())) {
				res = component.getAppUser().getRapportsHistorique();
			}
			else if (PubRapportSelectList.TAB_RECHERCHER.equals(component.getSelectedTab())) {
				res = component.getAppUser().getAllRapportAutorises(component.getFiltre(), Integer.valueOf(50));
			}
			getRapportsDg().setObjectArray(res);
		}

	}

}
