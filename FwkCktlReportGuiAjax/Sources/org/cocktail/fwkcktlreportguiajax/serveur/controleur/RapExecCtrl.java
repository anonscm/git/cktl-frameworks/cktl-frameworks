/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.controleur;

import java.io.FileOutputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlreport.server.CktlReportRapportCriteres;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;
import org.cocktail.fwkcktlreport.server.metier.EORapportJrxml;
import org.cocktail.fwkcktlreport.server.metier.EORapportJxls;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;
import org.cocktail.fwkcktlreportguiajax.serveur.CktlReportMailMessage;
import org.cocktail.fwkcktlreportguiajax.serveur.component.RapExec;
import org.cocktail.fwkcktlreportguiajax.serveur.exec.AExecFactory;
import org.cocktail.fwkcktlreportguiajax.serveur.exec.JrxmlExecFactory;
import org.cocktail.fwkcktlreportguiajax.serveur.exec.JxlsExecFactory;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.ICktlReportingTaskListener;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class RapExecCtrl {
	public final static Logger logger = Logger.getLogger(RapExecCtrl.class);
	private RapExec component;
	private AExecFactory execFactory;
	private CktlReportRapportCriteres lastCriteres;

	public RapExecCtrl(RapExec comp) {
		component = comp;
	}

	public CktlAbstractReporter lanceGenerationRapport(IRapportExt iRapportExt, EOTypeExport eoTypeExport, NSArray<ICktlReportingTaskListener> listeners) throws Throwable {
		CktlReportRapportCriteres criteres = CktlReportRapportCriteres.createFrom(null, component.getAppUser().getCktlReportRapportCriteresGlobal());
		//On ne conserve que les criteres propres au rapport
		criteres.cleanCriteres(component.getRapport());
		//On ajoute les parametres
		criteres.addEntriesFromDictionary(new NSDictionary<String, Object>(component.getAppUser().getApplicationParams()));
		criteres.checkRequiredCriteres(component.getRapport());
		//	CktlReportParameters params = component.getAppUser().getCktlReportParameters();
		lastCriteres = criteres;
		if (iRapportExt instanceof EORapportJrxml) {
			execFactory = new JrxmlExecFactory();
			return execFactory.lanceGeneration(component.getAppUser(), component.getRapport(), eoTypeExport, criteres, component.getAppUser().getApplicationParams(), listeners);
		}
		if (iRapportExt instanceof EORapportJxls) {
			execFactory = new JxlsExecFactory();
			return execFactory.lanceGeneration(component.getAppUser(), component.getRapport(), eoTypeExport, criteres, component.getAppUser().getApplicationParams(), listeners);
		}
		return null;
	}

	public CktlAbstractReporter getReporter() {
		return (getExecFactory() == null ? null : getExecFactory().getReporter());
	}

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
		return (getExecFactory() == null ? null : getExecFactory().getReporterProgress());
	}

	public AExecFactory getExecFactory() {
		return execFactory;
	}

	public void setExecFactory(AExecFactory execFactory) {
		this.execFactory = execFactory;
	}

	public void prepareAndSendMail(CktlReportMailMessage cktlMailMessage, IRapportExt rapportExt) throws Throwable {
		if (cktlMailMessage != null) {
			cktlMailMessage.setContentType("text/html; charset=utf-8");
			cktlMailMessage.setEncoding("utf-8");
			CktlWebApplication app = ((CktlWebApplication) CktlWebApplication.application());
			StringBuilder body = new StringBuilder();
			body.append("Veuillez trouver ci-joint le résultat du rapport <b>" + rapportExt.toRapport().rapLibelle() + " </b><br>\n");
			if (rapportExt.toRapport().toRapportCriteres().count() > 0) {
				body.append("généré à partir des critères suivants :");
				Iterator<EORapportCritere> it = rapportExt.toRapport().toRapportCriteres().iterator();
				body.append("<ul>");
				while (it.hasNext()) {
					EORapportCritere rc = (EORapportCritere) it.next();
					Object value = lastCriteres.getValueForDisplay(rc.getCle());
					body.append("\n<li><b>" + rc.rcrLibelleToDisplay() + " </b>= " + (value == null ? "" : value) + "</li>");
				}
				body.append("</ul>");
			}
			body.append("\n<br>");
			body.append("\n<br><b>Généré par : </b>" + component.getAppUser().getNomAndPrenom());
			body.append("\n<br><b>Modèle utilisé : </b>" + rapportExt.getSrcFile());
			body.append("\n<br><b>Durée d'execution : </b>" + getReporter().getRunningTimeAsString());
			body.append("\n\n<br><br><hr>");
			body.append("\n<br><small>Ce message a été généré automatiquement par l'application " + app.name() + "</small>");

			String fileName = getReporter().getResultFileName();
			java.io.File tmpDir = java.io.File.createTempFile("fwkCktlReport_", null);
			tmpDir.delete();
			if (!tmpDir.mkdir()) {
				throw new Exception("Impossible de créer le répertoire " + tmpDir.getAbsolutePath());
			}
			tmpDir.deleteOnExit();
			java.io.File tmpFile = new java.io.File(tmpDir, fileName);
			if (logger.isDebugEnabled()) {
				logger.debug("Fichier " + tmpFile.getAbsolutePath() + " créé.");
			}

			FileOutputStream fileOutputStream = new FileOutputStream(tmpFile.getAbsolutePath());
			getExecFactory().getReporter().printDifferedGetDataResult().writeToStream(fileOutputStream);
			cktlMailMessage.setAttachements(new NSMutableArray<String>());
			cktlMailMessage.addAttachment(tmpFile.getAbsolutePath());
			cktlMailMessage.setSubject("[Rapport - " + app.name() + "] " + rapportExt.toRapport().rapLibelle());
			cktlMailMessage.setBody(body.toString());
			cktlMailMessage.prepare();
			cktlMailMessage.initAndSend(app.config().stringForKey(CktlConfig.CONFIG_GRHUM_HOST_MAIL_KEY));
			if (logger.isDebugEnabled()) {
				logger.debug("Report " + rapportExt.toRapport().rapStrId() + " envoyé par email à " + cktlMailMessage.getDestinataires().toString());
			}
			getExecFactory().getReporterProgress().setStatus("Report envoyé par email.");
			//setInfoSaisieMessage("Les message a bien été envoyé");
		}
	}
}
