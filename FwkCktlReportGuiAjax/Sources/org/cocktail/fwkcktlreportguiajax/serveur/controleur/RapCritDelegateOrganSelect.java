/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.controleur;

import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktlreportguiajax.serveur.component.RapCrit;

public class RapCritDelegateOrganSelect implements IRapCritDelegate {
	public static final String CA_ETAB = "CA_ETAB";
	public static final String CA_UB = "CA_UB";
	public static final String CA_CR = "CA_CR";
	public static final String CA_SOUSCR = "CA_SOUSCR";

	private RapCrit component;

	private EOOrgan selectedOrganInTree;
	public String tmpOrganValue;

	public RapCritDelegateOrganSelect(RapCrit comp) {
		component = comp;
	}

	public String getOrganSelectId() {
		return component.getComponentId() + "_organselect";
	}

	public Object convertParamValueToDisplayValue(Object value) {
		return value;
	}

	public Object convertDisplayValueToParamValueTo(Object value) {
		return value;
	}

	/**
	 * @return the selectedOrganInTree
	 */
	public EOOrgan getSelectedOrganInTree() {
		return selectedOrganInTree;
	}

	/**
	 * @param selectedOrganInTree the selectedOrganInTree to set
	 */
	public void setSelectedOrganInTree(EOOrgan selectedOrganInTree) {
		this.selectedOrganInTree = selectedOrganInTree;
		if (selectedOrganInTree != null) {
			component.rapportCriteresCache().put(CA_ETAB, selectedOrganInTree.orgEtab());
			component.rapportCriteresCache().put(CA_UB, selectedOrganInTree.orgUb());
			component.rapportCriteresCache().put(CA_CR, selectedOrganInTree.orgCr());
			component.rapportCriteresCache().put(CA_SOUSCR, selectedOrganInTree.orgSouscr());
		}
		else {
			component.rapportCriteresCache().put(CA_ETAB, null);
			component.rapportCriteresCache().put(CA_UB, null);
			component.rapportCriteresCache().put(CA_CR, null);
			component.rapportCriteresCache().put(CA_SOUSCR, null);
		}

	}

	public String getType() {
		String res = FinderOrgan.ORGAN_BUDGET;
		return res;
	}

	public Integer exerciceInt() {
		Object exer = component.rapportCriteresCache().getValue("CA_EXER");
		if (exer == null) {
			//exer = DateCtrl.getCurrentYear();
			return null;
		}
		return Integer.valueOf(exer.toString());
	}

	public Boolean isDisabled() {
		return (exerciceInt() == null);
	}
}
