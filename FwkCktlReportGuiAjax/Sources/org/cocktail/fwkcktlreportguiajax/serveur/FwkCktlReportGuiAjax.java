package org.cocktail.fwkcktlreportguiajax.serveur;

import org.apache.log4j.Logger;

import er.extensions.ERXFrameworkPrincipal;

public class FwkCktlReportGuiAjax extends ERXFrameworkPrincipal {

	public static final Logger logger = Logger.getLogger(FwkCktlReportGuiAjax.class);

	public static FwkCktlReportGuiAjaxParamManager paramManager = new FwkCktlReportGuiAjaxParamManager();

	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlReportGuiAjax.class);
	}

	@Override
	public void didFinishInitialization() {
		super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();
	}

	@Override
	public void finishInitialization() {
	}
}
