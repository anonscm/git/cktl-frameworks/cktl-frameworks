/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur;

import java.util.Vector;

import javax.mail.MessagingException;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector.DestinataireListe;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector.DestinataireListeElt;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Représente un email pour travail avant envoi.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportMailMessage {

	/**
	 * L'encodage du contenue du message (codepage).
	 */
	private String encoding = "utf-8";

	/**
	 * Le type de contenu du message.
	 */
	private String contentType;

	private NSMutableArray<String> attachements;

	private String subject;
	private String body;

	private NSMutableArray<String> arrayFroms = new NSMutableArray<String>();
	private NSMutableArray<String> arrayReplyTos = new NSMutableArray<String>();

	private CktlAjaxDestinatairesListeSelector.DestinataireListe destinataires = new DestinataireListe();

	private NSMutableDictionary<String, String> headers = new NSMutableDictionary<String, String>();

	public CktlReportMailMessage() {
		attachements = new NSMutableArray<String>();
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Indique le systeme de codage pour le text et le sujet du message envoye. Le codage par defaut est "utf-8".
	 * 
	 * @param encoding Une chaine le codage.
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void addCc(String nom, String ad) {
		if (ad != null) {
			getDestinataires().addDestinataire(DestinataireListeElt.DESTINATAIRE_OPTION_CC, nom, ad);
		}
	}

	public void addTo(String nom, String ad) {
		if (ad != null) {
			getDestinataires().addDestinataire(DestinataireListeElt.DESTINATAIRE_OPTION_TO, nom, ad);
		}
	}

	public void addBcc(String nom, String ad) {
		if (ad != null) {
			getDestinataires().addDestinataire(DestinataireListeElt.DESTINATAIRE_OPTION_BCC, nom, ad);
		}
	}

	public void addReplyTo(String ad) {
		if (ad != null) {
			arrayReplyTos.add(ad);
		}
	}

	public void addFroms(String ad) {
		if (ad != null) {
			arrayFroms.add(ad);
		}
	}

	public void removeCc(String ad) {
		if (ad != null) {
			getDestinataires().removeDestinataire(DestinataireListeElt.DESTINATAIRE_OPTION_CC, ad);
		}
	}

	public void removeTo(String ad) {
		getDestinataires().removeDestinataire(DestinataireListeElt.DESTINATAIRE_OPTION_TO, ad);
	}

	public void removeReplyTo(String ad) {
		arrayReplyTos.remove(ad);
	}

	public void removeBcc(String ad) {
		getDestinataires().removeDestinataire(DestinataireListeElt.DESTINATAIRE_OPTION_BCC, ad);
	}

	public void removeFrom(String ad) {
		arrayFroms.remove(ad);
	}

	/**
	 * Permet de definir un champ dans l'entete du message. La definition est constitue du couple <code>nom-valeur</code>.
	 * 
	 * @param name Le nom du champ de l'entete.
	 * @param value La valeur du champ de l'entete.
	 * @exception MessagingException
	 */
	public void setHeader(String name, String value)
			throws MessagingException {
		getHeaders().put(name, value);
	}

	public String getEncoding() {
		return encoding;
	}

	public String getContentType() {
		return contentType;
	}

	/**
	 * @return true si les messsage peut être envoyé
	 */
	public Boolean isMessageReady() {
		return (getDestinataires().count() >= 0);
	}

	public void checkMailReady() throws Exception {
		if (getDestinataires().count() == 0) {
			throw new Exception("Aucun destinataire");
		}
		if (getDestinataires().hasEmailsNonValides()) {
			throw new Exception("Certaines adresses ne sont pas valides");
		}
		if (MyStringCtrl.isEmpty(getSubject()) && MyStringCtrl.isEmpty(getBody()) && getAttachements().count() == 0) {
			throw new Exception("Le message est vide (ni sujet, ni corps, ni pièces jointes");
		}
	}

	public void checkForWarnings() throws Exception {
		if (getDestinataires().getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_TO).count() == 0) {
			throw new Exception("Le destinataire principal n'est pas spécifié");
		}
		if (getArrayFroms().count() == 0) {
			throw new Exception("L'expéditeur n'est pas spécifié");
		}
	}

	public void addAttachment(String fileName) {
		attachements.add(fileName);
	}

	public void removeAttachment(String fileName) {
		attachements.remove(fileName);
	}

	public NSArray<String> initAndSend(String smtpHost) throws MessagingException {
		CktlMailMessage mailMsg = new CktlMailMessage(smtpHost);
		mailMsg.setContentType(getContentType());
		mailMsg.setEncoding(getEncoding());

		String emailFrom = arrayFroms.objectAtIndex(0);
		String to = getArrayTos().componentsJoinedByString(",");
		
		String replyTo = getArrayReplyTos().componentsJoinedByString(",");
		
		String[] attachs = (String[]) (getAttachements().toArray(new String[getAttachements().size()]));
		
		if (!getArrayCcs().isEmpty()) {
			mailMsg.addCCs((String[]) getArrayCcs().toArray(new String[getArrayCcs().size()]));
		}
		if (!getArrayBccs().isEmpty()) {
			mailMsg.addBCCs((String[]) getArrayBccs().toArray(new String[getArrayBccs().size()]));
		}
		if (replyTo.length() > 0) {
			mailMsg.setReplyTo(replyTo);
		}
		mailMsg.initMessage(emailFrom, to, getSubject(), getBody(), attachs);

		NSArray<String> arrayNotSent = NSArray.emptyArray();

		Vector<String> notSent = mailMsg.safeSend();
		if (notSent != null) {
			arrayNotSent = new NSArray<String>(notSent);
		}
		return arrayNotSent;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public NSMutableArray<String> getAttachements() {
		return attachements;
	}

	public void setAttachements(NSMutableArray<String> attachements) {
		this.attachements = attachements;
	}

	public NSMutableArray<String> getArrayFroms() {
		return arrayFroms;
	}

	public void setArrayFroms(NSMutableArray<String> arrayFroms) {
		this.arrayFroms = arrayFroms;
	}

	//
	//	//
	public NSArray<String> getArrayTos() {
		return getDestinataires().getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_TO);
		//	//		return arrayTos;
	}

	public NSArray<String> getArrayCcs() {
		return getDestinataires().getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_CC);
		//	//		return arrayTos;
	}

	public NSArray<String> getArrayBccs() {
		return getDestinataires().getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_BCC);
		//	//		return arrayTos;
	}

	//	//
	//	//	public void setArrayTos(NSMutableArray<String> arrayTos) {
	//	//		this.arrayTos = arrayTos;
	//	//	}
	//	//
	//	//	public NSMutableArray<String> getArrayCCs() {
	//	//		return arrayCCs;
	//	//	}
	//	//
	//	//	public void setArrayCCs(NSMutableArray<String> arrayCCs) {
	//	//		this.arrayCCs = arrayCCs;
	//	//	}
	//	//
	//	//	public NSMutableArray<String> getArrayBCCs() {
	//	//		return arrayBCCs;
	//	//	}
	//	//
	//	//	public void setArrayBCCs(NSMutableArray<String> arrayBCCs) {
	//	//		this.arrayBCCs = arrayBCCs;
	//	//	}

	public NSMutableArray<String> getArrayReplyTos() {
		return arrayReplyTos;
	}

	public void setArrayReplyTos(NSMutableArray<String> arrayReplyTos) {
		this.arrayReplyTos = arrayReplyTos;
	}

	public NSMutableDictionary<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(NSMutableDictionary<String, String> headers) {
		this.headers = headers;
	}

	public CktlAjaxDestinatairesListeSelector.DestinataireListe getDestinataires() {
		return destinataires;
	}

	public void setDestinataires(CktlAjaxDestinatairesListeSelector.DestinataireListe destinataires) {
		this.destinataires = destinataires;
	}

	public void prepare() {
		getDestinataires().cleanAdressesVides();
	}
}
