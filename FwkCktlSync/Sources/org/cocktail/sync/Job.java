package org.cocktail.sync;

/**
 * Représente un travail à effectuer de manière synchrone ou non. Le caractères
 * asynchrone du lancement doit être géré en amont par le lanceur du travail.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * 
 */
public interface Job {

    /**
     * @return le nom du job.
     */
    String name();

    /**
     * Initialize le job à partir d'informations renseignées en amont
     * (constructeur)
     * 
     * @throws JobInitializationException
     */
    void init() throws JobInitializationException;

    /**
     * Lance le job et retourne le résultat.
     * 
     * @param params
     *            le tableau des paramètres
     * @return l'objet résultat
     * @throws JobRuntimeException
     *             si une erreur survient durant l'initialization du job.
     */
    Object launch(Object... params) throws JobRuntimeException;

}
