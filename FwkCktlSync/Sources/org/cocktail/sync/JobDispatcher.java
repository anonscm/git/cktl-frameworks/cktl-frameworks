package org.cocktail.sync;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import er.extensions.ERXFrameworkPrincipal;

/**
 * 
 * Point d'accès unique pour le lancement des jobs dans le code Java :
 * <code> JobDispatcher.instance().launchJob('un_job',param1,param2);</code>
 * En tant que client exécutant des jobs, c'est la seule classe à manipuler.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * 
 */
public class JobDispatcher extends ERXFrameworkPrincipal {

    private static Logger LOG = Logger.getLogger(JobDispatcher.class);
    private static JobDispatcher jobDispatcher;
    private List<JobCenter> jobCenters = new ArrayList<JobCenter>();

    private JobDispatcher() {

    }

    public static JobDispatcher instance() {
        if (jobDispatcher == null) {
            jobDispatcher = new JobDispatcher();
            // TODO : pour plus tard rajouter de la reflection ici
            // pour instancier dynamiquement les classes suffixées par JobCenter
            TOSJobCenter tosJb = new TOSJobCenter();
            // Si le job center est bien initialisé on l'ajoute
            if (tosJb.init())
                jobDispatcher.jobCenters.add(tosJb);
        }
        return jobDispatcher;
    }

    public Object launchJob(String job, Object... params)
            throws JobRuntimeException {
        Object result = null;
        for (JobCenter jobCenter : jobCenters) {
            if (jobCenter.containsJob(job)) {
                LOG.info("Le job center : " + jobCenter + "contient le job "
                        + job + " et sera lancé.");
                result = jobCenter.launchJob(job, params);
                break;
            }
        }
        return result;
    }

    @Override
    public void finishInitialization() {
        instance();
    }

}
