package org.cocktail.sync;

/**
 * Exception ayant lieu lors de l'initialization d'un job.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class JobInitializationException extends Exception {

    private static final long serialVersionUID = -315215712269500647L;
    
    public JobInitializationException(String message) {
        super(message);
    }

    public JobInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

}
