package org.cocktail.sync;

/**
 * Exception ayant lieu lors de l'exécution d'un Job.
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class JobRuntimeException extends Exception {

    private static final long serialVersionUID = -8162492606721504011L;
    
    public JobRuntimeException(String message) {
        super(message);
    }

    public JobRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
