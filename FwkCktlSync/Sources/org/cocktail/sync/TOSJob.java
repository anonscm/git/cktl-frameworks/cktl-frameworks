package org.cocktail.sync;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * 
 * Job spécifique Talend. Le job est configuré automatiquement à partir des
 * "bundles" exportés via Talend Open Studio. Un Bundle TOS (exporté avec la
 * version 4.0.x actuellement), doit avoir au minimum la forme suivante pour
 * être détecté comme tel par le moteur :
 * 
 * <pre>
 *      -un_job_de_synchro_0.2
 *          +lib
 *          -un_job_de_synchro
 *              classpath.jar
 *              +items
 * </pre>
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * 
 */
public class TOSJob implements Job {

    private static final String TOS_METHOD_NAME = "runJob";
    private static final String TOS_VERSION = "4";
    private static final String[] TOS_DEFAULT_PARAMS = new String[] {
            "--context=Default", "--context_param" };
    private static final Logger LOG = Logger.getLogger(TOSJob.class);

    private File jobFolder;
    private String nom;
    @SuppressWarnings("unchecked")
    private Class jobClass;
    private Method jobMethod;

    /**
     * @param jobFolder
     *            le dossier correspondant au Job
     */
    public TOSJob(File jobFolder) {
        this.jobFolder = jobFolder;
        if (jobFolder == null)
            throw new IllegalArgumentException(
                    "Un dossier correspondant au Job doit etre fourni");
    }

    /**
     * Initalisation des informations nécessaires au lancement du Job Talend, à
     * partir du bundle disponible. On peut imaginer si besoin une
     * implémentation plus rigide de cette initalisation et plus générique aussi
     * comme cela a été fait avec le serveur de synchro développé par Aix.
     * 
     * Le nom du Job correspond au sous dossier direct du <code>jobFolder</code>
     * 
     * @see org.cocktail.sync.Job#init()
     */
    public void init() throws JobInitializationException {
        String className = null;
        List<URL> classpathURLs = new ArrayList<URL>();
        // Par convention ça commence par sync...
        try {
            for (File file : jobFolder.listFiles()) {
                if (isTOSJobBundle(file)) {
                    nom = file.getName();
                    LOG.info("Job \"" + nom
                            + "\" trouvé ! Repertoire de base  : "
                            + file.getAbsolutePath());
                    LOG.info("Extraction des infos de ce Job Talend...");
                    className = extractClassFromJar(file, classpathURLs);
                } else if (file.isDirectory() && "lib".equals(file.getName())) {
                    // Ajout des jars dans le classpath
                    for (File jobFile : file.listFiles()) {
                        if (jobFile.getName().endsWith(".jar"))
                            classpathURLs.add(jobFile.toURI().toURL());
                    }
                }
            }
            if (nom == null || className == null)
                throw new JobInitializationException(
                        "Pas pu trouver de job au format TOS " + TOS_VERSION
                                + " dans le répertoire : "
                                + jobFolder.getAbsolutePath());
            // Une fois qu'on a toutes les infos on tente de récupérer la classe
            // et
            // la méthode
            initClassAndMethod(className, classpathURLs);
        } catch (MalformedURLException e) {
            throw new JobInitializationException(e.getMessage(), e);
        }
    }

    /**
     * @param file
     *            le fichier ou dossier à vérifier
     * @return true si c'est un dossier au format TOS.
     */
    private boolean isTOSJobBundle(File file) {
        if (file.isDirectory()) {
            List<String> childrenFiles = Arrays.asList(file.list());
            return childrenFiles.contains("classpath.jar")
                    && childrenFiles.contains("items");
        }
        return false;
    }

    /**
     * @param folder
     * @param classpathURLs
     * @return
     * @throws JobInitializationException
     */
    private String extractClassFromJar(File folder, List<URL> classpathURLs)
            throws JobInitializationException {
        String className = null;
        try {
            for (File jobFile : folder.listFiles()) {
                String jobFileName = jobFile.getName();
                // Récupération de la version dans le nom du jar
                if (jobFileName.startsWith(nom) && jobFileName.endsWith(".jar")) {
                    // Récupération du nom de la classe dans le jar !
                    JarFile jarFile = new JarFile(jobFile);
                    Enumeration<JarEntry> enumeration = jarFile.entries();
                    while (enumeration.hasMoreElements()) {
                        String entryName = enumeration.nextElement().getName();
                        if (entryName.endsWith(nom + ".class")) {
                            className = entryName.replace("/", ".").split(
                                    ".class")[0];
                            LOG.info("Classe correspondant au Job \"" + nom
                                    + "\" trouvée : " + className);
                        }
                    }
                    if (className == null)
                        throw new JobInitializationException(
                                "Aucune classe Java "
                                        + "trouvée pour le Job : " + nom);
                    // Ajout du jar dans le classpath
                    classpathURLs.add(jobFile.toURI().toURL());
                    // Ajout du projet dans le cp pour permettre de retrouver
                    // les fichiers de conf...
                    classpathURLs.add(folder.toURI().toURL());
                }
            }
        } catch (MalformedURLException e) {
            throw new JobInitializationException(e.getMessage(), e);
        } catch (IOException e) {
            throw new JobInitializationException(e.getMessage(), e);
        }
        return className;
    }

    /**
     * Essaie de charger la classe du job Talend à lancer.
     * @param className
     * @param classpathURLs
     * @throws JobInitializationException
     */
    @SuppressWarnings("unchecked")
    private void initClassAndMethod(String className, List<URL> classpathURLs)
            throws JobInitializationException {
        URLClassLoader classLoader = new URLClassLoader((URL[]) classpathURLs
                .toArray(new URL[0]));
        try {
            this.jobClass = classLoader.loadClass(className);
            this.jobMethod = jobClass
                    .getMethod(TOS_METHOD_NAME, String[].class);
        } catch (ClassNotFoundException e) {
            throw new JobInitializationException(
                    "Pb de chargement de la classe du Job", e);
        } catch (SecurityException e) {
            throw new JobInitializationException(
                    "Pb de chargement de la méthode du Job", e);
        } catch (NoSuchMethodException e) {
            throw new JobInitializationException(
                    "Pb de chargement de la méthode du Job", e);
        }
    }

    /**
     * Méthode appelée pour lancer un job.
     * 
     * @see org.cocktail.sync.Job#launch(java.lang.Object[])
     */
    public Object launch(Object... params) throws JobRuntimeException {
        Object result = null;
        // Redirection temporaire des sorties vers log4j
        PrintStream oldOut = System.out;
        PrintStream oldErr = System.err;
        System.setOut(new PrintStream(new LoggingOutputStream(LOG, Level.INFO),
                true));
        System.setErr(new PrintStream(
                new LoggingOutputStream(LOG, Level.ERROR), true));
        Exception taskLaunchException = null;
        try {
            Object o = null;
            if (!Modifier.isStatic(jobMethod.getModifiers())) {
                // si methode non statique, il faut instancier un objet
                o = jobClass.newInstance();
            }
            // Mise en forme des params
            Object[] tosParams = computeParamsForTOS(params);
            result = jobMethod.invoke(o, tosParams);
        } catch (Exception ex) {
            ex.printStackTrace();
            // Attention quand même : une tâche qui se termine sans renvoyer
            // d'exception ne s'est pas
            // pour autant bien déroulée (les exception sont généralement
            // catchees par la tâche...)
            taskLaunchException = ex;
            // on laisse une trace dans le log des taches...
        }
        // restoration des System.out/err
        System.setOut(oldOut);
        System.setErr(oldErr);
        if (taskLaunchException != null) {
            // maintenant qu'on a restoré les System.out/err, on peut remonter
            // l'exception
            throw new JobRuntimeException(
                    "Une erreur est survenue pendant l'exécution du job",
                    taskLaunchException);
        }
        return result;
    }

    /** 
     * @see org.cocktail.sync.Job#name()
     */
    public String name() {
        return nom;
    }

    /**
     * @param param
     * @return
     */
    private Object[] computeParamsForTOS(Object...params) {
        String[] copy = new String[TOS_DEFAULT_PARAMS.length + params.length];
        // Copie des paramètres par défaut
        System.arraycopy(TOS_DEFAULT_PARAMS, 0, copy, 0, TOS_DEFAULT_PARAMS.length);
        // Copie des paramètres donnés en entrée 
        System.arraycopy(params, 0, copy, TOS_DEFAULT_PARAMS.length, params.length);
        return new Object[] { copy };
    }
    
}
