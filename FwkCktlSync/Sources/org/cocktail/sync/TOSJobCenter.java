package org.cocktail.sync;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOApplication;

/**
 * Centre de lancement des travaux de type Talend.
 * Ce centre est activé dès que le paramètre système TOS_JOB_CENTER_FOLDER pointe
 * vers un dossier valide.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class TOSJobCenter implements JobCenter {

    private static final Logger LOG = Logger.getLogger(TOSJobCenter.class);
    private static final String TOS_JOB_CENTER_FOLDER = "TOS_JOB_CENTER_FOLDER";
    private Map<String, Job> jobs = new HashMap<String, Job>();

    /**
     * Lancement d'un job Talend.
     * 
     * @see org.cocktail.sync.JobCenter#launchJob(java.lang.String,
     *      java.lang.Object[])
     */
    public Object launchJob(String job, Object... params)
            throws JobRuntimeException {
        Object result = null;
        if (jobs.get(job) == null)
            throw new JobRuntimeException("Le job " + job + " n'existe pas !");
        try {
            result = jobs.get(job).launch(params);
        } catch (JobRuntimeException e) {
            // L'erreur est loggé pour débug, doit être traîtée et loggée 
            // par l'appelant
            LOG.debug(e.getMessage(), e);
            throw e;
        }
        return result;
    }

    /** 
     * @see org.cocktail.sync.JobCenter#init()
     */
    public boolean init() {
        String param = System.getProperty(TOS_JOB_CENTER_FOLDER);
        if (param == null) {
            WOApplication app = WOApplication.application();
            if (app instanceof CktlWebApplication) {
                param = ((CktlWebApplication)app).config().stringForKey(TOS_JOB_CENTER_FOLDER);
            }
        }
        if (param != null) {
            File jobsFolder = new File(param);
            if (jobsFolder.exists()) {
                loadJobs(jobsFolder);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Chargement des jobs Talend à partir du dossier de base
     * <code>JobsFolder</code> donné.
     */
    private void loadJobs(File jobsFolder) {
        // Récupération de tous les sous dossier
        for (File file : jobsFolder.listFiles()) {
            if (file.isDirectory()) {
                // On vérifie qu'il y a bien 2 sous dossiers : un lib et un
                // autre
                // l'autre étant le job à enregistrer
                if (file.listFiles().length >= 2) {
                    Job job = new TOSJob(file);
                    try {
                        job.init();
                        jobs.put(job.name(), job);
                    } catch (JobInitializationException e) {
                        LOG.warn("Impossible de charger le job correspondant "
                                + "au repertoire " + file.getPath(), e);
                    }
                } else {
                    LOG.warn("Pas de Jobs Talend trouvé dans le répertoire "
                            + file.getAbsolutePath());
                }
            }
        }
    }

    /** 
     * @see org.cocktail.sync.JobCenter#containsJob(java.lang.String)
     */
    public boolean containsJob(String job) {
        return jobs.containsKey(job);
    }

}
