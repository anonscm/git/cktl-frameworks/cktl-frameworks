package org.cocktail.sync.ref;

import org.cocktail.sync.JobDispatcher;
import org.cocktail.sync.JobRuntimeException;

public class SynchroPasswordPersonne {

    private static final String SYNC_PASSWORD_JOB_NAME = System.getProperty("SYNC_PASSWORD_JOB_NAME") != null ? System
            .getProperty("SYNC_PASSWORD_JOB_NAME") : "sync_personne_password";

    public static void synchroPasswordPersonne(String persId) throws JobRuntimeException {
        JobDispatcher.instance().launchJob(SYNC_PASSWORD_JOB_NAME, "individu_persid=" + persId);
    }

}
