package org.cocktail.sync.ref;

import org.cocktail.sync.JobDispatcher;
import org.cocktail.sync.JobRuntimeException;

public class SynchroComptePersonne {

    private static final String SYNC_COMPTE_JOB_NAME = 
            System.getProperty("SYNC_COMPTE_JOB_NAME") != null ?
            System.getProperty("SYNC_COMPTE_JOB_NAME") : "sync_personne_activcompte";
    
    public static void synchroComptePersonne(String persId) throws JobRuntimeException {
        JobDispatcher.instance().launchJob(SYNC_COMPTE_JOB_NAME, "individu_persid=" + persId);
    }
    
}
