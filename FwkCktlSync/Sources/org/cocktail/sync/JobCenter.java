package org.cocktail.sync;

/**
 * Représente un centre de lancement des travaux. Chaque implémentation de
 * centre de lancement "sait" comment récupérer et lancer les travaux de son
 * type.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public interface JobCenter {

    /**
     * @param job le nom du job
     * @return true si ce job center gère ce job
     */
    boolean containsJob(String job);
    
    /**
     * @param job
     *            le nom du job à lancer
     * @param params
     *            les paramètres à lui passer
     * @return le résultat
     * @throws JobRuntimeException
     *             une exception durant l'exécution du job.
     */
    Object launchJob(String job, Object... params) throws JobRuntimeException;

    /**
     * S'initialise en fonction de paramètre de conf présent ou non (par
     * exemple). Si dispo, charge les jobs dont il a la charge.
     * 
     * @return true si le JobCenter s'est bien chargée et est disponible
     *         (contient des jobs).
     */
    boolean init();

}
