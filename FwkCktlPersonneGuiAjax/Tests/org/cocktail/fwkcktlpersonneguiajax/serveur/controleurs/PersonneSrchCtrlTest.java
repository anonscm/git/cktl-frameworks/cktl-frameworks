package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneSrch;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

@RunWith(MockitoJUnitRunner.class)
public class PersonneSrchCtrlTest {

  @Rule
  public MockEditingContext edc = new MockEditingContext("FwkCktlPersonne");

  @Dummy
  EOVlans vlanP;
  @Dummy
  EOVlans vlanX;
  @Dummy
  EOVlans vlanE;
  @Dummy
  EOVlans vlanR;

  
  @Dummy
  EOTypeStructure typeStructureComposante;
  
  @Dummy
  EOTypeGroupe typeGroupeEntreprise;
  
  Random random = new Random();
  
  
  @Before
  public void setup() {

    // VLANS
    vlanP.setCVlan(EOVlans.VLAN_P);
    vlanX.setCVlan(EOVlans.VLAN_X);
    vlanE.setCVlan(EOVlans.VLAN_E);
    vlanR.setCVlan(EOVlans.VLAN_R);

    // TYPES STRUCTURES
    typeStructureComposante.setCTypeStructure(EOTypeStructure.TYPE_STRUCTURE_C);
    
    // TYPES GROUPES
    typeGroupeEntreprise.setTgrpCode(EOTypeGroupe.TGRP_CODE_EN);
    
    // INDIVIDUS
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "ABADIE", "HUBERT", vlanP);
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "ABADIE", "ALBERT", vlanP);
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "ABADIE", "GERARD", vlanX);
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "ABADIE", "ROBERT", vlanX);
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "BORDES", "MARCELLE", vlanE);
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "CAZES", "MICHEL", vlanR);
    creerIndividuAvecNomEtPrenomEtCompteSurVlan(edc, "SANCERE", "PAUL", vlanX);

    creerStructureInterne(edc,"ACME", "ACME", "1");
    creerStructureExterne(edc,"BOUCHERIE SANZO", "BOUCHERIE SANZO", "2");
    creerStructureExterne(edc,"ACME", "ACME", "3");
    
    
  }

  private EOStructure creerStructureInterne(MockEditingContext edc, String llStructure, String lcStructure, String cStructure) {
    EOStructure structure = spy(new EOStructure());

    doReturn(cStructure).when(structure).construireCStructure();
    
    edc.insertSavedObject(structure);
    
    structure.setLlStructure(llStructure);
    structure.setLcStructure(lcStructure);
    structure.setTemValide(EOStructure.TEM_VALIDE_OUI);
    structure.setCTypeStructure(EOTypeStructure.TYPE_STRUCTURE_C);
    structure.setToTypeStructureRelationship(typeStructureComposante);

    return structure;
  }

  
  private EOStructure creerStructureExterne(MockEditingContext edc, String llStructure, String lcStructure, String cStructure) {
    EOStructure structure = spy(new EOStructure());

    doReturn(cStructure).when(structure).construireCStructure();
    
    edc.insertSavedObject(structure);
    
    structure.setLlStructure(llStructure);
    structure.setLcStructure(lcStructure);
    structure.setTemValide(EOStructure.TEM_VALIDE_OUI);

    EORepartTypeGroupe repartTypeGroupe = edc.createSavedObject(EORepartTypeGroupe.class);
    repartTypeGroupe.setTgrpCode(EOTypeGroupe.TGRP_CODE_EN);
    repartTypeGroupe.setTypeGroupeRelationship(typeGroupeEntreprise);

    structure.addToToRepartTypeGroupesRelationship(repartTypeGroupe);
    
    return structure;
  }

  
  @Test
  public void checkRechercheIndividuParNomPrenomPlaceIndividuExterneExactAuDebut() {

    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("ABADIE");
    when(personneSrch.getSrchPrenom()).thenReturn("GERARD");

    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourIndividu(personneSrch, false, false, true);

    NSArray<EOIndividu> resultats = personneSrchCtrl.getResultats();

    assertEquals(4, resultats.size());
    assertEquals("GERARD", resultats.get(0).prenom());

  }

  @Test
  public void checkConditionsInterneExterne() {

    PersonneSrchCtrl personneSrchCtrl = new PersonneSrchCtrl();

    // Recherche tout
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(true, true, true, true, true));
    assertTrue(personneSrchCtrl.isRechercheIndividuSimple());
    assertTrue(personneSrchCtrl.isRechercheIndividuInterne());
    assertTrue(personneSrchCtrl.isRechercheIndividuExterne());
    assertTrue(personneSrchCtrl.isRechercheStructureSimple());
    assertTrue(personneSrchCtrl.isRechercheMoraleInterne());
    assertTrue(personneSrchCtrl.isRechercheMoraleExterne());

    // Recherche tous les individus
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(true, true, true, true, false));
    assertTrue(personneSrchCtrl.isRechercheIndividuSimple());
    assertTrue(personneSrchCtrl.isRechercheIndividuInterne());
    assertTrue(personneSrchCtrl.isRechercheIndividuExterne());
    assertFalse(personneSrchCtrl.isRechercheStructureSimple());
    assertFalse(personneSrchCtrl.isRechercheMoraleInterne());
    assertFalse(personneSrchCtrl.isRechercheMoraleExterne());

    personneSrchCtrl.setMyComponent(getNewPersonneSrch(true, false, false, true, false));
    assertTrue(personneSrchCtrl.isRechercheIndividuSimple());
    assertTrue(personneSrchCtrl.isRechercheIndividuInterne());
    assertTrue(personneSrchCtrl.isRechercheIndividuExterne());
    assertFalse(personneSrchCtrl.isRechercheStructureSimple());
    assertFalse(personneSrchCtrl.isRechercheMoraleInterne());
    assertFalse(personneSrchCtrl.isRechercheMoraleExterne());

    // Recherche les individus internes
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(false, true, false, true, false));
    assertFalse(personneSrchCtrl.isRechercheIndividuSimple());
    assertTrue(personneSrchCtrl.isRechercheIndividuInterne());
    assertFalse(personneSrchCtrl.isRechercheIndividuExterne());
    assertFalse(personneSrchCtrl.isRechercheStructureSimple());
    assertFalse(personneSrchCtrl.isRechercheMoraleInterne());
    assertFalse(personneSrchCtrl.isRechercheMoraleExterne());

    // Recherche les individus externes
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(false, false, true, true, false));
    assertFalse(personneSrchCtrl.isRechercheIndividuSimple());
    assertFalse(personneSrchCtrl.isRechercheIndividuInterne());
    assertTrue(personneSrchCtrl.isRechercheIndividuExterne());
    assertFalse(personneSrchCtrl.isRechercheStructureSimple());
    assertFalse(personneSrchCtrl.isRechercheMoraleInterne());
    assertFalse(personneSrchCtrl.isRechercheMoraleExterne());

    // Recherche toutes les structures
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(true, true, true, false, true));
    assertFalse(personneSrchCtrl.isRechercheIndividuSimple());
    assertFalse(personneSrchCtrl.isRechercheIndividuInterne());
    assertFalse(personneSrchCtrl.isRechercheIndividuExterne());
    assertTrue(personneSrchCtrl.isRechercheStructureSimple());    
    assertTrue(personneSrchCtrl.isRechercheMoraleInterne());
    assertTrue(personneSrchCtrl.isRechercheMoraleExterne());
    
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(true, false, false, false, true));
    assertFalse(personneSrchCtrl.isRechercheIndividuSimple());
    assertFalse(personneSrchCtrl.isRechercheIndividuInterne());
    assertFalse(personneSrchCtrl.isRechercheIndividuExterne());
    assertTrue(personneSrchCtrl.isRechercheStructureSimple());    
    assertTrue(personneSrchCtrl.isRechercheMoraleInterne());
    assertTrue(personneSrchCtrl.isRechercheMoraleExterne());
    
    // Recherche les structures internes
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(false, true, false, false, true));
    assertFalse(personneSrchCtrl.isRechercheIndividuSimple());
    assertFalse(personneSrchCtrl.isRechercheIndividuInterne());
    assertFalse(personneSrchCtrl.isRechercheIndividuExterne());
    assertFalse(personneSrchCtrl.isRechercheStructureSimple());
    assertTrue(personneSrchCtrl.isRechercheMoraleInterne());
    assertFalse(personneSrchCtrl.isRechercheMoraleExterne());

    // Recherche les structures externes
    personneSrchCtrl.setMyComponent(getNewPersonneSrch(false, false, true, false, true));
    assertFalse(personneSrchCtrl.isRechercheIndividuSimple());
    assertFalse(personneSrchCtrl.isRechercheIndividuInterne());
    assertFalse(personneSrchCtrl.isRechercheIndividuExterne());
    assertFalse(personneSrchCtrl.isRechercheStructureSimple());
    assertFalse(personneSrchCtrl.isRechercheMoraleInterne());
    assertTrue(personneSrchCtrl.isRechercheMoraleExterne());


  }
  
  @Test
  public void jePeuxRechercherDesPersonnesPhysiquesInternes() {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("BORDES");
    when(personneSrch.getSrchPrenom()).thenReturn("MARCELLE");

    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourIndividu(personneSrch, false, true, false);

    
    NSMutableArray<EOIndividu> resultats = personneSrchCtrl.rechercherPersonnesPhysiquesInternes(null, null);
    assertEquals(1, resultats.size());
    assertEquals("BORDES", resultats.get(0).nomUsuel());

  }
  
  
  @Test
  public void jePeuxRechercherDesPersonnesPhysiquesExternes() {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("SANCERE");
    when(personneSrch.getSrchPrenom()).thenReturn("PAUL");

    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourIndividu(personneSrch, true, false, false);

    
    NSMutableArray<EOIndividu> resultats = personneSrchCtrl.rechercherPersonnesPhysiquesExternes(null, null);
    assertEquals(1, resultats.size());
    assertEquals("SANCERE", resultats.get(0).nomUsuel());

  }

  
  @Test
  public void jePeuxRechercherDesPersonnesPhysiques() {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("ABADIE");

    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourIndividu(personneSrch, false, false, true);
    
    NSMutableArray<EOIndividu> resultats = personneSrchCtrl.rechercherTousLesIndividus(null);
    assertEquals(4, resultats.size());

  }
  
  
  
  @Test
  public void jePeuxRechercherUneStructureInterneParSonNom() {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("ACME");
    
    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourStructure(personneSrch, false, true, false);
    NSMutableArray<EOStructure> resultats = personneSrchCtrl.recherchePersonnesMoralesInternes(null, null);
    assertEquals(1, resultats.size());
    assertEquals("ACME", resultats.get(0).llStructure());
    
    
    


  }
  
  @Test
  public void jePeuxRechercherUneStructureExterneParSonNom() {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("BOUCHERIE SANZO");
    
    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourStructure(personneSrch, true, false, false);
    NSMutableArray<EOStructure> resultats = personneSrchCtrl.rechercherPersonnesMoralesExternes(null, null);
    assertEquals(1, resultats.size());
    assertEquals("BOUCHERIE SANZO", resultats.get(0).llStructure());


  }
 
 
  
  @Test
  public void jePeuxRechercherUneStructureParSonNom() {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.getSrchNom()).thenReturn("ACME");
    
    PersonneSrchCtrl personneSrchCtrl = getNewPersonneSrchCtrlPourStructure(personneSrch, false, false, true);
    NSArray<EOStructure> resultats = personneSrchCtrl.rechercherPersonnesMorales(null, null);
    assertEquals(2, resultats.size());
    assertEquals("ACME", resultats.get(0).llStructure());
    assertEquals("ACME", resultats.get(1).llStructure());

    resultats = personneSrchCtrl.getResultats();
    assertEquals(2, resultats.size());
    assertEquals("ACME", resultats.get(0).llStructure());
    assertEquals("ACME", resultats.get(1).llStructure());
    
  }

  /**
   * Cree un stub de PersonneSrchCtrl pour la recherche d'individus
   * @param personneSrch
   * @param isRechercheIndividuExterne
   * @param isRechercheIndividuInterne
   * @param isRechercheIndividuSimple
   * @return
   */
  public PersonneSrchCtrl getNewPersonneSrchCtrlPourStructure(PersonneSrch personneSrch, boolean isRechercheStructureExterne, boolean isRechercheStructureInterne, boolean isRechercheStructureSimple) {
    PersonneApplicationUser personneApplicationUser = mock(PersonneApplicationUser.class);
    
    PersonneSrchCtrl personneSrchCtrl = spy(new PersonneSrchCtrl(personneSrch) {
      @Override
      protected NSMutableArray<EOStructure> filtrerLesStructuresNonAffichables(NSMutableArray res) {
        return res;
      }
    });
    doReturn(edc).when(personneSrchCtrl).edc();
    doReturn(isRechercheStructureExterne).when(personneSrchCtrl).isRechercheMoraleExterne();
    doReturn(isRechercheStructureInterne).when(personneSrchCtrl).isRechercheMoraleInterne();
    doReturn(isRechercheStructureSimple).when(personneSrchCtrl).isRechercheStructureSimple();
    return personneSrchCtrl;
  }

  /**
   * Cree un stub de PersonneSrchCtrl pour la recherche de structures
   * @param personneSrch
   * @param isRechercheIndividuExterne
   * @param isRechercheIndividuInterne
   * @param isRechercheIndividuSimple
   * @return
   */
  public PersonneSrchCtrl getNewPersonneSrchCtrlPourIndividu(PersonneSrch personneSrch, boolean isRechercheIndividuExterne, boolean isRechercheIndividuInterne, boolean isRechercheIndividuSimple) {
    PersonneSrchCtrl personneSrchCtrl = spy(new PersonneSrchCtrl(personneSrch));
    doReturn(edc).when(personneSrchCtrl).edc();
    doReturn(isRechercheIndividuExterne).when(personneSrchCtrl).isRechercheIndividuExterne();
    doReturn(isRechercheIndividuInterne).when(personneSrchCtrl).isRechercheIndividuInterne();
    doReturn(isRechercheIndividuSimple).when(personneSrchCtrl).isRechercheIndividuSimple();
    return personneSrchCtrl;
  }
  
  
  /**
   * Cree un mock de PersonneSrch
   * @param searchOnInterneAndExterne
   * @param persTypeInterneSelected
   * @param persTypeExterneSelected
   * @param persTypePhysiqueSelected
   * @param persTypeMoraleSelected
   * @return
   */
  public PersonneSrch getNewPersonneSrch(Boolean searchOnInterneAndExterne, Boolean persTypeInterneSelected, Boolean persTypeExterneSelected, Boolean persTypePhysiqueSelected, Boolean persTypeMoraleSelected) {
    PersonneSrch personneSrch = mock(PersonneSrch.class);
    when(personneSrch.searchOnInterneAndExterne()).thenReturn(searchOnInterneAndExterne);
    when(personneSrch.getPersTypeExterneSelected()).thenReturn(persTypeExterneSelected);
    when(personneSrch.getPersTypeInterneSelected()).thenReturn(persTypeInterneSelected);
    when(personneSrch.getPersTypePhysiqueSelected()).thenReturn(persTypePhysiqueSelected);
    when(personneSrch.getPersTypeMoraleSelected()).thenReturn(persTypeMoraleSelected);
    return personneSrch;
  }

  
  /**
   * Cree un indidu avec un compte sur le vlan specifie
   * @param edc
   * @param nom
   * @param prenom
   * @param vlan
   * @return
   */
  public EOIndividu creerIndividuAvecNomEtPrenomEtCompteSurVlan(MockEditingContext edc, String nom, String prenom, EOVlans vlan) {
    EOIndividu individu = edc.createSavedObject(EOIndividu.class);
    individu.setNomUsuel(nom);
    individu.setPrenom(prenom);
    individu.setTemValide(EOIndividu.TEM_VALIDE_O);
    EOCompte compte = edc.createSavedObject(EOCompte.class);
    individu.addToToComptesRelationship(compte);
    compte.setToVlansRelationship(vlan);
    return individu;
  }
}
