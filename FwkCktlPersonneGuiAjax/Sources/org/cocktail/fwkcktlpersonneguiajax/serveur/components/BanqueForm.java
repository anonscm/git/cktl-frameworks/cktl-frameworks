/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.controles.ControleBIC;
import org.cocktail.fwkcktlpersonne.common.metier.EOBanque;

import com.webobjects.appserver.WOContext;

public class BanqueForm extends AComponent {

	private static final String BINDING_BANQUE = "banque";
	private Boolean isTabBicSelected = Boolean.TRUE;
	private Boolean isTabCoordonneesSelected;

	public BanqueForm(WOContext arg0) {
		super(arg0);
	}

	public EOBanque getBanque() {
		return (EOBanque) valueForBinding(BINDING_BANQUE);
	}

	public String getTabId() {
		return getComponentId() + "_tabs";
	}

	public String getTabBicId() {
		return getTabId() + "_bic";
	}

	public String getTabCoordonneesId() {
		return getTabId() + "_coordonnees";
	}

	public void setTabBicSelected(Boolean val) {
		this.isTabBicSelected = val;
	}

	public Boolean tabBicSelected() {
		return isTabBicSelected;
	}

	public void setTabCoordonneesSelected(Boolean val) {
		this.isTabCoordonneesSelected = val;
	}

	public Boolean tabCoordonneesSelected() {
		return isTabCoordonneesSelected;
	}

	public String getStyleTab() {
		return "#" + getTabId() + "_panecontrol {min-height: 20px;}";
	}

	public Boolean showFormatFR() {
		return ControleBIC.isInZoneFR(getBanque().bic(), edc());
	}

	public String getFormatFrContainerId() {
		return getComponentId() + "_frContainer";
	}
}
