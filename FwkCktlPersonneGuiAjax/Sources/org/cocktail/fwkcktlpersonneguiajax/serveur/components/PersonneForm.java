/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneFormCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Formulaire de création d'une personne. Il s'agit d'un composant qui regroupe d'autres composants, c'est un formulaire par défaut, vous devrez
 * vraissemblablement utiliser les sous-composants en direct.
 * 
 * @binding utilisateurPersId
 * @binding selectedPersonne La personne à modifier
 * @binding callbackOnEnregistrer Nom d'une action sur le composant parent à executer lors de l'enregisrement de la saisie
 * @binding callbackOnAnnuler Nom d'une action sur le composant parent à executer lors de l'annulation de la saisie
 * @binding editingContext
 * @binding onFormAnnulerSuccess Script Javascript à executer lors de l'annulation de la saisie
 * @binding onFormEnregistrerSuccess Script Javascript à executer lors de l'enregisrement de la saisie
 * @binding updateContainerID Container à mettre à jour apres enregistrement ou annulation
 * @binding displayMessageOnParent will set : Permet d'afficher un message sur le composant appelant une fois l'enregistrement bien effectué.
 * @binding ctrl Controleur du composant. Doit être une sous-classe de {@link PersonneFormCtrl}
 * @binding formID ID de la form qui va contenir le formulaire
 * @binding showcompteUI Indique s'il faut afficher les informations sur les comptes dans le détail de la personne.
 * @binding PersonneAdminUI_disableAutoCompleteFields Si true, active la saisie des champs du composant PersonneAdminUI via des fenetres de selection
 *          plutot que via des autocomplete.
 * @author rprin
 */
public class PersonneForm extends AComponent {

	private static final long serialVersionUID = -6258213589636933537L;
	public final static String BINDING_callbackOnEnregistrer = "callbackOnEnregistrer";
	public final static String BINDING_callbackOnAnnuler = "callbackOnAnnuler";
	public static final String BINDING_displayMessageOnParent = "displayMessageOnParent";

	/** Container a mettre a jour suite a clic sur Enregistrer ou Annuler */
	public static final String BINDING_updateContainerID = "updateContainerID";

	/**
	 * Facultatif. Instance du controleur a utiliser (doit surcharger PersonneFormCtrl).
	 */
	public static final String BINDING_ctrl = "ctrl";

	/**
	 * Indique s'il faut afficher le compte dans le détail de la personne. Par defaut à FALSE.
	 */
	public static final String BINDING_showCompteUI = "showCompteUI";

	public static final Boolean DEFAULT_showCompteUI = Boolean.FALSE;

	private String callbackOnEnregistrer;
	private String callbackOnAnnuler;

	public EOCivilite unCivilite;
	public EORepartPersonneAdresse currentItemRepartPersonneAdresse;
	public EOCompte selectedCompte;

	private PersonneFormCtrl ctrl;
	private Boolean isAdresseEditing = Boolean.FALSE;
	private Boolean isTelephoneEditing = Boolean.FALSE;
	private Boolean isNomEditing = Boolean.FALSE;

	private Boolean isGroupeEditing = Boolean.TRUE;

	private Boolean wantRefreshGroupes = Boolean.TRUE;

	private static final EOQualifier qualifierForTypeAdresseSansPerso = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_ETUD), new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PAR), new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_INVAL), new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PERSO)
	}));
	private static final EOQualifier qualifierForTypeAdresseAvecPerso = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_INVAL)
	}));

	public EOQualifier qualifierForTypeTel = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_PAR), new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_ETUD), new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_PRV)
	}));

	private Boolean isElementsEditing = Boolean.FALSE;
	private Boolean isGroupeAdminEditing = Boolean.TRUE;
	private Boolean isStructureEffectifEditing = Boolean.FALSE;
	private Boolean isStructureCaEditing = Boolean.FALSE;
	private Boolean isCompteEditing = Boolean.FALSE;

	private Boolean wantRefreshElements;

	private EOStructure selectedStructureParent;
	private NSArray structureParentesExclus;

	public PersonneForm(WOContext context) {
		super(context);

	}

	public String getNom() {
		return getLaPersonne().getNomPrenomAffichage() + " (n°" + getLaPersonne().getNumero() + ")";

	}

	//	public EOQualifier qualifierForTypeAdresse() {
	//		if (getAppUserForAnnuaire().hasDroitCreationEOFournis(null, AUtils.currentExercice())) {
	//			return qualifierForTypeAdresseAvecPerso;
	//		}
	//		else {
	//			return qualifierForTypeAdresseSansPerso;
	//		}
	//	}

	public WOActionResults onAnnuler() {
		NSMutableDictionary userInfo = new NSMutableDictionary();
		userInfo.setObjectForKey(edc(), "edc");
		NotificationCtrl.postNotificationForOnAnnulerNotification(this, userInfo);
		resetForm();
		getCtrl().onAnnuler();
		return performParentAction(getCallbackOnAnnuler());
	}

	public WOActionResults onEnregistrer() {
		boolean newObject = false;
		try {
			setErreurSaisieMessage(null);
			String msgSuccess = "";
			if (isCreation()) {
				newObject = true;
				msgSuccess = getLaPersonne().getNomCompletAffichage() + " a été correctement créé.";
			}
			if (ctrl.onEnregistrer()) {
				IPersonneFormContainer myParent = ((IPersonneFormContainer) parent());
				myParent.setSelectedPersonneForForm(getLaPersonne());
				getCtrl().setPersonne(null);
				WOActionResults res = performParentAction(getCallbackOnEnregistrer());
				if (newObject) {
					setValueForBinding(msgSuccess, BINDING_displayMessageOnParent);
				}
				return res;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
			return null;
		}
	}

	public String getNumero() {
		return getLaPersonne().getNumero();
	}

	/**
	 * @return the listCivilites
	 */
	public NSArray getListCivilites() {
		return getCtrl().getListCivilites();
	}

	public EORepartPersonneAdresse getSelectedRepartPersonneAdresse() {
		return getCtrl().getSelectedRepartPersonneAdresse();
	}

	public void setSelectedRepartPersonneAdresse(EORepartPersonneAdresse repartPersonneAdresse) {
		getCtrl().setSelectedRepartPersonneAdresse(repartPersonneAdresse);
	}

	public Boolean isIndividu() {
		return getCtrl().isIndividu();
	}

	public PersonneFormCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (PersonneFormCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new PersonneFormCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}

	@Override
	public Integer getUtilisateurPersId() {
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}

	public IPersonne getLaPersonne() {
		//		return laPersonne;
		return getCtrl().getPersonne();
	}

	public void setLaPersonne(IPersonne personne) {
		getCtrl().setPersonne(personne);
	}

	@Override
	public void appendToResponse(WOResponse arg0, WOContext arg1) {
		//		if (getCtrl().getMyComponent() == null ) {
		//			getCtrl().setMyComponent(this);
		//		}
		IPersonneFormContainer myParent = ((IPersonneFormContainer) parent());
		if (myParent.wantResetForm() || getCtrl().getPersonne() == null) {
			resetForm();
			initialiseData();
			myParent.setResetForm(false);
		}
		super.appendToResponse(arg0, arg1);
	}

	/**
	 * Reinitialise le formulaire a vide.
	 */
	public void resetForm() {
		setIsAdresseEditing(Boolean.FALSE);
		setIsElementsEditing(Boolean.FALSE);
		setIsGroupeEditing(Boolean.FALSE);
		setIsGroupeAdminEditing(Boolean.FALSE);
		setIsStructureCaEditing(Boolean.FALSE);
		setIsStructureEffectifEditing(Boolean.FALSE);
		setIsNomEditing(Boolean.FALSE);
		setIsCompteEditing(Boolean.FALSE);
		setIsTelephoneEditing(Boolean.FALSE);
		setErreurSaisieMessage(null);
	}

	/**
	 * Appelle le controleur pour initialiser les donnees.
	 */
	public void initialiseData() {
		IPersonne laPersonne = ((IPersonneFormContainer) parent()).getSelectedPersonneForForm();
		getCtrl().initialiseData(laPersonne);
	}

	public String getCallbackOnEnregistrer() {
		if (callbackOnEnregistrer == null) {
			callbackOnEnregistrer = (String) valueForBinding(BINDING_callbackOnEnregistrer);
		}
		return callbackOnEnregistrer;
	}

	public String getCallbackOnAnnuler() {
		if (callbackOnAnnuler == null) {
			callbackOnAnnuler = (String) valueForBinding(BINDING_callbackOnAnnuler);
		}
		return callbackOnAnnuler;
	}

	/**
	 * Interface a respecter par le conteneur du composant PersonneForm. A utiliser a la place de bindings.
	 * 
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public interface IPersonneFormContainer {
		/**
		 * Indique si ce composant doit reinitialiser ses valeurs concernant le fournisseur a creer (logiquement lorsqu'on s'occupe d'un nouveau
		 * fournisseur)
		 */
		public boolean wantResetForm();

		/** */
		public void setSelectedPersonneForForm(IPersonne laPersonne);

		/** Renvoie la personne en cours */
		public IPersonne getSelectedPersonneForForm();

		public void setResetForm(Boolean b);
	}

	public Boolean isAdresseEditing() {
		return isAdresseEditing;
	}

	public void setIsAdresseEditing(Boolean isAdresseEditing) {
		this.isAdresseEditing = isAdresseEditing;
	}

	public Boolean isCreation() {
		return (getLaPersonne() == null || getLaPersonne().hasTemporaryGlobalID());
	}

	public EOStructure getLaStructure() {
		if (getLaPersonne() instanceof EOStructure) {
			return (EOStructure) getLaPersonne();
		}
		return null;
	}

	public EOIndividu getLeIndividu() {
		if (getLaPersonne() instanceof EOIndividu) {
			return (EOIndividu) getLaPersonne();
		}
		return null;
	}

	public Boolean isGroupeEditing() {
		return isGroupeEditing;
	}

	public void setIsGroupeEditing(Boolean isGroupeEditing) {
		this.isGroupeEditing = isGroupeEditing;
	}

	public Boolean getWantRefreshGroupes() {
		return wantRefreshGroupes;
	}

	public void setWantRefreshGroupes(Boolean wantRefreshGroupes) {
		this.wantRefreshGroupes = wantRefreshGroupes;
	}

	public Boolean isElementsEditing() {
		return isElementsEditing;
	}

	public void setIsElementsEditing(Boolean isElementEditing) {
		this.isElementsEditing = isElementEditing;
	}

	public Boolean getWantRefreshElements() {
		return wantRefreshElements;
	}

	public void setWantRefreshElements(Boolean wantRefreshElements) {
		this.wantRefreshElements = wantRefreshElements;
	}

	//	public String getFormId() {
	//		return getComponentId() + "_personneForm";
	//	}

	public String containerAdminId() {
		return getComponentId() + "_containerInfoAdmin";
	}

	public String structureRattachementId() {
		return getComponentId() + "_structureRattachement";
	}

	public EOStructure getSelectedStructureParent() {
		return selectedStructureParent;
	}

	public void setSelectedStructureParent(EOStructure selectedStructureParent) {
		this.selectedStructureParent = selectedStructureParent;
	}

	public NSArray structureParentesExclus() {
		if (structureParentesExclus == null) {
			structureParentesExclus = EOStructureForGroupeSpec.getGroupesFournisseurs(edc());
		}
		return NSArrayCtrl.unionOfNSArrays(new NSArray[] {
				new NSArray(getLaStructure()), structureParentesExclus
		});
	}

	public Boolean getIsGroupeAdminEditing() {
		return isGroupeAdminEditing;
	}

	public void setIsGroupeAdminEditing(Boolean isGroupeAdminEditing) {
		this.isGroupeAdminEditing = isGroupeAdminEditing;
	}

	public String groupeAdminTitle() {
		return "Administration du groupe " + getLaPersonne().persLibelleAffichage();
	}

	public String structureElementsTitle() {
		return "Membres du groupe " + getLaPersonne().persLibelleAffichage();
	}

	public String personneGroupesTitle() {
		return "Groupes dont d&eacute;pend " + getLaPersonne().getNomCompletAffichage();
	}

	public Boolean getIsStructureEffectifEditing() {
		return isStructureEffectifEditing;
	}

	public void setIsStructureEffectifEditing(Boolean isStructureEffectifEditing) {
		this.isStructureEffectifEditing = isStructureEffectifEditing;
	}

	public Boolean getIsStructureCaEditing() {
		return isStructureCaEditing;
	}

	public void setIsStructureCaEditing(Boolean isStructureCaEditing) {
		this.isStructureCaEditing = isStructureCaEditing;
	}

	public Boolean getIsNomEditing() {
		return isNomEditing;
	}

	public void setIsNomEditing(Boolean isNomEditing) {
		this.isNomEditing = isNomEditing;
	}

	public Boolean showCompteUI() {
		return booleanValueForBinding(BINDING_showCompteUI, DEFAULT_showCompteUI);
	}

	public Boolean determinerTypesSelonDroits() {
		return Boolean.TRUE;
	}

	public Boolean getIsCompteEditing() {
		return isCompteEditing;
	}

	public void setIsCompteEditing(Boolean isCompteEditing) {
		this.isCompteEditing = isCompteEditing;
	}

	public Boolean getIsTelephoneEditing() {
		return isTelephoneEditing;
	}

	public void setIsTelephoneEditing(Boolean isTelephoneEditing) {
		this.isTelephoneEditing = isTelephoneEditing;
	}

}
