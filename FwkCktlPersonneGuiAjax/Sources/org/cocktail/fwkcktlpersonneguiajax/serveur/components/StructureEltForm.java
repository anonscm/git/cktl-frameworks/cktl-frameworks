/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Formulaire pour gérer les membres d'un groupe. Utilisez plutot {@link StructureElementUI}.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class StructureEltForm extends AComponent {

	private static final long serialVersionUID = 1L;
	//public static final String BINDING_Structure = "personne";
	public static final String BINDING_repartStructure = "repartStructure";
	public static final String BINDING_isNew = "isNew";

	private String groupe;
	//	private String association;

	private EORepartAssociation selectedRepartAssociation;
	public EORepartAssociation unRepartAssociation;

	private EORepartStructure repartStructureCache;

	public StructureEltForm(WOContext context) {
		super(context);
	}

	public EORepartStructure repartStructure() {
		if (repartStructureCache == null || !repartStructureCache.equals(valueForBinding(BINDING_repartStructure))) {
			repartStructureCache = (EORepartStructure) valueForBinding(BINDING_repartStructure);
			resetForm();
		}
		return repartStructureCache;
		//    	return (EORepartStructure) valueForBinding(BINDING_repartStructure);
	}

	public void resetForm() {
		selectedRepartAssociation = null;
	}

	/**
	 * @return the groupe
	 */
	public String getGroupe() {
		return groupe;
	}

	/**
	 * @param groupe the groupe to set
	 */
	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	//
	//	/**
	//	 * @return the commentaires
	//	 */
	//	public String getCommentaires() {
	//		return commentaires;
	//	}
	//
	//	/**
	//	 * @param commentaires the commentaires to set
	//	 */
	//	public void setCommentaires(String commentaires) {
	//		this.commentaires = commentaires;
	//	}

	public IPersonne getSelectedPersonne() {
		return repartStructure().toPersonneElt();
	}

	public void setSelectedPersonne(IPersonne newPersonne) {
		try {
			if (isNew().booleanValue()) {
				if (newPersonne != null) {
					EOQualifier qual = new EOKeyValueQualifier(EORepartStructure.TO_PERSONNE_ELT_KEY, EOQualifier.QualifierOperatorEqual, newPersonne);
					if (repartStructure().toStructureGroupe().toRepartStructuresElts(qual).count() > 0) {
						throw new Exception("La personne " + newPersonne.getNomPrenomAffichage() + " est déja affectée au groupe " + repartStructure().toStructureGroupe().persLibelle() + ". Annuler la création, sélectionnez la personne dans la liste et modifiez-la.");
					}
				}
			}
			repartStructure().setToPersonneElt(newPersonne);
		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}

	}

	//	public String getSelectedPersonneValue() {
	//		return newPersonneValue;
	//	}

	public EOAssociation getSelectedAssociation() {
		return (getSelectedRepartAssociation() == null ? null : getSelectedRepartAssociation().toAssociation());
	}

	public void setSelectedAssociation(EOAssociation selectedAssociation) {
		//		this.selectedAssociation = selectedAssociation;
		EOAssociation ass = selectedAssociation;
		if (ass != null && !selectedAssociation.editingContext().equals(repartStructure().editingContext())) {
			ass = selectedAssociation.localInstanceIn(repartStructure().editingContext());
		}
		//		repartStructure().toRepartAssociation().setToAssociationRelationship(ass);
		selectedRepartAssociation.setToAssociationRelationship(ass);
	}

	public String associationSelectID() {
		return getComponentId() + "_associationSelect";
	}

	//	public String jsOnAssociationSelectionComplete() {
	//		String res = "function(oc){alert('Coucou');}";
	//		
	//		return res;
	//	}

	public EORepartAssociation getSelectedRepartAssociation() {
		if (selectedRepartAssociation == null && repartStructure().toRepartAssociations().count() > 0) {
			selectedRepartAssociation = (EORepartAssociation) repartStructure().toRepartAssociations().objectAtIndex(0);
		}
		return selectedRepartAssociation;
	}

	public void setSelectedRepartAssociation(EORepartAssociation selectedRepartAssociation) {
		this.selectedRepartAssociation = selectedRepartAssociation;
	}

	public Boolean showPopupRepartAssociations() {
		//		return Boolean.TRUE;
		return Boolean.valueOf(repartStructure().toRepartAssociations().count() > 0);
	}

	public WOActionResults onRepartAssociationCreer() {
		EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edc());
		//		repartAssociation.setPersIdCreation(getUtilisateurPersId());
		repartAssociation.setPersIdModification(getUtilisateurPersId());
		repartStructure().addToToRepartAssociationsRelationship(repartAssociation);
		setSelectedRepartAssociation(repartAssociation);
		return null;
	}

	public WOActionResults onRepartAssociationSupprimer() {
		if (selectedRepartAssociation != null) {
			repartStructure().removeFromToRepartAssociationsRelationship(selectedRepartAssociation);
			edc().deleteObject(selectedRepartAssociation);
		}
		setSelectedRepartAssociation(null);
		return null;
	}

	public String getUnRepartAssociationDisplayString() {
		return (unRepartAssociation.toAssociation() == null ? "Nouveau role" : unRepartAssociation.toAssociation().assLibelle());
	}

	public String addRepartAssociationTitle() {
		if (getSelectedPersonne() != null) {
			return "Affecter un nouveau rôle dans le groupe pour " + getSelectedPersonne().getNomPrenomAffichage();
		}
		return "Affecter un nouveau rôle dans le groupe pour la personne";
	}

	public NSArray getLesRepartAssociations() {
		if (repartStructure() == null) {
			return NSArray.EmptyArray;
		}
		NSArray res = repartStructure().toRepartAssociations();
		if (res.count() > 0) {
			if (selectedRepartAssociation == null || res.indexOf(selectedRepartAssociation) == NSArray.NotFound) {
				setSelectedRepartAssociation((EORepartAssociation) res.objectAtIndex(0));
			}
		}
		return res;
	}

	public Boolean isNew() {
		return booleanValueForBinding(BINDING_isNew, Boolean.TRUE);
	}

	public Boolean isShowRolesPopup() {
		return Boolean.valueOf(repartStructure() != null && repartStructure().toRepartAssociations().count() > 1);
	}

	public Boolean isShowRoles() {
		return Boolean.valueOf(repartStructure() != null && repartStructure().toRepartAssociations().count() > 0);
	}

	public String getPersonneNom() {
		return (getSelectedPersonne() == null ? "" : " de " + getSelectedPersonne().getNomCompletAffichage() + " ");
	}
}
