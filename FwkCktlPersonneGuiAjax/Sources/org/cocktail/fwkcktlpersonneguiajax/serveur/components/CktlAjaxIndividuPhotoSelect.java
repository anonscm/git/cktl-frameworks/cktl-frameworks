/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPhoto;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;

import com.sun.image.codec.jpeg.ImageFormatException;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Composant de visualisation / édition de la photo d'un utilisateur.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class CktlAjaxIndividuPhotoSelect extends AComponent {

	private static final long serialVersionUID = -420925122756444025L;
	private static final Logger LOG = Logger.getLogger(CktlAjaxIndividuPhotoSelect.class);
	public static final String BINDING_INDIVIDU = "individu";
	public static final String BINDING_EC = "editingContext";
	public static final String BINDING_IS_READONLY = "isReadOnly";
	public static final String BINDING_HAS_DROIT_PHOTO = "hasDroitPhoto";
	public static final String BINDING_IS_NOT_VALIDATE = "isNotValidate";

	public int PHOTO_HEIGHT;

	private IIndividu individu;
	private IPhoto photo;
	private EOEditingContext ec;
	private NSData dataUploaded;
	private String filePath;
	private String aucPhotoId;
	private String changePhotoId;
	private String aulChangementId;
	private boolean didDelete;

	public CktlAjaxIndividuPhotoSelect(WOContext context) {
		super(context);
		PHOTO_HEIGHT = Integer.parseInt(EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneGuiAjaxParamManager.GRHUM_PHOTO_HEIGHT));
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (shouldReset()) {
			ec = null;
			photo = null;
			didDelete = false;
			filePath = null;
		}
		super.appendToResponse(response, context);
	}

	private boolean shouldReset() {
		IIndividu indivTmp = (IIndividu) valueForBinding(BINDING_INDIVIDU);
		return (individu == null || !ERXEOControlUtilities.eoEquals((EOEnterpriseObject)indivTmp, (EOEnterpriseObject)individu));
	}

	private EOEditingContext editingContext() {
		if (ec == null) {
			ec = ERXEC.newEditingContext(edc());
		}
		return ec;
	}

	private boolean saveChanges() {
		CocktailAjaxSession session = (CocktailAjaxSession) session();
		boolean success = true;
		try {
			editingContext().saveChanges();
		} catch (ValidationException e) {
			session.addSimpleErrorMessage("Une erreur est survenue lors de l'enregistrement de la photo", e.getMessage());
			success = false;
		} catch (EOGeneralAdaptorException e) {
			session.addSimpleErrorMessage("Une erreur est survenue lors de l'enregistrement de la photo", e.getMessage());
			success = false;
			throw new NSForwardException(e);
		}
		return success;
	}

	
	private NSData reduirePhoto(NSData data) {
		ImageIcon original = new ImageIcon(data.bytes());
		if (original.getIconHeight() == -1) {
			throw new ValidationException("Le format du fichier n'est pas pris en compte");
		}
		int newWidth, newHeight;
		ImageIcon image = null;
		if (original.getIconHeight() <= PHOTO_HEIGHT) {//Si l'image est petite, on ne fait rien.
			image = original;
		}
		else {
			double ratio = 1.0;
			ratio = (double) (PHOTO_HEIGHT)
					/ (double) original.getIconHeight();
			newHeight = PHOTO_HEIGHT;
			newWidth = (int) (original.getIconWidth() * ratio);
			image = new ImageIcon(original.getImage().getScaledInstance(
					newWidth, newHeight, Image.SCALE_SMOOTH));
			original = null;
		}
		System.gc();
		/* On crée la nouvelle image */
		BufferedImage bufImage = new BufferedImage(image.getIconWidth(),
				image.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		bufImage.getGraphics().drawImage(image.getImage(), 0, 0, null);
		ByteArrayOutputStream byteImg = new ByteArrayOutputStream();
		JPEGImageEncoder encImg = JPEGCodec.createJPEGEncoder(byteImg);
		try {
			encImg.encode(bufImage);
		} catch (ImageFormatException e) {
			LOG.debug(e.getMessage(), e);
			throw new ValidationException("Le format du fichier n'est pas pris en compte");
		} catch (IOException e) {
			LOG.debug(e.getMessage(), e);
			throw new ValidationException("Le format du fichier n'est pas pris en compte");

		}
		NSData miniData = new NSData(byteImg.toByteArray());
		try {
			byteImg.close();
		} catch (IOException e) {
			LOG.debug(e);
		}
		return miniData;
	}

	public WOActionResults changerPhoto() {
		filePath = null;
		dataUploaded = null;
		return null;
	}

	public WOActionResults deletePhoto() {
		// Modification du 04/02/14 pour avoir le nom de l'application ayant servi à supprimer ou modifier la photo.
		String login = ((CocktailAjaxSession) session()).connectedUserInfo().login();
		String app = ((CocktailAjaxApplication) application()).getName();
		if (!MyStringCtrl.isEmpty(login) && !MyStringCtrl.isEmpty(app)) {
			IPhoto.IPhotoHelper.archiver(editingContext(), app,
					new NSTimestamp(), login, individu(), getPhoto());
			//		IPhoto.IPhotoHelper.archiver(editingContext(), "Information", new NSTimestamp(), login, individu(), getPhoto());
		} else {
			((CocktailAjaxSession) session()).addSimpleErrorMessage(
					"La photo ne peut être ni supprimer de la base, ni archiver", "Soit le login utilisateur n'est plus en session soit c'est le nom de l'application");
		}
		didDelete = true;
		return null;
	}

	public WOActionResults annuler() {
		editingContext().revert();
		CktlAjaxWindow.close(context(), changePhotoId);
		return null;
	}

	public WOActionResults valider() {
		try {

			if (!ERXStringUtilities.stringIsNullOrEmpty(getFilePath())) {
				NSData miniData = reduirePhoto(dataUploaded);
				IPhoto newPhoto = individu().setPhoto(miniData);
				//                if (saveChanges() && getPhoto() != null) {
				if (saveChanges()) {
					photo = newPhoto;
					didDelete = false;
					if (editingContext().hasChanges()) {
						editingContext().saveChanges();
					}
					CktlAjaxWindow.close(context(), changePhotoId);
				}
			}
			else {
				if (editingContext().hasChanges()) {
					editingContext().saveChanges();
				}
				//            	if (saveChanges() && getPhoto() != null) {
				if (getPhoto() == null) {
					CocktailAjaxSession session = (CocktailAjaxSession) session();
					session.addSimpleInfoMessage("Information", "Aucune photo de sélectionnée !");
					CktlAjaxWindow.close(context(), changePhotoId);
				}
				else {
					CktlAjaxWindow.close(context(), changePhotoId);
				}
			}
		} catch (ValidationException e) {
			context().response().setStatus(500);
			((CocktailAjaxSession) session()).addSimpleErrorMessage(
					"Une erreur est survenue lors de l'enregistrement de la photo", e.getMessage());
		}
		return null;
	}

	public IIndividu individu() {
		IIndividu indivTmp = (IIndividu) valueForBinding(BINDING_INDIVIDU);
		if (individu == null || !ERXEOControlUtilities.eoEquals((EOEnterpriseObject)indivTmp, (EOEnterpriseObject)individu)) {
			individu = indivTmp.localInstanceIn(editingContext());
			if (isNotValidate()) {
				individu.setValidatedWhenNested(false);
			}
		}
		return individu;
	}

	public String updateAucPhoto() {
		return "function() {" + getAucPhotoId() + "Update();}";
	}

	/**
	 * @return true si une photo est affichable
	 */
	public boolean hasPhoto() {
		return getPhoto() != null && getPhoto().datasPhoto() != null && !didDelete && canViewPhoto();
	}

	/**
	 * @return true si l'utilisateur courant peut voir la photo
	 */
	public boolean canViewPhoto() {
		if(hasDroitPhoto()) {
			return true;
		}
		return getAppUserForAnnuaire().hasDroitPhotoVisualisation() || IIndividu.TEM_VALIDE_O.equals(individu().indPhoto());
	}

	/**
	 * @return true si l'utilisateur peut changer la photo
	 */
	public boolean canChangePhoto() {
		if (hasDroitPhoto()) {
			return true;
		}
		return getAppUserForAnnuaire().hasDroitPhotoVisualisation() &&
				getAppUserForAnnuaire().hasDroitModificationIPersonne((IPersonne) individu());
	}

	public boolean canDeletePhoto() {
		if (hasPhoto() && canChangePhoto())
			// TODO : regarder le paramètre de l'appli CAN_DELETE_PHOTO
			return true;
		return false;
	}

	/**
	 * @return the getPhotoDatas
	 */
	public IPhoto getPhoto() {
		if (photo == null) {
			photo = individu().toPhoto();
		}
		return photo;
	}

	public int getPhotoHeight() {
		return PHOTO_HEIGHT;
	}
	
	public String stylePhoto() {
		return "margin:3px 10px; height:"+getPhotoHeight()+"px";
	}

	public NSData getDataUploaded() {
		return dataUploaded;
	}

	public void setDataUploaded(NSData dataUploaded) {
		this.dataUploaded = dataUploaded;
	}

	public String getAucDummyId() {
		return "AucDummyId";
	}

	public String getAucPhotoId() {
		if (aucPhotoId == null)
			aucPhotoId = ERXWOContext.safeIdentifierName(context(), true);
		return aucPhotoId;
	}

	public String getChangePhotoId() {
		if (changePhotoId == null)
			changePhotoId = ERXWOContext.safeIdentifierName(context(), true);
		return changePhotoId;
	}

	public String getAulChangementId() {
		if (aulChangementId == null)
			aulChangementId = ERXWOContext.safeIdentifierName(context(), true);
		return aulChangementId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public boolean didDelete() {
		return didDelete;
	}

	public void setDidDelete(boolean flag) {
		this.didDelete = flag;
	}

	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_IS_READONLY, Boolean.FALSE);
	}
	
	public Boolean hasDroitPhoto() {
		return booleanValueForBinding(BINDING_HAS_DROIT_PHOTO, Boolean.FALSE);
	}

	public Boolean isNotValidate() {
		return booleanValueForBinding(BINDING_IS_NOT_VALIDATE, Boolean.FALSE);
	}

	public String getPhotoPrivee() {
		return individu().indPhoto();
	}

	public void setPhotoPrivee(String valuePhotoPrivee) {
		individu().setIndPhoto(valuePhotoPrivee);
	}

	public boolean isPublic() {
		if (individu().indPhoto() != null && individu().indPhoto().equals("O")) {
			return true;
		}
		return false;
	}
	
	/**
	 * AJout d'un paramètre dans la table Grhum.paramètre pour que la photo
	 * ne puisse qu'être modifiée pour son statut de publication.
	 */
	public boolean isPhotoOnlyPublicated(){
		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PARAM_PHOTO_ONLYPUBLICATED_ACTIVE)){
//		if (myApp().config().booleanForKey(FwkCktlPersonneParamManager.PARAM_PHOTO_ONLYPUBLICATED_ACTIVE)){
//		if ( "O".equals(EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneParamManager.PARAM_PHOTO_ONLYPUBLICATED_ACTIVE)) ){
			return true;
		}
		return false;
	}
}