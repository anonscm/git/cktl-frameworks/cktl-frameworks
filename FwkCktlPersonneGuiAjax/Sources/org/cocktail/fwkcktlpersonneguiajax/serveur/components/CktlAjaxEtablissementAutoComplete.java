package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EORne;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXWOContext;

/**
 * Composant de sélection d'établissement
 * 
 * @binding selectedEtablissement pour setter l'établissement sélectionné.
 * @binding value la valeur initialement affiché dans le champ texte.
 * 
 */
public class CktlAjaxEtablissementAutoComplete extends AComponent {

	private static final long serialVersionUID = 4652316991080863196L;
    private static final String BINDING_SELECTED_ETABLISSEMENT = "selectedEtablissement";
    private static String BINDING_VALUE = "value";
    
    private EORne currentEtablissement;
    private String autoCompleteId;
    private NSArray<EORne> allEtablissements;
    
    public CktlAjaxEtablissementAutoComplete(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public String getValue() {
        return stringValueForBinding(BINDING_VALUE, null);
    }
    
    public void setValue(String value) {
        setValueForBinding(value, BINDING_VALUE);
    }
    
    public EORne getSelectedEtablissement() {
        return (EORne)valueForBinding(BINDING_SELECTED_ETABLISSEMENT);
    }
    
    public void setSelectedEtablissement(EORne selectedEtablissement) {
        setValueForBinding(selectedEtablissement, BINDING_SELECTED_ETABLISSEMENT);
    }
    
    public NSArray<EORne> getEtablissements() {
	    return EORne.getFilteredEtablissements(edc(), getValue());
    }
    
    public String getAutoCompleteId() {
        if (autoCompleteId == null)
            autoCompleteId = ERXWOContext.safeIdentifierName(context(), true);
        return autoCompleteId;
    }
    
    public EORne getCurrentEtablissement() {
        return currentEtablissement;
    }
    
    public void setCurrentEtablissement(EORne currentEtablissement) {
        this.currentEtablissement = currentEtablissement;
    }
    
    public String displayEtablissement() {
    	return currentEtablissement.cRne() + " - " + currentEtablissement.llRne();
    }
}