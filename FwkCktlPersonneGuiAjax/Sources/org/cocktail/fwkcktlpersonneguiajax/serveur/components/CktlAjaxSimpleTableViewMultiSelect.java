package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;

public class CktlAjaxSimpleTableViewMultiSelect extends AComponent {

    private static final long serialVersionUID = 1L;
    private static final String BINDING_LABEL = "label";
    private static final String BINDING_LIST = "list";
    private static final String BINDING_ID = "id";
    private static final String BINDING_WIDTH = "width";
    private static final String BINDING_HEIGHT = "height";
    private static final String BINDING_WINDOW_ID = "windowId";	
    private static final String BINDING_SELECTED_ITEMS = "selectedItems";
    private static final String BINDING_BOUTON_TEXTE = "btnText";
    private static final String BINDING_BOUTON_TYPE = "btnType";
    private static final String BINDING_CALLBACK_ON_ACTION = "callbackOnAction";
    private static final String BINDING_DISPLAY_STRING_KEY = "displayStringKey";
    private static final String BINDING_SORT_KEYPATH = "sortKeypath";
    
    private ERXDisplayGroup<Object> displayGroup;
    private Object currentItem;

	public CktlAjaxSimpleTableViewMultiSelect(WOContext context) {
        super(context);
    }
	
	/**
     * @return la chaine à afficher
     */
    public String getDisplayString() {
    	
    	if (getCurrentItem() instanceof EORepartStructure) {
    		return ((EORepartStructure) getCurrentItem()).toStructureGroupe().libelleForGroupe();
    	}
    	return getCurrentItem().toString();
    	
//    	if (getCurrentItem() instanceof NSKeyValueCoding) {
//		NSKeyValueCoding kvc = (NSKeyValueCoding) getCurrentValue();
//			NSKeyValueCoding kvc = (NSKeyValueCoding) getCurrentItem();
//    		return (String) kvc.valueForKey(getDisplayStringKey());
//    	}
//    	return ""; 
    }

	public String getDisplayStringKey() {
		return stringValueForBinding(BINDING_DISPLAY_STRING_KEY, null);
	}
	
	public String getLabel() {
		return valueForStringBinding(BINDING_LABEL, "");
	}
	
	public void setLabel(String label) {
	}
	
	public String getBtnText() {
		return valueForStringBinding(BINDING_BOUTON_TEXTE, "Action");
	}
	
	public void setBtnText(String btnText) {
	}
	
	public String getBtnType() {
		return valueForStringBinding(BINDING_BOUTON_TYPE, "");
	}
	
	public void setBtnType(String btnType) {
	}
	
	public String getWidth() {
		return valueForStringBinding(BINDING_WIDTH, "");
	}
	
	public void setWidth(String width) {
	}
	
	public String getHeight() {
		return valueForStringBinding(BINDING_HEIGHT, "");
	}
	
	public void setHeight(String height) {
	}
	
	public String getId() {
		return valueForStringBinding(BINDING_ID, "tableViewMultiSelect");
	}
	
	public void setId(String id) {
	}
	
	public NSArray<Object> getList() {
		return (NSArray<Object>) valueForBinding(BINDING_LIST);
	}
	
	public void setList(NSArray<Object> list) {
	}
	
	public NSArray<Object> getSelectedItems() {
		return (NSArray<Object>) valueForBinding(BINDING_SELECTED_ITEMS);
	}
	
	public void setSelectedItems(NSArray<Object> selectedItems) {
		setValueForBinding(selectedItems, BINDING_SELECTED_ITEMS);
	}
	
	public String getSortKeypath() {
		return valueForStringBinding(BINDING_SORT_KEYPATH, null);
	}
	
	public void setSortKeypath(String sortKeypath) {
	}
	
	public String getCallbackOnAction() {
		return (String) valueForBinding(BINDING_CALLBACK_ON_ACTION);
	}
	
	/**
 	 * @return le display group des types d'AEs
 	 */
 	public ERXDisplayGroup<Object> getDisplayGroup() {
 		if (displayGroup == null) {
 			displayGroup = new ERXDisplayGroup<Object>();
 			displayGroup.setObjectArray(getList());
 			displayGroup.setDelegate(new DisplayGroupDelegate());
 			displayGroup.setSelectsFirstObjectAfterFetch(false);
 			displayGroup.fetch();
 		}
 		return displayGroup;
 	}
 	
 	/**
 	 * delegate class
 	 * handle the current selected item
 	 */
 	public class DisplayGroupDelegate {

 		/**
 		 * @param group : groupe d'éléments sélectionnés
 		 */
 		public void displayGroupDidChangeSelection(WODisplayGroup group) {
 			NSArray<Object> array = group.selectedObjects();
 			if (array.size() > 0) {
 	    		setSelectedItems((NSArray<Object>) group.selectedObjects());
 	    	} else {
 	    		setSelectedItems(null);
 	    	}
 		}
 	}
 	
	public Object getCurrentItem() {
	    return currentItem;
    }

	public void setCurrentItem(Object currentItem) {
	    this.currentItem = currentItem;
    }
	
	public WOActionResults annuler() {
		edc().revert();
		fermerFenetre();
		return doNothing();
	}
	
	public WOActionResults action() {
		fermerFenetre();
		return performParentAction(getCallbackOnAction());
	}
	
	private void fermerFenetre() {
		CktlAjaxWindow.close(context(), valueForStringBinding(BINDING_WINDOW_ID, ""));
	}

}