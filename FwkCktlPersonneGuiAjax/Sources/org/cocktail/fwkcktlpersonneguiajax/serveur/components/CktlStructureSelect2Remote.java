package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.filtre.StructureFiltre;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.google.inject.Inject;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class CktlStructureSelect2Remote extends CktlAjaxWOComponent {
    
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_DISABLED = "disabled";
	// si pas de structures en entrée, les résultats retournés viendront d'une recherche en base, d'une recherche dans cette liste en mémoire sinon
	private static final String BINDING_STRUCTURES = "structures";
	private static final String BINDING_SEARCH_TEXT = "texte";
	
	// si true -> l'affichage sera le suivant : libellé long de la structure (nom de la composante)
	// si false -> l'affichage sera le suivant : libellé long de la structure 
	private static final String BINDING_AFFICHAGE_STRUCTURE_AVEC_COMPOSANTE = "afficherStructureEtComposante";
	private static final String BINDING_STYLE = "style";

	@Inject
	private StructureFiltre structureFiltre;

	private StructuresProvider structuresDataProvider;
	
	/**
	 * @param context : contexte d'édition
	 */
	public CktlStructureSelect2Remote(WOContext context) {
		super(context);		
	}

	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getDisabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}
	
	public Boolean getAffichageStructureAvecComposante() {
		if (valueForBinding(BINDING_AFFICHAGE_STRUCTURE_AVEC_COMPOSANTE) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_AFFICHAGE_STRUCTURE_AVEC_COMPOSANTE);
	}
	
	/**
	 * @return le provider de structures
	 */
	public StructuresProvider getStructuresDataProvider() {
		if (structuresDataProvider == null) {
			
			if (getStructures() != null) {
			
				structuresDataProvider = new StructuresMemoryProvider(getStructures(), getAffichageStructureAvecComposante()) {
	
					@Override
					public IStructure getSelectedStructure() {
						return getSelection();
					}
	
					@Override
					public void setSelectedStructure(IStructure structure) {
						setSelection(structure);
					}
	
				};
			
			} else {
	
				structuresDataProvider = new StructuresBaseProvider(getAffichageStructureAvecComposante()) {
					
					@Override
					public IStructure getSelectedStructure() {
						return getSelection();
					}
	
					@Override
					public void setSelectedStructure(IStructure structure) {
						setSelection(structure);
					}
	
				};
	
			}
		}
		return structuresDataProvider;
	}
	
	
	
	
	public IStructure getSelection() {
		return (IStructure) valueForBinding(BINDING_SELECTION);
	}
	
	/**
	 * @param selection la selection
	 */
	public void setSelection(IStructure selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}
	
	/**
	 * @param disabled vrai si le composant est inactif
	 */
	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}

	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, StringUtils.EMPTY);
	}

	/**
	 * @param style : style css du composant
	 */
	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}

	@SuppressWarnings("unchecked")
	public List<IStructure> getStructures() {
		return (List<IStructure>) valueForBinding(BINDING_STRUCTURES);
	}

	public String getSearchText() {
		return valueForStringBinding(BINDING_SEARCH_TEXT, StringUtils.EMPTY);
	}
	
	
	
	public abstract class StructuresProvider implements CktlAjaxSelect2RemoteDataProvider {
		
		protected final Integer MAX_SEARCH_RESULT = 100;
		
		private Boolean isAffichageAvecComposante = false;
		
		public StructuresProvider(Boolean isAffichageAvecComposante) {
			this.isAffichageAvecComposante = isAffichageAvecComposante;
		}
		
		/**
		 * {@inheritDoc}
		 */
		public abstract IStructure getSelectedStructure();

		/**
		 * {@inheritDoc}
		 */
		public abstract void setSelectedStructure(IStructure structure);

		protected Result getResult(IStructure structure) {
			return new Result(structure.cStructure(), getLibelleResult(structure));
		}
		
		private String getLibelleResult(IStructure structure) {
			String libelle = structure.llStructure();
			if (isAffichageAvecComposante) {
				libelle += " ("+structure.toStructurePere().lcStructure()+")";
			}
			return libelle;
		}
		
		/**
		 * {@inheritDoc}
		 */
		public Result selectionInitiale() {
			if (getSelectedStructure() == null) {
				return null;
			} else {
				return getResult(getSelectedStructure());
			}
		}
		
	}
	
	/**
	 *
	 */
	public abstract class StructuresMemoryProvider extends StructuresProvider {
		
		private List<IStructure> structures;
		
		public StructuresMemoryProvider(List<IStructure> structures, Boolean isAffichageAvecComposante) {
			super(isAffichageAvecComposante);
			this.structures = structures;
		}
		
		/**
		 * {@inheritDoc}
		 */
		public List<Result> results(final String searchTerm) {
			List<Result> resultat = new ArrayList<Result>();
			
			Collection<IStructure> listeFiltree = structureFiltre.filtrerParNomCompletAffichage(getStructures(), searchTerm);
			
			for (IStructure structure : listeFiltree) {
				resultat.add(getResult(structure));
			}
			
			return resultat;
		}
		
		/**
		 * {@inheritDoc}
		 */
		public Result onSelect(String idSelection) {
			
			IStructure structure = structureFiltre.filtreParCodeStructure(getStructures(), idSelection);
			
			if (structure == null) {
				setSelectedStructure(null);
				return null;
			} else {
				setSelectedStructure(structure);
				return getResult(structure);
			}
		}
		
		
		public List<IStructure> getStructures() {
			return structures;
		}

		public void setStructures(List<IStructure> structures) {
			this.structures = structures;
		}
		
		
		
		
	}
	
	
	public abstract class StructuresBaseProvider extends StructuresProvider {
		
		public StructuresBaseProvider(Boolean isAffichageAvecComposante) {
			super(isAffichageAvecComposante);
		}

		
		/**
		 * {@inheritDoc}
		 */
		public List<Result> results(final String searchTerm) {
			List<Result> resultat = new ArrayList<Result>();
			
			NSArray<EOStructure> listeFiltree = EOStructure.structuresInternesByNameAndSiret(edc(), searchTerm, null, null, MAX_SEARCH_RESULT);
			
			for (IStructure structure : listeFiltree) {
				resultat.add(getResult(structure));
			}
			
			return resultat;
		}
		
		
		/**
		 * {@inheritDoc}
		 */
		public Result onSelect(String idSelection) {
			
			IStructure structure = EOStructure.structurePourCode(edc(), idSelection);
			
			if (structure == null) {
				setSelectedStructure(null);
				return null;
			} else {
				setSelectedStructure(structure);
				return getResult(structure);
			}
		}
		
		
	}
	
	
}