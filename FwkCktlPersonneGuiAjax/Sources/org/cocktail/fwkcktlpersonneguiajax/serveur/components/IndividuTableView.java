/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * @deprecated Utilisez {@link PersonneTableView} à la place.
 */
public class IndividuTableView extends AComponent {

	private static final long serialVersionUID = 1L;

	//	public static final String BINDING_callbackOnSelect = "callbackOnSelect";
	//	public static final String BINDING_selectedIndividu = "selectedIndividu";
	public static final String BINDING_updateContainerID = "updateContainerID";
	//	public static final String BINDING_showFournisseur = "showFournisseur";

	public EOIndividu unRow;
	private EOIndividu unIndividu, selectedIndividu;

	/** Bindings pour les colonnes a afficher, {@link StructureTableView#DEFAULT_COLONNES_KEYS} */
	public static final String BINDING_colonnesKeys = "colonnesKeys";

	/** Bindings pour indiquer si on affiche une colonne permettant d'ouvrir une fenetre modale */
	/** avec le detail de la personne selectionnee */
	public static final String BINDING_showDetailInModalBoxId = "showDetailInModalBoxId";

	private static final String IND_KEY = "unIndividu.";
	public static final String COL_DETAIL_KEY = "detail";
	public static final String COL_NUMERO_KEY = IPersonne.NUMERO_KEY;
	public static final String COL_NOM_KEY = IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
	public static final String COL_FOURNISSEUR_KEY = EOStructure.TO_FOURNIS_KEY;
	public static final String COL_VILLE_KEY = EOAdresse.VILLE_KEY;
	//public static final String COL_DATE_KEY = EOStructure.D_CREATION_KEY;

	private NSArray colonnes;

	public static final NSMutableDictionary _colonnesMap = new NSMutableDictionary();
	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("&nbsp;");
		col0.setComponent(PersonneDetailColonne.class.getSimpleName());
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_NUMERO_KEY, "emptyValue");
		ass0.setObjectForKey("afficherDetailPersonne", "action");
		ass0.setObjectForKey("onClickDetailler", "onClickDetailler");
		col0.setAssociations(ass0);
		_colonnesMap.takeValueForKey(col0, COL_DETAIL_KEY);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Numéro");
		col1.setOrderKeyPath(COL_NUMERO_KEY);
		//		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NUMERO_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_NUMERO_KEY, "emptyValue");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, COL_NUMERO_KEY);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Nom");
		col2.setOrderKeyPath(COL_NOM_KEY);
		//CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_NOM_KEY, "emptyValue");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, COL_NOM_KEY);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Fournisseur");
		col4.setComponent("FouValideColonne");
		col4.setOrderKeyPath(COL_FOURNISSEUR_KEY + "." + EOFournis.FOU_VALIDE_KEY);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_FOURNISSEUR_KEY, null);
		col4.setAssociations(ass4);
		_colonnesMap.takeValueForKey(col4, COL_FOURNISSEUR_KEY);

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Ville");
		col5.setOrderKeyPath("getAdressePrincipale." + COL_VILLE_KEY);
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(IND_KEY + "getAdressePrincipale." + COL_VILLE_KEY, "emptyValue");
		col5.setAssociations(ass5);
		_colonnesMap.takeValueForKey(col5, COL_VILLE_KEY);

		//		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		//		col5.setLibelle("Test");
		//		col5.setOrderKeyPath(COL_DATE_KEY);
		//		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_DATE_KEY, "emptyValue");
		//		ass5.setDateFormat("%d/%m/%Y");
		//		//ass5.setFormatter("dateFormatter");
		//		col5.setAssociations(ass5);
		//		_colonnesMap.takeValueForKey(col5, COL_DATE_KEY);

	}

	public NSTimestampFormatter dateFormatter() {
		return new NSTimestampFormatter("%d/%m/%Y");
	}

	public static NSArray DEFAULT_COLONNES_KEYS = new NSArray(new Object[] {
			COL_NUMERO_KEY, COL_NOM_KEY
	});

	public IndividuTableView(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	//    
	//	public WOActionResults onSelect() {
	//		setSelectedIndividu(unRow);
	//		if (hasBinding(BINDING_callbackOnSelect)) {
	//			return performParentAction((String) valueForBinding(BINDING_callbackOnSelect));
	//		}
	//		return null;
	//	}

	//
	//	public EOIndividu getSelectedIndividu() {
	//		return selectedIndividu;
	//	}
	//
	//	public void setSelectedIndividu(EOIndividu selectedIndividu) {
	//		this.selectedIndividu = selectedIndividu;
	//		setValueForBinding(selectedIndividu, BINDING_selectedIndividu);
	//	}
	//	
	//
	//	public Boolean showFournisseur() {
	//		if (valueForBinding(BINDING_showFournisseur) != null) {
	//			return (Boolean)valueForBinding(BINDING_showFournisseur);
	//		}
	//		return Boolean.FALSE;
	//	}
	//	
	//	

	public NSArray getColonnes() {
		if (colonnes == null) {
			NSMutableArray res = new NSMutableArray();
			NSArray colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray getColonnesKeys() {
		NSArray keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}
		if (hasBinding(BINDING_showDetailInModalBoxId) && valueForBinding(BINDING_showDetailInModalBoxId) != null) {
			keys = new NSArray(COL_DETAIL_KEY).arrayByAddingObjectsFromArray(keys);
		}
		return keys;
	}

	/**
	 * @return the unIndividu
	 */
	public EOIndividu unIndividu() {
		return unIndividu;
	}

	/**
	 * @param unIndividu the unIndividu to set
	 */
	public void setUnIndividu(EOIndividu unIndividu) {
		this.unIndividu = unIndividu;
	}

	public String getEmptyValue() {
		return " ";
	}

	public String detailPersonneModalBoxTitle() {
		String detailPersonneModalBoxTitle = null;
		if (unIndividu() != null) {
			detailPersonneModalBoxTitle = unIndividu().persLibelleAffichage();
		}
		return detailPersonneModalBoxTitle;
	}

	public WOActionResults afficherDetailPersonne() {
		CktlAjaxPersonneDetailForWin nextPage = (CktlAjaxPersonneDetailForWin) pageWithName(CktlAjaxPersonneDetailForWin.class.getName());
		nextPage.setPersonne(unIndividu());
		nextPage.setPersId(getUtilisateurPersId());
		nextPage.setParentComponent(context().page());
		return nextPage;
	}

	public String onClickDetailler() {

		String onClickDetailler = null;
		//this.href : url de l'action du bouton (openWinSelection)
		onClickDetailler = "openGenericHRefWin(";
		// id
		onClickDetailler += "'" + (String) valueForBinding(BINDING_showDetailInModalBoxId) + "', ";
		// href
		onClickDetailler += "this.href, ";
		// title
		onClickDetailler += "'" + detailPersonneModalBoxTitle() + "', ";
		// resizable
		onClickDetailler += "true, ";
		// closable
		onClickDetailler += "true, ";
		// minimizable
		onClickDetailler += "false, ";
		// maximizable
		onClickDetailler += "false, ";
		// minWidth
		onClickDetailler += "500, ";
		// width
		onClickDetailler += "500, ";
		// minHeight
		onClickDetailler += "400, ";
		// height
		onClickDetailler += "400, ";
		// className
		if (session() instanceof CocktailAjaxSession) {
			String windowsClassName = (String) session().valueForKey("windowsClassName");
			if (windowsClassName != null && !windowsClassName.equals("")) {
				onClickDetailler += "'" + session().valueForKey("windowsClassName") + "', ";
			}
			else {
				onClickDetailler += "null, ";
			}
		}
		else {
			onClickDetailler += "null, ";
		}
		// updateContainerId
		onClickDetailler += "null);return false;";

		return onClickDetailler;
	}
}
