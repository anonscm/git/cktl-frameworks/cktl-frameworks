/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxSelectComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOBanque;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Recherche et sélection d'un banque.
 * 
 * @binding updateContainerID
 * @binding selectedBanque
 * @binding enabled
 * @author rprin
 */
public class CktlBanqueSelect extends AComponent {

	private static final long serialVersionUID = 1L;

	private static final String BANQUE = CktlAjaxSelectComponent.CURRENT_OBJ_KEY;

	//private static final String BINDING_UPDATE_CONTAINER_ID = "updateContainerID";

	private static final String BINDING_selectedBanque = "selectedBanque";

	private static final String LC_KEY = EOBanque.CODE_KEY;
	private static final String LL_KEY = EOBanque.LIBELLE_KEY;

	private String filtre;
	public EOBanque unBanque;
	private EOBanque selectedBanque;
	private NSArray<CktlAjaxTableViewColumn> colonnes;

	private Boolean refreshBanques;

	public CktlBanqueSelect(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public NSArray<EOBanque> getFilteredBanques(String filtBanque) {
		return EOBanque.getFilteredBanques(edc(), filtBanque);
	}

	public EOBanque getSelectedBanque() {
		return selectedBanque;
	}

	public WOActionResults containerOnCloseAction() {
		unBanque = null;
		filtre = null;
		return null;
	}

	public String getModalWindowId() {
		return getComponentId() + "_win";
	}

	public String getFiltreButtonId() {
		return getComponentId() + "_filtreBt";
	}

	public EOBanque getSelection() {
		return selectedBanque;
		//return (EOBanque) valueForBinding(BINDING_selectedBanque);
	}

	public void setSelection(EOBanque value) {
		selectedBanque = value;
		//setValueForBinding(value, BINDING_selectedBanque);
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
			// Colonne bic
			CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
			col.setLibelle("Bic");
			col.setOrderKeyPath(EOBanque.BIC_KEY);
			String keyPath = ERXQ.keyPath(BANQUE, EOBanque.BIC_KEY);
			CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne code banque
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Code banque");
			col.setOrderKeyPath(EOBanque.C_BANQUE_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.C_BANQUE_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne code guichet
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Code guichet");
			col.setOrderKeyPath(EOBanque.C_GUICHET_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.C_GUICHET_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne Libelle 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Domiciliation");
			col.setOrderKeyPath(EOBanque.DOMICILIATION_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.DOMICILIATION_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne adresse 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Adresse");
			col.setOrderKeyPath(EOBanque.ADRESSE_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.ADRESSE_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne cp 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Code postal");
			col.setOrderKeyPath(EOBanque.CODE_POSTAL_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.CODE_POSTAL_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne ville 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Ville");
			col.setOrderKeyPath(EOBanque.VILLE_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.VILLE_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne date ouverture 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Date ouverture");
			col.setOrderKeyPath(EOBanque.D_OUVERTURE_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.D_OUVERTURE_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			ass.setDateformat("dd/MM/yyyy");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne date fermeture 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Date fermeture");
			col.setOrderKeyPath(EOBanque.D_FERMETURE_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.D_FERMETURE_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			ass.setDateformat("dd/MM/yyyy");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne Local 
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Local");
			col.setOrderKeyPath(EOBanque.TEM_LOCAL_KEY);
			keyPath = ERXQ.keyPath(BANQUE, EOBanque.TEM_LOCAL_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			colonnes = colTmp.immutableClone();
		}
		return colonnes;
	}

	public EOQualifier getQualifier() {
		return EOBanque.getQualifierForFiltre(getFiltre());
	}

	public String getFiltre() {
		return filtre;
	}

	public void setFiltre(String filtre) {
		this.filtre = filtre;
	}

	//	@SuppressWarnings("unchecked")
	//	public NSArray<EOBanque> getAllBanques() {
	//		return EOBanque.fetchAll(edc());
	//	}

	public WOActionResults valider() {
		setValueForBinding(selectedBanque, BINDING_selectedBanque);
		CktlAjaxWindow.close(context(), getModalWindowId());
		return null;
	}

	public WOActionResults annuler() {
		//setValueForBinding(selectedBanque, BINDING_selectedBanque);
		CktlAjaxWindow.close(context(), getModalWindowId());
		return null;
	}

	public String getBanqueEntityName() {
		return EOBanque.ENTITY_NAME;
	}

	public NSArray<EOSortOrdering> sortOrderings() {
		return new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EOBanque.SORT_DOMICILIATION, EOBanque.SORT_BIC_ASC, EOBanque.SORT_CBANQUE_ASC, EOBanque.SORT_CGUICHET_ASC
		});
	}

	public String getDateMaj() {
		return cktlApp().config().stringForKey("org.cocktail.grhum.banque.date.maj");
	}

	public Boolean getRefreshBanques() {
		return refreshBanques;
	}

	public void setRefreshBanques(Boolean refreshBanques) {
		this.refreshBanques = refreshBanques;
	}

	public WOActionResults onOpenBanque() {
		setRefreshBanques(Boolean.TRUE);
		return null;
	}

	public Boolean isDateMaj() {
		return !ERXStringUtilities.stringIsNullOrEmpty(getDateMaj());
	}
}
