/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.AnnuaireLogService;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSValidation;

import er.ajax.CktlAjaxUtils;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXProperties;

/**
 * Permet de gérer les numeros de telephones d'une personne.
 * 
 * @binding personne
 * @binding editingContext
 * @binding isReadOnly False par defaut. Si non specifié, tient compte de qualifierForTypeTel : Si le type de telephone est géré, readOnly est false.
 * @binding isCompactView
 * @binding qualifierForTypeTel Facultatif. Qualifier pour les types de telephone affichables à la saisie
 * @binding defaultTypeTel Facultatif. Le code du type de telephone a selectionner dans la liste par defaut pour la creation (String)
 * @binding isStructureVisible Facultatif. Permet de cacher le champ strcuture de rattachement si besoin
 * @binding utilisateurPersId
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class PersonneTelephoneUI extends AComponent {

	private static final long serialVersionUID = 1L;
	//public static final String BINDING_callbackOnSupprimerTelephone = "callbackOnSupprimerTelephone";
	public static final String BINDING_personne = "personne";
	//public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_isReadOnly = "isReadOnly";
	public static final String BINDING_isCompactView = "isCompactView";
	public static final String BINDING_selectedTypeNoTel = "selectedTypeNoTel";
	public static final String BINDING_unPersonneTelephone = "unPersonneTelephone";
	public static final String BINDING_isStructureVisible = "isStructureVisible";
	/** Facultatif. Qualifier pour les types de telephone affichables */
	public static final String BINDING_qualifierForTypeTel = "qualifierForTypeTel";

	/** Facultatif. Le code du type de telephone a selectionner dans la liste par defaut pour la creation (String) */
	public static final String BINDING_defaultTypeTel = "defaultTypeTel";
	private static final String WANT_RESET = "wantReset";

	public static final Boolean DEFAULT_isStructureVisible = Boolean.TRUE;

	public static final String HIDDEN_TELEPHONE_NUMBER = "**.**.**.**.**";

	public EOPaysIndicatif unPaysIndicatif;
	public String filtPaysIndicatif;

	protected EOPersonneTelephone unPersonneTelephone;

	public EOTypeNoTel unTypeNoTel;
	protected EOTypeNoTel selectedTypeNoTel;

	public EOTypeTel unTypeTel;
	private EOTypeTel selectedTypeTel;

	private String sNoTelephone;
	private String listeRouge;

	public Integer iTelephoneIndicatif;
	//	private String erreurSaisieMessage;
	private NSArray allTypeNoTel;
	private NSArray allTypeTel;
	private EOPaysIndicatif selectedPaysIndicatif;
	private EOEditingContext _editingContext;
	private IPersonne _personne = null;
	private EOPaysIndicatif indicatifDefaut;
	private EOStructure selectedStructureTel;
	private int personneTelephoneIndex;
	private boolean telephoneAdded = false;

	private EOPersonneTelephone selectedPersonneTelephone;

	public PersonneTelephoneUI(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
			}
		}
	}

	public NSArray getFilteredPaysIndicatif(String filt) {
		NSArray res = NSArray.EmptyArray;
		NSMutableArray quals = new NSMutableArray();

		if (filt != null) {
			filt = filt.trim();
		}

		if (filt != null && filt.length() > 0) {
			try {
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPaysIndicatif.INDICATIF_KEY + " =%@", new NSArray(Integer.valueOf(filt))));
			} catch (NumberFormatException e) {
				//on ne fait rien
			}
			filt = MyStringCtrl.chaineSansAccents(filt, "?");
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPaysIndicatif.TO_PAYS_KEY + "." + EOPays.LC_PAYS_KEY + " caseInsensitiveLike '*" + filt + "*' ", null));
			res = EOPaysIndicatif.fetchAll(edc(), new EOOrQualifier(quals), null);

			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					EOPaysIndicatif.SORT_LL_PAYS
			}));

		}
		return res;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (wantReset()) {
			setWantReset(false);
		}
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptResourceInHead(context, response, frameworkName(), "scripts/cktlstringutils.js");
	}

	public String getTelephoneFormate() {
		//return unPersonneTelephone.getTelephoneFormateAvecIndicatif();
		if ((!EOTypeTel.C_TYPE_TEL_PRV.equals(unPersonneTelephone.toTypeTel().cTypeTel())) || ((EOTypeTel.C_TYPE_TEL_PRV.equals(unPersonneTelephone.toTypeTel().cTypeTel()))
				&& (getUtilisateurPersId() != null) && getPersonne()
				.getPersonneDelegate()
				.hasPersonneUtilisateurDroitModification(getUtilisateurPersId()))) {
			return unPersonneTelephone.getTelephoneFormateAvecIndicatif();
		} else {
			return HIDDEN_TELEPHONE_NUMBER;
		}
	}

	public Integer getITelephoneIndicatif() {
		return iTelephoneIndicatif;
	}

	public void setITelephoneIndicatif(Integer telephoneIndicatif) {
		iTelephoneIndicatif = telephoneIndicatif;
	}

	public WOActionResults onTelephoneSupprimer() {
		return onTelephoneSupprimer(unPersonneTelephone);
	}

	public String getFiltPaysIndicatif() {
		return filtPaysIndicatif;
	}

	public void setFiltPaysIndicatif(String filtPaysIndicatif) {
		this.filtPaysIndicatif = filtPaysIndicatif;
	}

	public NSArray getLestypesNoTel() {
		if (allTypeNoTel == null) {
			allTypeNoTel = EOTypeNoTel.fetchAll(edc(), new NSArray(new Object[] {
					EOSortOrdering.sortOrderingWithKey(EOTypeNoTel.L_TYPE_NO_TEL_KEY, EOSortOrdering.CompareAscending)
			}));
			// mettre le popup de telephone sur tel fixe
			//EOTypeNoTel typeNoTelFixe = EOTypeNoTel.fetchByKeyValue(edc(), EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
			//setSelectedTypeNoTel(typeNoTelFixe);
		}
		return allTypeNoTel;
	}

	public NSArray getLestypesTel() {
		//si il y a le binding BINDING_selectedTypeNoTel on refait la liste a chaque fois
		if (hasBinding(BINDING_selectedTypeNoTel)) {
			allTypeTel = null;
		}

		if (allTypeTel == null) {
			allTypeTel = EOTypeTel.fetchAll(edc(), getQualifierForTypeTel(), new NSArray(new Object[] {
					EOSortOrdering.sortOrderingWithKey(EOTypeTel.L_TYPE_TEL_KEY, EOSortOrdering.CompareAscending)
			}));
			//			if (allTypeTel.count() > 0) {
			//				EOTypeTel def = defaultTypeTel();
			//				if (def != null && allTypeTel.indexOf(def) != NSArray.NotFound) {
			//					selectedTypeTel = def;
			//				}
			//				else {
			//					selectedTypeTel = (EOTypeTel) allTypeTel.objectAtIndex(0);
			//				}
			//			}
		}
		return allTypeTel;
	}

	/**
	 * @return la valeur 'Oui' pour l'attribut "ListeRouge" d'un téléphone privé
	 * @author Alain MALAPLATE <alain.malaplate at cocktail.org>
	 */
	public String getListeRougeOui() {
		return EOPersonneTelephone.LISTE_ROUGE_O;
	}

	/**
	 * @return la valeur 'Non' pour l'attribut "ListeRouge" d'un téléphone privé
	 * @author Alain MALAPLATE <alain.malaplate at cocktail.org>
	 */
	public String getListeRougeNon() {
		//		System.out.println("Liste Rouge à NON");
		return EOPersonneTelephone.LISTE_ROUGE_N;
	}

	public String getListeRouge() {
		return listeRouge;
	}

	public void setListeRouge(String listeRouge) {
		this.listeRouge = listeRouge;
	}

	public void saveChanges() {
		try {
			if (edc().hasChanges()) {
				edc().saveChanges();
			}
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} finally {
			edc().revert();
		}
	}

	public WOActionResults onTelephoneAjouter() {
		setIsEditing(Boolean.TRUE);
		setTelephoneAdded(true);
		// mettre le popup de telephone sur tel fixe
		EOTypeNoTel typeNoTelFixe = EOTypeNoTel.fetchByKeyValue(edc(), EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOTypeNoTel.TYPE_NO_TEL_TEL);
		setSelectedTypeNoTel(typeNoTelFixe);

		if (getLestypesTel().count() > 0) {
			EOTypeTel def = defaultTypeTel();
			if (def != null && allTypeTel.indexOf(def) != NSArray.NotFound) {
				selectedTypeTel = def;
			}
			else {
				selectedTypeTel = (EOTypeTel) allTypeTel.objectAtIndex(0);
			}
		}

		return null;
	}

	public WOActionResults onDefineTelPrincipal() {
		saveChanges();
		return null;
	}

	public WOActionResults onTelephoneEnregistrer() {
		try {
			if (hasBinding(BINDING_IS_CHANGED)) {
				setBindingIsChanged(true);
			}
			
			edc().revert();
			String noTelephone = getSNoTelephone();
			if (MyStringCtrl.isEmpty(noTelephone)) {
				throw new NSValidation.ValidationException("Veuillez saisir un n° de téléphone.");
			}

			Integer indicatif = null;
			if (!EOTypeTel.C_TYPE_TEL_INT.equals(selectedTypeTel.cTypeTel())) {
				if (getFiltPaysIndicatif() == null) {
					setSelectedPaysIndicatif(EOPersonneTelephone.getIndicatifPaysDefaut(edc()));
				}
			} else {
				setSelectedPaysIndicatif(null);
			}
			if (getFiltPaysIndicatif() != null) {
				try {
					indicatif = Integer.valueOf(getFiltPaysIndicatif());
				} catch (NumberFormatException e) {
					throw new NSValidation.ValidationException("L'indicatif telephonique doit etre numerique (" + getFiltPaysIndicatif() + ").");
				}
			}

			noTelephone = EOPersonneTelephone.nettoieNoTelephone(noTelephone);

			if (noTelephone == null) {
				return null;
			}

			EOTypeNoTel typeNoTel = (EOTypeNoTel) EOUtilities.localInstanceOfObject(edc(), selectedTypeNoTel());
			EOTypeTel typeTel = (EOTypeTel) EOUtilities.localInstanceOfObject(edc(), selectedTypeTel);

			// Verifier que le telephone n'existe pas deja pour la personne
			NSArray personneTelephones = getLesPersonneTelephones();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.NO_TELEPHONE_KEY + "=%@ and " + EOPersonneTelephone.TO_TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY + "=%@ and " + EOPersonneTelephone.TO_TYPE_NO_TEL_KEY + "=%@", new NSArray(new Object[] {
					noTelephone, typeTel, typeNoTel
			}));
			if (EOQualifier.filteredArrayWithQualifier(personneTelephones, qual).count() > 0) {
				throw new NSValidation.ValidationException("Le n° de " + typeNoTel.lTypeNoTel() + " " + StringCtrl.formatPhoneNumber(noTelephone) + " est déjà saisi pour cette personne.");
			}

			EOPersonneTelephone personneTelephone;
			personneTelephone = EOPersonneTelephone.creerInstance(edc());

			// Test pour transférer l'indicateur du téléphone principal.
			if (getSelectedPersonneTelephone() != null) {
				if (getSelectedPersonneTelephone().isTelPrincipal()) {
					personneTelephone.setIsTelPrincipal(Boolean.TRUE);
				}
			}

			if (unPersonneTelephone != null) {
				personneTelephone.setDCreation(unPersonneTelephone.dCreation());
			}
			personneTelephone.setToTypeTelRelationship(typeTel);
			personneTelephone.setToTypeNoTelRelationship(typeNoTel);
			personneTelephone.setNoTelephone(noTelephone);
			personneTelephone.setIndicatif(indicatif);
			personneTelephone.setPersId(getPersonne().persId());
			getPersonne().setPersIdModification(getUtilisateurPersId());
			personneTelephone.setToPersonneRelationship(getPersonne());
			if (getListeRouge() != null) {
				//				System.out.println("Valeur de la liste Rouge : " + getListeRouge());
				personneTelephone.setListeRouge(getListeRouge());
			} else {
				personneTelephone.setListeRouge(getListeRougeNon());
			}
			if (getLesPersonneTelephones().count() == 0) {
				personneTelephone.setIsTelPrincipal(true);
			}
			//structure de rattachement eventuelle
			if (selectedStructureTel() != null) {
				personneTelephone.setToStructureRattachementRelationship(selectedStructureTel);
			}
			if (isOneTelInEditMode()) {
				telephoneSupprimer(getSelectedPersonneTelephone());
			}

			if (isTelephoneAdded()) {
				setTelephoneAdded(false);
			}

			// on push
			saveChanges();
			setIsEditing(Boolean.FALSE);
			//			sNoTelephone = null;
			//			setSelectedPersonneTelephone(null);

			cleanState();

		} catch (ValidationException e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Telephone", e.getMessage());
		} finally {
			edc().revert();
		}
		return null;

	}

	public void telephoneSupprimer(EOPersonneTelephone unPersonneTelephone) {
		if (unPersonneTelephone != null) {
			unPersonneTelephone.supprimer();
		}
	}

	public WOActionResults onTelephoneSupprimer(EOPersonneTelephone unPersonneTelephone) {
		AnnuaireLogService annuaireService = new AnnuaireLogService();
		if (unPersonneTelephone != null) {
			try {
				String message = "Suppression du téléphone (type : " + unPersonneTelephone.typeTel() + ") numéro : "
									+ unPersonneTelephone.noTelephone()
									+ " appartenant au PersId : " + unPersonneTelephone.persId();
				String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
				annuaireService.creationUnAnnuaireLog(edc(), message, nomApplication);
				unPersonneTelephone.supprimer();
				//				getPersonne().removeFromToPersonneTelephonesRelationship(unPersonneTelephone);
				//				unPersonneTelephone.setToPersonneRelationship(null);
				//				edc().deleteObject(unPersonneTelephone);
				//				edc().saveChanges();
				saveChanges();
				sNoTelephone = null;
				this.unPersonneTelephone = null;
				setSelectedPersonneTelephone(null);
			} catch (Exception e) {
				e.printStackTrace();
				setErreurSaisieMessage(e.getMessage());
			} finally {
				edc().revert();
			}
		}
		return null;
	}

	public WOActionResults doSave() {
		new Exception().printStackTrace();
		saveChanges();
		return null;
	}

	@Override
	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			//			_editingContext = new EOEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	//	public IPersonne getPersonne() {
	//		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
	//		IPersonne tmp = pers.localInstanceIn(edc());
	//		if (_personne == null || !_personne.equals(tmp)) {
	//			_personne = tmp;
	//			sNoTelephone = null;
	//		}
	//		return _personne;
	//	}
	//	
	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (pers == null) {
			_personne = null;
			sNoTelephone = null;
		//}	else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
		} else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			//La personne a changé
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			_personne = pers.localInstanceIn(edc());
			sNoTelephone = null;
		}
		return _personne;
	}

	public NSArray getLesPersonneTelephones() {
		//Initialiser l'indicatif telephonique avec le dernier indicatif saisi
		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOPersonneTelephone.D_CREATION_KEY, EOSortOrdering.CompareAscending);

		EOQualifier personneTelQualifier = EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_OK;

		if (!getAppUserForAnnuaire().hasDroitVoirInfosPerso(getPersonne())) {
			// On ne peut voir les tel privés si pas droit annuaire de voir infos perso
			personneTelQualifier = ERXQ.and(personneTelQualifier, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_PRIVE);
		}

		NSArray res = ERXArrayUtilities.sortedArraySortedWithKey(
				EOQualifier.filteredArrayWithQualifier(getPersonne().toPersonneTelephones(), personneTelQualifier),
				EOPersonneTelephone.D_CREATION_KEY);

		Number ind = null;
		for (int i = 0; i < res.count() && ind == null; i++) {
			ind = ((EOPersonneTelephone) res.objectAtIndex(i)).indicatif();
		}
		return res;
	}

	public NSArray getFilteredPaysIndicatif() {
		return getFilteredPaysIndicatif(filtPaysIndicatif);
	}

	public void setSelectedPaysIndicatif(EOPaysIndicatif selectedPaysIndicatif) {
		this.selectedPaysIndicatif = selectedPaysIndicatif;
		if (selectedPaysIndicatif == null) {
			filtPaysIndicatif = null;
			unPaysIndicatif = null;
		} else {
			filtPaysIndicatif = selectedPaysIndicatif.indicatif().toString();
		}
	}

	public EOPaysIndicatif getSelectedPaysIndicatif() {
		return selectedPaysIndicatif;
	}

	@Override
	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_isReadOnly, false);
	}
	public boolean showEditButtons() {
		return !isReadOnly() && !isEditing() && (getAppUserForAnnuaire().hasDroitCompteModification() || getAppUserForAnnuaire().hasDroitModificationIPersonne(getPersonne()));
	}

	// filtrage transféré dans la methode getLesPersonneTelephones()
	//	public Boolean isReadOnlyTelPrincipal() { 
	//		if ((Boolean) valueForBinding(BINDING_isReadOnly))
	//			return (Boolean) valueForBinding(BINDING_isReadOnly);
	//		else if ((unPersonneTelephone != null) && (getLesPersonneTelephones() != null)) {
	//			//verification si le tel principal est d'un type de num géré
	//			for (Object tel : getLesPersonneTelephones()) {
	//				if (((EOPersonneTelephone) tel).isTelPrincipal())
	//					return (!getLestypesTel().contains(((EOPersonneTelephone) tel).toTypeTel()));
	//			}
	//
	//		}
	//		return Boolean.TRUE;
	//	}

	public Boolean isCompactView() {
		return (Boolean) valueForBinding(BINDING_isCompactView);
	}

	public Boolean isStructureVisible() {
		//		if (hasBinding(BINDING_isStructureVisible)) {
		//			return (Boolean) valueForBinding(BINDING_isStructureVisible);
		//		}
		//		return DEFAULT_isStructureVisible;
		return (booleanValueForBinding(BINDING_isStructureVisible, DEFAULT_isStructureVisible) && getPersonne().isIndividu());
	}

	public EOQualifier getQualifierForTypeTel() {
		EOQualifier qual = (EOQualifier) valueForBinding(BINDING_qualifierForTypeTel);
		return qual;
	}

	public EOTypeTel getSelectedTypeTel() {
		return selectedTypeTel;
	}

	public void setSelectedTypeTel(EOTypeTel selectedTypeTel) {
		this.selectedTypeTel = selectedTypeTel;
	}

	public EOPaysIndicatif getIndicatifDefaut() {
		if (indicatifDefaut == null) {
			indicatifDefaut = EOPersonneTelephone.getIndicatifPaysDefaut(edc());
		}
		return indicatifDefaut;
	}

	public String paysIndicatifId() {
		return getComponentId() + "_FPaysIndicatif";
	}

	public String paysIndicatifAfterUpdateElement() {
		String res = "";
		res += " function(text, li) {";
		res += "        if (typeof(li)=='undefined') { ";
		res += "        $('" + paysIndicatifId() + "_field').value = '';}";
		res += "     else { ";
		res += "    var chaine = li.innerText;";
		res += "    if (typeof(chaine)=='undefined') {chaine = cleanString(li.textContent);}";
		res += "    $('" + paysIndicatifId() + "_field').value = chaine.replace(/^(\\w*)(\\s*)(.*)/g ,\"$1\");  Element.hide('busy1');}}";
		return res;
	}

	/**
	 * @return Le type de telephone a selectionner par defaut
	 */
	public EOTypeTel defaultTypeTel() {
		if (hasBinding(BINDING_defaultTypeTel)) {
			return EOTypeTel.fetchByKeyValue(edc(), EOTypeTel.C_TYPE_TEL_KEY, valueForBinding(BINDING_defaultTypeTel));
		}
		return null;
	}

	public void resetEdc() {
		_editingContext = null;
	}

	public String getSNoTelephone() {
		return sNoTelephone;
	}

	public void setSNoTelephone(String sNoTelephone) {
		this.sNoTelephone = sNoTelephone;
	}

	public Boolean hasTelephones() {
		return Boolean.valueOf(getLesPersonneTelephones().count() > 0);
	}

	public EOTypeNoTel selectedTypeNoTel() {
		if ((selectedTypeNoTel == null) && (hasBinding(BINDING_selectedTypeNoTel))) {
			selectedTypeNoTel = (EOTypeNoTel) valueForBinding(BINDING_selectedTypeNoTel);
		}
		return selectedTypeNoTel;
	}

	public void setSelectedTypeNoTel(EOTypeNoTel selectedTypeNoTel) {
		this.selectedTypeNoTel = selectedTypeNoTel;
		if (canSetValueForBinding(BINDING_selectedTypeNoTel)) {
			setValueForBinding(selectedTypeNoTel, BINDING_selectedTypeNoTel);
		}
	}

	public String auctypetelid() {
		return getComponentId() + "_auctypetelid";
	}
	
	public String aucpaysindicatifid() {
		return getComponentId() + "_aucpaysindicatifid";
	}

	public EOPersonneTelephone unPersonneTelephone() {
		return unPersonneTelephone;
	}

	public void setUnPersonneTelephone(EOPersonneTelephone unPersonneTelephone) {
		this.unPersonneTelephone = unPersonneTelephone;
		if (canSetValueForBinding(BINDING_unPersonneTelephone)) {
			setValueForBinding(unPersonneTelephone, BINDING_unPersonneTelephone);
		}
	}

	public String getStructureRattachementId() {
		return "StrR_" + getComponentId();
	}

	/**
	 * @return the selectedStructureTel
	 */
	public EOStructure selectedStructureTel() {
		return selectedStructureTel;
	}

	/**
	 * @param selectedStructureTel the selectedStructureTel to set
	 */
	public void setSelectedStructureTel(EOStructure selectedStructureTel) {
		this.selectedStructureTel = selectedStructureTel;
	}

	public WOActionResults onTelephoneEdit() {
		setIsEditing(Boolean.TRUE);
		// set unPersonneTelephone() comme le num en edition
		setSelectedPersonneTelephone(unPersonneTelephone());
		// Initialisation du formulaire à partir du téléphone
		setSelectedTypeNoTel(unPersonneTelephone().toTypeNoTel().localInstanceIn(edc()));
		selectedTypeTel = unPersonneTelephone().toTypeTel().localInstanceIn(edc());
		setSNoTelephone(unPersonneTelephone().noTelephone());
		if (unPersonneTelephone().indicatif() != null) {
			setFiltPaysIndicatif(unPersonneTelephone().indicatif().toString());
		}
		if (unPersonneTelephone().toStructureRattachement() != null) {
			setSelectedStructureTel(unPersonneTelephone().toStructureRattachement().localInstanceIn(edc()));
		}
		if (unPersonneTelephone().listeRouge() != null) {
			setListeRouge(unPersonneTelephone().listeRouge());
		} else {
			setListeRouge(null);
		}
		return null;
	}

	public String principalRadioButtonID() {
		return getComponentId() + "_RADTelPrincipal_" + getPersonneTelephoneIndex();
	}

	public String principalRadioButtonName() {
		return getComponentId() + "_RADTelPrincipal";
	}

	/**
	 * @return the personneTelephoneIndex
	 */
	public int getPersonneTelephoneIndex() {
		return personneTelephoneIndex;
	}

	/**
	 * @param personneTelephoneIndex the personneTelephoneIndex to set
	 */
	public void setPersonneTelephoneIndex(int personneTelephoneIndex) {
		this.personneTelephoneIndex = personneTelephoneIndex;
	}

	public boolean isTelEditEnabled() {
		boolean res = true;
		if (myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PERSONNE_TEL_PRF_DESACTIVE)
				&& unPersonneTelephone.toTypeTel().cTypeTel().equals(EOTypeTel.C_TYPE_TEL_PRF)) {
			return false;
		}
		if (myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PERSONNE_TEL_INT_DESACTIVE)
				&& unPersonneTelephone.toTypeTel().cTypeTel().equals(EOTypeTel.C_TYPE_TEL_INT)) {
			return false;
		}
		if (getQualifierForTypeTel() != null) {
			res = getQualifierForTypeTel().evaluateWithObject(unPersonneTelephone.toTypeTel());
		}
		return res && getAppUserForAnnuaire().hasDroitModificationEOPersonneTelephone(unPersonneTelephone());
	}

	public boolean isTelSupprimerEnabled() {
		boolean res = true;
		if (ERXProperties.booleanForKey("org.cocktail.profiler.ProTelModification.disabled")
				&& unPersonneTelephone.toTypeTel().cTypeTel().equals(EOTypeTel.C_TYPE_TEL_PRF)) {
			return false;
		}
		if (ERXProperties.booleanForKey("org.cocktail.profiler.IntTelModification.disabled")
				&& unPersonneTelephone.toTypeTel().cTypeTel().equals(EOTypeTel.C_TYPE_TEL_INT)) {
			return false;
		}
		if (getQualifierForTypeTel() != null) {
			res = getQualifierForTypeTel().evaluateWithObject(unPersonneTelephone.toTypeTel());
		}
		return res && getAppUserForAnnuaire().hasDroitModificationEOPersonneTelephone(unPersonneTelephone());
	}

	public boolean canDefineTelPrincipal() {
		if (isReadOnly().booleanValue()) {
			return false;
		}
		boolean res = true;
		if (getQualifierForTypeTel() != null) {
			res = getQualifierForTypeTel().evaluateWithObject(unPersonneTelephone.toTypeTel());
		}
		return res && getAppUserForAnnuaire().hasDroitModificationEOPersonneTelephone(unPersonneTelephone());
	}

	/**
	 * @return the isTelEditMode
	 */
	public boolean isTelEditMode() {
		return (!isReadOnly() && isEditing().booleanValue() && (getSelectedPersonneTelephone() != null) && (getSelectedPersonneTelephone().equals(unPersonneTelephone())));
	}

	/**
	 * @return the selectedPersonneTelephone
	 */
	public EOPersonneTelephone getSelectedPersonneTelephone() {
		return selectedPersonneTelephone;
	}

	/**
	 * @param selectedPersonneTelephone the selectedPersonneTelephone to set
	 */
	public void setSelectedPersonneTelephone(EOPersonneTelephone selectedPersonneTelephone) {
		this.selectedPersonneTelephone = selectedPersonneTelephone;
	}

	public Boolean isNewTelEditMode() {
		return (!isReadOnly() && getSelectedPersonneTelephone() == null);
	}

	public WOActionResults onTelephoneCancelEdit() {
		edc().revert();
		if (isTelephoneAdded()) {
			setTelephoneAdded(false);
		}
		cleanState();
		setIsEditing(Boolean.FALSE);
		return null;
	}

	public void cleanState() {
		setSelectedPersonneTelephone(null);
		setSelectedTypeTel(null);
		if (defaultTypeTel() != null && getLestypesTel().indexOf(defaultTypeTel()) != NSArray.NotFound) {
			setSelectedTypeTel(defaultTypeTel());
		} else {
			if (getLestypesTel() != null) {
				setSelectedTypeTel((EOTypeTel) getLestypesTel().objectAtIndex(0));
			}
		}
		setSNoTelephone(null);
		setFiltPaysIndicatif(null);
		setSelectedStructureTel(null);
		setListeRouge(null);
	}

	//	public boolean showAddTelForm() {
	//		return ((!isReadOnly()) && (getSelectedPersonneTelephone() == null) && getAppUserForAnnuaire().hasDroitCreationEOPersonneTelephone(getPersonne()));
	//	}

	public boolean showAddTelForm() {
		return isTelephoneAdded() && isEditing().booleanValue();
	}

	public boolean isTelephoneAdded() {
		return telephoneAdded;
	}

	public void setTelephoneAdded(boolean telephoneAdded) {
		this.telephoneAdded = telephoneAdded;
	}

	public boolean isOneTelInEditMode() {
		return ((!isReadOnly()) && (getSelectedPersonneTelephone() != null));
	}

	private Boolean wantReset() {
		return booleanValueForBinding(WANT_RESET, false);
	}

	private void setWantReset(Boolean value) {
		setValueForBinding(value, WANT_RESET);
	}

	public String getButtonTDTagString() {
		String otherTagString = "rowspan = \"3\"";
		if (!isStructureVisible()) {
			otherTagString = null;
		}
		return otherTagString;
	}

	public String getListeRougeOnName() {
		return getComponentId() + "_ListeRougeON";
	}
	
	public boolean isTypeTelInterne() {
		if (getSelectedTypeTel() == null) {
			return false;
		}
		
		return getSelectedTypeTel().isTypeInterne();
		
	}

	public WOActionResults onChangeTypeTel() {
		if (isTypeTelInterne()) {
			setSelectedPaysIndicatif(null);
		}
		
		return doNothing();
	}

}
