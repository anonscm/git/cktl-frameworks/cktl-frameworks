/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Permet de selectionner les alias de groupe (sans afficher le champ texte)
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @deprecated Utilisez à la place CktlAjaxGroupeSelect avec le binding showField positionné à False.
 */
public class CktlAjaxGroupeAliasSelect extends CktlAjaxGroupeSelect {

	private static final long serialVersionUID = -3147928493473286300L;
	private static final String ON_SUCCESS_BDG = "onSuccessSelect";

	public CktlAjaxGroupeAliasSelect(WOContext context) {
		super(context);
	}

	@Override
	public WOActionResults onSuccessSelect() {
		if (hasBinding(ON_SUCCESS_BDG))
			valueForBinding(ON_SUCCESS_BDG);
		return super.onSuccessSelect();
	}

}