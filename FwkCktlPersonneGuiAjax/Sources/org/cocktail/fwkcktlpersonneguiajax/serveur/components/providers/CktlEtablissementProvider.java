package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;

/**
 * 
 * @author jlafourc
 *
 */
public interface CktlEtablissementProvider extends CktlAjaxSelect2RemoteDataProvider {

	/**
	 * 
	 * @return l'établissement sélectionné
	 */
	EORne getSelectedEtablissement();
	
	/**
	 * 
	 * @param etablissement l'établissement sélectionné
	 */
	void setSelectedEtablissement(EORne etablissement);
	
	/**
	 * 
	 * @return le département
	 */
	EODepartement getDepartementFiltre();
	
}
