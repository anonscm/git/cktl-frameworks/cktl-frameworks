package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.providersAdresse;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AdresseFormPourONP;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Provider pour la liste de selection des types de voie des adresses
 * 
 */
public class TypeVoieAdresseProvider implements CktlAjaxSelect2RemoteDataProvider {

	private AdresseFormPourONP adresseFormOnp;
	
	
	public TypeVoieAdresseProvider(AdresseFormPourONP adresseOnp) {
		super();
		setAdresseFormOnp(adresseOnp);
	}

	public List<Result> results(String searchTerm) {
		NSArray<EOTypeVoie> typesVoie = getAdresseFormOnp().getCtrl().getFilteredTypeVoie(searchTerm);
		List<Result> results = new ArrayList<Result>();
		for (EOTypeVoie typeVoie : typesVoie) {
			results.add(resultFor(typeVoie));
		}
		return results;
	}

	public EOEditingContext edc() {
		return getAdresseFormOnp().edc();
	}
	

	public AdresseFormPourONP getAdresseFormOnp() {
		return adresseFormOnp;
	}

	public void setAdresseFormOnp(AdresseFormPourONP adresseFormOnp) {
		this.adresseFormOnp = adresseFormOnp;
	}
	
	public Result onSelect(String idSelection) {
		String[] selection = idSelection.split("_");
		EOQualifier qualifier;
		if (selection != null) {
			String libelleVoie = selection[0];
			qualifier = EOTypeVoie.LIBELLE_VOIE.eq(libelleVoie);
		} else {
			String libelleVoie = "Rue";
			qualifier = EOTypeVoie.LIBELLE_VOIE.eq(libelleVoie);
		}
		EOTypeVoie typeVoie = EOTypeVoie.fetchByQualifier(edc(), qualifier);
		getAdresseFormOnp().setSelectedTypeVoie(typeVoie);
		return new Result(typeVoie.libelleVoie(), typeVoie.libelleVoie());
	}

	public Result selectionInitiale() {
		if (getAdresseFormOnp().getAdresse() == null || getAdresseFormOnp().getAdresse().toTypeVoie() == null) {
			return new Result("Rue", "Rue");
		}
		return new Result(getAdresseFormOnp().getAdresse().toTypeVoie().libelleVoie(), getAdresseFormOnp().getAdresse().toTypeVoie().libelleVoie());
	}


	private Result resultFor(EOTypeVoie typeVoie) {
		Result result = null;
		if (typeVoie != null) {
			result = new Result(typeVoie.libelleVoie(), typeVoie.libelleVoie());
		}
		return result;
	}

}

