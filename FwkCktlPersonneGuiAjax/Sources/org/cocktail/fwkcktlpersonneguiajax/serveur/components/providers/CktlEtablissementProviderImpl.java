package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
/**
 * 
 * @author jlafourc
 *
 */
public abstract class CktlEtablissementProviderImpl implements CktlEtablissementProvider {

    private EOEditingContext edc;
    
    /**
     * @param edc editingContext de travail
     */
    public CktlEtablissementProviderImpl(EOEditingContext edc) {
        this.edc = edc;
    }
	
	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();
		
		if (StringUtils.isNotBlank(searchTerm)) {

			EOQualifier qualifier = EORne.LL_RNE.contains(searchTerm).or(EORne.C_RNE.contains(searchTerm));
			if (getDepartementFiltre() != null) {
				qualifier = ERXQ.and(qualifier, (EORne.C_RNE.startsWith(getDepartementFiltre().cDepartement())));
				//qualifier = ERXQ.and(qualifier, EORne.CODE_DEPARTEMENT.eq(getDepartementFiltre().cDepartement()));
			}
			if (isShowOnlyEtablissementsPereFiltre()) {
				qualifier = ERXQ.and(qualifier, EORne.TO_RNE_PERE.isNull());
			}

			ERXSortOrderings sortOrderings = EORne.LL_RNE.ascInsensitives();

			for (EORne rne : EORne.fetchAll(edc, qualifier, sortOrderings)) {
				resultat.add(new Result(rne.cRne(), rne.cRne() + " - " +  rne.llRne()));
			}
		
		}
		
		return resultat;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedEtablissement(null);
			return null;
		}
		EORne rne = EORne.fetchFirstByQualifier(edc, EORne.C_RNE.eq(idSelection));
		if (rne == null) {
			return null;
		} else {
			setSelectedEtablissement(rne);
			return new Result(rne.cRne(), rne.cRne() + " - " + rne.llRne());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedEtablissement() == null) {
			return null;
		} else {
			return new Result(getSelectedEtablissement().cRne(), getSelectedEtablissement().cRne() + " - " + getSelectedEtablissement().llRne());
		}	
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract EORne getSelectedEtablissement();

	/**
	 * {@inheritDoc}
	 */
	public abstract void setSelectedEtablissement(EORne etablissement);

	/**
	 * {@inheritDoc}
	 */
	public abstract EODepartement getDepartementFiltre();

	/**
	 * {@inheritDoc}
	 */
	public abstract boolean isShowOnlyEtablissementsPereFiltre();

}
