package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

/**
 * 
 */
public interface CktlEtablissementEtrangerProvider extends CktlAjaxSelect2RemoteDataProvider {

	/**
	 * 
	 * @return l'établissement étranger sélectionné
	 */
	EOStructure getSelectedEtablissementEtranger();
	
	/**
	 * 
	 * @param etablissement l'établissement étranger sélectionné
	 */
	void setSelectedEtablissementEtranger(EOStructure etablissement);
	
	/**
	 * 
	 * @return le pays de l'établissement
	 */
	EOPays getPaysFiltre();
	
}
