package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 * @author jlafourc
 *
 */
public interface CktlPaysProvider extends CktlAjaxSelect2RemoteDataProvider {

	/**
	 * 
	 * @return le pays sélectionné
	 */
	EOPays getSelectedPays();
	
	/**
	 * 
	 * @param pays le pays sélectionné
	 */
	void setSelectedPays(EOPays pays);
	
}
