package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.providersAdresse;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AdresseFormPourONP;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Provider pour la liste de selection des pays actuels
 * 
 */
public class PaysAdresseProvider implements CktlAjaxSelect2RemoteDataProvider {
	
	private AdresseFormPourONP adresseFormOnp;
	
	public PaysAdresseProvider(AdresseFormPourONP adresseOnp) {
		super();
		setAdresseFormOnp(adresseOnp);
	}
	
	public EOEditingContext edc() {
		return getAdresseFormOnp().edc();
	}
	

	public AdresseFormPourONP getAdresseFormOnp() {
		return adresseFormOnp;
	}

	public void setAdresseFormOnp(AdresseFormPourONP adresseFormOnp) {
		this.adresseFormOnp = adresseFormOnp;
	}

	public List<Result> results(String searchTerm) {
		NSArray<EOPays> nations = getAdresseFormOnp().getCtrl().getFilteredPaysActuels(searchTerm);
		List<Result> results = new ArrayList<Result>();
		for (EOPays pays : nations) {
			results.add(resultFor(pays));
		}
		return results;
	}

	public Result onSelect(String idSelection) {
		String[] selection = idSelection.split("_");
		String llPays = selection[0];
		EOQualifier qualifier = EOPays.LL_PAYS.eq(llPays);
		EOPays pays = EOPays.fetchByQualifier(edc(), qualifier);
		getAdresseFormOnp().setSelectedPays(pays);
		return new Result(pays.llPays(), pays.llPays());
	}

	public Result selectionInitiale() {
		return new Result(getAdresseFormOnp().getAdresse().toPays().llPays(), getAdresseFormOnp().getAdresse().toPays().llPays());
	}


	private Result resultFor(EOPays pays) {
		Result result = null;
		if (pays != null) {
			result = new Result(pays.llPays(), pays.llPays());
		}
		return result;
	}

}
