package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * 
 * @author jlafourc
 *
 */
public abstract class CktlPaysProviderImpl implements CktlPaysProvider {

	private EOEditingContext edc;
	
	/**
	 * 
	 * @param edc editingContext à utiliser
	 */
	public CktlPaysProviderImpl(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		
		List<Result> resultat = new ArrayList<Result>();
		
		if (StringUtils.isNotBlank(searchTerm)) {
		
			EOQualifier qualifier = EOPays.LL_PAYS.startsWithInsensitive(searchTerm);
			ERXSortOrderings sortOrderings = EOPays.LL_PAYS.ascInsensitives();
			
			for (EOPays pays : EOPays.fetchAll(edc, qualifier, sortOrderings)) {
				resultat.add(new Result(pays.cPays(), pays.llPays()));
			}
		
		}
		
		return resultat;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedPays(null);
			return null;
		}
		EOPays pays = EOPays.fetchPaysbyCode(edc, idSelection);
		if (pays == null) {
			return null;
		} else {
			setSelectedPays(pays);
			return new Result(pays.cPays(), pays.llPays());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedPays() == null) {
			return null;
		} else {
			return new Result(getSelectedPays().cPays(), getSelectedPays().llPays());
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public abstract EOPays getSelectedPays();

	/**
	 * {@inheritDoc}
	 */
	public abstract void setSelectedPays(EOPays pays);
}
