package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.providersAdresse;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AdresseFormPourONP;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * Provider pour la liste de selection des code postaux - communes
 * 
 */
public class CodePostalVilleProvider implements CktlAjaxSelect2RemoteDataProvider {

	private AdresseFormPourONP adresseFormOnp;
	
	
	public CodePostalVilleProvider(AdresseFormPourONP adresseOnp) {
		super();
		setAdresseFormOnp(adresseOnp);
	}
	
	public EOEditingContext edc() {
		return getAdresseFormOnp().edc();
	}
	

	public AdresseFormPourONP getAdresseFormOnp() {
		return adresseFormOnp;
	}

	public void setAdresseFormOnp(AdresseFormPourONP adresseFormOnp) {
		this.adresseFormOnp = adresseFormOnp;
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		return new Result(getAdresseFormOnp().getAdresse().codePostal(), getAdresseFormOnp().getAdresse().codePostal());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		NSArray<EOCommune> communes = getAdresseFormOnp().getCtrl().getFilteredCommunes(searchTerm);
		List<Result> results = new ArrayList<Result>();
		for (EOCommune commune : communes) {
			results.add(resultFor(commune));
		}
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		String[] selection = idSelection.split("_");
		String cInsee = selection[0];
		String cPostal = selection[1];
		EOQualifier qualifier = EOCommune.C_INSEE.eq(cInsee).and(EOCommune.C_POSTAL.eq(cPostal));
		EOCommune commune = EOCommune.fetchByQualifier(edc(), qualifier);
		getAdresseFormOnp().setSelectedCommune(commune);
		return new Result(cPostal, cPostal);
	}

	private Result resultFor(EOCommune commune) {
		Result result = null;
		if (commune != null) {
			result = new Result(commune.cInsee() + "_" + commune.cPostal(), commune.codePostalEtLlComm());
		}
		return result;
	}

}
