package org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.foundation.ERXArrayUtilities;
/**
 * 
 */
public abstract class CktlEtablissementEtrangerProviderImpl implements CktlEtablissementEtrangerProvider {

    private EOEditingContext edc;
    private EOQualifier qualifierEtablissementEtranger;
    
    /**
     * @param edc editingContext de travail
     * @param qualifierEtablissementEtranger qualifier des établissements étrangers
     */
    public CktlEtablissementEtrangerProviderImpl(EOEditingContext edc, EOQualifier qualifierEtablissementEtranger) {
        this.edc = edc;
        this.qualifierEtablissementEtranger = qualifierEtablissementEtranger;
    }
	
	/**
	 * {@inheritDoc}
	 */
	public List<Result> results(String searchTerm) {
		List<Result> resultat = new ArrayList<Result>();
		
		NSArray<EOStructure> groupes = EOStructure.fetchAll(edc, qualifierEtablissementEtranger);
		EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).in(groupes);
		
		if (StringUtils.isNotBlank(searchTerm)) {
			qualifier = ERXQ.and(
					qualifier,
					EOStructure.LL_STRUCTURE.contains(searchTerm).or(EOStructure.LC_STRUCTURE.contains(searchTerm))
					);
		}
		
		if (getPaysFiltre() != null) {
			qualifier = ERXQ.and(qualifier,
					EOStructure.TO_REPART_PERSONNE_ADRESSES.dot(EORepartPersonneAdresse.TO_ADRESSE.dot(EOAdresse.C_PAYS_KEY)).eq(getPaysFiltre().cPays())
					);
		}

		ERXSortOrderings sortOrderings = EOStructure.LL_STRUCTURE.ascInsensitives();
		NSArray<EOStructure> lesEtablissementsEtrangers = ERXArrayUtilities.arrayWithoutDuplicates(EOStructure.fetchAll(edc, qualifier, sortOrderings));
			
		for (EOStructure etablissement : lesEtablissementsEtrangers) {
			resultat.add(new Result(etablissement.cStructure(), etablissement.llStructure()));
		}
		
		return resultat;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Result onSelect(String idSelection) {
		// cas de la suppression
		if (StringUtils.isEmpty(idSelection)) {
			setSelectedEtablissementEtranger(null);
			return null;
		}
		EOStructure etab = EOStructure.fetchFirstByQualifier(edc, EOStructure.C_STRUCTURE.eq(idSelection));
		if (etab == null) {
			return null;
		} else {
			setSelectedEtablissementEtranger(etab);
			return new Result(etab.cStructure(), etab.llStructure());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Result selectionInitiale() {
		if (getSelectedEtablissementEtranger() == null) {
			return null;
		} else {
			return new Result(getSelectedEtablissementEtranger().cStructure(), getSelectedEtablissementEtranger().llStructure());
		}	
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract EOStructure getSelectedEtablissementEtranger();

	/**
	 * {@inheritDoc}
	 */
	public abstract void setSelectedEtablissementEtranger(EOStructure etablissement);

	/**
	 * {@inheritDoc}
	 */
	public abstract EOPays getPaysFiltre();


}
