/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.CktlAjaxAssociationTreeViewCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * Affiche un arbre d'association et permet la sélection.
 * 
 * @binding selection Variable pour la selection
 * @binding treeQualifier Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être
 *          applicable aux objets EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur.
 * @binding userFiltersDictionary Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent
 *          dans une liste deroulante, le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent êre
 *          applicable aux objets EOStructure.
 * @binding userFiltersFormID ID de l'objet FORM contenant le composant (necessaire si vous specifiez des filtres utilisateurs)
 * @binding treeRootObject Facultatif. Binding pour specifier la racine de l'arbre.
 * @binding textFieldSize Facultatif. Binding pour specifier la taille du champ (en colonnes).
 * @binding onSelectUpdateContainerID Facultatif. Binding pour specifier un container a mettre a jour qd la selection est effectuee.
 * @binding confirmBeforeSelection Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection.
 * @binding didSelectAssociation. Binding pour manipuler le modal window id avec le parent (will set)
 */
//public class CktlAjaxAssociationTreeView extends AComponent {
public class CktlAjaxAssociationTreeView extends ATreeViewComponent {

	private static final long serialVersionUID = 2300676302674499493L;

	/** Facultatif. Binding pour specifier la racine de l'arbre. */
	public static final String TREE_ROOT_OBJECT_BDG = "treeRootObject";
	/**
	 * Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être applicable aux objets
	 * EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur.
	 */
	public static final String TREE_QUALIFIER_BDG = "treeQualifier";
	/**
	 * Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent dans une liste deroulante,
	 * le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent êre applicable aux objets
	 * EOStrcuture.
	 */
	public static final String USER_FILTERS_DICTIONARY_BDG = "userFiltersDictionary";
	/** Facultatif. Binding spécifiant la classe css à appliquer sur le gorupe sélectionné */
	public static final String CSS_SELECTED_ASSOCIATION_BDG = "cssSelectedAssociation";

	public static final String DID_SELECT_ASSOCIATION = "didSelectAssociation";

	public static final String TREE_COLLAPSED_IMG_BDG = "treeCollapsedImage";
	public static final String TREE_COLLAPSED_IMG_FWK_BDG = "treeCollapsedImageFramework";
	public static final String TREE_EXPANDED_IMG_BDG = "treeExpandedImage";
	public static final String TREE_EXPANDED_IMG_FWK_BDG = "treeExpandedImageFramework";
	public static final String TREE_LEAF_IMG_BDG = "treeLeafImage";
	public static final String TREE_LEAF_IMG_FWK_BDG = "treeLeafImageFramework";

	public static final String TREE_VIEW_TITLE_BDG = "treeViewTitle";
	public static final String TREE_VIEW_WIDTH_BDG = "treeViewWidth";
	public static final String TREE_VIEW_HEIGHT_BDG = "treeViewHeight";
	public static final String TREE_VIEW_CLASS_NAME_BDG = "treeViewClassName";

	/** Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection. */
	public static final String CONFIRM_BEFORE_SELECTION_BDG = "confirmBeforeSelection";
	private int associationIndex;
	/** Facultatif. Binding pour specifier la racine de l'arbre. */
	public static final String TREE_PARENT_OBJECT_BDG = "treeParentObject";
	/** Facultatif. Binding boolean spécifiant si l'arbre doit être reseté */
	public static final String RESET_BDG = "reset";
	/** Facultatif. */
	public static final String EXPAND_TO_LEVEL_BDG = "expandToLevel";

	private CktlAjaxAssociationTreeViewCtrl ctrl;
	private EOAssociation selection;
	private EOAssociation selectionPere;
	private EOQualifier userFiltreQualifier;
	private EOQualifier treeQualifier;
	private NSDictionary filtres;
	private String unFiltre, unFiltreSelectionne;
	private String collapsedImage, collapsedImageFramework;
	private String expandedImage, expandedImageFramework;
	private String leafImage, leafImageFramework;
	private boolean _firstRender = true;
	
	

	public CktlAjaxAssociationTreeView(WOContext context) {
		super(context);
		ctrl = new CktlAjaxAssociationTreeViewCtrl(this);
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (ctrl().getWocomponent() == null) {
			ctrl().setWocomponent(this);
		}
		if (hasBinding(RESET_BDG)) {
			if (Boolean.TRUE.equals(valueForBinding(RESET_BDG))) {
				resetTree();
				expandToLevel();
			}
			if (canSetValueForBinding(RESET_BDG)) {
				setValueForBinding(Boolean.FALSE, RESET_BDG);
			}
		}
		if (_firstRender) {
		    expandToLevel();
		}
		super.appendToResponse(response, context);
		_firstRender = false;
	}
	
	
	public EOAssociation selection() {
		if (selection != valueForBinding(SELECTION_BDG))
			selection = (EOAssociation) valueForBinding(SELECTION_BDG);
		return selection;
	}

	/**
	 * @param selection
	 */
	public void setSelection(EOAssociation selection) {
		this.selection = selection;
		setValueForBinding(selection, CktlAjaxAssociationTreeView.SELECTION_BDG);
	}
	
	

	public EOAssociation getSelectionPere() {
		return selectionPere;
	}

	public void setSelectionPere(EOAssociation parentSelection) {
		this.selectionPere = parentSelection;
		setValueForBinding(selectionPere, TREE_PARENT_OBJECT_BDG);
	}
	
	
	private void expandToLevel() {
	    if (hasBinding(EXPAND_TO_LEVEL_BDG)) {
            Integer level = (Integer)valueForBinding(EXPAND_TO_LEVEL_BDG);
            ctrl().getMyTreeModel().setRootTreeNode(ctrl().rootAssociation());
            ctrl().expandAllNodesToLevel(level);
        }
	}
	
	private void resetTree() {
		ctrl().setRootAssociation(null);
		ctrl().rootAssociation().setChildren(null);
		EOAssociation association = selection();
		if (association != null)
			ctrl().selectObjectInTree(ctrl().rootAssociation(), association);
	}
	
	
	

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}

	public String filtrePopUpID() {
		return getComponentId() + "_filtrePopUp";
	}

	public EOAssociation treeRootObject() {
		return (EOAssociation) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

	public EOQualifier getQualifier() {
		EOQualifier qual = getTreeQualifier();
		if (qual != null) {
			if (getUserFiltreQualifier() != null) {
				qual = new EOAndQualifier(new NSArray(new Object[] {
						qual, getUserFiltreQualifier()
				}));
			}
		}
		else {
			qual = getUserFiltreQualifier();
		}
		return qual;
	}

	public EOQualifier getTreeQualifier() {
		if (treeQualifier == null) {
			treeQualifier = (EOQualifier) valueForBinding(TREE_QUALIFIER_BDG);
		}
		return treeQualifier;
	}

	public void setTreeViewQualifier(EOQualifier treeViewQualifier) {
		this.treeQualifier = treeViewQualifier;
	}

	public void setUserFiltreQualifier(EOQualifier userFiltreQualifier) {
		this.userFiltreQualifier = userFiltreQualifier;
	}

	public EOQualifier getUserFiltreQualifier() {
		return userFiltreQualifier;
	}

	public WOActionResults filtrer() {
		String filtre = unFiltreSelectionne();
		if (filtre != null) {
//			if (filtre.equalsIgnoreCase("Tous")) {
//				setUserFiltreQualifier(null);
//			}
//			else {
//				setUserFiltreQualifier((EOQualifier) filtres.objectForKey(filtre));
//			}
			setUserFiltreQualifier((EOQualifier) filtres.objectForKey(filtre));
			ctrl().setRootAssociation(null);
		}
		else {
//			setUserFiltreQualifier(null);
//			setUserFiltreQualifier((EOQualifier) filtres.objectForKey("Valides"));
			setUserFiltreQualifier(EOAssociation.QUAL_ASS_VALIDE);
			ctrl().setRootAssociation(null);
		}
		return null;
	}

	public WOActionResults didSelectAssociation() {
		ctrl().getWocomponent().reset();
		return (WOActionResults) valueForBinding(DID_SELECT_ASSOCIATION);
	}

	
	public boolean isFiltrageAvailable() {
		boolean isFiltrageAvailable = hasBinding(USER_FILTERS_DICTIONARY_BDG);
		if (isFiltrageAvailable) {
			NSDictionary dicoFiltres = filtres();
			isFiltrageAvailable = dicoFiltres.count() > 0 ? true : false;
		}
		return isFiltrageAvailable;
	}

	public NSDictionary filtres() {
		if (filtres == null) {
			filtres = (NSDictionary) valueForBinding(USER_FILTERS_DICTIONARY_BDG);
		}
		return filtres;
	}

	public String onSuccessSelect() {
		String onSuccessSelect = null;
		onSuccessSelect = "function () {Windows.close('";
		onSuccessSelect += getComponentId() + "_win');}";

		return onSuccessSelect;
	}

	public String selectionOnClickBefore() {
		if (hasBinding(CONFIRM_BEFORE_SELECTION_BDG)) {
			if (((Boolean) valueForBinding(CONFIRM_BEFORE_SELECTION_BDG)).booleanValue()) {
				return ("confirm('" + jsEncode("Voulez-vous selectionner l'association " + ctrl.uneAssociationLibelle() + " ?") + "')");
			}
		}
		return null;
	}

	/**
	 * @return the collapsedImage
	 */
	public String getTreeCollapsedImage() {
		if (collapsedImage == null) {
			collapsedImage = (String) valueForBinding(TREE_COLLAPSED_IMG_BDG);
			if (collapsedImage == null) {
				collapsedImage = "images/ico_node_collapsed_gris_16.png";
				collapsedImageFramework = "FwkCktlAjaxWebExt.framework";
			}
		}

		return collapsedImage;
	}

	/**
	 * @param collapsedImage the collapsedImage to set
	 */
	public void setTreeCollapsedImage(String collapsedImage) {
		this.collapsedImage = collapsedImage;
	}

	/**
	 * @return the collapsedImageFramework
	 */
	public String getTreeCollapsedImageFramework() {
		if (collapsedImageFramework == null) {
			collapsedImageFramework = (String) valueForBinding(TREE_COLLAPSED_IMG_FWK_BDG);
		}

		return collapsedImageFramework;
	}

	/**
	 * @param collapsedImageFramework the collapsedImageFramework to set
	 */
	public void setTreeCollapsedImageFramework(String collapsedImageFramework) {
		this.collapsedImageFramework = collapsedImageFramework;
	}

	/**
	 * @return the expandedImage
	 */
	public String getTreeExpandedImage() {
		if (expandedImage == null) {
			expandedImage = (String) valueForBinding(TREE_EXPANDED_IMG_BDG);
			if (expandedImage == null) {
				expandedImage = "images/ico_node_expanded_gris_16.png";
				expandedImageFramework = "FwkCktlAjaxWebExt.framework";
			}
		}
		return expandedImage;
	}

	/**
	 * @param expandedImage the expandedImage to set
	 */
	public void setTreeExpandedImage(String expandedImage) {
		this.expandedImage = expandedImage;
	}

	/**
	 * @return the expandedImageFramework
	 */
	public String getTreeExpandedImageFramework() {
		if (expandedImageFramework == null) {
			expandedImageFramework = (String) valueForBinding(TREE_EXPANDED_IMG_FWK_BDG);
		}
		return expandedImageFramework;
	}

	/**
	 * @param expandedImageFramework the expandedImageFramework to set
	 */
	public void setTreeExpandedImageFramework(String expandedImageFramework) {
		this.expandedImageFramework = expandedImageFramework;
	}

	/**
	 * @return the leafImage
	 */
	public String getTreeLeafImage() {
		if (leafImage == null) {
			leafImage = (String) valueForBinding(TREE_LEAF_IMG_BDG);
			if (leafImage == null) {
				leafImage = "images/ico_node_leaf_16.png";
				leafImageFramework = "FwkCktlAjaxWebExt.framework";
			}
		}
		return leafImage;
	}

	/**
	 * @param leafImage the leafImage to set
	 */
	public void setTreeLeafImage(String leafImage) {
		this.leafImage = leafImage;
	}

	/**
	 * @return the leafImageFramework
	 */
	public String getTreeLeafImageFramework() {
		if (leafImageFramework == null) {
			leafImageFramework = (String) valueForBinding(TREE_LEAF_IMG_FWK_BDG);
		}
		return leafImageFramework;
	}

	/**
	 * @param leafImageFramework the leafImageFramework to set
	 */
	public void setTreeLeafImageFramework(String leafImageFramework) {
		this.leafImageFramework = leafImageFramework;
	}

	/**
	 * @return the ctrl
	 */
	public CktlAjaxAssociationTreeViewCtrl ctrl() {
		return ctrl;
	}

	/**
	 * @param ctrl the ctrl to set
	 */
	public void setCtrl(CktlAjaxAssociationTreeViewCtrl ctrl) {
		this.ctrl = ctrl;
	}

	/**
	 * @return the unFiltreSelectionne
	 */
	public String unFiltreSelectionne() {
		return unFiltreSelectionne;
	}

	/**
	 * @param unFiltreSelectionne the unFiltreSelectionne to set
	 */
	public void setUnFiltreSelectionne(String unFiltreSelectionne) {
		this.unFiltreSelectionne = unFiltreSelectionne;
	}

	/**
	 * @return the unFiltre
	 */
	public String unFiltre() {
		return unFiltre;
	}

	/**
	 * @param unFiltre the unFiltre to set
	 */
	public void setUnFiltre(String unFiltre) {
		this.unFiltre = unFiltre;
	}
	
	/**
	 * @return the groupeIndex
	 */
	public int getAssociationIndex() {
		return associationIndex;
	}

	/**
	 * @param groupeIndex the groupeIndex to set
	 */
	public void setAssociationIndex(int associationIndex) {
		this.associationIndex = associationIndex;
	}
	
	public String cssClassForSelectedLink() {

		if (hasBinding(CSS_SELECTED_ASSOCIATION_BDG) &&
				selection() != null && selection().equals(ctrl().uneAssociation().object())) {
			return (String) valueForBinding(CSS_SELECTED_ASSOCIATION_BDG);
		}
		else {
			return String.valueOf("");
		}
	}
	
}