package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.EORome;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class RomeTreeNode extends CktlTreeTableNode<EORome> {

	private NSArray<CktlTreeTableNode<EORome>> childrenNodes;

	/**
	 * 
	 * @param data the data to set
	 * @param parent the parent to set
	 * @param visible the visible to set 
	 */
	public RomeTreeNode(EORome data, CktlTreeTableNode<EORome> parent, Boolean visible) {
		super(data, parent, visible);
		// TODO Auto-generated constructor stub
	}

	@Override
	public NSArray<CktlTreeTableNode<EORome>> getChildren() {

		if (childrenNodes == null) {
			childrenNodes = new NSMutableArray<CktlTreeTableNode<EORome>>();
			EOQualifier qualifierAppli = null;
			EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EORome.ROM_CODE_KEY, EOSortOrdering.CompareAscending);
			NSArray<EORome> children = getData().toChildren(qualifierAppli,  new NSArray(new Object[] {sort}));
			
			for (EORome c : children) {
				RomeTreeNode romeNode = new RomeTreeNode(c, this, true);
				childrenNodes.add(romeNode);
			}
		}


		return childrenNodes;
	}
}
