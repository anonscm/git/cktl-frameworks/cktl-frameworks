/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxActionButton;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.GroupeAdminFormCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Formulaire de saisie du responsable et des secretaires d'un groupe. Utilisez plutot GroupeAdminUI.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class GroupeAdminForm extends AComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String BINDING_groupe = "groupe";
	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_shouldShowTypeStructure = "shouldShowTypeStructure";
	public static final String BINDING_shouldShowTypesGroupes = "shouldShowTypesGroupes";
	public static final String BINDING_shouldShowAliasGroupes = "shouldShowAliasGroupes";
	public static final String BINDING_shouldShowAccesGroupes = "shouldShowAccesGroupes";
	public static final Boolean DEFAULT_shouldShowTypeStructure = Boolean.TRUE;
	public static final Boolean DEFAULT_shouldShowTypesGroupes = Boolean.TRUE;
	public static final Boolean DEFAULT_shouldShowAliasGroupes = Boolean.TRUE;

	//	private EOIndividu selectedResponsable;
	private EOIndividu selectedSecretaire;
	public EOSecretariat unSecretariat;
	public EOSecretariat selectedSecretariat;

	private GroupeAdminFormCtrl ctrl;
	private EOEditingContext _editingContext;

	private EOStructure _groupe;
	public EOTypeGroupe unTypeGroupe;
	
	private String currentGrpAcces;

	private final TbvRepartTypeGroupeMapper repartTypeGroupeMapper = new TbvRepartTypeGroupeMapper();

	public GroupeAdminForm(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
			}
		}
	}

	public EOStructure groupe() {
		boolean hasChanged = false;
		EOStructure tmp = (EOStructure) valueForBinding(BINDING_groupe);
		if (tmp == null) {
			_groupe = tmp;
			hasChanged = true;
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
		} else {
			if (_groupe == null || _groupe.globalID() == null
					|| !_groupe.globalID().equals(tmp.globalID())) {
				//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
				resetEdc();
				_groupe = tmp.localInstanceIn(edc());
				hasChanged = true;
			}
		}
		if (hasChanged) {
			initialiseRepartTypeGroupeDisplayGroup(_groupe);
		}
		return _groupe;
	}

	private void initialiseRepartTypeGroupeDisplayGroup(EOStructure groupe) {
		getCtrl().initialiseRepartTypeGroupeDisplayGroup(groupe);
	}

	public EOIndividu getSelectedResponsable() {
		return groupe().toResponsable();
	}

	public void setSelectedResponsable(EOIndividu selectedResponsable) {
		try {
			edc().revert();
			groupe().setToResponsableRelationship(selectedResponsable);
			getCtrl().onResponsableSelected(selectedResponsable);

			edc().saveChanges();

		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} finally {
			edc().revert();
		}

	}

	public GroupeAdminFormCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (GroupeAdminFormCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new GroupeAdminFormCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}

	/*
	 * On travaille sur un nested editingContext.
	 */
	@Override
	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public EOQualifier getIndividusForResponsableQualifier() {
		return getCtrl().getIndividusForResponsableQualifier();
	}

	public EOQualifier getIndividusForSecretariatQualifier() {
		return getCtrl().getIndividusForSecretariatQualifier();
	}

	public String secretariatFieldId() {
		return getComponentId() + "_" + "secretariatField";
	}

	public String responsableFieldId() {
		return getComponentId() + "_" + "responsableField";
	}

	public WOActionResults save() {
		try {
			edc().saveChanges();
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onSecretariatSupprimer() {
		WOActionResults res = onSecretariatSupprimer(selectedSecretariat);
		selectedSecretariat = null;
		return res;
	}

	public WOActionResults onSecretariatAjouter() {
		if (selectedSecretaire != null) {
			try {
//				edc().revert();
				getCtrl().onSecretariatAjouter(selectedSecretaire);
				//			
				//						EOIndividu secr = selectedSecretaire;
				//						NSArray res = groupe().toSecretariats(new EOKeyValueQualifier(EOSecretariat.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, secr));
				//						if (res.count() > 0) {
				//							throw new NSValidation.ValidationException(selectedSecretaire.getNomPrenomAffichage() + " est deja secretaire du groupe " + groupe().getNomPrenomAffichage());
				//						}
				//			
				//						EOSecretariat secretariat = EOSecretariat.creerInstance(edc());
				//						secretariat.setToIndividuRelationship(secr);
				//						groupe().addToToSecretariatsRelationship(secretariat);
				//						secretariat.setCStructure(groupe().cStructure());
				//
				edc().saveChanges();

			} catch (Exception e) {
				e.printStackTrace();
				setErreurSaisieMessage(e.getMessage());
			} finally {
				edc().revert();
				setSelectedSecretaire(null);
			}
		}
		return null;
	}

	public WOActionResults onSecretariatSupprimer(EOSecretariat secretariat) {
		if (secretariat != null) {
			try {
//				edc().revert();

				getCtrl().onSecretariatSupprimer(secretariat);

				edc().saveChanges();

			} catch (Exception e) {
				e.printStackTrace();
				setErreurSaisieMessage(e.getMessage());
			} finally {
				edc().revert();
			}
		}
		return null;
	}

	public EOIndividu getSelectedSecretaire() {
		return selectedSecretaire;
	}

	public void setSelectedSecretaire(EOIndividu selectedSecretaire) {
		this.selectedSecretaire = selectedSecretaire;
		onSecretariatAjouter();
	}

	public Boolean secretariatDeleteEnabled() {
		return Boolean.valueOf(selectedSecretariat != null);
	}

	public String secretariatDeleteOnClickBefore() {
		if (selectedSecretariat != null) {
			return "confirm('Souhaitez-vous reelement supprimer " + selectedSecretariat.toIndividu().getNomPrenomAffichage() + " de la liste des secrétaires ?')";
		}
		return "";

	}

	public Boolean hasSecretariat() {
		return Boolean.valueOf(groupe().toSecretariats().count() > 0);
	}

	// ajouter les types de groupe dans l'interface
	//
	private static class TbvRepartTypeGroupeMapper {
		private static final String IND_KEY = "unTypeGroupe.";
		public static final String COL_TGRP_LIBELLE_KEY = EOTypeGroupe.TGRP_LIBELLE_KEY;
		public static final String COL_TGRP_CODE_KEY = EOTypeGroupe.TGRP_CODE_KEY;

		//		public static final NSMutableDictionary _tbvRepartTypeGroupecolonnesMap = new NSMutableDictionary();
		public static final NSMutableDictionary _colonnesMap = new NSMutableDictionary();
		static {
			CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
			col1.setLibelle("Type");
			col1.setOrderKeyPath(COL_TGRP_LIBELLE_KEY);
			CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_TGRP_LIBELLE_KEY, " ");
			col1.setAssociations(ass1);
			_colonnesMap.takeValueForKey(col1, COL_TGRP_LIBELLE_KEY);

			CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
			col2.setLibelle("Code");
			col2.setOrderKeyPath(COL_TGRP_CODE_KEY);
			CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_TGRP_CODE_KEY, " ");
			col2.setAssociations(ass2);
			_colonnesMap.takeValueForKey(col2, COL_TGRP_CODE_KEY);

		}

		public static NSArray DEFAULT_COLONNES_KEYS = new NSArray(new Object[] {
				COL_TGRP_LIBELLE_KEY, COL_TGRP_CODE_KEY
		});

		private NSArray colonnes;

		public NSArray getColonnes() {
			if (colonnes == null) {
				NSMutableArray res = new NSMutableArray();
				NSArray colkeys = getColonnesKeys();
				for (int i = 0; i < colkeys.count(); i++) {
					res.addObject(_colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));
				}
				colonnes = res.immutableClone();
			}
			return colonnes;
		}

		public NSArray getColonnesKeys() {
			NSArray keys = DEFAULT_COLONNES_KEYS;
			return keys;
		}

	}

	public NSArray getTbvRepartTypeGroupeColonnes() {
		return repartTypeGroupeMapper.getColonnes();
	}

	public String getTbvRepartTypeGroupeId() {
		return getComponentId() + "_TbvRepartTypeGroupe";
	}

	public String getTbvRepartTypeGroupeROId() {
		return getComponentId() + "_TbvRepartTypeGroupeRO";
	}

	public String getTypeGroupeContainerId() {
		return getComponentId() + "_TypeGroupeContainer";
	}

	public ERXDisplayGroup<EOTypeGroupe> repartTypeGroupeDisplayGroup() {
		return getCtrl().repartTypeGroupeDisplayGroup();
	}

	public WOActionResults onRepartEnregistrer() {
		try {
			edc().saveChanges();
			getCtrl().setIsRepartEditing(Boolean.FALSE);

		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} finally {
			edc().revert();
			getCtrl().afterRepartEnregistrer();
		}
		return doNothing();

	}

	public WOActionResults onRepartAnnuler() {
		try {
			edc().revert();
			getCtrl().setIsRepartEditing(Boolean.FALSE);
			getCtrl().afterRepartAnnuler();
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} finally {
			getCtrl().setIsRepartEditing(Boolean.FALSE);
			edc().revert();
		}
		return doNothing();
	}

	public WOActionResults saveGrpAcces() {
	    try {
            edc().saveChanges();
        } catch (Exception e) {
            e.printStackTrace();
            setErreurSaisieMessage(e.getMessage());

        } finally {
            edc().revert();
            getCtrl().afterRepartEnregistrer();
        }
        return null;
    }
	
	public Boolean isRepartEditing() {
		return getCtrl().getIsRepartEditing();
	}

	public WOActionResults onRepartEdit() {
		getCtrl().onRepartEdit();
		return doNothing();
	}

	public String btRepartModifierOnComplete() {
		return "function(){" + getTypeGroupeContainerId() + "Update();}";
	}

	public Boolean shouldShowTypesGroupes() {
		if (valueForBinding(BINDING_shouldShowTypesGroupes) != null) {
			return (Boolean) valueForBinding(BINDING_shouldShowTypesGroupes);
		}
		return DEFAULT_shouldShowTypesGroupes;
	}

	public Boolean shouldShowAliasGroupes() {
		return booleanValueForBinding(BINDING_shouldShowAliasGroupes,
										DEFAULT_shouldShowAliasGroupes);
	}

	public Boolean hasTypeGroupe() {
		return Boolean.valueOf(repartTypeGroupeDisplayGroup().displayedObjects().count() > 0);
	}

	public String secretariatBtType() {
		return CktlAjaxActionButton.TYPE_NEW;
	}

	public void resetEdc() {
		_editingContext = null;
	}
	
	public NSArray getGrpAccess() {
	    return EOStructureForGroupeSpec.GRP_ACCESS.allKeys();
	}
	
	public String labelForGrpAccess() {
	    return (String) EOStructureForGroupeSpec.GRP_ACCESS.objectForKey(currentGrpAcces);
	}
	
	public String getCurrentGrpAcces() {
        return currentGrpAcces;
    }
	
	public void setCurrentGrpAcces(String currentGrpAcces) {
        this.currentGrpAcces = currentGrpAcces;
    }
	
	public Boolean shouldShowTypeStructure() {
		return booleanValueForBinding(BINDING_shouldShowTypeStructure, DEFAULT_shouldShowTypeStructure);
	}

	public boolean isTypeStructureDisabled() {
		return !getAppUserForAnnuaire().hasDroitTous();
	}

	private EOTypeStructure unTypeStructure;

	/**
	 * @return the unTypeStructure
	 */
	public EOTypeStructure getUnTypeStructure() {
		return unTypeStructure;
	}

	/**
	 * @param unTypeStructure the unTypeStructure to set
	 */
	public void setUnTypeStructure(EOTypeStructure unTypeStructure) {
		this.unTypeStructure = unTypeStructure;
	}
	
	
	/**
	 * 
	 * @return le type de structure selectionne
	 */
	public EOTypeStructure getSelectedTypeStructure() {
		return groupe().toTypeStructure();
	}

	/**
	 * 
	 * @param selectedTypeStructure le type de structure a mettre a jour dans la structure
	 */
	public void setSelectedTypeStructure(EOTypeStructure selectedTypeStructure) {
	  EOTypeStructure localTypeStructure = selectedTypeStructure.localInstanceIn(groupe().editingContext());
		groupe().setToTypeStructureRelationship(localTypeStructure);
	}

	
	private NSArray<EOTypeStructure> typesStructureList;

	/**
	 * @return the typesStructureList
	 */
	public NSArray<EOTypeStructure> getTypesStructureList() {
		if (typesStructureList == null || typesStructureList.isEmpty()) {
			typesStructureList = (NSArray<EOTypeStructure>) EOTypeStructure.fetchAll(edc(), 
					ERXSortOrdering.sortOrderingWithKey(EOTypeStructure.L_TYPE_STRUCTURE_KEY, 
							ERXSortOrdering.CompareAscending).array());
		}
		// Dans le cas ou l'on change d'editingContext (lorsqu'on change de structures)
		if (typesStructureList != null && !typesStructureList.isEmpty()) {
			EOEditingContext _ancienEdc = ERXArrayUtilities.firstObject(typesStructureList).editingContext();
			if (!edc().equals(_ancienEdc)) {
				typesStructureList = ERXEOControlUtilities.localInstancesOfObjects(edc(), typesStructureList);
			}
		}
		
		
		return typesStructureList;
	}

	/**
	 * @param typesStructureList the typesStructureList to set
	 */
	public void setTypesStructureList(NSArray<EOTypeStructure> typesStructureList) {
		this.typesStructureList = typesStructureList;
	}
	
	/**
	 * Remonter du nested quand le type de structure est change
	 * @return
	 */
	public WOActionResults saveTypeStructure() {
	    try {
            edc().saveChanges();
        } catch (Exception e) {
            e.printStackTrace();
            setErreurSaisieMessage(e.getMessage());

        } finally {
            edc().revert();
            getCtrl().afterRepartEnregistrer();
        }
        return null;
    }
	
}
