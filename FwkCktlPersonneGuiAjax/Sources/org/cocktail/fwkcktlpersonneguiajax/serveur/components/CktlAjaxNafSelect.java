/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * Selection d'un code NAF à partir d'une liste affichée dans une fenetre modale.
 * 
 * @binding selection
 */
public class CktlAjaxNafSelect extends AComponent {

	private static final long serialVersionUID = 1L;
	private static final String NAF = ASelectComponent.CURRENT_OBJ_KEY;
	private static final String BINDING_SELECTION = "selection";
	private static final String LC_KEY = EONaf.C_NAF_KEY;
	private static final String LL_KEY = EONaf.LL_NAF_KEY;

	private NSArray<CktlAjaxTableViewColumn> colonnes;
	private String filtre;
	private String containerId;
	private String modalWindowId;

	public CktlAjaxNafSelect(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public WOActionResults valider() {
		CktlAjaxWindow.close(context(), getModalWindowId());
		return null;
	}

	public WOActionResults editer() {
		return null;
	}

	public EONaf getSelection() {
		return (EONaf) valueForBinding(BINDING_SELECTION);
	}

	public void setSelection(EONaf value) {
		setValueForBinding(value, BINDING_SELECTION);
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
			// Colonne Libelle
			CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
			col.setLibelle("Code");
			col.setOrderKeyPath(LC_KEY);
			String keyPath = ERXQ.keyPath(NAF, LC_KEY);
			CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);
			// Colonne Libelle long
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Libellé");
			col.setOrderKeyPath(LL_KEY);
			keyPath = ERXQ.keyPath(NAF, LL_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);
			colonnes = colTmp.immutableClone();
		}
		return colonnes;
	}

	public EOQualifier getQualifier() {
		return ERXQ.containsAny(
				new NSArray<String>(LC_KEY,
									LL_KEY),
				getFiltre());
	}

	@SuppressWarnings("unchecked")
	public NSArray<EONaf> getAllNafs() {
		return EONaf.getAllNaf(edc(), new NSTimestamp());
	}

	public String filtreButtonId() {
	    return "Bid_" +  getContainerId();
	}
	
	public String getContainerId() {
		if (containerId == null)
			containerId = ERXWOContext.safeIdentifierName(context(), true);
		return containerId;
	}

	public String getModalWindowId() {
		if (modalWindowId == null)
			modalWindowId = ERXWOContext.safeIdentifierName(context(), true);
		return modalWindowId;
	}

	public String getFiltre() {
		return filtre;
	}

	public void setFiltre(String filtre) {
		this.filtre = filtre;
	}

}