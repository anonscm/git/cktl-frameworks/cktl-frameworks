/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.CktlAjaxGroupeTreeViewCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXQ;

/**
 * Composant de sélection d'un groupe à l'aide d'un arbre et d'un filtre sur cet arbre.
 * 
 * @binding id l'id du composant (obligatoire)
 * @binding utilisateurPersId l'id de la personne effectuant l'exploration de groupes (obligatoire)
 * @binding editingContext l'editingContext dans lequel seront fetchés les groupes
 * @binding selection mis à jour avec le groupe sélectionné
 * @binding treeViewTitle
 * @binding treeLeafImageFramework
 * @binding treeLeafImage
 * @binding treeCollapsedImageFramework
 * @binding treeCollapsedImage
 * @binding treeExpandedImage
 * @binding treeExpandedImageFramework
 * @binding treeViewWidth
 * @binding treeViewHeight
 * @binding treeViewClassName
 * @binding treeViewAllExpanded facultatif : permet de déplier complètement l'arbre, par defaut à false
   @binding treeViewShowRoot facultatif : permet de montrer la racine ou non de l'arbre, par defaut à false
 * @binding treeQualifier Facultatif. Specifie le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être applicable aux
 *          objets EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur
 * @binding treeRootObject Facultatif. Specifie la racine de l'arbre.
 * @binding userFiltersDictionary Facultatif. Specifie un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent dans une liste
 *          deroulante, le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent être applicable aux
 *          objets EOStructure.
 * @binding textFieldSize Facultatif. Specifie la taille du champ (en colonnes)
 * @binding userFiltersFormID ID de l'objet FORM contenant le composant (necessaire si vous specifiez des filtres utilisateurs)
 * @binding filtreTexte boolean indiquant si un champ de filtre texte est affiché pour filtrer les nodes de l'arbre
 * @binding treeExclusions Facultatif. Permet de specifier un NSArray d'objets a exclure de l'affichage de l'arbre.
 * @binding displayDeleteButton Facultatif.Specifie si le bouton supprimer doit etre affiche. Par defaut a true.
 * @binding confirmBeforeSelection Facultatif. Specifie s'il faut demander une confirmation avent la selection.
 * @binding reset Facultatif. Spécifie si l'arbre doit être reseté
 * @binding cssSelectedGroupe Facultatif. Spécifie la classe css à appliquer sur le gorupe sélectionné
 * @binding expandToLevel Facultatif. Entier spécifiant jusqu'à quel niveau de l'arbre est déroulé initialement et après
 *          chaque reset.
 * @binding ctrl Controleur eventuel, qui doit heriter de CktlAjaxGroupeTreeViewCtrl
 * @binding allGroupsSelect Facultatif. Spécifie si tout l'arbre peut être sélectionnable sans regarder dans l'annuaire. Par défaut à false.
 */
public class CktlAjaxGroupeTreeView extends ATreeViewComponent {

	private static final long serialVersionUID = 6310181028611897457L;
	/**
	 * Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent dans une liste deroulante,
	 * le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent √™re applicable aux objets
	 * EOStrcuture.
	 */
	public static final String USER_FILTERS_DICTIONARY_BDG = "userFiltersDictionary";
	/** Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection. */
	public static final String CONFIRM_BEFORE_SELECTION_BDG = "confirmBeforeSelection";
	/** Facultatif. Binding sp√©cifiant l'action a exécuter après sélection d'un groupe */
	public static final String ON_SUCCESS_SELECT_BDG = "onSuccessSelect";
	/** Facultatif. Binding boolean spécifiant si l'arbre doit être reseté */
	public static final String RESET_BDG = "reset";
	/** Facultatif. Binding spécifiant la classe css à appliquer sur le gorupe sélectionné */
	public static final String CSS_SELECTED_GROUPE_BDG = "cssSelectedGroupe";
	/** Facultatif. Controleur a utiliser. */
	public static final String CTRL_BDG = "ctrl";
	/** Facultatif. */
	public static final String EXPAND_TO_LEVEL_BDG = "expandToLevel";
	/** Facultatif. */
	public static final String ALL_GROUPS_SELECT_BDG = "allGroupsSelect";

	private NSDictionary filtres;
	private String unFiltre, unFiltreSelectionne;
	private EOQualifier userFiltreQualifier;
	private CktlAjaxGroupeTreeViewCtrl ctrl;
	private EOStructure selection;
	private String filtreTexte;
	private boolean _firstRender = true;
	private int groupeIndex;
	
	public CktlAjaxGroupeTreeView(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (ctrl().getWocomponent() == null) {
			ctrl().setWocomponent(this);
		}
		if (hasBinding(RESET_BDG)) {
			if (Boolean.TRUE.equals(valueForBinding(RESET_BDG))) {
				resetTree();
				expandToLevel();
			}
			if (canSetValueForBinding(RESET_BDG)) {
				setValueForBinding(Boolean.FALSE, RESET_BDG);
			}
		}
		if (_firstRender) {
		    expandToLevel();
		}
		super.appendToResponse(response, context);
		_firstRender = false;
	}

	private void expandToLevel() {
	    if (hasBinding(EXPAND_TO_LEVEL_BDG)) {
            Integer level = (Integer) valueForBinding(EXPAND_TO_LEVEL_BDG);
            ctrl().getMyTreeModel().setRootTreeNode(ctrl().rootGroupe());
            ctrl().expandAllNodesToLevel(level);
        }
	}
	
	private void resetTree() {
		ctrl().setRootGroupe(null);
		ctrl().rootGroupe().setChildren(null);
		EOStructure groupe = selection();
		if (groupe != null) {
			ctrl().selectObjectInTree(ctrl().rootGroupe(), groupe);
		}
	}

	public CktlAjaxGroupeTreeViewCtrl ctrl() {
		if (ctrl == null) {
			if (hasBinding(CTRL_BDG)) {
				ctrl = (CktlAjaxGroupeTreeViewCtrl) valueForBinding(CTRL_BDG);
			}
			if (ctrl == null) {
				ctrl = new CktlAjaxGroupeTreeViewCtrl(this);
			}
		}
		return ctrl;
	}

	public WOActionResults filtrer() {
		String filtre = unFiltreSelectionne();
		if (filtre != null) {
			if (filtre.equalsIgnoreCase("Tous")) {
				setUserFiltreQualifier(null);
			}
			else {
				setUserFiltreQualifier((EOQualifier) filtres.objectForKey(filtre));
			}
//			ctrl().setRootGroupe(null);
		}
		else {
			setUserFiltreQualifier(null);
//			ctrl().setRootGroupe(null);
		}
		resetTree();
		return null;
	}

	public WOActionResults resetFilters() {
	    filtreTexte = null;
	    setUnFiltreSelectionne(null);
	    setUserFiltreQualifier(null);
	    resetTree();
        return null;
    }
	
	public WOActionResults selectionner() {
		ctrl().afficherGroupeSelectionne();
		return (WOActionResults) valueForBinding(ON_SUCCESS_SELECT_BDG);
	}

	public String cssClassForSelectedLink() {

		if (hasBinding(CSS_SELECTED_GROUPE_BDG) &&
				selection() != null && selection().equals(ctrl().unGroupe().object())) {
			return (String) valueForBinding(CSS_SELECTED_GROUPE_BDG);
		}
		else {
			return String.valueOf("");
		}
	}
	
	public NSDictionary filtres() {
		if (filtres == null) {
			filtres = (NSDictionary) valueForBinding(USER_FILTERS_DICTIONARY_BDG);
		}
		return filtres;
	}

	public boolean isFiltrageAvailable() {
		boolean isFiltrageAvailable = hasBinding(USER_FILTERS_DICTIONARY_BDG);
		if (isFiltrageAvailable) {
			NSDictionary dicoFiltres = filtres();
			isFiltrageAvailable = dicoFiltres.count() > 0 ? true : false;
		}
		return isFiltrageAvailable;
	}

	public void setUserFiltreQualifier(EOQualifier userFiltreQualifier) {
		this.userFiltreQualifier = userFiltreQualifier;
	}

	public EOQualifier getUserFiltreQualifier() {
		return userFiltreQualifier;
	}

	private EOQualifier getTexteQualifier() {
		if (filtreTexte != null)
			return ERXQ.contains(EOStructure.STR_AFFICHAGE_KEY, filtreTexte);
		else
			return null;
	}

	public EOQualifier getQualifier() {
		EOQualifier qual = getTreeQualifier();
		if (qual != null) {
			if (getUserFiltreQualifier() != null) {
				qual = new EOAndQualifier(new NSArray(new Object[] {
						qual, getUserFiltreQualifier()
				}));
			}
		}
		else {
			qual = getUserFiltreQualifier();
		}
		EOQualifier qualTexte = getTexteQualifier();
		if (qualTexte != null) {
			qual = ERXQ.and(qual, qualTexte);
		}
		return qual;
	}

	public NSArray getTreeExclusions() {
		return (NSArray) valueForBinding(TREE_EXCLUSIONS_BDG);
	}

	public EOStructure treeRootObject() {
		return (EOStructure) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

//	public Boolean isGroupeSelectionnable() {
//		return (ctrl.unGroupe().object() instanceof EOStructure &&
//				getAppUserForAnnuaire().hasDroitGererGroupe((EOStructure) ctrl.unGroupe().object()) && 
//				!EOStructureForGroupeSpec.isGroupeGereEnAuto((EOStructure) ctrl.unGroupe().object()));
//	}
	public Boolean isGroupeSelectionnable() {
		if (hasBinding(ALL_GROUPS_SELECT_BDG)) { 
				if (((Boolean) valueForBinding(ALL_GROUPS_SELECT_BDG)).booleanValue()) {
					return true;
				}			
		}
		if (getAppUserForAnnuaire().hasDroitRhTrombi()) {
			return true;
		}
		return (ctrl.unGroupe().object() instanceof EOStructure &&
				//getAppUserForAnnuaire().hasDroitGererGroupe((EOStructure) ctrl.unGroupe().object()));
				getAppUserForAnnuaire().hasDroitVisualiserGroupe((EOStructure) ctrl.unGroupe().object()));
	}

	public String selectionOnClickBefore() {
		if (hasBinding(CONFIRM_BEFORE_SELECTION_BDG)) {
			if (((Boolean) valueForBinding(CONFIRM_BEFORE_SELECTION_BDG)).booleanValue()) {
				return ("confirm('Voulez-vous selectionner le groupe " + jsEncode(ctrl.unGroupeLibelle()) + " ?')");
			}
		}
		return null;
	}

	public String filtrePopUpID() {
		return getComponentId() + "_filtrePopUp";
	}

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}
	
	public String groupeLinkId() {
		EOStructure groupe = (EOStructure) ctrl.unGroupe().object();
		return getComponentId() + "_groupeLink_" + groupe.cStructure();
	}

	/**
	 * @return the unFiltreSelectionne
	 */
	public String unFiltreSelectionne() {
		return unFiltreSelectionne;
	}

	/**
	 * @param unFiltreSelectionne the unFiltreSelectionne to set
	 */
	public void setUnFiltreSelectionne(String unFiltreSelectionne) {
		this.unFiltreSelectionne = unFiltreSelectionne;
	}

	/**
	 * @return the unFiltre
	 */
	public String unFiltre() {
		return unFiltre;
	}

	/**
	 * @param unFiltre the unFiltre to set
	 */
	public void setUnFiltre(String unFiltre) {
		this.unFiltre = unFiltre;
	}

	/**
	 * @param ctrl the ctrl to set
	 */
	public void setCtrl(CktlAjaxGroupeTreeViewCtrl ctrl) {
		this.ctrl = ctrl;
	}

	public EOStructure selection() {
		if (selection != valueForBinding(SELECTION_BDG))
			selection = (EOStructure) valueForBinding(SELECTION_BDG);
		return selection;
	}

	/**
	 * @param selection
	 */
	public void setSelection(EOStructure selection) {
		this.selection = selection;
		setValueForBinding(selection, CktlAjaxGroupeTreeView.SELECTION_BDG);
	}

	public String getFiltreTexte() {
		return filtreTexte;
	}

	public void setFiltreTexte(String filtreTexte) {
		this.filtreTexte = filtreTexte;
	}

	/**
	 * @return the groupeIndex
	 */
	public int getGroupeIndex() {
		return groupeIndex;
	}

	/**
	 * @param groupeIndex the groupeIndex to set
	 */
	public void setGroupeIndex(int groupeIndex) {
		this.groupeIndex = groupeIndex;
	}
	
	
	/**
	 * On met la sélection en gras et on enlève le bold sur toutes les autres lignes
	 * @return onCompleteGroupeSelection
	 */
	public String onCompleteGroupeSelection(){
		String cssSelectedClass = (String) valueForBinding(CSS_SELECTED_GROUPE_BDG);;
		String onCompleteGroupeSelection = "function(){";
		onCompleteGroupeSelection += "var selections = $$('a[id^=" + getComponentId() + "_groupeLink_]'); selections.each(function(e){e.removeClassName('" + cssSelectedClass + "');});";
		onCompleteGroupeSelection += "$('" + groupeLinkId() + "').addClassName('" + cssSelectedClass + "');";
		onCompleteGroupeSelection += "}";
		return onCompleteGroupeSelection;
	}
	

}