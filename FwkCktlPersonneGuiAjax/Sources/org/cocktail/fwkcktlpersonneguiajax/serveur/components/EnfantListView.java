package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.util.Iterator;

import org.cocktail.fwkcktlpersonne.common.metier.EOEnfant;
import org.cocktail.fwkcktlpersonne.common.metier.EOLienFiliationEnfant;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartEnfant;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Permet l'affichage d'un tableau listant des enfants à partir d'une liste des EORepartEnfant fournie par le binding repartEnfants 
 * 
 * @binding repartEnfants Variable listant tous les EORepartEnfant à afficher. Type: NSArray<EORepartEnfant>
 * @author Frédéric Favreau
 */
public class EnfantListView extends AComponent {
	public static final String BINDING_repartEnfants = "repartEnfants";
	private EORepartEnfant repartEnfant;
	
	private boolean _rowFlip; // used to alternate colors

	private static final String CODE_ENFANT_LEGITIME = "LF10";
	private static final String CODE_ENFANT_NATUREL = "LF30";

	public EnfantListView(WOContext context) {
        super(context);
    }

	/**
	 * 
	 * @return la liste des repartEnfants de l'individu
	 */
	public NSArray<EORepartEnfant> lstRepartEnfants() {
		return (NSArray<EORepartEnfant>) valueForBinding(BINDING_repartEnfants);
	}

	/**
	 * @return the repartEnfant
	 */
	public EORepartEnfant repartEnfant() {
		return repartEnfant;
	}

	/**
	 * @param repartEnfant the repartEnfant to set
	 */
	public void setRepartEnfant(EORepartEnfant repartEnfant) {
		this.repartEnfant = repartEnfant;
	}

	public String getTimestampFormatter() {
		return DateCtrl.DEFAULT_FORMAT;
	}
	
	/**
	 * @return Vrai si l'individu possede au moins un enfant qui n'est ni légitime ni naturel. Faux sinon.
	 */
	public boolean possedeEnfantAvecDateArrivee() {
		Iterator<EORepartEnfant> iterator=lstRepartEnfants().iterator();
		while (iterator.hasNext()) {
			EORepartEnfant repartEnfant=iterator.next();
			EOLienFiliationEnfant lienFiliation=repartEnfant.toLienFiliationEnfant();
			if (lienFiliation!=null)
			{
				if (!lienFiliation.lfenCode().equals(CODE_ENFANT_LEGITIME) && !lienFiliation.lfenCode().equals(CODE_ENFANT_NATUREL)) {
					return true;
				}
			}
		}
		return false;	
	}
	
	/**
	 * @return Vrai si l'individu possede au moins un enfant decedé. Faux sinon.
	 */
	public boolean possedeEnfantDecede() {
		Iterator<EORepartEnfant> iterator=lstRepartEnfants().iterator();
		while (iterator.hasNext()) {
			EORepartEnfant repartEnfant=iterator.next();
			EOEnfant enfant=repartEnfant.toEnfant();
			if (enfant!=null)
			{
				if (enfant.dDeces()!=null)
					return true;
			}
		}
		return false;	
	}
	
	/**
	 * 
	 * @return la couleur de la ligne en fonction de la parité
	 */
	public String backgroundColorForRow() {
		_rowFlip = !_rowFlip;
		if (_rowFlip) {
			return "pair";
		} else {
			return "impair";
		}
	}

}