/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOActivites;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;
import org.cocktail.fwkcktlpersonne.common.metier.EORome;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecteurActivite;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * Affiche et permet de modifier les informations administrative d'une personne (physique ou morale). (forme juridique, SIRET, n° INSEE, etc.)
 * 
 * @binding personne
 * @binding allowEditCivilite
 * @binding showNoInsee
 * @binding allowEditNoInsee Si true, l'utilisateur peut modifier le n° INSEE. La règle est : si individu est personnel ou etudiant, ne pas autoriser
 *          la modif sauf dans les applis chargées de gérer les personnels ou etudiants.
 * @binding showEtatCivil
 * @binding allowEditEtatCivil Si true, l'utilisateur peut modifier la date et le lieu de naissance. : si individu est personnel ou etudiant, ne pas
 *          autoriser la modif sauf dans les applis chargées de gérer les personnels ou etudiants.
 * @binding utilisateurPersId
 * @binding editingContext
 * @binding isEditing Obligatoire, indique au composant si on est en mode modification ou pas.
 * @binding isReadOnly obligatoire, indique au composant si on est en mode lecture seule ou pas.
 * @author rprin
 * @see IndividuEtatCivilUI
 */
public class PersonneAdminUI extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_personne = "personne";
	public static final String BINDING_allowEditCivilite = "allowEditCivilite";
	public static final Boolean DEFAULT_allowEditCivilite = Boolean.FALSE;
	public static final String BINDING_showNoInsee = "showNoInsee";
	public static final Boolean DEFAULT_showNoInsee = Boolean.FALSE;
	public static final String BINDING_allowEditNoInsee = "allowEditNoInsee";
	public static final Boolean DEFAULT_allowEditNoInsee = Boolean.FALSE;
	public static final String BINDING_allowEditEtatCivil = "allowEditEtatCivil";
	public static final Boolean DEFAULT_allowEditEtatCivil = Boolean.FALSE;
	public static final String BINDING_showEtatCivil = "showEtatCivil";
	
	public static final String BINDING_STRUCTURE_APPARTENANCE = "structureAppartenance";

	public NSArray lesCodesNafFiltres = null;
	public EONaf unNaf;
	public String filtNaf, lastFiltNaf;
	private EONaf codeNafSelectionne;
	//private EONaf selectedNaf;
	private IPersonne _personne;
	private NSArray allNafs;

	public NSArray lesCodesFjFiltres = null;
	public String filtFj, lastFiltFj;
	public EOFormesJuridiquesDetails unFj;
	// public EOFormesJuridiquesDetails selectedFj;
	private NSArray allFjs;

	public NSArray lesCodesActivitesFiltres = null;
	public String filtActivites, lastFiltActivites;
	public EOActivites unActivites;
	private NSArray allActivites;

	private NSArray listCivilites;
	public EOCivilite unCivilite;
	
	private EORome selectedRome;
	private EORome unRome;
	
	private NSArray<EOSecteurActivite> listeSecteurActivite = null;
	private EOSecteurActivite currentSecteurActivite;
	private EOSecteurActivite selectedSecteurActivite = null;
	
	public PersonneAdminUI(WOContext context) {
		super(context);
	}

	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		//initDonnees(pers);
		if (pers == null) {
			_personne = null;
			filtNaf = null;
		} else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			_personne = pers;
			filtNaf = null;
			filtFj = null;
			filtActivites = null;
			if (_personne instanceof EOStructure) {
				if (((EOStructure) _personne).toNaf() != null) {
					filtNaf = ((EOStructure) _personne).toNaf().cNafEtLib();
				}
				if (((EOStructure) _personne).toFormesJuridiquesDetails() != null) {
					filtFj = ((EOStructure) _personne).toFormesJuridiquesDetails().llAffichage();
				}
				if (((EOStructure) _personne).strActivite() != null) {
					filtActivites = ((EOStructure) _personne).strActivite();
				}
			}
		}
		return _personne;
	}

	private void initDonnees(IPersonne _personne2) {
		//		filtNaf = null;
		//		if (_personne2 instanceof EOStructure) {
		//			if (((EOStructure) _personne2).toNaf() != null) {
		//				filtNaf = ((EOStructure) _personne2).toNaf().cNafEtLib();
		//			}
		//
		//		}
	}

	public EOStructure getStructure() {
		if (isStructure().booleanValue()) {
			return (EOStructure) getPersonne();
		}
		return null;
	}

	public EOIndividu getIndividu() {
		if (!isStructure().booleanValue()) {
			return (EOIndividu) getPersonne();
		}
		return null;
	}
	
	public EOStructure getStructureAppartenance() {
		if (hasBinding(BINDING_STRUCTURE_APPARTENANCE)) {
			return (EOStructure) valueForBinding(BINDING_STRUCTURE_APPARTENANCE);
		}
		return null;
	}
	

	public Boolean isStructure() {
		return Boolean.valueOf(getPersonne() instanceof EOStructure);
	}
	
	
	public EORome getSelectedRome() {
		return (getStructure() == null ? null : getStructure().toRome());
	}

	public void setSelectedRome(EORome selectedRome) {
		this.selectedRome = selectedRome;
		EORome rome = selectedRome;
		if (rome != null && !selectedRome.editingContext().equals(getStructure().editingContext())) {
			rome = selectedRome.localInstanceIn(getStructure().editingContext());
		}
		getStructure().setToRomeRelationship(rome);
	}

	public String getIndQualite() {
		return getIndividu().indQualite();
	}

	public void setIndQualite(String indQualite) {
		if (checkIndQualite(indQualite)) {
			getIndividu().setIndQualite(indQualite);
		}
	}

	public String getIndActivite() {
		return getIndividu().indActivite();
	}

	public void setIndActivite(String indActivite) {
		if (checkIndActivite(indActivite)) {
			getIndividu().setIndActivite(indActivite);
		}
	}

	
	
	
	public String getFiltNaf() {
		return filtNaf;
	}

	public void setFiltNaf(String filtNaf) {
		//		if (filtNaf == null) {
		//			setSelectedNaf(null);
		//		}
		//		else {
		this.filtNaf = filtNaf;
		//		}
	}

	public String getLlNaf() {
		return (getSelectedNaf() != null ? getSelectedNaf().llNaf() : "");
	}

	public NSArray getFilteredNaf() {
		if (filtNaf != null && filtNaf.length() > 0) {
			filtNaf = filtNaf.trim();
			filtNaf = MyStringCtrl.chaineSansAccents(filtNaf, "?");
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EONaf.C_NAF_KEY + " caseInsensitiveLike '" + filtNaf + "*' or " + EONaf.LL_NAF_KEY + " caseInsensitiveLike '*" + filtNaf + "*' ", null);
			if (lesCodesNafFiltres != null && lastFiltNaf != null && filtNaf.startsWith(lastFiltNaf)) {
				lesCodesNafFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesNafFiltres, qual);
				lastFiltNaf = filtNaf;
			} else {
				lesCodesNafFiltres = allNafs();
				lesCodesNafFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesNafFiltres, qual);
				lastFiltNaf = new String(filtNaf);
			}
			if (lesCodesNafFiltres != null && lesCodesNafFiltres.count() == 0) {
				if (filtNaf.length() > 6) {
					EOQualifier qual1 = new EOKeyValueQualifier(EONaf.C_NAF_KEY, EOKeyValueQualifier.QualifierOperatorEqual, filtNaf.substring(0, 5));
					lesCodesNafFiltres = allNafs();
					lesCodesNafFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesNafFiltres, qual1);
				}
			}
		}
		return lesCodesNafFiltres;
	}

	public NSArray<EONaf> allNafs() {
		if (allNafs == null) {
			allNafs = EONaf.fetchAll(edc(), new NSArray(EONaf.SORT_C_NAF));
		}	
		return allNafs;
	}


	public void setSelectedNaf(EONaf selectedNaf) {
		//this.selectedNaf = selectedNaf;
		// setFiltNaf(selectedNaf.cNafEtLib());
		if (selectedNaf == null) {
			lastFiltNaf = null;
			lesCodesNafFiltres = null;
		} else {
			setFiltNaf(selectedNaf.cNafEtLib());
			getStructure().setToNafRelationship(selectedNaf.localInstanceIn(edc()));
		}
	}

	public EONaf getSelectedNaf() {
		return (getStructure().toNaf() != null ? getStructure().toNaf().localInstanceIn(edc()) : null);
	}

	public String getCodeNafFieldId() {
		return getComponentId() + "_FCodeNaf";
	}

	public String getNafFieldId() {
		return getComponentId() + "_FNaf";
	}
	
	/**
	 * Secteur d'activité
	 */
	public String getListeSecteurActiviteId() {
		return getComponentId() + "_listeSecteurActiviteId";
	}

	public String getInseeProvisoireId() {
		return getComponentId() + "_inseeProv";
	}
	
	public String romeSelectID() {
		return getComponentId() + "_romeSelect";
	}

	//
	//	public String indNoInsee() {
	//		if (getIndividu() != null) {
	//			if (isNoInseeProvisoire().booleanValue()) {
	//				return getIndividu().indNoInseeProv();
	//			}
	//			else {
	//				return getIndividu().indNoInsee();
	//			}
	//		}
	//		return null;
	//	}

	//
	//	public void setIndNoInsee(String s) {
	//		if (getIndividu() != null) {
	//			if (isNoInseeProvisoire().booleanValue()) {
	//				getIndividu().setIndNoInseeProv(s);
	//			}
	//			else {
	//				getIndividu().setIndNoInsee(s);
	//			}
	//		}
	//	}
	//
	//	public Integer indCleInsee() {
	//		if (getIndividu() != null) {
	//			if (isNoInseeProvisoire().booleanValue()) {
	//				return getIndividu().indCleInseeProv();
	//			}
	//			else {
	//				return getIndividu().indCleInsee();
	//			}
	//		}
	//		return null;
	//	}

	//
	//	public void setIndCleInsee(Integer s) {
	//		if (getIndividu() != null) {
	//			if (isNoInseeProvisoire().booleanValue()) {
	//				getIndividu().setIndCleInseeProv(s);
	//			}
	//			else {
	//				getIndividu().setIndCleInsee(s);
	//			}
	//		}
	//	}

	public Boolean isNoInseeProvisoire() {
		if (getIndividu() != null) {
			return Boolean.valueOf(!EOIndividu.PRISE_CPT_INSEE_R.equals(getIndividu().priseCptInsee()));
		}
		return Boolean.TRUE;
	}

	public void setIsNoInseeProvisoire(Boolean val) {
		if (getIndividu() != null) {
			getIndividu().setPriseCptInsee((Boolean.FALSE.equals(val) ? EOIndividu.PRISE_CPT_INSEE_R : EOIndividu.PRISE_CPT_INSEE_P));
		}
		return;
	}

	public Boolean isSiret() {
		return Boolean.valueOf(getStructure() != null && (getStructure().siret() != null || getStructure().siren() != null));
	}

	public Boolean isFj() {
		return Boolean.valueOf(getStructure() != null && (getStructure().toFormesJuridiquesDetails() != null));
	}

	public Boolean isActivite() {
		return Boolean.valueOf(getStructure() != null && (!MyStringCtrl.isEmpty(getStructure().strActivite())));
	}

	public String siret() {
		if (getStructure().siret() == null) {
			return getStructure().siren();
		}
		return getStructure().siret();
	}

	public void setSiret(String s) {
		getStructure().setSiret(s);
	}

	public String siretForAffichage() {
		String s = siret();
		if (s != null) {
			if (s.length() > 9) {
				s = s.substring(0, 9) + " " + s.substring(9);
			} else {
				s = s + "  ?????";
			}
		}
		return s;
	}

	public String fjId() {
		return getComponentId() + "_fj";
	}

	public String siretId() {
		return getComponentId() + "_siret";
	}

	public String activitesId() {
		return getComponentId() + "_activites";
	}

	public NSArray getLesFjs() {
		if (filtFj != null && filtFj.length() > 0) {
			filtFj = filtFj.trim();
			filtFj = MyStringCtrl.chaineSansAccents(filtFj, "?");
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOFormesJuridiquesDetails.C_FJD_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtFj + "*"));
			quals.addObject(new EOKeyValueQualifier(EOFormesJuridiquesDetails.LL_FJD_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtFj + "*"));
			EOOrQualifier qual = new EOOrQualifier(quals);
			if (lesCodesFjFiltres != null && lastFiltFj != null && filtFj.startsWith(lastFiltFj)) {
				lesCodesFjFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesFjFiltres, qual);
				lastFiltFj = filtFj;
			} else {
				lesCodesFjFiltres = allFjs();
				lesCodesFjFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesFjFiltres, qual);
				lastFiltFj = new String(filtFj);
			}
			if (lesCodesFjFiltres != null && lesCodesFjFiltres.count() == 0) {
				if (filtFj.length() > 5) {
					EOQualifier qual1 = new EOKeyValueQualifier(EOFormesJuridiquesDetails.C_FJD_KEY, EOKeyValueQualifier.QualifierOperatorEqual, filtFj.substring(0, 4));
					lesCodesFjFiltres = allFjs();
					lesCodesFjFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesFjFiltres, qual1);
				}
			}
		}
		return lesCodesFjFiltres;
	}

	public NSArray getLesFjs1() {
		return getLesFjs(filtFj);
	}

	public String getFiltFj() {
		//		if (filtFj == null) {
		//			if (getStructure().toFormesJuridiquesDetails() != null) {
		//				return getStructure().toFormesJuridiquesDetails().llFjd();
		//			}
		//		}
		return filtFj;
	}

	public void setFiltFj(String filt) {
		this.filtFj = filt;
	}

	public EOFormesJuridiquesDetails getSelectedFj() {
		return (getStructure().toFormesJuridiquesDetails() != null ? getStructure().toFormesJuridiquesDetails().localInstanceIn(edc()) : null);
	}

	public void setSelectedFj(EOFormesJuridiquesDetails selectedFj) {
		// this.selectedFj = selectedFj;
		if (selectedFj == null) {
			lastFiltFj = null;
			lesCodesFjFiltres = null;
		} else {
			setFiltFj(selectedFj.llAffichage());
			getStructure().setToFormesJuridiquesDetailsRelationship(selectedFj.localInstanceIn(edc()));
		}
	}

	private NSArray getLesFjs(String filt) {
		//		if (filt==null) {
		//			return NSArray.EmptyArray;
		//		}
		//		filt = filt.trim();
		//		if (filt.length()==0) {
		//			return NSArray.EmptyArray;
		//		}

		NSMutableArray quals = new NSMutableArray();
		if (!MyStringCtrl.isEmpty(filt)) {
			filt = MyStringCtrl.chaineSansAccents(filt, "?");
			quals.addObject(new EOKeyValueQualifier(EOFormesJuridiquesDetails.LC_FJD_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filt + "*"));
			quals.addObject(new EOKeyValueQualifier(EOFormesJuridiquesDetails.LL_FJD_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filt + "*"));
			return EOQualifier.filteredArrayWithQualifier(allFjs(), new EOOrQualifier(quals));
		}
		return allFjs();

	}

	public NSArray allFjs() {
		if (allFjs == null) {
			allFjs = EOFormesJuridiquesDetails.fetchAll(edc(), new NSArray(new Object[] {
					EOFormesJuridiquesDetails.SORT_C_FJD
			}));
		}
		return allFjs;
	}

	public NSArray allActivites() {
		if (allActivites == null) {
			allActivites = EOActivites.fetchAll(edc(), new NSArray(new Object[] {
					EOActivites.SORT_L_ACTIVITE
			}));
		}
		return allActivites;
	}

	public NSArray getLesActivites1() {
		if (allActivites == null) {
			allActivites = (NSArray) EOActivites.fetchAll(edc(), new NSArray(new Object[] {
					EOActivites.SORT_L_ACTIVITE
			})).valueForKeyPath(EOActivites.L_ACTIVITE_KEY);
		}
		return allActivites;
	}

	public NSArray getLesActivites() {
		if (filtActivites != null && filtActivites.length() > 0) {
			filtActivites = filtActivites.trim();
			filtActivites = MyStringCtrl.chaineSansAccents(filtActivites, "?");
			EOKeyValueQualifier qual = new EOKeyValueQualifier(EOActivites.L_ACTIVITE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtActivites + "*");
			if (lesCodesActivitesFiltres != null && lastFiltActivites != null && filtActivites.startsWith(lastFiltActivites)) {
				lesCodesActivitesFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesActivitesFiltres, qual);
				lastFiltActivites = filtActivites;
			} else {
				lesCodesActivitesFiltres = allActivites();
				lesCodesActivitesFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesActivitesFiltres, qual);
				lastFiltActivites = new String(filtActivites);
			}
			if (lesCodesActivitesFiltres != null && lesCodesActivitesFiltres.count() == 0) {
				if (filtActivites.length() > 5) {
					EOQualifier qual1 = new EOKeyValueQualifier(EOActivites.L_ACTIVITE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, filtActivites.substring(0, 4));
					lesCodesActivitesFiltres = allActivites();
					lesCodesActivitesFiltres = EOQualifier.filteredArrayWithQualifier(lesCodesActivitesFiltres, qual1);
				}
			}
		}
		return lesCodesActivitesFiltres;
	}

	public String getFiltActivites() {
		//		if (filtFj == null) {
		//			if (getStructure().toFormesJuridiquesDetails() != null) {
		//				return getStructure().toFormesJuridiquesDetails().llFjd();
		//			}
		//		}
		return filtActivites;
	}

	public void setFiltActivites(String filt) {
		this.filtActivites = filt;
	}

	public EOActivites getSelectedActivite() {
		EOActivites selectedActivite = null;
		if (getStructure().strActivite() != null) {
			NSArray activites = allActivites();
			EOKeyValueQualifier qual = new EOKeyValueQualifier(EOActivites.L_ACTIVITE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, getStructure().strActivite());
			selectedActivite = (EOActivites) EOQualifier.filteredArrayWithQualifier(activites, qual).lastObject();
		}
		return selectedActivite;
	}

	public void setSelectedActivite(EOActivites selectedActivite) {
		getStructure().setStrActivite(selectedActivite.lActivite());
	}

	public NSArray getListCivilites() {
		if (listCivilites == null) {
			listCivilites = EOCivilite.fetchAll(edc());
		}
		return listCivilites;
	}
	
	/**
	 * @return NSArray<EOSecteurActivite> : la liste des secteurs d'activité
	 */
	@SuppressWarnings("unchecked")
	public NSArray<EOSecteurActivite> getListeSecteurActivite() {
		if (listeSecteurActivite == null) {
			listeSecteurActivite = new NSMutableArray<EOSecteurActivite>();
			listeSecteurActivite = ((NSArray<EOSecteurActivite>) EOSecteurActivite.getFilteredSecteursActivite(getStructure().editingContext(), null));
		}

		return listeSecteurActivite;
	}
	
	

	public EOSecteurActivite getCurrentSecteurActivite() {
		return currentSecteurActivite;
	}

	public void setCurrentSecteurActivite(EOSecteurActivite currentSecteurActivite) {
		this.currentSecteurActivite = currentSecteurActivite;
	}

	public EOSecteurActivite getSelectedSecteurActivite() {
		if (selectedSecteurActivite == null && getStructure().toSecteurActivite() != null ) {
			selectedSecteurActivite = getStructure().toSecteurActivite();
		}
		return selectedSecteurActivite;
	}

	public void setSelectedSecteurActivite(EOSecteurActivite selectedSecteurActivite) {
		this.selectedSecteurActivite = selectedSecteurActivite;
		
		EOSecteurActivite secteurActivite = selectedSecteurActivite;
		if (secteurActivite != null && !selectedSecteurActivite.editingContext().equals(getStructure().editingContext())) {
			secteurActivite = selectedSecteurActivite.localInstanceIn(getStructure().editingContext());
		}
		getStructure().setToSecteurActiviteRelationship(secteurActivite);
	}

	public Boolean allowEditCivilite() {
		if (hasBinding(BINDING_allowEditCivilite)) {
			return (Boolean) valueForBinding(BINDING_allowEditCivilite);
		}
		return DEFAULT_allowEditCivilite;
	}

	public Boolean allowEditNoInsee() {
		if (hasBinding(BINDING_allowEditNoInsee)) {
			return (Boolean) valueForBinding(BINDING_allowEditNoInsee);
		}
		return DEFAULT_allowEditNoInsee;
	}

	public Boolean allowEditEtatCivil() {
		if (hasBinding(BINDING_allowEditEtatCivil)) {
			return (Boolean) valueForBinding(BINDING_allowEditEtatCivil);
		}
		return DEFAULT_allowEditEtatCivil;
	}

	public Boolean noInseeDisabled() {
		return Boolean.valueOf(notAllowEditNoInsee().booleanValue() || isNoInseeProvisoire().booleanValue());
	}

	public Boolean noInseeProvDisabled() {
		return Boolean.valueOf(notAllowEditNoInsee().booleanValue() || !isNoInseeProvisoire().booleanValue());
	}

	public Boolean notAllowEditNoInsee() {
		return !allowEditNoInsee().booleanValue();
	}

	public boolean notAllowEditEtatCivil() {
		return !allowEditEtatCivil().booleanValue();
	}

	public Boolean showNoInsee() {
		if (hasBinding(BINDING_showNoInsee)) {
			return (Boolean) valueForBinding(BINDING_showNoInsee);
		}
		return DEFAULT_showNoInsee;
	}

	public boolean showEtatCivil() {
		return hasBinding(BINDING_showEtatCivil)
				&& booleanValueForBinding(BINDING_showEtatCivil, Boolean.FALSE);
	}
	
	
	public Boolean hasDroitModifFonctionEtStatut() {
		// Cas où le paramètre d'AGrhum pour l'accès au statut et la fonction est utilisé
		if (isAgrhumParamActive()) {
			return true;
		}
		// Cas où le paramètre d'AGrhum pour l'accès au statut et la fonction n'est pas utilisé
		// Définition de certains droits
		if (getStructureAppartenance() != null) {
			return getAppUserForAnnuaire().hasDroitGrhumCreateur() || getAppUserForAnnuaire().hasDroitTous() || getAppUserForAnnuaire().hasDroitGererGroupe(getStructureAppartenance());
		} else {
			return getAppUserForAnnuaire().hasDroitGrhumCreateur() || getAppUserForAnnuaire().hasDroitTous();
		}
		
	}
	
	protected boolean isAgrhumParamActive() {
		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PARAM_GRHUM_SATUT_FONCTION)) {
			return true;
		}
		return false;
	}

	/**
	 * la qualité renseignée en base correspond maintenant à la fonction en affichage
	 * @return true si l'individu a une qualité de renseigner
	 */
	public Boolean hasQualite() {
		return Boolean.valueOf(getIndividu() != null && !MyStringCtrl.isEmpty(getIndividu().indQualite()));
	}

	/**
	 * l'activité renseignée en base correspond maintenant au statut de l'individu en affichage
	 * @return true si l'individu a une activité
	 */
	public Boolean hasActivite() {
		return Boolean.valueOf(getIndividu() != null && !MyStringCtrl.isEmpty(getIndividu().indActivite()));
	}
	
	/**
	 * Contrôle de la désactivation de l'accès à la fonction (ex-qualité)
	 * @return true si la fonction est est géré par Scolarix pour les étudiants
	 */
	public boolean isFonctionDisabled() {
		// Modification faite pour que la fonction qui est renseignée par Scolarix pour les étudiants ne soient pas accessible, si on laisse la priorité à la Scolarité
		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PARAM_GRHUM_PEDA)
				&& ((EOIndividu) getPersonne()).isEtudiant()) {
			return true;
		}
//		if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_GRHUM_PEDA))
//				&& ((EOIndividu) getPersonne()).isEtudiant()) {
//			return true;
//		}
//		if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_SCOLARIX_INSTALLED))
//				&& ((EOIndividu) getPersonne()).isEtudiant()) {
//			return true;
//		}
		return false;
	}
	
	/**
	 * Contrôle de la désactivation de l'accès au statut (ex-fonction) des personnels
	 * @return true si le statut des personnels est géré par Mangue
	 */
	public boolean isStatutDisabled() {
		// Modification faite pour que le statut qui est renseignée par Mangue pour les personnels ne soient pas accessible, si on laisse la priorité à l'appication Mangue
		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PARAM_GRHUM_RH)
				&& ((EOIndividu) getPersonne()).isPersonnel()) {
			return true;
		}
//		if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_GRHUM_RH))
//				&& ((EOIndividu) getPersonne()).isPersonnel()) {
//			return true;
//		}
//		if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_MANGUE_INSTALLED))
//				&& ((EOIndividu) getPersonne()).isPersonnel()) {
//			return true;
//		}
		return false;
	}

	public EORome getUnRome() {
        return unRome;
    }
	
	public EOQualifier getRomeQualifier() {
		return EORome.QUAL_ROME_VALIDE;
	}

	/**
	 * Méthodes pour les radio boutons de statut
	 */

	/**
	 * @return ID du composant des radios boutons du statut de la structure
	 */
	public String getStatutStructure() {
		return getComponentId() + "_StatutStructure";
	}

	/**
	 * getter sur les infos de la structure
	 * @return toute la description de ce que fait la structure
	 */
	public String getInfoStatut() {
		return getStructure().strStatut();
	}

	/**
	 * setter sur les infos de la structure
	 * @param infoStatut pour la description de ce que fait
	 * ou à quoi sert cette structure
	 */
	public void setInfoStatut(String infoStatut) {
		getStructure().setStrStatut(infoStatut);
	}
	
	/**
	 * Méthodes pour les affichages en lecture du statut et du code ROME
	 */
	
	/**
	 * @return true si le Statut de la structure est renseigné
	 */
	public boolean isStatutNotNull() {
		if (!MyStringCtrl.isEmpty(getStructure().strStatut())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return true si un Secteur d'Activite est associé à la structure
	 */
	public boolean isSecteurActiviteNotNull() {
		if (getStructure() != null && getStructure().sactId() != null && !MyStringCtrl.isEmpty(getStructure().toSecteurActivite().sactLibelle())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return true si un code ROME est associé à la structure
	 */
	public boolean isCodeRomeNotNull() {
		if (getStructure() != null && getStructure().romId() != null && !MyStringCtrl.isEmpty(getStructure().romId().toString())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return la valeur du statut de la structure
	 */
	public String getStatut() {
		String statut = new String();
		if (MyStringCtrl.isEmpty(getStructure().strStatut())) {
			statut = "---";
		} else if (getStructure().strStatut().equals("PU")) {
			statut =  "Public";
		} else if  (getStructure().strStatut().equals("PR")) {
			statut = "Privé";
		} else if (getStructure().strStatut().equals("AU")) {
			statut = "Autre";
		}
		return statut;
	}
	
	/**
	 * getter du libellé Secteur d'Activite
	 * @return le libellé associé au Secteur d'Activite
	 */
	public String getLibelleSecteurActivite() {
		String libelleSecteurActivite = new String();
		
		if (getStructure().toSecteurActivite() == null || MyStringCtrl.isEmpty(getStructure().toSecteurActivite().sactLibelle())) {
			libelleSecteurActivite = "---";
		} else {
			libelleSecteurActivite = getStructure().toSecteurActivite().sactLibelle();
		}
		return libelleSecteurActivite;
	}
	
	/**
	 * getter du libellé ROME
	 * @return le libellé associé au code ROME
	 */
	public String getLibelleRome() {
		String libelleRome = new String();
		
		if (getStructure().romId() == null || MyStringCtrl.isEmpty(getStructure().romId().toString())) {
			libelleRome = "---";
		} else {
			libelleRome = getStructure().toRome().romLibelle();
		}
		return libelleRome;
	}
	
	/**
	 * Méthode qui contrôle l'utilisation du témoin sur le Siret
	 * @return true si TEM_SIRET_PROVISOIRE est renseigné avec P ou R en base
	 */
	public boolean hasTemoinSiret() {
		if (getStructure() != null
				&& getStructure().temSiretProvisoire() != null
				&& (getStructure().temSiretProvisoire().equals("P") || getStructure().temSiretProvisoire().equals("R"))) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Méthode de test pour savoir si le Siret est provisoire
	 * @return true si le Siret a le témoin mis à provisoire
	 */
	public boolean isSiretProvisoire() {
		if (getStructure() != null
				&& getStructure().temSiretProvisoire() != null
				&& getStructure().temSiretProvisoire().equals("P")) {
			return true;
		}
		return false;
	}
	
	public boolean isNotSiretProvisoire() {
		return !isSiretProvisoire();
	}
	
	/**
	 * @return ID du composant des radios boutons du témoin provisoire sur le N° de Siret
	 */
	public String getStatutSiret() {
		return getComponentId() + "_StatutSiret";
	}
	
	/**
	 * getter sur le témoin relatif au N° de Siret
	 * @return P ou R suivant que le N° Siret est provisoire ou réel
	 */
	public String getTemoinSiret() {
		return getStructure().temSiretProvisoire();
	}

	/**
	 * setter sur le témoin relatif au N° de Siret
	 * @param temoinSiret la valeur est P ou R
	 * suivant que le N° Siret est provisoire ou réel
	 */
	public void setTemoinSiret(String temoinSiret) {
		getStructure().setTemSiretProvisoire(temoinSiret);
	}
	
	/**
	 * Verifie la longueur de la fonction d'un individu limitée à 120 caractères. (Attribut IND_QUALITE)
	 */
	protected boolean checkIndQualite(String qualite) {
		int longueurFonction = 120;
		if (qualite != null && qualite.length() > longueurFonction) {
			mySession().addSimpleErrorMessage("La fonction de l'individu fait plus de " + longueurFonction + " caractères.");
			return false;
		}
		return true;
	}
	
	/**
	 * Verifie la longueur du statut d'un individu limitée à 100 caractères. (Attribut IND_ACTIVITEE)
	 */
	protected boolean checkIndActivite(String activite) {
		int longueurStatut = 100;
		if ( activite != null && activite.length() > longueurStatut) {
			mySession().addSimpleErrorMessage("Le statut de l'individu fait plus de " + longueurStatut + " caractères.");
			return false;
		}
		return true;
	}
	
	
}
