/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;

/**
 * Gestion des membres d'un groupe.
 * 
 * @binding editingContext
 * @binding utilisateurPersId
 * @binding formID
 * @binding structure
 * @binding isEditing
 * @binding isReadOnly
 * @binding selectedRepartStructure will set.
 * @binding selectionOnComplete Fonction Javascript a executer lors de la selection d'un element dans la liste.
 * @binding listSize Nombre d'elements a afficher dans la liste des membres (Integer). Par defaut 10.
 * @binding noElementMsg Message a afficher lorsque aucun element n'est affecte a la structure.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @see PersonneGroupesUI
 */
public class StructureElementUI extends AComponent {
	private static final long serialVersionUID = 1L;

	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_structure = "structure";
	public static final String BINDING_isEditing = "isEditing";
	public static final String BINDING_isReadOnly = "isReadOnly";

	/** Nombre d'elements a afficher dans la liste (Integer) */
	public static final String BINDING_listSize = "listSize";

	/**
	 * Fonction Javascript a executer lors de la selection d'un element dans la liste.
	 */
	public static final String BINDING_selectionOnComplete = "selectionOnComplete";
	public final static String BINDING_selectedRepartStructure = "selectedRepartStructure";

	/** Message a afficher lorsque aucun element n'est affecte a la structure. */
	public static final String BINDING_noElementMsg = "noElementMsg";
	public static final String DEFAULT_noElementMsg = "Aucun membre associé";
	public static final Integer DEFAULT_listSize = Integer.valueOf(10);

	/** */

	private EOEditingContext _editingContext;
	private EOStructure _structure;
	//	private String erreurSaisieMessage;

	public EORepartStructure unRepartStructure;
	private NSArray lesRepartStructures;
	public EORepartStructure editRepartStructure;
	private EORepartStructure selectedRepartStructure;

	public StructureElementUI(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
			}
		}
	}

	//
	//    public Boolean isErreurSaisie() {
	//		return Boolean.valueOf(erreurSaisieMessage != null);
	//	}
	//
	//	public String getErreurSaisieMessage() {
	//		String tmp = erreurSaisieMessage;
	//		erreurSaisieMessage = null;
	//		return tmp;
	//	}
	//	
	//	public void setErreurSaisieMessage(String erreurSaisieMessage) {
	//		this.erreurSaisieMessage = erreurSaisieMessage;
	//	}

	public Boolean isEditing() {
		return (Boolean) valueForBinding(BINDING_isEditing);
	}

	public void setIsEditing(Boolean value) {
		setValueForBinding(value, BINDING_isEditing);
	}

	public Boolean isCreatingNewAffectation() {
		return Boolean.valueOf(edc().insertedObjects().indexOf(editRepartStructure) != NSArray.NotFound);
	}

	@Override
	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			//_editingContext = new EOEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public void reset() {
		System.out.println("test reset");
	}

	public EOStructure getStructure() {
		EOStructure pers = (EOStructure) valueForBinding(BINDING_structure);
		if (pers == null) {
			_structure = null;
			lesRepartStructures = null;
		}
		else if (_structure == null || _structure.globalID() == null || !_structure.globalID().equals(pers.globalID())) {
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			_structure = pers.localInstanceIn(edc());
		}
		return _structure;
	}

	public NSArray lesRepartStructures() {
		if (getStructure() != null) {
			// FIXME Le tri suivant provoque la premiere fois le fetch sur toPersonne pour chaque element
			lesRepartStructures = getStructure().toRepartStructuresElts();
			lesRepartStructures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesRepartStructures, new NSArray(EOSortOrdering.sortOrderingWithKey(EORepartStructure.TO_PERSONNE_ELT_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending)));
			return lesRepartStructures;
		}
		return NSArray.EmptyArray;
	}

	public Boolean hasElements() {
		return (lesRepartStructures() != null && lesRepartStructures().count() > 0);
	}

	public String getLibelleForElement() {
		if (unRepartStructure.toPersonneElt() == null) {
			return null;
		}
		return MyStringCtrl.compactString(unRepartStructure.toPersonneElt().getNomCompletAffichage(), 40, "...");
	}

	public WOActionResults onAffectationCreer() {
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		editRepartStructure = EORepartStructure.creerInstance(edForGroupe);
		editRepartStructure.setToStructureGroupeRelationship(getStructure());
		editRepartStructure.setPersIdModification(getUtilisateurPersId());
		//editRepartStructure.setPersIdCreation(getUtilisateurPersId());

		EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edForGroupe);
		//		repartAssociation.setPersIdCreation(getUtilisateurPersId());
		repartAssociation.setPersIdModification(getUtilisateurPersId());
		editRepartStructure.addToToRepartAssociationsRelationship(repartAssociation);

		setIsEditing(Boolean.TRUE);
		return null;
	}

	public WOActionResults onAffectationModifier() {
		if (getSelectedRepartStructure() == null) {
			return null;
		}
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		editRepartStructure = getSelectedRepartStructure();
		editRepartStructure.setPersIdModification(getUtilisateurPersId());
		if (editRepartStructure.toRepartAssociations().count() == 0) {
			EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edForGroupe);
			repartAssociation.setPersIdModification(getUtilisateurPersId());
			editRepartStructure.addToToRepartAssociationsRelationship(repartAssociation);
		}
		setIsEditing(Boolean.TRUE);
		return null;
	}

	public WOActionResults onAffectationSupprimer() {
		EOEditingContext edForGroupe = edc();
		editRepartStructure = getSelectedRepartStructure();
		try {
			if (editRepartStructure != null) {
				getStructure().getPersonneDelegate().supprimerAffectationAUnGroupe(edForGroupe, getUtilisateurPersId(), editRepartStructure);

				//				getStructure().removeFromToRepartStructuresEltsRelationship(editRepartStructure);
				//
				//				editRepartStructure.deleteAllToRepartAssociationsRelationships();
				//
				//				//				editRepartStructure.setCStructure(null);
				//				edForGroupe.deleteObject(editRepartStructure);
				//
				//				//				if (repartAssociation != null) {
				//				//					edForGroupe.deleteObject(repartAssociation);
				//				//				}
				edForGroupe.saveChanges();
				setSelectedRepartStructure(null);
				setIsEditing(Boolean.FALSE);
			}
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		edForGroupe.revert();
		return null;
	}

	public WOActionResults onAffectationAnnuler() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		edForGroupe.revert();
		editRepartStructure = null;
		setSelectedRepartStructure(null);
		//initialiseSelectedAdresse();
		setIsEditing(Boolean.FALSE);
		return null;
	}

	public WOActionResults onAffectationEnregistrer() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		try {
			if (edForGroupe != null) {

				if (editRepartStructure.persId() == null) {
					throw new NSValidation.ValidationException("Veuillez choisir une personne.");
				}
				//				if (editRepartStructure.toRepartAssociation().toAssociation() == null) {
				//					edForGroupe.deleteObject(editRepartStructure.toRepartAssociation());
				//					editRepartStructure.setToRepartAssociationRelationship(null);
				//				}
				// nettoyer les repartassociation vides
				editRepartStructure.nettoieRepartAssociationVides();
				editRepartStructure.toPersonneElt().setPersIdModification(getUtilisateurPersId());

				//si la structure n'est pas un groupe, on la force
				EOStructureForGroupeSpec.setStructureAsGroupe(editRepartStructure.toStructureGroupe());

				// push dans l'ec parent
				edForGroupe.saveChanges();
			}
			setSelectedRepartStructure(editRepartStructure);
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		//		setValueForBinding(Boolean.TRUE, BINDING_wantRefresh);
		return null;
	}

	public Boolean isAffectationCreerDisabled() {
		return !isAffectationCreerEnabled();
	}

	public Boolean isAffectationCreerEnabled() {
		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitGererGroupe(getStructure()));
	}

	public Boolean isAffectationModifierDisabled() {
		return !isAffectationModifierEnabled();
	}

	public Boolean isAffectationModifierEnabled() {
		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitGererGroupe(getStructure()) && getSelectedRepartStructure() != null);
	}

	public Boolean isAffectationSupprimerDisabled() {
		return !isAffectationSupprimerEnabled();
	}

	public Boolean isAffectationSupprimerEnabled() {
		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitGererGroupe(getStructure()) && getSelectedRepartStructure() != null);
	}

	public EORepartStructure getSelectedRepartStructure() {
		if (hasBinding(BINDING_selectedRepartStructure)) {
			EORepartStructure rpa = (EORepartStructure) valueForBinding(BINDING_selectedRepartStructure);
			if (rpa != null && !edc().equals(rpa.editingContext())) {
				rpa = rpa.localInstanceIn(edc());
			}
			selectedRepartStructure = rpa;
		}
		return selectedRepartStructure;
	}

	public void setSelectedRepartStructure(EORepartStructure rp) {
		this.selectedRepartStructure = rp;
		if (canSetValueForBinding(BINDING_selectedRepartStructure)) {
			setValueForBinding(rp, BINDING_selectedRepartStructure);
		}
	}

	public String elementViewContainerId() {
		return getComponentId() + "_ElementViewContainer";
	}

	public Integer listSize() {
		if (hasBinding(BINDING_listSize)) {
			return (Integer) valueForBinding(BINDING_listSize);
		}
		return DEFAULT_listSize;
	}

	public String selectionOnComplete() {
		if (hasBinding(BINDING_selectionOnComplete)) {
			return (String) valueForBinding(BINDING_selectionOnComplete);
		}
		return null;
	}

	public String noElementMsg() {
		if (valueForBinding(BINDING_noElementMsg) != null) {
			return (String) valueForBinding(BINDING_noElementMsg);
		}
		return DEFAULT_noElementMsg;
	}

	public void resetEdc() {
		_editingContext = null;
	}

}
