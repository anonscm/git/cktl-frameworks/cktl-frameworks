/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTVCheckBoxCell;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * Permet d'afficher une liste de personne sous forme de TableView. Une liste de colonnes par defaut est disponible. Uptilisez le binding
 * <b>colonnesKeys</b> pour paramétrer les colonnes à afficher à partir de la liste par défaut. Si vous souhaitez ajouter une nouvelle
 * colonne, surcharger le composant. Les colonnes sont paramétrables.
 * 
 * @binding updateContainerID Facultatif.
 * @binding colonnesKeys Facultatif. Les colonnes a afficher sous forme de liste séparée par des virgules (cf. les constante commencant par
 *          COL_), {@link PersonneTableView#DEFAULT_COLONNES_KEYS}. Exemple :
 *          <code>colonnesKeys = numero,nomPrenomAffichage,ville,isInterneEtablissement</code> .
 * @binding showDetailInModalBox Facultatif. Indique si on affiche une colonne permettant d'ouvrir une fenetre modale avec le detail de la
 *          personne selectionnee
 * @binding dg Obligatoire. DisplayGroup contenant les données.
 * @binding selectionMultipleEnabled Facultatif. Si true, permet la sélection multiple. Charge au développeur de récupérer les objets
 *          sélectionnés avec le displayGroup.
 * @author rprin
 */
public class PersonneTableView extends AComponent {

	private static final long serialVersionUID = 5769452443460593254L;
	public static final String COL_DETAIL_KEY = "detail";
	public static final String COL_NUMERO_KEY = IPersonne.NUMERO_KEY;
	public static final String COL_NUMERO_INT_KEY = IPersonne.NUMERO_INT_KEY; // PYM : Remplace IPersonne.NUMERO_KEY pour des raisons de tri
																				// lorsque individus et structures se cotoient (cf
																				// cStructure étant une String)
	public static final String COL_NOM_KEY = IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
	public static final String COL_NOM_COMPLET_KEY = IPersonne.NOM_PRENOM_COMPLET_AFFICHAGE_KEY;
	public static final String COL_PERS_ID_KEY = IPersonne.PERSID_KEY;
	public static final String COL_FOURNISSEUR_KEY = IPersonne.TO_FOURNIS_KEY;
	public static final String COL_VILLE_KEY = EOAdresse.VILLE_KEY;
	public static final String COL_IS_INTERNE_KEY = IPersonne.IS_INTERNE_KEY;
	public static final String COL_SIRET_KEY = EOStructure.SIRET_KEY;
	public static final String COL_DATE_NAISSANCE_KEY = IPersonne.DATE_NAISSANCE_KEY;
	public static final String COL_NUM_ETUDIANT_KEY = IEtudiant.NUMERO_ETUDIANT_KEY;
	public static final String COL_INE_ETUDIANT_KEY = IEtudiant.CODE_INE_KEY;

	// public static final String BINDING_updateContainerID = "updateContainerID";

	private IPersonne unePersonne;
	private IPersonne selectedPersonne;

	/** Bindings pour les colonnes a afficher, {@link PersonneTableView#DEFAULT_COLONNES_KEYS} */
	public static final String BINDING_colonnesKeys = "colonnesKeys";

	public static final String BINDING_showDetailInModalBoxId = "showDetailInModalBoxId";

	private static final String PERS_KEY = "unePersonne.";
	private static final String IND_KEY = "unIndividu.";
	private static final String STR_KEY = "uneStructure.";
	private static final String ETUD_KEY = "unEtudiant.";

	private static final String BINDING_selectedPersonne = "selectedPersonne";

	// public static final String COL_DATE_KEY = EOStructure.D_CREATION_KEY;

	private NSArray colonnes;

	public static final NSMutableDictionary _colonnesMap = new NSMutableDictionary();

	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("&nbsp;");
		col0.setComponent(PersonneDetailColonne.class.getSimpleName());
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_NUMERO_KEY, " ");
		ass0.setObjectForKey("afficherDetailPersonne", "action");
		ass0.setObjectForKey("onClickDetailler", "onClickDetailler");
		col0.setAssociations(ass0);
		_colonnesMap.takeValueForKey(col0, COL_DETAIL_KEY);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Numéro");
		// col1.setOrderKeyPath(COL_NUMERO_KEY);
		col1.setOrderKeyPath(COL_NUMERO_INT_KEY);
		// CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NUMERO_KEY, " ");
		// CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_NUMERO_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_NUMERO_INT_KEY, " ");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, COL_NUMERO_KEY); // PYM : Garde la compatibilité avec les noms de colonne utilisant
															// IPersonne.NUMERO_KEY pour des raisons de tri (cf cStructure étant une String)
		_colonnesMap.takeValueForKey(col1, COL_NUMERO_INT_KEY);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Nom");
		col2.setOrderKeyPath(COL_NOM_KEY);
		// CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_NOM_KEY, " ");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, COL_NOM_KEY);

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Nom");
		col3.setOrderKeyPath(COL_NOM_COMPLET_KEY);
		CktlAjaxTableViewColumnAssociation assNomComplet = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_NOM_COMPLET_KEY, " ");
		col3.setAssociations(assNomComplet);
		_colonnesMap.takeValueForKey(col3, COL_NOM_COMPLET_KEY);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Fournisseur");
		col4.setComponent(FouValideColonne.class.getName());
		col4.setOrderKeyPath(COL_FOURNISSEUR_KEY + "." + EOFournis.FOU_VALIDE_KEY);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_FOURNISSEUR_KEY, null);
		col4.setAssociations(ass4);
		_colonnesMap.takeValueForKey(col4, COL_FOURNISSEUR_KEY);

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Ville");
		col5.setOrderKeyPath("getAdressePrincipale." + COL_VILLE_KEY);
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + "getAdressePrincipale." + COL_VILLE_KEY, " ");
		col5.setAssociations(ass5);
		_colonnesMap.takeValueForKey(col5, COL_VILLE_KEY);

		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Interne ?");
		col6.setComponent(CktlAjaxTVCheckBoxCell.class.getName());
		col6.setOrderKeyPath(COL_IS_INTERNE_KEY);
		col6.setRowCssClass("alignToCenter useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_IS_INTERNE_KEY, " ");
		ass6.setObjectForKey(PERS_KEY + COL_IS_INTERNE_KEY, "checked");
		col6.setAssociations(ass6);
		_colonnesMap.takeValueForKey(col6, COL_IS_INTERNE_KEY);

		CktlAjaxTableViewColumn siret = new CktlAjaxTableViewColumn();
		siret.setLibelle("Siret");
		siret.setOrderKeyPath(COL_SIRET_KEY);
		// CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(STR_KEY+COL_SIRET_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(STR_KEY + COL_SIRET_KEY, " ");
		siret.setAssociations(ass3);
		_colonnesMap.takeValueForKey(siret, COL_SIRET_KEY);

		CktlAjaxTableViewColumn dNaissance = new CktlAjaxTableViewColumn();
		dNaissance.setLibelle("Date de naissance");
		dNaissance.setOrderKeyPath(COL_DATE_NAISSANCE_KEY);
		CktlAjaxTableViewColumnAssociation ass7 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_DATE_NAISSANCE_KEY, "");
		ass7.setDateformat("%d/%m/%Y");
		dNaissance.setAssociations(ass7);
		_colonnesMap.takeValueForKey(dNaissance, COL_DATE_NAISSANCE_KEY);

		// CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		// col5.setLibelle("Test");
		// col5.setOrderKeyPath(COL_DATE_KEY);
		// CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_DATE_KEY, "emptyValue");
		// ass5.setDateFormat("%d/%m/%Y");
		// //ass5.setFormatter("dateFormatter");
		// col5.setAssociations(ass5);
		// _colonnesMap.takeValueForKey(col5, COL_DATE_KEY);
		
		CktlAjaxTableViewColumn numEtud = new CktlAjaxTableViewColumn();
		numEtud.setLibelle("Numéro Etudiant");
		numEtud.setOrderKeyPath(COL_NUM_ETUDIANT_KEY);
		CktlAjaxTableViewColumnAssociation ass8 = new CktlAjaxTableViewColumnAssociation(ETUD_KEY + COL_NUM_ETUDIANT_KEY, "");
		numEtud.setAssociations(ass8);
		_colonnesMap.takeValueForKey(numEtud, COL_NUM_ETUDIANT_KEY);
		
		CktlAjaxTableViewColumn codeIne = new CktlAjaxTableViewColumn();
		codeIne.setLibelle("Code INE");
		codeIne.setOrderKeyPath(COL_INE_ETUDIANT_KEY);
		CktlAjaxTableViewColumnAssociation ass9 = new CktlAjaxTableViewColumnAssociation(ETUD_KEY + COL_INE_ETUDIANT_KEY, "");
		codeIne.setAssociations(ass9);
		_colonnesMap.takeValueForKey(codeIne, COL_INE_ETUDIANT_KEY);
		
		CktlAjaxTableViewColumn persId = new CktlAjaxTableViewColumn();
		persId.setLibelle("PersId");
		persId.setOrderKeyPath(COL_INE_ETUDIANT_KEY);
		CktlAjaxTableViewColumnAssociation ass10 = new CktlAjaxTableViewColumnAssociation(PERS_KEY + COL_PERS_ID_KEY, "");
		persId.setAssociations(ass10);
		_colonnesMap.takeValueForKey(persId, COL_PERS_ID_KEY);

		

	}

	public NSTimestampFormatter dateFormatter() {
		return new NSTimestampFormatter("%d/%m/%Y");
	}

	/** Tableau contenant les clés identiofiant les colonnes à afficher par défaut. */
	public static NSArray DEFAULT_COLONNES_KEYS = new NSArray(new Object[] { COL_NUMERO_KEY, COL_NOM_KEY });

	public PersonneTableView(WOContext context) {
		super(context);
	}

	public NSArray getColonnes() {
		if (colonnes == null) {
			NSMutableArray res = new NSMutableArray();
			NSArray colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray getColonnesKeys() {
		NSArray keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			if(keysStr != null) {
				keys = NSArray.componentsSeparatedByString(keysStr, ",");
			}
		}
		if (hasBinding(BINDING_showDetailInModalBoxId) && valueForBinding(BINDING_showDetailInModalBoxId) != null) {
			keys = new NSArray(COL_DETAIL_KEY).arrayByAddingObjectsFromArray(keys);
		}
		return keys;
	}

	public String getEmptyValue() {
		return " ";
	}

	public String detailPersonneModalBoxTitle() {
		String detailPersonneModalBoxTitle = null;
		if (unePersonne() != null) {
			detailPersonneModalBoxTitle = unePersonne().persLibelleAffichage();
		}
		return detailPersonneModalBoxTitle;
	}

	public WOActionResults afficherDetailPersonne() {
		CktlAjaxPersonneDetailForWin nextPage = (CktlAjaxPersonneDetailForWin) pageWithName(CktlAjaxPersonneDetailForWin.class.getName());
		nextPage.setPersonne(unePersonne());
		nextPage.setPersId(getUtilisateurPersId());
		nextPage.setParentComponent(context().page());
		return nextPage;
	}

	public String onClickDetailler() {

		String onClickDetailler = null;
		// this.href : url de l'action du bouton (openWinSelection)
		onClickDetailler = "openGenericHRefWin(";
		// id
		onClickDetailler += "'" + (String) valueForBinding(BINDING_showDetailInModalBoxId) + "', ";
		// href
		onClickDetailler += "this.href, ";
		// title
		onClickDetailler += "'" + detailPersonneModalBoxTitle() + "', ";
		// resizable
		onClickDetailler += "true, ";
		// closable
		onClickDetailler += "true, ";
		// minimizable
		onClickDetailler += "false, ";
		// maximizable
		onClickDetailler += "false, ";
		// minWidth
		onClickDetailler += "500, ";
		// width
		onClickDetailler += "500, ";
		// minHeight
		onClickDetailler += "400, ";
		// height
		onClickDetailler += "400, ";
		// className
		if (session() instanceof CocktailAjaxSession) {
			String windowsClassName = (String) session().valueForKey("windowsClassName");
			if (windowsClassName != null && !windowsClassName.equals("")) {
				onClickDetailler += "'" + session().valueForKey("windowsClassName") + "', ";
			}
			else {
				onClickDetailler += "null, ";
			}
		}
		else {
			onClickDetailler += "null, ";
		}
		// updateContainerId
		onClickDetailler += "null);return false;";

		return onClickDetailler;
	}

	public IPersonne unePersonne() {
		return unePersonne;
	}
	
	public EOEtudiant unEtudiant() {
		
		if (unIndividu().isEtudiant()) {
			return EOEtudiant.etudiantForIndividu(edc(), unIndividu());
		}
		
		return null;
	}

	public EOIndividu unIndividu() {
		return (unePersonne.isIndividu() ? (EOIndividu) unePersonne : null);
	}

	public EOStructure uneStructure() {
		return (unePersonne.isStructure() ? (EOStructure) unePersonne : null);
	}

	public void setUnePersonne(IPersonne unePersonne) {
		this.unePersonne = unePersonne;
	}

	public IPersonne getSelectedPersonne() {
		return selectedPersonne;
	}

	public void setSelectedPersonne(IPersonne selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
		setValueForBinding(selectedPersonne, BINDING_selectedPersonne);
	}

}