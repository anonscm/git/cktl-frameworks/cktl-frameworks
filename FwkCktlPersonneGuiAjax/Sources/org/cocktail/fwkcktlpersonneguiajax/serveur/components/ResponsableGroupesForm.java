/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.GroupeAdminFormCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEC;

public class ResponsableGroupesForm extends AComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_groupe = "groupe";
	public static final String BINDING_editingContext = "editingContext";
	private GroupeAdminFormCtrl ctrl;
	private EOEditingContext editingContext;
	
	private EOStructure groupe;
	
    public ResponsableGroupesForm(WOContext context) {
        super(context);
    }
    
    /*
	 * On travaille sur un nested editingContext.
	 */
	@Override
	public EOEditingContext edc() {
		if (editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return editingContext;
	}
	
    public void resetEdc() {
		editingContext = null;
	}
    
    public String responsableFieldId() {
		return getComponentId() + "_" + "responsableField";
	}
    
    public EOStructure groupe() {
		boolean hasChanged = false;
		EOStructure tmp = (EOStructure) valueForBinding(BINDING_groupe);
		if (tmp == null) {
			groupe = tmp;
			hasChanged = true;
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
		}
		else {
			if (groupe == null || groupe.globalID() == null ||
					!groupe.globalID().equals(tmp.globalID())) {
				//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
				resetEdc();
				groupe = tmp.localInstanceIn(edc());
				hasChanged = true;
			}
		}
		if (hasChanged) {
			initialiseRepartTypeGroupeDisplayGroup(groupe);
		}
		return groupe;
	}
    
    private void initialiseRepartTypeGroupeDisplayGroup(EOStructure groupe) {
		getCtrl().initialiseRepartTypeGroupeDisplayGroup(groupe);
	}
    
    public GroupeAdminFormCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (GroupeAdminFormCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new GroupeAdminFormCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}
    
    public EOQualifier getIndividusForResponsableQualifier() {
		return getCtrl().getIndividusForResponsableQualifier();
	}
    
    public EOIndividu getSelectedResponsable() {
		return groupe().toResponsable();
	}
    
    
}