package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.CktlEtablissementEtrangerProvider;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.CktlEtablissementEtrangerProviderImpl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

public class CktlEtablissementEtrangerSelect2Remote extends CktlAjaxWOComponent {
    
    private static final long serialVersionUID = 4548500860002134892L;
    private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_PAYS = "pays";
	private static final String BINDING_EDITING_CONTEXT = "editingContext";
	private static final String BINDING_QUALIFIER_ETABLISSEMENT_ETRANGER = "qualifierEtablissementEtranger";
	
	private CktlEtablissementEtrangerProvider etablissementsEtrangerDataProvider;

	public CktlEtablissementEtrangerSelect2Remote(WOContext context) {
        super(context);
    }
	
	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
	}
	
	public EOStructure getSelection() {
		return (EOStructure) valueForBinding(BINDING_SELECTION);
	}
	
	/**
	 * @param selection la selection
	 */
	public void setSelection(EOStructure selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}
    
    public Boolean disabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}
    
    public EOPays getPays() {
		if (hasBinding(BINDING_PAYS)) {
			return (EOPays) valueForBinding(BINDING_PAYS);
		}
		return null;
	}
	
	public EOQualifier getQualifierEtablissementEtranger() {
		if (hasBinding(BINDING_QUALIFIER_ETABLISSEMENT_ETRANGER)) {
			return (EOQualifier) valueForBinding(BINDING_QUALIFIER_ETABLISSEMENT_ETRANGER);
		}
		return null;
	}
    
    /**
	 * 
	 * @return le paysProvider
	 */
	public CktlEtablissementEtrangerProvider getEtablissementsEtrangersDataProvider() {
		if (etablissementsEtrangerDataProvider == null) {
			etablissementsEtrangerDataProvider = new CktlEtablissementEtrangerProviderImpl(edc(), getQualifierEtablissementEtranger()) {
				
				@Override
				public void setSelectedEtablissementEtranger(EOStructure etablissement) {
					setSelection(etablissement);
				}
				
				@Override
				public EOStructure getSelectedEtablissementEtranger() {
					return getSelection();
				}

				@Override
				public EOPays getPaysFiltre() {
					return getPays();
				}

			}; 
		}
		return etablissementsEtrangerDataProvider;
	}
	
}