/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXWOContext;

/**
 * Composant de sélection d'une commune.
 * 
 * @binding selectedCommune pour setter la commune sélectionnée.
 * @binding value la valeur initialement affiché dans le champ texte (n'est pas
 *          prise à partir de selectedCommune...
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxCommuneAutoComplete extends AComponent {
    private static final long serialVersionUID = 4042004462611897806L;
    private static final String BINDING_SELECTED_COMMUNE = "selectedCommune";
    private static String BINDING_VALUE = "value";
    
    private EOCommune currentCommune;
    private String autoCompleteId;
    
    public CktlAjaxCommuneAutoComplete(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public String getValue() {
        return stringValueForBinding(BINDING_VALUE, null);
    }
    
    public void setValue(String value) {
        setValueForBinding(value, BINDING_VALUE);
    }
    
    public EOCommune getSelectedCommune() {
        return (EOCommune)valueForBinding(BINDING_SELECTED_COMMUNE);
    }
    
    public void setSelectedCommune(EOCommune selectedCommune) {
        setValueForBinding(selectedCommune, BINDING_SELECTED_COMMUNE);
    }
    
    @SuppressWarnings("unchecked")
    public NSArray<EOCommune> getCommunes() {
        return EOCommune.getFilteredCommunes(edc(), getValue(), null);
    }
    
    public String getAutoCompleteId() {
        if (autoCompleteId == null)
            autoCompleteId = ERXWOContext.safeIdentifierName(context(), true);
        return autoCompleteId;
    }
    
    public EOCommune getCurrentCommune() {
        return currentCommune;
    }
    
    public void setCurrentCommune(EOCommune currentCommune) {
        this.currentCommune = currentCommune;
    }
    
}