/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * Composant de sélection d'une UAI.
 * @binding selection will set le UAI sélectionné.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxUAISelect extends AComponent {
    
    private static final long serialVersionUID = -8944270241910907645L;
    private static final String UAI = "currentUAI";
    private static final String BINDING_SELECTION = "selection";
    private static final String BINDING_ELEMENT_NAME = "elementName";
    private static final String CODE_KEY = EORne.C_RNE_KEY;
    private static final String LL_KEY = EORne.LL_RNE_KEY;
    private static final String VILLE_KEY = EORne.VILLE_KEY;
    private static final int NB_DIP_PAR_PAGE = 30;

    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private String filtreVille;
    private String filtreLibelle;
    private String filtreCode;
    private String containerId;
    private String formContainerId;
    private String modalWindowId;
    private ERXDisplayGroup displayGroup;
    private EORne currentUAI;
    private EORne selectedUAI;

    public CktlAjaxUAISelect(WOContext context) {
        super(context);
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public WOActionResults valider() {
    	setSelection(getSelectedUAI());
    	reinitialiserValeurs();
        CktlAjaxWindow.close(context(), getModalWindowId());
        return null;
    }
    
    public WOActionResults annuler() {
    	reinitialiserValeurs();
        CktlAjaxWindow.close(context(), getModalWindowId());
        return doNothing();
    }

	private void reinitialiserValeurs() {
	    filtreCode = null;
    	filtreLibelle = null;
    	filtreVille = null;
    	displayGroup = null;
    	selectedUAI = null;
    }
    
    public WOActionResults filtrer() {
        // On refait un fetch à chaque fois plutôt que filtrer tous les objets
        // en mémoire étant donné le nombre faramineux d'UAI
        getDisplayGroup().setObjectArray(getFilteredUAIs());
        getDisplayGroup().updateDisplayedObjects();
        selectionner();
        return null;
    }
    
    public WOActionResults selectionner() {
        setSelectedUAI((EORne) getDisplayGroup().selectedObject());
        return null;
    }
    
    public EORne getSelection() {
        return (EORne) valueForBinding(BINDING_SELECTION);
    }
    
    public void setSelection(EORne value) {
        setValueForBinding(value, BINDING_SELECTION);
    }
    
    public String elementName() {
    	return stringValueForBinding(BINDING_ELEMENT_NAME, "div");
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Code
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Code");
            col.setOrderKeyPath(CODE_KEY);
            String keyPath = ERXQ.keyPath(UAI, CODE_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Ville
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Ville");
            col.setOrderKeyPath(VILLE_KEY);
            keyPath = ERXQ.keyPath(UAI, VILLE_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Libelle long
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Libellé");
            col.setOrderKeyPath(LL_KEY);
            keyPath = ERXQ.keyPath(UAI, LL_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "");
            col.setAssociations(ass);
            colTmp.add(col);
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }

    public EOQualifier getQualifier() {
        EOQualifier qual;
        qual = ERXQ.and(getQualifierForFiltre(CODE_KEY, getFiltreCode()),
                getQualifierForFiltre(VILLE_KEY, getFiltreVille()),
                getQualifierForFiltre(LL_KEY, getFiltreLibelle()));
        return qual;
    }
    
    private EOQualifier getQualifierForFiltre(String key, String filtre) {
        if (filtre != null && filtre.length() > 2) {
            return ERXQ.contains(key, filtre);
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public NSArray<EORne> getFilteredUAIs() {
        EOQualifier qual = getQualifier();
        if (qual != null && !qual._isEmpty()) {
            return EORne.fetchAll(edc(), qual);
        } else {
            return NSArray.EmptyArray;
        }
    }
    
    public ERXDisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new ERXDisplayGroup();
            displayGroup.setNumberOfObjectsPerBatch(NB_DIP_PAR_PAGE);
        }
        return displayGroup;
    }
    
    public String getContainerId() {
        if (containerId == null) {
            containerId = ERXWOContext.safeIdentifierName(context(), true);
        }
        return containerId;
    }
    
    public String getFormContainerId() {
        if (formContainerId == null) {
            formContainerId = ERXWOContext.safeIdentifierName(context(), true);
        }
        return formContainerId;
    }
    
    
    public String getModalWindowId() {
        if (modalWindowId == null) {
            modalWindowId = ERXWOContext.safeIdentifierName(context(), true);
        }
        return modalWindowId;
    }
    
    public String getFiltreCode() {
        return filtreCode;
    }
    
    public void setFiltreCode(String filtreCode) {
        this.filtreCode = filtreCode;
    }
    
    public String getFiltreVille() {
        return filtreVille;
    }
    
    public void setFiltreVille(String filtreVille) {
        this.filtreVille = filtreVille;
    }
   
    public String getFiltreLibelle() {
        return filtreLibelle;
    }
    
    public void setFiltreLibelle(String filtreLibelle) {
        this.filtreLibelle = filtreLibelle;
    }
    
    public EORne getCurrentUAI() {
        return currentUAI;
    }
    
    public void setCurrentUAI(EORne currentUAI) {
        this.currentUAI = currentUAI;
    }

	public EORne getSelectedUAI() {
	    return selectedUAI;
    }

	public void setSelectedUAI(EORne selectedUAI) {
	    this.selectedUAI = selectedUAI;
    }
    
	public boolean isValidateButtonDisabled() {
		return getSelectedUAI() == null;
	}
}