/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * Composant affichant une liste d'objet filtrable et permettant de sélectionner un objet.<br/>
 * 
 * 2 modes d'utilisation, le mode simple et le mode custom iltrer les objets :
 * <ul>
 *  <li><h2>Mode customisé</h2>
 *     l'utilisateur du composant passe : 
 *     - les colonnes {@link CktlAjaxTableViewColumn} de la tableView via le binding <code>colonnes</code>
 *     - un filtre et un qualifier utilisant le filtre (bindings <code>qualifier</code>et <code>filtre</code>)
 *  </li>
 *  <li>
 *      <h2>Mode simple</h2>
 *      L'utilisateur du composant passe :
 *      - la liste des colonnes sous la forme d'un dictionnaire <code>clefColonne => libelleColonne</code> à passer
 *        via le binding <code>simpleColonnes</code>
 *      - la clef surlaquelle on filtre les objets avec un {@link ERXQ#contains(String, String)}
 *  </li>
 * </ul>
 * Composant affichant une liste d'objet filtrable et permettant de sélectionner un objet.<br/>
 * 
 * 2 modes d'utilisation, le mode simple et le mode custom iltrer les objets :
 * <ul>
 *  <li><h2>Mode customisé</h2>
 *     l'utilisateur du composant passe : 
 *     - les colonnes {@link CktlAjaxTableViewColumn} de la tableView via le binding <code>colonnes</code>
 *     - un filtre et un qualifier utilisant le filtre (bindings <code>qualifier</code>et <code>filtre</code>)
 *  </li>
 *  <li>
 *      <h2>Mode simple</h2>
 *      L'utilisateur du composant passe :
 *      - la liste des colonnes sous la forme d'un dictionnaire <code>clefColonne => libelleColonne</code> à passer
 *        via le binding <code>simpleColonnes</code>
 *      - la clef surlaquelle on filtre les objets avec un {@link ERXQ#contains(String, String)}
 *      <br/>
 *      Exemple d'affichage pour selectionner un GrhumParametre : 
 *      <pre>
 *       WOD :
 *       ------------------------------------------
 *       ParametersMASelection : ASelectComponent {
 *          allObjects = allParameters;
 *          simpleColonnes = colonnes;
 *          selection = selectedParam;
 *          filtreKey = "paramKey";
 *       }
 *       
 *       Java:
 *       ------------------------------------------
 *       public NSArray allParameters { return EOGrhumParametres.fetchAll(); }
 *       public NSDictionary<String, String> getColonnes() {
 *          return new NSDictionary<String, String>("Paramètres",EOGrhumParametres.PARAM_KEY_KEY);
 *       }
 *       ...
 *      </pre>
 *  </li>
 * </ul>
 * 
 * @binding selection will set l'objet sélectionné dans la liste
 * @binding allObjects tous les objets à afficher dans la liste
 * @binding colonnes une liste de {@link CktlAjaxTableViewColumn}
 * @binding qualifier un qualifier pour filtrer la liste (optionnel, à utiliser en corélation avec le binding <code>qualifier</code>)
 * @binding filtre la valeur du filtre (optionnel, à utiliser en corélation avec le binding <code>qualifier</code>)
 * @binding simpleColonnes un dictionnaire décrivant les colonnes à afficher dans la tbv
 * @binding filtreKey la clef surlaquelle on filtre les objets (optionnel, à utiliser dans le cas où l'on n'a pas besoin de passer un qualifier et de manipuler le filtre, on passe juste
 *          la clef pour filtrer les objets récupérés)
 * @binding filtreButtonId l'id du bouton correspondant au filtre (optionnel)
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class ASelectComponent extends CktlWebComponent {
	
	private static final long serialVersionUID = 4592817434048250435L;
	private WODisplayGroup displayGroup;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_ALL_OBJECTS = "allObjects";
	private static final String BINDING_QUALIFIER = "qualifier";
	private static final String BINDING_COLONNES = "colonnes";
	private static final String BINDING_SIMPLE_COLONNES = "simpleColonnes";
	private static final String BINDING_FILTRE = "filtre";
	private static final String BINDING_FILTRE_A_APPLIQUER = "filtreAAppliquer";
	private static final String BINDING_FILTRE_KEY = "filtreKey";
	private static final String BINDING_FILTRE_BUTTON_ID = "filtreButtonId";
	private static final String BINDING_NB_OBJ_PAR_PAGE = "nbObjectsParPage";
	private static final String BINDING_WANT_RESET = "wantReset";
	private static final String BINDING_CALLBACK_ON_SELECTIONNER = "callBackOnSelectionner";

	
	private static final int NB_OBJ_PAR_PAGE_DEFAUT = 20;

	public static final String CURRENT_OBJ_KEY = "currentObject";

	private String containerId;
	private String _filtre;
	private NSArray<CktlAjaxTableViewColumn> _colonnes;
	private NSKeyValueCoding currentObject;

	public ASelectComponent(WOContext context) {
		super(context);
	}
	
	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}
	
	@Override
	public void appendToResponse(WOResponse arg0, WOContext arg1) {
		if (wantReset()) {
			displayGroup = null;
			_filtre = null;
			_colonnes = null;
			setWantReset(false);
			setSelection();
		}
		super.appendToResponse(arg0, arg1);
	}

	private void setSelection() {
	    if (canGetValueForBinding(BINDING_SELECTION)) {
	        Object selection = valueForBinding(BINDING_SELECTION);
	        getDisplayGroup().setSelectedObject(selection);
	    }
	}

	private void resetSelection() {
	    if (canSetValueForBinding(BINDING_SELECTION)) {
	        setValueForBinding(null, BINDING_SELECTION);
	    }
	    getDisplayGroup().setSelectedObject(null);
	}
	
	/**
	 * Methode appliquant un filtre passé dans le binding <code>filtreAApliquer</code> au displayGroup.
	 */
	public void appliquerFiltre() {
		if (hasBinding(BINDING_FILTRE_A_APPLIQUER)) {
//			displayGroup.setQualifier(ERXQ.startsWith("paramKey", valueForStringBinding(BINDING_FILTRE_A_APPLIQUER, "")));
			displayGroup.setQualifier(ERXQ.contains("paramKey", valueForStringBinding(BINDING_FILTRE_A_APPLIQUER, "")));
			displayGroup.updateDisplayedObjects();
		}
	}
	
	public WODisplayGroup getDisplayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			displayGroup.setObjectArray(getAllObjects());
			appliquerFiltre();
			displayGroup.setNumberOfObjectsPerBatch(nbObjParPage());
		}
		return displayGroup;
	}
	
	public boolean wantReset() {
        return booleanValueForBinding(BINDING_WANT_RESET, false);
    }
    
    public void setWantReset(boolean value) {
        if (hasBinding(BINDING_WANT_RESET) && canSetValueForBinding(BINDING_WANT_RESET)) {
            setValueForBinding(false, BINDING_WANT_RESET);
        }    
    }

	@SuppressWarnings("unchecked")
	private NSArray<NSKeyValueCoding> getAllObjects() {
		return (NSArray<NSKeyValueCoding>) valueForBinding(BINDING_ALL_OBJECTS);
	}

	@SuppressWarnings("unchecked")
	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
	    if (_colonnes == null) {
	        if (hasBinding(BINDING_COLONNES)) {
	            _colonnes = (NSArray<CktlAjaxTableViewColumn>) valueForBinding(BINDING_COLONNES);
	        } else if (hasBinding(BINDING_SIMPLE_COLONNES)) {
	            _colonnes = buildColonnes((NSDictionary<String, String>) valueForBinding(BINDING_SIMPLE_COLONNES));
	        } else {
	            throw new IllegalArgumentException("Vous devez fournir le binding " + BINDING_COLONNES + " ou le binding " +
	                            BINDING_SIMPLE_COLONNES);
	        }
	    }
	    return _colonnes;
	}

	private NSArray<CktlAjaxTableViewColumn> buildColonnes(NSDictionary<String, String> desc) {
        NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
	    for (String key : desc.allKeys()) {
            // Colonne Libelle
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle(desc.objectForKey(key));
            col.setOrderKeyPath(key);
            String keyPath = ERXQ.keyPath(CURRENT_OBJ_KEY, key);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "emptyValue");
            col.setAssociations(ass);
            colTmp.add(col);
	    }
	    return colTmp.immutableClone();
	}
	
	public WOActionResults filtrer() {
		getDisplayGroup().setQualifier(getQualifier());
		//RP : repositionne la liste de resultats en page 1
		getDisplayGroup().setCurrentBatchIndex(1);
		getDisplayGroup().updateDisplayedObjects();
		resetSelection();
		return null;
	}

	private EOQualifier getQualifier() {
		if(hasBinding(BINDING_FILTRE_A_APPLIQUER)) {
			if (hasBinding(BINDING_QUALIFIER)) {
//		        return ERXQ.and(ERXQ.startsWith("paramKey", valueForStringBinding(BINDING_FILTRE_A_APPLIQUER, "")),
		        return ERXQ.and(ERXQ.contains("paramKey", valueForStringBinding(BINDING_FILTRE_A_APPLIQUER, "")),
		        		(EOQualifier) valueForBinding(BINDING_QUALIFIER));
		    } else if (hasBinding(BINDING_FILTRE_KEY)) {
//		    	return ERXQ.and(ERXQ.startsWith("paramKey", valueForStringBinding(BINDING_FILTRE_A_APPLIQUER, "")),
		        return ERXQ.and(ERXQ.contains("paramKey", valueForStringBinding(BINDING_FILTRE_A_APPLIQUER, "")),
		        		ERXQ.contains(getFiltreKey(), getFiltre()));
		    } 
		} else if (hasBinding(BINDING_QUALIFIER)) {
	        return (EOQualifier) valueForBinding(BINDING_QUALIFIER);
	    } else if (hasBinding(BINDING_FILTRE_KEY)) {
	        return ERXQ.contains(getFiltreKey(), getFiltre());
	    }
		
        throw new IllegalArgumentException("Vous devez fournir le binding " + BINDING_QUALIFIER + 
            " ou bien le binding " + BINDING_FILTRE_KEY);
	}

    private String getFiltreKey() {
        return (String) valueForBinding(BINDING_FILTRE_KEY);
    }
	
	public WOActionResults afficherTous() {
		setFiltre(null);
		getDisplayGroup().setQualifier(null);
		appliquerFiltre();
		getDisplayGroup().setCurrentBatchIndex(1);
		getDisplayGroup().updateDisplayedObjects();
		resetSelection();
		return null;
	}

	public WOActionResults selectionner() {
		setValueForBinding(getDisplayGroup().selectedObject(), BINDING_SELECTION);
		return (WOActionResults) valueForBinding(BINDING_CALLBACK_ON_SELECTIONNER);
	}

	public String getContainerId() {
		if (containerId == null) {
			containerId = ERXWOContext.safeIdentifierName(context(), true);
		}	
		return containerId;
	}
	
	public String getListContainerId() {
		return "List_" + getContainerId();
	}

	public String filtreButtonId() {
	    if (hasBinding(BINDING_FILTRE_BUTTON_ID)) {
	        return (String) valueForBinding(BINDING_FILTRE_BUTTON_ID);
	    } else {
	        return "Bid_" + getContainerId();
	    }
	}
	
	public String getTbvId() {
	    return "Tbv_" + getContainerId();
	}

	public String getFiltre() {
	    if (hasBinding(BINDING_FILTRE)) {
	        return (String) valueForBinding(BINDING_FILTRE);
	    } else {
	        return _filtre;
	    }
	}

	public void setFiltre(String filtre) {
	    if (hasBinding(BINDING_FILTRE)) {
	        setValueForBinding(filtre, BINDING_FILTRE);
	    } else {
	        _filtre = filtre;
	    }
	}

	private Integer nbObjParPage() {
		if (hasBinding(BINDING_NB_OBJ_PAR_PAGE)) {
			return (Integer) valueForBinding(BINDING_NB_OBJ_PAR_PAGE);
		} else {
			return NB_OBJ_PAR_PAGE_DEFAUT;
		}
	}

	public NSKeyValueCoding getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(NSKeyValueCoding currentObject) {
		this.currentObject = currentObject;
	}
	
}