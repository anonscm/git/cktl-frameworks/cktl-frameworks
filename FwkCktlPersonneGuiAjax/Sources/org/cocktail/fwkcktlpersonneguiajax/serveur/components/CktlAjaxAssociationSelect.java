/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

/**
 * Permet la selection d'une "association" à partir d'un arbre affiché dans une fenêtre modale, ainsi que l'affichage de l'association selectionnee
 * dans un texfield.
 * 
 * @binding selection Variable pour la selection
 * @binding treeQualifier Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être
 *          applicable aux objets EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur.
 * @binding userFiltersDictionary Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent
 *          dans une liste deroulante, le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent êre
 *          applicable aux objets EOStructure.
 * @binding userFiltersFormID ID de l'objet FORM contenant le composant (necessaire si vous specifiez des filtres utilisateurs)
 * @binding treeRootObject Facultatif. Binding pour specifier la racine de l'arbre.
 * @binding textFieldSize Facultatif. Binding pour specifier la taille du champ (en colonnes).
 * @binding updateContainerID Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme.
 * @binding displayDeleteButton Facultatif. Binding pour specifier si le bouton supprimer doit etre affiche. Par defaut a true.
 * @binding confirmBeforeSelection Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection.
 * @binding associationTreeViewDialogIDFacultatif. Binding pour manipuler le modal window id avec le parent (will set)
 */
public class CktlAjaxAssociationSelect extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String SELECTION_BDG = "selection";
	public static final String VALUE_BDG = "value";
	public static final String EMPTY_VALUE_BDG = "emptyValue";
	//	public static final String EDC_BDG = "editingContext";

	public static final String TREE_VIEW_TITLE_BDG = "treeViewTitle";
	public static final String TREE_VIEW_WIDTH_BDG = "treeViewWidth";
	public static final String TREE_VIEW_HEIGHT_BDG = "treeViewHeight";
	public static final String TREE_VIEW_CLASS_NAME_BDG = "treeViewClassName";

	/**
	 * Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être applicable aux objets
	 * EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur.
	 */
	public static final String TREE_QUALIFIER_BDG = "treeQualifier";

	/**
	 * Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent dans une liste deroulante,
	 * le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent êre applicable aux objets
	 * EOStrcuture.
	 */
	public static final String USER_FILTERS_DICTIONARY_BDG = "userFiltersDictionary";

	/** ID de l'objet FORM contenant le composant (necessaire si vous specifiez des filtres utilisateurs) */
	public static final String USER_FILTERS_FORM_ID_BDG = "userFiltersFormID";

	/** Facultatif. Binding pour specifier la racine de l'arbre. */
	public static final String TREE_ROOT_OBJECT_BDG = "treeRootObject";

	/** Facultatif. Binding pour specifier la taille du champ (en colonnes). */
	public static final String TEXT_FIELD_SIZE_BDG = "textFieldSize";

	/** Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme. */
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";

	public static final String TREE_COLLAPSED_IMG_BDG = "treeCollapsedImage";
	public static final String TREE_COLLAPSED_IMG_FWK_BDG = "treeCollapsedImageFramework";
	public static final String TREE_EXPANDED_IMG_BDG = "treeExpandedImage";
	public static final String TREE_EXPANDED_IMG_FWK_BDG = "treeExpandedImageFramework";
	public static final String TREE_LEAF_IMG_BDG = "treeLeafImage";
	public static final String TREE_LEAF_IMG_FWK_BDG = "treeLeafImageFramework";

	/** Facultatif. Binding pour specifier si le bouton supprimer doit etre affiche. Par defaut a true. */
	public static final String DISPLAY_DELETE_BUTTON_BDG = "displayDeleteButton";

	/** Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection. */
	public static final String CONFIRM_BEFORE_SELECTION_BDG = "confirmBeforeSelection";

	/** Facultatif. Binding pour manipuler le modal window id avec le parent (will set) */
	public static final String ASSOCIATION_TREEVIEW_DIALOG_ID = "associationTreeViewDialogID";

	private EOAssociation selection;

	public CktlAjaxAssociationSelect(WOContext context) {
		super(context);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public EOAssociation selection() {
		return selection;
	}

	public void setSelection(EOAssociation selection) {
		this.selection = selection;
		setValueForBinding(selection, CktlAjaxAssociationSelect.SELECTION_BDG);
	}

	public EOAssociation treeRootObject() {
		return (EOAssociation) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

	public void onClose() {
		//		ctrl().setRootAssociation(null);
	}

	public WOActionResults closeWindow() {
		CktlAjaxWindow.close(context(), associationTreeViewDialogID());
		return null;
	}

	public String associationTreeViewDialogID() {
		String id = getComponentId() + "_associationTreeViewDialog";
		if (hasBinding(ASSOCIATION_TREEVIEW_DIALOG_ID) && canSetValueForBinding(ASSOCIATION_TREEVIEW_DIALOG_ID))
			setValueForBinding(id, ASSOCIATION_TREEVIEW_DIALOG_ID);
		return id;
	}

	public String containerAssociationSelectionneId() {
		return getComponentId() + "_selection";
	}

	public String containerOnCloseID() {
		return getComponentId() + "_containerOnClose";
	}

	public String typeAssociationsPopUpID() {
		return getComponentId() + "_typeAssociationsPopUp";
	}

	public Integer textFieldSize() {
		if (hasBinding(TEXT_FIELD_SIZE_BDG)) {
			return (Integer) valueForBinding(TEXT_FIELD_SIZE_BDG);
		}
		return Integer.valueOf(50);
	}

	public void resetAssociationTreeView() {
		return;
	}

	public String onSuccessRechercher() {
		String onSuccessRechercher = null;
		onSuccessRechercher = "function () {openWinAssociation('";
		onSuccessRechercher += getComponentId() + "', '";
		if (valueForBinding(TREE_VIEW_TITLE_BDG) != null) {
			onSuccessRechercher += valueForBinding(TREE_VIEW_TITLE_BDG) + "', true";
		}
		else {
			onSuccessRechercher += "Choisir une association', true";
		}

		if (valueForBinding(TREE_VIEW_WIDTH_BDG) != null) {
			onSuccessRechercher += ", " + valueForBinding(TREE_VIEW_WIDTH_BDG);
		}
		else {
			onSuccessRechercher += ", null";
		}
		if (valueForBinding(TREE_VIEW_HEIGHT_BDG) != null) {
			onSuccessRechercher += ", " + valueForBinding(TREE_VIEW_HEIGHT_BDG);
		}
		else {
			onSuccessRechercher += ", null";
		}
		if (valueForBinding(TREE_VIEW_CLASS_NAME_BDG) != null) {
			onSuccessRechercher += ", '" + valueForBinding(TREE_VIEW_CLASS_NAME_BDG) + "'";
		}
		else {
			onSuccessRechercher += ", null";
		}
		if (valueForBinding(UPDATE_CONTAINER_ID_BDG) != null) {
			onSuccessRechercher += ", '" + valueForBinding(UPDATE_CONTAINER_ID_BDG) + "'";
		}
		else {
			onSuccessRechercher += ", null";
		}
		onSuccessRechercher += ");}";

		return onSuccessRechercher;
	}

	public WOActionResults supprimerSelection() {
		setSelection(null);
		return null;
	}

	public Boolean displayDeleteButton() {
		if (hasBinding(DISPLAY_DELETE_BUTTON_BDG)) {
			return (Boolean) valueForBinding(DISPLAY_DELETE_BUTTON_BDG);
		}
		return Boolean.TRUE;
	}

	public String jsOnSupprimerSuccess() {
		if (valueForBinding(BINDING_updateContainerID) != null) {
			return "function() {" + valueForBinding(BINDING_updateContainerID) + "Update();}";
		}
		return null;
	}

	public String value() {
		if (valueForBinding(VALUE_BDG) != null) {
			return (String) valueForBinding(VALUE_BDG);
		}
		return emptyValue();
	}

	public String emptyValue() {
		return (String) valueForBinding(EMPTY_VALUE_BDG);
	}

	public String onCloseAssociationTreeViewDialog() {
		String onClose = "";
		if (valueForBinding(BINDING_updateContainerID) != null) {
//			onClose = "function() {" + valueForBinding(BINDING_updateContainerID) + "Update();}";
			AjaxUpdateContainer.updateContainerWithID((String)valueForBinding(BINDING_updateContainerID), context());
		}
		return onClose;
	}
}
