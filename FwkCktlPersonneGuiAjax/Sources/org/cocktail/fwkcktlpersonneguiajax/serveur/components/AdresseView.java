/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;

/**
 * Affiche une adresse en readOnly. Utilisez plutôt {@link PersonneAdressesUI}.
 * 
 * @binding adresse L'adresse de type {@link EOAdresse} à afficher (ne doit pas être nulle)
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */

public class AdresseView extends AComponent {
	public static final String BINDING_adresse = "adresse";

	//	public EOAdresse adresse;
	//private EOEditingContext editingContext;

	public AdresseView(WOContext context) {
		super(context);
	}

	public String getBp() {
		return (adresse() == null || StringCtrl.isEmpty(adresse().adrBp()) ? null : "BP " + adresse().adrBp());
	}

	public Boolean ifAdresse2() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().adrAdresse2()));
	}
	public Boolean ifAdresse34() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().adrAdresse2()) && !StringCtrl.isEmpty(adresse().adrAdresse3()));
	}
	
	public Boolean ifCpltLocalisationEtranger() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().cpltLocalisationEtranger()));
	}
	
	public Boolean isInFrance() {
		if (adresse().toPays() != null && adresse().toPays().cPays().equals(EOPays.CODE_PAYS_FRANCE)) {
			return true;
		} else {
			return false;
		}
	}

	public Boolean ifBp() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().adrBp()));
	}
	
	public Boolean ifLieuDit() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().lieuDit()));
	}

	public String adrGps() {
		if (adresse().adrGpsLongitude() != null || adresse().adrGpsLatitude() != null) {
			return "GPS Long. " + adresse().adrGpsLongitude() + ", Lat. " + adresse().adrGpsLatitude();
		}
		return "";
	}

	public EOAdresse adresse() {
		return (EOAdresse) valueForBinding(BINDING_adresse);
	}

	public Boolean ifUrlPere() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().adrUrlPere()));
	}

	public Boolean ifGps() {
		return Boolean.valueOf(adresse() != null && (adresse().adrGpsLatitude() != null || adresse().adrGpsLongitude() != null));
	}

	public String getAdresseUrl() {
		if (adresse() == null) {
			return null;
		}
		return StringCtrl.toHttpLink(adresse().adrUrlPere());
	}
}
