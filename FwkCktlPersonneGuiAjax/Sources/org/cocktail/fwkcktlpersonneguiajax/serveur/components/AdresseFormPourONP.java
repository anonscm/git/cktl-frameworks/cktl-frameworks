package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.providersAdresse.CodePostalVilleProvider;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.providersAdresse.PaysAdresseProvider;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.providersAdresse.TypeVoieAdresseProvider;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.AdresseFormPourOnpCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

import er.ajax.CktlAjaxUtils;

public class AdresseFormPourONP extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_ADRESSE = "adresse";
	public static final String BINDING_IS_NEW = "isNew";
	public static final String BINDING_LES_TYPES_ADRESSE = "lesTypeAdresses";
	public static final String BINDING_SELECTEDTYPEADRESSE = "selectedTypeAdresse";
	public static final String BINDING_SHOW_GPS = "showGPS";

	private String numVoie;
	private String nomVoie;
	private NSArray<String> listeBisTerVoie; 
	private String selectedBisTerVoie = null;
	private String currentBisTerVoie;
	
	private EOTypeVoie selectedTypeVoie;
	
	private String cpltLocalisationEtranger;


	private CktlAjaxSelect2RemoteDataProvider codePostalVilleProvider;
	private CktlAjaxSelect2RemoteDataProvider paysAdresseProvider;
	private CktlAjaxSelect2RemoteDataProvider typeVoieAdresseProvider;

	private AdresseFormPourOnpCtrl ctrl;
	public EOTypeAdresse unTypeAdresse;

	private EOCommune selectedCommune;

	private EOAdresse adresseCache = null;

	public AdresseFormPourONP(WOContext context) {
		super(context);
		ctrl = new AdresseFormPourOnpCtrl(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptResourceInHead(context, response, frameworkName(), "scripts/cktlstringutils.js");

	}


	public String getCadreAdresseOnpId() {
		return getComponentId() + "_cadreAdresseONP";
	}

	public String getNomVoie() {
		nomVoie = getAdresse().nomVoie();
		return nomVoie;
	}

	public void setNomVoie(String adrAdresse1) {
		nomVoie = nettoyageData(adrAdresse1);
		getAdresse().setNomVoie(nomVoie);

		recalculAdresse1();
	}

	public String getCpltLocalisationEtranger() {
		cpltLocalisationEtranger = getAdresse().cpltLocalisationEtranger();
		return cpltLocalisationEtranger;
	}

	public void setCpltLocalisationEtranger(String cpltLocalisationEtranger) {
		this.cpltLocalisationEtranger = nettoyageData(cpltLocalisationEtranger);
		getAdresse().setCpltLocalisationEtranger(this.cpltLocalisationEtranger);
	}

	protected void recalculAdresse1() {
		String chaineAdresse1 = "";
		chaineAdresse1 = chaineAdresse1 + getNumVoie();
		if (getSelectedBisTerVoie() != null && !getSelectedBisTerVoie().equals("---")) {
			chaineAdresse1 = chaineAdresse1 + getAdresse().bisTer();
		}
		if (getSelectedTypeVoie() != null) {
			chaineAdresse1 = chaineAdresse1 + " " + getSelectedTypeVoie().libelleVoie();
		}
		if (getNomVoie() != null) {
			chaineAdresse1 = chaineAdresse1 + " " + getNomVoie();
		}
		getAdresse().setAdrAdresse1(chaineAdresse1);
	}

	public String getAdrAdresse2() {
		return getAdresse().adrAdresse2ONP();
	}

	public void setAdrAdresse2(String adrAdresse2) {
		if (!StringCtrl.isEmpty(adrAdresse2)) {
			getAdresse().setAdrAdresse2ONP(adrAdresse2);
		} else {
			getAdresse().setAdrAdresse2ONP("");
		}
	}

	public String getAdrBp() {
		return getAdresse().adrBp();
	}

	public void setAdrBp(String adrBp) {
		getAdresse().setAdrBp(adrBp);
	}

	public String getCPCache() {
		return getAdresse().getCPCache();
	}

	public void setCPCache(String codePostal) {
		getAdresse().setCPCache(codePostal);
	}

	public EOPays getPays() {
		return getAdresse().toPays();
	}

	public void setPays(EOPays pays) {
		if (pays == null) {
			getAdresse().setToPaysRelationship(null);
		} else {
			getAdresse().setToPaysRelationship(EOPays.localInstanceIn(getAdresse().editingContext(), pays));
		}
	}

	public EOAdresse getAdresse() {
		return (EOAdresse) valueForBinding(BINDING_ADRESSE);
	}

	public NSArray<EOTypeAdresse> getLesTypeAdresses() {
		return (NSArray<EOTypeAdresse>) valueForBinding(BINDING_LES_TYPES_ADRESSE);
	}

	public EOTypeAdresse getSelectedTypeAdresse() {
		return (EOTypeAdresse) valueForBinding(BINDING_SELECTEDTYPEADRESSE);
	}

	public void setSelectedTypeAdresse(EOTypeAdresse selectedTypeAdresse) {
		setValueForBinding(selectedTypeAdresse, BINDING_SELECTEDTYPEADRESSE);
	}

	public Boolean showGPS() {
		Boolean value = (Boolean) valueForBinding(BINDING_SHOW_GPS);
		if (value == null) {
			value = Boolean.TRUE;
		}
		return value;
	}

	public String numVoieId() {
		return getComponentId() + "_numVoie";
	}

	public String nomVoieId() {
		return getComponentId() + "_nomVoie";
	}

	public String bisTerVoieId() {
		return getComponentId() + "_bisTerVoie";
	}

	public String typeVoieId() {
		return getComponentId() + "_typeVoie";
	}

	public String listeBisTerVoieId() {
		return getComponentId() + "_listeBisTerVoie";
	}

	public String listeTypeVoieId() {
		return getComponentId() + "_listeTypeVoie";
	}

	public String adresse2Id() {
		return getComponentId() + "_adresse2";
	}

	public String codePostalId() {
		return getComponentId() + "_codePostal";
	}

	public String cpEtrangerId() {
		return getComponentId() + "_codePostalEtranger";
	}

	public String lieuDitId() {
		return getComponentId() + "_lieuDit";
	}

	public String villeId() {
		return getComponentId() + "_ville";
	}

	public String villeFieldId() {
		return villeId() + "_field";
	}

	public String paysId() {
		return getComponentId() + "_pays";
	}

	public String paysFieldId() {
		return paysId() + "_field";
	}

	public String adrUrlPereId() {
		return getComponentId() + "_urlPere";
	}

	public String paysSelectId() {
		return getComponentId() + "_pays";
	}

	public String cpltLocalEtrangerId() {
		return getComponentId() + "_cpltLocalEtranger";
	}

	private boolean wantAdresseReset() {
		return (adresseCache != null && !adresseCache.equals(valueForBinding(BINDING_ADRESSE)));
	}

	public EOAdresse adresse() {
		if (wantAdresseReset()) {
			adresseCache = new EOAdresse();
		}
		adresseCache = (EOAdresse) valueForBinding(BINDING_ADRESSE);
		return adresseCache;
	}

	public String getAdrUrlPere() {
		return getAdresse().adrUrlPere();
	}

	public void setAdrUrlPere(String adrUrlPere) {
		getAdresse().setAdrUrlPere(adrUrlPere);
	}

	public EOCommune getSelectedCommune() {
		return selectedCommune;
	}

	public void setSelectedCommune(EOCommune selectedCommune) {
		this.selectedCommune = selectedCommune;
		if (selectedCommune == null) {
			setCPCache(null);
			adresse().setVille(null);
		} else {
			setCPCache(selectedCommune.cPostal());
			adresse().setVille(selectedCommune.llCom());
		}
	}

	public String getNumVoie() {
		numVoie = getAdresse().noVoie();
		return numVoie;
	}

	public void setNumVoie(String numVoie) {
		this.numVoie = nettoyageData(numVoie);
		getAdresse().setNoVoie(this.numVoie);
	}


	public String getSelectedBisTerVoie() {
		selectedBisTerVoie = getStringBisTer(getAdresse().bisTer());
		return selectedBisTerVoie;
	}

	public void setSelectedBisTerVoie(String selectedBisTerVoie) {
		this.selectedBisTerVoie = selectedBisTerVoie;
		getAdresse().setBisTer(getLettreBisTer(selectedBisTerVoie));
	}

	protected String getLettreBisTer(String selectedBisTerVoie) {
		if (selectedBisTerVoie == null || selectedBisTerVoie.equals("---")) {
			return null;
		}
		if (selectedBisTerVoie.equals("Bis")) {
			return "B";
		}
		if (selectedBisTerVoie.equals("Ter")) {
			return "T";
		}
		if (selectedBisTerVoie.equals("Quater")) {
			return "Q";
		}
		if (selectedBisTerVoie.equals("Quinquies")) {
			return "C";
		}
		return null;
	}

	protected String getStringBisTer(String selectedBisTerVoie) {
		if (selectedBisTerVoie == null || selectedBisTerVoie.equals("---")) {
			return null;
		}
		if (selectedBisTerVoie.equals("B")) {
			return "Bis";
		}
		if (selectedBisTerVoie.equals("T")) {
			return "Ter";
		}
		if (selectedBisTerVoie.equals("Q")) {
			return "Quater";
		}
		if (selectedBisTerVoie.equals("C")) {
			return "Quinquies";
		}
		return null;
	}

	public String getCurrentBisTerVoie() {
		return currentBisTerVoie;
	}

	public void setCurrentBisTerVoie(String currentBisTerVoie) {
		this.currentBisTerVoie = currentBisTerVoie;
	}

	public EOTypeVoie getSelectedTypeVoie() {
		if (selectedTypeVoie == null) {
			selectedTypeVoie = getAdresse().toTypeVoie();
		}
		
		return selectedTypeVoie;
	}

	public void setSelectedTypeVoie(EOTypeVoie selectedTypeVoie) {
		this.selectedTypeVoie = selectedTypeVoie;
		getAdresse().setCVoie(selectedTypeVoie.primaryKey());
		getAdresse().setToTypeVoieRelationship(selectedTypeVoie);
				recalculAdresse1();
	}

	public NSArray<String> getListeBisTerVoie() {
		if (listeBisTerVoie == null) {
			listeBisTerVoie = new NSArray<String>(new String [] {"---", "Bis", "Ter", "Quater", "Quinquies"});
		}
		return listeBisTerVoie;
	}

	public Boolean ifAdresse2() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().adrAdresse2()));
	}

	public Boolean ifBp() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().adrBp()));
	}

	public Boolean ifLieuDit() {
		return Boolean.valueOf(adresse() != null && !StringCtrl.isEmpty(adresse().lieuDit()));
	}

	public Boolean testExistenceAncienFormat() {
		return Boolean.valueOf(adresse() != null
				&& (StringCtrl.isEmpty(adresse().noVoie()) || StringCtrl.isEmpty(adresse().nomVoie()) || StringCtrl.isEmpty(adresse().cVoie()))
				&& !StringCtrl.isEmpty(adresse().adrAdresse1()));
	}

	public Boolean isInFrance() {
		if (adresse().toPays() != null && adresse().toPays().cPays().equals(EOPays.CODE_PAYS_FRANCE)) {
			return true;
		} else {
			return false;
		}
	}

	public AdresseFormPourOnpCtrl getCtrl() {
		return ctrl;
	}

	public void setCtrl(AdresseFormPourOnpCtrl ctrl) {
		this.ctrl = ctrl;
	}

	/**
	 * @param selectedPays the selectedPays to set
	 */
	public void setSelectedPays(EOPays selectedPays) {
		if (selectedPays != null) {
			getAdresse().setToPaysRelationship(selectedPays);
			//			effaceCodePostalEtVille();
		} else {
			getAdresse().setToPaysRelationship(EOPays.getPaysDefaut(edc()));
		}
	}

	/**
	 * renvoi du pays de l'adresse
	 * @return selectedPays
	 */
	public IPays getSelectedPays() {
		if (getAdresse().toPays() == null) {
			getAdresse().setToPaysRelationship(EOPays.getPaysDefaut(edc()));
		}
		return getAdresse().toPays();
	}

	private String nettoyageData(String value) {
		value = StringCtrl.replace2(value, "_", " ");
		value = StringCtrl.replace2(value, ",", "");
		value = StringCtrl.replace2(value, ";", "");
		return value;
	}

	/**
	 * @return le provider de la liste déroulante des codes postaux
	 */
	public CktlAjaxSelect2RemoteDataProvider codePostalVilleProvider() {
		if (codePostalVilleProvider == null) {
			codePostalVilleProvider = new CodePostalVilleProvider(this);
		}
		return codePostalVilleProvider;
	}
	
	/**
	 * @return le provider de la liste déroulante des pays de résidence
	 */
	public CktlAjaxSelect2RemoteDataProvider paysAdresseProvider() {
		if (paysAdresseProvider == null) {
			paysAdresseProvider = new PaysAdresseProvider(this);
		}
		return paysAdresseProvider;
	}
	/**
	 * @return le provider de la liste déroulante des pays de résidence
	 */
	public CktlAjaxSelect2RemoteDataProvider typeVoieAdresseProvider() {
		if (typeVoieAdresseProvider == null) {
			typeVoieAdresseProvider = new TypeVoieAdresseProvider(this);
		}
		return typeVoieAdresseProvider;
	}

}