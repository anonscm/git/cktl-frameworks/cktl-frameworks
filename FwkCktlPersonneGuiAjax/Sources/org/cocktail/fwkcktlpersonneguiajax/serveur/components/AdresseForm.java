/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.AdresseFormCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

import er.ajax.CktlAjaxUtils;

/**
 * Formulaire de création/modification d'adresse. Utilisez plutôt {@link PersonneAdressesUI}.
 * 
 * @binding adresse L'adresse de type {@link EOAdresse} à modifier (ne doit pas être nulle)
 * @binding isNew Indique s'il s'agit d'une nouvelle adresse ou d'une adresse à modifier
 * @binding lesTypeAdresses Les types d'adresses sélectionnables (dans le cas d'une création).
 * @binding selectedTypeAdresse Indique quel est le type d'adresse sélectionné par l'utilisateur (ou à préselectionner)
 * @author rprin
 */
public class AdresseForm extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_adresse = "adresse";
	public static final String BINDING_isNew = "isNew";
	public static final String BINDING_lesTypeAdresses = "lesTypeAdresses";
	public static final String BINDING_selectedTypeAdresse = "selectedTypeAdresse";
	public static final String BINDING_showGPS = "showGPS";
	public static final String BINDING_DisplayBtAdresse = "displayBtAdresse";

	private final AdresseFormCtrl ctrl;
	public EOTypeAdresse unTypeAdresse;

	private String filtPays2;
	public EOPays unPays2;
	public EOPays selectedPays2;

	private String filtCp;
	private String filtVille;

	public EOCommune unCommune;
	private EOCommune selectedCommune;

	private EOAdresse adresseCache = null;
	

	public AdresseForm(WOContext context) {
		super(context);
		ctrl = new AdresseFormCtrl(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptResourceInHead(context, response, frameworkName(), "scripts/cktlstringutils.js");

	}

	//    public boolean synchronizesVariablesWithBindings() {
	//        return true;
	//    }

	public String getAdrAdresse1() {
		return getAdresse().adrAdresse1();
	}

	public void setAdrAdresse1(String adrAdresse1) {
		getAdresse().setAdrAdresse1(adrAdresse1);
	}

	public String getAdrAdresse2() {
		return getAdresse().adrAdresse2();
	}

	public void setAdrAdresse2(String adrAdresse2) {
		if (!MyStringCtrl.isEmpty(adrAdresse2)) {
//			getAdresse().setAdrAdresse2(MyStringCtrl.extendWithChars(adrAdresse2, " ", 100, false));
			getAdresse().setAdrAdresse2(adrAdresse2);
		} else {
			getAdresse().setAdrAdresse2("");
		}
	}
	public String getAdrAdresse3() {
		return getAdresse().adrAdresse3();
	}

	public void setAdrAdresse3(String adrAdresse3) {
		if (!MyStringCtrl.isEmpty(adrAdresse3)) {
			getAdresse().setAdrAdresse2(MyStringCtrl.extendWithChars(getAdrAdresse2(), " ", 100, false));
//			getAdresse().setAdrAdresse3(MyStringCtrl.extendWithChars(adrAdresse3, " ", 100, false));
			getAdresse().setAdrAdresse3(adrAdresse3);
		} else {
			getAdresse().setAdrAdresse3("");
		}
	}
	public String getAdrAdresse4() {
		return getAdresse().adrAdresse4();
	}

	public void setAdrAdresse4(String adrAdresse4) {
		getAdresse().setAdrAdresse3(MyStringCtrl.extendWithChars(getAdrAdresse3(), " ", 100, false));
		getAdresse().setAdrAdresse4(adrAdresse4);
	}

	public String getAdrBp() {
		return getAdresse().adrBp();
	}

	public void setAdrBp(String adrBp) {
		getAdresse().setAdrBp(adrBp);
	}

	public String getCPCache() {
		return getAdresse().getCPCache();
	}

	public void setCPCache(String codePostal) {
		getAdresse().setCPCache(codePostal);
		filtCp = codePostal;
	}

	public EOPays getPays() {
		return getAdresse().toPays();
	}

	public void setPays(EOPays pays) {
		if (pays == null) {
			getAdresse().setToPaysRelationship(null);
		}
		else {
			getAdresse().setToPaysRelationship(EOPays.localInstanceIn(getAdresse().editingContext(), pays));
		}
	}

	public EOAdresse getAdresse() {
		return (EOAdresse) valueForBinding(BINDING_adresse);
	}

	public String getFiltPays() {
		return (getPays() == null ? null : getPays().llPays());
	}

	public String getFiltCommune() {
		return getCPCache();
	}

	public NSArray<EOTypeAdresse> getLesTypeAdresses() {
		return (NSArray<EOTypeAdresse>) valueForBinding(BINDING_lesTypeAdresses);
	}

	public EOTypeAdresse getSelectedTypeAdresse() {
		return (EOTypeAdresse) valueForBinding(BINDING_selectedTypeAdresse);
	}

	public void setSelectedTypeAdresse(EOTypeAdresse selectedTypeAdresse) {
		setValueForBinding(selectedTypeAdresse, BINDING_selectedTypeAdresse);
	}

	public Boolean showGPS() {
		Boolean value = (Boolean) valueForBinding(BINDING_showGPS);
		if (value == null) {
			value = Boolean.TRUE;
		}
		return value;
	}

	public String codePostalId() {
		return getComponentId() + "_codePostal";
	}

	public String codePostalFieldId() {
		return codePostalId() + "_field";
	}

	public String villeId() {
		return getComponentId() + "_ville";
	}

	public String villeFieldId() {
		return villeId() + "_field";
	}

	public String paysId() {
		return getComponentId() + "_pays";
	}

	public String paysFieldId() {
		return paysId() + "_field";
	}

	public String adrUrlPereId() {
		return getComponentId() + "_urlPere";
	}

	public String paysSelectId() {
		return getComponentId() + "_pays";
	}

	private boolean wantAdresseReset() {
		return (adresseCache != null && !adresseCache.equals(valueForBinding(BINDING_adresse)));
	}

	public EOAdresse adresse() {
		if (wantAdresseReset()) {
			resetForm();
		}
		adresseCache = (EOAdresse) valueForBinding(BINDING_adresse);
		return adresseCache;
	}

	private void resetForm() {
		filtPays2 = null;
		filtVille = null;
		filtCp = null;
	}

	public String getAdrUrlPere() {
		return getAdresse().adrUrlPere();
	}

	public void setAdrUrlPere(String adrUrlPere) {
		getAdresse().setAdrUrlPere(adrUrlPere);
	}

	public String getListeVilleId() {
		return getComponentId() + "_listeVilles";
	}

	public NSArray getFilteredPays2() {
		NSArray res = ctrl.getFilteredPays(filtPays2);
		if (res.count() == 0) {
			filtPays2 = null;
		}
		return res;
	}

	public String getFiltPays2() {
		if (filtPays2 == null) {
			if (adresse().toPays() != null) {
				return adresse().toPays().llPays();
			}
		}
		return filtPays2;
	}

	public void setFiltPays2(String filtPays2) {
		this.filtPays2 = filtPays2;
		if (filtPays2 == null) {
			adresse().setToPaysRelationship(null);
		}
	}

	public EOPays getSelectedPays2() {
		return selectedPays2;
	}

	public void setSelectedPays2(EOPays selectedPays2) {
		this.selectedPays2 = selectedPays2;
		adresse().setToPaysRelationship(selectedPays2);
	}

	public String getFiltCp() {
		if (wantAdresseReset()) {
			filtCp = null;
		}
		if (filtCp == null) {
			return getCPCache();
		}
		return filtCp;
	}

	public void setFiltCp(String filtCp) {
		this.filtCp = filtCp;
		if (adresse().isEtranger()) {
			setCPCache(filtCp);
		}

	}

	public String getFiltVille() {

		if (filtVille == null) {
			return adresse().ville();
		}
		return filtVille;
	}

	public void setFiltVille(String filtVille) {
		this.filtVille = filtVille;
	}

	public EOCommune getSelectedCommune() {
		return selectedCommune;
	}

	public void setSelectedCommune(EOCommune selectedCommune) {
		this.selectedCommune = selectedCommune;
		if (selectedCommune == null) {
			setCPCache(null);
			adresse().setVille(null);
		}
		else {
			setCPCache(selectedCommune.cPostal());
			adresse().setVille(selectedCommune.llCom());
		}
	}

	public NSArray getLesCPs() {
		return ctrl.getFilteredCommunes(filtCp);
	}

	public NSArray getLesVilles() {
		return ctrl.getFilteredCommunes(filtVille);
	}
	
	public WOActionResults onAjoutAdresse() {
		setIsDisplayBtAdresse(Boolean.FALSE);
		return null;
	}
	
	public Boolean isDisplayBtAdresse () {
	
		if ((Boolean) valueForBinding(BINDING_isNew) ) { 
			return displayBtAdresse(); 	
		} else {
			return (displayBtAdresse() && MyStringCtrl.isEmpty(adresse().adrAdresse3()));
		}
	}
	
	public Boolean isDisplayAdresse () {
		if ((Boolean) valueForBinding(BINDING_isNew) ) { 
			return !displayBtAdresse(); 	
		} else {
			return  !displayBtAdresse() || !MyStringCtrl.isEmpty(adresse().adrAdresse3());
		}
	}
	
	public Boolean displayBtAdresse() {  
		return booleanValueForBinding(BINDING_DisplayBtAdresse, true);
	}


	/**
	 * @return the isDisplayBtAdresse
	 */
/*	public Boolean getIsDisplayBtAdresse() {
		return isDisplayBtAdresse;
	}
*/
	/**
	 * @param isDisplayBtAdresse the isDisplayBtAdresse to set
	 */
	public void setIsDisplayBtAdresse(Boolean isDisplayBtAdresse) {
		setValueForBinding(isDisplayBtAdresse, BINDING_DisplayBtAdresse);
	}
	
}
