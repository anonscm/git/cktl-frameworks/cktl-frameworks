package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * 
 * Sélection d'un individu
 * 
 * @author Alexis Tual
 *
 */
public class CktlIndividuSelect2Remote extends CktlAjaxWOComponent {

    private static final long serialVersionUID = 1L;
    private static final String BINDING_SELECTION = "selection";
    private static final String BINDING_EDITING_CONTEXT = "editingContext";
    private static final String BINDING_SEARCH_TEXT = "searchText";
    private static final String BINDING_CSS_CLASS = "cssClass";
    private static final String BINDING_ONLY_PERSONNEL_ACTUEL = "onlyPersonnelActuel";
    
    private CktlIndividuDataProvider individuDataProvider;
    
    /**
     * @param context le contexte
     */
    public CktlIndividuSelect2Remote(WOContext context) {
        super(context);
    }
    
    @Override
    public EOEditingContext edc() {
        return (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
    }

    /**
     * 
     * @return le paysProvider
     */
    public CktlIndividuDataProvider getIndividuDataProvider() {
        if (individuDataProvider == null) {
            individuDataProvider = new CktlIndividuDataProvider(edc(), isOnlyPersonnelActuel()) {
                
                @Override
                public void setSelectedIndividu(EOIndividu individu) {
                    setSelection(individu);
                }
                
                @Override
                public EOIndividu getSelectedIndividu() {
                    return getSelection();
                }
                
            };
        }
        return individuDataProvider;
    }
    
    public EOIndividu getSelection() {
        return (EOIndividu) valueForBinding(BINDING_SELECTION);
    }
    
    /**
     * @param selection la selection
     */
    public void setSelection(EOIndividu selection) {
        setValueForBinding(selection, BINDING_SELECTION);
    }
    
    /**
     * @return le texte à afficher dans l'invite de la searchbox
     */
    public String searchText() {
        return stringValueForBinding(BINDING_SEARCH_TEXT, "Rechercher un individu");
    }
    
    /**
     * @return la classe css à appliquer
     */
    public String cssClass() {
        return stringValueForBinding(BINDING_CSS_CLASS, "individusAuto");
    }
    
    /**
     * @return le texte à afficher dans l'invite de la searchbox
     */
    public Boolean isOnlyPersonnelActuel() {
        return booleanValueForBinding(BINDING_ONLY_PERSONNEL_ACTUEL, false);
    }
    
    
    /**
     * Provider pour la recherche d'individu
     *
     */
    public abstract static class CktlIndividuDataProvider implements CktlAjaxSelect2RemoteDataProvider {

        private EOEditingContext editingContext;
        private Boolean isOnlyPersonnelActuel;
        
        /**
         * @param editingContext l'editingContext
         */
        public CktlIndividuDataProvider(EOEditingContext editingContext, Boolean isOnlyPersonnelActuel) {
            this.editingContext = editingContext;
            this.isOnlyPersonnelActuel = isOnlyPersonnelActuel;
        }
        
        /**
         * @param searchTerm le mot clef
         * @return la liste des individus dont le nom ou le prénom contient la chaine passée.
         * 
         * 
         * 
         */
        public List<Result> results(String searchTerm) {
            List<Result> resultat = new ArrayList<Result>();
            
            NSArray<EOIndividu> individus = EOIndividu.findIndividuForNomOrPrenom(editingContext, searchTerm, isOnlyPersonnelActuel);
            
            for (EOIndividu individu : individus) {
                resultat.add(new Result(individu.persId().toString(), individu.nomPrenom()));
            }
            return resultat;
        }

        /**
         * @param idSelection l'id de la liste selectionné par l'utilisateur
         * @return l'individu correspondant à l'id sélectionné
         */
        public Result onSelect(String idSelection) {
            // cas de la suppression
            if (StringUtils.isEmpty(idSelection)) {
                setSelectedIndividu(null);
                return null;
            }
            EOIndividu individu = EOIndividu.fetchByKeyValue(editingContext, EOIndividu.PERS_ID_KEY, Integer.valueOf(idSelection));
            if (individu != null) {
                setSelectedIndividu(individu);
                return new Result(individu.persId().toString(), individu.nomPrenom());
            } 
            return null;
        }

        /**
         * @return la sélection initiale
         */
        public Result selectionInitiale() {
            if (getSelectedIndividu() == null) {
                return null;
            } else {
                return new Result(getSelectedIndividu().persId().toString(), getSelectedIndividu().nomPrenom());
            }
        }
        
        /**
         * @param individu l'individu qui doit être sélectionné
         */
        public abstract void setSelectedIndividu(EOIndividu individu);
        
        /**
         * @return l'individu sélectionné
         */
        public abstract EOIndividu getSelectedIndividu();
        
    }
    
}