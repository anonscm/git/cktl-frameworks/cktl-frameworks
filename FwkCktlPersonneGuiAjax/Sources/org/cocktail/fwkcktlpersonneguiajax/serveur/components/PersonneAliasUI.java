/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneAlias;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Composant d'édition de l'alias d'une personne.
 * Pour l'instant le composant ne prend en charge que l'édition de l'alias
 * d'un groupe.
 * 
 * @binding personne la personne dont on veut éditer l'alias
 * @binding editingContext l'editingContext
 * @binding utilisateurPersId l'utilisateur 
 * @binding wantReset (optionnel) variable indiquant si l'on veut reseter le composant
 * @binding isEditing (optionnel) indique si le composant est mode édition
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class PersonneAliasUI extends AComponent {

    private static final long serialVersionUID = -447347425290332242L;
    private static final String PERSONNE_BDG = "personne";
    private static final String WANT_RESET_BDG = "wantReset";
    private static final String ON_ALIAS_SAVE = "onAliasSave";

    private IPersonne personne;
    private EOPersonneAlias currentAlias;
    private String updateContainerId;
    private EOStructure newGroupeForAlias;
    private EOEditingContext editingContext;
    private EOPersonneAlias selectedAlias;
    
    private EOPersonneAlias editingAliasForIndividu;
    private Boolean isEditingAliasForIndividu;
    
    
    public PersonneAliasUI(WOContext context) {
        super(context);
    }

    @Override
    public EOEditingContext edc() {
        if (hasBinding(BINDING_editingContext)) {
            if (editingContext == null || wantReset()) {
                if (editingContext != null)
                    editingContext.dispose();
                editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
                // FIX du BUG No snapshot....
                editingContext.setRetainsRegisteredObjects(true);
            }
            return editingContext;
        } else {
            throw new IllegalArgumentException("le binding editingContext est obligatoire");
        }
    }

    public WOActionResults edit() {
        setIsEditing(true);
        return null;
    }

    /**
     * Sauvegarde les changements sur l'ec local.
     * @return null
     */
    public WOActionResults save() {
        try {
            edc().saveChanges(); 
            if (hasBinding(ON_ALIAS_SAVE)) {
                return (WOActionResults) valueForBinding(ON_ALIAS_SAVE);
            }    
        } catch (Exception e) {
            edc().revert();
            setErreurSaisieMessage(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Supprime l'alias secondaire courant et sauvegarde dans l'ec local
     * @return null
     */
    public WOActionResults deleteAliasSecondaire() {
        personne().removeFromToPersonneAliasesRelationship(selectedAlias);
        edc().deleteObject(selectedAlias);
        selectedAlias = null;
        return save();
    }

    /**
     * Ajoute un alias secondaire et sauvegarde dans l'ec local
     * @return null
     */
    public WOActionResults addAliasSecondaire() {
        String alias = newGroupeForAlias.grpAlias();
        if (!ERXStringUtilities.stringIsNullOrEmpty(alias) && !alias.equals(aliasGroupe())) {
            // On crée et rajoute un alias personne au groupe
            EOPersonneAlias newAlias = EOPersonneAlias.creerInstance(edc());
            newAlias.setAlias(alias);
            newAlias.setPersId(personne().persId());
            personne().addToToPersonneAliasesRelationship(newAlias);
        }
        save();
        return null;
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    public boolean wantReset() {
        Boolean wantReset = (Boolean) valueForBinding(WANT_RESET_BDG);
        return wantReset != null && wantReset;
    }

    public IPersonne personne() {
        IPersonne personneTmp = (IPersonne) valueForBinding(PERSONNE_BDG);
        if (personneTmp == null) {
            throw new IllegalArgumentException("Une personne doit etre passee en binding");
        }    
        if (localPersonneHasChanged(personneTmp)) {
        	resetEdc();
            personne = (IPersonne) EOUtilities.localInstanceOfObject(edc(), personneTmp);
        }
        return personne;
    }
	
	public void resetEdc() {
		editingContext = null;
	}

    private boolean localPersonneHasChanged(IPersonne newPersonne) {
        return (personne == null || wantReset() || personne.globalID() == null
        		|| !personne.globalID().equals(newPersonne.globalID()));
    }
	public EOStructure Structure() {
		if (personne() instanceof EOStructure) {
			return (EOStructure) personne();
		}
		return null;
	}

    public String aliasGroupe() {
        if (personne() instanceof EOStructure) {
            return Structure().grpAlias();
        } else {
            return null;
        }
    }

    public void setAliasGroupe(String value) {
        if (personne() instanceof EOStructure) {
            Structure().setGrpAlias(value);
        }
    }

    @SuppressWarnings("unchecked")
    public NSArray<EOPersonneAlias> aliasesSecondairesGroupe() {
        if (personne() instanceof EOStructure) {
            EOStructure structure = (EOStructure) personne();
            return structure.toPersonneAliases();
        } else {
            return NSArray.EmptyArray;
        }
    }

    public String aliasesSecondairesGroupeAsString() {
        StringBuilder builder = new StringBuilder();
        for (EOPersonneAlias alias : aliasesSecondairesGroupe()) {
            builder.append(alias.alias());
            builder.append(",");
        }
        if (builder.length() > 0)
            builder.deleteCharAt(builder.length()-1);
        return builder.toString();
    }

    @SuppressWarnings("unchecked")
    public NSArray<EOPersonneAlias> aliasIndividu() {
        if (personne() instanceof EOIndividu) {
            EOIndividu individu = (EOIndividu) personne();
            return individu.toPersonneAliases();
        } else {
            return NSArray.EmptyArray;
        }
    }
    

    @Override
    public Boolean isEditing() {
    	if (personne().isStructure()) {
    		return (Boolean) valueForBinding(BINDING_isEditing) && getAppUserForAnnuaire().hasDroitGererGroupe((EOStructure) personne());
    	} else {
    		return (Boolean) valueForBinding(BINDING_isEditing) && getAppUserForAnnuaire().hasDroitModificationIPersonne(personne());
    	}
    }
    
//    @Override
//    public void setIsEditing(Boolean value) {
//        if (hasBinding(BINDING_isEditing) && canSetValueForBinding(BINDING_isEditing)) {
//            super.setIsEditing(value);
//        }
//    }

    public boolean isDeleteAliasDisabled() {
        return selectedAlias == null;
    }

    public EOPersonneAlias getCurrentAlias() {
        return currentAlias;
    }

    public void setCurrentAlias(EOPersonneAlias currentAlias) {
        this.currentAlias = currentAlias;
    }

    public String updateContainerId() {
        if (updateContainerId == null) {
            updateContainerId = "Alias" + ERXWOContext.safeIdentifierName(context(), true);
        }
        return updateContainerId;
    }

    public EOStructure getNewGroupeForAlias() {
        return newGroupeForAlias;
    }

    public void setNewGroupeForAlias(EOStructure newGroupeForAlias) {
        this.newGroupeForAlias = newGroupeForAlias;
    }

    public EOPersonneAlias getSelectedAlias() {
        return selectedAlias;
    }

    public void setSelectedAlias(EOPersonneAlias selectedAlias) {
        this.selectedAlias = selectedAlias;
    }
    
    public WOActionResults addAliasIndividu() {
    	editingAliasForIndividu = EOPersonneAlias.creerInstance(edc());
    	setIsEditingAliasForIndividu(true);
    	return doNothing();
    }

    
    public WOActionResults deleteAliasForIndividu() {
        personne().removeFromToPersonneAliasesRelationship(selectedAlias);
        edc().deleteObject(selectedAlias);
        selectedAlias = null;
        return save();
    }
    
    public Boolean hasNotSelectedAliasForIndividu() {
    	return selectedAlias == null;
    }
    
    /**
     * 
     * @return la valeur de l'alias après enregistrement validé
     */
    public WOActionResults validateAliasForIndividu() {
    	NSArray<String> aliasesStrings = (NSArray<String>) aliasIndividu().valueForKey(EOPersonneAlias.ALIAS_KEY);
    	if (aliasesStrings.contains(editingAliasForIndividu.alias())) {
    		mySession().addSimpleErrorMessage("Erreur", "Cet alias est déjà présent dans la liste");
    		return doNothing();
    	}
    	editingAliasForIndividu.setPersId(personne().persId());
		personne().addToToPersonneAliasesRelationship(editingAliasForIndividu);

    	try {
            edc().saveChanges();
            setIsEditingAliasForIndividu(false);
            editingAliasForIndividu = null;
            if (hasBinding(ON_ALIAS_SAVE)) {
                return (WOActionResults) valueForBinding(ON_ALIAS_SAVE);
            }    
        } catch (Exception e) {
            edc().revert();
            mySession().addSimpleErrorMessage("Erreur", e);
            setErreurSaisieMessage(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return null;
    }
    
    public WOActionResults cancelAliasForIndividu() {
    	edc().revert();
    	setIsEditingAliasForIndividu(false);
    	editingAliasForIndividu = null;
    	return doNothing();
    }
    
	public EOPersonneAlias getEditingAliasForIndividu() {
		return editingAliasForIndividu;
	}

	public void setEditingAliasForIndividu(EOPersonneAlias editingAliasForIndividu) {
		this.editingAliasForIndividu = editingAliasForIndividu;
	}

	public Boolean getIsEditingAliasForIndividu() {
		return isEditingAliasForIndividu;
	}

	public void setIsEditingAliasForIndividu(Boolean isEditingAliasForIndividu) {
		this.isEditingAliasForIndividu = isEditingAliasForIndividu;
	}

}