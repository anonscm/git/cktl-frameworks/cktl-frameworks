/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Composant permettant d'afficher un outil de recherche et selection de personne a l'interieur d'une fenetre de selection, avec un formulaire et
 * boutons Ok/Annuler. <br/>
 * Ce composant doit etre initialise via du code (pas directement). Exemple : <br/>
 * Attention : pour Specifier le theme visuel a utiliser, appelez
 * 
 * <pre>
 * setWindowsClassName(CktlAjaxWindow.WINDOWS_CLASS_NAME_GREYLIGHTING);
 * </pre>
 * 
 * par exemple dans le constructeur de la session. <br/>
 * <b>Ajout des ressources web :</b> Ce composant nécessite des ressources web qui doivent être injectées dynamiquement. Pour cela, le composant
 * parent doit implémenter l'interface CktlResourceProvider.
 * 
 * <pre>
 * public WOActionResults openWinSelection() {
 * 	pagePersonneSrch.setUtilisateurPersId(getUtilisateurPersId());
 * 	pagePersonneSrch.setContainerOnCloseID(winPersonneSelectionContainerOnCloseID());
 * 	pagePersonneSrch.setWinId(winPersonneSelectionID());
 * 	pagePersonneSrch.setShowIndividus(showIndividus());
 * 	pagePersonneSrch.setShowStructures(showStructures());
 * 	return pagePersonneSrch;
 * }
 * </pre>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxPersonneSrchForWin extends AComponent {
	public final static Logger logger = Logger.getLogger(CktlAjaxPersonneSrchForWin.class);

	private static final long serialVersionUID = 1L;
	public static final String ACTION_OK = "Ok";
	private static final String ACTION_CANCEL = "Annuler";
	private IPersonne selection;
	private String winId;
	private Integer utilisateurPersId;
	private String containerOnCloseID;
	private String lastAction = null;
	private EOEditingContext edc;
	private Boolean showIndividus = Boolean.TRUE;
	private Boolean showStructures = Boolean.TRUE;
	private Boolean showPersonnesExternes = Boolean.TRUE;
	private Boolean showPersonnesInternes = Boolean.TRUE;
	private Boolean showRadioInterneExterne = Boolean.TRUE;
	private Boolean wantReset = Boolean.FALSE;

	private EOQualifier qualifierForStructures;
	private EOQualifier qualifierForIndividus;
	private EOQualifier qualifierForGroupes;

	private String individuTableViewColonneKeys;
	
	/** Composant parent pour recuperation des feuilles de style chargees */
	private WOComponent parentComponent;

	public CktlAjaxPersonneSrchForWin(WOContext context) {
		super(context);
	}

	public WOActionResults onClose() {
		return null;
	}

	@Override
	public Integer getUtilisateurPersId() {
		return utilisateurPersId;
	}

	public void setUtilisateurPersId(Integer utilisateurPersId) {
		this.utilisateurPersId = utilisateurPersId;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		setLastAction(null);
		// On rajoute les ressources Web du composant parent
		if (parentComponent != null && parentComponent instanceof CktlResourceProvider) {
			((CktlResourceProvider) parentComponent).injectResources(response, context);
		}
		else {
			logger.error("La classe de parentComponent pour devrait implémenter CktlResourceProvider pour CktlAjaxPersonneSrchForWin pouvoir injecter les ressources web nécessaires");
		}
	}

	public WOActionResults onOk() {
		setLastAction(ACTION_OK);
		return null;
	}

	public WOActionResults onCancel() {
		setSelection(null);
		return null;
	}

	public String jsOnCancel() {
		String cmd = "function() {parent.Windows.close('" + getWinId() + "', null);}";
		return cmd;
	}

	public String jsOnOk() {
		String cmd = "function() {parent.Windows.close('" + getWinId() + "', null);}";
		return cmd;
	}

	public IPersonne getSelection() {
		return selection;
	}

	public void setSelection(IPersonne selection) {
		this.selection = selection;
	}

	public String getWinId() {
		return winId;
	}

	public void setWinId(String winId) {
		this.winId = winId;
	}

	public String getContainerOnCloseID() {
		return containerOnCloseID;
	}

	public void setContainerOnCloseID(String containerOnCloseID) {
		this.containerOnCloseID = containerOnCloseID;
	}

	public String getLastAction() {
		return lastAction;
	}

	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}

	public Boolean getShowIndividus() {
		return showIndividus;
	}

	public void setShowIndividus(Boolean showIndividus) {
		this.showIndividus = showIndividus;
	}

	public Boolean getShowStructures() {
		return showStructures;
	}

	public void setShowStructures(Boolean showStructures) {
		this.showStructures = showStructures;
	}

	public Boolean getShowPersonnesExternes() {
		return showPersonnesExternes;
	}

	public void setShowPersonnesExternes(Boolean showPersonnesExternes) {
		this.showPersonnesExternes = showPersonnesExternes;
	}

	public Boolean getShowPersonnesInternes() {
		return showPersonnesInternes;
	}

	public void setShowPersonnesInternes(Boolean showPersonnesInternes) {
		this.showPersonnesInternes = showPersonnesInternes;
	}

	public EOEditingContext edc() {
		if (edc != null) {
			return edc;
		}
		return super.edc();

	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public WOComponent getParentComponent() {
		return parentComponent;
	}

	public void setParentComponent(WOComponent parentComponent) {
		this.parentComponent = parentComponent;
	}

	public EOQualifier getQualifierForStructures() {
		return qualifierForStructures;
	}

	public void setQualifierForStructures(EOQualifier qualifierForStructures) {
		this.qualifierForStructures = qualifierForStructures;
	}

	public EOQualifier getQualifierForIndividus() {
		return qualifierForIndividus;
	}

	public void setQualifierForIndividus(EOQualifier qualifierForIndividus) {
		this.qualifierForIndividus = qualifierForIndividus;
	}

	public void setQualifierForGroupes(EOQualifier qualifierForGroupes) {
		this.qualifierForGroupes = qualifierForGroupes;
	}

	public EOQualifier getQualifierForGroupes() {
		return this.qualifierForGroupes;
	}

	public Boolean getShowRadioInterneExterne() {
		return showRadioInterneExterne;
	}

	public void setShowRadioInterneExterne(Boolean showRadioInterneExterne) {
		this.showRadioInterneExterne = showRadioInterneExterne;
	}

	public boolean isOkDisabled() {
		return getSelection() == null;
	}

	public Boolean getWantReset() {
		return wantReset;
	}

	public void setWantReset(Boolean wantReset) {
		this.wantReset = wantReset;
	}

	public String getIndividuTableViewColonneKeys() {
		return individuTableViewColonneKeys;
	}

	public void setIndividuTableViewColonneKeys(
			String individuTableViewColonneKeys) {
		this.individuTableViewColonneKeys = individuTableViewColonneKeys;
	}

}
