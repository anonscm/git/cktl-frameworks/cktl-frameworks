/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXWOContext;

/**
 * Composant de sélection d'un departement par un champ d'auto completion sur le numero du departement.
 * @binding selectedDept le departement selectionné dans la liste, un setter doit être
 *          disponible pour cette clef.
 * @binding value la valeur initiale apparaissant dans le champ texte.
 * 
 * @author Bruno Garnier  <bruno.garnier at cocktail.org> 
 *
 */

public class CktlAjaxDepartementAutoComplete extends AComponent {
	
	private static final long serialVersionUID = 3997415124368698257L;
	private static String BINDING_SELECTED_DEPARTEMENT = "selectedDepartement";
    private static String BINDING_VALUE = "value";
    private EODepartement currentDepartement;
    private NSArray<EODepartement> allDepartements;
    private String autoCompleteId;

    public CktlAjaxDepartementAutoComplete(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    public String getValue() {
        return stringValueForBinding(BINDING_VALUE, null);
    }

    public void setValue(String value) {
        setValueForBinding(value, BINDING_VALUE);
    }
    
    public EODepartement getSelectedDepartement() {
        return (EODepartement)valueForBinding(BINDING_SELECTED_DEPARTEMENT);
    }

    public void setSelectedDepartement(EODepartement selectedDepartement) {
        if (selectedDepartement == null) {
            setCurrentDepartement(null);
        }
        else {
            setValueForBinding(selectedDepartement, BINDING_SELECTED_DEPARTEMENT);
        }
    }

    public String getAutoCompleteId() {
        if (autoCompleteId == null)
            autoCompleteId = ERXWOContext.safeIdentifierName(context(), true);
        return autoCompleteId;
    }
    
    @SuppressWarnings("unchecked")
    public NSArray<EODepartement> getDepartements() {
       // return EODepartement.getFilteredDepts(edc(), getValue());
        if (allDepartements == null)
        	allDepartements = EODepartement.fetchAll(edc(), new NSArray(EODepartement.SORT_C_DEPARTEMENT));
        return EODepartement.getFilteredDepartements(edc(), getValue(), allDepartements,true,false);

    }

    public EODepartement getCurrentDepartement() {
        return currentDepartement;
    }

    public void setCurrentDepartement(EODepartement currentDepartement) {
        this.currentDepartement = currentDepartement;
    }


}