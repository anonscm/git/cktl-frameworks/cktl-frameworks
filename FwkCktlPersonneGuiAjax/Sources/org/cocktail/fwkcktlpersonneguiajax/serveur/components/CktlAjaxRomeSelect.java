package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EORome;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;

/**
 * Permet la selection d'une "association" à partir d'un arbre affiché dans une fenêtre modale, ainsi que l'affichage de l'association selectionnee
 * dans un texfield.
 * 
 * @binding selection Variable pour la selection
 * @binding treeQualifier Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être
 *          applicable aux objets EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur.
 * @binding userFiltersDictionary Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent
 *          dans une liste deroulante, le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent êre
 *          applicable aux objets EOStructure.
 * @binding userFiltersFormID ID de l'objet FORM contenant le composant (necessaire si vous specifiez des filtres utilisateurs)
 * @binding treeRootObject Facultatif. Binding pour specifier la racine de l'arbre.
 * @binding textFieldSize Facultatif. Binding pour specifier la taille du champ (en colonnes).
 * @binding updateContainerID Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme.
 * @binding displayDeleteButton Facultatif. Binding pour specifier si le bouton supprimer doit etre affiche. Par defaut a true.
 * @binding confirmBeforeSelection Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection.
 * @binding associationTreeViewDialogIDFacultatif. Binding pour manipuler le modal window id avec le parent (will set)
 */


public class CktlAjaxRomeSelect extends AComponent {
	
	private static final long serialVersionUID = 22222L;
	
	private EOEditingContext editingContext = null;
	
	public static final String SELECTION_BDG = "selection";
	public static final String VALUE_BDG = "value";
	public static final String EMPTY_VALUE_BDG = "emptyValue";
	//	public static final String EDC_BDG = "editingContext";


	/** Facultatif. Binding pour specifier la taille du champ (en colonnes). */
	public static final String TEXT_FIELD_SIZE_BDG = "textFieldSize";

	/** Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme. */
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";


	/** Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection. */
	public static final String CONFIRM_BEFORE_SELECTION_BDG = "confirmBeforeSelection";

	/** Facultatif. Binding pour manipuler le modal window id avec le parent (will set) */
	public static final String ROME_TREEVIEW_DIALOG_ID = "romeTreeViewDialogID";
	
	private EORome selection;
	
//	private EORome currentRome =  null;
	private RomeTreeNode rootNode = null;
	private RomeTreeNode currentNode = null;
	private RomeTreeNode selectedNode = null;
	
	
    public CktlAjaxRomeSelect(WOContext context) {
        super(context);
    }
    
    public boolean synchronizesVariablesWithBindings() {
		return false;
	}
    
    /**
	 * Nested editing context
	 * 
	 * @return EOEditingContext
	 */
	public EOEditingContext editingContext() {
		if (editingContext == null) {
			editingContext = ERXEC.newEditingContext(edc());
		}

		return editingContext;
	}

	public EORome selection() {
		return selection;
	}

	public void setSelection(EORome selection) {
		this.selection = selection;
		setValueForBinding(selection, CktlAjaxRomeSelect.SELECTION_BDG);
	}


	public void onClose() {
		//		ctrl().setRootAssociation(null);
	}

	public WOActionResults closeWindow() {
		CktlAjaxWindow.close(context(), romeTreeViewDialogID());
		return null;
	}

	public String romeTreeViewDialogID() {
		String id = getComponentId() + "_associationTreeViewDialog";
		if (hasBinding(ROME_TREEVIEW_DIALOG_ID) && canSetValueForBinding(ROME_TREEVIEW_DIALOG_ID))
			setValueForBinding(id, ROME_TREEVIEW_DIALOG_ID);
		return id;
	}

	public String containerRomeSelectionneId() {
		return getComponentId() + "_selection";
	}

	public String containerOnCloseID() {
		return getComponentId() + "_containerOnClose";
	}

	public String typeAssociationsPopUpID() {
		return getComponentId() + "_typeAssociationsPopUp";
	}

	public Integer textFieldSize() {
		if (hasBinding(TEXT_FIELD_SIZE_BDG)) {
			return (Integer) valueForBinding(TEXT_FIELD_SIZE_BDG);
		}
		return Integer.valueOf(50);
	}

	public void resetRomeTreeView() {
		return;
	}

	public String onSuccessRechercher() {
		String onSuccessRechercher = null;
		onSuccessRechercher = "function () {openWinAssociation('";
		onSuccessRechercher += getComponentId() + "', '";
		onSuccessRechercher += ");}";

		return onSuccessRechercher;
	}

	public WOActionResults supprimerSelection() {
		setSelection(null);
		return null;
	}

	public String jsOnSupprimerSuccess() {
		if (valueForBinding(BINDING_updateContainerID) != null) {
			return "function() {" + valueForBinding(BINDING_updateContainerID) + "Update();}";
		}
		return null;
	}

	public String getValue() {
		if (valueForBinding(VALUE_BDG) != null) {
			return (String) valueForBinding(VALUE_BDG);
		}
		return emptyValue();
	}
	
	public void setValue(String value){
		setValueForBinding(value, VALUE_BDG);
	}

	public String emptyValue() {
		return (String) valueForBinding(EMPTY_VALUE_BDG);
	}

	public String onCloseRomeTreeViewDialog() {
		String onClose = "";
		if (valueForBinding(BINDING_updateContainerID) != null) {
			AjaxUpdateContainer.updateContainerWithID((String)valueForBinding(BINDING_updateContainerID), context());
		}
		return onClose;
	}
	
	/* ***************************************************************** */
	/* ***************** Partie pour la TableTreeView ****************** */
	/* ***************************************************************** */
	
	/**
	 * @return the rootNode
	 */
	public RomeTreeNode getRootNode() {
		if (rootNode == null) {
			rootNode = new RomeTreeNode(getRoot(), null, false);
		}
		return rootNode;
	}
	
	private EORome getRoot() {
		EORome romeRoot = EORome.fetchRequiredByKeyValue(editingContext(), EORome.ROM_ID_KEY, 0);
		return romeRoot;
	}
	
	/**
	 * @param rootNode the rootNode 
	 */
	public void setRootNode(RomeTreeNode rootNode) {
		this.rootNode = rootNode;
	}


	/**
	 * @return the currentNode
	 */
	public RomeTreeNode getCurrentNode() {
		return currentNode;
	}


	/**
	 * @param currentNode the currentNode to set
	 */
	public void setCurrentNode(RomeTreeNode currentNode) {
		this.currentNode = currentNode;
	}
	
	/**
	 * @return the selectedNode
	 */
	public RomeTreeNode getSelectedNode() {
		return selectedNode;
	}


	/**
	 * @param selectedNode the selectedNode to set
	 */
	public void setSelectedNode(RomeTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	
	/**
	 *  
	 * @return WOActionResults
	 */
	public WOActionResults enregistrer() {
			
			if (getSelectedNode() != null) {
				setSelection(getSelectedNode().getData());
				setValue(getSelectedNode().getData().romLibelle());
			}
			setRootNode(null);
			setSelectedNode(null);
			
			closeWindow();
//		if (enregistrerAffichage()) {
//			setRootNode(null);
//			setSelectedNode(null);
//		}

		return doNothing();

	}
	
	/**
	 * 
	 * @return WOActionResults
	 */
	public WOActionResults annuler() {
		editingContext().revert();
		setRootNode(null);
		setSelectedNode(null);
		currentNode = null;
		closeWindow();
		return doNothing();
	}
	
	private boolean enregistrerAffichage()  {
		try {
			editingContext().saveChanges();
			//edc().saveChanges();

			return (true);
		} catch (ValidationException e) {
			editingContext().revert();

			return false;
		}

	}
}