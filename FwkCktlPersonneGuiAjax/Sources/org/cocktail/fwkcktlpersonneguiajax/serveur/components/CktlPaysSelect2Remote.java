package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.CktlPaysProvider;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.CktlPaysProviderImpl;

/**
 * 
 * @author jlafourc
 *
 */
public class CktlPaysSelect2Remote extends CktlAjaxWOComponent {
   
	private static final long serialVersionUID = 8081393030462686141L;

	private static final String BINDING_SELECTION = "selection";
	
	private CktlPaysProvider paysDataProvider;
	
	
	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding("editingContext");
	}
	
	/**
	 * Constructeur obligatoire
	 * @param context le contexte
	 */	
	public CktlPaysSelect2Remote(WOContext context) {
        super(context);
        
    }

	/**
	 * 
	 * @return le paysProvider
	 */
	public CktlPaysProvider getPaysDataProvider() {
		if (paysDataProvider == null) {
			paysDataProvider = new CktlPaysProviderImpl(edc()) {
				
				@Override
				public void setSelectedPays(EOPays pays) {
					setSelection(pays);
				}
				
				@Override
				public EOPays getSelectedPays() {
					return getSelection();
				}
				
			};
		}
		return paysDataProvider;
	}

	public void setPaysDataProvider(CktlPaysProvider paysDataProvider) {
		this.paysDataProvider = paysDataProvider;
	}
	
	public EOPays getSelection() {
		return (EOPays) valueForBinding(BINDING_SELECTION);
	}
	
	/**
	 * @param selection la selection
	 */
	public void setSelection(EOPays selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}
	
	
	
	
}