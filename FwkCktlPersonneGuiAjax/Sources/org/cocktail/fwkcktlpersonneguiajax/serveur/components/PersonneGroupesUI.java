/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForServiceSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.AnnuaireLogService;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

/**
 * Composant qui permet de gérer l'affectation de la personne à des groupes.
 *
 * @binding editingContext
 * @binding personne
 * @binding isEditing
 * @binding isReadOnly
 * @binding utilisateurPersId
 * @binding wantRefresh
 * @binding qualifierForGroupes Qualifier facultatif pour appliquer aux structures de type groupe affichables / selectionnables
 * @binding formID ID de la form dans laquelle est contenu le composant (nécessaire pour permetre le filtre des nouveaux groupes).
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class PersonneGroupesUI extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_personne = "personne";
	public static final String BINDING_isEditing = "isEditing";
	public static final String BINDING_isReadOnly = "isReadOnly";
	public final static String BINDING_wantRefresh = "wantRefresh";
	public final static String BINDING_selectedRepartStructure = "selectedRepartStructure";

	/** ID de la form dans laquelle est contenu le composant */
	public final static String BINDING_formID = "formID";

	/** Qualifier facultatif pour appliquer aux structures de type groupe affichables / selectionnables */
	public static final String BINDING_qualifierForGroupes = "qualifierForGroupes";

	private static final EOQualifier qualifierDefaultForGroupes = null;

	private EOEditingContext _editingContext;
	private IPersonne _personne;
	//	private String erreurSaisieMessage;
	private EOIndividu newIndividu;

	private EOStructure uneStructure;
	private EOStructure selectedStructureForResponsable;
	private EOSecretariat selectedSecretariat;
	private EOStructure editStructure;
	private EOSecretariat unSecretariat;
	private EOSecretariat editSecretariat;

	private int personneResponsableIndex;
	private int personneSecretariatIndex;
	private Boolean isAffectationEditing = false;
	private Boolean isResponsableEditing = false;
	private Boolean isSecretaireEditing = false;

	public EORepartStructure unRepartStructure;
	private NSArray lesRepartStructures;
	public EORepartStructure editRepartStructure;
	private EORepartStructure selectedRepartStructure;
	private NSArray<EORepartStructure> selectedRepartStructures;
	private NSArray groupeExclus;
	public EORepartAssociation unRepartAssociation;

	private EOIndividu nouveauSecretaire = null;
	private NSArray<EOSecretariat> lesSecretariats = null;

	private EOIndividu nouveauResponsable = null;
	private NSArray<EOStructure> lesResponsablesGroupes = null;

	private Boolean resetPersonneGroupesForm = false;

	public PersonneGroupesUI(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
	}

	public String modifierSecretaireWindowId() {
		return getComponentId() + "_modifierSecretaireWindow";
	}

	public String modifierResponsableWindowId() {
		return getComponentId() + "_modifierModifierResponsableWindow";
	}

	public String boutonsModifierSecretaireContainerId() {
		return getComponentId() + "_boutonsModifierSecretaireContainer";
	}

	public String boutonsModifierResponsableContainerId() {
		return getComponentId() + "_boutonsModifierResponsableContainer";
	}
	
	public String suppressionMultipleAffectationWindowId() {
		return getComponentId() + "_suppressionMultipleAffectationWindow";
	}
	
	public String suppressionMultipleAffectationTableViewId() {
		return getComponentId() + "_suppressionMultipleAffectationTableView";
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
			}
		}
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (booleanValueForBinding(BINDING_wantRefresh, Boolean.FALSE).booleanValue()) {
			lesRepartStructures = getPersonne().getRepartStructuresAffectes(getAppUserForAnnuaire());
			lesResponsablesGroupes = null;
			lesSecretariats = null;
			setSelectedRepartStructure(null);
			setSelectedSecretariat(null);
			setSelectedStructureForResponsable(null);
			setValueForBinding(Boolean.FALSE, BINDING_wantRefresh);
		}
		super.appendToResponse(response, context);
	}

	public Boolean isEditing() {
		// Mise à jour des 3 autres booléens + dans les accesseurs setIsResponsable, on fait un set du binding.
		if ((Boolean) valueForBinding(BINDING_isEditing) == Boolean.FALSE) {
			setIsAffectationEditing(Boolean.FALSE);
			setIsResponsableEditing(Boolean.FALSE);
			setIsSecretaireEditing(Boolean.FALSE);
		}
		return (Boolean) valueForBinding(BINDING_isEditing);
	}

	public void setIsEditing(Boolean value) {
		setValueForBinding(value, BINDING_isEditing);
	}

	public Boolean isCreatingNewAffectation() {
		return Boolean.valueOf(editRepartStructure != null && edc().insertedObjects().indexOf(editRepartStructure) != NSArray.NotFound);
	}

	public Boolean wantReset() {
		return (Boolean) valueForBinding(BINDING_wantRefresh);
	}


	public void setWantReset(Boolean value) {
		setValueForBinding(value, BINDING_wantRefresh);
	}

	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (pers == null) {
			_personne = null;
			lesRepartStructures = null;
		} else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			_personne = pers.localInstanceIn(edc());
		}
		return _personne;
	}

	public NSArray lesRepartStructures() {
		if (getPersonne() != null) {
			// On considère les groupes affectés mais en excluant ceux résérvés pour l'application.
			// JULIEN : on va afficher les groupes reserves mais ne pas pouvoir y toucher
			//EOQualifier sansGroupesReservesQual = ERXQ.notIn(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, groupeExclus());
			// NSArray tmp = _personne.getRepartStructuresAffectes(getAppUserForAnnuaire(), sansGroupesReservesQual);
			NSArray tmp = _personne.getRepartStructuresAffectes(getAppUserForAnnuaire());
			if (getQualifierForGroupes() == null) {
				lesRepartStructures = tmp;
			} else {
				// On applique l'eventuel filtre supplémentaire.
				NSMutableArray res = new NSMutableArray();
				for (int i = 0; i < tmp.count(); i++) {
					EORepartStructure repartStructure = (EORepartStructure) tmp.objectAtIndex(i);
					if (getQualifierForGroupes().evaluateWithObject(repartStructure.toStructureGroupe())) {
						res.addObject(repartStructure);
					}
				}
				lesRepartStructures = res.immutableClone();
			}
			EOQualifier rsQualForum = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_F);
			NSArray<EORepartStructure> rsForums = ERXQ.filtered(lesRepartStructures, rsQualForum);
			EOQualifier rsQualSystem = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_U);
	        NSArray<EORepartStructure> rsSystem = ERXQ.filtered(lesRepartStructures, rsQualSystem);
	        NSArray<EORepartStructure> rsGeneraux = ERXArrayUtilities.arrayMinusArray(lesRepartStructures, rsSystem);
	        rsGeneraux = ERXArrayUtilities.arrayMinusArray(rsGeneraux, rsForums);
	        
	        // Modification du 07/08/2014 pour exclure des groupes d'affectation les groupes fournisseurs et les groupes Partenaires
	        // Raison : comme il n'y a pas de contrôle à la suppression, on génère des disfonctionnements dans les applications
	        // avec des fournisseurs ou des partenaires (ex.: dans Physallis)
//	        EOQualifier rsQualFournis = EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("FO");
//	        NSArray<EORepartStructure> rsFournis = ERXQ.filtered(rsGeneraux, rsQualFournis);
//	        rsGeneraux = ERXArrayUtilities.arrayMinusArray(rsGeneraux, rsFournis);
	        EOQualifier rsQualPartenaire = EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("PN");
	        NSArray<EORepartStructure> rsPartenaire = ERXQ.filtered(rsGeneraux, rsQualPartenaire);
	        rsGeneraux = ERXArrayUtilities.arrayMinusArray(rsGeneraux, rsPartenaire);
	        

	        if (!getAppUserForAnnuaire().hasDroitGrhumCreateur()) {
		        EOQualifier qualifierDesGroupesAMasquer =
//		        		EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("RE")
//						.and(EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).atCount().eq(2))
//						.or(EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("FO"))
//						.or(EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("PN"));
		        		EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("RE")
		        		.and(EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).atCount().eq(2));


		        rsGeneraux = ERXArrayUtilities.arrayMinusArray(rsGeneraux, ERXQ.filtered(rsGeneraux, qualifierDesGroupesAMasquer));
	        }
	        NSArray<EORepartStructure> finalLesRepartStructures = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(rsGeneraux, rsSystem);
	       finalLesRepartStructures = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(finalLesRepartStructures, rsForums);

			return finalLesRepartStructures;
		}
		return NSArray.EmptyArray;
	}

	public Boolean hasGroupes() {
		NSArray res = lesRepartStructures();
		return (res != null && res.count() > 0);
	}

	public Boolean hasResponsability() {
		NSArray res = getLesResponsablesGroupes();
		return (res != null && res.count() > 0);
	}

	public Boolean hasSecretary() {
		NSArray res = getLesSecretariats();
		return (res != null && res.count() > 0);
	}

	public String getLibelleForGroupe() {
		return MyStringCtrl.compactString(unRepartStructure.toStructureGroupe().libelleForGroupe(), 40, "...");
	}

	public WOActionResults onAffectationCreer() {
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		editRepartStructure = EORepartStructure.creerInstance(edForGroupe);
		editRepartStructure.setPersIdCreation(getUtilisateurPersId());
		editRepartStructure.setPersIdModification(getUtilisateurPersId());
		editRepartStructure.setToPersonneElt(getPersonne());

		EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edForGroupe);
		repartAssociation.setPersIdCreation(getUtilisateurPersId());
		repartAssociation.setPersIdModification(getUtilisateurPersId());
		editRepartStructure.addToToRepartAssociationsRelationship(repartAssociation);

		setIsAffectationEditing(Boolean.TRUE);
		setIsEditing(Boolean.TRUE);
		return null;
	}

	public WOActionResults onAffectationModifier() {
		if (getSelectedRepartStructure() == null) {
			return null;
		}
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		editRepartStructure = getSelectedRepartStructure();
		editRepartStructure.setPersIdModification(getUtilisateurPersId());
		if (editRepartStructure.toRepartAssociations().count() == 0) {
			EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edForGroupe);
			repartAssociation.setPersIdCreation(getUtilisateurPersId());
			repartAssociation.setPersIdModification(getUtilisateurPersId());
			editRepartStructure.addToToRepartAssociationsRelationship(repartAssociation);
		}
		setIsAffectationEditing(Boolean.TRUE);
		setIsEditing(Boolean.TRUE);
		return null;
	}

	public WOActionResults onAffectationSupprimer() {
		AnnuaireLogService annuaireService = new AnnuaireLogService();
		EOEditingContext edcForAnnuaire = new EOEditingContext();
		EOEditingContext edForGroupe = edc();
		editRepartStructure = getSelectedRepartStructure();
		try {
			if (editRepartStructure != null) {
				String message = "Suppression de l'appartenance au groupe : " + editRepartStructure.libelleEtId()
								+ " pour la personne avec le PersId : " + editRepartStructure.persId();
				String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
				annuaireService.creationUnAnnuaireLog(edcForAnnuaire, message, nomApplication);
				
				getPersonne().getPersonneDelegate().supprimerAffectationAUnGroupe(edForGroupe, getUtilisateurPersId(), editRepartStructure);
				edForGroupe.saveChanges();
				
				edcForAnnuaire.saveChanges();
			}
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		edForGroupe.revert();
		setIsAffectationEditing(Boolean.FALSE);
		setIsEditing(Boolean.FALSE);
		return null;
	}
	
	public WOActionResults onMultipleSuppressionAffectation() {
		EOEditingContext edForGroupe = edc();
		NSArray<EORepartStructure> rsASupprimer = getSelectedRepartStructures();
		
		for (EORepartStructure rs : rsASupprimer) {
			try {
				getPersonne().getPersonneDelegate().supprimerAffectationAUnGroupe(edForGroupe, getUtilisateurPersId(), rs);
				edForGroupe.saveChanges();
			} catch (NSValidation.ValidationException e) {
				e.printStackTrace();
				setErreurSaisieMessage(e.getMessage());
			} catch (Exception e) {
				edForGroupe.revert();
				e.printStackTrace();
				setErreurSaisieMessage(e.getMessage());
			}
		}
		edForGroupe.revert();
		setIsAffectationEditing(Boolean.FALSE);
		setIsEditing(Boolean.FALSE);
		
		AjaxUpdateContainer.updateContainerWithID(getMainContainerId(), context());
		
		return doNothing();
	}

	public WOActionResults onAffectationAnnuler() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		edForGroupe.revert();
		editRepartStructure = null;
		setSelectedRepartStructure(null);
		setResetPersonneGroupesForm(true);
		//initialiseSelectedAdresse();
		setIsAffectationEditing(Boolean.FALSE);
		setIsEditing(Boolean.FALSE);
		return null;
	}

	public WOActionResults onAffectationEnregistrer() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		try {
			if (edForGroupe != null) {
				if (editRepartStructure.toStructureGroupe() == null) {
					throw new NSValidation.ValidationException("Veuillez choisir un groupe.");
				}

				//verifier que l'utilisateur a le droit de selectionner le groupe
				if (!getAppUserForAnnuaire().hasDroitGererGroupe(editRepartStructure.toStructureGroupe())) {
					throw new NSValidation.ValidationException("Vous n'avez pas les droit de gerer ce groupe");
				}
				editRepartStructure.toStructureGroupe().setPersIdModification(getUtilisateurPersId());
				editRepartStructure.toPersonneElt().setPersIdModification(getUtilisateurPersId());

				//nettoyer les repartAssociation vides
				editRepartStructure.nettoieRepartAssociationVides();

				// push de l'adresse dans l'ec parent
				edForGroupe.saveChanges();
			}
			setSelectedRepartStructure(editRepartStructure);
			setIsAffectationEditing(Boolean.FALSE);
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		setValueForBinding(Boolean.TRUE, BINDING_wantRefresh);
		return null;
	}
	
	public WOActionResults ouvrirWindowSuppressionMultiple() {
		CktlAjaxWindow.open(context(), suppressionMultipleAffectationWindowId(), "Selection multiple");
 		return doNothing();
	}

	public WOActionResults onResponsableAnnuler() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		edForGroupe.revert();
		setEditStructure(null);
		//initialiseSelectedAdresse();
		setIsResponsableEditing(Boolean.FALSE);
		setIsEditing(Boolean.FALSE);
		return null;
	}

	public WOActionResults onResponsableEnregistrer() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		try {
			if (edForGroupe != null) {

				setEditStructure(selectedStructureForResponsable());
				if (getEditStructure() == null) {
					throw new NSValidation.ValidationException("Veuillez choisir une structure.");
				}
				//verifier que l'utilisateur a le droit de selectionner le groupe
				if (!getAppUserForAnnuaire().hasDroitGererGroupe(getEditStructure())) {
					throw new NSValidation.ValidationException("Vous n'avez pas les droit de gérer ce groupe");
				}

				getEditStructure().setPersIdModification(getUtilisateurPersId());
				getEditSecretariat().setDModification(new NSTimestamp());
				if (editRepartStructure.toStructureGroupe() == null) {
					throw new NSValidation.ValidationException("Veuillez choisir un groupe.");
				}
				//verifier que l'utilisateur a le droit de selectionner le groupe
				if (!getAppUserForAnnuaire().hasDroitGererGroupe(editRepartStructure.toStructureGroupe())) {
					throw new NSValidation.ValidationException("Vous n'avez pas les droit de gerer ce groupe");
				}
				editRepartStructure.toStructureGroupe().setPersIdModification(getUtilisateurPersId());
				editRepartStructure.toPersonneElt().setPersIdModification(getUtilisateurPersId());
				//nettoyer les repartAssociation vides
				editRepartStructure.nettoieRepartAssociationVides();

				//				if (editRepartStructure.toRepartAssociation()!=null && editRepartStructure.toRepartAssociation().toAssociation() == null) {
				//					edForGroupe.deleteObject(editRepartStructure.toRepartAssociation());
				//					editRepartStructure.setToRepartAssociationRelationship(null);
				//				}

				// push de l'adresse dans l'ec parent
				edForGroupe.saveChanges();
			}
			setSelectedRepartStructure(editRepartStructure);
			setIsResponsableEditing(Boolean.FALSE);
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		setValueForBinding(Boolean.TRUE, BINDING_wantRefresh);
		return null;
	}

	public WOActionResults onResponsableModifier() {
		if (selectedStructureForResponsable() == null) {
			return null;
		}
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		setEditStructure(selectedStructureForResponsable());
		getEditStructure().setPersIdModification(getUtilisateurPersId());
		if (editRepartStructure.toRepartAssociations().count() == 0) {
			EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edForGroupe);
			repartAssociation.setPersIdCreation(getUtilisateurPersId());
			repartAssociation.setPersIdModification(getUtilisateurPersId());
			editRepartStructure.addToToRepartAssociationsRelationship(repartAssociation);
		}
		setIsResponsableEditing(Boolean.TRUE);
		setIsEditing(Boolean.TRUE);
		return null;
	}

	public WOActionResults onValiderModifierResponsable() {
		// Alerter si on choisit l'individu que l'on veut remplacer ;)
		if (getNouveauResponsable().globalID().equals(selectedStructureForResponsable().toResponsable().globalID())) {
			mySession().addSimpleErrorMessage("Attention", "L'individu sélectionné est le même que celui que vous souhaitez remplacer comme responsable");
			return doNothing();
		}
		selectedStructureForResponsable().setToResponsableRelationship(getNouveauResponsable());
		try {
			edc().saveChanges();
			mySession().addSimpleInfoMessage("Information", getNouveauResponsable().getNomPrenomAffichage()
					+ " est le nouveau secrétaire du groupe "
					+ selectedStructureForResponsable().llStructure());
			CktlAjaxWindow.close(context(), modifierResponsableWindowId());
			AjaxUpdateContainer.updateContainerWithID(getMainContainerId(), context());
			getLesResponsablesGroupes().remove(selectedStructureForResponsable());
			setSelectedStructureForResponsable(null);
		} catch (ValidationException ve) {
			mySession().addSimpleErrorMessage("Erreur", ve);
			edc().revert();
		}
		return doNothing();
	}

	public WOActionResults onAnnulerModifierResponsable() {
		CktlAjaxWindow.close(context(), modifierResponsableWindowId());
		return doNothing();
	}

	public WOActionResults onResponsableSupprimer() {
		AnnuaireLogService annuaireService = new AnnuaireLogService();
		EOEditingContext edcForAnnuaire = new EOEditingContext();
		// On teste si le groupe est un service
		if (EOStructureForServiceSpec.sharedInstance().isSpecificite(selectedStructureForResponsable())) {
			mySession().addSimpleErrorMessage("Attention", "Vous ne pouvez pas supprimer le responsable d'un service. Vous devez "
					+ "obligatoirement le remplacer");
			return doNothing();
		}
		// Suppression du responsable
		getLesResponsablesGroupes().remove(selectedStructureForResponsable());
		String confirmMessage = getPersonne().persLibelleAffichage()
				+ " n'est plus le responsable du groupe "
				+ selectedStructureForResponsable().llStructure();
		selectedStructureForResponsable().setToResponsableRelationship(null);
		try {
			String message = confirmMessage;
			String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
			annuaireService.creationUnAnnuaireLog(edcForAnnuaire, message, nomApplication);
			
			edc().saveChanges();
			edcForAnnuaire.saveChanges();
			
			mySession().addSimpleInfoMessage("Information", confirmMessage);
			AjaxUpdateContainer.updateContainerWithID(getMainContainerId(), context());
			getLesResponsablesGroupes().remove(selectedStructureForResponsable());
			setSelectedStructureForResponsable(null);
		} catch (ValidationException ve) {
			mySession().addSimpleErrorMessage("Erreur", ve);
			edc().revert();
		}
		return doNothing();
	}

	/**
	 * Traitement pour l'ajout d'un secretaire.
	 *
	 * @param secr
	 * @throws Exception
	 */
	public WOActionResults onSecretariatAjouter(EOStructure currentStructure, EOIndividu secr) throws Exception {
		@SuppressWarnings("rawtypes")
		NSArray res = currentStructure.toSecretariats(new EOKeyValueQualifier(EOSecretariat.NO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, secr.noIndividu()));
		if (res.count() > 0) {
			throw new NSValidation.ValidationException(secr.getNomPrenomAffichage() + " est deja secretaire du groupe " + currentStructure.getNomPrenomAffichage());
		}

		EOSecretariat secretariat = EOSecretariat.creerInstance(edc());
		secretariat.setToIndividuRelationship(secr);
		currentStructure.addToToSecretariatsRelationship(secretariat);
		secretariat.setCStructure(currentStructure.cStructure());

		return null;

	}

	public WOActionResults onSecretaireAnnuler() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		edForGroupe.revert();
		setEditSecretariat(null);
		//initialiseSelectedAdresse();
		setIsSecretaireEditing(Boolean.FALSE);
		setIsEditing(Boolean.FALSE);
		return null;
	}

	public WOActionResults onSecretaireModifier() {
		if (selectedSecretariat() == null) {
			return null;
		}
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		setEditSecretariat(selectedSecretariat());
		//		onSecretariatAjouter(getEditSecretariat().toStructure(), newIndividu);
		//		getEditStructure().setPersIdModification(getUtilisateurPersId());
		//		if (editRepartStructure.toRepartAssociations().count() == 0) {
		//			EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edForGroupe);
		//			repartAssociation.setPersIdCreation(getUtilisateurPersId());
		//			repartAssociation.setPersIdModification(getUtilisateurPersId());
		//			editRepartStructure.addToToRepartAssociationsRelationship(repartAssociation);
		//		}
		//		setIsEditing(Boolean.TRUE);
		return null;
	}

	public WOActionResults onSecretaireSupprimer() {
		AnnuaireLogService annuaireService = new AnnuaireLogService();
		EOEditingContext edcForAnnuaire = new EOEditingContext();
		// Suppression du secretariat
		String confirmMessage = selectedSecretariat().toIndividu().getNomPrenomAffichage()
				+ " n'est plus le secrétaire du groupe "
				+ selectedSecretariat().toStructure().llStructure();
		selectedSecretariat().setToIndividuRelationship(null);
		selectedSecretariat().setToStructureRelationship(null);
		edc().deleteObject(selectedSecretariat());
		try {
			String message = confirmMessage;
			String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
			annuaireService.creationUnAnnuaireLog(edcForAnnuaire, message, nomApplication);
			
			edc().saveChanges();
			edcForAnnuaire.saveChanges();
			
			mySession().addSimpleInfoMessage("Information", confirmMessage);
			AjaxUpdateContainer.updateContainerWithID(getMainContainerId(), context());
			getLesSecretariats().remove(selectedSecretariat());
			setSelectedSecretariat(null);
		} catch (ValidationException ve) {
			mySession().addSimpleErrorMessage("Erreur", ve);
			edc().revert();
		}
		return doNothing();

	}

	public WOActionResults onValiderModifierSecretaire() {
		// Alerter si on choisit l'individu que l'on veut remplacer ;)
		if (getNouveauSecretaire().globalID().equals(selectedSecretariat().toIndividu().globalID())) {
			mySession().addSimpleErrorMessage("Attention", "L'individu sélectionné est le même que celui que vous souhaitez remplacer comme secrétaire");
			return doNothing();
		}
		// Creation du nouvau secretariat
		EOSecretariat _nouveauSecretariat = EOSecretariat.creerInstance(edc());
		_nouveauSecretariat.setCStructure(selectedSecretariat().toStructure().cStructure());
		_nouveauSecretariat.setToStructureRelationship(selectedSecretariat().toStructure());
		_nouveauSecretariat.setToIndividuRelationship(getNouveauSecretaire());
		_nouveauSecretariat.setDCreation(ERXTimestampUtilities.today());
		// Suppression de l'ancien secretariat
		selectedSecretariat().setToIndividuRelationship(null);
		selectedSecretariat().setToStructureRelationship(null);
		edc().deleteObject(selectedSecretariat());
		try {
			edc().saveChanges();
			mySession().addSimpleInfoMessage("Information", _nouveauSecretariat.toIndividu().getNomPrenomAffichage()
					+ " est le nouveau secrétaire du groupe "
					+ _nouveauSecretariat.toStructure().llStructure());
			CktlAjaxWindow.close(context(), modifierSecretaireWindowId());
			AjaxUpdateContainer.updateContainerWithID(getMainContainerId(), context());
			getLesSecretariats().remove(selectedSecretariat());
			setSelectedSecretariat(null);
		} catch (ValidationException ve) {
			mySession().addSimpleErrorMessage("Erreur", ve);
			edc().revert();
		}
		return doNothing();
	}

	public WOActionResults onAnnulerModifierSecretaire() {
		CktlAjaxWindow.close(context(), modifierSecretaireWindowId());
		return doNothing();
	}

	public WOActionResults onSecretaireEnregistrer() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		try {
			if (edForGroupe != null) {
				setEditSecretariat(selectedSecretariat());
				if (getEditSecretariat() == null) {
					throw new NSValidation.ValidationException("Veuillez choisir un secrétariat.");
				}
				//verifier que l'utilisateur a le droit de selectionner le groupe
				if (!getAppUserForAnnuaire().hasDroitGererGroupe(getEditSecretariat().toStructure())) {
					throw new NSValidation.ValidationException("Vous n'avez pas les droit de gérer ce groupe");
				}
				getEditSecretariat().toStructure().setPersIdModification(getUtilisateurPersId());
				getEditSecretariat().setDModification(new NSTimestamp());

				edForGroupe.saveChanges();
			}
			//			setSelectedRepartStructure(editRepartStructure);
			setIsSecretaireEditing(Boolean.FALSE);
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		setValueForBinding(Boolean.TRUE, BINDING_wantRefresh);
		return null;
	}

	public Boolean isAffectationCreerEnabled() {
		//verifier que l'utilisateur a le droit de creer une nouvelle affectation
		//		return Boolean.TRUE;
		return getAppUserForAnnuaire().hasDroitGererAuMoinsUnGroupe();
	}

	public Boolean isAffectationModifierEnabled() {
		//verifier que l'utilisateur a le droit de selectionner le groupe
		return Boolean.valueOf(getSelectedRepartStructure() != null
				&& getSelectedRepartStructure().toStructureGroupe() != null
				&& groupeExclus().indexOf(getSelectedRepartStructure().toStructureGroupe()) == NSArray.NotFound
				&& getAppUserForAnnuaire().hasDroitGererGroupe(getSelectedRepartStructure().toStructureGroupe()));
	}

	public Boolean isAffectationSupprimerEnabled() {
		return Boolean.valueOf(getSelectedRepartStructure() != null
				&& getSelectedRepartStructure().toStructureGroupe() != null
				&& groupeExclus().indexOf(getSelectedRepartStructure().toStructureGroupe()) == NSArray.NotFound
				&& getAppUserForAnnuaire().hasDroitGererGroupe(getSelectedRepartStructure().toStructureGroupe()));
	}

	public boolean isResponsableModifierEnabled() {
		return selectedStructureForResponsable() != null && getAppUserForAnnuaire().hasDroitGererGroupe(selectedStructureForResponsable());
	}

	public boolean isSecretaireModifierEnabled() {
		return selectedSecretariat() != null && getAppUserForAnnuaire().hasDroitGererGroupe(selectedSecretariat().toStructure());
	}

	@SuppressWarnings("rawtypes")
	public NSArray groupeExclus() {
		if (groupeExclus == null) {
			//			groupeExclus = EOStructureForGroupeSpec.getGroupesFournisseurs(edc());
			groupeExclus = EOStructureForGroupeSpec.getGroupesReserves(edc());
		}
		return groupeExclus;
	}

	public EORepartStructure getSelectedRepartStructure() {
		if (hasBinding(BINDING_selectedRepartStructure)) {
			EORepartStructure rpa = (EORepartStructure) valueForBinding(BINDING_selectedRepartStructure);
			if (rpa != null && !edc().equals(rpa.editingContext())) {
				rpa = rpa.localInstanceIn(edc());
			}
			selectedRepartStructure = rpa;
		}
		return selectedRepartStructure;
	}

	public void setSelectedRepartStructure(EORepartStructure rp) {
		this.selectedRepartStructure = rp;
		if (canSetValueForBinding(BINDING_selectedRepartStructure)) {
			setValueForBinding(rp, BINDING_selectedRepartStructure);
		}
	}

	public EOQualifier getQualifierForGroupes() {
		EOQualifier qual = (EOQualifier) valueForBinding(BINDING_qualifierForGroupes);
		if (qual == null) {
			qual = qualifierDefaultForGroupes;
		}
		return qual;
	}

	public String groupeViewContainerId() {
		return getComponentId() + "_GroupeViewContainer";
	}

	public Boolean ifRoleDefini() {
		return Boolean.valueOf(getSelectedRepartStructure() != null && getSelectedRepartStructure().toRepartAssociations() != null && getSelectedRepartStructure().toRepartAssociations().count() > 0);
	}

	public String getPeriode() {
		String s = "";
		//FIXME pb de decalage (timezone ?)
		if (ifRoleDefini().booleanValue()) {
			if (unRepartAssociation.rasDOuverture() != null) {
				s = "A partir du " + MyDateCtrl.formatDateShort(unRepartAssociation.rasDOuverture());
			}
			if (unRepartAssociation.rasDFermeture() != null) {
				if (s.length() > 0) {
					s += ", ";
				}
				s += " jusqu'au " + MyDateCtrl.formatDateShort(unRepartAssociation.rasDFermeture());
			}
			if (s.length() > 0) {
				s = "(" + s + ")";
			}
		}
		return s;
	}

	public String affectationEnregistrerOnComplete() {
		return "function () {" + getMainContainerId() + "Update();}";
	}

	public String responsableEnregistrerOnComplete() {
		return "function () {" + getMainContainerId() + "Update();}";
	}

	public String secretaireEnregistrerOnComplete() {
		return "function () {" + getMainContainerId() + "Update();}";
	}

	public void resetEdc() {
		_editingContext = null;
	}

	public EOIndividu getNewIndividu() {
		return newIndividu;
	}

	public void setNewIndividu(EOIndividu newIndividu) {
		this.newIndividu = newIndividu;
	}

	public EOStructure uneStructure() {
		return uneStructure;
	}

	public void setUneStructure(EOStructure uneStructure) {
		this.uneStructure = uneStructure;

	}

	public EOStructure selectedStructureForResponsable() {
		return selectedStructureForResponsable;
	}

	public void setSelectedStructureForResponsable(EOStructure selectedStructureForResponsable) {
		this.selectedStructureForResponsable = selectedStructureForResponsable;

	}

	public EOIndividu getNouveauResponsable() {
		return nouveauResponsable;
	}

	public void setNouveauResponsable(EOIndividu nouveauResponsable) {
		this.nouveauResponsable = nouveauResponsable;
	}

	public Boolean hasNotSelectedNouveauResponsable() {
		return getNouveauResponsable() == null;
	}

	public EOSecretariat selectedSecretariat() {
		return selectedSecretariat;
	}

	public void setSelectedSecretariat(EOSecretariat selectedSecretariat) {
		this.selectedSecretariat = selectedSecretariat;

	}

	public EOIndividu getNouveauSecretaire() {
		return nouveauSecretaire;
	}

	public void setNouveauSecretaire(EOIndividu nouveauSecretaire) {
		this.nouveauSecretaire = nouveauSecretaire;
	}

	public Boolean hasNotSelectedNouveauSecretaire() {
		return getNouveauSecretaire() == null;
	}

	public EOStructure getEditStructure() {
		return editStructure;
	}

	public void setEditStructure(EOStructure editStructure) {
		this.editStructure = editStructure;
	}

	public EORepartStructure getUnRepartStructure() {
		return unRepartStructure;
	}

	public void setUnRepartStructure(EORepartStructure unRepartStructure) {
		this.unRepartStructure = unRepartStructure;
	}
	public EOSecretariat getUnSecretariat() {
		return unSecretariat;
	}

	public void setUnSecretariat(EOSecretariat unSecretariat) {
		this.unSecretariat = unSecretariat;
	}

	public EOSecretariat getEditSecretariat() {
		return editSecretariat;
	}

	public void setEditSecretariat(EOSecretariat editSecretariat) {
		this.editSecretariat = editSecretariat;
	}

	/**
	 * @return the personneSecretariatIndex
	 */
	public int getPersonneSecretariatIndex() {
		return personneSecretariatIndex;
	}

	/**
	 * @param personneSecretariatIndex the personneSecretariatIndex to set
	 */
	public void setPersonneSecretariatIndex(int personneSecretariatIndex) {
		this.personneSecretariatIndex = personneSecretariatIndex;
	}

	/**
	 * @return the personneResponsableIndex
	 */
	public int getPersonneResponsableIndex() {
		return personneResponsableIndex;
	}

	/**
	 * @param personneResponsableIndex the personneResponsableIndex to set
	 */
	public void setPersonneResponsableIndex(int personneResponsableIndex) {
		this.personneResponsableIndex = personneResponsableIndex;
	}

	public Boolean isAffectationEditing() {
		return isAffectationEditing;
	}

	public void setIsAffectationEditing(Boolean isAffectationEditing) {
		this.isAffectationEditing = isAffectationEditing;
	}

	public Boolean isResponsableEditing() {
		return isResponsableEditing;
	}

	public void setIsResponsableEditing(Boolean isResponsableEditing) {
		this.isResponsableEditing = isResponsableEditing;
	}

	public Boolean isSecretaireEditing() {
		return isSecretaireEditing;
	}

	public void setIsSecretaireEditing(Boolean isSecretaireEditing) {
		this.isSecretaireEditing = isSecretaireEditing;
	}


	@SuppressWarnings("unchecked")
	public NSArray<EOStructure> getLesResponsablesGroupes() {
		if (lesResponsablesGroupes == null) {
			lesResponsablesGroupes = this.getPersonne().getPersonneDelegate().getGroupesResponsable();
		}
		return lesResponsablesGroupes;
	}

	public NSArray<EOSecretariat> getLesSecretariats() {
		if (lesSecretariats == null) {
			EOIndividu individu = (EOIndividu) getPersonne();
			lesSecretariats = EOSecretariat.rechercherSecretariatsPourPersonne(edc(), individu);
		}

		return lesSecretariats;
	}

	public boolean isIndividu() {
		return this.getPersonne().isIndividu();
	}

	public Boolean getResetPersonneGroupesForm() {
		return resetPersonneGroupesForm;
	}

	public void setResetPersonneGroupesForm(Boolean resetPersonneGroupesForm) {
		this.resetPersonneGroupesForm = resetPersonneGroupesForm;
	}

	public String unGroupeLabel() {
		String label = new String();
		@SuppressWarnings("unchecked")
		NSArray<String> listeAffectation = (NSArray<String>) uneStructure().toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);

		if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_G)) {
			label = "Groupe général";
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_U)) {
				label = "Groupe Unix";
			}
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_F)) {
				label = "Forum";
			}
		}
		return label;
	}

	public String unGroupeType() {
		String typeGroupe = new String();
		@SuppressWarnings("unchecked")
		NSArray<String> listeAffectation = (NSArray<String>) uneStructure().toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);

		if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_G)) {
			typeGroupe = "GG";
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_U)) {
				typeGroupe = "GU";
			}
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_F)) {
				typeGroupe = "GF";
			}
		}

		return typeGroupe;
	}

	@SuppressWarnings("unchecked")
	public String unSecretariatGroupeLabel() {
		String label = "Groupe général";
		NSArray<String> listeAffectation = (NSArray<String>) getUnSecretariat().toStructure().toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);

		if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_G)) {
			label = "Groupe général";
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_U)) {
				label = "Groupe Unix";
			}
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_F)) {
				label = "Forum";
			}
		}

		return label;
	}


	@SuppressWarnings("unchecked")
	public String unSecretariatGroupeType() {
		String typeGroupe = "GG";
		NSArray<String> listeAffectation = (NSArray<String>) getUnSecretariat().toStructure().toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);

		if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_G)) {
			typeGroupe = "GG";
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_U)) {
				typeGroupe = "GU";
			}
			if (listeAffectation.contains(EOTypeGroupe.TGRP_CODE_F)) {
				typeGroupe = "GF";
			}
		}
		return typeGroupe;
	}


	@SuppressWarnings("unchecked")
	public String affectationGroupeLabel() {
		String label = "Groupe général";
		NSArray<String> listeAffectation = (NSArray<String>) unRepartStructure.toStructureGroupe().toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);

		EOQualifier rsQualForum = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_F);
		NSArray<EORepartStructure> rsForums = ERXQ.filtered(lesRepartStructures, rsQualForum);
		EOQualifier rsQualSystem = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_U);
        NSArray<EORepartStructure> rsSystem = ERXQ.filtered(lesRepartStructures, rsQualSystem);
        NSArray<EORepartStructure> rsGeneraux = ERXArrayUtilities.arrayMinusArray(lesRepartStructures, rsSystem);
        rsGeneraux = ERXArrayUtilities.arrayMinusArray(rsGeneraux, rsForums);

        NSArray<EORepartStructure> finalListeAffectation = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(rsGeneraux, rsSystem);
        finalListeAffectation = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(finalListeAffectation, rsForums);


		if (finalListeAffectation.contains(EOTypeGroupe.TGRP_CODE_G)) {
			label = "Groupe général";
			if (finalListeAffectation.contains(EOTypeGroupe.TGRP_CODE_U)) {
				label = "Groupe Unix";
			}
			if (finalListeAffectation.contains(EOTypeGroupe.TGRP_CODE_F)) {
				label = "Forum";
			}
		}
		return label;
	}

	@SuppressWarnings("unchecked")
	public String affectationGroupeType() {
		String typeGroupe = new String();
		NSArray<String> listeAffectation = (NSArray<String>) unRepartStructure.toStructureGroupe().toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);

		EOQualifier rsQualForum = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_F);
		NSArray<EORepartStructure> rsForums = ERXQ.filtered(lesRepartStructures, rsQualForum);
		EOQualifier rsQualSystem = ERXQ.is(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_U);
        NSArray<EORepartStructure> rsSystem = ERXQ.filtered(lesRepartStructures, rsQualSystem);
        NSArray<EORepartStructure> rsGeneraux = ERXArrayUtilities.arrayMinusArray(lesRepartStructures, rsSystem);
        rsGeneraux = ERXArrayUtilities.arrayMinusArray(rsGeneraux, rsForums);

        NSArray<EORepartStructure> finalListeAffectation = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(rsGeneraux, rsSystem);
        finalListeAffectation = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(finalListeAffectation, rsForums);


		if (finalListeAffectation.contains(EOTypeGroupe.TGRP_CODE_G)) {
			typeGroupe = "GG";
			if (finalListeAffectation.contains(EOTypeGroupe.TGRP_CODE_U)) {
				typeGroupe = "GU";
			}
			if (finalListeAffectation.contains(EOTypeGroupe.TGRP_CODE_F)) {
				typeGroupe = "GF";
			}
		}
		return typeGroupe;
	}

	public NSArray<EORepartStructure> getSelectedRepartStructures() {
	    return selectedRepartStructures;
    }

	public void setSelectedRepartStructures(NSArray<EORepartStructure> selectedRepartStructures) {
	    this.selectedRepartStructures = selectedRepartStructures;
    }
}
