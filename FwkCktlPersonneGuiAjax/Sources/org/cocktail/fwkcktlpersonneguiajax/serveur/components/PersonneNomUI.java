/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxUserQuestionDelegate;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjax;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXStringUtilities;

/**
 * Afichage et modification éventuelle du nom d'une personne.
 * 
 * @author rprin
 * @binding editingContext
 * @binding isEditing Boolean Obligatoire.
 * @binding isReadOnly
 * @binding useMinWidth Facultatif, boolean pour minimiser la largeur occupée par le composant. false par défaut.
 * @binding showCivilite Facultatif, boolean pour afficher l'édition de la civilité si c'est un individu. 
 *          (false par défaut)
 * @binding personne Obligatoire.
 * @binding utilisateurPersId Boolean Obligatoire.
 */
public class PersonneNomUI extends AComponent {
	private static final long serialVersionUID = -1085270815779643325L;

	public static final String BINDING_personne = "personne";
	public static final String BINDING_USE_MIN_WIDTH = "useMinWidth";


	private IPersonne _personne;
	private IPersonneNomUIMdl mdl;
	private final CktlAjaxUserQuestionDelegate userQuestionDelegate;

	private String _nomCache;
	private String _prenomCache;
	private String _nomInitial;
	private String _prenomInitial;
	
	private String nomFamille;
	private String prenomSecond;
	
    private EOCivilite currentCivilite;
    private NSArray<EOCivilite> civilites;

	public PersonneNomUI(WOContext context) {
		super(context);
		userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
	}

	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {

			//La personne a changé

			_personne = pers.localInstanceIn(edc());
			if (Boolean.valueOf(_personne.isIndividu())) {
				_nomInitial = ((EOIndividu) _personne).nomAffichage();
				nomFamille = ((EOIndividu) _personne).nomPatronymique();
				_prenomInitial = ((EOIndividu) _personne).prenomAffichage();
				prenomSecond = ((EOIndividu) _personne).prenom2();
				mdl = new IndividuNomMdl();
			} else {
				_nomInitial = ((EOStructure) _personne).strAffichage();
				_prenomInitial = null;
				mdl = new StructureNomMdl();
			}

			setIsEditing(Boolean.FALSE);
			_nomCache = null;
			_prenomCache = null;

		}
		return _personne;
	}
	
	public String individuCivilite() {
		return ((EOIndividu) getPersonne()).getCivilite().getCode();
	}

	public String width() {
	    boolean useMin = booleanValueForBinding(BINDING_USE_MIN_WIDTH, false);
	    if (!useMin) {
			return "100%";
		} else {
			return "auto";
		}
	}
	
	public Boolean isIndividu() {
		return Boolean.valueOf(getPersonne().isIndividu());
	}

	public boolean hasLc() {
	    if (!isIndividu()) {
	        EOStructure str = (EOStructure) getPersonne();
	        return !ERXStringUtilities.stringIsNullOrEmpty(str.lcStructure());
	    }
	    return false;
	}
	
	public String getNom() {
		if (!isEditing().booleanValue()) {
			return mdl().getNom();
		} else {
			return _nomCache;
		}
	}

	public String getPrenom() {
		if (!isEditing().booleanValue()) {
			return mdl().getPrenom();
		} else {
			return _prenomCache;
		}
	}

	public void setNom(String nom) {
		_nomCache = nom;
	}

	public void setPrenom(String prenom) {
		_prenomCache = prenom;
	}

	public IPersonneNomUIMdl mdl() {
		getPersonne();
		return mdl;
	}
	

	public String getNomFamille() {
		return nomFamille;
	}

	public void setNomFamille(String nomFamille) {
		this.nomFamille = nomFamille;
	}
	
	public String getNomUsage() {
		return ((EOIndividu) getPersonne()).nomUsuel();
	}

	public void setNomUsage(String nomUsage) {
		((EOIndividu) getPersonne()).setNomUsuel(nomUsage);
	}
	
	public String getPrenomIndividu() {
		return ((EOIndividu) getPersonne()).prenom();
	}

	public void setPrenomIndividu(String prenomIndividu) {
		((EOIndividu) getPersonne()).setPrenom(prenomIndividu);
	}

	public String getPrenomSecond() {
		return prenomSecond;
	}

	public void setPrenomSecond(String prenomSecond) {
		this.prenomSecond = prenomSecond;
	}


	public interface IPersonneNomUIMdl {
		public String getNom();

		public void setNom(String nom);

		public String getPrenom();

		public void setPrenom(String prenom);

	}

	public class StructureNomMdl implements IPersonneNomUIMdl {

		public String getNom() {
			return ((EOStructure) getPersonne()).strAffichage();
		}

		public void setNom(String nom) {
			((EOStructure) getPersonne()).setStrAffichage(nom);
		}

		public String getPrenom() {
			return null;
		}

		public void setPrenom(String prenom) {
			return;
		}

	}

	public class IndividuNomMdl implements IPersonneNomUIMdl {

		public String getNom() {
			return ((EOIndividu) getPersonne()).nomAffichage();
		}

		public void setNom(String nom) {
			((EOIndividu) getPersonne()).setNomAffichage(nom);
		}

		public String getPrenom() {
			return ((EOIndividu) getPersonne()).prenomAffichage();
		}

		public void setPrenom(String prenom) {
			((EOIndividu) getPersonne()).setPrenomAffichage(prenom);
		}
		
	}

	public Boolean isModificationAvailable() {
//		if ("O".equals(FwkCktlPersonneGuiAjax.paramManager.getParam(FwkCktlPersonneGuiAjaxParamManager.PERSONNE_NOM_READONLY_ACTIVE))) {
//			return Boolean.valueOf(getAppUserForAnnuaire().hasDroitGrhumCreateur());
//		}
		if (FwkCktlPersonneGuiAjax.paramManager.isCodeActivationActif(FwkCktlPersonneGuiAjaxParamManager.PERSONNE_NOM_READONLY_ACTIVE)) {
			return Boolean.valueOf(getAppUserForAnnuaire().hasDroitGrhumCreateur());
		}
		return Boolean.valueOf(!isReadOnly().booleanValue() && getAppUserForAnnuaire().hasDroitModificationIPersonne(getPersonne()));
	}

	public WOActionResults save() {

		boolean doIt = false;
		String nom = _nomCache;
		String prenom = _prenomCache;
		NSArray res = NSArray.EmptyArray;

		try {
			if (MyStringCtrl.isEmpty(nom)) {
				throw new NSValidation.ValidationException("Le nom est obligatoire");
			}
			if (isIndividu()) {
				if (MyStringCtrl.isEmpty(prenom)) {
					throw new NSValidation.ValidationException("Le prénom est obligatoire");
				}
			}

			if (isIndividu()) {
				res = EOIndividu.individusWithNameEqualsAndFirstNameEquals(edc(), nom, prenom, 1);
			} else {
				res = EOStructure.structuresByNameAndSiret(edc(), nom, null, null, 1);
			}
			if (res.count() > 0 && !((IPersonne) res.objectAtIndex(0)).globalID().equals(getPersonne().globalID())) {
				doIt = false;
				//FIXME PB avec trigger homonymie
				IPersonne doublon = (IPersonne) res.objectAtIndex(0);

				String s = "Une personne avec le même nom existe deja : " + doublon.getNomPrenomAffichage() + " (" + doublon.getNumero() + "). Confirmez-vous l'ajout de cette nouvelle personne ?";
				
				Boolean confirm = userQuestionDelegate.askUserAsBoolean("question1", s, getComponentId());
				
				if (confirm == null) {
					return null;
				} else {
					doIt = confirm.booleanValue();
				}
			} else {
				doIt = true;
			}

			if (doIt) {
				mdl().setNom(nom);
				mdl().setPrenom(prenom);
				if (isIndividu()) {
					((EOIndividu) _personne).setNomAffichage(nom);
					((EOIndividu) _personne).setPrenomAffichage(prenom);
//					((EOIndividu) _personne).setNomPatronymique(getNomFamille());
					
					if (getNomFamille() != null) {
						((EOIndividu) _personne)
								.setNomPatronymiqueAffichage(getNomFamille());
					} else {
						((EOIndividu) _personne)
						.setNomPatronymiqueAffichage(nom);
					}
					((EOIndividu) _personne).setPrenom2(getPrenomSecond());
				}
				userQuestionDelegate.clearAnswers();
				setIsEditing(Boolean.FALSE);
			} else {
				userQuestionDelegate.clearAnswers();
				//context().response().setStatus(500);
			}

		} catch (NSValidation.ValidationException e) {
			setErreurSaisieMessage(e.getMessage());
			setIsEditing(Boolean.TRUE);
		}

		return null;

	}

	public WOActionResults cancel() {
		setIsEditing(Boolean.FALSE);
		_nomCache = null;
		_prenomCache = null;
		return null;
	}

	public WOActionResults edit() {
		if (isModificationAvailable()) {
			setIsEditing(Boolean.TRUE);
			_nomCache = mdl().getNom();
			_prenomCache = mdl.getPrenom();
		}
		return null;
	}

	@Override
	public void setIsEditing(Boolean value) {
		super.setIsEditing(value);
	}

    public NSArray civilites() {
        if (civilites == null) {
            civilites = EOCivilite.fetchAll(edc());
        }
        return civilites;
    }
	
    public EOCivilite getCurrentCivilite() {
        return currentCivilite;
    }
    
    public void setCurrentCivilite(EOCivilite currentCivilite) {
        this.currentCivilite = currentCivilite;
    }
    
    public boolean isMan(){
    	if ("MME".equals(((EOIndividu) getPersonne()).toCivilite().cCivilite()) || "MLLE".equals(((EOIndividu) getPersonne()).toCivilite().cCivilite())) {
    		return false;
    	}
    	return true;
    }
    
    public boolean isNomIdentique(){
    	if ((getNom() != null && getNomFamille() != null) && (getNom().equalsIgnoreCase(getNomFamille()))) {
    		return true;
    	}
    	if ((((EOIndividu) getPersonne()).nomPatronymique() != null
    			&& ((EOIndividu) getPersonne()).nomUsuel() != null)
    			&& (((EOIndividu) getPersonne()).nomPatronymique().equals(((EOIndividu) getPersonne()).nomUsuel()))) {
    		return true;
    	}
    	return false;
    }
	
}