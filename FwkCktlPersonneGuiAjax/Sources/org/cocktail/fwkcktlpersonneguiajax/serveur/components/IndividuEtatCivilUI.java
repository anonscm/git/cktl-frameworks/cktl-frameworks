/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.validation.ERXValidationException;

/**
 * Composant d'édition de la date de naissance, de la ville de naissance, du 
 * pays de naissance.
 * Attention ce composant ne se base pas sur le n° INSEE mais directement sur
 * les attributs villeNaissance, dNaissance...Il est donc destiné
 * principalement à l'édition d'individu sans n°INSEE. (invités...)
 * 
 * Limitation : pour l'instant le champ département de naissance étant libre, il n'est pas renseigné pour éviter
 * les problèmes de cohérences de données.
 * 
 * @binding individu l'individu dont on veut éditer l'état civil
 * @binding editingContext l'ec parent
 * @binding isEditing true si en mode edition
 * @binding isReadOnly true si en mode read only
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class IndividuEtatCivilUI extends AComponent {

    private static final long serialVersionUID = 4505973647605775812L;
    private static final Logger LOG = Logger.getLogger(IndividuEtatCivilUI.class);
    public static final String BINDING_INDIVIDU = "individu";
    private EOEditingContext editingContext;
    private EOIndividu individu;
    private String message;
    private String containerId;
    private NSArray<EOPays> allPays;
    private NSArray<EODepartement> allDeps;
    private EOPays currentPays;
    private EODepartement currentDep;
    
    private CktlAjaxSelect2RemoteDataProvider paysNationaliteProvider;
    
    public IndividuEtatCivilUI(WOContext context) {
        super(context);
    }

    @Override
    public void sleep() {
        setMessage(null);
        super.sleep();
    }

    private void resetState() {
        this.allPays = null;
        this.allDeps = null;
    }
    
    /**
     * Capture d'une eventuelle erreur de formattage de la date de naissance...
     * 
     * @see com.webobjects.appserver.WOComponent#validationFailedWithException(java.lang.Throwable,
     *      java.lang.Object, java.lang.String)
     */
    @Override
    public void validationFailedWithException(Throwable exception,
            Object value, String keyPath) {
        if (exception instanceof ERXValidationException) {
            ERXValidationException validationException = (ERXValidationException) exception;
            message = validationException.getMessage();
        }
        super.validationFailedWithException(exception, value, keyPath);
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    public void save() {
        try {
        	if (editingContext().hasChanges()) {
        		editingContext().saveChanges();
			}
        } catch (NSValidation.ValidationException e) {
            message = e.getMessage();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new NSForwardException(e);
        }
    }
    
    public EOIndividu individu() {
        EOIndividu indivTmp = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
        if (individuHasChanged()) {
            individu = indivTmp.localInstanceIn(editingContext());
            this.resetState();
        }
        return individu;
    }

    public WOActionResults trouverPays() {
        if (getSelectedCommune() != null) {
            EOPays defaut = EOPays.getPaysDefaut(editingContext());
            setSelectedPays(defaut);
        } 
        return null;
    }
    
    private boolean individuHasChanged() {
        EOIndividu indivTmp = (EOIndividu) valueForBinding(BINDING_INDIVIDU);
        return individu == null || !individu.globalID().equals(indivTmp.globalID());
    }
    
    public EOEditingContext editingContext() {
        if (!hasBinding(BINDING_editingContext)) {
            throw new IllegalArgumentException("Le binding "
                    + BINDING_editingContext + " est obligatoire");
        }    
        if (editingContext == null || individuHasChanged()) {
            editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
        }
        return editingContext;
    }

    public String getContainerId() {
        if (containerId == null) {
        	containerId = ERXWOContext.safeIdentifierName(context(), true);
        }
        return containerId;
    }

    public EOPays getSelectedPays() {
        return individu().toPaysNaissance();
    }

    public void setSelectedPays(EOPays selectedPays) {
        individu().setToPaysNaissanceRelationship(selectedPays);
        if (!selectedPaysIsFrance()) {
        	setSelectedDep(null);
        }
//        save();
    }
    
    public EOPays getSelectedPaysNationalite() {
    	return individu().toPaysNationalite();
    }
    
    public void setSelectedPaysNationalite(EOPays selectedPaysNationalite) {
    	individu().setToPaysNationaliteRelationship(selectedPaysNationalite);
    }

    public boolean isWithDepartement(){
    	if (getSelectedDep() != null && !getSelectedDep().cDepartement().equals(EODepartement.SANS_DPT)) {
    		return true;
    	}
    	return false;
    }
    
    public boolean selectedPaysIsFrance() {
        return getSelectedPays() != null && EOPays.CODE_PAYS_FRANCE.equalsIgnoreCase(getSelectedPays().cPays());
    }
    
    public EODepartement getSelectedDep() {
        return individu().toDepartement();
    }
    
    public void setSelectedDep(EODepartement selectedDep) {
        individu().setToDepartementRelationship(selectedDep);
//        save();
    }
    
    public EOCommune getSelectedCommune() {
        if (individu().villeDeNaissance() != null) {
            return EOCommune.fetchFirstByQualifier(editingContext(),
                    ERXQ.equals(EOCommune.LL_COM_KEY, individu()
                            .villeDeNaissance()));
        } else {
            return null;
        }
    }

    public void setSelectedCommune(EOCommune commune) {
        individu().setVilleDeNaissance(commune.codePostalEtLlComm());
        EODepartement dep = commune.toDepartement();
        if (dep != null) {
        	individu().setToDepartementRelationship(
                    dep.localInstanceIn(editingContext()));
        }
//        save();
    }
	
	public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public NSArray<EOPays> getAllPays() {
    	EOQualifier qualifierTemporel;
    	EOQualifier qualDateFin;
    	final EOQualifier QUAL_DATE_FIN_NULLE = new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
    	
        if ( allPays == null && individu().dNaissance() == null) {
        	
        	qualDateFin = ERXQ.greaterThanOrEqualTo(EOPays.D_FIN_VAL_KEY, DateCtrl.now());
			qualifierTemporel = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					QUAL_DATE_FIN_NULLE, qualDateFin
			}));
        	allPays = EOPays.fetchAll(editingContext(), qualifierTemporel, new NSArray(EOPays.SORT_LL_PAYS));
        }
        
        if (individu().dNaissance() != null) {
        	
        	qualDateFin = ERXQ.greaterThanOrEqualTo(EOPays.D_FIN_VAL_KEY, individu().dNaissance());
			qualifierTemporel = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					QUAL_DATE_FIN_NULLE, qualDateFin
			}));
			
			allPays = EOPays.fetchAll(editingContext(), qualifierTemporel, new NSArray(EOPays.SORT_LL_PAYS));
        	
        }
        return allPays;
    }
    
    public NSArray<EODepartement> getAllDeps() {
        if (allDeps == null) {
        	allDeps = EODepartement.fetchAll(editingContext(), ERXS.asc(EODepartement.C_DEPARTEMENT_KEY).array());
        }
        
        if (individu().dNaissance() != null) {
        	
        	EOQualifier qualifierTemporel;
        	EOQualifier qualDateFin;
        	EOQualifier qualDateDebut;
    		final EOQualifier QUAL_DATE_FIN_NULLE = new EOKeyValueQualifier(EODepartement.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
    		final EOQualifier QUAL_DATE_DEBUT_NOT_NULLE = new EOKeyValueQualifier(EODepartement.D_DEB_VAL_KEY, EOQualifier.QualifierOperatorNotEqual, null);
    		final EOQualifier QUAL_DATE_DEBUT_NULLE = new EOKeyValueQualifier(EODepartement.D_DEB_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
        	
        	qualDateFin = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					QUAL_DATE_FIN_NULLE,
					ERXQ.greaterThanOrEqualTo(EODepartement.D_FIN_VAL_KEY, individu().dNaissance())
			}));
        	
        	qualDateDebut = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
        			QUAL_DATE_DEBUT_NULLE,
        			ERXQ.lessThanOrEqualTo(EODepartement.D_DEB_VAL_KEY, individu().dNaissance())
        	}));
        	
			qualifierTemporel = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					qualDateDebut,
					qualDateFin
			}));
			
			allDeps = EODepartement.fetchAll(editingContext(), qualifierTemporel, ERXS.asc(EODepartement.C_DEPARTEMENT_KEY).array());
        	
        }
        
        
        return allDeps;
    }
    
    public String currentDepAffichage() {
        return getCurrentDep().cDepartement() + "-" + getCurrentDep().llDepartement();
    }
    
    public EOPays getCurrentPays() {
        return currentPays;
    }
    
    public void setCurrentPays(EOPays currentPays) {
        this.currentPays = currentPays;
    }
    
    public EODepartement getCurrentDep() {
        return currentDep;
    }
    
    public void setCurrentDep(EODepartement currentDep) {
        this.currentDep = currentDep;
    }

	public WOActionResults doSave() {
		save();
		return null;
	}
	
}
