/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTVTextFieldCell;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCaStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.StructureCaUICtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;

/**
 * Composant qui gère l'hitorique du chiffre d'affaire d'une structure.
 * 
 * @binding editingContext
 * @binding utilisateurPersId
 * @binding structure
 * @binding ctrl Facultatif. Controleur, doit heriter de StructureCaUICtrl.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class StructureCaUI extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_structure = "structure";
	public static final String BINDING_ctrl = "ctrl";

	public EOCaStructure unRecord;

	private StructureCaUICtrl ctrl;

	private final TbvCaMapper caMapper = new TbvCaMapper();
	private EOStructure sructureCache;
	//private EOStructure sructureEditCache;
	private EOEditingContext _editingContext = null;

	public StructureCaUI(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
				getCtrl().initialiseCaDisplayGroup(structure());
			}
		}
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		//		structure();
		//		if (isReadOnly().booleanValue()) {
		//			initialiseDisplayGroup((EOStructure) valueForBinding(BINDING_structure));
		//		}
		super.appendToResponse(response, context);
	}

	public Boolean hasCas() {
		return Boolean.valueOf(structure() != null && structure().toCaStructures().count() > 0);
	}

	@Override
	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
			//_editingContext = new EOEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));

		}
		return _editingContext;
	}

	private static class TbvCaMapper {
		private static final String IND_KEY = "unRecord.";
		public static final String COL_ANNEE_KEY = EOCaStructure.CAST_ANNEE_KEY;
		public static final String COL_CA_KEY = EOCaStructure.CAST_CA_KEY;

		//		public static final NSMutableDictionary _tbvRepartTypeGroupecolonnesMap = new NSMutableDictionary();
		public static final NSMutableDictionary _colonnesMap = new NSMutableDictionary();
		static {
			CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
			col1.setLibelle("Annee");
			CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_ANNEE_KEY, " ");
			ass1.setNumberformat("0000");
			col1.setAssociations(ass1);
			col1.setComponent(CktlAjaxTVTextFieldCell.class.getSimpleName());
			_colonnesMap.takeValueForKey(col1, COL_ANNEE_KEY);

			CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
			col2.setLibelle("CA");
			CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_CA_KEY, " ");
			ass2.setNumberformat("0.00");
			col2.setAssociations(ass2);
			col2.setComponent(CktlAjaxTVTextFieldCell.class.getSimpleName());
			_colonnesMap.takeValueForKey(col2, COL_CA_KEY);

		}

		public static NSArray DEFAULT_COLONNES_KEYS = new NSArray(new Object[] {
				COL_ANNEE_KEY, COL_CA_KEY
		});

		private NSArray colonnes;

		public NSArray getColonnes() {
			if (colonnes == null) {
				NSMutableArray res = new NSMutableArray();
				NSArray colkeys = getColonnesKeys();
				for (int i = 0; i < colkeys.count(); i++) {
					res.addObject(_colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));
				}
				colonnes = res.immutableClone();
			}
			return colonnes;
		}

		public NSArray getColonnesKeys() {
			NSArray keys = DEFAULT_COLONNES_KEYS;
			return keys;
		}

	}

	public EOStructure structure() {
		boolean hasChanged = false;
		EOStructure tmp = (EOStructure) valueForBinding(BINDING_structure);
		if (tmp == null) {
			sructureCache = tmp;
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
		}
		else {
			if (sructureCache == null || sructureCache.globalID() == null ||
					!sructureCache.globalID().equals(tmp.globalID())) {
				//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
				resetEdc();
				sructureCache = tmp.localInstanceIn(edc());
				hasChanged = true;
			}
		}
		if (hasChanged) {
			//initialiseRepartTypeGroupeDisplayGroup(_groupe);
		}
		return sructureCache;
	}

	//	public EOStructure structure() {
	//		EOStructure struct = (EOStructure) valueForBinding(BINDING_structure);
	//		if (struct != null) {
	//			if (struct.editingContext().equals(edc())) {
	//				return struct;
	//			}
	//			return struct.localInstanceIn(edc());
	//		}
	//		return null;
	//		
	//	}

	public ERXDisplayGroup displayGroup() {
		return getCtrl().caDisplayGroup();
	}

	//	private void initialiseDisplayGroup(EOStructure groupe) {
	//		getCtrl().initialiseCaDisplayGroup(groupe);
	//	}

	public NSArray getTbvCasColonnes() {
		return caMapper.getColonnes();
	}

	public String getTbvCasROId() {
		return getComponentId() + "_TbvCaRO";
	}

	public String getTbvCasEditId() {
		return getComponentId() + "_TbvCaEdit";
	}

	public StructureCaUICtrl getCtrl() {

		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (StructureCaUICtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new StructureCaUICtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}

	public WOActionResults onCreer() {
		try {
			getCtrl().onCreer();
			setIsEditing(Boolean.TRUE);
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onSupprimer() {
		try {
			getCtrl().onSupprimer();
			onEnregistrer();
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public Boolean isCreerEnabled() {
		return Boolean.TRUE;
	}

	public Boolean isModifierEnabled() {
		return Boolean.valueOf(displayGroup() != null && displayGroup().selectedObjects().count() > 0);
	}

	public Boolean isSupprimerEnabled() {
		return Boolean.valueOf(displayGroup() != null && displayGroup().selectedObjects().count() > 0);
	}

	public WOActionResults onAnnuler() {
		//EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		getCtrl().onAnnuler();
		getCtrl().initialiseCaDisplayGroup(structure());
		setIsEditing(Boolean.FALSE);
		return null;
	}

	public WOActionResults onEnregistrer() {
		EOEditingContext edForGroupe = edc();
		setErreurSaisieMessage(null);
		try {
			getCtrl().onEnregistrer();
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		getCtrl().initialiseCaDisplayGroup(structure());
		setIsEditing(Boolean.FALSE);
		//	setValueForBinding(Boolean.TRUE, BINDING_wantRefresh);
		return null;
	}

	public WOActionResults onModifier() {
		if (structure() == null) {
			return null;
		}
		EOEditingContext edForGroupe = edc();
		edForGroupe.revert();
		setIsEditing(Boolean.TRUE);
		return null;
	}

	public void resetEdc() {
		_editingContext = null;
	}

}
