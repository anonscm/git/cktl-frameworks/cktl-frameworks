/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjax;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.MyCRIMailBus;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Affichage des informations concernant un fournisseur.
 * 
 * @binding fournis L'objet EOFournis a afficher
 * @binding showCompteUI Boolean. Indique s'il faut afficher le compte associé au fournisseur
 * @binding showRibUI Boolean. Indique s'il faut afficher le Rib associé au fournisseur
 * @author rprin
 */
public class FournisseurInfoView extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_fournis = "fournis";
	public static final String BINDING_showCompteUI = "showCompteUI";
	public static final String BINDING_showRibUI = "showRibUI";
	public static final Boolean DEFAULT_showRibUI = Boolean.FALSE;
	public static final Boolean DEFAULT_showCompteUI = Boolean.FALSE;
	private EOCompte selectedCompte;

	public FournisseurInfoView(WOContext context) {
		super(context);
	}

	public IPersonne getPersonne() {
		if (fournis() != null) {
			return fournis().toPersonne();
		}
		return null;
	}

	public Boolean isFournisseurEtranger() {
		return Boolean.valueOf((fournis() != null && EOFournis.FOU_ETRANGER_OUI.equals(fournis().fouEtranger())));
	}

	public Boolean isFournisseurValide() {
		return Boolean.valueOf((fournis() != null && EOFournis.FOU_VALIDE_OUI.equals(fournis().fouValide())));
	}

	/**
	 * @return Le compte associe au fournisseur selectionne.
	 */
	public EOCompte getSelectedCompte() {
		if (selectedCompte == null || (fournis() != null && !selectedCompte.toPersonne().equals(fournis().toPersonne()))) {
			selectedCompte = fournis() == null ? null : fournis().toCompte();
		}
		return selectedCompte;
	}

	public void setSelectedCompte(EOCompte compte) {
		selectedCompte = compte;
		//		if (fournis() != null && isEditing()) {
		//			fournis().setToCompteRelationship(compte);
		//		}
	}

	public Boolean ifEnvoiMail() {
		boolean res = true;
		if ( FwkCktlPersonneGuiAjax.paramManager.isCodeActivationActif(FwkCktlPersonneGuiAjaxParamManager.FOURNISSEUR_BT_MAIL_DESACTIVE)){
			res = false;
		} 
		return res;
	}
	
	public Boolean isCompteSelected() {
		Boolean res = Boolean.valueOf(getSelectedCompte() != null);
		return res;
	}

	public Boolean ifCompteHasEmail() {
		Boolean res = Boolean.valueOf(isCompteSelected().booleanValue() && getSelectedCompte().toCompteEmails() != null && getSelectedCompte().toCompteEmails().count() > 0);
		return res;
	}

	/**
	 * Envoie les informations de connexion par email au fournisseur.
	 */
	public WOActionResults onEnvoyerInfosConnexion() {
		try {
			//Verifier que le fournisseur a un compte associe.
			if (fournis().toCompte() != null) {
				if (fournis().toCompte().toCompteEmail() == null || MyStringCtrl.isEmpty(fournis().toCompte().toCompteEmail().getEmailFormatte())) {
					throw new Exception("Aucune adresse email n'a été définie pour le compte.");
				}

				String domain = EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_DOMAINE_PRINCIPAL);
				if (MyStringCtrl.isEmpty(domain)) {
					domain = "univ-lr.fr";
				}
				String from = "noreply@" + domain;
				String to = fournis().toCompte().toCompteEmail().getEmailFormatte();
				String cc = "";
				String subject = "Compte d'accès créé pour " + fournis().getNomComplet();
				//				String body = buildMailInfoConnexion("", fournis().toCompte(), null, commentaires);

				String urlConnexion = null;
				StringBuffer sb = new StringBuffer();
				sb.append("Bonjour, " + "\n");
				sb.append("" + "\n");
				sb.append("Veuillez trouvez ci après vos informations de connexion " + "\n");
				if (urlConnexion != null) {
					sb.append("pour accéder à " + urlConnexion + "\n");
				}
				sb.append("-------------------------------------------------------------------------------" + "\n");
				sb.append("Identifiant : " + fournis().toCompte().cptLogin() + "\n");
				sb.append("Mot de passe : " + fournis().toCompte().cptPasswdClair() + "\n");
				sb.append("-------------------------------------------------------------------------------" + "\n");
				sb.append("" + "\n");
				sb.append("Veuillez conserver ce message pour pouvoir le consulter ultérieurement." + "\n");
				sb.append("Ces informations vous permettront de suivre l'évolution du traitement de vos factures par nos services. L'url de connexion apparaitra sur les bons de commande.");
				sb.append("" + "\n");
				//		sb.append("-------------------------------------------------------------------------------" + "\n") ;		
				//		sb.append("Cet email a été généré automatiquement, merci de ne pas y répondre directement.");

				String body = sb.toString();

				MyCRIMailBus mailBus = new MyCRIMailBus(myApp().config());
				if (!mailBus.sendMail(from, to, cc, subject, body)) {
					throw new Exception("Impossible d'envoyer le mail.");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public String sendCompteInfoOnClick() {
		if (fournis().toCompte() == null || (fournis().toCompte().toCompteEmail() == null || MyStringCtrl.isEmpty(fournis().toCompte().toCompteEmail().getEmailFormatte()))) {
			return "return false";
		}
		return "confirm('Souhaitez-vous vraiment envoyer les informations de connexion par email a l\\'adresse " + fournis().toCompte().toCompteEmail().getEmailFormatte() + " ?')";
	}

	public EOFournis fournis() {
		return (EOFournis) valueForBinding(BINDING_fournis);
	}

	public Boolean showRibUI() {
		return booleanValueForBinding(BINDING_showRibUI, DEFAULT_showRibUI);
	}

	public Boolean showCompteUI() {
		return booleanValueForBinding(BINDING_showCompteUI, DEFAULT_showCompteUI);
	}
}
