/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxUserQuestionDelegate;
import org.cocktail.fwkcktlpersonne.common.controles.ControleBIC;
import org.cocktail.fwkcktlpersonne.common.metier.EOBanque;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class BanqueUI extends AComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_ON_ENREGISTRER_ACTION = "onEnregistrerAction";
	private static final String BINDING_ON_ANNULER_ACTION = "onAnnulerAction";
	private static final String BINDING_BANQUE = "banque";
	private EOEditingContext _editingContext;
	private EOBanque banque;
	private CktlAjaxUserQuestionDelegate userQuestionDelegate;

	public BanqueUI(WOContext context) {
		super(context);
		userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
	}

	//	public EOEditingContext edc() {
	//		if (_editingContext == null) {
	//			if (valueForBinding(BINDING_editingContext) == null) {
	//				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
	//			}
	//			//			_editingContext = new EOEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
	//			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
	//		}
	//		return _editingContext;
	//	}

	public WOActionResults onAnnuler() {
		try {
			setErreurSaisieMessage(null);
			setIsEditing(Boolean.FALSE);
			edc().revert();
			if (hasBinding(BINDING_ON_ANNULER_ACTION)) {
				return performParentAction((String) valueForBinding(BINDING_ON_ANNULER_ACTION));
			}
			_editingContext = null;
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onEnregistrerSansConfirmation() {
		setErreurSaisieMessage(null);
		try {
			if (getBanque() != null) {
				// push dans l'ec parent
				edc().saveChanges();
			}
			setIsEditing(Boolean.FALSE);

			if (hasBinding(BINDING_ON_ENREGISTRER_ACTION)) {
				return performParentAction((String) valueForBinding(BINDING_ON_ENREGISTRER_ACTION));
			}
			_editingContext = null;
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onEnregistrer() {
		setErreurSaisieMessage(null);
		boolean doIt = false;
		try {
			//chercher des doublons
			NSArray<EOBanque> doublons = NSArray.emptyArray();
			//	doublons = EOBanque.findDoublons(getBanque().editingContext(), getBanque());
			if (doublons.count() > 0) {
				doIt = false;
				EOBanque banqueDoublon = (EOBanque) doublons.objectAtIndex(0);
				String s = "La banque " + getBanque().libelle() + " existe deja, voulez-vous quand meme en créer une nouvelle ?";
				Boolean confirm = userQuestionDelegate.askUserAsBoolean("question1", s, getMainContainerId());
				if (confirm == null) {
					return null;
				}
				else {
					doIt = confirm.booleanValue();
				}
			}
			else {
				doIt = true;
			}

			if (doIt) {
//				getBanque().setPersIdCreation(getUtilisateurPersId());
//				getBanque().setPersIdModification(getUtilisateurPersId());
				getBanque().validateObjectMetier();
				// push dans l'ec parent
				edc().saveChanges();
				setIsEditing(Boolean.FALSE);
			}
			userQuestionDelegate.clearAnswers();

			if (doIt) {
				if (hasBinding(BINDING_ON_ENREGISTRER_ACTION)) {
					return performParentAction((String) valueForBinding(BINDING_ON_ENREGISTRER_ACTION));
				}
			}

		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public EOBanque getBanque() {
		EOBanque bdBanque = (EOBanque) valueForBinding(BINDING_BANQUE);
		return bdBanque;
		//		if (banque == null || !banque.equals(bdBanque) || !banque.editingContext().equals(edc())) {
		//			banque = bdBanque.localInstanceIn(edc());
		//		}
		//		return banque;
	}

	//	public void setBanque(EOBanque banque) {
	//		this.banque = banque;
	//	}

	public Boolean showFormatFR() {
		return ControleBIC.isInZoneFR(getBanque().bic(), edc());
	}

	@Override
	public Boolean isEditing() {
		Boolean res = super.isEditing();
		return res;
	}

	@Override
	public String getUpdateContainerID() {
		if (hasBinding(BINDING_updateContainerID)) {
			return (String) valueForBinding(BINDING_updateContainerID);
		}
		return super.getUpdateContainerID();
	}
}