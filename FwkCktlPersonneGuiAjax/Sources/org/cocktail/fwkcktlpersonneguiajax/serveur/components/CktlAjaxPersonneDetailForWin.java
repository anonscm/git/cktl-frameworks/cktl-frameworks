/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Composant pour affichage du détail d'une personne à l'intérieur d'une fenêtre modale.
 * 
 * @author rprin
 */
public class CktlAjaxPersonneDetailForWin extends AComponent {

	private static final long serialVersionUID = 1L;
	private IPersonne personne;
	private Integer persId;
	private Boolean wantRefreshGroupes = Boolean.TRUE;

	/** Composant parent pour recuperation des feuilles de style chargees */
	private WOComponent parentComponent;
	
	public CktlAjaxPersonneDetailForWin(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		// On rajoute les ressources Web du composant parent
		if (parentComponent != null && parentComponent instanceof CktlResourceProvider) {
		    ((CktlResourceProvider)parentComponent).injectResources(response, context);
		}
	}

	public WOComponent getParentComponent() {
		return parentComponent;
	}

	public void setParentComponent(WOComponent parentComponent) {
		this.parentComponent = parentComponent;
	}

	public EOEditingContext editingContext() {
		return personne().editingContext();
	}

	/**
	 * @return the personne
	 */
	public IPersonne personne() {
		return personne;
	}

	/**
	 * @param personne the personne to set
	 */
	public void setPersonne(IPersonne personne) {
		this.personne = personne;
	}

	/**
	 * @return the wantRefreshGroupes
	 */
	public Boolean wantRefreshGroupes() {
		return wantRefreshGroupes;
	}

	/**
	 * @param wantRefreshGroupes the wantRefreshGroupes to set
	 */
	public void setWantRefreshGroupes(Boolean wantRefreshGroupes) {
		this.wantRefreshGroupes = wantRefreshGroupes;
	}

	/**
	 * @return the persId
	 */
	public Integer persId() {
		return persId;
	}

	/**
	 * @param persId the persId to set
	 */
	public void setPersId(Integer persId) {
		this.persId = persId;
	}

}
