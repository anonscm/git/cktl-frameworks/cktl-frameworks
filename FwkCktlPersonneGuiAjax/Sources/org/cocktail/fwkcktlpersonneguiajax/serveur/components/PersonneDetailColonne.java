/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * Colonne de tableView qui permet l'affichage du détail d'une personne à partir d'un lien.
 * 
 * @binding dialogWidth
 * @binding dialogHeight
 * @author rprin
 */
public class PersonneDetailColonne extends AComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_dialogWidth = "dialogWidth";
	private static final String BINDING_dialogHeight = "dialogHeight";

	private String dialogId;
	private String dialogTitle;

	public PersonneDetailColonne(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		// CktlAjaxUtils.addScriptResourceInHead(context, response, frameworkName(), "scripts/cktlajaxpersonnedetailcolonne.js");
	}

	/**
	 * @return the dialogId
	 */
	public String dialogId() {
		return (String) valueForBinding("dialogId");
	}

	/**
	 * @param dialogId the dialogId to set
	 */
	public void setDialogId(String dialogId) {
		this.dialogId = dialogId;
	}

	/**
	 * @return the dialogTitle
	 */
	public String dialogTitle() {
		return (String) valueForBinding("dialogTitle");
	}

	/**
	 * @param dialogTitle the dialogTitle to set
	 */
	public void setDialogTitle(String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}

}
