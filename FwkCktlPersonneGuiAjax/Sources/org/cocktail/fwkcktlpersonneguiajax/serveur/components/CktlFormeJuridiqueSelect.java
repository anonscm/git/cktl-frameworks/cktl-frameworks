/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.CktlAjaxGroupeTreeViewCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;

/**
 * Selection et affichage d'une forme juridique dans une fenêtre modale.
 * 
 * @deprecated Cf. {@link CktlAjaxFormeJuridiqueSelect}
 * @author rprin
 */
public class CktlFormeJuridiqueSelect extends AComponent {

	private static final long serialVersionUID = 1L;

	public static final String VALUE_BDG = "value";

	/**
	 * Facultatif. Binding pour specifier un NSDictionary de paires "Libelle filtre"/Qualifier. Les Libelles apparaissent dans une liste deroulante,
	 * le qualifier correspondant au libelle selectionne est ensuite applique au treeView. Les qualifier doivent êre applicable aux objets
	 * EOStrcuture.
	 */
	public static final String USER_FILTERS_DICTIONARY_BDG = "userFiltersDictionary";

	/** ID de l'objet FORM contenant le composant (necessaire si vous specifiez des filtres utilisateurs) */
	public static final String USER_FILTERS_FORM_ID_BDG = "userFiltersFormID";

	/** Facultatif. Binding pour specifier la taille du champ (en colonnes). */
	public static final String TEXT_FIELD_SIZE_BDG = "textFieldSize";

	/** Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme. */
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";

	/** Facultatif. Binding pour specifier si le bouton supprimer doit etre affiche. Par defaut a true. */
	public static final String DISPLAY_DELETE_BUTTON_BDG = "displayDeleteButton";

	/** Facultatif. Binding pour specifier une fonction JavaScript a executer apres la suppression. */
	public static final String SUPPRIMER_ON_COMPLETE_BDG = "supprimerOnComplete";

	/** Facultatif. Binding pour specifier s'il faut demander une confirmation avent la selection. */
	public static final String CONFIRM_BEFORE_SELECTION_BDG = "confirmBeforeSelection";

	public static final String SELECTION_BDG = "selection";

	//	public static final String TREE_VIEW_TITLE_BDG = "treeViewTitle";
	//	public static final String TREE_VIEW_WIDTH_BDG = "treeViewWidth";
	//	public static final String TREE_VIEW_HEIGHT_BDG = "treeViewHeight";
	//	public static final String TREE_VIEW_CLASS_NAME_BDG = "treeViewClassName";
	//	
	//	/** Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les enfants d'un noeud. Ce qualifier doit être applicable aux objets EOStructure. Si non null, le qualifier se rajoute a l'eventuel filtre selectionne par l'utilisateur. */
	//	public static final String TREE_QUALIFIER_BDG = "treeQualifier";
	//	
	//	
	//	/** Facultatif. Binding pour specifier la racine de l'arbre.*/
	//	public static final String TREE_ROOT_OBJECT_BDG = "treeRootObject";
	//	
	//	/** Facultatif. Permet de specifier un NSArray d'objets a exclure de l'affichage de l'arbre. */
	//	public static final String TREE_EXCLUSIONS_BDG = "treeExclusions";
	//	
	//	public static final String TREE_COLLAPSED_IMG_BDG = "treeCollapsedImage";
	//	public static final String TREE_COLLAPSED_IMG_FWK_BDG = "treeCollapsedImageFramework";
	//	public static final String TREE_EXPANDED_IMG_BDG = "treeExpandedImage";
	//	public static final String TREE_EXPANDED_IMG_FWK_BDG = "treeExpandedImageFramework";
	//	public static final String TREE_LEAF_IMG_BDG = "treeLeafImage";
	//	public static final String TREE_LEAF_IMG_FWK_BDG = "treeLeafImageFramework";

	private CktlAjaxGroupeTreeViewCtrl ctrl;
	private EOStructure selection;
	//	private String collapsedImage, collapsedImageFramework;
	//	private String expandedImage, expandedImageFramework;
	//	private String leafImage, leafImageFramework;
	//	private EOQualifier treeQualifier;

	private NSDictionary filtres;
	private String unFiltre, unFiltreSelectionne;
	private EOQualifier userFiltreQualifier;

	public CktlFormeJuridiqueSelect(WOContext context) {
		super(context);
		//    	ctrl = new CktlFormeJuridiqueSelectCtrl(this);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);

	}

	/**
	 * @return the ctrl
	 */
	public CktlAjaxGroupeTreeViewCtrl ctrl() {
		return ctrl;
	}

	/**
	 * @param ctrl the ctrl to set
	 */
	public void setCtrl(CktlAjaxGroupeTreeViewCtrl ctrl) {
		this.ctrl = ctrl;
	}

	public EOStructure selection() {
		return selection;
	}

	public void setSelection(EOStructure selection) {
		this.selection = selection;
		setValueForBinding(selection, CktlAjaxGroupeSelect.SELECTION_BDG);
	}

	public String onSuccessRechercher() {
		String onSuccessRechercher = null;
		//FIXME modifier - Rod : modifier quoi ???
		onSuccessRechercher = "function () {openWinFJ('";
		onSuccessRechercher += getComponentId() + "', '";
		onSuccessRechercher += ");}";

		return onSuccessRechercher;
	}

	public String onSuccessSelect() {
		String onSuccessSelect = null;
		onSuccessSelect = "function () {Windows.close('";
		onSuccessSelect += getComponentId() + "_win');}";

		return onSuccessSelect;
	}

	public void setUserFiltreQualifier(EOQualifier userFiltreQualifier) {
		this.userFiltreQualifier = userFiltreQualifier;
	}

	public EOQualifier getUserFiltreQualifier() {
		return userFiltreQualifier;
	}

	public String containerFJSelectionneId() {
		return getComponentId() + "_selection";
	}

	public void onClose() {
		ctrl().setRootGroupe(null);
	}

	public NSDictionary filtres() {
		if (filtres == null) {
			filtres = (NSDictionary) valueForBinding(USER_FILTERS_DICTIONARY_BDG);
		}
		return filtres;
	}

	/**
	 * @return the unFiltreSelectionne
	 */
	public String unFiltreSelectionne() {
		return unFiltreSelectionne;
	}

	/**
	 * @param unFiltreSelectionne the unFiltreSelectionne to set
	 */
	public void setUnFiltreSelectionne(String unFiltreSelectionne) {
		this.unFiltreSelectionne = unFiltreSelectionne;
	}

	/**
	 * @return the unFiltre
	 */
	public String unFiltre() {
		return unFiltre;
	}

	/**
	 * @param unFiltre the unFiltre to set
	 */
	public void setUnFiltre(String unFiltre) {
		this.unFiltre = unFiltre;
	}

	public WOActionResults filtrer() {
		String filtre = unFiltreSelectionne();
		if (filtre != null) {
			if (filtre.equalsIgnoreCase("Tous")) {
				setUserFiltreQualifier(null);
			}
			else {
				setUserFiltreQualifier((EOQualifier) filtres.objectForKey(filtre));
			}
			ctrl().setRootGroupe(null);
		}
		else {
			setUserFiltreQualifier(null);
			ctrl().setRootGroupe(null);
		}
		return null;
	}

	public WOActionResults supprimerSelection() {
		setSelection(null);
		return null;
	}

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}

	public boolean isFiltrageAvailable() {
		boolean isFiltrageAvailable = hasBinding(USER_FILTERS_DICTIONARY_BDG);
		if (isFiltrageAvailable) {
			NSDictionary dicoFiltres = filtres();
			isFiltrageAvailable = dicoFiltres.count() > 0 ? true : false;
		}
		return isFiltrageAvailable;
	}

	public String filtrePopUpID() {
		return getComponentId() + "_filtrePopUp";
	}

	public String containerOnCloseID() {
		return getComponentId() + "_containerOnClose";
	}

	public Integer textFieldSize() {
		if (hasBinding(TEXT_FIELD_SIZE_BDG)) {
			return (Integer) valueForBinding(TEXT_FIELD_SIZE_BDG);
		}
		return Integer.valueOf(50);
	}

	public Boolean displayDeleteButton() {
		if (hasBinding(DISPLAY_DELETE_BUTTON_BDG)) {
			return (Boolean) valueForBinding(DISPLAY_DELETE_BUTTON_BDG);
		}
		return Boolean.TRUE;
	}

	public String supprimerOnComplete() {
		if (hasBinding(SUPPRIMER_ON_COMPLETE_BDG)) {
			return (String) valueForBinding(SUPPRIMER_ON_COMPLETE_BDG);
		}
		return null;
	}

	public String selectionOnClickBefore() {
		if (hasBinding(CONFIRM_BEFORE_SELECTION_BDG)) {
			if (((Boolean) valueForBinding(CONFIRM_BEFORE_SELECTION_BDG)).booleanValue()) {
				return ("confirm('Voulez-vous selectionner le groupe " + jsEncode(ctrl.unGroupeLibelle()) + " ?')");
			}
		}
		return null;
	}

	public Boolean isGroupeSelectionnable() {
		return (ctrl.unGroupe().object() instanceof EOStructure && getAppUserForAnnuaire().hasDroitGererGroupe((EOStructure) ctrl.unGroupe().object()));
	}
}
