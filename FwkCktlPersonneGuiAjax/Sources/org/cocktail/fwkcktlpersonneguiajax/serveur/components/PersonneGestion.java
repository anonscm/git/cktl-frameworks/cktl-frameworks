/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneFormCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneGestionCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneSrchCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * Composant pour la gestion des personnes. Offre la possibilité d'effectuer une recherche de personne, de récupérer la selection et eventuellement
 * d'en creer une nouvelle. Il est possible de specifier un controleur a utiliser. Dans ce cas, sous-classez {@link PersonneGestionCtrl} et affecter
 * une instance du controleur sur le binding ctrl. Ce composant est un container des composant {@link PersonneSrch} et {@link PersonneForm}. Vous
 * pouvez également avoir besoin de sous-classer les controleurs de ces composants. Dans ce cas, sous-classez {@link PersonneSrchCtrl} et/ou
 * {@link PersonneFormCtrl} et affectez une instance de ces classes aux bindings srchCtrl et formCtrl.
 * 
 * @binding showcompteUI Indique s'il faut afficher les informations sur les comptes dans le détail de la personne.
 * @binding showRadioInterneExterne Cf. {@link PersonneSrch}
 * @binding PersonneAdminUI_disableAutoCompleteFields Si true, active la saisie des champs du composant PersonneAdminUI via des fenetres de selection
 *          plutot que via des autocomplete.
 * @binding callbackOnSelectionnerPersonne
 * @binding selectedPersonne Obligatoire. Indique au composant parent l'objet {@link IPersonne} selectionne. Est mis a jour lors de l'enregistrement
 *          réussi du formulaire ou lors de la selection.
 * @binding utilisateurPersId
 * @binding wantReset
 * @binding editingContext
 * @binding disableSelection Facultatif. Affiche ou non un bouton pour selectionner la personne (a partir du detail).
 * @binding selectionnerPersonneLabel Facultatif. Libellé affiché pour le lien qui permet de selectionner la personne affichee.
 * @binding disableForm Facultatif. Active ou non le formulaire.
 * @binding disableSrch Facultatif. active ou non l'écran de recherche.
 * @binding callbackAfterFormEnregistrer Facultatif. Indiquez le nom de l'action a executer dans le composant parent lorsque l'utilisateur enregistre
 *          la personne.
 * @binding callbackAfterFormAnnuler Facultatif. Indiquez le nom de l'action a executer dans le composant parent lorsque l'utilisateur annule
 *          l'enregistrement de la personne.
 * @binding onFormEnregistrerSuccess
 * @binding onSelectionnerPersonneSuccess Facultatif. Fonction JS a executer suite a clic sur le lien Selectionner.
 * @binding ctrl Facultatif. Controleur qui doit heriter de PersonneGestionCtrl
 * @binding formCtrl Facultatif. Controleur pour PersonneForm, doit heriter de PersonneFormCtrl.
 * @binding srchCtrl Facultatif. Controleur pour PersonneSrch, doit heriter de PersonneSrchCtrl.
 * @binding formID
 * @binding individuTableViewColonneKeys Cf. {@link PersonneSrch}
 * @binding showCompteUI
 * @binding showIndividus Facultatif. Affiche ou non la recherche des individus. Cf. {@link PersonneSrch}
 * @binding showPersonnesInternes Facultatif. Indique au composant d'afficher le formulaire de recherche spécifique aux personnes internes a
 *          l'etablissement Cf. {@link PersonneSrch}
 * @binding showPersonnesExternes Facultatif. Indique au composant d'afficher le formulaire de recherche spécifique aux personnes externes a
 *          l'etablissmeent Cf. {@link PersonneSrch}
 * @binding showStructures Facultatif. Affiche ou non la recherche des structures. Cf. {@link PersonneSrch}
 * @binding srchResultListHeight Facultatif. Hauteur des tableViews de résultats. Cf. {@link #DEFAULT_srchResultListHeight}.
 * @binding structureTableViewColonneKeys Cf. {@link PersonneSrch}
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class PersonneGestion extends AComponent implements PersonneForm.IPersonneFormContainer {

	private static final long serialVersionUID = -7729144178176449647L;

	public static final String DEFAULT_srchResultListHeight = "200px";

	/**
	 * Obligatoire. Indique au composant parent l'objet {@link IPersonne} selectionne. Est mis a jour lors de l'enregistrement réussi du formulaire ou
	 * lors de la selection.
	 */
	public static final String BINDING_selectedPersonne = "selectedPersonne";

	/**
	 * Facultatif. Indiquez le nom de l'action a executer dans le composant parent lorsque l'utilisateur selectionne la personne (action de
	 * selection).
	 */
	public final static String BINDING_callbackOnSelectionnerPersonne = "callbackOnSelectionnerPersonne";

	/**
	 * Facultatif. Indiquez le nom de l'action a executer dans le composant parent lorsque l'utilisateur annule l'enregistrement de la personne.
	 */
	public final static String BINDING_callbackAfterFormAnnuler = "callbackAfterFormAnnuler";

	/**
	 * Facultatif. Indiquez le nom de l'action a executer dans le composant parent lorsque l'utilisateur enregistre la personne.
	 */
	public final static String BINDING_callbackAfterFormEnregistrer = "callbackAfterFormEnregistrer";

	public final static String BINDING_wantReset = "wantReset";

	/** Facultatif. active ou non l'écran de recherche. */
	public final static String BINDING_disableSrch = "disableSrch";

	/** Facultatif. Active ou non le formulaire. */
	public final static String BINDING_disableForm = "disableForm";

	/** Facultatif. Affiche ou non la recherche des individus. */
	public final static String BINDING_showIndividus = "showIndividus";

	/** Facultatif. Affiche ou non la recherche des structures. */
	public final static String BINDING_showStructures = "showStructures";

	/**
	 * Facultatif. Indique au composant d'afficher le formulaire de recherche spécifique aux personnes internes a l'etablissmeent
	 */
	public static final String BINDING_showPersonnesInternes = "showPersonnesInternes";

	/**
	 * Facultatif. Indique au composant d'afficher le formulaire de recherche spécifique aux personnes externes a l'etablissmeent
	 */
	public static final String BINDING_showPersonnesExternes = "showPersonnesExternes";

	/**
	 * Facultatif. Affiche ou non un bouton pour selectionner la personne (a partir du detail).
	 */
	public final static String BINDING_disableSelection = "disableSelection";

	/**
	 * Facultatif. Libellé affiché pour le lien qui permet de selectionner la personne affichee.
	 */
	public final static String BINDING_selectionnerPersonneLabel = "selectionnerPersonneLabel";

	/**
	 * Facultatif. Fonction JS a executer suite a clic sur le bouton Enregistrer du formulaire.
	 */
	public final static String BINDING_onFormEnregistrerSuccess = "onFormEnregistrerSuccess";

	/**
	 * Facultatif. Fonction JS a executer suite a clic sur le bouton Annuler du formulaire.
	 */
	public final static String BINDING_onFormAnnulerSuccess = "onFormAnnulerSuccess";

	/**
	 * Facultatif. Fonction JS a executer suite a clic sur le lien Selectionner.
	 */
	public final static String BINDING_onSelectionnerPersonneSuccess = "onSelectionnerPersonneSuccess";

	/** */
	public final static String BINDING_ctrl = "ctrl";

	/**
	 * Facultatif. Controleur du composant de recherche (doit heriter de PersonneSrchCtrl)
	 */
	public final static String BINDING_srchCtrl = "srchCtrl";

	/**
	 * Facultatif. Controleur du composant Formulaire (doit heriter de PersonneFormCtrl)
	 */
	public final static String BINDING_formCtrl = "formCtrl";

	/**
	 * Facultatif. Hauteur des tableViews de résultats. Cf. {@link #DEFAULT_srchResultListHeight}.
	 */
	public static final String BINDING_srchResultListHeight = "srchResultListHeight";

	private Boolean isVueSrch = Boolean.TRUE;
	private Boolean wantRefreshDetail = Boolean.FALSE;
	private Boolean wantResetForm = Boolean.FALSE;
	private Boolean wantResetSrch = Boolean.FALSE;
	private IPersonne selectedPersonne;
	private final ERXDisplayGroup dgPersonnes = new ERXDisplayGroup();
	private String afterFormEnregistrerMessage = null;
	private String scriptAfterForm = null;

	private PersonneGestionCtrl ctrl;

	public PersonneGestion(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (Boolean.TRUE.equals(valueForBinding(BINDING_wantReset))) {
			isVueSrch = Boolean.TRUE;
			wantResetSrch = Boolean.TRUE;
			setValueForBinding(Boolean.FALSE, BINDING_wantReset);
		}
		else {
			wantResetSrch = Boolean.FALSE;
		}
		super.appendToResponse(response, context);
	}

	public Boolean disableSelection() {
		if (hasBinding(BINDING_disableSelection)) {
			return (Boolean) valueForBinding(BINDING_disableSelection);
		}
		return Boolean.FALSE;
	}

	public Boolean disableSrch() {
		if (hasBinding(BINDING_disableSrch)) {
			return (Boolean) valueForBinding(BINDING_disableSrch);
		}
		return Boolean.FALSE;
	}

	public Boolean disableForm() {
		if (hasBinding(BINDING_disableForm)) {
			return (Boolean) valueForBinding(BINDING_disableForm);
		}
		return Boolean.FALSE;
	}

	public Boolean isVueSrch() {
		return (Boolean.FALSE.equals(disableSrch()) && isVueSrch);
		//		return isVueSrch;
	}

	public Boolean isVueForm() {
		return (Boolean.FALSE.equals(disableForm()) && !isVueSrch().booleanValue());
		//return Boolean.valueOf(! isVueSrch.booleanValue());
	}

	public void setIsVueSrch(Boolean isVueSrch) {
		this.isVueSrch = isVueSrch;
	}

	/**
	 * Personne selectionnee suite a la recherche.
	 */
	public IPersonne getSelectedPersonne() {
		//		if (selectedPersonne == null) {
		//			if (hasBinding(BINDING_selectedPersonne)) {
		//				selectedPersonne = (IPersonne) valueForBinding(BINDING_selectedPersonne);
		//			}
		//		}
		return selectedPersonne;
	}

	public void setSelectedPersonne(IPersonne selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
	}

	public WOActionResults onCreerPersonne() {
		try {
			//			if (getSelectedPersonne() == null) {
			//				throw new Exception("Aucune personne selectionnee.");
			//			}
			getCtrl().onCreerPersonne();
			wantResetForm = Boolean.TRUE;
			setIsVueSrch(Boolean.FALSE);
			setScriptAfterForm("enableCreation(false);");
		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onModifierPersonne() {
		try {
			getCtrl().onModifierPersonne();
			wantResetForm = Boolean.TRUE;
			setIsVueSrch(Boolean.FALSE);
		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onSelectionnerPersonne() {
		setValueForBinding(getSelectedPersonne(), BINDING_selectedPersonne);
		if (valueForBinding(BINDING_callbackOnSelectionnerPersonne) != null) {
			return performParentAction((String) valueForBinding(BINDING_callbackOnSelectionnerPersonne));
		}
		return null;
	}

	//	
	public WOActionResults onFormEnregistrer() {
		setIsVueSrch(Boolean.TRUE);
		setWantRefreshDetail(Boolean.TRUE);
		setValueForBinding(getSelectedPersonne(), BINDING_selectedPersonne);
		if (valueForBinding(BINDING_callbackAfterFormEnregistrer) != null) {
			return performParentAction((String) valueForBinding(BINDING_callbackAfterFormEnregistrer));
		}
		return null;
	}

	//	
	public WOActionResults onFormAnnuler() {
		setIsVueSrch(Boolean.TRUE);
		setWantRefreshDetail(Boolean.TRUE);
		if (selectedPersonne != null && (selectedPersonne.globalID() == null || selectedPersonne.hasTemporaryGlobalID())) {
			if (dgPersonnes.allObjects().indexOf(selectedPersonne) != NSArray.NotFound) {
				dgPersonnes.deleteObjectAtIndex(dgPersonnes.allObjects().indexOf(selectedPersonne));
			}
		}
		setScriptAfterForm(null);
		if (valueForBinding(BINDING_callbackAfterFormAnnuler) != null) {
			return performParentAction((String) valueForBinding(BINDING_callbackAfterFormAnnuler));
		}
		return null;
	}

	public boolean wantResetForm() {
		return wantResetForm;
	}

	public void setWantResetForm(Boolean wantReset) {
		this.wantResetForm = wantReset;
	}

	public Boolean wantRefreshDetail() {
		return wantRefreshDetail;
	}

	public void setWantRefreshDetail(Boolean wantRefreshDetail) {
		this.wantRefreshDetail = wantRefreshDetail;
	}

	public Boolean getWantResetSrch() {
		return wantResetSrch;
	}

	public void setWantResetSrch(Boolean wantResetSrch) {
		this.wantResetSrch = wantResetSrch;
	}

	public IPersonne getSelectedPersonneForForm() {
		return getSelectedPersonne();
	}

	public void setSelectedPersonneForForm(IPersonne laPersonne) {
		setSelectedPersonne(laPersonne);
	}

	public void setResetForm(Boolean b) {
		this.wantResetForm = b;

	}

	public Boolean showIndividus() {
		if (hasBinding(BINDING_showIndividus)) {
			return (Boolean) valueForBinding(BINDING_showIndividus);
		}
		return Boolean.TRUE;
	}

	public Boolean showStructures() {
		if (hasBinding(BINDING_showStructures)) {
			return (Boolean) valueForBinding(BINDING_showStructures);
		}
		return Boolean.TRUE;
	}

	public Boolean showPersonnesInternes() {
		if (hasBinding(BINDING_showPersonnesInternes)) {
			return (Boolean) valueForBinding(BINDING_showPersonnesInternes);
		}
		return Boolean.TRUE;
	}

	public Boolean showPersonnesExternes() {
		if (hasBinding(BINDING_showPersonnesExternes)) {
			return (Boolean) valueForBinding(BINDING_showPersonnesExternes);
		}
		return Boolean.TRUE;
	}

	public ERXDisplayGroup getDgPersonnes() {
		return dgPersonnes;
	}

	public Boolean hasScriptAfterForm() {
		return Boolean.valueOf(scriptAfterForm != null);
	}

	public void setScriptAfterForm(String object) {
		scriptAfterForm = object;
	}

	public String getScriptAfterForm() {
		String res = scriptAfterForm;
		setScriptAfterForm(null);
		return res;
	}

	public String getAfterFormEnregistrerMessage() {
		return afterFormEnregistrerMessage;
	}

	public Boolean hasAfterFormEnregistrerMessage() {
		return Boolean.valueOf(!MyStringCtrl.isEmpty(afterFormEnregistrerMessage));
	}

	public void setAfterFormEnregistrerMessage(String afterFormEnregistrerMessage) {
		this.afterFormEnregistrerMessage = afterFormEnregistrerMessage;
	}

	public PersonneGestionCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (PersonneGestionCtrl) valueForBinding(BINDING_ctrl);
			}
			else {
				ctrl = new PersonneGestionCtrl(this);
			}
		}
		return ctrl;
	}

	public PersonneSrchCtrl getSrchCtrl() {
		return (PersonneSrchCtrl) valueForBinding(BINDING_srchCtrl);
	}

	public PersonneFormCtrl getFormCtrl() {
		return (PersonneFormCtrl) valueForBinding(BINDING_formCtrl);
	}

	public String srchResultListHeight() {
		if (hasBinding(BINDING_srchResultListHeight)) {
			return (String) valueForBinding(BINDING_srchResultListHeight);
		}
		return DEFAULT_srchResultListHeight;
	}

}
