/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;

/**
 * Gere affichage/modification des adresses email associees a un compte.
 * 
 * @binding compte compte sur lequel le composant se base pour afficher/ajouter les compteEmail.
 * @binding editingContext
 * @binding erreurMessage will set. Variable contenant les messages d'erreur renvoyés par le composant.
 * @binding updateContainerID ID du container à mettre à jour apres ajout ou suppression d'une adresse email.
 * @binding isCompactView
 * @binding isReadOnly True par defaut
 * @binding isAnonymized False par defaut : permet de rendre anonyme les emails affichés. Exemple : p***@cocktail.org
 * @author rprin
 */
public class CompteEmailUI extends AComponent {

	private static final long serialVersionUID = 1L;

	/**
	 * Binding : compte sur lequel le composant se base pour afficher/ajouter les compteEmail.
	 */
	public static final String BINDING_compte = "compte";

	/** Binding : editingContext à utiliser */
	public static final String BINDING_editingContext = "editingContext";

	/**
	 * Binding : Variable contenant les messages d'erreur renvoyés par le composant.
	 */
	public static final String BINDING_erreurMessage = "erreurMessage";

	/**
	 * Binding : ID du container à mettre à jour apres ajout ou suppression d'une adresse email.
	 */
	public static final String BINDING_updateContainerID = "updateContainerID";

	public static final String BINDING_isCompactView = "isCompactView";
	public static final String BINDING_isReadOnly = "isReadOnly";
	public static final String BINDING_isAnonymized = "isAnonymized";

	public EOCompteEmail unCompteEmail;
	private int compteEmailIndex;

	private String _email;
	private EOEditingContext _editingContext;
	private EOCompte _compte;

	public EOCompteEmail selectedCompteEmail;

	public CompteEmailUI(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
//		getCompte();
		_email = "";
		super.appendToResponse(woresponse, wocontext);
	}

	@SuppressWarnings("rawtypes")
	public NSArray getLesCompteEmails() {
		return getCompte().toCompteEmails(null, new NSArray(EOCompteEmail.SORT_PRIORITE), false);
	}

	/**
	 * Action de supprimer un email
	 * @return null
	 */
	public WOActionResults onCompteEmailSupprimer() {
		try {
			getCompte().removeFromToCompteEmailsRelationship(unCompteEmail);
			getCompte().setCptEmail(null);
			
			edc().deleteObject(unCompteEmail);
			edc().saveChanges();
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onCompteEmailEnregistrer() {
		EOCompteEmail compteEmail = getSelectedCompteEmail();
		if (compteEmail != null) {
			setEmail(compteEmail.getEmailFormatte());
		}
		if (!MyStringCtrl.isEmpty(_email)) {
			try {
				if (!MyStringCtrl.isEmailValid(_email)) {
					throw new NSValidation.ValidationException("L'adresse email " + _email + " n'est pas une adresse email valide.");
				}

				if ((compteEmail != null && EOCompteEmail.isCompteEmailAlreadyExistInCompte(getCompte(), compteEmail))
						|| (compteEmail == null && EOCompteEmail.isEmailAffecte(getCompte(), _email))) {
					throw new NSValidation.ValidationException("L'adresse email " + _email + " est déjà affectée à ce compte.");
				}

				if (compteEmail == null) {
					if (EOCompteEmail.isEmailAffecte(getCompte(), _email)) {
						throw new NSValidation.ValidationException("L'adresse email " + _email + " est déjà affectée à ce compte.");
					}
					compteEmail = EOCompteEmail.creerInstance(edc());
					compteEmail.setToCompteRelationship(getCompte());
					compteEmail.setEmailFormatte(_email);

				}
				else {
					if (EOCompteEmail.isCompteEmailAlreadyExistInCompte(getCompte(), compteEmail)) {
						throw new NSValidation.ValidationException("L'adresse email " + _email + " est déjà affectée à ce compte.");
					}
				}

				//				EOCompteEmail compteEmail = EOCompteEmail.creerInstance(edc());
				//				compteEmail.setEmailFormatte(_email);
				//				compteEmail.setToCompteRelationship(getCompte());
				edc().saveChanges();
				_email = null;
			} catch (Exception e) {
				edc().revert();
				e.printStackTrace();
				setErreurMessage(e.getMessage());
			}
		}
		setSelectedCompteEmail(null);
		return null;
	}

	/**
	 * 
	 */
	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public EOCompte getCompte() {
		EOCompte compte = (EOCompte) valueForBinding(BINDING_compte);
		if (_compte == null || !ERXEOControlUtilities.eoEquals(_compte, compte)) {
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			_compte = compte.localInstanceIn(edc());
		}
		return _compte;
	}

	private void setErreurMessage(String erreurMsg) {
		setValueForBinding(erreurMsg, BINDING_erreurMessage);
	}

	/**
	 * Binding.
	 * 
	 * @return l'ID du container à mettre à jour apres ajout ou suppression d'une adresse email.
	 */
	public String updateContainerID() {
		return (String) valueForBinding(BINDING_updateContainerID);
	}

	public Boolean isCompactView() {
		return (Boolean) valueForBinding(BINDING_isCompactView);
	}

	public Boolean isAnonymized() {
		return booleanValueForBinding(BINDING_isAnonymized, Boolean.FALSE);
	}

	public void resetEdc() {
		_editingContext = null;
	}

	public String getEmail() {
		if (MyStringCtrl.isEmpty(_email) && getCompte() != null && getCompte().toIndividu() != null) {
//			_email = EOCompteEmail.computeCanonicalEmailAdresseFromPersonne(getCompte().toIndividu(), edc());
			_email = EOCompteEmail.computeCanonicalEmailAdresseAvecDomaineFromPersonne(getCompte().toIndividu(), edc(), getCompte().toVlans() );
		}
		return _email;
	}

	public void setEmail(String email) {
		this._email = email;
	}

	public String getEmailToDisplay() {
		if (isAnonymized())
			return unCompteEmail.getAnonymizedEmail();
		return unCompteEmail.getEmailFormatte();
	}

	/**
	 * @return the selectedCompteEmail
	 */
	public EOCompteEmail getSelectedCompteEmail() {
		return selectedCompteEmail;
	}

	/**
	 * @param selectedCompteEmail the selectedCompteEmail to set
	 */
	public void setSelectedCompteEmail(EOCompteEmail selectedCompteEmail) {
		this.selectedCompteEmail = selectedCompteEmail;
		if (selectedCompteEmail != null) {
			setEmail(unCompteEmail.getEmailFormatte());
		}
		else {
			setEmail(null);
		}
	}

	public boolean isCompteEmailInEditMode() {
		return ((getSelectedCompteEmail() != null) && (getSelectedCompteEmail().equals(unCompteEmail)));
	}

	public boolean isCompteEmailEditButtonEnabled() {
		return (!isReadOnly() && (getSelectedCompteEmail() == null));
	}

	public boolean isCompteEmailAddingFormEnabled() {
		return (!isReadOnly() && (getSelectedCompteEmail() == null));
	}

	public WOActionResults onCompteEmailEdit() {
		// set unCompteEmail comme le courriel en edition
		setSelectedCompteEmail(unCompteEmail);
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		return null;
	}

	public WOActionResults onCompteEmailCancelEdit() {
		setSelectedCompteEmail(null);
		return null;
	}

	/**
	 * @return the compteEmailIndex
	 */
	public int getCompteEmailIndex() {
		return compteEmailIndex;
	}

	/**
	 * @param compteEmailIndex the compteEmailIndex to set
	 */
	public void setCompteEmailIndex(int compteEmailIndex) {
		this.compteEmailIndex = compteEmailIndex;
	}

	public String getCompteEmailInPlaceID() {
		return getComponentId() + "_EmailInPlaceID_" + getCompteEmailIndex();
	}

	public String getInPlaceManualEditFunctionName() {
		return getCompteEmailInPlaceID() + "Edit()";
	}

	public String getEmailInPlaceFieldID() {
		return "FEmailInPlace_" + getCompteEmailInPlaceID();
	}

	public String getInPlaceManualCancelEditFunctionName() {
		return getCompteEmailInPlaceID() + "Cancel()";
	}

	public String getInPlaceManualValidEditFunctionName() {
		return getCompteEmailInPlaceID() + "Save()";
	}

	//	public String getEditSuccessUpdateJS() {
	//		return updateContainerID()+"Update();";
	//	}
}
