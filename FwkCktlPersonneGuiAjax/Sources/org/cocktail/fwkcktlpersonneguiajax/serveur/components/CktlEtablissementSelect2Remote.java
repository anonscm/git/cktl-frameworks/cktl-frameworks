package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.CktlEtablissementProvider;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.providers.CktlEtablissementProviderImpl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 * @author jlafourc
 *
 */
public class CktlEtablissementSelect2Remote extends CktlAjaxWOComponent {

	private static final long serialVersionUID = -7668823494680995416L;

	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_DEPARTEMENT = "departement";
	private static final String BINDING_IS_SHOW_ONLY_ETABLISSEMENTS_PERE = "isShowOnlyEtablissementsPere";
	
	private CktlEtablissementProvider etablissementsDataProvider;
	
	
	@Override
	public EOEditingContext edc() {
		return (EOEditingContext) valueForBinding("editingContext");
	}
	
	/**
	 * Constructeur obligatoire
	 * @param context le contexte
	 */	
    public CktlEtablissementSelect2Remote(WOContext context) {
        super(context);
    }

	/**
	 * 
	 * @return le paysProvider
	 */
	public CktlEtablissementProvider getEtablissementsDataProvider() {
		if (etablissementsDataProvider == null) {
			etablissementsDataProvider = new CktlEtablissementProviderImpl(edc()) {
				
				@Override
				public void setSelectedEtablissement(EORne etablissement) {
					setSelection(etablissement);
					
				}
				
				@Override
				public EORne getSelectedEtablissement() {
					return getSelection();
				}

				@Override
				public EODepartement getDepartementFiltre() {
					return getDepartement();
				}

				@Override
                public boolean isShowOnlyEtablissementsPereFiltre() {
	                return isShowOnlyEtablissementsPere();
                }

			}; 
		}
		return etablissementsDataProvider;
	}

	
	public EORne getSelection() {
		return (EORne) valueForBinding(BINDING_SELECTION);
	}
	
	/**
	 * @param selection la selection
	 */
	public void setSelection(EORne selection) {
		setValueForBinding(selection, BINDING_SELECTION);
	}
	
	public Boolean disabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}

	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}

	public EODepartement getDepartement() {
		if (hasBinding(BINDING_DEPARTEMENT)) {
			return (EODepartement) valueForBinding(BINDING_DEPARTEMENT);
		}
		return null;
	}
	
	public Boolean isShowOnlyEtablissementsPere() {
		return booleanValueForBinding(BINDING_IS_SHOW_ONLY_ETABLISSEMENTS_PERE, Boolean.FALSE);
	}

}