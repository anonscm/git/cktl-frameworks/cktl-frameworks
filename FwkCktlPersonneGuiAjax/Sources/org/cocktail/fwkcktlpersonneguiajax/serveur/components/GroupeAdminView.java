/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class GroupeAdminView extends AComponent {
	private static final long serialVersionUID = 6333195670044725802L;
	public static final String BINDING_groupe = "groupe";
	public static final String BINDING_shouldShowTypeStructure = "shouldShowTypeStructure";

	public static final String BINDING_shouldShowTypesGroupes = "shouldShowTypesGroupes";
	public static final String BINDING_shouldShowAliasGroupes = "shouldShowAliasGroupes";
	public static final String BINDING_shouldShowAccesGroupes = "shouldShowAccesGroupes";

	public static final String BINDING_shouldShowFilsAriane = "shouldShowFilsAriane";
	
//	public static final String BINDING_wantReset = "wantReset";

	public static final Boolean DEFAULT_shouldShowTypeStructure = Boolean.TRUE;
	public static final Boolean DEFAULT_shouldShowTypesGroupes = Boolean.TRUE;
	public static final Boolean DEFAULT_shouldShowAliasGroupes = Boolean.TRUE;

	public static final Boolean DEFAULT_shouldShowFilsAriane = Boolean.FALSE;

	public EOSecretariat unSecretariat;
	public EOTypeGroupe unTypeGroupe;
	private boolean showTypes = false;

	public GroupeAdminView(WOContext context) {
		super(context);
	}

	public EOStructure groupe() {
		return (EOStructure) valueForBinding(BINDING_groupe);
	}

	public String getLibelleSecretariat() {
		return unSecretariat.toIndividu().getNomPrenomAffichage();
	}

	public Boolean ifGroupe() {
		return Boolean.valueOf(groupe() != null);
	}

	public Boolean ifResponsable() {
		return Boolean.valueOf(groupe() != null && groupe().toResponsable() != null);
	}

	public Boolean ifSecretariat() {
		return Boolean.valueOf(groupe() != null && groupe().toSecretariats().count() > 0);
	}

	public Boolean isTypes() {
		return Boolean.valueOf(lesTypesGroupes().count() > 0);
	}

	public Boolean shouldShowTypeStructure() {
		return booleanValueForBinding(BINDING_shouldShowTypeStructure, DEFAULT_shouldShowTypeStructure);
	}
	
	public Boolean shouldShowTypesGroupes() {
		if (valueForBinding(BINDING_shouldShowTypesGroupes) != null) {
			return (Boolean) valueForBinding(BINDING_shouldShowTypesGroupes);
		}
		return DEFAULT_shouldShowTypesGroupes;
	}

    public Boolean shouldShowAliasGroupes() {
       return booleanValueForBinding(BINDING_shouldShowAliasGroupes, 
                                      DEFAULT_shouldShowAliasGroupes);
    }
	
    public boolean showAlias() {
        return groupe() != null && groupe().grpAlias() != null && shouldShowAliasGroupes();
    }
    
    public Boolean shouldShowFilsAriane(){
    	if (valueForBinding(BINDING_shouldShowFilsAriane) != null) {
			return (Boolean) valueForBinding(BINDING_shouldShowFilsAriane);
		}
		return DEFAULT_shouldShowFilsAriane;
    }
    
//    public Boolean wantReset() {
//		return hasBinding(BINDING_wantReset) && (Boolean)valueForBinding(BINDING_wantReset);
//	}
//	
//	public void setWantReset(Boolean value) {
//		setValueForBinding(value, BINDING_wantReset);
//	}
    
	public NSArray lesTypesGroupes() {
		NSArray res = NSArray.EmptyArray;
		if (groupe() != null) {
			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOStructureForGroupeSpec.getTypesGroupe(groupe()), new NSArray(EOTypeGroupe.SORT_TGRP_LIBELLE_ASC));
		}
		return res;
	}

    public String labelForGrpAccess() {
        return (String)EOStructureForGroupeSpec.GRP_ACCESS.objectForKey(groupe().grpAcces());
    }
    
//    /**
//     * Méthode pour identifier si le groupe est membre d'un groupe
//     * afin de bloquer alors le fils d'Ariane
//     */
//    public boolean isMembreUnGroupe(){
//    	if (EOStructureForGroupeSpec.isPersonneInGroupe(groupe(), groupe().toStructurePere())){
//    		return true;
//    	}
//    	return false;
//    }
    
    
    /**
     * Méthode pour identifier où se trouve le groupe sélectionné dans l'arborescence
     */
    public String filsAriane(){
    	String chemin = groupe().libelle();
    	if (!groupe().isEtablissement()){
    		// Recherche de la racine
    		EOStructure structureRacine = groupe().etablissement();
    		// Recherche du parent immédiat
    		EOStructure groupePere = groupe().toStructurePere();
//    		chemin = groupePere.lcStructure() + ">" + chemin;
    		chemin = groupePere.libelle() + ">" + chemin;
    		// itération du parent précédent si différent de la racine
    		while (groupePere != structureRacine){
    			groupePere = groupePere.toStructurePere();
//    			chemin = groupePere.lcStructure() + ">" + chemin;
    			chemin = groupePere.libelle() + ">" + chemin;
    		}
    	}
    	
    	
    	return chemin;
    }
	
}
