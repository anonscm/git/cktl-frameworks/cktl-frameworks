/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOAnnuaireLog;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.AnnuaireLogService;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXS;

/**
 * Composant d'édition des comptes d'une personne.
 * 
 * @binding editingContext un EOEditingContext parent
 * @binding personne un objet de type IPersonne
 * @binding utilisateurPersId persId de l'utilisateur courant
 * @binding selectedCompte pour mettre à jour le compte sélectionné sur le parent
 * @binding isMultipleAllowed booleen pour autoriser de multiples comptes
 * @binding isReadOnly (optionnel) booleen indiquant si le composant est en lecture seule. True par defaut.
 * @binding vlansAutorisesPourCreation (optionnel) NSArray de EOVLans apparaissant dans la liste des vlans dispos. Si non donné, seul le EOVLans de
 *          type Extérieur est disponible
 * @binding isCompactView (optionnel) booleen indiquant si la vue est compacte
 * @binding autoriserCreation (optionnel) boolean indisquant si la creation de compte est autorisee
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class CompteUI extends AComponent {

	private static final long serialVersionUID = -8406606684158841542L;
	
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_personne = "personne";
	public static final String BINDING_selectedCompte = "selectedCompte";
	public static final String BINDING_autoriserCreation = "autoriserCreation";
	public static final String BINDING_selectedCompteInvalide = "selectedCompteInvalide";

	public static final String DEFAULT_TIMESTAMP_FORMATTER = "%d/%m/%Y à %Hh%M";
	public static final String BINDING_dateFormat = "dateFormat";
	/** Par defaut X */
	public static final String BINDING_vlansAutorisesPourCreation = "vlansAutorisesPourCreation";
	public static final String BINDING_isReadOnly = "isReadOnly";
	public static final String BINDING_isMultipleAllowed = "isMultipleAllowed";
	public static final String BINDING_isCompactView = "isCompactView";
	public static final String BINDING_isDeleting = "isDeleting";

	private static final String FIN_J = "FIN_J";
	private static final String DEMAIN = "Demain";
	private static final String FIN_SEM = "FIN_SEM";
	private static final String UN_MOIS = "1Mois";
	private static final String SIX_MOIS = "6Mois";
	private static final String FIN_ANNEE_U = "FIN_ANNEE_U";
	private static final Calendar CAL = new GregorianCalendar();
	private String expiration = FIN_J;

	public EOCompte unCompte;
	public EOVlans unVlanAutorise;

	private EOEditingContext _editingContext;
	private IPersonne _personne;
	private String _id;
	private NSArray<EOVlans> vlansAutorisesPourCreationCompte;
	private NSArray<EOVlans> listeVlansAccessibles;
	private EOVlans currentVlan;
	private NSArray<EOTypeVpn> listeTypeVPN;
	private EOTypeVpn currentTypeVPN;
	
	private boolean isPasswordReseted = false;
//	private Integer valueUid;
//	private Integer valueGid;
	
	private String hasSignatureCharte = null;
	
	private NSArray<String> listeDomainesSecondaires = null;
	private String currentDomaine;

	@Inject
	private CompteService compteService;
	
	public CompteUI(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
		compteService = new CompteService();
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
			}
		}
	}

	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			setSelectedCompte(null);
			_personne = pers.localInstanceIn(edc());
		}
		return _personne;
	}

	public WOActionResults onSelectCompte() {
		return null;
	}

	public WOActionResults onCompteCreer() {
		EOEditingContext edForCompte = edc();
		// Initialisation à true qui est la valeur par défaut si aucunparamètre d'application ne vient surcharger le paramètre du ParamManager
		boolean passwordEnClair = true;
		try {
			edForCompte.revert();
			setIsEditing(true);
			if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.COMPTE_PASSWORD_EN_CLAIR_ACTIVE)) {
				passwordEnClair = true;
			} else {
				// Rajout pour être sûr qu'il prend bien la valeur false
				passwordEnClair = false;
			}
			EOCompte compte = EOCompte.creerInstance(edForCompte);
//			compte.initialiseCompte(edForCompte,
//					getAppUserForAnnuaire(),
//					getPersonne().localInstanceIn(edForCompte),
//					unVlanAutorise.cVlan(),
//					null, null, true, null, null, null);
			compte.initialiseCompte(edForCompte,
					getAppUserForAnnuaire(),
					getPersonne().localInstanceIn(edForCompte),
					unVlanAutorise.cVlan(),
					null, null, passwordEnClair, null, null, null);
			compte.setCptDomaine(compte.toVlans().domaine());
			setSelectedCompte(compte);
			
			//			setIsEditing(true);

		} catch (ValidationException e) {
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			edForCompte.revert();
			setIsEditing(false);
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults editer() {
		setIsEditing(true);
		setCurrentDomaine(getSelectedCompte().cptDomaine());
		EOCompteEmail.setNomSecondDomain(getCompte().cptDomaine());
		return null;
	}

	public WOActionResults supprimer() {
		AnnuaireLogService annuaireService = new AnnuaireLogService();
		if (getCompte() != null) {
			try {
				EORepartCompte repartCompte = selectionnerRepartCompteAssocie(getCompte());
				if (repartCompte != null) {
					edc().deleteObject(repartCompte);
				}
				verificationSiFournisPourAnnulerLien();
				
				String message = "Suppression du compte avec login " + getCompte().cptLogin() + " et Mdp clair : " + getCompte().cptPasswdClair();
				edc().deleteObject(getCompte());
				
				String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
				annuaireService.creationUnAnnuaireLog(edc(), message, nomApplication);
				
				edc().saveChanges();
				mySession().addSimpleInfoMessage("Information", "Le compte a été supprimé avec succès.");
				setSelectedCompte(null);
				mySession().addSimpleInfoMessage("Information", "Les changements ne seront pris en compte qu'après un enregistrement de votre part.");
				if (getLesComptes() == null || getLesComptes().isEmpty()) {
					mySession().addSimpleInfoMessage("Information", "Une personne sans compte ne peut plus être un utilisateur du système d'information.");
				}
			} catch (ValidationException e) {
				setErreurSaisieMessage(e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
				edc().revert();
				setErreurSaisieMessage(e.getLocalizedMessage());
			}
		}
		return null;
	}

	private void verificationSiFournisPourAnnulerLien() {
		if (getCompte().toPersonne().isIndividu()) {
			if (getCompte().toIndividu().toFournis() != null) {
				((EOFournis)getCompte().toIndividu().toFournis()).setCptOrdre(null);
			}
		} else {
			if (getCompte().toStructure().toFournis() != null) {
				((EOFournis)getCompte().toStructure().toFournis()).setCptOrdre(null);
			}
		}
	}
	
	private EORepartCompte selectionnerRepartCompteAssocie(EOCompte compte) {
		if (compte.primaryKey() == null) {
			return null;
		}
		EOQualifier qualCptOrdre = new EOKeyValueQualifier(EORepartCompte.CPT_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, new Integer(compte. primaryKey()));
		EOQualifier qualPersId = new EOKeyValueQualifier(EORepartCompte.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, compte.persId());
		
		EOQualifier qualRepartCompte = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qualCptOrdre, qualPersId
		}));
		
		return EORepartCompte.fetchByQualifier(compte.editingContext(), qualRepartCompte);
	}

	public WOActionResults enregistrer() {
		try {
			boolean isCreationMode = (getCompte() != null) ? ERXEOControlUtilities.isNewObject(getCompte()) : false;
			if (getCompte() != null) {
				getCompte().setCptModificateur(getUtilisateurPersId());
			}
			if (MyStringCtrl.isEmpty(getCompte().cptDomaine())) {
				getCompte().setCptDomaine(getCompte().toVlans().domaine());
			}
			/**
			 * Ajout le 12/07/12 par A. Malaplate pour permettre aux administrateurs du SI de se cacher les MdP s'ils le souhaitent
			 */
//			if (isPasswordReseted()) {
//				boolean isEnClair = FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.COMPTE_PASSWORD_EN_CLAIR_ACTIVE);
//				getCompte().changePassword(getAppUserForAnnuaire(), getCompte().cptPasswdClair(), isEnClair);
//				setPasswordReseted(false);
//			}
			compteService.determinationCptePrincipal(edc(), getUtilisateurPersId());
			edc().saveChanges();
			setIsEditing(false);

			if (isCreationMode) {
				CktlAjaxWindow.open(context(), getPasswordModalWindowID());
			}

		} catch (ValidationException e) {
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			edc().revert();
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public String getPasswordModalWindowID() {
		return "passwordModalWindow";
	}

	public WOActionResults closePasswordModal() {
		CktlAjaxWindow.close(context(), getPasswordModalWindowID());
		return null;
	}

	public WOActionResults annuler() {
		edc().revert();
		setSelectedCompte(null);
		setIsEditing(false);
		return null;
	}


	/**
	 * @return Les comptes valides et non valides associes a la personne.
	 */
	public NSArray<EOCompte> getLesComptes() {
		NSArray<EOCompte> res = new NSArray<EOCompte>();
		if (getPersonne() != null) {
			res = getPersonne().toComptes(EOCompte.QUAL_CPT);
			if (getSelectedCompte() == null && res.count() > 0) {
				setSelectedCompte((EOCompte) res.objectAtIndex(0));
			}
			EOSortOrdering triValidite = ERXS.desc("isValideAsString");
			EOSortOrdering triPriorite = EOCompte.SORT_VLANS_PRIORITE;
			res = ERXS.sorted(res, triValidite, triPriorite);
		}
		return res;
	}

	public EOCompte getSelectedCompte() {
		EOCompte rpa = (EOCompte) valueForBinding(BINDING_selectedCompte);
		if (rpa != null && !edc().equals(rpa.editingContext())) {
			rpa = rpa.localInstanceIn(edc());
		}
		
		return rpa;
	}

	public void setSelectedCompte(EOCompte compte) {
		setValueForBinding(compte, BINDING_selectedCompte);
	}
	

	public EOTypeVpn getCurrentTypeVPN() {
		return currentTypeVPN;
	}

	public void setCurrentTypeVPN(EOTypeVpn currentTypeVPN) {
		this.currentTypeVPN = currentTypeVPN;
	}

	public EOVlans getCurrentVlan() {
		return currentVlan;
	}

	public void setCurrentVlan(EOVlans currentVlan) {
		this.currentVlan = currentVlan;
	}

	public boolean isCompteSelected() {
		return getSelectedCompte() != null;
	}
	
	public boolean isPasswordReseted() {
		return isPasswordReseted;
	}

	public void setPasswordReseted(boolean isPasswordReseted) {
		this.isPasswordReseted = isPasswordReseted;
	}

	public boolean showPopupComptes() {
		return Boolean.valueOf(getLesComptes().count() > 0 && isMultipleAllowed().booleanValue()) && !isEditing();
	}

	public boolean showEditItems() {
		return !isReadOnly() && isEditing();
	}

	public boolean showUid() {
		return isEditing() && isSaisieUnixInfos()
				|| !isEditing() && getSelectedCompte().cptUid() != null;
	}

	public boolean showGid() {
		if (getPersonne().isStructure() && !getSelectedCompte().toVlans().cVlan().equalsIgnoreCase("X")) {
			return true;
		}
		return isEditing() && isSaisieUnixInfos()
				|| !isEditing() && getSelectedCompte().cptGid() != null;
	}

	public boolean showShell() {
		return isEditing() && isSaisieUnixInfos()
				|| !isEditing() && getSelectedCompte().cptShell() != null;
	}

	public boolean showHome() {
		return isEditing() && isSaisieUnixInfos() 
				|| !isEditing() && getSelectedCompte().cptHome() != null;
	}

	public boolean showEmail() {
		return isEditing() || getSelectedCompte().toCompteEmail() != null;
	}

	private boolean isSaisieUnixInfos() {
		return getSelectedCompte() != null
				&& EOCompte.isSaisieUnixInfoObligatoire(getSelectedCompte().toVlans());
	}

	public boolean hasComptes() {
		return getLesComptes().count() > 0;
	}

	public boolean showEditButtons() {
		return !isReadOnly() && !isEditing() && getAppUserForAnnuaire().hasDroitCompteModification();
	}

	public boolean showDeleteButton() {
		return !isEditing() && isDeleting() && getAppUserForAnnuaire().hasDroitCompteSuppression();
	}

	public boolean readOnlyViewMail() {
		return isReadOnly() || !isEditing();
	}

	public EOCompte getCompte() {
		return getSelectedCompte();
	}

	public boolean autoriserCreation() {
		return vlansAutorisesPourCreation().count() > 0;
	}
	

	public Integer getValueUid() {
		return getSelectedCompte().cptUid();
	}

	public void setValueUid(Integer valueUid) {
			getSelectedCompte().setCptUid(valueUid);
		
	}

	public Integer getValueGid() {
		return getSelectedCompte().cptGid();
	}

	public void setValueGid(Integer valueGid) {
		getSelectedCompte().setCptGid(valueGid);
	}

	@SuppressWarnings({
			"unchecked", "rawtypes"
	})
	public NSArray<EOVlans> vlansAutorisesPourCreation() {
		if (hasBinding(BINDING_vlansAutorisesPourCreation)) {
			return (NSArray) valueForBinding(BINDING_vlansAutorisesPourCreation);
		}
		return defaultVlansAutorisesPourCreation();

	}

	@SuppressWarnings({
			"unchecked", "rawtypes"
	})
	public NSArray<EOVlans> defaultVlansAutorisesPourCreation() {
		if (vlansAutorisesPourCreationCompte == null) {
			vlansAutorisesPourCreationCompte = new NSArray(new Object[] {
					EOVlans.fetchByKeyValue(edc(), EOVlans.C_VLAN_KEY, EOVlans.VLAN_X)
			});
		}
		return vlansAutorisesPourCreationCompte;
	}

	public String getLibelleVlan() {
		return MyStringCtrl.capitalize(unVlanAutorise.llVlan());
	}

	public Boolean isCompactView() {
		return (Boolean) valueForBinding(BINDING_isCompactView);
	}

	public Boolean isMultipleAllowed() {
		return (Boolean) valueForBinding(BINDING_isMultipleAllowed);
	}
	
	

	public String getHasSignatureCharte() {
		if (MyStringCtrl.isEmpty(hasSignatureCharte)) {
			hasSignatureCharte = getSelectedCompte().cptCharte();
		}
		return hasSignatureCharte;
	}

	public void setHasSignatureCharte(String hasSignatureCharte) {
		getSelectedCompte().setCptCharte(hasSignatureCharte);
		if ("O".equals(hasSignatureCharte)) {
			getSelectedCompte().setCptDateCharte(new NSTimestamp());
		} else {
			getSelectedCompte().setCptDateCharte(null);
		}
		this.hasSignatureCharte = hasSignatureCharte;
	}

	public void resetEdc() {
		_editingContext = null;
	}

	public boolean hasDroitsSuppressionCompte() {
		boolean droits = false;

		if (isReadOnly().booleanValue()) {
			droits = false;
		} else if (getAppUserForAnnuaire().hasDroitCompteModification() && !isEditing()) {
			droits = true;
		}
		return droits;
	}

	public boolean isDeleting() {
		return hasDroitsSuppressionCompte();
	}
	
	public boolean isChartreSignedWithDate() {
		if ("O".equals(getSelectedCompte().cptCharte()) && getSelectedCompte().cptDateCharte() != null) {
			return true;
		}
		return false;
	}
	
	

	/*
	 * Gestion de la date de fin de validité
	 */

	public WOActionResults setExpirationFinJ() {
		setExpiration(getFinJ());
		setDateFinValidite();
		return null;
	}

	public WOActionResults setExpirationDemain() {
		setExpiration(getDemain());
		setDateFinValidite();
		return null;
	}

	public WOActionResults setExpirationFinSem() {
		setExpiration(getFinSem());
		setDateFinValidite();
		return null;
	}

	public WOActionResults setExpiration1Mois() {
		setExpiration(getUnMois());
		setDateFinValidite();
		return null;
	}

	public WOActionResults setExpiration6Mois() {
		setExpiration(getSixMois());
		setDateFinValidite();
		return null;
	}

	public WOActionResults setExpirationFinAnneeU() {
		setExpiration(getFinAnneeU());
		setDateFinValidite();
		return null;
	}

	private void setDateFinValidite() {
		if (getSelectedCompte() != null) {
			getSelectedCompte().setCptFinValide(getFinValidite());
		}
	}

	public String getFinJ() {
		return FIN_J;
	}

	public String getDemain() {
		return DEMAIN;
	}

	public String getFinSem() {
		return FIN_SEM;
	}

	public static String getUnMois() {
		return UN_MOIS;
	}

	public static String getSixMois() {
		return SIX_MOIS;
	}

	public static String getFinAnneeU() {
		return FIN_ANNEE_U;
	}

	public String getExpiration() {
		return expiration;
	}

	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}

	private NSTimestamp getFinValidite() {
		NSTimestamp now = new NSTimestamp();
		CAL.setTime(now);
		NSTimestamp expDate = now;
		if (expiration == FIN_J) {
			int hour = CAL.get(Calendar.HOUR_OF_DAY);
			int ecart = 24 - hour;
			CAL.clear(Calendar.MINUTE);
			CAL.clear(Calendar.SECOND);
			now = new NSTimestamp(CAL.getTime());
			expDate = now.timestampByAddingGregorianUnits(0, 0, 0, ecart, 0, 0);

		} else if (expiration == DEMAIN) {
			CAL.clear(Calendar.HOUR_OF_DAY);
			CAL.clear(Calendar.MINUTE);
			CAL.clear(Calendar.SECOND);
			now = new NSTimestamp(CAL.getTime());
			expDate = now.timestampByAddingGregorianUnits(0, 0, 2, 0, 0, 0);
		} else if (expiration == FIN_SEM) {
			int hour = CAL.get(Calendar.HOUR_OF_DAY);
			int day = CAL.get(Calendar.DAY_OF_WEEK);
			int ecart = Calendar.SATURDAY - day + 1;
			int ecartH = 24 - hour;
			CAL.clear(Calendar.MINUTE);
			CAL.clear(Calendar.SECOND);
			now = new NSTimestamp(CAL.getTime());
			expDate = now.timestampByAddingGregorianUnits(0, 0, ecart, ecartH, 0, 0);
		} else if (expiration == UN_MOIS) {
			CAL.clear(Calendar.HOUR_OF_DAY);
			CAL.clear(Calendar.MINUTE);
			CAL.clear(Calendar.SECOND);
			now = new NSTimestamp(CAL.getTime());
			expDate = now.timestampByAddingGregorianUnits(0, 1, 0, 0, 0, 0);
		} else if (expiration == SIX_MOIS) {
			CAL.clear(Calendar.MINUTE);
			CAL.clear(Calendar.SECOND);
			now = new NSTimestamp(CAL.getTime());
			expDate = now.timestampByAddingGregorianUnits(0, 6, 0, 0, 0, 0);
		} else if (expiration == FIN_ANNEE_U) {
			expDate = new NSTimestamp(MyDateCtrl.getEndDateOfUniversityYear(CAL.getTime()));
		}
		return expDate;
	}

	public String getTimestampFormatter() {
		return stringValueForBinding(BINDING_dateFormat, DEFAULT_TIMESTAMP_FORMATTER);
	}

	protected String uniqueDomId() {
		if (_id == null) {
			_id = ERXWOContext.safeIdentifierName(context(), true);
		}
		return _id;
	}

	public String getValiditePopUpId() {
		return "BoutonSelectionContainer_" + uniqueDomId();
	}

	public String unCompteLabel() {
		String label = "Compte(s) Invalide(s)";
		if (unCompte.isValideAsString().equals("O")) {
			label = "Compte(s) Valide(s)";
			if ((unCompte.cptFinValide() != null) && (unCompte.cptFinValide().before(new NSTimestamp()))) {
				label = "Compte(s) Invalide(s)";
			}
		}
		return label;
	}

	public String unCompteCSS() {
		String styleCSS = new String();
		if (unCompte.isValideAsString().equals("O")) {
			styleCSS = "ValiditeFondBlanc";
			if ((unCompte.cptFinValide() != null) && (unCompte.cptFinValide().before(new NSTimestamp()))) {
				styleCSS = "InvaliditeFondGris";
			}
		} else {
			styleCSS = "InvaliditeFondGris";
		}
		return styleCSS;
	}

	public String selectedCompteImageValidite() {
		String styleCSS = new String();
		if (getSelectedCompte().isValideAsString().equals("O")) {
			styleCSS = "images/16x16/001_36_16x16.png";
			if ((getSelectedCompte().cptFinValide() != null) && (getSelectedCompte().cptFinValide().before(new NSTimestamp()))) {
				styleCSS = "images/16x16/001_34_16x16.png";
			}
		} else {
			styleCSS = "images/16x16/001_34_16x16.png";
		}
		return styleCSS;
	}

	public String selectedCompteLabel() {
		String label = new String();
		if (getSelectedCompte().isValideAsString().equals("O")) {
			label = " Compte valide";
			if ((getSelectedCompte().cptFinValide() != null) && (getSelectedCompte().cptFinValide().before(new NSTimestamp()))) {
				label = "Compte invalide";
			}
		} else {
			label = "Compte invalide";
		}
		return label;
	}

	/**
	 * Action de mise en coherence avec la date de fin de validite s'il y a un changement dans les radios boutons.
	 */

	public void rbHasChanged() {
		mySession().addSimpleInfoMessage("Information", "Valeur du témoin de validité : " + getSelectedCompte().cptValide());
		if (getSelectedCompte().cptValide().equals("N")) {
			if (getSelectedCompte().cptFinValide() == null || getSelectedCompte().cptFinValide().after(new NSTimestamp())) {
				getSelectedCompte().setCptFinValide(new NSTimestamp());
			}

		} else {
			if (getSelectedCompte().cptFinValide() != null && getSelectedCompte().cptFinValide().before(new NSTimestamp())) {
				getSelectedCompte().setCptDebutValide(new NSTimestamp());
				getSelectedCompte().setCptFinValide(null);
			}
		}

	}

	public void dateFinValiditeHasChanged() {
		if (getSelectedCompte().cptFinValide() != null) {
			if (getSelectedCompte().cptFinValide().before(new NSTimestamp())) {
				if (getSelectedCompte().cptValide().equals("O")) {
					getSelectedCompte().setCptValide("N");
				}
			}
			if (getSelectedCompte().cptValide().equals("N")
					&& getSelectedCompte().cptFinValide().after(new NSTimestamp())) {
				getSelectedCompte().setCptValide("O");
			}

		}

	}
	
	public NSArray<EOVlans> listeVlansAccessibles() {
		listeVlansAccessibles = null;
		if (getPersonne().isIndividu()) {
			if (listeVlansAccessibles == null) {
			 listeVlansAccessibles = EOVlans.fetchAllVLansAPrendreEnCompte(edc());
	        }
	        return listeVlansAccessibles;
		} else {
			if (listeVlansAccessibles == null) {
				 listeVlansAccessibles = EOVlans.fetchAllVLansAPrendreEnComptePourStructure(edc(), getPersonne());
		        }
		        return listeVlansAccessibles;
		}
		 
	}
	
	 public NSArray<EOTypeVpn> listeVPN() {
	        if (listeTypeVPN == null) {
	        	listeTypeVPN = EOTypeVpn.fetchAll(edc());
	        }
	        return listeTypeVPN;
	    }
	
	public String getTypeVpnCode() {
		return getSelectedCompte().toTypeVpn().tvpnCode();
	}
	
	public void setTypeVpnCode(String value) {
		EOTypeVpn typeVpn = EOTypeVpn.fetchByKeyValue(edc(), EOTypeVpn.TVPN_CODE_KEY, value);
		if (typeVpn != null) {
			getSelectedCompte().setToTypeVpnRelationship(typeVpn);
		}
		
	}
	
	public boolean isNotVlanX() {
		if ("X".equals(getSelectedCompte().toVlans().cVlan())) {
			return false;
		}
		return true;
	}
	
	public WOActionResults resetPassword() {
		boolean stockageEnClair = true;
		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.COMPTE_PASSWORD_EN_CLAIR_ACTIVE)) {
			stockageEnClair = true;
		} else {
			// Rajout pour être sûr qu'il prend bien la valeur false
			stockageEnClair = false;
		}
		getSelectedCompte().reinitialisePassword(edc(), getAppUserForAnnuaire(), null, stockageEnClair);
		getSelectedCompte().setDModification(new NSTimestamp());
//		setPasswordReseted(true);
		CktlAjaxWindow.open(context(), getPasswordModalWindowID());
		return doNothing();
	}
	
	public String getIntervalUID() {
		Integer rangeMinUid = Integer.parseInt(EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY));
		Integer rangeMaxUid = Integer.parseInt(EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_UID_MAX_KEY));
		
		return "[" + rangeMinUid + "-" + rangeMaxUid + "]";
	}
	
	public String getIntervalGID() {
		Integer rangeMinGid = Integer.parseInt(EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY));
		Integer rangeMaxGid = Integer.parseInt(EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY));
		
		return "[" + rangeMinGid + "-" + rangeMaxGid + "]";
	}
	
	/************** Partie pour les domaines secondaires *************************/
	
	/**
	 * Liste des domaines secondaires
	 * @return la liste des domaines secondaires sur le Vlan P
	 */
	public NSArray<String> getListeDomainesSecondairesVlanP() {
		return getListeDomainesSecondaires(compteService.getDomainesSecondairesVlanP(edc()));
	}
	
	/**
	 * Liste des domaines secondaires
	 * @return la liste des domaines secondaires sur le Vlan E
	 */
	public NSArray<String> getListeDomainesSecondairesVlanE() {
		return getListeDomainesSecondaires(compteService.getDomainesSecondairesVlanE(edc()));
	}
	
	/**
	 * Liste des domaines secondaires
	 * @return la liste des domaines secondaires sur le Vlan R
	 */
	public NSArray<String> getListeDomainesSecondairesVlanR() {
		return getListeDomainesSecondaires(compteService.getDomainesSecondairesVlanR(edc()));
	}

	protected NSArray<String> getListeDomainesSecondaires(NSArray<String> liste) {
		listeDomainesSecondaires = null;
		NSMutableArray<String> listeTotalDomains = new NSMutableArray<String>();
		if (!MyStringCtrl.isEmpty(getCompte().toVlans().domaine()) && !getCompte().toVlans().domaine().equals("univ.fr")) {
			listeTotalDomains.add(getCompte().toVlans().domaine());
			listeTotalDomains.addObjectsFromArray(liste);
			listeDomainesSecondaires = listeTotalDomains.immutableClone();
		} else {
			listeDomainesSecondaires = NSArray.emptyArray();
		}
		return listeDomainesSecondaires;
	}
	
	/**
	 * Test d'existence de domaine(s) secondaire(s) sur le Vlan P
	 * @return true si un ou des domaines secondaires sont définis
	 */
	public boolean hasSecondDomainsVlanP() {
		if (EnvoiMessageErreurDomaineSecondaire() == false) {
			return false;
		}
		return hasSecondDomains(compteService.getDomainesSecondairesVlanP(edc()), getSelectedCompte().toVlans().cVlan()) && EOVlans.VLAN_P.equals(getSelectedCompte().toVlans().cVlan());
	}

	private boolean EnvoiMessageErreurDomaineSecondaire() {
		if (compteService == null) {
			mySession().addSimpleErrorMessage("ERREUR", "le compte service ne s'est pas initialisé");
			return false;
		}
		if (getSelectedCompte() == null) {
			mySession().addSimpleErrorMessage("ERREUR", "le compte sélectionné est NULL");
			return false;
		}
		if (getSelectedCompte().toVlans() == null) {
			mySession().addSimpleErrorMessage("ERREUR", "le compte sélectionné n'est pas lié à un VLAN");
			return false;
		}
		return true;
	}
	
	/**
	 * Test d'existence de domaine(s) secondaire(s) sur le Vlan E
	 * @return true si un ou des domaines secondaires sont définis
	 */
	public boolean hasSecondDomainsVlanE() {
		if (EnvoiMessageErreurDomaineSecondaire() == false) {
			return false;
		}
		return hasSecondDomains(compteService.getDomainesSecondairesVlanE(edc()), getSelectedCompte().toVlans().cVlan()) && EOVlans.VLAN_E.equals(getSelectedCompte().toVlans().cVlan());
	}
	
	/**
	 * Test d'existence de domaine(s) secondaire(s) sur le Vlan R
	 * @return true si un ou des domaines secondaires sont définis
	 */
	public boolean hasSecondDomainsVlanR() {
		if (EnvoiMessageErreurDomaineSecondaire() == false) {
			return false;
		}
		return hasSecondDomains(compteService.getDomainesSecondairesVlanR(edc()), getSelectedCompte().toVlans().cVlan()) && EOVlans.VLAN_R.equals(getSelectedCompte().toVlans().cVlan());
	}

	protected boolean hasSecondDomains(NSArray<String> liste, String cVlan) {
		boolean hasSecondsDomains = false;
		
		if (cVlan.equals("P") && getListeDomainesSecondairesVlanP().isEmpty()) {
			return hasSecondsDomains;
		}
		
		if (cVlan.equals("R") && getListeDomainesSecondairesVlanR().isEmpty()) {
			return hasSecondsDomains;
		}
		
		if (cVlan.equals("E") && getListeDomainesSecondairesVlanE().isEmpty()) {
			return hasSecondsDomains;
		}
		
		if (getCompte() == null || MyStringCtrl.isEmpty(getCompte().toVlans().domaine()) || getCompte().toVlans().domaine().equals("univ.fr")) {
			return hasSecondsDomains;
		}
		
		if (getCompte() != null && MyStringCtrl.isEmpty(getCompte().cptDomaine())) {
			return hasSecondsDomains;
		}
		
		if (getCompte() != null && cVlan.equals(getCompte().toVlans().cVlan())) {
			if (liste != null && liste.count() > 0) {
				if (liste.count() == 1 && liste.contains("univ.fr")) {
					return hasSecondsDomains;
				}
				return true;
			}
		}
		return hasSecondsDomains;
	}
	
	/**
	 * getter de currentDomaine
	 * @return la string contenue dans currentDomaine
	 */
	public String getCurrentDomaine() {
		return currentDomaine;
	}

	/**
	 * setter de currentDomaine
	 * @param unDomaine to set currentDomaine
	 */
	public void setCurrentDomaine(String unDomaine) {
		this.currentDomaine = unDomaine;
	}
	
	/**
	 * Méthode pour changer le domaine lié au compte email
	 */
	public void changeDomain() {
		EOCompteEmail.setNomSecondDomain(getCompte().cptDomaine());
	}
	
	/**
	 * Test d'être un groupe Unix ou Diplome
	 * @return true si le groupe est de type Unix ou Diplome
	 */
	public boolean isGroupeTypeUnixOuDipl() {
		boolean groupeTypeUnixOuDipl = false;
		if (getPersonne().isStructure()) {
			EOStructure structure = (EOStructure) getPersonne();
			NSArray<EORepartTypeGroupe> repartTypeGroupes = structure.toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE
					.inObjects(EOTypeGroupe.TGRP_CODE_D, EOTypeGroupe.TGRP_CODE_U));
			//		if (getPersonne().isStructure() && (getCurrentVlan().cVlan().equals("U")||getCurrentVlan().cVlan().equals("D")) ){
			if (getPersonne().isStructure() && !repartTypeGroupes.isEmpty()) {
				return true;
			}
		}
		
		return groupeTypeUnixOuDipl;
	}
	

	/**
	 * Est-ce que le compte a un affichage complet ?
	 * <p>
	 * Pour obtenir un affichage restreint, il faut que le paramètre 'Affichage restreint' soit à 'Oui'
	 * et que le composant ne soit pas en mode édition.
	 * 
	 * @return <code>true</code> si l'affichage doit être complet, <code>false</code> dans le cas d'un affichage restreint
	 */
	public boolean isAffichageComplet() {
		return (showEditItems() || !myApp().config().booleanForKey(FwkCktlPersonneParamManager.COMPTE_AFFICHAGE_RESTREINT_ACTIVE));
	}
}
