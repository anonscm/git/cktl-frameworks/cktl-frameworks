/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * Composant de listing et filtre des diplômes.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CktlAjaxDiplomeSelect extends AComponent {
    
    private static final long serialVersionUID = -3740917168320842287L;
    private static final String DIP = "currentDiplome";
    private static final String LC_KEY = EODiplome.LC_DIPLOME_KEY;
    private static final String LL_KEY = EODiplome.LL_DIPLOME_KEY;
    private static final int NB_DIP_PAR_PAGE = 20;
    private static final String BINDING_SELECTION = "selection";
    
    private String containerId;
    private ERXDisplayGroup displayGroup;
    private EODiplome currentDiplome;
    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private String filtre;
    
    public CktlAjaxDiplomeSelect(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public WOActionResults filtrer() {
        getDisplayGroup().setQualifier(ERXQ.contains(LL_KEY , filtre));
        getDisplayGroup().updateDisplayedObjects();
        return null;
    }
    
    public WOActionResults afficherTous() {
        filtre = null;
        getDisplayGroup().setQualifier(null);
        getDisplayGroup().updateDisplayedObjects();
        return null;
    }
    
    public WOActionResults selectionner() {
        setValueForBinding(getDisplayGroup().selectedObject(), BINDING_SELECTION);
        return null;
    }
    
    public String getContainerId() {
        if (containerId == null)
            containerId = "DipCont_" + ERXWOContext.safeIdentifierName(context(), true);
        return containerId;
    }
    
    public ERXDisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new ERXDisplayGroup();
            displayGroup.setObjectArray(EODiplome.fetchAll(edc()));
            displayGroup.setNumberOfObjectsPerBatch(NB_DIP_PAR_PAGE);
        }
        return displayGroup;
    }
    
    public NSArray<CktlAjaxTableViewColumn> getColonnes() {
        if (colonnes == null) {
            NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
            // Colonne Libelle
            CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
            col.setLibelle("Libellé court");
            col.setOrderKeyPath(LC_KEY);
            String keyPath = ERXQ.keyPath(DIP, LC_KEY);
            CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "emptyValue");
            col.setAssociations(ass);
            colTmp.add(col);
            // Colonne Libelle long
            col = new CktlAjaxTableViewColumn();
            col.setLibelle("Libellé long");
            col.setOrderKeyPath(LL_KEY);
            keyPath = ERXQ.keyPath(DIP, LL_KEY);
            ass = new CktlAjaxTableViewColumnAssociation(
                    keyPath, "emptyValue");
            col.setAssociations(ass);
            colTmp.add(col);
            colonnes = colTmp.immutableClone();
        }
        return colonnes;
    }
    
    public EODiplome getCurrentDiplome() {
        return currentDiplome;
    }
    
    public void setCurrentDiplome(EODiplome currentDiplome) {
        this.currentDiplome = currentDiplome;
    }
    
    public String getFiltre() {
        return filtre;
    }
    
    public void setFiltre(String filtre) {
        this.filtre = filtre;
    }
    
}