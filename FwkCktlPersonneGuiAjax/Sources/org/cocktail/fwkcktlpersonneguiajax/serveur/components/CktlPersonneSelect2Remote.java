package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.CktlAjaxSelect2RemoteDataProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.select2.Result;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.tri.PersonneTri;

import com.google.inject.Inject;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class CktlPersonneSelect2Remote extends CktlAjaxWOComponent {
	
	private static final long serialVersionUID = 1L;
	private static final String BINDING_SELECTION = "selection";
	private static final String BINDING_EDITING_CONTEXT = "editingContext";
	private static final String BINDING_SEARCH_TEXT = "searchText";
	private static final String BINDING_STYLE = "style";
	private static final String BINDING_DISABLED = "disabled";
	private static final String BINDING_ONLY_PERSONNEL_ACTUEL = "onlyPersonnelActuel";
	private static final String BINDING_QUALIFIER_STRUCTURES = "qualifierStructures";
	private static final String BINDING_QUALIFIER_INDIVIDUS = "qualifierIndividus";
	
	private CktlPersonneDataProvider personneDataProvider;
	
	@Inject
	private PersonneTri personneTri;
	
	/**
	 * @param context le contexte
	 */
	public CktlPersonneSelect2Remote(WOContext context) {
	    super(context);
	}
	
	@Override
	public EOEditingContext edc() {
	    return (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
	}

	
	
	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getDisabled() {
		if (valueForBinding(BINDING_DISABLED) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_DISABLED);
	}

	/**
	 * @return vrai si le composant est inactif
	 */
	public Boolean getOnlyPersonnelActuel() {
		if (valueForBinding(BINDING_ONLY_PERSONNEL_ACTUEL) == null) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding(BINDING_ONLY_PERSONNEL_ACTUEL);
	}
	
	public EOQualifier getQualifierIndividus() {
		return (EOQualifier) valueForBinding(BINDING_QUALIFIER_INDIVIDUS);
	}
	
	public EOQualifier getQualifierStructures() {
		return (EOQualifier) valueForBinding(BINDING_QUALIFIER_STRUCTURES);
	}
	
	/**
	 * @param disabled vrai si le composant est inactif
	 */
	public void setDisabled(Boolean disabled) {
		setValueForBinding(disabled, BINDING_DISABLED);
	}

	/**
	 * 
	 * @param onlyPersonnelActuel vrai si recherche uniquement sur les structures et les individus faisant partie du personnel actuel
	 */
	public void setOnlyPersonnelActuel(Boolean onlyPersonnelActuel) {
		setValueForBinding(onlyPersonnelActuel, BINDING_ONLY_PERSONNEL_ACTUEL);
	}
	
	public String getStyle() {
		return valueForStringBinding(BINDING_STYLE, StringUtils.EMPTY);
	}

	/**
	 * @param style : style css du composant
	 */
	public void setStyle(String style) {
		setValueForBinding(style, BINDING_STYLE);
	}

	
	
	/**
	 * 
	 * @return le paysProvider
	 */
	public CktlPersonneDataProvider getPersonneDataProvider() {
	    if (personneDataProvider == null) {
	        personneDataProvider = new CktlPersonneDataProvider(edc(), getQualifierStructures(), getQualifierIndividus(), getOnlyPersonnelActuel()) {
	            
	            @Override
	            public void setSelectedPersonne(IPersonne individu) {
	                setSelection(individu);
	            }
	            
	            @Override
	            public IPersonne getSelectedPersonne() {
	                return getSelection();
	            }
	            
	        };
	    }
	    return personneDataProvider;
	}
	
	public IPersonne getSelection() {
	    return (IPersonne) valueForBinding(BINDING_SELECTION);
	}
	
	/**
	 * @param selection la selection
	 */
	public void setSelection(IPersonne selection) {
	    setValueForBinding(selection, BINDING_SELECTION);
	}
	
	/**
	 * @return le texte à afficher dans l'invite de la searchbox
	 */
	public String searchText() {
	    return stringValueForBinding(BINDING_SEARCH_TEXT, "Rechercher une personne");
	}
	
	/**
	 * Provider pour la recherche de personne
	 *
	 */
	public abstract class CktlPersonneDataProvider implements CktlAjaxSelect2RemoteDataProvider {
	
		protected final Integer MAX_SEARCH_RESULT = 100;
		
	    private EOEditingContext editingContext;
	    private EOQualifier qualifierStructures;
	    private EOQualifier qualifierIndividus;
	    private Boolean onlyInPersonnelActuel;
	    
	    /**
	     * @param editingContext l'editingContext
	     */
	    public CktlPersonneDataProvider(EOEditingContext editingContext, EOQualifier qualifierStructures, EOQualifier qualifierIndividus, Boolean onlyInPersonnelActuel) {
	        this.editingContext = editingContext;
	        this.qualifierStructures = qualifierStructures;
	        this.qualifierIndividus = qualifierIndividus;
	        this.onlyInPersonnelActuel = onlyInPersonnelActuel;
	    }
	    
	    /**
	     * @param searchTerm le mot clef
	     * @return la liste des individus dont le nom ou le prénom contient la chaine passée.
	     * 
	     * 
	     * 
	     */
	    public List<Result> results(String searchTerm) {
	        List<Result> resultat = new ArrayList<Result>();
	        
	        NSArray<EOIndividu> listeIndividu = EOIndividu.findIndividuForNomOrPrenom(editingContext, searchTerm, qualifierIndividus, onlyInPersonnelActuel);
	        NSArray<EOStructure> listeStructure = EOStructure.structuresInternesByNameAndSiret(editingContext, searchTerm, null, qualifierStructures, MAX_SEARCH_RESULT);
			
	        List<IPersonne> listePersonne = new ArrayList<IPersonne>();
	        
	        listePersonne.addAll(listeIndividu);
	        listePersonne.addAll(listeStructure);
	        
	        personneTri.trierParLibelleAffichage(listePersonne);
	        
	        for (IPersonne personne : listePersonne) {
	            resultat.add(new Result(personne.persId().toString(), personne.getNomPrenomAffichage()));
	        }
	        return resultat;
	    }
	
	    /**
	     * @param idSelection l'id de la liste selectionné par l'utilisateur
	     * @return l'individu correspondant à l'id sélectionné
	     */
	    public Result onSelect(String idSelection) {
	        // cas de la suppression
	        if (StringUtils.isEmpty(idSelection)) {
	            setSelectedPersonne(null);
	            return null;
	        }
	        EOIndividu individu = EOIndividu.fetchByKeyValue(editingContext, EOIndividu.PERS_ID_KEY, Integer.valueOf(idSelection));
	        if (individu != null) {
	        	setSelectedPersonne(individu);
	            return new Result(individu.persId().toString(), individu.nomPrenom());
	        } else {
	        	EOStructure structure = EOStructure.structureWithPersId(edc(), Integer.valueOf(idSelection));
	        	if (structure != null) {
		        	setSelectedPersonne(structure);
		        	return new Result(structure.persId().toString(), structure.getNomPrenomAffichage());
	        	} else {
	        		return null;
	        	}
	        }

	    }
	
	    /**
	     * @return la sélection initiale
	     */
	    public Result selectionInitiale() {
	        if (getSelectedPersonne() == null) {
	            return null;
	        } else {
	            return new Result(getSelectedPersonne().persId().toString(), getSelectedPersonne().getNomCompletAffichage());
	        }
	    }
	    
	    /**
	     * @param individu l'individu qui doit être sélectionné
	     */
	    public abstract void setSelectedPersonne(IPersonne individu);
	    
	    /**
	     * @return l'individu sélectionné
	     */
	    public abstract IPersonne getSelectedPersonne();
	    
	}

	
}