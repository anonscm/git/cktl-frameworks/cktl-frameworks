package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxFetchAndSelectComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EOBanque;

import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOSQLExpression;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.jdbc.ERXSQLHelper;

public class CktlAjaxBanqueFetchAndSelect extends CktlAjaxFetchAndSelectComponent {

	private static final long serialVersionUID = 1111L;
	private boolean isClosedAgence;

	public CktlAjaxBanqueFetchAndSelect(WOContext context) {
		super(context);
	}

	/**
	 * Met à jour les objets à afficher en effectuant une requete sql.
	 */
	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public void updateDisplayedObjects() {
		if (logger.isDebugEnabled()) {
			logger.debug("Recherche de banque avec qualifier =" + getQualifier());
		}
		EOQualifier banqueQual = getQualifier();

		if (!isClosedAgence()) {
			banqueQual = ERXQ.and(banqueQual, EOBanque.getQualifierForBanquesValides());
		}

		//		EOFetchSpecification spec = new EOFetchSpecification(getEntityName(), getQualifier(), getSortOrderings());
		EOFetchSpecification spec = new EOFetchSpecification(getEntityName(), banqueQual, getSortOrderings());
		EOModel model = EOModelGroup.defaultGroup().entityNamed(getEntityName()).model();
		ERXSQLHelper sqlHelper = ERXSQLHelper.newSQLHelper(edc(), model.name());
		int start = (numPage().intValue() - 1) * numberOfObjectsPerBatch();
		int end = start + numberOfObjectsPerBatch();

		NSMutableArray<EOAttribute> attributes = new NSMutableArray<EOAttribute>();
		attributes.addObjectsFromArray(getAttributes());
		attributes.addObjectsFromArray(EOModelGroup.defaultGroup().entityNamed(getEntityName()).primaryKeyAttributes());

		//Recuperer le nombre total d'enregistrements
		int maxRows = ERXEOAccessUtilities.rowCountForFetchSpecification(edc(), spec);
		maxPage = BigDecimal.valueOf(maxRows).divide(BigDecimal.valueOf(numberOfObjectsPerBatch()), BigDecimal.ROUND_HALF_UP).intValue();
		if (maxPage.intValue() == 0 && maxRows > 0) {
			maxPage = Integer.valueOf(1);
		}

		//Recuperer les enregistrements
		EOSQLExpression sqlExpr = sqlHelper.sqlExpressionForFetchSpecification(edc(), spec, start, end, attributes);
		NSArray<NSDictionary> result = ERXEOAccessUtilities.rawRowsForSQLExpression(edc(), model, sqlExpr, attributes);

		dg().setObjectArray(result);
		if (!result.isEmpty()) {
			dg().setSelectedObject(result.get(0));
			selectionner();
		}
		dg().updateDisplayedObjects();
	}

	public boolean isClosedAgence() {
		return isClosedAgence;
	}

	public void setIsClosedAgence(boolean isClosedAgence) {
		this.isClosedAgence = isClosedAgence;
	}

}