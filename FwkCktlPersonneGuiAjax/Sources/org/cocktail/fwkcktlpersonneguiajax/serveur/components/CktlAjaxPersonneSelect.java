/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxActionButton;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;

import er.ajax.CktlAjaxUtils;

/**
 * Composant pour afficher un champ disabled + un bouton de recherche qui affiche une fenetre modale de recherche/selection de personne. Une fois
 * selectionnee, le nom de la personne est affichee dans le champ.<br>
 * Ce composant fait appel a {@link CktlAjaxPersonneSrchForWin}.
 * 
 * @binding showRadioInterneExterne
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxPersonneSelect extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String SELECTION_BDG = "selection";
	public static final String EDC_BDG = "editingContext";

	/**
	 * Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer des individus.
	 */
	public static final String INDIVIDUS_QUALIFIER_BDG = "individusQualifier";

	/**
	 * Facultatif. Binding pour specifier le qualifier a appliquer pour recuperer les structures.
	 */
	public static final String STRUCTURES_QUALIFIER_BDG = "structuresQualifier";

	/** Facultatif. Binding pour specifier la taille du champ (en colonnes). */
	public static final String TEXT_FIELD_SIZE_BDG = "textFieldSize";

	/** Facultatif. Binding pour specifier la value du champ (affichage). */
	public static final String TEXT_FIELD_VALUE_BDG = "value";

	public static final String LIST_VIEW_TITLE_BDG = "listViewTitle";
	public static final String LIST_VIEW_WIDTH_BDG = "listViewWidth";
	public static final String LIST_VIEW_HEIGHT_BDG = "listViewHeight";
	public static final String LIST_VIEW_CLASS_NAME_BDG = "listViewClassName";
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";
	public static final String BINDING_showStructures = "showStructures";
	public static final String BINDING_showIndividus = "showIndividus";
	public static final String BINDING_qualifierForStructures = "qualifierForStructures";
	public static final String BINDING_qualifierForIndividus = "qualifierForIndividus";
	public static final String BINDING_qualifierForGroupes = "qualifierForGroupes";
	public static final String BINDING_showPersonnesExternes = "showPersonnesExternes";
	public static final String BINDING_showPersonnesInternes = "showPersonnesInternes";
	public static final String BINDING_showRadioInterneExterne = "showRadioInterneExterne";
	public static final String BINDING_showResetButton = "showResetButton";
	public static final String BINDING_individuTableViewColonneKeys = "individuTableViewColonneKeys";
	
	/** big ou small (taille de l'icone rechercher) */
	public static final String BINDING_iconSize = "iconSize";

	/** Affiche le champ texte */
	public static final String BINDING_showField = "showField";

	/** Type du bouton pour l'ajout ({@link CktlAjaxActionButton#TYPE_NEW}, {@link CktlAjaxActionButton#TYPE_FIND}, etc.) */
	public static final String BINDING_btType = "btType";
	public static final String DEFAULT_btType = CktlAjaxActionButton.TYPE_FIND;

	/** ID de la form dans laquelle doit s'executer le composant. */
	public static final String FORM_ID_BDG = "formID";

	private IPersonne selection;
	private Boolean wantResetSrch;
	private final CktlAjaxPersonneSrchForWin pagePersonneSrch = (CktlAjaxPersonneSrchForWin) pageWithName(CktlAjaxPersonneSrchForWin.class.getName());

	public CktlAjaxPersonneSelect(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public IPersonne selection() {
		selection = (IPersonne) valueForBinding(SELECTION_BDG);
		return selection;
	}

	public void setSelection(IPersonne selection) {
		this.selection = selection;
		setValueForBinding(selection, SELECTION_BDG);
	}

	public WOActionResults openWinSelection() {
		pagePersonneSrch.setUtilisateurPersId(getUtilisateurPersId());
		pagePersonneSrch.setContainerOnCloseID(winPersonneSelectionContainerOnCloseID());
		pagePersonneSrch.setWinId(winPersonneSelectionID());
		pagePersonneSrch.setShowIndividus(showIndividus());
		pagePersonneSrch.setShowStructures(showStructures());
		pagePersonneSrch.setShowPersonnesExternes(showPersonnesExternes());
		pagePersonneSrch.setShowPersonnesInternes(showPersonnesInternes());
		pagePersonneSrch.setQualifierForIndividus(getQualifierForIndividus());
		pagePersonneSrch.setQualifierForStructures(getQualifierForStructures());
		pagePersonneSrch.setQualifierForGroupes(qualifierForGroupes());
		pagePersonneSrch.setShowRadioInterneExterne(showRadioInterneExterne());
		pagePersonneSrch.setWantReset(true);
		pagePersonneSrch.setEdc(edc());
		pagePersonneSrch.setParentComponent(context().page());
		pagePersonneSrch.setIndividuTableViewColonneKeys(individuTableViewColonneKeys());
		return pagePersonneSrch;
	}

	public String onClickRechercher() {

		String onSuccessRechercher = null;
		//this.href : url de l'action du bouton (openWinSelection)
		onSuccessRechercher = "openWinSelect(this.href,";
		onSuccessRechercher += "'" + winPersonneSelectionID() + "','";

		if (valueForBinding(LIST_VIEW_TITLE_BDG) != null) {
			onSuccessRechercher += CktlAjaxUtils.jsEncode((String) valueForBinding(LIST_VIEW_TITLE_BDG)) + "', true";
		} else {
			onSuccessRechercher += "Choisir un element', true";
		}

		if (valueForBinding(LIST_VIEW_WIDTH_BDG) != null) {
			onSuccessRechercher += ", " + valueForBinding(LIST_VIEW_WIDTH_BDG);
		} else {
			onSuccessRechercher += ", null";
		}
		if (valueForBinding(LIST_VIEW_HEIGHT_BDG) != null) {
			onSuccessRechercher += ", " + valueForBinding(LIST_VIEW_HEIGHT_BDG);
		} else {
			onSuccessRechercher += ", null";
		}
		if (valueForBinding(LIST_VIEW_CLASS_NAME_BDG) != null) {
			onSuccessRechercher += ", '" + valueForBinding(LIST_VIEW_CLASS_NAME_BDG) + "'";
		} else {
			if (session() instanceof CocktailAjaxSession) {
				String windowsClassName = (String) session().valueForKey("windowsClassName");
				if (windowsClassName != null && !windowsClassName.equals("")) {
					onSuccessRechercher += ", '" + session().valueForKey("windowsClassName") + "'";
				} else {
					onSuccessRechercher += ", null";
				}
			} else {
				onSuccessRechercher += ", null";
			}
		}
		if (valueForBinding(UPDATE_CONTAINER_ID_BDG) != null) {
			//onSuccessRechercher += ", '"+valueForBinding(UPDATE_CONTAINER_ID_BDG)+"'";
			onSuccessRechercher += ", null";
		} else {
			onSuccessRechercher += ", null";
		}
		onSuccessRechercher += ");return false;";

		return onSuccessRechercher;
	}

	public Boolean wantResetSrch() {
		return wantResetSrch;
	}

	public Boolean setWantResetSrch(Boolean b) {
		return wantResetSrch = b;
	}

	public EOQualifier getQualifier() {
		//		EOQualifier qual = getTreeQualifier();
		//		if (qual != null) {
		//			if (getUserFiltreQualifier() != null) {
		//				qual = new EOAndQualifier(new NSArray(new Object[]{qual, getUserFiltreQualifier()}));
		//			}
		//		}
		//		else {
		//			qual = getUserFiltreQualifier();
		//		}
		//		return qual;
		return null;
	}

	/** ID du container pour afficher la valeur selectionnee */
	public String containerPersonneSelectionneId() {
		return getComponentId() + "_selection";
	}

	public Integer textFieldSize() {
		if (hasBinding(TEXT_FIELD_SIZE_BDG)) {
			return (Integer) valueForBinding(TEXT_FIELD_SIZE_BDG);
		}
		return Integer.valueOf(50);
	}

	public String textFieldValue() {
		if (hasBinding(TEXT_FIELD_VALUE_BDG)) {
			return (String) valueForBinding(TEXT_FIELD_VALUE_BDG);
		}
		return (selection() != null ? selection().getNomPrenomAffichage() : null);
	}
	
	/** Appele lors de la fermeture de la fenetre de selection */
	public WOActionResults winPersonneSelectionContainerOnClose() {
		if (CktlAjaxPersonneSrchForWin.ACTION_OK.equals(pagePersonneSrch.getLastAction())) {
			setSelection(pagePersonneSrch.getSelection());
			pagePersonneSrch.setLastAction(null);
		}
		return null;
	}

	/** ID de la fenetre de selection */
	private String winPersonneSelectionID() {
		return getComponentId() + "_personneSrch_win";
	}

	/**
	 * ID du container mis a jour lors de la fermeture de la fenetre de selection
	 */
	public String winPersonneSelectionContainerOnCloseID() {
		return winPersonneSelectionID() + "_containerOnClose";
	}

	/**
	 * Renvoie la fonction JS a executer sur le onComplete de winPersonneSelectionContainerOnCloseID
	 */
	public String winPersonneSelectionContainerOnCloseOncomplete() {
		return "function () {" + (valueForBinding(UPDATE_CONTAINER_ID_BDG) != null ? (String) valueForBinding(UPDATE_CONTAINER_ID_BDG) : containerPersonneSelectionneId()) + "Update();}";
	}

	public Boolean showIndividus() {
		if (hasBinding(BINDING_showIndividus)) {
			return (Boolean) valueForBinding(BINDING_showIndividus);
		}
		return Boolean.TRUE;
	}

	public Boolean showStructures() {
		if (hasBinding(BINDING_showStructures)) {
			return (Boolean) valueForBinding(BINDING_showStructures);
		}
		return Boolean.TRUE;
	}

	public Boolean showPersonnesExternes() {
		if (hasBinding(BINDING_showPersonnesExternes)) {
			return (Boolean) valueForBinding(BINDING_showPersonnesExternes);
		}
		return Boolean.TRUE;
	}

	public Boolean showPersonnesInternes() {
		if (hasBinding(BINDING_showPersonnesInternes)) {
			return (Boolean) valueForBinding(BINDING_showPersonnesInternes);
		}
		return Boolean.TRUE;
	}

	public Boolean showField() {
		if (hasBinding(BINDING_showField)) {
			return (Boolean) valueForBinding(BINDING_showField);
		}
		return Boolean.TRUE;
	}

	public String iconSize() {
		if (hasBinding(BINDING_iconSize)) {
			return (String) valueForBinding(BINDING_iconSize);
		}
		return CktlAjaxActionButton.ICON_SIZE_BIG;
	}

	public String btType() {
		if (hasBinding(BINDING_btType)) {
			return (String) valueForBinding(BINDING_btType);
		}
		return DEFAULT_btType;
	}

	public String personneSrchDialogId() {
		return getComponentId() + "_" + "personneSrchDialogId";
	}

	public String individuTableViewColonneKeys() {
		return stringValueForBinding(BINDING_individuTableViewColonneKeys, null);
	}
	
	public WOActionResults onOk() {
		CktlAjaxWindow.close(context());
		return null;
	}

	public WOActionResults onCancel() {
		CktlAjaxWindow.close(context());
		return null;
	}

	public void resetPersonneSrchModalDialog() {
	}

	public EOQualifier getQualifierForStructures() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForStructures);
	}

	public EOQualifier getQualifierForIndividus() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForIndividus);
	}

	public EOQualifier qualifierForGroupes() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForGroupes);
	}

	public Boolean showRadioInterneExterne() {
		return (Boolean) valueForBinding(BINDING_showRadioInterneExterne);
	}
	
	public Boolean showResetButton() {
		return booleanValueForBinding(BINDING_showResetButton, false);
	}
	
	public WOActionResults raz() {
		setSelection(null);
		return doNothing();
	}

}
