/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOContext;

/**
 * Affiche les details d'une personne. Utilisez les bindings pour indiquer ce qui doit etre affiche ou pas.
 * 
 * @binding personne
 * @binding showNom
 * @binding showStructureRattachement
 * @binding showPersonneAdminUI
 * @binding showPersonneAdressesUI
 * @binding showPersonneTelephoneUI
 * @binding showStructureEffectifUI
 * @binding showStructureCaUI
 * @binding showPersonneGroupesUI
 * @binding showGroupeAdminUI
 * @binding showStructureElementUI
 * @binding showCompteUI
 * @binding wantRefreshGroupes
 * @binding utilisateurPersId
 * @binding editingContext
 * @author rprin
 */
public class PersonneDetailView extends AComponent {

	private static final long serialVersionUID = 1L;

	public static final String BINDING_personne = "personne";

	public static final String BINDING_showNom = "showNom";
	public static final Boolean DEFAULT_showNom = Boolean.FALSE;

	public static final String BINDING_showStructureRattachement = "showStructureRattachement";
	public static final Boolean DEFAULT_showStructureRattachement = Boolean.FALSE;

	public static final String BINDING_showPersonneAdmin = "showPersonneAdminUI";
	public static final Boolean DEFAULT_showPersonneAdmin = Boolean.FALSE;

	public static final String BINDING_showPersonneAdresses = "showPersonneAdressesUI";
	public static final Boolean DEFAULT_showPersonneAdresses = Boolean.TRUE;

	public static final String BINDING_showPersonneTelephone = "showPersonneTelephoneUI";
	public static final Boolean DEFAULT_showPersonneTelephone = Boolean.TRUE;

	public static final String BINDING_showStructureEffectif = "showStructureEffectifUI";
	public static final Boolean DEFAULT_showStructureEffectif = Boolean.FALSE;

	public static final String BINDING_showStructureCa = "showStructureCaUI";
	public static final Boolean DEFAULT_showStructureCa = Boolean.FALSE;

	public static final String BINDING_showPersonneGroupes = "showPersonneGroupesUI";
	public static final Boolean DEFAULT_showPersonneGroupes = Boolean.FALSE;

	public static final String BINDING_showGroupeAdmin = "showGroupeAdminUI";
	public static final Boolean DEFAULT_showGroupeAdmin = Boolean.FALSE;

	public static final String BINDING_showStructureElement = "showStructureElementUI";
	public static final Boolean DEFAULT_showStructureElement = Boolean.FALSE;

	public static final String BINDING_showCompteUI = "showCompteUI";
	public static final Boolean DEFAULT_showCompteUI = Boolean.FALSE;

	public static final String BINDING_wantRefreshGroupes = "wantRefreshGroupes";
	public static final Boolean DEFAULT_wantRefreshGroupes = Boolean.FALSE;

	private IPersonne _personne;
	public EOCompte selectedCompte;

	public PersonneDetailView(WOContext context) {
		super(context);
	}

	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (pers == null) {
			changePersonne(null);
			//_personne = null;
		}
		else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			//_personne = pers;
			changePersonne(pers);
		}
		return _personne;
	}

	private void changePersonne(IPersonne newPers) {
		_personne = newPers;
		selectedCompte = null;
	}

	public EOStructure getStructure() {
		if (isStructure().booleanValue()) {
			return (EOStructure) getPersonne();
		}
		return null;
	}

	public EOIndividu getIndividu() {
		if (!isStructure().booleanValue()) {
			return (EOIndividu) getPersonne();
		}
		return null;
	}

	public Boolean isStructure() {
		return getPersonne() != null && getPersonne().isStructure();
	}

	public Boolean isIndividu() {
		return getPersonne() != null && getPersonne().isIndividu();
	}

	public String getStructureRattachement() {
		if (getStructure() != null) {
			String res = "";
			EOStructure struct = getStructure().toStructurePere();
			do {
				if (struct != null) {
					res = struct.llStructure() + " > " + res;
					struct = struct.toStructurePere();
				}
			} while (struct != null && !struct.cStructure().equals(struct.cStructurePere()));
			return res;
		}
		return null;
	}

	/**
	 * @return Le nom de la personne selectionnée sous la forme Nom (<i>nom patronymique</i>), <i>prenom</i>) N°
	 */
	public String personneNom() {
		if (getPersonne() != null) {
			return getPersonne().getNomCompletAffichage() + " (N° " + getPersonne().getNumero() + ")";
		}
		return "";
	}

	public String groupeAdminTitle() {
		return "Administration du groupe " + getPersonne().persLibelleAffichage();
	}

	public String structureElementsTitle() {
		return "Membres du groupe " + getPersonne().persLibelleAffichage();
	}

	public String personneGroupesTitle() {
		return "Groupes dont d&eacute;pend " + getPersonne().getNomCompletAffichage();
	}

	public Boolean wantRefreshGroupes() {
		return booleanValueForBinding(BINDING_wantRefreshGroupes, DEFAULT_wantRefreshGroupes);
	}

	public void setWantRefreshGroupes(Boolean value) {
		setValueForBinding(value, BINDING_wantRefreshGroupes);
	}

	public Boolean showNom() {
		return booleanValueForBinding(BINDING_showNom, DEFAULT_showNom);
	}

	public Boolean showStructureRattachement() {
		return booleanValueForBinding(BINDING_showStructureRattachement, DEFAULT_showStructureRattachement);
	}

	public Boolean showPersonneAdmin() {
		return booleanValueForBinding(BINDING_showPersonneAdmin, DEFAULT_showPersonneAdmin);
	}

	public Boolean showPersonneAdresses() {
		return booleanValueForBinding(BINDING_showPersonneAdresses, DEFAULT_showPersonneAdresses);
	}

	public Boolean showPersonneTelephone() {
		return booleanValueForBinding(BINDING_showPersonneTelephone, DEFAULT_showPersonneTelephone);
	}

	public Boolean showStructureEffectif() {
		return booleanValueForBinding(BINDING_showStructureEffectif, DEFAULT_showStructureEffectif);
	}

	public Boolean showStructureCa() {
		return booleanValueForBinding(BINDING_showStructureCa, DEFAULT_showStructureCa);
	}

	public Boolean showPersonneGroupes() {
		return booleanValueForBinding(BINDING_showPersonneGroupes, DEFAULT_showPersonneGroupes);
	}

	public Boolean showGroupeAdmin() {
		return booleanValueForBinding(BINDING_showGroupeAdmin, DEFAULT_showGroupeAdmin);
	}

	public Boolean showStructureElement() {
		return booleanValueForBinding(BINDING_showStructureElement, DEFAULT_showStructureElement);
	}

	public Boolean showCompteUI() {
		return booleanValueForBinding(BINDING_showCompteUI, DEFAULT_showCompteUI);
	}

	public Boolean isPersonne() {
		return Boolean.valueOf(getPersonne() != null);
	}

}
