/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxUserQuestionDelegate;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneSrchCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;

/**
 * Composant qui permet de gerer la recherche d'une personne. Permet aussi d'activer ou non la creation/modification de personne. Ce composant est un
 * sous-composant de {@link PersonneGestion}. Possibilités de paramétrage :
 * <ul>
 * <li>Les bindings</li>
 * <li>Le controleur (pour personnaliser la recherche et/ou la création) via le binding ctrl</li>
 * </ul>
 * 
 * @binding callbackOnCreerPersonne
 * @binding callbackOnModifierPersonne
 * @binding callbackOnSelectionnerPersonne
 * @binding ctrl Facultatif. Controleur a utiliser. Doit heriter de {@link PersonneSrchCtrl}.
 * @binding disableSelection Facultatif. Affiche ou non un bouton pour selectionner la personne (a partir du detail).
 * @binding displayGroup Facultatif. S'il n'est pas précisé, il est créé.
 * @binding fetchLimit Facultatif. Nombre d'enregistrements à ramener par fetch (ne correspond pas au nombre de lignes affichées dans la tableview,
 *          mais au nombre d'enregistrements renvoyés par requete - il y a plusieurs requetes à la suite par recherche). cf.
 *          {@link #DEFAULT_fetchLimit}.
 * @binding forceRecherche
 * @binding individuTableViewColonneKeys Facultatif. Liste des colonnes à afficher pour la tableView résultat des individus. Cf.
 *          {@link PersonneTableView}.
 * @binding isReadOnly True par defaut
 * @binding persType Facultatif. Indique au composant de préselectionner une type (individu / structure) par défaut. Valable si les radios boutons
 *          sont affichés.
 * @binding persTypeInterneExterne Facultatif. Indique au composant de préselectionner une type (interne / externe) par défaut. Valable si les radios
 *          boutons sont affichés.
 * @binding qualifierForGroupes
 * @binding qualifierForIndividus Facultatif. Qualifier optionnel a appliquer lors de la recherche d'individus.
 * @binding qualifierForStructures Facultatif. Qualifier a appliquer lors de la recherche de structures
 * @binding qualifierForTypeAdresse Facultatif. Qualifier a appliquer pour les types d'adresse a afficher. Si null, le qualifier par defaut sera
 *          appliqué.
 * @binding qualifierForTypeTel Facultatif. Qualifier a appliquer pour les types de telephone a afficher. Si null, le qualifier par defaut sera
 *          appliqué.
 * @binding resultListHeight Facultatif. Hauteur des tableViews de résultats. (ex. 300px ou 50%) Cf. {@link #DEFAULT_resultListHeight}.
 * @binding selectionnerPersonneLabel Facultatif. Libellé affiché pour le lien qui permet de selectionner la personne affichee.
 * @binding showCodeFournisInStructureForm Faciltatif. True par defaut. Indique s'il faut afficher le champ "code fournisseur" dans le formulaire de
 *          recherche de personne morales.
 * @binding showCompteUI Facultatif. Indique s'il faut afficher les informations sur les comptes dans le détail de la personne.
 * @binding showDetail Facultatif. Indique au composant d'afficher le detail de la personne selectionnee dans la liste.
 * @binding showDetailInModalBox Facultatif. Indique au composant d'afficher dans une fenetre modale le detail de la personne selectionnee dans la
 *          liste.
 * @binding showEmailInIndividuForm Facultatif. Indique s'il faut afficher le champ d'adresse email dans le formulaire de recherche d'un individu.
 *          (l'adresse email est obligatoire si cet indicateur est vrai).
 * @binding showStudentNumberInIndividuForm Facultatif. Indique s'il faut afficher le champ de numéro d'étudiant dans le formulaire de recherche d'un individu.
 * @binding showIndividus Facultatif. Indique au composant d'afficher le formulaire de recherche spécifique aux individus (personnes physiques) et
 *          donc de rechercher dans cet ensemble.
 * @binding showPersonnesExternes Facultatif. Indique au composant de rechercher sur les personnes externes a l'etablissement
 * @binding showPersonnesInternes Facultatif. Indique au composant de rechercher sur les personnes internes a l'etablissement
 * @binding showRadioInterneExterne Facultatif. Indique au composant d'afficher les boutons radios pour séparer la recherche (dans le cas ou
 *          showPersonnesInternes ET showPersonnesExternes sont à true). Si False et que showPersonnesInternes ET showPersonnesExternes sont à true,
 *          la recherche est effectuée sur les deux types de population.
 * @binding showStructures Facultatif. Indique au composant d'afficher le formulaire de recherche specifique aux structures (personnes morales) et
 *          donc d'effectuer la recherche dans cet ensemble.
 * @binding structureTableViewColonneKeys Facultatif. Liste des colonnes à afficher pour la tableView résultat des structures. Cf.
 *          {@link PersonneTableView}.
 * @binding titre Facultatif. Specifie le titre a afficher au dessus du formulaire de recherche.
 * @binding updateContainerIDOnSelectionnerPersonneInTableview
 * @binding userInstructions Facultatif. Specifie les instructions a afficher au dessus des champs de recherche
 * @binding wantRefreshDetail
 * @binding wantReset
 * @binding selectedPersonne Obligatoire. IPersonne sélectionnée.
 * @binding selectionMultipleEnabled Facultatif. Si true, permet la sélection multiple. Charge au développeur de récupérer les objets sélectionnés
 *          avec le displayGroup.
 * @binding champNom pré-remplissage du champ nom de la personne et lancement de la recherche
 * @binding champPrenom pré-remplissage du champ prenom de l'individu
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class PersonneSrch extends AComponent {

	private static final long serialVersionUID = 1275738791787605158L;

	public static final Integer DEFAULT_fetchLimit = Integer.valueOf(20);
	public static final String DEFAULT_resultListHeight = "200px";
	public static final Boolean DEFAULT_showRadioInterneExterne = Boolean.TRUE;

	public static final String BINDING_callbackOnCreerPersonne = "callbackOnCreerPersonne";
	public static final String BINDING_callbackOnModifierPersonne = "callbackOnModifierPersonne";
	public static final String BINDING_callbackOnSelectionnerPersonne = "callbackOnSelectionnerPersonne";
	public static final String BINDING_selectedPersonne = "selectedPersonne";
	public static final String BINDING_updateContainerIDOnSelectionnerPersonneInTableview = "updateContainerIDOnSelectionnerPersonneInTableview";
	public static final String BINDING_wantRefreshDetail = "wantRefreshDetail";
	public static final String BINDING_wantReset = "wantReset";

	public static final String BINDING_forceRecherche = "forceRecherche";
	public static final String BINDING_qualifierForTypeAdresse = "qualifierForTypeAdresse";
	public static final String BINDING_qualifierForTypeTel = "qualifierForTypeTel";
	public static final String BINDING_qualifierForGroupes = "qualifierForGroupes";
	public static final String BINDING_qualifierForStructures = "qualifierForStructures";
	public static final String BINDING_qualifierForIndividus = "qualifierForIndividus";

	public static final String BINDING_isReadOnly = "isReadOnly";
	public static final String BINDING_showStructures = "showStructures";
	public static final String BINDING_showIndividus = "showIndividus";
	public static final String BINDING_showPersonnesInternes = "showPersonnesInternes";
	public static final String BINDING_showPersonnesExternes = "showPersonnesExternes";
	public static final String BINDING_showRadioInterneExterne = "showRadioInterneExterne";
	public static final String BINDING_showDetail = "showDetail";
	public static final String BINDING_showDetailInModalBox = "showDetailInModalBox";
	public static final String BINDING_disableSelection = "disableSelection";
	public static final String BINDING_selectionnerPersonneLabel = "selectionnerPersonneLabel";
	public static final String BINDING_displayGroup = "displayGroup";
	public static final String BINDING_titre = "titre";
	public static final String BINDING_userInstructions = "userInstructions";
	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_fetchLimit = "fetchLimit";
	public static final String BINDING_persType = "persType";
	public static final String BINDING_persTypeInterneExterne = "persTypeInterneExterne";
	public static final String BINDING_resultListHeight = "resultListHeight";
	public static final String BINDING_showCompteUI = "showCompteUI";
	public static final String BINDING_structureTableViewColonneKeys = "structureTableViewColonneKeys";
	public static final String BINDING_individuTableViewColonneKeys = "individuTableViewColonneKeys";
	public static final String BINDING_showEmailInIndividuForm = "showEmailInIndividuForm";
	public static final String BINDING_showCodeFournisInStructureForm = "showCodeFournisInStructureForm";
	public static final String BINDING_selectionMultipleEnabled = "selectionMultipleEnabled";

	public static final String BINDING_CHAMP_NOM = "champNom";
	public static final String BINDING_CHAMP_PRENOM = "champPrenom";

	public static final String PERS_TYPE_INDIVIDU = "individu";
	public static final String PERS_TYPE_STRUCTURE = "structure";

	private static final String PERS_TYPE_INTERNE = "interne";
	private static final String PERS_TYPE_EXTERNE = "externe";

	public static final Boolean DEFAULT_showCompteUI = Boolean.FALSE;
	public static final String DEFAULT_structureTableViewColonneKeys = "numero,nomPrenomAffichage,ville";
	public static final String DEFAULT_individuTableViewColonneKeys = "numero,nomPrenomAffichage,ville";

	public static final String BINDING_showStudentNumberInIndividuForm = "showStudentNumberInIndividuForm";
	
	
	protected String persTypeSelected = PERS_TYPE_STRUCTURE;
	protected String persTypeInterneExterneSelected = PERS_TYPE_EXTERNE;
	protected Boolean isAjouterDisabled = Boolean.FALSE;

	private String srchNom;
	private String srchPrenom;
	private String srchSiret;
	private String srchSigle;
	private String srchFouCode;
	private String srchEmail;
    private String srchStudentNumber;
    
	private IPersonne selectedPersonne;
	private String callbackOnCreerPersonne;
	private String callbackOnModifierPersonne;
	private String callbackOnSelectionnerPersonne;

	private Boolean wantRefreshGroupes = Boolean.FALSE;
	private Boolean showDetail = Boolean.TRUE;
	private Boolean showDetailInModalBox = Boolean.FALSE;

	public Boolean isGroupeEditing;
	public Boolean isGroupeAdminEditing;
	public Boolean isStructureEffectifEditing;
	public Boolean isStructureCaEditing;
	public Boolean isStructureElementEditing;

	public EOStructure selectionStructure;

	private final CktlAjaxUserQuestionDelegate userQuestionDelegate;

	private ERXDisplayGroup displayGroup;
	private boolean srchDone;
	protected PersonneSrchCtrl ctrl;

	private Boolean radioButtonsInitialized = false;

	private String titreWhenEmpty;

	public PersonneSrch(WOContext context) {
		super(context);
		userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (Boolean.TRUE.equals(valueForBinding(BINDING_wantReset))) {
			setValueForBinding(Boolean.FALSE, BINDING_wantReset);
			resetComponent();
		}
		if (!radioButtonsInitialized) {
			if (showIndividus().booleanValue() && !showStructures().booleanValue()) {
				setPersTypePhysiqueSelected(Boolean.TRUE);
			}
			if (!showIndividus().booleanValue() && showStructures().booleanValue()) {
				setPersTypeMoraleSelected(Boolean.TRUE);
			}
			if (showPersonnesExternes().booleanValue() && !showPersonnesInternes().booleanValue()) {
				setPersTypeExterneSelected(Boolean.TRUE);
			}
			if (!showPersonnesExternes().booleanValue() && showPersonnesInternes().booleanValue()) {
				setPersTypeInterneSelected(Boolean.TRUE);
			}
			radioButtonsInitialized = true;
		}
		if (Boolean.TRUE.equals(valueForBinding(BINDING_forceRecherche))) {
			setValueForBinding(Boolean.FALSE, BINDING_forceRecherche);
			rechercher();
		}
		

		
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlPersonneGuiAjax.framework", "scripts/cktlstringutils.js");
		//		if (booleanValueForBinding(BINDING_showDetailInModalBox, Boolean.FALSE).booleanValue()) {
		//			CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlPersonneGuiAjax.framework", "scripts/cktlpersonnedetail.js");
		//		}

		//AjaxUtils.appendScript(context, jsDefaultScript());
	}

	public String persTypeSelected() {
		if (hasBinding(BINDING_persType)) {
			persTypeSelected = (String) valueForBinding(BINDING_persType);
		}
		return persTypeSelected;
	}

	public void setPersTypeSelected(String persTypeSelected) {
		if (canSetValueForBinding(BINDING_persType))
			setValueForBinding(persTypeSelected, BINDING_persType);
		this.persTypeSelected = persTypeSelected;
	}

	public Boolean getPersTypeMoraleSelected() {
		return Boolean.valueOf(PERS_TYPE_STRUCTURE.equals(persTypeSelected()));
	}

	public void setPersTypeMoraleSelected(Boolean persTypeMoraleSelected) {
		clearRes();
		//this.persTypeSelected = PERS_TYPE_STRUCTURE;
		setPersTypeSelected(PERS_TYPE_STRUCTURE);
	}

	public Boolean getPersTypePhysiqueSelected() {
		return Boolean.valueOf(PERS_TYPE_INDIVIDU.equals(persTypeSelected()));
	}

	public void setPersTypePhysiqueSelected(Boolean persTypePhysiqueSelected) {
		clearRes();
		//this.persTypeSelected = PERS_TYPE_INDIVIDU;
		setPersTypeSelected(PERS_TYPE_INDIVIDU);
	}

	public String persTypeInterneExterneSelected() {
		if (hasBinding(BINDING_persTypeInterneExterne)) {
			persTypeInterneExterneSelected = (String) valueForBinding(BINDING_persTypeInterneExterne);
		}
		return persTypeInterneExterneSelected;
	}

	public void setPersTypeInterneExterneSelected(String persTypeInterneExterneSelected) {
		if (canSetValueForBinding(BINDING_persTypeInterneExterne))
			setValueForBinding(persTypeInterneExterneSelected, BINDING_persTypeInterneExterne);
		this.persTypeInterneExterneSelected = persTypeInterneExterneSelected;
	}

	public Boolean getPersTypeExterneSelected() {
		return Boolean.valueOf(PERS_TYPE_EXTERNE.equals(persTypeInterneExterneSelected()));
	}

	public void setPersTypeExterneSelected(Boolean persTypeExterneSelected) {
		clearRes();
		setPersTypeInterneExterneSelected(PERS_TYPE_EXTERNE);
		//this.persTypeInterneExterneSelected = PERS_TYPE_EXTERNE;

	}

	public Boolean getPersTypeInterneSelected() {
		return Boolean.valueOf(PERS_TYPE_INTERNE.equals(persTypeInterneExterneSelected()));
	}

	public void setPersTypeInterneSelected(Boolean persTypeInterneSelected) {
		clearRes();
		setPersTypeInterneExterneSelected(PERS_TYPE_INTERNE);
		//this.persTypeInterneExterneSelected = PERS_TYPE_INTERNE;
	}

	public String getSrchNom() {

		if (srchNom == null &&  hasBinding(BINDING_CHAMP_NOM)) {
			srchNom = (String) valueForBinding(BINDING_CHAMP_NOM);
		}
		
		if (srchNom != null) {
			srchNom = srchNom.trim();
		}
		return srchNom;
	}

	public String getSrchSiret() {
		if (srchSiret != null) {
			srchSiret = srchSiret.trim();
		}
		return srchSiret;
	}

	public String getSrchPrenom() {
		if (srchPrenom == null &&  hasBinding(BINDING_CHAMP_PRENOM)) {
			srchPrenom = stringValueForBinding(BINDING_CHAMP_PRENOM, "");
		}

		if (srchPrenom != null) {
			srchPrenom = srchPrenom.trim();
		}
		return srchPrenom;
	}

	public String getSrchEmail() {
		if (srchEmail != null) {
			srchEmail = srchEmail.trim();
		}
		return srchEmail;
	}
	
	public String getSrchStudentNumber() {
		if (srchStudentNumber != null) { 
			return srchStudentNumber.trim(); 
	    }
		return srchStudentNumber;
	}
	
	public void setSrchStudentNumber(String srchStudentNumber) {
		this.srchStudentNumber = srchStudentNumber;
	}  

	public void setSrchNom(String srchNom) {
		this.srchNom = srchNom;
	}

	public void setSrchSigle(String srchSigle) {
		this.srchSigle = srchSigle;
	}

	public void setSrchFouCode(String srchFouCode) {
		this.srchFouCode = srchFouCode;
	}

	public String getSrchFouCode() {
		if (srchFouCode != null) {
			srchFouCode = srchFouCode.trim();
		}
		return srchFouCode;
	}

	public String getSrchSigle() {
		if (srchSigle != null) {
			srchSigle = srchSigle.trim();
		}
		return srchSigle;
	}

	/**
	 * Effectue une recherche à partir des criteres saisis et met a jour le displayGroup.
	 */
	public WOActionResults rechercher() {
		//setErreurSaisieMessage(null);
		try {
			//clearRes();
			srchDone = false;
			getCtrl().rechercher();
			srchDone = true;
		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	protected void resetComponent() {
		setSelectedPersonne(null);
		srchNom = null;
		srchPrenom = null;
		srchSiret = null;
		srchStudentNumber = null;
		clearRes();
	}

	public void clearRes() {
		//setErreurSaisieMessage(null);
		srchDone = false;
		radioButtonsInitialized = false;
		setSrchSiret(null);
		displayGroup().setObjectArray(NSArray.EmptyArray);
		setSelectedPersonne(null);
	}

	public WOActionResults onSelectIndividu() {

		return null;
	}

	public WOActionResults onSelectStructure() {
		return null;
	}

	/**
	 * @return Le nom de la personne selectionnée sous la forme Nom (<i>nom patronymique</i>), <i>prenom</i>) N°
	 */
	public String getSelectedPersonneNom() {
		if (isSelected().booleanValue()) {
			return getSelectedPersonne().getNomCompletAffichage() + " (N° " + getSelectedPersonne().getNumero() + ")";
		}
		return "";
	}

	public Boolean isSelected() {
		return Boolean.valueOf(getSelectedPersonne() != null);
	}

	public Boolean isStructureSelected() {
		return Boolean.valueOf(getSelectedStructure() != null);
	}

	public Boolean hasResults() {
		return Boolean.valueOf(displayGroup().allObjects() != null && displayGroup().allObjects().count() > 0);
	}

	/**
	 * @return TRUE si utilisateur a cliqué au moins une fois sur rechercher.
	 */
	public Boolean srchDone() {
		return Boolean.valueOf(srchDone);
	}

	public WOActionResults onModifierPersonne() {
		setValueForBinding(getSelectedPersonne(), BINDING_selectedPersonne);
		getSelectedPersonne().setPersIdModification(getUtilisateurPersId());
		return performParentAction(getCallbackOnModifierPersonne());
	}

	public WOActionResults onSelectionnerPersonne() {
		setValueForBinding(getSelectedPersonne(), BINDING_selectedPersonne);
		return performParentAction(getCallbackOnSelectionnerPersonne());
	}

	/**
	 * Cree une structure et lance la creation d'un fournisseur a partir de cette structure.
	 */
	public WOActionResults onCreerStructure() {
		//edc().revert();
		EOStructure structure = getCtrl().creerStructure();
		setSelectedPersonne(structure);
		displayGroup().insertObjectAtIndex(getSelectedPersonne(), 0);
		displayGroup().setSelectedObject(getSelectedPersonne());
		return performParentAction(getCallbackOnCreerPersonne());
	}

	public WOActionResults onCreerIndividu() {
		//edc().revert();
		boolean doIt = false;
		String nom = getSrchNom();
		String prenom = getSrchPrenom();

		NSArray res = EOIndividu.individusWithNameEqualsAndFirstNameEquals(edc(), nom, prenom, 1);

		if (res.count() > 0) {
			doIt = false;
			//FIXME PB avec trigger homonymie
			IPersonne doublon = (IPersonne) res.objectAtIndex(0);
			String s = "Une personne avec le même nom et prénom existe deja dans le référentiel : " + doublon.getNomPrenomAffichage() + " (" + doublon.getNumero()
					+ "), voulez-vous quand meme en créer une nouvelle (si la personne en question n'a pas été affichée dans la liste, vous devez peut-être changer les critères de recherche) ?";
			Boolean confirm = userQuestionDelegate.askUserAsBoolean("question1", s, getUpdateContainerID());
			if (confirm == null) {
				return null;
			} else {
				doIt = confirm.booleanValue();
			}
		} else {
			doIt = true;
		}

		if (doIt) {
			EOIndividu individu = getCtrl().creerIndividu();
			setSelectedPersonne(individu);
			displayGroup().insertObjectAtIndex(getSelectedPersonne(), 0);
			displayGroup().setSelectedObject(getSelectedPersonne());
			userQuestionDelegate.clearAnswers();
			return performParentAction(getCallbackOnCreerPersonne());
		} else {
			userQuestionDelegate.clearAnswers();
		}

		return null;

	}

	public void setSrchPrenom(String SrchPrenom) {
		this.srchPrenom = SrchPrenom;
	}

	public void setSrchEmail(String srchEmail) {
		this.srchEmail = srchEmail;
	}

	public void setSrchSiret(String SrchSiret) {
		this.srchSiret = SrchSiret;
	}

	public void setSelectedIndividu(EOIndividu individu) {
		setSelectedPersonne(individu);

	}

	public void setSelectedStructure(EOStructure structure) {
		setSelectedPersonne(structure);
	}

	public void setSelectedPersonne(IPersonne personne) {
		selectedPersonne = personne;
		setValueForBinding(personne, BINDING_selectedPersonne);
		setWantRefreshGroupes(Boolean.TRUE);
	}

	public String getSubmitTitleStructure() {
		return "Créer " + (getSrchNom() != null ? MyStringCtrl.initCap(getSrchNom()) : "") + " dans le référentiel";
	}

	public String getSubmitTitleIndividu() {
		return "Créer " + (getSrchNom() != null ? MyStringCtrl.initCap(getSrchNom()) : "") + " " + (getSrchPrenom() != null ? MyStringCtrl.initCap(getSrchPrenom()) : "") + " dans le référentiel";
	}

	public String getSubmitCreerIndividuId() {
		return getComponentId() + "submitCreerIndividu";
	}

	public String getSubmitCreerStructureId() {
		return getComponentId() + "submitCreerStructure";
	}

	/**
	 * @return La personne selectionnée suite à la recherche
	 */
	public IPersonne getSelectedPersonne() {
		selectedPersonne = (IPersonne) valueForBinding(BINDING_selectedPersonne);
		return selectedPersonne;
	}

	public EOFournis getSelectedFournis() {
		return (getSelectedPersonne() == null ? null : (getSelectedPersonne().toFourniss().count() == 0 ? null : (EOFournis) getSelectedPersonne().toFourniss().objectAtIndex(0)));
	}

	public EOStructure getSelectedStructure() {
		return (getSelectedPersonne() instanceof EOStructure ? (EOStructure) getSelectedPersonne() : null);
	}

	public EOIndividu getSelectedIndividu() {
		return (getSelectedPersonne() instanceof EOIndividu ? (EOIndividu) getSelectedPersonne() : null);
	}

	public String getCallbackOnCreerPersonne() {
		if (callbackOnCreerPersonne == null) {
			callbackOnCreerPersonne = (String) valueForBinding(BINDING_callbackOnCreerPersonne);
		}
		return callbackOnCreerPersonne;
	}

	public String getCallbackOnModifierPersonne() {
		if (callbackOnModifierPersonne == null) {
			callbackOnModifierPersonne = (String) valueForBinding(BINDING_callbackOnModifierPersonne);
		}
		return callbackOnModifierPersonne;
	}

	public String getCallbackOnSelectionnerPersonne() {
		if (callbackOnSelectionnerPersonne == null) {
			callbackOnSelectionnerPersonne = (String) valueForBinding(BINDING_callbackOnSelectionnerPersonne);
		}
		return callbackOnSelectionnerPersonne;
	}

	public Boolean wantRefreshDetail() {
		return (Boolean) valueForBinding(BINDING_wantRefreshDetail);
	}

	public Boolean isUserCanCreerStructure() {
		return getCtrl().isUserCanCreerStructure();
		//		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitCreerStructure(null));
	}

	public Boolean isUserCanCreerIndividu() {
		return getCtrl().isUserCanCreerIndividu();
		//		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitCreerIndividu(null));
	}

	public Boolean showSubmitCreerIndividu() {
		return Boolean.valueOf(!(MyStringCtrl.isEmpty(getSrchNom()) || MyStringCtrl.isEmpty(getSrchPrenom())));
	}

	public Boolean showSubmitCreerStructure() {
		if (!MyStringCtrl.isEmpty(getSrchNom())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public Boolean wantRefreshGroupes() {
		return wantRefreshGroupes;
	}

	public void setWantRefreshGroupes(Boolean wantRefreshGroupes) {
		this.wantRefreshGroupes = wantRefreshGroupes;
	}

	public Boolean isUserCanModifier() {
		return getCtrl().isUserCanModifier();
	}

	public Boolean showIndividus() {
		return booleanValueForBinding(BINDING_showIndividus, Boolean.TRUE);
	}

	public Boolean showStructures() {
		return booleanValueForBinding(BINDING_showStructures, Boolean.TRUE);
	}

	public Boolean showPersonnesInternes() {
		return booleanValueForBinding(BINDING_showPersonnesInternes, Boolean.TRUE);
	}

	public Boolean showPersonnesExternes() {
		return booleanValueForBinding(BINDING_showPersonnesExternes, Boolean.TRUE);
	}

	public Boolean showRadioInterneExterne() {
		return booleanValueForBinding(BINDING_showRadioInterneExterne, DEFAULT_showRadioInterneExterne);
	}

	public Boolean showEmailInIndividuForm() {
		return booleanValueForBinding(BINDING_showEmailInIndividuForm, Boolean.FALSE);
	}

	public Boolean showCodeFournisInStructureForm() {
		return booleanValueForBinding(BINDING_showCodeFournisInStructureForm, Boolean.TRUE);
	}

	public String resultListHeight() {
		if (hasBinding(BINDING_resultListHeight)) {
			return (String) valueForBinding(BINDING_resultListHeight);
		}
		return DEFAULT_resultListHeight;
	}

	public String titre() {
		return (String) valueForBinding(BINDING_titre);
	}

	public String userInstructions() {
		return (String) valueForBinding(BINDING_userInstructions);
	}

	public Boolean showTypeSelection() {
		return Boolean.valueOf(showIndividus().booleanValue() && showStructures().booleanValue());
	}

	public Boolean showInterneExterneSelection() {
		return Boolean.valueOf(showPersonnesExternes().booleanValue() && showPersonnesInternes().booleanValue() && showRadioInterneExterne().booleanValue());
	}

	public Boolean showDetail() {
		if (hasBinding(BINDING_showDetail)) {
			showDetail = Boolean.valueOf((Boolean) valueForBinding(BINDING_showDetail));
		}
		return showDetail;
	}

	public Boolean showDetailInModalBox() {
		if (hasBinding(BINDING_showDetailInModalBox)) {
			showDetailInModalBox = Boolean.valueOf((Boolean) valueForBinding(BINDING_showDetailInModalBox));
		}
		return showDetailInModalBox;
	}

	public String personneSrchContainerId() {
		return getComponentId() + "_PersonneSrch";
	}

	public String containerSrchId() {
		return getComponentId() + "_ContainerSrch";
	}

	public String formSrchId() {
		return getComponentId() + "_FormSrch";
	}

	public String listeResContainerId() {
		return getComponentId() + "_ListeResContainer";
	}

	public String detailResContainerId() {
		if (hasBinding(BINDING_updateContainerIDOnSelectionnerPersonneInTableview)) {
			return (String) valueForBinding(BINDING_updateContainerIDOnSelectionnerPersonneInTableview);
		}
		else {
			return (showDetail().booleanValue() ? getComponentId() + "_DetailResContainer" : null);
		}
	}

	public String onFormSrchComplete() {
		return "function(oC){" + (detailResContainerId() != null ? detailResContainerId() + "Update();" : "") + "}";
	}

	//	public String getStructureRattachement() {
	//		if (getSelectedStructure() != null) {
	//			String res = "";
	//			EOStructure struct = getSelectedStructure().toStructurePere();
	//			do {
	//				if (struct != null) {
	//					res = struct.llStructure() + " > " + res;
	//					struct = struct.toStructurePere();
	//				}
	//			} while (struct != null && !struct.cStructure().equals(struct.cStructurePere()));
	//			return res;
	//		}
	//		return null;
	//	}

	public Boolean disableSelection() {
		return booleanValueForBinding(BINDING_disableSelection, Boolean.FALSE);
	}

	public String getSelectionnerPersonneLabel() {
		return (String) valueForBinding(BINDING_selectionnerPersonneLabel);
	}

	public ERXDisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = (ERXDisplayGroup) valueForBinding(BINDING_displayGroup);
			if (displayGroup == null) {
				displayGroup = new ERXDisplayGroup();
				if (canSetValueForBinding(BINDING_displayGroup)) {
					setValueForBinding(displayGroup, BINDING_displayGroup);
				}
			}
			displayGroup.setDelegate(new DgDelegate());
		}
		return displayGroup;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedPersonne((IPersonne) group.selectedObject());
		}
	}

	public EOQualifier getQualifierForTypeAdresse() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForTypeAdresse);
	}

	public Integer getFetchLimit() {
		String parametreFechtLimit;
		if (hasBinding(BINDING_fetchLimit)) {
			return (Integer) valueForBinding(BINDING_fetchLimit);
		}
		
		parametreFechtLimit = EOGrhumParametres.parametrePourCle(ERXEC.newEditingContext(), FwkCktlPersonneParamManager.PARAM_FECHT_LIMIT_MAX);
		if (!MyStringCtrl.isEmpty(parametreFechtLimit) && MyStringCtrl.hasOnlyDigits(parametreFechtLimit)) {
			return Integer.parseInt(EOGrhumParametres.parametrePourCle(ERXEC.newEditingContext(), FwkCktlPersonneParamManager.PARAM_FECHT_LIMIT_MAX));
		}
		
		return DEFAULT_fetchLimit;
	}

	public EOQualifier getQualifierForTypeTel() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForTypeTel);
	}

	public EOQualifier getQualifierForStructures() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForStructures);
	}

	public EOQualifier getQualifierForIndividus() {
		return (EOQualifier) valueForBinding(BINDING_qualifierForIndividus);
	}

	public EOQualifier getQualifierForGroupes() {
		EOQualifier qualifier = (EOQualifier) valueForBinding(BINDING_qualifierForGroupes);
		if (qualifier != null) {
			NSArray<EOStructure> groupes = EOStructure.fetchAll(edc(), qualifier);
			return EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).in(groupes);
		}
		return null;
	}

	public String jsOnCreerStructure() {
		String res = "";
		if (MyStringCtrl.isEmpty(getSrchNom())) {
			res = "(return false)";
		}
		else {
			res = "confirm('Souhaitez-vous vraiment créer une nouvelle structure ayant comme nom " + jsEncode(MyStringCtrl.initCap(getSrchNom())) + "  dans l\\'annuaire ?')";
		}
		return res;
	}

	public String jsOnCreerIndividu() {
		String res = "";
		if (MyStringCtrl.isEmpty(getSrchNom())) {
			res = "(return false)";
		}
		else {
			res = "confirm('Souhaitez-vous vraiment créer un nouvel individu ayant comme nom " + jsEncode(MyStringCtrl.initCap(getSrchNom())) + " " + jsEncode(MyStringCtrl.initCap(getSrchPrenom())) + " dans l\\'annuaire ?')";
		}
		return res;
	}

	/**
	 * @return Le script par defaut.
	 */
	public String jsDefaultScript() {
		StringBuffer sb = new StringBuffer();
		sb.append("var lastKeyCode = null;");
		sb.append("enableCreation = function(enabled) {");
		sb.append("if ($('" + getSubmitCreerIndividuId() + "') != null) {" + "if (enabled) {" + "$('" + getSubmitCreerIndividuId() + "').show();" + "} else {" + "$('" + getSubmitCreerIndividuId() + "').hide();" + "} " + "$('" + getSubmitCreerIndividuId() + "').disabled = !enabled; " + "}");

		sb.append("if ($('" + getSubmitCreerStructureId() + "') != null) { " + "if (enabled) { " + "$('" + getSubmitCreerStructureId() + "').show();" + "} else { " + "$('" + getSubmitCreerStructureId() + "').hide(); " + "} " + "$('" + getSubmitCreerStructureId() + "').disabled = !enabled; " + "}");

		sb.append("}");
		return sb.toString();
	}

	public PersonneSrchCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (PersonneSrchCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new PersonneSrchCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return ctrl;
	}

	public String persTypeMoraleId() {
		return getComponentId() + "_" + "radioM";
	}

	public String persTypePhysiqueId() {
		return getComponentId() + "_" + "radioP";
	}

	public String persTypeExterneId() {
		return getComponentId() + "_" + "radioE";
	}

	public String persTypeInterneId() {
		return getComponentId() + "_" + "radioI";
	}

	public String srchNomFieldId() {
		return getComponentId() + "_" + "TFSrchNom";
	}

	public String srchSigleFieldId() {
		return getComponentId() + "_" + "TFSrchSigle";
	}

	public String srchFouCodeFieldId() {
		return getComponentId() + "_" + "TFSrchFouCode";
	}

	public String groupeAdminTitle() {
		return "Administration du groupe " + getSelectedPersonne().persLibelleAffichage();
	}

	public String structureElementsTitle() {
		return "Membres du groupe " + getSelectedPersonne().persLibelleAffichage();
	}

	public String personneGroupesTitle() {
		return "Groupes dont d&eacute;pend " + getSelectedPersonne().getNomCompletAffichage();
	}

	public String detailPersonneModalBoxId() {
		String detailPersonneModalBoxId = null;

		if (hasBinding(BINDING_showDetailInModalBox) && (Boolean) valueForBinding(BINDING_showDetailInModalBox).equals(Boolean.TRUE)) {
			detailPersonneModalBoxId = getComponentId() + "_" + "DetailPersonneModalBox";
		}
		return detailPersonneModalBoxId;
	}

	/**
	 * Indique s'il faut effectuer une recherche sur les deux types de population (sans les separer).
	 * 
	 * @return true si {@link PersonneSrch#showInterneExterneSelection()} renvoit FALSE et que {@link PersonneSrch#showPersonnesExternes()} et
	 *         {@link PersonneSrch#showPersonnesInternes()} renvoient true tous les deux.
	 */
	public boolean searchOnInterneAndExterne() {
		return (!showInterneExterneSelection().booleanValue() && showPersonnesExternes().booleanValue() && showPersonnesInternes().booleanValue());
	}

	public Boolean showCompteInDetail() {
		return booleanValueForBinding(BINDING_showCompteUI, DEFAULT_showCompteUI);
	}

	public String structureTableViewColonneKeys() {
		return stringValueForBinding(BINDING_structureTableViewColonneKeys, DEFAULT_structureTableViewColonneKeys);
	}

	public String individuTableViewColonneKeys() {
		return stringValueForBinding(BINDING_individuTableViewColonneKeys, DEFAULT_individuTableViewColonneKeys);
	}

	public String btRechercherId() {
		return getComponentId() + "_btRechercher";
	}

	public String btRechercherOnClick() {
		//		return "var elem = $('" + btRechercherId() + "_btn_base_image'); elem.addClassName('cktl_action_busy');jsOnRechercher(null);elem.removeClassName('cktl_action_busy');";
		return "jsOnRechercher(null);";
	}

	public Boolean isSelectionMultipleEnabled() {
		return booleanValueForBinding(BINDING_selectionMultipleEnabled, false);
	}
	public Boolean showStudentNumberInIndividuForm() {
		return booleanValueForBinding(BINDING_showStudentNumberInIndividuForm, Boolean.FALSE) 
		&& (showInterneExterneSelection() || showPersonnesInternes());
	}

	/**
	 * @return the titreWhenEmpty
	 */
	public String getTitreWhenEmpty() {
		if (isReadOnly()) {
			return "Recherche";
		} else {
			return "Recherche/cr&eacute;ation";
		}
	}

}
