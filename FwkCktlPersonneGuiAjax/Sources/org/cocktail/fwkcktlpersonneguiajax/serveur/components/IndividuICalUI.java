/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIcal;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXStringUtilities;

public class IndividuICalUI extends AComponent {
	/**
	 *
	 */
	private static final long serialVersionUID = 111111111111111111L;

	public static final String BND_EDITED_PERSONNE = "editedPersonne";
	public static final String BND_EC = "ec";
	public static final String BND_WANT_RESET = "wantReset";

	private EOEditingContext ec;

	private IPersonne editedPersonne;
	private NSArray<String> lstRefreshZones;
	private String saveOk;

	private EOIcal icalOccur;
	private EOIcal editedIcal;

	private NSArray<EOIcal> lstIcals;

	public IndividuICalUI(WOContext context) {
		super(context);
		setLstRefreshZones(new NSArray<String>(new String[] {
				icalsId, btbId
		}));
		lstDeletedObjects = new NSMutableArray<EOIcal>();
	}

	/**
	 * @return the editing context ec
	 */
	public EOEditingContext getEc() {
		if (hasBinding(BND_EC)) {
			ec = (EOEditingContext) valueForBinding(BND_EC);
		}
		if (ec == null) {
			ec = ERXEC.newEditingContext();
		}
		return ec;
	}

	//	/**
	//	 * @param ec
	//	 *            the ec to set
	//	 */
	//	public void setEc(EOEditingContext ec) {
	//		this.ec = ec;
	//		if (canSetValueForBinding(BND_EC)) {
	//			setValueForBinding(ec, BND_EC);
	//		}
	//	}

	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		if (wantReset()) {
			setWantReset(false);
		}
		// Pour obliger un rafraîchissement à tous les coups de la liste des calendriers.
		setLstIcals(null);
		super.appendToResponse(woresponse, wocontext);
	}

	public Boolean wantReset() {
		Boolean wantReset = Boolean.FALSE;
		if (hasBinding(BND_WANT_RESET)) {
			wantReset = (Boolean) valueForBinding(BND_WANT_RESET);
		}
		return wantReset;
	}

	public void setWantReset(Boolean value) {
		setValueForBinding(value, BND_WANT_RESET);
	}

	/**
	 * @return the lstRefreshZones
	 */
	public NSArray<String> lstRefreshZones() {
		return lstRefreshZones;
	}

	/**
	 * @param lstRefreshZones the lstRefreshZones to set
	 */
	public void setLstRefreshZones(NSArray<String> lstRefreshZones) {
		this.lstRefreshZones = lstRefreshZones;
	}

	/**
	 * @return the icalOccur
	 */
	public EOIcal icalOccur() {
		return icalOccur;
	}

	/**
	 * @param icalOccur the icalOccur to set
	 */
	public void setIcalOccur(EOIcal icalOccur) {
		this.icalOccur = icalOccur;
	}

	/**
	 * @return the editedPersonne
	 */
	public IPersonne editedPersonne() {
		if (hasBinding(BND_EDITED_PERSONNE)) {
			setEditedPersonne((IPersonne) valueForBinding(BND_EDITED_PERSONNE));
		}
		if (editedPersonne == null) {
			if (getUtilisateurPersId() != null) {
				setEditedPersonne(PersonneDelegate.fetchPersonneByPersId(getEc(), getUtilisateurPersId()));
			}
		}
		return editedPersonne;
	}

	/**
	 * @param editedPersonne the editedPersonne to set
	 */
	public void setEditedPersonne(IPersonne editedPersonne) {
		this.editedPersonne = editedPersonne;

		if (canSetValueForBinding(BND_EDITED_PERSONNE)) {
			setValueForBinding(editedPersonne, BND_EDITED_PERSONNE);
		}

	}

	public boolean haveDroitsModification(IPersonne pers) {
		if (pers == null)
			return false;
		//		PersonneApplicationUser appUser = new PersonneApplicationUser(pers.editingContext(), pers.persId());
		return getAppUserForAnnuaire().hasDroitModificationIPersonne(pers);
	}

	public boolean canEditIcals() {
		return haveDroitsModification(editedPersonne());
	}

	/**
	 * @return the isDeleted
	 */
	public boolean isDeleted() {
		return ((lstDeletedObjects() != null) && (lstDeletedObjects()
				.contains(icalOccur())));
	}

	private NSMutableArray<EOIcal> lstDeletedObjects;

	public NSMutableArray<EOIcal> lstDeletedObjects() {
		return lstDeletedObjects;
	}

	public void setLstDeletedObjects(NSMutableArray<EOIcal> lst) {
		lstDeletedObjects = lst;
	}

	public WOActionResults restore() {
		lstDeletedObjects().removeObject(icalOccur());
		return null;
	}

	private String icalsId = ERXStringUtilities.safeIdentifierName("icalsId"
			+ UUID.randomUUID());

	/**
	 * @return the icalsId
	 */
	public String icalsId() {
		return icalsId;
	}

	private String editIcalId = ERXStringUtilities
			.safeIdentifierName("editIcalId" + UUID.randomUUID());

	/**
	 * @return the editIcalId
	 */
	public String editIcalId() {
		return editIcalId;
	}

	/**
	 * @return the lstIcals
	 */

	@SuppressWarnings("unchecked")
	public NSArray<EOIcal> lstIcals() {
		if (lstIcals == null) {
			setLstIcals(EOIcal.icalsForIndividu(getEc(), getEditedIndividu()));
		}
		return lstIcals;
	}

	/**
	 * @param lstIcals the lstIcals to set
	 */
	public void setLstIcals(NSArray<EOIcal> lstIcals) {
		this.lstIcals = lstIcals;
	}

	public EOIndividu getEditedIndividu() {
		if (editedPersonne().isIndividu())
			return (EOIndividu) editedPersonne();
		return null;
	}

	public WOActionResults delete() {
		lstDeletedObjects().addObject(icalOccur());
		return null;
	}

	private String aucTriggerIcalId = ERXStringUtilities
			.safeIdentifierName("aucTriggerIcalId" + UUID.randomUUID());

	/**
	 * @return the aucTriggerIcalId
	 */
	public String aucTriggerIcalId() {
		return aucTriggerIcalId;
	}

	public WOActionResults valide() {
		saveOk = "false";
//		if ((editedIcal.icalName() == null)
//				|| ("".equals(editedIcal.icalName()))
//				|| (" ".equals(editedIcal.icalName()))) {
//			mySession().addSimpleInfoMessage("IndividuIcalUI", "Le fichier est obligatoire !!");
//			return null;
//		}
		if ((editedIcal.icalLien() == null)
				|| ("".equals(editedIcal.icalLien()))
				|| (" ".equals(editedIcal.icalLien()))) {
			mySession().addSimpleInfoMessage("IndividuICalUI", "Le chemin est obligatoire !!");
			return null;
		}

		editedIcal.setDModification(new NSTimestamp());
		if (editedIcal.editingContext() == null)
			getEc().insertObject(editedIcal);

		try {
			editedIcal.editingContext().saveChanges();
		} catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("IndividuICAlUI", e.getMessage());
			return null;
		}
		saveOk = "true";
		setLstIcals(null);
		CktlAjaxWindow.close(context());
		return null;
	}

	public WOActionResults cancelChange() {
		if (editedIcal.editingContext() != null) {
			editedIcal.editingContext().revert();
		}
		CktlAjaxWindow.close(context());
		return null;
	}

	public EOIcal getEditedIcal() {
		return editedIcal;
	}

	public void setEditedIcal(EOIcal editedIcal) {
		this.editedIcal = editedIcal;
	}

	public WOActionResults editIcal() {
		setEditedIcal(icalOccur);
		saveOk = "false";
		return null;
	}

	/**
	 * @return the classRow
	 */
	public String classRow() {
		return ((isDeleted()) ? "td deleted" : "td");
	}

	public WOActionResults addIcal() {
		EOIcal newIcal = EOIcal.creerInstance(getEc());
		newIcal.setDCreation(new NSTimestamp());
		newIcal.setToIndividuRelationship(getEditedIndividu());
		newIcal.setIcalType("PRO");
		setEditedIcal(newIcal);
		saveOk = "false";
		return null;
	}

	private String btbId = ERXStringUtilities.safeIdentifierName("btbId"
			+ UUID.randomUUID());

	/**
	 * @return the btbId
	 */
	public String btbId() {
		return btbId;
	}

	public WOActionResults saveDelete() {
		// del des objets supprimés
		Enumeration<EOIcal> enumDel = lstDeletedObjects.objectEnumerator();
		while (enumDel.hasMoreElements()) {
			EOIcal ical = enumDel.nextElement();
			getEc().deleteObject(ical);

		}
		lstDeletedObjects.removeAllObjects();

		getEc().saveChanges();
		setLstIcals(null);
		return null;
	}

	public WOActionResults cancelDelete() {
		lstDeletedObjects.removeAllObjects();
		return null;
	}

	public boolean haveChanges() {
		return lstDeletedObjects.size() > 0;
	}

	public String urlIcal() {
		if (MyStringCtrl.isEmpty(icalOccur().icalName())) {
			return icalOccur().icalLien();
		}
		return icalOccur().icalLien() + icalOccur().icalName();
	}

	public String urlIcalEncoded() throws UnsupportedEncodingException {
		return URLEncoder.encode(icalOccur().icalLien() + icalOccur().icalName(), "UTF-8");
	}

	/**
	 * @return la valeur 'Oui' pour l'attribut "ind_agenda" d'un individu
	 * @author Alain MALAPLATE <alain.malaplate at cocktail.org>
	 */
	public String getAgendaProPublicOui() {
		return EOIndividu.LISTE_ROUGE_O;
	}

	/**
	 * @return la valeur 'Non' pour l'attribut "ind_agenda" d'un individu
	 * @author Alain MALAPLATE <alain.malaplate at cocktail.org>
	 */
	public String getAgendaProPublicNon() {
		return EOIndividu.LISTE_ROUGE_N;
	}

	public String getAgendaPro() {
		return getEditedIndividu().indAgenda();
	}

	public void setAgendaPro(String valueAgendaPro) {
		getEditedIndividu().setIndAgenda(valueAgendaPro);
	}

	public Boolean getAgendaProPublicOuiChecked() {
		return Boolean.valueOf(EOIndividu.LISTE_ROUGE_O.equals(getEditedIndividu().indAgenda()));
	}

	public void setAgendaProPublicOuiChecked(Boolean val) {
		if (val.booleanValue()) {
			getEditedIndividu().setIndAgenda(EOIndividu.LISTE_ROUGE_O);
		}
	}

	public Boolean getAgendaProPublicNonChecked() {
		return Boolean.valueOf(EOIndividu.LISTE_ROUGE_N.equals(getEditedIndividu().indAgenda()));
	}

	public void setAgendaProPublicNonChecked(Boolean val) {
		if (val.booleanValue()) {
			getEditedIndividu().setIndAgenda(EOIndividu.LISTE_ROUGE_N);
		}
	}

	public String getRadioOId() {
		return getComponentId() + "_radioO";
	}

	public String getRadioNId() {
		return getComponentId() + "_radioN";
	}

	/**
	 * Ajout d'un droit d'accès aux radios boutons pour faire les agendas publics ou privés.
	 */
	public boolean accesRadioBt(){
		if (getAppUserForAnnuaire().hasDroitGrhumCreateur() || getAppUserForAnnuaire().getPersId().equals(editedPersonne().persId()) ){
			return true;
		}
		return false;
	}
}
