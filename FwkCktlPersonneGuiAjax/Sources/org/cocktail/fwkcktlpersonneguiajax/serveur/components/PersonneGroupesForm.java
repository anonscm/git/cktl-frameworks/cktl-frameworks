/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Formulaire de saisie des groupes affectés à la personne. Utilisez plutot PersonneGroupesUI.
 *
 * @binding personne
 * @binding repartStructure
 * @binding isNew
 * @binding filtrerParTypeGroupe Facultatif Boolean. Affiche une liste déroulante des types de groupe pour filtrer l'arbre. Par defaut True.
 * @binding filtrerParVisibilite Facultatif. Boolean. Affiche une liste déroulante Tous/Public/Privés pour filtrer l'arbre. Par défaut False.
 * @binding formID Obligatoire pour les filtres.
 * @author rprin
 */
public class PersonneGroupesForm extends AComponent {

	private static final long serialVersionUID = 1L;

	public static final String BINDING_repartStructure = "repartStructure";
	public static final String BINDING_isNew = "isNew";
	public static final String BINDING_filtrerParTypeGroupe = "filtrerParTypeGroupe";
	public static final String BINDING_filtrerParVisibilite = "filtrerParVisibilite";
	public static final String BINDING_wantReset = "wantReset";

	public static final Boolean DEFAULT_filtrerParTypeGroupe = Boolean.TRUE;
	public static final Boolean DEFAULT_filtrerParVisibilite = Boolean.FALSE;

	private String groupe;
	private NSMutableDictionary userFiltersDictionary;

	private NSArray groupeExclus;

	private EORepartAssociation selectedRepartAssociation;
	public EORepartAssociation unRepartAssociation;
	private EORepartStructure repartStructureCache;

	public PersonneGroupesForm(WOContext context) {
		super(context);
	}

	public Boolean getWantReset() {
		return booleanValueForBinding(BINDING_wantReset, false);
	}

	public void setWantReset(Boolean wantReset) {
		setValueForBinding(wantReset, BINDING_wantReset);
	}

	public void reset() {
		if (getWantReset()) {
			setSelectedAssociation(null);
			unRepartAssociation = null;
			repartStructureCache = null;
			setWantReset(false);
		}
	}

	@Override
	public void awake() {
		super.awake();
		reset();
	}

	public EORepartStructure repartStructure() {
		if (repartStructureCache == null || !repartStructureCache.equals(valueForBinding(BINDING_repartStructure))) {
			repartStructureCache = (EORepartStructure) valueForBinding(BINDING_repartStructure);
			resetForm();
		}
		return repartStructureCache;
	}

	public void resetForm() {
		selectedRepartAssociation = null;
	}

	/**
	 * @return null ou l'élement affecte au groupe
	 */
	public IPersonne personne() {
		return (repartStructure() == null ? null : repartStructure().toPersonneElt());
	}

	/**
	 * @return the groupe
	 */
	public String getGroupe() {
		return groupe;
	}

	/**
	 * @param groupe the groupe to set
	 */
	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	public EOStructure getSelectedStructureGroupe() {
		EORepartStructure repartStructure = (EORepartStructure) valueForBinding(BINDING_repartStructure);
		return (repartStructure == null ? null : repartStructure.toStructureGroupe());
	}

	public void setSelectedStructureGroupe(EOStructure selectedStructure) {
		try {
			if (isNew().booleanValue()) {
				if (selectedStructure != null) {
					EOQualifier qual = new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, selectedStructure);
					if (repartStructure().toPersonneElt().toRepartStructures(qual).count() > 0) {
						throw new Exception("Le groupe " + selectedStructure.persLibelle() + " est déja affectée à la personne " + repartStructure().toPersonneElt().persLibelle() + ". Annuler la création de l'affectation, sélectionnez le groupe dans la liste et modifiez-le.");
					}
				}
			}
			repartStructure().setToStructureGroupeRelationship(selectedStructure);
		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}

	}

	public EOQualifier groupeQualifier() {
		return null;
	}

	public NSDictionary userFiltersDictionary() {
		if (userFiltersDictionary == null) {
			userFiltersDictionary = new NSMutableDictionary();

			if (Boolean.TRUE.equals(filtrerParVisibilite())) {
				userFiltersDictionary.takeValueForKey(EOStructure.QUAL_GROUPE_PRIVE, "Groupes Privés");
				userFiltersDictionary.takeValueForKey(EOStructure.QUAL_GROUPE_PUBLIC, "Groupes Public");
			}
			if (Boolean.TRUE.equals(filtrerParTypeGroupe())) {
				userFiltersDictionary.put("Services", EOStructureForGroupeSpec.QUAL_GROUPES_SERVICES);
				userFiltersDictionary.put("Labos", EOStructureForGroupeSpec.QUAL_GROUPES_LABORATOIRES);
				userFiltersDictionary.put("Forums", EOStructureForGroupeSpec.QUAL_GROUPE_FORUMS);
				userFiltersDictionary.put("Entreprises", EOStructureForGroupeSpec.QUAL_GROUPE_ENTREPRISES);
			}
		}
		return userFiltersDictionary;
	}

	public String associationSelectID() {
		return getComponentId() + "_associationSelect";
	}

	public String ajaxGroupeSelectId() {
		return getComponentId() + "_ajaxGroupeSelect";
	}

	public NSArray groupeExclus() {
		if (groupeExclus == null) {
			groupeExclus = EOStructureForGroupeSpec.getGroupesReserves(edc());
		}
		return groupeExclus;
	}

	public EOAssociation getSelectedAssociation() {
		return (getSelectedRepartAssociation() == null ? null : getSelectedRepartAssociation().toAssociation());
	}

	public void setSelectedAssociation(EOAssociation selectedAssociation) {
		EOAssociation ass = selectedAssociation;
		if (ass != null && !selectedAssociation.editingContext().equals(repartStructure().editingContext())) {
			ass = selectedAssociation.localInstanceIn(repartStructure().editingContext());
		}
		selectedRepartAssociation.setToAssociationRelationship(ass);

	}

	public Boolean hasSelectedAssociation() {
		return getSelectedAssociation() != null;
	}

	public Boolean hasNotSelectedAssociation() {
		return !hasSelectedAssociation();
	}

	public EORepartAssociation getSelectedRepartAssociation() {
		if (selectedRepartAssociation == null && repartStructure().toRepartAssociations().count() > 0) {
			selectedRepartAssociation = (EORepartAssociation) repartStructure().toRepartAssociations().objectAtIndex(0);
		}
		return selectedRepartAssociation;
	}

	public void setSelectedRepartAssociation(EORepartAssociation selectedRepartAssociation) {
		this.selectedRepartAssociation = selectedRepartAssociation;
	}

	public Boolean showPopupRepartAssociations() {
		return Boolean.valueOf(repartStructure().toRepartAssociations().count() > 0);
	}

	public String getUnRepartAssociationDisplayString() {
		//		return (unRepartAssociation.toAssociation() == null ? "Nouveau role" : (unRepartAssociation.toAssociation().assLibelle().length() > 20 ? unRepartAssociation.toAssociation().assLibelle().substring(0, 20) : unRepartAssociation.toAssociation().assLibelle()));
		return (unRepartAssociation.toAssociation() == null ? "Nouveau role" : (unRepartAssociation.toAssociation().assLibelle().length() > 20 ? unRepartAssociation.toAssociation().assLibelle().substring(0, 20) : unRepartAssociation.toAssociation().assLibelle()));
	}

	public WOActionResults onRepartAssociationCreer() {
		//si le role en cours n'est pas precise, on n'en rajoute pas un nouveau
		//		if (repartStructure().toRepartAssociations().count() == 0  ) {
		EORepartAssociation repartAssociation = EORepartAssociation.creerInstance(edc());
		//		repartAssociation.setPersIdCreation(getUtilisateurPersId());
		repartAssociation.setPersIdModification(getUtilisateurPersId());
		repartStructure().addToToRepartAssociationsRelationship(repartAssociation);
		setSelectedRepartAssociation(repartAssociation);
		//}
		return null;
	}

	public WOActionResults onRepartAssociationSupprimer() {
		if (selectedRepartAssociation != null) {
			repartStructure().removeFromToRepartAssociationsRelationship(selectedRepartAssociation);
			edc().deleteObject(selectedRepartAssociation);
		}
		setSelectedRepartAssociation(null);
		//		//Toujours avoir au moins un rapartAssociation
		//		if (repartStructure().toRepartAssociations().count() == 0) {
		//			onRepartAssociationCreer();
		//		}

		return null;
	}

	public String addRepartAssociationTitle() {
		if (getSelectedStructureGroupe() != null) {
			return "Affecter un nouveau rôle à la personne pour le groupe " + getSelectedStructureGroupe().getNomPrenomAffichage();
		}
		return "Affecter un nouveau rôle dans le groupe pour la personne";
	}

	public NSArray getLesRepartAssociations() {
		if (repartStructure() == null) {
			return NSArray.EmptyArray;
		}
		NSArray res = repartStructure().toRepartAssociations();
		if (res.count() > 0) {
			if (selectedRepartAssociation == null || res.indexOf(selectedRepartAssociation) == NSArray.NotFound) {
				setSelectedRepartAssociation((EORepartAssociation) res.objectAtIndex(0));
			}
		}
		return res;
	}

	public Boolean isShowRolesPopup() {
		return Boolean.valueOf(repartStructure() != null && repartStructure().toRepartAssociations().count() > 1);
	}

	public Boolean isShowRoles() {
		return Boolean.valueOf(repartStructure() != null && repartStructure().toRepartAssociations().count() > 0);
	}

	public String getPersonneNom() {
		return (personne() == null ? "" : " de " + personne().getNomCompletAffichage() + " ");
	}

	public Boolean isNew() {
		return booleanValueForBinding(BINDING_isNew, Boolean.TRUE);
	}

	public Boolean filtrerParTypeGroupe() {
		return booleanValueForBinding(BINDING_filtrerParTypeGroupe, DEFAULT_filtrerParTypeGroupe);
	}

	public Boolean filtrerParVisibilite() {
		return booleanValueForBinding(BINDING_filtrerParVisibilite, DEFAULT_filtrerParVisibilite);
	}

}
