/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.AnnuaireLogService;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjax;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

/**
 * @author rprin
 * @binding determinerTypesSelonDroits Si true, les types d'adresses possibles sont determinés d'après les droits de l'utilisateur et non avec
 *          qualifierForTypeAdresse.
 * @binding editingContext
 * @binding isEditing Obligatoire. Will set. Permet de savoir si le composant est en mode edition. Initialisez-le à false.
 * @binding isReadOnly
 * @binding personne
 * @binding qualifierForTypeAdresse Qualifier pour les types d'adresses possibles. (Par défaut toutes sauf les invalides). Attention aux adresses
 *          perso, dans certains cas il ne faut pas les afficher. Utilisez plutot le binding determinerTypesSelonDroits.
 * @binding defaultTypeAdr Facultatif. Le code du type d'adresse a selectionner dans la liste par defaut pour la creation (String)
 * @binding prioriteAffichageAdresse Facultatif. Le code des types d'adresses séparés par des virgules à afficher en priorité
 * @binding buildDefaultEmail Boolean. Facultatif. L'adresse email est automatiquement pré-remplie avec nom.prenom@domain.fr (true par defaut)
 * @binding isSelectAdrPrincipale Boolean. Facultatif. L'adresse principale est selectionnee par defaut (false par defaut)
 * @binding selectedRepartPersonneAdresse
 * @binding utilisateurPersId
 */
public class PersonneAdressesUI extends AComponent {
	private static final long serialVersionUID = -2627158402280674126L;
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_personne = "personne";
	public static final String BINDING_selectedRepartPersonneAdresse = "selectedRepartPersonneAdresse";
	public static final String BINDING_isEditing = "isEditing";
	public static final String BINDING_isReadOnly = "isReadOnly";
	public static final String BINDING_isDeterminerTypesSelonDroits = "isDeterminerTypesSelonDroits";
	public static final String BINDING_onAdresseDidChange = "onAdresseDidChange";
	public static final String BINDING_defaultTypeAdr = "defaultTypeAdr";
	public static final String BINDING_prioriteAffichageAdresse = "prioriteAffichageAdresse";
	public static final String BINDING_buildDefaultEmail = "buildDefaultEmail";
	public static final String BINDING_isSelectAdrPrincipale = "isSelectAdrPrincipale";

	public static final String BINDING_blocagePopupAdresses = "blocagePopupAdresses";

	/** Qualifier pour les types d'adresse affichables */
	public static final String BINDING_qualifierForTypeAdresse = "qualifierForTypeAdresse";
	public static final String BINDING_qualifierForTypeAdresseForbidden = "qualifierForTypeAdresseForbidden";
	public static final Boolean DEFAULT_isReadOnly = Boolean.TRUE;
	public static final Boolean DEFAULT_buildDefaultEmail = Boolean.TRUE;

	public static final Logger logger = Logger.getLogger(PersonneAdressesUI.class);
	
	private static final String FONCTION_ADRESSE_ONP = "cocktail.feature.adresse.ONP";

	private EOEditingContext _editingContext;
	private IPersonne _personne;
	//	private String erreurSaisieMessage;

	public EORepartPersonneAdresse unRepartPersonneAdresse;
	private EORepartPersonneAdresse editRepartPersonneAdresse;
	private EOAdresse editAdresse;
	private EOTypeAdresse editTypeAdresse;

	public EOTypeAdresse unTypeAdresse;
	private NSArray lesTypeAdresses;
	private EORepartPersonneAdresse selectedRepartPersonneAdresse;

	private String email;

	private Boolean displayBtAdresse;

	private static final EOQualifier qualifierDefaultForTypeAdresse = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_INVAL)
	}));

	public PersonneAdressesUI(WOContext context) {
		super(context);
		NotificationCtrl.addObserverForOnAnnulerNotification(this, null);
		setDisplayBtAdresse(Boolean.TRUE);
	}

	public void onAnnulerNotification(NSNotification not) {
		if (not.userInfo() != null && not.userInfo().containsKey("edc")) {
			EOEditingContext ed = (EOEditingContext) not.userInfo().objectForKey("edc");
			if (edc().parentObjectStore().equals(ed)) {
				edc().revert();
			}
		}
	}

	public void resetEdc() {
		if (_editingContext != null) {
			// AT : c'est trop tôt pour dispose(), l'ec est locké surement à ce point...
			// _editingContext.dispose();
			lesTypeAdresses = null;
		}
		_editingContext = null;
	}

	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est obligatoire.");
			}
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (pers == null) {
			_personne = null;
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			setSelectedRepartPersonneAdresse(null);
		}
		else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			//else if (_personne == null || _personne.globalID() == null || !_personne.equals(pers)) {
			//La personne a changé
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			resetEdc();
			_personne = pers.localInstanceIn(edc());
			selectedRepartPersonneAdresse = null;
			//setSelectedRepartPersonneAdresse(null);
		}
		return _personne;
	}

	/**
	 * @return La liste des types d'adresse modifiables
	 */
	public EOQualifier getQualifierModifiablesForTypeAdresse() {
		EOQualifier qual = null;
		if (isTypesSelonDroits().booleanValue()) {
			qual = EOTypeAdresse.qualifierModifiablesForApplicationUser(getAppUserForAnnuaire(), getPersonne());
		}
		else {
			String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
			if (!MyStringCtrl.isEmpty(nomApplication) && nomApplication.equals("PROFILER")) {
				qual = (EOQualifier) valueForBinding(BINDING_qualifierForTypeAdresse);
				EOQualifier qualModif = (EOQualifier) valueForBinding(BINDING_qualifierForTypeAdresseForbidden);
				qual = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
						qual, qualModif }));
			}
			if (qual == null) {
				qual = qualifierDefaultForTypeAdresse;
			}

		}
		if (FwkCktlPersonneGuiAjax.paramManager.isCodeActivationActif(FwkCktlPersonneGuiAjaxParamManager.PERSONNE_ADRESSE_PERSO_DESACTIVE)) {
			qual = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
					qual,
					new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PERSO)
			}));
		}

		return qual;
	}

	public EOQualifier getQualifierVisualisablesForTypeAdresse() {
		if (isTypesSelonDroits().booleanValue()) {
			return EOTypeAdresse.qualifierVisualisablesForApplicationUser(getAppUserForAnnuaire(), getPersonne());
		}
		else {
			EOQualifier qual = (EOQualifier) valueForBinding(BINDING_qualifierForTypeAdresse);
			if (qual == null) {
				qual = qualifierDefaultForTypeAdresse;
			}

			return qual;
		}
	}

	private NSArray _getLesRepartPersonneAdresses() {
		NSArray res = NSArray.EmptyArray;
		if (getPersonne() != null) {
			EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {

					getQualifierVisualisablesForTypeAdresse(), EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE

			}));
			EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EORepartPersonneAdresse.ADR_ORDRE_KEY, EOSortOrdering.CompareDescending);
			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(getPersonne().toRepartPersonneAdresses(), qual), new NSArray(sort));
		}
		return res;
	}

	private Boolean isSelectAdrPrincipale() {
		return valueForBooleanBinding(BINDING_isSelectAdrPrincipale, false);
	}

	public NSArray getLesRepartPersonneAdresses() {
		NSArray res = _getLesRepartPersonneAdresses();

		if ((getSelectedRepartPersonneAdresse() == null || res.indexOfObject(getSelectedRepartPersonneAdresse()) == NSArray.NotFound) && res.count() > 0) {
			String str = (String) valueForBinding(BINDING_prioriteAffichageAdresse);
			if (str != null && str.trim().length() > 0) {
				int i = 0;
				String listePriorite[];
				listePriorite = str.split(",");
				for (int n = 0; n < listePriorite.length; n++) {
					for (i = 0; i < res.size(); i++) {
						EORepartPersonneAdresse eora = (EORepartPersonneAdresse) res.get(i);
						if (eora.tadrCode().equalsIgnoreCase(listePriorite[n]))
							break;
					}
					if (i < res.size()) {
						break;
					}
				}
				if (i >= res.size()) {
					i = 0;
				}
				
				setSelectedRepartPersonneAdresse((EORepartPersonneAdresse) res.objectAtIndex(i));
			} else {
				NSArray res2 = ERXQ.filtered(res, EORepartPersonneAdresse.QUAL_RPA_PRINCIPAL);
				if (isSelectAdrPrincipale() && res2.count() > 0) {
					setSelectedRepartPersonneAdresse((EORepartPersonneAdresse) res2.objectAtIndex(0));
				} else {
					setSelectedRepartPersonneAdresse((EORepartPersonneAdresse) res.objectAtIndex(0));
				}
			}
		}
		if (res.count() == 0) {
			setSelectedRepartPersonneAdresse(null);
		}
		return res;
	}

	public EORepartPersonneAdresse getSelectedRepartPersonneAdresse() {
		if (hasBinding(BINDING_selectedRepartPersonneAdresse)) {
			EORepartPersonneAdresse rpa = (EORepartPersonneAdresse) valueForBinding(BINDING_selectedRepartPersonneAdresse);
			if (rpa != null && rpa.editingContext() == null) {
				rpa = null;
			}
			if (rpa != null && !edc().equals(rpa.editingContext())) {
				rpa = rpa.localInstanceIn(edc());
			}
			selectedRepartPersonneAdresse = rpa;
		}
		return selectedRepartPersonneAdresse;
	}

	public void setSelectedRepartPersonneAdresse(EORepartPersonneAdresse repartPersonneAdresse) {
		this.selectedRepartPersonneAdresse = repartPersonneAdresse;
		if (canSetValueForBinding(BINDING_selectedRepartPersonneAdresse)) {
			setValueForBinding(repartPersonneAdresse, BINDING_selectedRepartPersonneAdresse);
		}
		if (repartPersonneAdresse != null) {
			editTypeAdresse = repartPersonneAdresse.toTypeAdresse().localInstanceIn(edc());
		}
	}

	public Boolean isRepartPersonneAdresseSelected() {
		return Boolean.valueOf(getSelectedRepartPersonneAdresse() != null);
	}

	public Boolean isPopupAdressesNotBlocked() {
		if (hasBinding(BINDING_blocagePopupAdresses)) {
			if (Boolean.TRUE == booleanValueForBinding(BINDING_blocagePopupAdresses, null)) {
				// Mise à false pour ne plus voir la Popup
				return Boolean.FALSE;
			}
			if (Boolean.FALSE == booleanValueForBinding(BINDING_blocagePopupAdresses, null)) {
				// Mise à True pour voir la Popup
				return Boolean.TRUE;
			}

		}
		return Boolean.TRUE;
	}

	public Boolean showPopupAdresses() {
		return Boolean.valueOf(getLesRepartPersonneAdresses().count() > 0);
		//		if (getLesRepartPersonneAdresses().count() > 0){
		//			return Boolean.valueOf(true);
		//		} else {
		//			return Boolean.valueOf(false);
		//		}
	}

	public WOActionResults onSelectTypeAdresse() {
		if (hasBinding(BINDING_onAdresseDidChange))
			return (WOActionResults) valueForBinding(BINDING_onAdresseDidChange);
		return null;
	}

	public WOActionResults onTypeAdresseDefinir() {
		EOEditingContext edForAdresse = edc();
		try {
			edForAdresse.revert();
			EORepartPersonneAdresse newRepartPersonneAdresse = null;
			//Si l'adresse n'est pas deja typee
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TO_ADRESSE_KEY + "=%@ and " + EORepartPersonneAdresse.TO_TYPE_ADRESSE_KEY + "=%@", new NSArray(new Object[] {
					getSelectedRepartPersonneAdresse().toAdresse(), unTypeAdresse
			}));
			NSArray res = EOQualifier.filteredArrayWithQualifier(getPersonne().toRepartPersonneAdresses(), qual);
			if (res.count() == 0) {
				newRepartPersonneAdresse = EORepartPersonneAdresse.creerInstance(edForAdresse);
				newRepartPersonneAdresse.initForPersonne(edForAdresse, getPersonne(), getSelectedRepartPersonneAdresse().toAdresse(), unTypeAdresse);
				getSelectedRepartPersonneAdresse().toAdresse().setPersIdModification(getAppUserForAnnuaire().getPersId());
				newRepartPersonneAdresse.setEMail(getSelectedRepartPersonneAdresse().eMail());
			}
			else if (res.count() >= 1) {
				newRepartPersonneAdresse = (EORepartPersonneAdresse) res.objectAtIndex(0);
				newRepartPersonneAdresse.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);
			}
			edForAdresse.saveChanges();
			if (newRepartPersonneAdresse != null) {
				setSelectedRepartPersonneAdresse(newRepartPersonneAdresse);
			}
			if (hasBinding(BINDING_onAdresseDidChange))
				return (WOActionResults) valueForBinding(BINDING_onAdresseDidChange);
		} catch (Exception e) {
			edForAdresse.revert();
			//			e.printStackTrace();
			logger.error("Erreur dans onTypeAdresseDefinir", e);
			setErreurSaisieMessage(e.getMessage());
		}
		return null;

	}


	public WOActionResults onAdresseCreer() {
		try {
			//			if (!getAppUserForAnnuaire().hasDroitCreationEOAdresse(null)) {
			if (!getAppUserForAnnuaire().hasDroitModificationEOAdresseDeType(getEditTypeAdresse().tadrLibelle(), getPersonne())) {
				throw new Exception("Vous n'avez pas le droit de créer une adresse de type " + getEditTypeAdresse().tadrLibelle());
			}

			EOEditingContext edForAdresse = edc();
			setIsEditing(Boolean.TRUE);
			setDisplayBtAdresse(Boolean.TRUE);
			edForAdresse.revert();
			editRepartPersonneAdresse = EORepartPersonneAdresse.creerInstance(edForAdresse);
			editAdresse = EOAdresse.creerInstance(edForAdresse);
			editAdresse.setPersIdModification(getAppUserForAnnuaire().getPersId());
			editAdresse.setToPaysRelationship(EOPays.getPaysDefaut(edForAdresse));
			editRepartPersonneAdresse.initForPersonne(edForAdresse, getPersonne(), editAdresse, getEditTypeAdresse());
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onAdresseModifier() {
		if (getSelectedRepartPersonneAdresse() == null) {
			return null;
		}
		try {
			if (!getAppUserForAnnuaire().hasDroitModificationEOAdresse(getSelectedRepartPersonneAdresse().toAdresse())) {
				throw new Exception("Vous n'avez pas le droit de modifier cette adresse.");
			}

			EOEditingContext edForAdresse = edc();
			edForAdresse.revert();
			editAdresse = EOAdresse.localInstanceIn(edForAdresse, getSelectedRepartPersonneAdresse().toAdresse());
			editAdresse.setPersIdModification(getAppUserForAnnuaire().getPersId());
			editAdresse.willChange();
			editRepartPersonneAdresse = getSelectedRepartPersonneAdresse();
			setIsEditing(Boolean.TRUE);
		} catch (Exception e) {
			logger.debug("Erreur dans onAdresseModifier", e);
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onAdresseSupprimer() {
		AnnuaireLogService annuaireService = new AnnuaireLogService();
		EOEditingContext edcForAnnuaire = new EOEditingContext();
		EOEditingContext edForAdresse = edc();
		setErreurSaisieMessage(null);
		if (getSelectedRepartPersonneAdresse() != null) {
			try {
				String message = "Suppression de l'adresse (type : " + getSelectedRepartPersonneAdresse().tadrCode() + ") adresse ordre : "
									+ getSelectedRepartPersonneAdresse().toAdresse().primaryKey()
									+ " pour le PersId : " + getSelectedRepartPersonneAdresse().persId();
				String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
				annuaireService.creationUnAnnuaireLog(edcForAnnuaire, message, nomApplication);
				edcForAnnuaire.saveChanges();
				edForAdresse.revert();
				getSelectedRepartPersonneAdresse().supprimer();
				prepareFournis();
				edForAdresse.saveChanges();
				if (hasBinding(BINDING_onAdresseDidChange)) {
					setSelectedRepartPersonneAdresse(null);
					return (WOActionResults) valueForBinding(BINDING_onAdresseDidChange);
				}
			} catch (Exception e) {
				logger.error("Erreur dans onAdresseSupprimer", e);
				setErreurSaisieMessage(e.getMessage());
			} finally {
				edForAdresse.revert();
				setSelectedRepartPersonneAdresse(null);
			}
		}
		return null;
	}

	private void prepareFournis() {

        NSArray<EORepartPersonneAdresse> repartsFacturation = 
            ERXQ.filtered(getPersonne().toRepartPersonneAdresses(), 
                    ERXQ.equals(EORepartPersonneAdresse.TADR_CODE_KEY, EOTypeAdresse.TADR_CODE_FACT));
        
        repartsFacturation = 
            ERXQ.filtered(repartsFacturation, EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE);
        
        if (repartsFacturation != null && getPersonne().toFournis() != null && repartsFacturation.lastObject() != null) {
        	((EOFournis)getPersonne().toFournis()).setToAdresseRelationship(repartsFacturation.lastObject().toAdresse());
        }
        System.out.println("RelationShip") ;  
    }
	
	public WOActionResults onAdresseDefinirPrincipale() {
		EOEditingContext edForAdresse = edc();
		setErreurSaisieMessage(null);
		if (getSelectedRepartPersonneAdresse() != null) {
			try {
				edForAdresse.revert();
				getSelectedRepartPersonneAdresse().setRpaPrincipal(EORepartPersonneAdresse.OUI);
				edForAdresse.saveChanges();
			} catch (Exception e) {
				//				e.printStackTrace();
				logger.debug("Erreur dans onAdresseDefinirPrincipale", e);
				setErreurSaisieMessage(e.getMessage());
			}
		}
		return null;
	}


	public EOAdresse getEditAdresse() {
		return editAdresse;
	}

	public WOActionResults onAdresseAnnuler() {
		EOEditingContext edForAdresse = edc();
		setErreurSaisieMessage(null);
		edForAdresse.revert();
		editAdresse = null;
		editRepartPersonneAdresse = null;
		setSelectedRepartPersonneAdresse(null);

		//initialiseSelectedAdresse();
		setIsEditing(Boolean.FALSE);
		setDisplayBtAdresse(Boolean.TRUE);
		return null;
	}

	public WOActionResults onAdresseEnregistrer() {
		EOEditingContext edForAdresse = edc();
		setErreurSaisieMessage(null);
		try {
			if (editAdresse != null && edForAdresse.hasChanges()) {
				if (hasBinding(BINDING_IS_CHANGED)) {
					setBindingIsChanged(true);
				}
				// push de l'adresse dans l'ec parent
				edForAdresse.saveChanges();
			}
			editAdresse = null;
			setSelectedRepartPersonneAdresse(editRepartPersonneAdresse);
			//			setSelectedRepartPersonneAdresse(editRepartPersonneAdresse.localInstanceIn(edc()));
			editRepartPersonneAdresse = null;
			setIsEditing(Boolean.FALSE);
			setDisplayBtAdresse(Boolean.TRUE);
			if (hasBinding(BINDING_onAdresseDidChange))
				return (WOActionResults) valueForBinding(BINDING_onAdresseDidChange);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForAdresse.revert();
			//			e.printStackTrace();
			logger.error("Erreur dans onAdresseEnregistrer", e);
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public boolean showEditButtons() {
		return !isReadOnly() && (getAppUserForAnnuaire().hasDroitCompteModification() || getAppUserForAnnuaire().hasDroitModificationIPersonne(getPersonne()));
		// || getAppUserForAnnuaire().hasDroitModificationIPersonne(getPersonne())
	}

	public Boolean isAdresseCreerEnabled() {
		return Boolean.valueOf(getPersonne() != null && getAppUserForAnnuaire().hasDroitModificationIPersonne(getPersonne()));
	}

	public Boolean isAdresseModifierEnabled() {
		boolean res = true;
		if (myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PERSONNE_ADRESSE_PERSO_DESACTIVE)
				&& getSelectedRepartPersonneAdresse() != null
				&& getSelectedRepartPersonneAdresse().tadrCode().equals(EOTypeAdresse.TADR_CODE_PERSO)) {
			return Boolean.valueOf(false);
		}
		if (getSelectedRepartPersonneAdresse() != null && getQualifierModifiablesForTypeAdresse() != null) {
			res = getQualifierModifiablesForTypeAdresse().evaluateWithObject(getSelectedRepartPersonneAdresse().toTypeAdresse());
		}
		return res && Boolean.valueOf(getSelectedRepartPersonneAdresse() != null && getAppUserForAnnuaire().hasDroitModificationEOAdresse(getSelectedRepartPersonneAdresse().toAdresse()));
	}

	public Boolean isAdresseSupprimerEnabled() {
		return isAdresseModifierEnabled();
	}

	public Boolean isAdresseDefinirPrincipaleEnabled() {
		return Boolean.valueOf(getSelectedRepartPersonneAdresse() != null && !EORepartPersonneAdresse.OUI.equals(getSelectedRepartPersonneAdresse().rpaPrincipal()));
	}

	public NSArray getLesTypeAdressesNonAffectes() {
		EOAdresse adr = getSelectedRepartPersonneAdresse().toAdresse();
		EOQualifier qual = new EOKeyValueQualifier(EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, adr);

		NSArray reps = EOQualifier.filteredArrayWithQualifier(getPersonne().toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE), qual);
		NSArray res = NSArrayCtrl.complementOfNSArray((NSArray) reps.valueForKey(EORepartPersonneAdresse.TO_TYPE_ADRESSE_KEY), getLesTypeAdressesModifiables());
		
		return res;
	}

	public NSArray getLesTypeAdressesModifiables() {
		if (lesTypeAdresses == null) {
			lesTypeAdresses = EOTypeAdresse.fetchAll(edc(), getQualifierModifiablesForTypeAdresse());
		}
		return lesTypeAdresses;
	}

	public EOTypeAdresse getEditTypeAdresse() {
		if (editTypeAdresse == null) {
			if (getLesTypeAdressesModifiables().count() > 0) {
				if (hasBinding(BINDING_defaultTypeAdr)) {
					String code = (String) valueForBinding(BINDING_defaultTypeAdr);
					editTypeAdresse = EOTypeAdresse.fetchByKeyValue(edc(), EOTypeAdresse.TADR_CODE_KEY, code);
				}
				if (editTypeAdresse == null) {
					editTypeAdresse = (EOTypeAdresse) getLesTypeAdressesModifiables().objectAtIndex(0);
				}
			}
		}
		return editTypeAdresse;
	}

	public void setEditTypeAdresse(EOTypeAdresse editTypeAdresse) {
		this.editTypeAdresse = editTypeAdresse;
		if (editRepartPersonneAdresse != null) {
			editRepartPersonneAdresse.setToTypeAdresseRelationship(editTypeAdresse);
			editRepartPersonneAdresse.setTadrCode(editTypeAdresse == null ? null : editTypeAdresse.tadrCode());
		}
	}

	public Boolean isCreatingNewAdresse() {
		return Boolean.valueOf(editAdresse == null || edc().insertedObjects().indexOf(editAdresse) != NSArray.NotFound);
	}

	public EORepartPersonneAdresse getEditRepartPersonneAdresse() {
		return editRepartPersonneAdresse;
	}

	public Boolean isTypesSelonDroits() {
		if (hasBinding(BINDING_isDeterminerTypesSelonDroits)) {
			return (Boolean) valueForBinding(BINDING_isDeterminerTypesSelonDroits);
		}
		else {
			return Boolean.TRUE;
		}

	}

	public String getEmail() {
		if (MyStringCtrl.isEmpty(getEditRepartPersonneAdresse().eMail())
				&& booleanValueForBinding(BINDING_buildDefaultEmail, DEFAULT_buildDefaultEmail).booleanValue()
				&& getEditRepartPersonneAdresse() != null
				&& getPersonne() != null
				&& getPersonne().isIndividu()) {
			setEmail(EORepartPersonneAdresse.computeCanonicalEmailAdresseFromPersonne(getPersonne(), this.edc()));
		}
		return (getEditRepartPersonneAdresse() == null) ? null : getEditRepartPersonneAdresse().eMail();
	}

	public void setEmail(String email) {
		if (editRepartPersonneAdresse != null) {
			editRepartPersonneAdresse.setEMail(email);
		}
	}

	/**
	 * @return the displayBtAdresse
	 */
	public Boolean getDisplayBtAdresse() {
		return displayBtAdresse;
	}

	/**
	 * @param displayBtAdresse the displayBtAdresse to set
	 */
	public void setDisplayBtAdresse(Boolean displayBtAdresse) {
		this.displayBtAdresse = displayBtAdresse;
	}

	public String getPopUpAdressesID() {
		return getComponentId() + "_popupAdresses";
	}

	/**
	 * Test de blocage du bouton de création des adresses
	 * Actuellement seule Profiler empêche la création puisque c'est pour faire uniquement de la consultation
	 * @return true si l'application qui appelle est autorisée
	 */
	public boolean hasNoBlocageCreerAdresse() {
		String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
		boolean isBoutonActif = FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PARAM_ADRESSE_AJOUT_ENABLED);
		if (!MyStringCtrl.isEmpty(nomApplication) && nomApplication.equals("PROFILER") && !isBoutonActif) {
			return false;
		}
		return true;
	}
	
	/**
	 * Test de blocage du bouton de suppression des adresses
	 * Actuellement seule Profiler empêche la suppression puisque c'est pour faire uniquement de la consultation
	 * @return true si l'application qui appelle est autorisée
	 */
	public boolean hasNoBlocageSupprimerAdresse() {
		String nomApplication = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PARAM_NOM_APPLICATION);
		boolean isBoutonActif = FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PARAM_ADRESSE_SUPPRESSION_ENABLED);
		if (!MyStringCtrl.isEmpty(nomApplication) && nomApplication.equals("PROFILER") && !isBoutonActif) {
			return false;
		}
		return true;
	}
	
	public boolean isAdrOnpActive() {
		return isFonctionActive(FONCTION_ADRESSE_ONP);
	}
	
	/**
	 * Méthode privée pour tester une activation de Feature Flipping
	 * @param param nom du paramètre à tester
	 * @return true si le paramètre existe et est à true
	 */
	private boolean isFonctionActive(String param) {
		// Par défaut fonction active
		return ERXProperties.booleanForKeyWithDefault(param, false);
	}
	
}
