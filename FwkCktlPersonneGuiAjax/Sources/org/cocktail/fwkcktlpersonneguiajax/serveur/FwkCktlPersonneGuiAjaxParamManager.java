/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.foundation.ERXThreadStorage;

/**
 * Centralise les parametres propres au framework FwkCktlPersonneGuiAjax.
 * <table>
 * <thead>
 * <tr>
 * <th>Parametre</th>
 * <th>Valeur par défaut</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>org.cocktail.personnegui.fournisseur.detail.individus.showinsee</td>
 * <td>Afficher ou non le N° Insee de l'individu lors de la recherche de fournisseur</td>
 * <td>N</td>
 * </tr>
 * <tr>
 * <td>org.cocktail.personnegui.fournisseur.detail.individus.shownaissance</td>
 * <td>Afficher ou non la date et le pays de naissance de l'individu lors de la recherche de fournisseur</td>
 * <td>N</td>
 * </tr>
 * <tr>
 * <td>org.cocktail.personnegui.fournisseur.form.individus.showinsee</td>
 * <td>Permettre ou non la modification du N° Insee de l'individu lors de la création/modification de fournisseur</td>
 * <td>N</td>
 * </tr>
 * <tr>
 * <td>org.cocktail.personnegui.fournisseur.form.individus.shownaissance</td>
 * <td>Permettre ou non la modification du de la date / pays de naissance de l'individu lors de la création/modification de fournisseur</td>
 * <td>N</td>
 * </tr>
 * </thead>
 * </table>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class FwkCktlPersonneGuiAjaxParamManager extends CktlParamManager {
	public static final Logger log = Logger.getLogger(FwkCktlPersonneGuiAjaxParamManager.class);
	public static final String PARAM_FOURNIS_DETAIL_SHOWINSEE = "org.cocktail.personnegui.fournisseur.detail.individus.showinsee";
	public static final String PARAM_FOURNIS_DETAIL_SHOWNAISSANCE = "org.cocktail.personnegui.fournisseur.detail.individus.shownaissance";
	public static final String PARAM_FOURNIS_FORM_SHOWINSEE = "org.cocktail.personnegui.fournisseur.form.individus.showinsee";
	public static final String PARAM_FOURNIS_FORM_SHOWNAISSANCE = "org.cocktail.personnegui.fournisseur.form.individus.shownaissance";
	public static final String GRHUM_PHOTO_HEIGHT = "org.cocktail.grhum.photo.height";

	public static final String PERSONNE_ADRESSE_PERSO_DESACTIVE = "org.cocktail.fwkcktlpersonnegui.adresse.personaladressmodification.disabled";
	public static final String PERSONNE_ADRESSE_ETUDIANT_ET_PARENT_DESACTIVE = "org.cocktail.fwkcktlpersonnegui.adresse.etudiantadressemodification.disabled";
	public static final String PERSONNE_TEL_INT_DESACTIVE = "org.cocktail.fwkcktlpersonnegui.telephone.internetelmodification.disabled";
	public static final String PERSONNE_TEL_PRF_DESACTIVE = "org.cocktail.fwkcktlpersonnegui.telephone.protelmodification.disabled";

	public static final String PERSONNE_NOM_READONLY_ACTIVE = "org.cocktail.fwkcktlpersonnegui.personne.nom.readonly.enabled";
	public static final String FOURNISSEUR_BT_MAIL_DESACTIVE = "org.cocktail.fwkcktlpersonnegui.fournisseur.btenvoimail.disabled";

	private EOEditingContext ec = ERXEC.newEditingContext();

	public FwkCktlPersonneGuiAjaxParamManager() {

		getParamList().add(PARAM_FOURNIS_DETAIL_SHOWINSEE);
		getParamComments().put(PARAM_FOURNIS_DETAIL_SHOWINSEE, "Afficher ou pas le code INSEE de l'individu lors de la recherche des fournisseurs");
		getParamDefault().put(PARAM_FOURNIS_DETAIL_SHOWINSEE, "N");
		getParamTypes().put(PARAM_FOURNIS_DETAIL_SHOWINSEE, EOGrhumParametresType.codeActivation);

		getParamList().add(PARAM_FOURNIS_DETAIL_SHOWNAISSANCE);
		getParamComments().put(PARAM_FOURNIS_DETAIL_SHOWNAISSANCE, "Afficher ou pas les informations concernant la naissance de l'individu lors de la recherche des fournisseurs");
		getParamDefault().put(PARAM_FOURNIS_DETAIL_SHOWNAISSANCE, "N");
		getParamTypes().put(PARAM_FOURNIS_DETAIL_SHOWNAISSANCE, EOGrhumParametresType.codeActivation);

		getParamList().add(PARAM_FOURNIS_FORM_SHOWINSEE);
		getParamComments().put(PARAM_FOURNIS_FORM_SHOWINSEE, "Afficher ou pas le code INSEE de l'individu lors de la modification des fournisseurs");
		getParamDefault().put(PARAM_FOURNIS_FORM_SHOWINSEE, "N");
		getParamTypes().put(PARAM_FOURNIS_FORM_SHOWINSEE, EOGrhumParametresType.codeActivation);

		getParamList().add(PARAM_FOURNIS_FORM_SHOWNAISSANCE);
		getParamComments().put(PARAM_FOURNIS_FORM_SHOWNAISSANCE, "Afficher ou pas les informations concernant la naissance de l'individu lors de la modification des fournisseurs");
		getParamDefault().put(PARAM_FOURNIS_FORM_SHOWNAISSANCE, "N");
		getParamTypes().put(PARAM_FOURNIS_FORM_SHOWNAISSANCE, EOGrhumParametresType.codeActivation);

		getParamList().add(GRHUM_PHOTO_HEIGHT);
		getParamComments().put(GRHUM_PHOTO_HEIGHT, "Hauteur par défaut des photos");
		getParamDefault().put(GRHUM_PHOTO_HEIGHT, "130");
		getParamTypes().put(GRHUM_PHOTO_HEIGHT, EOGrhumParametresType.valeurNumerique);

		// NE PAS SUPPRIMER C'EST A GERER PLUS TARD !

		//		getParamList().add(PERSONNE_ADRESSE_PERSO_DESACTIVE);
		//		getParamComments().put(PERSONNE_ADRESSE_PERSO_DESACTIVE, "Autoriser ou non un individu à modifier son adresse personnelle");
		//		getParamDefault().put(PERSONNE_ADRESSE_PERSO_DESACTIVE, "N");
		//		getParamTypes().put(PERSONNE_ADRESSE_PERSO_DESACTIVE, EOGrhumParametresType.codeActivation);
		//		
		//		getParamList().add(PERSONNE_TEL_INT_DESACTIVE);
		//		getParamComments().put(PERSONNE_TEL_INT_DESACTIVE, "Autoriser ou non un individu à modifier son numéro de téléphone interne");
		//		getParamDefault().put(PERSONNE_TEL_INT_DESACTIVE, "N");
		//		getParamTypes().put(PERSONNE_TEL_INT_DESACTIVE, EOGrhumParametresType.codeActivation);
		//		
		//		getParamList().add(PERSONNE_TEL_PRF_DESACTIVE);
		//		getParamComments().put(PERSONNE_TEL_PRF_DESACTIVE, "Autoriser ou non un individu à modifier son numéro de téléphone interne");
		//		getParamDefault().put(PERSONNE_TEL_PRF_DESACTIVE, "N");
		//		getParamTypes().put(PERSONNE_TEL_PRF_DESACTIVE, EOGrhumParametresType.codeActivation);

		//		getParamList().add(PERSONNE_NOM_READONLY_ACTIVE);
		//		getParamComments().put(PERSONNE_NOM_READONLY_ACTIVE, "Autoriser ou non un individu à modifier le nom d'une personne (physique ou morale)");
		//		getParamDefault().put(PERSONNE_NOM_READONLY_ACTIVE, "N");
		//		getParamTypes().put(PERSONNE_NOM_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);

	}

	@Override
	public void checkAndInitParamsWithDefault() {

		//Recuperer un grhum_createur
		String cptLogin = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
		if (cptLogin != null) {
			EOCompte cpt = EOCompte.compteForLogin(ec, cptLogin);
			if (cpt != null) {
				ERXThreadStorage.takeValueForKey(cpt.persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			}
		}

		super.checkAndInitParamsWithDefault();
	}

	/**
	 * Cree un nouveau parametre de type EOGrhumParametresType.codeActivation
	 */
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);
	}

	public void createNewParam(String key, String value, String comment, String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {

				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();

			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}
	}

	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		//		String res = EOGrhumParametres.parametrePourCle(ec, key);
		return res;
	}

}
