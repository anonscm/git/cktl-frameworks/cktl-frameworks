/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

/**
 * Gestion du versioning. Modifiez cette classe pour gérer les dépendances de version. Modifiez la classe VersionMe pour les changements de version.
 */
public final class Version extends A_CktlVersion {
	// Nom de l'appli
	public static final String APPLICATIONFINALNAME = VersionMe.APPLICATIONFINALNAME;
	public static final String APPLICATIONINTERNALNAME = VersionMe.APPLICATIONINTERNALNAME;
	public static final String APPLICATION_STRID = VersionMe.APPLICATION_STRID;

	// Version appli
	public static final long SERIALVERSIONUID = VersionMe.SERIALVERSIONUID;

	public static final int VERSIONNUMMAJ = VersionMe.VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN = VersionMe.VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH = VersionMe.VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD = VersionMe.VERSIONNUMBUILD;

	public static final String VERSIONDATE = VersionMe.VERSIONDATE;
	public static final String COMMENT = VersionMe.COMMENT;

	private static final String FWKCKTLAJAXWEBEXT_VERSION_MIN = "1.0.0.4";
	private static final String FWKCKTLAJAXWEBEXT_VERSION_MAX = null;

	/** Version de frmkCtrlPersonne */
	private static final String FWKCKTLPERSONNE_VERSION_MIN = "1.7.1.5";
	private static final String FWKCKTLPERSONNE_VERSION_MAX = null;

	/** Version du frmk Themes */
	private static final String FWKCKTLTHEMES_REVISION_MIN = "10245";
	private static final String FWKCKTLTHEMES_REVISION_MAX = null;

	@Override
	public String name() {
		return APPLICATIONFINALNAME;
	}

	public String internalName() {
		return APPLICATIONINTERNALNAME;
	}

	@Override
	public int versionNumBuild() {
		return VERSIONNUMBUILD;
	}

	@Override
	public int versionNumMaj() {
		return VERSIONNUMMAJ;
	}

	@Override
	public int versionNumMin() {
		return VERSIONNUMMIN;
	}

	@Override
	public int versionNumPatch() {
		return VERSIONNUMPATCH;
	}

	@Override
	public String date() {
		return VERSIONDATE;
	}

	@Override
	public String comment() {
		return COMMENT;
	}

	/**
	 * Liste des dépendances
	 * 
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#dependencies()
	 */
	@Override
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new org.cocktail.fwkcktlajaxwebext.serveur.Version(), FWKCKTLAJAXWEBEXT_VERSION_MIN, FWKCKTLAJAXWEBEXT_VERSION_MAX, true)
				,
				new CktlVersionRequirements(new org.cocktail.fwkcktlpersonne.server.Version(), FWKCKTLPERSONNE_VERSION_MIN, FWKCKTLPERSONNE_VERSION_MAX, false)
				,
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.CktlRevisionFwkCktlThemes(), FWKCKTLTHEMES_REVISION_MIN, FWKCKTLTHEMES_REVISION_MAX, false)
		//,new CktlVersionRequirements(new CktlVersionJar("net.sf.jasperreports.engine.JasperReport"), JASPER_VERSION_MIN, JASPER_VERSION_MAX, true)
		};
	}

}
