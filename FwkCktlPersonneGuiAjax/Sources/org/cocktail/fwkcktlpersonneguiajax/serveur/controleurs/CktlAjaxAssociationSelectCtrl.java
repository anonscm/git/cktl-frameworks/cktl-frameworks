/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.CktlAjaxAssociationSelect;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxTreeModel;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CktlAjaxAssociationSelectCtrl {
	private CktlAjaxAssociationSelect wocomponent;

	private Object _delegate;
	private AssociationNode rootAssociation;
	private AssociationNode unAssociation;

	public CktlAjaxAssociationSelectCtrl(CktlAjaxAssociationSelect component) {
		wocomponent = component;

	}

	private EOEditingContext edc() {
		return wocomponent.edc();
	}

	public WOActionResults afficherAssociationSelectionne() {
		AssociationNode node = unAssociation();
		if (node != null && node != rootAssociation()) {
			EOAssociation association = (EOAssociation) node.object();
			((CktlAjaxAssociationSelect) wocomponent).setSelection(association);
			rootAssociation = null;
		}
		CktlAjaxWindow.close(wocomponent.context(), ((CktlAjaxAssociationSelect) wocomponent).associationTreeViewDialogID());
		return null;
	}

	public AssociationNode rootAssociation() {
		if (rootAssociation == null ||
				(wocomponent.treeRootObject() != null && rootAssociation.object() instanceof EmptyRootObject) ||
				(wocomponent.treeRootObject() == null && !(rootAssociation.object() instanceof EmptyRootObject))) {
			if (wocomponent.treeRootObject() != null) {
				rootAssociation = new AssociationNode(null, wocomponent.treeRootObject());
			}
			else {
				rootAssociation = new AssociationNode(null, new EmptyRootObject());
			}
		}
		return rootAssociation;
	}

	public void setRootAssociation(AssociationNode rootAssociation) {
		this.rootAssociation = rootAssociation;
	}

	public AssociationNode unAssociation() {
		return unAssociation;
	}

	public void setUnAssociation(AssociationNode unAssociation) {
		this.unAssociation = unAssociation;
	}

	public String unAssociationLibelle() {
		NSKeyValueCoding obj = unAssociation().object();
		return (String) obj.valueForKey(EOAssociation.ASS_LIBELLE_KEY);
	}

	public Object delegate() {
		if (_delegate == null) {
			_delegate = new CktlAjaxAssociationSelectCtrl.Delegate();
		}
		return _delegate;

	}

	public class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			if (node != null) {
				return ((AssociationNode) node).children();
			}
			return NSArray.EmptyArray;
		}

		public boolean isLeaf(Object node) {
			if (node != null) {
				return ((AssociationNode) node).isLeaf();
			}
			return true;
		}

		public Object parentTreeNode(Object node) {
			if (node != null) {
				return ((AssociationNode) node).parent();
			}
			return null;
		}
	}

	public class AssociationNode {
		public NSArray children;
		public AssociationNode parent;
		public NSKeyValueCoding object;

		public AssociationNode(AssociationNode parent, NSKeyValueCoding obj) {
			object = obj;
			this.parent = parent;
		}

		public NSArray children() {
			if (children == null) {
				NSArray res = NSArray.EmptyArray;
				if (object() instanceof EmptyRootObject) {
					res = EOAssociation.getRacines(edc());
					//					if (wocomponent.getTypeAssociationQualifier() != null) {
					//						res = EOQualifier.filteredArrayWithQualifier(res, wocomponent.getTypeAssociationQualifier());
					//					}					
				}
				else {
					NSMutableArray array = new NSMutableArray();
//					if (wocomponent.getQualifier() != null) {
//						array.addObject(wocomponent.getQualifier());
//					}
					//					if (wocomponent.getTypeAssociationQualifier() != null) {
					//						array.addObject( wocomponent.getTypeAssociationQualifier());
					//					}
					if (array.count() > 0) {
						res = EOQualifier.filteredArrayWithQualifier(((EOAssociation) object()).getFils(edc()), new EOAndQualifier(array));
					}
					else {
						res = ((EOAssociation) object()).getFils(edc());
					}
				}
				NSMutableArray res2 = new NSMutableArray();
				for (int i = 0; i < res.count(); i++) {
					AssociationNode node = new AssociationNode(this, (NSKeyValueCoding) res.objectAtIndex(i));
					res2.addObject(node);
				}
				children = res2.immutableClone();
			}
			return children;
		}

		public boolean isLeaf() {
			return (children().count() == 0);
		}

		public AssociationNode parent() {
			return parent;
		}

		public void setParent(AssociationNode node) {
			parent = node;
		}

		public NSKeyValueCoding object() {
			return object;
		}
	}

	public class EmptyRootObject implements NSKeyValueCoding {

		public EmptyRootObject() {
		}

		public void takeValueForKey(Object arg0, String arg1) {

		}

		public Object valueForKey(String arg0) {
			return "root";
		}

	}

}
