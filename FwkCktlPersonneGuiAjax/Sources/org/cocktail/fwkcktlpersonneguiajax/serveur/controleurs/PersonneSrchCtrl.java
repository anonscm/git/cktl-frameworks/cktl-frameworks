/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.AFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneSrch;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class PersonneSrchCtrl extends AComponentCtrl {

	public PersonneSrchCtrl(AComponent component) {
		super(component);
	}

	public PersonneSrchCtrl() {
		super();
	}

	@Override
	public PersonneSrch getMyComponent() {
		return (PersonneSrch) super.getMyComponent();
	}

	public Boolean isUserCanCreerStructure() {
		// LRLogger.debugCaller();
		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitCreerStructure(null));
	}

	public Boolean isUserCanCreerIndividu() {
		// LRLogger.debugCaller();
		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitCreerIndividu(null));
	}

	public EOIndividu creerIndividu() {
		// On cree l'individu a partir des informations saisies
		EOIndividu individu = EOIndividu.creerInstance(edc(), new NSArray());
		individu.setPersIdCreation(getMyComponent().getUtilisateurPersId());
		individu.setPersIdModification(getMyComponent().getUtilisateurPersId());
		individu.setNomAffichage(MyStringCtrl.initCap(getMyComponent().getSrchNom()));
		individu.setNomPatronymique(MyStringCtrl.initCap(getMyComponent().getSrchNom()));
		individu.setPrenomAffichage(MyStringCtrl.initCap(getMyComponent().getSrchPrenom()));
		if (getAppUserForAnnuaire().iPersonne().isIndividu()
				&& ((EOIndividu) getAppUserForAnnuaire().iPersonne()).noIndividu() != null) {
			individu.setNoIndividuCreateur(((EOIndividu) getAppUserForAnnuaire().iPersonne()).noIndividu());
		}
		
		individu.setToPaysNaissanceRelationship(EOPays.getPaysDefaut(edc()));
		individu.setToPaysNationaliteRelationship(EOPays.getPaysDefaut(edc()));
		
		return individu;
	}

	public EOStructure creerStructure() {
		EOStructure structure = EOStructure.creerInstance(edc(), new NSArray());
		structure.setPersIdCreation(getMyComponent().getUtilisateurPersId());
		structure.setPersIdModification(getMyComponent().getUtilisateurPersId());
		structure.setStrAffichage(MyStringCtrl.initCap(getMyComponent().getSrchNom()));
		structure.setSiret(getMyComponent().getSrchSiret());
		return structure;
	}

	public Boolean isUserCanModifier() {
		return Boolean.valueOf(getAppUserForAnnuaire().hasDroitModificationIPersonne(
				getMyComponent().getSelectedPersonne()));
	}

	/**
	 * Effectue la recherche a partir des criteres et remplit le displayGroup.
	 * Appelle {@link PersonneSrchCtrl#checkSaisie()} et
	 * {@link PersonneSrchCtrl#getResultats()}. Donc privilegiez la surcharge de
	 * ces deux méthodes si besoin.
	 * 
	 * @throws Exception
	 */
	public void rechercher() throws Exception {
		NSArray res = NSArray.EmptyArray;
		getMyComponent().displayGroup().setObjectArray(res);
		checkSaisie();
		res = getResultats();
		getMyComponent().displayGroup().setObjectArray(res);
		if (getMyComponent().isSelectionMultipleEnabled()) {
			getMyComponent().displayGroup().clearSelection();
		}
	}

	/**
	 * Verifie si les champs necessaires a la recherche sont bien remplis.
	 * 
	 * @throws Exception
	 */
	public void checkSaisie() throws Exception {
		// Si tous les criteres sont null exception
		if (isRechercheMoraleExterne()) {
			if (MyStringCtrl.isEmpty(getMyComponent().getSrchNom())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchSiret())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchSigle())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchFouCode())) {
				throw new NSValidation.ValidationException("Veuillez remplir les champs de recherche.");
			}

			if (!MyStringCtrl.isEmpty(getMyComponent().getSrchSiret())
					&& !MyStringCtrl.isDigits(getMyComponent().getSrchSiret())) {
				throw new NSValidation.ValidationException("Le numéro de Siret doit etre numerique.");
			}
		}

		if (isRechercheMoraleInterne()) {
			if (MyStringCtrl.isEmpty(getMyComponent().getSrchNom())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchSigle())) {
				throw new NSValidation.ValidationException("Veuillez remplir les champs de recherche.");
			}
		}

		if (isRechercheIndividuInterne() || isRechercheIndividuExterne()) {
			if (MyStringCtrl.isEmpty(getMyComponent().getSrchNom())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchPrenom())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchEmail())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchStudentNumber())) {
				throw new NSValidation.ValidationException("Veuillez remplir les champs de recherche.");
			}
			
		   if ( !MyStringCtrl.isEmpty(getMyComponent().getSrchStudentNumber()) 
			        && !MyStringCtrl.isDigits(getMyComponent().getSrchStudentNumber())) {
				throw new NSValidation.ValidationException("Le numéro d'étudiant doit etre numérique.");
		   }
		}
	}

	/**
	 * Cette méthode ne retourne que les résutats présents en base. Si vous avez besoin d'avoir, dans la liste des
	 * résulats, des objets pas encore commités, alors il faut utiliser
	 * {@link ERXEOControlUtilities#filteredObjectsWithQualifier}<br>
	 * Exemple d'utilisation dans l'application Stages
	 * (org.cocktail.stages.serveur.components.controlers.StagesPersonneSrchCtrl)
	 * 
	 * @return Le résultat de la recherche. Methode a surcharger si vous voulez specialiser la recherche.
	 */
	public NSArray getResultats() {	
		NSMutableArray res = new NSMutableArray();
		EOQualifier qual = null;
		
		EOQualifier qualStructures = getMyComponent().getQualifierForStructures();
		EOQualifier qualIndividus = getMyComponent().getQualifierForIndividus();
		
		qual = ERXQ.and(qual, getMyComponent().getQualifierForGroupes());
        
		if (!MyStringCtrl.isEmpty(getMyComponent().getSrchStudentNumber())) {
			EOIndividu ind = EOIndividu.individuWithEtudNumeroEquals(edc(), getMyComponent().getSrchStudentNumber());
			if (ind==null)  return new NSArray<EOIndividu>();
			EOQualifier qualEtudiant = ERXQ.equals(EOIndividu.NO_INDIVIDU_KEY, ind.noIndividu());
			if (MyStringCtrl.isEmpty(getMyComponent().getSrchNom())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchPrenom())
					&& MyStringCtrl.isEmpty(getMyComponent().getSrchEmail())) {
				qual = ERXQ.and(qual, qualIndividus, qualEtudiant);
				return EOIndividu.fetchAllValides(edc(), qual, null);
			} 
			qualIndividus = ERXQ.and(qualIndividus, qualEtudiant);
        }
		
		if(isRechercheStructureSimple()) {
		  res = rechercherPersonnesMorales(qual, qualStructures);
		} else {
  		if (isRechercheMoraleExterne()) {
  			res = rechercherPersonnesMoralesExternes(qual, qualStructures);
  		}
  
  		if (isRechercheMoraleInterne()) {
  			res = recherchePersonnesMoralesInternes(qual, qualStructures);
  		}
		}

		if(isRechercheIndividuSimple()) {
		  res = rechercherTousLesIndividus(qualIndividus);
		} else {
		  
	    if (isRechercheIndividuInterne()) {
	       res = rechercherPersonnesPhysiquesInternes(qual, qualIndividus);
	    }

	    if (isRechercheIndividuExterne()) {
	      res = rechercherPersonnesPhysiquesExternes(qual, qualIndividus);
	    }
	    
		}
		
		return ERXArrayUtilities.arrayWithoutDuplicates(res);
	}


  
  public NSMutableArray<EOIndividu> rechercherPersonnesPhysiquesExternes(EOQualifier qual, EOQualifier qualIndividus) {
    NSMutableArray<EOIndividu> resultat = new NSMutableArray<EOIndividu>();
    EOQualifier qualifierPersonnesPhysiquesExternes = ERXQ.and(qual, qualIndividus);
    resultat.addObjectsFromArray(EOIndividu.individusExternesByNameFirstnameAndEmail(edc(), getMyComponent()
        .getSrchNom(), getMyComponent().getSrchPrenom(), getMyComponent().getSrchEmail(), qualifierPersonnesPhysiquesExternes,
        getMyComponent().getFetchLimit().intValue()));
    resultat.addObjectsFromArray(EOIndividu.individusWithMiddleNameLike(edc(),getMyComponent().getSrchNom(),qualIndividus, getMyComponent().getFetchLimit().intValue()));
    AFinder.removeDuplicatesInNSArray(resultat);
   return resultat;
  }
  
  public NSMutableArray<EOIndividu> rechercherPersonnesPhysiquesInternes(EOQualifier qual, EOQualifier qualIndividus) {
    NSMutableArray<EOIndividu> resultat = new NSMutableArray<EOIndividu>();
    resultat.addObjectsFromArray(EOIndividu.individusInternesByNameAndFirstname(edc(),
        getMyComponent().getSrchNom(), getMyComponent().getSrchPrenom(),
        qualIndividus, getMyComponent().getFetchLimit().intValue()));
       EOQualifier qualifierPersonnesPhysiquesInternes = ERXQ.and(qual, qualIndividus);
       resultat.addObjectsFromArray(EOIndividu.individusInternesByNameFirstnameAndEmail(edc(), getMyComponent()
           .getSrchNom(), getMyComponent().getSrchPrenom(), getMyComponent().getSrchEmail(), qualifierPersonnesPhysiquesInternes,
           getMyComponent().getFetchLimit().intValue()));
       resultat.addObjectsFromArray(EOIndividu.individusWithMiddleNameLike(edc(),getMyComponent().getSrchNom(),qualIndividus, getMyComponent().getFetchLimit().intValue()));
       AFinder.removeDuplicatesInNSArray(resultat);
       return resultat;
  }




  
  public NSMutableArray<EOIndividu> rechercherTousLesIndividus(EOQualifier qualIndividus) {
    NSMutableArray<EOIndividu> resultat = new NSMutableArray<EOIndividu>();
    resultat.addObjectsFromArray(EOIndividu.individusByNameAndFirstnameAndEmail(edc(),
         getMyComponent().getSrchNom(), getMyComponent().getSrchPrenom(), getMyComponent().getSrchEmail(),
         qualIndividus, getMyComponent().getFetchLimit().intValue()));
    resultat.addObjectsFromArray(EOIndividu.individusWithMiddleNameLike(edc(), getMyComponent().getSrchNom(), qualIndividus, getMyComponent().getFetchLimit().intValue()));
    AFinder.removeDuplicatesInNSArray(resultat);
    return resultat;
  }


  
  public NSMutableArray recherchePersonnesMoralesInternes(EOQualifier qual, EOQualifier qualStructures) {
    NSMutableArray<EOStructure> res = new NSMutableArray<EOStructure>();

    EOQualifier qualifierPersonneMoraleInterne = ERXQ.and(qual, qualStructures);
    res.addObjectsFromArray(EOStructure.structuresInternesByNameAndSigleAndSiretAndFouCode(edc(),
        getMyComponent().getSrchNom(), getMyComponent().getSrchSigle(), null, null, qualifierPersonneMoraleInterne, getMyComponent()
            .getFetchLimit().intValue()));
    res = filtrerLesStructuresNonAffichables(res);
    return res;
  }

  
  public NSMutableArray<EOStructure> rechercherPersonnesMorales(EOQualifier qual, EOQualifier qualStructures) {
    NSMutableArray<EOStructure> res = new NSMutableArray<EOStructure>();
    EOQualifier qualifierPersonneMoraleInterne = ERXQ.and(qual, qualStructures);
    res.addObjectsFromArray(EOStructure.structuresByNameAndSigleAndSiretAndFouCode(edc(),
        getMyComponent().getSrchNom(), getMyComponent().getSrchSigle(), null, null, qualifierPersonneMoraleInterne, getMyComponent()
            .getFetchLimit().intValue()));
    res = filtrerLesStructuresNonAffichables(res);
    return res;
  }

  protected NSMutableArray<EOStructure> filtrerLesStructuresNonAffichables(NSMutableArray res) {
    return EOStructure.filtrerLesStructuresNonAffichables(res, edc(), getAppUserForAnnuaire().getPersId())
    		.mutableClone();
  }
  
  
  public NSMutableArray rechercherPersonnesMoralesExternes(EOQualifier qual, EOQualifier qualStructures) {
    NSMutableArray<EOStructure> res = new NSMutableArray<EOStructure>();

    EOQualifier qualifierMoraleExterne = ERXQ.and(qual, EOStructure.QUAL_STRUCTURES_TYPE_AUTRES);
    if (qualStructures != null) {
      qualifierMoraleExterne = ERXQ.and(qualifierMoraleExterne, qualStructures);
    }
    res.addObjectsFromArray(EOStructure.structuresExternesByNameAndSigleAndSiretAndFouCode(edc(),
        getMyComponent().getSrchNom(), getMyComponent().getSrchSigle(), getMyComponent().getSrchSiret(),
        getMyComponent().getSrchFouCode(), qual, getMyComponent().getFetchLimit().intValue()));
    res = filtrerLesStructuresNonAffichables(res);
    return res;
  }
  

  public boolean isRechercheIndividuSimple() {
    return getMyComponent().getPersTypePhysiqueSelected().booleanValue() && getMyComponent().searchOnInterneAndExterne();
  }

  public boolean isRechercheStructureSimple() {
    return getMyComponent().getPersTypeMoraleSelected().booleanValue() && getMyComponent().searchOnInterneAndExterne();
  }

  

	public boolean isRechercheMoraleExterne() {
		return (getMyComponent().getPersTypeMoraleSelected().booleanValue() && (getMyComponent()
				.getPersTypeExterneSelected().booleanValue() || getMyComponent().searchOnInterneAndExterne()));
	}

	public boolean isRechercheMoraleInterne() {
		return (getMyComponent().getPersTypeMoraleSelected().booleanValue() && (getMyComponent()
				.getPersTypeInterneSelected().booleanValue() || getMyComponent().searchOnInterneAndExterne()));
	}

	public boolean isRechercheIndividuExterne() {
		return (getMyComponent().getPersTypePhysiqueSelected().booleanValue() && (getMyComponent()
				.getPersTypeExterneSelected().booleanValue() || getMyComponent().searchOnInterneAndExterne()));
	}

	public boolean isRechercheIndividuInterne() {
		return (getMyComponent().getPersTypePhysiqueSelected().booleanValue() && (getMyComponent()
				.getPersTypeInterneSelected().booleanValue() || getMyComponent().searchOnInterneAndExterne()));
	}



}
