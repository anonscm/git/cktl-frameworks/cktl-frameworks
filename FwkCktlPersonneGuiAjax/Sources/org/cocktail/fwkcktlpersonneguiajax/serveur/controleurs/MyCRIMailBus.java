/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class MyCRIMailBus extends CktlMailBus {
	private static String FORWARD_ALL_TO = null;

	public MyCRIMailBus(CktlConfig config) {
		super(config);
	}

	public boolean sendMail(String from, String to, String cc, String subject, String msgText) {
		if (FORWARD_ALL_TO != null) {
			System.out.println("Envoie d'un mail a " + FORWARD_ALL_TO);
			String newBody = "Le message suivant vous est envoye pour test.\n" +
					"TO : " + to + "\n" +
					"CC : " + cc + "\n" +
					"--------------------------------------------------------\n" +
					msgText;
			return super.sendMail(from, FORWARD_ALL_TO, null, subject, newBody);
		}
		System.out.println("Envoie d'un mail a " + to);
		return super.sendMail(from, to, cc, subject, msgText);
	}

	public static String getFORWARD_ALL_TO() {
		return FORWARD_ALL_TO;
	}

	public static void setFORWARD_ALL_TO(String forward_all_to) {
		System.out.println("Redirection de tous les emails envoyes avec MyCktlMailBus vers " + forward_all_to);
		FORWARD_ALL_TO = forward_all_to;
	}

}
