/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;

/**
 * Construit et envoie des messages e-mail. Elle contient des methodes facilitant la construction des messages avec les plusieurs destinataires et les
 * attachements. <h3>Example:</h3> <h4>Envoi d'un message</h4> Le message peut etre envoye en le construissant et en appelant la methode
 * <code>send()</code>. Une exception est leve dans le cas des adresses mail incorrectes ou d'autres erreurs survenues lors de l'envoi. Elles doivent
 * etre traitees explicitement.
 * <p/>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * &lt;code class=&quot;comment&quot;&gt;// L'adresse du serveur mail&lt;/code&gt;
 * String smtpHost = &quot;mail.univ-lr.fr&quot;;
 * &lt;code class=&quot;comment&quot;&gt;// Les adresses mail des destinataires, etc...&lt;/code&gt;
 * String from = &quot;arunas@ulr.fr&quot;;
 * String to = &quot;hugues@ulr.fr&quot;;
 * String reply = &quot;arunas@company.com&quot;;
 * String cc = &quot;jm@ulr.com, cyril@ulr.fr&quot;;
 * &lt;code class=&quot;comment&quot;&gt;// Messages, etc...&lt;/code&gt;
 * String subject = &quot;Test Mail&quot;;
 * String body = &quot;Un message de test avec un fichier attache.&quot;;
 * &lt;code class=&quot;comment&quot;&gt;// Le chemin de fichier attache&lt;/code&gt;
 * String files = &quot;E:/DocumentTest.pdf&quot;;
 * String[] fileNames;
 * &lt;code class=&quot;comment&quot;&gt;// On construit un message un indiquant le serveur mail&lt;/code&gt;
 * CktlMailMessage mailMessage = new CktlMailMessage(smtpHost);
 * &lt;code class=&quot;comment&quot;&gt;// Un tableau des noms des fichiers&lt;/code&gt;
 * fileNames = CktlMailMessage.toArray(files);
 * &lt;code class=&quot;comment&quot;&gt;// Initialiser les message&lt;/code&gt;
 * mailMessage.initMessage(from, to, subject, body, fileNames);
 * &lt;code class=&quot;comment&quot;&gt;// Les informations supplementaires&lt;/code&gt;
 * mailMessage.addCCs(CktlMailMessage.toArray(cc));
 * mailMessage.setReplyTo(reply);
 * &lt;code class=&quot;comment&quot;&gt;// L'envoi du message.
 * // Cet appel peut lever des exceptions dans le cas d'un probleme. &lt;/code&gt;
 * try {
 *   mailMessage.send();
 * } catch(Throwable ex) {
 *   ex.printStackTrace();
 *   &lt;code class=&quot;comment&quot;&gt;// Traitement de l'erreur...&lt;/code&gt;
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * <h4>Envoi &quot;sur&quot;</h4> L'envoi d'un message echoue s'il y a des adresses incorrectes parmi les adresses mails specifiees. Pour envoyer le
 * message uniquement aux adresse correctes et ignorant celles incorrectes, on peut utiliser la methode safeSend().
 * <p/>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * &lt;code class=&quot;comment&quot;&gt;// L'adresse du serveur mail&lt;/code&gt;
 * String smtpHost = &quot;mail.univ-lr.fr&quot;;
 * &lt;code class=&quot;comment&quot;&gt;// Les adresses mail des destinataires, etc...&lt;/code&gt;
 * String from = &quot;arunas@ulr.fr&quot;;
 * String to = &quot;hugues@ulr.fr&quot;;
 * String reply = &quot;arunas@company.com&quot;;
 * String cc = &quot;jm@ulr.com, cyril@ulr.fr&quot;;
 * &lt;code class=&quot;comment&quot;&gt;// Messages, etc...&lt;/code&gt;
 * String subject = &quot;Test Mail&quot;;
 * String body = &quot;Un message de test.&quot;;
 * &lt;code class=&quot;comment&quot;&gt;// On construit un message un indiquant le serveur mail&lt;/code&gt;
 * CktlMailMessage mailMessage = new CktlMailMessage(smtpHost);
 * &lt;code class=&quot;comment&quot;&gt;// Initialiser les message&lt;/code&gt;
 * mailMessage.initMessage(from, to, subject, body);
 * &lt;code class=&quot;comment&quot;&gt;// Les informations supplementaires&lt;/code&gt;
 * mailMessage.addCCs(CktlMailMessage.toArray(cc));
 * mailMessage.setReplyTo(reply);
 * &lt;code class=&quot;comment&quot;&gt;// L'envoi &quot;sur&quot; du message&lt;/code&gt;
 * try {
 *   Vector pbAdrs = mailMessage.safeSend();
 *   if (pbAdrs.size() == 0)
 *     System.out.println(&quot;OK. Le message envoye.&quot;);
 *   else
 *     System.out.println(&quot;Adresses erronees : &quot;+pb);
 * } catch(Throwable ex) {
 *   ex.printStackTrace();
 *   &lt;code class=&quot;comment&quot;&gt;// Traitement de l'erreur...&lt;/code&gt;
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 */
public class MyCRIMailMessage {

	/**
	 * L'objet Session, necessaire pour creer un message.
	 */
	private javax.mail.Session session;

	/**
	 * L'encodage du contenue du message (codepage).
	 */
	private String encoding;

	/**
	 * Le message a envoyer.
	 */
	private MimeMessage message;

	/**
	 * La liste des adresses TO. Au moins une des adresses TO, CC ou BCC doit etre specifiee.
	 */
	private Vector addrsTO;

	/**
	 * La liste des adresses CC. Au moins une des adresses TO, CC ou BCC doit etre specifiee.
	 */
	private Vector addrsCC;

	/**
	 * La liste des adresses BCC. Au moins une des adresses TO, CC ou BCC doit etre specifiee.
	 */
	private Vector addrsBCC;

	/**
	 * Le type de contenu du message.
	 */
	private String contentType;

	/**
	 * Cree un nouveau objet pour gerer l'envoi des messages mail. L'objet cree peut etre reutilise pour envoyer plusieurs messages. Dans ce cas, il
	 * faut faire plusieurs appels a la methode initMessage.
	 * 
	 * @param mailServer L'adresse du serveur mail via lequel le message sera expedie.
	 */
	public MyCRIMailMessage(String mailServer) {
		Properties props = System.getProperties();
		// Serveur de mail
		props.put("mail.smtp.host", mailServer);
		// La session
		session = javax.mail.Session.getDefaultInstance(props, null);
		// Encodage par defaut
		encoding = "iso-8859-1";
		contentType = null;
		addrsTO = new Vector();
		addrsCC = new Vector();
		addrsBCC = new Vector();
	}

	/**
	 * Cree un message mail avec les fichiers attaches. Les parametres doivent etre correctement formates. Par exemple, "attachements" est un tableau
	 * des objets String's representant les chemins (complets) d'acces aux fichiers. Pour construite un tableau, on peut utiliser la methode statique
	 * <code>toArray()</code> de la classe.
	 * <p/>
	 * Plus d'information peut etre indiquee a l'aide des appels consecutifs des methodes <code>addCC()</code>, <code>setReplyTo()</code>, etc...
	 * <p/>
	 * Le message construite un utilisant cette methode sera envoie au format "multipart". Il est effectivement envoye lors de l'appel aux methodes
	 * <code>send()</code> ou <code>safeSend()</code>.
	 * 
	 * @param from L'adresse d'expediteur.
	 * @param to L'adresse du destinataire. Plusieurs adresses peuvent etre indiquees en les separant par virgule (",").
	 * @param subject Le sujet du message.
	 * @param body Le text du message.
	 * @param attachements La liste des chemins des fichiers qui serons attaches au message. <code>null</code> si il n'y a aucun fichier.
	 * @exception MessagingException
	 * @exception AddressException
	 */
	public void initMessage(String from, String to, String subject, String body, String[] attachements) throws MessagingException, AddressException {
		// Nouvelle message avec l'entete definie
		createNewMimeMessage(from, to, subject);
		// Le contenu du message multi-part
		Multipart multipart = new MimeMultipart();
		// Le text du message
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		// messageBodyPart.setText(body, encoding);
		setContent(messageBodyPart, body);
		multipart.addBodyPart(messageBodyPart);
		// Les fichiers attaches, s'il en a
		if (attachements != null) {
			for (int i = 0; i < attachements.length; i++) {
				if (attachements[i] == null)
					continue;
				messageBodyPart = new MimeBodyPart();
				// Le fichier attache
				DataSource source = new FileDataSource(attachements[i]);
				messageBodyPart.setDataHandler(new DataHandler(source));
				// Le nom du fichier qu'on vera dans le message
				messageBodyPart.setFileName(getNameFromPath(attachements[i]));
				multipart.addBodyPart(messageBodyPart);
			}
		}
		// Ajouter le contenu dans le message
		message.setContent(multipart);
	}

	/**
	 * Cree un message mail avec les fichiers attaches. Les parametres "attacheNames" et "attachData" representent respectivement les tableaux des
	 * noms des fichiers et de leurs contenus (les tableaux d'octes). Les deux tableaux doivent contenir le meme nombre d'elements.
	 * <p/>
	 * Plus d'information peut etre indiquee a l'aide des appels consecutifs des methodes <code>addCC()</code>, <code>setReplyTo()</code>, etc...
	 * <p/>
	 * Le message construite un utilisant cette methode sera envoie au format "multipart". Il est effectivement envoye lors de l'appel aux methodes
	 * <code>send()</code> ou <code>safeSend()</code>.
	 * 
	 * @param from L'adresse d'expediteur.
	 * @param to L'adresse destinataire. Plusieurs adresses peuvent etre donnees en les separant par virgule (",").
	 * @param subject Le sujet du message.
	 * @param body Le texte du message.
	 * @param attachNames Les noms des fichiers attaches.
	 * @param attachData Les contenus des fichiers attaches (les tableaux d'octets).
	 * @exception MessagingException
	 * @exception AddressException
	 */
	public void initMessage(String from, String to, String subject, String body, String[] attachNames, byte[][] attachData) throws MessagingException, AddressException {
		// Nouvelle message avec l'entete definie
		createNewMimeMessage(from, to, subject);
		// Le contenu du message multi-part
		Multipart multipart = new MimeMultipart();
		// Le text du message
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		// messageBodyPart.setText(body, encoding);
		setContent(messageBodyPart, body);
		multipart.addBodyPart(messageBodyPart);
		// Les fichiers attaches, s'il en a
		if (attachNames != null) {
			if ((attachData == null) || (attachData.length != attachNames.length))
				throw new MessagingException("Attachements' set do not corresponds to data set");
			for (int i = 0; i < attachNames.length; i++) {
				messageBodyPart = new MimeBodyPart();
				// Le fichier attache
				DataSource source = new ByteArrayDataSource(attachData[i], attachNames[i]);
				messageBodyPart.setDataHandler(new DataHandler(source));
				// Le nom du fichier qu'on vera dans le message
				messageBodyPart.setFileName(attachNames[i]);
				multipart.addBodyPart(messageBodyPart);
			}
		}
		// Ajouter le contenu dans le message
		message.setContent(multipart);
	}

	/**
	 * Cree un message mail simple (sans fichiers attaches). Le message construite un utilisant cette methode sera envoie au format "text/plain". Il
	 * est effectivement envoye lors de l'appel aux methodes <code>send()</code> ou <code>safeSend()</code>.
	 * <p/>
	 * Plus d'information peut etre indiquee a l'aide des appels consecutifs des methodes <code>addCC()</code>, <code>setReplyTo()</code>, etc...
	 * 
	 * @param from L'adresse d'expediteur.
	 * @param to L'adresse destinataire. Plusieurs adresses peuvent etre donnees en les separant par virgule (",").
	 * @param subject Le sujet du message.
	 * @param body Le texte du message.
	 * @exception MessagingException
	 * @exception AddressException
	 */
	public void initMessage(String from, String to, String subject, String body) throws MessagingException, AddressException {
		createNewMimeMessage(from, to, subject);
		// message.setText(body, encoding);
		setContent(message, body);
	}

	/**
	 * Cette methode cree effectivement un objet correspondant a un message mail. Elle est implicitement appele par les methodes
	 * <code>initMessage(...)</code>.
	 * 
	 * @param from L'adresse d'expediteur.
	 * @param to L'adresse de destinataire. Plusieurs adresses peuvent etre donnees. Elles seront ajoutees dans la liste " <code>addrsTO</code>".
	 * @param subject Le sujet du message. Le codage du message est defini en " <code>encoding</code>".
	 * @exception MessagingException
	 * @exception AddressException
	 */
	private void createNewMimeMessage(String from, String to, String subject) throws MessagingException, AddressException {
		message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		// Le(s) champ TO
		String[] atos = toArray(to);
		if (atos.length == 0)
			throw new AddressException("Addresse TO not specified");
		for (int i = 0; i < atos.length; i++)
			addTO(atos[i]);
		message.setSubject(subject, encoding);
	}

	/**
	 * Definit le contenu du message. Si le type de contenu n'est pas precise, alors le message est cree avec le type "text/plain". Sinon, la valeur
	 * de contentType est utilisee.
	 * 
	 * @see #setContentType
	 */
	private void setContent(MimePart msgPart, String content) throws MessagingException {
		if (contentType == null)
			msgPart.setText(content, encoding);
		else
			msgPart.setContent(content, contentType);
	}

	/**
	 * Definit le type de contenu de nouveau message. Le type par defaut est "text/plain".
	 * <p>
	 * <i>Si utilisee, cette methode doit etre appellee avant l'appel de la methode <code>initMessage()</code> !!!</i>
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Indique le systeme de codage pour le text et le sujet du message envoye. Le codage par defaut est "iso-8859-1".
	 * 
	 * @param encoding Une chaine le codage.
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	/**
	 * Ajoute plusieurs adresses a la liste des adresses CC (carbon copy) du message en cours. Cette methode peut etre appelee plusiers fois.
	 * 
	 * @param cc Un tableau des adresse mail.
	 * @exception AddressException
	 */
	public void addCCs(String[] cc) throws AddressException {
		if (cc != null) {
			for (int i = 0; i < cc.length; i++)
				addCC(cc[i]);
		}
	}

	/**
	 * Ajoute une adresse a la liste des adresse CC (carbon copy) du message en cours. Cette methode peut etre appelee plusieurs fois.
	 * 
	 * @param aCC Une adresse mails.
	 * @exception AddressException
	 */
	public void addCC(String aCC) throws AddressException {
		addrsCC.addElement(new InternetAddress(aCC));
	}

	/**
	 * Ajoute plusieurs adresses a la liste des adresses BCC (blind carbon copy) du message en cours. Cette methode peut etre appelee plusiers fois.
	 * 
	 * @param bcc Un tableau des adresses mail.
	 * @exception AddressException
	 */
	public void addBCCs(String[] bcc) throws AddressException {
		if (bcc != null) {
			for (int i = 0; i < bcc.length; i++)
				addBCC(bcc[i]);
		}
	}

	/**
	 * Ajoute une nouvelle adresse a la liste des adresse BCC (blind carbon copy) du message en cours. Cette methode peut etre appelee plusieurs fois.
	 * 
	 * @param aBCC Une adresse mail.
	 * @exception AddressException
	 */
	public void addBCC(String aBCC) throws AddressException {
		addrsBCC.addElement(new InternetAddress(aBCC));
	}

	/**
	 * Ajoute une nouvelle adresse a la liste des adresse TO du message en cours. Cette methode peut etre appelee plusieurs fois.
	 * 
	 * @param aTO Une adresse mail.
	 * @exception AddressException
	 */
	public void addTO(String aTO) throws AddressException {
		addrsTO.addElement(new InternetAddress(aTO));
	}

	/**
	 * Supprime l'adresse donnee des listes TO, CC et BCC. Toutes ces occurences sont enlevees de toutes les listes.
	 * 
	 * @param address L'adresse mail a supprimer.
	 */
	public void removeAddress(String address) {
		try {
			InternetAddress addr = new InternetAddress(address);
			addrsBCC.removeElement(addr);
			addrsCC.removeElement(addr);
			// if (addrTO.size() > 1)
			addrsTO.removeElement(addr);
		} catch (AddressException ex) {
		}
	}

	/**
	 * Definit une adresse de type Reply-To. L'implementation actuelle de la classe n'autorise qu'une seule adresse reply-to.
	 * 
	 * @param address L'adresse mail.
	 * @exception MessagingException
	 */
	public void setReplyTo(String address) throws MessagingException {
		InternetAddress[] addrList = null;
		if (address != null) {
			addrList = new InternetAddress[1];
			addrList[0] = new InternetAddress(address);
			message.setReplyTo(null);
		}
		message.setReplyTo(addrList);
	}

	/**
	 * Permet de definir un champ dans l'entete du message. La definition est constitue du couple <code>nom-valeur</code>.
	 * 
	 * @param name Le nom du champ de l'entete.
	 * @param value La valeur du champ de l'entete.
	 * @exception MessagingException
	 */
	public void setHeader(String name, String value) throws MessagingException {
		message.setHeader(name, value);
	}

	/**
	 * Redefinie les adresse du type donnee (TO, CC, BCC) en prenant celle de la liste "<code>addresses</code>". Toutes les adresse definies
	 * precedamment sont supprimees.
	 * 
	 * @param addresses Une liste des adresses mail.
	 * @param type Le type des adresses. Cette valeur doit correspondre au type defini par <code>Message.RecipientType</code>.
	 * @exception MessagingException
	 */
	private void resetAddresses(Vector addresses, Message.RecipientType type) throws MessagingException {
		message.setRecipients(type, (Address[]) null);
		for (int i = 0; i < addresses.size(); i++)
			message.addRecipient(type, (InternetAddress) addresses.elementAt(i));
	}

	/**
	 * Envoie le message prealablement construit et configure. Une exception est levee si le message ne peut pas etre envoye.
	 * <p/>
	 * Cette methode ne permet pas d'envoyer un message si il contient une adresse incorrecte. Utilisez la methode <code>safeSend()</code> dans ce
	 * cas.
	 * 
	 * @exception MessagingException
	 * @exception SendFailedException
	 * @see #safeSend() safeSend()
	 */
	public void send() throws MessagingException, SendFailedException {
		// Les destinataires
		resetAddresses(addrsTO, Message.RecipientType.TO);
		resetAddresses(addrsCC, Message.RecipientType.CC);
		resetAddresses(addrsBCC, Message.RecipientType.BCC);
		// La date d'envoi
		message.setSentDate(new Date());
		//
		Transport.send(message);
	}

	/**
	 * Envoie le message prealablement contruit et configure. Si le message contient des adresses incorrectes, elles sont enlevees de la liste des
	 * destinataires avant son expedition.
	 * <p>
	 * La methode retourne la liste des adresses incorrectes. Cette liste est vide (<code>size()==0</code>) si le message a pu etre envoye avec succes
	 * a tous les destinataires.
	 * <p>
	 * Une exception est levee si le message ne peut pas etre envoye pour d'autres raisons.
	 * 
	 * @return La liste des adresse auxquelles le message n'a pas pu etre expedie (vecteur des objets String).
	 * @exception MessagingException
	 * @exception SendFailedException
	 * @see #send() send()
	 */
	public Vector safeSend() throws MessagingException, SendFailedException {
		Vector failedAddrs = new Vector();
		try {
			this.send();
		} catch (SendFailedException ex) {
			Address[] addrs = ex.getInvalidAddresses();
			if (addrs != null) {
				for (int i = 0; i < addrs.length; i++) {
					failedAddrs.addElement(addrs[i].toString());
					removeAddress(addrs[i].toString());
				}
				this.send();
			}
			else {
				throw ex;
			}
		}
		return failedAddrs;
	}

	/**
	 * Decompose la chaine de caracteres donnees en vecteur des chaines. La chaine est decomposee suivant le separateur donne. Ignore toutes les
	 * chaines vides.
	 * 
	 * @param strings Les chaines de caracteres separes par un separateur.
	 * @param separator Le separateur.
	 */
	private static Vector toVector(String strings, String separator) {
		return toVector(strings, separator, true);
	}

	/**
	 * Decompose la chaine de caracteres donnees en un vecteur des chaines. La chaine est decomposee suivant le separateur donne.
	 * 
	 * @param strings Les chaines de caracteres separes par un separateur.
	 * @param separator Le separateur.
	 * @param ignoreEmptyStrings Indique s'il faut ignorer les chaines vides.
	 */
	private static Vector toVector(String strings, String separator, boolean ignoreEmptyStrings) {
		Vector v = new Vector();
		if (strings != null) {
			StringTokenizer st = new StringTokenizer(strings, separator);
			String s;
			while (st.hasMoreTokens()) {
				s = st.nextToken().trim();
				if (ignoreEmptyStrings) {
					if ((s == null) || s.equals(""))
						continue;
				}
				v.addElement(s);
			}
		}
		return v;
	}

	/**
	 * Convertie un vecteur des chaines de carateres en un tableau des chaines.
	 * 
	 * @param strings Un vecteur des chaines.
	 */
	private static String[] toArray(Vector strings) {
		if (strings == null) {
			return null;
		}
		else {
			String[] sv = new String[strings.size()];
			strings.copyInto(sv);
			return sv;
		}
	}

	/**
	 * Convertit une chaine de caracteres en un tableau. Les elements de la chaine sont separes par une virgule (le separateur par defaut).
	 * 
	 * @param aString Une chaine de caracteres a decomposer.
	 */
	public static String[] toArray(String aString) {
		if (aString == null)
			return null;
		return toArray(toVector(aString, ","));
	}

	/**
	 * Permet d'extraire le nom du fichier a partir d'un chemin donnees. Si le chemin indique un repertoire, le nom du repertoire est extrait.
	 * 
	 * @param filePath Le chmenin d'un fichier.
	 */
	public static String getNameFromPath(String filePath) {
		int n, m;
		while (filePath.endsWith("/") || filePath.endsWith("\\"))
			filePath = filePath.substring(0, filePath.length() - 1);
		n = filePath.lastIndexOf("/");
		m = filePath.lastIndexOf("\\");
		if ((n != -1) || (m != -1)) {
			if (m > n)
				n = m;
		}
		return filePath.substring(n + 1);
	}

	/**
	 * Cette classe permet d'envoyer un tableau d'octets en tant qu'un fichier attache. Elle implemente l'interface
	 * <code>javax.activation.DataSource</code> et peut representer le contenu d'un message "multipart".
	 */
	public class ByteArrayDataSource implements DataSource {
		/**
		 * Les donnees.
		 */
		private byte[] data;

		/**
		 * Le nom que l'on associe aux donnees. Si les donnees correspondent au contenu d'un fichier, ce champ peut indiquer le nom du fichier.
		 */
		private String name;

		/**
		 * Le type MIME des donnees.
		 */
		private String contentType;

		/**
		 * Creer un nouveau objet avec les donnees, le nom et le type de donnees specifies.
		 * 
		 * @param data Les donnees (le tableau d'octets).
		 * @param name Le nom associe aux donnees.
		 * @param contentType Le type MIME des donnees.
		 */
		public ByteArrayDataSource(byte[] data, String name, String contentType) {
			this.data = data;
			this.name = name;
			this.contentType = contentType;
		}

		/**
		 * Creer un nouveau objet avec les donnees et le nom specifies. Ces donnees vont avoir le type MIME par defaut "application/octet-stream".
		 * 
		 * @param data Le contenu des donnees (le tableau d'octets).
		 * @param name Le nom associe aux donnees.
		 */
		public ByteArrayDataSource(byte[] data, String name) {
			this(data, name, "application/octet-stream");
		}

		/**
		 * Retourne le type MIME des donnees.
		 */
		public String getContentType() {
			return contentType;
		}

		/**
		 * Retourne le InputStream vers les donnees. Une exception <code>java.io.IOException</code> est levee si la source de donnees est vide.
		 * 
		 * @exception IOException
		 */
		public InputStream getInputStream() throws IOException {
			if (data == null)
				throw new IOException("Data source is null");
			return new ByteArrayInputStream(data);
		}

		/**
		 * Returne le nom associe aux donnees.
		 */
		public String getName() {
			return name;
		}

		/**
		 * Renvoie un OutputStream vers la source de donnees. Une exception <code>java.io.IOEXception</code> est levee si le stream ne peux pas etre
		 * cree.
		 * 
		 * @exception IOException
		 */
		public OutputStream getOutputStream() throws IOException {
			if (data == null)
				throw new IOException("Data source is null");
			return new ByteArrayOutputStream(data.length);
		}
	}

	public javax.mail.Session getSession() {
		return session;
	}

	public String getEncoding() {
		return encoding;
	}

	public MimeMessage getMessage() {
		return message;
	}

	public Vector getAddrsTO() {
		return addrsTO;
	}

	public Vector getAddrsCC() {
		return addrsCC;
	}

	public Vector getAddrsBCC() {
		return addrsBCC;
	}

	public String getContentType() {
		return contentType;
	}

	public static String mergeAdress(List list, String separator) {

		StringBuffer stringbuffer = new StringBuffer();
		Iterator iterator1 = list.iterator();
		while (iterator1.hasNext()) {
			InternetAddress object = (InternetAddress) iterator1.next();
			stringbuffer.append(object.getAddress() + separator);
		}
		return stringbuffer.toString();

	}

}
