/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneForm;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * Controleur pour le composant {@link PersonneForm}. A surcharger si besoin.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */
public class PersonneFormCtrl extends AComponentCtrl {

	private NSArray listCivilites;
	public EORepartPersonneAdresse selectedRepartPersonneAdresse;
	private IPersonne personne;

	public PersonneFormCtrl(AComponent component) {
		super(component);
	}

	public PersonneFormCtrl() {
		super();
	}

	public IPersonne getPersonne() {
		return personne;
	}

	public void setPersonne(IPersonne personne) {
		this.personne = personne;
	}

	public PersonneForm getMyComponent() {
		return (PersonneForm) super.getMyComponent();
	}

	public NSArray getListCivilites() {
		if (listCivilites == null) {
			listCivilites = EOCivilite.fetchAll(edc());
		}
		return listCivilites;
	}

	public NSArray getFilteredNaf(String filtNaf) {
		NSArray res = NSArray.EmptyArray;
		NSMutableArray quals = new NSMutableArray();

		if (filtNaf != null) {
			filtNaf = filtNaf.trim();
		}

		if (filtNaf != null && filtNaf.length() > 0) {
			filtNaf = MyStringCtrl.chaineSansAccents(filtNaf, "?");
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EONaf.C_NAF_KEY + " caseInsensitiveLike '" + filtNaf + "*' or " + EONaf.LL_NAF_KEY + " caseInsensitiveLike '*" + filtNaf + "*' ", null));
			res = EONaf.fetchAll(edc(), new EOOrQualifier(quals), new NSArray(new Object[] {
					EONaf.SORT_C_NAF
			}));
		}
		return res;
	}

	public void onAnnuler() {
		edc().revert();
	}

	/**
	 * Prepare les donnees avant qu'elles soient enregistrees.
	 * 
	 * @throws Exception
	 */
	public void prepareDataForSave() throws Exception {
		try {
			//			Integer persIdForUser = getMyComponent().getAppUserForAnnuaire().getUtilisateur().toPersonne().persId();
			if (getMyComponent().isAdresseEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les adresses avant de poursuivre.");
			}
			if (getMyComponent().isElementsEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les membres du groupe avant de poursuivre.");
			}
			if (getMyComponent().isGroupeEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les groupes avant de poursuivre.");
			}
			if (getMyComponent().getIsStructureEffectifEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les effectifs avant de poursuivre.");
			}
			if (getMyComponent().getIsStructureCaEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur le chifre d'affaires avant de poursuivre.");
			}
			if (getMyComponent().getIsNomEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur le nom avant de poursuivre.");
			}
			if (getMyComponent().getIsTelephoneEditing().booleanValue()) {
				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les téléphones avant de poursuivre.");
			}
			//			if (getMyComponent().getIsGroupeAdminEditing().booleanValue()) {
			//				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur l'administration du groupe avant de poursuivre.");
			//			}
			//On force la validation des objets
			getPersonne().willChange();
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Appelle {@link PersonneFormCtrl#prepareDataForSave} et enregistre les modifications. Surchargez plutot prepareDataForSave.
	 * 
	 * @return true si l'enregistrement dans l'editing context se passe bien.
	 * @throws Exception Si une erreur lors de la preparation des donnees ou lors de l'enregistrement.
	 */
	public boolean onEnregistrer() throws Exception {
		try {
			prepareDataForSave();
			edc().saveChanges();
			return true;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Nettoie les donnees pour que la page soit bien reinitialisee entre deux affichages.
	 */
	public void nettoieDonnees() {
		setSelectedRepartPersonneAdresse(null);
	}

	/**
	 * Initialise les données avant affichage du formulaire.
	 * 
	 * @param personne la structure ou l'individu
	 */
	public void initialiseData(IPersonne personne) {
		nettoieDonnees();
		this.personne = personne;
		if (personne instanceof EOStructure) {
			EOStructure structure = (EOStructure) personne;
			structure.setValidationEditingContext(edc());
		}
		else if (personne instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu) personne;
			individu.setValidationEditingContext(edc());
			if (individu.toCivilite() == null) {
				individu.setToCiviliteRelationship((EOCivilite) getListCivilites().objectAtIndex(0));
			}
		}

		if (personne.persIdCreation() == null) {
			personne.setPersIdCreation(getMyComponent().getUtilisateurPersId());
		}
		if (personne.persIdModification() == null) {
			personne.setPersIdModification(getMyComponent().getUtilisateurPersId());
		}
	}

	/**
	 * @return TRUE si on est sur la creation d'un individu, FALSE sinon.
	 */
	public Boolean isIndividu() {
		return Boolean.valueOf(getMyComponent().getLaPersonne() instanceof EOIndividu);
	}

	public EORepartPersonneAdresse getSelectedRepartPersonneAdresse() {
		return selectedRepartPersonneAdresse;
	}

	public EOTypeAdresse getTypeAdresseFact(EOEditingContext ec) {
		return EOTypeAdresse.fetchByKeyValue(ec, EOTypeAdresse.TADR_CODE_KEY, EOTypeAdresse.TADR_CODE_FACT);
	}

	public void setSelectedRepartPersonneAdresse(EORepartPersonneAdresse repartPersonneAdresse) {
		selectedRepartPersonneAdresse = repartPersonneAdresse;
	}

	public EOStructure getLaStructure() {
		return getMyComponent().getLaStructure();
	}

	public EOIndividu getLeIndividu() {
		return getMyComponent().getLeIndividu();
	}

}
