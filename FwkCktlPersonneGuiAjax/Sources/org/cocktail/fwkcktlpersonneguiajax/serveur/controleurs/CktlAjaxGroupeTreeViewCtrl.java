/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import java.util.Enumeration;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.CktlAjaxGroupeTreeView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxTreeModel;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */

public class CktlAjaxGroupeTreeViewCtrl {

	private CktlAjaxGroupeTreeView wocomponent;

	private Object _delegate;
	private GroupeNode rootGroupe;
	private GroupeNode unGroupe;
	private AjaxTreeModel myTreeModel = new AjaxTreeModel();
	//private static NSArray groupes = null;
	
	private boolean flag = true;

	public CktlAjaxGroupeTreeViewCtrl(CktlAjaxGroupeTreeView component) {
		wocomponent = component;
	}

	private EOEditingContext edc() {
		return wocomponent.edc();
	}

	public WOActionResults afficherGroupeSelectionne() {
		GroupeNode node = unGroupe();
		if (node != null && node != rootGroupe()) {
			EOStructure groupe = (EOStructure) node.object();
			((CktlAjaxGroupeTreeView) wocomponent).setSelection(groupe);
		}
		return null;
	}

	public GroupeNode rootGroupe() {
		if (rootGroupe == null ||
				(wocomponent.treeRootObject() != null && rootGroupe.object() instanceof EmptyRootObject) ||
				(wocomponent.treeRootObject() == null && !(rootGroupe.object() instanceof EmptyRootObject))) {
			if (wocomponent.treeRootObject() != null) {
				rootGroupe = new GroupeNode(null, wocomponent.treeRootObject());
			}
			else {
				rootGroupe = new GroupeNode(null, new EmptyRootObject());
			}
		}
		return rootGroupe;
	}

	public void setRootGroupe(GroupeNode rootGroupe) {
		this.rootGroupe = rootGroupe;
	}

	public GroupeNode unGroupe() {
		return unGroupe;
	}

	public void setUnGroupe(GroupeNode unGroupe) {
		this.unGroupe = unGroupe;
	}

	public String unGroupeLibelle() {
		NSKeyValueCoding obj = unGroupe().object();
		return (String) obj.valueForKey(EOStructure.LIBELLE_FOR_GROUPE_KEY);
	}

	public AjaxTreeModel getMyTreeModel() {
		return myTreeModel;
	}
	
	public void setMyTreeModel(AjaxTreeModel myTreeModel) {
		this.myTreeModel = myTreeModel;
	}
	
	public CktlAjaxGroupeTreeView getWocomponent() {
		return wocomponent;
	}
	
	public void setWocomponent(CktlAjaxGroupeTreeView wocomponent) {
		this.wocomponent = wocomponent;
	}
	
	public GroupeNode selectObjectInTree(GroupeNode node, EOStructure groupe) {
		GroupeNode newSelectedNode = null;
		if (node.object().equals(groupe)) {
			newSelectedNode = node;
		}
		else {
			Enumeration en = node.children().objectEnumerator();
			while (newSelectedNode == null && en.hasMoreElements()) {
				GroupeNode tmpNode = (GroupeNode) en.nextElement();
				newSelectedNode = selectObjectInTree(tmpNode, groupe);
			}
		}
		if (newSelectedNode != null) {
			myTreeModel.setExpanded(node, true);
		}
		return newSelectedNode;
	}
	
	/**
	 * Ouvre tous les noeuds jusqu'au niveau donné compris.<br/>
	 * Exemple : <br/>
	 *     <pre>
	 *         expandAllNodesToLevel(1); 
	 *     </pre>
	 *     Va Ouvrir la racine (level = 0), puis les fils de la racine (level = 1)
	 * @param maxLevel le niveau jusqu'où on ouvre les noeuds
	 */
	public void expandAllNodesToLevel(int maxLevel) {
	    _expandAllNodesToLevel(rootGroupe(), 0, maxLevel);
	}
	
	private void _expandAllNodesToLevel(GroupeNode node, int currentLevel, int maxLevel) {
	    getMyTreeModel().setExpanded(node, true);
	    if (currentLevel < maxLevel) {
	        for (Object child : node.children()) {
	            currentLevel = currentLevel + 1;
	            _expandAllNodesToLevel((GroupeNode)child, currentLevel, maxLevel);
	        }
	    }
	}

	/**
	 * 
	 * Cette méthode filtre les nodes affichées dans l'arbre selon le qualifier.
	 * L'algo est le suivant :
	 * - Premier parcours de l'arbre "depthFirst"
	 *     - Si une node satisfait le filtre, on l'enregistre dans un tableau ainsi que tous ses parents
	 *       (pour que son affichage soit garantie)
	 *     - Sinon on l'ajoute dans un tableau
	 * - On parcourt le tableau des nodes à supprimer et on réorganise l'arbre en fonction (suppression 
	 *     de la node du tableau des enfants de son parent (ouf).
	 *     
	 *  Cette méthode n'est pas utilisée pour l'instant...  
	 *     
	 */
	private void filterTree() {
	    if (wocomponent.getQualifier() != null) {
	        Enumeration<GroupeNode> enumeration = myTreeModel.depthFirstEnumeration(rootGroupe, false);
	        NSMutableArray<GroupeNode> nodesToPrune = new NSMutableArray<CktlAjaxGroupeTreeViewCtrl.GroupeNode>();
	        NSMutableArray<GroupeNode> nodesToKeep = new NSMutableArray<CktlAjaxGroupeTreeViewCtrl.GroupeNode>();
	        // On parcours les nodes affichées et on marque au fur et à mesure celles à garder selon le filtre
	        // Et celles à enlever
	        while (enumeration.hasMoreElements()) {
	            GroupeNode node = enumeration.nextElement();
	            if (!(node.object instanceof EmptyRootObject) &&
	                    !wocomponent.getQualifier().evaluateWithObject(node.object)) {
	                nodesToPrune.addObject(node);
	            } else {
	                // On rajoute la node dans les nodes à garder et on enlève ses parents
	                // des nodes à enlever
	                removeFromNodeToPruneAndAddToNodeToKeep(node, nodesToPrune, nodesToKeep);
	            }
	        }
	        // On parcours les nodes à enlever, et on réorganise l'arbre
	        for (GroupeNode node : nodesToPrune) {
	            if (!nodesToKeep.contains(node) && node.parent() != null && !(node.object instanceof EmptyRootObject)) {
	                NSMutableArray<GroupeNode> childrenMut = node.parent().children().mutableClone();
	                childrenMut.removeObject(node);
	                node.parent().children = childrenMut.immutableClone();
	            }
	        }
	    } else {
	        setRootGroupe(null);
	    }
	}
	   
    private void removeFromNodeToPruneAndAddToNodeToKeep(GroupeNode node, 
            NSMutableArray<GroupeNode> nodesToPrune, 
            NSMutableArray<GroupeNode> nodesToKeep) {
        nodesToPrune.removeObjectsInArray(node.parents());
        nodesToPrune.removeObject(node);
        nodesToKeep.addObject(node);
        nodesToKeep.addObjectsFromArray(node.parents());
    }
	
	public Object delegate() {
		if (_delegate == null) {
			_delegate = new CktlAjaxGroupeTreeViewCtrl.Delegate();
		}
		return _delegate;

	}

	public class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			if (node != null) {
				return ((GroupeNode) node).children();
			}
			return NSArray.EmptyArray;
		}

		public boolean isLeaf(Object node) {
			if (node != null) {
				return ((GroupeNode) node).isLeaf();
			}
			return true;
		}

		public Object parentTreeNode(Object node) {
			if (node != null) {
				return ((GroupeNode) node).parent();
			}
			return null;
		}
	}

	public class GroupeNode {
		NSArray children;
		GroupeNode parent;
		NSKeyValueCoding object;

		public GroupeNode(GroupeNode parent, NSKeyValueCoding obj) {
			object = obj;
			this.parent = parent;
		}

		public NSArray children() {
			if (children == null) {
				NSArray res = NSArray.EmptyArray;
				if (object() instanceof EmptyRootObject) {
					res = EOStructureForGroupeSpec.getGroupesRacine(edc());
				} else {
					NSMutableArray array = new NSMutableArray();
					array.addObject(new EOKeyValueQualifier(EOStructure.TO_STRUCTURE_PERE_KEY, EOKeyValueQualifier.QualifierOperatorEqual, object()));
					array.addObject(new EOKeyValueQualifier(EOStructure.C_STRUCTURE_KEY, EOKeyValueQualifier.QualifierOperatorNotEqual, object().valueForKey(EOStructure.C_STRUCTURE_KEY)));
					if (wocomponent.getQualifier() != null) {
						array.addObject(wocomponent.getQualifier());
					}
					res = EOStructureForGroupeSpec.rechercherGroupes(edc(), new EOAndQualifier(array), 0, false);
				}

				NSMutableArray res2 = new NSMutableArray();
				for (int i = 0; i < res.count(); i++) {
					//verifier si objet dans liste exclusions
					if (wocomponent.getTreeExclusions() == null || wocomponent.getTreeExclusions().indexOf(res.objectAtIndex(i)) == NSArray.NotFound) {
						//Si groupe prive, verifier que l'utilisateur a le droit de les voir
						EOStructure struct = (EOStructure) res.objectAtIndex(i);
						boolean isPrive = EOStructureForGroupeSpec.isGroupePrive(struct);
						if (!isPrive || (isPrive && (wocomponent.getAppUserForAnnuaire().hasDroitAllGroupesPrivesVisualisation() || wocomponent.getAppUserForAnnuaire().hasDroitGererGroupe(struct)))) {
							GroupeNode node = new GroupeNode(this, struct);
							res2.addObject(node);
						}
					}
				}

				children = res2.immutableClone();
			}
			return children;
		}

		public void setChildren(NSArray children) {
            this.children = children;
        }
		
		public boolean isLeaf() {
			return (children().count() == 0);
		}

		public GroupeNode parent() {
			return parent;
		}

		public void setParent(GroupeNode node) {
			parent = node;
		}

		public NSKeyValueCoding object() {
			return object;
		}

		public NSArray<GroupeNode> parents() {
		    NSMutableArray<GroupeNode> _parents = new NSMutableArray<CktlAjaxGroupeTreeViewCtrl.GroupeNode>();
		    _parentsRecurs(this, _parents);
		    return _parents.immutableClone();
		}
		
		private void _parentsRecurs(GroupeNode node, NSMutableArray<GroupeNode> parents) {
		    if (node.parent() != null) {
		        parents.addObject(node.parent());
		        _parentsRecurs(node.parent, parents);
		    }
		}
		
		@Override
		public String toString() {
		    return object != null ? object.toString() : super.toString();
		}
		
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((object == null) ? 0 : object.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            GroupeNode other = (GroupeNode) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (object == null) {
                if (other.object != null)
                    return false;
            } else if (!object.equals(other.object))
                return false;
            return true;
        }

        private CktlAjaxGroupeTreeViewCtrl getOuterType() {
            return CktlAjaxGroupeTreeViewCtrl.this;
        }
		
	}

	public class EmptyRootObject implements NSKeyValueCoding {

		public EmptyRootObject() {
		}

		public void takeValueForKey(Object arg0, String arg1) {

		}

		public Object valueForKey(String arg0) {
			return "root";
		}

	}
	
	public String cssItemTree() {
		String styleCSS = new String();
		
		if ( (getWocomponent().getGroupeIndex()%2) == 0 ) {
//			styleCSS = "ValiditeFondBlanc";
			styleCSS = "even";
		}
		else {
//			styleCSS = "InvaliditeFondGris";
			styleCSS = "odd";
		}
		return styleCSS;
	}

}
