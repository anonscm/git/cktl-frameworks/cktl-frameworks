/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class NotificationCtrl {
	public final static String ON_ANNULER_NOTIFICATION = "onAnnulerNotification";

	/**
	 * Declarer un objet comme observer de la notification onAnnulerNotification. Celui-ci doit implementer une methode methodName, avec en parametre
	 * un objet NSNotification (si methodName null, la classe doit implementer une methode onAnnulerNotification.
	 * 
	 * @param object
	 */
	public static void addObserverForOnAnnulerNotification(Object object, String methodName) {
		if (methodName == null) {
			methodName = ON_ANNULER_NOTIFICATION;
		}
		NSNotificationCenter.defaultCenter().addObserver(object, new NSSelector(methodName, new Class[] {
			NSNotification.class
		}), ON_ANNULER_NOTIFICATION, null);
	}

	public static void postNotificationForOnAnnulerNotification(Object notificationObject, NSDictionary userInfo) {
		NSNotificationCenter.defaultCenter().postNotification(ON_ANNULER_NOTIFICATION, notificationObject, userInfo);
	}

}
