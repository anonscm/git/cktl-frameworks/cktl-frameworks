/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOEffectifStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.StructureEffectifUI;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class StructureEffectifUICtrl extends AComponentCtrl {
	private ERXDisplayGroup effectifDisplayGroup;

	private EOStructure structureCache;

	//private Boolean isEditing = Boolean.FALSE;
	//private final EffectifDGDelegate dgDelegate = new EffectifDGDelegate();

	public StructureEffectifUICtrl(AComponent component) {
		super(component);
	}

	@Override
	public StructureEffectifUI getMyComponent() {
		return (StructureEffectifUI) super.getMyComponent();
	}

	//	/**
	//	 * Traitement pour la suppression.
	//	 * 
	//	 * @param secretariat
	//	 * @throws Exception
	//	 */
	//	public void onEffectifSupprimer(EOEffectifStructure item) throws Exception {
	//		structure().removeFromToEffectifStructuresRelationship(item);
	//		edc().deleteObject(item);
	//	}

	private EOStructure structure() {
		return getMyComponent().structure();
	}

	public ERXDisplayGroup effectifDisplayGroup() {
		if (!structure().equals(structureCache) || getMyComponent().isReadOnly().booleanValue()) {
			getMyComponent().setErreurSaisieMessage(null);
			structureCache = structure();
			initialiseEffectifDisplayGroup(structureCache);
		}
		return effectifDisplayGroup;

	}

	public void initialiseEffectifDisplayGroup(EOStructure groupe) {
		if (effectifDisplayGroup == null) {
			effectifDisplayGroup = new ERXDisplayGroup();
		}
		edc().revert();
		effectifDisplayGroup.setObjectArray(new NSArray());

		if (groupe != null) {
			NSArray res = groupe.toEffectifStructures(null, new NSArray(EOEffectifStructure.SORT_CODE_ANNEE_DESC), false);
			effectifDisplayGroup.setObjectArray(res);
		}
		//	effectifDisplayGroup.fetch();

	}

	public void afterEnregistrer() {
		//initialiseRepartTypeGroupeDisplayGroup(groupe());
	}

	public void afterAnnuler() {
		//initialiseRepartTypeGroupeDisplayGroup(groupe());
	}

	public Boolean getIsEditing() {
		return getMyComponent().isEditing();
	}

	//	public void setIsRepartEditing(Boolean isRepartEditing) {
	//		this.isEditing = isRepartEditing;
	//		//getMyComponent().setValueForBinding(isRepartEditing, GroupeAdminForm.BINDING_isEditing);
	//	}

	//	public void onEdit() {
	//		setIsRepartEditing(Boolean.TRUE);
	//		initialiseEffectifDisplayGroup(structure());
	//
	//	}

	public void onCreer() throws Exception {
		//Recuperer le max des annees
		Integer max = Integer.valueOf(MyDateCtrl.getYear(new NSTimestamp()));
		if (effectifDisplayGroup().allObjects().count() > 0) {
			max = Integer.valueOf((String) ((EOEffectifStructure) effectifDisplayGroup().allObjects().objectAtIndex(0)).codeAnnee());
		}
		max = Integer.valueOf(max.intValue());
		EOEffectifStructure newObject = EOEffectifStructure.creerInstance(edc());
		effectifDisplayGroup().insertObjectAtIndex(newObject, 0);
		newObject.setCodeAnnee(max.toString());
		structure().addToToEffectifStructuresRelationship(newObject);
		//effectifDisplayGroup().setDelegate(null);

	}

	public void onSupprimer() throws Exception {
		if (effectifDisplayGroup() != null) {
			EOEffectifStructure object = (EOEffectifStructure) effectifDisplayGroup().selectedObject();
			structure().removeFromToEffectifStructuresRelationship(object);
			effectifDisplayGroup().deleteSelection();
			edc().deleteObject(object);

			//			
			//			if (object != null) {
			//				if (object.hasTemporaryGlobalID()) {
			//					edc().revert();
			//				}
			//				else {
			//					structure().removeFromToEffectifStructuresRelationship(object);
			//					effectifDisplayGroup().deleteSelection();
			//					edc().deleteObject(object);
			//				}
			//			}
		}
	}

	public void onEnregistrer() throws Exception {
		if (edc() != null) {
			if (structure() == null) {
				throw new NSValidation.ValidationException("Veuillez choisir une structure.");
			}
			//verifier que l'utilisateur a le droit de gerer la structure
			if (!getAppUserForAnnuaire().hasDroitModificationIPersonne(structure())) {
				throw new NSValidation.ValidationException("Vous n'avez pas le droit de modifier la personne");
			}
			// push de l'adresse dans l'ec parent
			edc().saveChanges();
		}
	}

	public void onAnnuler() {
		if (edc() != null) {
			edc().revert();
		}
	}

}
