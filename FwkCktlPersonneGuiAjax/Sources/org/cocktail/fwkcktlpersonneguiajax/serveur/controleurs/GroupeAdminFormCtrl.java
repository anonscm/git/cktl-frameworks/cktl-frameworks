/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyNSArrayCtrl;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.GroupeAdminForm;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class GroupeAdminFormCtrl extends AComponentCtrl {

	private static EOQualifier QUAL_INDIVIDUS_INTERNES = EOIndividu.QUAL_INDIVIDU_INTERNE;
	private ERXDisplayGroup<EOTypeGroupe> repartTypeGroupeDisplayGroup;
	private final RepartTypeGroupeDGDelegate repartTypeGroupeDGDelegate = new RepartTypeGroupeDGDelegate();

	private NSArray lastSelection;
	private Boolean isRepartEditing = Boolean.FALSE;

	public GroupeAdminFormCtrl(AComponent component) {
		super(component);
	}

	@Override
	public GroupeAdminForm getMyComponent() {
		return (GroupeAdminForm) super.getMyComponent();
	}

	/**
	 * @return le qualifier a utiliser pour proposer une liste de responsables possibles.
	 */
	public EOQualifier getIndividusForResponsableQualifier() {
		return QUAL_INDIVIDUS_INTERNES;
	}

	/**
	 * @return le qualifier a utiliser pour proposer une liste de secretaires possibles.
	 */
	public EOQualifier getIndividusForSecretariatQualifier() {
		return QUAL_INDIVIDUS_INTERNES;
	}

	public void onResponsableSelected(EOIndividu responsable) {

	}

	public void onSecretariatSelected(EOIndividu responsable) {

	}

	/**
	 * Traitement pour la suppression d'un secretaire.
	 * 
	 * @param secretariat
	 * @throws Exception
	 */
	public void onSecretariatSupprimer(EOSecretariat secretariat) throws Exception {
		secretariat.setToIndividuRelationship(null);
		groupe().removeFromToSecretariatsRelationship(secretariat);
		edc().deleteObject(secretariat);
	}

	private EOStructure groupe() {
		return getMyComponent().groupe();
	}

	/**
	 * Traitement pour l'ajout d'un secretaire.
	 * 
	 * @param secr
	 * @throws Exception
	 */
	public WOActionResults onSecretariatAjouter(EOIndividu secr) throws Exception {
		NSArray res = groupe().toSecretariats(new EOKeyValueQualifier(EOSecretariat.NO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, secr.noIndividu()));
		if (res.count() > 0) {
			throw new NSValidation.ValidationException(secr.getNomPrenomAffichage() + " est deja secretaire du groupe " + groupe().getNomPrenomAffichage());
		}

		EOSecretariat secretariat = EOSecretariat.creerInstance(edc());
		secretariat.setToIndividuRelationship(secr);
		groupe().addToToSecretariatsRelationship(secretariat);
		secretariat.setCStructure(groupe().cStructure());

		return null;

	}

	public ERXDisplayGroup<EOTypeGroupe> repartTypeGroupeDisplayGroup() {
		if (repartTypeGroupeDisplayGroup == null) {
			repartTypeGroupeDisplayGroup = new ERXDisplayGroup<EOTypeGroupe>();
			
//			NSArray<EOTypeGroupe> types = EOTypeGroupe.getTypesGroupeForUtilisateur(edc(), getAppUserForAnnuaire());
//            types = ERXQ.filtered(types, ERXQ.not(EOTypeGroupe.QUAL_TYPE_GROUPE_G));
//            if (!getAppUserForAnnuaire().hasDroitGrhumCreateur()) {
//				types = ERXQ.filtered(types,
//						ERXQ.not(EOTypeGroupe.QUAL_TEM_WEB_NON));
//			}
//			types = ERXS.sorted(types, EOTypeGroupe.SORT_TGRP_LIBELLE_ASC);
		}
		return repartTypeGroupeDisplayGroup;
	}

	public void initialiseRepartTypeGroupeDisplayGroup(EOStructure groupe) {
		repartTypeGroupeDisplayGroup().setDelegate(null);
		repartTypeGroupeDisplayGroup().setObjectArray(new NSArray());
		lastSelection = NSArray.EmptyArray;

		if (groupe != null) {
			//recuperer les types groupe en fonction de l'utilisateur
			NSArray allTypesGroupe = EOTypeGroupe.getTypesGroupeForUtilisateur(edc(), getAppUserForAnnuaire());
			
			allTypesGroupe = ERXQ.filtered(allTypesGroupe, ERXQ.not(EOTypeGroupe.QUAL_TYPE_GROUPE_G));
			allTypesGroupe = ERXQ.filtered(allTypesGroupe, ERXQ.not(EOTypeGroupe.QUAL_TEM_UTIL_NON));
            if (!getAppUserForAnnuaire().hasDroitGrhumCreateur()) {
            	allTypesGroupe = ERXQ.filtered(allTypesGroupe,
						ERXQ.not(EOTypeGroupe.QUAL_TEM_WEB_NON));
			}
            allTypesGroupe = ERXS.sorted(allTypesGroupe, EOTypeGroupe.SORT_TGRP_LIBELLE_ASC);

			NSArray typeGroupesAffectes = EOSortOrdering.sortedArrayUsingKeyOrderArray(EOStructureForGroupeSpec.getTypesGroupe(groupe), new NSArray(EOTypeGroupe.SORT_TGRP_LIBELLE_ASC));
			NSArray typeGroupesNonAffectes = MyNSArrayCtrl.complementOfNSArray(typeGroupesAffectes, allTypesGroupe);

			//on enleve le type G car il doit toujours etre affecte dans le cas d'un groupe
			typeGroupesAffectes = EOQualifier.filteredArrayWithQualifier(typeGroupesAffectes, new EONotQualifier(EOTypeGroupe.QUAL_TYPE_GROUPE_G));
			typeGroupesNonAffectes = EOQualifier.filteredArrayWithQualifier(typeGroupesNonAffectes, new EONotQualifier(EOTypeGroupe.QUAL_TYPE_GROUPE_G));

			if (getIsRepartEditing()) {
				repartTypeGroupeDisplayGroup().setObjectArray(MyNSArrayCtrl.unionOfNSArrays(new NSArray[] {
						typeGroupesAffectes, typeGroupesNonAffectes
				}));
				repartTypeGroupeDisplayGroup().setSelectedObjects(typeGroupesAffectes);
				lastSelection = new NSArray(typeGroupesAffectes);
				repartTypeGroupeDisplayGroup.setDelegate(repartTypeGroupeDGDelegate);
			}
			else {
				repartTypeGroupeDisplayGroup().setObjectArray(typeGroupesAffectes);
				//lastSelection = NSArray.EmptyArray;
			}

		}

	}


	public class RepartTypeGroupeDGDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			NSArray newSelection = group.selectedObjects();
			for (int i = 0; i < lastSelection.count(); i++) {
				if (newSelection.indexOfObject(lastSelection.objectAtIndex(i)) == NSArray.NotFound) {
					//supprimer lastSelection.objectAtIndex(i)
					String tgrpCode = ((EOTypeGroupe) lastSelection.objectAtIndex(i)).tgrpCode();
					EOStructureForGroupeSpec.removeTypeOfGroupe(edc(), getAppUserForAnnuaire().getPersId(), groupe(), tgrpCode);
				}
			}

			for (int i = 0; i < newSelection.count(); i++) {
				if (lastSelection.indexOfObject(newSelection.objectAtIndex(i)) == NSArray.NotFound) {
					//ajouter newSelection.objectAtIndex(i)
					String tgrpCode = ((EOTypeGroupe) newSelection.objectAtIndex(i)).tgrpCode();
					EOStructureForGroupeSpec.setTypeOfGroupe(edc(), getAppUserForAnnuaire().getPersId(), groupe(), (EOTypeGroupe) newSelection.objectAtIndex(i));
				}
			}
			lastSelection = new NSArray(newSelection);

		}
	}

	public void afterRepartEnregistrer() {
		initialiseRepartTypeGroupeDisplayGroup(groupe());
	}

	public void afterRepartAnnuler() {
		initialiseRepartTypeGroupeDisplayGroup(groupe());
	}

	public Boolean getIsRepartEditing() {
		return isRepartEditing;
	}

	public void setIsRepartEditing(Boolean isRepartEditing) {
		this.isRepartEditing = isRepartEditing;
		//getMyComponent().setValueForBinding(isRepartEditing, GroupeAdminForm.BINDING_isEditing);
	}

	public void onRepartEdit() {
		setIsRepartEditing(Boolean.TRUE);
		initialiseRepartTypeGroupeDisplayGroup(groupe());

	}
}
