/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AdresseFormPourONP;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */

public class AdresseFormPourOnpCtrl extends AComponentCtrl {
	private NSArray allPays = null;

	public AdresseFormPourOnpCtrl(AComponent component) {
		super(component);

	}

	public NSArray allPays() {
		
		EOQualifier qualifierTemporel;
    	EOQualifier qualDateFin;
		final EOQualifier QUAL_DATE_FIN_NULLE = new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
		
		if (allPays == null) {
			
			qualDateFin = ERXQ.greaterThanOrEqualTo(EOPays.D_FIN_VAL_KEY, new NSTimestamp());
			qualifierTemporel = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					QUAL_DATE_FIN_NULLE, qualDateFin
			}));
        	allPays = EOPays.fetchAll(edc(), qualifierTemporel, new NSArray(EOPays.SORT_LL_PAYS));
			
//			allPays = EOPays.fetchAll(edc(), new NSArray(new Object[] {
//				EOPays.SORT_LL_PAYS
//			}));
		}
		return allPays;
	}

	public NSArray<EOCommune> getFilteredCommunes(String filtCommune) {
		return EOCommune.getFilteredCommunes(edc(), filtCommune, getAdresse().toPays());
	}

	
	public NSArray<EOPays> getFilteredPaysActuels(String filtPays) {
		return EOPays.getFilteredPaysActuels(edc(), filtPays, allPays(), true, true);
	}

//	public EOPays getPaysForCode(String codePays) {
//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPays.C_PAYS_KEY + "=%@", new NSArray(new Object[] {
//			codePays
//		}));
//		NSArray res = EOQualifier.filteredArrayWithQualifier(allPays, qual);
//		return (EOPays) (res.count() > 0 ? res.objectAtIndex(0) : null);
//	}
	
	public NSArray<EOTypeVoie> getFilteredTypeVoie(String filtTypeVoie) {
		NSArray<EOTypeVoie> allTypeVoie = null;
		
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOTypeVoie.LIBELLE_VOIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		allTypeVoie = EOTypeVoie.fetchAll(edc(), sortOrderings);
		
		filtTypeVoie = filtTypeVoie.trim();
		filtTypeVoie = StringCtrl.chaineSansAccents(filtTypeVoie, "?");
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeVoie.LIBELLE_VOIE_KEY + " caseInsensitiveLike '*" + filtTypeVoie + "*'", null);
		NSArray<EOTypeVoie> res = EOQualifier.filteredArrayWithQualifier(allTypeVoie, qual);
		
		return res;
		
//		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
//		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOTypeVoie.LIBELLE_VOIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
//		return EOTypeVoie.fetchAll(edc(), sortOrderings);
	}

	public EOAdresse getAdresse() {
		return ((AdresseFormPourONP) getMyComponent()).getAdresse();
	}

}
