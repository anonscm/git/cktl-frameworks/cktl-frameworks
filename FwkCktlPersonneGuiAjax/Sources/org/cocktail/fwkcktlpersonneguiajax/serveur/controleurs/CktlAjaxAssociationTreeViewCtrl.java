/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs;

import java.util.Enumeration;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.CktlAjaxAssociationTreeView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxTreeModel;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CktlAjaxAssociationTreeViewCtrl {
    private CktlAjaxAssociationTreeView wocomponent;
    private AjaxTreeModel myTreeModel = new AjaxTreeModel();

    private Object _delegate;
    private AssociationNode rootAssociation;
    private AssociationNode uneAssociation;
    
    public CktlAjaxAssociationTreeViewCtrl(CktlAjaxAssociationTreeView component) {
        wocomponent = component;

    }

    private EOEditingContext edc() {
        return wocomponent.edc();
    }

//    public WOActionResults afficherAssociationSelectionne() {
//        AssociationNode node = uneAssociation();
//        if (node != null && node != rootAssociation()) {
//            EOAssociation association = (EOAssociation) node.object();
//            ((CktlAjaxAssociationTreeView) wocomponent).setSelection(association);
//            EOAssociation associationPere = (EOAssociation) node.parent().object();
//            ((CktlAjaxAssociationTreeView) wocomponent).setSelectionPere(associationPere);
////            rootAssociation = null;
//        }
//        // Callback
//        return ((CktlAjaxAssociationTreeView) wocomponent).didSelectAssociation();
//    }
    
    public WOActionResults afficherAssociationSelectionne() {
        AssociationNode node = uneAssociation();
        if (node != null && node != rootAssociation()) {
            EOAssociation association = (EOAssociation) node.object();
            ((CktlAjaxAssociationTreeView) wocomponent).setSelection(association);
            if (node.parent().object() == null || (node.parent().object() instanceof EmptyRootObject)) {
                ((CktlAjaxAssociationTreeView) wocomponent).setSelectionPere(null);
            }
            else {
                EOAssociation associationPere = (EOAssociation) node.parent().object();
                ((CktlAjaxAssociationTreeView) wocomponent).setSelectionPere(associationPere);
                //            rootAssociation = null;
            }
        }
        // Callback
        return ((CktlAjaxAssociationTreeView) wocomponent).didSelectAssociation();
    }

    public AssociationNode rootAssociation() {
        if (rootAssociation == null ||
                (wocomponent.treeRootObject() != null && rootAssociation.object() instanceof EmptyRootObject) ||
                (wocomponent.treeRootObject() == null && !(rootAssociation.object() instanceof EmptyRootObject))) {
            if (wocomponent.treeRootObject() != null) {
                rootAssociation = new AssociationNode(null, wocomponent.treeRootObject());
            }
            else {
                rootAssociation = new AssociationNode(null, new EmptyRootObject());
            }
        }
        return rootAssociation;
    }

    public void setRootAssociation(AssociationNode rootAssociation) {
        this.rootAssociation = rootAssociation;
    }

    public AssociationNode uneAssociation() {
        return uneAssociation;
    }

    public void setUneAssociation(AssociationNode uneAssociation) {
        this.uneAssociation = uneAssociation;
    }

    public String uneAssociationLibelle() {
        NSKeyValueCoding obj = uneAssociation().object();
        return (String) obj.valueForKey(EOAssociation.ASS_LIBELLE_KEY);
    }

    public Object delegate() {
        if (_delegate == null) {
            _delegate = new CktlAjaxAssociationTreeViewCtrl.Delegate();
        }
        return _delegate;

    }

    public class Delegate implements AjaxTreeModel.Delegate {

        public NSArray childrenTreeNodes(Object node) {
            if (node != null) {
                return ((AssociationNode) node).children();
            }
            return NSArray.EmptyArray;
        }

        public boolean isLeaf(Object node) {
            if (node != null) {
                return ((AssociationNode) node).isLeaf();
            }
            return true;
        }

        public Object parentTreeNode(Object node) {
            if (node != null) {
                return ((AssociationNode) node).parent();
            }
            return null;
        }
    }

    public class AssociationNode {
        public NSArray children;
        public AssociationNode parent;
        public NSKeyValueCoding object;

        public AssociationNode(AssociationNode parent, NSKeyValueCoding obj) {
            object = obj;
            this.parent = parent;
        }

        public NSArray children() {
            if (children == null) {
                NSArray res = NSArray.EmptyArray;
                if (object() instanceof EmptyRootObject) {
                    res = EOAssociation.getRacines(edc());
                    //                  if (wocomponent.getTypeAssociationQualifier() != null) {
                    //                      res = EOQualifier.filteredArrayWithQualifier(res, wocomponent.getTypeAssociationQualifier());
                    //                  }                   
                }
                else {
                    NSMutableArray array = new NSMutableArray();
                    if (wocomponent.getQualifier() != null) {
                        array.addObject(wocomponent.getQualifier());
                    }
                    //                  if (wocomponent.getTypeAssociationQualifier() != null) {
                    //                      array.addObject( wocomponent.getTypeAssociationQualifier());
                    //                  }
                    if (array.count() > 0) {
                        res = EOQualifier.filteredArrayWithQualifier(((EOAssociation) object()).getFils(edc()), new EOAndQualifier(array));
                    }
                    else {
//                        res = ((EOAssociation) object()).getFils(edc());
//                        res = ((EOAssociation) object()).getFils(edc(), EOAssociation.QUAL_ASS_VALIDE);
                        res = EOQualifier.filteredArrayWithQualifier(((EOAssociation) object()).getFils(edc()), EOAssociation.QUAL_ASS_VALIDE);
                    }
                    res = ERXArrayUtilities.sortedArraySortedWithKey(res, EOAssociation.ASS_LIBELLE_KEY, ERXSortOrdering.CompareCaseInsensitiveAscending);
                }
                
                NSMutableArray res2 = new NSMutableArray();
                for (int i = 0; i < res.count(); i++) {
                    AssociationNode node = new AssociationNode(this, (NSKeyValueCoding) res.objectAtIndex(i));
                    res2.addObject(node);
                }
                children = res2.immutableClone();
            }
            return children;
        }
        
        public void setChildren(NSArray children) {
            this.children = children;
        }

        public boolean isLeaf() {
            return (children().count() == 0);
        }

        public AssociationNode parent() {
            return parent;
        }

        public void setParent(AssociationNode node) {
            parent = node;
        }

        public NSKeyValueCoding object() {
            return object;
        }
    }

    public class EmptyRootObject implements NSKeyValueCoding {

        public EmptyRootObject() {
        }

        public void takeValueForKey(Object arg0, String arg1) {

        }

        public Object valueForKey(String arg0) {
            return "root";
        }

    }
    
	public WOActionResults afficherFonctionSelectionnee() {
		AssociationNode node = uneAssociation();
		if (node != null ) {
//			if (node != null && node != rootAssociation()) {
			EOAssociation groupe = (EOAssociation) node.object();
			((CktlAjaxAssociationTreeView) wocomponent).setSelection(groupe);
		}
		return null;
	}
	
	
	public AjaxTreeModel getMyTreeModel() {
		return myTreeModel;
	}
	
	public void setMyTreeModel(AjaxTreeModel myTreeModel) {
		this.myTreeModel = myTreeModel;
	}
	
	public CktlAjaxAssociationTreeView getWocomponent() {
		return wocomponent;
	}
	
	public void setWocomponent(CktlAjaxAssociationTreeView wocomponent) {
		this.wocomponent = wocomponent;
	}
	
	public AssociationNode selectObjectInTree(AssociationNode node, EOAssociation association) {
		AssociationNode newSelectedNode = null;
		if (node.object().equals(association)) {
			newSelectedNode = node;
		}
		else {
			Enumeration en = node.children().objectEnumerator();
			while (newSelectedNode == null && en.hasMoreElements()) {
				AssociationNode tmpNode = (AssociationNode) en.nextElement();
				newSelectedNode = selectObjectInTree(tmpNode, association);
			}
		}
		if (newSelectedNode != null) {
			myTreeModel.setExpanded(node, true);
		}
		return newSelectedNode;
	}
	
	/**
	 * Ouvre tous les noeuds jusqu'au niveau donné compris.<br/>
	 * Exemple : <br/>
	 *     <pre>
	 *         expandAllNodesToLevel(1); 
	 *     </pre>
	 *     Va Ouvrir la racine (level = 0), puis les fils de la racine (level = 1)
	 * @param maxLevel le niveau jusqu'où on ouvre les noeuds
	 */
	public void expandAllNodesToLevel(int maxLevel) {
	    _expandAllNodesToLevel(rootAssociation(), 0, maxLevel);
	}
	
	private void _expandAllNodesToLevel(AssociationNode node, int currentLevel, int maxLevel) {
	    getMyTreeModel().setExpanded(node, true);
	    if (currentLevel < maxLevel) {
	        for (Object child : node.children()) {
	            currentLevel = currentLevel + 1;
	            _expandAllNodesToLevel((AssociationNode)child, currentLevel, maxLevel);
	        }
	    }
	}
	
	public NSArray<String> getLesAssociations() {
		NSMutableArray<String> res = new NSMutableArray<String>();
		res.add(0, "Institutionnelles");
		res.add(1, "Locales");
		res.add(2, "Expirées");
		res.add(3, "Toutes");
		
		return res;
	}
	
	public String cssItemTree() {
		String styleCSS = new String();
		
		if ( (getWocomponent().getAssociationIndex()%2) == 0 ) {
//			styleCSS = "ValiditeFondBlanc";
			styleCSS = "even";
		}
		else {
//			styleCSS = "InvaliditeFondGris";
			styleCSS = "odd";
		}
		return styleCSS;
	}
}
