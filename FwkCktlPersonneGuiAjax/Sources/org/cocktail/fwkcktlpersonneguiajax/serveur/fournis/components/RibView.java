/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components;

import org.cocktail.fwkcktlpersonne.common.controles.ControleIban;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOContext;

/**
 * Affiche le détail d'un rib. Utilisez plutot {@link RibUI} à la place.
 * 
 * @binding rib obligatoire
 * @binding utilisateurPersId obligatoire
 * @binding maskNumeroSuivantPersId facultatif. Mettre à false pour ne pas masquer les n° de compte suivant les droits de l'utilisateur.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RibView extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_rib = "rib";
	public static final String BINDING_isCompactView = "isCompactView";
	public static final String BINDING_maskNumeroSuivantPersId = "maskNumeroSuivantPersId";

	public static final Boolean DEFAULT_isCompactView = Boolean.FALSE;
	public static final Boolean DEFAULT_maskNumeroSuivantPersId = Boolean.TRUE;

	public RibView(WOContext context) {
		super(context);
	}

	public EORib getRib() {
		return (EORib) valueForBinding(BINDING_rib);
	}

	public Boolean isCompactView() {
		return booleanValueForBinding(BINDING_isCompactView, DEFAULT_isCompactView);
	}

	public Boolean maskNumeroSuivantPersId() {
		return booleanValueForBinding(BINDING_maskNumeroSuivantPersId, DEFAULT_maskNumeroSuivantPersId);
	}

	public Boolean ifRibPaye() {
		return Boolean.valueOf(getRib() != null && getRib().isRibControleParPaye());
	}

	public String getNoCompteFR() {
		if (getRib() != null) {
			if (maskNumeroSuivantPersId() && !getAppUserForAnnuaire().hasDroitModificationEORib(getRib())) {
				return getRib().noCompteMasque();
			}
			else {
				return getRib().noCompte();
			}
		}
		return null;
	}

	public String getIban() {
		String res = null;
		if (getRib() != null) {
			if (maskNumeroSuivantPersId() && !getAppUserForAnnuaire().hasDroitModificationEORib(getRib())) {
				res = getRib().ibanMasque();
			}
			else {
				res = getRib().iban();
			}
			return ControleIban.formatIban(res);
		}
		return null;
	}

	public Boolean showFormatFR() {
		return ControleIban.isCompteFR(getIban());
	}

}
