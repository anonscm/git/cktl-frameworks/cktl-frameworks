/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneGestion;
import org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs.FournisseurFormCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs.FournisseurSrchCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Composant qui permet de gerer la recherche, la creation et la modification des fournisseurs. Les bindings de {@link PersonneGestion} peuvent être
 * utilisés.
 * 
 * @binding selectedFournisseur
 * @binding callbackOnSelectionnerFournisseur
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class FournisseurCreation extends PersonneGestion implements FournisseurForm.IFournisseurFormContainer {

	private static final long serialVersionUID = 1L;

	/** Obligatoire. Indique au composant parent l'objet {@link EOFournis} renvoye. Ce fournisseur n'est pas obligatoirement valide */
	public static final String BINDING_selectedFournisseur = "selectedFournisseur";

	/** Facultatif. Indiquez le nom de l'action a executer dans le composant parent lorsque l'utilisateur selectionne le fournisseur. */
	public final static String BINDING_callbackOnSelectionnerFournisseur = "callbackOnSelectionnerFournisseur";
	//	
	//	/**Facultatif. Indique s'il faut reinitialiser le formulaire de recherche (one shot). Si précisé, doit etre settable. */
	//	public final static String BINDING_resetSrchForm= "resetSrchForm";

	//private FournisseurCreationCtrl ctrl;
	private EOFournis selectedFournis;

	public FournisseurCreation(WOContext context) {
		super(context);
		//ctrl = new FournisseurCreationCtrl(this);
	}

	/**
	 * Fournisseur eventuellement selectionne suite a la recherche.
	 */
	public EOFournis getSelectedFournis() {
		return selectedFournis;
	}

	public void setSelectedFournis(EOFournis selectedFournis) {
		//LRLogger.debugCaller();
		//LRLogger.debug(selectedFournis);
		this.selectedFournis = selectedFournis;
	}

	public WOActionResults onCreerFournisseur() {
		//System.out.println("FournisseurCreation.onCreerFournisseur()");
		try {
			if (getSelectedPersonne() == null) {
				throw new Exception("Aucune personne selectionnee. Impossible de creer un fournisseur.");
			}
			setSelectedFournis(null);
			setWantResetForm(Boolean.TRUE);
			setIsVueSrch(Boolean.FALSE);
			setScriptAfterForm("enableCreation(false);");

		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onModifierFournisseur() {
		WOActionResults res = null;
		try {
			if (getSelectedFournis() == null) {
				throw new Exception("Aucun fournisseur selectionne. Impossible de le modifier.");
			}
			res = super.onModifierPersonne();
		} catch (Exception e) {
			setErreurSaisieMessage(e.getMessage());
		}
		return res;

		//		System.out.println("FournisseurCreation.onModifierFournisseur()");
		//		try {
		//			if (getSelectedPersonne() == null) {
		//				throw new Exception("Aucune personne selectionnee. Impossible de creer un fournisseur.");
		//			}
		//			if (getSelectedFournis() == null) {
		//				throw new Exception("Aucun fournisseur selectionne. Impossible de le modifier.");
		//			}
		//			setWantResetForm(Boolean.TRUE);
		//			setIsVueSrch(Boolean.FALSE);
		//		} catch (Exception e) {
		//			setErreurSaisieMessage(e.getMessage());
		//		}
		//		return null;		
	}

	public WOActionResults onValiderFournisseur() {

		System.out.println("FournisseurCreation.onValiderFournisseur() = " + getSelectedFournis());
		return null;
	}

	public WOActionResults onSelectionnerPersonne() {
		setValueForBinding(getSelectedFournis(), BINDING_selectedFournisseur);
		WOActionResults res = super.onSelectionnerPersonne();
		if (valueForBinding(BINDING_callbackOnSelectionnerFournisseur) != null) {
			res = performParentAction((String) valueForBinding(BINDING_callbackOnSelectionnerFournisseur));
		}
		return res;
	}

	public WOActionResults onFormEnregistrer() {
		return super.onFormEnregistrer();
		//		setIsVueSrch(Boolean.TRUE);
		//		setWantRefreshDetail(Boolean.TRUE);
		//		return null;
	}

	public WOActionResults onFormAnnuler() {
		return super.onFormAnnuler();
		//		setIsVueSrch(Boolean.TRUE);
		//		setWantRefreshDetail(Boolean.TRUE);
		//		//setListeResOncomplete("function(oc) { enableCreation(true);}");
		//		if (selectedPersonne != null && (selectedPersonne.globalID() == null ||  selectedPersonne.hasTemporaryGlobalID() ) ) {
		//			//setSelectedPersonne(null);
		//			if (dgPersonnes.allObjects().indexOf(  selectedPersonne  ) != NSArray.NotFound  ) {
		//				dgPersonnes.deleteObjectAtIndex( dgPersonnes.allObjects().indexOf(  selectedPersonne  )   );
		//			}
		//		}		
		//		setScriptAfterForm(null);
		//		return null;

	}

	//	private void setScriptAfterForm(String object) {
	//		scriptAfterForm = object;
	//	}
	//
	//
	//	public Boolean wantReset() {
	//		return wantReset;
	//	}
	//
	//	public void setWantReset(Boolean wantReset) {
	//		this.wantReset = wantReset;
	//	}
	//	
	//	public Boolean wantRefreshDetail() {
	//		return wantRefreshDetail;
	//	}
	//	
	//	public void setWantRefreshDetail(Boolean wantRefreshDetail) {
	//		this.wantRefreshDetail = wantRefreshDetail;
	//	}

	//	public boolean wantResetFournisForm() {
	//		System.out.println("FournisseurCreation.wantResetFournisForm() " + wantReset);
	//		return wantReset;
	//	}

	public EOFournis getSelectedFournisForForm() {
		return getSelectedFournis();
	}

	//	public IPersonne getSelectedPersonneForForm() {
	//		return getSelectedPersonne();
	//	}
	//
	//	public void setResetFournisForm(boolean b) {
	//		wantReset = b;
	//	}

	public void setSelectedFournisForForm(EOFournis leFournis) {
		setSelectedFournis(leFournis);

	}

	//	public void setSelectedPersonneForForm(IPersonne laPersonne) {
	//		setSelectedPersonne(laPersonne);
	//	}

	//	public Boolean getForceRecherche() {
	//		return forceRecherche;
	//	}
	//
	//	public void setForceRecherche(Boolean forceRecherche) {
	//		this.forceRecherche = forceRecherche;
	//	}
	//
	//	public String getAfterFormEnregistrerMessage() {
	//		return afterFormEnregistrerMessage;
	//	}
	//	
	//	public Boolean hasAfterFormEnregistrerMessage() {
	//		return Boolean.valueOf(!MyStringCtrl.isEmpty(afterFormEnregistrerMessage));
	//	}
	//	
	//	public void setAfterFormEnregistrerMessage(String afterFormEnregistrerMessage) {
	//		this.afterFormEnregistrerMessage = afterFormEnregistrerMessage;
	//	}

	public Boolean iFUserHasDroitsForFournisseurs() {
		return (getAppUserForAnnuaire().hasDroitVisualisationEOFournis(null, null));
	}

	//	public ERXDisplayGroup getDgPersonnes() {
	//		return dgPersonnes;
	//	}

	//
	//	public String getListeResOncomplete() {
	//		String res = listeResOncomplete;
	//		setListeResOncomplete(null);
	//		return listeResOncomplete;
	//	}
	//
	//
	//	public void setListeResOncomplete(String listeResOncomplete) {
	//		this.listeResOncomplete = listeResOncomplete;
	//	}

	//	public Boolean hasScriptAfterForm() {
	//		return Boolean.valueOf(scriptAfterForm!=null);
	//	}
	//
	//
	//	public String getScriptAfterForm() {
	//		String res = scriptAfterForm;
	//		setScriptAfterForm(null);
	//		return res;
	//	}

	public FournisseurFormCtrl fournisseurFormCtrl = new FournisseurFormCtrl();
	public FournisseurSrchCtrl fournisseurSrchCtrl = new FournisseurSrchCtrl();

}
