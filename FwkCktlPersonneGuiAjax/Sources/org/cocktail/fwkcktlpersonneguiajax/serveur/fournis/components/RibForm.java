/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components;

import java.util.HashMap;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.controles.ControleIban;
import org.cocktail.fwkcktlpersonne.common.metier.EOBanque;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEC;

/**
 * Formulaire de création d'un RIB. Utilisez plutot {@link RibUI} à la place.
 * 
 * @binding rib
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RibForm extends AComponent {

	private static final long serialVersionUID = 1L;

	public static final String BINDING_rib = "rib";
	public static final String BINDING_resetRibForm = "resetRibForm";

	public EOBanque unBanque;
	//private EOBanque selectedBanque;
	private String erreurSaisieMessage;

	private Boolean isTabRibFrSelected;
	private Boolean isTabRibIbanSelected = Boolean.TRUE;

	//private Boolean isSaisieManuelle = Boolean.FALSE;
	private EOBanque banqueACreer = null;

	//private EOBanque selectedBanqueForBic;
	public EOBanque unFilteredBanqueForBic;
	private NSArray<EOBanque> filteredBanquesForBic = NSArray.emptyArray();

	private EOEditingContext edcForNewBanque;

	private HashMap<String, String> warningMessages = new HashMap<String, String>();

	public RibForm(WOContext context) {
		super(context);
	}

	public void appendToResponse(WOResponse arg0, WOContext arg1) {
		if (booleanValueForBinding(BINDING_resetRibForm, Boolean.FALSE)) {
			filteredBanquesForBic = NSArray.emptyArray();
			setValueForBinding(Boolean.FALSE, BINDING_resetRibForm);
		}
		super.appendToResponse(arg0, arg1);
		AjaxUtils.addScriptResourceInHead(arg1, arg0, frameworkName(), "scripts/cktlstringutils.js");

	}

	public EORib getRib() {
		return (EORib) valueForBinding(BINDING_rib);
	}

	public void setRib(EORib rib) {
		this.setValueForBinding(rib, BINDING_rib);
	}

	public String getErreurSaisieMessage() {
		String tmp = erreurSaisieMessage;
		return tmp;
	}

	public void setErreurSaisieMessage(String msg) {
		erreurSaisieMessage = msg;
	}

	public Boolean isErreurSaisie() {
		return Boolean.valueOf(erreurSaisieMessage != null);
	}

	//	public NSArray<EOBanque> getFilteredBanque() {
	//		return getFilteredBanque(getRib().cBanque());
	//	}

	public String ribEtabFieldId() {
		return getComponentId() + "_RibEtab";
	}

	public String ribGuichetFieldId() {
		return getComponentId() + "_RibGuichet";
	}

	public String ribDomiciliationFieldId() {
		return getComponentId() + "_RibDomiciliation";
	}

	public String ribDomiciliationField2Id() {
		return getComponentId() + "_RibDomiciliation2";
	}

	public String jsAfterUpdateBanque() {
		String res = "";
		res = "function(text, li) {var chaine = li.innerText; if (typeof(chaine)=='undefined') {chaine = cleanString(li.textContent);}";
		res += "$('" + ribEtabFieldId() + "_field').value = chaine.substring(0, 5);";
		res += "$('" + ribGuichetFieldId() + "').value = chaine.substring(6, 11);";
		res += "$('" + ribDomiciliationFieldId() + "').value = chaine.substring(12);";
		res += "Element.hide('busy1');}";
		return res;
	}

	//	public NSArray<EOBanque> getFilteredBanque(String filtBanque) {
	//		return EOBanque.getFilteredBanques(edc(), filtBanque);
	//	}

	public EOBanque getSelectedBanque() {
		return getRib().toBanque();
	}

	public void setSelectedBanque(EOBanque selectedBanque) {
		//this.selectedBanque = selectedBanque;
		if (selectedBanque == null) {
			//			getRib().setToBanqueRelationship(null);
			getRib().setCBanque(null);
			getRib().setCGuichet(null);
			//		filteredBanquesForBic = NSArray.emptyArray();
		}
		else {
			if (selectedBanque.bic() != null) {
				updateFilteredBanquesForBic(selectedBanque.bic());
			}
			else {
				filteredBanquesForBic = new NSArray<EOBanque>(selectedBanque);
			}
			getRib().setToBanqueRelationship(selectedBanque);
			if (selectedBanque != null) {
				if (getRib().cBanque() == null && getRib().cGuichet() == null) {
					getRib().setCBanque(selectedBanque.cBanque());
					getRib().setCGuichet(selectedBanque.cGuichet());
				}
			}
		}
	}

	public String domiciliationContainerId() {
		return getComponentId() + "_domiciliation";
	}

	public String domiciliationContainer2Id() {
		return getComponentId() + "_domiciliation2";
	}

	public WOActionResults updateDomiciliation() {
		if (getRib() != null && getRib().toBanque() != null){
			setSelectedBanque(getRib().toBanque());
		}
		return null;
	}

	public String getTabId() {
		return getComponentId() + "_tabs";
	}

	public String getTabRibIbanId() {
		return getComponentId() + "_tabIban";
	}

	public String getTabRibFrId() {
		return getComponentId() + "_tabFr";
	}

	public Boolean isTabRibFrSelected() {
		return isTabRibFrSelected;
	}

	public void setTabRibFrSelected(Boolean isTabRibFrSelected) {
		this.isTabRibFrSelected = isTabRibFrSelected;
	}

	public Boolean isTabRibIbanSelected() {
		return isTabRibIbanSelected;
	}

	public void setTabRibIbanSelected(Boolean isTabRibIbanelected) {
		this.isTabRibIbanSelected = isTabRibIbanelected;
	}

	public String getStyleTab() {
		return "#" + getTabId() + "_panecontrol {min-height: 20px;}";
	}

	public String getBic() {
		return (getRib() == null ? null : (getRib().toBanque() == null ? null : getRib().toBanque().bic()));
	}

	public void setBic(String bic) {
		if (getRib() == null) {
			return;
		}
		getRib().setBic(bic);
		getRib().setToBanqueRelationship(null);
		updateFilteredBanquesForBic(bic);
		if (filteredBanquesForBic.count() >= 1) {
			setSelectedBanque(filteredBanquesForBic.objectAtIndex(0));
			//getRib().setToBanqueRelationship(filteredBanquesForBic.objectAtIndex(0));
		}
	}

	public void updateFilteredBanquesForBic(String bic) {
		filteredBanquesForBic = banquesForBic(bic);
	}

	private NSArray<EOBanque> banquesForBic(String bic) {
		if (bic != null) {
			return EOBanque.findBanquesWithBic(edc(), bic);
		}
		return NSArray.emptyArray();
	}

	public NSArray<EOBanque> banquesForBic() {
		return banquesForBic(getBic());
	}

	//
	//	public EOBanque getSelectedBanqueForBic() {
	//		if (getRib() == null) {
	//			return null;
	//		}
	//		return getRib().toBanque();
	//		//return selectedBanqueForBic;
	//	}
	//
	//	public void setSelectedBanqueForBic(EOBanque selectedBanqueForBic) {
	//		//this.selectedBanqueForBic = selectedBanqueForBic;
	//		getRib().setToBanqueRelationship(selectedBanqueForBic);
	//	}

	public NSArray<EOBanque> filteredBanquesForBic() {
		return filteredBanquesForBic;
	}

	public Boolean isNotBicSaisi() {
		return getBic() == null;
	}

	public WOActionResults addBanque() {
		//On crée la banque directement dans la base indépendamment de l'editingContext du composant
		edcForNewBanque = ERXEC.newEditingContext(mySession().defaultEditingContext().parentObjectStore());
		banqueACreer = EOBanque.creerInstance(edcForNewBanque);
		banqueACreer.setPersIdCreation(getUtilisateurPersId());
		banqueACreer.setPersIdModification(getUtilisateurPersId());
		return null;
	}

	public String getNewBanqueDialogID() {
		return getComponentId() + "_newBanqueDialog";
	}

	public WOActionResults onBanqueAnnuler() {
		banqueACreer = null;
		CktlAjaxWindow.close(context(), getNewBanqueDialogID());
		return null;
	}

	public WOActionResults onBanqueEnregistrer() {
		CktlAjaxWindow.close(context(), getNewBanqueDialogID());
		setSelectedBanque(banqueACreer.localInstanceIn(edc()));
		return null;
	}

	public EOBanque getBanqueACreer() {
		return banqueACreer;
	}

	public void setBanqueACreer(EOBanque banqueACreer) {
		this.banqueACreer = banqueACreer;
	}

	public EOEditingContext edcForNewBanque() {
		return edcForNewBanque;
	}

	public Boolean isDomiciliationPopupDisabled() {
		return (filteredBanquesForBic == null || filteredBanquesForBic.count() == 0);
	}

	public String getIban() {
		String res = null;
		if (getRib() != null) {
			res = getRib().iban();
			if (res != null) {
				return ControleIban.formatIban(res);
			}
		}
		return null;
	}

	public void setIban(String s) {
		String res = s;
		if (res != null) {
			res = s.trim().replaceAll(" ", "");
		}
		getRib().setIban(res);
		//if (!getRib().isRibFRRempli()) {
		getRib().fillRibFRFromIban();
		//}
	}

	public String getMsgContainerId() {
		return getComponentId() + "_msgs";
	}

	public String getMsgContainer2Id() {
		return getComponentId() + "_msgsx";
	}

	public WOActionResults onSetBanqueFR() {
		if (getRib().toBanque() != null) {
			updateFilteredBanquesForBic(getRib().toBanque().bic());
		}
		return null;
	}

}
