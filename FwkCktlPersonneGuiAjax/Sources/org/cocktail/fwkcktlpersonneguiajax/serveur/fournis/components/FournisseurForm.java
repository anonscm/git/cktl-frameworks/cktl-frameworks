/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxUserQuestionDelegate;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneForm;
import org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components.FournisseurForm.IFournisseurFormContainer;
import org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs.FournisseurFormCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.CktlAjaxUtils;

/**
 * Composant pour le formulaire de creation d'un fournisseur. Ce composant est un sous-composant de {@link FournisseurCreation} et n'est pas sensé
 * être utilisé directement, utilisez {@link FournisseurCreation} à la place. Pour utiliser ce composant, son composant parent doit respecter
 * l'interface java {@link IFournisseurFormContainer}. par défaut pour un individu dans le contexte "gestion des fournisseurs", le N° INSEE et les
 * informations de naissance ne sont pas affichés (ni en lecture ni en modification). C'est géré par les paramètres
 * org.cocktail.personnegui.fournisseur.detail.individus.showxxxx et org.cocktail.personnegui.fournisseur.form.individus.showxxx qui sont à false par
 * défaut. On a donc la possibilité de passer les paramètres à true si besoin. Ensuite les règles suivantes sont appliquées :
 * <ul>
 * <li>La modification du n° INSEE n'est permise que si l'individu n'est pas personnel ni etudiant</li>
 * <li>La modification de la date de naissance n'est permise que si l'individu n'est pas personnel ni etudiant</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class FournisseurForm extends PersonneForm {

	private static final long serialVersionUID = 1L;
	public EORib currentItemRib;
	public EOCompte currentItemCompte;
	private Boolean isRibEditing = Boolean.FALSE;
	private static final String BINDING_TYPE_FOURNISSEUR_DEFAUT = "typeFournisseurDefaut";

	public EOQualifier qualifierForTypeTel = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_PAR), new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_ETUD), new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_PRV)
	}));

	private final CktlAjaxUserQuestionDelegate userQuestionDelegate;

	public FournisseurForm(WOContext context) {
		super(context);
		userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
	}

	public String getNom() {
		return getMyCtrl().getLeFournis().toPersonne().getNomPrenomAffichage() + " (n°" + getMyCtrl().getLeFournis().toPersonne().getNumero() + ")";
	}

	public WOActionResults onAnnuler() {
		setIsRibEditing(Boolean.FALSE);
		return super.onAnnuler();
	}

	public WOActionResults onEnregistrer() {
		boolean doIt = false;
		if (fouValideEncoursSelected().booleanValue() && getAppUserForAnnuaire().hasDroitValidationEOFournis(getLeFournis(), AUtils.currentExercice())) {
			doIt = false;
			String s = "Souhaitez-vous rendre ce fournisseur valide ?";
			Boolean confirm = userQuestionDelegate.askUserAsBoolean("question1", s, getMainContainerId());

			if (confirm == null) {
				//La reponse n'a pas encore été donnée, on pose la question
				return null;
			}
			else {
				doIt = confirm.booleanValue();
			}

			if (doIt) {
				setFouValideValideSelected(Boolean.TRUE);
			}
			userQuestionDelegate.clearAnswers();
		}

		boolean newFournis = false;
		try {
			String msgSuccess = "";
			if (getLeFournis().hasTemporaryGlobalID()) {
				newFournis = true;
			}
			if (getMyCtrl().onEnregistrer()) {
				if (newFournis) {
					//Envoyer le mail aux valideurs si fournisseur pas valide
					if (getLeFournis().isFouValideEncours() && !getMyCtrl().isUserCanValiderFournis()) {
						try {
							getMyCtrl().sendMailDemandeValidationToValideurs(getLeFournis());
						} catch (Exception e) {
							msgSuccess = msgSuccess + "\n" + e.getMessage();
						}
					}
				}
				else {
					if (!getLeFournis().fouValide().equals(getMyCtrl().previousFouValide)) {
						//Si le valideur n'est pas le createur
						if (getLeFournis().toValideFournis().getCreateurAsUtilisateur() != null && !getLeFournis().toValideFournis().getValideurAsUtilisateur().equals(getLeFournis().toValideFournis().getCreateurAsUtilisateur())) {
							try {
								getMyCtrl().sendMailInformationValidation(getLeFournis());
							} catch (Exception e) {
								// Si le mail ne part pas on ne bloque pas le reste...
								e.printStackTrace();
								setErreurSaisieMessage(e.getMessage());
							}
						}
					}
				}

				if (!getLeFournis().hasRibsValides()) {
					msgSuccess += "ATTENTION : " + "Le fournisseur n'a aucun rib valide d'affecté.";
				}

				if (newFournis) {
					msgSuccess = "Le fournisseur " + getLeFournis().getNomComplet() + " (code : " + getLeFournis().fouCode() + ") a été correctement créé.\n" + msgSuccess;
				}
				setValueForBinding(msgSuccess, BINDING_displayMessageOnParent);

				IFournisseurFormContainer myParent = ((IFournisseurFormContainer) parent());
				myParent.setSelectedFournisForForm(getLeFournis());
				myParent.setSelectedPersonneForForm(getLaPersonne());
				setLaPersonne(null);
				setLeFournis(null);
				WOActionResults res = performParentAction(getCallbackOnEnregistrer());
				return res;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
			return null;
		}
	}

	public String getNumero() {
		return getMyCtrl().getLeFournis().toPersonne().getNumero();
	}

	public WOActionResults onSelectCompte() {
		return null;
	}

	public FournisseurFormCtrl getMyCtrl() {
		return (FournisseurFormCtrl) getCtrl();
	}

	public Boolean getIsEtranger() {
		return getMyCtrl().getLeFournis().isEtranger();
	}

	public void setIsEtranger(Boolean isEtranger) {
		//	LRLogger.debugCaller();
		//	LRLogger.debug(isEtranger);
		getMyCtrl().getLeFournis().setIsEtranger(isEtranger);
	}

	public String getFouCode() {
		return (getMyCtrl().getLeFournis().fouCode() == null ? "n/a" : getMyCtrl().getLeFournis().fouCode());
	}

	public Boolean getCreerCompteFournisseurAuto() {
		return getMyCtrl().getCreerCompteFournisseurAuto();
	}

	public Boolean showCreationCompte() {
		return getMyCtrl().showCreationCompte();
	}

	public EOCompte getSelectedCompte() {
		return getMyCtrl().getSelectedCompte();
	}

	public void setSelectedCompte(EOCompte compte) {
		getMyCtrl().setSelectedCompte(compte);
	}

	public EOFournis getLeFournis() {
		return getCtrl().getLeFournis();
	}

	public void setLeFournis(EOFournis leFournis) {
		//this.leFournis = leFournis;
		getCtrl().setLeFournis(leFournis);
	}

	public FournisseurFormCtrl getCtrl() {
		return (FournisseurFormCtrl) super.getCtrl();
	}

	@Override
	public void appendToResponse(WOResponse arg0, WOContext arg1) {
		super.appendToResponse(arg0, arg1);
		CktlAjaxUtils.addScriptResourceInHead(arg1, arg0, frameworkName(), "scripts/cktlstringutils.js");

	}

	public void resetForm() {
		setIsRibEditing(Boolean.FALSE);
		super.resetForm();
	}

	/**
	 * Appelle le controleur pour initialiser les donnees.
	 */
	public void initialiseData() {
		//LRLogger.debugCaller();
		IFournisseurFormContainer myParent = ((IFournisseurFormContainer) parent());
		IPersonne laPersonne = myParent.getSelectedPersonneForForm();
		EOFournis leFournis = myParent.getSelectedFournisForForm();
		getCtrl().initialiseData(laPersonne, leFournis);

		//si binding typeFournisseurDefaut est renseigné, modifier le fouType
		if (hasBinding(BINDING_TYPE_FOURNISSEUR_DEFAUT) && valueForBinding(BINDING_TYPE_FOURNISSEUR_DEFAUT) != null) {
			if (isCreation()) {
				getLeFournis().setFouType((String) valueForBinding(BINDING_TYPE_FOURNISSEUR_DEFAUT));
			}
		}

	}

	//	
	//	
	/**
	 * Interface a respecter par le conteneur du composant FournisseurForm. A utiliser a la place de bindings.
	 * 
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public interface IFournisseurFormContainer extends IPersonneFormContainer {
		public void setSelectedFournisForForm(EOFournis leFournis);

		/** Renvoie le fournisseur selectionne (null s'il s'agit d'une création) */
		public EOFournis getSelectedFournisForForm();

	}

	public Boolean fouTypeFournisseurSelected() {
		return Boolean.valueOf(getLeFournis().isTypeFournisseur());
	}

	public void setFouTypeFournisseurSelected(Boolean val) {
		if (val.booleanValue()) {
			getLeFournis().setFouType(EOFournis.FOU_TYPE_FOURNISSEUR);
		}
	}

	public Boolean fouTypeClientSelected() {
		return Boolean.valueOf(getLeFournis().isTypeClient());
	}

	public void setFouTypeClientSelected(Boolean val) {
		if (val.booleanValue()) {
			getLeFournis().setFouType(EOFournis.FOU_TYPE_CLIENT);
		}
	}

	public Boolean fouTypeTiersSelected() {
		return Boolean.valueOf(getLeFournis().isTypeTiers());
	}

	public void setFouTypeTiersSelected(Boolean val) {
		if (val.booleanValue()) {
			getLeFournis().setFouType(EOFournis.FOU_TYPE_TIERS);
		}
	}

	public Boolean fouValideEncoursSelected() {
		return Boolean.valueOf(EOFournis.FOU_VALIDE_NON.equals(getLeFournis().fouValide()));
	}

	public void setFouValideEncoursSelected(Boolean val) {
		if (val.booleanValue()) {
			getLeFournis().setFouValide(EOFournis.FOU_VALIDE_NON);
		}
	}

	public Boolean fouValideValideSelected() {
		return Boolean.valueOf(EOFournis.FOU_VALIDE_OUI.equals(getLeFournis().fouValide()));
	}

	public void setFouValideValideSelected(Boolean val) {
		if (val.booleanValue()) {
			getLeFournis().setFouValide(EOFournis.FOU_VALIDE_OUI);
		}
	}

	public Boolean fouValideAnnuleSelected() {
		return Boolean.valueOf(EOFournis.FOU_VALIDE_ANNULE.equals(getLeFournis().fouValide()));
	}

	public void setFouValideAnnuleSelected(Boolean val) {
		if (val.booleanValue()) {
			getLeFournis().setFouValide(EOFournis.FOU_VALIDE_ANNULE);
		}
	}

	public Boolean hasComptes() {
		return Boolean.valueOf(getLaPersonne().toComptes(EOCompte.QUAL_CPT_VALIDE_OUI).count() > 0);
	}

	public Boolean isCompteCreationAutorisee() {
		return getMyCtrl().isUserCanValiderFournis();
	}

	public NSArray vlansAutorisesPourCreationCompte() {
		return getMyCtrl().vlansAutorisesPourCreationCompte();
	}

	public Boolean isRibEditing() {
		return isRibEditing;
	}

	public void setIsRibEditing(Boolean isRibEditing) {
		this.isRibEditing = isRibEditing;
	}

	public Boolean isCreation() {
		return (getLeFournis() != null && getLeFournis().hasTemporaryGlobalID());
	}

	public String getFournisCreateur() {
		if (getLeFournis() != null && getLeFournis().toValideFournis() != null) {
			Integer utlOrdre = getLeFournis().toValideFournis().valCreation();
			if (utlOrdre != null) {
				EOUtilisateur util = EOUtilisateur.fetchByKeyValue(edc(), EOUtilisateur.UTL_ORDRE_KEY, utlOrdre);
				if (util != null) {
					return util.getPrenomAndNom();
				}
			}
		}
		return null;
	}

	public String getFournisValideur() {
		if (getLeFournis() != null && getLeFournis().toValideFournis() != null) {
			Integer utlOrdre = getLeFournis().toValideFournis().valValidation();
			if (utlOrdre != null) {
				EOUtilisateur util = EOUtilisateur.fetchByKeyValue(edc(), EOUtilisateur.UTL_ORDRE_KEY, utlOrdre);
				if (util != null) {
					return util.getPrenomAndNom();
				}
			}
		}
		return null;
	}

	public NSTimestamp getFournisValidationDateCreation() {
		if (getLeFournis() != null && getLeFournis().toValideFournis() != null) {
			return getLeFournis().toValideFournis().valDateCreate();
		}
		return null;
	}

	public NSTimestamp getFournisValidationDateValidation() {
		if (getLeFournis() != null && getLeFournis().toValideFournis() != null) {
			return getLeFournis().toValideFournis().valDateVal();
		}
		return null;
	}

	public String containerFouValiditeId() {
		return getComponentId() + "_ContainerFouValidite";
	}

	public String containerInfosAdminId() {
		return getComponentId() + "_infosAdmin";
	}

	public String jsFieldObserverEtrangerOnComplete() {
		return "function (oC){" + containerInfosAdminId() + "Update();}";
	}

	public Boolean allowEditNoInsee() {
		if (getLaPersonne().isIndividu()) {
			return !(getLeIndividu().isPersonnel() || getLeIndividu().isEtudiant());
		}
		return false;
	}

	public Boolean allowEditEtatCivil() {
		if (getLaPersonne().isIndividu()) {
			return !(getLeIndividu().isPersonnel() || getLeIndividu().isEtudiant());
		}
		return false;
	}

	public Boolean showEtatCivil() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWNAISSANCE);
	}

	public Boolean showNoInsee() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWINSEE);
	}

	public String defaultTypeAdresse() {
		return EOTypeAdresse.TADR_CODE_FACT;
	}

}
