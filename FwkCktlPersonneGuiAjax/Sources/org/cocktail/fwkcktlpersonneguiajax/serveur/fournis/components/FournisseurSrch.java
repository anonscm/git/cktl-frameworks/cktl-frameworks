/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxUserQuestionDelegate;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneSrch;
import org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs.FournisseurSrchCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Composant pour la recherche de personne lors de la création d'un fournisseur. Ce composant est un sous-composant de {@link FournisseurCreation} et
 * n'est pas sensé être utilisé directement, utilisez {@link FournisseurCreation} à la place. - Par défaut le n° insee et les infos de naissance ne
 * sont pas du tout affichées dans les recherches / création des fournisseurs
 * 
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */
public class FournisseurSrch extends PersonneSrch {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_callbackOnCreerFournisseur = "callbackOnCreerFournisseur";
	public static final String BINDING_callbackOnModifierFournisseur = "callbackOnModifierFournisseur";
	public static final String BINDING_callbackOnValiderFournisseur = "callbackOnValiderFournisseur";
	public static final String BINDING_callbackOnSelectionnerFournisseur = "callbackOnSelectionnerFournisseur";
	public static final String BINDING_selectionnerFournisseurLabel = "selectionnerFournisseurLabel";
	public static final String BINDING_unPersonneFournisseur = "unPersonneFournisseur";
	public static final String BINDING_srchTableViewHeight = "srchTableViewHeight";

	/** Facultatif. Indique s'il faut reinitialiser le formulaire de recherche (one shot). Si précisé, doit etre settable. */
	public final static String BINDING_resetSrchForm = "resetSrchForm";

	public EOQualifier qualifierForTypeAdresse = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_ETUD), new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_PAR), new EONotQualifier(EOTypeAdresse.QUAL_TADR_CODE_INVAL)
	}));
	public EOQualifier qualifierForTypeTel = new EOAndQualifier(new NSArray(new Object[] {
			new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_PAR), new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_ETUD), new EONotQualifier(EOTypeTel.QUAL_C_TYPE_TEL_PRV)
	}));

	public EOFournis unPersonneFournisseur;

	private String callbackOnCreerFournisseur;
	private String callbackOnModifierFournisseur;
	private String callbackOnValiderFournisseur;
	private String callbackOnSelectionnerFournisseur;

	private final CktlAjaxUserQuestionDelegate userQuestionDelegate;

	public FournisseurSrch(WOContext context) {
		super(context);
		userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (booleanValueForBinding(BINDING_resetSrchForm, false)) {
			resetComponent();
			clearRes();
			setSrchFouCode(null);
			setSrchSigle(null);
			setSrchEmail(null);
			unPersonneFournisseur = null;
			edc().revert();
			setValueForBinding(Boolean.FALSE, BINDING_resetSrchForm);
		}
		super.appendToResponse(response, context);
	}

	/**
	 * @return Les fournisseurs associes a la personne selectionnee.
	 */
	public NSArray getLesPersonneFournisseurs() {
		//		return (getselectedPersonne() == null ? null : EOQualifier.filteredArrayWithQualifier(getselectedPersonne().toFourniss(), new EOOrQualifier(new NSArray(new Object[] { EOFournis.QUAL_FOU_VALIDE_OUI, EOFournis.QUAL_FOU_VALIDE_INSTANCE }))));
		return (getSelectedPersonne() == null ? null : getSelectedPersonne().toFourniss());
	}

	public Boolean hasFournisseur() {
		NSArray res = getLesPersonneFournisseurs();
		return Boolean.valueOf(res != null && res.count() > 0);
	}

	public WOActionResults onSelectFournisseur() {
		setValueForBinding(unPersonneFournisseur, BINDING_unPersonneFournisseur);

		return performParentAction(getCallbackOnSelectionnerFournisseur());
	}

	public WOActionResults onCreerFournisseur() {
		//L'objet EOFournis est cree dans FournisseurFormCtrl.initialiseData 
		setValueForBinding(null, BINDING_unPersonneFournisseur);
		return performParentAction(getCallbackOnCreerFournisseur());
	}

	public WOActionResults onModifierFournisseur() {
		setValueForBinding(getSelectedPersonne(), BINDING_selectedPersonne);
		setValueForBinding(unPersonneFournisseur, BINDING_unPersonneFournisseur);
		getSelectedPersonne().setPersIdModification(getUtilisateurPersId());
		return performParentAction(getCallbackOnModifierFournisseur());
	}

	public WOActionResults onValiderFournisseur() {
		return performParentAction(getCallbackOnValiderFournisseur());
	}

	/**
	 * Cree une structure et lance la creation d'un fournisseur a partir de cette structure.
	 */
	public WOActionResults onCreerStructure() {
		//		LRLogger.debugCaller();
		//		//On cree la structure a partir des informations saisies
		//		EOStructure structure = EOStructure.creerInstance(edc(), new NSArray(EOStructureForFournisseurSpec.sharedInstance()));
		//		structure.setPersIdCreation(getUtilisateurPersId());
		//		structure.setPersIdModification(getUtilisateurPersId());				
		//		structure.setStrAffichage(MyStringCtrl.initCap(getSrchNom()));
		//		structure.setSiret(getSrchSiret());
		//		setSelectedPersonne(structure);
		//		displayGroup().insertObjectAtIndex(getSelectedPersonne(), 0);
		super.onCreerStructure();
		((EOStructure) getSelectedPersonne()).registerSpecificite(EOStructureForFournisseurSpec.sharedInstance());
		return onCreerFournisseur();
	}

	/**
	 * On ne peut pas utiliser super.onCreerIndividu() à cause du userQuestionDelegate.
	 */
	public WOActionResults onCreerIndividu() {

		boolean doIt = false;
		String nom = getSrchNom();
		String prenom = getSrchPrenom();

		NSArray res = EOIndividu.individusWithNameEqualsAndFirstNameEquals(edc(), nom, prenom, 1);

		//		System.out.println(edc().updatedObjects());

		if (res.count() > 0) {
			doIt = false;
			//FIXME PB avec trigger homonymie
			IPersonne doublon = (IPersonne) res.objectAtIndex(0);
			String s = "Une personne avec le même nom et prénom existe deja dans le référentiel : " + doublon.getNomPrenomAffichage() + " (" + doublon.getNumero()
					+ "), voulez-vous quand meme en créer une nouvelle (si la personne en question n'a pas été affichée dans la liste, vous devez peut-être changer les critères de recherche) ?";
			Boolean confirm = userQuestionDelegate.askUserAsBoolean("question1", s, getUpdateContainerID());
			if (confirm == null) {
				return null;
			}
			else {
				doIt = confirm.booleanValue();
			}
		}
		else {
			doIt = true;
		}

		if (doIt) {
			//System.out.println(edc().updatedObjects());
			EOIndividu individu = getCtrl().creerIndividu();
			setSelectedPersonne(individu);
			displayGroup().insertObjectAtIndex(getSelectedPersonne(), 0);
			displayGroup().setSelectedObject(getSelectedPersonne());
			userQuestionDelegate.clearAnswers();

			((EOIndividu) getSelectedPersonne()).registerSpecificite(EOIndividuForFournisseurSpec.sharedInstance());
			return onCreerFournisseur();
		}
		else {
			userQuestionDelegate.clearAnswers();
		}

		return null;
	}

	public Boolean isFournisseurValide() {
		return Boolean.valueOf((unPersonneFournisseur != null && EOFournis.FOU_VALIDE_OUI.equals(unPersonneFournisseur.fouValide())));
	}

	public Boolean isFournisseurSelectionnable() {
		return Boolean.valueOf((valueForBinding(BINDING_callbackOnSelectionnerFournisseur) != null && unPersonneFournisseur != null && !EOFournis.FOU_VALIDE_ANNULE.equals(unPersonneFournisseur.fouValide())));
	}

	/**
	 * @return TRUE si l'utilisateur en cours a le droit de valider un fournisseur.
	 */
	public Boolean isUserCanValiderFournis() {

		return getAppUserForAnnuaire().hasDroitValidationEOFournis(unPersonneFournisseur, AUtils.currentExercice());
		//		return ctrl.isUserCanValiderFournis();

	}

	/**
	 * @return TRUE si l'utilisateur en cours a le droit de creer un fournisseur.
	 */
	public Boolean isUserCanCreerFournis() {
		return getAppUserForAnnuaire().hasDroitCreationEOFournis(null, AUtils.currentExercice());
		//		return ctrl.isUserCanCreerFournis();
	}

	/**
	 * @return TRUE si l'utilisateur a le droit de validation des fournisseurs ou bien s'il a les droits de création et qu'il est le créateur du
	 *         fournisseur et que le fournisseru n'est pas valide.
	 */
	public Boolean isUserCanModifierFournis() {
		return (isUserCanValiderFournis() || (isUserCanCreerFournis() && unPersonneFournisseur.toValideFournis() != null && EOFournis.FOU_VALIDE_NON.equals(unPersonneFournisseur.fouValide()) && getAppUserForAnnuaire().getUtilisateur().utlOrdre().equals(
				unPersonneFournisseur.toValideFournis().valCreation())));
	}

	public EOFournis getSelectedFournis() {
		return (getSelectedPersonne() == null ? null : (getSelectedPersonne().toFourniss().count() == 0 ? null : (EOFournis) getSelectedPersonne().toFourniss().objectAtIndex(0)));
	}

	public String getCallbackOnCreerFournisseur() {
		if (callbackOnCreerFournisseur == null) {
			callbackOnCreerFournisseur = (String) valueForBinding(BINDING_callbackOnCreerFournisseur);
		}
		return callbackOnCreerFournisseur;
	}

	public String getCallbackOnModifierFournisseur() {
		if (callbackOnModifierFournisseur == null) {
			callbackOnModifierFournisseur = (String) valueForBinding(BINDING_callbackOnModifierFournisseur);
		}
		return callbackOnModifierFournisseur;
	}

	public String getCallbackOnValiderFournisseur() {
		if (callbackOnValiderFournisseur == null) {
			callbackOnValiderFournisseur = (String) valueForBinding(BINDING_callbackOnValiderFournisseur);
		}
		return callbackOnValiderFournisseur;
	}

	public String getCallbackOnSelectionnerFournisseur() {
		if (callbackOnSelectionnerFournisseur == null) {
			callbackOnSelectionnerFournisseur = (String) valueForBinding(BINDING_callbackOnSelectionnerFournisseur);
		}
		return callbackOnSelectionnerFournisseur;
	}

	public String selectionnerFournisseurLabel() {
		return (String) valueForBinding(BINDING_selectionnerFournisseurLabel);
	}

	public String onFormSrchObserverComplete() {
		return "function(oC){" + containerSrchId() + "Update();" + listeResContainerId() + "Update();" + detailResContainerId() + "Update();" + "}";
	}

	public EOQualifier getQualifierForTypeAdresse() {
		return qualifierForTypeAdresse;
	}

	public EOQualifier getQualifierForTypeTel() {
		return qualifierForTypeTel;
	}

	public FournisseurSrchCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (FournisseurSrchCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new FournisseurSrchCtrl(this);
			}
		}
		if (ctrl.getMyComponent() == null) {
			ctrl.setMyComponent(this);
		}
		return (FournisseurSrchCtrl) ctrl;
	}

	public Boolean canShowEtatCivil() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_DETAIL_SHOWNAISSANCE);
	}

	public Boolean canShowNoInsee() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_DETAIL_SHOWINSEE);
	}
}
