/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxUserQuestionDelegate;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

/**
 * Gestion de la création et suppression des Ribs liés à un fournisseur (ou client).
 * 
 * @binding fournis
 * @binding isEditing
 * @binding isReadOnly True par defaut
 * @binding isCompactView
 * @binding editingContext
 * @binding utilisateurPersId
 * @binding createFounisIfNecessary : booleen pour specifier qu'il faut créer un fournisseur s'il n'existe pas
 * @binding personneForFournisCreation : IPersonne correspondant à la personne pour laquelle on va devoir créer le fournisseur si le binding
 *          'createFounisIfNecessary' est vrai.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 */
public class RibUI extends AComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_fournis = "fournis";
	public static final String BINDING_isEditing = "isEditing";
	public static final String BINDING_isReadOnly = "isReadOnly";
	public static final String BINDING_isCompactView = "isCompactView";
	public static final String BINDING_createFounisIfNecessary = "createFounisIfNecessary";
	public static final String BINDING_personneForFournisCreation = "personneForFournisCreation";
	public static final String BINDING_selectedRib = "selectedRib";

	private EOEditingContext _editingContext;
	private EOFournis _fournis = null;

	private EORib editRib;
	private EORib selectedRib;

	public EORib unRib;
	private CktlAjaxUserQuestionDelegate userQuestionDelegate;
	private Boolean resetRibForm = Boolean.FALSE;

	public RibUI(WOContext context) {
		super(context);
		userQuestionDelegate = new CktlAjaxUserQuestionDelegate(this);
	}

	public EOEditingContext edc() {
		if (_editingContext == null) {
			if (valueForBinding(BINDING_editingContext) == null) {
				throw new RuntimeException("Le binding " + BINDING_editingContext + " est null.");
			}
			//			_editingContext = new EOEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
			_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
		}
		return _editingContext;
	}

	public EOFournis getFournis() {
		EOFournis fournis = (EOFournis) valueForBinding(BINDING_fournis);
		if (fournis == null || fournis.editingContext() == null) {
			_fournis = null;
			setSelectedRib(null);
		}
		else if (_fournis == null || _fournis.editingContext() != edc() || (_fournis.editingContext() != null && !_fournis.globalID().equals(fournis.globalID()))) {
			_fournis = fournis.localInstanceIn(edc());
			setSelectedRib(null);
		}
		return _fournis;
	}

	public EORib getEditRib() {
		return editRib;
	}

	private NSArray _getLesRibs() {
		NSArray res = new NSArray();
		if (getFournis() != null) {
			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(getFournis().toRibsNonAnnules(), new NSArray(new Object[] {
					EORib.SORT_RIB_VALIDE_DESC, EORib.SORT_DATE_CREATION_DESC
			}));
		}
		return res;
	}

	public NSArray getLesRibs() {
		NSArray res = _getLesRibs();
		if (res.count() > 0 && (getSelectedRib() == null || res.indexOfObject(getSelectedRib()) == NSArray.NotFound)) {
			setSelectedRib((EORib) res.objectAtIndex(0));
		}
		if (res.count() == 0) {
			setSelectedRib(null);
		}
		return res;
	}

	public EORib getSelectedRib() {
		return selectedRib;
	}

	public void setSelectedRib(EORib rib) {
		selectedRib = rib;
		if (hasBinding(BINDING_selectedRib)) {
			setValueForBinding(rib, BINDING_selectedRib);
		}
	}

	public Boolean isRibSelected() {
		return Boolean.valueOf(getSelectedRib() != null);
	}

	/**
	 * Rafraichit le fournisseur à partir des valeurs du parent.
	 * @param fournis le fournisseur a rafraichir 
	 */
	private void rafraichirFournis(EOFournis fournis) {
	    if (fournis != null && fournis.editingContext() != null) {
	        fournis.editingContext().refaultObject(fournis);
	    }
	}
	
	public WOActionResults onRibCreer() {
		EOEditingContext edForRib = edc();
		try {
			edForRib.revert();
			setIsEditing(Boolean.TRUE);
			//On cree les nveaux objets 
			editRib = (EORib.creerInstance(edForRib));

			if (getFournis() != null) {
				editRib.setRibTitco(getFournis().getNomComplet());
			}
			else if (booleanValueForBinding(BINDING_createFounisIfNecessary, false) && hasBinding(BINDING_personneForFournisCreation)) {
				editRib.setRibTitco(((IPersonne) valueForBinding(BINDING_personneForFournisCreation)).getNomCompletAffichage());
			}

			editRib.setPersIdCreation(getUtilisateurPersId());
			editRib.setPersIdModification(getUtilisateurPersId());
			setResetRibForm(Boolean.TRUE);
		} catch (Exception e) {
			edForRib.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	/**
	 * Supprime le rib en cours (le passe a l'etat erchive si le rib existe deja, sinon le supprime s'il n'a pas deja ete enregistre dans la base).
	 */
	public WOActionResults onRibSupprimer() {
		EOEditingContext edForRib = edc();
		try {
			edForRib.revert();
			if (getSelectedRib() != null) {
				getSelectedRib().localInstanceIn(edForRib).supprimer(getUtilisateurPersId());
				//LRLogger.debug(edForRib);
				edForRib.saveChanges();
				setSelectedRib(null);
				_fournis = null;
			}
		} catch (Exception e) {
			edForRib.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	/**
	 * Annule les modifications sur le rib.
	 * 
	 * @return null
	 */
	public WOActionResults onRibAnnuler() {
		EOEditingContext edForRib = edc();
		try {
			setErreurSaisieMessage(null);
			editRib = null;
			setSelectedRib(null);
			setIsEditing(Boolean.FALSE);
			edForRib.revert();
			_fournis = null;
			//edForRib.dispose();
			_editingContext = null;
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onRibEnregistrerSansConfirmation() {
		EOEditingContext edForRib = edc();
        rafraichirFournis(getFournis());
		setErreurSaisieMessage(null);
		try {
			if (editRib != null) {
				//LRLogger.verbose("editRib", editRib.editingContext());
				if (editRib.toFournis() == null) {
					editRib.setToFournisRelationship(getFournis());
				}
				// push dans l'ec parent
				edForRib.saveChanges();
			}
			setSelectedRib(editRib);
			setIsEditing(Boolean.FALSE);

		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForRib.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	public WOActionResults onRibEnregistrer() {
		EOEditingContext edForRib = edc();
//        rafraichirFournis(getFournis());
		setErreurSaisieMessage(null);
		EOFournis fournisNew = null;
		boolean doIt = false;
		try {
			if (editRib != null) {
				if (editRib.toFournis() == null) {
					if (getFournis() != null) {
						rafraichirFournis(getFournis());
						editRib.setToFournisRelationship(getFournis());
					}
					else {
						// Si le fournisseur doit être créé on le crée
						if (booleanValueForBinding(BINDING_createFounisIfNecessary, false)
								&& hasBinding(BINDING_personneForFournisCreation)) {
							fournisNew = createNewFournisseurForIndividu();
							editRib.setToFournisRelationship(fournisNew);
						}
					}
				}

				//Verifier que le rib est complet
				if (editRib.isCompteEtIbanEmpty()) {
					throw new NSValidation.ValidationException(EORib.EXCEPTION_RIB_OU_IBAN);
				}
				//editRib.checkRibComplet();

				//chercher des doublons
				NSArray<EORib> doublons = NSArray.emptyArray();
				if (editRib.isRibFRRempli()) {
					//				if (!editRib.isRib9999()) {
					doublons = EORib.findDoublonsFr(editRib.editingContext(), editRib);
				}
				if (doublons.count() == 0 && editRib.isRibIbanRempli()) {
					doublons = EORib.findDoublonsIban(editRib.editingContext(), editRib);
				}

				if (doublons.count() > 0) {
					doIt = false;
					EORib ribDoublon = (EORib) doublons.objectAtIndex(0);
					String s = "Le rib " + editRib.getRibConcatene() + " existe deja et est affecte au fournisseur " + ribDoublon.toFournis().getNomComplet() + ", voulez-vous quand meme affecter celui-ci ?";
					Boolean confirm = userQuestionDelegate.askUserAsBoolean("question1", s, getMainContainerId());
					if (confirm == null) {
						return null;
					}
					else {
						doIt = confirm.booleanValue();
					}
				}
				else {
					doIt = true;
				}

				if (doIt) {
					//initialisation du fournisseur cree pour l'occaz
					if (booleanValueForBinding(BINDING_createFounisIfNecessary, false)
							&& hasBinding(BINDING_personneForFournisCreation)) {
						prepareFournis(editRib.toFournis());
					}
					editRib.validateObjectMetier();
					// push dans l'ec parent
					edForRib.saveChanges();
					setSelectedRib(editRib);
					setIsEditing(Boolean.FALSE);
				}
				userQuestionDelegate.clearAnswers();
			}

		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForRib.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		return null;
	}

	private EOFournis createNewFournisseurForIndividu() {
		EOFournis fournis = EOFournis.creerInstance(edc());
		initFournis(fournis);
		return fournis;
	}

	private void prepareFournis(EOFournis fournis) throws Exception {
		IPersonne personne = getPersonneForFournisCreation();

		NSArray<EORepartPersonneAdresse> repartsFacturation =
				ERXQ.filtered(personne.toRepartPersonneAdresses(),
						ERXQ.equals(EORepartPersonneAdresse.TADR_CODE_KEY, EOTypeAdresse.TADR_CODE_FACT));
		if (repartsFacturation != null && !repartsFacturation.isEmpty())
			fournis.setToAdresseRelationship(repartsFacturation.lastObject().toAdresse().localInstanceIn(fournis.editingContext()));
		// Si le compte est null, on le cree (si demandé)
		try {
			//            if (getAppUserForAnnuaire().peutValiderFournisseur()) {
			//                if (getSelectedCompteForFournis() == null && creerCompteAuto()) {
			//                    getSelectedFournisseur().associerCompte(userPersId);
			//                }
			//            }
			fournis.initialise(
					getUtilisateurPersId(),
					(repartsFacturation.lastObject() == null ? null : repartsFacturation.lastObject().toAdresse()),
					fournis.isEtranger(),
					(MyStringCtrl.isEmpty(fournis.fouType()) ? fournis.fouType() : EOFournis.FOU_TYPE_FOURNISSEUR)
					);
		} catch (Exception e) {
			throw new NSForwardException(e, "Une erreur est survenue lors de la création du fournisseur associé à ce RIB.");
		}

	}

	private IPersonne getPersonneForFournisCreation() {
		IPersonne personne = (IPersonne) valueForBinding(BINDING_personneForFournisCreation);
		return personne.localInstanceIn(edc());
	}

	private void initFournis(EOFournis fournis) {
		IPersonne personne = getPersonneForFournisCreation();
		fournis.setToPersonne(personne);
		if (personne instanceof EOIndividu) {
			((EOIndividu) personne).registerSpecificite(EOIndividuForFournisseurSpec.sharedInstance());
		}
		if (personne instanceof EOStructure) {
			EOStructure structure = (EOStructure) personne;
			structure.registerSpecificite(EOStructureForFournisseurSpec.sharedInstance());
		}
		try {
			fournis.initialise(getUtilisateurPersId(), null, false, EOFournis.FOU_TYPE_FOURNISSEUR);
		} catch (Exception e) {
			throw new NSForwardException(e);
		}
	}

	public WOActionResults onSelectRib() {
		return null;
	}

	public Boolean showPopupRibs() {
		return Boolean.valueOf(getLesRibs().count() > 0);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public Boolean isRibVueFormulaire() {
		return isEditing();
	}

	public Boolean isRibCreerEnabled() {
		return getAppUserForAnnuaire().hasDroitCreationEORib(null);
	}

	public Boolean isRibSupprimerEnabled() {
		return Boolean.valueOf((isRibSelected().booleanValue() && !getSelectedRib().isRibControleParPaye() && getAppUserForAnnuaire().hasDroitModificationEORib(getSelectedRib())));
	}

	//	/**
	//	 * Selectionne le rib le plus recent a partir de la liste des ribs.
	//	 */
	//	private void initialiseSelectedRib() {
	//		if (lesRibs != null && lesRibs.count() > 0) {
	//			setSelectedRib((EORib) lesRibs.objectAtIndex(0));
	//			setSelectedRib((EORib) EOSortOrdering.sortedArrayUsingKeyOrderArray(lesRibs, new NSArray(new Object[] { EORib.SORT_DATE_CREATION_DESC })).objectAtIndex(0));
	//		} else {
	//			setSelectedRib(null);
	//		}
	//	}	

	public Boolean isCompactView() {
		return (Boolean) valueForBinding(BINDING_isCompactView);
	}

	public Boolean getResetRibForm() {
		return resetRibForm;
	}

	public void setResetRibForm(Boolean resetRibForm) {
		this.resetRibForm = resetRibForm;
	}

}
