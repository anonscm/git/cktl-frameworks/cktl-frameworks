/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneSrchCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */
public class FournisseurSrchCtrl extends PersonneSrchCtrl {

	public FournisseurSrchCtrl(AComponent component) {
		super(component);
	}

	public FournisseurSrchCtrl() {
		super();
	}

	/**
	 * @return Le résultat de la recherche. Methode a surcharger si vous voulez specialiser la recherche.
	 */
	@Override
	public NSArray getResultats() {
		NSArray res = NSArray.EmptyArray;
		if (getMyComponent().getPersTypeMoraleSelected().booleanValue() && getMyComponent().getPersTypeExterneSelected().booleanValue()) {


			if (!((getMyComponent().getSrchNom() == null || getMyComponent().getSrchNom().length() == 0) 
					&& (getMyComponent().getSrchSiret() == null || getMyComponent().getSrchSiret().length() == 0))) {
				res = EOStructure.structuresByNameAndSiret(edc(), getMyComponent().getSrchNom(), 
						getMyComponent().getSrchSiret(), EOStructure.QUAL_STRUCTURES_TYPE_AUTRES, 
						getMyComponent().getFetchLimit().intValue());
			}
			
			res = EOStructure.filtrerLesStructuresNonAffichables(res, edc(), getAppUserForAnnuaire().getPersId());
			
		} else {
			if (!(getMyComponent().getSrchNom() == null || getMyComponent().getSrchNom().length() == 0)) {
				res = EOIndividu.individusByNameAndFirstname(edc(), getMyComponent().getSrchNom(), 
						getMyComponent().getSrchPrenom(), null, getMyComponent().getFetchLimit().intValue());
			}
			if (getMyComponent().getSrchNom() == null && !MyStringCtrl.isEmpty(getMyComponent().getSrchPrenom())) {
				res = EOIndividu.individusWithFirstNameLike(edc(), getMyComponent().getSrchPrenom(), null, getMyComponent().getFetchLimit().intValue());
			}
		}

		return res;
	}
}
