/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.AComponentCtrl;

/**
 * Classe abstraite pour les controleurs qui s'occupe de la creation de
 * fournisseurs. Fourni des methodes pour indiquer si l'utilisateur a les droits
 * de creer / valider un fournisseur.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */

public abstract class AFournisComponentCtrl extends AComponentCtrl {

	private Boolean userCanCreerFournis;
	private Boolean userCanValiderFournis;

	public AFournisComponentCtrl(AComponent component) {
		super(component);
	}

	public Boolean isUserCanCreerFournis() {
		if (userCanCreerFournis == null) {
			initialiseDroitsUtilisateur();
		}		
		return userCanCreerFournis;
	}
	


	public void setUserCanCreerFournis(boolean userCanCreerFournis) {
		this.userCanCreerFournis = userCanCreerFournis;
	}

	public Boolean isUserCanValiderFournis() {
		if (userCanValiderFournis == null) {
			initialiseDroitsUtilisateur();
		}
		return userCanValiderFournis;
	}

	public void setUserCanValiderFournis(Boolean userCanValiderFournis) {
		this.userCanValiderFournis = userCanValiderFournis;
	}

	public void initialiseDroitsUtilisateur() {
		userCanCreerFournis = getMyComponent().getAppUserForAnnuaire().hasDroitCreationEOFournis(null, AUtils.currentExercice());
//		userCanValiderFournis =   EOFournis.verifierDroitValidationFournisseur(getMyComponent().getAppUserForAnnuaire());
		userCanValiderFournis = getMyComponent().getAppUserForAnnuaire().hasDroitValidationEOFournis(null, AUtils.currentExercice());;
	}
	
	
}
