/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.controleurs;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOExercice;
import org.cocktail.fwkcktldroitsutils.common.metier.EOJefyAdminParametre;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.MyCRIMailBus;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.MyCRIMailMessage;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.PersonneFormCtrl;
import org.cocktail.fwkcktlpersonneguiajax.serveur.fournis.components.FournisseurForm;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * @author Rodolphe PRIN <rodolphe.prin at univ-lr.fr>
 */
public class FournisseurFormCtrl extends PersonneFormCtrl {
	public final static Logger logger = Logger.getLogger(FournisseurFormCtrl.class);
	//	public EORepartPersonneAdresse selectedRepartPersonneAdresse;

	private Boolean creerCompteFournisseurAuto;
	private NSArray vlansAutorisesPourCreationCompte;
	private EOCompte selectedCompte;
	public String previousFouValide = null;
	private EOFournis leFournis;

	public FournisseurFormCtrl(FournisseurForm component) {
		super(component);
	}

	public FournisseurFormCtrl() {
		super();
	}

	public EOFournis getLeFournis() {
		return leFournis;
	}

	public void setLeFournis(EOFournis leFournis) {
		this.leFournis = leFournis;
	}

	public FournisseurForm getMyComponent() {
		return (FournisseurForm) super.getMyComponent();
	}

	public void prepareDataForSave() throws Exception {
		super.prepareDataForSave();
		Integer persIdForUser = getMyComponent().getAppUserForAnnuaire().getUtilisateur().toPersonne().persId();
		if (getMyComponent().isRibEditing().booleanValue()) {
			throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les ribs avant de poursuivre.");
		}

		EORepartPersonneAdresse repartPersonneAdresse = getSelectedRepartPersonneAdresse();
		if (repartPersonneAdresse != null) {
			repartPersonneAdresse = repartPersonneAdresse.localInstanceIn(edc());
		}
		getLeFournis().initialise(persIdForUser, (repartPersonneAdresse == null ? null : repartPersonneAdresse.toAdresse()), getMyComponent().getIsEtranger().booleanValue(), getLeFournis().fouType());

		try {
			getLeFournis().checkAdresse();
		} catch (Exception e) {
			throw new Exception("Une adresse de type FACTURATION doit être affectée au fournisseur (c'est son adresse par défaut). Veuillez en sélectionner une dans la liste.");
		}

		if (showCreationCompte().booleanValue()) {
			if (getSelectedCompte() != null) {
				getLeFournis().setToCompteRelationship(getSelectedCompte().localInstanceIn(edc()));
			}

			//Si le compte est null, on le cree (si demandé)
			if (getLeFournis().toCompte() == null && (getCreerCompteFournisseurAuto().booleanValue())) {
				getLeFournis().associerCompte(persIdForUser);
			}
		}
		getLeFournis().willChange();

	}

	//	public boolean onEnregistrer() throws Exception {
	//		try {
	//			Integer persIdForUser = getMyComponent().getAppUserForAnnuaire().getUtilisateur().toPersonne().persId();
	//			if (myView.isRibEditing().booleanValue()) {
	//				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les ribs avant de poursuivre.");
	//			}
	//
	//			EORepartPersonneAdresse repartPersonneAdresse = getSelectedRepartPersonneAdresse();
	//			if (repartPersonneAdresse != null) {
	//				repartPersonneAdresse = repartPersonneAdresse.localInstanceIn(edc());
	//			}
	//			getLeFournis().initialise(persIdForUser, (repartPersonneAdresse == null ? null : repartPersonneAdresse.toAdresse()), myView.getIsEtranger().booleanValue(), getLeFournis().fouType());
	//			
	//			try {
	//				getLeFournis().checkAdresse();
	//			}
	//			catch (Exception e) {
	//				throw new Exception("Une adresse de type FACTURATION doit être affectée au fournisseur (c'est son adresse par défaut). Veuillez en sélectionner une dans la liste.");
	//			}			
	//						
	//			
	//			if (showCreationCompte().booleanValue()) {
	//				if (getSelectedCompte() != null) {
	//					getLeFournis().setToCompteRelationship(getSelectedCompte().localInstanceIn(edc()));
	//				}
	//
	//				//Si le compte est null, on le cree (si demandé)
	//				if (getLeFournis().toCompte() == null && (getCreerCompteFournisseurAuto().booleanValue())) {
	//					getLeFournis().associerCompte(persIdForUser);
	//				}
	//			}
	//
	//			//
	//			LRLogger.debug("+++++++++++++++++++++++");
	//		//	LRLogger.verbose(edc());
	//
	//			//On force la validation des objets
	//			getLeFournis().willChange();
	//			getLeFournis().toPersonne().willChange();
	//
	//			edc().saveChanges();
	//			LRLogger.debug("ENREGISTREMENT EFFECTUE ");
	//			return true;
	//		} catch (Exception e) {
	//			throw e;
	//		}
	//
	//	}
	//	public boolean onEnregistrer() throws Exception {
	//		try {
	//			Integer persIdForUser = getMyComponent().getAppUserForAnnuaire().getUtilisateur().toPersonne().persId();
	//			if (myView.isAdresseEditing().booleanValue()) {
	//				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les adresses avant de poursuivre.");
	//			}
	//			if (myView.isRibEditing().booleanValue()) {
	//				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les ribs avant de poursuivre.");
	//			}
	//			if (myView.isGroupeEditing().booleanValue()) {
	//				throw new NSValidation.ValidationException("Veuillez enregistrer ou annuler les modifications sur les groupes avant de poursuivre.");
	//			}
	//			
	//			myView.setErreurSaisieMessage(null);
	//			
	//			EORepartPersonneAdresse repartPersonneAdresse = getSelectedRepartPersonneAdresse();
	//			if (repartPersonneAdresse != null) {
	//				repartPersonneAdresse = repartPersonneAdresse.localInstanceIn(edc());
	//			}
	//			
	//			
	//			getLeFournis().initialise(persIdForUser, (repartPersonneAdresse == null ? null : repartPersonneAdresse.toAdresse()), myView.getIsEtranger().booleanValue(), getLeFournis().fouType());
	//			
	//			try {
	//				getLeFournis().checkAdresse();
	//			}
	//			catch (Exception e) {
	//				throw new Exception("Une adresse de type FACTURATION doit être affectée au fournisseur (c'est son adresse par défaut). Veuillez en sélectionner une dans la liste.");
	//			}			
	//			
	//			
	//			if (showCreationCompte().booleanValue()) {
	//				if (getSelectedCompte() != null) {
	//					getLeFournis().setToCompteRelationship(getSelectedCompte().localInstanceIn(edc()));
	//				}
	//				
	//				//Si le compte est null, on le cree (si demandé)
	//				if (getLeFournis().toCompte() == null && (getCreerCompteFournisseurAuto().booleanValue())) {
	//					getLeFournis().associerCompte(persIdForUser);
	//				}
	//			}
	//			
	//			//
	//			LRLogger.debug("+++++++++++++++++++++++");
	//			//	LRLogger.verbose(edc());
	//			
	//			//On force la validation des objets
	//			getLeFournis().willChange();
	//			getLeFournis().toPersonne().willChange();
	//			
	//			edc().saveChanges();
	//			LRLogger.debug("ENREGISTREMENT EFFECTUE ");
	//			return true;
	//		} catch (Exception e) {
	//			throw e;
	//		}
	//		
	//	}

	/**
	 * Nettoie les donnees pour que la page soit bien reinitialisee entre deux affichages.
	 */
	public void nettoieDonnees() {
		super.nettoieDonnees();
		setSelectedRepartPersonneAdresse(null);
		setSelectedCompte(null);
		setLeFournis(null);
	}

	/**
	 * Initialise les données avant affichage du formulaire pour une structure.
	 * 
	 * @param personne la structure ou l'individu
	 * @param fournis L'objet EOFournis associe a la structure, ou bien null (dans ce cas un nouvel objet sera cree)
	 */
	public void initialiseData(IPersonne personne, EOFournis fournis) {
		//LRLogger.debugCaller();
		super.initialiseData(personne);

		if (fournis == null) {
			fournis = EOFournis.creerInstance(edc());
		}
		setLeFournis(fournis);
		getLeFournis().setValidationEditingContext(edc());

		//memoriser la validite
		previousFouValide = getLeFournis().fouValide();
		getLeFournis().setToPersonne(personne);
		if (personne instanceof EOStructure) {
			EOStructure structure = (EOStructure) personne;
			structure.registerSpecificite(EOStructureForFournisseurSpec.sharedInstance());
		}
		else if (personne instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu) personne;
		}

		if (getLeFournis().toAdresse() != null) {
			EOQualifier qual = new EOKeyValueQualifier(EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, getLeFournis().toAdresse());
			EOQualifier qual2 = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_FACT);
			NSArray res = getLeFournis().toPersonne().toRepartPersonneAdresses(new EOAndQualifier(new NSArray(new Object[] {
					qual, qual2
			})));
			if (res.count() > 0) {
				setSelectedRepartPersonneAdresse((EORepartPersonneAdresse) res.objectAtIndex(0));
			}
		}

		if (getLeFournis().toCompte() != null) {
			setSelectedCompte(getLeFournis().toCompte());
		}

		if (getLeFournis().toValideFournis() != null) {
			getLeFournis().toValideFournis().fixCreateur(getMyComponent().getAppUserForAnnuaire());
			//			getLeFournis().toValideFournis().fixValidateur(getMyComponent().getAppUserForAnnuaire());
		}
	}

	//	public void initialiseDonnees(IPersonne personne, EOFournis fournis) {
	//		nettoieDonnees();
	//		System.out.println("FournisseurFormCtrl.initialiseDonnees()" + edc());
	//		System.out.println("FournisseurFormCtrl.initialiseDonnees()" + personne);
	//		System.out.println("FournisseurFormCtrl.initialiseDonnees()" + fournis);
	//		
	//		if (fournis == null) {
	//			fournis = EOFournis.creerInstance(edc());
	//		}
	//		fournis.setValidationEditingContext(edc());
	//		setFournis(fournis);
	//		
	//		//memoriser la validite
	//		previousFouValide = fournis.fouValide();
	//		if (personne instanceof EOStructure) {
	//			EOStructure structure = (EOStructure) personne;
	//			structure.setValidationEditingContext(edc());
	//			
	//			structure.registerSpecificite(EOStructureForFournisseurSpec.sharedInstance());
	//			fournis.setToStructure((EOStructure) personne);
	////			myView.filtNaf = (getLeFournis().toStructure().toNaf() != null ? getLeFournis().toStructure().toNaf().cNafEtLib() : null);
	////			if (getLeFournis().toStructure().cNaf() != null) {
	////				setSelectedNaf(EONaf.getNaf(edc(), MyDateCtrl.now(), getLeFournis().toStructure().cNaf()));
	////			}
	//		} else if (personne instanceof EOIndividu) {
	//			EOIndividu individu = (EOIndividu) personne;
	//			individu.setValidationEditingContext(edc());
	//			getLeFournis().setToIndividu(individu);
	//			if (getLeFournis().toIndividu().toCivilite() == null) {
	//				getLeFournis().toIndividu().setToCiviliteRelationship((EOCivilite) getListCivilites().objectAtIndex(0));
	//			}
	//		}
	//		
	//		myView.setErreurSaisieMessage(null);
	//		
	//		if (getLeFournis().toAdresse() != null) {
	//			EOQualifier qual = new EOKeyValueQualifier(EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, getLeFournis().toAdresse());
	//			EOQualifier qual2 = new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeAdresse.TADR_CODE_FACT);
	//			NSArray res = getLeFournis().toPersonne().toRepartPersonneAdresses(new EOAndQualifier(new NSArray(new Object[] { qual, qual2 })));
	//			if (res.count() > 0) {
	//				setSelectedRepartPersonneAdresse((EORepartPersonneAdresse) res.objectAtIndex(0));
	//			}
	//		}
	//		
	//		if (getLeFournis().toCompte() != null) {
	//			setSelectedCompte(getLeFournis().toCompte());
	//		}
	//		
	//		if (getLeFournis().toValideFournis() != null) {
	//			getLeFournis().toValideFournis().fixCreateur(getMyComponent().getAppUserForAnnuaire());
	//			getLeFournis().toValideFournis().fixCreateur(getMyComponent().getAppUserForAnnuaire());
	//		}
	//	}

	//	/**
	//	 * @return TRUE si on est sur la creation d'un fournisseur Individu.
	//	 */
	//	public Boolean isIndividu() {
	//		return Boolean.valueOf(myView.getLaPersonne() instanceof EOIndividu);
	//	}

	//	public EORepartPersonneAdresse getSelectedRepartPersonneAdresse() {
	//		return selectedRepartPersonneAdresse;
	//	}
	//
	//	public void setSelectedNaf(EONaf selectedNaf) {
	//		this.selectedNaf = selectedNaf;
	//		getLeFournis().toStructure().setToNafRelationship(selectedNaf);
	//		myView.filtNaf = selectedNaf.cNafEtLib();
	//	}
	//
	//	public EONaf getSelectedNaf() {
	//		return selectedNaf;
	//	}

	//	public void setFournis(EOFournis fournis) {
	//		getMyComponent().setLeFournis(fournis);
	//	}

	//	public void setSelectedCivilite(EOCivilite selectedCivilite) {
	//		getLeFournis().toIndividu().setToCiviliteRelationship(selectedCivilite);
	//		//		getIndividu().setToCiviliteRelationship(selectedCivilite);
	//
	//	}

	public Boolean getCreerCompteFournisseurAuto() {
		if (creerCompteFournisseurAuto == null) {
			setCreerCompteFournisseurAuto(Boolean.valueOf(EOGrhumParametres.OUI.equals(EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_ANNUAIRE_FOU_COMPTE_AUTO))));
		}
		return creerCompteFournisseurAuto;
	}

	public void setCreerCompteFournisseurAuto(Boolean creerCompteFournisseurAuto) {
		this.creerCompteFournisseurAuto = creerCompteFournisseurAuto;
	}

	public Boolean showCreationCompte() {
		return Boolean.valueOf(isUserCanValiderFournis());
	}

	public EOTypeAdresse getTypeAdresseFact(EOEditingContext ec) {
		return EOTypeAdresse.fetchByKeyValue(ec, EOTypeAdresse.TADR_CODE_KEY, EOTypeAdresse.TADR_CODE_FACT);
	}

	//	public void setSelectedRepartPersonneAdresse(EORepartPersonneAdresse repartPersonneAdresse) {
	//		selectedRepartPersonneAdresse = repartPersonneAdresse;
	//	}

	public EOCompte getSelectedCompte() {
		//		return getLeFournis().toCompte();
		return selectedCompte;
	}

	public void setSelectedCompte(EOCompte selectedCompte) {
		this.selectedCompte = selectedCompte;

		//		EOCompte rpa = selectedCompte;
		//		if (rpa != null && !edc().equals(rpa.editingContext()) ) {
		//			rpa = rpa.localInstanceIn(edc());
		//		}
		//		getLeFournis().setToCompteRelationship(rpa);		
	}

	public NSArray vlansAutorisesPourCreationCompte() {
		if (vlansAutorisesPourCreationCompte == null) {
			vlansAutorisesPourCreationCompte = new NSArray(new Object[] {
					EOVlans.fetchByKeyValue(edc(), EOVlans.C_VLAN_KEY, EOVlans.VLAN_X)
			});
		}
		return vlansAutorisesPourCreationCompte;
	}

	/**
	 * Envoie un mail informant de la creation d'un fournisseur et demandant sa validation.
	 * 
	 * @param leFournis
	 * @throws Exception
	 */
	public void sendMailDemandeValidationToValideurs(EOFournis leFournis) throws Exception {
		boolean proceed = false;
		boolean envoiMailClientDisabled = FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.CLIENT_DEMANDEVALIDATION_MAIL_DISABLED);
		boolean envoiMailFournisseurDisabled = FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.FOURNISSEUR_DEMANDEVALIDATION_MAIL_DISABLED);
		if (leFournis.isTypeClient()) {
			proceed = !envoiMailClientDisabled;
		}
		else if (leFournis.isTypeFournisseur()) {
			proceed = !envoiMailFournisseurDisabled;
		}
		else {
			proceed = !(envoiMailClientDisabled && envoiMailFournisseurDisabled);
		}
		if (proceed) {
			try {
				MyCRIMailMessage msg = buildDemandeValidationMail(leFournis);

				new MyCRIMailBus(((CktlWebApplication) CktlWebApplication.application()).config()).sendMail(
						msg.getMessage().getFrom()[0].toString(),
						MyCRIMailMessage.mergeAdress(msg.getAddrsTO(), ","),
						MyCRIMailMessage.mergeAdress(msg.getAddrsCC(), ","),
						msg.getMessage().getSubject(),
						(String) msg.getMessage().getContent());
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Le courriel de demande de validation n'a "
						+ "pu etre envoye en raison de l'erreur suivante : \n"
						+ e.getMessage());
			}
		}
	}

	public void sendMailInformationValidation(EOFournis leFournis) throws Exception {
		try {
			MyCRIMailMessage msg = buildInformationValidation(leFournis);
			new MyCRIMailBus(((CktlWebApplication) CktlWebApplication.application()).config()).sendMail(
					msg.getMessage().getFrom()[0].toString(),
					MyCRIMailMessage.mergeAdress(msg.getAddrsTO(), ","),
					MyCRIMailMessage.mergeAdress(msg.getAddrsCC(), ","),
					msg.getMessage().getSubject(),
					(String) msg.getMessage().getContent());
		} catch (Exception e) {
			throw new Exception("Le courriel d'information de validation n'a "
					+ "pu etre envoye en raison de l'erreur suivante : \n"
					+ e.getMessage(), e);
		}
	}

	/**
	 * @return Un message email pret a etre envoye informant les destinataires de l'adresse indiquee dans le parametre de JefyAdmin
	 *         EMAIL_VALIDFOURNIS, demandant la validation du fournisseur, emis par l'utilisateur en cours.
	 */
	public MyCRIMailMessage buildDemandeValidationMail(EOFournis fournis) throws Exception {
		//Recuperer l'adresse des destinataires
		String exer = new SimpleDateFormat("yyyy").format(new Date());
		EOExercice exercice =
				EOExercice.getExercice(fournis.editingContext(), Integer.valueOf(exer));
		if (exercice == null) {
			throw new NullPointerException("Aucun exercice defini pour " + exer);
		}
		EOJefyAdminParametre param =
				EOJefyAdminParametre.getParametre(fournis.editingContext(),
						EOFournis.EMAIL_VALIDFOURNIS_PARAMKEY, exercice);
		if (param == null) {
			logger.warn("Parametre JEFY_ADMIN "
					+ EOFournis.EMAIL_VALIDFOURNIS_PARAMKEY
					+ " non defini pour " + exer + ". Aucun mail ne sera envoyé pour demande de validation.");

		}

		String hostmail = ((CktlWebApplication) CktlWebApplication.application()).config().stringForKey(EOFournis.GRHUM_HOST_MAIL_PARAMKEY);
		//				EOGrhumParametres.parametrePourCle(fournis.editingContext(),
		//						EOFournis.GRHUM_HOST_MAIL_PARAMKEY);
		if (hostmail == null) {
			throw new NullPointerException("Parametre GRHUM "
					+ EOFournis.GRHUM_HOST_MAIL_PARAMKEY + " non defini.");
		}

		String to = param.parValue();
		//		String from = fournis.toValideFournis().getCreateurAsUtilisateur().getEmail();
		String from = EOCompte.compteForPersId(fournis.editingContext(),
				fournis.toValideFournis().getCreateurAsUtilisateur().toPersonne().persId()).toCompteEmail().getEmailFormatte();

		String reply = from;
		String cc = from;
		String subject = "Demande de validation du fournisseur " + fournis.getNomComplet();
		String body = fournis.toValideFournis().getCreateurAsUtilisateur().getPrenomAndNom() + " demande la validation du fournisseur suivant\n" + fournis.getNomComplet() + " (code : " + fournis.fouCode() + ").";

		MyCRIMailMessage tmp = new MyCRIMailMessage(hostmail);
		tmp.initMessage(from, to, subject, body);
		tmp.addCC(cc);
		tmp.setReplyTo(reply);
		return tmp;

	}

	/**
	 * @return Un message email pret a etre envoye informant le createur du fournisseur que celui-ci a ete valide ou annule.
	 * @throws Exception
	 */
	public MyCRIMailMessage buildInformationValidation(EOFournis fournis) throws Exception {
		//Recuperer l'adresse des destinataires
		String exer = new SimpleDateFormat("yyyy").format(new Date());
		//		String exer = "2010";
		EOExercice exercice = EOExercice.getExercice(fournis.editingContext(), Integer.valueOf(exer));
		if (exercice == null) {
			throw new NullPointerException("Aucun exercice defini pour " + exer);
		}
		EOJefyAdminParametre param = EOJefyAdminParametre.getParametre(fournis.editingContext(), EOFournis.EMAIL_VALIDFOURNIS_PARAMKEY, exercice);
		if (param == null) {
			throw new NullPointerException("Parametre JEFY_ADMIN " + EOFournis.EMAIL_VALIDFOURNIS_PARAMKEY + " non defini pour " + exer);
		}

		String hostmail = EOGrhumParametres.parametrePourCle(fournis.editingContext(), EOFournis.GRHUM_HOST_MAIL_PARAMKEY);
		if (hostmail == null) {
			throw new NullPointerException("Parametre GRHUM " + EOFournis.GRHUM_HOST_MAIL_PARAMKEY + " non defini.");
		}

		//		String to = fournis.toValideFournis().getCreateurAsUtilisateur().getEmail();
		String to = EOCompte.compteForPersId(fournis.editingContext(),
				fournis.toValideFournis().getCreateurAsUtilisateur().toPersonne().persId()).toCompteEmail().getEmailFormatte();

		//		String from = fournis.toValideFournis().getValideurAsUtilisateur().getEmail();
		String from = EOCompte.compteForPersId(fournis.editingContext(),
				fournis.toValideFournis().getValideurAsUtilisateur().toPersonne().persId()).toCompteEmail().getEmailFormatte();

		String reply = from;
		//String cc = from;
		String subject = null;
		String body = null;
		if (EOFournis.FOU_VALIDE_OUI.equals(fournis.fouValide())) {
			subject = "Fournisseur validé : " + fournis.getNomComplet();
			body = fournis.toValideFournis().getValideurAsUtilisateur().getPrenomAndNom()
					+ " a validé le fournisseur suivant : \n"
					+ fournis.getNomComplet()
					+ " (code : " + fournis.fouCode() + ").";
		}
		else if (EOFournis.FOU_VALIDE_ANNULE.equals(fournis.fouValide())) {
			subject = "Fournisseur annulé : " + fournis.getNomComplet();
			body = fournis.toValideFournis().getValideurAsUtilisateur().getPrenomAndNom()
					+ " a annulé le fournisseur suivant : \n"
					+ fournis.getNomComplet()
					+ " (code : " + fournis.fouCode() + ").";
		}
		else if (EOFournis.FOU_VALIDE_NON.equals(fournis.fouValide())) {
			subject = "Fournisseur passé en cours de validation : "
					+ fournis.getNomComplet();
			body = fournis.toValideFournis().getValideurAsUtilisateur().getPrenomAndNom()
					+ " a passé dans l'état \"en cours de validation\" "
					+ "le fournisseur suivant : \n" + fournis.getNomComplet()
					+ " (code : " + fournis.fouCode() + ").";
		}
		if (subject != null) {
			MyCRIMailMessage tmp = new MyCRIMailMessage(hostmail);
			tmp.initMessage(from, to, subject, body);
			tmp.addCC(param.parValue());
			tmp.setReplyTo(reply);
			return tmp;
		}
		return null;
	}

	private Boolean userCanCreerFournis;
	private Boolean userCanValiderFournis;

	public Boolean isUserCanCreerFournis() {
		if (userCanCreerFournis == null) {
			initialiseDroitsUtilisateur();
		}
		return userCanCreerFournis;
	}

	public void setUserCanCreerFournis(boolean userCanCreerFournis) {
		this.userCanCreerFournis = userCanCreerFournis;
	}

	public Boolean isUserCanValiderFournis() {
		if (userCanValiderFournis == null) {
			initialiseDroitsUtilisateur();
		}
		return userCanValiderFournis;
	}

	public void setUserCanValiderFournis(Boolean userCanValiderFournis) {
		this.userCanValiderFournis = userCanValiderFournis;
	}

	public void initialiseDroitsUtilisateur() {
		userCanCreerFournis = getMyComponent().getAppUserForAnnuaire().hasDroitCreationEOFournis(null, AUtils.currentExercice());
		//		userCanValiderFournis =   EOFournis.verifierDroitValidationFournisseur(getMyComponent().getAppUserForAnnuaire());
		userCanValiderFournis = getMyComponent().getAppUserForAnnuaire().hasDroitValidationEOFournis(null, AUtils.currentExercice());
		;
	}

}
