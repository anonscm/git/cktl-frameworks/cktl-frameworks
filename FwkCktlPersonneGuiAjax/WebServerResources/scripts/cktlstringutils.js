	ltrimString = function(inputString) { 
            	if (typeof inputString != "string") return inputString; 
            	return inputString.replace(/(\s*)(.*)/g,"$2"); 
         } 

         rtrimString = function(inputString) { 
            	if (typeof inputString != "string") return inputString; 
            	return inputString.replace(/\s*$/g,""); 
         } 

         trimString = function(inputString) { 
            	if (typeof inputString != "string") return inputString; 
            	return rtrimString(ltrimString(inputString)); 
         } 
          
         initCap = function(inputString) {
         	if (typeof inputString != "string") return inputString;
         	if (inputString.length == 0) {
         	 	return inputString;
         	} 
         	var debut = inputString.substring(0,1); 
         	return debut.toUpperCase()+inputString.substring(1);
         } 
         
       		/** Nettoie une string en remplacant les retours chariots par des espaces, en remplacant tous les espaces consecutifs par un seul et en supprimant les espaces de debut et de fin.*/
        cleanString = function(inputString) {
          	if (typeof inputString != "string") return inputString;
          	return trimString(inputString.replace(/\n|\r|\t/g, " ").replace(/\s+/g, " "));
          }