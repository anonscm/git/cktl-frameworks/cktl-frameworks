Déprécié
========

Cette documentation est obsolète suite au choix de ne faire qu'un dépot pour tous les framework. Je la laisse pour archivage.


Dépôt "SuperBuild"
==================

Ce dépôt permet de référencer les autres dépôts contenant les frameworks Cocktails participants au SuperBuild.

Chaque framework doit se trouver dans son propre dépôt.

Cloner ce dépôt
---------------

Lorsque ce dépôt est cloné, il faut ensuite se déplacer dans le répertoire local et utilisé les commandes suivantes :

>     git submodule init
>     git submodule update

Ajouter une référence à un framework 
------------------------------------

Si l'on souhaite ajouter un framework pour qu'il participe au SuperBuild, il faut l'ajouter de cette façon :

>     git submodule add gitolite@git.asso-cocktail.org:<nom_du_dépôt>.git [<nom_du_framework>]

Exemple avec le dépôt du framework FwkCktlPeche :

>     git submodule add gitolite@git.asso-cocktail.org:FwkCktlPeche.git

Il faut ensuite ajouter le nom du framework dans le fichier `settings.gradle` afin qu'il soit pris en compte dans le SuperBuild.

Utilisation du SuperBuild
=========================

Le SuperBuild consiste à lancer une compilation sur l'ensemble des framework, en n'utilisant aucun des jar fabriqués : tout est fait localement.

Cela nécessite plusieurs étape : 

- récupération des dernières versions des frameworks ;
- modification des fichiers `build.gradle` de chaque framework afin d'indiquer une dépendance vers une version appelée `snapshot` ;
- génération d'un fichier `build.gradle` au niveau du SuperBuild ;
- lancement de la compilation ;
- lancement des tests ;
- réinitialisations des modifications apportées aux fichiers `build.gradle` des frameworks ;
- mise en configuration du SuperBuild.

1. Récupération des dernières versions des frameworks
-----------------------------------------------------
Une fois le dépôt cloné via `git clone gitolite@git.asso-cocktail.org:cktlframeworks.git`, il faut descendre les frameworks à l'aide de la commande `git submodule update`.

2. Modification des fichier `build.gradle` de chaque framework.
---------------------------------------------------------------
Cette tâche est automatisée par le script `fix-version.sh`. Celui-ci lit le fichier `settings.gradle` (qui contient la liste des frameworks participants au SuperBuild), parcours les répertoires correspondant, lit le fichier `build.gradle`, et change, pour les frameworks Cocktail, le numéro de la version référencée dans les dépendances.

En unifiant les versions dans les dépendances, on s'affranchit du problème classique "Fwk1 dépend de Fwk3 en version 1.0, alors que Fwk2 dépend aussi de Fwk3, mais en version 1.2". De plus, en ayant la même version partout, gradle est capable de détecter qu'il à tout ce qu'il faut localement, et n'ira pas voir sur les dépôt Nexus. 

Le script se lance tout simplement avec la commande `./fix-version.sh`

3. Génération d'un fichier `build.gradle` au niveau du SuperBuild
-----------------------------------------------------------------
Cette tâche est également automatisée par le script `genbuild.sh`. Celui-ci lit le fichier `settings.gradle`, parcours les répertoires correspondant, lit le fichier `build.gradle`, et génère un fichier `build.gradle` pour le SuperBuild. Il se lance avec la commande `./gen-build.sh <version>`, ou `version` est la version commune à l'ensemble des frameworks. Il n'a de sens que si l'on souhaite générer des JARs, par exemple dans le but de fabriquer une nouvelle version de ces JARs et de les envoyer sur le Nexus de Cocktail.

Dans le cas ou l'on souhaite juste vérifier que le build fonctionne et que les tests unitaires passent, la valeur de `version` n'a pas d'importance.

4. Lancement de la compilation
------------------------------
Pour vérifier rapidement que tout compile correctement, il suffit de passer la commande `gradle clean jar`. Cela aura pour effet de fabriquer les JARs de tous les frameworks. Si une erreur de compilation est détectée, prendre contact avec la personne qui à fait le commit. On peut obtenir une liste des dernières activités en se positionnant dans le dossier d'un framework et en tappant la commande `git log`.

5. Lancement des tests unitaires
--------------------------------
Si tout est compilé dans problème, on peut lancer les tests unitaires avec la commande `gradle test`. Tout comme pour la compilation, si un framework ne compile pas, prendre contact avec la personne qui à fait le commit fautif.

6. Réinitialisation des modifications
-------------------------------------
En lançant le script `./reset-modules.sh`, les modifications apportées aux fichiers `build.gradle` des frameworks seront annulées. On se retrouve alors dans le même état qu'au début de la procédure.

7. Mise en configuration du SuperBuild
--------------------------------------
Si l'on souhaite garder une trace du build qui vient d'être fait, il faut envoyer les modification sur le dépôt Cocktail. Pour cela, on commence par préparer la mise en configuration avec la commande `git add .`, puis on les valide localement avec la commande `git commit -m <message>`. Enfin, on pousse tout cela vers le serveur Cocktail à l'aide de la commande `git push -u origin master` (ou `git push` tout court si l'on a déjà fait la commande précédente au moins une fois et que l'on n'a pas changé de branche).

