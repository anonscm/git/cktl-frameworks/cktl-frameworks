package org.cocktail.fwkcktlrestservices.serveur.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indique que l'on doit être authentifié pour accéder à cette méthode</br>
 * Si ce n'est pas la cas l'authentification sera déclanchée</br>
 * C'est une étape préalable à l'utilisation de @Restrcit
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface LoggedIn {

}
