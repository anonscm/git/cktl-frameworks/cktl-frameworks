package org.cocktail.fwkcktlrestservices.serveur.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.cocktail.fwkcktlrestservices.serveur.HttpStatus;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ResponseBody {
	
	/**
	 * HTTP status code
	 * @return HTTP status code.
	 */
	int statusCode() default HttpStatus.SC_OK;

}
