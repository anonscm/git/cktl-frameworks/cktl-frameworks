package org.cocktail.fwkcktlrestservices.serveur.converter;

public interface RestConverter<S, T> {
	
	T convert(S source);

}
