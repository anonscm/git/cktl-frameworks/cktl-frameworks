package org.cocktail.fwkcktlrestservices.serveur.converter;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestExceptionRepresentation;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.ICktlRestException;

public class ThrowableJsonConverter implements RestConverter<Throwable, Object> {

    public Object convert(Throwable source) {
        Object result = null;

        if (source instanceof ICktlRestException) {
            result = buildRepresentation((ICktlRestException) source);
        } else {
            result = buildRepresentation(source);
        }
        return result;
    }

    protected CktlRestExceptionRepresentation buildRepresentation(ICktlRestException ex) {
        return new CktlRestExceptionRepresentation(ex.getErrorCode(), ex.getMessage(), ExceptionUtils.getStackTrace((Throwable) ex), null);
    }

    protected CktlRestExceptionRepresentation buildRepresentation(Throwable t) {
        return new CktlRestExceptionRepresentation(ICktlRestException.UNKNOWN_ERROR_CODE, ExceptionUtils.getMessage(t),
                ExceptionUtils.getStackTrace(t), null);
    }
}
