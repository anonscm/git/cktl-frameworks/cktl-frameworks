package org.cocktail.fwkcktlrestservices.serveur.exceptions;

import org.cocktail.fwkcktlrestservices.serveur.HttpStatus;

public class CktlRestParseException extends Exception implements ICktlRestException {

	public CktlRestParseException() {
	}

	public CktlRestParseException(String msg) {
		super(msg);
	}

	public CktlRestParseException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public String getErrorCode() {
		return PARSE_ERROR_CODE;
	}

	public int getHttpResponseStatus() {
		return HttpStatus.SC_BAD_REQUEST;
	}

}
