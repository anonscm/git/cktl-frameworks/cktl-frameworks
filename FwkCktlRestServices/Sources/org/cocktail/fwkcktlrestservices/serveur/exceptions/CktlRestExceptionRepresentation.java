package org.cocktail.fwkcktlrestservices.serveur.exceptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Represente une exception telle quelle sera transmise au client.
 * 
 * Le format retenu est :
 * <ul>
 *   <li>code : Code unique correspondant à une classe d'exception serveur (ex. CKTLREST-BUD-0005)</li>
 *   <li>message : String (message à destination de l'utilisateur). (ex. Impossible d'enregistrer la prévision)</li>
 *   <li>stacktrace : String stacktrace de l'exception, à ne remplir que quand on est en dev</li>
 *   <li>errorList : Liste de chaînes représentant les messages d'erreur</li>
 *   <li>timestamp : String date au format ISO (yyyy-MM-dd HH:mm:ss.SSSS)</li>
 * </ul>
 * Exemple :
 * <code>
 * 	 code: 'CKTLREST-0005',
 *   message:'Impossible d'enregistrer la prévision',</li>
 *   stacktrace: 'IllegalArgumentException caused by ...',</li>
 *   errorList : ['Montant positif obligatoire', 'Nature non trouvée'],</li>
 *   timestamp : '2015-01-31 10:28:31.4563'
 * </code>
 *
 */
public class CktlRestExceptionRepresentation {

	private static final DateFormat ISO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	private String code;
	private String message;
	private String stacktrace;
	private List<String> errorList;
	private String timestamp;
	
	public CktlRestExceptionRepresentation(String code, String message, String stacktrace, List<String> errorList) {
		this.code = StringUtils.defaultString(code);
		this.message = StringUtils.defaultString(message);
		this.stacktrace = StringUtils.defaultString(stacktrace);
		this.errorList = new ArrayList<String>();
		
		initErrorList(errorList);
		initTimestamp();
	}
		
	private void initErrorList(List<String> errorList) {
		if (errorList == null) {
			return;
		}
		this.errorList.addAll(errorList);
	}
	
	private void initTimestamp() {
		Date now = new Date();
		this.timestamp = ISO_DATE_FORMAT.format(now);
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getStacktrace() {
		return stacktrace;
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public String getTimestamp() {
		return timestamp;
	}
}
