/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestServiceDescription;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;

import er.extensions.eof.ERXKeyFilter;
import er.rest.routes.jsr311.GET;
import er.rest.routes.jsr311.Path;

/**
 * Classe permettant de récupérer l'ensemble des services proposés.<br>
 * La réponse contient les URL complètes pour accéder aux services, du type <code>https://serveur/.../Application.woa/ra/filter/exercice/list</code><br>
 * Pour accéder à cette liste en json ou en xml, accédez à l'url http://..../Application.woa/ra/services.
 */
public class ServicesListController extends CktlRestController {

	private static final String SERVICES_ROUTE = "services";

	private static final CktlRestServiceDescription SERVICE_DESCRIPTION = new CktlRestServiceDescription();
	static {
		SERVICE_DESCRIPTION.setRoute(SERVICES_ROUTE);
		SERVICE_DESCRIPTION.setLibelle("Liste des services");
	}

	/**
	 * @param request Une {@link WORequest}
	 */
	public ServicesListController(WORequest request) {
		super(request);
	}

	/**
	 * @return Une réponse contenant la liste des services
	 */
	@GET
	@Path("/" + SERVICES_ROUTE)
	public WOActionResults indexAction() {
		FwkCktlRestServices.restServicesLogger().info(prepareLogMessage());
		Collection<CktlRestServiceDescription> liste = getServicesList();
		return response(liste, ERXKeyFilter.filterWithAll());
	}

	/**
	 * @return La liste des services sous forme d'URL
	 * @see ServicesListController#getServiceUrl(com.webobjects.appserver.WOContext)
	 */
	/*
	 * public Collection<String> getServicesListAsUrl() { Collection<String> liste = new ArrayList<String>(); String url = getServiceUrl(context());
	 * for (String service : getServicesList()) { liste.add(url + service); } return liste; }
	 */

	@Override
	public Collection<CktlRestServiceDescription> getServicesList() {
		Collection<CktlRestServiceDescription> liste = new ArrayList<CktlRestServiceDescription>();
		liste.add(serviceDescription());
		liste.addAll(new AuthController(request()).getServicesList());
		liste.addAll(new QueryController(request()).getServicesList());

		return liste;
	}

	public CktlRestServiceDescription serviceDescription() {
		return SERVICE_DESCRIPTION;
	}

}
