/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestServiceDescription;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;

import er.extensions.appserver.ERXRedirect;
import er.rest.routes.jsr311.GET;
import er.rest.routes.jsr311.Path;

/**
 * Classe permettant de gérer l'authentification.<br>
 * L'utilisateur doit la requêter avec un ticket, afin de recevoir un cookie qui lui permettre ensuite d'accéder aux différents services.<br>
 * <br>
 * <b>Exemple :</b><br>
 * <code>https://serveur/.../Application.woa/ra/auth?ticket=ST-33004-RfQZoqOlFurrP4XjkF6h</code>
 */
public class LoginCallbackController extends CktlRestController {
	private static Logger log = Logger.getLogger(LoginCallbackController.class);
	private static final String LOGINCALLBACK_ROUTE = "logincallback";

	private static final CktlRestServiceDescription SERVICE_DESCRIPTION = new CktlRestServiceDescription();
	static {
		SERVICE_DESCRIPTION.setRoute(LOGINCALLBACK_ROUTE);
		SERVICE_DESCRIPTION.setLibelle("Login et callback");
	}

	/**
	 * @param request Une {@link WORequest}
	 */
	public LoginCallbackController(WORequest request) {
		super(request);
	}

	/**
	 * @return Si l'authentification s'est bien déroulée, une réponse ok (200) avec un cookie.<br/>
	 *         Sinon, une réponse avec un code d'erreur et le texte d'explication dans le corps. si callbakcurl est present, redirection vers cette
	 *         url avec ticket en parametre
	 */
	@GET
	@Path("/" + LOGINCALLBACK_ROUTE)
	public WOActionResults loginCallbackAction() {
		FwkCktlRestServices.restServicesLogger().info(prepareLogMessage());
		String callbackUrl = request().stringFormValueForKey(AuthController.CALLBACK_URL);
		String serviceUrl = getServiceUrl(request().context()) + AuthController.AUTH_ROUTE + "?" + AuthController.CALLBACK_URL + "=" + callbackUrl;

		ERXRedirect redirectTo = new ERXRedirect(request().context());
		redirectTo.setUrl(getAuthServiceUrl() + "login?service=" + serviceUrl);
		return redirectTo;
	}

	private String getAuthServiceUrl() {
		// TODO Gérer la possibilité de fournir l'URL directement dans un paramètre lors de l'appel
		return CktlWebAction.casServiceURL();
	}

	@Override
	public Collection<CktlRestServiceDescription> getServicesList() {
		Collection<CktlRestServiceDescription> listeServices = new ArrayList<CktlRestServiceDescription>();
		listeServices.add(SERVICE_DESCRIPTION);
		return listeServices;
	}
}