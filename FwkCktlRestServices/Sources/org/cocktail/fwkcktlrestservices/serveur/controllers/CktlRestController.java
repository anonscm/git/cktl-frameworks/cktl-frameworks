/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.controllers;

import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.AUTH_KEY;
import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.HEADER_AUTH_KEY;
import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.HEADER_AUTH_KEY_EXPIRATION;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlrestservices.serveur.HttpStatus;
import org.cocktail.fwkcktlrestservices.serveur.annotations.LoggedIn;
import org.cocktail.fwkcktlrestservices.serveur.annotations.RequestBody;
import org.cocktail.fwkcktlrestservices.serveur.annotations.ResponseBody;
import org.cocktail.fwkcktlrestservices.serveur.authentification.context.AuthContextHolder;
import org.cocktail.fwkcktlrestservices.serveur.converter.RestConverter;
import org.cocktail.fwkcktlrestservices.serveur.converter.ThrowableJsonConverter;
import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IRestParamBean;
import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IRestRequete;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.ICktlRestAuthException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.ICktlRestException;
import org.cocktail.fwkcktlrestservices.serveur.log.CktlRestLoggerFormat;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestServiceDescription;
import org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODirectAction;
import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation._NSUtilities;

import er.extensions.appserver.ERXHttpStatusCodes;
import er.extensions.foundation.ERXExceptionUtilities;
import er.extensions.foundation.ERXStringUtilities;
import er.rest.ERXRestUtils;
import er.rest.format.ERXRestFormat;
import er.rest.routes.ERXRouteController;
import er.rest.routes.jsr311.CookieParam;
import er.rest.routes.jsr311.HeaderParam;
import er.rest.routes.jsr311.Path;
import er.rest.routes.jsr311.PathParam;
import er.rest.routes.jsr311.Paths;
import er.rest.routes.jsr311.QueryParam;

/**
 * Classe abstraite qui doit être étendue par tous les controllers REST
 */
public abstract class CktlRestController extends ERXRouteController {

    private static final Logger LOG = Logger.getLogger(CktlRestController.class);
    private static final String ERROR_JSON_KEY = "error";
    protected static final CktlWebApplication CKTL_APP = (CktlWebApplication) WOApplication.application();

    private static final String KEY_EMPTY_VAL = WORequest._IsmapCoords;

    private Auth auth;
    private RestConverter<Throwable, Object> throwableJsonConverter;

    /**
     * @param request
     *            Une {@link WORequest}
     */
    public CktlRestController(WORequest request) {
        super(request);
        this.throwableJsonConverter = new ThrowableJsonConverter();
    }

    @Override
    protected WOActionResults performActionNamedWithError(String actionName, Throwable t) {
        Throwable ex = ERXExceptionUtilities.getMeaningfulThrowable(t);
        LOG.debug("performActionNamedWithError : Action=" + actionName + " : " + ex.getMessage() + " : " + ex.getCause());
        if (ex instanceof ICktlRestException) {
            int errorCode = ERXHttpStatusCodes.BAD_REQUEST;
            if (ex instanceof ICktlRestAuthException) {
                errorCode = ERXHttpStatusCodes.UNAUTHORIZED;
            }
            return restErrorResponse((ICktlRestException) ex, actionName, errorCode);
        }
        return super.performActionNamedWithError(actionName, ex);
    }

    protected WOActionResults restErrorResponse(ICktlRestException ex, String actionName, int statusCode) {
        return errorResponse(formatActionNamedWithError(actionName, ex), statusCode);
    }

    protected String formatActionNamedWithError(String actionName, ICktlRestException ex) {
        return actionName + " : " + ex.getErrorCode() + " : " + ex.getMessage();
    }

    @Override
    protected ERXRestFormat defaultFormat() {
        return ERXRestFormat.json();
    }

    /**
     * @param context
     *            Un {@link WOContext}
     * @return L'URL complète du service REST du type
     *         <code>http(s)://server/.../Application.woa/ra/</code>
     */
    public String getServiceUrl(WOContext context) {
        String appInstanceURL;
        try {
            appInstanceURL = CKTL_APP.getApplicationInstanceURL(context).replaceAll(InetAddress.getLocalHost().getHostAddress(), CKTL_APP.getHost());
        } catch (Exception e) {
            appInstanceURL = CKTL_APP.getApplicationInstanceURL(context);
        }
        StringBuffer actionUrl = new StringBuffer(appInstanceURL);
        if (!appInstanceURL.endsWith("/")) {
            actionUrl.append("/");
        }
        actionUrl.append("ra/");
        return actionUrl.toString();
    }

    /**
     * Vérifie que l'utilisateur est authentifié
     * 
     * @param resourceId
     *            Le libellé de la ressource
     */
    public void checkUserAccess(String resourceId) {
        setAuth(new AuthenticationService().checkAccess(getAuthKey(), resourceId));
    }

    protected String getAuthKey() {
        String authKey = (String) request().formValueForKey(AUTH_KEY);
        if (ERXStringUtilities.stringIsNullOrEmpty(authKey)) {
            authKey = request().headerForKey(HEADER_AUTH_KEY);
        }
        if (ERXStringUtilities.stringIsNullOrEmpty(authKey)) {
            authKey = request().cookieValueForKey(AUTH_KEY);
        }
        return authKey;
    }

    /**
     * @return La liste des services disponibles
     */
    public abstract Collection<CktlRestServiceDescription> getServicesList();

    protected String getRoutePath() {
        return request().uri().substring(request().uri().indexOf("/ra") + 3);
    }

    /**
     * @return une map construite à partir des parametres passes dans l'url
     */
    protected Map<String, List<Object>> getRestParams() {
        NSDictionary<String, NSArray<Object>> formValues = request().formValues();
        Map<String, List<Object>> restParams = new HashMap<String, List<Object>>();

        for (String aKey : formValues.allKeys()) {
            if (KEY_EMPTY_VAL.equals(aKey)) {
                List<Object> list = formValues.get(aKey);
                for (Object object : list) {
                    restParams.put(object.toString(), null);
                }
            } else {
                restParams.put(aKey, formValues.get(aKey));
            }
        }
        return restParams;
    }

    protected Map<String, List<Object>> getRestPreparedParams() {
        Map<String, List<Object>> restPreparedParams = new HashMap<String, List<Object>>();
        restPreparedParams.putAll(getRestParams());
        if (getAuth() != null) {
            restPreparedParams.put(Auth.UTILISATEUR_PERSID_PARAMKEY, Collections.singletonList((Object) getAuth().getPersId()));
        }
        return restPreparedParams;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    protected String prepareLogMessage() {
        String logMsg = CktlRestLoggerFormat.formatLog(request(), route().method().name(), getRoutePath(), getRestPreparedParams());
        return logMsg;
    }

    protected CktlRestServiceDescription getServiceDescription(IRestRequete iRestRequete, Collection<? extends IRestParamBean> params, String route) {
        CktlRestServiceDescription serviceDescription = new CktlRestServiceDescription();
        serviceDescription.setCategorie(iRestRequete.categorie());
        serviceDescription.setCommentaireDeveloppeur(iRestRequete.commentaireDeveloppeur());
        serviceDescription.setCommentaireUtilisateur(iRestRequete.commentaireUtilisateur());
        serviceDescription.setLibelle(iRestRequete.libelle());
        serviceDescription.setStrId(iRestRequete.strId());
        serviceDescription.setParams(params);
        serviceDescription.setRoute(route);
        return serviceDescription;
    }

    @Override
    protected WOActionResults performActionWithArguments(Method actionMethod, Object... args) throws Exception {

        try {
        	String newToken = null;
            if (isRouteRequiredAuthentication(actionMethod)) {
                newToken = checkUserLoggedIn();
            }

            ResponseBody responseBody = actionMethod.getAnnotation(ResponseBody.class);
			if (responseBody != null) {
				Object result = actionMethod.invoke(this, args);
				int httpStatusCode = responseBody.statusCode();
				WOResponse resp = jsonResponseWithObject(result, httpStatusCode);
				if (newToken != null) {
					resp.setHeader(HEADER_AUTH_KEY + ", " + HEADER_AUTH_KEY_EXPIRATION, "Access-Control-Expose-Headers");
					resp.setHeader(newToken, HEADER_AUTH_KEY);
					resp.setHeader(getExpirationDate(newToken).toString(), HEADER_AUTH_KEY_EXPIRATION);
				}
				return resp;
			} else {
				return super.performActionWithArguments(actionMethod, args);
			}
        } catch (InvocationTargetException targetError) {
            Throwable ex = targetError.getTargetException();
            Object error = getThrowableJsonConverter().convert(ex);
            int respHttpStatus = HttpStatus.SC_INTERNAL_SERVER_ERROR;

            if (ex instanceof ICktlRestException) {
                respHttpStatus = ((ICktlRestException) ex).getHttpResponseStatus();
            }

            LOG.error("Une erreur est survenue lors de l'appel REST", ex);
            return responseError(error, respHttpStatus);
        } catch (Exception e) {
            Object error = getThrowableJsonConverter().convert(e);
            int respHttpStatus = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            if (e instanceof ICktlRestException) {
                respHttpStatus = ((ICktlRestException) e).getHttpResponseStatus();
            }
            LOG.error("Une erreur est survenue lors de l'appel REST", e);
            return responseError(error, respHttpStatus);
        }
    }

    private boolean isRouteRequiredAuthentication(Method actionMethod) {
        boolean isSecured = false;
        Annotation[] methodAnnotations = actionMethod.getAnnotations();
        for (Annotation annoation : methodAnnotations) {
            if (annoation instanceof LoggedIn) {
                isSecured = true;
                break;
            }
        }
        return isSecured;
    }

    private String checkUserLoggedIn() throws SecurityException {
        // FLA je n'utilise pas la méthode checkUserAccess car je n'ai pas
        // reellement de resourceId
        // et je n'ai pas non plus besoin de charger l'utilisateur depuis la
        // base.
        // on peut voir comment combiner les deux.
        Auth localAUth = AuthContextHolder.getContext().getAuthentication();
        AuthenticationService authService = new AuthenticationService();
        authService.checkAuth(localAUth);
        return authService.updateToken(localAUth);
    }
    
    private DateTime getExpirationDate(String token) {
    	AuthenticationService authService = new AuthenticationService();
        return authService.getExpirationDate(token);
    }

    /**
     * Called when performRouteAction dispatches a method that uses parameter
     * annotations.
     * 
     * @param actionMethod
     *            the action method to dispatch
     * @param parameterAnnotations
     *            the list of annotations
     * @return the results of the action
     * @throws Exception
     *             if anything fails
     */
    @Override
    protected WOActionResults performActionWithAnnotations(Method actionMethod, List<Annotation> parameterAnnotations) throws Exception {
        Class<?>[] parameterTypes = actionMethod.getParameterTypes();
        Object[] params = new Object[parameterAnnotations.size()];
        for (int paramNum = 0; paramNum < params.length; paramNum++) {
            Annotation param = parameterAnnotations.get(paramNum);
            if (param instanceof PathParam) {
                params[paramNum] = routeObjectForKey(((PathParam) param).value());
            } else if (param instanceof QueryParam) {
                String value = request().stringFormValueForKey(((QueryParam) param).value());
                params[paramNum] = ERXRestUtils.coerceValueToTypeNamed(value, parameterTypes[paramNum].getName(), restContext(), true);
            } else if (param instanceof CookieParam) {
                String value = request().cookieValueForKey(((CookieParam) param).value());
                params[paramNum] = ERXRestUtils.coerceValueToTypeNamed(value, parameterTypes[paramNum].getName(), restContext(), true);
            } else if (param instanceof HeaderParam) {
                String value = request().headerForKey(((HeaderParam) param).value());
                params[paramNum] = ERXRestUtils.coerceValueToTypeNamed(value, parameterTypes[paramNum].getName(), restContext(), true);
            } else if (param instanceof RequestBody) {
                params[paramNum] = getRequestBodyAsObjectOfClass(actionMethod.getParameterTypes()[paramNum]);
            } else {
                throw new IllegalArgumentException("Unknown parameter #" + paramNum + " of " + actionMethod.getName() + ".");
            }
        }
        return performActionWithArguments(actionMethod, params);
    }

    /**
     * If this request is for a normal route action, this method is called to
     * dispatch it.
     * 
     * @param actionName
     *            the name of the action to perform
     * @return the results of the action
     * @throws Exception
     *             if anything fails
     */
    @Override
    protected WOActionResults performRouteActionNamed(String actionName) throws Exception {
        WOActionResults results = null;

        String actionMethodName = actionName + WODirectAction.actionText;
        Method actionMethod = _methodForAction(actionMethodName, "");
        if (actionMethod == null) {
            actionMethod = _methodForAction(actionName, "");
            if (actionMethod == null || (actionMethod.getAnnotation(Path.class) == null && actionMethod.getAnnotation(Paths.class) == null)) {
                actionMethod = null;
            }
        }

        if (actionMethod == null || actionMethod.getParameterTypes().length > 0) {
            actionMethod = null;

            int bestMatchParameterCount = 0;
            List<Annotation> bestMatchAnnotations = null;
            for (Method method : getClass().getDeclaredMethods()) {
                String methodName = method.getName();
                boolean nameMatches = methodName.equals(actionMethodName);
                if (!nameMatches && methodName.equals(actionName)
                        && (method.getAnnotation(Path.class) != null || method.getAnnotation(Paths.class) != null)) {
                    nameMatches = true;
                }
                if (nameMatches) {
                    int parameterCount = 0;
                    List<Annotation> params = new LinkedList<Annotation>();
                    for (Annotation[] parameterAnnotations : method.getParameterAnnotations()) {
                        for (Annotation parameterAnnotation : parameterAnnotations) {
                            if (parameterAnnotation instanceof PathParam || parameterAnnotation instanceof QueryParam
                                    || parameterAnnotation instanceof CookieParam || parameterAnnotation instanceof HeaderParam
                                    || parameterAnnotation instanceof RequestBody) {
                                params.add(parameterAnnotation);
                                parameterCount++;
                            } else {
                                parameterCount = -1;
                                break;
                            }
                        }
                        if (parameterCount == -1) {
                            break;
                        }
                    }
                    if (parameterCount > bestMatchParameterCount) {
                        actionMethod = method;
                        bestMatchParameterCount = parameterCount;
                        bestMatchAnnotations = params;
                    }
                }
            }

            if (actionMethod == null) {
                results = performUnknownAction(actionName);
            } else if (bestMatchParameterCount == 0) {
                results = performActionWithArguments(actionMethod, _NSUtilities._NoObjectArray);
            } else {
                results = performActionWithAnnotations(actionMethod, bestMatchAnnotations);
            }
        } else {
            results = performActionWithArguments(actionMethod, _NSUtilities._NoObjectArray);
        }
        return results;
    }

    protected <T> T getRequestBodyAsObjectOfClass(Class<T> clazz) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        NSData requestBody = request().content();
        try {
            return mapper.readValue(requestBody.stream(), clazz);
        } catch (Throwable t) {
            LOG.error("Une erreur est survenue lors de la deserialisation", t);
            throw new CktlRestParseException("Erreur de désérialisation : impossible d'analyser le contenu de la requête");
        }
    }

    protected WOResponse jsonResponseWithObject(Object object) {
        return jsonResponseWithObject(object, WOResponse.HTTP_STATUS_OK);
    }

    protected WOResponse jsonResponseWithObject(Object object, int httpStatus) {
        WOResponse response = new WOResponse();
        response.setStatus(httpStatus);
        response.setHeader("application/json", "Content-Type");

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            response.setContent(mapper.writeValueAsString(object));
        } catch (Exception e) {
            LOG.error("Une erreur est survenue lors de la serialisation", e);
        }
        return response;
    }

    protected WOResponse responseError(Object error) {
        return responseError(error, WOMessage.HTTP_STATUS_INTERNAL_ERROR);
    }

    protected WOResponse responseError(Object error, int httpStatus) {
        Map<String, Object> errorObjectAsMap = new HashMap<String, Object>();
        errorObjectAsMap.put(ERROR_JSON_KEY, error);

        return jsonResponseWithObject(errorObjectAsMap, httpStatus);
    }

    public RestConverter<Throwable, Object> getThrowableJsonConverter() {
        return throwableJsonConverter;
    }

    public void setThrowableJsonConverter(RestConverter<Throwable, Object> throwableJsonConverter) {
        if (throwableJsonConverter != null) {
            this.throwableJsonConverter = throwableJsonConverter;
        }
    }

}
