/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.controllers;

import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.AUTH_KEY;
import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.HEADER_AUTH_KEY;
import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.HEADER_AUTH_KEY_EXPIRATION;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.annotations.LoggedIn;
import org.cocktail.fwkcktlrestservices.serveur.annotations.RequestBody;
import org.cocktail.fwkcktlrestservices.serveur.authentification.IAuthProvider;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationAuthKeyTimeOutException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationFailureException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestParamBean;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestServiceDescription;
import org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOCookie;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXKeyFilter;
import er.rest.routes.jsr311.GET;
import er.rest.routes.jsr311.POST;
import er.rest.routes.jsr311.Path;

/**
 * Classe permettant de gérer l'authentification.<br>
 * L'utilisateur doit la requêter avec un ticket, afin de recevoir un cookie qui
 * lui permettre ensuite d'accéder aux différents services.<br>
 * <br>
 * <b>Exemple :</b><br>
 * <code>https://serveur/.../Application.woa/ra/auth?ticket=ST-33004-RfQZoqOlFurrP4XjkF6h</code>
 */
public class LoginController extends CktlRestController {
    public static final String LOGIN_ROUTE = "login";

    private static Logger log = Logger.getLogger(LoginController.class);
    private static final String CONTENT_TYPE = "content-type";
    private static final String METHODE = "methode";
    private static final String CALLBACK_URL = "callbackurl";
    private static final String CALLBACK = "callback";
    private static final String POST_MESSAGE = "postMessage";

    private static final CktlRestServiceDescription SERVICE_DESCRIPTION = new CktlRestServiceDescription();

    static {
        SERVICE_DESCRIPTION.setRoute(LOGIN_ROUTE);
        SERVICE_DESCRIPTION.setParams(Collections.singletonList(new CktlRestParamBean(IAuthProvider.TICKET)));
        SERVICE_DESCRIPTION.setLibelle("Authentification");
    }

    /**
     * TODO Gérer le provider d'authentification de manière dynamique (pouvoir
     * specifier dans les parametres de l'application)
     */
    @Inject
    private IAuthProvider authProvider;

    /**
     * @param request
     *            Une {@link WORequest}
     */
    public LoginController(WORequest request) {
        super(request);
    }

    @GET
    @Path("/" + LOGIN_ROUTE + "Test")
    @LoggedIn
    public WOActionResults loginTestAction() {
        WOActionResults respAuthKey = authKeyValide();
        return respAuthKey;
    }

    @GET
    @Path("/" + LOGIN_ROUTE + "/" + "logout")
    public WOActionResults logoutAction() {
        String logoutUrl = authProvider.logoutUrl();
        Map<String, String> json = new HashMap<String, String>();
        json.put("logoutUrl", logoutUrl);
        log.debug("Déconnexion " + getAuthKey());
        return jsonResponseWithObject(json);
    }

    @POST
    @Path("/" + LOGIN_ROUTE)
    public WOActionResults loginActionSingleLogOn(@RequestBody LoginInfo loginInfo) {

        // Récupération des infos de login
        String login = loginInfo.getLogin();
        String password = loginInfo.getPassword();

        Map<String, String> values = new HashMap();
        values.put(IAuthProvider.LOGIN, login);
        values.put(IAuthProvider.PASSWORD, password);

        WOActionResults result = authentication(values);
        return result;
    }

    /**
     * @return Si l'authentification s'est bien déroulée, une réponse ok (200)
     *         avec un cookie.<br/>
     *         Sinon, une réponse avec un code d'erreur et le texte
     *         d'explication dans le corps. si callbakcurl est present,
     *         redirection vers cette url avec ticket en parametre
     */
    @GET
    @Path("/" + LOGIN_ROUTE)
    public WOActionResults loginActionSingleSignOn() {
        FwkCktlRestServices.restServicesLogger().info(prepareLogMessage());

        if (isDemandeDeDescriptionModeAuthentification()) {
            return jsonResponseWithObject(getDescriptionModeAuthentification());
        }

        // -- 1 -- On vérifie s'il existe une authKey
        WOActionResults respAuthKey = authKeyValide();
        if (respAuthKey != null) {
            return respAuthKey;
        }

        // Initialisation des paramètres de requête
        Map<String, String> values = parseRequest();

        // -- 2 -- On regarde alors si on est authentifié à CAS
        WOActionResults respPreTraitements = authProvider.preTraitements(context(), values);
        if (respPreTraitements != null) {
            return respPreTraitements;
        }

        WOActionResults result = authentication(values);
        return result;
    }

    protected Map<String, String> getDescriptionModeAuthentification() {
        Map<String, String> desc = new HashMap<String, String>();
        desc.put("isExternal", authProvider.isExternalAuth().toString());
        desc.put("libelle", authProvider.authLibelle());
        return desc;
    }

    protected boolean isDemandeDeDescriptionModeAuthentification() {
        return request().stringFormValueForKey("description") != null;
    }

    private WOActionResults authentication(Map<String, String> values) {
        Auth auth = authProvider.getAuthentication(values);
        String hash = createAuthKey(auth);
        JsonNode authJSON = createAuthJson(auth, hash);

        if (values.get(POST_MESSAGE) != null) {
            return postMessageResponse(authJSON);
        } else if (values.get(CALLBACK_URL) != null) {
            return callbackUrlResponse(values, hash);
        } else if (values.get(CALLBACK) != null) {
            return callbackResponse(values, hash, authJSON);
        } else {
            return defaultResponse(hash, authJSON);
        }
    }

    /**
     * @param auth
     *            Un objet {@link Auth}
     * @param hash
     *            Le hash de l'objet auth
     * @return Un {@link JsonNode} créé à partir de auth et hash
     */
    private JsonNode createAuthJson(Auth auth, String hash) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            String authJsonStr = mapper.writeValueAsString(auth);
            ObjectNode json = (ObjectNode) mapper.readTree(authJsonStr);
            json.put(AUTH_KEY, hash);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CktlRestAuthenticationFailureException("Erreur lors du parse auth", e);
        }
    }

    private WOActionResults defaultResponse(String hash, JsonNode authJSON) {
        String type = "application/json";
        WOActionResults restResponse = response(authJSON.toString(), ERXKeyFilter.filterWithAll());
        WOResponse response = new WOResponse();

        // if (authProvider.useCookies()) {
        // response.addCookie(createCookie(hash));
        // }
        response.setHeader(type, CONTENT_TYPE);
        response.setHeader(hash, HEADER_AUTH_KEY);
        response.setContent(restResponse.generateResponse().content());
        return response;
    }

    private WOActionResults callbackResponse(Map<String, String> values, String hash, JsonNode authJSON) {
        String type = "application/json";
        String content = values.get(CALLBACK) + "(" + authJSON.toString() + ")";
        WOResponse response = new WOResponse();
        // if (authProvider.useCookies()) {
        // response.addCookie(createCookie(hash));
        // }
        response.setHeader(type, CONTENT_TYPE);
        response.setHeader(hash, HEADER_AUTH_KEY);
        response.setContent(content);
        log.info("Login callback [" + values.get(CALLBACK) + "]...");
        return response;
    }

    private WOActionResults callbackUrlResponse(Map<String, String> values, String hash) {
        ERXRedirect redirectTo = new ERXRedirect(request().context());
        redirectTo.setUrl(values.get(CALLBACK_URL) + "?" + AUTH_KEY + "=" + hash);
        log.info("Login Redirect callbackUrl [" + values.get(CALLBACK_URL) + "]...");
        return redirectTo;
    }

    private WOActionResults postMessageResponse(JsonNode authJSON) {
        String type = "text/html";
        String content = "Login success, please wait...\n<script>\n (window.opener.postMessage ? window.opener : window.opener.document).postMessage('loggedUser=' + JSON.stringify("
                + authJSON.toString() + "), '*');\n</script>";
        WOResponse response = new WOResponse();
        response.setHeader(type, CONTENT_TYPE);
        response.setContent(content);
        log.info("Login postMessage...");
        return response;
    }

    private WOActionResults authKeyValide() {
        String authKey = getAuthKey();
        if (authKey != null) {
            // Est-ce que cette authKey est valide ?
            try {
                AuthenticationService authService = new AuthenticationService();
                Auth auth = authService.checkAuthKey(authKey);

                String newAuthKey = authService.updateToken(auth);
                Auth newAuth = authService.parseAuthKey(newAuthKey);
                String expirationDate = authService.getExpirationDate(newAuthKey).toString();
                JsonNode authJSON = createAuthJson(newAuth, newAuthKey);

                WOActionResults restResponse = response(authJSON.toString(), ERXKeyFilter.filterWithAll());
                WOResponse response = new WOResponse();
                response.setHeader(HEADER_AUTH_KEY + ", " + HEADER_AUTH_KEY_EXPIRATION, "Access-Control-Expose-Headers");
                response.setHeader(expirationDate, HEADER_AUTH_KEY_EXPIRATION);
                response.setHeader(newAuthKey, HEADER_AUTH_KEY);
                response.setContent(restResponse.generateResponse().content());
                log.info("Login AuthKey : user=" + newAuth.getUser() + " : OK");
                return response;
            } catch (CktlRestAuthenticationAuthKeyTimeOutException e) {
                log.info("Login : " + e.getMessage());
                // On continue
            }
        }
        return null;
    }

    private String _fillMapFromRequest(Map<String, String> m, String key) {
        return m.put(key, request().stringFormValueForKey(key));
    }

    private Map<String, String> parseRequest() {
        Map<String, String> values = new HashMap<String, String>();
        _fillMapFromRequest(values, IAuthProvider.TICKET);
        _fillMapFromRequest(values, CALLBACK_URL);
        _fillMapFromRequest(values, CALLBACK);
        _fillMapFromRequest(values, METHODE);
        _fillMapFromRequest(values, POST_MESSAGE);

        String serviceUrl = getServiceUrl(request().context()) + LOGIN_ROUTE;

        String args = addToArgs(null, CALLBACK_URL, values.get(CALLBACK_URL));
        args = addToArgs(args, METHODE, values.get(METHODE));
        args = addToArgs(args, CALLBACK, values.get(CALLBACK));
        args = addToArgs(args, POST_MESSAGE, values.get(POST_MESSAGE));
        serviceUrl += args;
        values.put(IAuthProvider.SERVICE_URL, serviceUrl);

        return values;
    }

    private String addToArgs(String args, String key, String value) {
        if (args == null) {
            args = "";
        }
        if (value != null) {
            if (args.length() == 0) {
                args += "?";
            } else {
                args += "&";
            }
            if (value.length() == 0) {
                value = "''";
            }
            args += key + "=" + value;
        }
        return args;
    }

    private WOCookie createCookie(String value) {
        NSTimestamp timeout = new NSTimestamp().timestampByAddingGregorianUnits(0, 0, 0, 0, 0, FwkCktlRestServices.getAuthkeyTimeOut());
        WOCookie cookie = new WOCookie(AUTH_KEY, value, null, null, timeout, false);
        log.debug("Création d'un cookie : " + cookie);
        return cookie;
    }

    private String createAuthKey(Auth auth) {
        String hash = new AuthenticationService().encryptUser(auth.formatToJson());
        return hash;
    }

    @Override
    public Collection<CktlRestServiceDescription> getServicesList() {
        Collection<CktlRestServiceDescription> listeServices = new ArrayList<CktlRestServiceDescription>();
        listeServices.add(SERVICE_DESCRIPTION);
        return listeServices;
    }
}