/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.controllers;

import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.annotations.LoggedIn;
import org.cocktail.fwkcktlrestservices.serveur.authentification.IAuthProvider;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationFailureException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestParamBean;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestServiceDescription;
import org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOCookie;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXKeyFilter;
import er.rest.routes.jsr311.GET;
import er.rest.routes.jsr311.Path;

/**
 * Classe permettant de gérer l'authentification.<br>
 * L'utilisateur doit la requêter avec un ticket, afin de recevoir un cookie qui lui permet ensuite d'accéder aux différents services.<br>
 * <br>
 * <b>Exemple :</b><br>
 * <code>https://serveur/.../Application.woa/ra/auth?ticket=ST-33004-RfQZoqOlFurrP4XjkF6h</code>
 */
public class AuthController extends CktlRestController {
	private static Logger log = Logger.getLogger(AuthController.class);
	public static final String AUTH_ROUTE = "auth";
	private static final String TICKET = "ticket";
	public static final String CALLBACK_URL = "callbackurl";

	private static final CktlRestServiceDescription SERVICE_DESCRIPTION = new CktlRestServiceDescription();
	static {
		SERVICE_DESCRIPTION.setRoute(AUTH_ROUTE);
		SERVICE_DESCRIPTION.setParams(Collections.singletonList(new CktlRestParamBean(TICKET)));
		SERVICE_DESCRIPTION.setLibelle("Authentification");
	}

	/** TODO Gérer le provider d'authentification de manière dynamique (pouvoir specifier dans les parametres de l'application) */
	@Inject
	private IAuthProvider authProvider;

	/**
	 * @param request Une {@link WORequest}
	 */
	public AuthController(WORequest request) {
		super(request);
	}
	
	/**
	 * @return Si l'authentification s'est bien déroulée, une réponse ok (200) avec un cookie.<br/>
	 *         Sinon, une réponse avec un code d'erreur et le texte d'explication dans le corps. si callbackurl est present, redirection vers cette
	 *         url avec ticket en parametre
	 */
	@GET
	@Path("/" + AUTH_ROUTE)
	public WOActionResults authAction() {
		FwkCktlRestServices.restServicesLogger().info(prepareLogMessage());
		String ticket = request().stringFormValueForKey(TICKET);
		String callbackUrl = request().stringFormValueForKey(CALLBACK_URL);
		String serviceUrl = getServiceUrl(request().context()) + AUTH_ROUTE;
		if (callbackUrl != null) {
			serviceUrl = serviceUrl + "?" + CALLBACK_URL + "=" + callbackUrl;
		}
		String user = authProvider.checkAuthentication(getAuthServiceUrl(), serviceUrl, ticket);
		CktlUserInfo ui = initUserInfo(user);
		Auth auth = new Auth(user, ui.persId().intValue());
		String hash = createAuthKey(auth);
		WOCookie cookie = createCookie(hash);
		Map<String, String> token = new HashMap<String, String>();
		token.put(AUTH_KEY, hash);

		if (callbackUrl != null) {
			ERXRedirect redirectTo = new ERXRedirect(request().context());
			redirectTo.setUrl(callbackUrl + "?" + AUTH_KEY + "=" + hash);
			return redirectTo;
		}
		else {
			WOActionResults restResponse = response(token, ERXKeyFilter.filterWithAll());
			WOResponse response = new WOResponse();
			response.addCookie(cookie);
			response.setHeader(hash, HEADER_AUTH_KEY);
			response.setContent(restResponse.generateResponse().content());
			log.info("Authentification : user=" + user + " : OK");
			return response;
		}
	}

	private CktlUserInfoDB initUserInfo(String user) {
		CktlUserInfoDB ui = new CktlUserInfoDB(CKTL_APP.dataBus());
		ui.compteForLogin(user, null, true);
		if (ui.hasError()) {
			throw new CktlRestAuthenticationFailureException("Utilisateur non reconnu : " + user);
		}
		return ui;
	}

	private String getAuthServiceUrl() {
		// TODO Gérer la possibilité de fournir l'URL directement dans un paramètre lors de l'appel
		return CktlWebAction.casServiceURL();
	}

	private WOCookie createCookie(String value) {
		NSTimestamp timeout = new NSTimestamp().timestampByAddingGregorianUnits(0, 0, 0, 0, 0, FwkCktlRestServices.getAuthkeyTimeOut());
		WOCookie cookie = new WOCookie(AUTH_KEY, value, null, null, timeout, false);
		log.debug("Création d'un cookie : " + cookie);
		return cookie;
	}

	private String createAuthKey(Auth auth) {
		String hash = new AuthenticationService().encryptUser(auth.formatToJson());
		return hash;
	}

	@Override
	public Collection<CktlRestServiceDescription> getServicesList() {
		Collection<CktlRestServiceDescription> listeServices = new ArrayList<CktlRestServiceDescription>();
		listeServices.add(SERVICE_DESCRIPTION);
		return listeServices;
	}
}