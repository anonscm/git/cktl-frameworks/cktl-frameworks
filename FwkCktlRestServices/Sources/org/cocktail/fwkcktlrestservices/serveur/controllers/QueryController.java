/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IDataProvider;
import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IRestParamBean;
import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IRestRequete;
import org.cocktail.fwkcktlrestservices.serveur.dataproviders.SQLDataProvider;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNoDataException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;
import org.cocktail.fwkcktlrestservices.serveur.finders.FinderRestSQLRequete;
import org.cocktail.fwkcktlrestservices.serveur.model.CktlRestServiceDescription;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXKeyFilter;
import er.rest.routes.jsr311.GET;
import er.rest.routes.jsr311.Path;
import er.rest.routes.jsr311.PathParam;

/**
 * Gère les filtres et la façon de les réccupérer
 */
public class QueryController extends CktlRestController {

	private static final String QUERYID_KEY = "queryId";
	public static final String QUERYCLASS = "query";
	public static final String QUERY_ROUTE_PATH = "/" + QUERYCLASS + "/{" + QUERYID_KEY + ":String}/list";
	public static final String QUERY_COUNT_ROUTE_PATH = "/" + QUERYCLASS + "/{" + QUERYID_KEY + ":String}/count";
	public static final String QUERY_SCHEMA_ROUTE_PATH = "/" + QUERYCLASS + "/{" + QUERYID_KEY + ":String}/doc";

	/**
	 * The data provider. TODO Google Guice
	 */
	private IDataProvider dataProvider = new SQLDataProvider();

	/**
	 * @param request Une {@link WORequest}
	 */
	public QueryController(WORequest request) {
		super(request);
	}

	/**
	 * @param queryId
	 *            L'identifiant de la requête.
	 * @return Les données correspondantes.
	 * @throws CktlRestNoDataException
	 *             Si la requête n'a pas été trouvée.
	 * @throws CktlRestParseException
	 *             Si erreur lors du traitement de la requête.
	 * 
	 */
	@GET
	@Path(QUERY_ROUTE_PATH)
	public WOActionResults listAction(@PathParam(QUERYID_KEY) String queryId) throws CktlRestNoDataException, CktlRestParseException {
		checkUserAccess(queryId);
		String logMsg = prepareLogMessage();

		Collection<Map<String, Object>> res = null;
		try {
			res = dataProvider.getData(queryId, getRestPreparedParams());
			logMsg += " | OK";
		} catch (CktlRestNoDataException e) {
			logMsg += " | ERREUR : " + e.getErrorCode() + " : " + e.getMessage();
			FwkCktlRestServices.restServicesLogger().info(logMsg);
			throw e;
		}
		FwkCktlRestServices.restServicesLogger().info(logMsg);
		return response(res, ERXKeyFilter.filterWithAll());
	}

	/**
	 * @param queryId
	 *            L'identifiant de la requête.
	 * @return Le nombre de données correspondantes.
	 * @throws CktlRestNoDataException
	 *             Si la requête n'a pas été trouvée.
	 * @throws CktlRestParseException
	 *             Si erreur lors du traitement de la requête.
	 * 
	 */
	@GET
	@Path(QUERY_COUNT_ROUTE_PATH)
	public WOActionResults countAction(@PathParam(QUERYID_KEY) String queryId) throws CktlRestNoDataException, CktlRestParseException {
		checkUserAccess(queryId);
		String logMsg = prepareLogMessage();
		
		Long res;
		try {
			res = dataProvider.count(queryId, getRestPreparedParams());
			logMsg += " | OK";
		} catch (CktlRestNoDataException e) {
			logMsg += " | ERREUR : " + e.getErrorCode() + " : " + e.getMessage();
			FwkCktlRestServices.restServicesLogger().info(logMsg);
			throw e;
		}
		FwkCktlRestServices.restServicesLogger().info(logMsg);
		return response(res, ERXKeyFilter.filterWithAll());
	}
	
	/**
	 * @param filterId
	 *            L'identifiant de la requête.
	 * @return La description de la requête
	 * @throws CktlRestNoDataException
	 *             Si la requête n'a pas été trouvée.
	 */
	@GET
	@Path(QUERY_SCHEMA_ROUTE_PATH)
	public WOActionResults docAction(@PathParam(QUERYID_KEY) String filterId) throws CktlRestNoDataException {
		IRestRequete iRestRequete = FinderRestSQLRequete.instance().findByStringId(ERXEC.newEditingContext(), filterId);
		Collection<? extends IRestParamBean> params = dataProvider.getParamsForQuery(iRestRequete);
		String route = QUERYCLASS + "/" + iRestRequete.strId() + "/list";
		CktlRestServiceDescription serviceDescription = getServiceDescription(iRestRequete, params, route);

		String logMsg = prepareLogMessage();
		FwkCktlRestServices.restServicesLogger().info(logMsg);

		ERXKeyFilter filter = ERXKeyFilter.filterWithAllRecursive();
		filter.exclude(new ERXKey<String>("params.formattedValue"));
		filter.exclude(new ERXKey<String>("params.rawName"));

		return response(serviceDescription, filter);
	}

	@Override
	public Collection<CktlRestServiceDescription> getServicesList() {
		List<CktlRestServiceDescription> serviceList = new ArrayList<CktlRestServiceDescription>();
		Collection<? extends IRestRequete> listeQuery = dataProvider.getAvailableQueries();
		for (IRestRequete iRestRequete : listeQuery) {
			Collection<? extends IRestParamBean> params = dataProvider.getParamsForQuery(iRestRequete);
			String route = QUERYCLASS + "/" + iRestRequete.strId() + "/list";
			CktlRestServiceDescription serviceDescription = getServiceDescription(iRestRequete, params, route);
			String countRoute = QUERYCLASS + "/" + iRestRequete.strId() + "/count";
			serviceDescription.setCountRoute(countRoute);
			serviceList.add(serviceDescription);
		}
		Collections.sort(serviceList);
		return serviceList;
	}
}
