/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur;

import javax.management.Query;

import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.cocktail.fwkcktlrestservices.serveur.controllers.AuthController;
import org.cocktail.fwkcktlrestservices.serveur.controllers.LoginController;
import org.cocktail.fwkcktlrestservices.serveur.controllers.QueryController;
import org.cocktail.fwkcktlrestservices.serveur.controllers.ServicesListController;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlrestservices.serveur.model.Services;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;
import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXStringUtilities;
import er.rest.routes.ERXRouteRequestHandler;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 */
public class FwkCktlRestServices extends ERXFrameworkPrincipal {

	private static final String CKTLRESTSERVICES_TRANSACTIONLOG_ENABLED = "org.cocktail.cktlrestservices.transactionlog.enabled";
	private static final String ER_REST_TIMESTAMP_FORMAT = "er.rest.timestampFormat";
	private static final String ER_REST_TIMESTAMP_DEFAULT_FORMAT = "%d/%m/%Y";

	public static final String MODEL_ID = "FwkCktlRestServices";
	public static final String CKTLRESTSERVICES_TRANSACTIONLOG_FILE_KEY = "org.cocktail.cktlrestservices.transactionlog.file";
	public static final String CKTLRESTSERVICES_AUTHKEY_TIMEOUT_KEY = "org.cocktail.cktlrestservices.authkey.timeout";
	public static final String CKTLRESTSERVICES_MOCKAUTH = "org.cocktail.cktlrestservices.mockauth";
	private static final String CKTLRESTSERVICES_MOKEDUSER = "org.cocktail.cktlrestservices.mockeduser";
	private static final String CKTLRESTSERVICES_MOKEDPERSID = "org.cocktail.cktlrestservices.mockedpersid";
	public static final String CKTLRESTSERVICES_PAGINATION_PAGE_SIZE_KEY = "org.cocktail.cktlrestservices.pagination.pagesize";

	/** Fichier d'enregistrement des logs de transaction */
	private static final String CKTLRESTSERVICES_TRANSACTIONS_LOG_FILEPATH = "/tmp/CktlRestServiceTransactions.log";
	/** Durée par défaut (en secondes) avant la péremption d'un token d'authentification (authKey) = 12h */
	private static final int DEFAULT_AUTHKEY_TIMEOUT = 43200;
	/** Nombre par défaut de résultats renvoyés par les requêtes */
	private static final int CKTLRESTSERVICES_PAGINATION_PAGE_SIZE_DEFAULT_VALUE = 100;

	private static Logger REST_SERVICES_LOGGER = Logger.getLogger("org.cocktail.CktlRestServices");

	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlRestServices.class);
	}

	private void initTransactionLogs(String filePath, Logger logger) {
		FileAppender fa = new DailyRollingFileAppender();
		fa.setName("F1");
		fa.setFile(filePath);
		PatternLayout pattern = new PatternLayout();
		String appNameAndPort = getApplication().name() + ":" + getApplication().port();
		pattern.setConversionPattern("%d{dd/MM/yyyy HH:mm:ss} | " + appNameAndPort + " | %-5p| %c %x| %m%n");
		fa.setLayout(pattern);

		fa.setAppend(true);
		fa.activateOptions();

		logger.addAppender(fa);
	}

	@Override
	public void finishInitialization() {
		if (getApplication().config().booleanForKey(CKTLRESTSERVICES_TRANSACTIONLOG_ENABLED)) {
			String filePath = getApplication().config().stringForKey(CKTLRESTSERVICES_TRANSACTIONLOG_FILE_KEY);
			if (ERXStringUtilities.stringIsNullOrEmpty(filePath)) {
				filePath = CKTLRESTSERVICES_TRANSACTIONS_LOG_FILEPATH;
			}
			initTransactionLogs(filePath, REST_SERVICES_LOGGER);
		}

		CktlRouteRequestHandler routeRequestHandler = (CktlRouteRequestHandler) getApplication().requestHandlerForKey(ERXRouteRequestHandler.Key);
		if (routeRequestHandler == null) {
			routeRequestHandler = new CktlRouteRequestHandler(ERXRouteRequestHandler.WO);
		}

		routeRequestHandler.addDefaultRoutes(Services.class.getSimpleName(), ServicesListController.class);
		routeRequestHandler.addDefaultRoutes(Query.class.getSimpleName(), QueryController.class);
		routeRequestHandler.addDefaultRoutes(Auth.class.getSimpleName(), AuthController.class);

		routeRequestHandler.addRoutes(LoginController.class);
		
		// Register the request handler with the application -- it becomes the "ra" request handler
		ERXRouteRequestHandler.register(routeRequestHandler);

		// Formatage des dates dans les réponses REST
		String dateFormat = ERXProperties.stringForKeyWithDefault(ER_REST_TIMESTAMP_FORMAT, ER_REST_TIMESTAMP_DEFAULT_FORMAT);
		ERXProperties.setStringForKey(dateFormat, ER_REST_TIMESTAMP_FORMAT);

		ERXProperties.systemPropertiesChanged();

		ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
		moduleRegister.addModule(new FwkCktlRestServicesModule());

	}

	/**
	 * @return Durée (en secondes) avant la péremption d'un token d'authentification (authKey).
	 * @see FwkCktlRestServices#CKTLRESTSERVICES_AUTHKEY_TIMEOUT_KEY
	 */
	public static int getAuthkeyTimeOut() {
		int authkeyTimeOut = getApplication().config().intForKey(CKTLRESTSERVICES_AUTHKEY_TIMEOUT_KEY);
		if (authkeyTimeOut == -1) {
			authkeyTimeOut = DEFAULT_AUTHKEY_TIMEOUT;
		}
		return authkeyTimeOut;
	}
	
	public static boolean isAuthMockEnabled() {
		boolean ret = getApplication().config().booleanForKey(CKTLRESTSERVICES_MOCKAUTH);
		return ret;
	}
	
	public static Auth getMockedAuth() {
		String user = getApplication().config().stringForKey(CKTLRESTSERVICES_MOKEDUSER);
		int persId = getApplication().config().integerForKey(CKTLRESTSERVICES_MOKEDPERSID);
		return new Auth(user, persId);
	}

	/**
	 * @return Le logger des services REST
	 */
	public static Logger restServicesLogger() {
		return REST_SERVICES_LOGGER;
	}

	public static CktlWebApplication getApplication() {
		return (CktlWebApplication) CktlWebApplication.application();
	}

	/**
	 * @return Le nombre par défaut de résultats renvoyés par les requêtes, sur une page
	 */
	public static int getPaginationDefaultPageSize() {
		int defaultPageSize = getApplication().config().intForKey(CKTLRESTSERVICES_PAGINATION_PAGE_SIZE_KEY);
		if (defaultPageSize == -1) {
			defaultPageSize = CKTLRESTSERVICES_PAGINATION_PAGE_SIZE_DEFAULT_VALUE;
		}
		return defaultPageSize;
	}
}
