package org.cocktail.fwkcktlrestservices.serveur.services;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationAuthKeyTimeOutException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.joda.time.DateTime;

public class AuthTimeoutValidator {

	private int timeout;
	
	public AuthTimeoutValidator(int timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * Verifie la validite de la authKey.
	 * 
	 * @param auth Un object {@link Auth}
	 */
	public void validate(Auth auth) {
		DateTime authDate = auth.getCreationDate();

		if (hasExpired(authDate)) {
			throw new CktlRestAuthenticationAuthKeyTimeOutException(
					"Token d'authentification " + AuthenticationService.AUTH_KEY + " périmé.");
		}
	}
	
	protected boolean hasExpired(DateTime authDate) {
		DateTime now = new DateTime();
		return now.isAfter(authDate.plusSeconds(timeout));
	}
	
	protected DateTime getExpirationDate(Auth auth) {
		DateTime authDate = auth.getCreationDate();
		return authDate.plusSeconds(timeout);
	}
}
