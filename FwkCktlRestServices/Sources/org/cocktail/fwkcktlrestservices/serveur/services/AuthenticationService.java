/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.services;

import org.apache.axis.encoding.Base64;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.accessproviders.DefaultAccessProvider;
import org.cocktail.fwkcktlrestservices.serveur.accessproviders.IAccessProvider;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNotAuthenticatedException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.joda.time.DateTime;

/**
 * Classe de vérification de l'authentification et des droits d'accès.
 */
public class AuthenticationService {

    public static final String AUTH_KEY = "cktlrestauthkey";
	public static final String HEADER_AUTH_KEY_EXPIRATION = "x-cktlrestauthkey-expiration";
    public static final String HEADER_AUTH_KEY = "x-" + AUTH_KEY;

    private static final Logger LOG = Logger.getLogger(AuthenticationService.class);
    // FIXME Suppression de l'utilisation de ERXBlowfishCrypter car causait de
    // temps en temps des erreurs au décodage
    // private static final ERXBlowfishCrypter CRYPTER = new
    // ERXBlowfishCrypter();

    private IAccessProvider accessProvider = new DefaultAccessProvider();

    public AuthenticationService() {
    }

    /**
     * A appeler dans tous les services sauf auth.
     * 
     * @param authKey
     *            La valeur du token
     * @throws SecurityException
     *             Si erreur de cookie ou de droit d'accès
     */
    public Auth checkAccess(String authKey, String resourceId) throws SecurityException {
        if (authKey != null) {
            Auth auth = getAuthFromAuthKey(authKey);
            AuthTimeoutValidator timeoutValidator = new AuthTimeoutValidator(FwkCktlRestServices.getAuthkeyTimeOut());
            timeoutValidator.validate(auth);
            checkUserAccess(auth, resourceId);
            return auth;
        } else {
            // Tentative cas ?
            LOG.debug("checkAccess : token d'authentification " + AUTH_KEY + " non trouvé : " + authKey);
            throw new CktlRestNotAuthenticatedException("Token d'authentification non trouvé");
        }
    }

    public Auth checkAuthKey(String authKey) {
        Auth auth = parseAuthKey(authKey);

        if (auth == null) {
            LOG.debug("checkAuthKey : token d'authentification " + AUTH_KEY + " non trouvé : " + authKey);
            throw new CktlRestNotAuthenticatedException("Token d'authentification non trouvé");
        }

        AuthTimeoutValidator timeoutValidator = new AuthTimeoutValidator(FwkCktlRestServices.getAuthkeyTimeOut());
        timeoutValidator.validate(auth);

        return auth;
    }

    public void checkAuth(Auth auth) {
        assertAuthNotNull(auth);
        assertAuthNonExpiree(auth);
        // TODO a voir si il faut controler que le persId existe dans la base de
        // données === appel a checkUserAccess
    }

    private void assertAuthNotNull(Auth auth) {
        if (auth == null) {
            throw new CktlRestNotAuthenticatedException("Informations d'authentification non trouvées : Auth non présent");
        }
    }

    private void assertAuthNonExpiree(Auth auth) throws SecurityException {
        AuthTimeoutValidator timeoutValidator = new AuthTimeoutValidator(FwkCktlRestServices.getAuthkeyTimeOut());
        timeoutValidator.validate(auth);
    }

    public Auth parseAuthKey(String authKey) {
        if (FwkCktlRestServices.isAuthMockEnabled()) {
            return FwkCktlRestServices.getMockedAuth();
        }

        if (authKey == null) {
            return null;
        }
        return getAuthFromAuthKey(authKey);
    }

    private Auth getAuthFromAuthKey(String authKey) {
        String authKeyAsjson = new String(Base64.decode(authKey));
        // String authKeyAsjson = CRYPTER.decrypt(authKey);
        Auth auth = Auth.parseFromJson(authKeyAsjson);
        return auth;
    }

    public void checkUserAccessFromAuthKey(String authKey, String resourceId) {
        Auth auth = getAuthFromAuthKey(authKey);
        checkUserAccess(auth, resourceId);
    }

    protected void checkUserAccess(Auth auth, String resourceId) {
        getAccessProvider().checkAccess(auth, resourceId);
    }

    /**
     * @param user
     *            Le login de l'utilisateur
     * @return Le login crypté
     */
    public String encryptUser(String user) {
        // return CRYPTER.encrypt(user);
        return Base64.encode(user.getBytes());
    }

    public IAccessProvider getAccessProvider() {
        return accessProvider;
    }

    public void setAccessProvider(IAccessProvider accessProvider) {
        this.accessProvider = accessProvider;
    }
    
    public String updateToken(Auth auth) {
    	auth.setCreationDate(new DateTime());
    	String hash = encryptUser(auth.formatToJson());
    	return hash;
    }
    
    public DateTime getExpirationDate(String authKey) {
    	Auth auth = getAuthFromAuthKey(authKey);
    	AuthTimeoutValidator timeoutValidator = new AuthTimeoutValidator(FwkCktlRestServices.getAuthkeyTimeOut());
        return timeoutValidator.getExpirationDate(auth);
    }
}
