package org.cocktail.fwkcktlrestservices.serveur.model;

import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IRestParamBean;

public class CktlRestParamBean implements IRestParamBean {
	private String name;
	private boolean isObligatoire = true;
	private boolean isSetable = true;

	public CktlRestParamBean(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isObligatoire() {
		return isObligatoire;
	}
	
	public void setIsObligatoire(boolean val) {
		isObligatoire = val;
	}

	public boolean isSetable() {
		return isSetable;
	}

	public void setIsSetable(boolean isSetable) {
		this.isSetable = isSetable;
	}
}
