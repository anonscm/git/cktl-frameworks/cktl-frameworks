/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationFailureException;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 */
public class Auth implements ICktlRestObject {
	public static final String UTILISATEUR_PERSID_PARAMKEY = "utilisateur.persid";

	private static final String CREATIONDATE_KEY = "creationDate";
	private static final String PERSID_KEY = "persId";
	private static final String USER_KEY = "user";

	private String user;
	private DateTime creationDate;
	private Integer persId;

	public Auth() {

	}

	public Auth(String user, Integer persId) {
		this.user = user;

		this.persId = persId;
		this.creationDate = new DateTime();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public DateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(DateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getPersId() {
		return persId;
	}

	public void setPersId(Integer persId) {
		this.persId = persId;
	}

	public boolean isSame(Auth auth) {
		if (getPersId() != null && getPersId().equals(auth.getPersId())) {
			return true;
		}
		
		if (getUser() != null && getUser().equals(auth.getUser())) {
			return true;
		}
		
		return false;
	}
	
	public Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(USER_KEY, getUser());
		map.put(PERSID_KEY, getPersId());
		map.put(CREATIONDATE_KEY, getCreationDate());
		return map;
	}

	public String formatToJson() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JodaModule());
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		try {
			return mapper.writeValueAsString(this);
		} catch (Exception e) {
			throw new CktlRestAuthenticationFailureException("Une erreur est survenue lors de la serialisation ", e);
		}
	}

	public static Auth parseFromJson(String src) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JodaModule());
		try {
			Auth auth = mapper.readValue(src.getBytes(), Auth.class);
			checkCondition(CREATIONDATE_KEY, auth.getCreationDate() != null);
			checkCondition(USER_KEY, !StringUtils.isEmpty(auth.getUser()));
			checkCondition(PERSID_KEY, auth.getPersId() != null);
			return auth;
		} catch (Exception e) {
			throw new CktlRestAuthenticationFailureException("Une erreur est survenue lors de la serialisation ", e);
		}
	}

	public static void checkCondition(String attributeName, boolean condition) {
		if (!condition) {
			throw new CktlRestAuthenticationFailureException("Token mal formé : " + attributeName + " est vide");
		}
	}
}
