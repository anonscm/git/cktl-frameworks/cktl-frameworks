package org.cocktail.fwkcktlrestservices.serveur;

import org.cocktail.fwkcktlrestservices.serveur.authentification.IAuthProvider;

import com.google.inject.AbstractModule;

/**
 * @author jlafourc
 */
public class FwkCktlRestServicesModule extends AbstractModule {

	@Override
	protected void configure() {
		//Il faut ajouter une propriété dans le fichier properties de l'application de ce type :
		//IAuthProvider=org.cocktail.fwkcktlrestservices.serveur.authentification.CASAuthProvider

		bind(IAuthProvider.class).to(getImplementationClassFor(IAuthProvider.class));

	}

	private <T> Class<? extends T> getImplementationClassFor(Class<T> baseClass) {
		try {
			String implementationClassName = System.getProperty(baseClass.getSimpleName());
			Class<? extends T> clazz = (Class<? extends T>) Class.forName(implementationClassName);
			return clazz;
		} catch (ClassNotFoundException e) {
			System.err.println("Il faut ajouter une propriété dans le fichier properties de l'application de ce type : IAuthProvider=org.cocktail.fwkcktlrestservices.serveur.authentification.CASAuthProvider");
			throw new RuntimeException("Impossible de trouver une classe implementant " + baseClass.getName(), e);
		}
	}

}
