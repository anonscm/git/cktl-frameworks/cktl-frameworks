/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;

import com.webobjects.appserver.WORequest;

import er.extensions.logging.ERXLogger.Factory;

public class CktlRestLoggerFormat extends Factory {

	private static final String SEPARATOR = " | ";
	public static final String PASSWORD_MASQUE = "********";

	public static String formatLog(WORequest request, String httpMethod, String route, Map<String, List<Object>> params) {
		String oldPassword = null;

		if (params.containsKey("password")) {
			if (params.get("password") != null) {
				oldPassword = (String) params.get("password").get(0);
				List<Object> pass = new ArrayList<Object>();
				pass.add(PASSWORD_MASQUE);
				params.put("password", pass);
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append(FwkCktlRestServices.getApplication().getRequestIPAddress(request));
		sb.append(SEPARATOR);
		sb.append(httpMethod);
		sb.append(SEPARATOR);
		sb.append(route);
		sb.append(SEPARATOR);
		sb.append(params);
		String logMsg = sb.toString();

		logMsg = masquePassword(logMsg, oldPassword);

		return logMsg;
	}

	public static String masquePassword(String logMsg, String oldPassword) {
		return logMsg.replaceAll("password=" + oldPassword, "password=" + PASSWORD_MASQUE);
	}

}
