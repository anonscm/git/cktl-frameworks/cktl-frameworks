package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;

/**
 * Représentation d'un paramètre de remplacement dans une requête SQL.
 */
public class SQLParamBean implements IRestParamBean, Comparable<SQLParamBean> {

	private static final List<String> NON_SETABLE_PARAMS = Arrays.asList(Auth.UTILISATEUR_PERSID_PARAMKEY);
	
	/**
	 * Liste des types possibles du paramètre.
	 */
	static enum Type {
		INTEGER,
		NUMERIC,
		STRING,
		DATE
	};

	public static final String PATTERN_FORMAT_DECIMAL = "#.00";
	private static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat(PATTERN_FORMAT_DECIMAL);
	public static final String PATTERN_FORMAT_DATE = "dd/MM/yyyy";
	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(PATTERN_FORMAT_DATE);

	static {
		DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
		formatSymbols.setDecimalSeparator('.');
		DECIMAL_FORMATTER.setDecimalFormatSymbols(formatSymbols);
		// Vérification stricte du format de la date (pas de 32/01/1970)
		DATE_FORMATTER.setLenient(false);
	}

	private static final Set<String> SPECS_WHITE_LIST;
	static {
		Set<String> tmp = new HashSet<String>();
		tmp.add("SUO");
		tmp.add("SUN");
		tmp.add("SUP");
		tmp.add("SLO");
		tmp.add("SLN");
		tmp.add("IUO");
		tmp.add("IUN");
		tmp.add("ILO");
		tmp.add("ILN");
		tmp.add("DUO");
		tmp.add("DUN");
		tmp.add("DLO");
		tmp.add("DLN");
		tmp.add("NUO");
		tmp.add("NUN");
		tmp.add("NLO");
		tmp.add("NLN");
		SPECS_WHITE_LIST = Collections.unmodifiableSet(tmp);
	}

	private static final char UNIQUE = 'U';

	private static final char OBLIGATOIRE = 'O';
	private static final char NULLABLE = 'N';
	private static final char PARTIEL = 'P';

	private final boolean unique;
	private final char caractereConditionnel;
	private final SQLParamBean.Type type;
	private final String name;
	private String formattedValue;
	private String rawName;

	/**
	 * @param specs
	 *            Les caractéristiques du paramètre
	 * @param name
	 *            Le libellé du paramètre
	 * @throws CktlRestParseException
	 *             si une erreur avec les caractéritiques lors de la création du paramètre
	 */
	public SQLParamBean(String specs, String name) throws CktlRestParseException {
		checkCompatibiliteSpecs(specs);
		this.name = name;
		this.type = getType(specs.charAt(0));
		this.unique = UNIQUE == specs.charAt(1);
		this.caractereConditionnel = specs.charAt(2);
	}

	private void checkCompatibiliteSpecs(String specs) throws CktlRestParseException {
		if (!SPECS_WHITE_LIST.contains(specs)) {
			throw new CktlRestParseException("Les caractéristiques (" + specs + ") du paramètre sont invalides.");
		}
	}

	protected SQLParamBean.Type getType(char c) {
		SQLParamBean.Type t = null;
		switch (c) {
		case 'I':
			t = Type.INTEGER;
			break;
		case 'N':
			t = Type.NUMERIC;
			break;
		case 'D':
			t = Type.DATE;
			break;
		case 'S':
		default:
			t = Type.STRING;
			break;
		}
		return t;
	}

	/**
	 * Vérifie et converti le paramètre, en fonction des données fournies
	 * 
	 * @param vars
	 *            La liste des valeurs du paramètre de la requête
	 * @throws CktlRestParseException
	 *             si exception lors du traitement du paramètre
	 */
	public void checkAndConvert(List<Object> vars) throws CktlRestParseException {
		if (vars == null) {
			// La liste des valeurs est null, on l'initialise alors vide
			vars = new ArrayList<Object>();
		}

		if (!checkUniciteRespectee(vars)) {
			throw new CktlRestParseException(String.format("Le paramètre '%s' ne doit pas avoir plusieurs valeurs.", getName()));
		}

		if (!checkCaractereObligatoire(vars)) {
			throw new CktlRestParseException(String.format("Le paramètre '%s' est obligatoire.", getName()));
		}

		List<String> convertedValues = convert(vars);
		if (convertedValues == null) {
			throw new CktlRestParseException(String.format("Le paramètre '%s' n'as pas pu être converti vers le type attendu.", getName()));
		}

		formattedValue = format(convertedValues);

	}

	protected boolean checkCaractereObligatoire(List<Object> vars) {
		if (isObligatoire()) {
			List<Object> cleanVars = new ArrayList<Object>(vars);
			cleanVars.removeAll(Collections.singletonList(null));
			return cleanVars.size() > 0;
		} else {
			return true;
		}
	}

	/**
	 * @param vars
	 *            La liste des valeurs du paramètre
	 * @return Une liste contenant les valeurs converties<br>
	 *         <code>null</code> si erreur de convertion d'une valeur
	 */
	protected List<String> convert(List<Object> vars) {
		List<String> convertedValuesList = new ArrayList<String>();
		try {
			for (Object currentVar : vars) {
				if (currentVar == null) {
					// TODO
					continue;
				}
				String varStr = currentVar.toString();
				switch (getType()) {
				case INTEGER:
					convertedValuesList.add(Integer.valueOf(varStr).toString());
					break;
				case NUMERIC:
					convertedValuesList.add(DECIMAL_FORMATTER.format(DECIMAL_FORMATTER.parse(varStr)));
					break;
				case DATE:
					String date = DATE_FORMATTER.format(DATE_FORMATTER.parse(varStr));
					convertedValuesList.add("to_date('" + date + "', 'DD/MM/YYYY')");
					break;
				default:
					convertedValuesList.add("'" + varStr + "'");
					break;
				}
			}
		} catch (NumberFormatException e) {
			convertedValuesList = null;
		} catch (ParseException e) {
			convertedValuesList = null;
		}
		return convertedValuesList;
	}

	protected String format(List<String> convertedValues) {
		String str = "";
		if (isUnique()) {
			str = formatUnique(convertedValues);
		} else {
			str = formatList(convertedValues);
		}
		return str;
	}

	protected String formatList(List<String> convertedValues) {
		if (convertedValues == null) {
			return "null";
		}
		List<String> cleanValues = new ArrayList<String>(convertedValues);
		cleanValues.removeAll(Collections.singleton(null));

		if (cleanValues.isEmpty()) {
			return "null";
		}

		return StringUtils.join(convertedValues, ", ");
	}

	protected String formatUnique(List<String> convertedValues) {
		if (isNullable()) {
			return formatUniqueNullable(convertedValues);
		}
		if (isPartiel()) {
			return formatUniquePartiel(convertedValues);
		}
		return convertedValues.get(0);
	}

	private String formatUniqueNullable(List<String> convertedValues) {
		if (convertedValues == null) {
			return "null";
		}
		List<String> cleanValues = new ArrayList<String>(convertedValues);
		cleanValues.removeAll(Collections.singleton(null));

		if (cleanValues.isEmpty()) {
			return "null";
		}
		return convertedValues.get(0);
	}

	private String formatUniquePartiel(List<String> convertedValues) {
		String likeAll = "'%'";
		if (convertedValues == null) {
			return likeAll;
		}
		List<String> cleanValues = new ArrayList<String>(convertedValues);
		cleanValues.removeAll(Collections.singleton(null));

		if (cleanValues.isEmpty()) {
			return likeAll;
		}
		String value = convertedValues.get(0);
		if ("''".equals(value)) {
			return likeAll;
		}
		return "'%" + value.subSequence(1, value.length() - 1) + "%'";
	}

	protected boolean checkUniciteRespectee(List<Object> vars) {
		return !(isUnique() && vars.size() > 1);
	}

	public boolean isUnique() {
		return unique;
	}

	public boolean isObligatoire() {
		return OBLIGATOIRE == caractereConditionnel;
	}

	public boolean isNullable() {
		return NULLABLE == caractereConditionnel;
	}

	public boolean isPartiel() {
		return PARTIEL == caractereConditionnel;
	}

	protected char getCaractereConditionnel() {
		return caractereConditionnel;
	}

	public SQLParamBean.Type getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getFormattedValue() {
		return formattedValue;
	}

	protected void setFormattedValue(String formattedValue) {
		this.formattedValue = formattedValue;
	}

	public String getRawName() {
		return rawName;
	}

	public void setRawName(String rawName) {
		this.rawName = rawName;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getCaractereConditionnel();
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + (isUnique() ? 1231 : 1237);
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		SQLParamBean other = (SQLParamBean) obj;
		if (getCaractereConditionnel() != other.getCaractereConditionnel()) {
			return false;
		}
		if (getName() == null) {
			if (other.getName() != null) {
				return false;
			}
		} else if (!getName().equals(other.getName())) {
			return false;
		}
		if (getType() != other.getType()) {
			return false;
		}
		if (isUnique() != other.isUnique()) {
			return false;
		}
		return true;
	}

	/** {@inheritDoc} */
	public int compareTo(SQLParamBean o) {
		if (isSetable() != o.isSetable()) {
			return compareBool(isSetable(), o.isSetable());
		}
		if (isObligatoire() != o.isObligatoire()) {
			return compareBool(!isObligatoire(), !o.isObligatoire());
		}
		return getName().compareTo(o.getName());
	}

	private int compareBool(boolean b1, boolean b2) {
		return Boolean.valueOf(b1).compareTo(Boolean.valueOf(b2));
	}

	public boolean isSetable() {
		return !NON_SETABLE_PARAMS.contains(getName());
	}

}
