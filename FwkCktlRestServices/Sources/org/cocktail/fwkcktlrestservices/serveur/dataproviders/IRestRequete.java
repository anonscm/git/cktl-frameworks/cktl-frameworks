package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

public interface IRestRequete {

	public String categorie();

	public String commentaireDeveloppeur();

	public String commentaireUtilisateur();

	public String libelle();

	/**
	 * @return Un id qui sert à identifier le requete de maniere unique.
	 */
	public String strId();

}
