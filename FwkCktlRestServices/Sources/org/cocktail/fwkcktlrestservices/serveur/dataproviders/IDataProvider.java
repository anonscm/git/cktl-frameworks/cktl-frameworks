/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNoDataException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;

/**
 * Interface pour les fournisseurs de données
 */
public interface IDataProvider {

	/**
	 * @param stringId
	 *            L'identifiant de la requête demandée
	 * @return Le résultat de la requête
	 * @throws CktlRestNoDataException
	 *             Si la requête n'est pas trouvée
	 * @throws CktlRestParseException
	 *             Si erreur lors du parsing de la requête
	 */
	Collection<Map<String, Object>> getData(String stringId) throws CktlRestNoDataException, CktlRestParseException;

	/**
	 * @param stringId
	 *            L'identifiant de la requête demandée
	 * @param params
	 *            La liste des paramètres à injecter dans la requête
	 * @return Le résultat de la requête
	 * @throws CktlRestNoDataException
	 *             Si la requête n'est pas trouvée
	 * @throws CktlRestParseException
	 *             Si erreur lors du parsing de la requête
	 */
	Collection<Map<String, Object>> getData(String stringId, Map<String, List<Object>> params) throws CktlRestNoDataException, CktlRestParseException;

	/**
	 * @return La liste des identifiants des filtres disponibles
	 */
	Collection<String> getQueryIds();

	/**
	 * @return La liste des services disponibles
	 */
	Collection<? extends IRestRequete> getAvailableQueries();

	/**
	 * @param query
	 *            Un objet {@link IRestRequete} qui décrit la requête
	 * @return La liste des paramètres
	 */
	Collection<? extends IRestParamBean> getParamsForQuery(IRestRequete query);

	/**
	 * @param stringId
	 *            L'identifiant de la requête demandée
	 * @param params
	 *            La liste des paramètres à injecter dans la requête
	 * @return Le nombre de résultats de la requête
	 * @throws CktlRestNoDataException
	 *             Si la requête n'est pas trouvée
	 * @throws CktlRestParseException
	 *             Si erreur lors du parsing de la requête
	 */
	Long count(String stringId, Map<String, List<Object>> params) throws CktlRestNoDataException, CktlRestParseException;
}
