package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

public interface IRestSQLRequete extends IRestRequete {
	// Accessors methods
	public String sqlReq();
}
