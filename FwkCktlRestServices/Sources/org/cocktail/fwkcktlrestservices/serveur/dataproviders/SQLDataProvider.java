/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.FwkCktlRestServices;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNoDataException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;
import org.cocktail.fwkcktlrestservices.serveur.finders.FinderRestSQLRequete;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;

/**
 * Fournisseur de données issues de la BDD
 */
public class SQLDataProvider implements IDataProvider {

	private static final String ROW_ALIAS_CKTL_RN = "CKTL_RN";
	private static final String PARAM_PAGE_SIZE = "page.size";
	private static final String PARAM_PAGE = "page.num";
	private static final int DEFAULT_PAGE = 1;
	private static final String SQL_PAGINATION_WRAPPER = "select * from ( select /*+ first_rows(%2$d) */ a.*, rownum cktl_rn from ( %1$s ) a WHERE rownum < %2$d) b WHERE cktl_rn >= %3$d";
	private static final String SQL_COUNT_WRAPPER = "select count(*) nb_res from (%1$s)";

	/** {@inheritDoc} */
	public Collection<Map<String, Object>> getData(String stringId) throws CktlRestNoDataException, CktlRestParseException {
		return getData(stringId, Collections.EMPTY_MAP);
	}

	/** {@inheritDoc} */
	public Collection<Map<String, Object>> getData(String stringId, Map<String, List<Object>> params)
			throws CktlRestNoDataException, CktlRestParseException {
		EOEditingContext edc = ERXEC.newEditingContext();
		IRestSQLRequete requete = FinderRestSQLRequete.instance().findByStringId(edc, stringId);

		String sql = SQLParser.instance().parse(requete.sqlReq(), params);

		String newSql = transformSqlToPaginatedSql(params, sql);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(edc, FwkCktlRestServices.MODEL_ID, newSql, null);
		NSMutableArray<Map<String, Object>> results = new NSMutableArray<Map<String, Object>>();
		for (NSDictionary<String, Object> currentRow : rawRows) {
			NSMutableDictionary<String, Object> newRow = currentRow.mutableClone();
			newRow.removeObjectForKey(ROW_ALIAS_CKTL_RN);
			newRow.removeObjectForKey(ROW_ALIAS_CKTL_RN.toLowerCase());
			results.add(newRow.immutableClone());
		}
		return results;
	}
	
	/** {@inheritDoc} */
	public Long count(String stringId, Map<String, List<Object>> params) throws CktlRestNoDataException, CktlRestParseException {
		EOEditingContext edc = ERXEC.newEditingContext();
		IRestSQLRequete requete = FinderRestSQLRequete.instance().findByStringId(edc, stringId);

		String sql = SQLParser.instance().parse(requete.sqlReq(), params);

		String newSql = String.format(SQL_COUNT_WRAPPER, sql);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(edc, FwkCktlRestServices.MODEL_ID, newSql, null);
		Double res = (Double) rawRows.objectAtIndex(0).allValues().lastObject();
		return res.longValue();
	}

	protected String transformSqlToPaginatedSql(Map<String, List<Object>> params, String sql) {
		Integer page = extractIntegerParam(params, PARAM_PAGE, DEFAULT_PAGE);
		Integer pageSize = extractIntegerParam(params, PARAM_PAGE_SIZE, FwkCktlRestServices.getPaginationDefaultPageSize());
		
		int debut = (page - 1) * pageSize + 1;
		int fin = debut + pageSize;

		return String.format(SQL_PAGINATION_WRAPPER, sql, fin, debut);
	}

	protected Integer extractIntegerParam(Map<String, List<Object>> params, String paramName, final Integer defaultValue) {
		Integer value = defaultValue;
		if (params.get(paramName) != null && params.get(paramName).size() == 1) {
			try {
				value = Integer.valueOf((String) params.get(paramName).get(0));
				if (value < 1) {
					value = defaultValue;
				}
			} catch (NumberFormatException e) {
				// TODO un truc 
			}
		}
		return value;
	}

	/** {@inheritDoc} */
	public Collection<String> getQueryIds() {
		return FinderRestSQLRequete.instance().findAllIds(ERXEC.newEditingContext());
	}
	
	/** {@inheritDoc} */
	public Collection<IRestSQLRequete> getAvailableQueries() {
		return FinderRestSQLRequete.instance().findAll(ERXEC.newEditingContext());
	}

	/** {@inheritDoc} */
	public Collection<SQLParamBean> getParamsForQuery(IRestRequete query) {
		try {
			List<SQLParamBean> params = SQLParser.instance().analyseParams(((IRestSQLRequete) query).sqlReq());
			Collections.sort(params);
			params.add(new SQLParamBean("IUN", PARAM_PAGE));
			params.add(new SQLParamBean("IUN", PARAM_PAGE_SIZE));
			return params;
		} catch (CktlRestParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
