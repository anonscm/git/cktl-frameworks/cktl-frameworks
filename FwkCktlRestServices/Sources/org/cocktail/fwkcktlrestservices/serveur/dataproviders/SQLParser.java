package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;

public class SQLParser {

	public static final String REGEXP_PARAM_MATCHER = "\\$P_(\\w{3})\\{([\\w\\.]*)\\}";
	public static final String ERR_MSG_SEPARATOR = "\n";

	/** Commentaires en une ligne, qui commencent par "<code>//</code>" ou "<code>#</code>" ou "<code>--</code>" */
	private static final String PATTERN_INLINE_COMMENT = "\\s*(--|//|#).*[\\n\\r]+";
	/** Commentaires multilignes, du type "<code>&#47;* ... *&#47;</code>" */
	private static final String PATTERN_MULTILINE_COMMENT = "/\\*(.|\\s)*\\*/";

	private static final SQLParser INSTANCE = new SQLParser();

	public static SQLParser instance() {
		return INSTANCE;
	}

	public String parse(final String sql, Map<String, List<Object>> map) throws CktlRestParseException {
		checkRequeteSQL(sql);

		List<SQLParamBean> parametresSQL = analyseParams(sql);
		checkParams(parametresSQL, map);
		return replaceParams(sql, parametresSQL);
	}

	protected void checkRequeteSQL(final String sql) throws CktlRestParseException {
		if (sql == null) {
			throw new CktlRestParseException("La requête SQL est invalide.");
		}
		String cleanSql = sql;
		// On supprime les commentaires
		cleanSql = cleanSql.replaceAll(PATTERN_MULTILINE_COMMENT, "");
		cleanSql = cleanSql.replaceAll(PATTERN_INLINE_COMMENT, "");

		if (!cleanSql.trim().toUpperCase().startsWith("SELECT")) {
			throw new CktlRestParseException("La requête SQL doit commencer par SELECT. \"" + cleanSql + "\" :: \"" + sql + "\"");
		}
	}

	public List<SQLParamBean> analyseParams(String sqlSelect) throws CktlRestParseException {
		List<SQLParamBean> res = new ArrayList<SQLParamBean>();
		Pattern p = Pattern.compile(REGEXP_PARAM_MATCHER);
		Matcher m = p.matcher(sqlSelect);
		while (m.find()) {
			SQLParamBean aParam = new SQLParamBean(m.group(1), m.group(2));
			aParam.setRawName(m.group());
			if (res.indexOf(aParam) == -1) {
				res.add(aParam);
			}
		}
		return res;
	}

	protected void checkParams(List<SQLParamBean> parametresSQL, Map<String, List<Object>> map) throws CktlRestParseException {
		if (map == null) {
			throw new IllegalArgumentException("La map de paramètres ne doit pas être null");
		}

		StringBuilder exMsgBuilder = new StringBuilder();
		for (SQLParamBean currentParam : parametresSQL) {
			List<Object> vars = map.get(currentParam.getName());
			try {
				currentParam.checkAndConvert(vars);
			} catch (CktlRestParseException crpe) {
				exMsgBuilder.append(crpe.getMessage()).append(ERR_MSG_SEPARATOR);
			}
		}

		String exMsg = exMsgBuilder.toString();
		if (!StringUtils.isEmpty(exMsg)) {
			throw new CktlRestParseException(exMsg);
		}
	}

	protected String replaceParams(final String sqlOrigin, List<SQLParamBean> params) {
		String sqlResult = sqlOrigin;
		for (SQLParamBean currentParam : params) {
			sqlResult = sqlResult.replace(currentParam.getRawName(), currentParam.getFormattedValue());
		}
		return sqlResult;
	}
}
