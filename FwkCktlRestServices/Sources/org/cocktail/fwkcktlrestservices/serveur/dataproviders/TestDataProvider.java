/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNoDataException;

/**
 * Fournisseur de données de test
 */
public class TestDataProvider implements IDataProvider {

	private static final String STRING_ID_EXERCICE = "exercice";
	private static Collection<Map<String, Object>> EXERCICE_LIST = new ArrayList<Map<String, Object>>();

	static {
		EXERCICE_LIST.add(getExerciceData(2011));
		EXERCICE_LIST.add(getExerciceData(2012));
		EXERCICE_LIST.add(getExerciceData(2013));
	}

	private static HashMap<String, Object> getExerciceData(int exercice) {
		HashMap<String, Object> obj = new HashMap<String, Object>();
		obj.put("id", Integer.valueOf(exercice));
		obj.put("value", Integer.valueOf(exercice));
		return obj;
	}

	/** {@inheritDoc} */
	public Collection<Map<String, Object>> getData(String stringId) throws CktlRestNoDataException {
		Map<String, List<Object>> emptyParams = Collections.emptyMap();
		return getData(stringId, emptyParams);
	}

	/** {@inheritDoc} */
	public Collection<Map<String, Object>> getData(String stringId, Map<String, List<Object>> params)
			throws CktlRestNoDataException {
		if (STRING_ID_EXERCICE.equals(stringId)) {
			return EXERCICE_LIST;
		}
		throw new CktlRestNoDataException("L'identifiant " + stringId + " n'est pas reconnu");
	}

	/** {@inheritDoc} */
	public Collection<String> getQueryIds() {
		Collection<String> liste = new ArrayList<String>();
		liste.add(STRING_ID_EXERCICE);
		return liste;
	}

	/** {@inheritDoc} */
	public Collection<? extends IRestRequete> getAvailableQueries() {
		// TODO Auto-generated method stub
		return null;
	}

	/** {@inheritDoc} */
	public Collection<IRestParamBean> getParamsForQuery(IRestRequete query) {
		// TODO Auto-generated method stub
		return null;
	}

	/** {@inheritDoc} */
	public Long count(String stringId, Map<String, List<Object>> params) throws CktlRestNoDataException {
		// TODO Auto-generated method stub
		return null;
	}
}
