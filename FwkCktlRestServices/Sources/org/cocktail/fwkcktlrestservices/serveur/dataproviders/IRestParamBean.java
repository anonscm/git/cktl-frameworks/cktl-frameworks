package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

public interface IRestParamBean {
	public String getName();
	public boolean isObligatoire();
	public boolean isSetable();
}
