package org.cocktail.fwkcktlrestservices.serveur.authentification;

import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationFailureException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class SQLAuthProvider implements IAuthProvider {

	public String checkAuthentication(String authServiceUrl, String serviceUrl, String ticket) {
		throw new CktlRestAuthenticationFailureException("Mauvaise route !");
	}

	public Auth getAuthentication(Map<String, String> values) {
		String login = values.get(LOGIN);
		String password = values.get(PASSWORD);
		login = StringCtrl.normalize(login);

		if (login.length() == 0) {
			throw new CktlRestAuthenticationFailureException("Erreur d'authentification : login vide !");
		} else {
			if (password == null) {
				password = "";
			}
			CktlUserInfo loggedUserInfo = new CktlUserInfoDB(CKTL_APP.dataBus());
			loggedUserInfo.setAcceptEmptyPass(false);
			loggedUserInfo.compteForLogin(login, password, true);
			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
				throw new CktlRestAuthenticationFailureException("Erreur d'authentification : " + loggedUserInfo.errorMessage());
			}
			Auth auth = new Auth(loggedUserInfo.login(), loggedUserInfo.persId().intValue());
			return auth;
		}
	}
	
	/** {@inheritDoc} */	
	public String logoutUrl() {
		return "";
	}

	public WOActionResults preTraitements(WOContext context, Map<String, String> values) {
		return null;
	}

	public Boolean isExternalAuth() {
		return Boolean.FALSE;
	}

	public String authLibelle() {
		return "Authentification via la base de données";
	}

	public Boolean useCookies() {
		return Boolean.TRUE;

	}
}
