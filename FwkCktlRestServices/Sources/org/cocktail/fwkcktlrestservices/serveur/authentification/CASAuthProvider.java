/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.authentification;

import java.security.Principal;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationFailureException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.extensions.appserver.ERXRedirect;

/**
 * Gère la vérification de l'authentification avec CAS
 * 
 */
public class CASAuthProvider implements IAuthProvider {

	/** {@inheritDoc} */
	public String checkAuthentication(String authServiceUrl, String serviceUrl, String ticket) {
		Principal principal = new CASTicketValidator().validateTicket(authServiceUrl, serviceUrl, ticket);
		return principal.getName();
	}

	/** {@inheritDoc} */
	public Auth getAuthentication(Map<String, String> values) {
		Principal principal = new CASTicketValidator().validateTicket(getAuthServiceUrl(), values.get(SERVICE_URL), values.get(TICKET));
		String user = principal.getName();
		CktlUserInfo ui = initUserInfo(user);
		Auth auth = new Auth(user, ui.persId().intValue());
		return auth;
	}
	
	/** {@inheritDoc} */	
	public String logoutUrl() {
		return CktlWebAction.casServiceURL() + "logout?service=";
	}

	/** {@inheritDoc} */
	public WOActionResults preTraitements(WOContext context, Map<String, String> values) {
		if (values.get(TICKET) == null) {
			ERXRedirect redirectTo = new ERXRedirect(context);
			redirectTo.setUrl(getAuthServiceUrl() + "login?service=" + values.get(IAuthProvider.SERVICE_URL));
			return redirectTo;
		}
		return null;
	}

	public Boolean isExternalAuth() {
		return Boolean.TRUE;
	}

	public String authLibelle() {
		return "Authentification via un service CAS";
	}

	private String getAuthServiceUrl() {
		return CktlWebAction.casServiceURL();
	}

	private CktlUserInfoDB initUserInfo(String user) {
		if (CKTL_APP.config().booleanForKey("MODE_DEBUG")) {
			// FIXME Si besoin de tester via un autre CAS que celui lié à la base (ex: CAS ULR avec cockdev), on remplace un user par grhumadm
			user = user.replaceAll("jblandin", "grhumadm");
		}
		CktlUserInfoDB ui = new CktlUserInfoDB(CKTL_APP.dataBus());
		ui.compteForLogin(user, null, true);
		if (ui.hasError()) {
			throw new CktlRestAuthenticationFailureException("Utilisateur non reconnu : " + user);
		}
		return ui;
	}

	public Boolean useCookies() {
		return Boolean.FALSE;
	}

}
