/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.authentification;

import java.security.Principal;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAuthenticationFailureException;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;

/**
 * Gère la validation d'un ticket CAS en utilisant la bibliothèque JASIG
 */
public class CASTicketValidator {
	/**
	 * @param casServerUrl L'URL du service CAS (ou proxy TODO à vérifier)
	 * @param serviceUrl L'URL du service associé au ticket
	 * @param ticket Le ticket CAS
	 * @return Un objet {@link Principal} contenant les infos de l'utilisateur
	 */
	public final Principal validateTicket(String casServerUrl, String serviceUrl, String ticket) {
		AttributePrincipal principal = null;
		Cas20ProxyTicketValidator sv = new Cas20ProxyTicketValidator(casServerUrl);
		sv.setAcceptAnyProxy(true);
		try {
			// there is no need, that the legacy application is accessible
			// through this URL. But for validation purpose, even a non-web-app
			// needs a valid looking URL as identifier.
			Assertion a = sv.validate(ticket, serviceUrl);
			principal = a.getPrincipal();
			System.out.println("user name:" + principal.getName());
			return principal;
		} catch (TicketValidationException e) {
			// e.printStackTrace();
			throw new CktlRestAuthenticationFailureException("Ticket non reconnu ou non spécifié", e);
		}
	}
}
