/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrestservices.serveur.authentification;

import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;

/**
 * Interface de vérification de l'authentification.
 */
public interface IAuthProvider {
	public static final CktlWebApplication CKTL_APP = (CktlWebApplication) WOApplication.application();
	public static final String SERVICE_URL = "serviceUrl";
	public static final String TICKET = "ticket";
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";

	/**
	 * @param authServiceUrl
	 *            L'URL du service CAS (ou proxy TODO à vérifier)
	 * @param serviceUrl
	 *            L'URL du service associé au ticket
	 * @param ticket
	 *            Le ticket
	 * @return Le login de l'utilisateur
	 * @deprecated Utiliser la méthode {@link IAuthProvider#getAuthentication(Map)}
	 */
	@Deprecated
	String checkAuthentication(String authServiceUrl, String serviceUrl, String ticket);

	/**
	 * @param values
	 *            Une {@link Map} de clés/valeurs utiles pour le provider
	 * @return Un objet {@link Auth}
	 */
	Auth getAuthentication(Map<String, String> values);

	/**
	 * @return L'URL du logout
	 */
	String logoutUrl();
	
	/**
	 * Permet de faire une éventuelle redirection avant même de faire le traitement interne d'authentification
	 * 
	 * @param context
	 * @param values
	 *            Une {@link Map} de clés/valeurs utiles pour le provider
	 * @return
	 */
	WOActionResults preTraitements(WOContext context, Map<String, String> values);

	/**
	 * 
	 * @return <code>true</code> si l'authentification est gérée par un service externe (genre CAS, Shibboleth, etc.)
	 */
	Boolean isExternalAuth();
	
	/**
	 * @return Le libellé de l'authentification
	 */
	String authLibelle();

	Boolean useCookies();
}
