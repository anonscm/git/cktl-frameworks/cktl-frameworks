package org.cocktail.fwkcktlrestservices.serveur.authentification.context;

import org.apache.commons.lang.Validate;

public class ThreadLocalAuthContextHolderStrategy {
    
    // ~ Static fields/initializers
    // =====================================================================================

    private static final ThreadLocal<AuthContext> contextHolder = new ThreadLocal<AuthContext>();

    // ~ Methods
    // ========================================================================================================

    public void clearContext() {
        contextHolder.remove();
    }

    public AuthContext getContext() {
        AuthContext ctx = contextHolder.get();

        if (ctx == null) {
            ctx = createEmptyContext();
            contextHolder.set(ctx);
        }

        return ctx;
    }

    public void setContext(AuthContext context) {
        Validate.notNull(context, "Only non-null AuthCOntext instances are permitted");
        contextHolder.set(context);
    }

    public AuthContext createEmptyContext() {
        return new AuthContext();
    }
}

