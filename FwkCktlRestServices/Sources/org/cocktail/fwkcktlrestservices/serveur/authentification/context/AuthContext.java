package org.cocktail.fwkcktlrestservices.serveur.authentification.context;

import org.cocktail.fwkcktlrestservices.serveur.model.Auth;

public class AuthContext {

	//~ Instance fields ================================================================================================

    private Auth authentication;

    //~ Methods ========================================================================================================

    public boolean equals(Object obj) {
        if (obj instanceof AuthContext) {
            AuthContext test = (AuthContext) obj;

            if ((this.getAuthentication() == null) && (test.getAuthentication() == null)) {
                return true;
            }

            if ((this.getAuthentication() != null) && (test.getAuthentication() != null)
                && this.getAuthentication().equals(test.getAuthentication())) {
                return true;
            }
        }

        return false;
    }

    public Auth getAuthentication() {
        return authentication;
    }

    public int hashCode() {
        if (this.authentication == null) {
            return -1;
        } else {
            return this.authentication.hashCode();
        }
    }

    public void setAuthentication(Auth authentication) {
        this.authentication = authentication;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());

        if (this.authentication == null) {
            sb.append(": Null authentication");
        } else {
            sb.append(": Authentication: ").append(this.authentication);
        }

        return sb.toString();
    }
	
}
