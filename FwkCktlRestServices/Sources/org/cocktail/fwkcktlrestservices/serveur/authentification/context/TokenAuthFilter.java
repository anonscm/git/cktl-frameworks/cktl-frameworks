package org.cocktail.fwkcktlrestservices.serveur.authentification.context;

import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.AUTH_KEY;
import static org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService.HEADER_AUTH_KEY;

import org.apache.log4j.MDC;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.cocktail.fwkcktlrestservices.serveur.services.AuthenticationService;
import org.cocktail.fwkcktlwebapp.server.filter.AbstractDefaultFilter;

import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

import er.extensions.foundation.ERXStringUtilities;

public class TokenAuthFilter extends AbstractDefaultFilter {

	private static final String MDC_USERNAME = "username";
	private static final String USERNAME_ANONYME = "anonyme";
	
    private AuthenticationService authService;
    
    public TokenAuthFilter() {
    	this.authService = new AuthenticationService();
    }
    
    @Override
	public void doPreFilter(WORequest request) {
    	super.doPreFilter(request);
		try {
			String authKey = getAuthKey(request);
			Auth authDetails = authService.parseAuthKey(authKey);
			String username = USERNAME_ANONYME;
			if (authDetails != null) {
				AuthContextHolder.getContext().setAuthentication(authDetails);
				username = authDetails.getUser();
			}
			MDC.put(MDC_USERNAME, username);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	@Override
	public void doPostFilter(WOResponse response) {
		super.doPostFilter(response);
		AuthContextHolder.clearContext();
		MDC.remove(MDC_USERNAME);
	}

	protected String getAuthKey(WORequest request) {
		String authKey = (String) request.formValueForKey(AUTH_KEY);
		if (ERXStringUtilities.stringIsNullOrEmpty(authKey)) {
			authKey = request.headerForKey(HEADER_AUTH_KEY);
		}
		
		if (ERXStringUtilities.stringIsNullOrEmpty(authKey)) {
			authKey = request.cookieValueForKey(AUTH_KEY);
		}
		return authKey;
	}
}
