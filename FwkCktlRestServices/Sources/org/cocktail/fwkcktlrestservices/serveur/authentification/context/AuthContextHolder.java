package org.cocktail.fwkcktlrestservices.serveur.authentification.context;

import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import static org.apache.commons.lang.Validate.notNull;

public class AuthContextHolder {

	//~ Static fields/initializers =====================================================================================

    public static final String MODE_THREADLOCAL = "MODE_THREADLOCAL";
    private static ThreadLocalAuthContextHolderStrategy strategy;
    private static int initializeCount = 0;

    static {
        initialize();
    }

    //~ Methods ========================================================================================================

    /**
     * Explicitly clears the context value from the current thread.
     */
    public static void clearContext() {
        strategy.clearContext();
    }

    /**
     * Obtain the current <code>AuthContext</code>.
     *
     * @return the security context (never <code>null</code>)
     */
    public static AuthContext getContext() {
        return strategy.getContext();
    }
    
    /**
     * @return Authentication form AuthContext
     */
    public static Auth getAuthentication() {
        Auth ret = getContext().getAuthentication(); 
        notNull(ret, "Auth non présente. L'annotation @LoggedIn doit être utilisée.");
        return ret;
    }
    
    /**
     * @return connected user ID
     */
    public static Integer getPersId() {
        Integer ret = getAuthentication().getPersId();
        notNull(ret);
        return ret;
    }

    /**
     * @return connected user userName
     */
    public static String getUser() {
        String ret = getAuthentication().getUser();
        notNull(ret);
        return ret;
    }

    /**
     * Primarily for troubleshooting purposes, this method shows how many times the class has re-initialized its
     * <code>SecurityContextHolderStrategy</code>.
     *
     * @return the count (should be one unless you've called {@link #setStrategyName(String)} to switch to an alternate
     *         strategy.
     */
    public static int getInitializeCount() {
        return initializeCount;
    }

    private static void initialize() {
        strategy = new ThreadLocalAuthContextHolderStrategy();
        initializeCount++;
    }

    /**
     * Associates a new <code>SecurityContext</code> with the current thread of execution.
     *
     * @param context the new <code>SecurityContext</code> (may not be <code>null</code>)
     */
    public static void setContext(AuthContext context) {
        strategy.setContext(context);
    }

    /**
     * Delegates the creation of a new, empty context to the configured strategy.
     */
    public static AuthContext createEmptyContext() {
        return strategy.createEmptyContext();
    }

    public String toString() {
        return "SecurityContextHolder[strategy='" + MODE_THREADLOCAL + "'; initializeCount=" + initializeCount + "]";
    }

	public static void clear() {
		strategy.clearContext();		
	}
	
}
