package org.cocktail.fwkcktlrestservices.serveur;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.cocktail.fwkcktlrestservices.serveur.annotations.ControllerPath;
import org.cocktail.fwkcktlrestservices.serveur.authentification.context.TokenAuthFilter;
import org.cocktail.fwkcktlwebapp.server.filter.Filter;
import org.cocktail.fwkcktlwebapp.server.filter.FilterChain;

import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

import er.rest.routes.ERXRoute;
import er.rest.routes.ERXRouteController;
import er.rest.routes.ERXRouteRequestHandler;
import er.rest.routes.jsr311.DELETE;
import er.rest.routes.jsr311.GET;
import er.rest.routes.jsr311.HttpMethod;
import er.rest.routes.jsr311.POST;
import er.rest.routes.jsr311.PUT;
import er.rest.routes.jsr311.Path;
import er.rest.routes.jsr311.Paths;

public class CktlRouteRequestHandler extends ERXRouteRequestHandler {

	private FilterChain filterChain;
	
	public CktlRouteRequestHandler(NameFormat format) {
		super(format);
		this.filterChain = new FilterChain();
		initFilters();
	}
	
	private void initFilters() {
		this.filterChain.registerFilter(new TokenAuthFilter());
	}

	@Override
	public void addRoutes(Class<? extends ERXRouteController> controllerClass) {
		myAddDeclaredRoutes(null, controllerClass, true);
	}
	
	@Override
	public WOResponse handleRequest(WORequest request) {
		applyPreFilters(request);
		WOResponse response = super.handleRequest(request);
		applyPostFilters(response);
		return response;
	}
	
	private void applyPreFilters(WORequest request) {
		filterChain.doPreFilter(request);
	}
	
	private void applyPostFilters(WOResponse response) {
		filterChain.doPostFilter(response);
	}
	
	public void registerFilter(Filter filter) {
		this.filterChain.registerFilter(filter);
	}

	protected void myAddDeclaredRoutes(String entityName, Class<? extends ERXRouteController> routeControllerClass, boolean addDefaultRoutesIfNoDeclaredRoutesFound) {
		boolean declaredRoutesFound = false;
		try {
			Method addRoutesMethod = routeControllerClass.getMethod("addRoutes", String.class, ERXRouteRequestHandler.class);
			addRoutesMethod.invoke(null, entityName, this);
			declaredRoutesFound = true;
		} catch (NoSuchMethodException e) {
			// ignore
		} catch (Throwable t) {
			throw new RuntimeException("Failed to add routes for " + routeControllerClass + ".", t);
		}

		for (Method routeMethod : routeControllerClass.getDeclaredMethods()) {
			String routeMethodName = routeMethod.getName();
			Path pathAnnotation = routeMethod.getAnnotation(Path.class);
			Paths pathsAnnotation = routeMethod.getAnnotation(Paths.class);
			if (pathAnnotation != null || pathsAnnotation != null) {
				String actionName;
				if (routeMethodName.endsWith("Action")) {
					actionName = routeMethodName.substring(0, routeMethodName.length() - "Action".length());
				} else {
					actionName = routeMethodName;
				}

				ERXRoute.Method method = null;
				for (Annotation annotation : routeMethod.getAnnotations()) {
					HttpMethod httpMethod = annotation.annotationType().getAnnotation(HttpMethod.class);
					if (httpMethod != null) {
						if (method == null) {
							method = httpMethod.value();
						} else {
							throw new IllegalArgumentException(routeControllerClass.getSimpleName() + "." + routeMethod.getName() + " is annotated as more than one http method.");
						}
					}
				}
				if (method == null) {
					method = ERXRoute.Method.Get;
				}

				Annotation methodAnnotation = routeMethod.getAnnotation(GET.class);
				if (methodAnnotation == null) {
					methodAnnotation = routeMethod.getAnnotation(POST.class);
					if (methodAnnotation == null) {
						methodAnnotation = routeMethod.getAnnotation(PUT.class);
						if (methodAnnotation == null) {
							methodAnnotation = routeMethod.getAnnotation(DELETE.class);
						}
					}
				}
				if (methodAnnotation != null) {
					method = methodAnnotation.annotationType().getAnnotation(HttpMethod.class).value();
				}

				ControllerPath controllerPathAnnotation = routeControllerClass.getAnnotation(ControllerPath.class);
				String controllerPath = controllerPathAnnotation == null ? null : controllerPathAnnotation.value();

				if (pathAnnotation != null) {
					String pathValue = pathAnnotation.value();
					if (controllerPath != null) {
						pathValue = controllerPath + pathValue;
					}
					addRoute(new ERXRoute(entityName, pathValue, method, routeControllerClass, actionName));
					addRoute(new ERXRoute(entityName, pathValue, ERXRoute.Method.Options, routeControllerClass, "options"));
					
					declaredRoutesFound = true;
				}
				if (pathsAnnotation != null) {
					for (Path path : pathsAnnotation.value()) {
						String pathValue = path.value();
						if (controllerPath != null) {
							pathValue = controllerPath + pathValue;
						}
						addRoute(new ERXRoute(entityName, pathValue, method, routeControllerClass, actionName));
						addRoute(new ERXRoute(entityName, pathValue, ERXRoute.Method.Options, routeControllerClass, actionName));
					}
					declaredRoutesFound = true;
				}
			}
		}

		if (addDefaultRoutesIfNoDeclaredRoutesFound && !declaredRoutesFound) {
			ERXRouteRequestHandler.log.warn("No 'addRoutes(entityName, routeRequetHandler)' method and no @Path designations found on '" + routeControllerClass.getSimpleName()
					+ "'. Registering default routes instead.");
			addDefaultRoutes(entityName, routeControllerClass);
		}
	}

}
