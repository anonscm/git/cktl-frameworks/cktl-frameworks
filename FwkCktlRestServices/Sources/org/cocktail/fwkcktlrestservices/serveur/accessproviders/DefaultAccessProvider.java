package org.cocktail.fwkcktlrestservices.serveur.accessproviders;

import org.cocktail.fwkcktldroitsutils.common.finders.EOUtilisateurFinder;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAccessNotGrantedException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;

import er.extensions.eof.ERXEC;

public class DefaultAccessProvider implements IAccessProvider {

	public void checkAccess(Auth auth, String resourceId) throws SecurityException {
		EOUtilisateur utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(ERXEC.newEditingContext(), auth.getPersId());
		if (utilisateur == null) {
			throw new CktlRestAccessNotGrantedException("Accès à la ressource " + resourceId + " refusé pour l'utilisateur " + auth.getUser());
		}
	}
}
