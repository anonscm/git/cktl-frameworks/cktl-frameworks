package org.cocktail.fwkcktlrestservices.serveur.finders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlrestservices.serveur.dataproviders.IRestSQLRequete;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNoDataException;
import org.cocktail.fwkcktlrestservices.serveur.metier.EORestRequete;
import org.cocktail.fwkcktlrestservices.serveur.metier.EORestRequeteLocal;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;

public class FinderRestSQLRequete {

	private static FinderRestSQLRequete INSTANCE = new FinderRestSQLRequete();

	public static final FinderRestSQLRequete instance() {
		return INSTANCE;
	}

	private FinderRestSQLRequete() {
	}

	/**
	 * Retourne la requete locale si elle existe ; sinon la requete Cocktail.
	 *
	 * @param editingContext editing context.
	 * @param stringId identifiant (STR_ID) de la requete.
	 * @return la requete locale si elle existe ; sinon la requete Cocktail.
	 * @throws CktlRestNoDataException si aucune requete trouvée pour l'identifiant donné.
	 */
	public IRestSQLRequete findByStringId(EOEditingContext editingContext, String stringId) throws CktlRestNoDataException {
		IRestSQLRequete restRequete = EORestRequeteLocal.fetchByKeyValue(
				editingContext, EORestRequeteLocal.STR_ID_KEY, stringId);
		if (restRequete == null) {
			restRequete = EORestRequete.fetchByKeyValue(editingContext, EORestRequete.STR_ID_KEY, stringId);
		}

		if (restRequete == null) {
			throw new CktlRestNoDataException("L'identifiant " + stringId + " n'est pas reconnu");
		}
		editingContext.invalidateObjectsWithGlobalIDs(
				new NSArray<EOGlobalID>(editingContext.globalIDForObject((EOEnterpriseObject) restRequete)));
		return restRequete;
	}

	public List<String> findAllIds(EOEditingContext editingContext) {
		NSArray<EORestRequeteLocal> local_list = EORestRequeteLocal.fetchAll(editingContext);
		List<String> local_ids = (List<String>) local_list.valueForKey(EORestRequeteLocal.STR_ID_KEY);

		NSArray<EORestRequete> cktl_list = EORestRequete.fetchAll(editingContext);
		List<String> cktl_ids = (List<String>) cktl_list.valueForKey(EORestRequete.STR_ID_KEY);

		Set<String> setIds = new HashSet<String>(local_ids);
		setIds.addAll(cktl_ids);
		return new ArrayList<String>(setIds);
	}

	/**
	 * @param editingContext
	 * @return La liste de toutes les requetes trouvées (si des requetes sont surchargees dans les requetes locales, les requetes cocktail ne sont pas
	 *         renvoyees).
	 */
	public List<IRestSQLRequete> findAll(EOEditingContext editingContext) {
		Map<String, IRestSQLRequete> alls = new HashMap<String, IRestSQLRequete>();
		NSArray<EORestRequete> cktl_list = EORestRequete.fetchAll(editingContext);
		Iterator<EORestRequete> listIt = cktl_list.iterator();
		while (listIt.hasNext()) {
			EORestRequete eoRestRequete = (EORestRequete) listIt.next();
			alls.put(eoRestRequete.strId(), eoRestRequete);
		}

		NSArray<EORestRequeteLocal> cktl_locallist = EORestRequeteLocal.fetchAll(editingContext);
		Iterator<EORestRequeteLocal> listLocalIt = cktl_locallist.iterator();
		while (listLocalIt.hasNext()) {
			EORestRequeteLocal eoRestRequete = (EORestRequeteLocal) listLocalIt.next();
			alls.put(eoRestRequete.strId(), eoRestRequete);
		}
		return new ArrayList<IRestSQLRequete>(alls.values());
	}
}
