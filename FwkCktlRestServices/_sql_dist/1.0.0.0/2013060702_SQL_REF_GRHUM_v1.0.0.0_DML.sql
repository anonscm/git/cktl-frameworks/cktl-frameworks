DECLARE
    sqlReq VARCHAR2(32767);
BEGIN

    delete from GRHUM.REST_REQUETE;

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.exercice.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.exercice.list', 'Liste des exercices', 'GFC', 'Liste des exercices existant en GFC classés par ordre décroissant',
        'Liste des exercices existant', 'select exe_ordre as id, exe_exercice as value from jefy_admin.exercice order by exe_exercice desc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.convention.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval,
       'cocktail.gfc.filter.convention.list',
       'Liste des conventions',
       'GFC',
       'Liste des conventions classées par référence externe',
       'Liste des conventions existantes',
       'select con_ordre as ID, con_reference_externe as VALUE from jefy_report.v_convention where exe_ordre = $P_IUO{exercice.id} order by con_reference_externe asc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.organigramme_etab.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_etab.list', 'Liste des établissements budgétaires', 'GFC', 'Liste des établissements budgétaires. Il faut spécifier l''exercice et l''utilisateur',
        'Liste des établissements budgétaires',
        'select DISTINCT etab.org_id_etab as id, o.org_etab as value
    from jefy_admin.v_etab_for_organ etab
    inner join jefy_admin.utilisateur_organ uo on (etab.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (etab.org_id_etab=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    order by o.org_etab asc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.organigramme_ub.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
   (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
    COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_ub.list', 'Liste des unités budgétaires', 'GFC', 'Liste des unités budgétaires. Il faut spécifier l''exercice, l''utilisateur et l''établissement budgétaire parent',
        'Liste des unités budgétaires',
        'select DISTINCT ub.org_id_ub as id, o.org_ub as value
    from jefy_admin.v_ub_for_organ ub
    inner join jefy_admin.utilisateur_organ uo on (ub.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (ub.org_id_ub=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    and o.org_pere=$P_IUO{organigramme_etab.id}
    order by o.org_ub asc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.organigramme_cr.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_cr.list', 'Liste des centres de responsabilité budgétaires', 'GFC', 'Liste des centres de responsabilité budgétaires. Il faut spécifier l''exercice, l''utilisateur et l''unité budgétaire parente',
        'Liste des centres de responsabilité',
        'select DISTINCT cr.org_id_cr as id, o.org_cr as value
    from jefy_admin.v_cr_for_organ cr
    inner join jefy_admin.utilisateur_organ uo on (cr.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (cr.org_id_cr=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    and o.org_pere=$P_IUO{organigramme_ub.id}
    order by o.org_cr asc');

    -------------------------------------------------------------------
    --  cocktail.gfc.filter.organigramme_souscr.list
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.filter.organigramme_souscr.list', 'Liste des sous-centres de responsabilité budgétaires', 'GFC', 'Liste des sous-centres de responsabilité budgétaires. Il faut spécifier l''exercice, l''utilisateur et le centre de responsabilité parent',
        'Liste des centres de responsabilité',
        'select DISTINCT cr.org_id as id, o.org_souscr as value
    from jefy_admin.organ cr
    inner join jefy_admin.utilisateur_organ uo on (cr.org_id=uo.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre)
    inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id)
    inner join jefy_admin.organ o on (cr.org_id=o.org_id)
    where u.pers_id=$P_IUO{utilisateur.persid}
    and o1.exe_ordre=$P_IUO{exercice.id}
    and o.org_pere=$P_IUO{organigramme_cr.id}
    order by o.org_souscr asc');

    -------------------------------------------------------------------
    --  cocktail.gfc.query.situationbudgetaire.depense.synthese
    -------------------------------------------------------------------
    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
   (GRHUM.REST_REQUETE_seq.nextval,
    'cocktail.gfc.query.situationbudgetaire.depense.synthese',
    'Situation budgétaire en dépense par masse de crédit',
    'GFC',
    'Renvoie les montants crédits ouverts, restes engagés, mandatés, disponibles par masse de crédit. Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.',
    'Situation budgétaire en dépense par masse de crédit',
    'select tcd_libelle LIBELLE_CREDITS, tcd_code CREDITS, sum(bdxc_ouverts) montant_credits_ouverts, sum(bdxc_engagements) montant_engage, sum(bdxc_mandats) montant_mandate, sum(bdxc_disponible) montant_disponible
       from (select o.org_id, tcd_libelle, tcd_code, bdxc_ouverts, bdxc_engagements, bdxc_mandats, bdxc_disponible
               from (
                --suivi avec et sans convention (con_ordre fixé à -1000)
                select vo.org_id,
                vo.exe_ordre,
                -1000 as con_ordre,
                vbec.tcd_ordre,
                bdxc_ouverts,
                bdxc_engagements,
                bdxc_mandats,
                bdxc_disponible
                from jefy_admin.v_organ vo
                inner join jefy_depense.v_budget_exec_credit vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
                union all
                -- suivi par convention
                select vo.org_id,
                vo.exe_ordre,
                vbec.con_ordre,
                vbec.tcd_ordre,
                vbec.bdxc_ouverts,
                nvl(eng.bdxc_engagements,0) bdxc_engagements,
                nvl(mand.bdxc_mandats,0) bdxc_mandats,
                nvl(vbec.bdxc_ouverts-(bdxc_engagements+bdxc_mandats),0) as bdxc_disponible
                from jefy_admin.v_organ vo
                inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
                left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(ECON_MONTANT_BUDGETAIRE_RESTE) as bdxc_engagements from accords.v_suivi_eng p group by org_id, con_ordre, exe_ordre, tcd_ordre) eng on (eng.con_ordre = vbec.con_ordre and eng.org_id=vbec.org_id and eng.exe_ordre=vbec.exe_ordre and eng.tcd_ordre=vbec.tcd_ordre)
                left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(DCON_MONTANT_BUDGETAIRE) as bdxc_mandats from accords.v_suivi_dep p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
            ) x
            inner join jefy_admin.organ o on (x.org_id=o.org_id)
            inner join jefy_depense.v_type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre)
            inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
            inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
            where u.pers_id=$P_IUO{utilisateur.persid}
              and x.exe_ordre=$P_IUO{exercice.id}
              and o.org_id in (
                select $P_IUO{organigramme.id} from dual
                union all
                select org.org_id from jefy_admin.organ org
                 start with org.org_pere in (
                    select org_id from jefy_admin.organ tmpO
                     where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                   connect by prior org.org_id = org.org_pere)
              and x.con_ordre=nvl($P_IUN{convention.id}, -1000)
            order by org_id) innerQuery
     group by tcd_code, tcd_libelle
     order by tcd_code');

    -------------------------------------------------------------------
    --  cocktail.gfc.query.situationbudgetaire.recette.synthese
    -------------------------------------------------------------------
    sqlReq := 'select tcd_libelle LIBELLE_CREDITS, tcd_code CREDITS, sum(bdxc_ouverts) MONTANT_CREDITS_OUVERTS, sum(bdxc_titres) MONTANT_TITRE, sum(bdxc_ouverts) - sum(bdxc_titres) MONTANT_RESTE_A_REALISER
       from (select o.org_id, con_ordre, tcd_libelle, tcd_code, bdxc_ouverts, bdxc_titres, nvl(bdxc_ouverts, 0) - nvl(bdxc_titres,0)
               from (
                    --suivi avec et sans convention (con_ordre fixé à -1000)
                    select vo.org_id,
                    vo.exe_ordre,
                    -1000 as con_ordre,
                    bvg.tcd_ordre ,
                    nvl(BDVN_OUVERTS,0) as bdxc_ouverts,
                    nvl(bdxc_titres,0) as bdxc_titres
                    from jefy_admin.v_organ vo
                    inner join (select org_id, exe_ordre, tcd_ordre, sum(BDVN_OUVERTS) BDVN_OUVERTS from jefy_budget.budget_vote_nature group by org_id, exe_ordre, tcd_ordre) bvg on (bvg.org_id = vo.org_id and bvg.exe_ordre=vo.exe_ordre)
                    inner join jefy_admin.type_credit vtc on (bvg.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'' and tcd_code<>''00'')
                    full outer join (
                    select typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre,  sum(recPlanco.RPCO_HT_SAISIE ) as bdxc_titres
                        from jefy_recette.recette_ctrl_planco recPlanco
                        join jefy_recette.recette rec on rec.rec_id = recPlanco.rec_id
                        join jefy_recette.facture fac on rec.fac_id = fac.fac_id
                        join maracuja.titre titre on titre.tit_id = recPlanco.tit_id
                        join jefy_admin.type_credit typeCredit on typeCredit.tcd_ordre = fac.tcd_ordre
                        group by typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre
                    ) x on (x.org_id = vo.org_id and x.tcd_ordre=vtc.tcd_ordre and x.exe_ordre=vo.exe_ordre)
                    union all
                    -- suivi par convention
                    select vo.org_id,
                    vo.exe_ordre,
                    vbec.con_ordre,
                    vbec.tcd_ordre,
                    nvl(bdxc_ouverts,0) as bdxc_ouverts,
                    nvl(mand.bdxc_titres,0) bdxc_titres
                    from jefy_admin.v_organ vo
                    inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes_rec p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by  p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
                    full outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(RCON_MONTANT_BUDGETAIRE) as bdxc_titres from accords.v_suivi_rec p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
               ) x
              inner join jefy_admin.organ o on (x.org_id = o.org_id)
              inner join jefy_admin.type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'')
              inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
              inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
              where u.pers_id = $P_IUO{utilisateur.persid}
                and x.exe_ordre = $P_IUO{exercice.id}
                and o.org_id in (
                        select $P_IUO{organigramme.id} from dual
                         union all
                        select org.org_id
                          from jefy_admin.organ org
                         start with org.org_pere in (
                            select org_id from jefy_admin.organ tmpO
                             where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                       connect by prior org.org_id = org.org_pere )
                and x.con_ordre = nvl($P_IUN{convention.id}, -1000)
              order by org_id) innerQuery
     group by tcd_code, tcd_libelle
     order by tcd_code';

    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
       (GRHUM.REST_REQUETE_seq.nextval,
        'cocktail.gfc.query.situationbudgetaire.recette.synthese',
        'Situation budgétaire en recette par masse de crédit (Synthèse)',
        'GFC',
        'Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention  ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.',
        'Situation budgétaire en recette par masse de crédit (Synthèse)',
        sqlReq);

    -------------------------------------------------------------------
    --  cocktail.gfc.query.situationbudgetaire.depense.details
    -------------------------------------------------------------------
    sqlReq := 'select o.org_id ORGANIGRAMME_ID, o.org_etab ORGANIGRAMME_ETAB, o.org_ub ORGANIGRAMME_UB, o.org_cr ORGANIGRAMME_CR, o.org_souscr ORGANIGRAMME_SOUSCR, vtc.tcd_ordre TYPECREDIT_ID, vtc.tcd_libelle LIBELLE_CREDITS, vtc.tcd_code CREDITS, bdxc_ouverts MONTANT_CREDITS_OUVERTS, bdxc_engagements MONTANT_ENGAGE, bdxc_mandats MONTANT_MANDATE, bdxc_disponible MONTANT_DISPONIBLE
       from (
        --suivi avec et sans convention (con_ordre fixé à -1000)
        select vo.org_id,
        vo.exe_ordre,
        -1000 as con_ordre,
        vbec.tcd_ordre,
        bdxc_ouverts,
        bdxc_engagements,
        bdxc_mandats,
        bdxc_disponible
        from jefy_admin.v_organ vo
        inner join jefy_depense.v_budget_exec_credit vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
        union all
        -- suivi par convention
        select vo.org_id,
        vo.exe_ordre,
        vbec.con_ordre,
        vbec.tcd_ordre,
        vbec.bdxc_ouverts,
        nvl(eng.bdxc_engagements,0) bdxc_engagements,
        nvl(mand.bdxc_mandats,0) bdxc_mandats,
        nvl(vbec.bdxc_ouverts-(bdxc_engagements+bdxc_mandats),0) as bdxc_disponible
        from jefy_admin.v_organ vo
        inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
        left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(ECON_MONTANT_BUDGETAIRE_RESTE) as bdxc_engagements from accords.v_suivi_eng p group by org_id, con_ordre, exe_ordre, tcd_ordre) eng on (eng.con_ordre = vbec.con_ordre and eng.org_id=vbec.org_id and eng.exe_ordre=vbec.exe_ordre and eng.tcd_ordre=vbec.tcd_ordre)
        left outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(DCON_MONTANT_BUDGETAIRE) as bdxc_mandats from accords.v_suivi_dep p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
    ) x
    inner join jefy_admin.organ o on (x.org_id = o.org_id)
    inner join jefy_depense.v_type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre)
    inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
    inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
    where u.pers_id = $P_IUO{utilisateur.persid}
      and x.exe_ordre=$P_IUO{exercice.id}
      and o.org_id in (
            select $P_IUO{organigramme.id} from dual
            union all
            select org.org_id from jefy_admin.organ org
             start with org.org_pere in (
                select org_id from jefy_admin.organ tmpO
                 where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
               connect by prior org.org_id = org.org_pere)
      and x.con_ordre = nvl($P_IUN{convention.id}, -1000)
    order by o.org_id, tcd_code';

    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
   (GRHUM.REST_REQUETE_seq.nextval,
    'cocktail.gfc.query.situationbudgetaire.depense.details',
    'Situation budgétaire en dépense par ligne budgétaire et masse de crédit',
    'GFC',
    'Renvoie les montants crédits ouverts, restes engagés, mandatés, disponibles par ligne budgétaire et par masse de crédit. Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention  ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.',
    'Situation budgétaire en dépense par ligne budgétaire et masse de crédit', sqlReq);

    -------------------------------------------------------------------
    --  cocktail.gfc.query.situationbudgetaire.recette.details
    -------------------------------------------------------------------
    sqlReq := 'select o.org_id ORGANIGRAMME_ID, o.org_etab ORGANIGRAMME_ETAB, o.org_ub ORGANIGRAMME_UB, o.org_cr ORGANIGRAMME_CR, o.org_souscr ORGANIGRAMME_SOUSCR, o.org_lib ORGANIGRAMME_LIBELLE, tcd_libelle LIBELLE_CREDITS, tcd_code CREDITS, bdxc_ouverts MONTANT_CREDITS_OUVERTS, bdxc_titres MONTANT_TITRE, nvl(bdxc_ouverts, 0) - nvl(bdxc_titres, 0) MONTANT_RESTE_A_REALISER
       from (
            --suivi avec et sans convention (con_ordre fixé à -1000)
            select vo.org_id,
            vo.exe_ordre,
            -1000 as con_ordre,
            bvg.tcd_ordre ,
            nvl(BDVN_OUVERTS,0) as bdxc_ouverts,
            nvl(bdxc_titres,0) as bdxc_titres
            from jefy_admin.v_organ vo
            inner join (select org_id, exe_ordre, tcd_ordre, sum(BDVN_OUVERTS) BDVN_OUVERTS from jefy_budget.budget_vote_nature group by org_id, exe_ordre, tcd_ordre) bvg on (bvg.org_id = vo.org_id and bvg.exe_ordre=vo.exe_ordre)
            inner join jefy_admin.type_credit vtc on (bvg.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'' and tcd_code<>''00'')
            full outer join (
            select typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre,  sum(recPlanco.RPCO_HT_SAISIE ) as bdxc_titres
                from jefy_recette.recette_ctrl_planco recPlanco
                join jefy_recette.recette rec on rec.rec_id = recPlanco.rec_id
                join jefy_recette.facture fac on rec.fac_id = fac.fac_id
                join maracuja.titre titre on titre.tit_id = recPlanco.tit_id
                join jefy_admin.type_credit typeCredit on typeCredit.tcd_ordre = fac.tcd_ordre
                group by typeCredit.tcd_ordre, fac.org_id,titre.exe_ordre
            ) x on (x.org_id = vo.org_id and x.tcd_ordre=vtc.tcd_ordre and x.exe_ordre=vo.exe_ordre)
            union all
            -- suivi par convention
            select vo.org_id,
            vo.exe_ordre,
            vbec.con_ordre,
            vbec.tcd_ordre,
            nvl(bdxc_ouverts,0) as bdxc_ouverts,
            nvl(mand.bdxc_titres,0) bdxc_titres
            from jefy_admin.v_organ vo
            inner join (select p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre, sum(montant) as bdxc_ouverts from accords.v_credits_positionnes_rec p inner join accords.tranche t on (p.tra_ordre=t.tra_ordre) group by  p.org_id, t.con_ordre, p.exe_ordre, p.tcd_ordre ) vbec on (vbec.org_id = vo.org_id and vbec.exe_ordre=vo.exe_ordre)
            full outer join (select org_id, con_ordre, exe_ordre, tcd_ordre, sum(RCON_MONTANT_BUDGETAIRE) as bdxc_titres from accords.v_suivi_rec p group by org_id, con_ordre, exe_ordre, tcd_ordre) mand on (mand.con_ordre = vbec.con_ordre and mand.org_id=vbec.org_id and mand.exe_ordre=vbec.exe_ordre and mand.tcd_ordre=vbec.tcd_ordre)
       ) x
      inner join jefy_admin.organ o on (x.org_id = o.org_id)
      inner join jefy_admin.type_credit vtc on (x.tcd_ordre = vtc.tcd_ordre and vtc.tcd_type=''RECETTE'')
      inner join jefy_admin.utilisateur_organ uo on (uo.org_id = o.org_id)
      inner join jefy_admin.utilisateur u on (uo.utl_ordre = u.utl_ordre)
      where u.pers_id = $P_IUO{utilisateur.persid}
        and x.exe_ordre = $P_IUO{exercice.id}
        and o.org_id in (
                select $P_IUO{organigramme.id} from dual
                 union all
                select org.org_id
                  from jefy_admin.organ org
                 start with org.org_pere in (
                    select org_id from jefy_admin.organ tmpO
                     where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
               connect by prior org.org_id = org.org_pere )
        and x.con_ordre = nvl($P_IUN{convention.id}, -1000)
      order by o.org_id, tcd_code';

    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
   (GRHUM.REST_REQUETE_seq.nextval, 'cocktail.gfc.query.situationbudgetaire.recette.details', 'Situation budgétaire en recette par masse de crédit (Détails)', 'GFC', 'Il faut spécifier l''utilisateur, l''exercice, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.',
    'Situation budgétaire en recette par masse de crédit (Détails)', sqlReq);

    -------------------------------------------------------------------
    --  cocktail.gfc.query.situationbudgetaire.depense.mandats
    -------------------------------------------------------------------
    sqlReq :=  'select case when mandat.man_ttc < 0 then ''ORV'' else ''Mandat'' end TYPE_PIECE, mandat.man_numero NUMERO,
       (select rtrim(XMLAGG(XMLELEMENT("depNum", dpp.dpp_numero_facture, '', '').extract(''//text()'')), '', '')
          from jefy_depense.depense_papier dpp join jefy_depense.depense_budget dep on dpp.dpp_id = dep.dpp_id where dep_id = innerMandat.dep_id) LIBELLE,
       fournisseur.fou_nom FOURNISSEUR, innerMandat.mtht MONTANT_HT_LIQUIDE, innerMandat.mtttc MONTANT_TTC_LIQUIDE, innerMandat.mtbud MONTANT_BUDGETAIRE_LIQUIDE,
       case mandat.man_etat
        when ''ATTENTE'' then ''Mandaté le '' || bordereau.bor_date_creation
        when ''VISE'' then ''Visé le '' || bordereau.bor_date_visa
        when ''PAYE'' then ''Payé le '' || p.pai_date_creation
        when ''ANNULE'' then ''Annulé'' || bordereau.bor_date_visa
        else ''''
       end
     from (
        select man.man_id, engagement.fou_ordre, depense.dep_id, man.pai_ordre, sum(depPlanco.dpco_ht_saisie) mtht, sum(depPlanco.dpco_ttc_saisie) mtttc, sum(depPlanco.dpco_montant_budgetaire) mtbud
          from
            (-- suivi par convention
             select dep.eng_id, dep.dep_id, dep.con_ordre, dep.tcd_ordre, dep.org_id
               from accords.v_suivi_dep dep
              where dep.exe_ordre = $P_IUO{exercice.id}

            union all

            --suivi avec et sans convention (con_ordre fixé à -1000)
            select eng.eng_id, dep.dep_id, -1000, eng.tcd_ordre, eng.org_id
              from jefy_depense.depense_budget dep
              join jefy_depense.engage_budget eng on eng.eng_id = dep.eng_id
             where dep.exe_ordre = $P_IUO{exercice.id}
            ) depense
          join jefy_depense.engage_budget engagement on engagement.eng_id = depense.eng_id
          join jefy_depense.depense_ctrl_planco depPlanco on depPlanco.dep_id = depense.dep_id
          join maracuja.mandat man on man.man_id = depPlanco.man_id
          join jefy_admin.organ o on (o.org_id = depense.org_id)
          join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = depense.tcd_ordre
          join jefy_admin.utilisateur_organ uo on uo.org_id = o.org_id
          join jefy_admin.utilisateur u on uo.utl_ordre = u.utl_ordre
          where u.pers_id = $P_IUO{utilisateur.persid}
            and depense.con_ordre = nvl($P_IUN{convention.id}, -1000)
            and vtc.tcd_ordre = $P_IUO{typeCredit.id}
            and o.org_id in (
                  select $P_IUO{organigramme.id} from dual
                  union all
                  select org.org_id from jefy_admin.organ org
                   start with org.org_pere in (
                      select org_id from jefy_admin.organ tmpO
                       where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                     connect by prior org.org_id = org.org_pere)
          group by man.man_id, engagement.fou_ordre, depense.dep_id, man.pai_ordre) innerMandat
     join maracuja.mandat mandat on mandat.man_id = innerMandat.man_id
     join maracuja.bordereau bordereau on bordereau.bor_id = mandat.bor_id
     join jefy_depense.v_fournisseur fournisseur on fournisseur.fou_ordre = mandat.fou_ordre
     left join maracuja.paiement p on p.pai_ordre = innerMandat.pai_ordre';

    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
        (GRHUM.REST_REQUETE_seq.nextval,
         'cocktail.gfc.query.situationbudgetaire.depense.mandats',
         'Situation budgétaire en depense detaillée par mandat et par masse de crédit',
         'GFC',
         'Renvoie le montant HT liquidé, le montant TTC liquidé et le montant budgétaire liquidé, le status par mandat. Il faut spécifier l''utilisateur, l''exercice, la masse de crédit, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié.',
         'Situation budgétaire en depense detaillée par mandat et par masse de crédit', sqlReq);

    -------------------------------------------------------------------
    --  cocktail.gfc.query.situationbudgetaire.details.resteengage
    -------------------------------------------------------------------
    sqlReq :=  'select cmd.comm_numero CMDE_NUMERO, eng.eng_libelle ENGAGE_LIBELLE, fournisseur.fou_nom FOURNISSEUR, sum(eca.mtHt) , sum(eca.mtTTC), sum(eca.mtBUD), eng.eng_montant_budgetaire_reste,
                       decode(eng.eng_montant_budgetaire - eng.eng_montant_budgetaire_reste, 0, ''ENGAGE'', eng.eng_montant_budgetaire, ''SOLDE'', ''PARTIELLEMENT SOLDE'') status
                  from jefy_depense.engage_budget eng 
                  inner join(-- suivi par convention
                             select ec.eng_id, ec.con_ordre
                               from accords.v_suivi_eng ec
                              where ec.exe_ordre = $P_IUO{exercice.id}

                            union all

                            --suivi avec et sans convention (con_ordre fixé à -1000)
                            select e.eng_id, -1000
                              from jefy_depense.engage_budget e
                             where e.exe_ordre = $P_IUO{exercice.id}
                            ) engageConvention on engageConvention.eng_id = eng.eng_id
                  left join (select depense.eng_id, depense.con_ordre, sum(dpco.dpco_ht_saisie) mtHt, sum(dpco.dpco_ttc_saisie) mtTTC, sum(dpco.dpco_montant_budgetaire) mtBUD
                               from (-- suivi par convention
                                     select dep.eng_id, dep.dep_id, dep.con_ordre, dep.tcd_ordre, dep.org_id
                                       from accords.v_suivi_dep dep
                                      where dep.exe_ordre = $P_IUO{exercice.id}

                                    union all

                                    --suivi avec et sans convention (con_ordre fixé à -1000)
                                    select eng.eng_id, dep.dep_id, -1000, eng.tcd_ordre, eng.org_id
                                      from jefy_depense.depense_budget dep
                                      join jefy_depense.engage_budget eng on eng.eng_id = dep.eng_id
                                     where dep.exe_ordre = $P_IUO{exercice.id}
                                    ) depense
                               join jefy_depense.depense_ctrl_planco dpco on dpco.dep_id = depense.dep_id
                               join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = depense.tcd_ordre
                              where vtc.tcd_ordre = $P_IUO{typeCredit.id}
                                and depense.con_ordre = nvl($P_IUN{convention.id}, -1000)
                              group by depense.eng_id, depense.con_ordre) eca on eca.eng_id = eng.eng_id
                 inner join jefy_depense.v_fournisseur fournisseur on fournisseur.fou_ordre = eng.fou_ordre
                 inner join jefy_admin.organ o on (o.org_id = eng.org_id)
                 inner join jefy_depense.v_type_credit vtc on vtc.tcd_ordre = eng.tcd_ordre
                 inner join jefy_admin.utilisateur_organ uo on uo.org_id = o.org_id
                 inner join jefy_admin.utilisateur u on uo.utl_ordre = u.utl_ordre
                  left join jefy_depense.commande_engagement ce on ce.eng_id = eng.eng_id
                  left join jefy_depense.commande cmd on ce.comm_id = cmd.comm_id
                 where u.pers_id = $P_IUO{utilisateur.persid}
                   and eng.exe_ordre = $P_IUO{exercice.id}
                   and vtc.tcd_ordre = $P_IUO{typeCredit.id}
                   and engageConvention.con_ordre = nvl($P_IUN{convention.id}, -1000)
                   and o.org_id in (
                        select $P_IUO{organigramme.id} from dual
                        union all
                        select org.org_id from jefy_admin.organ org
                            start with org.org_pere in (
                                    select org_id from jefy_admin.organ tmpO
                                     where tmpO.org_id in ( select case when $P_SUN{inclure.sousniveaux} in (''1'', ''true'') then $P_IUO{organigramme.id} else null end from dual) )
                                   connect by prior org.org_id = org.org_pere)
                 group by cmd.comm_id, eng.eng_id, eng.eng_libelle, eng.eng_montant_budgetaire, eng.eng_montant_budgetaire_reste, fournisseur.fou_nom, cmd.comm_numero
                 order by cmd.comm_id';

    Insert into GRHUM.REST_REQUETE
       (ID_REST_REQUETE, STR_ID, LIBELLE, CATEGORIE, COMMENTAIRE_DEVELOPPEUR,
        COMMENTAIRE_UTILISATEUR, sql_req )
     Values
   (GRHUM.REST_REQUETE_seq.nextval,
    'cocktail.gfc.query.situationbudgetaire.details.resteengage',
    'Situation budgétaire en recette par masse de crédit (Reste à engager)',
    'GFC',
    'Renvoie le numéro de commande, le montant HT liquidé, le montant TTC liquidé et le montant budgétaire liquidé par commande / engagement. Il faut spécifier l''utilisateur, l''exercice, la masse de crédit, la branche de l''organigramme budgétaire et éventuellement la convention ainsi qu''un marqueur pour (ne pas) prendre en compte les sous-niveaux de l''organigramme budgétaire spécifié',
    'Situation budgétaire en recette par masse de crédit (Reste à engager)',
    sqlReq);

    -------------------------------------------------------------------

    COMMIT;

END;
/
