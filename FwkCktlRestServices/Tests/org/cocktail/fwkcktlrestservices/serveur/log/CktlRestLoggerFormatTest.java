package org.cocktail.fwkcktlrestservices.serveur.log;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CktlRestLoggerFormatTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testMasquePassword() {
		String chaine = "toto/?login=titi&password=asupprimer";
		String chaineClaire = "toto/?login=titi&password=" + CktlRestLoggerFormat.PASSWORD_MASQUE;

		assertEquals(chaineClaire, CktlRestLoggerFormat.masquePassword(chaine, "asupprimer"));

		chaine = "toto/?login=titi&password=asupprimer&tata";
		chaineClaire = "toto/?login=titi&password=" + CktlRestLoggerFormat.PASSWORD_MASQUE + "&tata";
		assertEquals(chaineClaire, CktlRestLoggerFormat.masquePassword(chaine, "asupprimer"));

	}

}
