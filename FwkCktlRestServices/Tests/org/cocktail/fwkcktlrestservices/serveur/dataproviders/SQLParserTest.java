package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;
import org.junit.Test;

public class SQLParserTest {

	private static final String SQL_TEST = new StringBuilder()
			.append("select DISTINCT etab.org_id_ub as id, o.org_ub as value ")
			.append("  from jefy_admin.organ cr")
			.append(" inner join jefy_admin.utilisateur_organ uo on (cr.org_id=uo.org_id) ")
			.append(" inner join jefy_admin.utilisateur u on (uo.utl_ordre=u.utl_ordre) ")
			.append(" inner join jefy_admin.v_organ o1 on (o1.org_id=uo.org_id) ")
			.append(" inner join jefy_admin.organ o on (cr.org_id_ub=o.org_id) ")
			.append(" where u.pers_id=$P_IUO{utilisateur.persid} ")
			.append("   and o1.exe_ordre=$P_IUO{exercice.id} ")
			.append("   and o.org_pere=$P_IUO{organigramme_cr.id} ")
			.append(" order by o.org_souscr asc ")
			.toString();

	private static final String SQL_TEST_SANS_PARAM = "select 1 from dual";
	private static final String SQL_TEST_AVEC_PARAMS = "select * from table where c1 = $P_IUO{p1.id} and c2 in $P_SLO{p2.id} and c3 = $P_DUN{p3.id} and c4 = $P_NUO{p4.id}";
	private static final String SQL_TEST_AVEC_PARAMS_DOUBLONS = "select * from table where c1 = $P_IUO{p1.id} and c2 in $P_SLO{p2.id} and c3 = $P_IUO{p1.id}";
	private static final String SQL_TEST_AVEC_PARAMS_FAUX_DOUBLONS = "select * from table where c1 = $P_IUO{p1.id} and c2 in $P_SLO{p2.id} and c3 = $P_SUO{p1.id}";

	private static final String SQL_TEST_PARSING = "select * from table where c1 = $P_IUO{p1.id} and c2 in ($P_SLO{p2.id}) and c3 = $P_DUO{p3.id} and c4 = $P_NUO{p4.id} and c5 = $P_SUP{p5.id} and c6 = $P_SUP{p6.id} and c7 = $P_SUP{p7.id}";
	private static final String SQL_TEST_PARSING_RESULT = "select * from table where c1 = 1234 and c2 in ('a', 'b') and c3 = to_date('21/12/2012', 'DD/MM/YYYY') and c4 = 1.23 and c5 = '%a%' and c6 = '%' and c7 = '%'";

	private static final String SQL_TEST_REPLACE_PARAMS = "select * from table where c1 = $P_IUO{p1.id} and c2 = $P_SUO{p2.id} and c3 in ($P_SLO{p3.id}) and c4 = $P_DUO{p4.id} and c5 = $P_IUO{p1.id} and c6 = $P_SUN{p6.id}";
	private static final String SQL_TEST_REPLACE_PARAMS_RESULT = "select * from table where c1 = 1234 and c2 = 'a' and c3 in ('a', 'b') and c4 = to_date('21/12/2012', 'DD/MM/YYYY') and c5 = 1234 and c6 = null";

	@Test
	public void testParseSansParams() throws Exception {
		SQLParser parser = SQLParser.instance();
		String sql = parser.parse(SQL_TEST_SANS_PARAM, new HashMap<String, List<Object>>());
		assertNotNull(sql);
	}

	@Test
	public void testParseAvecParams() throws Exception {
		SQLParser parser = SQLParser.instance();

		Map<String, List<Object>> map = new HashMap<String, List<Object>>();
		map.put("p1.id", Arrays.asList((Object) "1234"));
		map.put("p2.id", Arrays.asList((Object) "a", (Object) "b"));
		map.put("p3.id", Arrays.asList((Object) "21/12/2012"));
		map.put("p4.id", Arrays.asList((Object) "1.23"));
		map.put("p5.id", Arrays.asList((Object) "a"));
		map.put("p6.id", Arrays.asList((Object) ""));
		map.put("p7.id", Arrays.asList((Object) null));

		String sql = parser.parse(SQL_TEST_PARSING, map);
		assertEquals(SQL_TEST_PARSING_RESULT, sql);
	}

	@Test
	public void testAnalyzeParams() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = parser.analyseParams(SQL_TEST_AVEC_PARAMS);
		assertEquals(4, parametresSQL.size());
		checkParam(parametresSQL.get(0), "p1.id", SQLParamBean.Type.INTEGER, true, true);
		checkParam(parametresSQL.get(1), "p2.id", SQLParamBean.Type.STRING, false, true);
		checkParam(parametresSQL.get(2), "p3.id", SQLParamBean.Type.DATE, true, false);
		checkParam(parametresSQL.get(3), "p4.id", SQLParamBean.Type.NUMERIC, true, true);
	}

	@Test
	public void testAnalyseParamsAvecDoublons() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = parser.analyseParams(SQL_TEST_AVEC_PARAMS_DOUBLONS);
		assertEquals(2, parametresSQL.size());
		checkParam(parametresSQL.get(0), "p1.id", SQLParamBean.Type.INTEGER, true, true);
		checkParam(parametresSQL.get(1), "p2.id", SQLParamBean.Type.STRING, false, true);
	}

	/**
	 * On considère actuellement que $P_IUO{p1.id} et $P_SUO{p1.id} sont deux paramètres distincts. 
	 * À voir si il n'y a pas moyen de faire mieux.
	 */
	@Test
	public void testAnalyseParamsAvecFauxDoublons() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = parser.analyseParams(SQL_TEST_AVEC_PARAMS_FAUX_DOUBLONS);
		assertEquals(3, parametresSQL.size());
		checkParam(parametresSQL.get(0), "p1.id", SQLParamBean.Type.INTEGER, true, true);
		checkParam(parametresSQL.get(1), "p2.id", SQLParamBean.Type.STRING, false, true);
		checkParam(parametresSQL.get(2), "p1.id", SQLParamBean.Type.STRING, true, true);
	}

	private void checkParam(SQLParamBean param, String name, SQLParamBean.Type type, boolean isUnique, boolean isObligatoire) {
		assertEquals(name, param.getName());
		assertEquals(type, param.getType());
		assertEquals(isUnique, param.isUnique());
		assertEquals(isObligatoire, param.isObligatoire());
	}

	@Test
	public void testCheckParamsAvecArgumentNull() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = parser.analyseParams(SQL_TEST_AVEC_PARAMS);
		try {
			parser.checkParams(parametresSQL, null);
			fail("On doit lever une exception quand la map de parametres passée au service est null.");
		} catch (Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
		}
	}

	@Test(expected = CktlRestParseException.class)
	public void testCheckParamsManquants() throws Exception {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = parser.analyseParams(SQL_TEST_AVEC_PARAMS);
		parser.checkParams(parametresSQL, new HashMap<String, List<Object>>());
	}

	@Test
	public void testCheckParamsTypesExceptions() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = new ArrayList<SQLParamBean>();
		Map<String, List<Object>> paramValues = new HashMap<String, List<Object>>();

		// erreur IUO
		parametresSQL.add(new SQLParamBean("IUO", "p"));
		paramValues.put("p", Collections.singletonList((Object) "testChaine"));

		try {
			parser.checkParams(parametresSQL, paramValues);
			fail("On doit lever une exception quand le parametre n'est pas un entier avec IUO.");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}

		// erreur DUO
		parametresSQL.clear();
		paramValues.clear();
		parametresSQL.add(new SQLParamBean("DUO", "p"));
		paramValues.put("p", Collections.singletonList((Object) "testChaine"));

		try {
			parser.checkParams(parametresSQL, paramValues);
			fail("On doit lever une exception quand le parametre n'est pas un entier avec IUO.");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}

		// multiples erreurs
		parametresSQL.clear();
		paramValues.clear();
		parametresSQL.add(new SQLParamBean("IUO", "p"));
		parametresSQL.add(new SQLParamBean("DUO", "p"));
		paramValues.put("p", Collections.singletonList((Object) "testChaine"));

		try {
			parser.checkParams(parametresSQL, paramValues);
			fail("On doit lever une exception quand le parametre n'est pas un entier avec IUO.");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
			assertEquals(2, e.getMessage().split(SQLParser.ERR_MSG_SEPARATOR).length);
		}

	}

	@Test
	public void testCheckParamsTypes() throws Exception {
		SQLParser parser = SQLParser.instance();
		List<SQLParamBean> parametresSQL = new ArrayList<SQLParamBean>();
		Map<String, List<Object>> paramValues = new HashMap<String, List<Object>>();

		// succes
		parametresSQL.clear();
		paramValues.clear();

		parametresSQL.add(new SQLParamBean("SUO", "p1"));
		parametresSQL.add(new SQLParamBean("IUO", "p2"));
		parametresSQL.add(new SQLParamBean("DUO", "p3"));
		parametresSQL.add(new SQLParamBean("NUO", "p4"));
		// On laisse la possibilité d'avoir un même paramètre ayant deux types de convertion
		parametresSQL.add(new SQLParamBean("SUO", "p2"));

		paramValues.put("p1", Collections.singletonList((Object) "v1"));
		paramValues.put("p2", Collections.singletonList((Object) "1"));
		paramValues.put("p3", Collections.singletonList((Object) "24/12/2012"));
		paramValues.put("p4", Collections.singletonList((Object) "1.2"));

		try {
			parser.checkParams(parametresSQL, paramValues);
		} catch (Exception e) {
			fail("Les parametres testés ne devraient pas lever d'exception.");
		}
	}

	@Test
	public void testReplaceParams() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		// $P_IUO{p1.id} = 1234
		SQLParamBean p1 = new SQLParamBean("IUO", "p1.id");
		p1.setRawName("$P_IUO{p1.id}");
		p1.setFormattedValue("1234");
		// $P_SUO{p2.id}
		SQLParamBean p2 = new SQLParamBean("SUO", "p2.id");
		p2.setRawName("$P_SUO{p2.id}");
		p2.setFormattedValue("'a'");
		// $P_SLO{p3.id}
		SQLParamBean p3 = new SQLParamBean("SLO", "p3.id");
		p3.setRawName("$P_SLO{p3.id}");
		p3.setFormattedValue("'a', 'b'");
		// $P_DUO{p4.id}
		SQLParamBean p4 = new SQLParamBean("DUO", "p4.id");
		p4.setRawName("$P_DUO{p4.id}");
		p4.setFormattedValue("to_date('21/12/2012', 'DD/MM/YYYY')");
		// $P_SUN{p6.id}
		SQLParamBean p6 = new SQLParamBean("SUN", "p6.id");
		p6.setRawName("$P_SUN{p6.id}");
		p6.setFormattedValue("null");

		String sql = parser.replaceParams(SQL_TEST_REPLACE_PARAMS, Arrays.asList(p1, p2, p3, p4, p6));
		assertEquals(SQL_TEST_REPLACE_PARAMS_RESULT, sql);
	}

	@Test
	public void testCheckRequeteSQLFailure() {
		SQLParser parser = SQLParser.instance();

		try {
			parser.checkRequeteSQL(null);
			fail("Une requête vide doit lever une erreur");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}

		try {
			parser.checkRequeteSQL("UPDATE test");
			fail("Une requête qui ne commence pas par SELECT doit lever une erreur");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}

		try {
			parser.checkRequeteSQL("/* test SELECT test");
			fail("Une requête qui ne commence pas par SELECT doit lever une erreur");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}

		try {
			parser.checkRequeteSQL("-- Commentaire SELECT test");
			fail("Une requête qui ne commence pas par SELECT doit lever une erreur");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}

		try {
			parser.checkRequeteSQL("-- Commentaire \nDELETE test");
			fail("Une requête qui ne commence pas par SELECT doit lever une erreur");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}
	}

	@Test
	public void testCheckRequeteSQLSuccess() throws CktlRestParseException {
		SQLParser parser = SQLParser.instance();
		parser.checkRequeteSQL("SELECT * FROM dual");
		parser.checkRequeteSQL("-- Un commentaire test\nSELECT * FROM DUAL");
		parser.checkRequeteSQL("	\n		-- Un commentaire test\nSELECT * FROM DUAL");

		parser.checkRequeteSQL("/* Un commentaire test */\nSELECT * FROM DUAL");
		parser.checkRequeteSQL("      /* Un commentaire test */   \n  SELECT * FROM DUAL");
		parser.checkRequeteSQL("   \r   /* Un commentaire  -- test */   \n  SELECT * FROM DUAL");
		parser.checkRequeteSQL("/* Un commentaire test */-- Commentaire   \n  SELECT * FROM DUAL");

		parser.checkRequeteSQL("   -- Un commentaire test\n\r -- Un commentaire test\n\tSELECT * /* commentaire */ FROM DUAL");
		parser.checkRequeteSQL("  /* test \n retest \t\n -- Commentaire  */-- Un commentaire test\nSELECT * FROM DUAL");

		parser.checkRequeteSQL("/* UPDATE test */ \tSELECT test");
	}
}
