package org.cocktail.fwkcktlrestservices.serveur.dataproviders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestParseException;
import org.junit.Before;
import org.junit.Test;

public class ParamBeanTest {

	private static final String NULL_STR = "null";

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCompatibiliteSpecs() throws CktlRestParseException {
		assertCompatibiliteSpecsSuccess("SUO");
		assertCompatibiliteSpecsSuccess("SUN");
		assertCompatibiliteSpecsSuccess("SUP");

		assertCompatibiliteSpecsSuccess("SLO");
		assertCompatibiliteSpecsSuccess("SLN");
		assertCompatibiliteSpecsFailure("SLP");

		assertCompatibiliteSpecsSuccess("IUO");
		assertCompatibiliteSpecsSuccess("IUN");
		assertCompatibiliteSpecsFailure("IUP");

		assertCompatibiliteSpecsSuccess("ILO");
		assertCompatibiliteSpecsSuccess("ILN");
		assertCompatibiliteSpecsFailure("ILP");

		assertCompatibiliteSpecsSuccess("DUO");
		assertCompatibiliteSpecsSuccess("DUN");
		assertCompatibiliteSpecsFailure("DUP");

		assertCompatibiliteSpecsSuccess("DLO");
		assertCompatibiliteSpecsSuccess("DLN");
		assertCompatibiliteSpecsFailure("DLP");

		assertCompatibiliteSpecsSuccess("NUO");
		assertCompatibiliteSpecsSuccess("NUN");
		assertCompatibiliteSpecsFailure("NUP");

		assertCompatibiliteSpecsSuccess("NLO");
		assertCompatibiliteSpecsSuccess("NLN");
		assertCompatibiliteSpecsFailure("NLP");
	}

	private void assertCompatibiliteSpecsFailure(String specs) {
		try {
			new SQLParamBean(specs, "a");
			fail("La specs " + specs + " n'est pas valide.");
		} catch (Exception e) {
			assertTrue(e instanceof CktlRestParseException);
		}
	}

	private void assertCompatibiliteSpecsSuccess(String specs) throws CktlRestParseException {
		new SQLParamBean(specs, "a");
	}

	@Test(expected = CktlRestParseException.class)
	public void testCheckObligatoire() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("IUO", "p");
		param.checkAndConvert(new ArrayList<Object>());
	}

	@Test
	public void testCheckNonObligatoire() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("IUN", "p");
		param.checkAndConvert(new ArrayList<Object>());
		param.checkAndConvert(null);
	}

	@Test(expected = CktlRestParseException.class)
	public void testCheckUniciteFails() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("IUO", "p");
		param.checkAndConvert(Arrays.asList((Object) "a", (Object) "b"));
	}

	@Test
	public void testCheckUniciteOk() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SUO", "p");
		param.checkAndConvert(Arrays.asList((Object) "a"));

		param = new SQLParamBean("SLO", "p");
		param.checkAndConvert(Arrays.asList((Object) "a"));
	}

	@Test
	public void testConvertString() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SUO", "p");

		List<String> convertedValue = param.convert(Arrays.asList((Object) "a"));
		assertNotNull(convertedValue);
		assertEquals("'a'", convertedValue.get(0));

		param = new SQLParamBean("SLO", "p");

		convertedValue = param.convert(Arrays.asList((Object) "a", (Object) "b"));
		assertNotNull(convertedValue);
		assertEquals("'a'", convertedValue.get(0));
		assertEquals("'b'", convertedValue.get(1));
	}

	@Test
	public void testConvertInteger() throws CktlRestParseException, ParseException {
		SQLParamBean param = new SQLParamBean("IUO", "1");

		List<String> convertedValue = param.convert(Arrays.asList((Object) "1"));
		assertNotNull(convertedValue);
		assertEquals("1", convertedValue.get(0));

		assertNull(param.convert(Arrays.asList((Object) "a")));
	}

	@Test
	public void testConvertNumeric() throws CktlRestParseException, ParseException {
		SQLParamBean param = new SQLParamBean("NUO", "p");

		List<String> convertedValue = param.convert(Arrays.asList((Object) "1.1"));
		assertNotNull(convertedValue);
		assertEquals("1.10", convertedValue.get(0));

		// TODO Vérifier le roundingMode (1.125 => 1.13) ?
		convertedValue = param.convert(Arrays.asList((Object) "1.1254"));
		assertNotNull(convertedValue);
		assertEquals("1.13", convertedValue.get(0));

		convertedValue = param.convert(Arrays.asList((Object) "1.1250"));
		assertNotNull(convertedValue);
		assertEquals("1.12", convertedValue.get(0));

		convertedValue = param.convert(Arrays.asList((Object) "1"));
		assertNotNull(convertedValue);
		assertEquals("1.00", convertedValue.get(0));

		assertNull(param.convert(Arrays.asList((Object) "a")));
	}

	@Test
	public void testConvertDate() throws CktlRestParseException, ParseException {
		SQLParamBean param = new SQLParamBean("DUO", "p");

		List<String> convertedValue = param.convert(Arrays.asList((Object) "21/12/2012"));
		assertNotNull(convertedValue);
		assertEquals("to_date('21/12/2012', 'DD/MM/YYYY')", convertedValue.get(0));

		assertNull(param.convert(Arrays.asList((Object) "a")));

		assertNull(param.convert(Arrays.asList((Object) "21-12-2012")));

		assertNull(param.convert(Arrays.asList((Object) "32/11/2012")));
	}

	/**
	 * Cas de tests données uniques :
	 * <ul>
	 * <li>Egalite Stricte non-Nullable (i.e. Obligatoire): "'a'"<br>
	 * => <code>xUO</code></li>
	 * <li>Egalite Stricte Nullable (i.e. Facultative) : "null" ou "'a'"<br>
	 * => <code>xUN</code></li>
	 * <li>Egalité Partielle : "'%a%'" ou "'%'"<br>
	 * => <code>xUP</code></li>
	 * </ul>
	 * Cas de tests listes de données :
	 * <ul>
	 * <li>Liste Obligatoire : "'a', 'b'"<br>
	 * => <code>xLO</code></li>
	 * <li>Liste Facultative : "null" ou "'a', 'b'"<br>
	 * => <code>xLN</code></li>
	 * <li>Liste Partielles : ERREUR<br>
	 * => <code>xLP</code></li>
	 */
	@Test
	public void testFormatUniqueValueObligatoire() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SUO", "p");
		List<String> convertedValues = Arrays.asList("'a'");
		String formattedString = param.formatUnique(convertedValues);
		assertEquals("'a'", formattedString);
	}

	@Test
	public void testFormatUniqueValueNullable() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SUN", "p");
		List<String> convertedValues = Collections.singletonList("'a'");
		assertEquals("'a'", param.formatUnique(convertedValues));

		convertedValues = Collections.emptyList();
		assertEquals(NULL_STR, param.formatUnique(convertedValues));

		convertedValues = Collections.singletonList(null);
		assertEquals(NULL_STR, param.formatUnique(convertedValues));
	}

	@Test
	public void testFormatUniqueValuePartiel() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SUP", "p");
		List<String> convertedValues = Collections.singletonList("'a'");
		assertEquals("'%a%'", param.formatUnique(convertedValues));

		convertedValues = Collections.emptyList();
		assertEquals("'%'", param.formatUnique(convertedValues));

		convertedValues = Collections.singletonList(null);
		assertEquals("'%'", param.formatUnique(convertedValues));
	}

	@Test
	public void testFormatListValuesObligatoire() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SLO", "p");
		List<String> convertedValues = Arrays.asList("'a'");
		String formattedString = param.formatList(convertedValues);
		assertEquals("'a'", formattedString);

		convertedValues = Arrays.asList("'a'", "'b'");
		formattedString = param.formatList(convertedValues);
		assertEquals("'a', 'b'", formattedString);

		convertedValues = Arrays.asList("'a'", "'b'");
		formattedString = param.formatList(convertedValues);
		assertEquals("'a', 'b'", formattedString);
	}

	@Test
	public void testFormatListValuesNullable() throws CktlRestParseException {
		SQLParamBean param = new SQLParamBean("SLN", "p");

		List<String> convertedValues = Arrays.asList("'a'");
		String formattedString = param.formatList(convertedValues);
		assertEquals("'a'", formattedString);

		convertedValues = Collections.emptyList();
		formattedString = param.formatList(convertedValues);
		assertEquals(NULL_STR, formattedString);

		convertedValues = Collections.singletonList(null);
		assertEquals(NULL_STR, param.formatList(convertedValues));
	}
}
