package org.cocktail.fwkcktlrestservices.serveur.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestAccessNotGrantedException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.CktlRestNotAuthenticatedException;
import org.cocktail.fwkcktlrestservices.serveur.exceptions.ICktlRestException;
import org.cocktail.fwkcktlrestservices.serveur.model.Auth;
import org.junit.Before;
import org.junit.Test;

public class AuthenticationServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCheckCookieNonPresent() {
		AuthenticationService as = new AuthenticationService();
		try {
			as.checkAccess(null, "toto");
			fail("aaaaaeeemmmm On devrait avoir une exception du type CktlRestNotAuthenticatedException");
		} catch (Exception e) {
			assertTrue("Exception devrait etre du type CktlRestNotAuthenticatedException", e instanceof CktlRestNotAuthenticatedException);
		}
	}

	@Test
	public void testCheckCookiePasDAcces() {
		AuthenticationService as = new AuthenticationService() {
			@Override
			protected void checkUserAccess(Auth auth, String resourceId) {
				throw new CktlRestAccessNotGrantedException("Pas d'acces !");
			}
		};
		try {
			as.checkAccess(null, "");
			fail("aaaaaeeemmmm On devrait avoir une exception du type CktlRestAccessNotGrantedException");
		} catch (Exception e) {
			assertTrue("Exception devrait etre du type CktlRestAccessNotGrantedException", e instanceof ICktlRestException);
		}
	}

}
