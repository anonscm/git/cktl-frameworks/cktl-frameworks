/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrestationLigne.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrestationLigne extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "PrestationLigne";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_ligne";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prligId";

	public static final String PRLIG_ART_HT_KEY = "prligArtHt";
	public static final String PRLIG_ART_TTC_KEY = "prligArtTtc";
	public static final String PRLIG_ART_TTC_INITIAL_KEY = "prligArtTtcInitial";
	public static final String PRLIG_DATE_KEY = "prligDate";
	public static final String PRLIG_DESCRIPTION_KEY = "prligDescription";
	public static final String PRLIG_QUANTITE_KEY = "prligQuantite";
	public static final String PRLIG_QUANTITE_RESTE_KEY = "prligQuantiteReste";
	public static final String PRLIG_REFERENCE_KEY = "prligReference";
	public static final String PRLIG_TOTAL_HT_KEY = "prligTotalHt";
	public static final String PRLIG_TOTAL_RESTE_HT_KEY = "prligTotalResteHt";
	public static final String PRLIG_TOTAL_RESTE_TTC_KEY = "prligTotalResteTtc";
	public static final String PRLIG_TOTAL_TTC_KEY = "prligTotalTtc";

// Attributs non visibles
	public static final String CAAR_ID_KEY = "caarId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PREST_ID_KEY = "prestId";
	public static final String PRLIG_ID_KEY = "prligId";
	public static final String PRLIG_ID_PERE_KEY = "prligIdPere";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TVA_ID_INITIAL_KEY = "tvaIdInitial";
	public static final String TYAR_ID_KEY = "tyarId";

//Colonnes dans la base de donnees
	public static final String PRLIG_ART_HT_COLKEY = "PRLIG_ART_HT";
	public static final String PRLIG_ART_TTC_COLKEY = "PRLIG_ART_TTC";
	public static final String PRLIG_ART_TTC_INITIAL_COLKEY = "PRLIG_ART_TTC_INITIAL";
	public static final String PRLIG_DATE_COLKEY = "PRLIG_DATE";
	public static final String PRLIG_DESCRIPTION_COLKEY = "PRLIG_DESCRIPTION";
	public static final String PRLIG_QUANTITE_COLKEY = "PRLIG_QUANTITE";
	public static final String PRLIG_QUANTITE_RESTE_COLKEY = "PRLIG_QUANTITE_RESTE";
	public static final String PRLIG_REFERENCE_COLKEY = "PRLIG_REFERENCE";
	public static final String PRLIG_TOTAL_HT_COLKEY = "PRLIG_TOTAL_HT";
	public static final String PRLIG_TOTAL_RESTE_HT_COLKEY = "PRLIG_TOTAL_RESTE_HT";
	public static final String PRLIG_TOTAL_RESTE_TTC_COLKEY = "PRLIG_TOTAL_RESTE_TTC";
	public static final String PRLIG_TOTAL_TTC_COLKEY = "PRLIG_TOTAL_TTC";

	public static final String CAAR_ID_COLKEY = "CAAR_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String PRLIG_ID_COLKEY = "PRLIG_ID";
	public static final String PRLIG_ID_PERE_COLKEY = "PRLIG_ID_PERE";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TVA_ID_INITIAL_COLKEY = "TVA_ID_INITIAL";
	public static final String TYAR_ID_COLKEY = "TYAR_ID";


	// Relationships
	public static final String CATALOGUE_ARTICLE_KEY = "catalogueArticle";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String PRESTATION_KEY = "prestation";
	public static final String PRESTATION_LIGNE_PERE_KEY = "prestationLignePere";
	public static final String PRESTATION_LIGNES_KEY = "prestationLignes";
	public static final String TVA_KEY = "tva";
	public static final String TVA_INITIAL_KEY = "tvaInitial";
	public static final String TYPE_ARTICLE_KEY = "typeArticle";



	// Accessors methods
  public java.math.BigDecimal prligArtHt() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_ART_HT_KEY);
  }

  public void setPrligArtHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_ART_HT_KEY);
  }

  public java.math.BigDecimal prligArtTtc() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_ART_TTC_KEY);
  }

  public void setPrligArtTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_ART_TTC_KEY);
  }

  public java.math.BigDecimal prligArtTtcInitial() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_ART_TTC_INITIAL_KEY);
  }

  public void setPrligArtTtcInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_ART_TTC_INITIAL_KEY);
  }

  public NSTimestamp prligDate() {
    return (NSTimestamp) storedValueForKey(PRLIG_DATE_KEY);
  }

  public void setPrligDate(NSTimestamp value) {
    takeStoredValueForKey(value, PRLIG_DATE_KEY);
  }

  public String prligDescription() {
    return (String) storedValueForKey(PRLIG_DESCRIPTION_KEY);
  }

  public void setPrligDescription(String value) {
    takeStoredValueForKey(value, PRLIG_DESCRIPTION_KEY);
  }

  public java.math.BigDecimal prligQuantite() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_QUANTITE_KEY);
  }

  public void setPrligQuantite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_QUANTITE_KEY);
  }

  public java.math.BigDecimal prligQuantiteReste() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_QUANTITE_RESTE_KEY);
  }

  public void setPrligQuantiteReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_QUANTITE_RESTE_KEY);
  }

  public String prligReference() {
    return (String) storedValueForKey(PRLIG_REFERENCE_KEY);
  }

  public void setPrligReference(String value) {
    takeStoredValueForKey(value, PRLIG_REFERENCE_KEY);
  }

  public java.math.BigDecimal prligTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_TOTAL_HT_KEY);
  }

  public void setPrligTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal prligTotalResteHt() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_TOTAL_RESTE_HT_KEY);
  }

  public void setPrligTotalResteHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_TOTAL_RESTE_HT_KEY);
  }

  public java.math.BigDecimal prligTotalResteTtc() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_TOTAL_RESTE_TTC_KEY);
  }

  public void setPrligTotalResteTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_TOTAL_RESTE_TTC_KEY);
  }

  public java.math.BigDecimal prligTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(PRLIG_TOTAL_TTC_KEY);
  }

  public void setPrligTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRLIG_TOTAL_TTC_KEY);
  }

  public org.cocktail.kava.client.metier.EOCatalogueArticle catalogueArticle() {
    return (org.cocktail.kava.client.metier.EOCatalogueArticle)storedValueForKey(CATALOGUE_ARTICLE_KEY);
  }

  public void setCatalogueArticleRelationship(org.cocktail.kava.client.metier.EOCatalogueArticle value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOCatalogueArticle oldValue = catalogueArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_ARTICLE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOPrestation prestation() {
    return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(PRESTATION_KEY);
  }

  public void setPrestationRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPrestation oldValue = prestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOPrestationLigne prestationLignePere() {
    return (org.cocktail.kava.client.metier.EOPrestationLigne)storedValueForKey(PRESTATION_LIGNE_PERE_KEY);
  }

  public void setPrestationLignePereRelationship(org.cocktail.kava.client.metier.EOPrestationLigne value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPrestationLigne oldValue = prestationLignePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_LIGNE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_LIGNE_PERE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOTva tva() {
    return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.kava.client.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTva oldValue = tva();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOTva tvaInitial() {
    return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_INITIAL_KEY);
  }

  public void setTvaInitialRelationship(org.cocktail.kava.client.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTva oldValue = tvaInitial();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_INITIAL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_INITIAL_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOTypeArticle typeArticle() {
    return (org.cocktail.kava.client.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
  }

  public void setTypeArticleRelationship(org.cocktail.kava.client.metier.EOTypeArticle value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTypeArticle oldValue = typeArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
    }
  }
  
  public NSArray prestationLignes() {
    return (NSArray)storedValueForKey(PRESTATION_LIGNES_KEY);
  }

  public NSArray prestationLignes(EOQualifier qualifier) {
    return prestationLignes(qualifier, null, false);
  }

  public NSArray prestationLignes(EOQualifier qualifier, boolean fetch) {
    return prestationLignes(qualifier, null, fetch);
  }

  public NSArray prestationLignes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOPrestationLigne.PRESTATION_LIGNE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOPrestationLigne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prestationLignes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
  }

  public void removeFromPrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
  }

  public org.cocktail.kava.client.metier.EOPrestationLigne createPrestationLignesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PrestationLigne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRESTATION_LIGNES_KEY);
    return (org.cocktail.kava.client.metier.EOPrestationLigne) eo;
  }

  public void deletePrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrestationLignesRelationships() {
    Enumeration objects = prestationLignes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrestationLignesRelationship((org.cocktail.kava.client.metier.EOPrestationLigne)objects.nextElement());
    }
  }


  public static EOPrestationLigne createPrestationLigne(EOEditingContext editingContext, java.math.BigDecimal prligArtHt
, java.math.BigDecimal prligArtTtc
, java.math.BigDecimal prligArtTtcInitial
, NSTimestamp prligDate
, String prligDescription
, java.math.BigDecimal prligQuantite
, java.math.BigDecimal prligQuantiteReste
, java.math.BigDecimal prligTotalHt
, java.math.BigDecimal prligTotalResteHt
, java.math.BigDecimal prligTotalResteTtc
, java.math.BigDecimal prligTotalTtc
) {
    EOPrestationLigne eo = (EOPrestationLigne) createAndInsertInstance(editingContext, _EOPrestationLigne.ENTITY_NAME);    
		eo.setPrligArtHt(prligArtHt);
		eo.setPrligArtTtc(prligArtTtc);
		eo.setPrligArtTtcInitial(prligArtTtcInitial);
		eo.setPrligDate(prligDate);
		eo.setPrligDescription(prligDescription);
		eo.setPrligQuantite(prligQuantite);
		eo.setPrligQuantiteReste(prligQuantiteReste);
		eo.setPrligTotalHt(prligTotalHt);
		eo.setPrligTotalResteHt(prligTotalResteHt);
		eo.setPrligTotalResteTtc(prligTotalResteTtc);
		eo.setPrligTotalTtc(prligTotalTtc);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPrestationLigne.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPrestationLigne.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPrestationLigne localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrestationLigne)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPrestationLigne localInstanceIn(EOEditingContext editingContext, EOPrestationLigne eo) {
    EOPrestationLigne localInstance = (eo == null) ? null : (EOPrestationLigne)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPrestationLigne#localInstanceIn a la place.
   */
	public static EOPrestationLigne localInstanceOf(EOEditingContext editingContext, EOPrestationLigne eo) {
		return EOPrestationLigne.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPrestationLigne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPrestationLigne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrestationLigne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrestationLigne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrestationLigne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrestationLigne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrestationLigne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrestationLigne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPrestationLigne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrestationLigne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrestationLigne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrestationLigne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
