/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecetteCtrlConvention.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecetteCtrlConvention extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "RecetteCtrlConvention";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_convention";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rconId";

	public static final String RCON_DATE_SAISIE_KEY = "rconDateSaisie";
	public static final String RCON_HT_SAISIE_KEY = "rconHtSaisie";
	public static final String RCON_MONTANT_BUDGETAIRE_KEY = "rconMontantBudgetaire";
	public static final String RCON_TTC_SAISIE_KEY = "rconTtcSaisie";
	public static final String RCON_TVA_SAISIE_KEY = "rconTvaSaisie";
	public static final String REC_ID_KEY = "recId";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String RCON_ID_KEY = "rconId";

//Colonnes dans la base de donnees
	public static final String RCON_DATE_SAISIE_COLKEY = "RCON_DATE_SAISIE";
	public static final String RCON_HT_SAISIE_COLKEY = "RCON_HT_SAISIE";
	public static final String RCON_MONTANT_BUDGETAIRE_COLKEY = "RCON_MONTANT_BUDGETAIRE";
	public static final String RCON_TTC_SAISIE_COLKEY = "RCON_TTC_SAISIE";
	public static final String RCON_TVA_SAISIE_COLKEY = "RCON_TVA_SAISIE";
	public static final String REC_ID_COLKEY = "REC_ID";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String RCON_ID_COLKEY = "RCON_ID";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public NSTimestamp rconDateSaisie() {
    return (NSTimestamp) storedValueForKey(RCON_DATE_SAISIE_KEY);
  }

  public void setRconDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, RCON_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal rconHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RCON_HT_SAISIE_KEY);
  }

  public void setRconHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RCON_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal rconMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(RCON_MONTANT_BUDGETAIRE_KEY);
  }

  public void setRconMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RCON_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal rconTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RCON_TTC_SAISIE_KEY);
  }

  public void setRconTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RCON_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal rconTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RCON_TVA_SAISIE_KEY);
  }

  public void setRconTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RCON_TVA_SAISIE_KEY);
  }

  public Integer recId() {
    return (Integer) storedValueForKey(REC_ID_KEY);
  }

  public void setRecId(Integer value) {
    takeStoredValueForKey(value, REC_ID_KEY);
  }

  public org.cocktail.kava.client.metier.EOConvention convention() {
    return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

  public static EORecetteCtrlConvention createRecetteCtrlConvention(EOEditingContext editingContext, NSTimestamp rconDateSaisie
, java.math.BigDecimal rconHtSaisie
, java.math.BigDecimal rconMontantBudgetaire
, java.math.BigDecimal rconTtcSaisie
, java.math.BigDecimal rconTvaSaisie
, Integer recId
) {
    EORecetteCtrlConvention eo = (EORecetteCtrlConvention) createAndInsertInstance(editingContext, _EORecetteCtrlConvention.ENTITY_NAME);    
		eo.setRconDateSaisie(rconDateSaisie);
		eo.setRconHtSaisie(rconHtSaisie);
		eo.setRconMontantBudgetaire(rconMontantBudgetaire);
		eo.setRconTtcSaisie(rconTtcSaisie);
		eo.setRconTvaSaisie(rconTvaSaisie);
		eo.setRecId(recId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecetteCtrlConvention.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecetteCtrlConvention.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EORecetteCtrlConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecetteCtrlConvention)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORecetteCtrlConvention localInstanceIn(EOEditingContext editingContext, EORecetteCtrlConvention eo) {
    EORecetteCtrlConvention localInstance = (eo == null) ? null : (EORecetteCtrlConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecetteCtrlConvention#localInstanceIn a la place.
   */
	public static EORecetteCtrlConvention localInstanceOf(EOEditingContext editingContext, EORecetteCtrlConvention eo) {
		return EORecetteCtrlConvention.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORecetteCtrlConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecetteCtrlConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecetteCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecetteCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecetteCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecetteCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecetteCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecetteCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecetteCtrlConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecetteCtrlConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecetteCtrlConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecetteCtrlConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
