/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCataloguePrestationWeb.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCataloguePrestationWeb extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "CataloguePrestationWeb";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.catalogue_prestation_web";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "catPwebId";

	public static final String CAT_CLE_KEY = "catCle";
	public static final String CAT_COMMANTAIRE_KEY = "catCommantaire";
	public static final String CAT_VALEUR_KEY = "catValeur";

// Attributs non visibles
	public static final String CAT_ID_KEY = "catId";
	public static final String CAT_PWEB_ID_KEY = "catPwebId";

//Colonnes dans la base de donnees
	public static final String CAT_CLE_COLKEY = "CAT_CLE";
	public static final String CAT_COMMANTAIRE_COLKEY = "CAT_COMMANTAIRE";
	public static final String CAT_VALEUR_COLKEY = "CAT_VALEUR";

	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CAT_PWEB_ID_COLKEY = "CAT_PWEB_ID";


	// Relationships
	public static final String CATALOGUE_KEY = "catalogue";



	// Accessors methods
  public String catCle() {
    return (String) storedValueForKey(CAT_CLE_KEY);
  }

  public void setCatCle(String value) {
    takeStoredValueForKey(value, CAT_CLE_KEY);
  }

  public String catCommantaire() {
    return (String) storedValueForKey(CAT_COMMANTAIRE_KEY);
  }

  public void setCatCommantaire(String value) {
    takeStoredValueForKey(value, CAT_COMMANTAIRE_KEY);
  }

  public String catValeur() {
    return (String) storedValueForKey(CAT_VALEUR_KEY);
  }

  public void setCatValeur(String value) {
    takeStoredValueForKey(value, CAT_VALEUR_KEY);
  }

  public org.cocktail.kava.client.metier.EOCatalogue catalogue() {
    return (org.cocktail.kava.client.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
  }

  public void setCatalogueRelationship(org.cocktail.kava.client.metier.EOCatalogue value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOCatalogue oldValue = catalogue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
    }
  }
  

  public static EOCataloguePrestationWeb createCataloguePrestationWeb(EOEditingContext editingContext) {
    EOCataloguePrestationWeb eo = (EOCataloguePrestationWeb) createAndInsertInstance(editingContext, _EOCataloguePrestationWeb.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCataloguePrestationWeb.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCataloguePrestationWeb.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCataloguePrestationWeb localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCataloguePrestationWeb)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCataloguePrestationWeb localInstanceIn(EOEditingContext editingContext, EOCataloguePrestationWeb eo) {
    EOCataloguePrestationWeb localInstance = (eo == null) ? null : (EOCataloguePrestationWeb)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCataloguePrestationWeb#localInstanceIn a la place.
   */
	public static EOCataloguePrestationWeb localInstanceOf(EOEditingContext editingContext, EOCataloguePrestationWeb eo) {
		return EOCataloguePrestationWeb.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCataloguePrestationWeb fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCataloguePrestationWeb fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCataloguePrestationWeb eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCataloguePrestationWeb)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCataloguePrestationWeb fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCataloguePrestationWeb fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCataloguePrestationWeb eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCataloguePrestationWeb)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCataloguePrestationWeb fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCataloguePrestationWeb eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCataloguePrestationWeb ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCataloguePrestationWeb fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
