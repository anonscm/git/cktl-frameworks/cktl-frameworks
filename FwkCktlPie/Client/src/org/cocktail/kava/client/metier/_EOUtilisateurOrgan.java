/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurOrgan.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOUtilisateurOrgan extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "UtilisateurOrgan";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.utilisateur_organ";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "uoId";


// Attributs non visibles
	public static final String ORG_ID_KEY = "orgId";
	public static final String UO_ID_KEY = "uoId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees

	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String UO_ID_COLKEY = "uo_id";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ORGAN_KEY = "organ";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public org.cocktail.kava.client.metier.EOOrgan organ() {
    return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOUtilisateurOrgan createUtilisateurOrgan(EOEditingContext editingContext) {
    EOUtilisateurOrgan eo = (EOUtilisateurOrgan) createAndInsertInstance(editingContext, _EOUtilisateurOrgan.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOUtilisateurOrgan.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOUtilisateurOrgan.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOUtilisateurOrgan localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurOrgan)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOUtilisateurOrgan localInstanceIn(EOEditingContext editingContext, EOUtilisateurOrgan eo) {
    EOUtilisateurOrgan localInstance = (eo == null) ? null : (EOUtilisateurOrgan)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOUtilisateurOrgan#localInstanceIn a la place.
   */
	public static EOUtilisateurOrgan localInstanceOf(EOEditingContext editingContext, EOUtilisateurOrgan eo) {
		return EOUtilisateurOrgan.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOUtilisateurOrgan fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOUtilisateurOrgan fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurOrgan)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurOrgan)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOUtilisateurOrgan fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurOrgan eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurOrgan ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurOrgan fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
