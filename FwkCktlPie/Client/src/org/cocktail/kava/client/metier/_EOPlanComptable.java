/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlanComptable.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPlanComptable extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "PlanComptable";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_plan_comptable";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pcoNum";

	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_BUDGETAIRE_KEY = "pcoBudgetaire";
	public static final String PCO_EMARGEMENT_KEY = "pcoEmargement";
	public static final String PCO_J_BE_KEY = "pcoJBe";
	public static final String PCO_J_EXERCICE_KEY = "pcoJExercice";
	public static final String PCO_J_FIN_EXERCICE_KEY = "pcoJFinExercice";
	public static final String PCO_LIBELLE_KEY = "pcoLibelle";
	public static final String PCO_NATURE_KEY = "pcoNature";
	public static final String PCO_NIVEAU_KEY = "pcoNiveau";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_VALIDITE_KEY = "pcoValidite";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_BUDGETAIRE_COLKEY = "pco_budgetaire";
	public static final String PCO_EMARGEMENT_COLKEY = "pco_emargement";
	public static final String PCO_J_BE_COLKEY = "pco_j_be";
	public static final String PCO_J_EXERCICE_COLKEY = "pco_j_exercice";
	public static final String PCO_J_FIN_EXERCICE_COLKEY = "pco_j_fin_exercice";
	public static final String PCO_LIBELLE_COLKEY = "pco_libelle";
	public static final String PCO_NATURE_COLKEY = "pco_nature";
	public static final String PCO_NIVEAU_COLKEY = "pco_niveau";
	public static final String PCO_NUM_COLKEY = "pco_num";
	public static final String PCO_VALIDITE_COLKEY = "pco_validite";



	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLANCO_CREDIT_DEPS_KEY = "plancoCreditDeps";
	public static final String PLANCO_CREDIT_REC_REAUTIMPS_KEY = "plancoCreditRecReautimps";
	public static final String PLANCO_CREDIT_RECS_KEY = "plancoCreditRecs";



	// Accessors methods
  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String pcoBudgetaire() {
    return (String) storedValueForKey(PCO_BUDGETAIRE_KEY);
  }

  public void setPcoBudgetaire(String value) {
    takeStoredValueForKey(value, PCO_BUDGETAIRE_KEY);
  }

  public String pcoEmargement() {
    return (String) storedValueForKey(PCO_EMARGEMENT_KEY);
  }

  public void setPcoEmargement(String value) {
    takeStoredValueForKey(value, PCO_EMARGEMENT_KEY);
  }

  public String pcoJBe() {
    return (String) storedValueForKey(PCO_J_BE_KEY);
  }

  public void setPcoJBe(String value) {
    takeStoredValueForKey(value, PCO_J_BE_KEY);
  }

  public String pcoJExercice() {
    return (String) storedValueForKey(PCO_J_EXERCICE_KEY);
  }

  public void setPcoJExercice(String value) {
    takeStoredValueForKey(value, PCO_J_EXERCICE_KEY);
  }

  public String pcoJFinExercice() {
    return (String) storedValueForKey(PCO_J_FIN_EXERCICE_KEY);
  }

  public void setPcoJFinExercice(String value) {
    takeStoredValueForKey(value, PCO_J_FIN_EXERCICE_KEY);
  }

  public String pcoLibelle() {
    return (String) storedValueForKey(PCO_LIBELLE_KEY);
  }

  public void setPcoLibelle(String value) {
    takeStoredValueForKey(value, PCO_LIBELLE_KEY);
  }

  public String pcoNature() {
    return (String) storedValueForKey(PCO_NATURE_KEY);
  }

  public void setPcoNature(String value) {
    takeStoredValueForKey(value, PCO_NATURE_KEY);
  }

  public Integer pcoNiveau() {
    return (Integer) storedValueForKey(PCO_NIVEAU_KEY);
  }

  public void setPcoNiveau(Integer value) {
    takeStoredValueForKey(value, PCO_NIVEAU_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String pcoValidite() {
    return (String) storedValueForKey(PCO_VALIDITE_KEY);
  }

  public void setPcoValidite(String value) {
    takeStoredValueForKey(value, PCO_VALIDITE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public NSArray plancoCreditDeps() {
    return (NSArray)storedValueForKey(PLANCO_CREDIT_DEPS_KEY);
  }

  public NSArray plancoCreditDeps(EOQualifier qualifier) {
    return plancoCreditDeps(qualifier, null, false);
  }

  public NSArray plancoCreditDeps(EOQualifier qualifier, boolean fetch) {
    return plancoCreditDeps(qualifier, null, fetch);
  }

  public NSArray plancoCreditDeps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOPlancoCreditDep.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOPlancoCreditDep.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = plancoCreditDeps();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPlancoCreditDepsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_DEPS_KEY);
  }

  public void removeFromPlancoCreditDepsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_DEPS_KEY);
  }

  public org.cocktail.kava.client.metier.EOPlancoCreditDep createPlancoCreditDepsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PlancoCreditDep");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PLANCO_CREDIT_DEPS_KEY);
    return (org.cocktail.kava.client.metier.EOPlancoCreditDep) eo;
  }

  public void deletePlancoCreditDepsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_DEPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPlancoCreditDepsRelationships() {
    Enumeration objects = plancoCreditDeps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePlancoCreditDepsRelationship((org.cocktail.kava.client.metier.EOPlancoCreditDep)objects.nextElement());
    }
  }

  public NSArray plancoCreditRecReautimps() {
    return (NSArray)storedValueForKey(PLANCO_CREDIT_REC_REAUTIMPS_KEY);
  }

  public NSArray plancoCreditRecReautimps(EOQualifier qualifier) {
    return plancoCreditRecReautimps(qualifier, null, false);
  }

  public NSArray plancoCreditRecReautimps(EOQualifier qualifier, boolean fetch) {
    return plancoCreditRecReautimps(qualifier, null, fetch);
  }

  public NSArray plancoCreditRecReautimps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = plancoCreditRecReautimps();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPlancoCreditRecReautimpsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
  }

  public void removeFromPlancoCreditRecReautimpsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
  }

  public org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp createPlancoCreditRecReautimpsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PlancoCreditRecReautimp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
    return (org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp) eo;
  }

  public void deletePlancoCreditRecReautimpsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPlancoCreditRecReautimpsRelationships() {
    Enumeration objects = plancoCreditRecReautimps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePlancoCreditRecReautimpsRelationship((org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp)objects.nextElement());
    }
  }

  public NSArray plancoCreditRecs() {
    return (NSArray)storedValueForKey(PLANCO_CREDIT_RECS_KEY);
  }

  public NSArray plancoCreditRecs(EOQualifier qualifier) {
    return plancoCreditRecs(qualifier, null, false);
  }

  public NSArray plancoCreditRecs(EOQualifier qualifier, boolean fetch) {
    return plancoCreditRecs(qualifier, null, fetch);
  }

  public NSArray plancoCreditRecs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOPlancoCreditRec.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOPlancoCreditRec.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = plancoCreditRecs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPlancoCreditRecsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_RECS_KEY);
  }

  public void removeFromPlancoCreditRecsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_RECS_KEY);
  }

  public org.cocktail.kava.client.metier.EOPlancoCreditRec createPlancoCreditRecsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PlancoCreditRec");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PLANCO_CREDIT_RECS_KEY);
    return (org.cocktail.kava.client.metier.EOPlancoCreditRec) eo;
  }

  public void deletePlancoCreditRecsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_RECS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPlancoCreditRecsRelationships() {
    Enumeration objects = plancoCreditRecs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePlancoCreditRecsRelationship((org.cocktail.kava.client.metier.EOPlancoCreditRec)objects.nextElement());
    }
  }


  public static EOPlanComptable createPlanComptable(EOEditingContext editingContext, Integer exeOrdre
, String pcoBudgetaire
, String pcoEmargement
, String pcoJBe
, String pcoJExercice
, String pcoJFinExercice
, String pcoLibelle
, String pcoNature
, Integer pcoNiveau
, String pcoNum
, String pcoValidite
) {
    EOPlanComptable eo = (EOPlanComptable) createAndInsertInstance(editingContext, _EOPlanComptable.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
		eo.setPcoBudgetaire(pcoBudgetaire);
		eo.setPcoEmargement(pcoEmargement);
		eo.setPcoJBe(pcoJBe);
		eo.setPcoJExercice(pcoJExercice);
		eo.setPcoJFinExercice(pcoJFinExercice);
		eo.setPcoLibelle(pcoLibelle);
		eo.setPcoNature(pcoNature);
		eo.setPcoNiveau(pcoNiveau);
		eo.setPcoNum(pcoNum);
		eo.setPcoValidite(pcoValidite);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPlanComptable.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPlanComptable.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPlanComptable localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPlanComptable)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPlanComptable localInstanceIn(EOEditingContext editingContext, EOPlanComptable eo) {
    EOPlanComptable localInstance = (eo == null) ? null : (EOPlanComptable)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPlanComptable#localInstanceIn a la place.
   */
	public static EOPlanComptable localInstanceOf(EOEditingContext editingContext, EOPlanComptable eo) {
		return EOPlanComptable.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
