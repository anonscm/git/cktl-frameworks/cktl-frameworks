/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPiEngFac.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPiEngFac extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "PiEngFac";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.pi_eng_fac";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pefId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String PREST_ID_KEY = "prestId";

// Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String FAC_ID_KEY = "facId";
	public static final String PEF_ID_KEY = "pefId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String PREST_ID_COLKEY = "PREST_ID";

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String PEF_ID_COLKEY = "PEF_ID";


	// Relationships
	public static final String ENGAGE_BUDGET_KEY = "engageBudget";
	public static final String FACTURE_KEY = "facture";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public Integer prestId() {
    return (Integer) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Integer value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public org.cocktail.kava.client.metier.EOEngageBudget engageBudget() {
    return (org.cocktail.kava.client.metier.EOEngageBudget)storedValueForKey(ENGAGE_BUDGET_KEY);
  }

  public void setEngageBudgetRelationship(org.cocktail.kava.client.metier.EOEngageBudget value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOEngageBudget oldValue = engageBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOFacture facture() {
    return (org.cocktail.kava.client.metier.EOFacture)storedValueForKey(FACTURE_KEY);
  }

  public void setFactureRelationship(org.cocktail.kava.client.metier.EOFacture value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOFacture oldValue = facture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FACTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_KEY);
    }
  }
  

  public static EOPiEngFac createPiEngFac(EOEditingContext editingContext, NSTimestamp dCreation
, Integer prestId
) {
    EOPiEngFac eo = (EOPiEngFac) createAndInsertInstance(editingContext, _EOPiEngFac.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPrestId(prestId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPiEngFac.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPiEngFac.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPiEngFac localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPiEngFac)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPiEngFac localInstanceIn(EOEditingContext editingContext, EOPiEngFac eo) {
    EOPiEngFac localInstance = (eo == null) ? null : (EOPiEngFac)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPiEngFac#localInstanceIn a la place.
   */
	public static EOPiEngFac localInstanceOf(EOEditingContext editingContext, EOPiEngFac eo) {
		return EOPiEngFac.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPiEngFac fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPiEngFac fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPiEngFac eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPiEngFac)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPiEngFac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPiEngFac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPiEngFac eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPiEngFac)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPiEngFac fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPiEngFac eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPiEngFac ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPiEngFac fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
