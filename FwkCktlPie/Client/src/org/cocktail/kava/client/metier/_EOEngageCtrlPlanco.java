/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngageCtrlPlanco.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngageCtrlPlanco extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "EngageCtrlPlanco";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_engage_ctrl_planco";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "epcoId";

	public static final String EPCO_DATE_SAISIE_KEY = "epcoDateSaisie";
	public static final String EPCO_HT_SAISIE_KEY = "epcoHtSaisie";
	public static final String EPCO_MONTANT_BUDGETAIRE_KEY = "epcoMontantBudgetaire";
	public static final String EPCO_MONTANT_BUDGETAIRE_RESTE_KEY = "epcoMontantBudgetaireReste";
	public static final String EPCO_TTC_SAISIE_KEY = "epcoTtcSaisie";
	public static final String EPCO_TVA_SAISIE_KEY = "epcoTvaSaisie";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

// Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String EPCO_ID_KEY = "epcoId";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String EPCO_DATE_SAISIE_COLKEY = "EPCO_DATE_SAISIE";
	public static final String EPCO_HT_SAISIE_COLKEY = "EPCO_HT_SAISIE";
	public static final String EPCO_MONTANT_BUDGETAIRE_COLKEY = "EPCO_MONTANT_BUDGETAIRE";
	public static final String EPCO_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EPCO_MONTANT_BUDGETAIRE_RESTE";
	public static final String EPCO_TTC_SAISIE_COLKEY = "EPCO_TTC_SAISIE";
	public static final String EPCO_TVA_SAISIE_COLKEY = "EPCO_TVA_SAISIE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EPCO_ID_COLKEY = "EPCO_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
  public NSTimestamp epcoDateSaisie() {
    return (NSTimestamp) storedValueForKey(EPCO_DATE_SAISIE_KEY);
  }

  public void setEpcoDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, EPCO_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal epcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EPCO_HT_SAISIE_KEY);
  }

  public void setEpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal epcoMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(EPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEpcoMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal epcoMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(EPCO_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEpcoMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EPCO_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal epcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EPCO_TTC_SAISIE_KEY);
  }

  public void setEpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal epcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EPCO_TVA_SAISIE_KEY);
  }

  public void setEpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EPCO_TVA_SAISIE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

  public static EOEngageCtrlPlanco createEngageCtrlPlanco(EOEditingContext editingContext, NSTimestamp epcoDateSaisie
, java.math.BigDecimal epcoHtSaisie
, java.math.BigDecimal epcoMontantBudgetaire
, java.math.BigDecimal epcoMontantBudgetaireReste
, java.math.BigDecimal epcoTtcSaisie
, java.math.BigDecimal epcoTvaSaisie
, Integer exeOrdre
) {
    EOEngageCtrlPlanco eo = (EOEngageCtrlPlanco) createAndInsertInstance(editingContext, _EOEngageCtrlPlanco.ENTITY_NAME);    
		eo.setEpcoDateSaisie(epcoDateSaisie);
		eo.setEpcoHtSaisie(epcoHtSaisie);
		eo.setEpcoMontantBudgetaire(epcoMontantBudgetaire);
		eo.setEpcoMontantBudgetaireReste(epcoMontantBudgetaireReste);
		eo.setEpcoTtcSaisie(epcoTtcSaisie);
		eo.setEpcoTvaSaisie(epcoTvaSaisie);
		eo.setExeOrdre(exeOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngageCtrlPlanco.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngageCtrlPlanco.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEngageCtrlPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngageCtrlPlanco)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEngageCtrlPlanco localInstanceIn(EOEditingContext editingContext, EOEngageCtrlPlanco eo) {
    EOEngageCtrlPlanco localInstance = (eo == null) ? null : (EOEngageCtrlPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngageCtrlPlanco#localInstanceIn a la place.
   */
	public static EOEngageCtrlPlanco localInstanceOf(EOEditingContext editingContext, EOEngageCtrlPlanco eo) {
		return EOEngageCtrlPlanco.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEngageCtrlPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngageCtrlPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngageCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngageCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngageCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngageCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngageCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngageCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngageCtrlPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngageCtrlPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngageCtrlPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngageCtrlPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
