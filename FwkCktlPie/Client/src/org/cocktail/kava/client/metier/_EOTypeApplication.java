/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeApplication.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeApplication extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "TypeApplication";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_type_application";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tyapId";

	public static final String TYAP_LIBELLE_KEY = "tyapLibelle";
	public static final String TYAP_STRID_KEY = "tyapStrid";

// Attributs non visibles
	public static final String DOM_ID_KEY = "domId";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String TYAP_LIBELLE_COLKEY = "tyap_libelle";
	public static final String TYAP_STRID_COLKEY = "tyap_strid";

	public static final String DOM_ID_COLKEY = "DOM_ID";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";


	// Relationships
	public static final String DOMAINE_KEY = "domaine";



	// Accessors methods
  public String tyapLibelle() {
    return (String) storedValueForKey(TYAP_LIBELLE_KEY);
  }

  public void setTyapLibelle(String value) {
    takeStoredValueForKey(value, TYAP_LIBELLE_KEY);
  }

  public String tyapStrid() {
    return (String) storedValueForKey(TYAP_STRID_KEY);
  }

  public void setTyapStrid(String value) {
    takeStoredValueForKey(value, TYAP_STRID_KEY);
  }

  public org.cocktail.application.client.eof.EODomaine domaine() {
    return (org.cocktail.application.client.eof.EODomaine)storedValueForKey(DOMAINE_KEY);
  }

  public void setDomaineRelationship(org.cocktail.application.client.eof.EODomaine value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EODomaine oldValue = domaine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DOMAINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DOMAINE_KEY);
    }
  }
  

  public static EOTypeApplication createTypeApplication(EOEditingContext editingContext, String tyapLibelle
, String tyapStrid
) {
    EOTypeApplication eo = (EOTypeApplication) createAndInsertInstance(editingContext, _EOTypeApplication.ENTITY_NAME);    
		eo.setTyapLibelle(tyapLibelle);
		eo.setTyapStrid(tyapStrid);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTypeApplication.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTypeApplication.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOTypeApplication localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeApplication)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTypeApplication localInstanceIn(EOEditingContext editingContext, EOTypeApplication eo) {
    EOTypeApplication localInstance = (eo == null) ? null : (EOTypeApplication)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTypeApplication#localInstanceIn a la place.
   */
	public static EOTypeApplication localInstanceOf(EOEditingContext editingContext, EOTypeApplication eo) {
		return EOTypeApplication.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTypeApplication fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTypeApplication fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeApplication eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeApplication)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeApplication eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeApplication)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTypeApplication fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeApplication eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeApplication ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeApplication fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
