/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecette.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecette extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "Recette";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.recette";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recId";

	public static final String FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY = "facture_personne_persNomPrenom";
	public static final String FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY = "facture_typeApplication_tyapLibelle";
	public static final String REC_DATE_SAISIE_KEY = "recDateSaisie";
	public static final String REC_HT_SAISIE_KEY = "recHtSaisie";
	public static final String REC_LIB_KEY = "recLib";
	public static final String REC_MONTANT_BUDGETAIRE_KEY = "recMontantBudgetaire";
	public static final String REC_NUMERO_KEY = "recNumero";
	public static final String REC_TTC_SAISIE_KEY = "recTtcSaisie";
	public static final String REC_TTC_SAISIE_STRING_KEY = "recTtcSaisieString";
	public static final String REC_TVA_SAISIE_KEY = "recTvaSaisie";
	public static final String TIT_ID_KEY = "titId";
	public static final String TIT_NUMERO_KEY = "titNumero";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FAC_ID_KEY = "facId";
	public static final String REC_ID_KEY = "recId";
	public static final String REC_ID_REDUCTION_KEY = "recIdReduction";
	public static final String RPP_ID_KEY = "rppId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String FACTURE_PERSONNE_PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_COLKEY = "$attribute.columnName";
	public static final String REC_DATE_SAISIE_COLKEY = "REC_DATE_SAISIE";
	public static final String REC_HT_SAISIE_COLKEY = "REC_HT_SAISIE";
	public static final String REC_LIB_COLKEY = "REC_LIB";
	public static final String REC_MONTANT_BUDGETAIRE_COLKEY = "REC_MONTANT_BUDGETAIRE";
	public static final String REC_NUMERO_COLKEY = "REC_NUMERO";
	public static final String REC_TTC_SAISIE_COLKEY = "REC_TTC_SAISIE";
	public static final String REC_TTC_SAISIE_STRING_COLKEY = "$attribute.columnName";
	public static final String REC_TVA_SAISIE_COLKEY = "REC_TVA_SAISIE";
	public static final String TIT_ID_COLKEY = "$attribute.columnName";
	public static final String TIT_NUMERO_COLKEY = "$attribute.columnName";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String REC_ID_REDUCTION_COLKEY = "REC_ID_REDUCTION";
	public static final String RPP_ID_COLKEY = "RPP_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String FACTURE_KEY = "facture";
	public static final String PI_DEP_RECS_KEY = "piDepRecs";
	public static final String RECETTE_CTRL_ACTIONS_KEY = "recetteCtrlActions";
	public static final String RECETTE_CTRL_ANALYTIQUES_KEY = "recetteCtrlAnalytiques";
	public static final String RECETTE_CTRL_CONVENTIONS_KEY = "recetteCtrlConventions";
	public static final String RECETTE_CTRL_PLANCOS_KEY = "recetteCtrlPlancos";
	public static final String RECETTE_PAPIER_KEY = "recettePapier";
	public static final String RECETTE_REDUCTION_KEY = "recetteReduction";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String facture_personne_persNomPrenom() {
    return (String) storedValueForKey(FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public void setFacture_personne_persNomPrenom(String value) {
    takeStoredValueForKey(value, FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public String facture_typeApplication_tyapLibelle() {
    return (String) storedValueForKey(FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY);
  }

  public void setFacture_typeApplication_tyapLibelle(String value) {
    takeStoredValueForKey(value, FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY);
  }

  public NSTimestamp recDateSaisie() {
    return (NSTimestamp) storedValueForKey(REC_DATE_SAISIE_KEY);
  }

  public void setRecDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, REC_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal recHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REC_HT_SAISIE_KEY);
  }

  public void setRecHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_HT_SAISIE_KEY);
  }

  public String recLib() {
    return (String) storedValueForKey(REC_LIB_KEY);
  }

  public void setRecLib(String value) {
    takeStoredValueForKey(value, REC_LIB_KEY);
  }

  public java.math.BigDecimal recMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONTANT_BUDGETAIRE_KEY);
  }

  public void setRecMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONTANT_BUDGETAIRE_KEY);
  }

  public Integer recNumero() {
    return (Integer) storedValueForKey(REC_NUMERO_KEY);
  }

  public void setRecNumero(Integer value) {
    takeStoredValueForKey(value, REC_NUMERO_KEY);
  }

  public java.math.BigDecimal recTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REC_TTC_SAISIE_KEY);
  }

  public void setRecTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_TTC_SAISIE_KEY);
  }

  public String recTtcSaisieString() {
    return (String) storedValueForKey(REC_TTC_SAISIE_STRING_KEY);
  }

  public void setRecTtcSaisieString(String value) {
    takeStoredValueForKey(value, REC_TTC_SAISIE_STRING_KEY);
  }

  public java.math.BigDecimal recTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REC_TVA_SAISIE_KEY);
  }

  public void setRecTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_TVA_SAISIE_KEY);
  }

  public Integer titId() {
    return (Integer) storedValueForKey(TIT_ID_KEY);
  }

  public void setTitId(Integer value) {
    takeStoredValueForKey(value, TIT_ID_KEY);
  }

  public Integer titNumero() {
    return (Integer) storedValueForKey(TIT_NUMERO_KEY);
  }

  public void setTitNumero(Integer value) {
    takeStoredValueForKey(value, TIT_NUMERO_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOFacture facture() {
    return (org.cocktail.kava.client.metier.EOFacture)storedValueForKey(FACTURE_KEY);
  }

  public void setFactureRelationship(org.cocktail.kava.client.metier.EOFacture value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOFacture oldValue = facture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FACTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EORecettePapier recettePapier() {
    return (org.cocktail.kava.client.metier.EORecettePapier)storedValueForKey(RECETTE_PAPIER_KEY);
  }

  public void setRecettePapierRelationship(org.cocktail.kava.client.metier.EORecettePapier value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EORecettePapier oldValue = recettePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EORecette recetteReduction() {
    return (org.cocktail.kava.client.metier.EORecette)storedValueForKey(RECETTE_REDUCTION_KEY);
  }

  public void setRecetteReductionRelationship(org.cocktail.kava.client.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EORecette oldValue = recetteReduction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_REDUCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_REDUCTION_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray piDepRecs() {
    return (NSArray)storedValueForKey(PI_DEP_RECS_KEY);
  }

  public NSArray piDepRecs(EOQualifier qualifier) {
    return piDepRecs(qualifier, null, false);
  }

  public NSArray piDepRecs(EOQualifier qualifier, boolean fetch) {
    return piDepRecs(qualifier, null, fetch);
  }

  public NSArray piDepRecs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOPiDepRec.RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOPiDepRec.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = piDepRecs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPiDepRecsRelationship(org.cocktail.kava.client.metier.EOPiDepRec object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PI_DEP_RECS_KEY);
  }

  public void removeFromPiDepRecsRelationship(org.cocktail.kava.client.metier.EOPiDepRec object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PI_DEP_RECS_KEY);
  }

  public org.cocktail.kava.client.metier.EOPiDepRec createPiDepRecsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PiDepRec");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PI_DEP_RECS_KEY);
    return (org.cocktail.kava.client.metier.EOPiDepRec) eo;
  }

  public void deletePiDepRecsRelationship(org.cocktail.kava.client.metier.EOPiDepRec object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PI_DEP_RECS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPiDepRecsRelationships() {
    Enumeration objects = piDepRecs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePiDepRecsRelationship((org.cocktail.kava.client.metier.EOPiDepRec)objects.nextElement());
    }
  }

  public NSArray recetteCtrlActions() {
    return (NSArray)storedValueForKey(RECETTE_CTRL_ACTIONS_KEY);
  }

  public NSArray recetteCtrlActions(EOQualifier qualifier) {
    return recetteCtrlActions(qualifier, null);
  }

  public NSArray recetteCtrlActions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = recetteCtrlActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRecetteCtrlActionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ACTIONS_KEY);
  }

  public void removeFromRecetteCtrlActionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ACTIONS_KEY);
  }

  public org.cocktail.kava.client.metier.EORecetteCtrlAction createRecetteCtrlActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteCtrlAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_CTRL_ACTIONS_KEY);
    return (org.cocktail.kava.client.metier.EORecetteCtrlAction) eo;
  }

  public void deleteRecetteCtrlActionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteCtrlActionsRelationships() {
    Enumeration objects = recetteCtrlActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteCtrlActionsRelationship((org.cocktail.kava.client.metier.EORecetteCtrlAction)objects.nextElement());
    }
  }

  public NSArray recetteCtrlAnalytiques() {
    return (NSArray)storedValueForKey(RECETTE_CTRL_ANALYTIQUES_KEY);
  }

  public NSArray recetteCtrlAnalytiques(EOQualifier qualifier) {
    return recetteCtrlAnalytiques(qualifier, null);
  }

  public NSArray recetteCtrlAnalytiques(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = recetteCtrlAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRecetteCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ANALYTIQUES_KEY);
  }

  public void removeFromRecetteCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ANALYTIQUES_KEY);
  }

  public org.cocktail.kava.client.metier.EORecetteCtrlAnalytique createRecetteCtrlAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteCtrlAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_CTRL_ANALYTIQUES_KEY);
    return (org.cocktail.kava.client.metier.EORecetteCtrlAnalytique) eo;
  }

  public void deleteRecetteCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteCtrlAnalytiquesRelationships() {
    Enumeration objects = recetteCtrlAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteCtrlAnalytiquesRelationship((org.cocktail.kava.client.metier.EORecetteCtrlAnalytique)objects.nextElement());
    }
  }

  public NSArray recetteCtrlConventions() {
    return (NSArray)storedValueForKey(RECETTE_CTRL_CONVENTIONS_KEY);
  }

  public NSArray recetteCtrlConventions(EOQualifier qualifier) {
    return recetteCtrlConventions(qualifier, null);
  }

  public NSArray recetteCtrlConventions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = recetteCtrlConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRecetteCtrlConventionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_CONVENTIONS_KEY);
  }

  public void removeFromRecetteCtrlConventionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_CONVENTIONS_KEY);
  }

  public org.cocktail.kava.client.metier.EORecetteCtrlConvention createRecetteCtrlConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteCtrlConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_CTRL_CONVENTIONS_KEY);
    return (org.cocktail.kava.client.metier.EORecetteCtrlConvention) eo;
  }

  public void deleteRecetteCtrlConventionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteCtrlConventionsRelationships() {
    Enumeration objects = recetteCtrlConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteCtrlConventionsRelationship((org.cocktail.kava.client.metier.EORecetteCtrlConvention)objects.nextElement());
    }
  }

  public NSArray recetteCtrlPlancos() {
    return (NSArray)storedValueForKey(RECETTE_CTRL_PLANCOS_KEY);
  }

  public NSArray recetteCtrlPlancos(EOQualifier qualifier) {
    return recetteCtrlPlancos(qualifier, null, false);
  }

  public NSArray recetteCtrlPlancos(EOQualifier qualifier, boolean fetch) {
    return recetteCtrlPlancos(qualifier, null, fetch);
  }

  public NSArray recetteCtrlPlancos(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORecetteCtrlPlanco.RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORecetteCtrlPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = recetteCtrlPlancos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRecetteCtrlPlancosRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCOS_KEY);
  }

  public void removeFromRecetteCtrlPlancosRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCOS_KEY);
  }

  public org.cocktail.kava.client.metier.EORecetteCtrlPlanco createRecetteCtrlPlancosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteCtrlPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_CTRL_PLANCOS_KEY);
    return (org.cocktail.kava.client.metier.EORecetteCtrlPlanco) eo;
  }

  public void deleteRecetteCtrlPlancosRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteCtrlPlancosRelationships() {
    Enumeration objects = recetteCtrlPlancos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteCtrlPlancosRelationship((org.cocktail.kava.client.metier.EORecetteCtrlPlanco)objects.nextElement());
    }
  }


  public static EORecette createRecette(EOEditingContext editingContext, String facture_personne_persNomPrenom
, String facture_typeApplication_tyapLibelle
, NSTimestamp recDateSaisie
, java.math.BigDecimal recHtSaisie
, java.math.BigDecimal recMontantBudgetaire
, Integer recNumero
, java.math.BigDecimal recTtcSaisie
, java.math.BigDecimal recTvaSaisie
) {
    EORecette eo = (EORecette) createAndInsertInstance(editingContext, _EORecette.ENTITY_NAME);    
		eo.setFacture_personne_persNomPrenom(facture_personne_persNomPrenom);
		eo.setFacture_typeApplication_tyapLibelle(facture_typeApplication_tyapLibelle);
		eo.setRecDateSaisie(recDateSaisie);
		eo.setRecHtSaisie(recHtSaisie);
		eo.setRecMontantBudgetaire(recMontantBudgetaire);
		eo.setRecNumero(recNumero);
		eo.setRecTtcSaisie(recTtcSaisie);
		eo.setRecTvaSaisie(recTvaSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecette.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecette.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EORecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecette)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORecette localInstanceIn(EOEditingContext editingContext, EORecette eo) {
    EORecette localInstance = (eo == null) ? null : (EORecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecette#localInstanceIn a la place.
   */
	public static EORecette localInstanceOf(EOEditingContext editingContext, EORecette eo) {
		return EORecette.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
