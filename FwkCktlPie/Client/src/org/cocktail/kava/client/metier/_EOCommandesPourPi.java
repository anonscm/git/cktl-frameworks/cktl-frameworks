/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommandesPourPi.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCommandesPourPi extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "CommandesPourPi";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_commandes_pour_pi";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "commId";

	public static final String COMM_DATE_CREATION_KEY = "commDateCreation";
	public static final String COMM_LIBELLE_KEY = "commLibelle";
	public static final String COMM_NUMERO_KEY = "commNumero";
	public static final String COMM_REFERENCE_KEY = "commReference";
	public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
	public static final String ENG_ID_KEY = "engId";
	public static final String ENG_NUMERO_KEY = "engNumero";
	public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
	public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";
	public static final String PCO_NUM_KEY = "pcoNum";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String COMM_DATE_CREATION_COLKEY = "COMM_DATE_CREATION";
	public static final String COMM_LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String COMM_NUMERO_COLKEY = "COMM_NUMERO";
	public static final String COMM_REFERENCE_COLKEY = "COMM_REFERENCE";
	public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
	public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
	public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String ORGAN_KEY = "organ";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp commDateCreation() {
    return (NSTimestamp) storedValueForKey(COMM_DATE_CREATION_KEY);
  }

  public void setCommDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, COMM_DATE_CREATION_KEY);
  }

  public String commLibelle() {
    return (String) storedValueForKey(COMM_LIBELLE_KEY);
  }

  public void setCommLibelle(String value) {
    takeStoredValueForKey(value, COMM_LIBELLE_KEY);
  }

  public Integer commNumero() {
    return (Integer) storedValueForKey(COMM_NUMERO_KEY);
  }

  public void setCommNumero(Integer value) {
    takeStoredValueForKey(value, COMM_NUMERO_KEY);
  }

  public String commReference() {
    return (String) storedValueForKey(COMM_REFERENCE_KEY);
  }

  public void setCommReference(String value) {
    takeStoredValueForKey(value, COMM_REFERENCE_KEY);
  }

  public java.math.BigDecimal engHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_HT_SAISIE_KEY);
  }

  public void setEngHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_HT_SAISIE_KEY);
  }

  public Integer engId() {
    return (Integer) storedValueForKey(ENG_ID_KEY);
  }

  public void setEngId(Integer value) {
    takeStoredValueForKey(value, ENG_ID_KEY);
  }

  public Integer engNumero() {
    return (Integer) storedValueForKey(ENG_NUMERO_KEY);
  }

  public void setEngNumero(Integer value) {
    takeStoredValueForKey(value, ENG_NUMERO_KEY);
  }

  public java.math.BigDecimal engTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TTC_SAISIE_KEY);
  }

  public void setEngTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal engTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TVA_SAISIE_KEY);
  }

  public void setEngTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TVA_SAISIE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
    return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
  }

  public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOFournisUlr oldValue = fournisUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOOrgan organ() {
    return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOCommandesPourPi createCommandesPourPi(EOEditingContext editingContext, NSTimestamp commDateCreation
, String commLibelle
, Integer commNumero
, String commReference
, java.math.BigDecimal engHtSaisie
, Integer engId
, Integer engNumero
, java.math.BigDecimal engTtcSaisie
, java.math.BigDecimal engTvaSaisie
, String pcoNum
) {
    EOCommandesPourPi eo = (EOCommandesPourPi) createAndInsertInstance(editingContext, _EOCommandesPourPi.ENTITY_NAME);    
		eo.setCommDateCreation(commDateCreation);
		eo.setCommLibelle(commLibelle);
		eo.setCommNumero(commNumero);
		eo.setCommReference(commReference);
		eo.setEngHtSaisie(engHtSaisie);
		eo.setEngId(engId);
		eo.setEngNumero(engNumero);
		eo.setEngTtcSaisie(engTtcSaisie);
		eo.setEngTvaSaisie(engTvaSaisie);
		eo.setPcoNum(pcoNum);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCommandesPourPi.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCommandesPourPi.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCommandesPourPi localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommandesPourPi)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCommandesPourPi localInstanceIn(EOEditingContext editingContext, EOCommandesPourPi eo) {
    EOCommandesPourPi localInstance = (eo == null) ? null : (EOCommandesPourPi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCommandesPourPi#localInstanceIn a la place.
   */
	public static EOCommandesPourPi localInstanceOf(EOEditingContext editingContext, EOCommandesPourPi eo) {
		return EOCommandesPourPi.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCommandesPourPi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCommandesPourPi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandesPourPi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandesPourPi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandesPourPi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandesPourPi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandesPourPi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandesPourPi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCommandesPourPi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandesPourPi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandesPourPi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandesPourPi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
