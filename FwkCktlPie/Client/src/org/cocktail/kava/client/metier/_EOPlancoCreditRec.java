/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlancoCreditRec.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPlancoCreditRec extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "PlancoCreditRec";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_planco_credit_rec";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pccOrdre";

	public static final String PCC_ETAT_KEY = "pccEtat";
	public static final String PCC_ORDRE_KEY = "pccOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_VALIDITE_KEY = "pcoValidite";

// Attributs non visibles
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String PCC_ETAT_COLKEY = "PCC_ETAT";
	public static final String PCC_ORDRE_COLKEY = "PCC_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_VALIDITE_COLKEY = "pco_Validite";

	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";



	// Accessors methods
  public String pccEtat() {
    return (String) storedValueForKey(PCC_ETAT_KEY);
  }

  public void setPccEtat(String value) {
    takeStoredValueForKey(value, PCC_ETAT_KEY);
  }

  public Integer pccOrdre() {
    return (Integer) storedValueForKey(PCC_ORDRE_KEY);
  }

  public void setPccOrdre(Integer value) {
    takeStoredValueForKey(value, PCC_ORDRE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String pcoValidite() {
    return (String) storedValueForKey(PCO_VALIDITE_KEY);
  }

  public void setPcoValidite(String value) {
    takeStoredValueForKey(value, PCO_VALIDITE_KEY);
  }

  public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
  }

  public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCreditRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
    }
  }
  

  public static EOPlancoCreditRec createPlancoCreditRec(EOEditingContext editingContext, String pccEtat
, Integer pccOrdre
, String pcoNum
, String pcoValidite
) {
    EOPlancoCreditRec eo = (EOPlancoCreditRec) createAndInsertInstance(editingContext, _EOPlancoCreditRec.ENTITY_NAME);    
		eo.setPccEtat(pccEtat);
		eo.setPccOrdre(pccOrdre);
		eo.setPcoNum(pcoNum);
		eo.setPcoValidite(pcoValidite);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPlancoCreditRec.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPlancoCreditRec.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPlancoCreditRec localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPlancoCreditRec)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPlancoCreditRec localInstanceIn(EOEditingContext editingContext, EOPlancoCreditRec eo) {
    EOPlancoCreditRec localInstance = (eo == null) ? null : (EOPlancoCreditRec)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPlancoCreditRec#localInstanceIn a la place.
   */
	public static EOPlancoCreditRec localInstanceOf(EOEditingContext editingContext, EOPlancoCreditRec eo) {
		return EOPlancoCreditRec.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPlancoCreditRec fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPlancoCreditRec fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlancoCreditRec eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlancoCreditRec)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlancoCreditRec fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlancoCreditRec fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlancoCreditRec eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlancoCreditRec)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPlancoCreditRec fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlancoCreditRec eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlancoCreditRec ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlancoCreditRec fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
