/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFactureCtrlPlanco.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFactureCtrlPlanco extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "FactureCtrlPlanco";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_ctrl_planco";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fpcoId";

	public static final String FAC_ID_KEY = "facId";
	public static final String FPCO_DATE_SAISIE_KEY = "fpcoDateSaisie";
	public static final String FPCO_HT_RESTE_KEY = "fpcoHtReste";
	public static final String FPCO_HT_SAISIE_KEY = "fpcoHtSaisie";
	public static final String FPCO_TTC_SAISIE_KEY = "fpcoTtcSaisie";
	public static final String FPCO_TVA_RESTE_KEY = "fpcoTvaReste";
	public static final String FPCO_TVA_SAISIE_KEY = "fpcoTvaSaisie";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FPCO_ID_KEY = "fpcoId";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FPCO_DATE_SAISIE_COLKEY = "FPCO_DATE_SAISIE";
	public static final String FPCO_HT_RESTE_COLKEY = "FPCO_HT_RESTE";
	public static final String FPCO_HT_SAISIE_COLKEY = "FPCO_HT_SAISIE";
	public static final String FPCO_TTC_SAISIE_COLKEY = "FPCO_TTC_SAISIE";
	public static final String FPCO_TVA_RESTE_COLKEY = "FPCO_TVA_RESTE";
	public static final String FPCO_TVA_SAISIE_COLKEY = "FPCO_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FPCO_ID_COLKEY = "FPCO_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
  public Integer facId() {
    return (Integer) storedValueForKey(FAC_ID_KEY);
  }

  public void setFacId(Integer value) {
    takeStoredValueForKey(value, FAC_ID_KEY);
  }

  public NSTimestamp fpcoDateSaisie() {
    return (NSTimestamp) storedValueForKey(FPCO_DATE_SAISIE_KEY);
  }

  public void setFpcoDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, FPCO_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal fpcoHtReste() {
    return (java.math.BigDecimal) storedValueForKey(FPCO_HT_RESTE_KEY);
  }

  public void setFpcoHtReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FPCO_HT_RESTE_KEY);
  }

  public java.math.BigDecimal fpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FPCO_HT_SAISIE_KEY);
  }

  public void setFpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal fpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FPCO_TTC_SAISIE_KEY);
  }

  public void setFpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal fpcoTvaReste() {
    return (java.math.BigDecimal) storedValueForKey(FPCO_TVA_RESTE_KEY);
  }

  public void setFpcoTvaReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FPCO_TVA_RESTE_KEY);
  }

  public java.math.BigDecimal fpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FPCO_TVA_SAISIE_KEY);
  }

  public void setFpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FPCO_TVA_SAISIE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

  public static EOFactureCtrlPlanco createFactureCtrlPlanco(EOEditingContext editingContext, Integer facId
, NSTimestamp fpcoDateSaisie
, java.math.BigDecimal fpcoHtReste
, java.math.BigDecimal fpcoHtSaisie
, java.math.BigDecimal fpcoTtcSaisie
, java.math.BigDecimal fpcoTvaReste
, java.math.BigDecimal fpcoTvaSaisie
) {
    EOFactureCtrlPlanco eo = (EOFactureCtrlPlanco) createAndInsertInstance(editingContext, _EOFactureCtrlPlanco.ENTITY_NAME);    
		eo.setFacId(facId);
		eo.setFpcoDateSaisie(fpcoDateSaisie);
		eo.setFpcoHtReste(fpcoHtReste);
		eo.setFpcoHtSaisie(fpcoHtSaisie);
		eo.setFpcoTtcSaisie(fpcoTtcSaisie);
		eo.setFpcoTvaReste(fpcoTvaReste);
		eo.setFpcoTvaSaisie(fpcoTvaSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOFactureCtrlPlanco.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOFactureCtrlPlanco.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOFactureCtrlPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFactureCtrlPlanco)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOFactureCtrlPlanco localInstanceIn(EOEditingContext editingContext, EOFactureCtrlPlanco eo) {
    EOFactureCtrlPlanco localInstance = (eo == null) ? null : (EOFactureCtrlPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOFactureCtrlPlanco#localInstanceIn a la place.
   */
	public static EOFactureCtrlPlanco localInstanceOf(EOEditingContext editingContext, EOFactureCtrlPlanco eo) {
		return EOFactureCtrlPlanco.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOFactureCtrlPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOFactureCtrlPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFactureCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFactureCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFactureCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFactureCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFactureCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFactureCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOFactureCtrlPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFactureCtrlPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFactureCtrlPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFactureCtrlPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
