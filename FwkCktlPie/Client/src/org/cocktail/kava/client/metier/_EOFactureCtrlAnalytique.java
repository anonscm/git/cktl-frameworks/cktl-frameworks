/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFactureCtrlAnalytique.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFactureCtrlAnalytique extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "FactureCtrlAnalytique";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_ctrl_analytique";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fanaId";

	public static final String FAC_ID_KEY = "facId";
	public static final String FANA_DATE_SAISIE_KEY = "fanaDateSaisie";
	public static final String FANA_HT_SAISIE_KEY = "fanaHtSaisie";
	public static final String FANA_MONTANT_BUDGETAIRE_KEY = "fanaMontantBudgetaire";
	public static final String FANA_MONTANT_BUDGETAIRE_RESTE_KEY = "fanaMontantBudgetaireReste";
	public static final String FANA_TTC_SAISIE_KEY = "fanaTtcSaisie";
	public static final String FANA_TVA_SAISIE_KEY = "fanaTvaSaisie";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FANA_ID_KEY = "fanaId";

//Colonnes dans la base de donnees
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FANA_DATE_SAISIE_COLKEY = "FANA_DATE_SAISIE";
	public static final String FANA_HT_SAISIE_COLKEY = "FANA_HT_SAISIE";
	public static final String FANA_MONTANT_BUDGETAIRE_COLKEY = "FANA_MONTANT_BUDGETAIRE";
	public static final String FANA_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FANA_MONTANT_BUDGETAIRE_RESTE";
	public static final String FANA_TTC_SAISIE_COLKEY = "FANA_TTC_SAISIE";
	public static final String FANA_TVA_SAISIE_COLKEY = "FANA_TVA_SAISIE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FANA_ID_COLKEY = "FANA_ID";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public Integer facId() {
    return (Integer) storedValueForKey(FAC_ID_KEY);
  }

  public void setFacId(Integer value) {
    takeStoredValueForKey(value, FAC_ID_KEY);
  }

  public NSTimestamp fanaDateSaisie() {
    return (NSTimestamp) storedValueForKey(FANA_DATE_SAISIE_KEY);
  }

  public void setFanaDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, FANA_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal fanaHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FANA_HT_SAISIE_KEY);
  }

  public void setFanaHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FANA_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal fanaMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(FANA_MONTANT_BUDGETAIRE_KEY);
  }

  public void setFanaMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FANA_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal fanaMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(FANA_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setFanaMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FANA_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal fanaTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FANA_TTC_SAISIE_KEY);
  }

  public void setFanaTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FANA_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal fanaTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FANA_TVA_SAISIE_KEY);
  }

  public void setFanaTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FANA_TVA_SAISIE_KEY);
  }

  public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

  public static EOFactureCtrlAnalytique createFactureCtrlAnalytique(EOEditingContext editingContext, Integer facId
, NSTimestamp fanaDateSaisie
, java.math.BigDecimal fanaHtSaisie
, java.math.BigDecimal fanaMontantBudgetaire
, java.math.BigDecimal fanaMontantBudgetaireReste
, java.math.BigDecimal fanaTtcSaisie
, java.math.BigDecimal fanaTvaSaisie
) {
    EOFactureCtrlAnalytique eo = (EOFactureCtrlAnalytique) createAndInsertInstance(editingContext, _EOFactureCtrlAnalytique.ENTITY_NAME);    
		eo.setFacId(facId);
		eo.setFanaDateSaisie(fanaDateSaisie);
		eo.setFanaHtSaisie(fanaHtSaisie);
		eo.setFanaMontantBudgetaire(fanaMontantBudgetaire);
		eo.setFanaMontantBudgetaireReste(fanaMontantBudgetaireReste);
		eo.setFanaTtcSaisie(fanaTtcSaisie);
		eo.setFanaTvaSaisie(fanaTvaSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOFactureCtrlAnalytique.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOFactureCtrlAnalytique.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOFactureCtrlAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFactureCtrlAnalytique)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOFactureCtrlAnalytique localInstanceIn(EOEditingContext editingContext, EOFactureCtrlAnalytique eo) {
    EOFactureCtrlAnalytique localInstance = (eo == null) ? null : (EOFactureCtrlAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOFactureCtrlAnalytique#localInstanceIn a la place.
   */
	public static EOFactureCtrlAnalytique localInstanceOf(EOEditingContext editingContext, EOFactureCtrlAnalytique eo) {
		return EOFactureCtrlAnalytique.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOFactureCtrlAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOFactureCtrlAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFactureCtrlAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFactureCtrlAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFactureCtrlAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFactureCtrlAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFactureCtrlAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFactureCtrlAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOFactureCtrlAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFactureCtrlAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFactureCtrlAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFactureCtrlAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
