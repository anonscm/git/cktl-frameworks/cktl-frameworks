/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFonction.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFonction extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "Fonction";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.fonction";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fonOrdre";

	public static final String FON_CATEGORIE_KEY = "fonCategorie";
	public static final String FON_DESCRIPTION_KEY = "fonDescription";
	public static final String FON_ID_INTERNE_KEY = "fonIdInterne";
	public static final String FON_LIBELLE_KEY = "fonLibelle";
	public static final String FON_SPEC_EXERCICE_KEY = "fonSpecExercice";
	public static final String FON_SPEC_GESTION_KEY = "fonSpecGestion";
	public static final String FON_SPEC_ORGAN_KEY = "fonSpecOrgan";

// Attributs non visibles
	public static final String FON_ORDRE_KEY = "fonOrdre";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String FON_CATEGORIE_COLKEY = "FON_CATEGORIE";
	public static final String FON_DESCRIPTION_COLKEY = "FON_DESCRIPTION";
	public static final String FON_ID_INTERNE_COLKEY = "FON_ID_INTERNE";
	public static final String FON_LIBELLE_COLKEY = "FON_LIBELLE";
	public static final String FON_SPEC_EXERCICE_COLKEY = "FON_SPEC_EXERCICE";
	public static final String FON_SPEC_GESTION_COLKEY = "FON_SPEC_GESTION";
	public static final String FON_SPEC_ORGAN_COLKEY = "FON_SPEC_ORGAN";

	public static final String FON_ORDRE_COLKEY = "FON_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYap_ID";


	// Relationships
	public static final String TYPE_APPLICATION_KEY = "typeApplication";



	// Accessors methods
  public String fonCategorie() {
    return (String) storedValueForKey(FON_CATEGORIE_KEY);
  }

  public void setFonCategorie(String value) {
    takeStoredValueForKey(value, FON_CATEGORIE_KEY);
  }

  public String fonDescription() {
    return (String) storedValueForKey(FON_DESCRIPTION_KEY);
  }

  public void setFonDescription(String value) {
    takeStoredValueForKey(value, FON_DESCRIPTION_KEY);
  }

  public String fonIdInterne() {
    return (String) storedValueForKey(FON_ID_INTERNE_KEY);
  }

  public void setFonIdInterne(String value) {
    takeStoredValueForKey(value, FON_ID_INTERNE_KEY);
  }

  public String fonLibelle() {
    return (String) storedValueForKey(FON_LIBELLE_KEY);
  }

  public void setFonLibelle(String value) {
    takeStoredValueForKey(value, FON_LIBELLE_KEY);
  }

  public String fonSpecExercice() {
    return (String) storedValueForKey(FON_SPEC_EXERCICE_KEY);
  }

  public void setFonSpecExercice(String value) {
    takeStoredValueForKey(value, FON_SPEC_EXERCICE_KEY);
  }

  public String fonSpecGestion() {
    return (String) storedValueForKey(FON_SPEC_GESTION_KEY);
  }

  public void setFonSpecGestion(String value) {
    takeStoredValueForKey(value, FON_SPEC_GESTION_KEY);
  }

  public String fonSpecOrgan() {
    return (String) storedValueForKey(FON_SPEC_ORGAN_KEY);
  }

  public void setFonSpecOrgan(String value) {
    takeStoredValueForKey(value, FON_SPEC_ORGAN_KEY);
  }

  public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  

  public static EOFonction createFonction(EOEditingContext editingContext, String fonCategorie
, String fonIdInterne
, String fonLibelle
, String fonSpecExercice
, String fonSpecGestion
, String fonSpecOrgan
) {
    EOFonction eo = (EOFonction) createAndInsertInstance(editingContext, _EOFonction.ENTITY_NAME);    
		eo.setFonCategorie(fonCategorie);
		eo.setFonIdInterne(fonIdInterne);
		eo.setFonLibelle(fonLibelle);
		eo.setFonSpecExercice(fonSpecExercice);
		eo.setFonSpecGestion(fonSpecGestion);
		eo.setFonSpecOrgan(fonSpecOrgan);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOFonction.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOFonction.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOFonction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFonction)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOFonction localInstanceIn(EOEditingContext editingContext, EOFonction eo) {
    EOFonction localInstance = (eo == null) ? null : (EOFonction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOFonction#localInstanceIn a la place.
   */
	public static EOFonction localInstanceOf(EOEditingContext editingContext, EOFonction eo) {
		return EOFonction.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOFonction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOFonction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFonction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOFonction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFonction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFonction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFonction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
