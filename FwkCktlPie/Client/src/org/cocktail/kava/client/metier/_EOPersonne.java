/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersonne.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPersonne extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "Personne";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_personne";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "persId";

	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_NOM_PRENOM_KEY = "persNomPrenom";
	public static final String PERS_NOMPTR_KEY = "persNomptr";
	public static final String PERS_ORDRE_KEY = "persOrdre";
	public static final String PERS_TYPE_KEY = "persType";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String PERS_NOMPTR_COLKEY = "PERS_NOMPTR";
	public static final String PERS_ORDRE_COLKEY = "PERS_ORDRE";
	public static final String PERS_TYPE_COLKEY = "PERS_TYPE";



	// Relationships
	public static final String BOUTIQUE_CLIENTS_KEY = "boutiqueClients";
	public static final String INDIVIDU_ULR_KEY = "individuUlr";
	public static final String PERSONNE_TELEPHONES_KEY = "personneTelephones";
	public static final String REPART_PERSONNE_ADRESSES_KEY = "repartPersonneAdresses";
	public static final String REPART_PERSONNE_MAIL_KEY = "repartPersonneMail";
	public static final String REPART_STRUCTURES_KEY = "repartStructures";
	public static final String RPT_PERSONNE_ADRESSES_KEY = "rptPersonneAdresses";
	public static final String STRUCTURE_ULR_KEY = "structureUlr";



	// Accessors methods
  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String persLc() {
    return (String) storedValueForKey(PERS_LC_KEY);
  }

  public void setPersLc(String value) {
    takeStoredValueForKey(value, PERS_LC_KEY);
  }

  public String persLibelle() {
    return (String) storedValueForKey(PERS_LIBELLE_KEY);
  }

  public void setPersLibelle(String value) {
    takeStoredValueForKey(value, PERS_LIBELLE_KEY);
  }

  public String persNomPrenom() {
    return (String) storedValueForKey(PERS_NOM_PRENOM_KEY);
  }

  public void setPersNomPrenom(String value) {
    takeStoredValueForKey(value, PERS_NOM_PRENOM_KEY);
  }

  public String persNomptr() {
    return (String) storedValueForKey(PERS_NOMPTR_KEY);
  }

  public void setPersNomptr(String value) {
    takeStoredValueForKey(value, PERS_NOMPTR_KEY);
  }

  public Integer persOrdre() {
    return (Integer) storedValueForKey(PERS_ORDRE_KEY);
  }

  public void setPersOrdre(Integer value) {
    takeStoredValueForKey(value, PERS_ORDRE_KEY);
  }

  public String persType() {
    return (String) storedValueForKey(PERS_TYPE_KEY);
  }

  public void setPersType(String value) {
    takeStoredValueForKey(value, PERS_TYPE_KEY);
  }

  public org.cocktail.kava.client.metier.EOIndividuUlr individuUlr() {
    return (org.cocktail.kava.client.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
  }

  public void setIndividuUlrRelationship(org.cocktail.kava.client.metier.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOIndividuUlr oldValue = individuUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EORepartPersonneMail repartPersonneMail() {
    return (org.cocktail.kava.client.metier.EORepartPersonneMail)storedValueForKey(REPART_PERSONNE_MAIL_KEY);
  }

  public void setRepartPersonneMailRelationship(org.cocktail.kava.client.metier.EORepartPersonneMail value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EORepartPersonneMail oldValue = repartPersonneMail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REPART_PERSONNE_MAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REPART_PERSONNE_MAIL_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOStructureUlr structureUlr() {
    return (org.cocktail.kava.client.metier.EOStructureUlr)storedValueForKey(STRUCTURE_ULR_KEY);
  }

  public void setStructureUlrRelationship(org.cocktail.kava.client.metier.EOStructureUlr value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOStructureUlr oldValue = structureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
    }
  }
  
  public NSArray boutiqueClients() {
    return (NSArray)storedValueForKey(BOUTIQUE_CLIENTS_KEY);
  }

  public NSArray boutiqueClients(EOQualifier qualifier) {
    return boutiqueClients(qualifier, null, false);
  }

  public NSArray boutiqueClients(EOQualifier qualifier, boolean fetch) {
    return boutiqueClients(qualifier, null, fetch);
  }

  public NSArray boutiqueClients(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOBoutiqueClient.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOBoutiqueClient.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = boutiqueClients();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToBoutiqueClientsRelationship(org.cocktail.kava.client.metier.EOBoutiqueClient object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
  }

  public void removeFromBoutiqueClientsRelationship(org.cocktail.kava.client.metier.EOBoutiqueClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
  }

  public org.cocktail.kava.client.metier.EOBoutiqueClient createBoutiqueClientsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BoutiqueClient");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BOUTIQUE_CLIENTS_KEY);
    return (org.cocktail.kava.client.metier.EOBoutiqueClient) eo;
  }

  public void deleteBoutiqueClientsRelationship(org.cocktail.kava.client.metier.EOBoutiqueClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBoutiqueClientsRelationships() {
    Enumeration objects = boutiqueClients().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBoutiqueClientsRelationship((org.cocktail.kava.client.metier.EOBoutiqueClient)objects.nextElement());
    }
  }

  public NSArray personneTelephones() {
    return (NSArray)storedValueForKey(PERSONNE_TELEPHONES_KEY);
  }

  public NSArray personneTelephones(EOQualifier qualifier) {
    return personneTelephones(qualifier, null, false);
  }

  public NSArray personneTelephones(EOQualifier qualifier, boolean fetch) {
    return personneTelephones(qualifier, null, fetch);
  }

  public NSArray personneTelephones(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOPersonneTelephone.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOPersonneTelephone.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = personneTelephones();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersonneTelephonesRelationship(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSONNE_TELEPHONES_KEY);
  }

  public void removeFromPersonneTelephonesRelationship(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_TELEPHONES_KEY);
  }

  public org.cocktail.kava.client.metier.EOPersonneTelephone createPersonneTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PersonneTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSONNE_TELEPHONES_KEY);
    return (org.cocktail.kava.client.metier.EOPersonneTelephone) eo;
  }

  public void deletePersonneTelephonesRelationship(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_TELEPHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersonneTelephonesRelationships() {
    Enumeration objects = personneTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersonneTelephonesRelationship((org.cocktail.kava.client.metier.EOPersonneTelephone)objects.nextElement());
    }
  }

  public NSArray repartPersonneAdresses() {
    return (NSArray)storedValueForKey(REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier) {
    return repartPersonneAdresses(qualifier, null, false);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier, boolean fetch) {
    return repartPersonneAdresses(qualifier, null, fetch);
  }

  public NSArray repartPersonneAdresses(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORepartPersonneAdresse.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORepartPersonneAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromRepartPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.kava.client.metier.EORepartPersonneAdresse createRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.kava.client.metier.EORepartPersonneAdresse) eo;
  }

  public void deleteRepartPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartPersonneAdressesRelationships() {
    Enumeration objects = repartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartPersonneAdressesRelationship((org.cocktail.kava.client.metier.EORepartPersonneAdresse)objects.nextElement());
    }
  }

  public NSArray repartStructures() {
    return (NSArray)storedValueForKey(REPART_STRUCTURES_KEY);
  }

  public NSArray repartStructures(EOQualifier qualifier) {
    return repartStructures(qualifier, null, false);
  }

  public NSArray repartStructures(EOQualifier qualifier, boolean fetch) {
    return repartStructures(qualifier, null, fetch);
  }

  public NSArray repartStructures(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORepartStructure.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartStructures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
  }

  public void removeFromRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
  }

  public org.cocktail.kava.client.metier.EORepartStructure createRepartStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_STRUCTURES_KEY);
    return (org.cocktail.kava.client.metier.EORepartStructure) eo;
  }

  public void deleteRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartStructuresRelationships() {
    Enumeration objects = repartStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartStructuresRelationship((org.cocktail.kava.client.metier.EORepartStructure)objects.nextElement());
    }
  }

  public NSArray rptPersonneAdresses() {
    return (NSArray)storedValueForKey(RPT_PERSONNE_ADRESSES_KEY);
  }

  public NSArray rptPersonneAdresses(EOQualifier qualifier) {
    return rptPersonneAdresses(qualifier, null, false);
  }

  public NSArray rptPersonneAdresses(EOQualifier qualifier, boolean fetch) {
    return rptPersonneAdresses(qualifier, null, fetch);
  }

  public NSArray rptPersonneAdresses(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORptPersonneAdresse.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORptPersonneAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = rptPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRptPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORptPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RPT_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromRptPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORptPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RPT_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.kava.client.metier.EORptPersonneAdresse createRptPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RptPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RPT_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.kava.client.metier.EORptPersonneAdresse) eo;
  }

  public void deleteRptPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORptPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RPT_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRptPersonneAdressesRelationships() {
    Enumeration objects = rptPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRptPersonneAdressesRelationship((org.cocktail.kava.client.metier.EORptPersonneAdresse)objects.nextElement());
    }
  }


  public static EOPersonne createPersonne(EOEditingContext editingContext, Integer persId
, String persLibelle
, String persNomPrenom
, Integer persOrdre
, String persType
) {
    EOPersonne eo = (EOPersonne) createAndInsertInstance(editingContext, _EOPersonne.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setPersLibelle(persLibelle);
		eo.setPersNomPrenom(persNomPrenom);
		eo.setPersOrdre(persOrdre);
		eo.setPersType(persType);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPersonne.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPersonne.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPersonne localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersonne)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPersonne localInstanceIn(EOEditingContext editingContext, EOPersonne eo) {
    EOPersonne localInstance = (eo == null) ? null : (EOPersonne)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPersonne#localInstanceIn a la place.
   */
	public static EOPersonne localInstanceOf(EOEditingContext editingContext, EOPersonne eo) {
		return EOPersonne.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPersonne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPersonne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersonne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersonne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPersonne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersonne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersonne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersonne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
