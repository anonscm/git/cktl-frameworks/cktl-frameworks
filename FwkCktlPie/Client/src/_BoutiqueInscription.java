/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to BoutiqueInscription.java instead.
import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _BoutiqueInscription extends  AfwkClientPersRecord {
	public static final String ENTITY_NAME = "BoutiqueInscription";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.boutique_inscription";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "biId";


// Attributs non visibles
	public static final String BI_ID_KEY = "biId";
	public static final String BOUTIQUE_ID_KEY = "boutiqueId";
	public static final String PERS_ID_KEY = "persId";
	public static final String PREST_ID_KEY = "prestId";

//Colonnes dans la base de donnees

	public static final String BI_ID_COLKEY = "BI_ID";
	public static final String BOUTIQUE_ID_COLKEY = "BOUTIQUE_ID";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PREST_ID_COLKEY = "PREST_ID";


	// Relationships
	public static final String BOUTIQUE_KEY = "boutique";
	public static final String PERSONNE_KEY = "personne";
	public static final String PRESTATION_KEY = "prestation";



	// Accessors methods
  public com.webobjects.eocontrol.EOGenericRecord boutique() {
    return (com.webobjects.eocontrol.EOGenericRecord)storedValueForKey(BOUTIQUE_KEY);
  }

  public void setBoutiqueRelationship(com.webobjects.eocontrol.EOGenericRecord value) {
    if (value == null) {
    	com.webobjects.eocontrol.EOGenericRecord oldValue = boutique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BOUTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BOUTIQUE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOPersonne personne() {
    return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.kava.client.metier.EOPrestation prestation() {
    return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(PRESTATION_KEY);
  }

  public void setPrestationRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPrestation oldValue = prestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_KEY);
    }
  }
  

  public static BoutiqueInscription createBoutiqueInscription(EOEditingContext editingContext, com.webobjects.eocontrol.EOGenericRecord boutique, org.cocktail.kava.client.metier.EOPersonne personne) {
    BoutiqueInscription eo = (BoutiqueInscription) createAndInsertInstance(editingContext, _BoutiqueInscription.ENTITY_NAME);    
    eo.setBoutiqueRelationship(boutique);
    eo.setPersonneRelationship(personne);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _BoutiqueInscription.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _BoutiqueInscription.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public BoutiqueInscription localInstanceIn(EOEditingContext editingContext) {
	  		return (BoutiqueInscription)localInstanceOfObject(editingContext, this);
	  }
	  
  public static BoutiqueInscription localInstanceIn(EOEditingContext editingContext, BoutiqueInscription eo) {
    BoutiqueInscription localInstance = (eo == null) ? null : (BoutiqueInscription)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez BoutiqueInscription#localInstanceIn a la place.
   */
	public static BoutiqueInscription localInstanceOf(EOEditingContext editingContext, BoutiqueInscription eo) {
		return BoutiqueInscription.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static BoutiqueInscription fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static BoutiqueInscription fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    BoutiqueInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (BoutiqueInscription)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static BoutiqueInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static BoutiqueInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    BoutiqueInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (BoutiqueInscription)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static BoutiqueInscription fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  BoutiqueInscription eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet BoutiqueInscription ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static BoutiqueInscription fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
