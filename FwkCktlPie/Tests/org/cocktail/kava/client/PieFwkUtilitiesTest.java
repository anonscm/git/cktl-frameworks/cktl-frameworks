package org.cocktail.kava.client;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class PieFwkUtilitiesTest {

	SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
	
	@Test
	public void testDatToString() throws ParseException {
		NSTimestamp dt = new NSTimestamp(SDF.parse("01/02/2014"));
		
		assertEquals("01/02/2014", PieFwkUtilities.datToString(dt, "%d/%m/%Y"));
		
	}

	@Test
	public void testFormaterPeriodeValiditePie() throws ParseException {
		
		assertEquals("01/02/2014-28/02/2014", 
				PieFwkUtilities.formaterPeriodeValidite(
						SDF.parse("01/02/2014"),
						SDF.parse("28/02/2014")));
		
		assertEquals("01/02/2014-?", 
				PieFwkUtilities.formaterPeriodeValidite(
						SDF.parse("01/02/2014"),
						null));

		assertEquals("?-28/02/2014", 
				PieFwkUtilities.formaterPeriodeValidite(
						null,
						SDF.parse("28/02/2014")));
	
	}
	
	@Test
	public void FormaterPeriodeValidite() throws ParseException {
		
		assertEquals("20140201 - 20140228", 
				PieFwkUtilities.formaterPeriodeValidite(
						SDF.parse("01/02/2014"),
						SDF.parse("28/02/2014"),
						"yyyyMMdd", " - ", "Nil"));
		
		assertEquals("20140201 - Nil", 
				PieFwkUtilities.formaterPeriodeValidite(
						SDF.parse("01/02/2014"),
						null,
						"yyyyMMdd", " - ", "Nil"));

		assertEquals("Nil - 20140228", 
				PieFwkUtilities.formaterPeriodeValidite(
						null,
						SDF.parse("28/02/2014"),
						"yyyyMMdd", " - ", "Nil"));
	
	}


}
