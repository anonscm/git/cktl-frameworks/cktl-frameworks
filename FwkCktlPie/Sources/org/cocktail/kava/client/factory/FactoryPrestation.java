/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOCodeAnalytique;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOEngageCtrlAction;
import org.cocktail.kava.client.metier.EOEngageCtrlAnalytique;
import org.cocktail.kava.client.metier.EOEngageCtrlConvention;
import org.cocktail.kava.client.metier.EOEngageCtrlPlanco;
import org.cocktail.kava.client.metier.EOLolfNomenclatureDepense;
import org.cocktail.kava.client.metier.EOOrgan;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOPrestationLigne;
import org.cocktail.kava.client.metier.EOTauxProrata;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.procedures.ApiPrestation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class FactoryPrestation extends Factory {

	public FactoryPrestation() {
		super();
	}

	public FactoryPrestation(boolean withLog) {
		super(withLog);
	}

	public static EOPrestation newObject(EOEditingContext ec) {
		EOPrestation object = (EOPrestation) Factory.instanceForEntity(ec, EOPrestation.ENTITY_NAME);
		object.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
		object.setPrestDate(Factory.getDateJour());
		ec.insertObject(object);
		return object;
	}

	/**
	 * Valide la prestation cote client, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible de valider
	 */
	public static void valideClient(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.prestDateValideClient() != null) {
			// deja validee client
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la valider !");
		}
		if (!prestation.isValidableClient()) {
			throw new Exception("Il manque des informations budgetaires cote client, impossible de valider la prestation "
					+ prestation.prestNumero() + " !");
		}

		ApiPrestation.validePrestationClient(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	/**
	 * Devalide la prestation cote client, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible de devalider
	 */
	public static void devalideClient(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.prestDateValideClient() == null) {
			// deja devalidee client
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la devalider !");
		}
		decloture(ec, prestation, utilisateur);
		devalidePrestataire(ec, prestation, utilisateur);
		ApiPrestation.devalidePrestationClient(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	/**
	 * Valide la prestation cote prestataire, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible de valider
	 */
	public static void validePrestataire(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.prestDateValidePrest() != null) {
			// deja validee prestataire
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la valider !");
		}
		if (!prestation.isValidablePrest()) {
			throw new Exception("Il manque des informations budgetaires cote prestataire, impossible de valider la prestation "
					+ prestation.prestNumero() + " !");
		}
		valideClient(ec, prestation, utilisateur);
		ApiPrestation.validePrestationPrestataire(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	/**
	 * Devalide la prestation cote prestataire, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible de devalider
	 */
	public static void devalidePrestataire(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.prestDateValidePrest() == null) {
			// deja devalidee prestataire
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la devalider !");
		}
		decloture(ec, prestation, utilisateur);
		ApiPrestation.devalidePrestationPrestataire(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	/**
	 * Clos la prestation, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible de clore
	 */
	public static void cloture(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.prestDateCloture() != null) {
			// deja clos
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la cloturer !");
		}
		if (!prestation.isCloturable()) {
			throw new Exception("Il manque des informations, impossible de cloturer la prestation " + prestation.prestNumero() + " !");
		}
		valideClient(ec, prestation, utilisateur);
		validePrestataire(ec, prestation, utilisateur);
		ApiPrestation.cloturePrestation(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	/**
	 * Reouvre ("declos") la prestation, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible de la reouvrir
	 */
	public static void decloture(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.prestDateCloture() == null) {
			// deja decloturee
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la \"decloturer\" !");
		}
		if (prestation.prestDateFacturation() != null || (prestation.facturePapiers() != null && prestation.facturePapiers().count() > 0)) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est deja facturee, impossible de la \"decloturer\" !");
		}
		ApiPrestation.decloturePrestation(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	/**
	 * Genere en auto la (les) facture(s)
	 * 
	 * @param ec
	 * @param prestation
	 * @throws Exception
	 *             si ce n'est pas possible
	 */
	public static void genereFacture(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null) {
			return;
		}
		if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivee, impossible de la facturer !");
		}
		if (prestation.prestDateCloture() == null) {
			throw new Exception("La prestation " + prestation.prestNumero() + " n'est pas cloturee, impossible de la facturer !");
		}
		if (!prestation.isFacturable()) {
			throw new Exception("Il manque des informations, impossible de facturer la prestation " + prestation.prestNumero() + " !");
		}
		ApiPrestation.genereFacturePapier(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
	}

	public static void archivePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation.prestDateValideClient() != null) {
			devalideClient(ec, prestation, utilisateur);
		}
		ApiPrestation.archivePrestation(ec, prestation, utilisateur);
		if (prestation.prestationLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
//		try {
//			prestation.setTypeEtatRelationship(FinderTypeEtat.typeEtatAnnule());
//			ec.saveChanges();
//		}
//		catch (NSValidation.ValidationException ve) {
//			ec.revert();
//			throw ve;
//		}
	}

	public static void reactivePrestation(EOEditingContext ec, EOPrestation prestation) throws Exception {
		try {
			prestation.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
			ec.saveChanges();
		}
		catch (NSValidation.ValidationException ve) {
			ec.revert();
			throw ve;
		}
	}
	
	/**
	 * Regroupe les prestations selectionnees en une seule prestation, par client et infos budgetaires
	 */
	public static boolean regrouperPrestation(EOEditingContext ec, NSArray arrayPrestations, EOUtilisateur utilisateur) {
		try {
			ApiPrestation.regrouperPrestation(ec, arrayPrestations, utilisateur);
		}
		catch (Exception e) {
			if (e instanceof Exception) {
				e.printStackTrace();
			}
			return false;
		}
		return true;
	}
	/*
	public static boolean regrouperPrestation(EOEditingContext ec, NSArray arrayPrestations, EOUtilisateur utilisateur) {
		try {
				// regroupements par client et infos budgetaires...
				NSMutableArray prestations = new NSMutableArray(arrayPrestations);

				while (prestations.count() > 0) {
					NSMutableArray ma = new NSMutableArray();
					EOPrestation p0 = (EOPrestation) prestations.removeObjectAtIndex(0);
					ma.addObject(p0);
					for (int i = 0; i < prestations.count();) {
						EOPrestation p1 = ((EOPrestation) prestations.objectAtIndex(i));
						// meme client && memes infos budgetaires prest et client ??
						if (testRegroupementPossible(p0,p1)) {
							ma.addObject((EOPrestation) prestations.removeObjectAtIndex(i));
						}
						else {
							System.out.println("un regroupement n'est pas possible");
							i++;
						}
					}
					// regroupement
					if (ma.count() > 1) {
						if (!regroupePrestations(ec, ma, utilisateur)) {
							ec.revert();
							return false;
						}
					}
				}
		}
		catch (Exception e) {
			if (e instanceof Exception) {
				e.printStackTrace();
			}
			ec.revert();
			return false;
		}
		return true;
	}*/

	private static boolean testRegroupementPossible(EOPrestation p0, EOPrestation p1) {
		System.out.println(p0.prestNumero()+"<>"+p1.prestNumero());
		System.out.println(p0.personne().persLibelle()+"\n"+p1.personne().persLibelle()+"\n");
		if (p0.personne()!=p1.personne()) return false;
		System.out.println(p0.organ().orgLib()+"\n"+p1.organ().orgLib()+"\n");
		if (p0.organ()!=p1.organ()) return false;
		System.out.println(p0.tauxProrata()+"\n"+p1.tauxProrata()+"\n");
		if (p0.tauxProrata()!=p1.tauxProrata()) return false;
		System.out.println(p0.typeCreditRec()+"\n"+p1.typeCreditRec()+"\n");
		if (p0.typeCreditRec()!=p1.typeCreditRec()) return false;
		System.out.println(p0.lolfNomenclatureRecette()+"\n"+p1.lolfNomenclatureRecette()+"\n");
		if (p0.lolfNomenclatureRecette()!=p1.lolfNomenclatureRecette()) return false;
		System.out.println(p0.codeAnalytique()+"\n"+p1.codeAnalytique()+"\n");
		if (p0.codeAnalytique()!=p1.codeAnalytique()) return false;
		System.out.println(p0.convention()+"\n"+p1.convention()+"\n");
		if (p0.convention()!=p1.convention()) return false;
		System.out.println(p0.planComptable()+"\n"+p1.planComptable()+"\n");
		if (p0.planComptable()!=p1.planComptable()) return false;
		System.out.println(p0.fournisUlrPrest()+"\n"+p1.fournisUlrPrest()+"\n");
		if (p0.fournisUlrPrest()!=p1.fournisUlrPrest()) return false;
		System.out.println(p0.fournisUlr()+"\n"+p1.fournisUlr()+"\n");
		if (p0.fournisUlr()!=p1.fournisUlr()) return false;

		EOOrgan organ0=p0.prestationBudgetClient().organ();
		EOTypeCredit typeCredit0=p0.prestationBudgetClient().typeCreditDep();
		EOTauxProrata prorata0=p0.prestationBudgetClient().tauxProrata();
		EOLolfNomenclatureDepense lolf0=p0.prestationBudgetClient().lolfNomenclatureDepense();
		EOPlanComptable planco0=p0.prestationBudgetClient().planComptable();
		EOCodeAnalytique analy0=p0.prestationBudgetClient().codeAnalytique();
		EOConvention convention0=p0.prestationBudgetClient().convention();
				
		if (organ0==null && p0.prestationBudgetClient().engageBudget()!=null)
			organ0=p0.prestationBudgetClient().engageBudget().organ();
		if (typeCredit0==null && p0.prestationBudgetClient().engageBudget()!=null)
			typeCredit0=p0.prestationBudgetClient().engageBudget().typeCreditDep();
		if (prorata0==null && p0.prestationBudgetClient().engageBudget()!=null)
			prorata0=p0.prestationBudgetClient().engageBudget().tauxProrata();
		if (lolf0==null && p0.prestationBudgetClient().engageBudget()!=null)
			lolf0=((EOEngageCtrlAction)p0.prestationBudgetClient().engageBudget().engageCtrlActions().objectAtIndex(0)).lolfNomenclatureDepense();
		if (planco0==null && p0.prestationBudgetClient().engageBudget()!=null)
			planco0=((EOEngageCtrlPlanco)p0.prestationBudgetClient().engageBudget().engageCtrlPlancos().objectAtIndex(0)).planComptable();
		if (analy0==null && p0.prestationBudgetClient().engageBudget()!=null && p0.prestationBudgetClient().engageBudget().engageCtrlAnalytique()!=null &&
				p0.prestationBudgetClient().engageBudget().engageCtrlAnalytique().count()>0)
			analy0=((EOEngageCtrlAnalytique)p0.prestationBudgetClient().engageBudget().engageCtrlAnalytique().objectAtIndex(0)).codeAnalytique();
		if (convention0==null && p0.prestationBudgetClient().engageBudget()!=null && p0.prestationBudgetClient().engageBudget().engageCtrlConventions()!=null &&
				p0.prestationBudgetClient().engageBudget().engageCtrlConventions().count()>0)
			convention0=((EOEngageCtrlConvention)p0.prestationBudgetClient().engageBudget().engageCtrlConventions().objectAtIndex(0)).convention();
				
		EOOrgan organ1=p1.prestationBudgetClient().organ();
		EOTypeCredit typeCredit1=p1.prestationBudgetClient().typeCreditDep();
		EOTauxProrata prorata1=p1.prestationBudgetClient().tauxProrata();
		EOLolfNomenclatureDepense lolf1=p1.prestationBudgetClient().lolfNomenclatureDepense();
		EOPlanComptable planco1=p1.prestationBudgetClient().planComptable();
		EOCodeAnalytique analy1=p1.prestationBudgetClient().codeAnalytique();
		EOConvention convention1=p1.prestationBudgetClient().convention();
				
		if (organ1==null && p1.prestationBudgetClient().engageBudget()!=null)
			organ1=p1.prestationBudgetClient().engageBudget().organ();
		if (typeCredit1==null && p1.prestationBudgetClient().engageBudget()!=null)
			typeCredit1=p1.prestationBudgetClient().engageBudget().typeCreditDep();
		if (prorata1==null && p1.prestationBudgetClient().engageBudget()!=null)
			prorata1=p1.prestationBudgetClient().engageBudget().tauxProrata();
		if (lolf1==null && p1.prestationBudgetClient().engageBudget()!=null)
			lolf1=((EOEngageCtrlAction)p1.prestationBudgetClient().engageBudget().engageCtrlActions().objectAtIndex(0)).lolfNomenclatureDepense();
		if (planco1==null && p1.prestationBudgetClient().engageBudget()!=null)
			planco1=((EOEngageCtrlPlanco)p1.prestationBudgetClient().engageBudget().engageCtrlPlancos().objectAtIndex(0)).planComptable();
		if (analy1==null && p1.prestationBudgetClient().engageBudget()!=null && p1.prestationBudgetClient().engageBudget().engageCtrlAnalytique().count()>0)
			analy1=((EOEngageCtrlAnalytique)p1.prestationBudgetClient().engageBudget().engageCtrlAnalytique().objectAtIndex(0)).codeAnalytique();
		if (convention1==null && p1.prestationBudgetClient().engageBudget()!=null && p1.prestationBudgetClient().engageBudget().engageCtrlConventions().count()>0)
			convention1=((EOEngageCtrlConvention)p1.prestationBudgetClient().engageBudget().engageCtrlConventions().objectAtIndex(0)).convention();

		System.out.println(organ0.orgLib()+"\n"+organ1.orgLib()+"\n");
		if (organ0!=organ1) return false;
		System.out.println(typeCredit0+"\n"+typeCredit1+"\n");
		if (typeCredit0!=typeCredit1) return false;
		System.out.println(prorata0+"\n"+prorata1+"\n");
		if (prorata0!=prorata1) return false;
		System.out.println(lolf0+"\n"+lolf1+"\n");
		if (lolf0!=lolf1) return false;
		System.out.println(planco0+"\n"+planco1+"\n");
		if (planco0!=planco1) return false;
		System.out.println(analy0+"\n"+analy1+"\n");
		if (analy0!=analy1) return false;
		System.out.println(convention0+"\n"+convention1+"\n");
		if (convention0!=convention1) return false;
		
		p1.prestationBudgetClient().setCodeAnalytiqueRelationship(analy1);
		p1.prestationBudgetClient().setConventionRelationship(convention1);
		p1.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(lolf1);
		p1.prestationBudgetClient().setOrganRelationship(organ1);
		p1.prestationBudgetClient().setPlanComptableRelationship(planco1);
		p1.prestationBudgetClient().setTauxProrataRelationship(prorata1);
		p1.prestationBudgetClient().setTypeCreditDepRelationship(typeCredit1);

		p0.prestationBudgetClient().setCodeAnalytiqueRelationship(analy0);
		p0.prestationBudgetClient().setConventionRelationship(convention0);
		p0.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(lolf0);
		p0.prestationBudgetClient().setOrganRelationship(organ0);
		p0.prestationBudgetClient().setPlanComptableRelationship(planco0);
		p0.prestationBudgetClient().setTauxProrataRelationship(prorata0);
		p0.prestationBudgetClient().setTypeCreditDepRelationship(typeCredit0);

		return true;
	}
	
	private static boolean regroupePrestations(EOEditingContext ec, NSArray prestations, EOUtilisateur utilisateur) {
		System.out.print("Regroupement de " + prestations.count() + " prestations... ");
		try {
			// On regroupe tous les devis / prestations en un seul devis / prestation :
			//	- on cree une nouvelle prestation avec toutes les prestation_ligne
			//  - on stocke les engagements correspondants pour suppression ulterieure
			//	- on invalide les prestations et degage du meme coup les prestation_commande deja generes (on va les
			//    re-generer a partir de la seule prestation cumulee)
			//    et on flag les prestations selectionnees comme devalidees (cachees pour regroupement cumulatif)
			//  - on degage les commandes jefy
			//	- et on enregistre le tout pour deja sauver le regroupement et avant de valider

			// on invalide les objets pour pouvoir recuperer les cles
			// et on met a jour l'affichage de la liste des prestations

			// - on sauve les modifs de ce regroupement (pour retour en arriere eventuel ou autre si pb)...

			//	- on valide cote client le devis/prestation.new (avec generation des commandes)
			//	- on valide cote prestataire
			//	- on ferme (prestation realisee)
			//	- on genere les commandes
			//	- et on sauve a nouveau si la generation de commande s'est bien passee

			//	- on Cree un nouveau devis avec tous les devis_lignes des devis a regrouper
			
			if (prestations.count()==0)
				return true;

			
			
			//	- on invalide les devis et degage du meme coup les prestation_commande deja generes (on va les
			// re-generer a partir de la seule prestation cumulee)
			// et on flag les devis et prestations selectionnes comme devalides (caches pour regroupement cumulatif)
			EOPrestation prestReference=(EOPrestation)prestations.objectAtIndex(0);
			EOTypeEtat prestEtat = FinderTypeEtat.typeEtatAnnule(ec);
			EOTypeEtat etatReference=prestReference.typeEtat();
			for (int i = 0; i < prestations.count(); i++) {
				EOPrestation prestation = (EOPrestation) prestations.objectAtIndex(i);

				decloture(ec, prestation, utilisateur);
				devalidePrestataire(ec, prestation, utilisateur);
				devalideClient(ec, prestation, utilisateur);

				prestation.setTypeEtat(prestEtat);
//				ApiPrestation.changePrestationDT(ec, prestation, eoPrestation);
			}

			
			
					
			EOPrestation eoPrestation = FactoryPrestation.newObject(ec);
			eoPrestation.setExerciceRelationship(prestReference.exercice());
			eoPrestation.setCatalogueRelationship(prestReference.catalogue());
			eoPrestation.setPersonneRelationship(prestReference.personne());
			eoPrestation.setFournisUlrRelationship(prestReference.fournisUlr());
			eoPrestation.setIndividuUlrRelationship(prestReference.individuUlr());
			eoPrestation.setUtilisateurRelationship(utilisateur);
			eoPrestation.setPrestLibelle("Devis cumulatif pour "+prestReference.fournisUlr().personne_persNomPrenom());
			eoPrestation.setPrestDate(new NSTimestamp());
			eoPrestation.setPrestCommentaireClient(prestReference.prestCommentaireClient());
			eoPrestation.setPrestCommentairePrest(prestReference.prestCommentairePrest());
			eoPrestation.setPrestRemiseGlobale(prestReference.prestRemiseGlobale());
			eoPrestation.setPrestApplyTva(prestReference.prestApplyTva());
			eoPrestation.setModeRecouvrement(prestReference.modeRecouvrement());
			eoPrestation.setOrganRelationship(prestReference.organ());
			eoPrestation.setTauxProrataRelationship(prestReference.tauxProrata());
			eoPrestation.setTypeCreditRecRelationship(prestReference.typeCreditRec());
			eoPrestation.setLolfNomenclatureRecetteRelationship(prestReference.lolfNomenclatureRecette());
			eoPrestation.setPlanComptableRelationship(prestReference.planComptable());
			eoPrestation.setCodeAnalytiqueRelationship(prestReference.codeAnalytique());
			eoPrestation.setConventionRelationship(prestReference.convention());
			eoPrestation.setTypePublicRelationship(prestReference.typePublic());
			eoPrestation.setTypeEtatRelationship(etatReference);
			eoPrestation.setFournisUlrPrestRelationship(prestReference.fournisUlrPrest());

			BigDecimal ht=new BigDecimal(0.0), tva=new BigDecimal(0.0), ttc=new BigDecimal(0.0);
			for (int i = 0; i < prestations.count(); i++) {
				System.out.println("prestation n° "+i);
				EOPrestation prestation=(EOPrestation)prestations.objectAtIndex(i);
				if (prestation.prestationLignes() == null || prestation.prestationLignes().count()==0)
					continue;
				System.out.println("nblignes "+prestation.prestationLignes().count());
				for (int j=0; j<prestation.prestationLignes().count(); j++) {
					System.out.println("    ligne "+j);
					EOPrestationLigne ligne=(EOPrestationLigne)prestation.prestationLignes().objectAtIndex(j);
					FactoryPrestationLigne.newObject(ec, eoPrestation, ligne);

					ht=ht.add(ligne.prligTotalHt());
					ttc=ttc.add(ligne.prligTotalTtc());
					tva=tva.add(ligne.prligTotalTva());
				}
			}

			eoPrestation.setPrestTotalHt(ht);
			eoPrestation.setPrestTotalTva(tva);
			eoPrestation.setPrestTotalTtc(ttc);

			eoPrestation.prestationBudgetClient().setCodeAnalytiqueRelationship(prestReference.prestationBudgetClient().codeAnalytique());
			eoPrestation.prestationBudgetClient().setConventionRelationship(prestReference.prestationBudgetClient().convention());
			eoPrestation.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(prestReference.prestationBudgetClient().lolfNomenclatureDepense());
			eoPrestation.prestationBudgetClient().setOrganRelationship(prestReference.prestationBudgetClient().organ());
			eoPrestation.prestationBudgetClient().setPlanComptableRelationship(prestReference.prestationBudgetClient().planComptable());
			eoPrestation.prestationBudgetClient().setTauxProrataRelationship(prestReference.prestationBudgetClient().tauxProrata());
			eoPrestation.prestationBudgetClient().setTypeCreditDepRelationship(prestReference.prestationBudgetClient().typeCreditDep());


			ec.saveChanges();
			

			valideClient(ec, eoPrestation, utilisateur);
			validePrestataire(ec, eoPrestation, utilisateur);
			cloture(ec, eoPrestation, utilisateur);
			
			//	- et on enregistre le tout pour deja sauver le regroupement et avant de valider
//			ec.saveChanges();
			
			for (int i = 0; i < prestations.count(); i++) {
				EOPrestation prestation = (EOPrestation) prestations.objectAtIndex(i);
				ApiPrestation.changePrestationDT(ec, prestation, eoPrestation);
			}

		}
		catch (Exception e) {
			if (e instanceof Exception) {
				e.printStackTrace();
			}
			ec.revert();
			System.out.println("KO!");
			return false;
		}
		System.out.println("OK.");
		return true;
	}

}
