/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.factory;

import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.procedures.ApiPrestation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryFacturePapier extends Factory {

	public FactoryFacturePapier() {
		super();
	}

	public FactoryFacturePapier(boolean withLog) {
		super(withLog);
	}

	public static EOFacturePapier newObject(EOEditingContext ec) {
		EOFacturePapier object = (EOFacturePapier) Factory.instanceForEntity(ec, EOFacturePapier.ENTITY_NAME);
		object.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
		object.setFapDate(Factory.getDateJour());
		ec.insertObject(object);
		return object;
	}

	public static void delFacturePapier(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Il faut donner la facture papier ET un utilisateur pour supprimer une facture papier!!");
		}
		if (facturePapier.prestation() != null) {
			if (facturePapier.prestation().prestationLignes() != null) {
				ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(facturePapier.prestation().prestationLignes()));
			}
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier.prestation())));
		}
		ApiPrestation.delFacturePapier(ec, facturePapier, utilisateur);
		if (facturePapier.facturePapierLignes() != null) {
			ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(facturePapier.facturePapierLignes()));
		}
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
	}

	/**
	 * Met a jour la reference de la facture papier, peu importe qu'elle en ait deja une ou pas...
	 * 
	 * @param ec
	 * @param facturePapier
	 * @throws Exception
	 */
	public static void updFapRef(EOEditingContext ec, EOFacturePapier facturePapier) throws Exception {
		if (facturePapier == null) {
			return;
		}
		ApiPrestation.updFapRef(ec, facturePapier);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
	}

	/**
	 * Valide la facture papier cote client, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param facturePapier
	 * @throws Exception
	 *             si ce n'est pas possible de valider
	 */
	public static void valideClient(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null) {
			return;
		}
		if (facturePapier.fapDateValidationClient() != null) {
			// deja validee client
			return;
		}
		if (facturePapier.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La facture papier " + facturePapier.fapNumero() + " est annulee, impossible de la valider !");
		}
		if (!facturePapier.isValidableClient()) {
			throw new Exception("Il manque des informations cote client, impossible de valider la facture papier " + facturePapier.fapNumero()
					+ " !");
		}

		ApiPrestation.valideFacturePapierClient(ec, facturePapier, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
	}

	/**
	 * Devalide la facture papier cote client, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param facturePapier
	 * @throws Exception
	 *             si ce n'est pas possible de devalider
	 */
	public static void devalideClient(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null) {
			return;
		}
		if (facturePapier.fapDateValidationClient() == null) {
			// deja devalidee client
			return;
		}
		if (facturePapier.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La facture papier " + facturePapier.fapNumero() + " est annulee, impossible de la devalider !");
		}
		devalidePrest(ec, facturePapier, utilisateur);
		ApiPrestation.devalideFacturePapierClient(ec, facturePapier, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
	}

	/**
	 * Valide la facture papier cote prestataire, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param facturePapier
	 * @throws Exception
	 *             si ce n'est pas possible de valider
	 */
	public static void validePrest(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null) {
			return;
		}
		if (facturePapier.fapDateValidationPrest() != null) {
			// deja validee prestataire
			return;
		}
		if (facturePapier.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La facture papier " + facturePapier.fapNumero() + " est annulee, impossible de la valider !");
		}
		if (!facturePapier.isValidablePrest()) {
			throw new Exception("Il manque des informations cote prestataire, impossible de valider la facture papier "
					+ facturePapier.fapNumero() + " !");
		}
		// verif si mode recouvrement echeancier
		if (facturePapier.modeRecouvrement() != null && facturePapier.modeRecouvrement().isEcheancier()) {
			if (facturePapier.echeId() == null) {
				throw new Exception("La facture papier " + facturePapier.fapNumero()
						+ " est en mode de recouvrement Echeancier, et elle n'a pas d'echeancier rattache, a corriger pour pouvoir valider !");
			}
		}
		valideClient(ec, facturePapier, utilisateur);
		ApiPrestation.valideFacturePapierPrest(ec, facturePapier, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
	}

	/**
	 * Devalide la facture papier cote prestataire, si ce n'est pas deja fait
	 * 
	 * @param ec
	 * @param facturePapier
	 * @throws Exception
	 *             si ce n'est pas possible de devalider
	 */
	public static void devalidePrest(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null) {
			return;
		}
		if (facturePapier.fapDateValidationPrest() == null) {
			// deja devalidee prestataire
			return;
		}
		if (facturePapier.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
			throw new Exception("La facture papier " + facturePapier.fapNumero() + " est annulee, impossible de la devalider !");
		}
		ApiPrestation.devalideFacturePapierPrest(ec, facturePapier, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
	}

}
