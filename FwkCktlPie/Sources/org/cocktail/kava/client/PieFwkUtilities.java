package org.cocktail.kava.client;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class PieFwkUtilities {

    private static final String DATE_SEPARATEUR = "-";
	private static final String FORMAT_NULL = "?";
	public static final String FORMAT = "dd/MM/yyyy";	
	public static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");	
	
    @Deprecated
	public static String datToString(NSTimestamp date,String format){
		String aString = null;
		try {
			NSTimestampFormatter formatter = new NSTimestampFormatter(format);
			aString = formatter.format(date);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return aString;
	}

	public static String formaterPeriodeValidite(Date dtDebut, Date dtFin) {
		return formaterPeriodeValidite(dtDebut, dtFin, FORMAT, DATE_SEPARATEUR, FORMAT_NULL);
	}

	/**
     * Formate une période avec deux date, un séparateur et un chaine si une des date est null
     * @param dtDebut Date de début de la période
     * @param dtFin Date de fin de la période
     * @param format Format des dates
     * @param separateur Chaîne de séparation des deux dates
     * @param formatSiNull Chaîne de substitution si la date est null
     * @return
     */
	public static String formaterPeriodeValidite(Date dtDebut, Date dtFin, String format, String separateur, String formatSiNull) {
		return formateDateAvecNull(dtDebut, format, formatSiNull) + 
				separateur
				+ formateDateAvecNull(dtFin, format, formatSiNull);
	}
	
	/**
	 * Foramte une date avec un format Java. Si la date est null une chaîne de caratère spécifique est retourné
	 * 
	 * @param date date a formater
	 * @param format format Java
	 * @param formatSiNull chaîne si la date est null
	 * @return
	 */
	public static String formateDateAvecNull(Date date, String format, String formatSiNull) {
		if(date == null) return formatSiNull;
		return (new SimpleDateFormat(format)).format(date);		
	}

}
