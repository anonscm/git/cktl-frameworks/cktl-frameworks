/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EOCodeAnalytique;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EOFactureCtrlAction;
import org.cocktail.kava.client.metier.EOFactureCtrlAnalytique;
import org.cocktail.kava.client.metier.EOFactureCtrlConvention;
import org.cocktail.kava.client.metier.EOFactureCtrlPlanco;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOOrgan;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOTauxProrata;
import org.cocktail.kava.client.metier.EOTypeApplication;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class InsFacture {

	/**
	 * Appele la procedure de creation de la facture<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param facture
	 *            EOFacture qui sera enregistre
	 */
	public static void save(EOEditingContext ec, EOFacture facture) throws Exception {
		if (facture == null) {
			throw new Exception("Facture a enregistrer null!!");
		}
		facture.validateObjectMetier();
		ServerProxy.apiInsFacture(ec, dico(ec, facture));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'objet<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param facture
	 *            EOFacture pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	protected static NSDictionary dico(EOEditingContext ec, EOFacture facture) throws Exception {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010aFacId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020aExeOrdre");

		// on met le numero a null, il sera generee dans la procedure
		dico.takeValueForKey(facture.facNumero(), "030aFacNumero");

		if (facture.fournisUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.fournisUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.PRIMARY_KEY_KEY), "040aFouOrdre");
		}
		else {
			dico.takeValueForKey(null, "040aFouOrdre");
		}
		
		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.personne());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPersonne.PRIMARY_KEY_KEY), "050aPersId");

		dico.takeValueForKey(facture.facLib(), "060aFacLib");

		if (facture.modeRecouvrement() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.modeRecouvrement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOModeRecouvrement.PRIMARY_KEY_KEY), "070aMorOrdre");
		}
		else {
			dico.takeValueForKey(null, "070aMorOrdre");
		}

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.organ());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOOrgan.PRIMARY_KEY_KEY), "080aOrgId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.typeCreditRec());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeCredit.PRIMARY_KEY_KEY), "090aTcdOrdre");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.PRIMARY_KEY_KEY), "100aTapId");

		dico.takeValueForKey(facture.facHtSaisie(), "110aFacHtSaisie");
		dico.takeValueForKey(facture.facTtcSaisie(), "120aFacTtcSaisie");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.typeApplication());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeApplication.PRIMARY_KEY_KEY), "130aTyapId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, facture.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.PRIMARY_KEY_KEY), "140aUtlOrdre");

		dico.takeValueForKey(chaineCtrlAction(ec, facture.factureCtrlActions()), "150aChaineAction");
		dico.takeValueForKey(chaineCtrlAnalytique(ec, facture.factureCtrlAnalytiques()), "160aChaineAnalytique");
		dico.takeValueForKey(chaineCtrlConvention(ec, facture.factureCtrlConventions()), "170aChaineConvention");
		dico.takeValueForKey(chaineCtrlPlanco(ec, facture.factureCtrlPlancos()), "180aChainePlanco");

		return dico;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOFactureCtrlAction de la facture<BR>
	 * Format de la chaine : lolf_id$fact_ht_saisie$fact_ttc_saisie$...$ <BR>
	 * 
	 * @param a
	 *            EOFactureCtrlAction a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlAction(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EOFactureCtrlAction factureCtrlAction = (EOFactureCtrlAction) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, factureCtrlAction.lolfNomenclatureRecette());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOLolfNomenclatureRecette.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + factureCtrlAction.factHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + factureCtrlAction.factTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOFactureCtrlAnalytique de la facture<BR>
	 * Format de la chaine : can_id$fana_ht_saisie$fana_ttc_saisie$...$ <BR>
	 * 
	 * @param a
	 *            EOFactureCtrlAnalytique a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlAnalytique(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EOFactureCtrlAnalytique factureCtrlAnalytique = (EOFactureCtrlAnalytique) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, factureCtrlAnalytique.codeAnalytique());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOCodeAnalytique.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + factureCtrlAnalytique.fanaHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + factureCtrlAnalytique.fanaTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOFactureCtrlConvention de la facture<BR>
	 * Format de la chaine : con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$ <BR>
	 * 
	 * @param a
	 *            EOFactureCtrlConvention a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlConvention(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EOFactureCtrlConvention factureCtrlConvention = (EOFactureCtrlConvention) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, factureCtrlConvention.convention());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOConvention.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + factureCtrlConvention.fconHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + factureCtrlConvention.fconTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOFactureCtrlPlanco de la facture<BR>
	 * Format de la chaine : pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$ <BR>
	 * 
	 * @param a
	 *            EOFactureCtrlPlanco a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlPlanco(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EOFactureCtrlPlanco factureCtrlPlanco = (EOFactureCtrlPlanco) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, factureCtrlPlanco.planComptable());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPlanComptable.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + factureCtrlPlanco.fpcoHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + factureCtrlPlanco.fpcoTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

}