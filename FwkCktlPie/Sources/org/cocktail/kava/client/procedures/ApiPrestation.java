/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EOCommandesPourPi;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class ApiPrestation {

	public static void regrouperPrestation(EOEditingContext ec, NSArray prestations, EOUtilisateur utilisateur) throws Exception {
		if (prestations == null || prestations.count()==0 || utilisateur == null) {
			throw new Exception("Il faut donner les prestations ET un utilisateur pour regrouper les prestations!!");
		}
		ServerProxy.regrouperPrestation(ec, prestations, utilisateur);
	}

	public static void validePrestationClient(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour valider une prestation!!");
		}
		ServerProxy.apiPrestationValidePrestationClient(ec, prestation, utilisateur);
	}

	public static void devalidePrestationClient(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour devalider une prestation!!");
		}
		ServerProxy.apiPrestationDevalidePrestationClient(ec, prestation, utilisateur);
	}

	public static void validePrestationPrestataire(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour valider une prestation!!");
		}
		ServerProxy.apiPrestationValidePrestationPrestataire(ec, prestation, utilisateur);
	}

	public static void devalidePrestationPrestataire(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour devalider une prestation!!");
		}
		ServerProxy.apiPrestationDevalidePrestationPrestataire(ec, prestation, utilisateur);
	}

	public static void cloturePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour valider une prestation!!");
		}
		ServerProxy.apiPrestationCloturePrestation(ec, prestation, utilisateur);
	}

	public static void decloturePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour devalider une prestation!!");
		}
		ServerProxy.apiPrestationDecloturePrestation(ec, prestation, utilisateur);
	}

	public static void changePrestationDT(EOEditingContext ec, EOPrestation oldPrestation, EOPrestation newPrestation) throws Exception {
		if (oldPrestation == null || newPrestation == null) {
			throw new Exception("Il faut donner l'ancienne et la nouvelle prestation !!");
		}
		ServerProxy.apiPrestationChangePrestationDT(ec, oldPrestation, newPrestation);
	}

	
	public static void archivePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour archiver une prestation!!");
		}
		ServerProxy.apiPrestationArchivePrestation(ec, prestation, utilisateur);
	}

	public static void genereFacturePapier(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour generer les factures papier!!");
		}
		ServerProxy.apiPrestationGenereFacturePapier(ec, prestation, utilisateur);
	}

	public static void delFacturePapier(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Il faut donner la facture papier ET un utilisateur pour supprimer une facture papier!!");
		}
		ServerProxy.apiPrestationDelFacturePapier(ec, facturePapier, utilisateur);
	}

	public static NSDictionary prestationFromCommande(EOEditingContext ec, EOCommandesPourPi commande, EOUtilisateur utilisateur,
			EOFournisUlr fournisUlrClient, String pcoNum) throws Exception {
		if (commande == null || utilisateur == null || fournisUlrClient == null) {
			throw new Exception("Il faut donner tous les parametres pour creer une prestation interne depuis une commande!!");
		}
		NSDictionary dico = ServerProxy.apiPrestationPrestationFromCommande(ec, commande, utilisateur, fournisUlrClient, pcoNum);
		return dico;
	}

	public static NSDictionary facturePapierFromCommande(EOEditingContext ec, EOCommandesPourPi commande, EOUtilisateur utilisateur,
			EOFournisUlr fournisUlrClient, String pcoNum) throws Exception {
		if (commande == null || utilisateur == null || fournisUlrClient == null) {
			throw new Exception("Il faut donner tous les parametres pour creer une facture papier interne depuis une commande!!");
		}
		NSDictionary dico = ServerProxy.apiPrestationFacturePapierFromCommande(ec, commande, utilisateur, fournisUlrClient, pcoNum);
		return dico;
	}

	public static void soldePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur pour solder une prestation!!");
		}
		ServerProxy.apiPrestationSoldePrestation(ec, prestation, utilisateur);
	}

	public static void dupliquePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur, EOExercice exerciceCible) throws Exception {
		if (prestation == null || utilisateur == null || exerciceCible==null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur ET un exercice pour dupliquer une prestation!!");
		}
		ServerProxy.apiPrestationDupliquePrestation(ec, prestation, utilisateur, exerciceCible);
	}

	public static void basculePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur, EOExercice exerciceCible) throws Exception {
		if (prestation == null || utilisateur == null || exerciceCible==null) {
			throw new Exception("Il faut donner la prestation ET un utilisateur ET un exercice pour basculer une prestation!!");
		}
		ServerProxy.apiPrestationBasculePrestation(ec, prestation, utilisateur, exerciceCible);
	}

	/**
	 * Met a jour la reference de la facture papier (champ fap_ref)
	 * 
	 * @param ec
	 * @param facturePapier
	 * @throws Exception
	 */
	public static void updFapRef(EOEditingContext ec, EOFacturePapier facturePapier) throws Exception {
		if (facturePapier == null) {
			throw new Exception("Il faut donner la facture papier pour la referencer (fap_ref)!!");
		}
		ServerProxy.apiPrestationUpdFapRef(ec, facturePapier);
	}

	public static void valideFacturePapierClient(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Il faut donner la facture papier ET un utilisateur pour valider une facture papier!!");
		}
		ServerProxy.apiPrestationValideFacturePapierClient(ec, facturePapier, utilisateur);
	}

	public static void devalideFacturePapierClient(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Il faut donner la facture papier ET un utilisateur pour devalider une facture papier!!");
		}
		ServerProxy.apiPrestationDevalideFacturePapierClient(ec, facturePapier, utilisateur);
	}

	public static void valideFacturePapierPrest(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Il faut donner la facture papier ET un utilisateur pour valider une facture papier!!");
		}
		ServerProxy.apiPrestationValideFacturePapierPrest(ec, facturePapier, utilisateur);
	}

	public static void devalideFacturePapierPrest(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Il faut donner la facture papier ET un utilisateur pour devalider une facture papier!!");
		}
		ServerProxy.apiPrestationDevalideFacturePapierPrest(ec, facturePapier, utilisateur);
	}

	public static NSDictionary duplicateFacturePapier(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur,
			EOFournisUlr fournisUlrClient) throws Exception {
		if (facturePapier == null || utilisateur == null || fournisUlrClient == null) {
			throw new Exception("Il faut donner tous les parametres pour dupliquer une facture papier!!");
		}
		NSDictionary dico = ServerProxy.apiPrestationDuplicateFacturePapier(ec, facturePapier, utilisateur, fournisUlrClient);
		return dico;
	}

}