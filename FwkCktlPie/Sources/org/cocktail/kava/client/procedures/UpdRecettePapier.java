/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EORibfourUlr;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class UpdRecettePapier {

	/**
	 * Appele la procedure de modification de la recette papier<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecettePapier qui sera enregistree
	 */
	public static void save(EOEditingContext ec, EORecettePapier recettePapier, Number rppId) throws Exception {
		if (recettePapier == null) {
			throw new Exception("Recette a enregistrer null!!");
		}
		if (rppId == null) {
			throw new Exception("cle de recette papier (rppId) a modifier null!!");
		}
		recettePapier.validateObjectMetier();
		ServerProxy.apiUpdRecettePapier(ec, dico(ec, recettePapier, rppId));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'objet<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecette pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	protected static NSDictionary dico(EOEditingContext ec, EORecettePapier recettePapier, Number rppId) throws Exception {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(rppId, "010aRppId");

		dico.takeValueForKey(recettePapier.rppNumero(), "020aRppNumero");

		dico.takeValueForKey(recettePapier.rppHtSaisie(), "030aRppHtSaisie");
		dico.takeValueForKey(recettePapier.rppTtcSaisie(), "040aRppTtcSaisie");

		if (recettePapier.personne() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.personne());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPersonne.PRIMARY_KEY_KEY), "050aPersId");
		}
		else {
			dico.takeValueForKey(null, "050aPersId");
		}

		if (recettePapier.fournisUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.fournisUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.PRIMARY_KEY_KEY), "060aFouOrdre");
		}
		else {
			dico.takeValueForKey(null, "060aFouOrdre");
		}

		if (recettePapier.ribfourUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.ribfourUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORibfourUlr.PRIMARY_KEY_KEY), "070aRibOrdre");
		}
		else {
			dico.takeValueForKey(null, "070aRibOrdre");
		}

		if (recettePapier.modeRecouvrement() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.modeRecouvrement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOModeRecouvrement.PRIMARY_KEY_KEY), "080aMorOrdre");
		}
		else {
			dico.takeValueForKey(null, "080aMorOrdre");
		}

		dico.takeValueForKey(recettePapier.rppDateRecette(), "090aRppDateRecette");
		dico.takeValueForKey(recettePapier.rppDateReception(), "100aRppDateReception");
		dico.takeValueForKey(recettePapier.rppDateServiceFait(), "110aRppDateServiceFait");
		dico.takeValueForKey(recettePapier.rppNbPiece(), "120aRppNbPiece");

		if (recettePapier.utilisateur() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.utilisateur());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.PRIMARY_KEY_KEY), "130aUtlOrdre");
		}
		else {
			dico.takeValueForKey(null, "130aUtlOrdre");
		}
		return dico;
	}

}