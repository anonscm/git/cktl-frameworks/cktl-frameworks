
// _EORecetteCtrlPlanco.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecetteCtrlPlanco.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecetteCtrlPlanco extends EOGenericRecord {

    public static final String ENTITY_NAME = "RecetteCtrlPlanco";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_planco";

    public static final String RPCO_DATE_SAISIE_KEY = "rpcoDateSaisie";
    public static final String RPCO_HT_SAISIE_KEY = "rpcoHtSaisie";
    public static final String RPCO_TTC_SAISIE_KEY = "rpcoTtcSaisie";
    public static final String RPCO_TVA_SAISIE_KEY = "rpcoTvaSaisie";
    public static final String TBO_ORDRE_KEY = "tboOrdre";
    public static final String TIT_ID_KEY = "titId";

    public static final String RPCO_DATE_SAISIE_COLKEY = "RPCO_DATE_SAISIE";
    public static final String RPCO_HT_SAISIE_COLKEY = "RPCO_HT_SAISIE";
    public static final String RPCO_TTC_SAISIE_COLKEY = "RPCO_TTC_SAISIE";
    public static final String RPCO_TVA_SAISIE_COLKEY = "RPCO_TVA_SAISIE";
    public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
    public static final String TIT_ID_COLKEY = "TIT_ID";

    public static final String EXERCICE_KEY = "exercice";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String RECETTE_KEY = "recette";
    public static final String TITRE_KEY = "titre";

    public static final String RECETTE_CTRL_PLANCO_CTPS_KEY = "recetteCtrlPlancoCtps";
    public static final String RECETTE_CTRL_PLANCO_TVAS_KEY = "recetteCtrlPlancoTvas";



	
    public _EORecetteCtrlPlanco() {
        super();
    }




    public NSTimestamp rpcoDateSaisie() {
        return (NSTimestamp)storedValueForKey(RPCO_DATE_SAISIE_KEY);
    }
    public void setRpcoDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RPCO_DATE_SAISIE_KEY);
    }

    public BigDecimal rpcoHtSaisie() {
        return (BigDecimal)storedValueForKey(RPCO_HT_SAISIE_KEY);
    }
    public void setRpcoHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPCO_HT_SAISIE_KEY);
    }

    public BigDecimal rpcoTtcSaisie() {
        return (BigDecimal)storedValueForKey(RPCO_TTC_SAISIE_KEY);
    }
    public void setRpcoTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPCO_TTC_SAISIE_KEY);
    }

    public BigDecimal rpcoTvaSaisie() {
        return (BigDecimal)storedValueForKey(RPCO_TVA_SAISIE_KEY);
    }
    public void setRpcoTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPCO_TVA_SAISIE_KEY);
    }

    public Number tboOrdre() {
        return (Number)storedValueForKey(TBO_ORDRE_KEY);
    }
    public void setTboOrdre(Number aValue) {
        takeStoredValueForKey(aValue, TBO_ORDRE_KEY);
    }

    public Number titId() {
        return (Number)storedValueForKey(TIT_ID_KEY);
    }
    public void setTitId(Number aValue) {
        takeStoredValueForKey(aValue, TIT_ID_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORecette recette() {
        return (org.cocktail.kava.client.metier.EORecette)storedValueForKey(RECETTE_KEY);
    }
    public void setRecette(org.cocktail.kava.client.metier.EORecette aValue) {
        takeStoredValueForKey(aValue, RECETTE_KEY);
    }
	
    public void setRecetteRelationship(org.cocktail.kava.client.metier.EORecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORecette object = recette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTitre titre() {
        return (org.cocktail.kava.client.metier.EOTitre)storedValueForKey(TITRE_KEY);
    }
    public void setTitre(org.cocktail.kava.client.metier.EOTitre aValue) {
        takeStoredValueForKey(aValue, TITRE_KEY);
    }
	
    public void setTitreRelationship(org.cocktail.kava.client.metier.EOTitre value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTitre object = titre();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
        }
    }




    public NSArray recetteCtrlPlancoCtps() {
        return (NSArray)storedValueForKey(RECETTE_CTRL_PLANCO_CTPS_KEY);
    }
    public void setRecetteCtrlPlancoCtps(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTE_CTRL_PLANCO_CTPS_KEY);
    }
    public void addToRecetteCtrlPlancoCtps(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlPlancoCtps();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecetteCtrlPlancoCtps(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlPlancoCtps();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecetteCtrlPlancoCtpsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_CTPS_KEY);
    }
    public void removeFromRecetteCtrlPlancoCtpsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_CTPS_KEY);
    }
	

    public NSArray recetteCtrlPlancoTvas() {
        return (NSArray)storedValueForKey(RECETTE_CTRL_PLANCO_TVAS_KEY);
    }
    public void setRecetteCtrlPlancoTvas(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTE_CTRL_PLANCO_TVAS_KEY);
    }
    public void addToRecetteCtrlPlancoTvas(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlPlancoTvas();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecetteCtrlPlancoTvas(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlPlancoTvas();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecetteCtrlPlancoTvasRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_TVAS_KEY);
    }
    public void removeFromRecetteCtrlPlancoTvasRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_TVAS_KEY);
    }
	


}

