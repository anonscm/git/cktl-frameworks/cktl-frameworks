
// _EOFacturePapier.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFacturePapier.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFacturePapier extends EOGenericRecord {

    public static final String ENTITY_NAME = "FacturePapier";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_papier";

    public static final String ECHE_ID_KEY = "echeId";
    public static final String FAP_APPLY_TVA_KEY = "fapApplyTva";
    public static final String FAP_COMMENTAIRE_CLIENT_KEY = "fapCommentaireClient";
    public static final String FAP_COMMENTAIRE_PREST_KEY = "fapCommentairePrest";
    public static final String FAP_DATE_KEY = "fapDate";
    public static final String FAP_DATE_LIMITE_PAIEMENT_KEY = "fapDateLimitePaiement";
    public static final String FAP_DATE_REGLEMENT_KEY = "fapDateReglement";
    public static final String FAP_DATE_VALIDATION_CLIENT_KEY = "fapDateValidationClient";
    public static final String FAP_DATE_VALIDATION_PREST_KEY = "fapDateValidationPrest";
    public static final String FAP_LIB_KEY = "fapLib";
    public static final String FAP_NUMERO_KEY = "fapNumero";
    public static final String FAP_REF_KEY = "fapRef";
    public static final String FAP_REFERENCE_REGLEMENT_KEY = "fapReferenceReglement";
    public static final String FAP_REMISE_GLOBALE_KEY = "fapRemiseGlobale";
    public static final String FAP_TOTAL_HT_KEY = "fapTotalHt";
    public static final String FAP_TOTAL_TTC_KEY = "fapTotalTtc";
    public static final String FAP_TOTAL_TVA_KEY = "fapTotalTva";
    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";

    public static final String ECHE_ID_COLKEY = "ECHE_ID";
    public static final String FAP_APPLY_TVA_COLKEY = "FAP_APPLY_TVA";
    public static final String FAP_COMMENTAIRE_CLIENT_COLKEY = "FAP_COMMENTAIRE_CLIENT";
    public static final String FAP_COMMENTAIRE_PREST_COLKEY = "FAP_COMMENTAIRE_PREST";
    public static final String FAP_DATE_COLKEY = "FAP_DATE";
    public static final String FAP_DATE_LIMITE_PAIEMENT_COLKEY = "FAP_DATE_LIMITE_PAIEMENT";
    public static final String FAP_DATE_REGLEMENT_COLKEY = "FAP_DATE_REGLEMENT";
    public static final String FAP_DATE_VALIDATION_CLIENT_COLKEY = "FAP_DATE_VALIDATION_CLIENT";
    public static final String FAP_DATE_VALIDATION_PREST_COLKEY = "FAP_DATE_VALIDATION_PREST";
    public static final String FAP_LIB_COLKEY = "FAP_LIB";
    public static final String FAP_NUMERO_COLKEY = "FAP_NUMERO";
    public static final String FAP_REF_COLKEY = "FAP_REF";
    public static final String FAP_REFERENCE_REGLEMENT_COLKEY = "FAP_REFERENCE_REGLEMENT";
    public static final String FAP_REMISE_GLOBALE_COLKEY = "FAP_REMISE_GLOBALE";
    public static final String FAP_TOTAL_HT_COLKEY = "FAP_TOTAL_HT";
    public static final String FAP_TOTAL_TTC_COLKEY = "FAP_TOTAL_TTC";
    public static final String FAP_TOTAL_TVA_COLKEY = "FAP_TOTAL_TVA";

    public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
    public static final String CONVENTION_KEY = "convention";
    public static final String ENGAGE_BUDGET_KEY = "engageBudget";
    public static final String EXERCICE_KEY = "exercice";
    public static final String FACTURE_KEY = "facture";
    public static final String FOURNIS_ULR_KEY = "fournisUlr";
    public static final String FOURNIS_ULR_PREST_KEY = "fournisUlrPrest";
    public static final String INDIVIDU_ULR_KEY = "individuUlr";
    public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";
    public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
    public static final String ORGAN_KEY = "organ";
    public static final String PERSONNE_KEY = "personne";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String PLAN_COMPTABLE_CTP_KEY = "planComptableCtp";
    public static final String PLAN_COMPTABLE_TVA_KEY = "planComptableTva";
    public static final String PRESTATION_KEY = "prestation";
    public static final String RIBFOUR_ULR_KEY = "ribfourUlr";
    public static final String TAUX_PRORATA_KEY = "tauxProrata";
    public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";
    public static final String TYPE_ETAT_KEY = "typeEtat";
    public static final String TYPE_PUBLIC_KEY = "typePublic";
    public static final String UTILISATEUR_KEY = "utilisateur";
    public static final String UTILISATEUR_VALIDATION_CLIENT_KEY = "utilisateurValidationClient";
    public static final String UTILISATEUR_VALIDATION_PREST_KEY = "utilisateurValidationPrest";

    public static final String FACTURE_PAPIER_LIGNES_KEY = "facturePapierLignes";



	
    public _EOFacturePapier() {
        super();
    }




    public Number echeId() {
        return (Number)storedValueForKey(ECHE_ID_KEY);
    }
    public void setEcheId(Number aValue) {
        takeStoredValueForKey(aValue, ECHE_ID_KEY);
    }

    public String fapApplyTva() {
        return (String)storedValueForKey(FAP_APPLY_TVA_KEY);
    }
    public void setFapApplyTva(String aValue) {
        takeStoredValueForKey(aValue, FAP_APPLY_TVA_KEY);
    }

    public String fapCommentaireClient() {
        return (String)storedValueForKey(FAP_COMMENTAIRE_CLIENT_KEY);
    }
    public void setFapCommentaireClient(String aValue) {
        takeStoredValueForKey(aValue, FAP_COMMENTAIRE_CLIENT_KEY);
    }

    public String fapCommentairePrest() {
        return (String)storedValueForKey(FAP_COMMENTAIRE_PREST_KEY);
    }
    public void setFapCommentairePrest(String aValue) {
        takeStoredValueForKey(aValue, FAP_COMMENTAIRE_PREST_KEY);
    }

    public NSTimestamp fapDate() {
        return (NSTimestamp)storedValueForKey(FAP_DATE_KEY);
    }
    public void setFapDate(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FAP_DATE_KEY);
    }

    public NSTimestamp fapDateLimitePaiement() {
        return (NSTimestamp)storedValueForKey(FAP_DATE_LIMITE_PAIEMENT_KEY);
    }
    public void setFapDateLimitePaiement(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FAP_DATE_LIMITE_PAIEMENT_KEY);
    }

    public NSTimestamp fapDateReglement() {
        return (NSTimestamp)storedValueForKey(FAP_DATE_REGLEMENT_KEY);
    }
    public void setFapDateReglement(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FAP_DATE_REGLEMENT_KEY);
    }

    public NSTimestamp fapDateValidationClient() {
        return (NSTimestamp)storedValueForKey(FAP_DATE_VALIDATION_CLIENT_KEY);
    }
    public void setFapDateValidationClient(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FAP_DATE_VALIDATION_CLIENT_KEY);
    }

    public NSTimestamp fapDateValidationPrest() {
        return (NSTimestamp)storedValueForKey(FAP_DATE_VALIDATION_PREST_KEY);
    }
    public void setFapDateValidationPrest(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FAP_DATE_VALIDATION_PREST_KEY);
    }

    public String fapLib() {
        return (String)storedValueForKey(FAP_LIB_KEY);
    }
    public void setFapLib(String aValue) {
        takeStoredValueForKey(aValue, FAP_LIB_KEY);
    }

    public Number fapNumero() {
        return (Number)storedValueForKey(FAP_NUMERO_KEY);
    }
    public void setFapNumero(Number aValue) {
        takeStoredValueForKey(aValue, FAP_NUMERO_KEY);
    }

    public String fapRef() {
        return (String)storedValueForKey(FAP_REF_KEY);
    }
    public void setFapRef(String aValue) {
        takeStoredValueForKey(aValue, FAP_REF_KEY);
    }

    public String fapReferenceReglement() {
        return (String)storedValueForKey(FAP_REFERENCE_REGLEMENT_KEY);
    }
    public void setFapReferenceReglement(String aValue) {
        takeStoredValueForKey(aValue, FAP_REFERENCE_REGLEMENT_KEY);
    }

    public BigDecimal fapRemiseGlobale() {
        return (BigDecimal)storedValueForKey(FAP_REMISE_GLOBALE_KEY);
    }
    public void setFapRemiseGlobale(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAP_REMISE_GLOBALE_KEY);
    }

    public BigDecimal fapTotalHt() {
        return (BigDecimal)storedValueForKey(FAP_TOTAL_HT_KEY);
    }
    public void setFapTotalHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAP_TOTAL_HT_KEY);
    }

    public BigDecimal fapTotalTtc() {
        return (BigDecimal)storedValueForKey(FAP_TOTAL_TTC_KEY);
    }
    public void setFapTotalTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAP_TOTAL_TTC_KEY);
    }

    public BigDecimal fapTotalTva() {
        return (BigDecimal)storedValueForKey(FAP_TOTAL_TVA_KEY);
    }
    public void setFapTotalTva(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAP_TOTAL_TVA_KEY);
    }

    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }




    public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
        return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
    }
    public void setCodeAnalytique(org.cocktail.kava.client.metier.EOCodeAnalytique aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_KEY);
    }
	
    public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCodeAnalytique object = codeAnalytique();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOConvention convention() {
        return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
    }
    public void setConvention(org.cocktail.kava.client.metier.EOConvention aValue) {
        takeStoredValueForKey(aValue, CONVENTION_KEY);
    }
	
    public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOConvention object = convention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOEngageBudget engageBudget() {
        return (org.cocktail.kava.client.metier.EOEngageBudget)storedValueForKey(ENGAGE_BUDGET_KEY);
    }
    public void setEngageBudget(org.cocktail.kava.client.metier.EOEngageBudget aValue) {
        takeStoredValueForKey(aValue, ENGAGE_BUDGET_KEY);
    }
	
    public void setEngageBudgetRelationship(org.cocktail.kava.client.metier.EOEngageBudget value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOEngageBudget object = engageBudget();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_BUDGET_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_BUDGET_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFacture facture() {
        return (org.cocktail.kava.client.metier.EOFacture)storedValueForKey(FACTURE_KEY);
    }
    public void setFacture(org.cocktail.kava.client.metier.EOFacture aValue) {
        takeStoredValueForKey(aValue, FACTURE_KEY);
    }
	
    public void setFactureRelationship(org.cocktail.kava.client.metier.EOFacture value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFacture object = facture();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlrPrest() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_PREST_KEY);
    }
    public void setFournisUlrPrest(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_PREST_KEY);
    }
	
    public void setFournisUlrPrestRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlrPrest();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_PREST_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_PREST_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOIndividuUlr individuUlr() {
        return (org.cocktail.kava.client.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
    }
    public void setIndividuUlr(org.cocktail.kava.client.metier.EOIndividuUlr aValue) {
        takeStoredValueForKey(aValue, INDIVIDU_ULR_KEY);
    }
	
    public void setIndividuUlrRelationship(org.cocktail.kava.client.metier.EOIndividuUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOIndividuUlr object = individuUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, INDIVIDU_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
    }
    public void setLolfNomenclatureRecette(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
	
    public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureRecette object = lolfNomenclatureRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOModeRecouvrement modeRecouvrement() {
        return (org.cocktail.kava.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
    }
    public void setModeRecouvrement(org.cocktail.kava.client.metier.EOModeRecouvrement aValue) {
        takeStoredValueForKey(aValue, MODE_RECOUVREMENT_KEY);
    }
	
    public void setModeRecouvrementRelationship(org.cocktail.kava.client.metier.EOModeRecouvrement value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOModeRecouvrement object = modeRecouvrement();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, MODE_RECOUVREMENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableCtp() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_CTP_KEY);
    }
    public void setPlanComptableCtp(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_CTP_KEY);
    }
	
    public void setPlanComptableCtpRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableCtp();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_CTP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_CTP_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableTva() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_TVA_KEY);
    }
    public void setPlanComptableTva(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_TVA_KEY);
    }
	
    public void setPlanComptableTvaRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableTva();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_TVA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_TVA_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPrestation prestation() {
        return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(PRESTATION_KEY);
    }
    public void setPrestation(org.cocktail.kava.client.metier.EOPrestation aValue) {
        takeStoredValueForKey(aValue, PRESTATION_KEY);
    }
	
    public void setPrestationRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestation object = prestation();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORibfourUlr ribfourUlr() {
        return (org.cocktail.kava.client.metier.EORibfourUlr)storedValueForKey(RIBFOUR_ULR_KEY);
    }
    public void setRibfourUlr(org.cocktail.kava.client.metier.EORibfourUlr aValue) {
        takeStoredValueForKey(aValue, RIBFOUR_ULR_KEY);
    }
	
    public void setRibfourUlrRelationship(org.cocktail.kava.client.metier.EORibfourUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORibfourUlr object = ribfourUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, RIBFOUR_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, RIBFOUR_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
        return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
    }
    public void setTauxProrata(org.cocktail.kava.client.metier.EOTauxProrata aValue) {
        takeStoredValueForKey(aValue, TAUX_PRORATA_KEY);
    }
	
    public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTauxProrata object = tauxProrata();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TAUX_PRORATA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
    }
    public void setTypeCreditRec(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_REC_KEY);
    }
	
    public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditRec();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_REC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypePublic typePublic() {
        return (org.cocktail.kava.client.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
    }
    public void setTypePublic(org.cocktail.kava.client.metier.EOTypePublic aValue) {
        takeStoredValueForKey(aValue, TYPE_PUBLIC_KEY);
    }
	
    public void setTypePublicRelationship(org.cocktail.kava.client.metier.EOTypePublic value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypePublic object = typePublic();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_PUBLIC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateurValidationClient() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VALIDATION_CLIENT_KEY);
    }
    public void setUtilisateurValidationClient(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_VALIDATION_CLIENT_KEY);
    }
	
    public void setUtilisateurValidationClientRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateurValidationClient();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_VALIDATION_CLIENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VALIDATION_CLIENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateurValidationPrest() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VALIDATION_PREST_KEY);
    }
    public void setUtilisateurValidationPrest(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_VALIDATION_PREST_KEY);
    }
	
    public void setUtilisateurValidationPrestRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateurValidationPrest();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_VALIDATION_PREST_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VALIDATION_PREST_KEY);
        }
    }




    public NSArray facturePapierLignes() {
        return (NSArray)storedValueForKey(FACTURE_PAPIER_LIGNES_KEY);
    }
    public void setFacturePapierLignes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_PAPIER_LIGNES_KEY);
    }
    public void addToFacturePapierLignes(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        NSMutableArray array = (NSMutableArray)facturePapierLignes();
        willChange();
        array.addObject(object);
    }
    public void removeFromFacturePapierLignes(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        NSMutableArray array = (NSMutableArray)facturePapierLignes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFacturePapierLignesRelationship(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
    }
    public void removeFromFacturePapierLignesRelationship(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
    }
	


}

