
// _EOMandat.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOMandat.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOMandat extends EOGenericRecord {

    public static final String ENTITY_NAME = "Mandat";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_mandat";

    public static final String BOR_ID_KEY = "borId";
    public static final String MAN_ID_KEY = "manId";
    public static final String MAN_NUMERO_KEY = "manNumero";

    public static final String BOR_ID_COLKEY = "BOR_ID";
    public static final String MAN_ID_COLKEY = "MAN_ID";
    public static final String MAN_NUMERO_COLKEY = "MAN_NUMERO";

    public static final String BORDEREAU_KEY = "bordereau";




	
    public _EOMandat() {
        super();
    }




    public Number borId() {
        return (Number)storedValueForKey(BOR_ID_KEY);
    }
    public void setBorId(Number aValue) {
        takeStoredValueForKey(aValue, BOR_ID_KEY);
    }

    public Number manId() {
        return (Number)storedValueForKey(MAN_ID_KEY);
    }
    public void setManId(Number aValue) {
        takeStoredValueForKey(aValue, MAN_ID_KEY);
    }

    public Number manNumero() {
        return (Number)storedValueForKey(MAN_NUMERO_KEY);
    }
    public void setManNumero(Number aValue) {
        takeStoredValueForKey(aValue, MAN_NUMERO_KEY);
    }




    public org.cocktail.kava.client.metier.EOBordereau bordereau() {
        return (org.cocktail.kava.client.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
    }
    public void setBordereau(org.cocktail.kava.client.metier.EOBordereau aValue) {
        takeStoredValueForKey(aValue, BORDEREAU_KEY);
    }
	
    public void setBordereauRelationship(org.cocktail.kava.client.metier.EOBordereau value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOBordereau object = bordereau();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, BORDEREAU_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
        }
    }





}

