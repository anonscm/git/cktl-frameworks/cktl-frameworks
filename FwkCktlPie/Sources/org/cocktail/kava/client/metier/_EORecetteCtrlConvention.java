
// _EORecetteCtrlConvention.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecetteCtrlConvention.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecetteCtrlConvention extends EOGenericRecord {

    public static final String ENTITY_NAME = "RecetteCtrlConvention";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_convention";

    public static final String RCON_DATE_SAISIE_KEY = "rconDateSaisie";
    public static final String RCON_HT_SAISIE_KEY = "rconHtSaisie";
    public static final String RCON_MONTANT_BUDGETAIRE_KEY = "rconMontantBudgetaire";
    public static final String RCON_TTC_SAISIE_KEY = "rconTtcSaisie";
    public static final String RCON_TVA_SAISIE_KEY = "rconTvaSaisie";
    public static final String REC_ID_KEY = "recId";

    public static final String RCON_DATE_SAISIE_COLKEY = "RCON_DATE_SAISIE";
    public static final String RCON_HT_SAISIE_COLKEY = "RCON_HT_SAISIE";
    public static final String RCON_MONTANT_BUDGETAIRE_COLKEY = "RCON_MONTANT_BUDGETAIRE";
    public static final String RCON_TTC_SAISIE_COLKEY = "RCON_TTC_SAISIE";
    public static final String RCON_TVA_SAISIE_COLKEY = "RCON_TVA_SAISIE";
    public static final String REC_ID_COLKEY = "REC_ID";

    public static final String CONVENTION_KEY = "convention";
    public static final String EXERCICE_KEY = "exercice";




	
    public _EORecetteCtrlConvention() {
        super();
    }




    public NSTimestamp rconDateSaisie() {
        return (NSTimestamp)storedValueForKey(RCON_DATE_SAISIE_KEY);
    }
    public void setRconDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RCON_DATE_SAISIE_KEY);
    }

    public BigDecimal rconHtSaisie() {
        return (BigDecimal)storedValueForKey(RCON_HT_SAISIE_KEY);
    }
    public void setRconHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RCON_HT_SAISIE_KEY);
    }

    public BigDecimal rconMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(RCON_MONTANT_BUDGETAIRE_KEY);
    }
    public void setRconMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RCON_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal rconTtcSaisie() {
        return (BigDecimal)storedValueForKey(RCON_TTC_SAISIE_KEY);
    }
    public void setRconTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RCON_TTC_SAISIE_KEY);
    }

    public BigDecimal rconTvaSaisie() {
        return (BigDecimal)storedValueForKey(RCON_TVA_SAISIE_KEY);
    }
    public void setRconTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RCON_TVA_SAISIE_KEY);
    }

    public Number recId() {
        return (Number)storedValueForKey(REC_ID_KEY);
    }
    public void setRecId(Number aValue) {
        takeStoredValueForKey(aValue, REC_ID_KEY);
    }




    public org.cocktail.kava.client.metier.EOConvention convention() {
        return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
    }
    public void setConvention(org.cocktail.kava.client.metier.EOConvention aValue) {
        takeStoredValueForKey(aValue, CONVENTION_KEY);
    }
	
    public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOConvention object = convention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }





}

