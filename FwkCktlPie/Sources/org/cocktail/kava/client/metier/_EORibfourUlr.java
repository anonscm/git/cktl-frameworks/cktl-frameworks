
// _EORibfourUlr.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORibfourUlr.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORibfourUlr extends EOGenericRecord {

    public static final String ENTITY_NAME = "RibfourUlr";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_ribfour_ulr";

    public static final String BIC_KEY = "bic";
    public static final String C_BANQUE_KEY = "cBanque";
    public static final String C_GUICHET_KEY = "cGuichet";
    public static final String CLE_RIB_KEY = "cleRib";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String IBAN_KEY = "iban";
    public static final String MOD_CODE_KEY = "modCode";
    public static final String NO_COMPTE_KEY = "noCompte";
    public static final String RIB_ORDRE_KEY = "ribOrdre";
    public static final String RIB_TITCO_KEY = "ribTitco";
    public static final String RIB_VALIDE_KEY = "ribValide";
    public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";

    public static final String BIC_COLKEY = "BIC";
    public static final String C_BANQUE_COLKEY = "C_BANQUE";
    public static final String C_GUICHET_COLKEY = "C_GUICHET";
    public static final String CLE_RIB_COLKEY = "CLE_RIB";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String IBAN_COLKEY = "IBAN";
    public static final String MOD_CODE_COLKEY = "MOD_CODE";
    public static final String NO_COMPTE_COLKEY = "NO_COMPTE";
    public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
    public static final String RIB_TITCO_COLKEY = "RIB_TITCO";
    public static final String RIB_VALIDE_COLKEY = "RIB_VALIDE";
    public static final String TEM_PAYE_UTIL_COLKEY = "TEM_PAYE_UTIL";

    public static final String FOURNIS_ULR_KEY = "fournisUlr";




	
    public _EORibfourUlr() {
        super();
    }




    public String bic() {
        return (String)storedValueForKey(BIC_KEY);
    }
    public void setBic(String aValue) {
        takeStoredValueForKey(aValue, BIC_KEY);
    }

    public String cBanque() {
        return (String)storedValueForKey(C_BANQUE_KEY);
    }
    public void setCBanque(String aValue) {
        takeStoredValueForKey(aValue, C_BANQUE_KEY);
    }

    public String cGuichet() {
        return (String)storedValueForKey(C_GUICHET_KEY);
    }
    public void setCGuichet(String aValue) {
        takeStoredValueForKey(aValue, C_GUICHET_KEY);
    }

    public String cleRib() {
        return (String)storedValueForKey(CLE_RIB_KEY);
    }
    public void setCleRib(String aValue) {
        takeStoredValueForKey(aValue, CLE_RIB_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public String iban() {
        return (String)storedValueForKey(IBAN_KEY);
    }
    public void setIban(String aValue) {
        takeStoredValueForKey(aValue, IBAN_KEY);
    }

    public String modCode() {
        return (String)storedValueForKey(MOD_CODE_KEY);
    }
    public void setModCode(String aValue) {
        takeStoredValueForKey(aValue, MOD_CODE_KEY);
    }

    public String noCompte() {
        return (String)storedValueForKey(NO_COMPTE_KEY);
    }
    public void setNoCompte(String aValue) {
        takeStoredValueForKey(aValue, NO_COMPTE_KEY);
    }

    public Number ribOrdre() {
        return (Number)storedValueForKey(RIB_ORDRE_KEY);
    }
    public void setRibOrdre(Number aValue) {
        takeStoredValueForKey(aValue, RIB_ORDRE_KEY);
    }

    public String ribTitco() {
        return (String)storedValueForKey(RIB_TITCO_KEY);
    }
    public void setRibTitco(String aValue) {
        takeStoredValueForKey(aValue, RIB_TITCO_KEY);
    }

    public String ribValide() {
        return (String)storedValueForKey(RIB_VALIDE_KEY);
    }
    public void setRibValide(String aValue) {
        takeStoredValueForKey(aValue, RIB_VALIDE_KEY);
    }

    public String temPayeUtil() {
        return (String)storedValueForKey(TEM_PAYE_UTIL_KEY);
    }
    public void setTemPayeUtil(String aValue) {
        takeStoredValueForKey(aValue, TEM_PAYE_UTIL_KEY);
    }




    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }





}

