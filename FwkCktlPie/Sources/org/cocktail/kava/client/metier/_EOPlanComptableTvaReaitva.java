
// _EOPlanComptableTvaReaitva.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPlanComptableTvaReaitva.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOPlanComptableTvaReaitva extends EOGenericRecord {

    public static final String ENTITY_NAME = "PlanComptableTvaReaivta";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_plan_comptable_tva_reaitva";

    public static final String PCO_NUM_KEY = "pcoNum";
    public static final String PCO_VALIDITE_KEY = "pcoValidite";

    public static final String PCO_NUM_COLKEY = "PCO_NUM";
    public static final String PCO_VALIDITE_COLKEY = "PCO_VALIDITE";

    public static final String PLAN_COMPTABLE_KEY = "planComptable";




	
    public _EOPlanComptableTvaReaitva() {
        super();
    }




    public String pcoNum() {
        return (String)storedValueForKey(PCO_NUM_KEY);
    }
    public void setPcoNum(String aValue) {
        takeStoredValueForKey(aValue, PCO_NUM_KEY);
    }

    public String pcoValidite() {
        return (String)storedValueForKey(PCO_VALIDITE_KEY);
    }
    public void setPcoValidite(String aValue) {
        takeStoredValueForKey(aValue, PCO_VALIDITE_KEY);
    }




    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }





}

