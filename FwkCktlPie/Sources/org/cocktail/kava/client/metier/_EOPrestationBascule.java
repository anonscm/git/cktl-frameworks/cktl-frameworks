
// _EOPrestationBascule.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPrestationBascule.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOPrestationBascule extends EOGenericRecord {

    public static final String ENTITY_NAME = "PrestationBascule";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_bascule";

    public static final String PREST_ID_ORIGINE_BIS_KEY = "prestIdOrigineBis";

    public static final String PREST_ID_ORIGINE_BIS_COLKEY = "PREST_ID_ORIGINE";

    public static final String PRESTATION_DESTINATION_KEY = "prestationDestination";
    public static final String PRESTATION_ORIGINE_KEY = "prestationOrigine";




	
    public _EOPrestationBascule() {
        super();
    }




    public Number prestIdOrigineBis() {
        return (Number)storedValueForKey(PREST_ID_ORIGINE_BIS_KEY);
    }
    public void setPrestIdOrigineBis(Number aValue) {
        takeStoredValueForKey(aValue, PREST_ID_ORIGINE_BIS_KEY);
    }




    public org.cocktail.kava.client.metier.EOPrestation prestationDestination() {
        return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(PRESTATION_DESTINATION_KEY);
    }
    public void setPrestationDestination(org.cocktail.kava.client.metier.EOPrestation aValue) {
        takeStoredValueForKey(aValue, PRESTATION_DESTINATION_KEY);
    }
	
    public void setPrestationDestinationRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestation object = prestationDestination();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_DESTINATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_DESTINATION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPrestation prestationOrigine() {
        return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(PRESTATION_ORIGINE_KEY);
    }
    public void setPrestationOrigine(org.cocktail.kava.client.metier.EOPrestation aValue) {
        takeStoredValueForKey(aValue, PRESTATION_ORIGINE_KEY);
    }
	
    public void setPrestationOrigineRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestation object = prestationOrigine();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_ORIGINE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_ORIGINE_KEY);
        }
    }





}

