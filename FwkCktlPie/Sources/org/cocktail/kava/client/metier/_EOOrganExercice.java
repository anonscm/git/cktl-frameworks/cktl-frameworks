
// _EOOrganExercice.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOOrganExercice.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOOrganExercice extends EOGenericRecord {

    public static final String ENTITY_NAME = "OrganExercice";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_organ_exercice";

    public static final String ORG_CR_KEY = "orgCr";
    public static final String ORG_DATE_CLOTURE_KEY = "orgDateCloture";
    public static final String ORG_DATE_OUVERTURE_KEY = "orgDateOuverture";
    public static final String ORG_ETAB_KEY = "orgEtab";
    public static final String ORG_LIB_KEY = "orgLib";
    public static final String ORG_LIBELLE_KEY = "orgLibelle";
    public static final String ORG_LUCRATIVITE_KEY = "orgLucrativite";
    public static final String ORG_NIV_KEY = "orgNiv";
    public static final String ORG_SOUSCR_KEY = "orgSouscr";
    public static final String ORG_UB_KEY = "orgUb";
    public static final String ORG_UNIV_KEY = "orgUniv";

    public static final String ORG_CR_COLKEY = "ORG_CR";
    public static final String ORG_DATE_CLOTURE_COLKEY = "ORG_DATE_CLOTURE";
    public static final String ORG_DATE_OUVERTURE_COLKEY = "ORG_DATE_OUVERTURE";
    public static final String ORG_ETAB_COLKEY = "ORG_ETAB";
    public static final String ORG_LIBELLE_COLKEY = "ORG_LIB";
    public static final String ORG_LUCRATIVITE_COLKEY = "ORG_LUCRATIVITE";
    public static final String ORG_NIV_COLKEY = "ORG_NIV";
    public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
    public static final String ORG_UB_COLKEY = "ORG_UB";
    public static final String ORG_UNIV_COLKEY = "ORG_UNIV";

    public static final String EXERCICE_KEY = "exercice";
    public static final String GESTION_KEY = "gestion";
    public static final String ORGAN_KEY = "organ";
    public static final String ORGAN_PERE_KEY = "organPere";
    public static final String STRUCTURE_ULR_KEY = "structureUlr";
    public static final String TYPE_ORGAN_KEY = "typeOrgan";

    public static final String BUDGET_EXEC_CREDITS_KEY = "budgetExecCredits";
    public static final String ORGAN_FILS_KEY = "organFils";
    public static final String ORGAN_SIGNATAIRES_KEY = "organSignataires";
    public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";



	
    public _EOOrganExercice() {
        super();
    }




    public String orgCr() {
        return (String)storedValueForKey(ORG_CR_KEY);
    }
    public void setOrgCr(String aValue) {
        takeStoredValueForKey(aValue, ORG_CR_KEY);
    }

    public NSTimestamp orgDateCloture() {
        return (NSTimestamp)storedValueForKey(ORG_DATE_CLOTURE_KEY);
    }
    public void setOrgDateCloture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ORG_DATE_CLOTURE_KEY);
    }

    public NSTimestamp orgDateOuverture() {
        return (NSTimestamp)storedValueForKey(ORG_DATE_OUVERTURE_KEY);
    }
    public void setOrgDateOuverture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ORG_DATE_OUVERTURE_KEY);
    }

    public String orgEtab() {
        return (String)storedValueForKey(ORG_ETAB_KEY);
    }
    public void setOrgEtab(String aValue) {
        takeStoredValueForKey(aValue, ORG_ETAB_KEY);
    }

    public String orgLib() {
        return (String)storedValueForKey(ORG_LIB_KEY);
    }
    public void setOrgLib(String aValue) {
        takeStoredValueForKey(aValue, ORG_LIB_KEY);
    }

    public String orgLibelle() {
        return (String)storedValueForKey(ORG_LIBELLE_KEY);
    }
    public void setOrgLibelle(String aValue) {
        takeStoredValueForKey(aValue, ORG_LIBELLE_KEY);
    }

    public Number orgLucrativite() {
        return (Number)storedValueForKey(ORG_LUCRATIVITE_KEY);
    }
    public void setOrgLucrativite(Number aValue) {
        takeStoredValueForKey(aValue, ORG_LUCRATIVITE_KEY);
    }

    public Number orgNiv() {
        return (Number)storedValueForKey(ORG_NIV_KEY);
    }
    public void setOrgNiv(Number aValue) {
        takeStoredValueForKey(aValue, ORG_NIV_KEY);
    }

    public String orgSouscr() {
        return (String)storedValueForKey(ORG_SOUSCR_KEY);
    }
    public void setOrgSouscr(String aValue) {
        takeStoredValueForKey(aValue, ORG_SOUSCR_KEY);
    }

    public String orgUb() {
        return (String)storedValueForKey(ORG_UB_KEY);
    }
    public void setOrgUb(String aValue) {
        takeStoredValueForKey(aValue, ORG_UB_KEY);
    }

    public String orgUniv() {
        return (String)storedValueForKey(ORG_UNIV_KEY);
    }
    public void setOrgUniv(String aValue) {
        takeStoredValueForKey(aValue, ORG_UNIV_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOGestion gestion() {
        return (org.cocktail.kava.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
    }
    public void setGestion(org.cocktail.kava.client.metier.EOGestion aValue) {
        takeStoredValueForKey(aValue, GESTION_KEY);
    }
	
    public void setGestionRelationship(org.cocktail.kava.client.metier.EOGestion value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOGestion object = gestion();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, GESTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organPere() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_PERE_KEY);
    }
    public void setOrganPere(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_PERE_KEY);
    }
	
    public void setOrganPereRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organPere();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PERE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_PERE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOStructureUlr structureUlr() {
        return (org.cocktail.kava.client.metier.EOStructureUlr)storedValueForKey(STRUCTURE_ULR_KEY);
    }
    public void setStructureUlr(org.cocktail.kava.client.metier.EOStructureUlr aValue) {
        takeStoredValueForKey(aValue, STRUCTURE_ULR_KEY);
    }
	
    public void setStructureUlrRelationship(org.cocktail.kava.client.metier.EOStructureUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOStructureUlr object = structureUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeOrgan typeOrgan() {
        return (org.cocktail.application.client.eof.EOTypeOrgan)storedValueForKey(TYPE_ORGAN_KEY);
    }
    public void setTypeOrgan(org.cocktail.application.client.eof.EOTypeOrgan aValue) {
        takeStoredValueForKey(aValue, TYPE_ORGAN_KEY);
    }
	
    public void setTypeOrganRelationship(org.cocktail.application.client.eof.EOTypeOrgan value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeOrgan object = typeOrgan();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORGAN_KEY);
        }
    }




    public NSArray budgetExecCredits() {
        return (NSArray)storedValueForKey(BUDGET_EXEC_CREDITS_KEY);
    }
    public void setBudgetExecCredits(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, BUDGET_EXEC_CREDITS_KEY);
    }
    public void addToBudgetExecCredits(org.cocktail.kava.client.metier.EOBudgetExecCredit object) {
        NSMutableArray array = (NSMutableArray)budgetExecCredits();
        willChange();
        array.addObject(object);
    }
    public void removeFromBudgetExecCredits(org.cocktail.kava.client.metier.EOBudgetExecCredit object) {
        NSMutableArray array = (NSMutableArray)budgetExecCredits();
        willChange();
        array.removeObject(object);
    }
	
    public void addToBudgetExecCreditsRelationship(org.cocktail.kava.client.metier.EOBudgetExecCredit object) {
        addObjectToBothSidesOfRelationshipWithKey(object, BUDGET_EXEC_CREDITS_KEY);
    }
    public void removeFromBudgetExecCreditsRelationship(org.cocktail.kava.client.metier.EOBudgetExecCredit object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, BUDGET_EXEC_CREDITS_KEY);
    }
	

    public NSArray organFils() {
        return (NSArray)storedValueForKey(ORGAN_FILS_KEY);
    }
    public void setOrganFils(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ORGAN_FILS_KEY);
    }
    public void addToOrganFils(org.cocktail.kava.client.metier.EOOrgan object) {
        NSMutableArray array = (NSMutableArray)organFils();
        willChange();
        array.addObject(object);
    }
    public void removeFromOrganFils(org.cocktail.kava.client.metier.EOOrgan object) {
        NSMutableArray array = (NSMutableArray)organFils();
        willChange();
        array.removeObject(object);
    }
	
    public void addToOrganFilsRelationship(org.cocktail.kava.client.metier.EOOrgan object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
    }
    public void removeFromOrganFilsRelationship(org.cocktail.kava.client.metier.EOOrgan object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
    }
	

    public NSArray organSignataires() {
        return (NSArray)storedValueForKey(ORGAN_SIGNATAIRES_KEY);
    }
    public void setOrganSignataires(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ORGAN_SIGNATAIRES_KEY);
    }
    public void addToOrganSignataires(org.cocktail.kava.client.metier.EOOrganSignataire object) {
        NSMutableArray array = (NSMutableArray)organSignataires();
        willChange();
        array.addObject(object);
    }
    public void removeFromOrganSignataires(org.cocktail.kava.client.metier.EOOrganSignataire object) {
        NSMutableArray array = (NSMutableArray)organSignataires();
        willChange();
        array.removeObject(object);
    }
	
    public void addToOrganSignatairesRelationship(org.cocktail.kava.client.metier.EOOrganSignataire object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
    }
    public void removeFromOrganSignatairesRelationship(org.cocktail.kava.client.metier.EOOrganSignataire object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
    }
	

    public NSArray utilisateurOrgans() {
        return (NSArray)storedValueForKey(UTILISATEUR_ORGANS_KEY);
    }
    public void setUtilisateurOrgans(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_ORGANS_KEY);
    }
    public void addToUtilisateurOrgans(org.cocktail.kava.client.metier.EOUtilisateurOrgan object) {
        NSMutableArray array = (NSMutableArray)utilisateurOrgans();
        willChange();
        array.addObject(object);
    }
    public void removeFromUtilisateurOrgans(org.cocktail.kava.client.metier.EOUtilisateurOrgan object) {
        NSMutableArray array = (NSMutableArray)utilisateurOrgans();
        willChange();
        array.removeObject(object);
    }
	
    public void addToUtilisateurOrgansRelationship(org.cocktail.kava.client.metier.EOUtilisateurOrgan object) {
        addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    }
    public void removeFromUtilisateurOrgansRelationship(org.cocktail.kava.client.metier.EOUtilisateurOrgan object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    }
	


}

