
// _EOCatalogue.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCatalogue.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOCatalogue extends EOGenericRecord {

    public static final String ENTITY_NAME = "Catalogue";

    public static final String ENTITY_TABLE_NAME = "jefy_catalogue.catalogue";

    public static final String CAT_COMMENTAIRE_KEY = "catCommentaire";
    public static final String CAT_DATE_DEBUT_KEY = "catDateDebut";
    public static final String CAT_DATE_FIN_KEY = "catDateFin";
    public static final String CAT_LIBELLE_KEY = "catLibelle";

    public static final String CAT_COMMENTAIRE_COLKEY = "CAT_COMMENTAIRE";
    public static final String CAT_DATE_DEBUT_COLKEY = "CAT_DATE_DEBUT";
    public static final String CAT_DATE_FIN_COLKEY = "CAT_DATE_FIN";
    public static final String CAT_LIBELLE_COLKEY = "CAT_LIBELLE";

    public static final String CATALOGUE_PRESTATION_KEY = "cataloguePrestation";
    public static final String CODE_MARCHE_KEY = "codeMarche";
    public static final String FOURNIS_ULR_KEY = "fournisUlr";
    public static final String TYPE_APPLICATION_KEY = "typeApplication";
    public static final String TYPE_ETAT_KEY = "typeEtat";

    public static final String CATALOGUE_ARTICLES_KEY = "catalogueArticles";
    public static final String CATALOGUE_PRESTATION_WEBS_KEY = "cataloguePrestationWebs";
    public static final String CATALOGUE_PUBLICS_KEY = "cataloguePublics";
    public static final String CATALOGUE_RESPONSABLES_KEY = "catalogueResponsables";



	
    public _EOCatalogue() {
        super();
    }




    public String catCommentaire() {
        return (String)storedValueForKey(CAT_COMMENTAIRE_KEY);
    }
    public void setCatCommentaire(String aValue) {
        takeStoredValueForKey(aValue, CAT_COMMENTAIRE_KEY);
    }

    public NSTimestamp catDateDebut() {
        return (NSTimestamp)storedValueForKey(CAT_DATE_DEBUT_KEY);
    }
    public void setCatDateDebut(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, CAT_DATE_DEBUT_KEY);
    }

    public NSTimestamp catDateFin() {
        return (NSTimestamp)storedValueForKey(CAT_DATE_FIN_KEY);
    }
    public void setCatDateFin(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, CAT_DATE_FIN_KEY);
    }

    public String catLibelle() {
        return (String)storedValueForKey(CAT_LIBELLE_KEY);
    }
    public void setCatLibelle(String aValue) {
        takeStoredValueForKey(aValue, CAT_LIBELLE_KEY);
    }




    public org.cocktail.kava.client.metier.EOCataloguePrestation cataloguePrestation() {
        return (org.cocktail.kava.client.metier.EOCataloguePrestation)storedValueForKey(CATALOGUE_PRESTATION_KEY);
    }
    public void setCataloguePrestation(org.cocktail.kava.client.metier.EOCataloguePrestation aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_PRESTATION_KEY);
    }
	
    public void setCataloguePrestationRelationship(org.cocktail.kava.client.metier.EOCataloguePrestation value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCataloguePrestation object = cataloguePrestation();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PRESTATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_PRESTATION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOCodeMarche codeMarche() {
        return (org.cocktail.application.client.eof.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
    }
    public void setCodeMarche(org.cocktail.application.client.eof.EOCodeMarche aValue) {
        takeStoredValueForKey(aValue, CODE_MARCHE_KEY);
    }
	
    public void setCodeMarcheRelationship(org.cocktail.application.client.eof.EOCodeMarche value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOCodeMarche object = codeMarche();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_MARCHE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
        return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
    }
    public void setTypeApplication(org.cocktail.kava.client.metier.EOTypeApplication aValue) {
        takeStoredValueForKey(aValue, TYPE_APPLICATION_KEY);
    }
	
    public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeApplication object = typeApplication();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_APPLICATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }




    public NSArray catalogueArticles() {
        return (NSArray)storedValueForKey(CATALOGUE_ARTICLES_KEY);
    }
    public void setCatalogueArticles(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_ARTICLES_KEY);
    }
    public void addToCatalogueArticles(org.cocktail.kava.client.metier.EOCatalogueArticle object) {
        NSMutableArray array = (NSMutableArray)catalogueArticles();
        willChange();
        array.addObject(object);
    }
    public void removeFromCatalogueArticles(org.cocktail.kava.client.metier.EOCatalogueArticle object) {
        NSMutableArray array = (NSMutableArray)catalogueArticles();
        willChange();
        array.removeObject(object);
    }
	
    public void addToCatalogueArticlesRelationship(org.cocktail.kava.client.metier.EOCatalogueArticle object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_ARTICLES_KEY);
    }
    public void removeFromCatalogueArticlesRelationship(org.cocktail.kava.client.metier.EOCatalogueArticle object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_ARTICLES_KEY);
    }
	

    public NSArray cataloguePrestationWebs() {
        return (NSArray)storedValueForKey(CATALOGUE_PRESTATION_WEBS_KEY);
    }
    public void setCataloguePrestationWebs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_PRESTATION_WEBS_KEY);
    }
    public void addToCataloguePrestationWebs(org.cocktail.kava.client.metier.EOCataloguePrestationWeb object) {
        NSMutableArray array = (NSMutableArray)cataloguePrestationWebs();
        willChange();
        array.addObject(object);
    }
    public void removeFromCataloguePrestationWebs(org.cocktail.kava.client.metier.EOCataloguePrestationWeb object) {
        NSMutableArray array = (NSMutableArray)cataloguePrestationWebs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToCataloguePrestationWebsRelationship(org.cocktail.kava.client.metier.EOCataloguePrestationWeb object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_PRESTATION_WEBS_KEY);
    }
    public void removeFromCataloguePrestationWebsRelationship(org.cocktail.kava.client.metier.EOCataloguePrestationWeb object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PRESTATION_WEBS_KEY);
    }
	

    public NSArray cataloguePublics() {
        return (NSArray)storedValueForKey(CATALOGUE_PUBLICS_KEY);
    }
    public void setCataloguePublics(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_PUBLICS_KEY);
    }
    public void addToCataloguePublics(org.cocktail.kava.client.metier.EOCataloguePublic object) {
        NSMutableArray array = (NSMutableArray)cataloguePublics();
        willChange();
        array.addObject(object);
    }
    public void removeFromCataloguePublics(org.cocktail.kava.client.metier.EOCataloguePublic object) {
        NSMutableArray array = (NSMutableArray)cataloguePublics();
        willChange();
        array.removeObject(object);
    }
	
    public void addToCataloguePublicsRelationship(org.cocktail.kava.client.metier.EOCataloguePublic object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_PUBLICS_KEY);
    }
    public void removeFromCataloguePublicsRelationship(org.cocktail.kava.client.metier.EOCataloguePublic object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PUBLICS_KEY);
    }
	

    public NSArray catalogueResponsables() {
        return (NSArray)storedValueForKey(CATALOGUE_RESPONSABLES_KEY);
    }
    public void setCatalogueResponsables(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_RESPONSABLES_KEY);
    }
    public void addToCatalogueResponsables(org.cocktail.kava.client.metier.EOCatalogueResponsable object) {
        NSMutableArray array = (NSMutableArray)catalogueResponsables();
        willChange();
        array.addObject(object);
    }
    public void removeFromCatalogueResponsables(org.cocktail.kava.client.metier.EOCatalogueResponsable object) {
        NSMutableArray array = (NSMutableArray)catalogueResponsables();
        willChange();
        array.removeObject(object);
    }
	
    public void addToCatalogueResponsablesRelationship(org.cocktail.kava.client.metier.EOCatalogueResponsable object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_RESPONSABLES_KEY);
    }
    public void removeFromCatalogueResponsablesRelationship(org.cocktail.kava.client.metier.EOCatalogueResponsable object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_RESPONSABLES_KEY);
    }
	


}

