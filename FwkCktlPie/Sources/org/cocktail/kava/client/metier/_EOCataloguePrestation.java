
// _EOCataloguePrestation.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCataloguePrestation.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOCataloguePrestation extends EOGenericRecord {

    public static final String ENTITY_NAME = "CataloguePrestation";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.catalogue_prestation";

    public static final String CAT_DATE_VOTE_KEY = "catDateVote";
    public static final String CAT_NUMERO_KEY = "catNumero";
    public static final String CAT_PUBLIE_WEB_KEY = "catPublieWeb";

    public static final String CAT_DATE_VOTE_COLKEY = "CAT_DATE_VOTE";
    public static final String CAT_NUMERO_COLKEY = "CAT_NUMERO";
    public static final String CAT_PUBLIE_WEB_COLKEY = "CAT_PUBLIE_WEB";

    public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";
    public static final String ORGAN_RECETTE_KEY = "organRecette";
    public static final String PLAN_COMPTABLE_DEPENSE_KEY = "planComptableDepense";
    public static final String PLAN_COMPTABLE_RECETTE_KEY = "planComptableRecette";




	
    public _EOCataloguePrestation() {
        super();
    }




    public NSTimestamp catDateVote() {
        return (NSTimestamp)storedValueForKey(CAT_DATE_VOTE_KEY);
    }
    public void setCatDateVote(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, CAT_DATE_VOTE_KEY);
    }

    public Number catNumero() {
        return (Number)storedValueForKey(CAT_NUMERO_KEY);
    }
    public void setCatNumero(Number aValue) {
        takeStoredValueForKey(aValue, CAT_NUMERO_KEY);
    }

    public String catPublieWeb() {
        return (String)storedValueForKey(CAT_PUBLIE_WEB_KEY);
    }
    public void setCatPublieWeb(String aValue) {
        takeStoredValueForKey(aValue, CAT_PUBLIE_WEB_KEY);
    }




    public org.cocktail.kava.client.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
    }
    public void setLolfNomenclatureRecette(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
	
    public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureRecette object = lolfNomenclatureRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organRecette() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_RECETTE_KEY);
    }
    public void setOrganRecette(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_RECETTE_KEY);
    }
	
    public void setOrganRecetteRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_RECETTE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableDepense() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_DEPENSE_KEY);
    }
    public void setPlanComptableDepense(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_DEPENSE_KEY);
    }
	
    public void setPlanComptableDepenseRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableDepense();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_DEPENSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_DEPENSE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableRecette() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_RECETTE_KEY);
    }
    public void setPlanComptableRecette(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_RECETTE_KEY);
    }
	
    public void setPlanComptableRecetteRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_RECETTE_KEY);
        }
    }





}

