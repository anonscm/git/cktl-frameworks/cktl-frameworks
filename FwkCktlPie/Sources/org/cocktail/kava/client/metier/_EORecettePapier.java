
// _EORecettePapier.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecettePapier.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecettePapier extends EOGenericRecord {

    public static final String ENTITY_NAME = "RecettePapier";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_papier";

    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
    public static final String RPP_DATE_RECEPTION_KEY = "rppDateReception";
    public static final String RPP_DATE_RECETTE_KEY = "rppDateRecette";
    public static final String RPP_DATE_SAISIE_KEY = "rppDateSaisie";
    public static final String RPP_DATE_SERVICE_FAIT_KEY = "rppDateServiceFait";
    public static final String RPP_HT_SAISIE_KEY = "rppHtSaisie";
    public static final String RPP_NB_PIECE_KEY = "rppNbPiece";
    public static final String RPP_NUMERO_KEY = "rppNumero";
    public static final String RPP_TTC_SAISIE_KEY = "rppTtcSaisie";
    public static final String RPP_TVA_SAISIE_KEY = "rppTvaSaisie";
    public static final String RPP_VISIBLE_KEY = "rppVisible";

    public static final String RPP_DATE_RECEPTION_COLKEY = "RPP_DATE_RECEPTION";
    public static final String RPP_DATE_RECETTE_COLKEY = "RPP_DATE_RECETTE";
    public static final String RPP_DATE_SAISIE_COLKEY = "RPP_DATE_SAISIE";
    public static final String RPP_DATE_SERVICE_FAIT_COLKEY = "RPP_DATE_SERVICE_FAIT";
    public static final String RPP_HT_SAISIE_COLKEY = "RPP_HT_SAISIE";
    public static final String RPP_NB_PIECE_COLKEY = "RPP_NB_PIECE";
    public static final String RPP_NUMERO_COLKEY = "RPP_NUMERO";
    public static final String RPP_TTC_SAISIE_COLKEY = "RPP_TTC_SAISIE";
    public static final String RPP_TVA_SAISIE_COLKEY = "RPP_TVA_SAISIE";
    public static final String RPP_VISIBLE_COLKEY = "RPP_VISIBLE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String FOURNIS_ULR_KEY = "fournisUlr";
    public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
    public static final String PERSONNE_KEY = "personne";
    public static final String RIBFOUR_ULR_KEY = "ribfourUlr";
    public static final String UTILISATEUR_KEY = "utilisateur";

    public static final String RECETTES_KEY = "recettes";



	
    public _EORecettePapier() {
        super();
    }




    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public NSTimestamp rppDateReception() {
        return (NSTimestamp)storedValueForKey(RPP_DATE_RECEPTION_KEY);
    }
    public void setRppDateReception(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RPP_DATE_RECEPTION_KEY);
    }

    public NSTimestamp rppDateRecette() {
        return (NSTimestamp)storedValueForKey(RPP_DATE_RECETTE_KEY);
    }
    public void setRppDateRecette(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RPP_DATE_RECETTE_KEY);
    }

    public NSTimestamp rppDateSaisie() {
        return (NSTimestamp)storedValueForKey(RPP_DATE_SAISIE_KEY);
    }
    public void setRppDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RPP_DATE_SAISIE_KEY);
    }

    public NSTimestamp rppDateServiceFait() {
        return (NSTimestamp)storedValueForKey(RPP_DATE_SERVICE_FAIT_KEY);
    }
    public void setRppDateServiceFait(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RPP_DATE_SERVICE_FAIT_KEY);
    }

    public BigDecimal rppHtSaisie() {
        return (BigDecimal)storedValueForKey(RPP_HT_SAISIE_KEY);
    }
    public void setRppHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPP_HT_SAISIE_KEY);
    }

    public Number rppNbPiece() {
        return (Number)storedValueForKey(RPP_NB_PIECE_KEY);
    }
    public void setRppNbPiece(Number aValue) {
        takeStoredValueForKey(aValue, RPP_NB_PIECE_KEY);
    }

    public String rppNumero() {
        return (String)storedValueForKey(RPP_NUMERO_KEY);
    }
    public void setRppNumero(String aValue) {
        takeStoredValueForKey(aValue, RPP_NUMERO_KEY);
    }

    public BigDecimal rppTtcSaisie() {
        return (BigDecimal)storedValueForKey(RPP_TTC_SAISIE_KEY);
    }
    public void setRppTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPP_TTC_SAISIE_KEY);
    }

    public BigDecimal rppTvaSaisie() {
        return (BigDecimal)storedValueForKey(RPP_TVA_SAISIE_KEY);
    }
    public void setRppTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPP_TVA_SAISIE_KEY);
    }

    public String rppVisible() {
        return (String)storedValueForKey(RPP_VISIBLE_KEY);
    }
    public void setRppVisible(String aValue) {
        takeStoredValueForKey(aValue, RPP_VISIBLE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOModeRecouvrement modeRecouvrement() {
        return (org.cocktail.kava.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
    }
    public void setModeRecouvrement(org.cocktail.kava.client.metier.EOModeRecouvrement aValue) {
        takeStoredValueForKey(aValue, MODE_RECOUVREMENT_KEY);
    }
	
    public void setModeRecouvrementRelationship(org.cocktail.kava.client.metier.EOModeRecouvrement value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOModeRecouvrement object = modeRecouvrement();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, MODE_RECOUVREMENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORibfourUlr ribfourUlr() {
        return (org.cocktail.kava.client.metier.EORibfourUlr)storedValueForKey(RIBFOUR_ULR_KEY);
    }
    public void setRibfourUlr(org.cocktail.kava.client.metier.EORibfourUlr aValue) {
        takeStoredValueForKey(aValue, RIBFOUR_ULR_KEY);
    }
	
    public void setRibfourUlrRelationship(org.cocktail.kava.client.metier.EORibfourUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORibfourUlr object = ribfourUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, RIBFOUR_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, RIBFOUR_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }




    public NSArray recettes() {
        return (NSArray)storedValueForKey(RECETTES_KEY);
    }
    public void setRecettes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTES_KEY);
    }
    public void addToRecettes(org.cocktail.kava.client.metier.EORecette object) {
        NSMutableArray array = (NSMutableArray)recettes();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecettes(org.cocktail.kava.client.metier.EORecette object) {
        NSMutableArray array = (NSMutableArray)recettes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    }
    public void removeFromRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    }
	


}

