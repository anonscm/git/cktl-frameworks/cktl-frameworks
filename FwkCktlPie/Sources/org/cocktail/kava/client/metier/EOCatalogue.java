// EOCatalogue.java
// 
package org.cocktail.kava.client.metier;

import org.cocktail.kava.client.PieFwkUtilities;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCatalogue extends _EOCatalogue {
	
//	EOTypeEtat etatValide = null;
	
	public EOCatalogue() {
		super();
		
	}
	
	
	public String periodeValidite() {
		String periode = PieFwkUtilities.formaterPeriodeValidite(catDateDebut(), catDateFin());
		return periode;
	}


	
	
	public String compCatLibelle() {
		
		return "["+typeEtat().tyetLibelle()+"] "+catLibelle();
		
	}
	

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (catDateDebut() == null) {
			throw new ValidationException("Il faut une date de debut de validite pour le catalogue!");
		}
		if (catLibelle() == null) {
			throw new ValidationException("Il faut un libelle pour le catalogue!");
		}
		if (catCommentaire() == null) {
			throw new ValidationException("Il faut un commentaire pour le catalogue!");
		}
		if (fournisUlr() == null) {
			throw new ValidationException("Il faut un fournisseur (prestataire) pour le catalogue!");
		}
		if (catDateFin() != null && catDateDebut().after(catDateFin())) {
			throw new ValidationException("Il serait sans doute judicieux que la date de debut de validite du catalogue soit AVANT la date de fin!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}

}
