
// _EOEngageCtrlPlanco.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOEngageCtrlPlanco.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOEngageCtrlPlanco extends EOGenericRecord {

    public static final String ENTITY_NAME = "EngageCtrlPlanco";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_engage_ctrl_planco";

    public static final String EPCO_DATE_SAISIE_KEY = "epcoDateSaisie";
    public static final String EPCO_HT_SAISIE_KEY = "epcoHtSaisie";
    public static final String EPCO_MONTANT_BUDGETAIRE_KEY = "epcoMontantBudgetaire";
    public static final String EPCO_MONTANT_BUDGETAIRE_RESTE_KEY = "epcoMontantBudgetaireReste";
    public static final String EPCO_TTC_SAISIE_KEY = "epcoTtcSaisie";
    public static final String EPCO_TVA_SAISIE_KEY = "epcoTvaSaisie";
    public static final String EXE_ORDRE_KEY = "exeOrdre";

    public static final String EPCO_DATE_SAISIE_COLKEY = "EPCO_DATE_SAISIE";
    public static final String EPCO_HT_SAISIE_COLKEY = "EPCO_HT_SAISIE";
    public static final String EPCO_MONTANT_BUDGETAIRE_COLKEY = "EPCO_MONTANT_BUDGETAIRE";
    public static final String EPCO_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EPCO_MONTANT_BUDGETAIRE_RESTE";
    public static final String EPCO_TTC_SAISIE_COLKEY = "EPCO_TTC_SAISIE";
    public static final String EPCO_TVA_SAISIE_COLKEY = "EPCO_TVA_SAISIE";
    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

    public static final String PLAN_COMPTABLE_KEY = "planComptable";




	
    public _EOEngageCtrlPlanco() {
        super();
    }




    public NSTimestamp epcoDateSaisie() {
        return (NSTimestamp)storedValueForKey(EPCO_DATE_SAISIE_KEY);
    }
    public void setEpcoDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, EPCO_DATE_SAISIE_KEY);
    }

    public BigDecimal epcoHtSaisie() {
        return (BigDecimal)storedValueForKey(EPCO_HT_SAISIE_KEY);
    }
    public void setEpcoHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EPCO_HT_SAISIE_KEY);
    }

    public BigDecimal epcoMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(EPCO_MONTANT_BUDGETAIRE_KEY);
    }
    public void setEpcoMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EPCO_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal epcoMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(EPCO_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setEpcoMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EPCO_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public BigDecimal epcoTtcSaisie() {
        return (BigDecimal)storedValueForKey(EPCO_TTC_SAISIE_KEY);
    }
    public void setEpcoTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EPCO_TTC_SAISIE_KEY);
    }

    public BigDecimal epcoTvaSaisie() {
        return (BigDecimal)storedValueForKey(EPCO_TVA_SAISIE_KEY);
    }
    public void setEpcoTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EPCO_TVA_SAISIE_KEY);
    }

    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }




    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }





}

