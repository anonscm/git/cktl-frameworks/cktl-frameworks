
// _EOPersonneTelephone.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPersonneTelephone.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOPersonneTelephone extends EOGenericRecord {

    public static final String ENTITY_NAME = "PersonneTelephone";

    public static final String ENTITY_TABLE_NAME = "grhum.personne_telephone";

    public static final String C_STRUCTURE_KEY = "cStructure";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_DEB_VAL_KEY = "dDebVal";
    public static final String D_FIN_VAL_KEY = "dFinVal";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String INDICATIF_KEY = "indicatif";
    public static final String LISTE_ROUGE_KEY = "listeRouge";
    public static final String NO_TELEPHONE_KEY = "noTelephone";

    public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
    public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String INDICATIF_COLKEY = "INDICATIF";
    public static final String LISTE_ROUGE_COLKEY = "LISTE_ROUGE";
    public static final String NO_TELEPHONE_COLKEY = "NO_TELEPHONE";

    public static final String TYPE_NO_TEL_KEY = "typeNoTel";
    public static final String TYPE_TEL_RELATIONSHIP_KEY = "typeTelRelationship";




	
    public _EOPersonneTelephone() {
        super();
    }




    public String cStructure() {
        return (String)storedValueForKey(C_STRUCTURE_KEY);
    }
    public void setCStructure(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dDebVal() {
        return (NSTimestamp)storedValueForKey(D_DEB_VAL_KEY);
    }
    public void setDDebVal(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_DEB_VAL_KEY);
    }

    public NSTimestamp dFinVal() {
        return (NSTimestamp)storedValueForKey(D_FIN_VAL_KEY);
    }
    public void setDFinVal(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_FIN_VAL_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public Number indicatif() {
        return (Number)storedValueForKey(INDICATIF_KEY);
    }
    public void setIndicatif(Number aValue) {
        takeStoredValueForKey(aValue, INDICATIF_KEY);
    }

    public String listeRouge() {
        return (String)storedValueForKey(LISTE_ROUGE_KEY);
    }
    public void setListeRouge(String aValue) {
        takeStoredValueForKey(aValue, LISTE_ROUGE_KEY);
    }

    public String noTelephone() {
        return (String)storedValueForKey(NO_TELEPHONE_KEY);
    }
    public void setNoTelephone(String aValue) {
        takeStoredValueForKey(aValue, NO_TELEPHONE_KEY);
    }




    public org.cocktail.kava.client.metier.EOTypeNoTel typeNoTel() {
        return (org.cocktail.kava.client.metier.EOTypeNoTel)storedValueForKey(TYPE_NO_TEL_KEY);
    }
    public void setTypeNoTel(org.cocktail.kava.client.metier.EOTypeNoTel aValue) {
        takeStoredValueForKey(aValue, TYPE_NO_TEL_KEY);
    }
	
    public void setTypeNoTelRelationship(org.cocktail.kava.client.metier.EOTypeNoTel value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeNoTel object = typeNoTel();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_NO_TEL_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_NO_TEL_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeTel typeTelRelationship() {
        return (org.cocktail.kava.client.metier.EOTypeTel)storedValueForKey(TYPE_TEL_RELATIONSHIP_KEY);
    }
    public void setTypeTelRelationship(org.cocktail.kava.client.metier.EOTypeTel aValue) {
        takeStoredValueForKey(aValue, TYPE_TEL_RELATIONSHIP_KEY);
    }
	
    public void setTypeTelRelationshipRelationship(org.cocktail.kava.client.metier.EOTypeTel value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeTel object = typeTelRelationship();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_TEL_RELATIONSHIP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TEL_RELATIONSHIP_KEY);
        }
    }





}

