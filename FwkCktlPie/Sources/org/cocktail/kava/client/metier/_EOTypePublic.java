
// _EOTypePublic.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOTypePublic.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOTypePublic extends EOGenericRecord {

    public static final String ENTITY_NAME = "TypePublic";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.type_public";

    public static final String TYPU_ACCES_INTERNE_KEY = "typuAccesInterne";
    public static final String TYPU_LIBELLE_KEY = "typuLibelle";

    public static final String TYPU_ACCES_INTERNE_COLKEY = "TYPU_ACCES_INTERNE";
    public static final String TYPU_LIBELLE_COLKEY = "TYPU_LIBELLE";

    public static final String TYPE_APPLICATION_KEY = "typeApplication";




	
    public _EOTypePublic() {
        super();
    }




    public String typuAccesInterne() {
        return (String)storedValueForKey(TYPU_ACCES_INTERNE_KEY);
    }
    public void setTypuAccesInterne(String aValue) {
        takeStoredValueForKey(aValue, TYPU_ACCES_INTERNE_KEY);
    }

    public String typuLibelle() {
        return (String)storedValueForKey(TYPU_LIBELLE_KEY);
    }
    public void setTypuLibelle(String aValue) {
        takeStoredValueForKey(aValue, TYPU_LIBELLE_KEY);
    }




    public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
        return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
    }
    public void setTypeApplication(org.cocktail.kava.client.metier.EOTypeApplication aValue) {
        takeStoredValueForKey(aValue, TYPE_APPLICATION_KEY);
    }
	
    public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeApplication object = typeApplication();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_APPLICATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
        }
    }





}

