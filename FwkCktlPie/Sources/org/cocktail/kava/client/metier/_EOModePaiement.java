
// _EOModePaiement.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOModePaiement.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOModePaiement extends EOGenericRecord {

    public static final String ENTITY_NAME = "ModePaiement";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_mode_paiement";

    public static final String MOD_CODE_KEY = "modCode";
    public static final String MOD_DOM_KEY = "modDom";
    public static final String MOD_EMA_AUTO_KEY = "modEmaAuto";
    public static final String MOD_LIBELLE_KEY = "modLibelle";
    public static final String MOD_VALIDITE_KEY = "modValidite";

    public static final String MOD_CODE_COLKEY = "MOD_CODE";
    public static final String MOD_DOM_COLKEY = "MOD_DOM";
    public static final String MOD_EMA_AUTO_COLKEY = "MOD_EMA_AUTO";
    public static final String MOD_LIBELLE_COLKEY = "MOD_LIBELLE";
    public static final String MOD_VALIDITE_COLKEY = "MOD_VALIDITE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String PLAN_COMPTABLE_PAIEMENT_KEY = "planComptablePaiement";
    public static final String PLAN_COMPTABLE_VISA_KEY = "planComptableVisa";




	
    public _EOModePaiement() {
        super();
    }




    public String modCode() {
        return (String)storedValueForKey(MOD_CODE_KEY);
    }
    public void setModCode(String aValue) {
        takeStoredValueForKey(aValue, MOD_CODE_KEY);
    }

    public String modDom() {
        return (String)storedValueForKey(MOD_DOM_KEY);
    }
    public void setModDom(String aValue) {
        takeStoredValueForKey(aValue, MOD_DOM_KEY);
    }

    public String modEmaAuto() {
        return (String)storedValueForKey(MOD_EMA_AUTO_KEY);
    }
    public void setModEmaAuto(String aValue) {
        takeStoredValueForKey(aValue, MOD_EMA_AUTO_KEY);
    }

    public String modLibelle() {
        return (String)storedValueForKey(MOD_LIBELLE_KEY);
    }
    public void setModLibelle(String aValue) {
        takeStoredValueForKey(aValue, MOD_LIBELLE_KEY);
    }

    public String modValidite() {
        return (String)storedValueForKey(MOD_VALIDITE_KEY);
    }
    public void setModValidite(String aValue) {
        takeStoredValueForKey(aValue, MOD_VALIDITE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptablePaiement() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_PAIEMENT_KEY);
    }
    public void setPlanComptablePaiement(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_PAIEMENT_KEY);
    }
	
    public void setPlanComptablePaiementRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptablePaiement();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_PAIEMENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_PAIEMENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableVisa() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_VISA_KEY);
    }
    public void setPlanComptableVisa(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_VISA_KEY);
    }
	
    public void setPlanComptableVisaRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableVisa();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_VISA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_VISA_KEY);
        }
    }





}

