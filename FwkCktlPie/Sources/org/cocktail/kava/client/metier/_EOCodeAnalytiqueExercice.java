
// _EOCodeAnalytiqueExercice.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCodeAnalytiqueExercice.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOCodeAnalytiqueExercice extends EOGenericRecord {

    public static final String ENTITY_NAME = "CodeAnalytiqueExercice";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_code_analytique_exercice";

    public static final String CAN_CODE_KEY = "canCode";
    public static final String CAN_LIBELLE_KEY = "canLibelle";

    public static final String CAN_CODE_COLKEY = "CAN_CODE";
    public static final String CAN_LIBELLE_COLKEY = "CAN_LIBELLE";

    public static final String CODE_ANALYTIQUE_PERE_KEY = "codeAnalytiquePere";
    public static final String EXERCICE_KEY = "exercice";
    public static final String TYPE_ETAT_KEY = "typeEtat";
    public static final String TYPE_ETAT_PUBLIC_KEY = "typeEtatPublic";
    public static final String TYPE_ETAT_UTILISABLE_KEY = "typeEtatUtilisable";

    public static final String CODE_ANALYTIQUE_ORGANS_KEY = "codeAnalytiqueOrgans";



	
    public _EOCodeAnalytiqueExercice() {
        super();
    }




    public String canCode() {
        return (String)storedValueForKey(CAN_CODE_KEY);
    }
    public void setCanCode(String aValue) {
        takeStoredValueForKey(aValue, CAN_CODE_KEY);
    }

    public String canLibelle() {
        return (String)storedValueForKey(CAN_LIBELLE_KEY);
    }
    public void setCanLibelle(String aValue) {
        takeStoredValueForKey(aValue, CAN_LIBELLE_KEY);
    }




    public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytiquePere() {
        return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_PERE_KEY);
    }
    public void setCodeAnalytiquePere(org.cocktail.kava.client.metier.EOCodeAnalytique aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_PERE_KEY);
    }
	
    public void setCodeAnalytiquePereRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCodeAnalytique object = codeAnalytiquePere();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_PERE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_PERE_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtatPublic() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_PUBLIC_KEY);
    }
    public void setTypeEtatPublic(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_PUBLIC_KEY);
    }
	
    public void setTypeEtatPublicRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtatPublic();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_PUBLIC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_PUBLIC_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtatUtilisable() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_UTILISABLE_KEY);
    }
    public void setTypeEtatUtilisable(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_UTILISABLE_KEY);
    }
	
    public void setTypeEtatUtilisableRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtatUtilisable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_UTILISABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_UTILISABLE_KEY);
        }
    }




    public NSArray codeAnalytiqueOrgans() {
        return (NSArray)storedValueForKey(CODE_ANALYTIQUE_ORGANS_KEY);
    }
    public void setCodeAnalytiqueOrgans(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_ORGANS_KEY);
    }
    public void addToCodeAnalytiqueOrgans(org.cocktail.kava.client.metier.EOCodeAnalytiqueOrgan object) {
        NSMutableArray array = (NSMutableArray)codeAnalytiqueOrgans();
        willChange();
        array.addObject(object);
    }
    public void removeFromCodeAnalytiqueOrgans(org.cocktail.kava.client.metier.EOCodeAnalytiqueOrgan object) {
        NSMutableArray array = (NSMutableArray)codeAnalytiqueOrgans();
        willChange();
        array.removeObject(object);
    }
	
    public void addToCodeAnalytiqueOrgansRelationship(org.cocktail.kava.client.metier.EOCodeAnalytiqueOrgan object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
    }
    public void removeFromCodeAnalytiqueOrgansRelationship(org.cocktail.kava.client.metier.EOCodeAnalytiqueOrgan object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
    }
	


}

