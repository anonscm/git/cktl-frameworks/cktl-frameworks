
// _EOStructureUlr.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOStructureUlr.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOStructureUlr extends EOGenericRecord {

    public static final String ENTITY_NAME = "StructureUlr";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_structure_ulr";

    public static final String C_NAF_KEY = "cNaf";
    public static final String C_RNE_KEY = "cRne";
    public static final String C_STRUCTURE_KEY = "cStructure";
    public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
    public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
    public static final String LC_STRUCTURE_KEY = "lcStructure";
    public static final String LL_STRUCTURE_KEY = "llStructure";
    public static final String ORG_ORDRE_KEY = "orgOrdre";
    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
    public static final String SIREN_KEY = "siren";
    public static final String SIRET_KEY = "siret";
    public static final String TEM_VALIDE_KEY = "temValide";

    public static final String C_NAF_COLKEY = "C_NAF";
    public static final String C_RNE_COLKEY = "C_RNE";
    public static final String C_STRUCTURE_COLKEY = "c_structure";
    public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
    public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
    public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
    public static final String LL_STRUCTURE_COLKEY = "ll_structure";
    public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
    public static final String SIREN_COLKEY = "SIREN";
    public static final String SIRET_COLKEY = "SIRET";
    public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

    public static final String PERSONNE_KEY = "personne";

    public static final String REPART_STRUCTURES_KEY = "repartStructures";
    public static final String REPART_TYPE_GROUPES_KEY = "repartTypeGroupes";
    public static final String SECRETARIATS_KEY = "secretariats";



	
    public _EOStructureUlr() {
        super();
    }




    public String cNaf() {
        return (String)storedValueForKey(C_NAF_KEY);
    }
    public void setCNaf(String aValue) {
        takeStoredValueForKey(aValue, C_NAF_KEY);
    }

    public String cRne() {
        return (String)storedValueForKey(C_RNE_KEY);
    }
    public void setCRne(String aValue) {
        takeStoredValueForKey(aValue, C_RNE_KEY);
    }

    public String cStructure() {
        return (String)storedValueForKey(C_STRUCTURE_KEY);
    }
    public void setCStructure(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_KEY);
    }

    public String cStructurePere() {
        return (String)storedValueForKey(C_STRUCTURE_PERE_KEY);
    }
    public void setCStructurePere(String aValue) {
        takeStoredValueForKey(aValue, C_STRUCTURE_PERE_KEY);
    }

    public String cTypeStructure() {
        return (String)storedValueForKey(C_TYPE_STRUCTURE_KEY);
    }
    public void setCTypeStructure(String aValue) {
        takeStoredValueForKey(aValue, C_TYPE_STRUCTURE_KEY);
    }

    public String lcStructure() {
        return (String)storedValueForKey(LC_STRUCTURE_KEY);
    }
    public void setLcStructure(String aValue) {
        takeStoredValueForKey(aValue, LC_STRUCTURE_KEY);
    }

    public String llStructure() {
        return (String)storedValueForKey(LL_STRUCTURE_KEY);
    }
    public void setLlStructure(String aValue) {
        takeStoredValueForKey(aValue, LL_STRUCTURE_KEY);
    }

    public Number orgOrdre() {
        return (Number)storedValueForKey(ORG_ORDRE_KEY);
    }
    public void setOrgOrdre(Number aValue) {
        takeStoredValueForKey(aValue, ORG_ORDRE_KEY);
    }

    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public String siren() {
        return (String)storedValueForKey(SIREN_KEY);
    }
    public void setSiren(String aValue) {
        takeStoredValueForKey(aValue, SIREN_KEY);
    }

    public String siret() {
        return (String)storedValueForKey(SIRET_KEY);
    }
    public void setSiret(String aValue) {
        takeStoredValueForKey(aValue, SIRET_KEY);
    }

    public String temValide() {
        return (String)storedValueForKey(TEM_VALIDE_KEY);
    }
    public void setTemValide(String aValue) {
        takeStoredValueForKey(aValue, TEM_VALIDE_KEY);
    }




    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }




    public NSArray repartStructures() {
        return (NSArray)storedValueForKey(REPART_STRUCTURES_KEY);
    }
    public void setRepartStructures(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, REPART_STRUCTURES_KEY);
    }
    public void addToRepartStructures(org.cocktail.kava.client.metier.EORepartStructure object) {
        NSMutableArray array = (NSMutableArray)repartStructures();
        willChange();
        array.addObject(object);
    }
    public void removeFromRepartStructures(org.cocktail.kava.client.metier.EORepartStructure object) {
        NSMutableArray array = (NSMutableArray)repartStructures();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
        addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    }
    public void removeFromRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    }
	

    public NSArray repartTypeGroupes() {
        return (NSArray)storedValueForKey(REPART_TYPE_GROUPES_KEY);
    }
    public void setRepartTypeGroupes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, REPART_TYPE_GROUPES_KEY);
    }
    public void addToRepartTypeGroupes(org.cocktail.kava.client.metier.EORepartTypeGroupe object) {
        NSMutableArray array = (NSMutableArray)repartTypeGroupes();
        willChange();
        array.addObject(object);
    }
    public void removeFromRepartTypeGroupes(org.cocktail.kava.client.metier.EORepartTypeGroupe object) {
        NSMutableArray array = (NSMutableArray)repartTypeGroupes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRepartTypeGroupesRelationship(org.cocktail.kava.client.metier.EORepartTypeGroupe object) {
        addObjectToBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
    }
    public void removeFromRepartTypeGroupesRelationship(org.cocktail.kava.client.metier.EORepartTypeGroupe object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
    }
	

    public NSArray secretariats() {
        return (NSArray)storedValueForKey(SECRETARIATS_KEY);
    }
    public void setSecretariats(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, SECRETARIATS_KEY);
    }
    public void addToSecretariats(org.cocktail.kava.client.metier.EOSecretariat object) {
        NSMutableArray array = (NSMutableArray)secretariats();
        willChange();
        array.addObject(object);
    }
    public void removeFromSecretariats(org.cocktail.kava.client.metier.EOSecretariat object) {
        NSMutableArray array = (NSMutableArray)secretariats();
        willChange();
        array.removeObject(object);
    }
	
    public void addToSecretariatsRelationship(org.cocktail.kava.client.metier.EOSecretariat object) {
        addObjectToBothSidesOfRelationshipWithKey(object, SECRETARIATS_KEY);
    }
    public void removeFromSecretariatsRelationship(org.cocktail.kava.client.metier.EOSecretariat object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, SECRETARIATS_KEY);
    }
	


}

