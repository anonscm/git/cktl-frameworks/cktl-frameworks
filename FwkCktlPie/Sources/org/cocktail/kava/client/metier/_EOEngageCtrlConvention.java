
// _EOEngageCtrlConvention.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOEngageCtrlConvention.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOEngageCtrlConvention extends EOGenericRecord {

    public static final String ENTITY_NAME = "EngageCtrlConvention";

    public static final String ENTITY_TABLE_NAME = "jefy_depense.engage_ctrl_convention";

    public static final String ECON_DATE_SAISIE_KEY = "econDateSaisie";
    public static final String ECON_HT_SAISIE_KEY = "econHtSaisie";
    public static final String ECON_MONTANT_BUDGETAIRE_KEY = "econMontantBudgetaire";
    public static final String ECON_MONTANT_BUDGETAIRE_RESTE_KEY = "econMontantBudgetaireReste";
    public static final String ECON_TTC_SAISIE_KEY = "econTtcSaisie";
    public static final String ECON_TVA_SAISIE_KEY = "econTvaSaisie";
    public static final String EXE_ORDRE_KEY = "exeOrdre";

    public static final String ECON_DATE_SAISIE_COLKEY = "ECON_DATE_SAISIE";
    public static final String ECON_HT_SAISIE_COLKEY = "ECON_HT_SAISIE";
    public static final String ECON_MONTANT_BUDGETAIRE_COLKEY = "ECON_MONTANT_BUDGETAIRE";
    public static final String ECON_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ECON_MONTANT_BUDGETAIRE_RESTE";
    public static final String ECON_TTC_SAISIE_COLKEY = "ECON_TTC_SAISIE";
    public static final String ECON_TVA_SAISIE_COLKEY = "ECON_TVA_SAISIE";
    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

    public static final String CONVENTION_KEY = "convention";




	
    public _EOEngageCtrlConvention() {
        super();
    }




    public NSTimestamp econDateSaisie() {
        return (NSTimestamp)storedValueForKey(ECON_DATE_SAISIE_KEY);
    }
    public void setEconDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ECON_DATE_SAISIE_KEY);
    }

    public BigDecimal econHtSaisie() {
        return (BigDecimal)storedValueForKey(ECON_HT_SAISIE_KEY);
    }
    public void setEconHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ECON_HT_SAISIE_KEY);
    }

    public BigDecimal econMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(ECON_MONTANT_BUDGETAIRE_KEY);
    }
    public void setEconMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ECON_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal econMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(ECON_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setEconMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ECON_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public BigDecimal econTtcSaisie() {
        return (BigDecimal)storedValueForKey(ECON_TTC_SAISIE_KEY);
    }
    public void setEconTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ECON_TTC_SAISIE_KEY);
    }

    public BigDecimal econTvaSaisie() {
        return (BigDecimal)storedValueForKey(ECON_TVA_SAISIE_KEY);
    }
    public void setEconTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ECON_TVA_SAISIE_KEY);
    }

    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }




    public org.cocktail.kava.client.metier.EOConvention convention() {
        return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
    }
    public void setConvention(org.cocktail.kava.client.metier.EOConvention aValue) {
        takeStoredValueForKey(aValue, CONVENTION_KEY);
    }
	
    public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOConvention object = convention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
        }
    }





}

