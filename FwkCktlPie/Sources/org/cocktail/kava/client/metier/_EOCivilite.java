
// _EOCivilite.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCivilite.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOCivilite extends EOGenericRecord {

    public static final String ENTITY_NAME = "Civilite";

    public static final String ENTITY_TABLE_NAME = "grhum.civilite";

    public static final String C_CIVILITE_KEY = "cCivilite";
    public static final String L_CIVILITE_KEY = "lCivilite";
    public static final String SEXE_KEY = "sexe";

    public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
    public static final String L_CIVILITE_COLKEY = "L_CIVILITE";
    public static final String SEXE_COLKEY = "SEXE";





	
    public _EOCivilite() {
        super();
    }




    public String cCivilite() {
        return (String)storedValueForKey(C_CIVILITE_KEY);
    }
    public void setCCivilite(String aValue) {
        takeStoredValueForKey(aValue, C_CIVILITE_KEY);
    }

    public String lCivilite() {
        return (String)storedValueForKey(L_CIVILITE_KEY);
    }
    public void setLCivilite(String aValue) {
        takeStoredValueForKey(aValue, L_CIVILITE_KEY);
    }

    public String sexe() {
        return (String)storedValueForKey(SEXE_KEY);
    }
    public void setSexe(String aValue) {
        takeStoredValueForKey(aValue, SEXE_KEY);
    }








}

