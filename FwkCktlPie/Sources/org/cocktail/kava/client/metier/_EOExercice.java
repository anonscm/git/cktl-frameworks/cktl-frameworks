
// _EOExercice.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOExercice.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOExercice extends EOGenericRecord {

    public static final String ENTITY_NAME = "Exercice";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_exercice";

    public static final String EXE_CLOTURE_KEY = "exeCloture";
    public static final String EXE_EXERCICE_KEY = "exeExercice";
    public static final String EXE_INVENTAIRE_KEY = "exeInventaire";
    public static final String EXE_OUVERTURE_KEY = "exeOuverture";
    public static final String EXE_STAT_KEY = "exeStat";
    public static final String EXE_STAT_ENG_KEY = "exeStatEng";
    public static final String EXE_STAT_FAC_KEY = "exeStatFac";
    public static final String EXE_STAT_LIQ_KEY = "exeStatLiq";
    public static final String EXE_STAT_REC_KEY = "exeStatRec";
    public static final String EXE_TYPE_KEY = "exeType";

    public static final String EXE_CLOTURE_COLKEY = "EXE_CLOTURE";
    public static final String EXE_EXERCICE_COLKEY = "EXE_EXERCICE";
    public static final String EXE_INVENTAIRE_COLKEY = "EXE_INVENTAIRE";
    public static final String EXE_OUVERTURE_COLKEY = "EXE_OUVERTURE";
    public static final String EXE_STAT_COLKEY = "EXE_STAT";
    public static final String EXE_STAT_ENG_COLKEY = "EXE_STAT_ENG";
    public static final String EXE_STAT_FAC_COLKEY = "EXE_STAT_FAC";
    public static final String EXE_STAT_LIQ_COLKEY = "EXE_STAT_LIQ";
    public static final String EXE_STAT_REC_COLKEY = "EXE_STAT_REC";
    public static final String EXE_TYPE_COLKEY = "EXE_TYPE";





	
    public _EOExercice() {
        super();
    }




    public NSTimestamp exeCloture() {
        return (NSTimestamp)storedValueForKey(EXE_CLOTURE_KEY);
    }
    public void setExeCloture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, EXE_CLOTURE_KEY);
    }

    public Number exeExercice() {
        return (Number)storedValueForKey(EXE_EXERCICE_KEY);
    }
    public void setExeExercice(Number aValue) {
        takeStoredValueForKey(aValue, EXE_EXERCICE_KEY);
    }

    public NSTimestamp exeInventaire() {
        return (NSTimestamp)storedValueForKey(EXE_INVENTAIRE_KEY);
    }
    public void setExeInventaire(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, EXE_INVENTAIRE_KEY);
    }

    public NSTimestamp exeOuverture() {
        return (NSTimestamp)storedValueForKey(EXE_OUVERTURE_KEY);
    }
    public void setExeOuverture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, EXE_OUVERTURE_KEY);
    }

    public String exeStat() {
        return (String)storedValueForKey(EXE_STAT_KEY);
    }
    public void setExeStat(String aValue) {
        takeStoredValueForKey(aValue, EXE_STAT_KEY);
    }

    public String exeStatEng() {
        return (String)storedValueForKey(EXE_STAT_ENG_KEY);
    }
    public void setExeStatEng(String aValue) {
        takeStoredValueForKey(aValue, EXE_STAT_ENG_KEY);
    }

    public String exeStatFac() {
        return (String)storedValueForKey(EXE_STAT_FAC_KEY);
    }
    public void setExeStatFac(String aValue) {
        takeStoredValueForKey(aValue, EXE_STAT_FAC_KEY);
    }

    public String exeStatLiq() {
        return (String)storedValueForKey(EXE_STAT_LIQ_KEY);
    }
    public void setExeStatLiq(String aValue) {
        takeStoredValueForKey(aValue, EXE_STAT_LIQ_KEY);
    }

    public String exeStatRec() {
        return (String)storedValueForKey(EXE_STAT_REC_KEY);
    }
    public void setExeStatRec(String aValue) {
        takeStoredValueForKey(aValue, EXE_STAT_REC_KEY);
    }

    public String exeType() {
        return (String)storedValueForKey(EXE_TYPE_KEY);
    }
    public void setExeType(String aValue) {
        takeStoredValueForKey(aValue, EXE_TYPE_KEY);
    }








}

