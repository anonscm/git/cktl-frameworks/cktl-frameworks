
// _EOFacture.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFacture.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFacture extends EOGenericRecord {

    public static final String ENTITY_NAME = "Facture";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.facture";

    public static final String ECHE_ID_KEY = "echeId";
    public static final String FAC_DATE_SAISIE_KEY = "facDateSaisie";
    public static final String FAC_HT_SAISIE_KEY = "facHtSaisie";
    public static final String FAC_LIB_KEY = "facLib";
    public static final String FAC_MONTANT_BUDGETAIRE_KEY = "facMontantBudgetaire";
    public static final String FAC_MONTANT_BUDGETAIRE_RESTE_KEY = "facMontantBudgetaireReste";
    public static final String FAC_NUMERO_KEY = "facNumero";
    public static final String FAC_TTC_SAISIE_KEY = "facTtcSaisie";
    public static final String FAC_TVA_SAISIE_KEY = "facTvaSaisie";

    public static final String ECHE_ID_COLKEY = "ECHE_ID";
    public static final String FAC_DATE_SAISIE_COLKEY = "FAC_DATE_SAISIE";
    public static final String FAC_HT_SAISIE_COLKEY = "FAC_HT_SAISIE";
    public static final String FAC_LIB_COLKEY = "FAC_LIB";
    public static final String FAC_MONTANT_BUDGETAIRE_COLKEY = "FAC_MONTANT_BUDGETAIRE";
    public static final String FAC_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FAC_MONTANT_BUDGETAIRE_RESTE";
    public static final String FAC_NUMERO_COLKEY = "FAC_NUMERO";
    public static final String FAC_TTC_SAISIE_COLKEY = "FAC_TTC_SAISIE";
    public static final String FAC_TVA_SAISIE_COLKEY = "FAC_TVA_SAISIE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String FOURNIS_ULR_KEY = "fournisUlr";
    public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
    public static final String ORGAN_KEY = "organ";
    public static final String PERSONNE_KEY = "personne";
    public static final String TAUX_PRORATA_KEY = "tauxProrata";
    public static final String TYPE_APPLICATION_KEY = "typeApplication";
    public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";
    public static final String TYPE_ETAT_KEY = "typeEtat";
    public static final String UTILISATEUR_KEY = "utilisateur";

    public static final String FACTURE_CTRL_ACTIONS_KEY = "factureCtrlActions";
    public static final String FACTURE_CTRL_ANALYTIQUES_KEY = "factureCtrlAnalytiques";
    public static final String FACTURE_CTRL_CONVENTIONS_KEY = "factureCtrlConventions";
    public static final String FACTURE_CTRL_PLANCOS_KEY = "factureCtrlPlancos";
    public static final String FACTURE_PAPIER_KEY = "facturePapier";
    public static final String RECETTES_KEY = "recettes";



	
    public _EOFacture() {
        super();
    }




    public Number echeId() {
        return (Number)storedValueForKey(ECHE_ID_KEY);
    }
    public void setEcheId(Number aValue) {
        takeStoredValueForKey(aValue, ECHE_ID_KEY);
    }

    public NSTimestamp facDateSaisie() {
        return (NSTimestamp)storedValueForKey(FAC_DATE_SAISIE_KEY);
    }
    public void setFacDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FAC_DATE_SAISIE_KEY);
    }

    public BigDecimal facHtSaisie() {
        return (BigDecimal)storedValueForKey(FAC_HT_SAISIE_KEY);
    }
    public void setFacHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAC_HT_SAISIE_KEY);
    }

    public String facLib() {
        return (String)storedValueForKey(FAC_LIB_KEY);
    }
    public void setFacLib(String aValue) {
        takeStoredValueForKey(aValue, FAC_LIB_KEY);
    }

    public BigDecimal facMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(FAC_MONTANT_BUDGETAIRE_KEY);
    }
    public void setFacMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAC_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal facMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(FAC_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setFacMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAC_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public Number facNumero() {
        return (Number)storedValueForKey(FAC_NUMERO_KEY);
    }
    public void setFacNumero(Number aValue) {
        takeStoredValueForKey(aValue, FAC_NUMERO_KEY);
    }

    public BigDecimal facTtcSaisie() {
        return (BigDecimal)storedValueForKey(FAC_TTC_SAISIE_KEY);
    }
    public void setFacTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAC_TTC_SAISIE_KEY);
    }

    public BigDecimal facTvaSaisie() {
        return (BigDecimal)storedValueForKey(FAC_TVA_SAISIE_KEY);
    }
    public void setFacTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FAC_TVA_SAISIE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOModeRecouvrement modeRecouvrement() {
        return (org.cocktail.kava.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
    }
    public void setModeRecouvrement(org.cocktail.kava.client.metier.EOModeRecouvrement aValue) {
        takeStoredValueForKey(aValue, MODE_RECOUVREMENT_KEY);
    }
	
    public void setModeRecouvrementRelationship(org.cocktail.kava.client.metier.EOModeRecouvrement value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOModeRecouvrement object = modeRecouvrement();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, MODE_RECOUVREMENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
        return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
    }
    public void setTauxProrata(org.cocktail.kava.client.metier.EOTauxProrata aValue) {
        takeStoredValueForKey(aValue, TAUX_PRORATA_KEY);
    }
	
    public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTauxProrata object = tauxProrata();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TAUX_PRORATA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
        return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
    }
    public void setTypeApplication(org.cocktail.kava.client.metier.EOTypeApplication aValue) {
        takeStoredValueForKey(aValue, TYPE_APPLICATION_KEY);
    }
	
    public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeApplication object = typeApplication();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_APPLICATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
    }
    public void setTypeCreditRec(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_REC_KEY);
    }
	
    public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditRec();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_REC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }




    public NSArray factureCtrlActions() {
        return (NSArray)storedValueForKey(FACTURE_CTRL_ACTIONS_KEY);
    }
    public void setFactureCtrlActions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_CTRL_ACTIONS_KEY);
    }
    public void addToFactureCtrlActions(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
        NSMutableArray array = (NSMutableArray)factureCtrlActions();
        willChange();
        array.addObject(object);
    }
    public void removeFromFactureCtrlActions(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
        NSMutableArray array = (NSMutableArray)factureCtrlActions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFactureCtrlActionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ACTIONS_KEY);
    }
    public void removeFromFactureCtrlActionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ACTIONS_KEY);
    }
	

    public NSArray factureCtrlAnalytiques() {
        return (NSArray)storedValueForKey(FACTURE_CTRL_ANALYTIQUES_KEY);
    }
    public void setFactureCtrlAnalytiques(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_CTRL_ANALYTIQUES_KEY);
    }
    public void addToFactureCtrlAnalytiques(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
        NSMutableArray array = (NSMutableArray)factureCtrlAnalytiques();
        willChange();
        array.addObject(object);
    }
    public void removeFromFactureCtrlAnalytiques(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
        NSMutableArray array = (NSMutableArray)factureCtrlAnalytiques();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFactureCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ANALYTIQUES_KEY);
    }
    public void removeFromFactureCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ANALYTIQUES_KEY);
    }
	

    public NSArray factureCtrlConventions() {
        return (NSArray)storedValueForKey(FACTURE_CTRL_CONVENTIONS_KEY);
    }
    public void setFactureCtrlConventions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_CTRL_CONVENTIONS_KEY);
    }
    public void addToFactureCtrlConventions(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
        NSMutableArray array = (NSMutableArray)factureCtrlConventions();
        willChange();
        array.addObject(object);
    }
    public void removeFromFactureCtrlConventions(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
        NSMutableArray array = (NSMutableArray)factureCtrlConventions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFactureCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_CONVENTIONS_KEY);
    }
    public void removeFromFactureCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_CONVENTIONS_KEY);
    }
	

    public NSArray factureCtrlPlancos() {
        return (NSArray)storedValueForKey(FACTURE_CTRL_PLANCOS_KEY);
    }
    public void setFactureCtrlPlancos(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_CTRL_PLANCOS_KEY);
    }
    public void addToFactureCtrlPlancos(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)factureCtrlPlancos();
        willChange();
        array.addObject(object);
    }
    public void removeFromFactureCtrlPlancos(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)factureCtrlPlancos();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFactureCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_PLANCOS_KEY);
    }
    public void removeFromFactureCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_PLANCOS_KEY);
    }
	

    public NSArray facturePapier() {
        return (NSArray)storedValueForKey(FACTURE_PAPIER_KEY);
    }
    public void setFacturePapier(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_PAPIER_KEY);
    }
    public void addToFacturePapier(org.cocktail.kava.client.metier.EOFacturePapier object) {
        NSMutableArray array = (NSMutableArray)facturePapier();
        willChange();
        array.addObject(object);
    }
    public void removeFromFacturePapier(org.cocktail.kava.client.metier.EOFacturePapier object) {
        NSMutableArray array = (NSMutableArray)facturePapier();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFacturePapierRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_KEY);
    }
    public void removeFromFacturePapierRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_KEY);
    }
	

    public NSArray recettes() {
        return (NSArray)storedValueForKey(RECETTES_KEY);
    }
    public void setRecettes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTES_KEY);
    }
    public void addToRecettes(org.cocktail.kava.client.metier.EORecette object) {
        NSMutableArray array = (NSMutableArray)recettes();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecettes(org.cocktail.kava.client.metier.EORecette object) {
        NSMutableArray array = (NSMutableArray)recettes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    }
    public void removeFromRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    }
	


}

