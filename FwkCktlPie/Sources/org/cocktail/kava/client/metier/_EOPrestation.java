
// _EOPrestation.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPrestation.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOPrestation extends EOGenericRecord {

    public static final String ENTITY_NAME = "Prestation";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation";

    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
    public static final String PREST_APPLY_TVA_KEY = "prestApplyTva";
    public static final String PREST_COMMENTAIRE_CLIENT_KEY = "prestCommentaireClient";
    public static final String PREST_COMMENTAIRE_PREST_KEY = "prestCommentairePrest";
    public static final String PREST_DATE_KEY = "prestDate";
    public static final String PREST_DATE_CLOTURE_KEY = "prestDateCloture";
    public static final String PREST_DATE_FACTURATION_KEY = "prestDateFacturation";
    public static final String PREST_DATE_VALIDE_CLIENT_KEY = "prestDateValideClient";
    public static final String PREST_DATE_VALIDE_PREST_KEY = "prestDateValidePrest";
    public static final String PREST_LIBELLE_KEY = "prestLibelle";
    public static final String PREST_NUMERO_KEY = "prestNumero";
    public static final String PREST_REMISE_GLOBALE_KEY = "prestRemiseGlobale";
    public static final String PREST_TOTAL_HT_KEY = "prestTotalHt";
    public static final String PREST_TOTAL_TTC_KEY = "prestTotalTtc";
    public static final String PREST_TOTAL_TVA_KEY = "prestTotalTva";

    public static final String PREST_APPLY_TVA_COLKEY = "PREST_APPLY_TVA";
    public static final String PREST_COMMENTAIRE_CLIENT_COLKEY = "PREST_COMMENTAIRE_CLIENT";
    public static final String PREST_COMMENTAIRE_PREST_COLKEY = "PREST_COMMENTAIRE_PREST";
    public static final String PREST_DATE_COLKEY = "PREST_DATE";
    public static final String PREST_DATE_CLOTURE_COLKEY = "PREST_DATE_CLOTURE";
    public static final String PREST_DATE_FACTURATION_COLKEY = "PREST_DATE_FACTURATION";
    public static final String PREST_DATE_VALIDE_CLIENT_COLKEY = "PREST_DATE_VALIDE_CLIENT";
    public static final String PREST_DATE_VALIDE_PREST_COLKEY = "PREST_DATE_VALIDE_PREST";
    public static final String PREST_LIBELLE_COLKEY = "PREST_LIBELLE";
    public static final String PREST_NUMERO_COLKEY = "PREST_NUMERO";
    public static final String PREST_REMISE_GLOBALE_COLKEY = "PREST_REMISE_GLOBALE";
    public static final String PREST_TOTAL_HT_COLKEY = "PREST_TOTAL_HT";
    public static final String PREST_TOTAL_TTC_COLKEY = "PREST_TOTAL_TTC";
    public static final String PREST_TOTAL_TVA_COLKEY = "PREST_TOTAL_TVA";

    public static final String CATALOGUE_KEY = "catalogue";
    public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
    public static final String CONVENTION_KEY = "convention";
    public static final String EXERCICE_KEY = "exercice";
    public static final String FOURNIS_ULR_KEY = "fournisUlr";
    public static final String FOURNIS_ULR_PREST_KEY = "fournisUlrPrest";
    public static final String INDIVIDU_ULR_KEY = "individuUlr";
    public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";
    public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
    public static final String ORGAN_KEY = "organ";
    public static final String PERSONNE_KEY = "personne";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String PRESTATION_BUDGET_CLIENT_KEY = "prestationBudgetClient";
    public static final String TAUX_PRORATA_KEY = "tauxProrata";
    public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";
    public static final String TYPE_ETAT_KEY = "typeEtat";
    public static final String TYPE_PUBLIC_KEY = "typePublic";
    public static final String UTILISATEUR_KEY = "utilisateur";

    public static final String FACTURE_PAPIERS_KEY = "facturePapiers";
    public static final String PRESTATION_BASCULES_KEY = "prestationBascules";
    public static final String PRESTATION_LIGNES_KEY = "prestationLignes";



	
    public _EOPrestation() {
        super();
    }




    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public String prestApplyTva() {
        return (String)storedValueForKey(PREST_APPLY_TVA_KEY);
    }
    public void setPrestApplyTva(String aValue) {
        takeStoredValueForKey(aValue, PREST_APPLY_TVA_KEY);
    }

    public String prestCommentaireClient() {
        return (String)storedValueForKey(PREST_COMMENTAIRE_CLIENT_KEY);
    }
    public void setPrestCommentaireClient(String aValue) {
        takeStoredValueForKey(aValue, PREST_COMMENTAIRE_CLIENT_KEY);
    }

    public String prestCommentairePrest() {
        return (String)storedValueForKey(PREST_COMMENTAIRE_PREST_KEY);
    }
    public void setPrestCommentairePrest(String aValue) {
        takeStoredValueForKey(aValue, PREST_COMMENTAIRE_PREST_KEY);
    }

    public NSTimestamp prestDate() {
        return (NSTimestamp)storedValueForKey(PREST_DATE_KEY);
    }
    public void setPrestDate(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, PREST_DATE_KEY);
    }

    public NSTimestamp prestDateCloture() {
        return (NSTimestamp)storedValueForKey(PREST_DATE_CLOTURE_KEY);
    }
    public void setPrestDateCloture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, PREST_DATE_CLOTURE_KEY);
    }

    public NSTimestamp prestDateFacturation() {
        return (NSTimestamp)storedValueForKey(PREST_DATE_FACTURATION_KEY);
    }
    public void setPrestDateFacturation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, PREST_DATE_FACTURATION_KEY);
    }

    public NSTimestamp prestDateValideClient() {
        return (NSTimestamp)storedValueForKey(PREST_DATE_VALIDE_CLIENT_KEY);
    }
    public void setPrestDateValideClient(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, PREST_DATE_VALIDE_CLIENT_KEY);
    }

    public NSTimestamp prestDateValidePrest() {
        return (NSTimestamp)storedValueForKey(PREST_DATE_VALIDE_PREST_KEY);
    }
    public void setPrestDateValidePrest(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, PREST_DATE_VALIDE_PREST_KEY);
    }

    public String prestLibelle() {
        return (String)storedValueForKey(PREST_LIBELLE_KEY);
    }
    public void setPrestLibelle(String aValue) {
        takeStoredValueForKey(aValue, PREST_LIBELLE_KEY);
    }

    public Number prestNumero() {
        return (Number)storedValueForKey(PREST_NUMERO_KEY);
    }
    public void setPrestNumero(Number aValue) {
        takeStoredValueForKey(aValue, PREST_NUMERO_KEY);
    }

    public BigDecimal prestRemiseGlobale() {
        return (BigDecimal)storedValueForKey(PREST_REMISE_GLOBALE_KEY);
    }
    public void setPrestRemiseGlobale(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PREST_REMISE_GLOBALE_KEY);
    }

    public BigDecimal prestTotalHt() {
        return (BigDecimal)storedValueForKey(PREST_TOTAL_HT_KEY);
    }
    public void setPrestTotalHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PREST_TOTAL_HT_KEY);
    }

    public BigDecimal prestTotalTtc() {
        return (BigDecimal)storedValueForKey(PREST_TOTAL_TTC_KEY);
    }
    public void setPrestTotalTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PREST_TOTAL_TTC_KEY);
    }

    public BigDecimal prestTotalTva() {
        return (BigDecimal)storedValueForKey(PREST_TOTAL_TVA_KEY);
    }
    public void setPrestTotalTva(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PREST_TOTAL_TVA_KEY);
    }




    public org.cocktail.kava.client.metier.EOCatalogue catalogue() {
        return (org.cocktail.kava.client.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
    }
    public void setCatalogue(org.cocktail.kava.client.metier.EOCatalogue aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_KEY);
    }
	
    public void setCatalogueRelationship(org.cocktail.kava.client.metier.EOCatalogue value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCatalogue object = catalogue();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
        return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
    }
    public void setCodeAnalytique(org.cocktail.kava.client.metier.EOCodeAnalytique aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_KEY);
    }
	
    public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCodeAnalytique object = codeAnalytique();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOConvention convention() {
        return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
    }
    public void setConvention(org.cocktail.kava.client.metier.EOConvention aValue) {
        takeStoredValueForKey(aValue, CONVENTION_KEY);
    }
	
    public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOConvention object = convention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlrPrest() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_PREST_KEY);
    }
    public void setFournisUlrPrest(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_PREST_KEY);
    }
	
    public void setFournisUlrPrestRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlrPrest();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_PREST_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_PREST_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOIndividuUlr individuUlr() {
        return (org.cocktail.kava.client.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
    }
    public void setIndividuUlr(org.cocktail.kava.client.metier.EOIndividuUlr aValue) {
        takeStoredValueForKey(aValue, INDIVIDU_ULR_KEY);
    }
	
    public void setIndividuUlrRelationship(org.cocktail.kava.client.metier.EOIndividuUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOIndividuUlr object = individuUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, INDIVIDU_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
    }
    public void setLolfNomenclatureRecette(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
	
    public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureRecette object = lolfNomenclatureRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOModeRecouvrement modeRecouvrement() {
        return (org.cocktail.kava.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
    }
    public void setModeRecouvrement(org.cocktail.kava.client.metier.EOModeRecouvrement aValue) {
        takeStoredValueForKey(aValue, MODE_RECOUVREMENT_KEY);
    }
	
    public void setModeRecouvrementRelationship(org.cocktail.kava.client.metier.EOModeRecouvrement value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOModeRecouvrement object = modeRecouvrement();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, MODE_RECOUVREMENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPrestationBudgetClient prestationBudgetClient() {
        return (org.cocktail.kava.client.metier.EOPrestationBudgetClient)storedValueForKey(PRESTATION_BUDGET_CLIENT_KEY);
    }
    public void setPrestationBudgetClient(org.cocktail.kava.client.metier.EOPrestationBudgetClient aValue) {
        takeStoredValueForKey(aValue, PRESTATION_BUDGET_CLIENT_KEY);
    }
	
    public void setPrestationBudgetClientRelationship(org.cocktail.kava.client.metier.EOPrestationBudgetClient value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestationBudgetClient object = prestationBudgetClient();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_BUDGET_CLIENT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_BUDGET_CLIENT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
        return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
    }
    public void setTauxProrata(org.cocktail.kava.client.metier.EOTauxProrata aValue) {
        takeStoredValueForKey(aValue, TAUX_PRORATA_KEY);
    }
	
    public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTauxProrata object = tauxProrata();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TAUX_PRORATA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
    }
    public void setTypeCreditRec(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_REC_KEY);
    }
	
    public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditRec();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_REC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypePublic typePublic() {
        return (org.cocktail.kava.client.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
    }
    public void setTypePublic(org.cocktail.kava.client.metier.EOTypePublic aValue) {
        takeStoredValueForKey(aValue, TYPE_PUBLIC_KEY);
    }
	
    public void setTypePublicRelationship(org.cocktail.kava.client.metier.EOTypePublic value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypePublic object = typePublic();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_PUBLIC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }




    public NSArray facturePapiers() {
        return (NSArray)storedValueForKey(FACTURE_PAPIERS_KEY);
    }
    public void setFacturePapiers(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_PAPIERS_KEY);
    }
    public void addToFacturePapiers(org.cocktail.kava.client.metier.EOFacturePapier object) {
        NSMutableArray array = (NSMutableArray)facturePapiers();
        willChange();
        array.addObject(object);
    }
    public void removeFromFacturePapiers(org.cocktail.kava.client.metier.EOFacturePapier object) {
        NSMutableArray array = (NSMutableArray)facturePapiers();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFacturePapiersRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIERS_KEY);
    }
    public void removeFromFacturePapiersRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIERS_KEY);
    }
	

    public NSArray prestationBascules() {
        return (NSArray)storedValueForKey(PRESTATION_BASCULES_KEY);
    }
    public void setPrestationBascules(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PRESTATION_BASCULES_KEY);
    }
    public void addToPrestationBascules(org.cocktail.kava.client.metier.EOPrestationBascule object) {
        NSMutableArray array = (NSMutableArray)prestationBascules();
        willChange();
        array.addObject(object);
    }
    public void removeFromPrestationBascules(org.cocktail.kava.client.metier.EOPrestationBascule object) {
        NSMutableArray array = (NSMutableArray)prestationBascules();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPrestationBasculesRelationship(org.cocktail.kava.client.metier.EOPrestationBascule object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PRESTATION_BASCULES_KEY);
    }
    public void removeFromPrestationBasculesRelationship(org.cocktail.kava.client.metier.EOPrestationBascule object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_BASCULES_KEY);
    }
	

    public NSArray prestationLignes() {
        return (NSArray)storedValueForKey(PRESTATION_LIGNES_KEY);
    }
    public void setPrestationLignes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PRESTATION_LIGNES_KEY);
    }
    public void addToPrestationLignes(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        NSMutableArray array = (NSMutableArray)prestationLignes();
        willChange();
        array.addObject(object);
    }
    public void removeFromPrestationLignes(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        NSMutableArray array = (NSMutableArray)prestationLignes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
    }
    public void removeFromPrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
    }
	


}

