
// _EOConventionLimitative.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOConventionLimitative.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOConventionLimitative extends EOGenericRecord {

    public static final String ENTITY_NAME = "ConventionLimitative";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_convention_limitative";

    public static final String CL_MONTANT_KEY = "clMontant";

    public static final String CL_MONTANT_COLKEY = "CL_MONTANT";

    public static final String EXERCICE_KEY = "exercice";
    public static final String ORGAN_KEY = "organ";
    public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";
    public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";




	
    public _EOConventionLimitative() {
        super();
    }




    public BigDecimal clMontant() {
        return (BigDecimal)storedValueForKey(CL_MONTANT_KEY);
    }
    public void setClMontant(BigDecimal aValue) {
        takeStoredValueForKey(aValue, CL_MONTANT_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditDep() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
    }
    public void setTypeCreditDep(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_DEP_KEY);
    }
	
    public void setTypeCreditDepRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditDep();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_DEP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
    }
    public void setTypeCreditRec(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_REC_KEY);
    }
	
    public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditRec();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_REC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
        }
    }





}

