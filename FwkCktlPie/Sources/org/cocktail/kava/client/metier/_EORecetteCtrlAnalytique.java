
// _EORecetteCtrlAnalytique.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecetteCtrlAnalytique.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecetteCtrlAnalytique extends EOGenericRecord {

    public static final String ENTITY_NAME = "RecetteCtrlAnalytique";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_analytique";

    public static final String RANA_DATE_SAISIE_KEY = "ranaDateSaisie";
    public static final String RANA_HT_SAISIE_KEY = "ranaHtSaisie";
    public static final String RANA_MONTANT_BUDGETAIRE_KEY = "ranaMontantBudgetaire";
    public static final String RANA_TTC_SAISIE_KEY = "ranaTtcSaisie";
    public static final String RANA_TVA_SAISIE_KEY = "ranaTvaSaisie";
    public static final String REC_ID_KEY = "recId";

    public static final String RANA_DATE_SAISIE_COLKEY = "RANA_DATE_SAISIE";
    public static final String RANA_HT_SAISIE_COLKEY = "RANA_HT_SAISIE";
    public static final String RANA_MONTANT_BUDGETAIRE_COLKEY = "RANA_MONTANT_BUDGETAIRE";
    public static final String RANA_TTC_SAISIE_COLKEY = "RANA_TTC_SAISIE";
    public static final String RANA_TVA_SAISIE_COLKEY = "RANA_TVA_SAISIE";
    public static final String REC_ID_COLKEY = "REC_ID";

    public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
    public static final String EXERCICE_KEY = "exercice";




	
    public _EORecetteCtrlAnalytique() {
        super();
    }




    public NSTimestamp ranaDateSaisie() {
        return (NSTimestamp)storedValueForKey(RANA_DATE_SAISIE_KEY);
    }
    public void setRanaDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RANA_DATE_SAISIE_KEY);
    }

    public BigDecimal ranaHtSaisie() {
        return (BigDecimal)storedValueForKey(RANA_HT_SAISIE_KEY);
    }
    public void setRanaHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RANA_HT_SAISIE_KEY);
    }

    public BigDecimal ranaMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(RANA_MONTANT_BUDGETAIRE_KEY);
    }
    public void setRanaMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RANA_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal ranaTtcSaisie() {
        return (BigDecimal)storedValueForKey(RANA_TTC_SAISIE_KEY);
    }
    public void setRanaTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RANA_TTC_SAISIE_KEY);
    }

    public BigDecimal ranaTvaSaisie() {
        return (BigDecimal)storedValueForKey(RANA_TVA_SAISIE_KEY);
    }
    public void setRanaTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RANA_TVA_SAISIE_KEY);
    }

    public Number recId() {
        return (Number)storedValueForKey(REC_ID_KEY);
    }
    public void setRecId(Number aValue) {
        takeStoredValueForKey(aValue, REC_ID_KEY);
    }




    public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
        return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
    }
    public void setCodeAnalytique(org.cocktail.kava.client.metier.EOCodeAnalytique aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_KEY);
    }
	
    public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCodeAnalytique object = codeAnalytique();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }





}

