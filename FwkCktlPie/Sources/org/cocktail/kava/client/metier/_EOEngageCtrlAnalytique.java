
// _EOEngageCtrlAnalytique.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOEngageCtrlAnalytique.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOEngageCtrlAnalytique extends EOGenericRecord {

    public static final String ENTITY_NAME = "EngageCtrlAnalytique";

    public static final String ENTITY_TABLE_NAME = "jefy_depense.engage_ctrl_analytique";

    public static final String EANA_DATE_SAISIE_KEY = "eanaDateSaisie";
    public static final String EANA_HT_SAISIE_KEY = "eanaHtSaisie";
    public static final String EANA_ID_KEY = "eanaId";
    public static final String EANA_MONTANT_BUDGETAIRE_KEY = "eanaMontantBudgetaire";
    public static final String EANA_MONTANT_BUDGETAIRE_RESTE_KEY = "eanaMontantBudgetaireReste";
    public static final String EANA_TTC_SAISIE_KEY = "eanaTtcSaisie";
    public static final String EANA_TVA_SAISIE_KEY = "eanaTvaSaisie";
    public static final String EXE_ORDRE_KEY = "exeOrdre";

    public static final String EANA_DATE_SAISIE_COLKEY = "EANA_DATE_SAISIE";
    public static final String EANA_HT_SAISIE_COLKEY = "EANA_HT_SAISIE";
    public static final String EANA_ID_COLKEY = "EANA_ID";
    public static final String EANA_MONTANT_BUDGETAIRE_COLKEY = "EANA_MONTANT_BUDGETAIRE";
    public static final String EANA_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EANA_MONTANT_BUDGETAIRE_RESTE";
    public static final String EANA_TTC_SAISIE_COLKEY = "EANA_TTC_SAISIE";
    public static final String EANA_TVA_SAISIE_COLKEY = "EANA_TVA_SAISIE";
    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

    public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";




	
    public _EOEngageCtrlAnalytique() {
        super();
    }




    public NSTimestamp eanaDateSaisie() {
        return (NSTimestamp)storedValueForKey(EANA_DATE_SAISIE_KEY);
    }
    public void setEanaDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, EANA_DATE_SAISIE_KEY);
    }

    public BigDecimal eanaHtSaisie() {
        return (BigDecimal)storedValueForKey(EANA_HT_SAISIE_KEY);
    }
    public void setEanaHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EANA_HT_SAISIE_KEY);
    }

    public Number eanaId() {
        return (Number)storedValueForKey(EANA_ID_KEY);
    }
    public void setEanaId(Number aValue) {
        takeStoredValueForKey(aValue, EANA_ID_KEY);
    }

    public BigDecimal eanaMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(EANA_MONTANT_BUDGETAIRE_KEY);
    }
    public void setEanaMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EANA_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal eanaMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(EANA_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setEanaMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EANA_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public BigDecimal eanaTtcSaisie() {
        return (BigDecimal)storedValueForKey(EANA_TTC_SAISIE_KEY);
    }
    public void setEanaTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EANA_TTC_SAISIE_KEY);
    }

    public BigDecimal eanaTvaSaisie() {
        return (BigDecimal)storedValueForKey(EANA_TVA_SAISIE_KEY);
    }
    public void setEanaTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EANA_TVA_SAISIE_KEY);
    }

    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }




    public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
        return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
    }
    public void setCodeAnalytique(org.cocktail.kava.client.metier.EOCodeAnalytique aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_KEY);
    }
	
    public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCodeAnalytique object = codeAnalytique();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
        }
    }





}

