
// _EORecetteCtrlPlancoCtp.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecetteCtrlPlancoCtp.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecetteCtrlPlancoCtp extends EOGenericRecord {

    public static final String ENTITY_NAME = "RecetteCtrlPlancoCtp";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_planco_ctp";

    public static final String RPCO_ID_KEY = "rpcoId";
    public static final String RPCOCTP_DATE_SAISIE_KEY = "rpcoctpDateSaisie";
    public static final String RPCOCTP_TTC_SAISIE_KEY = "rpcoctpTtcSaisie";

    public static final String RPCO_ID_COLKEY = "RPCO_ID";
    public static final String RPCOCTP_DATE_SAISIE_COLKEY = "RPCOCTP_DATE_SAISIE";
    public static final String RPCOCTP_TTC_SAISIE_COLKEY = "RPCOCTP_TTC_SAISIE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String GESTION_KEY = "gestion";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";




	
    public _EORecetteCtrlPlancoCtp() {
        super();
    }




    public Number rpcoId() {
        return (Number)storedValueForKey(RPCO_ID_KEY);
    }
    public void setRpcoId(Number aValue) {
        takeStoredValueForKey(aValue, RPCO_ID_KEY);
    }

    public NSTimestamp rpcoctpDateSaisie() {
        return (NSTimestamp)storedValueForKey(RPCOCTP_DATE_SAISIE_KEY);
    }
    public void setRpcoctpDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RPCOCTP_DATE_SAISIE_KEY);
    }

    public BigDecimal rpcoctpTtcSaisie() {
        return (BigDecimal)storedValueForKey(RPCOCTP_TTC_SAISIE_KEY);
    }
    public void setRpcoctpTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RPCOCTP_TTC_SAISIE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOGestion gestion() {
        return (org.cocktail.kava.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
    }
    public void setGestion(org.cocktail.kava.client.metier.EOGestion aValue) {
        takeStoredValueForKey(aValue, GESTION_KEY);
    }
	
    public void setGestionRelationship(org.cocktail.kava.client.metier.EOGestion value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOGestion object = gestion();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, GESTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }





}

