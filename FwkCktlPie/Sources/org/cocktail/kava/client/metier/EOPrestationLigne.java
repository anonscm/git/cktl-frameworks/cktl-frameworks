// EOPrestationLigne.java
// 
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import org.cocktail.kava.client.factory.FactoryPrestationLigne;
import org.cocktail.kava.client.finder.FinderCatalogueArticle;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.finder.FinderTypeEtat;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPrestationLigne extends _EOPrestationLigne {

	public static final String PRLIG_TOTAL_TVA_KEY = "prligTotalTva";
	public static final String PRLIG_TOTAL_RESTE_TVA_KEY = "prligTotalResteTva";

	public EOPrestationLigne() {
		super();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise
	 *            HT
	 */
	public void setPrligArtHt(BigDecimal aValue) {
		super.setPrligArtHt(aValue);
		updateTotalHt();
		updateTotalResteHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total
	 *            remise TTC
	 */
	public void setPrligArtTtc(BigDecimal aValue) {
		super.setPrligArtTtc(aValue);
		updateTotalTtc();
		updateTotalResteTtc();
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite, la quantite restante, recalcule les
	 *            totaux remises HT et TTC, et met a jour les options/remises,
	 *            et fait le cafe, le menage et les devoirs des enfants...
	 *            pfff....
	 */
	public void setPrligQuantite(BigDecimal aValue) {
		super.setPrligQuantite(aValue);
		super.setPrligQuantiteReste(aValue);
		updateTotaux();

		// mise a jour des remises quantitatives possibles
		if (catalogueArticle() == null || catalogueArticle().article() == null || catalogueArticle().article().articles() == null
				|| catalogueArticle().article().articles().count() == 0) {
			return;
		}
		updateOptionsRemises();
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite restante et recalcule les totaux
	 *            restants remises HT et TTC
	 */
	public void setPrligQuantiteReste(BigDecimal aValue) {
		super.setPrligQuantiteReste(aValue);
		updateTotalResteHt();
		updateTotalResteTtc();

		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				((EOPrestationLigne) prestationLignes().objectAtIndex(i)).setPrligQuantiteReste(prligQuantiteReste());
			}
		}
	}

	private void updateOptionsRemises() {
		if (catalogueArticle() == null) {
			return;
		}
		// recherche des remises quantitatives valides pour le type de public
		NSArray remises = FinderCatalogueArticle.find(editingContext(), catalogueArticle(), catalogueArticle().article().articlePrestation()
				.typePublic(), FinderTypeEtat.typeEtatValide(editingContext()), FinderTypeArticle.typeArticleRemise(editingContext()), prligQuantite());

		// suppression de celles qui ne doivent plus y etre, mise a jour de la
		// quantite des autres
		if (prestationLignes() != null) {
			int i = 0;
			while (prestationLignes().count() > i) {
				EOPrestationLigne pl = (EOPrestationLigne) prestationLignes().objectAtIndex(i);
				if (pl.catalogueArticle() != null && pl.catalogueArticle().article().typeArticle().equals(FinderTypeArticle.typeArticleRemise(editingContext()))) {
					if (remises != null && remises.containsObject(pl.catalogueArticle())) {
						pl.setPrligQuantite(prligQuantite());
						i++;
					}
					else {
						removeFromPrestationLignesRelationship(pl);
						editingContext().deleteObject(pl);
					}
				}
				else {
					pl.setPrligQuantite(prligQuantite());
					i++;
				}
			}
		}

		// ajout des nouvelles qui n'y sont pas encore
		if (remises != null && remises.count() > 0) {
			for (int i = 0; i < remises.count(); i++) {
				EOCatalogueArticle catalogueArticle = (EOCatalogueArticle) remises.objectAtIndex(i);
				if (!((NSArray) prestationLignes().valueForKey(EOPrestationLigne.CATALOGUE_ARTICLE_KEY)).containsObject(catalogueArticle)) {
					FactoryPrestationLigne.newObject(editingContext(), this, catalogueArticle);
				}
			}
		}
	}

	public BigDecimal prligTotalTva() {
		if (prligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (prligTotalHt() == null) {
			return prligTotalTtc();
		}
		return prligTotalTtc().subtract(prligTotalHt());
	}

	public BigDecimal prligTotalResteTva() {
		if (prligTotalResteTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (prligTotalResteHt() == null) {
			return prligTotalResteTtc();
		}
		return prligTotalResteTtc().subtract(prligTotalResteHt());
	}

	public void updateTotaux() {
		updateTotalHt();
		updateTotalTtc();
		updateTotalResteHt();
		updateTotalResteTtc();
	}

	/**
	 * met a jour le total ht de la ligne, fonction du prix unitaire, de la
	 * quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ht a 0 ou plus
	 */
	private void updateTotalHt() {
		if (prligArtHt() != null && prligQuantite() != null) {
			setPrligTotalHt(applyRemise(prligArtHt().multiply(prligQuantite()).setScale(prligArtHt().scale() > 0 ? 2 : 0, BigDecimal.ROUND_HALF_UP)));
		}
		else {
			setPrligTotalHt(null);
		}
	}

	/**
	 * met a jour le total ttc de la ligne, fonction du prix unitaire, de la
	 * quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ttc a 0 ou plus
	 */
	private void updateTotalTtc() {
		if (prligArtTtc() != null && prligQuantite() != null) {
			BigDecimal htQte=prligArtHt().multiply(prligQuantite());
			BigDecimal tauxTva=new BigDecimal(1.0);

			if (catalogueArticle()!=null && catalogueArticle().tva()!=null)
				tauxTva=catalogueArticle().tva().tvaTaux().divide(new BigDecimal(100.0), BigDecimal.ROUND_HALF_UP, 3).add(new BigDecimal(1.0));
			else
				if (prligArtHt().floatValue()!=0.0)
					tauxTva=prligArtTtc().divide(prligArtHt(), BigDecimal.ROUND_HALF_UP, 3).setScale(3, BigDecimal.ROUND_HALF_UP);

			setPrligTotalTtc(applyRemise(htQte.multiply(tauxTva)).setScale(prligArtHt().scale() > 0 ? 2 : 0, BigDecimal.ROUND_HALF_UP));
		}
		else {
			setPrligTotalTtc(null);
		}
	}

	/**
	 * met a jour le total reste ht de la ligne, fonction du prix unitaire, de
	 * la quantite restante et de la remise eventuelle, scale 0 ou 2 selon scale
	 * prix unitaire ht a 0 ou plus
	 */
	private void updateTotalResteHt() {
		if (prligArtHt() != null && prligQuantiteReste() != null) {
			setPrligTotalResteHt(applyRemise(prligArtHt().multiply(prligQuantiteReste()).setScale(prligArtHt().scale() > 0 ? 2 : 0,
					BigDecimal.ROUND_HALF_UP)));
		}
		else {
			setPrligTotalResteHt(null);
		}
	}

	/**
	 * met a jour le total rest ttc de la ligne, fonction du prix unitaire, de
	 * la quantite restante et de la remise eventuelle, scale 0 ou 2 selon scale
	 * prix unitaire ttc a 0 ou plus
	 */
	private void updateTotalResteTtc() {
		if (prligArtTtc() != null && prligQuantiteReste() != null) {
			if (prligQuantiteReste()==null)
				setPrligQuantiteReste(prligQuantite());
			BigDecimal htQte=prligArtHt().multiply(prligQuantiteReste());
			BigDecimal tauxTva=new BigDecimal(1.0);

			if (catalogueArticle()!=null && catalogueArticle().tva()!=null)
				tauxTva=catalogueArticle().tva().tvaTaux().divide(new BigDecimal(100.0), BigDecimal.ROUND_HALF_UP, 3).add(new BigDecimal(1.0));
			else
				if (prligArtHt().floatValue()!=0.0)
					tauxTva=prligArtTtc().divide(prligArtHt(), BigDecimal.ROUND_HALF_UP, 3).setScale(3, BigDecimal.ROUND_HALF_UP);

			setPrligTotalResteTtc(applyRemise(htQte.multiply(tauxTva)).setScale(prligArtHt().scale() > 0 ? 2 : 0, BigDecimal.ROUND_HALF_UP));
		}
		else {
			setPrligTotalResteTtc(null);
		}
	}

	/**
	 * @param b
	 * @return le montant eventuellement remise, scale conserve
	 */
	private BigDecimal applyRemise(BigDecimal b) {
		if (b == null) {
			return new BigDecimal(0.0);
		}
		if (prestation() == null || prestation().prestRemiseGlobale() == null) {
			return b;
		}
		BigDecimal bd = new BigDecimal(100.0);
		BigDecimal toMultiply = prestation().prestRemiseGlobale().divide(bd, BigDecimal.ROUND_HALF_UP);
		BigDecimal toSubtract = b.multiply(toMultiply);
		return b.subtract(toSubtract).setScale(b.scale(), BigDecimal.ROUND_HALF_UP);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (typeArticle() == null) {
			throw new ValidationException("Il faut un type article pour les lignes du panier!");
		}
		if (prestation() == null) {
			throw new ValidationException("Il faut une prestation pour les lignes du panier!");
		}
		// if (catalogueArticle() == null) {
		// throw new ValidationException("Il faut un article pour les lignes du
		// panier!");
		// }
		if (prligDate() == null) {
			throw new ValidationException("Il faut une date de creation pour les lignes du panier!");
		}
		if (prligDescription() == null) {
			throw new ValidationException("Il faut un libelle pour les lignes du panier!");
		}
		if (prligArtHt() == null) {
			throw new ValidationException("Il faut un prix HT pour les lignes du panier!");
		}
		if (prligArtTtc() == null) {
			throw new ValidationException("Il faut un prix TTC pour les lignes du panier!");
		}
		if (prligQuantite() == null) {
			throw new ValidationException("Il faut une quantite pour les lignes du panier!");
		}
		if (prligQuantiteReste() == null) {
			throw new ValidationException("Il faut une quantite restante pour les lignes du panier!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
