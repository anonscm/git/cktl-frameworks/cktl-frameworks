// EOFacturePapier.java
// 
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.finder.FinderTypeApplication;

import com.webobjects.foundation.NSValidation;

public class EOFacturePapier extends _EOFacturePapier {

	public static final String	FAP_TOTAL_HT_LIVE_KEY	= "fapTotalHtLive";
	public static final String	FAP_TOTAL_TTC_LIVE_KEY	= "fapTotalTtcLive";
	public static final String	FAP_TOTAL_TVA_LIVE_KEY	= "fapTotalTvaLive";

	public static final String	PRIMARY_KEY_KEY			= "fapId";

	public EOFacturePapier() {
		super();
	}

	public void setFapRemiseGlobale(BigDecimal aValue) {
		if (aValue != null && aValue.equals(fapRemiseGlobale())) {
			return;
		}
		super.setFapRemiseGlobale(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				((EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i)).updateTotaux();
			}
		}
	}

	/**
	 * @param aValue
	 *            (O/N) applique ou non la tva, et met a jour les lignes de la facture papier, et recalcule le total ttc
	 */
	public void setFapApplyTva(String aValue) {
		if (aValue != null && aValue.equals(fapApplyTva())) {
			return;
		}
		super.setFapApplyTva(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				EOFacturePapierLigne flig = (EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i);
				if ("N".equalsIgnoreCase(fapApplyTva())) {
					flig.setTvaRelationship(null);
					flig.setFligArtTtc(flig.fligArtHt());
				}
				else {
					flig.setTvaRelationship(flig.tvaInitial());
					flig.setFligArtTtc(flig.fligArtTtcInitial());
				}
			}
		}
	}

	public BigDecimal fapTotalHtLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) facturePapierLignes().valueForKey("@sum." + EOFacturePapierLigne.FLIG_TOTAL_HT_KEY);
	}

	public BigDecimal fapTotalTtcLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) facturePapierLignes().valueForKey("@sum." + EOFacturePapierLigne.FLIG_TOTAL_TTC_KEY);
	}

	public BigDecimal fapTotalTvaLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) facturePapierLignes().valueForKey("@sum." + EOFacturePapierLigne.FLIG_TOTAL_TVA_KEY);
	}

	public void updateTotaux() {
		setFapTotalHt(fapTotalHtLive());
		setFapTotalTva(fapTotalTvaLive());
		setFapTotalTtc(fapTotalTtcLive());
	}

	/**
	 * verifie si les informations obligatoires sont la pour valider cote client (peu importe son etape actuelle de validation)
	 * 
	 * @return
	 */
	public boolean isValidableClient() {
		if (!typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext()))) {
			return true;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire (peu importe son etape actuelle de validation)
	 * 
	 * @return
	 */
	public boolean isValidablePrest() {
		if (organ() == null || tauxProrata() == null || typeCreditRec() == null || lolfNomenclatureRecette() == null || planComptable() == null
				|| modeRecouvrement() == null) {
			return false;
		}
		return true;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la facture papier!");
		}
		if (typePublic() == null) {
			throw new ValidationException("Il faut un type de client pour la facture papier!");
		}
		if (typeEtat() == null) {
			throw new ValidationException("Il faut un etat pour la facture papier!");
		}
		if (personne() == null) {
			throw new ValidationException("Il faut un client pour la facture papier!");
		}
		if (utilisateur() == null) {
			throw new ValidationException("Il faut un agent (utilisateur) pour la facture papier!");
		}
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			throw new ValidationException("Il faut au moins une ligne dans le panier pour la facture papier!");
		}
		if (fapDate() == null) {
			throw new ValidationException("Il faut une date de creation pour la facture papier!");
		}
		if (fapLib() == null) {
			throw new ValidationException("Il faut un libelle pour la facture papier!");
		}
		if (fapRemiseGlobale() != null) {
			if (fapRemiseGlobale().doubleValue() < 0.0 || fapRemiseGlobale().doubleValue() > 100.0) {
				throw new ValidationException("Le pourcentage de remise globale doit etre entre 0 et 100 :-) !");
			}
		}
		if (ribfourUlr() == null) {
			if (modeRecouvrement() != null && modeRecouvrement().isEcheancier()) {
				throw new ValidationException("Il faut un rib pour ce mode de recouvrement!");
			}
		}
		if (echeId() != null) {
			if (modeRecouvrement() == null || !modeRecouvrement().isEcheancier()) {
				throw new ValidationException(
						"Un echeancier existe pour cette facture, supprimer d'abord l'echeancier avant de changer de mode de recouvrement !");
			}
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (fapNumero() == null) {
			try {
				setFapNumero(ServerProxy.getNumerotation(editingContext(), exercice(), null, "FACTURE_PAPIER"));
			}
			catch (Exception e) {
				throw new ValidationException("Probleme pour numeroter la facture papier : " + e);
			}
		}
		updateTotaux();
	}

}
