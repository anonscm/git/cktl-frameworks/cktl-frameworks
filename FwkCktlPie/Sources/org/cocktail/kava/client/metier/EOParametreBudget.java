

// EOParametreBudget.java
// 
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOParametreBudget extends _EOParametreBudget {

    public static final String PARAM_NIVEAU_SAISIE="NIVEAU_SAISIE";
    public static final String VALUE_NIVEAU_SAISIE_UB="UB";
    public static final String VALUE_NIVEAU_SAISIE_CR="CR";

    public EOParametreBudget() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
