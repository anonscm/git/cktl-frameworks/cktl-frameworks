
// _EOLolfNomenclatureDepense.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOLolfNomenclatureDepense.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOLolfNomenclatureDepense extends EOGenericRecord {

    public static final String ENTITY_NAME = "LolfNomenclatureDepense";

    public static final String ENTITY_TABLE_NAME = "jefy_admin.Lolf_Nomenclature_Depense";

    public static final String LOLF_ABREVIATION_KEY = "lolfAbreviation";
    public static final String LOLF_CODE_KEY = "lolfCode";
    public static final String LOLF_FERMETURE_KEY = "lolfFermeture";
    public static final String LOLF_LIBELLE_KEY = "lolfLibelle";
    public static final String LOLF_NIVEAU_KEY = "lolfNiveau";
    public static final String LOLF_ORDRE_AFFICHAGE_KEY = "lolfOrdreAffichage";
    public static final String LOLF_OUVERTURE_KEY = "lolfOuverture";
    public static final String LOLF_PERE_KEY = "lolfPere";

    public static final String LOLF_ABREVIATION_COLKEY = "LOLF_ABREVIATION";
    public static final String LOLF_CODE_COLKEY = "LOLF_CODE";
    public static final String LOLF_FERMETURE_COLKEY = "LOLF_FERMETURE";
    public static final String LOLF_LIBELLE_COLKEY = "LOLF_LIBELLE";
    public static final String LOLF_NIVEAU_COLKEY = "LOLF_NIVEAU";
    public static final String LOLF_ORDRE_AFFICHAGE_COLKEY = "LOLF_ORDRE_AFFICHAGE";
    public static final String LOLF_OUVERTURE_COLKEY = "LOLF_OUVERTURE";
    public static final String LOLF_PERE_COLKEY = "LOLF_PERE";

    public static final String LOLF_NOMENCLATURE_TYPE_KEY = "lolfNomenclatureType";
    public static final String TYPE_ETAT_KEY = "typeEtat";

    public static final String ORGAN_ACTION_DEPS_KEY = "organActionDeps";



	
    public _EOLolfNomenclatureDepense() {
        super();
    }




    public String lolfAbreviation() {
        return (String)storedValueForKey(LOLF_ABREVIATION_KEY);
    }
    public void setLolfAbreviation(String aValue) {
        takeStoredValueForKey(aValue, LOLF_ABREVIATION_KEY);
    }

    public String lolfCode() {
        return (String)storedValueForKey(LOLF_CODE_KEY);
    }
    public void setLolfCode(String aValue) {
        takeStoredValueForKey(aValue, LOLF_CODE_KEY);
    }

    public NSTimestamp lolfFermeture() {
        return (NSTimestamp)storedValueForKey(LOLF_FERMETURE_KEY);
    }
    public void setLolfFermeture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, LOLF_FERMETURE_KEY);
    }

    public String lolfLibelle() {
        return (String)storedValueForKey(LOLF_LIBELLE_KEY);
    }
    public void setLolfLibelle(String aValue) {
        takeStoredValueForKey(aValue, LOLF_LIBELLE_KEY);
    }

    public Number lolfNiveau() {
        return (Number)storedValueForKey(LOLF_NIVEAU_KEY);
    }
    public void setLolfNiveau(Number aValue) {
        takeStoredValueForKey(aValue, LOLF_NIVEAU_KEY);
    }

    public Number lolfOrdreAffichage() {
        return (Number)storedValueForKey(LOLF_ORDRE_AFFICHAGE_KEY);
    }
    public void setLolfOrdreAffichage(Number aValue) {
        takeStoredValueForKey(aValue, LOLF_ORDRE_AFFICHAGE_KEY);
    }

    public NSTimestamp lolfOuverture() {
        return (NSTimestamp)storedValueForKey(LOLF_OUVERTURE_KEY);
    }
    public void setLolfOuverture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, LOLF_OUVERTURE_KEY);
    }

    public Number lolfPere() {
        return (Number)storedValueForKey(LOLF_PERE_KEY);
    }
    public void setLolfPere(Number aValue) {
        takeStoredValueForKey(aValue, LOLF_PERE_KEY);
    }




    public org.cocktail.kava.client.metier.EOLolfNomenclatureType lolfNomenclatureType() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureType)storedValueForKey(LOLF_NOMENCLATURE_TYPE_KEY);
    }
    public void setLolfNomenclatureType(org.cocktail.kava.client.metier.EOLolfNomenclatureType aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_TYPE_KEY);
    }
	
    public void setLolfNomenclatureTypeRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureType value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureType object = lolfNomenclatureType();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_TYPE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_TYPE_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }




    public NSArray organActionDeps() {
        return (NSArray)storedValueForKey(ORGAN_ACTION_DEPS_KEY);
    }
    public void setOrganActionDeps(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ORGAN_ACTION_DEPS_KEY);
    }
    public void addToOrganActionDeps(org.cocktail.kava.client.metier.EOOrganActionDep object) {
        NSMutableArray array = (NSMutableArray)organActionDeps();
        willChange();
        array.addObject(object);
    }
    public void removeFromOrganActionDeps(org.cocktail.kava.client.metier.EOOrganActionDep object) {
        NSMutableArray array = (NSMutableArray)organActionDeps();
        willChange();
        array.removeObject(object);
    }
	
    public void addToOrganActionDepsRelationship(org.cocktail.kava.client.metier.EOOrganActionDep object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_ACTION_DEPS_KEY);
    }
    public void removeFromOrganActionDepsRelationship(org.cocktail.kava.client.metier.EOOrganActionDep object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_ACTION_DEPS_KEY);
    }
	


}

