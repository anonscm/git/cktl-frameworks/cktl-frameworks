
// _EOFacturePapierLigne.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFacturePapierLigne.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFacturePapierLigne extends EOGenericRecord {

    public static final String ENTITY_NAME = "FacturePapierLigne";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_papier_ligne";

    public static final String FLIG_ART_HT_KEY = "fligArtHt";
    public static final String FLIG_ART_TTC_KEY = "fligArtTtc";
    public static final String FLIG_ART_TTC_INITIAL_KEY = "fligArtTtcInitial";
    public static final String FLIG_DATE_KEY = "fligDate";
    public static final String FLIG_DESCRIPTION_KEY = "fligDescription";
    public static final String FLIG_QUANTITE_KEY = "fligQuantite";
    public static final String FLIG_REFERENCE_KEY = "fligReference";
    public static final String FLIG_TOTAL_HT_KEY = "fligTotalHt";
    public static final String FLIG_TOTAL_TTC_KEY = "fligTotalTtc";

    public static final String FLIG_ART_HT_COLKEY = "FLIG_ART_HT";
    public static final String FLIG_ART_TTC_COLKEY = "FLIG_ART_TTC";
    public static final String FLIG_ART_TTC_INITIAL_COLKEY = "FLIG_ART_TTC_INITIAL";
    public static final String FLIG_DATE_COLKEY = "FLIG_DATE";
    public static final String FLIG_DESCRIPTION_COLKEY = "FLIG_DESCRIPTION";
    public static final String FLIG_QUANTITE_COLKEY = "FLIG_QUANTITE";
    public static final String FLIG_REFERENCE_COLKEY = "FLIG_REFERENCE";
    public static final String FLIG_TOTAL_HT_COLKEY = "FLIG_TOTAL_HT";
    public static final String FLIG_TOTAL_TTC_COLKEY = "FLIG_TOTAL_TTC";

    public static final String FACTURE_PAPIER_KEY = "facturePapier";
    public static final String FACTURE_PAPIER_LIGNE_PERE_KEY = "facturePapierLignePere";
    public static final String PRESTATION_LIGNE_KEY = "prestationLigne";
    public static final String TVA_KEY = "tva";
    public static final String TVA_INITIAL_KEY = "tvaInitial";
    public static final String TYPE_ARTICLE_KEY = "typeArticle";

    public static final String FACTURE_PAPIER_LIGNES_KEY = "facturePapierLignes";



	
    public _EOFacturePapierLigne() {
        super();
    }




    public BigDecimal fligArtHt() {
        return (BigDecimal)storedValueForKey(FLIG_ART_HT_KEY);
    }
    public void setFligArtHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FLIG_ART_HT_KEY);
    }

    public BigDecimal fligArtTtc() {
        return (BigDecimal)storedValueForKey(FLIG_ART_TTC_KEY);
    }
    public void setFligArtTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FLIG_ART_TTC_KEY);
    }

    public BigDecimal fligArtTtcInitial() {
        return (BigDecimal)storedValueForKey(FLIG_ART_TTC_INITIAL_KEY);
    }
    public void setFligArtTtcInitial(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FLIG_ART_TTC_INITIAL_KEY);
    }

    public NSTimestamp fligDate() {
        return (NSTimestamp)storedValueForKey(FLIG_DATE_KEY);
    }
    public void setFligDate(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FLIG_DATE_KEY);
    }

    public String fligDescription() {
        return (String)storedValueForKey(FLIG_DESCRIPTION_KEY);
    }
    public void setFligDescription(String aValue) {
        takeStoredValueForKey(aValue, FLIG_DESCRIPTION_KEY);
    }

    public BigDecimal fligQuantite() {
        return (BigDecimal)storedValueForKey(FLIG_QUANTITE_KEY);
    }
    public void setFligQuantite(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FLIG_QUANTITE_KEY);
    }

    public String fligReference() {
        return (String)storedValueForKey(FLIG_REFERENCE_KEY);
    }
    public void setFligReference(String aValue) {
        takeStoredValueForKey(aValue, FLIG_REFERENCE_KEY);
    }

    public BigDecimal fligTotalHt() {
        return (BigDecimal)storedValueForKey(FLIG_TOTAL_HT_KEY);
    }
    public void setFligTotalHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FLIG_TOTAL_HT_KEY);
    }

    public BigDecimal fligTotalTtc() {
        return (BigDecimal)storedValueForKey(FLIG_TOTAL_TTC_KEY);
    }
    public void setFligTotalTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FLIG_TOTAL_TTC_KEY);
    }




    public org.cocktail.kava.client.metier.EOFacturePapier facturePapier() {
        return (org.cocktail.kava.client.metier.EOFacturePapier)storedValueForKey(FACTURE_PAPIER_KEY);
    }
    public void setFacturePapier(org.cocktail.kava.client.metier.EOFacturePapier aValue) {
        takeStoredValueForKey(aValue, FACTURE_PAPIER_KEY);
    }
	
    public void setFacturePapierRelationship(org.cocktail.kava.client.metier.EOFacturePapier value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFacturePapier object = facturePapier();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_PAPIER_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFacturePapierLigne facturePapierLignePere() {
        return (org.cocktail.kava.client.metier.EOFacturePapierLigne)storedValueForKey(FACTURE_PAPIER_LIGNE_PERE_KEY);
    }
    public void setFacturePapierLignePere(org.cocktail.kava.client.metier.EOFacturePapierLigne aValue) {
        takeStoredValueForKey(aValue, FACTURE_PAPIER_LIGNE_PERE_KEY);
    }
	
    public void setFacturePapierLignePereRelationship(org.cocktail.kava.client.metier.EOFacturePapierLigne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFacturePapierLigne object = facturePapierLignePere();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNE_PERE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_PAPIER_LIGNE_PERE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPrestationLigne prestationLigne() {
        return (org.cocktail.kava.client.metier.EOPrestationLigne)storedValueForKey(PRESTATION_LIGNE_KEY);
    }
    public void setPrestationLigne(org.cocktail.kava.client.metier.EOPrestationLigne aValue) {
        takeStoredValueForKey(aValue, PRESTATION_LIGNE_KEY);
    }
	
    public void setPrestationLigneRelationship(org.cocktail.kava.client.metier.EOPrestationLigne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestationLigne object = prestationLigne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_LIGNE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTva tva() {
        return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_KEY);
    }
    public void setTva(org.cocktail.kava.client.metier.EOTva aValue) {
        takeStoredValueForKey(aValue, TVA_KEY);
    }
	
    public void setTvaRelationship(org.cocktail.kava.client.metier.EOTva value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTva object = tva();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TVA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTva tvaInitial() {
        return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_INITIAL_KEY);
    }
    public void setTvaInitial(org.cocktail.kava.client.metier.EOTva aValue) {
        takeStoredValueForKey(aValue, TVA_INITIAL_KEY);
    }
	
    public void setTvaInitialRelationship(org.cocktail.kava.client.metier.EOTva value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTva object = tvaInitial();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TVA_INITIAL_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TVA_INITIAL_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeArticle typeArticle() {
        return (org.cocktail.kava.client.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
    }
    public void setTypeArticle(org.cocktail.kava.client.metier.EOTypeArticle aValue) {
        takeStoredValueForKey(aValue, TYPE_ARTICLE_KEY);
    }
	
    public void setTypeArticleRelationship(org.cocktail.kava.client.metier.EOTypeArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeArticle object = typeArticle();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ARTICLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
        }
    }




    public NSArray facturePapierLignes() {
        return (NSArray)storedValueForKey(FACTURE_PAPIER_LIGNES_KEY);
    }
    public void setFacturePapierLignes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, FACTURE_PAPIER_LIGNES_KEY);
    }
    public void addToFacturePapierLignes(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        NSMutableArray array = (NSMutableArray)facturePapierLignes();
        willChange();
        array.addObject(object);
    }
    public void removeFromFacturePapierLignes(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        NSMutableArray array = (NSMutableArray)facturePapierLignes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToFacturePapierLignesRelationship(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
    }
    public void removeFromFacturePapierLignesRelationship(org.cocktail.kava.client.metier.EOFacturePapierLigne object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
    }
	


}

