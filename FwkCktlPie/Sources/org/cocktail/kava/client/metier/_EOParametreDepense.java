
// _EOParametreDepense.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOParametreDepense.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOParametreDepense extends EOGenericRecord {

    public static final String ENTITY_NAME = "ParametreDepense";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_parametre_depense";

    public static final String PAR_DESCRIPTION_KEY = "parDescription";
    public static final String PAR_KEY_KEY = "parKey";
    public static final String PAR_VALUE_KEY = "parValue";

    public static final String PAR_DESCRIPTION_COLKEY = "PAR_DESCRIPTION";
    public static final String PAR_KEY_COLKEY = "PAR_KEY";
    public static final String PAR_VALUE_COLKEY = "PAR_VALUE";

    public static final String EXERCICE_KEY = "exercice";




	
    public _EOParametreDepense() {
        super();
    }




    public String parDescription() {
        return (String)storedValueForKey(PAR_DESCRIPTION_KEY);
    }
    public void setParDescription(String aValue) {
        takeStoredValueForKey(aValue, PAR_DESCRIPTION_KEY);
    }

    public String parKey() {
        return (String)storedValueForKey(PAR_KEY_KEY);
    }
    public void setParKey(String aValue) {
        takeStoredValueForKey(aValue, PAR_KEY_KEY);
    }

    public String parValue() {
        return (String)storedValueForKey(PAR_VALUE_KEY);
    }
    public void setParValue(String aValue) {
        takeStoredValueForKey(aValue, PAR_VALUE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }





}

