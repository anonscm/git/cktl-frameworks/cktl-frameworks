
// _EOCommandesPourPi.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCommandesPourPi.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOCommandesPourPi extends EOGenericRecord {

    public static final String ENTITY_NAME = "CommandesPourPi";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_commandes_pour_pi";

    public static final String COMM_DATE_CREATION_KEY = "commDateCreation";
    public static final String COMM_LIBELLE_KEY = "commLibelle";
    public static final String COMM_NUMERO_KEY = "commNumero";
    public static final String COMM_REFERENCE_KEY = "commReference";
    public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
    public static final String ENG_ID_KEY = "engId";
    public static final String ENG_NUMERO_KEY = "engNumero";
    public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
    public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";
    public static final String PCO_NUM_KEY = "pcoNum";

    public static final String COMM_DATE_CREATION_COLKEY = "COMM_DATE_CREATION";
    public static final String COMM_LIBELLE_COLKEY = "COMM_LIBELLE";
    public static final String COMM_NUMERO_COLKEY = "COMM_NUMERO";
    public static final String COMM_REFERENCE_COLKEY = "COMM_REFERENCE";
    public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
    public static final String ENG_ID_COLKEY = "ENG_ID";
    public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
    public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
    public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";
    public static final String PCO_NUM_COLKEY = "PCO_NUM";

    public static final String EXERCICE_KEY = "exercice";
    public static final String FOURNIS_ULR_KEY = "fournisUlr";
    public static final String ORGAN_KEY = "organ";
    public static final String UTILISATEUR_KEY = "utilisateur";




	
    public _EOCommandesPourPi() {
        super();
    }




    public NSTimestamp commDateCreation() {
        return (NSTimestamp)storedValueForKey(COMM_DATE_CREATION_KEY);
    }
    public void setCommDateCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, COMM_DATE_CREATION_KEY);
    }

    public String commLibelle() {
        return (String)storedValueForKey(COMM_LIBELLE_KEY);
    }
    public void setCommLibelle(String aValue) {
        takeStoredValueForKey(aValue, COMM_LIBELLE_KEY);
    }

    public Number commNumero() {
        return (Number)storedValueForKey(COMM_NUMERO_KEY);
    }
    public void setCommNumero(Number aValue) {
        takeStoredValueForKey(aValue, COMM_NUMERO_KEY);
    }

    public String commReference() {
        return (String)storedValueForKey(COMM_REFERENCE_KEY);
    }
    public void setCommReference(String aValue) {
        takeStoredValueForKey(aValue, COMM_REFERENCE_KEY);
    }

    public BigDecimal engHtSaisie() {
        return (BigDecimal)storedValueForKey(ENG_HT_SAISIE_KEY);
    }
    public void setEngHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_HT_SAISIE_KEY);
    }

    public Number engId() {
        return (Number)storedValueForKey(ENG_ID_KEY);
    }
    public void setEngId(Number aValue) {
        takeStoredValueForKey(aValue, ENG_ID_KEY);
    }

    public Number engNumero() {
        return (Number)storedValueForKey(ENG_NUMERO_KEY);
    }
    public void setEngNumero(Number aValue) {
        takeStoredValueForKey(aValue, ENG_NUMERO_KEY);
    }

    public BigDecimal engTtcSaisie() {
        return (BigDecimal)storedValueForKey(ENG_TTC_SAISIE_KEY);
    }
    public void setEngTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_TTC_SAISIE_KEY);
    }

    public BigDecimal engTvaSaisie() {
        return (BigDecimal)storedValueForKey(ENG_TVA_SAISIE_KEY);
    }
    public void setEngTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_TVA_SAISIE_KEY);
    }

    public String pcoNum() {
        return (String)storedValueForKey(PCO_NUM_KEY);
    }
    public void setPcoNum(String aValue) {
        takeStoredValueForKey(aValue, PCO_NUM_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
        return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
    }
    public void setFournisUlr(org.cocktail.kava.client.metier.EOFournisUlr aValue) {
        takeStoredValueForKey(aValue, FOURNIS_ULR_KEY);
    }
	
    public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFournisUlr object = fournisUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }





}

