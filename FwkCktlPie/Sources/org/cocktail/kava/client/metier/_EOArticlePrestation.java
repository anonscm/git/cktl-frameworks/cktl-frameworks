
// _EOArticlePrestation.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOArticlePrestation.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOArticlePrestation extends EOGenericRecord {

    public static final String ENTITY_NAME = "ArticlePrestation";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.article_prestation";

    public static final String ARTP_CFC_DATE_DEBUT_KEY = "artpCfcDateDebut";
    public static final String ARTP_CFC_DATE_FIN_KEY = "artpCfcDateFin";
    public static final String ARTP_CFC_DUREE_KEY = "artpCfcDuree";
    public static final String ARTP_CFC_NO_DECLARATION_KEY = "artpCfcNoDeclaration";
    public static final String ARTP_CFC_RESPONSABLE_KEY = "artpCfcResponsable";
    public static final String ARTP_INVISIBLE_WEB_KEY = "artpInvisibleWeb";
    public static final String ARTP_QTE_DISPO_KEY = "artpQteDispo";
    public static final String ARTP_QTE_MAX_KEY = "artpQteMax";
    public static final String ARTP_QTE_MAX_PAR_CMD_KEY = "artpQteMaxParCmd";
    public static final String ARTP_QTE_MIN_KEY = "artpQteMin";

    public static final String ARTP_CFC_DATE_DEBUT_COLKEY = "ARTP_CFC_DATE_DEBUT";
    public static final String ARTP_CFC_DATE_FIN_COLKEY = "ARTP_CFC_DATE_FIN";
    public static final String ARTP_CFC_DUREE_COLKEY = "ARTP_CFC_DUREE";
    public static final String ARTP_CFC_NO_DECLARATION_COLKEY = "ARTP_CFC_NO_DECLARATION";
    public static final String ARTP_CFC_RESPONSABLE_COLKEY = "ARTP_CFC_RESPONSABLE";
    public static final String ARTP_INVISIBLE_WEB_COLKEY = "ARTP_INVISIBLE_WEB";
    public static final String ARTP_QTE_DISPO_COLKEY = "ARTP_QTE_DISPO";
    public static final String ARTP_QTE_MAX_COLKEY = "ARTP_QTE_MAX";
    public static final String ARTP_QTE_MAX_PAR_CMD_COLKEY = "ARTP_QTE_MAX_PAR_CMD";
    public static final String ARTP_QTE_MIN_COLKEY = "ARTP_QTE_MIN";

    public static final String PLAN_COMPTABLE_DEPENSE_KEY = "planComptableDepense";
    public static final String PLAN_COMPTABLE_RECETTE_KEY = "planComptableRecette";
    public static final String TYPE_PUBLIC_KEY = "typePublic";




	
    public _EOArticlePrestation() {
        super();
    }




    public NSTimestamp artpCfcDateDebut() {
        return (NSTimestamp)storedValueForKey(ARTP_CFC_DATE_DEBUT_KEY);
    }
    public void setArtpCfcDateDebut(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ARTP_CFC_DATE_DEBUT_KEY);
    }

    public NSTimestamp artpCfcDateFin() {
        return (NSTimestamp)storedValueForKey(ARTP_CFC_DATE_FIN_KEY);
    }
    public void setArtpCfcDateFin(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ARTP_CFC_DATE_FIN_KEY);
    }

    public Number artpCfcDuree() {
        return (Number)storedValueForKey(ARTP_CFC_DUREE_KEY);
    }
    public void setArtpCfcDuree(Number aValue) {
        takeStoredValueForKey(aValue, ARTP_CFC_DUREE_KEY);
    }

    public String artpCfcNoDeclaration() {
        return (String)storedValueForKey(ARTP_CFC_NO_DECLARATION_KEY);
    }
    public void setArtpCfcNoDeclaration(String aValue) {
        takeStoredValueForKey(aValue, ARTP_CFC_NO_DECLARATION_KEY);
    }

    public String artpCfcResponsable() {
        return (String)storedValueForKey(ARTP_CFC_RESPONSABLE_KEY);
    }
    public void setArtpCfcResponsable(String aValue) {
        takeStoredValueForKey(aValue, ARTP_CFC_RESPONSABLE_KEY);
    }

    public String artpInvisibleWeb() {
        return (String)storedValueForKey(ARTP_INVISIBLE_WEB_KEY);
    }
    public void setArtpInvisibleWeb(String aValue) {
        takeStoredValueForKey(aValue, ARTP_INVISIBLE_WEB_KEY);
    }

    public Number artpQteDispo() {
        return (Number)storedValueForKey(ARTP_QTE_DISPO_KEY);
    }
    public void setArtpQteDispo(Number aValue) {
        takeStoredValueForKey(aValue, ARTP_QTE_DISPO_KEY);
    }

    public BigDecimal artpQteMax() {
        return (BigDecimal)storedValueForKey(ARTP_QTE_MAX_KEY);
    }
    public void setArtpQteMax(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ARTP_QTE_MAX_KEY);
    }

    public BigDecimal artpQteMaxParCmd() {
        return (BigDecimal)storedValueForKey(ARTP_QTE_MAX_PAR_CMD_KEY);
    }
    public void setArtpQteMaxParCmd(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ARTP_QTE_MAX_PAR_CMD_KEY);
    }

    public BigDecimal artpQteMin() {
        return (BigDecimal)storedValueForKey(ARTP_QTE_MIN_KEY);
    }
    public void setArtpQteMin(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ARTP_QTE_MIN_KEY);
    }




    public org.cocktail.kava.client.metier.EOPlanComptable planComptableDepense() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_DEPENSE_KEY);
    }
    public void setPlanComptableDepense(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_DEPENSE_KEY);
    }
	
    public void setPlanComptableDepenseRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableDepense();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_DEPENSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_DEPENSE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableRecette() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_RECETTE_KEY);
    }
    public void setPlanComptableRecette(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_RECETTE_KEY);
    }
	
    public void setPlanComptableRecetteRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_RECETTE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypePublic typePublic() {
        return (org.cocktail.kava.client.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
    }
    public void setTypePublic(org.cocktail.kava.client.metier.EOTypePublic aValue) {
        takeStoredValueForKey(aValue, TYPE_PUBLIC_KEY);
    }
	
    public void setTypePublicRelationship(org.cocktail.kava.client.metier.EOTypePublic value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypePublic object = typePublic();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_PUBLIC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
        }
    }





}

