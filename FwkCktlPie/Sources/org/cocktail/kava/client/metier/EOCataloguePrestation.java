// EOCataloguePrestation.java
// 
package org.cocktail.kava.client.metier;

import org.cocktail.kava.client.ServerProxy;

import com.webobjects.foundation.NSValidation;

public class EOCataloguePrestation extends _EOCataloguePrestation {

	public EOCataloguePrestation() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (catPublieWeb() == null) {
			throw new ValidationException("Il faut choisir de publier ou non le catalogue sur le web!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (catNumero() == null) {
			try {
				setCatNumero(ServerProxy.getNumerotation(editingContext(), null, null, "CATALOGUE"));
			}
			catch (Exception e) {
				throw new ValidationException("Probleme pour numeroter le catalogue : " + e);
			}
		}
	}

}
