
// _EORecetteCtrlAction.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecetteCtrlAction.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecetteCtrlAction extends EOGenericRecord {

    public static final String ENTITY_NAME = "RecetteCtrlAction";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_action";

    public static final String RACT_DATE_SAISIE_KEY = "ractDateSaisie";
    public static final String RACT_HT_SAISIE_KEY = "ractHtSaisie";
    public static final String RACT_MONTANT_BUDGETAIRE_KEY = "ractMontantBudgetaire";
    public static final String RACT_TTC_SAISIE_KEY = "ractTtcSaisie";
    public static final String RACT_TVA_SAISIE_KEY = "ractTvaSaisie";
    public static final String REC_ID_KEY = "recId";

    public static final String RACT_DATE_SAISIE_COLKEY = "RACT_DATE_SAISIE";
    public static final String RACT_HT_SAISIE_COLKEY = "RACT_HT_SAISIE";
    public static final String RACT_MONTANT_BUDGETAIRE_COLKEY = "RACT_MONTANT_BUDGETAIRE";
    public static final String RACT_TTC_SAISIE_COLKEY = "RACT_TTC_SAISIE";
    public static final String RACT_TVA_SAISIE_COLKEY = "RACT_TVA_SAISIE";
    public static final String REC_ID_COLKEY = "REC_ID";

    public static final String EXERCICE_KEY = "exercice";
    public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";




	
    public _EORecetteCtrlAction() {
        super();
    }




    public NSTimestamp ractDateSaisie() {
        return (NSTimestamp)storedValueForKey(RACT_DATE_SAISIE_KEY);
    }
    public void setRactDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, RACT_DATE_SAISIE_KEY);
    }

    public BigDecimal ractHtSaisie() {
        return (BigDecimal)storedValueForKey(RACT_HT_SAISIE_KEY);
    }
    public void setRactHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RACT_HT_SAISIE_KEY);
    }

    public BigDecimal ractMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(RACT_MONTANT_BUDGETAIRE_KEY);
    }
    public void setRactMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RACT_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal ractTtcSaisie() {
        return (BigDecimal)storedValueForKey(RACT_TTC_SAISIE_KEY);
    }
    public void setRactTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RACT_TTC_SAISIE_KEY);
    }

    public BigDecimal ractTvaSaisie() {
        return (BigDecimal)storedValueForKey(RACT_TVA_SAISIE_KEY);
    }
    public void setRactTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, RACT_TVA_SAISIE_KEY);
    }

    public Number recId() {
        return (Number)storedValueForKey(REC_ID_KEY);
    }
    public void setRecId(Number aValue) {
        takeStoredValueForKey(aValue, REC_ID_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
    }
    public void setLolfNomenclatureRecette(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
	
    public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureRecette object = lolfNomenclatureRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
    }





}

