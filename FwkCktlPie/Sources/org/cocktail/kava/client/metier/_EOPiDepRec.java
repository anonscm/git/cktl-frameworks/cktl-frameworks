
// _EOPiDepRec.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPiDepRec.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOPiDepRec extends EOGenericRecord {

    public static final String ENTITY_NAME = "PiDepRec";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.pi_dep_rec";

    public static final String D_CREATION_KEY = "dCreation";

    public static final String D_CREATION_COLKEY = "D_CREATION";

    public static final String DEPENSE_BUDGET_KEY = "depenseBudget";
    public static final String PI_ENG_FAC_KEY = "piEngFac";
    public static final String RECETTE_KEY = "recette";




	
    public _EOPiDepRec() {
        super();
    }




    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }




    public org.cocktail.kava.client.metier.EODepenseBudget depenseBudget() {
        return (org.cocktail.kava.client.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_KEY);
    }
    public void setDepenseBudget(org.cocktail.kava.client.metier.EODepenseBudget aValue) {
        takeStoredValueForKey(aValue, DEPENSE_BUDGET_KEY);
    }
	
    public void setDepenseBudgetRelationship(org.cocktail.kava.client.metier.EODepenseBudget value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EODepenseBudget object = depenseBudget();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGET_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPiEngFac piEngFac() {
        return (org.cocktail.kava.client.metier.EOPiEngFac)storedValueForKey(PI_ENG_FAC_KEY);
    }
    public void setPiEngFac(org.cocktail.kava.client.metier.EOPiEngFac aValue) {
        takeStoredValueForKey(aValue, PI_ENG_FAC_KEY);
    }
	
    public void setPiEngFacRelationship(org.cocktail.kava.client.metier.EOPiEngFac value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPiEngFac object = piEngFac();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PI_ENG_FAC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PI_ENG_FAC_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORecette recette() {
        return (org.cocktail.kava.client.metier.EORecette)storedValueForKey(RECETTE_KEY);
    }
    public void setRecette(org.cocktail.kava.client.metier.EORecette aValue) {
        takeStoredValueForKey(aValue, RECETTE_KEY);
    }
	
    public void setRecetteRelationship(org.cocktail.kava.client.metier.EORecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORecette object = recette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_KEY);
        }
    }





}

