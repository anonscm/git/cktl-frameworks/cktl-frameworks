
// _EOPrestationBudgetClient.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPrestationBudgetClient.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOPrestationBudgetClient extends EOGenericRecord {

    public static final String ENTITY_NAME = "PrestationBudgetClient";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_budget_client";



    public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
    public static final String CONVENTION_KEY = "convention";
    public static final String ENGAGE_BUDGET_KEY = "engageBudget";
    public static final String LOLF_NOMENCLATURE_DEPENSE_KEY = "lolfNomenclatureDepense";
    public static final String ORGAN_KEY = "organ";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String TAUX_PRORATA_KEY = "tauxProrata";
    public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";




	
    public _EOPrestationBudgetClient() {
        super();
    }







    public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
        return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
    }
    public void setCodeAnalytique(org.cocktail.kava.client.metier.EOCodeAnalytique aValue) {
        takeStoredValueForKey(aValue, CODE_ANALYTIQUE_KEY);
    }
	
    public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCodeAnalytique object = codeAnalytique();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOConvention convention() {
        return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
    }
    public void setConvention(org.cocktail.kava.client.metier.EOConvention aValue) {
        takeStoredValueForKey(aValue, CONVENTION_KEY);
    }
	
    public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOConvention object = convention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOEngageBudget engageBudget() {
        return (org.cocktail.kava.client.metier.EOEngageBudget)storedValueForKey(ENGAGE_BUDGET_KEY);
    }
    public void setEngageBudget(org.cocktail.kava.client.metier.EOEngageBudget aValue) {
        takeStoredValueForKey(aValue, ENGAGE_BUDGET_KEY);
    }
	
    public void setEngageBudgetRelationship(org.cocktail.kava.client.metier.EOEngageBudget value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOEngageBudget object = engageBudget();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_BUDGET_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_BUDGET_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOLolfNomenclatureDepense lolfNomenclatureDepense() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureDepense)storedValueForKey(LOLF_NOMENCLATURE_DEPENSE_KEY);
    }
    public void setLolfNomenclatureDepense(org.cocktail.kava.client.metier.EOLolfNomenclatureDepense aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_DEPENSE_KEY);
    }
	
    public void setLolfNomenclatureDepenseRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureDepense value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureDepense object = lolfNomenclatureDepense();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_DEPENSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_DEPENSE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
        return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
    }
    public void setTauxProrata(org.cocktail.kava.client.metier.EOTauxProrata aValue) {
        takeStoredValueForKey(aValue, TAUX_PRORATA_KEY);
    }
	
    public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTauxProrata object = tauxProrata();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TAUX_PRORATA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditDep() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
    }
    public void setTypeCreditDep(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_DEP_KEY);
    }
	
    public void setTypeCreditDepRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditDep();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_DEP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
        }
    }





}

