
// _EORepartPersonneAdresse.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORepartPersonneAdresse.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EORepartPersonneAdresse extends EOGenericRecord {

    public static final String ENTITY_NAME = "RepartPersonneAdresse";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_repart_personne_adresse";

    public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
    public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
    public static final String ADR_BP_KEY = "adrBp";
    public static final String ADRESSE_SHORT_KEY = "adresseShort";
    public static final String BIS_TER_KEY = "bisTer";
    public static final String C_VOIE_KEY = "cVoie";
    public static final String CODE_POSTAL_KEY = "codePostal";
    public static final String CP_ETRANGER_KEY = "cpEtranger";
    public static final String E_MAIL_KEY = "eMail";
    public static final String HABITANT_CHEZ_KEY = "habitantChez";
    public static final String LC_PAYS_KEY = "lcPays";
    public static final String LL_PAYS_KEY = "llPays";
    public static final String LOCALITE_KEY = "localite";
    public static final String NO_VOIE_KEY = "noVoie";
    public static final String NOM_VOIE_KEY = "nomVoie";
    public static final String RPA_PRINCIPAL_KEY = "rpaPrincipal";
    public static final String TADR_CODE_KEY = "tadrCode";
    public static final String VILLE_KEY = "ville";

    public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
    public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
    public static final String ADR_BP_COLKEY = "ADR_BP";
    public static final String BIS_TER_COLKEY = "BIS_TER";
    public static final String C_VOIE_COLKEY = "C_VOIE";
    public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
    public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
    public static final String E_MAIL_COLKEY = "E_MAIL";
    public static final String HABITANT_CHEZ_COLKEY = "HABITANT_CHEZ";
    public static final String LC_PAYS_COLKEY = "LC_PAYS";
    public static final String LL_PAYS_COLKEY = "LL_PAYS";
    public static final String LOCALITE_COLKEY = "LOCALITE";
    public static final String NO_VOIE_COLKEY = "NO_VOIE";
    public static final String NOM_VOIE_COLKEY = "NOM_VOIE";
    public static final String RPA_PRINCIPAL_COLKEY = "RPA_PRINCIPAL";
    public static final String TADR_CODE_COLKEY = "TADR_CODE";
    public static final String VILLE_COLKEY = "VILLE";

    public static final String ADRESSE_KEY = "adresse";




	
    public _EORepartPersonneAdresse() {
        super();
    }




    public String adrAdresse1() {
        return (String)storedValueForKey(ADR_ADRESSE1_KEY);
    }
    public void setAdrAdresse1(String aValue) {
        takeStoredValueForKey(aValue, ADR_ADRESSE1_KEY);
    }

    public String adrAdresse2() {
        return (String)storedValueForKey(ADR_ADRESSE2_KEY);
    }
    public void setAdrAdresse2(String aValue) {
        takeStoredValueForKey(aValue, ADR_ADRESSE2_KEY);
    }

    public String adrBp() {
        return (String)storedValueForKey(ADR_BP_KEY);
    }
    public void setAdrBp(String aValue) {
        takeStoredValueForKey(aValue, ADR_BP_KEY);
    }

    public String adresseShort() {
        return (String)storedValueForKey(ADRESSE_SHORT_KEY);
    }
    public void setAdresseShort(String aValue) {
        takeStoredValueForKey(aValue, ADRESSE_SHORT_KEY);
    }

    public String bisTer() {
        return (String)storedValueForKey(BIS_TER_KEY);
    }
    public void setBisTer(String aValue) {
        takeStoredValueForKey(aValue, BIS_TER_KEY);
    }

    public String cVoie() {
        return (String)storedValueForKey(C_VOIE_KEY);
    }
    public void setCVoie(String aValue) {
        takeStoredValueForKey(aValue, C_VOIE_KEY);
    }

    public String codePostal() {
        return (String)storedValueForKey(CODE_POSTAL_KEY);
    }
    public void setCodePostal(String aValue) {
        takeStoredValueForKey(aValue, CODE_POSTAL_KEY);
    }

    public String cpEtranger() {
        return (String)storedValueForKey(CP_ETRANGER_KEY);
    }
    public void setCpEtranger(String aValue) {
        takeStoredValueForKey(aValue, CP_ETRANGER_KEY);
    }

    public String eMail() {
        return (String)storedValueForKey(E_MAIL_KEY);
    }
    public void setEMail(String aValue) {
        takeStoredValueForKey(aValue, E_MAIL_KEY);
    }

    public String habitantChez() {
        return (String)storedValueForKey(HABITANT_CHEZ_KEY);
    }
    public void setHabitantChez(String aValue) {
        takeStoredValueForKey(aValue, HABITANT_CHEZ_KEY);
    }

    public String lcPays() {
        return (String)storedValueForKey(LC_PAYS_KEY);
    }
    public void setLcPays(String aValue) {
        takeStoredValueForKey(aValue, LC_PAYS_KEY);
    }

    public String llPays() {
        return (String)storedValueForKey(LL_PAYS_KEY);
    }
    public void setLlPays(String aValue) {
        takeStoredValueForKey(aValue, LL_PAYS_KEY);
    }

    public String localite() {
        return (String)storedValueForKey(LOCALITE_KEY);
    }
    public void setLocalite(String aValue) {
        takeStoredValueForKey(aValue, LOCALITE_KEY);
    }

    public String noVoie() {
        return (String)storedValueForKey(NO_VOIE_KEY);
    }
    public void setNoVoie(String aValue) {
        takeStoredValueForKey(aValue, NO_VOIE_KEY);
    }

    public String nomVoie() {
        return (String)storedValueForKey(NOM_VOIE_KEY);
    }
    public void setNomVoie(String aValue) {
        takeStoredValueForKey(aValue, NOM_VOIE_KEY);
    }

    public String rpaPrincipal() {
        return (String)storedValueForKey(RPA_PRINCIPAL_KEY);
    }
    public void setRpaPrincipal(String aValue) {
        takeStoredValueForKey(aValue, RPA_PRINCIPAL_KEY);
    }

    public String tadrCode() {
        return (String)storedValueForKey(TADR_CODE_KEY);
    }
    public void setTadrCode(String aValue) {
        takeStoredValueForKey(aValue, TADR_CODE_KEY);
    }

    public String ville() {
        return (String)storedValueForKey(VILLE_KEY);
    }
    public void setVille(String aValue) {
        takeStoredValueForKey(aValue, VILLE_KEY);
    }




    public org.cocktail.kava.client.metier.EOAdresse adresse() {
        return (org.cocktail.kava.client.metier.EOAdresse)storedValueForKey(ADRESSE_KEY);
    }
    public void setAdresse(org.cocktail.kava.client.metier.EOAdresse aValue) {
        takeStoredValueForKey(aValue, ADRESSE_KEY);
    }
	
    public void setAdresseRelationship(org.cocktail.kava.client.metier.EOAdresse value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOAdresse object = adresse();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ADRESSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
        }
    }





}

