// EOArticle.java
// 
package org.cocktail.kava.client.metier;

import org.cocktail.kava.client.finder.FinderTypeArticle;

import com.webobjects.foundation.NSValidation;

public class EOArticle extends _EOArticle {

	public EOArticle() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (articlePrestation() == null) {
			throw new ValidationException("Il faut que l'article soit lie a un articlePrestation... BUG de l'appli., contactez les pompiers...!");
		}
		if (artLibelle() == null) {
			throw new ValidationException("Il faut un libelle pour l'article!");
		}
		if (typeArticle() == null) {
			throw new ValidationException("Il faut que l'article ait un type... BUG de l'appli., contactez les pompiers...!");
		}
		if (typeArticle().equals(FinderTypeArticle.typeArticleRemise(editingContext())) && articlePrestation().artpQteMin() == null) {
			throw new ValidationException("Pour une remise quantitative, il faut une quantite minimum !");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
