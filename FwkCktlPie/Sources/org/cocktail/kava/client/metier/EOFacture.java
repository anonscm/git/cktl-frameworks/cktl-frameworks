
// EOFacture.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOFacture extends _EOFacture {

	public static final String	PRIMARY_KEY_KEY	= "facId";

	public EOFacture() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (facDateSaisie() == null) {
			throw new ValidationException("Il faut une date de creation pour la facture!");
		}
		if (facHtSaisie() == null) {
			throw new ValidationException("Il faut un montant HT pour la facture!");
		}
		if (facTtcSaisie() == null) {
			throw new ValidationException("Il faut un montant TTC pour la facture!");
		}
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la facture!");
		}
		if (personne() == null) {
			throw new ValidationException("Il faut un client pour la facture!");
		}
		if (organ() == null) {
			throw new ValidationException("Il faut une ligne budgetaire pour la facture!");
		}
		if (typeCreditRec() == null) {
			throw new ValidationException("Il faut un type de credit recette pour la facture!");
		}
		if (tauxProrata() == null) {
			throw new ValidationException("Il faut un taux de prorata pour la facture!");
		}
		if (typeApplication() == null) {
			throw new ValidationException("Il faut un type d'application pour la facture!");
		}
		if (utilisateur() == null) {
			throw new ValidationException("Il faut un utilisateur (agent) pour la facture!");
		}
		if (factureCtrlActions() == null || factureCtrlActions().count() == 0) {
			throw new ValidationException("Il faut au moins une action lolf renseignee pour la facture!!");
		}
		if (factureCtrlPlancos() == null || factureCtrlPlancos().count() == 0) {
			throw new ValidationException("Il faut au moins un plan comptable pour la facture!!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
