
// _EORecette.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EORecette.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EORecette extends EOGenericRecord {

    public static final String ENTITY_NAME = "Recette";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.recette";

    public static final String FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY = "facture_personne_persNomPrenom";
    public static final String FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY = "facture_typeApplication_tyapLibelle";
    public static final String REC_DATE_SAISIE_KEY = "recDateSaisie";
    public static final String REC_HT_SAISIE_KEY = "recHtSaisie";
    public static final String REC_LIB_KEY = "recLib";
    public static final String REC_MONTANT_BUDGETAIRE_KEY = "recMontantBudgetaire";
    public static final String REC_NUMERO_KEY = "recNumero";
    public static final String REC_TTC_SAISIE_KEY = "recTtcSaisie";
    public static final String REC_TTC_SAISIE_STRING_KEY = "recTtcSaisieString";
    public static final String REC_TVA_SAISIE_KEY = "recTvaSaisie";
    public static final String TIT_ID_KEY = "titId";
    public static final String TIT_NUMERO_KEY = "titNumero";

    public static final String REC_DATE_SAISIE_COLKEY = "REC_DATE_SAISIE";
    public static final String REC_HT_SAISIE_COLKEY = "REC_HT_SAISIE";
    public static final String REC_LIB_COLKEY = "REC_LIB";
    public static final String REC_MONTANT_BUDGETAIRE_COLKEY = "REC_MONTANT_BUDGETAIRE";
    public static final String REC_NUMERO_COLKEY = "REC_NUMERO";
    public static final String REC_TTC_SAISIE_COLKEY = "REC_TTC_SAISIE";
    public static final String REC_TVA_SAISIE_COLKEY = "REC_TVA_SAISIE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String FACTURE_KEY = "facture";
    public static final String RECETTE_PAPIER_KEY = "recettePapier";
    public static final String RECETTE_REDUCTION_KEY = "recetteReduction";
    public static final String TAUX_PRORATA_KEY = "tauxProrata";
    public static final String TYPE_ETAT_KEY = "typeEtat";
    public static final String UTILISATEUR_KEY = "utilisateur";

    public static final String PI_DEP_RECS_KEY = "piDepRecs";
    public static final String RECETTE_CTRL_ACTIONS_KEY = "recetteCtrlActions";
    public static final String RECETTE_CTRL_ANALYTIQUES_KEY = "recetteCtrlAnalytiques";
    public static final String RECETTE_CTRL_CONVENTIONS_KEY = "recetteCtrlConventions";
    public static final String RECETTE_CTRL_PLANCOS_KEY = "recetteCtrlPlancos";



	
    public _EORecette() {
        super();
    }




    public String facture_personne_persNomPrenom() {
        return (String)storedValueForKey(FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setFacture_personne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public String facture_typeApplication_tyapLibelle() {
        return (String)storedValueForKey(FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY);
    }
    public void setFacture_typeApplication_tyapLibelle(String aValue) {
        takeStoredValueForKey(aValue, FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY);
    }

    public NSTimestamp recDateSaisie() {
        return (NSTimestamp)storedValueForKey(REC_DATE_SAISIE_KEY);
    }
    public void setRecDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, REC_DATE_SAISIE_KEY);
    }

    public BigDecimal recHtSaisie() {
        return (BigDecimal)storedValueForKey(REC_HT_SAISIE_KEY);
    }
    public void setRecHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, REC_HT_SAISIE_KEY);
    }

    public String recLib() {
        return (String)storedValueForKey(REC_LIB_KEY);
    }
    public void setRecLib(String aValue) {
        takeStoredValueForKey(aValue, REC_LIB_KEY);
    }

    public BigDecimal recMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(REC_MONTANT_BUDGETAIRE_KEY);
    }
    public void setRecMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, REC_MONTANT_BUDGETAIRE_KEY);
    }

    public Number recNumero() {
        return (Number)storedValueForKey(REC_NUMERO_KEY);
    }
    public void setRecNumero(Number aValue) {
        takeStoredValueForKey(aValue, REC_NUMERO_KEY);
    }

    public BigDecimal recTtcSaisie() {
        return (BigDecimal)storedValueForKey(REC_TTC_SAISIE_KEY);
    }
    public void setRecTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, REC_TTC_SAISIE_KEY);
    }

    public String recTtcSaisieString() {
        return (String)storedValueForKey(REC_TTC_SAISIE_STRING_KEY);
    }
    public void setRecTtcSaisieString(String aValue) {
        takeStoredValueForKey(aValue, REC_TTC_SAISIE_STRING_KEY);
    }

    public BigDecimal recTvaSaisie() {
        return (BigDecimal)storedValueForKey(REC_TVA_SAISIE_KEY);
    }
    public void setRecTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, REC_TVA_SAISIE_KEY);
    }

    public Number titId() {
        return (Number)storedValueForKey(TIT_ID_KEY);
    }
    public void setTitId(Number aValue) {
        takeStoredValueForKey(aValue, TIT_ID_KEY);
    }

    public Number titNumero() {
        return (Number)storedValueForKey(TIT_NUMERO_KEY);
    }
    public void setTitNumero(Number aValue) {
        takeStoredValueForKey(aValue, TIT_NUMERO_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFacture facture() {
        return (org.cocktail.kava.client.metier.EOFacture)storedValueForKey(FACTURE_KEY);
    }
    public void setFacture(org.cocktail.kava.client.metier.EOFacture aValue) {
        takeStoredValueForKey(aValue, FACTURE_KEY);
    }
	
    public void setFactureRelationship(org.cocktail.kava.client.metier.EOFacture value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFacture object = facture();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORecettePapier recettePapier() {
        return (org.cocktail.kava.client.metier.EORecettePapier)storedValueForKey(RECETTE_PAPIER_KEY);
    }
    public void setRecettePapier(org.cocktail.kava.client.metier.EORecettePapier aValue) {
        takeStoredValueForKey(aValue, RECETTE_PAPIER_KEY);
    }
	
    public void setRecettePapierRelationship(org.cocktail.kava.client.metier.EORecettePapier value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORecettePapier object = recettePapier();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_PAPIER_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_PAPIER_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORecette recetteReduction() {
        return (org.cocktail.kava.client.metier.EORecette)storedValueForKey(RECETTE_REDUCTION_KEY);
    }
    public void setRecetteReduction(org.cocktail.kava.client.metier.EORecette aValue) {
        takeStoredValueForKey(aValue, RECETTE_REDUCTION_KEY);
    }
	
    public void setRecetteReductionRelationship(org.cocktail.kava.client.metier.EORecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORecette object = recetteReduction();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_REDUCTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_REDUCTION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
        return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
    }
    public void setTauxProrata(org.cocktail.kava.client.metier.EOTauxProrata aValue) {
        takeStoredValueForKey(aValue, TAUX_PRORATA_KEY);
    }
	
    public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTauxProrata object = tauxProrata();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TAUX_PRORATA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }




    public NSArray piDepRecs() {
        return (NSArray)storedValueForKey(PI_DEP_RECS_KEY);
    }
    public void setPiDepRecs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PI_DEP_RECS_KEY);
    }
    public void addToPiDepRecs(org.cocktail.kava.client.metier.EOPiDepRec object) {
        NSMutableArray array = (NSMutableArray)piDepRecs();
        willChange();
        array.addObject(object);
    }
    public void removeFromPiDepRecs(org.cocktail.kava.client.metier.EOPiDepRec object) {
        NSMutableArray array = (NSMutableArray)piDepRecs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPiDepRecsRelationship(org.cocktail.kava.client.metier.EOPiDepRec object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PI_DEP_RECS_KEY);
    }
    public void removeFromPiDepRecsRelationship(org.cocktail.kava.client.metier.EOPiDepRec object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PI_DEP_RECS_KEY);
    }
	

    public NSArray recetteCtrlActions() {
        return (NSArray)storedValueForKey(RECETTE_CTRL_ACTIONS_KEY);
    }
    public void setRecetteCtrlActions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTE_CTRL_ACTIONS_KEY);
    }
    public void addToRecetteCtrlActions(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlActions();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecetteCtrlActions(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlActions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecetteCtrlActionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ACTIONS_KEY);
    }
    public void removeFromRecetteCtrlActionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAction object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ACTIONS_KEY);
    }
	

    public NSArray recetteCtrlAnalytiques() {
        return (NSArray)storedValueForKey(RECETTE_CTRL_ANALYTIQUES_KEY);
    }
    public void setRecetteCtrlAnalytiques(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTE_CTRL_ANALYTIQUES_KEY);
    }
    public void addToRecetteCtrlAnalytiques(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlAnalytiques();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecetteCtrlAnalytiques(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlAnalytiques();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecetteCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ANALYTIQUES_KEY);
    }
    public void removeFromRecetteCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EORecetteCtrlAnalytique object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_ANALYTIQUES_KEY);
    }
	

    public NSArray recetteCtrlConventions() {
        return (NSArray)storedValueForKey(RECETTE_CTRL_CONVENTIONS_KEY);
    }
    public void setRecetteCtrlConventions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTE_CTRL_CONVENTIONS_KEY);
    }
    public void addToRecetteCtrlConventions(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlConventions();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecetteCtrlConventions(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlConventions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecetteCtrlConventionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_CONVENTIONS_KEY);
    }
    public void removeFromRecetteCtrlConventionsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlConvention object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_CONVENTIONS_KEY);
    }
	

    public NSArray recetteCtrlPlancos() {
        return (NSArray)storedValueForKey(RECETTE_CTRL_PLANCOS_KEY);
    }
    public void setRecetteCtrlPlancos(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RECETTE_CTRL_PLANCOS_KEY);
    }
    public void addToRecetteCtrlPlancos(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlPlancos();
        willChange();
        array.addObject(object);
    }
    public void removeFromRecetteCtrlPlancos(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)recetteCtrlPlancos();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRecetteCtrlPlancosRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCOS_KEY);
    }
    public void removeFromRecetteCtrlPlancosRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlanco object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCOS_KEY);
    }
	


}

