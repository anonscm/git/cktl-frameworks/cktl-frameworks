
// _EOArticle.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOArticle.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOArticle extends EOGenericRecord {

    public static final String ENTITY_NAME = "Article";

    public static final String ENTITY_TABLE_NAME = "jefy_catalogue.article";

    public static final String ART_LIBELLE_KEY = "artLibelle";

    public static final String ART_LIBELLE_COLKEY = "ART_LIBELLE";

    public static final String ARTICLE_PERE_KEY = "articlePere";
    public static final String ARTICLE_PRESTATION_KEY = "articlePrestation";
    public static final String CODE_MARCHE_KEY = "codeMarche";
    public static final String TYPE_ARTICLE_KEY = "typeArticle";

    public static final String ARTICLE_PRESTATION_WEBS_KEY = "articlePrestationWebs";
    public static final String ARTICLES_KEY = "articles";



	
    public _EOArticle() {
        super();
    }




    public String artLibelle() {
        return (String)storedValueForKey(ART_LIBELLE_KEY);
    }
    public void setArtLibelle(String aValue) {
        takeStoredValueForKey(aValue, ART_LIBELLE_KEY);
    }




    public org.cocktail.kava.client.metier.EOArticle articlePere() {
        return (org.cocktail.kava.client.metier.EOArticle)storedValueForKey(ARTICLE_PERE_KEY);
    }
    public void setArticlePere(org.cocktail.kava.client.metier.EOArticle aValue) {
        takeStoredValueForKey(aValue, ARTICLE_PERE_KEY);
    }
	
    public void setArticlePereRelationship(org.cocktail.kava.client.metier.EOArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOArticle object = articlePere();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_PERE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_PERE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOArticlePrestation articlePrestation() {
        return (org.cocktail.kava.client.metier.EOArticlePrestation)storedValueForKey(ARTICLE_PRESTATION_KEY);
    }
    public void setArticlePrestation(org.cocktail.kava.client.metier.EOArticlePrestation aValue) {
        takeStoredValueForKey(aValue, ARTICLE_PRESTATION_KEY);
    }
	
    public void setArticlePrestationRelationship(org.cocktail.kava.client.metier.EOArticlePrestation value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOArticlePrestation object = articlePrestation();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_PRESTATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_PRESTATION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOCodeMarche codeMarche() {
        return (org.cocktail.application.client.eof.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
    }
    public void setCodeMarche(org.cocktail.application.client.eof.EOCodeMarche aValue) {
        takeStoredValueForKey(aValue, CODE_MARCHE_KEY);
    }
	
    public void setCodeMarcheRelationship(org.cocktail.application.client.eof.EOCodeMarche value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOCodeMarche object = codeMarche();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_MARCHE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeArticle typeArticle() {
        return (org.cocktail.kava.client.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
    }
    public void setTypeArticle(org.cocktail.kava.client.metier.EOTypeArticle aValue) {
        takeStoredValueForKey(aValue, TYPE_ARTICLE_KEY);
    }
	
    public void setTypeArticleRelationship(org.cocktail.kava.client.metier.EOTypeArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeArticle object = typeArticle();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ARTICLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
        }
    }




    public NSArray articlePrestationWebs() {
        return (NSArray)storedValueForKey(ARTICLE_PRESTATION_WEBS_KEY);
    }
    public void setArticlePrestationWebs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ARTICLE_PRESTATION_WEBS_KEY);
    }
    public void addToArticlePrestationWebs(org.cocktail.kava.client.metier.EOArticlePrestationWeb object) {
        NSMutableArray array = (NSMutableArray)articlePrestationWebs();
        willChange();
        array.addObject(object);
    }
    public void removeFromArticlePrestationWebs(org.cocktail.kava.client.metier.EOArticlePrestationWeb object) {
        NSMutableArray array = (NSMutableArray)articlePrestationWebs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToArticlePrestationWebsRelationship(org.cocktail.kava.client.metier.EOArticlePrestationWeb object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ARTICLE_PRESTATION_WEBS_KEY);
    }
    public void removeFromArticlePrestationWebsRelationship(org.cocktail.kava.client.metier.EOArticlePrestationWeb object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_PRESTATION_WEBS_KEY);
    }
	

    public NSArray articles() {
        return (NSArray)storedValueForKey(ARTICLES_KEY);
    }
    public void setArticles(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ARTICLES_KEY);
    }
    public void addToArticles(org.cocktail.kava.client.metier.EOArticle object) {
        NSMutableArray array = (NSMutableArray)articles();
        willChange();
        array.addObject(object);
    }
    public void removeFromArticles(org.cocktail.kava.client.metier.EOArticle object) {
        NSMutableArray array = (NSMutableArray)articles();
        willChange();
        array.removeObject(object);
    }
	
    public void addToArticlesRelationship(org.cocktail.kava.client.metier.EOArticle object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
    }
    public void removeFromArticlesRelationship(org.cocktail.kava.client.metier.EOArticle object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
    }
	


}

