
// _EOAdresse.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOAdresse.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOAdresse extends EOGenericRecord {

    public static final String ENTITY_NAME = "Adresse";

    public static final String ENTITY_TABLE_NAME = "grhum.adresse";

    public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
    public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
    public static final String ADR_BP_KEY = "adrBp";
    public static final String ADR_LISTE_ROUGE_KEY = "adrListeRouge";
    public static final String ADR_URL_PERE_KEY = "adrUrlPere";
    public static final String ADR_URL_RELATIVE_KEY = "adrUrlRelative";
    public static final String ADR_URL_TEMPLATE_KEY = "adrUrlTemplate";
    public static final String BIS_TER_KEY = "bisTer";
    public static final String C_IMPLANTATION_KEY = "cImplantation";
    public static final String C_VOIE_KEY = "cVoie";
    public static final String CODE_POSTAL_KEY = "codePostal";
    public static final String CP_ETRANGER_KEY = "cpEtranger";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_DEB_VAL_KEY = "dDebVal";
    public static final String D_FIN_VAL_KEY = "dFinVal";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String HABITANT_CHEZ_KEY = "habitantChez";
    public static final String LOCALITE_KEY = "localite";
    public static final String NO_VOIE_KEY = "noVoie";
    public static final String NOM_VOIE_KEY = "nomVoie";
    public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";
    public static final String VILLE_KEY = "ville";

    public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
    public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
    public static final String ADR_BP_COLKEY = "ADR_BP";
    public static final String ADR_LISTE_ROUGE_COLKEY = "ADR_LISTE_ROUGE";
    public static final String ADR_URL_PERE_COLKEY = "ADR_URL_PERE";
    public static final String ADR_URL_RELATIVE_COLKEY = "ADR_URL_RELATIVE";
    public static final String ADR_URL_TEMPLATE_COLKEY = "ADR_URL_TEMPLATE";
    public static final String BIS_TER_COLKEY = "BIS_TER";
    public static final String C_IMPLANTATION_COLKEY = "C_IMPLANTATION";
    public static final String C_VOIE_COLKEY = "C_VOIE";
    public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
    public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
    public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String HABITANT_CHEZ_COLKEY = "HABITANT_CHEZ";
    public static final String LOCALITE_COLKEY = "LOCALITE";
    public static final String NO_VOIE_COLKEY = "NO_VOIE";
    public static final String NOM_VOIE_COLKEY = "NOM_VOIE";
    public static final String TEM_PAYE_UTIL_COLKEY = "TEM_PAYE_UTIL";
    public static final String VILLE_COLKEY = "VILLE";

    public static final String PAYS_KEY = "pays";




	
    public _EOAdresse() {
        super();
    }




    public String adrAdresse1() {
        return (String)storedValueForKey(ADR_ADRESSE1_KEY);
    }
    public void setAdrAdresse1(String aValue) {
        takeStoredValueForKey(aValue, ADR_ADRESSE1_KEY);
    }

    public String adrAdresse2() {
        return (String)storedValueForKey(ADR_ADRESSE2_KEY);
    }
    public void setAdrAdresse2(String aValue) {
        takeStoredValueForKey(aValue, ADR_ADRESSE2_KEY);
    }

    public String adrBp() {
        return (String)storedValueForKey(ADR_BP_KEY);
    }
    public void setAdrBp(String aValue) {
        takeStoredValueForKey(aValue, ADR_BP_KEY);
    }

    public String adrListeRouge() {
        return (String)storedValueForKey(ADR_LISTE_ROUGE_KEY);
    }
    public void setAdrListeRouge(String aValue) {
        takeStoredValueForKey(aValue, ADR_LISTE_ROUGE_KEY);
    }

    public String adrUrlPere() {
        return (String)storedValueForKey(ADR_URL_PERE_KEY);
    }
    public void setAdrUrlPere(String aValue) {
        takeStoredValueForKey(aValue, ADR_URL_PERE_KEY);
    }

    public String adrUrlRelative() {
        return (String)storedValueForKey(ADR_URL_RELATIVE_KEY);
    }
    public void setAdrUrlRelative(String aValue) {
        takeStoredValueForKey(aValue, ADR_URL_RELATIVE_KEY);
    }

    public String adrUrlTemplate() {
        return (String)storedValueForKey(ADR_URL_TEMPLATE_KEY);
    }
    public void setAdrUrlTemplate(String aValue) {
        takeStoredValueForKey(aValue, ADR_URL_TEMPLATE_KEY);
    }

    public String bisTer() {
        return (String)storedValueForKey(BIS_TER_KEY);
    }
    public void setBisTer(String aValue) {
        takeStoredValueForKey(aValue, BIS_TER_KEY);
    }

    public String cImplantation() {
        return (String)storedValueForKey(C_IMPLANTATION_KEY);
    }
    public void setCImplantation(String aValue) {
        takeStoredValueForKey(aValue, C_IMPLANTATION_KEY);
    }

    public String cVoie() {
        return (String)storedValueForKey(C_VOIE_KEY);
    }
    public void setCVoie(String aValue) {
        takeStoredValueForKey(aValue, C_VOIE_KEY);
    }

    public String codePostal() {
        return (String)storedValueForKey(CODE_POSTAL_KEY);
    }
    public void setCodePostal(String aValue) {
        takeStoredValueForKey(aValue, CODE_POSTAL_KEY);
    }

    public String cpEtranger() {
        return (String)storedValueForKey(CP_ETRANGER_KEY);
    }
    public void setCpEtranger(String aValue) {
        takeStoredValueForKey(aValue, CP_ETRANGER_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dDebVal() {
        return (NSTimestamp)storedValueForKey(D_DEB_VAL_KEY);
    }
    public void setDDebVal(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_DEB_VAL_KEY);
    }

    public NSTimestamp dFinVal() {
        return (NSTimestamp)storedValueForKey(D_FIN_VAL_KEY);
    }
    public void setDFinVal(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_FIN_VAL_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public String habitantChez() {
        return (String)storedValueForKey(HABITANT_CHEZ_KEY);
    }
    public void setHabitantChez(String aValue) {
        takeStoredValueForKey(aValue, HABITANT_CHEZ_KEY);
    }

    public String localite() {
        return (String)storedValueForKey(LOCALITE_KEY);
    }
    public void setLocalite(String aValue) {
        takeStoredValueForKey(aValue, LOCALITE_KEY);
    }

    public String noVoie() {
        return (String)storedValueForKey(NO_VOIE_KEY);
    }
    public void setNoVoie(String aValue) {
        takeStoredValueForKey(aValue, NO_VOIE_KEY);
    }

    public String nomVoie() {
        return (String)storedValueForKey(NOM_VOIE_KEY);
    }
    public void setNomVoie(String aValue) {
        takeStoredValueForKey(aValue, NOM_VOIE_KEY);
    }

    public String temPayeUtil() {
        return (String)storedValueForKey(TEM_PAYE_UTIL_KEY);
    }
    public void setTemPayeUtil(String aValue) {
        takeStoredValueForKey(aValue, TEM_PAYE_UTIL_KEY);
    }

    public String ville() {
        return (String)storedValueForKey(VILLE_KEY);
    }
    public void setVille(String aValue) {
        takeStoredValueForKey(aValue, VILLE_KEY);
    }




    public org.cocktail.application.client.eof.EOPays pays() {
        return (org.cocktail.application.client.eof.EOPays)storedValueForKey(PAYS_KEY);
    }
    public void setPays(org.cocktail.application.client.eof.EOPays aValue) {
        takeStoredValueForKey(aValue, PAYS_KEY);
    }
	
    public void setPaysRelationship(org.cocktail.application.client.eof.EOPays value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOPays object = pays();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PAYS_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PAYS_KEY);
        }
    }





}

