
// _EOCataloguePublic.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCataloguePublic.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOCataloguePublic extends EOGenericRecord {

    public static final String ENTITY_NAME = "CataloguePublic";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.catalogue_public";

    public static final String CAP_POURCENTAGE_KEY = "capPourcentage";

    public static final String CAP_POURCENTAGE_COLKEY = "CAP_POURCENTAGE";

    public static final String CATALOGUE_KEY = "catalogue";
    public static final String TYPE_PUBLIC_KEY = "typePublic";




	
    public _EOCataloguePublic() {
        super();
    }




    public BigDecimal capPourcentage() {
        return (BigDecimal)storedValueForKey(CAP_POURCENTAGE_KEY);
    }
    public void setCapPourcentage(BigDecimal aValue) {
        takeStoredValueForKey(aValue, CAP_POURCENTAGE_KEY);
    }




    public org.cocktail.kava.client.metier.EOCatalogue catalogue() {
        return (org.cocktail.kava.client.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
    }
    public void setCatalogue(org.cocktail.kava.client.metier.EOCatalogue aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_KEY);
    }
	
    public void setCatalogueRelationship(org.cocktail.kava.client.metier.EOCatalogue value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCatalogue object = catalogue();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypePublic typePublic() {
        return (org.cocktail.kava.client.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
    }
    public void setTypePublic(org.cocktail.kava.client.metier.EOTypePublic aValue) {
        takeStoredValueForKey(aValue, TYPE_PUBLIC_KEY);
    }
	
    public void setTypePublicRelationship(org.cocktail.kava.client.metier.EOTypePublic value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypePublic object = typePublic();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_PUBLIC_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
        }
    }





}

