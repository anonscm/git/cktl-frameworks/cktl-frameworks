
// _EOPlancoVisa.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPlancoVisa.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOPlancoVisa extends EOGenericRecord {

    public static final String ENTITY_NAME = "PlancoVisa";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_planco_visa";

    public static final String PVI_CONTREPARTIE_GESTION_KEY = "pviContrepartieGestion";
    public static final String PVI_LIBELLE_KEY = "pviLibelle";

    public static final String PVI_CONTREPARTIE_GESTION_COLKEY = "PVI_CONTREPARTIE_GESTION";
    public static final String PVI_LIBELLE_COLKEY = "PVI_LIBELLE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String PLAN_COMPTABLE_CTP_KEY = "planComptableCtp";
    public static final String PLAN_COMPTABLE_TVA_KEY = "planComptableTva";




	
    public _EOPlancoVisa() {
        super();
    }




    public String pviContrepartieGestion() {
        return (String)storedValueForKey(PVI_CONTREPARTIE_GESTION_KEY);
    }
    public void setPviContrepartieGestion(String aValue) {
        takeStoredValueForKey(aValue, PVI_CONTREPARTIE_GESTION_KEY);
    }

    public String pviLibelle() {
        return (String)storedValueForKey(PVI_LIBELLE_KEY);
    }
    public void setPviLibelle(String aValue) {
        takeStoredValueForKey(aValue, PVI_LIBELLE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableCtp() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_CTP_KEY);
    }
    public void setPlanComptableCtp(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_CTP_KEY);
    }
	
    public void setPlanComptableCtpRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableCtp();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_CTP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_CTP_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptableTva() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_TVA_KEY);
    }
    public void setPlanComptableTva(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_TVA_KEY);
    }
	
    public void setPlanComptableTvaRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptableTva();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_TVA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_TVA_KEY);
        }
    }





}

