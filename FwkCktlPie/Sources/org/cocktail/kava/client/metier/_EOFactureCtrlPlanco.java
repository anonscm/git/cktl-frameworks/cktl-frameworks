
// _EOFactureCtrlPlanco.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFactureCtrlPlanco.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFactureCtrlPlanco extends EOGenericRecord {

    public static final String ENTITY_NAME = "FactureCtrlPlanco";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_ctrl_planco";

    public static final String FAC_ID_KEY = "facId";
    public static final String FPCO_DATE_SAISIE_KEY = "fpcoDateSaisie";
    public static final String FPCO_HT_RESTE_KEY = "fpcoHtReste";
    public static final String FPCO_HT_SAISIE_KEY = "fpcoHtSaisie";
    public static final String FPCO_TTC_SAISIE_KEY = "fpcoTtcSaisie";
    public static final String FPCO_TVA_RESTE_KEY = "fpcoTvaReste";
    public static final String FPCO_TVA_SAISIE_KEY = "fpcoTvaSaisie";

    public static final String FAC_ID_COLKEY = "FAC_ID";
    public static final String FPCO_DATE_SAISIE_COLKEY = "FPCO_DATE_SAISIE";
    public static final String FPCO_HT_RESTE_COLKEY = "FPCO_HT_RESTE";
    public static final String FPCO_HT_SAISIE_COLKEY = "FPCO_HT_SAISIE";
    public static final String FPCO_TTC_SAISIE_COLKEY = "FPCO_TTC_SAISIE";
    public static final String FPCO_TVA_RESTE_COLKEY = "FPCO_TVA_RESTE";
    public static final String FPCO_TVA_SAISIE_COLKEY = "FPCO_TVA_SAISIE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";




	
    public _EOFactureCtrlPlanco() {
        super();
    }




    public Number facId() {
        return (Number)storedValueForKey(FAC_ID_KEY);
    }
    public void setFacId(Number aValue) {
        takeStoredValueForKey(aValue, FAC_ID_KEY);
    }

    public NSTimestamp fpcoDateSaisie() {
        return (NSTimestamp)storedValueForKey(FPCO_DATE_SAISIE_KEY);
    }
    public void setFpcoDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FPCO_DATE_SAISIE_KEY);
    }

    public BigDecimal fpcoHtReste() {
        return (BigDecimal)storedValueForKey(FPCO_HT_RESTE_KEY);
    }
    public void setFpcoHtReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FPCO_HT_RESTE_KEY);
    }

    public BigDecimal fpcoHtSaisie() {
        return (BigDecimal)storedValueForKey(FPCO_HT_SAISIE_KEY);
    }
    public void setFpcoHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FPCO_HT_SAISIE_KEY);
    }

    public BigDecimal fpcoTtcSaisie() {
        return (BigDecimal)storedValueForKey(FPCO_TTC_SAISIE_KEY);
    }
    public void setFpcoTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FPCO_TTC_SAISIE_KEY);
    }

    public BigDecimal fpcoTvaReste() {
        return (BigDecimal)storedValueForKey(FPCO_TVA_RESTE_KEY);
    }
    public void setFpcoTvaReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FPCO_TVA_RESTE_KEY);
    }

    public BigDecimal fpcoTvaSaisie() {
        return (BigDecimal)storedValueForKey(FPCO_TVA_SAISIE_KEY);
    }
    public void setFpcoTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FPCO_TVA_SAISIE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }





}

