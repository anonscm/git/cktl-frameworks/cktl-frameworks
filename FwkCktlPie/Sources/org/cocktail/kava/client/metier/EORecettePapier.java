// EORecettePapier.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EORecettePapier extends _EORecettePapier {

	public static final String	PRIMARY_KEY_KEY	= "rppId";

	public EORecettePapier() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (rppNumero() == null) {
			throw new ValidationException("Il faut un numero/reference pour la recette papier!");
		}
		if (rppDateSaisie() == null) {
			throw new ValidationException("Il faut une date de creation pour la recette papier!");
		}
		if (rppHtSaisie() == null) {
			throw new ValidationException("Il faut un montant HT pour la recette papier!");
		}
		if (rppTtcSaisie() == null) {
			throw new ValidationException("Il faut un montant TTC pour la recette papier!");
		}
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la recette papier!");
		}
		if (utilisateur() == null) {
			throw new ValidationException("Il faut un utilisateur (agent) pour la recette papier!");
		}
		if (personne() == null) {
			throw new ValidationException("Il faut un \"client\" pour la recette papier!");
		}
		if (rppNbPiece() == null) {
			throw new ValidationException("Il faut indiquer le nombre de piece(s) pour la recette papier!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
