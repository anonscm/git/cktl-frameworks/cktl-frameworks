
// _EOFactureCtrlConvention.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFactureCtrlConvention.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFactureCtrlConvention extends EOGenericRecord {

    public static final String ENTITY_NAME = "FactureCtrlConvention";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_ctrl_convention";

    public static final String FAC_ID_KEY = "facId";
    public static final String FCON_DATE_SAISIE_KEY = "fconDateSaisie";
    public static final String FCON_HT_SAISIE_KEY = "fconHtSaisie";
    public static final String FCON_MONTANT_BUDGETAIRE_KEY = "fconMontantBudgetaire";
    public static final String FCON_MONTANT_BUDGETAIRE_RESTE_KEY = "fconMontantBudgetaireReste";
    public static final String FCON_TTC_SAISIE_KEY = "fconTtcSaisie";
    public static final String FCON_TVA_SAISIE_KEY = "fconTvaSaisie";

    public static final String FAC_ID_COLKEY = "FAC_ID";
    public static final String FCON_DATE_SAISIE_COLKEY = "FCON_DATE_SAISIE";
    public static final String FCON_HT_SAISIE_COLKEY = "FCON_HT_SAISIE";
    public static final String FCON_MONTANT_BUDGETAIRE_COLKEY = "FCON_MONTANT_BUDGETAIRE";
    public static final String FCON_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FCON_MONTANT_BUDGETAIRE_RESTE";
    public static final String FCON_TTC_SAISIE_COLKEY = "FCON_TTC_SAISIE";
    public static final String FCON_TVA_SAISIE_COLKEY = "FCON_TVA_SAISIE";

    public static final String CONVENTION_KEY = "convention";
    public static final String EXERCICE_KEY = "exercice";




	
    public _EOFactureCtrlConvention() {
        super();
    }




    public Number facId() {
        return (Number)storedValueForKey(FAC_ID_KEY);
    }
    public void setFacId(Number aValue) {
        takeStoredValueForKey(aValue, FAC_ID_KEY);
    }

    public NSTimestamp fconDateSaisie() {
        return (NSTimestamp)storedValueForKey(FCON_DATE_SAISIE_KEY);
    }
    public void setFconDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FCON_DATE_SAISIE_KEY);
    }

    public BigDecimal fconHtSaisie() {
        return (BigDecimal)storedValueForKey(FCON_HT_SAISIE_KEY);
    }
    public void setFconHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FCON_HT_SAISIE_KEY);
    }

    public BigDecimal fconMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(FCON_MONTANT_BUDGETAIRE_KEY);
    }
    public void setFconMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FCON_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal fconMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(FCON_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setFconMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FCON_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public BigDecimal fconTtcSaisie() {
        return (BigDecimal)storedValueForKey(FCON_TTC_SAISIE_KEY);
    }
    public void setFconTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FCON_TTC_SAISIE_KEY);
    }

    public BigDecimal fconTvaSaisie() {
        return (BigDecimal)storedValueForKey(FCON_TVA_SAISIE_KEY);
    }
    public void setFconTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FCON_TVA_SAISIE_KEY);
    }




    public org.cocktail.kava.client.metier.EOConvention convention() {
        return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
    }
    public void setConvention(org.cocktail.kava.client.metier.EOConvention aValue) {
        takeStoredValueForKey(aValue, CONVENTION_KEY);
    }
	
    public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOConvention object = convention();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }





}

