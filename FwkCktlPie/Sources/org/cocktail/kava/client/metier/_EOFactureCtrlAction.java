
// _EOFactureCtrlAction.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFactureCtrlAction.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFactureCtrlAction extends EOGenericRecord {

    public static final String ENTITY_NAME = "FactureCtrlAction";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_ctrl_action";

    public static final String FAC_ID_KEY = "facId";
    public static final String FACT_DATE_SAISIE_KEY = "factDateSaisie";
    public static final String FACT_HT_SAISIE_KEY = "factHtSaisie";
    public static final String FACT_MONTANT_BUDGETAIRE_KEY = "factMontantBudgetaire";
    public static final String FACT_MONTANT_BUDGETAIRE_RESTE_KEY = "factMontantBudgetaireReste";
    public static final String FACT_TTC_SAISIE_KEY = "factTtcSaisie";
    public static final String FACT_TVA_SAISIE_KEY = "factTvaSaisie";

    public static final String FAC_ID_COLKEY = "FAC_ID";
    public static final String FACT_DATE_SAISIE_COLKEY = "FACT_DATE_SAISIE";
    public static final String FACT_HT_SAISIE_COLKEY = "FACT_HT_SAISIE";
    public static final String FACT_MONTANT_BUDGETAIRE_COLKEY = "FACT_MONTANT_BUDGETAIRE";
    public static final String FACT_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FACT_MONTANT_BUDGETAIRE_RESTE";
    public static final String FACT_TTC_SAISIE_COLKEY = "FACT_TTC_SAISIE";
    public static final String FACT_TVA_SAISIE_COLKEY = "FACT_TVA_SAISIE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";




	
    public _EOFactureCtrlAction() {
        super();
    }




    public Number facId() {
        return (Number)storedValueForKey(FAC_ID_KEY);
    }
    public void setFacId(Number aValue) {
        takeStoredValueForKey(aValue, FAC_ID_KEY);
    }

    public NSTimestamp factDateSaisie() {
        return (NSTimestamp)storedValueForKey(FACT_DATE_SAISIE_KEY);
    }
    public void setFactDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FACT_DATE_SAISIE_KEY);
    }

    public BigDecimal factHtSaisie() {
        return (BigDecimal)storedValueForKey(FACT_HT_SAISIE_KEY);
    }
    public void setFactHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FACT_HT_SAISIE_KEY);
    }

    public BigDecimal factMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(FACT_MONTANT_BUDGETAIRE_KEY);
    }
    public void setFactMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FACT_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal factMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(FACT_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setFactMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FACT_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public BigDecimal factTtcSaisie() {
        return (BigDecimal)storedValueForKey(FACT_TTC_SAISIE_KEY);
    }
    public void setFactTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FACT_TTC_SAISIE_KEY);
    }

    public BigDecimal factTvaSaisie() {
        return (BigDecimal)storedValueForKey(FACT_TVA_SAISIE_KEY);
    }
    public void setFactTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, FACT_TVA_SAISIE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
    }
    public void setLolfNomenclatureRecette(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
	
    public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureRecette value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureRecette object = lolfNomenclatureRecette();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
        }
    }





}

