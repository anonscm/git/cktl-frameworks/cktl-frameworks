
// _EOCatalogueArticle.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCatalogueArticle.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOCatalogueArticle extends EOGenericRecord {

    public static final String ENTITY_NAME = "CatalogueArticle";

    public static final String ENTITY_TABLE_NAME = "jefy_catalogue.catalogue_article";

    public static final String CAAR_DOC_ID_KEY = "caarDocId";
    public static final String CAAR_DOC_REFERENCE_KEY = "caarDocReference";
    public static final String CAAR_PRIX_HT_KEY = "caarPrixHt";
    public static final String CAAR_PRIX_TTC_KEY = "caarPrixTtc";
    public static final String CAAR_REFERENCE_KEY = "caarReference";

    public static final String CAAR_DOC_ID_COLKEY = "CAAR_DOC_ID";
    public static final String CAAR_DOC_REFERENCE_COLKEY = "CAAR_DOC_REFERENCE";
    public static final String CAAR_PRIX_HT_COLKEY = "CAAR_PRIX_HT";
    public static final String CAAR_PRIX_TTC_COLKEY = "CAAR_PRIX_TTC";
    public static final String CAAR_REFERENCE_COLKEY = "CAAR_REFERENCE";

    public static final String ARTICLE_KEY = "article";
    public static final String CATALOGUE_KEY = "catalogue";
    public static final String TVA_KEY = "tva";
    public static final String TYPE_ETAT_KEY = "typeEtat";




	
    public _EOCatalogueArticle() {
        super();
    }




    public Number caarDocId() {
        return (Number)storedValueForKey(CAAR_DOC_ID_KEY);
    }
    public void setCaarDocId(Number aValue) {
        takeStoredValueForKey(aValue, CAAR_DOC_ID_KEY);
    }

    public String caarDocReference() {
        return (String)storedValueForKey(CAAR_DOC_REFERENCE_KEY);
    }
    public void setCaarDocReference(String aValue) {
        takeStoredValueForKey(aValue, CAAR_DOC_REFERENCE_KEY);
    }

    public BigDecimal caarPrixHt() {
        return (BigDecimal)storedValueForKey(CAAR_PRIX_HT_KEY);
    }
    public void setCaarPrixHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, CAAR_PRIX_HT_KEY);
    }

    public BigDecimal caarPrixTtc() {
        return (BigDecimal)storedValueForKey(CAAR_PRIX_TTC_KEY);
    }
    public void setCaarPrixTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, CAAR_PRIX_TTC_KEY);
    }

    public String caarReference() {
        return (String)storedValueForKey(CAAR_REFERENCE_KEY);
    }
    public void setCaarReference(String aValue) {
        takeStoredValueForKey(aValue, CAAR_REFERENCE_KEY);
    }




    public org.cocktail.kava.client.metier.EOArticle article() {
        return (org.cocktail.kava.client.metier.EOArticle)storedValueForKey(ARTICLE_KEY);
    }
    public void setArticle(org.cocktail.kava.client.metier.EOArticle aValue) {
        takeStoredValueForKey(aValue, ARTICLE_KEY);
    }
	
    public void setArticleRelationship(org.cocktail.kava.client.metier.EOArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOArticle object = article();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOCatalogue catalogue() {
        return (org.cocktail.kava.client.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
    }
    public void setCatalogue(org.cocktail.kava.client.metier.EOCatalogue aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_KEY);
    }
	
    public void setCatalogueRelationship(org.cocktail.kava.client.metier.EOCatalogue value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCatalogue object = catalogue();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTva tva() {
        return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_KEY);
    }
    public void setTva(org.cocktail.kava.client.metier.EOTva aValue) {
        takeStoredValueForKey(aValue, TVA_KEY);
    }
	
    public void setTvaRelationship(org.cocktail.kava.client.metier.EOTva value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTva object = tva();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TVA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }





}

