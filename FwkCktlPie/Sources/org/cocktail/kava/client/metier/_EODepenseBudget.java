
// _EODepenseBudget.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EODepenseBudget.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EODepenseBudget extends EOGenericRecord {

    public static final String ENTITY_NAME = "DepenseBudget";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_depense_budget";

    public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
    public static final String DEP_ID_REVERSEMENT_KEY = "depIdReversement";
    public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
    public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
    public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";
    public static final String DPP_ID_KEY = "dppId";
    public static final String ENG_ID_KEY = "engId";
    public static final String TAP_ID_KEY = "tapId";
    public static final String UTL_ORDRE_KEY = "utlOrdre";

    public static final String DEP_HT_SAISIE_COLKEY = "DEP_HT_SAISIE";
    public static final String DEP_ID_REVERSEMENT_COLKEY = "DEP_ID_REVERSEMENT";
    public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "DEP_MONTANT_BUDGETAIRE";
    public static final String DEP_TTC_SAISIE_COLKEY = "DEP_TTC_SAISIE";
    public static final String DEP_TVA_SAISIE_COLKEY = "DEP_TVA_SAISIE";
    public static final String DPP_ID_COLKEY = "DPP_ID";
    public static final String ENG_ID_COLKEY = "ENG_ID";
    public static final String TAP_ID_COLKEY = "TAP_ID";
    public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

    public static final String EXERCICE_KEY = "exercice";

    public static final String DEPENSE_CTRL_PLANCOS_KEY = "depenseCtrlPlancos";



	
    public _EODepenseBudget() {
        super();
    }




    public BigDecimal depHtSaisie() {
        return (BigDecimal)storedValueForKey(DEP_HT_SAISIE_KEY);
    }
    public void setDepHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, DEP_HT_SAISIE_KEY);
    }

    public Number depIdReversement() {
        return (Number)storedValueForKey(DEP_ID_REVERSEMENT_KEY);
    }
    public void setDepIdReversement(Number aValue) {
        takeStoredValueForKey(aValue, DEP_ID_REVERSEMENT_KEY);
    }

    public BigDecimal depMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
    }
    public void setDepMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, DEP_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal depTtcSaisie() {
        return (BigDecimal)storedValueForKey(DEP_TTC_SAISIE_KEY);
    }
    public void setDepTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, DEP_TTC_SAISIE_KEY);
    }

    public BigDecimal depTvaSaisie() {
        return (BigDecimal)storedValueForKey(DEP_TVA_SAISIE_KEY);
    }
    public void setDepTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, DEP_TVA_SAISIE_KEY);
    }

    public Number dppId() {
        return (Number)storedValueForKey(DPP_ID_KEY);
    }
    public void setDppId(Number aValue) {
        takeStoredValueForKey(aValue, DPP_ID_KEY);
    }

    public Number engId() {
        return (Number)storedValueForKey(ENG_ID_KEY);
    }
    public void setEngId(Number aValue) {
        takeStoredValueForKey(aValue, ENG_ID_KEY);
    }

    public Number tapId() {
        return (Number)storedValueForKey(TAP_ID_KEY);
    }
    public void setTapId(Number aValue) {
        takeStoredValueForKey(aValue, TAP_ID_KEY);
    }

    public Number utlOrdre() {
        return (Number)storedValueForKey(UTL_ORDRE_KEY);
    }
    public void setUtlOrdre(Number aValue) {
        takeStoredValueForKey(aValue, UTL_ORDRE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }




    public NSArray depenseCtrlPlancos() {
        return (NSArray)storedValueForKey(DEPENSE_CTRL_PLANCOS_KEY);
    }
    public void setDepenseCtrlPlancos(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, DEPENSE_CTRL_PLANCOS_KEY);
    }
    public void addToDepenseCtrlPlancos(org.cocktail.kava.client.metier.EODepenseCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)depenseCtrlPlancos();
        willChange();
        array.addObject(object);
    }
    public void removeFromDepenseCtrlPlancos(org.cocktail.kava.client.metier.EODepenseCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)depenseCtrlPlancos();
        willChange();
        array.removeObject(object);
    }
	
    public void addToDepenseCtrlPlancosRelationship(org.cocktail.kava.client.metier.EODepenseCtrlPlanco object) {
        addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CTRL_PLANCOS_KEY);
    }
    public void removeFromDepenseCtrlPlancosRelationship(org.cocktail.kava.client.metier.EODepenseCtrlPlanco object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CTRL_PLANCOS_KEY);
    }
	


}

