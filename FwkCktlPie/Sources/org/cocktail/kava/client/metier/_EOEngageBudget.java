
// _EOEngageBudget.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOEngageBudget.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOEngageBudget extends EOGenericRecord {

    public static final String ENTITY_NAME = "EngageBudget";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_engage_budget";

    public static final String ENG_DATE_SAISIE_KEY = "engDateSaisie";
    public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
    public static final String ENG_LIBELLE_KEY = "engLibelle";
    public static final String ENG_MONTANT_BUDGETAIRE_KEY = "engMontantBudgetaire";
    public static final String ENG_MONTANT_BUDGETAIRE_RESTE_KEY = "engMontantBudgetaireReste";
    public static final String ENG_NUMERO_KEY = "engNumero";
    public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
    public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";
    public static final String FOU_ORDRE_KEY = "fouOrdre";

    public static final String ENG_DATE_SAISIE_COLKEY = "ENG_DATE_SAISIE";
    public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
    public static final String ENG_LIBELLE_COLKEY = "ENG_LIBELLE";
    public static final String ENG_MONTANT_BUDGETAIRE_COLKEY = "ENG_MONTANT_BUDGETAIRE";
    public static final String ENG_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ENG_MONTANT_BUDGETAIRE_RESTE";
    public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
    public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
    public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";
    public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";

    public static final String EXERCICE_KEY = "exercice";
    public static final String ORGAN_KEY = "organ";
    public static final String TAUX_PRORATA_KEY = "tauxProrata";
    public static final String TYPE_APPLICATION_KEY = "typeApplication";
    public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";
    public static final String UTILISATEUR_KEY = "utilisateur";

    public static final String ENGAGE_CTRL_ACTIONS_KEY = "engageCtrlActions";
    public static final String ENGAGE_CTRL_ANALYTIQUE_KEY = "engageCtrlAnalytique";
    public static final String ENGAGE_CTRL_CONVENTIONS_KEY = "engageCtrlConventions";
    public static final String ENGAGE_CTRL_PLANCOS_KEY = "engageCtrlPlancos";



	
    public _EOEngageBudget() {
        super();
    }




    public NSTimestamp engDateSaisie() {
        return (NSTimestamp)storedValueForKey(ENG_DATE_SAISIE_KEY);
    }
    public void setEngDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ENG_DATE_SAISIE_KEY);
    }

    public BigDecimal engHtSaisie() {
        return (BigDecimal)storedValueForKey(ENG_HT_SAISIE_KEY);
    }
    public void setEngHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_HT_SAISIE_KEY);
    }

    public String engLibelle() {
        return (String)storedValueForKey(ENG_LIBELLE_KEY);
    }
    public void setEngLibelle(String aValue) {
        takeStoredValueForKey(aValue, ENG_LIBELLE_KEY);
    }

    public BigDecimal engMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(ENG_MONTANT_BUDGETAIRE_KEY);
    }
    public void setEngMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal engMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setEngMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public Number engNumero() {
        return (Number)storedValueForKey(ENG_NUMERO_KEY);
    }
    public void setEngNumero(Number aValue) {
        takeStoredValueForKey(aValue, ENG_NUMERO_KEY);
    }

    public BigDecimal engTtcSaisie() {
        return (BigDecimal)storedValueForKey(ENG_TTC_SAISIE_KEY);
    }
    public void setEngTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_TTC_SAISIE_KEY);
    }

    public BigDecimal engTvaSaisie() {
        return (BigDecimal)storedValueForKey(ENG_TVA_SAISIE_KEY);
    }
    public void setEngTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ENG_TVA_SAISIE_KEY);
    }

    public Number fouOrdre() {
        return (Number)storedValueForKey(FOU_ORDRE_KEY);
    }
    public void setFouOrdre(Number aValue) {
        takeStoredValueForKey(aValue, FOU_ORDRE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
        return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
    }
    public void setTauxProrata(org.cocktail.kava.client.metier.EOTauxProrata aValue) {
        takeStoredValueForKey(aValue, TAUX_PRORATA_KEY);
    }
	
    public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTauxProrata object = tauxProrata();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TAUX_PRORATA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
        return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
    }
    public void setTypeApplication(org.cocktail.kava.client.metier.EOTypeApplication aValue) {
        takeStoredValueForKey(aValue, TYPE_APPLICATION_KEY);
    }
	
    public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeApplication object = typeApplication();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_APPLICATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditDep() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
    }
    public void setTypeCreditDep(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_DEP_KEY);
    }
	
    public void setTypeCreditDepRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditDep();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_DEP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }




    public NSArray engageCtrlActions() {
        return (NSArray)storedValueForKey(ENGAGE_CTRL_ACTIONS_KEY);
    }
    public void setEngageCtrlActions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ENGAGE_CTRL_ACTIONS_KEY);
    }
    public void addToEngageCtrlActions(org.cocktail.kava.client.metier.EOEngageCtrlAction object) {
        NSMutableArray array = (NSMutableArray)engageCtrlActions();
        willChange();
        array.addObject(object);
    }
    public void removeFromEngageCtrlActions(org.cocktail.kava.client.metier.EOEngageCtrlAction object) {
        NSMutableArray array = (NSMutableArray)engageCtrlActions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToEngageCtrlActionsRelationship(org.cocktail.kava.client.metier.EOEngageCtrlAction object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ACTIONS_KEY);
    }
    public void removeFromEngageCtrlActionsRelationship(org.cocktail.kava.client.metier.EOEngageCtrlAction object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ACTIONS_KEY);
    }
	

    public NSArray engageCtrlAnalytique() {
        return (NSArray)storedValueForKey(ENGAGE_CTRL_ANALYTIQUE_KEY);
    }
    public void setEngageCtrlAnalytique(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ENGAGE_CTRL_ANALYTIQUE_KEY);
    }
    public void addToEngageCtrlAnalytique(org.cocktail.kava.client.metier.EOEngageCtrlAnalytique object) {
        NSMutableArray array = (NSMutableArray)engageCtrlAnalytique();
        willChange();
        array.addObject(object);
    }
    public void removeFromEngageCtrlAnalytique(org.cocktail.kava.client.metier.EOEngageCtrlAnalytique object) {
        NSMutableArray array = (NSMutableArray)engageCtrlAnalytique();
        willChange();
        array.removeObject(object);
    }
	
    public void addToEngageCtrlAnalytiqueRelationship(org.cocktail.kava.client.metier.EOEngageCtrlAnalytique object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ANALYTIQUE_KEY);
    }
    public void removeFromEngageCtrlAnalytiqueRelationship(org.cocktail.kava.client.metier.EOEngageCtrlAnalytique object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ANALYTIQUE_KEY);
    }
	

    public NSArray engageCtrlConventions() {
        return (NSArray)storedValueForKey(ENGAGE_CTRL_CONVENTIONS_KEY);
    }
    public void setEngageCtrlConventions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ENGAGE_CTRL_CONVENTIONS_KEY);
    }
    public void addToEngageCtrlConventions(org.cocktail.kava.client.metier.EOEngageCtrlConvention object) {
        NSMutableArray array = (NSMutableArray)engageCtrlConventions();
        willChange();
        array.addObject(object);
    }
    public void removeFromEngageCtrlConventions(org.cocktail.kava.client.metier.EOEngageCtrlConvention object) {
        NSMutableArray array = (NSMutableArray)engageCtrlConventions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToEngageCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOEngageCtrlConvention object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_CONVENTIONS_KEY);
    }
    public void removeFromEngageCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOEngageCtrlConvention object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_CONVENTIONS_KEY);
    }
	

    public NSArray engageCtrlPlancos() {
        return (NSArray)storedValueForKey(ENGAGE_CTRL_PLANCOS_KEY);
    }
    public void setEngageCtrlPlancos(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ENGAGE_CTRL_PLANCOS_KEY);
    }
    public void addToEngageCtrlPlancos(org.cocktail.kava.client.metier.EOEngageCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)engageCtrlPlancos();
        willChange();
        array.addObject(object);
    }
    public void removeFromEngageCtrlPlancos(org.cocktail.kava.client.metier.EOEngageCtrlPlanco object) {
        NSMutableArray array = (NSMutableArray)engageCtrlPlancos();
        willChange();
        array.removeObject(object);
    }
	
    public void addToEngageCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOEngageCtrlPlanco object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_PLANCOS_KEY);
    }
    public void removeFromEngageCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOEngageCtrlPlanco object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_PLANCOS_KEY);
    }
	


}

