
// _EOArticlePrestationWeb.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOArticlePrestationWeb.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOArticlePrestationWeb extends EOGenericRecord {

    public static final String ENTITY_NAME = "ArticlePrestationWeb";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.article_prestation_web";

    public static final String ART_CLE_KEY = "artCle";
    public static final String ART_COMMANTAIRE_KEY = "artCommantaire";
    public static final String ART_VALEUR_KEY = "artValeur";

    public static final String ART_CLE_COLKEY = "ART_CLE";
    public static final String ART_COMMANTAIRE_COLKEY = "ART_COMMANTAIRE";
    public static final String ART_VALEUR_COLKEY = "ART_VALEUR";

    public static final String ARTICLE_KEY = "article";




	
    public _EOArticlePrestationWeb() {
        super();
    }




    public String artCle() {
        return (String)storedValueForKey(ART_CLE_KEY);
    }
    public void setArtCle(String aValue) {
        takeStoredValueForKey(aValue, ART_CLE_KEY);
    }

    public String artCommantaire() {
        return (String)storedValueForKey(ART_COMMANTAIRE_KEY);
    }
    public void setArtCommantaire(String aValue) {
        takeStoredValueForKey(aValue, ART_COMMANTAIRE_KEY);
    }

    public String artValeur() {
        return (String)storedValueForKey(ART_VALEUR_KEY);
    }
    public void setArtValeur(String aValue) {
        takeStoredValueForKey(aValue, ART_VALEUR_KEY);
    }




    public org.cocktail.kava.client.metier.EOArticle article() {
        return (org.cocktail.kava.client.metier.EOArticle)storedValueForKey(ARTICLE_KEY);
    }
    public void setArticle(org.cocktail.kava.client.metier.EOArticle aValue) {
        takeStoredValueForKey(aValue, ARTICLE_KEY);
    }
	
    public void setArticleRelationship(org.cocktail.kava.client.metier.EOArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOArticle object = article();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_KEY);
        }
    }





}

