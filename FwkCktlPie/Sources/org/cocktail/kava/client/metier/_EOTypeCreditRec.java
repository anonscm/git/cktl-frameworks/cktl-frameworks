
// _EOTypeCreditRec.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOTypeCreditRec.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOTypeCreditRec extends EOGenericRecord {

    public static final String ENTITY_NAME = "TypeCreditRec";

    public static final String ENTITY_TABLE_NAME = "jefy_admin.type_credit_rec_executoire";

    public static final String LIB_KEY = "lib";
    public static final String TCD_ABREGE_KEY = "tcdAbrege";
    public static final String TCD_BUDGET_KEY = "tcdBudget";
    public static final String TCD_CODE_KEY = "tcdCode";
    public static final String TCD_LIBELLE_KEY = "tcdLibelle";
    public static final String TCD_PRESIDENT_KEY = "tcdPresident";
    public static final String TCD_SECT_KEY = "tcdSect";
    public static final String TCD_TYPE_KEY = "tcdType";

    public static final String TCD_ABREGE_COLKEY = "TCD_ABREGE";
    public static final String TCD_BUDGET_COLKEY = "TCD_BUDGET";
    public static final String TCD_CODE_COLKEY = "TCD_CODE";
    public static final String TCD_LIBELLE_COLKEY = "TCD_LIBELLE";
    public static final String TCD_PRESIDENT_COLKEY = "TCD_PRESIDENT";
    public static final String TCD_SECT_COLKEY = "tcd_sect";
    public static final String TCD_TYPE_COLKEY = "TCD_type";

    public static final String EXERCICE_KEY = "exercice";




	
    public _EOTypeCreditRec() {
        super();
    }




    public String lib() {
        return (String)storedValueForKey(LIB_KEY);
    }
    public void setLib(String aValue) {
        takeStoredValueForKey(aValue, LIB_KEY);
    }

    public String tcdAbrege() {
        return (String)storedValueForKey(TCD_ABREGE_KEY);
    }
    public void setTcdAbrege(String aValue) {
        takeStoredValueForKey(aValue, TCD_ABREGE_KEY);
    }

    public String tcdBudget() {
        return (String)storedValueForKey(TCD_BUDGET_KEY);
    }
    public void setTcdBudget(String aValue) {
        takeStoredValueForKey(aValue, TCD_BUDGET_KEY);
    }

    public String tcdCode() {
        return (String)storedValueForKey(TCD_CODE_KEY);
    }
    public void setTcdCode(String aValue) {
        takeStoredValueForKey(aValue, TCD_CODE_KEY);
    }

    public String tcdLibelle() {
        return (String)storedValueForKey(TCD_LIBELLE_KEY);
    }
    public void setTcdLibelle(String aValue) {
        takeStoredValueForKey(aValue, TCD_LIBELLE_KEY);
    }

    public Number tcdPresident() {
        return (Number)storedValueForKey(TCD_PRESIDENT_KEY);
    }
    public void setTcdPresident(Number aValue) {
        takeStoredValueForKey(aValue, TCD_PRESIDENT_KEY);
    }

    public String tcdSect() {
        return (String)storedValueForKey(TCD_SECT_KEY);
    }
    public void setTcdSect(String aValue) {
        takeStoredValueForKey(aValue, TCD_SECT_KEY);
    }

    public String tcdType() {
        return (String)storedValueForKey(TCD_TYPE_KEY);
    }
    public void setTcdType(String aValue) {
        takeStoredValueForKey(aValue, TCD_TYPE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }





}

