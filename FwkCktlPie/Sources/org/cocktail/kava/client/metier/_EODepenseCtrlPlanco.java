
// _EODepenseCtrlPlanco.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EODepenseCtrlPlanco.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EODepenseCtrlPlanco extends EOGenericRecord {

    public static final String ENTITY_NAME = "DepenseCtrlPlanco";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_depense_ctrl_planco";

    public static final String DPCO_ID_KEY = "dpcoId";
    public static final String PCO_NUM_KEY = "pcoNum";

    public static final String DPCO_ID_COLKEY = "DPCO_ID";
    public static final String PCO_NUM_COLKEY = "PCO_NUM";

    public static final String EXERCICE_KEY = "exercice";
    public static final String MANDAT_KEY = "mandat";




	
    public _EODepenseCtrlPlanco() {
        super();
    }




    public Number dpcoId() {
        return (Number)storedValueForKey(DPCO_ID_KEY);
    }
    public void setDpcoId(Number aValue) {
        takeStoredValueForKey(aValue, DPCO_ID_KEY);
    }

    public String pcoNum() {
        return (String)storedValueForKey(PCO_NUM_KEY);
    }
    public void setPcoNum(String aValue) {
        takeStoredValueForKey(aValue, PCO_NUM_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOMandat mandat() {
        return (org.cocktail.kava.client.metier.EOMandat)storedValueForKey(MANDAT_KEY);
    }
    public void setMandat(org.cocktail.kava.client.metier.EOMandat aValue) {
        takeStoredValueForKey(aValue, MANDAT_KEY);
    }
	
    public void setMandatRelationship(org.cocktail.kava.client.metier.EOMandat value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOMandat object = mandat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, MANDAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
        }
    }





}

