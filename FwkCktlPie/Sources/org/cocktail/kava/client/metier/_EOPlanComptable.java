
// _EOPlanComptable.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPlanComptable.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOPlanComptable extends EOGenericRecord {

    public static final String ENTITY_NAME = "PlanComptable";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_plan_comptable";

    public static final String EXE_ORDRE_KEY = "exeOrdre";
    public static final String PCO_BUDGETAIRE_KEY = "pcoBudgetaire";
    public static final String PCO_EMARGEMENT_KEY = "pcoEmargement";
    public static final String PCO_JBE_KEY = "pcoJBe";
    public static final String PCO_JEXERCICE_KEY = "pcoJExercice";
    public static final String PCO_JFIN_EXERCICE_KEY = "pcoJFinExercice";
    public static final String PCO_LIBELLE_KEY = "pcoLibelle";
    public static final String PCO_NATURE_KEY = "pcoNature";
    public static final String PCO_NIVEAU_KEY = "pcoNiveau";
    public static final String PCO_NUM_KEY = "pcoNum";
    public static final String PCO_VALIDITE_KEY = "pcoValidite";

    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
    public static final String PCO_BUDGETAIRE_COLKEY = "pco_budgetaire";
    public static final String PCO_EMARGEMENT_COLKEY = "pco_emargement";
    public static final String PCO_JBE_COLKEY = "pco_j_be";
    public static final String PCO_JEXERCICE_COLKEY = "pco_j_exercice";
    public static final String PCO_JFIN_EXERCICE_COLKEY = "pco_j_fin_exercice";
    public static final String PCO_LIBELLE_COLKEY = "pco_libelle";
    public static final String PCO_NATURE_COLKEY = "pco_nature";
    public static final String PCO_NIVEAU_COLKEY = "pco_niveau";
    public static final String PCO_NUM_COLKEY = "pco_num";
    public static final String PCO_SENS_EMARGEMENT_COLKEY = "pco_sens_emargement";
    public static final String PCO_VALIDITE_COLKEY = "pco_validite";

    public static final String EXERCICE_KEY = "exercice";

    public static final String PLANCO_CREDIT_DEPS_KEY = "plancoCreditDeps";
    public static final String PLANCO_CREDIT_REC_REAUTIMPS_KEY = "plancoCreditRecReautimps";
    public static final String PLANCO_CREDIT_RECS_KEY = "plancoCreditRecs";



	
    public _EOPlanComptable() {
        super();
    }




    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }

    public String pcoBudgetaire() {
        return (String)storedValueForKey(PCO_BUDGETAIRE_KEY);
    }
    public void setPcoBudgetaire(String aValue) {
        takeStoredValueForKey(aValue, PCO_BUDGETAIRE_KEY);
    }

    public String pcoEmargement() {
        return (String)storedValueForKey(PCO_EMARGEMENT_KEY);
    }
    public void setPcoEmargement(String aValue) {
        takeStoredValueForKey(aValue, PCO_EMARGEMENT_KEY);
    }

    public String pcoJBe() {
        return (String)storedValueForKey(PCO_JBE_KEY);
    }
    public void setPcoJBe(String aValue) {
        takeStoredValueForKey(aValue, PCO_JBE_KEY);
    }

    public String pcoJExercice() {
        return (String)storedValueForKey(PCO_JEXERCICE_KEY);
    }
    public void setPcoJExercice(String aValue) {
        takeStoredValueForKey(aValue, PCO_JEXERCICE_KEY);
    }

    public String pcoJFinExercice() {
        return (String)storedValueForKey(PCO_JFIN_EXERCICE_KEY);
    }
    public void setPcoJFinExercice(String aValue) {
        takeStoredValueForKey(aValue, PCO_JFIN_EXERCICE_KEY);
    }

    public String pcoLibelle() {
        return (String)storedValueForKey(PCO_LIBELLE_KEY);
    }
    public void setPcoLibelle(String aValue) {
        takeStoredValueForKey(aValue, PCO_LIBELLE_KEY);
    }

    public String pcoNature() {
        return (String)storedValueForKey(PCO_NATURE_KEY);
    }
    public void setPcoNature(String aValue) {
        takeStoredValueForKey(aValue, PCO_NATURE_KEY);
    }

    public Number pcoNiveau() {
        return (Number)storedValueForKey(PCO_NIVEAU_KEY);
    }
    public void setPcoNiveau(Number aValue) {
        takeStoredValueForKey(aValue, PCO_NIVEAU_KEY);
    }

    public String pcoNum() {
        return (String)storedValueForKey(PCO_NUM_KEY);
    }
    public void setPcoNum(String aValue) {
        takeStoredValueForKey(aValue, PCO_NUM_KEY);
    }

    public String pcoValidite() {
        return (String)storedValueForKey(PCO_VALIDITE_KEY);
    }
    public void setPcoValidite(String aValue) {
        takeStoredValueForKey(aValue, PCO_VALIDITE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }




    public NSArray plancoCreditDeps() {
        return (NSArray)storedValueForKey(PLANCO_CREDIT_DEPS_KEY);
    }
    public void setPlancoCreditDeps(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PLANCO_CREDIT_DEPS_KEY);
    }
    public void addToPlancoCreditDeps(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
        NSMutableArray array = (NSMutableArray)plancoCreditDeps();
        willChange();
        array.addObject(object);
    }
    public void removeFromPlancoCreditDeps(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
        NSMutableArray array = (NSMutableArray)plancoCreditDeps();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPlancoCreditDepsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_DEPS_KEY);
    }
    public void removeFromPlancoCreditDepsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditDep object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_DEPS_KEY);
    }
	

    public NSArray plancoCreditRecReautimps() {
        return (NSArray)storedValueForKey(PLANCO_CREDIT_REC_REAUTIMPS_KEY);
    }
    public void setPlancoCreditRecReautimps(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
    }
    public void addToPlancoCreditRecReautimps(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
        NSMutableArray array = (NSMutableArray)plancoCreditRecReautimps();
        willChange();
        array.addObject(object);
    }
    public void removeFromPlancoCreditRecReautimps(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
        NSMutableArray array = (NSMutableArray)plancoCreditRecReautimps();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPlancoCreditRecReautimpsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
    }
    public void removeFromPlancoCreditRecReautimpsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRecReautimp object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_REC_REAUTIMPS_KEY);
    }
	

    public NSArray plancoCreditRecs() {
        return (NSArray)storedValueForKey(PLANCO_CREDIT_RECS_KEY);
    }
    public void setPlancoCreditRecs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PLANCO_CREDIT_RECS_KEY);
    }
    public void addToPlancoCreditRecs(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
        NSMutableArray array = (NSMutableArray)plancoCreditRecs();
        willChange();
        array.addObject(object);
    }
    public void removeFromPlancoCreditRecs(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
        NSMutableArray array = (NSMutableArray)plancoCreditRecs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPlancoCreditRecsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_RECS_KEY);
    }
    public void removeFromPlancoCreditRecsRelationship(org.cocktail.kava.client.metier.EOPlancoCreditRec object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PLANCO_CREDIT_RECS_KEY);
    }
	


}

