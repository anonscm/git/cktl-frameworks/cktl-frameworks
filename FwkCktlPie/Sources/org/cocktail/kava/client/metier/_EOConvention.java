
// _EOConvention.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOConvention.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOConvention extends EOGenericRecord {

    public static final String ENTITY_NAME = "Convention";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_convention";

    public static final String CON_INDEX_KEY = "conIndex";
    public static final String CON_OBJET_KEY = "conObjet";
    public static final String CON_OBJET_COURT_KEY = "conObjetCourt";
    public static final String CON_REFERENCE_EXTERNE_KEY = "conReferenceExterne";
    public static final String CON_SUPPR_KEY = "conSuppr";

    public static final String CON_INDEX_COLKEY = "CON_INDEX";
    public static final String CON_OBJET_COLKEY = "CON_OBJET";
    public static final String CON_OBJET_COURT_COLKEY = "CON_OBJET_COURT";
    public static final String CON_REFERENCE_EXTERNE_COLKEY = "CON_REFERENCE_EXTERNE";
    public static final String CON_SUPPR_COLKEY = "CON_SUPPR";

    public static final String EXERCICE_KEY = "exercice";

    public static final String CONVENTION_LIMITATIVES_KEY = "conventionLimitatives";
    public static final String CONVENTION_NON_LIMITATIVES_KEY = "conventionNonLimitatives";



	
    public _EOConvention() {
        super();
    }




    public Number conIndex() {
        return (Number)storedValueForKey(CON_INDEX_KEY);
    }
    public void setConIndex(Number aValue) {
        takeStoredValueForKey(aValue, CON_INDEX_KEY);
    }

    public String conObjet() {
        return (String)storedValueForKey(CON_OBJET_KEY);
    }
    public void setConObjet(String aValue) {
        takeStoredValueForKey(aValue, CON_OBJET_KEY);
    }

    public String conObjetCourt() {
        return (String)storedValueForKey(CON_OBJET_COURT_KEY);
    }
    public void setConObjetCourt(String aValue) {
        takeStoredValueForKey(aValue, CON_OBJET_COURT_KEY);
    }

    public String conReferenceExterne() {
        return (String)storedValueForKey(CON_REFERENCE_EXTERNE_KEY);
    }
    public void setConReferenceExterne(String aValue) {
        takeStoredValueForKey(aValue, CON_REFERENCE_EXTERNE_KEY);
    }

    public String conSuppr() {
        return (String)storedValueForKey(CON_SUPPR_KEY);
    }
    public void setConSuppr(String aValue) {
        takeStoredValueForKey(aValue, CON_SUPPR_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }




    public NSArray conventionLimitatives() {
        return (NSArray)storedValueForKey(CONVENTION_LIMITATIVES_KEY);
    }
    public void setConventionLimitatives(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CONVENTION_LIMITATIVES_KEY);
    }
    public void addToConventionLimitatives(org.cocktail.kava.client.metier.EOConventionLimitative object) {
        NSMutableArray array = (NSMutableArray)conventionLimitatives();
        willChange();
        array.addObject(object);
    }
    public void removeFromConventionLimitatives(org.cocktail.kava.client.metier.EOConventionLimitative object) {
        NSMutableArray array = (NSMutableArray)conventionLimitatives();
        willChange();
        array.removeObject(object);
    }
	
    public void addToConventionLimitativesRelationship(org.cocktail.kava.client.metier.EOConventionLimitative object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
    }
    public void removeFromConventionLimitativesRelationship(org.cocktail.kava.client.metier.EOConventionLimitative object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
    }
	

    public NSArray conventionNonLimitatives() {
        return (NSArray)storedValueForKey(CONVENTION_NON_LIMITATIVES_KEY);
    }
    public void setConventionNonLimitatives(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, CONVENTION_NON_LIMITATIVES_KEY);
    }
    public void addToConventionNonLimitatives(org.cocktail.kava.client.metier.EOConventionNonLimitative object) {
        NSMutableArray array = (NSMutableArray)conventionNonLimitatives();
        willChange();
        array.addObject(object);
    }
    public void removeFromConventionNonLimitatives(org.cocktail.kava.client.metier.EOConventionNonLimitative object) {
        NSMutableArray array = (NSMutableArray)conventionNonLimitatives();
        willChange();
        array.removeObject(object);
    }
	
    public void addToConventionNonLimitativesRelationship(org.cocktail.kava.client.metier.EOConventionNonLimitative object) {
        addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
    }
    public void removeFromConventionNonLimitativesRelationship(org.cocktail.kava.client.metier.EOConventionNonLimitative object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
    }
	


}

