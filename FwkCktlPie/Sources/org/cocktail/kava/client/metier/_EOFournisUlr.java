
// _EOFournisUlr.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOFournisUlr.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFournisUlr extends EOGenericRecord {

    public static final String ENTITY_NAME = "FournisUlr";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_fournis_ulr";

    public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
    public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
    public static final String ADR_CIVILITE_KEY = "adrCivilite";
    public static final String ADR_CP_KEY = "adrCp";
    public static final String ADR_NOM_KEY = "adrNom";
    public static final String ADR_PRENOM_KEY = "adrPrenom";
    public static final String ADR_VILLE_KEY = "adrVille";
    public static final String AGT_ORDRE_KEY = "agtOrdre";
    public static final String CPT_ORDRE_KEY = "cptOrdre";
    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String FOU_CODE_KEY = "fouCode";
    public static final String FOU_DATE_KEY = "fouDate";
    public static final String FOU_ETRANGER_KEY = "fouEtranger";
    public static final String FOU_MARCHE_KEY = "fouMarche";
    public static final String FOU_TYPE_KEY = "fouType";
    public static final String FOU_VALIDE_KEY = "fouValide";
    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
    public static final String SIRET_KEY = "siret";

    public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
    public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
    public static final String ADR_CIVILITE_COLKEY = "ADR_CIVILITE";
    public static final String ADR_CP_COLKEY = "ADR_CP";
    public static final String ADR_NOM_COLKEY = "ADR_NOM";
    public static final String ADR_PRENOM_COLKEY = "ADR_PRENOM";
    public static final String ADR_VILLE_COLKEY = "ADR_VILLE";
    public static final String AGT_ORDRE_COLKEY = "AGT_ORDRE";
    public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String FOU_CODE_COLKEY = "FOU_CODE";
    public static final String FOU_DATE_COLKEY = "FOU_DATE";
    public static final String FOU_ETRANGER_COLKEY = "FOU_ETRANGER";
    public static final String FOU_MARCHE_COLKEY = "FOU_MARCHE";
    public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
    public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
    public static final String SIRET_COLKEY = "SIRET";

    public static final String ADRESSE_KEY = "adresse";
    public static final String PERSONNE_KEY = "personne";

    public static final String RIBFOUR_ULRS_KEY = "ribfourUlrs";



	
    public _EOFournisUlr() {
        super();
    }




    public String adrAdresse1() {
        return (String)storedValueForKey(ADR_ADRESSE1_KEY);
    }
    public void setAdrAdresse1(String aValue) {
        takeStoredValueForKey(aValue, ADR_ADRESSE1_KEY);
    }

    public String adrAdresse2() {
        return (String)storedValueForKey(ADR_ADRESSE2_KEY);
    }
    public void setAdrAdresse2(String aValue) {
        takeStoredValueForKey(aValue, ADR_ADRESSE2_KEY);
    }

    public String adrCivilite() {
        return (String)storedValueForKey(ADR_CIVILITE_KEY);
    }
    public void setAdrCivilite(String aValue) {
        takeStoredValueForKey(aValue, ADR_CIVILITE_KEY);
    }

    public String adrCp() {
        return (String)storedValueForKey(ADR_CP_KEY);
    }
    public void setAdrCp(String aValue) {
        takeStoredValueForKey(aValue, ADR_CP_KEY);
    }

    public String adrNom() {
        return (String)storedValueForKey(ADR_NOM_KEY);
    }
    public void setAdrNom(String aValue) {
        takeStoredValueForKey(aValue, ADR_NOM_KEY);
    }

    public String adrPrenom() {
        return (String)storedValueForKey(ADR_PRENOM_KEY);
    }
    public void setAdrPrenom(String aValue) {
        takeStoredValueForKey(aValue, ADR_PRENOM_KEY);
    }

    public String adrVille() {
        return (String)storedValueForKey(ADR_VILLE_KEY);
    }
    public void setAdrVille(String aValue) {
        takeStoredValueForKey(aValue, ADR_VILLE_KEY);
    }

    public Number agtOrdre() {
        return (Number)storedValueForKey(AGT_ORDRE_KEY);
    }
    public void setAgtOrdre(Number aValue) {
        takeStoredValueForKey(aValue, AGT_ORDRE_KEY);
    }

    public Number cptOrdre() {
        return (Number)storedValueForKey(CPT_ORDRE_KEY);
    }
    public void setCptOrdre(Number aValue) {
        takeStoredValueForKey(aValue, CPT_ORDRE_KEY);
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public String fouCode() {
        return (String)storedValueForKey(FOU_CODE_KEY);
    }
    public void setFouCode(String aValue) {
        takeStoredValueForKey(aValue, FOU_CODE_KEY);
    }

    public NSTimestamp fouDate() {
        return (NSTimestamp)storedValueForKey(FOU_DATE_KEY);
    }
    public void setFouDate(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, FOU_DATE_KEY);
    }

    public String fouEtranger() {
        return (String)storedValueForKey(FOU_ETRANGER_KEY);
    }
    public void setFouEtranger(String aValue) {
        takeStoredValueForKey(aValue, FOU_ETRANGER_KEY);
    }

    public String fouMarche() {
        return (String)storedValueForKey(FOU_MARCHE_KEY);
    }
    public void setFouMarche(String aValue) {
        takeStoredValueForKey(aValue, FOU_MARCHE_KEY);
    }

    public String fouType() {
        return (String)storedValueForKey(FOU_TYPE_KEY);
    }
    public void setFouType(String aValue) {
        takeStoredValueForKey(aValue, FOU_TYPE_KEY);
    }

    public String fouValide() {
        return (String)storedValueForKey(FOU_VALIDE_KEY);
    }
    public void setFouValide(String aValue) {
        takeStoredValueForKey(aValue, FOU_VALIDE_KEY);
    }

    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public String siret() {
        return (String)storedValueForKey(SIRET_KEY);
    }
    public void setSiret(String aValue) {
        takeStoredValueForKey(aValue, SIRET_KEY);
    }




    public org.cocktail.kava.client.metier.EOAdresse adresse() {
        return (org.cocktail.kava.client.metier.EOAdresse)storedValueForKey(ADRESSE_KEY);
    }
    public void setAdresse(org.cocktail.kava.client.metier.EOAdresse aValue) {
        takeStoredValueForKey(aValue, ADRESSE_KEY);
    }
	
    public void setAdresseRelationship(org.cocktail.kava.client.metier.EOAdresse value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOAdresse object = adresse();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ADRESSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }




    public NSArray ribfourUlrs() {
        return (NSArray)storedValueForKey(RIBFOUR_ULRS_KEY);
    }
    public void setRibfourUlrs(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, RIBFOUR_ULRS_KEY);
    }
    public void addToRibfourUlrs(org.cocktail.kava.client.metier.EORibfourUlr object) {
        NSMutableArray array = (NSMutableArray)ribfourUlrs();
        willChange();
        array.addObject(object);
    }
    public void removeFromRibfourUlrs(org.cocktail.kava.client.metier.EORibfourUlr object) {
        NSMutableArray array = (NSMutableArray)ribfourUlrs();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRibfourUlrsRelationship(org.cocktail.kava.client.metier.EORibfourUlr object) {
        addObjectToBothSidesOfRelationshipWithKey(object, RIBFOUR_ULRS_KEY);
    }
    public void removeFromRibfourUlrsRelationship(org.cocktail.kava.client.metier.EORibfourUlr object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, RIBFOUR_ULRS_KEY);
    }
	


}

