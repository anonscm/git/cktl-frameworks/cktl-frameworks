
// _EOOrganSignataire.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOOrganSignataire.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOOrganSignataire extends EOGenericRecord {

    public static final String ENTITY_NAME = "OrganSignataire";

    public static final String ENTITY_TABLE_NAME = "jefy_admin.organ_signataire";

    public static final String ORSI_DATE_CLOTURE_KEY = "orsiDateCloture";
    public static final String ORSI_DATE_OUVERTURE_KEY = "orsiDateOuverture";
    public static final String ORSI_LIBELLE_SIGNATAIRE_KEY = "orsiLibelleSignataire";
    public static final String ORSI_REFERENCE_DELEGATION_KEY = "orsiReferenceDelegation";
    public static final String TYSI_ID_KEY = "tysiId";

    public static final String ORSI_DATE_CLOTURE_COLKEY = "ORSI_DATE_CLOTURE";
    public static final String ORSI_DATE_OUVERTURE_COLKEY = "ORSI_DATE_OUVERTURE";
    public static final String ORSI_LIBELLE_SIGNATAIRE_COLKEY = "ORSI_LIBELLE_SIGNATAIRE";
    public static final String ORSI_REFERENCE_DELEGATION_COLKEY = "ORSI_REFERENCE_DELEGATION";
    public static final String TYSI_ID_COLKEY = "TYSI_ID";

    public static final String INDIVIDU_ULR_KEY = "individuUlr";
    public static final String ORGAN_KEY = "organ";




	
    public _EOOrganSignataire() {
        super();
    }




    public NSTimestamp orsiDateCloture() {
        return (NSTimestamp)storedValueForKey(ORSI_DATE_CLOTURE_KEY);
    }
    public void setOrsiDateCloture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ORSI_DATE_CLOTURE_KEY);
    }

    public NSTimestamp orsiDateOuverture() {
        return (NSTimestamp)storedValueForKey(ORSI_DATE_OUVERTURE_KEY);
    }
    public void setOrsiDateOuverture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, ORSI_DATE_OUVERTURE_KEY);
    }

    public String orsiLibelleSignataire() {
        return (String)storedValueForKey(ORSI_LIBELLE_SIGNATAIRE_KEY);
    }
    public void setOrsiLibelleSignataire(String aValue) {
        takeStoredValueForKey(aValue, ORSI_LIBELLE_SIGNATAIRE_KEY);
    }

    public String orsiReferenceDelegation() {
        return (String)storedValueForKey(ORSI_REFERENCE_DELEGATION_KEY);
    }
    public void setOrsiReferenceDelegation(String aValue) {
        takeStoredValueForKey(aValue, ORSI_REFERENCE_DELEGATION_KEY);
    }

    public Number tysiId() {
        return (Number)storedValueForKey(TYSI_ID_KEY);
    }
    public void setTysiId(Number aValue) {
        takeStoredValueForKey(aValue, TYSI_ID_KEY);
    }




    public org.cocktail.kava.client.metier.EOIndividuUlr individuUlr() {
        return (org.cocktail.kava.client.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
    }
    public void setIndividuUlr(org.cocktail.kava.client.metier.EOIndividuUlr aValue) {
        takeStoredValueForKey(aValue, INDIVIDU_ULR_KEY);
    }
	
    public void setIndividuUlrRelationship(org.cocktail.kava.client.metier.EOIndividuUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOIndividuUlr object = individuUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, INDIVIDU_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }





}

