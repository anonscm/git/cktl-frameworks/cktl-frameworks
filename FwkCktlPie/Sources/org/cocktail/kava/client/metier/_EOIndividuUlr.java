
// _EOIndividuUlr.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOIndividuUlr.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOIndividuUlr extends EOGenericRecord {

    public static final String ENTITY_NAME = "IndividuUlr";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_individu_ulr";

    public static final String C_CIVILITE_KEY = "cCivilite";
    public static final String D_NAISSANCE_KEY = "dNaissance";
    public static final String IND_CLE_INSEE_KEY = "indCleInsee";
    public static final String IND_NO_INSEE_KEY = "indNoInsee";
    public static final String NO_INDIVIDU_KEY = "noIndividu";
    public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
    public static final String NOM_USUEL_KEY = "nomUsuel";
    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
    public static final String PRENOM_KEY = "prenom";
    public static final String TEM_VALIDE_KEY = "temValide";

    public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
    public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
    public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
    public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
    public static final String NO_INDIVIDU_COLKEY = "no_individu";
    public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
    public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
    public static final String PRENOM_COLKEY = "PRENOM";
    public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

    public static final String PERSONNE_KEY = "personne";

    public static final String REPART_STRUCTURES_KEY = "repartStructures";



	
    public _EOIndividuUlr() {
        super();
    }




    public String cCivilite() {
        return (String)storedValueForKey(C_CIVILITE_KEY);
    }
    public void setCCivilite(String aValue) {
        takeStoredValueForKey(aValue, C_CIVILITE_KEY);
    }

    public NSTimestamp dNaissance() {
        return (NSTimestamp)storedValueForKey(D_NAISSANCE_KEY);
    }
    public void setDNaissance(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_NAISSANCE_KEY);
    }

    public Number indCleInsee() {
        return (Number)storedValueForKey(IND_CLE_INSEE_KEY);
    }
    public void setIndCleInsee(Number aValue) {
        takeStoredValueForKey(aValue, IND_CLE_INSEE_KEY);
    }

    public String indNoInsee() {
        return (String)storedValueForKey(IND_NO_INSEE_KEY);
    }
    public void setIndNoInsee(String aValue) {
        takeStoredValueForKey(aValue, IND_NO_INSEE_KEY);
    }

    public Number noIndividu() {
        return (Number)storedValueForKey(NO_INDIVIDU_KEY);
    }
    public void setNoIndividu(Number aValue) {
        takeStoredValueForKey(aValue, NO_INDIVIDU_KEY);
    }

    public String nomPatronymique() {
        return (String)storedValueForKey(NOM_PATRONYMIQUE_KEY);
    }
    public void setNomPatronymique(String aValue) {
        takeStoredValueForKey(aValue, NOM_PATRONYMIQUE_KEY);
    }

    public String nomUsuel() {
        return (String)storedValueForKey(NOM_USUEL_KEY);
    }
    public void setNomUsuel(String aValue) {
        takeStoredValueForKey(aValue, NOM_USUEL_KEY);
    }

    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public String prenom() {
        return (String)storedValueForKey(PRENOM_KEY);
    }
    public void setPrenom(String aValue) {
        takeStoredValueForKey(aValue, PRENOM_KEY);
    }

    public String temValide() {
        return (String)storedValueForKey(TEM_VALIDE_KEY);
    }
    public void setTemValide(String aValue) {
        takeStoredValueForKey(aValue, TEM_VALIDE_KEY);
    }




    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }




    public NSArray repartStructures() {
        return (NSArray)storedValueForKey(REPART_STRUCTURES_KEY);
    }
    public void setRepartStructures(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, REPART_STRUCTURES_KEY);
    }
    public void addToRepartStructures(org.cocktail.kava.client.metier.EORepartStructure object) {
        NSMutableArray array = (NSMutableArray)repartStructures();
        willChange();
        array.addObject(object);
    }
    public void removeFromRepartStructures(org.cocktail.kava.client.metier.EORepartStructure object) {
        NSMutableArray array = (NSMutableArray)repartStructures();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
        addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    }
    public void removeFromRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    }
	


}

