// EOFournisUlr.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOFournisUlr extends _EOFournisUlr {

	public static final String	PRIMARY_KEY_KEY			= "fouOrdre";

	public static final String	FOU_ETAT_VALIDE			= "O";
	public static final String	FOU_TYPE_FOURNISSEUR	= "F";
	public static final String	FOU_TYPE_CLIENT			= "C";
	public static final String	FOU_TYPE_TOUS			= "T";

	public EOFournisUlr() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
