
// _EOPiEngFac.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPiEngFac.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOPiEngFac extends EOGenericRecord {

    public static final String ENTITY_NAME = "PiEngFac";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.pi_eng_fac";

    public static final String D_CREATION_KEY = "dCreation";
    public static final String PREST_ID_KEY = "prestId";

    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String PREST_ID_COLKEY = "PREST_ID";

    public static final String ENGAGE_BUDGET_KEY = "engageBudget";
    public static final String FACTURE_KEY = "facture";




	
    public _EOPiEngFac() {
        super();
    }




    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public Number prestId() {
        return (Number)storedValueForKey(PREST_ID_KEY);
    }
    public void setPrestId(Number aValue) {
        takeStoredValueForKey(aValue, PREST_ID_KEY);
    }




    public org.cocktail.kava.client.metier.EOEngageBudget engageBudget() {
        return (org.cocktail.kava.client.metier.EOEngageBudget)storedValueForKey(ENGAGE_BUDGET_KEY);
    }
    public void setEngageBudget(org.cocktail.kava.client.metier.EOEngageBudget aValue) {
        takeStoredValueForKey(aValue, ENGAGE_BUDGET_KEY);
    }
	
    public void setEngageBudgetRelationship(org.cocktail.kava.client.metier.EOEngageBudget value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOEngageBudget object = engageBudget();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_BUDGET_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_BUDGET_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOFacture facture() {
        return (org.cocktail.kava.client.metier.EOFacture)storedValueForKey(FACTURE_KEY);
    }
    public void setFacture(org.cocktail.kava.client.metier.EOFacture aValue) {
        takeStoredValueForKey(aValue, FACTURE_KEY);
    }
	
    public void setFactureRelationship(org.cocktail.kava.client.metier.EOFacture value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOFacture object = facture();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_KEY);
        }
    }





}

