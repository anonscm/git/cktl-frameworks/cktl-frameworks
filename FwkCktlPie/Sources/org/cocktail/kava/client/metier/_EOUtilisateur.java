
// _EOUtilisateur.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOUtilisateur extends EOGenericRecord {

    public static final String ENTITY_NAME = "Utilisateur";

    public static final String ENTITY_TABLE_NAME = "jefy_admin.utilisateur";

    public static final String PERS_ID_KEY = "persId";
    public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
    public static final String UTL_FERMETURE_KEY = "utlFermeture";
    public static final String UTL_OUVERTURE_KEY = "utlOuverture";

    public static final String PERS_ID_COLKEY = "PERS_ID";
    public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
    public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

    public static final String PERSONNE_KEY = "personne";
    public static final String TYPE_ETAT_KEY = "typeEtat";

    public static final String UTILISATEUR_FONCTIONS_KEY = "utilisateurFonctions";



	
    public _EOUtilisateur() {
        super();
    }




    public Number persId() {
        return (Number)storedValueForKey(PERS_ID_KEY);
    }
    public void setPersId(Number aValue) {
        takeStoredValueForKey(aValue, PERS_ID_KEY);
    }

    public String personne_persNomPrenom() {
        return (String)storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
    }
    public void setPersonne_persNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERSONNE_PERS_NOM_PRENOM_KEY);
    }

    public NSTimestamp utlFermeture() {
        return (NSTimestamp)storedValueForKey(UTL_FERMETURE_KEY);
    }
    public void setUtlFermeture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, UTL_FERMETURE_KEY);
    }

    public NSTimestamp utlOuverture() {
        return (NSTimestamp)storedValueForKey(UTL_OUVERTURE_KEY);
    }
    public void setUtlOuverture(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, UTL_OUVERTURE_KEY);
    }




    public org.cocktail.kava.client.metier.EOPersonne personne() {
        return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
    }
    public void setPersonne(org.cocktail.kava.client.metier.EOPersonne aValue) {
        takeStoredValueForKey(aValue, PERSONNE_KEY);
    }
	
    public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPersonne object = personne();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }




    public NSArray utilisateurFonctions() {
        return (NSArray)storedValueForKey(UTILISATEUR_FONCTIONS_KEY);
    }
    public void setUtilisateurFonctions(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_FONCTIONS_KEY);
    }
    public void addToUtilisateurFonctions(org.cocktail.kava.client.metier.EOUtilisateurFonction object) {
        NSMutableArray array = (NSMutableArray)utilisateurFonctions();
        willChange();
        array.addObject(object);
    }
    public void removeFromUtilisateurFonctions(org.cocktail.kava.client.metier.EOUtilisateurFonction object) {
        NSMutableArray array = (NSMutableArray)utilisateurFonctions();
        willChange();
        array.removeObject(object);
    }
	
    public void addToUtilisateurFonctionsRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonction object) {
        addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
    }
    public void removeFromUtilisateurFonctionsRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonction object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
    }
	


}

