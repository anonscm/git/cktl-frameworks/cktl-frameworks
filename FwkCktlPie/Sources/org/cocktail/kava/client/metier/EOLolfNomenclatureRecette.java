
// EOLolfNomenclatureRecette.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOLolfNomenclatureRecette extends _EOLolfNomenclatureRecette {

	public static final String	PRIMARY_KEY_KEY	= "lolfId";

	public EOLolfNomenclatureRecette() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
