
// _EOGestion.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOGestion.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOGestion extends EOGenericRecord {

    public static final String ENTITY_NAME = "Gestion";

    public static final String ENTITY_TABLE_NAME = "maracuja.gestion";

    public static final String GES_CODE_KEY = "gesCode";

    public static final String GES_CODE_COLKEY = "GES_CODE";

    public static final String COMPTABILITE_KEY = "comptabilite";

    public static final String GESTION_EXERCICES_KEY = "gestionExercices";



	
    public _EOGestion() {
        super();
    }




    public String gesCode() {
        return (String)storedValueForKey(GES_CODE_KEY);
    }
    public void setGesCode(String aValue) {
        takeStoredValueForKey(aValue, GES_CODE_KEY);
    }




    public org.cocktail.kava.client.metier.EOComptabilite comptabilite() {
        return (org.cocktail.kava.client.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
    }
    public void setComptabilite(org.cocktail.kava.client.metier.EOComptabilite aValue) {
        takeStoredValueForKey(aValue, COMPTABILITE_KEY);
    }
	
    public void setComptabiliteRelationship(org.cocktail.kava.client.metier.EOComptabilite value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOComptabilite object = comptabilite();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, COMPTABILITE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
        }
    }




    public NSArray gestionExercices() {
        return (NSArray)storedValueForKey(GESTION_EXERCICES_KEY);
    }
    public void setGestionExercices(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, GESTION_EXERCICES_KEY);
    }
    public void addToGestionExercices(org.cocktail.kava.client.metier.EOGestionExercice object) {
        NSMutableArray array = (NSMutableArray)gestionExercices();
        willChange();
        array.addObject(object);
    }
    public void removeFromGestionExercices(org.cocktail.kava.client.metier.EOGestionExercice object) {
        NSMutableArray array = (NSMutableArray)gestionExercices();
        willChange();
        array.removeObject(object);
    }
	
    public void addToGestionExercicesRelationship(org.cocktail.kava.client.metier.EOGestionExercice object) {
        addObjectToBothSidesOfRelationshipWithKey(object, GESTION_EXERCICES_KEY);
    }
    public void removeFromGestionExercicesRelationship(org.cocktail.kava.client.metier.EOGestionExercice object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, GESTION_EXERCICES_KEY);
    }
	


}

