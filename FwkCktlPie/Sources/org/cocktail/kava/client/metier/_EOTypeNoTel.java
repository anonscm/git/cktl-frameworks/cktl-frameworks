
// _EOTypeNoTel.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOTypeNoTel.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOTypeNoTel extends EOGenericRecord {

    public static final String ENTITY_NAME = "TypeNoTel";

    public static final String ENTITY_TABLE_NAME = "grhum.type_no_tel";

    public static final String D_CREATION_KEY = "dCreation";
    public static final String D_MODIFICATION_KEY = "dModification";
    public static final String L_TYPE_NO_TEL_KEY = "lTypeNoTel";

    public static final String D_CREATION_COLKEY = "D_CREATION";
    public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
    public static final String L_TYPE_NO_TEL_COLKEY = "L_TYPE_NO_TEL";





	
    public _EOTypeNoTel() {
        super();
    }




    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey(D_CREATION_KEY);
    }
    public void setDCreation(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_CREATION_KEY);
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey(D_MODIFICATION_KEY);
    }
    public void setDModification(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, D_MODIFICATION_KEY);
    }

    public String lTypeNoTel() {
        return (String)storedValueForKey(L_TYPE_NO_TEL_KEY);
    }
    public void setLTypeNoTel(String aValue) {
        takeStoredValueForKey(aValue, L_TYPE_NO_TEL_KEY);
    }








}

