
// _EOEngageCtrlAction.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOEngageCtrlAction.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOEngageCtrlAction extends EOGenericRecord {

    public static final String ENTITY_NAME = "EngageCtrlAction";

    public static final String ENTITY_TABLE_NAME = "jefy_depense.engage_ctrl_action";

    public static final String EACT_DATE_SAISIE_KEY = "eactDateSaisie";
    public static final String EACT_HT_SAISIE_KEY = "eactHtSaisie";
    public static final String EACT_MONTANT_BUDGETAIRE_KEY = "eactMontantBudgetaire";
    public static final String EACT_MONTANT_BUDGETAIRE_RESTE_KEY = "eactMontantBudgetaireReste";
    public static final String EACT_TTC_SAISIE_KEY = "eactTtcSaisie";
    public static final String EACT_TVA_SAISIE_KEY = "eactTvaSaisie";
    public static final String EXE_ORDRE_KEY = "exeOrdre";

    public static final String EACT_DATE_SAISIE_COLKEY = "EACT_DATE_SAISIE";
    public static final String EACT_HT_SAISIE_COLKEY = "EACT_HT_SAISIE";
    public static final String EACT_MONTANT_BUDGETAIRE_COLKEY = "EACT_MONTANT_BUDGETAIRE";
    public static final String EACT_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EACT_MONTANT_BUDGETAIRE_RESTE";
    public static final String EACT_TTC_SAISIE_COLKEY = "EACT_TTC_SAISIE";
    public static final String EACT_TVA_SAISIE_COLKEY = "EACT_TVA_SAISIE";
    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

    public static final String LOLF_NOMENCLATURE_DEPENSE_KEY = "lolfNomenclatureDepense";




	
    public _EOEngageCtrlAction() {
        super();
    }




    public NSTimestamp eactDateSaisie() {
        return (NSTimestamp)storedValueForKey(EACT_DATE_SAISIE_KEY);
    }
    public void setEactDateSaisie(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, EACT_DATE_SAISIE_KEY);
    }

    public BigDecimal eactHtSaisie() {
        return (BigDecimal)storedValueForKey(EACT_HT_SAISIE_KEY);
    }
    public void setEactHtSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EACT_HT_SAISIE_KEY);
    }

    public BigDecimal eactMontantBudgetaire() {
        return (BigDecimal)storedValueForKey(EACT_MONTANT_BUDGETAIRE_KEY);
    }
    public void setEactMontantBudgetaire(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EACT_MONTANT_BUDGETAIRE_KEY);
    }

    public BigDecimal eactMontantBudgetaireReste() {
        return (BigDecimal)storedValueForKey(EACT_MONTANT_BUDGETAIRE_RESTE_KEY);
    }
    public void setEactMontantBudgetaireReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EACT_MONTANT_BUDGETAIRE_RESTE_KEY);
    }

    public BigDecimal eactTtcSaisie() {
        return (BigDecimal)storedValueForKey(EACT_TTC_SAISIE_KEY);
    }
    public void setEactTtcSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EACT_TTC_SAISIE_KEY);
    }

    public BigDecimal eactTvaSaisie() {
        return (BigDecimal)storedValueForKey(EACT_TVA_SAISIE_KEY);
    }
    public void setEactTvaSaisie(BigDecimal aValue) {
        takeStoredValueForKey(aValue, EACT_TVA_SAISIE_KEY);
    }

    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }




    public org.cocktail.kava.client.metier.EOLolfNomenclatureDepense lolfNomenclatureDepense() {
        return (org.cocktail.kava.client.metier.EOLolfNomenclatureDepense)storedValueForKey(LOLF_NOMENCLATURE_DEPENSE_KEY);
    }
    public void setLolfNomenclatureDepense(org.cocktail.kava.client.metier.EOLolfNomenclatureDepense aValue) {
        takeStoredValueForKey(aValue, LOLF_NOMENCLATURE_DEPENSE_KEY);
    }
	
    public void setLolfNomenclatureDepenseRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureDepense value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOLolfNomenclatureDepense object = lolfNomenclatureDepense();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, LOLF_NOMENCLATURE_DEPENSE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_DEPENSE_KEY);
        }
    }





}

