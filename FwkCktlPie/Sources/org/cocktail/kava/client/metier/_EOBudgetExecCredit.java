
// _EOBudgetExecCredit.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOBudgetExecCredit.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOBudgetExecCredit extends EOGenericRecord {

    public static final String ENTITY_NAME = "BudgetExecCredit";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_budget_exec_credit";

    public static final String BDXC_CONVENTIONS_KEY = "bdxcConventions";
    public static final String BDXC_CREDITS_KEY = "bdxcCredits";
    public static final String BDXC_DBMS_KEY = "bdxcDbms";
    public static final String BDXC_DEBITS_KEY = "bdxcDebits";
    public static final String BDXC_DISPO_RESERVE_KEY = "bdxcDispoReserve";
    public static final String BDXC_DISPONIBLE_KEY = "bdxcDisponible";
    public static final String BDXC_ENGAGEMENTS_KEY = "bdxcEngagements";
    public static final String BDXC_MANDATS_KEY = "bdxcMandats";
    public static final String BDXC_OUVERTS_KEY = "bdxcOuverts";
    public static final String BDXC_PRIMITIFS_KEY = "bdxcPrimitifs";
    public static final String BDXC_PROVISOIRES_KEY = "bdxcProvisoires";
    public static final String BDXC_RELIQUATS_KEY = "bdxcReliquats";
    public static final String BDXC_RESERVE_KEY = "bdxcReserve";
    public static final String BDXC_VOTES_KEY = "bdxcVotes";

    public static final String BDXC_CONVENTIONS_COLKEY = "BDXC_CONVENTIONS";
    public static final String BDXC_CREDITS_COLKEY = "BDXC_CREDITS";
    public static final String BDXC_DBMS_COLKEY = "BDXC_DBMS";
    public static final String BDXC_DEBITS_COLKEY = "BDXC_DEBITS";
    public static final String BDXC_DISPO_RESERVE_COLKEY = "BDXC_DISPO_RESERVE";
    public static final String BDXC_DISPONIBLE_COLKEY = "BDXC_DISPONIBLE";
    public static final String BDXC_ENGAGEMENTS_COLKEY = "BDXC_ENGAGEMENTS";
    public static final String BDXC_MANDATS_COLKEY = "BDXC_MANDATS";
    public static final String BDXC_OUVERTS_COLKEY = "BDXC_OUVERTS";
    public static final String BDXC_PRIMITIFS_COLKEY = "BDXC_PRIMITIFS";
    public static final String BDXC_PROVISOIRES_COLKEY = "BDXC_PROVISOIRES";
    public static final String BDXC_RELIQUATS_COLKEY = "BDXC_RELIQUATS";
    public static final String BDXC_RESERVE_COLKEY = "BDXC_RESERVE";
    public static final String BDXC_VOTES_COLKEY = "BDXC_VOTES";

    public static final String EXERCICE_KEY = "exercice";
    public static final String ORGAN_KEY = "organ";
    public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";




	
    public _EOBudgetExecCredit() {
        super();
    }




    public BigDecimal bdxcConventions() {
        return (BigDecimal)storedValueForKey(BDXC_CONVENTIONS_KEY);
    }
    public void setBdxcConventions(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_CONVENTIONS_KEY);
    }

    public BigDecimal bdxcCredits() {
        return (BigDecimal)storedValueForKey(BDXC_CREDITS_KEY);
    }
    public void setBdxcCredits(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_CREDITS_KEY);
    }

    public BigDecimal bdxcDbms() {
        return (BigDecimal)storedValueForKey(BDXC_DBMS_KEY);
    }
    public void setBdxcDbms(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_DBMS_KEY);
    }

    public BigDecimal bdxcDebits() {
        return (BigDecimal)storedValueForKey(BDXC_DEBITS_KEY);
    }
    public void setBdxcDebits(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_DEBITS_KEY);
    }

    public Number bdxcDispoReserve() {
        return (Number)storedValueForKey(BDXC_DISPO_RESERVE_KEY);
    }
    public void setBdxcDispoReserve(Number aValue) {
        takeStoredValueForKey(aValue, BDXC_DISPO_RESERVE_KEY);
    }

    public BigDecimal bdxcDisponible() {
        return (BigDecimal)storedValueForKey(BDXC_DISPONIBLE_KEY);
    }
    public void setBdxcDisponible(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_DISPONIBLE_KEY);
    }

    public BigDecimal bdxcEngagements() {
        return (BigDecimal)storedValueForKey(BDXC_ENGAGEMENTS_KEY);
    }
    public void setBdxcEngagements(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_ENGAGEMENTS_KEY);
    }

    public BigDecimal bdxcMandats() {
        return (BigDecimal)storedValueForKey(BDXC_MANDATS_KEY);
    }
    public void setBdxcMandats(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_MANDATS_KEY);
    }

    public BigDecimal bdxcOuverts() {
        return (BigDecimal)storedValueForKey(BDXC_OUVERTS_KEY);
    }
    public void setBdxcOuverts(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_OUVERTS_KEY);
    }

    public BigDecimal bdxcPrimitifs() {
        return (BigDecimal)storedValueForKey(BDXC_PRIMITIFS_KEY);
    }
    public void setBdxcPrimitifs(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_PRIMITIFS_KEY);
    }

    public BigDecimal bdxcProvisoires() {
        return (BigDecimal)storedValueForKey(BDXC_PROVISOIRES_KEY);
    }
    public void setBdxcProvisoires(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_PROVISOIRES_KEY);
    }

    public BigDecimal bdxcReliquats() {
        return (BigDecimal)storedValueForKey(BDXC_RELIQUATS_KEY);
    }
    public void setBdxcReliquats(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_RELIQUATS_KEY);
    }

    public Number bdxcReserve() {
        return (Number)storedValueForKey(BDXC_RESERVE_KEY);
    }
    public void setBdxcReserve(Number aValue) {
        takeStoredValueForKey(aValue, BDXC_RESERVE_KEY);
    }

    public BigDecimal bdxcVotes() {
        return (BigDecimal)storedValueForKey(BDXC_VOTES_KEY);
    }
    public void setBdxcVotes(BigDecimal aValue) {
        takeStoredValueForKey(aValue, BDXC_VOTES_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOOrgan organ() {
        return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
    }
    public void setOrgan(org.cocktail.kava.client.metier.EOOrgan aValue) {
        takeStoredValueForKey(aValue, ORGAN_KEY);
    }
	
    public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOOrgan object = organ();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
        }
    }

    public org.cocktail.application.client.eof.EOTypeCredit typeCreditDep() {
        return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
    }
    public void setTypeCreditDep(org.cocktail.application.client.eof.EOTypeCredit aValue) {
        takeStoredValueForKey(aValue, TYPE_CREDIT_DEP_KEY);
    }
	
    public void setTypeCreditDepRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeCredit object = typeCreditDep();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDIT_DEP_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
        }
    }





}

