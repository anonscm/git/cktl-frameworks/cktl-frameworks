
// _EOTypeApplication.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOTypeApplication.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOTypeApplication extends EOGenericRecord {

    public static final String ENTITY_NAME = "TypeApplication";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_type_application";

    public static final String TYAP_LIBELLE_KEY = "tyapLibelle";
    public static final String TYAP_STRID_KEY = "tyapStrid";

    public static final String TYAP_LIBELLE_COLKEY = "tyap_libelle";
    public static final String TYAP_STRID_COLKEY = "tyap_strid";

    public static final String DOMAINE_KEY = "domaine";




	
    public _EOTypeApplication() {
        super();
    }




    public String tyapLibelle() {
        return (String)storedValueForKey(TYAP_LIBELLE_KEY);
    }
    public void setTyapLibelle(String aValue) {
        takeStoredValueForKey(aValue, TYAP_LIBELLE_KEY);
    }

    public String tyapStrid() {
        return (String)storedValueForKey(TYAP_STRID_KEY);
    }
    public void setTyapStrid(String aValue) {
        takeStoredValueForKey(aValue, TYAP_STRID_KEY);
    }




    public org.cocktail.application.client.eof.EODomaine domaine() {
        return (org.cocktail.application.client.eof.EODomaine)storedValueForKey(DOMAINE_KEY);
    }
    public void setDomaine(org.cocktail.application.client.eof.EODomaine aValue) {
        takeStoredValueForKey(aValue, DOMAINE_KEY);
    }
	
    public void setDomaineRelationship(org.cocktail.application.client.eof.EODomaine value) {
        if (value == null) {
            org.cocktail.application.client.eof.EODomaine object = domaine();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, DOMAINE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, DOMAINE_KEY);
        }
    }





}

