
// _EOCatalogueResponsable.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOCatalogueResponsable.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOCatalogueResponsable extends EOGenericRecord {

    public static final String ENTITY_NAME = "CatalogueResponsable";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.catalogue_responsable";



    public static final String CATALOGUE_KEY = "catalogue";
    public static final String UTILISATEUR_KEY = "utilisateur";




	
    public _EOCatalogueResponsable() {
        super();
    }







    public org.cocktail.kava.client.metier.EOCatalogue catalogue() {
        return (org.cocktail.kava.client.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
    }
    public void setCatalogue(org.cocktail.kava.client.metier.EOCatalogue aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_KEY);
    }
	
    public void setCatalogueRelationship(org.cocktail.kava.client.metier.EOCatalogue value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCatalogue object = catalogue();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
        return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
    }
    public void setUtilisateur(org.cocktail.kava.client.metier.EOUtilisateur aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_KEY);
    }
	
    public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateur object = utilisateur();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
        }
    }





}

