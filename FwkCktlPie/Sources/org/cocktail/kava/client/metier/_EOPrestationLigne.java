
// _EOPrestationLigne.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPrestationLigne.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOPrestationLigne extends EOGenericRecord {

    public static final String ENTITY_NAME = "PrestationLigne";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_ligne";

    public static final String PRLIG_ART_HT_KEY = "prligArtHt";
    public static final String PRLIG_ART_TTC_KEY = "prligArtTtc";
    public static final String PRLIG_ART_TTC_INITIAL_KEY = "prligArtTtcInitial";
    public static final String PRLIG_DATE_KEY = "prligDate";
    public static final String PRLIG_DESCRIPTION_KEY = "prligDescription";
    public static final String PRLIG_QUANTITE_KEY = "prligQuantite";
    public static final String PRLIG_QUANTITE_RESTE_KEY = "prligQuantiteReste";
    public static final String PRLIG_REFERENCE_KEY = "prligReference";
    public static final String PRLIG_TOTAL_HT_KEY = "prligTotalHt";
    public static final String PRLIG_TOTAL_RESTE_HT_KEY = "prligTotalResteHt";
    public static final String PRLIG_TOTAL_RESTE_TTC_KEY = "prligTotalResteTtc";
    public static final String PRLIG_TOTAL_TTC_KEY = "prligTotalTtc";

    public static final String PRLIG_ART_HT_COLKEY = "PRLIG_ART_HT";
    public static final String PRLIG_ART_TTC_COLKEY = "PRLIG_ART_TTC";
    public static final String PRLIG_ART_TTC_INITIAL_COLKEY = "PRLIG_ART_TTC_INITIAL";
    public static final String PRLIG_DATE_COLKEY = "PRLIG_DATE";
    public static final String PRLIG_DESCRIPTION_COLKEY = "PRLIG_DESCRIPTION";
    public static final String PRLIG_QUANTITE_COLKEY = "PRLIG_QUANTITE";
    public static final String PRLIG_QUANTITE_RESTE_COLKEY = "PRLIG_QUANTITE_RESTE";
    public static final String PRLIG_REFERENCE_COLKEY = "PRLIG_REFERENCE";
    public static final String PRLIG_TOTAL_HT_COLKEY = "PRLIG_TOTAL_HT";
    public static final String PRLIG_TOTAL_RESTE_HT_COLKEY = "PRLIG_TOTAL_RESTE_HT";
    public static final String PRLIG_TOTAL_RESTE_TTC_COLKEY = "PRLIG_TOTAL_RESTE_TTC";
    public static final String PRLIG_TOTAL_TTC_COLKEY = "PRLIG_TOTAL_TTC";

    public static final String CATALOGUE_ARTICLE_KEY = "catalogueArticle";
    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String PRESTATION_KEY = "prestation";
    public static final String PRESTATION_LIGNE_PERE_KEY = "prestationLignePere";
    public static final String TVA_KEY = "tva";
    public static final String TVA_INITIAL_KEY = "tvaInitial";
    public static final String TYPE_ARTICLE_KEY = "typeArticle";

    public static final String PRESTATION_LIGNES_KEY = "prestationLignes";



	
    public _EOPrestationLigne() {
        super();
    }




    public BigDecimal prligArtHt() {
        return (BigDecimal)storedValueForKey(PRLIG_ART_HT_KEY);
    }
    public void setPrligArtHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_ART_HT_KEY);
    }

    public BigDecimal prligArtTtc() {
        return (BigDecimal)storedValueForKey(PRLIG_ART_TTC_KEY);
    }
    public void setPrligArtTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_ART_TTC_KEY);
    }

    public BigDecimal prligArtTtcInitial() {
        return (BigDecimal)storedValueForKey(PRLIG_ART_TTC_INITIAL_KEY);
    }
    public void setPrligArtTtcInitial(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_ART_TTC_INITIAL_KEY);
    }

    public NSTimestamp prligDate() {
        return (NSTimestamp)storedValueForKey(PRLIG_DATE_KEY);
    }
    public void setPrligDate(NSTimestamp aValue) {
        takeStoredValueForKey(aValue, PRLIG_DATE_KEY);
    }

    public String prligDescription() {
        return (String)storedValueForKey(PRLIG_DESCRIPTION_KEY);
    }
    public void setPrligDescription(String aValue) {
        takeStoredValueForKey(aValue, PRLIG_DESCRIPTION_KEY);
    }

    public BigDecimal prligQuantite() {
        return (BigDecimal)storedValueForKey(PRLIG_QUANTITE_KEY);
    }
    public void setPrligQuantite(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_QUANTITE_KEY);
    }

    public BigDecimal prligQuantiteReste() {
        return (BigDecimal)storedValueForKey(PRLIG_QUANTITE_RESTE_KEY);
    }
    public void setPrligQuantiteReste(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_QUANTITE_RESTE_KEY);
    }

    public String prligReference() {
        return (String)storedValueForKey(PRLIG_REFERENCE_KEY);
    }
    public void setPrligReference(String aValue) {
        takeStoredValueForKey(aValue, PRLIG_REFERENCE_KEY);
    }

    public BigDecimal prligTotalHt() {
        return (BigDecimal)storedValueForKey(PRLIG_TOTAL_HT_KEY);
    }
    public void setPrligTotalHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_TOTAL_HT_KEY);
    }

    public BigDecimal prligTotalResteHt() {
        return (BigDecimal)storedValueForKey(PRLIG_TOTAL_RESTE_HT_KEY);
    }
    public void setPrligTotalResteHt(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_TOTAL_RESTE_HT_KEY);
    }

    public BigDecimal prligTotalResteTtc() {
        return (BigDecimal)storedValueForKey(PRLIG_TOTAL_RESTE_TTC_KEY);
    }
    public void setPrligTotalResteTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_TOTAL_RESTE_TTC_KEY);
    }

    public BigDecimal prligTotalTtc() {
        return (BigDecimal)storedValueForKey(PRLIG_TOTAL_TTC_KEY);
    }
    public void setPrligTotalTtc(BigDecimal aValue) {
        takeStoredValueForKey(aValue, PRLIG_TOTAL_TTC_KEY);
    }




    public org.cocktail.kava.client.metier.EOCatalogueArticle catalogueArticle() {
        return (org.cocktail.kava.client.metier.EOCatalogueArticle)storedValueForKey(CATALOGUE_ARTICLE_KEY);
    }
    public void setCatalogueArticle(org.cocktail.kava.client.metier.EOCatalogueArticle aValue) {
        takeStoredValueForKey(aValue, CATALOGUE_ARTICLE_KEY);
    }
	
    public void setCatalogueArticleRelationship(org.cocktail.kava.client.metier.EOCatalogueArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOCatalogueArticle object = catalogueArticle();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_ARTICLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_ARTICLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
        return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(org.cocktail.kava.client.metier.EOPlanComptable aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
	
    public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPlanComptable object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPrestation prestation() {
        return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(PRESTATION_KEY);
    }
    public void setPrestation(org.cocktail.kava.client.metier.EOPrestation aValue) {
        takeStoredValueForKey(aValue, PRESTATION_KEY);
    }
	
    public void setPrestationRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestation object = prestation();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOPrestationLigne prestationLignePere() {
        return (org.cocktail.kava.client.metier.EOPrestationLigne)storedValueForKey(PRESTATION_LIGNE_PERE_KEY);
    }
    public void setPrestationLignePere(org.cocktail.kava.client.metier.EOPrestationLigne aValue) {
        takeStoredValueForKey(aValue, PRESTATION_LIGNE_PERE_KEY);
    }
	
    public void setPrestationLignePereRelationship(org.cocktail.kava.client.metier.EOPrestationLigne value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOPrestationLigne object = prestationLignePere();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNE_PERE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_LIGNE_PERE_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTva tva() {
        return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_KEY);
    }
    public void setTva(org.cocktail.kava.client.metier.EOTva aValue) {
        takeStoredValueForKey(aValue, TVA_KEY);
    }
	
    public void setTvaRelationship(org.cocktail.kava.client.metier.EOTva value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTva object = tva();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TVA_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTva tvaInitial() {
        return (org.cocktail.kava.client.metier.EOTva)storedValueForKey(TVA_INITIAL_KEY);
    }
    public void setTvaInitial(org.cocktail.kava.client.metier.EOTva aValue) {
        takeStoredValueForKey(aValue, TVA_INITIAL_KEY);
    }
	
    public void setTvaInitialRelationship(org.cocktail.kava.client.metier.EOTva value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTva object = tvaInitial();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TVA_INITIAL_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TVA_INITIAL_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOTypeArticle typeArticle() {
        return (org.cocktail.kava.client.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
    }
    public void setTypeArticle(org.cocktail.kava.client.metier.EOTypeArticle aValue) {
        takeStoredValueForKey(aValue, TYPE_ARTICLE_KEY);
    }
	
    public void setTypeArticleRelationship(org.cocktail.kava.client.metier.EOTypeArticle value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOTypeArticle object = typeArticle();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ARTICLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
        }
    }




    public NSArray prestationLignes() {
        return (NSArray)storedValueForKey(PRESTATION_LIGNES_KEY);
    }
    public void setPrestationLignes(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PRESTATION_LIGNES_KEY);
    }
    public void addToPrestationLignes(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        NSMutableArray array = (NSMutableArray)prestationLignes();
        willChange();
        array.addObject(object);
    }
    public void removeFromPrestationLignes(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        NSMutableArray array = (NSMutableArray)prestationLignes();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
    }
    public void removeFromPrestationLignesRelationship(org.cocktail.kava.client.metier.EOPrestationLigne object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
    }
	


}

