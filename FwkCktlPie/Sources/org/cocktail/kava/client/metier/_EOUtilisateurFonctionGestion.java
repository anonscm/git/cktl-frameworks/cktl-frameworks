
// _EOUtilisateurFonctionGestion.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOUtilisateurFonctionGestion.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOUtilisateurFonctionGestion extends EOGenericRecord {

    public static final String ENTITY_NAME = "UtilisateurFonctionGestion";

    public static final String ENTITY_TABLE_NAME = "jefy_admin.utilisateur_fonct_gestion";



    public static final String GESTION_KEY = "gestion";
    public static final String UTILISATEUR_FONCTION_KEY = "utilisateurFonction";




	
    public _EOUtilisateurFonctionGestion() {
        super();
    }







    public org.cocktail.kava.client.metier.EOGestion gestion() {
        return (org.cocktail.kava.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
    }
    public void setGestion(org.cocktail.kava.client.metier.EOGestion aValue) {
        takeStoredValueForKey(aValue, GESTION_KEY);
    }
	
    public void setGestionRelationship(org.cocktail.kava.client.metier.EOGestion value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOGestion object = gestion();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, GESTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOUtilisateurFonction utilisateurFonction() {
        return (org.cocktail.kava.client.metier.EOUtilisateurFonction)storedValueForKey(UTILISATEUR_FONCTION_KEY);
    }
    public void setUtilisateurFonction(org.cocktail.kava.client.metier.EOUtilisateurFonction aValue) {
        takeStoredValueForKey(aValue, UTILISATEUR_FONCTION_KEY);
    }
	
    public void setUtilisateurFonctionRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonction value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOUtilisateurFonction object = utilisateurFonction();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_FONCTION_KEY);
        }
    }





}

