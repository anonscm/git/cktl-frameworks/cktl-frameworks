
// _EOParametreBudget.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOParametreBudget.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;


public abstract class _EOParametreBudget extends EOGenericRecord {

    public static final String ENTITY_NAME = "ParametreBudget";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_parametre_budget";

    public static final String BPAR_COMMENTAIRES_KEY = "bparCommentaires";
    public static final String BPAR_KEY_KEY = "bparKey";
    public static final String BPAR_UPDATE_KEY = "bparUpdate";
    public static final String BPAR_VALUE_KEY = "bparValue";

    public static final String BPAR_COMMENTAIRES_COLKEY = "BPAR_COMMENTAIRES";
    public static final String BPAR_KEY_COLKEY = "BPAR_KEY";
    public static final String BPAR_UPDATE_COLKEY = "BPAR_UPDATE";
    public static final String BPAR_VALUE_COLKEY = "BPAR_VALUE";

    public static final String EXERCICE_KEY = "exercice";




	
    public _EOParametreBudget() {
        super();
    }




    public String bparCommentaires() {
        return (String)storedValueForKey(BPAR_COMMENTAIRES_KEY);
    }
    public void setBparCommentaires(String aValue) {
        takeStoredValueForKey(aValue, BPAR_COMMENTAIRES_KEY);
    }

    public String bparKey() {
        return (String)storedValueForKey(BPAR_KEY_KEY);
    }
    public void setBparKey(String aValue) {
        takeStoredValueForKey(aValue, BPAR_KEY_KEY);
    }

    public String bparUpdate() {
        return (String)storedValueForKey(BPAR_UPDATE_KEY);
    }
    public void setBparUpdate(String aValue) {
        takeStoredValueForKey(aValue, BPAR_UPDATE_KEY);
    }

    public String bparValue() {
        return (String)storedValueForKey(BPAR_VALUE_KEY);
    }
    public void setBparValue(String aValue) {
        takeStoredValueForKey(aValue, BPAR_VALUE_KEY);
    }




    public org.cocktail.application.client.eof.EOExercice exercice() {
        return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
    }
    public void setExercice(org.cocktail.application.client.eof.EOExercice aValue) {
        takeStoredValueForKey(aValue, EXERCICE_KEY);
    }
	
    public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOExercice object = exercice();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, EXERCICE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
        }
    }





}

