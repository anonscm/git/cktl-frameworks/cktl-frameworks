
// _EOPersonne.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPersonne.java instead.

package org.cocktail.kava.client.metier;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOPersonne extends EOGenericRecord {

    public static final String ENTITY_NAME = "Personne";

    public static final String ENTITY_TABLE_NAME = "jefy_recette.v_personne";

    public static final String PERS_LC_KEY = "persLc";
    public static final String PERS_LIBELLE_KEY = "persLibelle";
    public static final String PERS_NOM_PRENOM_KEY = "persNomPrenom";
    public static final String PERS_NOMPTR_KEY = "persNomptr";
    public static final String PERS_TYPE_KEY = "persType";

    public static final String PERS_LC_COLKEY = "PERS_LC";
    public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
    public static final String PERS_NOMPTR_COLKEY = "PERS_NOMPTR";
    public static final String PERS_TYPE_COLKEY = "PERS_TYPE";

    public static final String INDIVIDU_ULR_KEY = "individuUlr";
    public static final String REPART_PERSONNE_MAIL_KEY = "repartPersonneMail";
    public static final String STRUCTURE_ULR_KEY = "structureUlr";

    public static final String PERSONNE_TELEPHONES_KEY = "personneTelephones";
    public static final String REPART_PERSONNE_ADRESSES_KEY = "repartPersonneAdresses";
    public static final String REPART_STRUCTURES_KEY = "repartStructures";



	
    public _EOPersonne() {
        super();
    }




    public String persLc() {
        return (String)storedValueForKey(PERS_LC_KEY);
    }
    public void setPersLc(String aValue) {
        takeStoredValueForKey(aValue, PERS_LC_KEY);
    }

    public String persLibelle() {
        return (String)storedValueForKey(PERS_LIBELLE_KEY);
    }
    public void setPersLibelle(String aValue) {
        takeStoredValueForKey(aValue, PERS_LIBELLE_KEY);
    }

    public String persNomPrenom() {
        return (String)storedValueForKey(PERS_NOM_PRENOM_KEY);
    }
    public void setPersNomPrenom(String aValue) {
        takeStoredValueForKey(aValue, PERS_NOM_PRENOM_KEY);
    }

    public String persNomptr() {
        return (String)storedValueForKey(PERS_NOMPTR_KEY);
    }
    public void setPersNomptr(String aValue) {
        takeStoredValueForKey(aValue, PERS_NOMPTR_KEY);
    }

    public String persType() {
        return (String)storedValueForKey(PERS_TYPE_KEY);
    }
    public void setPersType(String aValue) {
        takeStoredValueForKey(aValue, PERS_TYPE_KEY);
    }




    public org.cocktail.kava.client.metier.EOIndividuUlr individuUlr() {
        return (org.cocktail.kava.client.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
    }
    public void setIndividuUlr(org.cocktail.kava.client.metier.EOIndividuUlr aValue) {
        takeStoredValueForKey(aValue, INDIVIDU_ULR_KEY);
    }
	
    public void setIndividuUlrRelationship(org.cocktail.kava.client.metier.EOIndividuUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOIndividuUlr object = individuUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, INDIVIDU_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EORepartPersonneMail repartPersonneMail() {
        return (org.cocktail.kava.client.metier.EORepartPersonneMail)storedValueForKey(REPART_PERSONNE_MAIL_KEY);
    }
    public void setRepartPersonneMail(org.cocktail.kava.client.metier.EORepartPersonneMail aValue) {
        takeStoredValueForKey(aValue, REPART_PERSONNE_MAIL_KEY);
    }
	
    public void setRepartPersonneMailRelationship(org.cocktail.kava.client.metier.EORepartPersonneMail value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EORepartPersonneMail object = repartPersonneMail();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_MAIL_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, REPART_PERSONNE_MAIL_KEY);
        }
    }

    public org.cocktail.kava.client.metier.EOStructureUlr structureUlr() {
        return (org.cocktail.kava.client.metier.EOStructureUlr)storedValueForKey(STRUCTURE_ULR_KEY);
    }
    public void setStructureUlr(org.cocktail.kava.client.metier.EOStructureUlr aValue) {
        takeStoredValueForKey(aValue, STRUCTURE_ULR_KEY);
    }
	
    public void setStructureUlrRelationship(org.cocktail.kava.client.metier.EOStructureUlr value) {
        if (value == null) {
            org.cocktail.kava.client.metier.EOStructureUlr object = structureUlr();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULR_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
        }
    }




    public NSArray personneTelephones() {
        return (NSArray)storedValueForKey(PERSONNE_TELEPHONES_KEY);
    }
    public void setPersonneTelephones(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, PERSONNE_TELEPHONES_KEY);
    }
    public void addToPersonneTelephones(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
        NSMutableArray array = (NSMutableArray)personneTelephones();
        willChange();
        array.addObject(object);
    }
    public void removeFromPersonneTelephones(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
        NSMutableArray array = (NSMutableArray)personneTelephones();
        willChange();
        array.removeObject(object);
    }
	
    public void addToPersonneTelephonesRelationship(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
        addObjectToBothSidesOfRelationshipWithKey(object, PERSONNE_TELEPHONES_KEY);
    }
    public void removeFromPersonneTelephonesRelationship(org.cocktail.kava.client.metier.EOPersonneTelephone object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNE_TELEPHONES_KEY);
    }
	

    public NSArray repartPersonneAdresses() {
        return (NSArray)storedValueForKey(REPART_PERSONNE_ADRESSES_KEY);
    }
    public void setRepartPersonneAdresses(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, REPART_PERSONNE_ADRESSES_KEY);
    }
    public void addToRepartPersonneAdresses(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
        NSMutableArray array = (NSMutableArray)repartPersonneAdresses();
        willChange();
        array.addObject(object);
    }
    public void removeFromRepartPersonneAdresses(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
        NSMutableArray array = (NSMutableArray)repartPersonneAdresses();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRepartPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
        addObjectToBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
    }
    public void removeFromRepartPersonneAdressesRelationship(org.cocktail.kava.client.metier.EORepartPersonneAdresse object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_PERSONNE_ADRESSES_KEY);
    }
	

    public NSArray repartStructures() {
        return (NSArray)storedValueForKey(REPART_STRUCTURES_KEY);
    }
    public void setRepartStructures(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, REPART_STRUCTURES_KEY);
    }
    public void addToRepartStructures(org.cocktail.kava.client.metier.EORepartStructure object) {
        NSMutableArray array = (NSMutableArray)repartStructures();
        willChange();
        array.addObject(object);
    }
    public void removeFromRepartStructures(org.cocktail.kava.client.metier.EORepartStructure object) {
        NSMutableArray array = (NSMutableArray)repartStructures();
        willChange();
        array.removeObject(object);
    }
	
    public void addToRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
        addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    }
    public void removeFromRepartStructuresRelationship(org.cocktail.kava.client.metier.EORepartStructure object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    }
	


}

