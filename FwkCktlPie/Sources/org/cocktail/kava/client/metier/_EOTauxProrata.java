
// _EOTauxProrata.java
// 
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOTauxProrata.java instead.

package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public abstract class _EOTauxProrata extends EOGenericRecord {

    public static final String ENTITY_NAME = "TauxProrata";

    public static final String ENTITY_TABLE_NAME = "jefy_admin.taux_prorata";

    public static final String TAP_TAUX_KEY = "tapTaux";

    public static final String TAP_TAUX_COLKEY = "TAP_TAUX";

    public static final String TYPE_ETAT_KEY = "typeEtat";

    public static final String ORGAN_PRORATAS_KEY = "organProratas";



	
    public _EOTauxProrata() {
        super();
    }




    public BigDecimal tapTaux() {
        return (BigDecimal)storedValueForKey(TAP_TAUX_KEY);
    }
    public void setTapTaux(BigDecimal aValue) {
        takeStoredValueForKey(aValue, TAP_TAUX_KEY);
    }




    public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
        return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
    }
    public void setTypeEtat(org.cocktail.application.client.eof.EOTypeEtat aValue) {
        takeStoredValueForKey(aValue, TYPE_ETAT_KEY);
    }
	
    public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
        if (value == null) {
            org.cocktail.application.client.eof.EOTypeEtat object = typeEtat();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_ETAT_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
        }
    }




    public NSArray organProratas() {
        return (NSArray)storedValueForKey(ORGAN_PRORATAS_KEY);
    }
    public void setOrganProratas(NSMutableArray aValue) {
        takeStoredValueForKey(aValue, ORGAN_PRORATAS_KEY);
    }
    public void addToOrganProratas(org.cocktail.kava.client.metier.EOOrganProrata object) {
        NSMutableArray array = (NSMutableArray)organProratas();
        willChange();
        array.addObject(object);
    }
    public void removeFromOrganProratas(org.cocktail.kava.client.metier.EOOrganProrata object) {
        NSMutableArray array = (NSMutableArray)organProratas();
        willChange();
        array.removeObject(object);
    }
	
    public void addToOrganProratasRelationship(org.cocktail.kava.client.metier.EOOrganProrata object) {
        addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
    }
    public void removeFromOrganProratasRelationship(org.cocktail.kava.client.metier.EOOrganProrata object) {
        removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
    }
	


}

