// EOFacturePapierLigne.java
// 
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

public class EOFacturePapierLigne extends _EOFacturePapierLigne {

	public static final String	FLIG_TOTAL_TVA_KEY	= "fligTotalTva";

	public EOFacturePapierLigne() {
		super();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise HT
	 */
	public void setFligArtHt(BigDecimal aValue) {
		super.setFligArtHt(aValue);
		updateTotalHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total remise TTC
	 */
	public void setFligArtTtc(BigDecimal aValue) {
		super.setFligArtTtc(aValue);
		updateTotalTtc();
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite et recalcule les totaux remises HT et TTC
	 */
	public void setFligQuantite(BigDecimal aValue) {
		// si valeur change pas, return
		if (aValue == null && fligQuantite() == null) {
			return;
		}
		if (aValue == null && fligQuantite().equals(aValue)) {
			return;
		}
		if (fligQuantite() == null && aValue.equals(fligQuantite())) {
			return;
		}
		if (aValue.equals(fligQuantite())) {
			return;
		}
		super.setFligQuantite(aValue);
		updateTotaux();

		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				((EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i)).setFligQuantite(fligQuantite());
			}
		}
	}

	public BigDecimal fligTotalTva() {
		if (fligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (fligTotalHt() == null) {
			return fligTotalTtc();
		}
		return fligTotalTtc().subtract(fligTotalHt());
	}

	public void updateTotaux() {
		updateTotalHt();
		updateTotalTtc();
	}

	/**
	 * met a jour le total ht de la ligne, fonction du prix unitaire, de la quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ht a 0 ou plus
	 */
	private void updateTotalHt() {
		if (fligArtHt() != null && fligQuantite() != null) {
			setFligTotalHt(applyRemise(fligArtHt().multiply(fligQuantite()).setScale(fligArtHt().scale() > 0 ? 2 : 0, BigDecimal.ROUND_HALF_UP)));
		}
		else {
			setFligTotalHt(null);
		}
	}

	/**
	 * met a jour le total ttc de la ligne, fonction du prix unitaire, de la quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ttc a 0 ou plus
	 */
	private void updateTotalTtc() {
		if (fligArtTtc() != null && fligQuantite() != null) {
			BigDecimal htQte=fligArtHt().multiply(fligQuantite());
			BigDecimal tauxTva=new BigDecimal(1.0);
			if (fligArtHt().floatValue()!=0.0)
				tauxTva=fligArtTtc().divide(fligArtHt(), BigDecimal.ROUND_HALF_UP, 3).setScale(3, BigDecimal.ROUND_HALF_UP);
			
			setFligTotalTtc(applyRemise(htQte.multiply(tauxTva)).setScale(fligArtHt().scale() > 0 ? 2 : 0, BigDecimal.ROUND_HALF_UP));
		}
		else {
			setFligTotalTtc(null);
		}
	}

	/**
	 * @param b
	 * @return le montant eventuellement remise, scale conserve
	 */
	private BigDecimal applyRemise(BigDecimal b) {
		if (b == null) {
			return new BigDecimal(0.0);
		}
		if (facturePapier() == null || facturePapier().fapRemiseGlobale() == null) {
			return b;
		}
		return b.subtract(b.multiply(facturePapier().fapRemiseGlobale().divide(new BigDecimal(100.0), BigDecimal.ROUND_HALF_UP))).setScale(
				b.scale(), BigDecimal.ROUND_HALF_UP);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (typeArticle() == null) {
			throw new ValidationException("Il faut un type article pour les lignes de la facture papier!");
		}
		if (facturePapier() == null) {
			throw new ValidationException("Il faut une facture papier pour les lignes de la facture papier!");
		}
		if (fligDate() == null) {
			throw new ValidationException("Il faut une date de creation pour les lignes de la facture papier!");
		}
		if (fligDescription() == null) {
			throw new ValidationException("Il faut un libelle pour les lignes de la facture papier!");
		}
		if (fligArtHt() == null) {
			throw new ValidationException("Il faut un prix HT pour les lignes de la facture papier!");
		}
		if (fligArtTtc() == null) {
			throw new ValidationException("Il faut un prix TTC pour les lignes de la facture papier!");
		}
		if (fligQuantite() == null) {
			throw new ValidationException("Il faut une quantite pour les lignes de la facture papier!");
		}
		if (fligArtHt().abs().compareTo(fligArtTtc().abs()) == 1) {
			throw new ValidationException("Le prix HT ne peut etre superieur au prix TTC!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
