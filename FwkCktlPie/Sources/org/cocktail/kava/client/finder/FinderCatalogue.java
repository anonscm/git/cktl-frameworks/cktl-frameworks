/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.finder;

import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCataloguePrestation;
import org.cocktail.kava.client.metier.EOCataloguePrestationWeb;
import org.cocktail.kava.client.metier.EOCataloguePublic;
import org.cocktail.kava.client.metier.EOCatalogueResponsable;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOTypeApplication;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderCatalogue {

	public static EOCatalogue find(EOEditingContext ec, Integer catNumero) {
		NSMutableArray quals = new NSMutableArray();
		if (catNumero != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.CATALOGUE_PRESTATION_KEY + "."
					+ EOCataloguePrestation.CAT_NUMERO_KEY + " = %@", new NSArray(catNumero)));
		}
		EOFetchSpecification fs = new EOFetchSpecification(EOCatalogue.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOCatalogue.CATALOGUE_PRESTATION_KEY));
		try {
			return (EOCatalogue) ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String findKeyWeb(EOEditingContext ec, EOCatalogue catalogue, String key) {
		if (catalogue != null && key != null) {
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCataloguePrestationWeb.CATALOGUE_KEY + " = %@", new NSArray(catalogue)));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCataloguePrestationWeb.CAT_CLE_KEY + " = %@", new NSArray(key)));
			EOFetchSpecification fs = new EOFetchSpecification(EOCataloguePrestationWeb.ENTITY_NAME, new EOAndQualifier(quals), null);
			try {
				return ((EOCataloguePrestationWeb) ec.objectsWithFetchSpecification(fs).objectAtIndex(0)).catValeur();
			}
			catch (Exception e) {
				return null;
			}
		}
		return null;
	}

	public static NSArray find(EOEditingContext ec, EOUtilisateur utilisateur, EOTypeApplication typeApplication, EOTypeEtat typeEtat,
			EOTypePublic typePublic) {
		NSMutableArray quals = new NSMutableArray();
		if (utilisateur != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.CATALOGUE_RESPONSABLES_KEY + "."
					+ EOCatalogueResponsable.UTILISATEUR_KEY + "=%@", new NSArray(utilisateur)));
		}
		if (typeApplication != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.TYPE_APPLICATION_KEY + "=%@", new NSArray(typeApplication)));
		}
		if (typeEtat != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.TYPE_ETAT_KEY + "=%@", new NSArray(typeEtat)));
		}
		if (typePublic != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.CATALOGUE_PUBLICS_KEY + "." + EOCataloguePublic.TYPE_PUBLIC_KEY
					+ "=%@", new NSArray(typePublic)));
		}
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(EOCatalogue.CATALOGUE_PRESTATION_KEY + "."
				+ EOCataloguePrestation.CAT_NUMERO_KEY, EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOCatalogue.ENTITY_NAME, new EOAndQualifier(quals), sort);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOCatalogue.CATALOGUE_PRESTATION_KEY));
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static NSArray find(EOEditingContext ec, EOUtilisateur utilisateur, EOFournisUlr fournisUlr, EOTypePublic typePublic) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier
				.qualifierWithQualifierFormat(EOCatalogue.TYPE_ETAT_KEY + "=%@", new NSArray(FinderTypeEtat.typeEtatValide(ec))));
		if (utilisateur != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.CATALOGUE_RESPONSABLES_KEY + "."
					+ EOCatalogueResponsable.UTILISATEUR_KEY + "=%@", new NSArray(utilisateur)));
		}
		if (fournisUlr != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.FOURNIS_ULR_KEY + "=%@", new NSArray(fournisUlr)));
		}
		if (typePublic != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.CATALOGUE_PUBLICS_KEY + "." + EOCataloguePublic.TYPE_PUBLIC_KEY
					+ "=%@", new NSArray(typePublic)));
		}
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(EOCatalogue.CATALOGUE_PRESTATION_KEY + "."
				+ EOCataloguePrestation.CAT_NUMERO_KEY, EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOCatalogue.ENTITY_NAME, new EOAndQualifier(quals), sort);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOCatalogue.CATALOGUE_PRESTATION_KEY));
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

}
