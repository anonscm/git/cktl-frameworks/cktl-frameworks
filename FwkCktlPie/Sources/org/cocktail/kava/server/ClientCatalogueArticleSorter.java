package org.cocktail.kava.server;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSLog;

public class ClientCatalogueArticleSorter {
	
	
	
	
	
	
	public NSArray<ClientCatalogueArticle> sortByCaarId(NSArray<ClientCatalogueArticle> objects) {
		
		
		NSArray<ClientCatalogueArticle> sortedObjects = null;
		try {
			sortedObjects = objects.sortedArrayUsingComparator(new ClientCatalogueArticleComparator());
		}
		catch (NSComparator.ComparisonException e) {
			e.printStackTrace();
			NSLog.out.appendln("erreur sort : " + e.getMessage());
		}
		
		return sortedObjects;
	}
	
	
	public class ClientCatalogueArticleComparator extends NSComparator {

		public int compare(Object object1, Object object2) throws NSComparator.ComparisonException {

			if (!(object1 instanceof ClientCatalogueArticle) || !(object2 instanceof ClientCatalogueArticle)) {
				throw new NSComparator.ComparisonException("Les objets compares doivent etre des instances de ClientCatalogueArticle");
			}

			ClientCatalogueArticle o1 = (ClientCatalogueArticle) object1;
			ClientCatalogueArticle o2 = (ClientCatalogueArticle) object2;
			
			int compareValue = 0;

			if(o1.caarId()==o2.caarId()) {
				compareValue = NSComparator.OrderedSame;
			}

			if(o1.caarId() > o2.caarId()) {
				compareValue = NSComparator.OrderedDescending;
			}
			
			if(o1.caarId()<o2.caarId()) {
				compareValue = NSComparator.OrderedAscending;
			}
		
			return compareValue;
		}


	}


}
