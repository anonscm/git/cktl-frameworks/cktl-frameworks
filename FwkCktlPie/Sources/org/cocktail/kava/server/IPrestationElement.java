package org.cocktail.kava.server;

import java.math.BigDecimal;

import org.cocktail.kava.server.metier.EOCatalogueArticle;

import com.webobjects.foundation.NSArray;


/**
 * Cette interface permet de fournir le necessaire à toute classe représentant un objet du panier afin d'être traité 
 * par AdvancedPrestationFacotry comme element de la commande avec obligatoirement un catalogueArticle, et des options 
 * choisies par l'acheteur.
 */

public interface IPrestationElement {

	public EOCatalogueArticle catalogueArticle();
	
	public NSArray options();
	
	public BigDecimal quantite();
}
