package org.cocktail.kava.server.finder;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPlancoCreditDep;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPlancoCreditDep {

	public static NSArray findAll(EOEditingContext ec) {
		EOFetchSpecification fs = new EOFetchSpecification(EOPlancoCreditDep.ENTITY_NAME, null, null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	private static EOQualifier qualValide() {
		return EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PLANCO_CREDIT_DEPS_KEY + "." + 
				EOPlancoCreditDep.PCC_ETAT_KEY + "=%@ AND " + EOPlanComptable.PLANCO_CREDIT_DEPS_KEY + "." + EOPlancoCreditDep.PCO_VALIDITE_KEY + "=%@", new NSArray(new Object[] { "VALIDE",
						"VALIDE" }));
	}

	private static EOQualifier qual(EOTypeCredit typeCreditDep) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PLANCO_CREDIT_DEPS_KEY + "." + EOPlancoCreditDep.TYPE_CREDIT_DEP_KEY + "=%@", new NSArray(typeCreditDep)));
		return new EOAndQualifier(args);
	}

	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditDep) {
		return find(ec, typeCreditDep, null);
	}
	
	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditDep,EOExercice exercice) {
		return find(ec, typeCreditDep, exercice, null);
	}
	
	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditDep,EOExercice exercice,EOQualifier additionalQualifier) {

		NSMutableArray quals = new NSMutableArray();
		
		quals.addObject(qualValide());
		
		if(additionalQualifier!=null) {
			quals.addObject( additionalQualifier );
		}

		if (typeCreditDep != null) {
			quals.addObject(qual(typeCreditDep));
		}
		
		if(exercice!=null) {
			quals.addObject( EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.EXERCICE_KEY+"=%@",new NSArray(exercice)) );
		}
		
		
		EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	
}
