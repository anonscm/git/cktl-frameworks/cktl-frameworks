package org.cocktail.kava.server.finder;

import org.cocktail.application.serveur.eof.EOCodeMarche;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class FinderCodeMarche {
	
	public static final EOQualifier QUALIFIER_VALIDITE = 
						new EOKeyValueQualifier(EOCodeMarche.CM_SUPPR_KEY,EOQualifier.QualifierOperatorEqual,"N");
	
	public static final EOSortOrdering SORT_CM_CODE = 
						EOSortOrdering.sortOrderingWithKey(EOCodeMarche.CM_CODE_KEY, EOSortOrdering.CompareAscending);
	
						public static final EOSortOrdering SORT_LIB_COURT = EOSortOrdering.sortOrderingWithKey(
						EOCodeMarche.CM_LIB_COURT_KEY, EOSortOrdering.CompareAscending);
	
						
						
	public static NSArray<EOCodeMarche> smartFindValide(EOEditingContext ec,String input) {
		EOQualifier qual = null;
		if(input!=null && !input.trim().equals("")) {
			qual = EOQualifier.qualifierWithQualifierFormat(
								EOCodeMarche.CM_CODE_KEY+" caseInsensitiveLike '*"+input+"*' or "+
								EOCodeMarche.CM_LIB_COURT_KEY+" caseInsensitiveLike '*"+input+"*' or "+
								EOCodeMarche.CM_LIB_KEY+" caseInsensitiveLike '*"+input+"*'",
								null
								);
		}
		return findValide(ec,qual);

	}

	public static NSArray<EOCodeMarche> findValide(EOEditingContext ec,EOQualifier qualifier) {
		
		EOQualifier totalQual = null;
		if(qualifier!=null) {
			totalQual = new EOAndQualifier(new NSArray(new EOQualifier[]{qualifier,QUALIFIER_VALIDITE}));
		}
		else {
			totalQual = qualifier;
		}
		
		NSArray<EOSortOrdering> totalSort = new NSArray<EOSortOrdering>( new EOSortOrdering[]{SORT_CM_CODE,SORT_LIB_COURT} );
		
		EOFetchSpecification fs = new EOFetchSpecification(EOCodeMarche.ENTITY_NAME, totalQual, totalSort);
		fs.setUsesDistinct(true);
		try {
			return (NSArray<EOCodeMarche>) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
		
		
	}
	
}
