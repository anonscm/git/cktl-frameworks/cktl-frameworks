package org.cocktail.kava.server.finder;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPlancoCreditRec;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;



public class FinderPlancoCreditRec {

	
	public static NSArray findAll(EOEditingContext ec) {
		EOFetchSpecification fs = new EOFetchSpecification(EOPlancoCreditRec.ENTITY_NAME, null, null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	private static EOQualifier qualValide() {
		return EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PLANCO_CREDIT_RECS_KEY + "." + EOPlancoCreditRec.PCC_ETAT_KEY
				+ "=%@ AND " + EOPlanComptable.PLANCO_CREDIT_RECS_KEY + "." + EOPlancoCreditRec.PCO_VALIDITE_KEY + "=%@", new NSArray(
				new Object[] { "VALIDE", "VALIDE" }));
	}

	private static EOQualifier qual(EOTypeCredit typeCreditRec) {
		return EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PLANCO_CREDIT_RECS_KEY + "." + EOPlancoCreditRec.TYPE_CREDIT_REC_KEY
				+ "=%@", new NSArray(typeCreditRec));
	}

	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditRec) {
		return find(ec, typeCreditRec, null);
	}
	
	
	/************ Ajouts pour tenir compte de la table PLAN_COMPTABLE_EXER a la place de PLAN_COMPTABLE ************/
	
	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditRec, EOExercice exercice) {
		return find(ec, typeCreditRec, exercice,null);
	}
	
	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCreditRec, EOExercice exercice,EOQualifier additionalQualifier) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(qualValide());
		
		if(additionalQualifier!=null) {
			quals.addObject( additionalQualifier );
		}
		
		if (typeCreditRec != null) {
			quals.addObject(qual(typeCreditRec));
		}
		
		if(exercice!=null) {
			quals.addObject( EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.EXERCICE_KEY+"=%@",new NSArray(exercice)) );
		}

		
		EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
}
