package org.cocktail.kava.server.finder;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.kava.server.metier.EOLolfNomenclatureRecette;
import org.cocktail.kava.server.metier.EOOrgan;
import org.cocktail.kava.server.metier.EOOrganActionRec;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderLolfNomenclatureRecette {

	public static final String JEFY_ADMIN_PARAM_LOLF_NIVEAU_RECETTE = "LOLF_NIVEAU_RECETTE";
	
	public static NSArray find(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCreditRec,EOQualifier additionalQualifier) {
		
		if (exercice == null && organ == null && typeCreditRec == null) {
			return findAll(ec, exercice);
		}
		
		NSMutableArray quals = new NSMutableArray();
		
		quals.addObject( qualTypeEtatValide(ec) );
		
		if(additionalQualifier != null) {
			quals.add( additionalQualifier );
		}
		
		if (exercice != null) {
			quals.addObject(FinderExercice.getQualifierForPeriodeAndExercice(EOLolfNomenclatureRecette.LOLF_OUVERTURE_KEY, EOLolfNomenclatureRecette.LOLF_FERMETURE_KEY, exercice));
		}
		
		if (organ != null) {
			quals.addObject(qual(organ));
		}
		
		if (typeCreditRec != null) {
			quals.addObject(qual(typeCreditRec));
		}
		
		Integer niveau = lolfNiveauMin(ec,exercice);
		if(niveau !=null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.LOLF_NIVEAU_KEY+" >= %@", new NSArray(niveau)));
		}
		
		EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification fs = new EOFetchSpecification(EOLolfNomenclatureRecette.ENTITY_NAME, qual, null);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	
	public static NSArray findAll(EOEditingContext ec,EOExercice exercice) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(FinderExercice.getQualifierForPeriodeAndExercice(EOLolfNomenclatureRecette.LOLF_OUVERTURE_KEY, EOLolfNomenclatureRecette.LOLF_FERMETURE_KEY, exercice));
		Integer niveau = lolfNiveauMin(ec,exercice);
		if(niveau!=null)
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.LOLF_NIVEAU_KEY+" >= %@", new NSArray(niveau)));
		EOFetchSpecification fs = new EOFetchSpecification(EOLolfNomenclatureRecette.ENTITY_NAME, new EOAndQualifier(args), null);
		try {
			fs.setUsesDistinct(true);
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	private static Integer lolfNiveauMin(EOEditingContext ec,EOExercice exercice) {
		Integer niveau;
		try {
			String strNiveau = FinderParametreJefyAdmin.find(ec, JEFY_ADMIN_PARAM_LOLF_NIVEAU_RECETTE, exercice);
			niveau = new Integer(strNiveau);
		} 
		catch (Throwable e) {
			e.printStackTrace();
			niveau = null;
		}
		return niveau;
	}
	
	private static EOQualifier qualTypeEtatValide(EOEditingContext ec) {
		return EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.TYPE_ETAT_KEY + "=%@", new NSArray(FinderTypeEtat.typeEtatValide(ec)));
	}
	
	private static EOQualifier qual(EOOrgan organ) {
		return EOQualifier.qualifierWithQualifierFormat(
				EOLolfNomenclatureRecette.ORGAN_ACTION_RECS_KEY + "." + EOOrganActionRec.ORGAN_KEY + " = %@", new NSArray(organ));
	}

	private static EOQualifier qual(EOTypeCredit typeCreditRec) {
		return EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.ORGAN_ACTION_RECS_KEY + "." + EOOrganActionRec.TYPE_CREDIT_REC_KEY
				+ " = %@", new NSArray(typeCreditRec));
	}
	
}
