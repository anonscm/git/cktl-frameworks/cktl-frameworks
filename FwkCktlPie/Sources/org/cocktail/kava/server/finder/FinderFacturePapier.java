// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fullnames 
// Source File Name:   FinderFacturePapier.java

package org.cocktail.kava.server.finder;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.server.metier.EOFacturePapier;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

// Referenced classes of package org.cocktail.kava.server.finder:
//            FinderTypeEtat

public class FinderFacturePapier
{

    public FinderFacturePapier()
    {
    }

    public static org.cocktail.kava.server.metier.EOFacturePapier find(com.webobjects.eocontrol.EOEditingContext ec, EOExercice exercice, java.lang.Integer fapNumero)
    {
        com.webobjects.eocontrol.EOFetchSpecification fs;
        com.webobjects.foundation.NSMutableArray quals = new NSMutableArray();
        if(exercice != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(exercice)));
        if(fapNumero != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("fapNumero = %@", new NSArray(fapNumero)));
        try {
            fs = new EOFetchSpecification("FacturePapier", new EOAndQualifier(quals), null);
    		fs.setRefreshesRefetchedObjects(true);
            return (org.cocktail.kava.server.metier.EOFacturePapier)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static com.webobjects.foundation.NSArray find(com.webobjects.eocontrol.EOEditingContext ec, EOExercice exercice, org.cocktail.kava.server.metier.EOUtilisateur utilisateur, org.cocktail.kava.server.metier.EOTypeApplication typeApplication)
    {
        com.webobjects.eocontrol.EOFetchSpecification fs;
        com.webobjects.foundation.NSMutableArray quals = new NSMutableArray();
        quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("typeEtat=%@", new NSArray(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatValide(ec))));
        if(exercice != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(exercice)));
        if(utilisateur != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("utilisateur=%@", new NSArray(utilisateur)));
        if(typeApplication != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("typePublic.typeApplication=%@", new NSArray(typeApplication)));
        com.webobjects.foundation.NSArray sort = new NSArray(com.webobjects.eocontrol.EOSortOrdering.sortOrderingWithKey("fapNumero", com.webobjects.eocontrol.EOSortOrdering.CompareDescending));
        try {
            fs = new EOFetchSpecification("FacturePapier", new EOAndQualifier(quals), sort);
    		fs.setRefreshesRefetchedObjects(true);
            return ec.objectsWithFetchSpecification(fs); 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new NSArray();
    }

    public static com.webobjects.foundation.NSArray<EOFacturePapier> find(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
    {
        com.webobjects.eocontrol.EOFetchSpecification fs;
        com.webobjects.foundation.NSMutableArray quals = new NSMutableArray();
        quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("typeEtat=%@", new NSArray(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatValide(ec))));
        if(prestation != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("prestation=%@", new NSArray(prestation)));
        com.webobjects.foundation.NSArray sort = new NSArray(com.webobjects.eocontrol.EOSortOrdering.sortOrderingWithKey("fapNumero", com.webobjects.eocontrol.EOSortOrdering.CompareDescending));
        try {
            fs = new EOFetchSpecification("FacturePapier", new EOAndQualifier(quals), sort);
    		fs.setRefreshesRefetchedObjects(true);
            return ec.objectsWithFetchSpecification(fs); 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new NSArray();
    }

    public static com.webobjects.foundation.NSArray find(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOFacture facture)
    {
        com.webobjects.eocontrol.EOFetchSpecification fs;
        com.webobjects.foundation.NSMutableArray quals = new NSMutableArray();
        if(facture != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("facture=%@", new NSArray(facture)));
        com.webobjects.foundation.NSArray sort = new NSArray(com.webobjects.eocontrol.EOSortOrdering.sortOrderingWithKey("fapNumero", com.webobjects.eocontrol.EOSortOrdering.CompareDescending));
        try {
            fs = new EOFetchSpecification("FacturePapier", new EOAndQualifier(quals), sort);
    		fs.setRefreshesRefetchedObjects(true);
            return ec.objectsWithFetchSpecification(fs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new NSArray();
    }
    
    public static NSArray find(EOEditingContext ec, Integer fapId)
    {
//        com.webobjects.eocontrol.EOFetchSpecification fs;
        com.webobjects.foundation.NSMutableArray quals = new NSMutableArray();
        if(fapId != null)
            quals.addObject(com.webobjects.eocontrol.EOQualifier.qualifierWithQualifierFormat("fapId=%@", new NSArray(fapId)));
        else
            return null;
        //com.webobjects.foundation.NSArray sort = new NSArray(com.webobjects.eocontrol.EOSortOrdering.sortOrderingWithKey("fapNumero", com.webobjects.eocontrol.EOSortOrdering.CompareDescending));
        quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.EXERCICE_KEY+"=%@",new NSArray(FinderExercice.findExerciceEnCours(ec))));
        try {
//            fs = new EOFetchSpecification("FacturePapier", new EOAndQualifier(quals), sort);
//            return ec.objectsWithFetchSpecification(fs);
            return ((CktlWebApplication)WOApplication.application()).dataBus().fetchArray(ec,"FacturePapier", new EOAndQualifier(quals),null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
