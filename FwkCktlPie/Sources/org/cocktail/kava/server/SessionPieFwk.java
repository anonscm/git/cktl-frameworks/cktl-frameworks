package org.cocktail.kava.server;

import java.util.TimeZone;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.kava.server.metier.EOCommandesPourPi;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOGestion;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOUtilisateur;
import org.cocktail.kava.server.procedures.Api;
import org.cocktail.kava.server.procedures.ApiPrestation;
import org.cocktail.kava.server.procedures.GetNumerotation;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

public class SessionPieFwk extends Object {

	protected CktlDataBus dataBus;
	protected EOEditingContext editingContext;

	public SessionPieFwk(CktlDataBus db, EOEditingContext ec) {
		dataBus = db;
		editingContext = ec;
	}

	public void clientSideRequestRegrouperPrestation(NSArray prestations, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.regrouperPrestation(dataBus, editingContext, prestations, (EOUtilisateur) utilisateur);
	}
	
	// Procedures...
	// Prestations
	public void clientSideRequestApiPrestationValidePrestationClient(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.validePrestationClient(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationDevalidePrestationClient(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.devalidePrestationClient(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationValidePrestationPrestataire(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.validePrestationPrestataire(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationSoldePrestation(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.soldePrestation(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationDupliquePrestation(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur, EOEnterpriseObject exerciceCible) throws Exception {
		ApiPrestation.dupliquePrestation(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur, (EOExercice) exerciceCible);
	}

	public void clientSideRequestApiPrestationBasculePrestation(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur, EOEnterpriseObject exerciceCible) throws Exception {
		ApiPrestation.basculePrestation(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur, (EOExercice) exerciceCible);
	}

	public void clientSideRequestApiPrestationDevalidePrestationPrestataire(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.devalidePrestationPrestataire(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationCloturePrestation(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.cloturePrestation(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationDecloturePrestation(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.decloturePrestation(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationChangePrestationDT(EOEnterpriseObject oldPrestation, EOEnterpriseObject newPrestation) throws Exception {
		ApiPrestation.changePrestationDT(dataBus, editingContext, (EOPrestation) oldPrestation, (EOPrestation) newPrestation);
	}

	public void clientSideRequestApiPrestationArchivePrestation(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.archivePrestation(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationGenereFacturePapier(EOEnterpriseObject prestation, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.genereFacturePapier(dataBus, editingContext, (EOPrestation) prestation, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationDelFacturePapier(EOEnterpriseObject facturePapier, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.delFacturePapier(dataBus, editingContext, (EOFacturePapier) facturePapier, (EOUtilisateur) utilisateur);
	}

	public NSDictionary clientSideRequestApiPrestationPrestationFromCommande(EOEnterpriseObject commande, EOEnterpriseObject utilisateur, EOEnterpriseObject fournisUlrClient, String pcoNum)
			throws Exception {
		return ApiPrestation.prestationFromCommande(dataBus, editingContext, (EOCommandesPourPi) commande, (EOUtilisateur) utilisateur, (EOFournisUlr) fournisUlrClient, pcoNum);
	}

	public NSDictionary clientSideRequestApiPrestationFacturePapierFromCommande(EOEnterpriseObject commande, EOEnterpriseObject utilisateur, EOEnterpriseObject fournisUlrClient, String pcoNum)
			throws Exception {
		return ApiPrestation.facturePapierFromCommande(dataBus, editingContext, (EOCommandesPourPi) commande, (EOUtilisateur) utilisateur, (EOFournisUlr) fournisUlrClient, pcoNum);
	}

	public void clientSideRequestApiPrestationUpdFapRef(EOEnterpriseObject facturePapier) throws Exception {
		ApiPrestation.updFapRef(dataBus, editingContext, (EOFacturePapier) facturePapier);
	}

	// Factures papier
	public void clientSideRequestApiPrestationValideFacturePapierClient(EOEnterpriseObject facturePapier, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.valideFacturePapierClient(dataBus, editingContext, (EOFacturePapier) facturePapier, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationDevalideFacturePapierClient(EOEnterpriseObject facturePapier, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.devalideFacturePapierClient(dataBus, editingContext, (EOFacturePapier) facturePapier, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationValideFacturePapierPrest(EOEnterpriseObject facturePapier, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.valideFacturePapierPrest(dataBus, editingContext, (EOFacturePapier) facturePapier, (EOUtilisateur) utilisateur);
	}

	public void clientSideRequestApiPrestationDevalideFacturePapierPrest(EOEnterpriseObject facturePapier, EOEnterpriseObject utilisateur) throws Exception {
		ApiPrestation.devalideFacturePapierPrest(dataBus, editingContext, (EOFacturePapier) facturePapier, (EOUtilisateur) utilisateur);
	}

	public NSDictionary clientSideRequestApiPrestationDuplicateFacturePapier(EOEnterpriseObject facturePapier, EOEnterpriseObject utilisateur, EOEnterpriseObject fournisUlrClient) throws Exception {
		return ApiPrestation.duplicateFacturePapier(dataBus, editingContext, (EOFacturePapier) facturePapier, (EOUtilisateur) utilisateur, (EOFournisUlr) fournisUlrClient);
	}

	// Factures
	public void clientSideRequestApiInsFacture(NSDictionary dico) throws Exception {
		Api.insFacture(dataBus, dico);
	}

	public void clientSideRequestApiUpdFacture(NSDictionary dico) throws Exception {
		Api.updFacture(dataBus, dico);
	}

	public void clientSideRequestApiDelFacture(NSDictionary dico) throws Exception {
		Api.delFacture(dataBus, dico);
	}

	// Recette papier
	public void clientSideRequestApiInsRecettePapier(NSDictionary dico) throws Exception {
		Api.insRecettePapier(dataBus, dico);
	}

	public void clientSideRequestApiUpdRecettePapier(NSDictionary dico) throws Exception {
		Api.updRecettePapier(dataBus, dico);
	}

	public void clientSideRequestApiDelRecettePapier(NSDictionary dico) throws Exception {
		Api.delRecettePapier(dataBus, dico);
	}

	// Recette
	public void clientSideRequestApiInsRecette(NSDictionary dico) throws Exception {
		Api.insRecette(dataBus, dico);
	}

	public void clientSideRequestApiDelRecette(NSDictionary dico) throws Exception {
		Api.delRecette(dataBus, dico);
	}

	// Facture-Recette
	public NSDictionary clientSideRequestApiInsFactureRecette(NSDictionary dico) throws Exception {
		return Api.insFactureRecette(editingContext, dataBus, dico);
	}

	public void clientSideRequestApiUpdFactureRecette(NSDictionary dico) throws Exception {
		Api.updFactureRecette(dataBus, dico);
	}

	public void clientSideRequestApiDelFactureRecette(NSDictionary dico) throws Exception {
		Api.delFactureRecette(dataBus, dico);
	}

	// Reduction
	public NSDictionary clientSideRequestApiInsReduction(NSDictionary dico) throws Exception {
		return Api.insReduction(dataBus, dico);
	}

	public void clientSideRequestApiDelReduction(NSDictionary dico) throws Exception {
		Api.delReduction(dataBus, dico);
	}

	public NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) {
		return EOUtilities.primaryKeyForObject(editingContext, eo);
	}

	public NSDictionary clientSideRequestPrimaryKeyForGlobalID(EOGlobalID id) {
		// EOEditingContext editingContext = new
		// EOEditingContext(ec.parentObjectStore());
		// EOEnterpriseObject record = editingContext.faultForGlobalID(id,
		// editingContext);
		// return EOUtilities.primaryKeyForObject(editingContext, record);
		EOEnterpriseObject record = editingContext.faultForGlobalID(id, editingContext);
		return EOUtilities.primaryKeyForObject(editingContext, record);
	}

	public Number clientSideRequestGetNumerotation(EOEnterpriseObject exercice, EOEnterpriseObject gestion, String tnuEntite) throws Exception {
		return GetNumerotation.get(dataBus, editingContext, (EOExercice) exercice, (EOGestion) gestion, tnuEntite);
	}

	public NSDictionary clientSideRequestExecuteStoredProcedureNamed(String proc, NSDictionary dico) throws Exception {
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure(proc, dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		} catch (Exception e) {
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static final Object getClassPie(Object ob) {
		if (ob == null)
			return null;
		try {
			if (ob instanceof org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette) {
				org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette lolf = (org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette) ob;
				NSMutableArray args = new NSMutableArray();
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.LOLF_CODE_KEY + " = %@", new NSArray(lolf.lolfCode())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.LOLF_NIVEAU_KEY + " = %@", new NSArray(lolf.lolfNiveau())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.LOLF_LIBELLE_KEY + " = %@", new NSArray(lolf.lolfLibelle())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureRecette.LOLF_ABREVIATION_KEY + " = %@", new NSArray(lolf.lolfAbreviation())));
				Object tmp = ((CktlWebApplication)WOApplication.application()).dataBus().fetchArray(EOLolfNomenclatureRecette.ENTITY_NAME,
						new EOAndQualifier(args), null).lastObject();
				return tmp;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return ob;
	}

	public static final Object getClassCocktailApp(Object ob) {
		if (ob == null)
			return null;
		try {
			if (ob instanceof EOLolfNomenclatureRecette) {
				EOLolfNomenclatureRecette lolf = (EOLolfNomenclatureRecette) ob;
				NSMutableArray args = new NSMutableArray();
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette.LOLF_CODE_KEY + " = %@", new NSArray(lolf.lolfCode())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette.LOLF_NIVEAU_KEY + " = %@", new NSArray(lolf.lolfNiveau())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette.LOLF_LIBELLE_KEY + " = %@", new NSArray(lolf.lolfLibelle())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette.LOLF_ABREVIATION_KEY + " = %@", new NSArray(lolf
						.lolfAbreviation())));
				Object tmp = ((CktlWebApplication) WOApplication.application()).dataBus().fetchArray(org.cocktail.application.serveur.eof.EOLolfNomenclatureRecette.ENTITY_NAME,
						new EOAndQualifier(args), null).lastObject();
				return tmp;
			}
			if (ob instanceof EOExercice) {
				EOExercice exe = (EOExercice) ob;
				NSMutableArray args = new NSMutableArray();
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOExercice.EXE_EXERCICE_KEY + " = %@", new NSArray(exe.exeExercice())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOExercice.EXE_TYPE_KEY + " = %@", new NSArray(exe.exeType())));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(org.cocktail.application.serveur.eof.EOExercice.EXE_STAT_KEY + " = %@", new NSArray(exe.exeStat())));
				Object tmp = ((CktlWebApplication) WOApplication.application()).dataBus().fetchArray(org.cocktail.application.serveur.eof.EOExercice.ENTITY_NAME, new EOAndQualifier(args), null)
						.lastObject();
				return tmp;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return ob;
	}

	public static EOQualifier getQualifierForPeriodeAndExercice(String pathKeyOuverture, String pathKeyCloture, EOExercice exercice) {
		NSMutableArray ou = new NSMutableArray();
		NSMutableArray et1 = new NSMutableArray();
		org.cocktail.application.serveur.eof.EOExercice exer = (org.cocktail.application.serveur.eof.EOExercice) getClassCocktailApp(exercice);
		et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture + " = nil", null));
		et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture + " < %@", new NSArray(dateFinForExercice(exer))));
		NSMutableArray et2 = new NSMutableArray();
		et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture + " >= %@", new NSArray(dateDebutForExercice(exer))));
		et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture + " < %@", new NSArray(dateFinForExercice(exer))));
		NSMutableArray et3 = new NSMutableArray();
		et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture + " >= %@", new NSArray(dateFinForExercice(exer))));
		et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture + " < %@", new NSArray(dateDebutForExercice(exer))));
		ou.addObject(new EOAndQualifier(et1));
		ou.addObject(new EOAndQualifier(et2));
		ou.addObject(new EOAndQualifier(et3));
		return new EOOrQualifier(ou);
	}

	private static NSTimestamp dateDebutForExercice(org.cocktail.application.serveur.eof.EOExercice exercice) {
		String timeZone = ((CktlWebApplication) WOApplication.application()).config().stringForKey("GRHUM_TIMEZONE");
		if (timeZone == null)
			timeZone = "Europe/Paris";
		return new NSTimestamp(exercice.exeOrdre().intValue(), 1, 1, 0, 0, 0, TimeZone.getTimeZone(timeZone));
	}

	private static NSTimestamp dateFinForExercice(org.cocktail.application.serveur.eof.EOExercice exercice) {
		String timeZone = ((CktlWebApplication) WOApplication.application()).config().stringForKey("GRHUM_TIMEZONE");
		if (timeZone == null)
			timeZone = "Europe/Paris";
		return new NSTimestamp(exercice.exeOrdre().intValue() + 1, 1, 1, 0, 0, 0, TimeZone.getTimeZone(timeZone));
	}

}
