/*
Copyright Cocktail (Consortium) 1995-2007

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.


This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package org.cocktail.kava.server.factory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Hashtable;

import org.cocktail.application.serveur.eof.EODevise;
import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.kava.server.CktlXMLSerialize;
import org.cocktail.kava.server.StringTool;
import org.cocktail.kava.server.finder.FinderDevise;
import org.cocktail.kava.server.finder.FinderParametreJefyAdmin;
import org.cocktail.kava.server.finder.FinderParametreMaracuja;
import org.cocktail.kava.server.finder.FinderPays;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.finder.FinderTypePublic;
import org.cocktail.kava.server.metier.EOAdresse;
import org.cocktail.kava.server.metier.EOArticlePrestation;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EODepenseCtrlPlanco;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFacturePapierLigne;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOIndividuUlr;
import org.cocktail.kava.server.metier.EOPersonne;
import org.cocktail.kava.server.metier.EOPersonneTelephone;
import org.cocktail.kava.server.metier.EOPiDepRec;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EORecette;
import org.cocktail.kava.server.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.server.metier.EORepartPersonneAdresse;
import org.cocktail.kava.server.metier.EOTypeNoTel;
import org.cocktail.kava.server.metier.EOTypeTel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class ReportSixFactory {
   
	private static final String     ID_SIX_PIE_DEVIS           = "ID_SIX_PIE_DEVIS";
	private static final String     ID_SIX_PIE_DEVIS_ANGLAIS  	= "ID_SIX_PIE_DEVIS_ANGLAIS";
	private static final String     ID_SIX_PIE_FACTURE         = "ID_SIX_PIE_FACTURE";
	private static final String     ID_SIX_PIE_FACTURE_ANGLAIS = "ID_SIX_PIE_FACTURE_ANGLAIS";
   
   private CktlPrinter               printer;
   
   private EOEditingContext editingContext;
   private Hashtable parameters;
   
   public ReportSixFactory(Hashtable parameters,EOEditingContext editingContext) throws Exception {
	   printer = CktlPrinter.newDefaultInstance(parameters);
	   this.editingContext = editingContext;
	   this.parameters = parameters;
   }

   public boolean checkMaquetteExists(String maquetteId) {
       try {
    	   return printer.checkTemplate(maquetteId);
       }
       catch (Exception e) {
           return false;
       }
   }

   public NSData printDevis(EOPrestation prestation, NSDictionary metadata,String cLangue) throws Exception {
	   int printJobId = -1;
       try {

    	   String maquetteId = null;
    	   maquetteId = getParam(ID_SIX_PIE_DEVIS);
    	   
    	   /*
    	   if(cLangue==null || cLangue.equals(EOLangue.C_LANGUE_FR)) {
    		   maquetteId = getParam(ID_SIX_PIE_DEVIS);
    	   }
    	   else {
    		   maquetteId = getParam(ID_SIX_PIE_DEVIS_ANGLAIS);
    	   }
    	    */
    	   
           printJobId = getPDFJobIdForDevis(prestation, maquetteId, metadata,cLangue);
       }
       catch (Exception e1) {
           e1.printStackTrace();
           throw new Exception("Impossible de générer le fichier PDF : " + e1.getMessage());
       }
       return getPdf(printJobId);
   }

   public NSData printFacturePapier(EOFacturePapier facturePapier, NSDictionary metadata,String cLangue)
   throws Exception {
       int printJobId = -1;
       try {
    	   boolean isAnglais = false;
    	   String maquetteId = null;
    	   if(cLangue==null || cLangue.equals("fr")) {
    		   maquetteId = getParam(ID_SIX_PIE_FACTURE);
    	   }
    	   else {
    		   maquetteId = getParam(ID_SIX_PIE_FACTURE_ANGLAIS);
    		   isAnglais = true;
    	   }
    	   
    	   System.out.println("maquetteId:"+maquetteId);
    	   
    	   //maquetteId = getParam(ID_SIX_PIE_FACTURE);
    	   
           printJobId = getPDFJobIdForFacturePapier(facturePapier, maquetteId, metadata, isAnglais);
       }
       catch (Exception e1) {
           e1.printStackTrace();
           throw new Exception("Impossible de générer le fichier PDF : " + e1.getMessage());
       }
       return getPdf(printJobId);
   }

   


   private NSData getPdf(int printJobId) throws Exception {
       if ((printJobId != -1)) {
           try {
               InputStream pdfStream = getPdfStream(printJobId);
               if (pdfStream == null) {
                   throw new Exception("Le flux PDF est nul.");
               }
               ByteArrayOutputStream tmpByteArrayOutputStream = new ByteArrayOutputStream();
               StreamCtrl.writeContentToStream(pdfStream, tmpByteArrayOutputStream, -1);
               return new NSData(tmpByteArrayOutputStream.toByteArray());
           }
           catch (Exception e) {
               e.printStackTrace();
               throw new Exception("Impossible de récupérer le fichier PDF : " + e.getMessage());
           }
       }
       else {
           throw new Exception("Impossible de récupérer le fichier PDF.");
       }
   }
   
   private int getPDFJobIdForDevis(EOPrestation prestation, String maquetteID, NSDictionary metaData) 
   throws Exception {
	   return getPDFJobIdForDevis(prestation, maquetteID, metaData,"fr");
   }
   
   private int getPDFJobIdForDevis(EOPrestation prestation, String maquetteID, NSDictionary metaData,String codeLangue)
           throws Exception {
       if (prestation == null) {
           throw new Exception("Aucun devis transmis pour l'impression, rien a imprimer.");
       }
       StringWriter myStringWriter = new StringWriter();
       CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
       myCktlXMLWriter.setUseCompactMode(true);
       myCktlXMLWriter.setEscapeSpecChars(true);
       try {
           myCktlXMLWriter.startDocument();
           myCktlXMLWriter.startElement("reportdevis");

           // ajout des meta data
           NSMutableDictionary newMetadata;
           if (metaData == null) {
               newMetadata = new NSMutableDictionary();
           }
           else {
               newMetadata = metaData.mutableClone();
           }
           newMetadata.takeValueForKey(getParam("APP_ALIAS"), "appalias");
           if (newMetadata != null) {
               CktlXMLSerialize tmpCktlXMLSerialize = new CktlXMLSerialize(myCktlXMLWriter, new NSArray());
               tmpCktlXMLSerialize.wantToEscapeSpecialsChars = true;
               tmpCktlXMLSerialize.objectToXml(newMetadata, "metadata");
           }

           // indicateur pour signaler si c'est de la facture interne ou externe ou fc
           if (prestation.typePublic().equals(FinderTypePublic.TYPE_PUBLIC_FORMATION_CONTINUE)) {
               myCktlXMLWriter.writeElement("typeprestation", "FORMATION CONTINUE");
           }
           else {
               if (prestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext))) {
                   myCktlXMLWriter.writeElement("typeprestation", "INTERNE");
               }
               else {
                   myCktlXMLWriter.writeElement("typeprestation", "EXTERNE");
               }
           }
           // indicateur pour signaler le type de public
           myCktlXMLWriter.writeElement("typepublic", prestation.typePublic().typuLibelle());

           // myCktlXMLWriter.writeElement("typeprestation", "Prestation " + prestation.typePublic().typuLibelle());
           myCktlXMLWriter.writeElement("lieuimpression", FinderParametreJefyAdmin.find(editingContext, "VILLE", prestation.exercice()));
           myCktlXMLWriter.writeElement("dateimpression", (new NSTimestampFormatter("%Y-%m-%d")).format(new NSTimestamp()));

           if (prestation.fournisUlrPrest() != null) {
               addEtablissmentInXml(prestation.exercice(), prestation.fournisUlrPrest().siret(), myCktlXMLWriter, false);
           }
           else {
               addEtablissmentInXml(prestation.exercice(), null, myCktlXMLWriter, false);
           }
           addClientInXml(myCktlXMLWriter, editingContext, prestation.fournisUlr(), prestation.personne(), prestation.individuUlr());
           addFournisseurInXml(myCktlXMLWriter, editingContext, prestation.fournisUlrPrest());
           addTotauxInXmlForPrestation(myCktlXMLWriter, prestation);
           EODevise devise = getDeviseEnCours(editingContext, prestation.exercice());
           if (devise != null) {
               myCktlXMLWriter.writeElement("libmonnaie", StringTool.ifNull(devise.devLibelle(), ""));
           }

           myCktlXMLWriter.startElement("devis");
           CktlXMLSerialize tmpCktlXMLSerialize = new CktlXMLSerialize(myCktlXMLWriter, new NSArray());
           tmpCktlXMLSerialize.wantToEscapeSpecialsChars = true;

           tmpCktlXMLSerialize.objectToXml(prestation, "devisdetail");
           tmpCktlXMLSerialize.objectToXml(prestation.prestationBudgetClient().organ(), "organ");
           tmpCktlXMLSerialize.objectToXml(prestation.organ(), "organprest");
           tmpCktlXMLSerialize.objectToXml(prestation.prestationBudgetClient().planComptable(), "plancodepense");
           if(prestation.personne().isPersonneMorale()) {
        	   tmpCktlXMLSerialize.objectToXml(prestation.personne().structureUlr(), "structureulr");
           }
           if (prestation.individuUlr() != null) {
               tmpCktlXMLSerialize.objectToXml(prestation.individuUlr(), "individuulr");
           }
           if (prestation.catalogue() != null) {
               tmpCktlXMLSerialize.objectToXml(prestation.catalogue(), "catalogue");
               tmpCktlXMLSerialize.objectToXml(prestation.catalogue().cataloguePrestation(), "catalogueprestation");
           }

           // Ajouter les lignes + options et remises
           MyPRLIGComp dligComp = new MyPRLIGComp();
           NSMutableArray prestationLignesArticles = prestation.prestationLignes().mutableClone();
           EOQualifier.filterArrayWithQualifier(prestationLignesArticles, EOQualifier.qualifierWithQualifierFormat(
                   EOPrestationLigne.PRESTATION_LIGNE_PERE_KEY + " = nil", null));
           prestationLignesArticles.sortUsingComparator(dligComp);

           for (int i = 0; i < prestationLignesArticles.count(); i++) {
               myCktlXMLWriter.startElement("devisligne");
               EOPrestationLigne tmpLig = (EOPrestationLigne) prestationLignesArticles.objectAtIndex(i);
               NSArray prestationLignesOptionsRemisesTmp = tmpLig.prestationLignes().sortedArrayUsingComparator(dligComp);

               addPrestationLigneInXml(myCktlXMLWriter, tmpLig,codeLangue);
               for (int j = 0; j < prestationLignesOptionsRemisesTmp.count(); j++) {
                   addPrestationLigneInXml(myCktlXMLWriter, (EOPrestationLigne) prestationLignesOptionsRemisesTmp.objectAtIndex(j),codeLangue);
               }
               myCktlXMLWriter.endElement();

           }
           myCktlXMLWriter.endElement();// devis
           myCktlXMLWriter.endElement(); // reportdevis
           myCktlXMLWriter.close();

           // sauvegarde du flux xml (pour test)
           // FileOutputStream fs;
           // try {
           // fs = new FileOutputStream(System.getProperty("java.io.tmpdir") + "/testpdmfiles.xml");
           // fs.write(myStringWriter.toString().getBytes());
           // fs.close();
           // }
           // catch (Exception e) {
           // e.printStackTrace();
           // }

           byte[] xmlbytes = myStringWriter.toString().getBytes();
           ByteArrayInputStream xmlstream = new ByteArrayInputStream(xmlbytes);
           int jobId = genererPDF(xmlstream, xmlbytes.length, maquetteID);
           return jobId;
       }
       catch (Exception e) {
           e.printStackTrace();
           throw new Exception("Erreur lors de la génération du document PDF : " + e.getMessage());
       }
   }

   private int getPDFJobIdForFacturePapier(EOFacturePapier facturePapier, String maquetteID,
           NSDictionary metaData, boolean anglais) throws Exception {
       if (facturePapier == null) {
           throw new Exception("Aucune facture transmise pour l'impression, rien a imprimer.");
       }
       StringWriter myStringWriter = new StringWriter();
       CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
       myCktlXMLWriter.setUseCompactMode(true);
       myCktlXMLWriter.setEscapeSpecChars(true);
       try {
           myCktlXMLWriter.startDocument();
           myCktlXMLWriter.startElement("reportfacturepapier");

           // ajout des meta data
           NSMutableDictionary newMetadata;
           if (metaData == null) {
               newMetadata = new NSMutableDictionary();
           }
           else {
               newMetadata = metaData.mutableClone();
           }
           newMetadata.takeValueForKey(getParam("APP_ALIAS"), "appalias");
           if (newMetadata != null) {
               CktlXMLSerialize tmpCktlXMLSerialize = new CktlXMLSerialize(myCktlXMLWriter, new NSArray());
               tmpCktlXMLSerialize.wantToEscapeSpecialsChars = true;
               tmpCktlXMLSerialize.objectToXml(newMetadata, "metadata");
           }

           myCktlXMLWriter.writeElement("lieuimpression", FinderParametreJefyAdmin.find(editingContext, "VILLE", facturePapier.exercice()));
           myCktlXMLWriter.writeElement("dateimpression", (new NSTimestampFormatter("%Y-%m-%d")).format(new NSTimestamp()));

           addEtablissmentInXml(facturePapier.exercice(), facturePapier.fournisUlrPrest().siret(), myCktlXMLWriter, anglais);
           addClientInXml(myCktlXMLWriter, editingContext, facturePapier.fournisUlr(), facturePapier.personne(), facturePapier.individuUlr());
           addFournisseurInXml(myCktlXMLWriter, editingContext, facturePapier.fournisUlrPrest());
           addTotauxInXmlForFacturePapier(myCktlXMLWriter, facturePapier);
           EODevise devise = getDeviseEnCours(editingContext, facturePapier.exercice());
           if (devise != null) {
               myCktlXMLWriter.writeElement("libmonnaie", StringTool.ifNull(devise.devLibelle(), ""));
           }

           myCktlXMLWriter.startElement("facturepapier");
           CktlXMLSerialize tmpCktlXMLSerialize = new CktlXMLSerialize(myCktlXMLWriter, new NSArray());
           tmpCktlXMLSerialize.wantToEscapeSpecialsChars = true;

           tmpCktlXMLSerialize.objectToXml(facturePapier, "facturepapierdetail");
           tmpCktlXMLSerialize.objectToXml(facturePapier.prestation(), "prestation");
           if (facturePapier.convention() != null) {
               tmpCktlXMLSerialize.objectToXml(facturePapier.convention(), "convention");
           }
           if (facturePapier.facture() != null && facturePapier.facture().recettes() != null && facturePapier.facture().recettes().count() > 0) {
               EORecette recette = (EORecette) facturePapier.facture().recettes().objectAtIndex(0);
               if (recette != null && recette.recetteCtrlPlancos() != null && recette.recetteCtrlPlancos().count() > 0) {
                   EORecetteCtrlPlanco rpco = (EORecetteCtrlPlanco) recette.recetteCtrlPlancos().objectAtIndex(0);
                   if (rpco != null && rpco.titre() != null) {
                       tmpCktlXMLSerialize.objectToXml(rpco.titre(), "titre");
                       if (rpco.titre().bordereau() != null) {
                           tmpCktlXMLSerialize.objectToXml(rpco.titre().bordereau(), "bordereautitre");
                       }
                   }
               }
           }
           if (facturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext))) {
               if (facturePapier.facture() != null && facturePapier.facture().recettes() != null
                       && facturePapier.facture().recettes().count() > 0) {
                   EORecette recette = (EORecette) facturePapier.facture().recettes().objectAtIndex(0);
                   if (recette != null && recette.piDepRecs() != null && recette.piDepRecs().count() > 0) {
                       EOPiDepRec pdr = (EOPiDepRec) recette.piDepRecs().objectAtIndex(0);
                       if (pdr != null && pdr.depenseBudget() != null && pdr.depenseBudget().depenseCtrlPlancos() != null
                               && pdr.depenseBudget().depenseCtrlPlancos().count() > 0) {
                           EODepenseCtrlPlanco dpco = (EODepenseCtrlPlanco) pdr.depenseBudget().depenseCtrlPlancos().objectAtIndex(0);
                           if (dpco != null && dpco.mandat() != null) {
                               tmpCktlXMLSerialize.objectToXml(dpco.mandat(), "mandat");
                               if (dpco.mandat().bordereau() != null) {
                                   tmpCktlXMLSerialize.objectToXml(dpco.mandat().bordereau(), "bordereaumandat");
                               }
                           }
                       }
                   }
               }
           }
           tmpCktlXMLSerialize.objectToXml(facturePapier.exercice(), "exercice");
           // indicateur pour signaler si c'est de la facture interne ou externe
           if (facturePapier.typePublic().equals(FinderTypePublic.TYPE_PUBLIC_FORMATION_CONTINUE)) {
               myCktlXMLWriter.writeElement("typeprestation", "FORMATION CONTINUE");
           }
           else {
               if (facturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext))) {
                   myCktlXMLWriter.writeElement("typeprestation", "INTERNE");
               }
               else {
                   myCktlXMLWriter.writeElement("typeprestation", "EXTERNE");
               }
           }
           // libellé de la facture
           if (facturePapier.typeEtat().equals(FinderTypeEtat.typeEtatNon(editingContext))) {
               if (anglais) {
                   myCktlXMLWriter.writeElement("libellefacture", "PRO FORMA INVOICE No");
               }
               else {
                   myCktlXMLWriter.writeElement("libellefacture", "FACTURE PRO FORMA No");
               }
           }
           else {
               if (facturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext))) {
                   if (anglais) {
                       myCktlXMLWriter.writeElement("libellefacture", "INVOICE (internal) No");
                   }
                   else {
                       myCktlXMLWriter.writeElement("libellefacture", "FACTURE sur P.I. No");
                   }
               }
               else {
                   if (anglais) {
                       myCktlXMLWriter.writeElement("libellefacture", "INVOICE No");
                   }
                   else {
                       myCktlXMLWriter.writeElement("libellefacture", "FACTURE No");
                   }
               }
           }
           // indicateur pour signaler le type de public
           myCktlXMLWriter.writeElement("typepublic", facturePapier.typePublic().typuLibelle());

           // infos budgetaires prestataire
           tmpCktlXMLSerialize.objectToXml(facturePapier.organ(), "organ");
           tmpCktlXMLSerialize.objectToXml(facturePapier.typeCreditRec(), "typecreditrec");
           tmpCktlXMLSerialize.objectToXml(facturePapier.tauxProrata(), "tauxprorata");
           tmpCktlXMLSerialize.objectToXml(facturePapier.lolfNomenclatureRecette(), "lolfnomenclaturerecette");
           tmpCktlXMLSerialize.objectToXml(facturePapier.planComptable(), "plancomptable");
           tmpCktlXMLSerialize.objectToXml(facturePapier.planComptableTva(), "plancomptabletva");
           tmpCktlXMLSerialize.objectToXml(facturePapier.planComptableCtp(), "plancomptablectp");
           tmpCktlXMLSerialize.objectToXml(facturePapier.codeAnalytique(), "codeanalytique");
           tmpCktlXMLSerialize.objectToXml(facturePapier.convention(), "convention");

           // infos budgetaires client interne
           myCktlXMLWriter.startElement("facturepapierbudgetclient");
           if (facturePapier.engageBudget() != null && facturePapier.engageBudget().organ() != null) {
               tmpCktlXMLSerialize.objectToXml(facturePapier.engageBudget().organ(), "organ");
               // tmpCktlXMLSerialize.objectToXml(facturePapier.facturePapierBudgetClient().typeCreditDep(), "typecreditdep");
               // tmpCktlXMLSerialize.objectToXml(facturePapier.facturePapierBudgetClient().tauxProrata(), "tauxprorata");
               // tmpCktlXMLSerialize.objectToXml(facturePapier.facturePapierBudgetClient().lolfNomenclatureDepense(), "lolfnomenclaturedepense");
               // tmpCktlXMLSerialize.objectToXml(facturePapier.facturePapierBudgetClient().planComptable(), "plancomptable");
               // tmpCktlXMLSerialize.objectToXml(facturePapier.facturePapierBudgetClient().codeAnalytique(), "codeanalytique");
               // tmpCktlXMLSerialize.objectToXml(facturePapier.facturePapierBudgetClient().convention(), "convention");
           }
           myCktlXMLWriter.endElement();

           // tmpCktlXMLSerialize.objectToXml(t, "titre");
           // if (t != null) {
           // tmpCktlXMLSerialize.objectToXml(t.storedValueForKey("bordero"), "bordero");
           // }
           // CM TODO
           /*
            * t = facturetitre.titrePrestInterneExercice(); if (t != null) { if (t.storedValueForKey("mandat") != null) {
            * tmpCktlXMLSerialize.objectToXml(t.storedValueForKey("mandat"), "mandat"); } if (t.storedValueForKey("borderoMandat") != null) {
            * tmpCktlXMLSerialize.objectToXml(t.storedValueForKey("borderoMandat"), "borderomandat"); } }
            */

           // Ajouter les lignes + options et remises
           MyFLIGComp fligComp = new MyFLIGComp();
           NSMutableArray facturePapierLignesArticles = facturePapier.facturePapierLignes().mutableClone();
           EOQualifier.filterArrayWithQualifier(facturePapierLignesArticles, EOQualifier.qualifierWithQualifierFormat(
                   EOFacturePapierLigne.FACTURE_PAPIER_LIGNE_PERE_KEY + " = nil", null));
           facturePapierLignesArticles.sortUsingComparator(fligComp);

           for (int i = 0; i < facturePapierLignesArticles.count(); i++) {
               myCktlXMLWriter.startElement("facturepapierligne");
               EOFacturePapierLigne tmpLig = (EOFacturePapierLigne) facturePapierLignesArticles.objectAtIndex(i);
               NSArray facturePapierLignesOptionsRemisesTmp = tmpLig.facturePapierLignes().sortedArrayUsingComparator(fligComp);

               addFacturePapierLigneInXml(myCktlXMLWriter, tmpLig);
               for (int j = 0; j < facturePapierLignesOptionsRemisesTmp.count(); j++) {
                   addFacturePapierLigneInXml(myCktlXMLWriter, (EOFacturePapierLigne) facturePapierLignesOptionsRemisesTmp.objectAtIndex(j));
               }
               myCktlXMLWriter.endElement(); // facturepapierligne

           }
           myCktlXMLWriter.endElement();// facturepapier
           myCktlXMLWriter.endElement(); // reportfacturepapier
           myCktlXMLWriter.close();

           // sauvegarde du flux xml (pour test)
           // FileOutputStream fs;
           // try {
           // String path = System.getProperty("java.io.tmpdir") + "/testpdmfacture.xml";
           // fs = new FileOutputStream(path);
           // fs.write(myStringWriter.toString().getBytes());
           // fs.close();
           // System.out.println("Fichier " + path + " généré !");
           // }
           // catch (Exception e) {
           // e.printStackTrace();
           // }

           byte[] xmlbytes = myStringWriter.toString().getBytes();
           ByteArrayInputStream xmlstream = new ByteArrayInputStream(xmlbytes);
           int jobId = genererPDF(xmlstream, xmlbytes.length, maquetteID);
           return jobId;
       }
       catch (Exception e) {
           e.printStackTrace();
           throw new Exception("Erreur lors de la génération du document PDF : " + e.getMessage());
       }
   }

   private EODevise getDeviseEnCours(EOEditingContext editingContext, EOExercice exercice) {
       String devCode = FinderParametreJefyAdmin.find(editingContext, "DEVISE_DEV_CODE", exercice);
       if (devCode != null) {
           return FinderDevise.find(editingContext, devCode);
       }
       return null;
   }
   
   
   public String getParam(String key) {
	   return (String)parameters.get(key);
   }
   
   /**
    * Ajoute les infos de l'établissement dans le flux xml.
    * 
    * @param tmCktlXMLWriter
    * @throws IOException
    */
   private void addEtablissmentInXml(EOExercice exercice, String siret, CktlXMLWriter tmCktlXMLWriter,boolean anglais) throws IOException {
       tmCktlXMLWriter.startElement("etablissement");
       tmCktlXMLWriter.writeElement("nom", StringTool.replace(StringTool.ifNull(FinderParametreJefyAdmin.find(editingContext, "UNIV", exercice), ""), "&",
               CktlXMLWriter.SpecChars.amp));
       tmCktlXMLWriter.writeElement("adresseuniv", StringTool.replace(StringTool.replace(StringTool.ifNull(FinderParametreJefyAdmin.find(editingContext,
               "ADRESSE_UNIV", exercice), ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));
       tmCktlXMLWriter.writeElement("numerosiret", siret != null ? siret : StringTool.ifNull(FinderParametreJefyAdmin.find(editingContext, "NUMERO_SIRET",
               exercice), ""));
       tmCktlXMLWriter.writeElement("tvaintracom", StringTool.ifNull(FinderParametreMaracuja.find(editingContext, "TVA_INTRACOM", exercice), ""));
       tmCktlXMLWriter.writeElement("numeroape", StringTool.ifNull(getParam("GRHUM_DEFAULT_APE"), ""));
       tmCktlXMLWriter.writeElement("numerodetof", StringTool.ifNull(getParam("GRHUM_DEFAULT_DETOF"), ""));
       String paramReglementCheque = "REGLEMENT_CHEQUE", paramReglementVirement = "REGLEMENT_VIREMENT";
       if (anglais) {
           paramReglementCheque = "REGLEMENT_CHEQUE_ANGLAIS";
           paramReglementVirement = "REGLEMENT_VIREMENT_ANGLAIS";
       }
       tmCktlXMLWriter.writeElement("msgreglementcheque", StringTool.replace(StringTool.replace(StringTool.ifNull(FinderParametreMaracuja.find(
               editingContext, paramReglementCheque, exercice), ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));
       tmCktlXMLWriter.writeElement("msgreglementvirement", StringTool.replace(StringTool.replace(StringTool.ifNull(FinderParametreMaracuja.find(
               editingContext, paramReglementVirement, exercice), ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));
       tmCktlXMLWriter.endElement();
   }

   private void addTotauxInXmlForPrestation(CktlXMLWriter myCktlXMLWriter, EOPrestation prestation) throws Exception {
       myCktlXMLWriter.startElement("totaux");
       myCktlXMLWriter.writeElement("montanttotalht", prestation.prestTotalHt().toString());
       myCktlXMLWriter.writeElement("montanttotaltva", prestation.prestTotalTva().toString());
       myCktlXMLWriter.writeElement("montanttotalttc", prestation.prestTotalTtc().toString());
       myCktlXMLWriter.endElement();
   }

   private void addTotauxInXmlForFacturePapier(CktlXMLWriter myCktlXMLWriter, EOFacturePapier facturePapier) throws Exception {
       myCktlXMLWriter.startElement("totaux");
       myCktlXMLWriter.writeElement("montanttotalht", facturePapier.fapTotalHt().toString());
       myCktlXMLWriter.writeElement("montanttotaltva", facturePapier.fapTotalTva().toString());
       myCktlXMLWriter.writeElement("montanttotalttc", facturePapier.fapTotalTtc().toString());
       myCktlXMLWriter.endElement();
   }

   private void addFournisseurInXml(CktlXMLWriter tmCktlXMLWriter, EOEditingContext editingContext, EOFournisUlr fournisUlr) throws IOException {
       tmCktlXMLWriter.startElement("fournisseur");
       if (fournisUlr != null) {
           tmCktlXMLWriter.writeElement("nom", fournisUlr.personne_persNomPrenom());
           tmCktlXMLWriter.startElement("adresse");
           addAdresseInXml(tmCktlXMLWriter, editingContext, fournisUlr.adresse());
           tmCktlXMLWriter.endElement();
           // TODO
           if (fournisUlr != null) {
               tmCktlXMLWriter.writeElement("tel", StringTool.ifNull(getTelForFournis(fournisUlr.personne()), ""));
               tmCktlXMLWriter.writeElement("fax", StringTool.ifNull(getFaxForFournis(fournisUlr.personne()), ""));
           }
       }
       tmCktlXMLWriter.endElement();
   }

   private void addClientInXml(CktlXMLWriter tmCktlXMLWriter, EOEditingContext editingContext, EOFournisUlr fournisUlr, EOPersonne personne,
           EOIndividuUlr individuUlr) throws IOException {
       tmCktlXMLWriter.startElement("client");
       if (personne != null) {
           String client = personne.persLibelle();
           if (personne.persLc() != null) {
               client = client + " " + personne.persLc();
           }
           tmCktlXMLWriter.writeElement("nommoral", StringCtrl.replace(StringTool.ifNull(client, ""), "&", CktlXMLWriter.SpecChars.amp));
       }
       if (individuUlr != null && individuUlr.personne() != null) {
           String client = individuUlr.personne().persLibelle();
           if (individuUlr.personne().persLc() != null) {
               client = client + " " + individuUlr.personne().persLc();
           }
           tmCktlXMLWriter.writeElement("nomindividu", StringCtrl.replace(StringTool.ifNull(client, ""), "&", CktlXMLWriter.SpecChars.amp));
       }
       tmCktlXMLWriter.startElement("adresse");
       if (fournisUlr != null) {
           addAdresseInXml(tmCktlXMLWriter, editingContext, fournisUlr.adresse());
       }
       else {
           if (personne != null) {
               addAdresseInXml(tmCktlXMLWriter, editingContext, getEOAdresse(personne));
           }
       }
       tmCktlXMLWriter.endElement();

       // TODO
       if (fournisUlr != null) {
           tmCktlXMLWriter.writeElement("tel", StringTool.ifNull(getTelForFournis(fournisUlr.personne()), ""));
           tmCktlXMLWriter.writeElement("fax", StringTool.ifNull(getFaxForFournis(fournisUlr.personne()), ""));
       }
       else {
           if (personne != null) {
               tmCktlXMLWriter.writeElement("tel", StringTool.ifNull(getTelForFournis(personne), ""));
               tmCktlXMLWriter.writeElement("fax", StringTool.ifNull(getFaxForFournis(personne), ""));
           }
       }
       tmCktlXMLWriter.endElement();
   }

   private EOAdresse getEOAdresse(EOPersonne personne) {
       if (personne == null) {
           return null;
       }
       NSArray listeAdresses = personne.repartPersonneAdresses();
       // - Recherche de l'adresse principale
       EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + " = %@", new NSArray("O"));
       NSArray adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
       if (adresse.count() > 0) {
           return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
       }
       // - Recherche de l'adresse de fact
       qualifier = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + " = %@", new NSArray("FACT"));
       adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
       if (adresse.count() > 0) {
           return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
       }
       // - Recherche de l'adresse pro
       qualifier = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + " = %@", new NSArray("PRO"));
       adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
       if (adresse.count() > 0) {
           return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
       }
       // Recherche d'autres adresses que PRO ou FACT
       if (listeAdresses != null && listeAdresses.count() > 0) {
           return ((EORepartPersonneAdresse) listeAdresses.objectAtIndex(0)).adresse();
       }
       return null;
   }

   private String getTelForFournis(EOPersonne personne) {
       if (personne == null) {
           return null;
       }
       NSArray listTel = personne.personneTelephones();
       NSMutableArray args = new NSMutableArray();
       args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY
               + " = %@", new NSArray("TEL")));
       args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_TEL_RELATIONSHIP_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY
               + " = %@", new NSArray("PRF")));
       listTel = EOQualifier.filteredArrayWithQualifier(listTel, new EOAndQualifier(args));
       if (listTel == null || listTel.count() == 0) {
           return null;
       }
       return (String) ((EOPersonneTelephone) listTel.lastObject()).noTelephone();
   }

   private String getFaxForFournis(EOPersonne personne) {
       if (personne == null) {
           return null;
       }
       NSArray listFax = personne.personneTelephones();
       NSMutableArray args = new NSMutableArray();
       args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY
               + " = %@", new NSArray("FAX")));
       args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_TEL_RELATIONSHIP_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY
               + " = %@", new NSArray("PRF")));
       listFax = EOQualifier.filteredArrayWithQualifier(listFax, new EOAndQualifier(args));
       if (listFax == null || listFax.count() == 0) {
           return null;
       }
       return (String) ((EOPersonneTelephone) listFax.lastObject()).noTelephone();
   }

   private void addAdresseInXml(CktlXMLWriter tmCktlXMLWriter, EOEditingContext editingContext, EOAdresse adr) throws IOException {
       if (adr != null) {
           tmCktlXMLWriter.writeElement("adrbp", StringTool.replace(StringTool.ifNull(adr.adrBp(), ""), "&", CktlXMLWriter.SpecChars.amp));
           tmCktlXMLWriter.writeElement("adradresse1", StringTool.replace(StringTool.ifNull(adr.adrAdresse1(), ""), "&",
                   CktlXMLWriter.SpecChars.amp));
           tmCktlXMLWriter.writeElement("adradresse2", StringTool.replace(StringTool.ifNull(adr.adrAdresse2(), ""), "&",
                   CktlXMLWriter.SpecChars.amp));
           tmCktlXMLWriter.writeElement("adrcp", StringTool.ifNull(StringTool.ifNull(adr.cpEtranger(), adr.codePostal()), ""));
           tmCktlXMLWriter.writeElement("adrville", StringTool.replace(StringTool.ifNull(adr.ville(), ""), "&", CktlXMLWriter.SpecChars.amp));
           if (adr.pays() != null && !adr.pays().equals(FinderPays.findDefault(editingContext))) {
               tmCktlXMLWriter.writeElement("lcpays", StringTool.replace(StringTool.ifNull(adr.pays().lcPays(), ""), "&",
                       CktlXMLWriter.SpecChars.amp));
           }
           tmCktlXMLWriter.writeElement("adrcpetranger", StringTool.ifNull(adr.cpEtranger(), ""));
       }
   }

   private void addPrestationLigneInXml(CktlXMLWriter tmCktlXMLWriter, EOPrestationLigne ligne,String codeLangue) throws IOException {
       if (ligne != null) {
           tmCktlXMLWriter.startElement(ligne.typeArticle().tyarLibelle().toLowerCase());
           tmCktlXMLWriter.writeElement("prixlignettc", StringTool.ifNull(ligne.prligTotalTtc(), ""));
           tmCktlXMLWriter.writeElement("cartptype", StringTool.ifNull(ligne.typeArticle().tyarLibelle(), ""));
           tmCktlXMLWriter.writeElement("dlignbresteafacturer", StringTool.ifNull(ligne.prligQuantiteReste(), ""));
           tmCktlXMLWriter.writeElement("dlignbarticles", StringTool.ifNull(ligne.prligQuantite(), ""));
           tmCktlXMLWriter.writeElement("dligartht", StringTool.ifNull(ligne.prligArtHt(), ""));
           
           EOCatalogueArticle ca = ligne.catalogueArticle();
           
           tmCktlXMLWriter.writeElement("dligdescription", StringTool.replace(StringTool.ifNull(/*ligne.prligDescription()*/ca.artLibelleLocalise(codeLangue), ""), "&",
                   CktlXMLWriter.SpecChars.amp));

           tmCktlXMLWriter.writeElement("dligreference", StringTool.replace(StringTool.ifNull(/*ligne.prligReference()*/ca.artReferenceLocalise(codeLangue), ""), "&",
                   CktlXMLWriter.SpecChars.amp));
           tmCktlXMLWriter.writeElement("prixligneht", StringTool.ifNull(ligne.prligTotalHt(), ""));
           tmCktlXMLWriter.startElement("taux");
           if (ligne.tva() != null) {
               tmCktlXMLWriter.writeElement("taucode", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
               tmCktlXMLWriter.writeElement("tautaux", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
           }
           tmCktlXMLWriter.endElement();// taux

           if (ligne.catalogueArticle() != null) {
               EOArticlePrestation artp = ligne.catalogueArticle().article().articlePrestation();
               tmCktlXMLWriter.writeElement("responsable", StringTool.ifNull(artp.artpCfcResponsable(), ""));
               if (artp.artpCfcDateDebut() != null) {
                   tmCktlXMLWriter.writeElement("datedebut", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateDebut()));
               }
               if (artp.artpCfcDateFin() != null) {
                   tmCktlXMLWriter.writeElement("datefin", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateFin()));
               }
               tmCktlXMLWriter.writeElement("duree", StringTool.ifNull(artp.artpCfcDuree(), ""));
               tmCktlXMLWriter.writeElement("nodeclaration", StringTool.ifNull(artp.artpCfcNoDeclaration(), ""));
           }
           tmCktlXMLWriter.endElement();// typeLigne
       }
   }

   private void addFacturePapierLigneInXml(CktlXMLWriter tmCktlXMLWriter, EOFacturePapierLigne ligne) throws IOException {
       if (ligne != null) {
           tmCktlXMLWriter.startElement(ligne.typeArticle().tyarLibelle().toLowerCase());
           tmCktlXMLWriter.writeElement("fapnumero", ligne.facturePapier().fapNumero().toString());
           tmCktlXMLWriter.writeElement("cartptype", StringTool.ifNull(ligne.typeArticle().tyarLibelle(), ""));
           tmCktlXMLWriter.writeElement("fligquantite", StringTool.ifNull(ligne.fligQuantite(), ""));
           tmCktlXMLWriter.writeElement("fligartht", StringTool.ifNull(ligne.fligArtHt(), ""));
           tmCktlXMLWriter.writeElement("fligdescription", StringCtrl.replace(StringTool.ifNull(ligne.fligDescription(), ""), "&",
                   CktlXMLWriter.SpecChars.amp));
           tmCktlXMLWriter.writeElement("fligreference", StringCtrl.replace(StringTool.ifNull(ligne.fligReference(), ""), "&",
                   CktlXMLWriter.SpecChars.amp));
           tmCktlXMLWriter.writeElement("fligtotalht", StringTool.ifNull(ligne.fligTotalHt(), ""));
           tmCktlXMLWriter.writeElement("fligtotalttc", StringTool.ifNull(ligne.fligTotalTtc(), ""));
           tmCktlXMLWriter.writeElement("fligtotaltva", StringTool.ifNull(ligne.fligTotalTva(), ""));
           tmCktlXMLWriter.startElement("taux");
           if (ligne.tva() != null) {
               tmCktlXMLWriter.writeElement("taucode", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
               tmCktlXMLWriter.writeElement("tautaux", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
           }
           tmCktlXMLWriter.endElement();// taux

           if (ligne.prestationLigne() != null && ligne.prestationLigne().catalogueArticle() != null) {
               EOArticlePrestation artp = ligne.prestationLigne().catalogueArticle().article().articlePrestation();
               tmCktlXMLWriter.writeElement("responsable", StringTool.ifNull(artp.artpCfcResponsable(), ""));
               if (artp.artpCfcDateDebut() != null) {
                   tmCktlXMLWriter.writeElement("datedebut", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateDebut()));
               }
               if (artp.artpCfcDateFin() != null) {
                   tmCktlXMLWriter.writeElement("datefin", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateFin()));
               }
               tmCktlXMLWriter.writeElement("duree", StringTool.ifNull(artp.artpCfcDuree(), ""));
               tmCktlXMLWriter.writeElement("nodeclaration", StringTool.ifNull(artp.artpCfcNoDeclaration(), ""));
           }

           tmCktlXMLWriter.endElement();// typeLigne
       }
   }

   private int genererPDF(InputStream xmlstream, int streamSize, String maquetteID) throws Exception {
       if (!printer.checkTemplate(maquetteID)) {
           throw new Exception("Le modèle " + maquetteID + " n'est pas présent sur le service d'impression");
       }
       printer.setJobAliveTimeout(300);
       int jobId = printer.printFileDiffered(maquetteID, xmlstream, streamSize);
       // On continue s'il n'y a pas d'erreurs.
       if (printer.hasSuccess()) {
           // OK. On attend la fin de creation du document...
           // On pose les verrous pour pouvoir executer "wait"
           synchronized (this) {
               do {
                   // On attend 1 seconde avant de redemander
                   try {
                       wait(1000);
                   }
                   catch (Exception e) {
                   }
                   // On verifie l'etat d'impression et on s'arrete si c'est fini
                   printer.getPrintStatus(jobId);
               }
               while (printer.isPrintInProgress());
           }
       }
       if (printer.hasError())
           throw new Exception("Erreur lors de l'impression PDF : " + printer.getMessage());
       return jobId;
   }

   private InputStream getPdfStream(int jobId) throws Exception {
       InputStream loc = printer.getPrintResult(jobId);
       if (printer.getContentSize() == 0) {
           throw new Exception("La longueur du flux PDF généré est nulle.");
       }
       return loc;
   }

   private class MyPRLIGComp extends NSComparator {
       public int compare(Object o1, Object o2) {
           NSTimestamp d1 = ((NSTimestamp) ((EOPrestationLigne) o1).prligDate());
           NSTimestamp d2 = ((NSTimestamp) ((EOPrestationLigne) o2).prligDate());
           if (d1 == null || d2 == null) {
               return NSComparator.OrderedAscending;
           }
           return d1.compare(d2);
       }
   }

   private class MyFLIGComp extends NSComparator {
       public int compare(Object o1, Object o2) {
           NSTimestamp d1 = ((NSTimestamp) ((EOFacturePapierLigne) o1).fligDate());
           NSTimestamp d2 = ((NSTimestamp) ((EOFacturePapierLigne) o2).fligDate());
           if (d1 == null || d2 == null) {
               return NSComparator.OrderedAscending;
           }
           return d1.compare(d2);
       }
   }

}