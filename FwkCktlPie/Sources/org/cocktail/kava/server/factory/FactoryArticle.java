package org.cocktail.kava.server.factory;

import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.factory.FactoryArticlePrestation;
import org.cocktail.kava.server.metier.EOArticle;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryArticle extends Factory {
	
	
	public static EOArticle newObject(EOEditingContext ec) {
		EOArticle object = (EOArticle) Factory.instanceForEntity(ec, EOArticle.ENTITY_NAME);
		object.setArticlePrestationRelationship(FactoryArticlePrestation.newObject(ec));
		ec.insertObject(object);
		return object;
	}

}
