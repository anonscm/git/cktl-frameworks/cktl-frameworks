package org.cocktail.kava.server.factory;

import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.factory.FactoryCataloguePrestation;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.metier.EOCatalogue;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCatalogue extends Factory {
	
	
	
	public static EOCatalogue newObject(EOEditingContext ec) {
		EOCatalogue object = (EOCatalogue) Factory.instanceForEntity(ec, EOCatalogue.ENTITY_NAME);
		object.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
		object.setCatDateDebut(Factory.getDateJour());
		object.setCatDateFin(null);
		object.setTypeApplicationRelationship(FinderTypeApplication.typeApplicationPrestationExterne(ec));
		object.setCataloguePrestationRelationship(FactoryCataloguePrestation.newObject(ec));
		ec.insertObject(object);
		return object;
	}
	
	
	public static void recopieInfosBudgetaires(EOCatalogue src, EOCatalogue dest) {
		if(src==null || dest==null) { return; }
		
		dest.setCatDateDebut( src.catDateDebut() );
		dest.setCatDateFin( src.catDateFin() );
		dest.cataloguePrestation().setCatDateVote( src.cataloguePrestation().catDateVote() );
		
		dest.setFournisUlr( src.fournisUlr() );
		dest.cataloguePrestation().setPlanComptableRecette( src.cataloguePrestation().planComptableRecette() );
		dest.cataloguePrestation().setPlanComptableDepense( src.cataloguePrestation().planComptableDepense() );
		dest.cataloguePrestation().setOrganRecette( src.cataloguePrestation().organRecette() );
		dest.cataloguePrestation().setLolfNomenclatureRecette( src.cataloguePrestation().lolfNomenclatureRecette() );
		dest.setCodeMarche( src.codeMarche() );
	}

	
}
