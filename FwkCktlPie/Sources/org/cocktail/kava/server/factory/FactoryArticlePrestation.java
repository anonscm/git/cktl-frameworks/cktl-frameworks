package org.cocktail.kava.server.factory;

import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.metier.EOArticlePrestation;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryArticlePrestation extends Factory {
	
	
	public static EOArticlePrestation newObject(EOEditingContext ec) {
		EOArticlePrestation object = (EOArticlePrestation) Factory.instanceForEntity(ec, EOArticlePrestation.ENTITY_NAME);
		object.setArtpInvisibleWeb("N");
		object.setArtpQteDispo(new Integer(-1));
		ec.insertObject(object);
		
		return object;
	}

}
