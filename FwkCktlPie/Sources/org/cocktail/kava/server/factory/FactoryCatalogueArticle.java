package org.cocktail.kava.server.factory;

import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCatalogueArticle extends Factory {

	
	
	public static EOCatalogueArticle newObject(EOEditingContext ec) {
		EOCatalogueArticle object = (EOCatalogueArticle) Factory.instanceForEntity(ec, EOCatalogueArticle.ENTITY_NAME);
		
		object.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
		object.setArticleRelationship(FactoryArticle.newObject(ec));
		ec.insertObject(object);
		return object;
	}

	public static EOCatalogueArticle newObject(EOEditingContext ec, EOCatalogue catalogue) {
		EOCatalogueArticle object = newObject(ec);
		object.setCatalogueRelationship(catalogue);
		return object;
	}

	public static EOCatalogueArticle newObject(EOEditingContext ec, EOCatalogueArticle catalogueArticle) {
		EOCatalogueArticle object = newObject(ec);
		if (catalogueArticle != null) {
			object.setCatalogueRelationship(catalogueArticle.catalogue());
			object.article().setArticlePereRelationship(catalogueArticle.article());
		}
		return object;
	}
	
	
	
	public static EOCatalogueArticle createCatalogueArticle(EOEditingContext ec) {
		EOCatalogueArticle object = (EOCatalogueArticle) Factory.instanceForEntity(ec, EOCatalogueArticle.ENTITY_NAME);
		object.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
		object.setArticleRelationship(FactoryArticle.newObject(ec));
		//object.setCatalogueRelationship(catalogue);
		ec.insertObject(object);
		return object;

	}
	
	
	
}
