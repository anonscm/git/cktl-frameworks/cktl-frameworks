package org.cocktail.kava.server.factory;

import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.metier.EOCataloguePrestation;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCataloguePrestation extends Factory {
	
	
	
	public static EOCataloguePrestation newObject(EOEditingContext ec) {
		EOCataloguePrestation object = (EOCataloguePrestation) Factory.instanceForEntity(ec, EOCataloguePrestation.ENTITY_NAME);
		ec.insertObject(object);
		return object;
	}

}
