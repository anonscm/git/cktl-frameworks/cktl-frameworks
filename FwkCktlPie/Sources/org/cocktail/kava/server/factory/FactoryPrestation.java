/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.factory;

import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOUtilisateur;
import org.cocktail.kava.server.procedures.ApiPrestation;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

public class FactoryPrestation extends Factory {
	 private static CktlWebApplication app = (CktlWebApplication)WOApplication.application();

	public FactoryPrestation() {
		super();
	}

	public FactoryPrestation(boolean withLog) {
		super(withLog);
	}

	public static EOPrestation newObject(EOEditingContext ec) {
		EOPrestation object = (EOPrestation) Factory.instanceForEntity(ec, EOPrestation.ENTITY_NAME);
		EOTypeEtat typeEtat = FinderTypeEtat.typeEtatValide(ec);
		typeEtat = (EOTypeEtat) EOUtilities.localInstanceOfObject(ec, typeEtat);
		object.setTypeEtatRelationship(typeEtat);
		object.setPrestDate(Factory.getDateJour());
		ec.insertObject(object);
		return object;
	}
	
	 public static void valideClient(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.prestDateValideClient() != null)
         return;
     if(prestation.typeEtat().equals(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec)))
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la valider !");
     if(!prestation.isValidableClient(ec))
         throw new Exception("Il manque des informations budg\351taires c\364t\351 client, impossible de valider la prestation " + prestation.prestNumero() + " !");
     prestation.setPrestDateValideClient(new NSTimestamp());
     try
     {
         ec.saveChanges();
     }
     catch(java.lang.Exception e)
     {
         ec.revert();
     }
 }

 public static void devalideClient(CktlDataBus dataBus, com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation, EOUtilisateur utilisateur)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.prestDateValideClient() == null)
         return;
     if(prestation.typeEtat().equals(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec)))
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la d\351valider !");
     FactoryPrestation.decloture(ec, prestation);
     FactoryPrestation.devalidePrestataire(ec, prestation);
     ApiPrestation.devalidePrestationClient(dataBus, ec, prestation, utilisateur);
     if(prestation.prestationLignes() != null)
         ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
     ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
 }

 public static void validePrestataire(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.prestDateValidePrest() != null)
         return;
     if(prestation.typeEtat().equals(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec)))
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la valider !");
//     if(!prestation.isValidablePrest())
//     {
//         throw new Exception("Il manque des informations budg\351taires c\364t\351 prestataire, impossible de valider la prestation " + prestation.prestNumero() + " !");
//     } else
//     {
         FactoryPrestation.valideClient(ec, prestation);
         prestation.setPrestDateValidePrest(new NSTimestamp());
         ec.saveChanges();
         return;
//     }
 }

 public static void devalidePrestataire(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.prestDateValidePrest() == null)
         return;
     if(prestation.typeEtat().equals(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec)))
     {
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la d\351valider !");
     } else
     {
         FactoryPrestation.decloture(ec, prestation);
         prestation.setPrestDateValidePrest(null);
         ec.saveChanges();
         return;
     }
 }

 public static void cloture(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.prestDateCloture() != null)
         return;
     if(prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec)))
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la cl\364turer !");
//     if(!prestation.isCloturable())
//     {
//         throw new Exception("Il manque des informations, impossible de cl\364turer la prestation " + prestation.prestNumero() + " !");
//     } else
//     {
         valideClient(ec, prestation);
         validePrestataire(ec, prestation);
         prestation.setPrestDateCloture(new NSTimestamp());
         ec.saveChanges();
         return;
//     }
 }

 public static void decloture(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.prestDateCloture() == null)
         return;
     if(prestation.typeEtat().equals(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec)))
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la \"d\351cl\364turer\" !");
     if(prestation.prestDateFacturation() != null || prestation.facturePapiers() != null && prestation.facturePapiers().count() > 0)
     {
         throw new Exception("La prestation " + prestation.prestNumero() + " est d\351j\340 factur\351e, impossible de la \"d\351cl\364turer\" !");
     } else
     {
         prestation.setPrestDateCloture(null);
         ec.saveChanges();
         return;
     }
 }

 public static void genereFacture(com.webobjects.eocontrol.EOEditingContext ec, EOPrestation prestation)
     throws java.lang.Exception
 {
     if(prestation == null)
         return;
     if(prestation.typeEtat().equals(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec)))
         throw new Exception("La prestation " + prestation.prestNumero() + " est archiv\351e, impossible de la facturer !");
     if(prestation.prestDateCloture() == null)
         throw new Exception("La prestation " + prestation.prestNumero() + " n'est pas cl\364tur\351e, impossible de la facturer !");
//     if(!prestation.isFacturable())
//         throw new Exception("Il manque des informations, impossible de facturer la prestation " + prestation.prestNumero() + " !");
     org.cocktail.kava.server.procedures.ApiPrestation.genereFacturePapier(app.dataBus(), ec, prestation, prestation.utilisateur());
     if(prestation.prestationLignes() != null)
         ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(prestation.prestationLignes()));
     ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(prestation)));
 }

 public static void archivePrestation(org.cocktail.fwkcktlwebapp.server.database.CktlDataBus databus, com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation, EOUtilisateur utilisateur)
     throws java.lang.Exception
 {
     if(prestation.prestDateValideClient() != null)
         FactoryPrestation.devalideClient(databus, ec, prestation, utilisateur);
     prestation.setTypeEtatRelationship(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatAnnule(ec));
     ec.saveChanges();
 }

 public static void reactivePrestation(com.webobjects.eocontrol.EOEditingContext ec, org.cocktail.kava.server.metier.EOPrestation prestation)
     throws java.lang.Exception
 {
     prestation.setTypeEtatRelationship(org.cocktail.kava.server.finder.FinderTypeEtat.typeEtatValide(ec));
     ec.saveChanges();
 }




}
