/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurPreference.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOUtilisateurPreference extends  FwkPieRecord {
	public static final String ENTITY_NAME = "UtilisateurPreference";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.utilisateur_preference";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "upId";

	public static final String UP_VALUE_KEY = "upValue";

// Attributs non visibles
	public static final String PREF_ID_KEY = "prefId";
	public static final String UP_ID_KEY = "upId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String UP_VALUE_COLKEY = "UP_VALUE";

	public static final String PREF_ID_COLKEY = "PREF_id";
	public static final String UP_ID_COLKEY = "up_id";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String PREFERENCE_KEY = "preference";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String upValue() {
    return (String) storedValueForKey(UP_VALUE_KEY);
  }

  public void setUpValue(String value) {
    takeStoredValueForKey(value, UP_VALUE_KEY);
  }

  public org.cocktail.kava.server.metier.EOPreferences preference() {
    return (org.cocktail.kava.server.metier.EOPreferences)storedValueForKey(PREFERENCE_KEY);
  }
  
	public void setPreference(org.cocktail.kava.server.metier.EOPreferences value) {
		takeStoredValueForKey(value,PREFERENCE_KEY);
	}


  public void setPreferenceRelationship(org.cocktail.kava.server.metier.EOPreferences value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPreferences oldValue = preference();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PREFERENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PREFERENCE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.kava.server.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOUtilisateurPreference avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOUtilisateurPreference createEOUtilisateurPreference(EOEditingContext editingContext			) {
    EOUtilisateurPreference eo = (EOUtilisateurPreference) createAndInsertInstance(editingContext, _EOUtilisateurPreference.ENTITY_NAME);    
    return eo;
  }

  
	  public EOUtilisateurPreference localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurPreference)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurPreference creerInstance(EOEditingContext editingContext) {
	  		EOUtilisateurPreference object = (EOUtilisateurPreference)createAndInsertInstance(editingContext, _EOUtilisateurPreference.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOUtilisateurPreference localInstanceIn(EOEditingContext editingContext, EOUtilisateurPreference eo) {
    EOUtilisateurPreference localInstance = (eo == null) ? null : (EOUtilisateurPreference)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOUtilisateurPreference#localInstanceIn a la place.
   */
	public static EOUtilisateurPreference localInstanceOf(EOEditingContext editingContext, EOUtilisateurPreference eo) {
		return EOUtilisateurPreference.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurPreference fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurPreference fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurPreference eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurPreference)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurPreference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurPreference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurPreference eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurPreference)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurPreference fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurPreference eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurPreference ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurPreference fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
