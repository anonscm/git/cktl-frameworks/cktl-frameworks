/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOConventionNonLimitative.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOConventionNonLimitative extends  FwkPieRecord {
	public static final String ENTITY_NAME = "ConventionNonLimitative";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_convention_non_limitative";



	// Attributes


	public static final String CNL_CON_REFERENCE_KEY = "cnlConReference";
	public static final String CNL_MONTANT_TOTAL_KEY = "cnlMontantTotal";
	public static final String CNL_TYPE_MONTANT_KEY = "cnlTypeMontant";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CNL_CON_REFERENCE_COLKEY = "CNL_CON_REFERENCE";
	public static final String CNL_MONTANT_TOTAL_COLKEY = "CNL_MONTANT_TOTAL";
	public static final String CNL_TYPE_MONTANT_COLKEY = "CNL_TYPE_MONTANT";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";
	public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";



	// Accessors methods
  public String cnlConReference() {
    return (String) storedValueForKey(CNL_CON_REFERENCE_KEY);
  }

  public void setCnlConReference(String value) {
    takeStoredValueForKey(value, CNL_CON_REFERENCE_KEY);
  }

  public java.math.BigDecimal cnlMontantTotal() {
    return (java.math.BigDecimal) storedValueForKey(CNL_MONTANT_TOTAL_KEY);
  }

  public void setCnlMontantTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CNL_MONTANT_TOTAL_KEY);
  }

  public String cnlTypeMontant() {
    return (String) storedValueForKey(CNL_TYPE_MONTANT_KEY);
  }

  public void setCnlTypeMontant(String value) {
    takeStoredValueForKey(value, CNL_TYPE_MONTANT_KEY);
  }

  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.application.serveur.eof.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOOrgan organ() {
    return (org.cocktail.kava.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.kava.server.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.kava.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCreditDep() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
  }
  
	public void setTypeCreditDep(org.cocktail.application.serveur.eof.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_DEP_KEY);
	}


  public void setTypeCreditDepRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCreditDep();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_DEP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCreditRec() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
  }
  
	public void setTypeCreditRec(org.cocktail.application.serveur.eof.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_REC_KEY);
	}


  public void setTypeCreditRecRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCreditRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
    }
  }
  

/**
 * Créer une instance de EOConventionNonLimitative avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOConventionNonLimitative createEOConventionNonLimitative(EOEditingContext editingContext, String cnlConReference
, java.math.BigDecimal cnlMontantTotal
, String cnlTypeMontant
, org.cocktail.application.serveur.eof.EOExercice exercice			) {
    EOConventionNonLimitative eo = (EOConventionNonLimitative) createAndInsertInstance(editingContext, _EOConventionNonLimitative.ENTITY_NAME);    
		eo.setCnlConReference(cnlConReference);
		eo.setCnlMontantTotal(cnlMontantTotal);
		eo.setCnlTypeMontant(cnlTypeMontant);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EOConventionNonLimitative localInstanceIn(EOEditingContext editingContext) {
	  		return (EOConventionNonLimitative)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOConventionNonLimitative creerInstance(EOEditingContext editingContext) {
	  		EOConventionNonLimitative object = (EOConventionNonLimitative)createAndInsertInstance(editingContext, _EOConventionNonLimitative.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOConventionNonLimitative localInstanceIn(EOEditingContext editingContext, EOConventionNonLimitative eo) {
    EOConventionNonLimitative localInstance = (eo == null) ? null : (EOConventionNonLimitative)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOConventionNonLimitative#localInstanceIn a la place.
   */
	public static EOConventionNonLimitative localInstanceOf(EOEditingContext editingContext, EOConventionNonLimitative eo) {
		return EOConventionNonLimitative.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOConventionNonLimitative fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOConventionNonLimitative fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConventionNonLimitative eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConventionNonLimitative)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOConventionNonLimitative fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOConventionNonLimitative fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConventionNonLimitative eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConventionNonLimitative)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOConventionNonLimitative fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConventionNonLimitative eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConventionNonLimitative ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOConventionNonLimitative fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
