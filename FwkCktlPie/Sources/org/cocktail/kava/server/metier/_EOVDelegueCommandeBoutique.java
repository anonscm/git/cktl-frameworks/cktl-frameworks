/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVDelegueCommandeBoutique.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOVDelegueCommandeBoutique extends  FwkPieRecord {
	public static final String ENTITY_NAME = "VDelegueCommandeBoutique";
	public static final String ENTITY_TABLE_NAME = "JEFY_RECETTE.V_DELEGUE_COMMANDE_BOUTIQUE";



	// Attributes


	public static final String BOUTIQUE_ID_KEY = "boutiqueId";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String BOUTIQUE_ID_COLKEY = "BOUTIQUE_ID";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PERS_ID_COLKEY = "PERS_ID";



	// Relationships
	public static final String BOUTIQUE_KEY = "boutique";
	public static final String INDIVIDU_ULR_KEY = "individuUlr";
	public static final String PERSONNE_KEY = "personne";
	public static final String STRUCTURE_ULR_KEY = "structureUlr";



	// Accessors methods
  public Integer boutiqueId() {
    return (Integer) storedValueForKey(BOUTIQUE_ID_KEY);
  }

  public void setBoutiqueId(Integer value) {
    takeStoredValueForKey(value, BOUTIQUE_ID_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutique boutique() {
    return (org.cocktail.kava.server.metier.EOBoutique)storedValueForKey(BOUTIQUE_KEY);
  }
  
	public void setBoutique(org.cocktail.kava.server.metier.EOBoutique value) {
		takeStoredValueForKey(value,BOUTIQUE_KEY);
	}


  public void setBoutiqueRelationship(org.cocktail.kava.server.metier.EOBoutique value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOBoutique oldValue = boutique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BOUTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BOUTIQUE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOIndividuUlr individuUlr() {
    return (org.cocktail.kava.server.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
  }
  
	public void setIndividuUlr(org.cocktail.kava.server.metier.EOIndividuUlr value) {
		takeStoredValueForKey(value,INDIVIDU_ULR_KEY);
	}


  public void setIndividuUlrRelationship(org.cocktail.kava.server.metier.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOIndividuUlr oldValue = individuUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPersonne personne() {
    return (org.cocktail.kava.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }
  
	public void setPersonne(org.cocktail.kava.server.metier.EOPersonne value) {
		takeStoredValueForKey(value,PERSONNE_KEY);
	}


  public void setPersonneRelationship(org.cocktail.kava.server.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOStructureUlr structureUlr() {
    return (org.cocktail.kava.server.metier.EOStructureUlr)storedValueForKey(STRUCTURE_ULR_KEY);
  }
  
	public void setStructureUlr(org.cocktail.kava.server.metier.EOStructureUlr value) {
		takeStoredValueForKey(value,STRUCTURE_ULR_KEY);
	}


  public void setStructureUlrRelationship(org.cocktail.kava.server.metier.EOStructureUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOStructureUlr oldValue = structureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
    }
  }
  

/**
 * Créer une instance de EOVDelegueCommandeBoutique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVDelegueCommandeBoutique createEOVDelegueCommandeBoutique(EOEditingContext editingContext, Integer boutiqueId
, String cStructure
, Integer noIndividu
, Integer persId
, org.cocktail.kava.server.metier.EOBoutique boutique, org.cocktail.kava.server.metier.EOIndividuUlr individuUlr, org.cocktail.kava.server.metier.EOPersonne personne, org.cocktail.kava.server.metier.EOStructureUlr structureUlr			) {
    EOVDelegueCommandeBoutique eo = (EOVDelegueCommandeBoutique) createAndInsertInstance(editingContext, _EOVDelegueCommandeBoutique.ENTITY_NAME);    
		eo.setBoutiqueId(boutiqueId);
		eo.setCStructure(cStructure);
		eo.setNoIndividu(noIndividu);
		eo.setPersId(persId);
    eo.setBoutiqueRelationship(boutique);
    eo.setIndividuUlrRelationship(individuUlr);
    eo.setPersonneRelationship(personne);
    eo.setStructureUlrRelationship(structureUlr);
    return eo;
  }

  
	  public EOVDelegueCommandeBoutique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVDelegueCommandeBoutique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVDelegueCommandeBoutique creerInstance(EOEditingContext editingContext) {
	  		EOVDelegueCommandeBoutique object = (EOVDelegueCommandeBoutique)createAndInsertInstance(editingContext, _EOVDelegueCommandeBoutique.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOVDelegueCommandeBoutique localInstanceIn(EOEditingContext editingContext, EOVDelegueCommandeBoutique eo) {
    EOVDelegueCommandeBoutique localInstance = (eo == null) ? null : (EOVDelegueCommandeBoutique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVDelegueCommandeBoutique#localInstanceIn a la place.
   */
	public static EOVDelegueCommandeBoutique localInstanceOf(EOEditingContext editingContext, EOVDelegueCommandeBoutique eo) {
		return EOVDelegueCommandeBoutique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVDelegueCommandeBoutique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVDelegueCommandeBoutique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVDelegueCommandeBoutique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVDelegueCommandeBoutique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVDelegueCommandeBoutique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVDelegueCommandeBoutique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVDelegueCommandeBoutique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVDelegueCommandeBoutique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVDelegueCommandeBoutique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVDelegueCommandeBoutique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVDelegueCommandeBoutique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVDelegueCommandeBoutique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
