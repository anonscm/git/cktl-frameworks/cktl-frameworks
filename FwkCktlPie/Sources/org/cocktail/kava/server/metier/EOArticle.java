
// EOArticle.java
// 

package org.cocktail.kava.server.metier;



import org.cocktail.kava.server.finder.FinderTypeArticle;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOArticle extends _EOArticle {
	
	
	public EOArticle() {
		super();
	}
	
	

	
	/** retourne la cle primaire artId de l'objet */ 
	public Number getPkArtId() {
		NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(),this);
		if(dico!=null) {
			return (Number)dico.objectForKey(EOArticle.ART_ID_KEY);
		}
		else {
			return null;
		}
	}

	
	
	
	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public java.lang.Object valueForKey(java.lang.String arg0) {
		java.lang.Object obj = null;
		try {
			obj = super.valueForKey(arg0);
		}
		catch (java.lang.Throwable e) {
			int i = 0;
			for (com.webobjects.foundation.NSArray list = (com.webobjects.foundation.NSArray) articlePrestationWebs().valueForKey(arg0); (obj == null || obj
					.equals(NullValue))
					&& list.count() > i; i++)
				obj = list.objectAtIndex(i);

		}
		return obj;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
     * Creer un nouvel article
     * 
     * @param ec
     * @return
     */
    public static EOArticle creerNouvelArticle(EOEditingContext ec) {
            if (ec == null) {
                    throw new NSValidation.ValidationException(
                                    "Erreur de creation d'une nouvelle instance d'Article. EOEditingContext absent.");
            }
            EOArticle article = EOArticle.creerInstance(ec);
            article.setTypeArticle(FinderTypeArticle.typeArticleArticle(ec));
            article.setArtLibelle("");
            return article;
    }
	
	
}
