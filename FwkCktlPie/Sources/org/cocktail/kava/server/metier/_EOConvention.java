/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOConvention.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOConvention extends  FwkPieRecord {
	public static final String ENTITY_NAME = "Convention";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_convention";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "conOrdre";

	public static final String CON_INDEX_KEY = "conIndex";
	public static final String CON_OBJET_KEY = "conObjet";
	public static final String CON_OBJET_COURT_KEY = "conObjetCourt";
	public static final String CON_REFERENCE_EXTERNE_KEY = "conReferenceExterne";
	public static final String CON_SUPPR_KEY = "conSuppr";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CON_INDEX_COLKEY = "CON_INDEX";
	public static final String CON_OBJET_COLKEY = "CON_OBJET";
	public static final String CON_OBJET_COURT_COLKEY = "CON_OBJET_COURT";
	public static final String CON_REFERENCE_EXTERNE_COLKEY = "CON_REFERENCE_EXTERNE";
	public static final String CON_SUPPR_COLKEY = "CON_SUPPR";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CONVENTION_LIMITATIVES_KEY = "conventionLimitatives";
	public static final String CONVENTION_NON_LIMITATIVES_KEY = "conventionNonLimitatives";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public Integer conIndex() {
    return (Integer) storedValueForKey(CON_INDEX_KEY);
  }

  public void setConIndex(Integer value) {
    takeStoredValueForKey(value, CON_INDEX_KEY);
  }

  public String conObjet() {
    return (String) storedValueForKey(CON_OBJET_KEY);
  }

  public void setConObjet(String value) {
    takeStoredValueForKey(value, CON_OBJET_KEY);
  }

  public String conObjetCourt() {
    return (String) storedValueForKey(CON_OBJET_COURT_KEY);
  }

  public void setConObjetCourt(String value) {
    takeStoredValueForKey(value, CON_OBJET_COURT_KEY);
  }

  public String conReferenceExterne() {
    return (String) storedValueForKey(CON_REFERENCE_EXTERNE_KEY);
  }

  public void setConReferenceExterne(String value) {
    takeStoredValueForKey(value, CON_REFERENCE_EXTERNE_KEY);
  }

  public String conSuppr() {
    return (String) storedValueForKey(CON_SUPPR_KEY);
  }

  public void setConSuppr(String value) {
    takeStoredValueForKey(value, CON_SUPPR_KEY);
  }

  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.application.serveur.eof.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public NSArray conventionLimitatives() {
    return (NSArray)storedValueForKey(CONVENTION_LIMITATIVES_KEY);
  }
  
  //ICI
  public void setConventionLimitatives(NSArray values) {
	  takeStoredValueForKey(values,CONVENTION_LIMITATIVES_KEY);
  }
  

  public NSArray conventionLimitatives(EOQualifier qualifier) {
    return conventionLimitatives(qualifier, null);
  }

  public NSArray conventionLimitatives(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = conventionLimitatives();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToConventionLimitativesRelationship(org.cocktail.kava.server.metier.EOConventionLimitative object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
  }

  public void removeFromConventionLimitativesRelationship(org.cocktail.kava.server.metier.EOConventionLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
  }

  public org.cocktail.kava.server.metier.EOConventionLimitative createConventionLimitativesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ConventionLimitative");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTION_LIMITATIVES_KEY);
    return (org.cocktail.kava.server.metier.EOConventionLimitative) eo;
  }

  public void deleteConventionLimitativesRelationship(org.cocktail.kava.server.metier.EOConventionLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_LIMITATIVES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllConventionLimitativesRelationships() {
    Enumeration objects = conventionLimitatives().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteConventionLimitativesRelationship((org.cocktail.kava.server.metier.EOConventionLimitative)objects.nextElement());
    }
  }

  public NSArray conventionNonLimitatives() {
    return (NSArray)storedValueForKey(CONVENTION_NON_LIMITATIVES_KEY);
  }
  
  //ICI
  public void setConventionNonLimitatives(NSArray values) {
	  takeStoredValueForKey(values,CONVENTION_NON_LIMITATIVES_KEY);
  }
  

  public NSArray conventionNonLimitatives(EOQualifier qualifier) {
    return conventionNonLimitatives(qualifier, null);
  }

  public NSArray conventionNonLimitatives(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = conventionNonLimitatives();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToConventionNonLimitativesRelationship(org.cocktail.kava.server.metier.EOConventionNonLimitative object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
  }

  public void removeFromConventionNonLimitativesRelationship(org.cocktail.kava.server.metier.EOConventionNonLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
  }

  public org.cocktail.kava.server.metier.EOConventionNonLimitative createConventionNonLimitativesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ConventionNonLimitative");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTION_NON_LIMITATIVES_KEY);
    return (org.cocktail.kava.server.metier.EOConventionNonLimitative) eo;
  }

  public void deleteConventionNonLimitativesRelationship(org.cocktail.kava.server.metier.EOConventionNonLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllConventionNonLimitativesRelationships() {
    Enumeration objects = conventionNonLimitatives().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteConventionNonLimitativesRelationship((org.cocktail.kava.server.metier.EOConventionNonLimitative)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOConvention createEOConvention(EOEditingContext editingContext, org.cocktail.application.serveur.eof.EOExercice exercice			) {
    EOConvention eo = (EOConvention) createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME);    
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EOConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOConvention)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOConvention creerInstance(EOEditingContext editingContext) {
	  		EOConvention object = (EOConvention)createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOConvention localInstanceIn(EOEditingContext editingContext, EOConvention eo) {
    EOConvention localInstance = (eo == null) ? null : (EOConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOConvention#localInstanceIn a la place.
   */
	public static EOConvention localInstanceOf(EOEditingContext editingContext, EOConvention eo) {
		return EOConvention.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
