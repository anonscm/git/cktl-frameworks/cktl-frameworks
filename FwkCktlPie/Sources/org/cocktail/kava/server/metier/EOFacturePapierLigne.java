// EOFacturePapierLigne.java
// 

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.AFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOFacturePapierLigne extends _EOFacturePapierLigne {
	public static final String FLIG_TOTAL_TVA_KEY = "fligTotalTva";
	public static final String SEQ_EOFACTUREPAPIERLIGNE_ENTITY_NAME = "SeqEOFacturePapierLigne";

	public EOFacturePapierLigne() {
		super();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise
	 *            HT
	 */
	public void setFligArtHt(BigDecimal aValue) {
		super.setFligArtHt(aValue);
		updateTotalHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total
	 *            remise TTC
	 */
	public void setFligArtTtc(BigDecimal aValue) {
		super.setFligArtTtc(aValue);
		updateTotalTtc();
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite et recalcule les totaux remises HT et
	 *            TTC
	 */
	public void setFligQuantite(BigDecimal aValue) {
		// si valeur change pas, return
		if (aValue == null && fligQuantite() == null) {
			return;
		}
		if (aValue == null && fligQuantite().equals(aValue)) {
			return;
		}
		if (fligQuantite() == null && aValue.equals(fligQuantite())) {
			return;
		}
		if (aValue.equals(fligQuantite())) {
			return;
		}
		super.setFligQuantite(aValue);
		updateTotaux();

		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				((EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i))
						.setFligQuantite(fligQuantite());
			}
		}
	}

	public BigDecimal fligTotalTva() {
		if (fligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (fligTotalHt() == null) {
			return fligTotalTtc();
		}
		return fligTotalTtc().subtract(fligTotalHt());
	}

	public void updateTotaux() {
		updateTotalHt();
		updateTotalTtc();
	}

	/**
	 * met a jour le total ht de la ligne, fonction du prix unitaire, de la
	 * quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ht a 0 ou plus
	 */
	private void updateTotalHt() {
		if (fligArtHt() != null && fligQuantite() != null) {
			setFligTotalHt(applyRemise(fligArtHt().multiply(fligQuantite())
					.setScale(fligArtHt().scale() > 0 ? 2 : 0,
							BigDecimal.ROUND_HALF_UP)));
		} else {
			setFligTotalHt(null);
		}
	}

	/**
	 * met a jour le total ttc de la ligne, fonction du prix unitaire, de la
	 * quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ttc a 0 ou plus
	 */
	private void updateTotalTtc() {
		if (fligArtTtc() != null && fligQuantite() != null) {
			setFligTotalTtc(applyRemise(fligArtTtc().multiply(fligQuantite())
					.setScale(fligArtTtc().scale() > 0 ? 2 : 0,
							BigDecimal.ROUND_HALF_UP)));
		} else {
			setFligTotalTtc(null);
		}
	}

	/**
	 * @param b
	 * @return le montant eventuellement remise, scale conserve
	 */
	private BigDecimal applyRemise(BigDecimal b) {
		if (b == null) {
			return new BigDecimal(0.0);
		}
		if (facturePapier() == null
				|| facturePapier().fapRemiseGlobale() == null) {
			return b;
		}
		return b.subtract(
				b.multiply(facturePapier().fapRemiseGlobale().divide(
						new BigDecimal(100.0), BigDecimal.ROUND_HALF_UP)))
				.setScale(b.scale(), BigDecimal.ROUND_HALF_UP);
	}

	/*
	 * // If you add instance variables to store property values you // should
	 * add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the
	 * superclass). private void writeObject(java.io.ObjectOutputStream out)
	 * throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws
	 * java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave()
			throws NSValidation.ValidationException {

	}

	public Integer creerFapLigneId(EOEditingContext ec) {
		return AFinder
				.clePrimairePour(
						ec,
						EOFacturePapierLigne.ENTITY_NAME,
						EOFacturePapierLigne.FLIG_ID_KEY,
						EOFacturePapierLigne.SEQ_EOFACTUREPAPIERLIGNE_ENTITY_NAME,
						true);
	}
}
