

// EOCataloguePublic.java
// 

package org.cocktail.kava.server.metier;


import java.math.BigDecimal;

import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCataloguePublic;
import org.cocktail.kava.server.metier.EOTypePublic;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOCataloguePublic extends _EOCataloguePublic
{
    public EOCataloguePublic() {
        super();
    }

	public static EOCataloguePublic newObject(EOEditingContext ec) {
		EOCataloguePublic object = (EOCataloguePublic) Factory.instanceForEntity(ec, EOCataloguePublic.ENTITY_NAME);
		object.setCapPourcentage(new BigDecimal(0.0));
		ec.insertObject(object);
		return object;
	}

	public static EOCataloguePublic newObject(EOEditingContext ec, EOCatalogue catalogue, EOTypePublic typePublic) {
		EOCataloguePublic object = newObject(ec);
		object.setCatalogueRelationship(catalogue);
		object.setTypePublicRelationship(typePublic);
		return object;
	}



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
