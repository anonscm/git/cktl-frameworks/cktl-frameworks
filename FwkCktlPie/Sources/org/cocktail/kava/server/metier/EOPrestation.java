// EOPrestation.java
// 

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.client.metier.EOPrestationLigne;
import org.cocktail.kava.server.PieException;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.kava.server.finder.FinderTypeArticle;
import org.cocktail.kava.server.finder.FinderTypeEtat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPrestation extends _EOPrestation {

	public static final String PREST_TOTAL_HT_LIVE_KEY = "prestTotalHtLive";
	public static final String PREST_TOTAL_TTC_LIVE_KEY = "prestTotalTtcLive";
	public static final String PREST_TOTAL_TVA_LIVE_KEY = "prestTotalTvaLive";

	public EOPrestation() {
		super();
	}

	/*
	 * public boolean haveAtLeastOnePaidInvoice() { NSArray fps =
	 * facturePapiers(); EOQualifier qual }
	 */

	public Integer pkPrestId() {
		NSDictionary pk = EOUtilities.primaryKeyForObject(editingContext(),
				this);
		return (Integer) pk.valueForKey(PREST_ID_KEY);
	}

	public String libelleEtNumero() {
		return prestLibelle() + " - No " + prestNumero().intValue();
	}

	public NSArray listePrestationLignesArticles() {
		EOTypeArticle typeArtArticle = FinderTypeArticle
				.typeArticleArticle(editingContext());
		EOQualifier qualArt = new EOKeyValueQualifier(
				EOPrestationLigne.TYPE_ARTICLE_KEY,
				EOQualifier.QualifierOperatorEqual, typeArtArticle);
		return prestationLignes(qualArt);
	}

	/*
	 * Generation d'un numero de prestation en fonction de l'exercice a partir
	 * de la procedure stockee
	 */
	public static Integer generatePrestaNumero(EOExercice exercice,
			EOEditingContext ec) {

		Integer exeOrdre = (Integer) exercice
				.valueForKey(EOExercice.EXE_ORDRE_KEY);
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(exeOrdre, "010aExeOrdre");
		dico.setObjectForKey(NullValue, "020aGesCode");
		dico.setObjectForKey(EOPrestation.ENTITY_NAME.toUpperCase(),
				"030aTnuEntite");
		NSDictionary result = null;
		try {
			result = EOUtilities.executeStoredProcedureNamed(ec,
					"GetNumerotationProc", dico);
		} catch (Throwable e) {
			e.printStackTrace();
			ec.revert();
			return null;
		}
		if (result == null) {
			return null;
		} else {
			Integer i = (Integer) result.objectForKey("040aNumNumero");
			System.out.println("prestaNumero=" + i);
			return i;
		}
	}

	public void setPrestRemiseGlobale(BigDecimal aValue) {
		super.setPrestRemiseGlobale(aValue);
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				((EOPrestationLigne) prestationLignes().objectAtIndex(i))
						.updateTotaux();
			}
		}
	}

	/**
	 * @param aValue
	 *            (O/N) applique ou non la tva, et met a jour les lignes de la
	 *            prestation, et recalcule les totaux
	 */
	public void setPrestApplyTva(String aValue) {
		super.setPrestApplyTva(aValue);
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				EOPrestationLigne pl = (EOPrestationLigne) prestationLignes()
						.objectAtIndex(i);
				if ("N".equalsIgnoreCase(prestApplyTva())) {
					pl.setTvaRelationship(null);
					pl.setPrligArtTtc(pl.prligArtHt());
				} else {
					pl.setTvaRelationship(pl.tvaInitial());
					pl.setPrligArtTtc(pl.prligArtTtcInitial());
				}
			}
		}
	}

	public BigDecimal prestTotalHtLive() {
		if (prestationLignes() == null || prestationLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) prestationLignes().valueForKey(
				"@sum." + EOPrestationLigne.PRLIG_TOTAL_HT_KEY);
	}

	public BigDecimal prestTotalTtcLive() {
		if (prestationLignes() == null || prestationLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) prestationLignes().valueForKey(
				"@sum." + EOPrestationLigne.PRLIG_TOTAL_TTC_KEY);
	}

	public BigDecimal prestTotalTvaLive() {
		if (prestationLignes() == null || prestationLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) prestationLignes().valueForKey(
				"@sum." + EOPrestationLigne.PRLIG_TOTAL_TVA_KEY);
	}

	public void updateTotaux() {
		// NSArray lignes = (NSArray)valueForKey(PRESTATION_LIGNES_KEY);
		BigDecimal total = prestTotalHtLive();
		setPrestTotalHt(total);
		setPrestTotalTva(prestTotalTvaLive());
		total = prestTotalTtcLive();
		setPrestTotalTtc(prestTotalTtcLive());
	}

	/**
	 * Retourne true si la prestation possede au moins une facturepapier payee;
	 * false sinon
	 */
	public boolean aAuMoinsUneFacturePayee() {
		@SuppressWarnings("unchecked")
		NSArray<EOFacturePapier> fps = facturePapiers();
		for (EOFacturePapier fp : fps) {
			if (fp.fapDateReglement() != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * verifie si les informations obligatoires sont la pour valider cote client
	 * (peu importe son etape actuelle de validation)
	 * 
	 * @return
	 */
	public boolean isValidableClient(EOEditingContext ec) {
		if (!typePublic().typeApplication().equals(
				FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			return true;
		}
		if (prestationBudgetClient() == null
				|| prestationBudgetClient().organ() == null
				|| prestationBudgetClient().tauxProrata() == null
				|| prestationBudgetClient().typeCreditDep() == null
				|| prestationBudgetClient().lolfNomenclatureDepense() == null
				|| prestationBudgetClient().planComptable() == null) {
			return false;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire
	 * (peu importe son etape actuelle de validation)
	 * 
	 * @return
	 */
	public boolean isValidablePrest(EOEditingContext ec) {
		if (organ() == null || tauxProrata() == null || typeCreditRec() == null
				|| lolfNomenclatureRecette() == null) {
			return false;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour cloturer la prestation
	 * (peu importe son etape actuelle de validation)
	 * 
	 * @return
	 */
	public boolean isCloturable(EOEditingContext ec) {
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour facturer la prestation
	 * (peu importe son etape actuelle de validation)
	 * 
	 * @return
	 */
	public boolean isFacturable(EOEditingContext ec) {
		return true;
	}

	public EOPersonne personneContact() {
		EOPersonne pc = personneContactForPersonneClient(personne());
		if (pc == null) {
			EOFournisUlr fournis = fournisUlr();
			if (fournis != null && fournis.personne() != null) {
				pc = personneContactForPersonneClient(fournis.personne());
			}
		}
		return pc;
	}

	/*
	 * retourne la personne contact de cette prestation, en testant d'abord s'il
	 * n'y a pas de contact individu designe
	 */
	private EOPersonne personneContactForPersonneClient(EOPersonne personne) {

		EOPersonne pc = null;
		if (personne != null) {
			if (personne.isPersonneMorale()) { // pers morale
				if (this.individuUlr() != null) { // y a un contact physique
					pc = this.individuUlr().personne();
				} else {
					pc = personne;
				}
			} else {
				pc = personne;
			}
		} // pers physique
		else {
			if (this.individuUlr() != null) {
				pc = this.individuUlr().personne();
			}
		}
		return pc;
	}

	/*
	 * determine le contact individu de la prestation, si le fournisseur est une
	 * personne morale, alors il y a (peut-être) un contact individu, sinon le
	 * contact est la meme personne que le client
	 */
	public EOIndividuUlr individuContact() {
		EOIndividuUlr ind = individuContactForPersonneClient(personne());
		if (ind == null) {
			EOFournisUlr fournis = fournisUlr();
			if (fournis != null && fournis.personne() != null) {
				ind = individuContactForPersonneClient(fournis.personne());
			}
		}
		return ind;
	}

	private EOIndividuUlr individuContactForPersonneClient(EOPersonne personne) {
		EOIndividuUlr ind = null;
		if (personne != null) {
			if (personne.isPersonneMorale()) {
				ind = this.individuUlr();
			} else {
				ind = personne.individuUlr();
			}
		}
		return ind;
	}

	/*
	 * // If you add instance variables to store property values you // should
	 * add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the
	 * superclass). private void writeObject(java.io.ObjectOutputStream out)
	 * throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws
	 * java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave()
			throws NSValidation.ValidationException {
		updateTotaux();
	}

	/**
	 * Valider une prestation coté client (copié de PIE et Utilisé ds Formco)
	 * 
	 * @param ec
	 * @param prestation
	 * @param etatAnnule
	 * @throws PieException
	 */
	public void validerPrestationClient(EOEditingContext ec,
			EOTypeEtat etatAnnule) throws PieException {
		if ((ec == null)) {
			throw new PieException(
					"Impossible de valider la prestation coté client.");
		}
		if (etatAnnule == null) {
			etatAnnule = FinderTypeEtat.typeEtatAnnule(ec);
		}
		if (this.typeEtat().equals(etatAnnule)) {
			throw new PieException("La prestation " + this.prestNumero()
					+ " est archivée, impossible de la valider !");
		}

		if (!this.isValidableClient(ec)) {
			throw new PieException(
					"Il manque des informations budgétaires côté client, impossible de valider la prestation "
							+ this.prestNumero() + " !");
		}
		this.setPrestDateValideClient(new NSTimestamp());
	}

	/**
	 * Valider la prestation coté prestataire (copié de PIE et Utilisé ds
	 * Formco)
	 * 
	 * @param ec
	 * @param prestation
	 * @param etatAnnule
	 * @throws FormcoException
	 */
	public void validerPrestationPrestataire(EOEditingContext ec,
			EOTypeEtat etatAnnule) throws PieException {
		if ((ec == null)) {
			throw new PieException(
					"Impossible de valider la prestation coté prestataire.");
		}
		if (this.prestDateValidePrest() != null) {
			return;
		}

		if (etatAnnule == null) {
			etatAnnule = FinderTypeEtat.typeEtatAnnule(ec);
		}

		if (this.typeEtat().equals(etatAnnule)) {
			throw new PieException("La prestation " + this.prestNumero()
					+ " est archivée, impossible de la valider !");
		} else {
			validerPrestationClient(ec, etatAnnule);
			this.setPrestDateValidePrest(new NSTimestamp());
		}
	}

	/**
	 * Cloturer la prestation (copié de PIE et Utilisé ds Formco)
	 * 
	 * @param ec
	 * @param prestation
	 * @param etatAnnule
	 * @throws FormcoException
	 */
	public void cloturerPrestation(EOEditingContext ec, EOTypeEtat etatAnnule)
			throws PieException {
		if ((ec == null)) {
			throw new PieException("Impossible de cloturer la prestation.");
		}

		if (this.prestDateCloture() != null) {
			return;
		}
		if (etatAnnule == null) {
			etatAnnule = FinderTypeEtat.typeEtatAnnule(ec);
		}
		if (this.typeEtat().equals(etatAnnule)) {
			throw new PieException("La prestation " + this.prestNumero()
					+ " est archivée, impossible de la valider !");
		}

		this.validerPrestationPrestataire(ec, etatAnnule);
		this.setPrestDateCloture(new NSTimestamp());
	}
}
