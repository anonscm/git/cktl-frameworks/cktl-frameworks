/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArticlePrestation.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOArticlePrestation extends  FwkPieRecord {
	public static final String ENTITY_NAME = "ArticlePrestation";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.article_prestation";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "artId";

	public static final String ARTP_CFC_DATE_DEBUT_KEY = "artpCfcDateDebut";
	public static final String ARTP_CFC_DATE_FIN_KEY = "artpCfcDateFin";
	public static final String ARTP_CFC_DUREE_KEY = "artpCfcDuree";
	public static final String ARTP_CFC_NO_DECLARATION_KEY = "artpCfcNoDeclaration";
	public static final String ARTP_CFC_RESPONSABLE_KEY = "artpCfcResponsable";
	public static final String ARTP_INVISIBLE_WEB_KEY = "artpInvisibleWeb";
	public static final String ARTP_QTE_DISPO_KEY = "artpQteDispo";
	public static final String ARTP_QTE_MAX_KEY = "artpQteMax";
	public static final String ARTP_QTE_MAX_PAR_CMD_KEY = "artpQteMaxParCmd";
	public static final String ARTP_QTE_MIN_KEY = "artpQteMin";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String PCO_NUM_DEPENSE_KEY = "pcoNumDepense";
	public static final String PCO_NUM_RECETTE_KEY = "pcoNumRecette";
	public static final String TYPU_ID_KEY = "typuId";

//Colonnes dans la base de donnees
	public static final String ARTP_CFC_DATE_DEBUT_COLKEY = "ARTP_CFC_DATE_DEBUT";
	public static final String ARTP_CFC_DATE_FIN_COLKEY = "ARTP_CFC_DATE_FIN";
	public static final String ARTP_CFC_DUREE_COLKEY = "ARTP_CFC_DUREE";
	public static final String ARTP_CFC_NO_DECLARATION_COLKEY = "ARTP_CFC_NO_DECLARATION";
	public static final String ARTP_CFC_RESPONSABLE_COLKEY = "ARTP_CFC_RESPONSABLE";
	public static final String ARTP_INVISIBLE_WEB_COLKEY = "ARTP_INVISIBLE_WEB";
	public static final String ARTP_QTE_DISPO_COLKEY = "ARTP_QTE_DISPO";
	public static final String ARTP_QTE_MAX_COLKEY = "ARTP_QTE_MAX";
	public static final String ARTP_QTE_MAX_PAR_CMD_COLKEY = "ARTP_QTE_MAX_PAR_CMD";
	public static final String ARTP_QTE_MIN_COLKEY = "ARTP_QTE_MIN";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String PCO_NUM_DEPENSE_COLKEY = "PCO_NUM_DEPENSE";
	public static final String PCO_NUM_RECETTE_COLKEY = "PCO_NUM_RECETTE";
	public static final String TYPU_ID_COLKEY = "TYPU_ID";


	// Relationships
	public static final String PLAN_COMPTABLE_DEPENSE_KEY = "planComptableDepense";
	public static final String PLAN_COMPTABLE_RECETTE_KEY = "planComptableRecette";
	public static final String TYPE_PUBLIC_KEY = "typePublic";



	// Accessors methods
  public NSTimestamp artpCfcDateDebut() {
    return (NSTimestamp) storedValueForKey(ARTP_CFC_DATE_DEBUT_KEY);
  }

  public void setArtpCfcDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, ARTP_CFC_DATE_DEBUT_KEY);
  }

  public NSTimestamp artpCfcDateFin() {
    return (NSTimestamp) storedValueForKey(ARTP_CFC_DATE_FIN_KEY);
  }

  public void setArtpCfcDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, ARTP_CFC_DATE_FIN_KEY);
  }

  public Integer artpCfcDuree() {
    return (Integer) storedValueForKey(ARTP_CFC_DUREE_KEY);
  }

  public void setArtpCfcDuree(Integer value) {
    takeStoredValueForKey(value, ARTP_CFC_DUREE_KEY);
  }

  public String artpCfcNoDeclaration() {
    return (String) storedValueForKey(ARTP_CFC_NO_DECLARATION_KEY);
  }

  public void setArtpCfcNoDeclaration(String value) {
    takeStoredValueForKey(value, ARTP_CFC_NO_DECLARATION_KEY);
  }

  public String artpCfcResponsable() {
    return (String) storedValueForKey(ARTP_CFC_RESPONSABLE_KEY);
  }

  public void setArtpCfcResponsable(String value) {
    takeStoredValueForKey(value, ARTP_CFC_RESPONSABLE_KEY);
  }

  public String artpInvisibleWeb() {
    return (String) storedValueForKey(ARTP_INVISIBLE_WEB_KEY);
  }

  public void setArtpInvisibleWeb(String value) {
    takeStoredValueForKey(value, ARTP_INVISIBLE_WEB_KEY);
  }

  public Integer artpQteDispo() {
    return (Integer) storedValueForKey(ARTP_QTE_DISPO_KEY);
  }

  public void setArtpQteDispo(Integer value) {
    takeStoredValueForKey(value, ARTP_QTE_DISPO_KEY);
  }

  public java.math.BigDecimal artpQteMax() {
    return (java.math.BigDecimal) storedValueForKey(ARTP_QTE_MAX_KEY);
  }

  public void setArtpQteMax(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ARTP_QTE_MAX_KEY);
  }

  public java.math.BigDecimal artpQteMaxParCmd() {
    return (java.math.BigDecimal) storedValueForKey(ARTP_QTE_MAX_PAR_CMD_KEY);
  }

  public void setArtpQteMaxParCmd(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ARTP_QTE_MAX_PAR_CMD_KEY);
  }

  public java.math.BigDecimal artpQteMin() {
    return (java.math.BigDecimal) storedValueForKey(ARTP_QTE_MIN_KEY);
  }

  public void setArtpQteMin(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ARTP_QTE_MIN_KEY);
  }

  public org.cocktail.kava.server.metier.EOPlanComptable planComptableDepense() {
    return (org.cocktail.kava.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_DEPENSE_KEY);
  }
  
	public void setPlanComptableDepense(org.cocktail.kava.server.metier.EOPlanComptable value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_DEPENSE_KEY);
	}


  public void setPlanComptableDepenseRelationship(org.cocktail.kava.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPlanComptable oldValue = planComptableDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPlanComptable planComptableRecette() {
    return (org.cocktail.kava.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_RECETTE_KEY);
  }
  
	public void setPlanComptableRecette(org.cocktail.kava.server.metier.EOPlanComptable value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_RECETTE_KEY);
	}


  public void setPlanComptableRecetteRelationship(org.cocktail.kava.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPlanComptable oldValue = planComptableRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_RECETTE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypePublic typePublic() {
    return (org.cocktail.kava.server.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
  }
  
	public void setTypePublic(org.cocktail.kava.server.metier.EOTypePublic value) {
		takeStoredValueForKey(value,TYPE_PUBLIC_KEY);
	}


  public void setTypePublicRelationship(org.cocktail.kava.server.metier.EOTypePublic value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypePublic oldValue = typePublic();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PUBLIC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
    }
  }
  

/**
 * Créer une instance de EOArticlePrestation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOArticlePrestation createEOArticlePrestation(EOEditingContext editingContext, String artpInvisibleWeb
			) {
    EOArticlePrestation eo = (EOArticlePrestation) createAndInsertInstance(editingContext, _EOArticlePrestation.ENTITY_NAME);    
		eo.setArtpInvisibleWeb(artpInvisibleWeb);
    return eo;
  }

  
	  public EOArticlePrestation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArticlePrestation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArticlePrestation creerInstance(EOEditingContext editingContext) {
	  		EOArticlePrestation object = (EOArticlePrestation)createAndInsertInstance(editingContext, _EOArticlePrestation.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOArticlePrestation localInstanceIn(EOEditingContext editingContext, EOArticlePrestation eo) {
    EOArticlePrestation localInstance = (eo == null) ? null : (EOArticlePrestation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOArticlePrestation#localInstanceIn a la place.
   */
	public static EOArticlePrestation localInstanceOf(EOEditingContext editingContext, EOArticlePrestation eo) {
		return EOArticlePrestation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOArticlePrestation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOArticlePrestation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArticlePrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArticlePrestation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArticlePrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArticlePrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArticlePrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArticlePrestation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOArticlePrestation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArticlePrestation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArticlePrestation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArticlePrestation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
