/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrestation.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPrestation extends  FwkPieRecord {
	public static final String ENTITY_NAME = "Prestation";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prestId";

	public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
	public static final String PREST_APPLY_TVA_KEY = "prestApplyTva";
	public static final String PREST_COMMENTAIRE_CLIENT_KEY = "prestCommentaireClient";
	public static final String PREST_COMMENTAIRE_PREST_KEY = "prestCommentairePrest";
	public static final String PREST_DATE_KEY = "prestDate";
	public static final String PREST_DATE_CLOTURE_KEY = "prestDateCloture";
	public static final String PREST_DATE_FACTURATION_KEY = "prestDateFacturation";
	public static final String PREST_DATE_VALIDE_CLIENT_KEY = "prestDateValideClient";
	public static final String PREST_DATE_VALIDE_PREST_KEY = "prestDateValidePrest";
	public static final String PREST_LIBELLE_KEY = "prestLibelle";
	public static final String PREST_NUMERO_KEY = "prestNumero";
	public static final String PREST_REMISE_GLOBALE_KEY = "prestRemiseGlobale";
	public static final String PREST_TOTAL_HT_KEY = "prestTotalHt";
	public static final String PREST_TOTAL_TTC_KEY = "prestTotalTtc";
	public static final String PREST_TOTAL_TVA_KEY = "prestTotalTva";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CAT_ID_KEY = "catId";
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String FOU_ORDRE_PREST_KEY = "fouOrdrePrest";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PERS_ID_KEY = "persId";
	public static final String PREST_ID_KEY = "prestId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYPU_ID_KEY = "typuId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PERSONNE_PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String PREST_APPLY_TVA_COLKEY = "PREST_APPLY_TVA";
	public static final String PREST_COMMENTAIRE_CLIENT_COLKEY = "PREST_COMMENTAIRE_CLIENT";
	public static final String PREST_COMMENTAIRE_PREST_COLKEY = "PREST_COMMENTAIRE_PREST";
	public static final String PREST_DATE_COLKEY = "PREST_DATE";
	public static final String PREST_DATE_CLOTURE_COLKEY = "PREST_DATE_CLOTURE";
	public static final String PREST_DATE_FACTURATION_COLKEY = "PREST_DATE_FACTURATION";
	public static final String PREST_DATE_VALIDE_CLIENT_COLKEY = "PREST_DATE_VALIDE_CLIENT";
	public static final String PREST_DATE_VALIDE_PREST_COLKEY = "PREST_DATE_VALIDE_PREST";
	public static final String PREST_LIBELLE_COLKEY = "PREST_LIBELLE";
	public static final String PREST_NUMERO_COLKEY = "PREST_NUMERO";
	public static final String PREST_REMISE_GLOBALE_COLKEY = "PREST_REMISE_GLOBALE";
	public static final String PREST_TOTAL_HT_COLKEY = "PREST_TOTAL_HT";
	public static final String PREST_TOTAL_TTC_COLKEY = "PREST_TOTAL_TTC";
	public static final String PREST_TOTAL_TVA_COLKEY = "PREST_TOTAL_TVA";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String FOU_ORDRE_PREST_COLKEY = "FOU_ORDRE_PREST";
	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYPU_ID_COLKEY = "TYPU_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CATALOGUE_KEY = "catalogue";
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FACTURE_PAPIERS_KEY = "facturePapiers";
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String FOURNIS_ULR_PREST_KEY = "fournisUlrPrest";
	public static final String INDIVIDU_ULR_KEY = "individuUlr";
	public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String ORGAN_KEY = "organ";
	public static final String PERSONNE_KEY = "personne";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String PRESTATION_BASCULES_KEY = "prestationBascules";
	public static final String PRESTATION_BUDGET_CLIENT_KEY = "prestationBudgetClient";
	public static final String PRESTATION_LIGNES_KEY = "prestationLignes";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_PUBLIC_KEY = "typePublic";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String personne_persNomPrenom() {
    return (String) storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public void setPersonne_persNomPrenom(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public String prestApplyTva() {
    return (String) storedValueForKey(PREST_APPLY_TVA_KEY);
  }

  public void setPrestApplyTva(String value) {
    takeStoredValueForKey(value, PREST_APPLY_TVA_KEY);
  }

  public String prestCommentaireClient() {
    return (String) storedValueForKey(PREST_COMMENTAIRE_CLIENT_KEY);
  }

  public void setPrestCommentaireClient(String value) {
    takeStoredValueForKey(value, PREST_COMMENTAIRE_CLIENT_KEY);
  }

  public String prestCommentairePrest() {
    return (String) storedValueForKey(PREST_COMMENTAIRE_PREST_KEY);
  }

  public void setPrestCommentairePrest(String value) {
    takeStoredValueForKey(value, PREST_COMMENTAIRE_PREST_KEY);
  }

  public NSTimestamp prestDate() {
    return (NSTimestamp) storedValueForKey(PREST_DATE_KEY);
  }

  public void setPrestDate(NSTimestamp value) {
    takeStoredValueForKey(value, PREST_DATE_KEY);
  }

  public NSTimestamp prestDateCloture() {
    return (NSTimestamp) storedValueForKey(PREST_DATE_CLOTURE_KEY);
  }

  public void setPrestDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, PREST_DATE_CLOTURE_KEY);
  }

  public NSTimestamp prestDateFacturation() {
    return (NSTimestamp) storedValueForKey(PREST_DATE_FACTURATION_KEY);
  }

  public void setPrestDateFacturation(NSTimestamp value) {
    takeStoredValueForKey(value, PREST_DATE_FACTURATION_KEY);
  }

  public NSTimestamp prestDateValideClient() {
    return (NSTimestamp) storedValueForKey(PREST_DATE_VALIDE_CLIENT_KEY);
  }

  public void setPrestDateValideClient(NSTimestamp value) {
    takeStoredValueForKey(value, PREST_DATE_VALIDE_CLIENT_KEY);
  }

  public NSTimestamp prestDateValidePrest() {
    return (NSTimestamp) storedValueForKey(PREST_DATE_VALIDE_PREST_KEY);
  }

  public void setPrestDateValidePrest(NSTimestamp value) {
    takeStoredValueForKey(value, PREST_DATE_VALIDE_PREST_KEY);
  }

  public String prestLibelle() {
    return (String) storedValueForKey(PREST_LIBELLE_KEY);
  }

  public void setPrestLibelle(String value) {
    takeStoredValueForKey(value, PREST_LIBELLE_KEY);
  }

  public Integer prestNumero() {
    return (Integer) storedValueForKey(PREST_NUMERO_KEY);
  }

  public void setPrestNumero(Integer value) {
    takeStoredValueForKey(value, PREST_NUMERO_KEY);
  }

  public java.math.BigDecimal prestRemiseGlobale() {
    return (java.math.BigDecimal) storedValueForKey(PREST_REMISE_GLOBALE_KEY);
  }

  public void setPrestRemiseGlobale(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PREST_REMISE_GLOBALE_KEY);
  }

  public java.math.BigDecimal prestTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(PREST_TOTAL_HT_KEY);
  }

  public void setPrestTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PREST_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal prestTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(PREST_TOTAL_TTC_KEY);
  }

  public void setPrestTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PREST_TOTAL_TTC_KEY);
  }

  public java.math.BigDecimal prestTotalTva() {
    return (java.math.BigDecimal) storedValueForKey(PREST_TOTAL_TVA_KEY);
  }

  public void setPrestTotalTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PREST_TOTAL_TVA_KEY);
  }

  public org.cocktail.kava.server.metier.EOCatalogue catalogue() {
    return (org.cocktail.kava.server.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
  }
  
	public void setCatalogue(org.cocktail.kava.server.metier.EOCatalogue value) {
		takeStoredValueForKey(value,CATALOGUE_KEY);
	}


  public void setCatalogueRelationship(org.cocktail.kava.server.metier.EOCatalogue value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCatalogue oldValue = catalogue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kava.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }
  
	public void setCodeAnalytique(org.cocktail.kava.server.metier.EOCodeAnalytique value) {
		takeStoredValueForKey(value,CODE_ANALYTIQUE_KEY);
	}


  public void setCodeAnalytiqueRelationship(org.cocktail.kava.server.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOConvention convention() {
    return (org.cocktail.kava.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }
  
	public void setConvention(org.cocktail.kava.server.metier.EOConvention value) {
		takeStoredValueForKey(value,CONVENTION_KEY);
	}


  public void setConventionRelationship(org.cocktail.kava.server.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.application.serveur.eof.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFournisUlr fournisUlr() {
    return (org.cocktail.kava.server.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
  }
  
	public void setFournisUlr(org.cocktail.kava.server.metier.EOFournisUlr value) {
		takeStoredValueForKey(value,FOURNIS_ULR_KEY);
	}


  public void setFournisUlrRelationship(org.cocktail.kava.server.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFournisUlr oldValue = fournisUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFournisUlr fournisUlrPrest() {
    return (org.cocktail.kava.server.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_PREST_KEY);
  }
  
	public void setFournisUlrPrest(org.cocktail.kava.server.metier.EOFournisUlr value) {
		takeStoredValueForKey(value,FOURNIS_ULR_PREST_KEY);
	}


  public void setFournisUlrPrestRelationship(org.cocktail.kava.server.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFournisUlr oldValue = fournisUlrPrest();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_PREST_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_PREST_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOIndividuUlr individuUlr() {
    return (org.cocktail.kava.server.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
  }
  
	public void setIndividuUlr(org.cocktail.kava.server.metier.EOIndividuUlr value) {
		takeStoredValueForKey(value,INDIVIDU_ULR_KEY);
	}


  public void setIndividuUlrRelationship(org.cocktail.kava.server.metier.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOIndividuUlr oldValue = individuUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
    return (org.cocktail.kava.server.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
  }
  
	public void setLolfNomenclatureRecette(org.cocktail.kava.server.metier.EOLolfNomenclatureRecette value) {
		takeStoredValueForKey(value,LOLF_NOMENCLATURE_RECETTE_KEY);
	}


  public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.server.metier.EOLolfNomenclatureRecette value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOLolfNomenclatureRecette oldValue = lolfNomenclatureRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_NOMENCLATURE_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.kava.server.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }
  
	public void setModeRecouvrement(org.cocktail.kava.server.metier.EOModeRecouvrement value) {
		takeStoredValueForKey(value,MODE_RECOUVREMENT_KEY);
	}


  public void setModeRecouvrementRelationship(org.cocktail.kava.server.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOOrgan organ() {
    return (org.cocktail.kava.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.kava.server.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.kava.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPersonne personne() {
    return (org.cocktail.kava.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }
  
	public void setPersonne(org.cocktail.kava.server.metier.EOPersonne value) {
		takeStoredValueForKey(value,PERSONNE_KEY);
	}


  public void setPersonneRelationship(org.cocktail.kava.server.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kava.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }
  
	public void setPlanComptable(org.cocktail.kava.server.metier.EOPlanComptable value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_KEY);
	}


  public void setPlanComptableRelationship(org.cocktail.kava.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPrestationBudgetClient prestationBudgetClient() {
    return (org.cocktail.kava.server.metier.EOPrestationBudgetClient)storedValueForKey(PRESTATION_BUDGET_CLIENT_KEY);
  }
  
	public void setPrestationBudgetClient(org.cocktail.kava.server.metier.EOPrestationBudgetClient value) {
		takeStoredValueForKey(value,PRESTATION_BUDGET_CLIENT_KEY);
	}


  public void setPrestationBudgetClientRelationship(org.cocktail.kava.server.metier.EOPrestationBudgetClient value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPrestationBudgetClient oldValue = prestationBudgetClient();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_BUDGET_CLIENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_BUDGET_CLIENT_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.kava.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }
  
	public void setTauxProrata(org.cocktail.kava.server.metier.EOTauxProrata value) {
		takeStoredValueForKey(value,TAUX_PRORATA_KEY);
	}


  public void setTauxProrataRelationship(org.cocktail.kava.server.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCreditRec() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
  }
  
	public void setTypeCreditRec(org.cocktail.application.serveur.eof.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_REC_KEY);
	}


  public void setTypeCreditRecRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCreditRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.application.serveur.eof.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypePublic typePublic() {
    return (org.cocktail.kava.server.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
  }
  
	public void setTypePublic(org.cocktail.kava.server.metier.EOTypePublic value) {
		takeStoredValueForKey(value,TYPE_PUBLIC_KEY);
	}


  public void setTypePublicRelationship(org.cocktail.kava.server.metier.EOTypePublic value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypePublic oldValue = typePublic();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PUBLIC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.kava.server.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray facturePapiers() {
    return (NSArray)storedValueForKey(FACTURE_PAPIERS_KEY);
  }
  
  //ICI
  public void setFacturePapiers(NSArray values) {
	  takeStoredValueForKey(values,FACTURE_PAPIERS_KEY);
  }
  

  public NSArray facturePapiers(EOQualifier qualifier) {
    return facturePapiers(qualifier, null, false);
  }

  public NSArray facturePapiers(EOQualifier qualifier, boolean fetch) {
    return facturePapiers(qualifier, null, fetch);
  }

  public NSArray facturePapiers(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOFacturePapier.PRESTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOFacturePapier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = facturePapiers();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFacturePapiersRelationship(org.cocktail.kava.server.metier.EOFacturePapier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIERS_KEY);
  }

  public void removeFromFacturePapiersRelationship(org.cocktail.kava.server.metier.EOFacturePapier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIERS_KEY);
  }

  public org.cocktail.kava.server.metier.EOFacturePapier createFacturePapiersRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FacturePapier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_PAPIERS_KEY);
    return (org.cocktail.kava.server.metier.EOFacturePapier) eo;
  }

  public void deleteFacturePapiersRelationship(org.cocktail.kava.server.metier.EOFacturePapier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIERS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFacturePapiersRelationships() {
    Enumeration objects = facturePapiers().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFacturePapiersRelationship((org.cocktail.kava.server.metier.EOFacturePapier)objects.nextElement());
    }
  }

  public NSArray prestationBascules() {
    return (NSArray)storedValueForKey(PRESTATION_BASCULES_KEY);
  }
  
  //ICI
  public void setPrestationBascules(NSArray values) {
	  takeStoredValueForKey(values,PRESTATION_BASCULES_KEY);
  }
  

  public NSArray prestationBascules(EOQualifier qualifier) {
    return prestationBascules(qualifier, null, false);
  }

  public NSArray prestationBascules(EOQualifier qualifier, boolean fetch) {
    return prestationBascules(qualifier, null, fetch);
  }

  public NSArray prestationBascules(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOPrestationBascule.PRESTATION_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOPrestationBascule.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prestationBascules();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrestationBasculesRelationship(org.cocktail.kava.server.metier.EOPrestationBascule object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRESTATION_BASCULES_KEY);
  }

  public void removeFromPrestationBasculesRelationship(org.cocktail.kava.server.metier.EOPrestationBascule object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_BASCULES_KEY);
  }

  public org.cocktail.kava.server.metier.EOPrestationBascule createPrestationBasculesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PrestationBascule");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRESTATION_BASCULES_KEY);
    return (org.cocktail.kava.server.metier.EOPrestationBascule) eo;
  }

  public void deletePrestationBasculesRelationship(org.cocktail.kava.server.metier.EOPrestationBascule object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_BASCULES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrestationBasculesRelationships() {
    Enumeration objects = prestationBascules().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrestationBasculesRelationship((org.cocktail.kava.server.metier.EOPrestationBascule)objects.nextElement());
    }
  }

  public NSArray prestationLignes() {
    return (NSArray)storedValueForKey(PRESTATION_LIGNES_KEY);
  }
  
  //ICI
  public void setPrestationLignes(NSArray values) {
	  takeStoredValueForKey(values,PRESTATION_LIGNES_KEY);
  }
  

  public NSArray prestationLignes(EOQualifier qualifier) {
    return prestationLignes(qualifier, null, false);
  }

  public NSArray prestationLignes(EOQualifier qualifier, boolean fetch) {
    return prestationLignes(qualifier, null, fetch);
  }

  public NSArray prestationLignes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOPrestationLigne.PRESTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOPrestationLigne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prestationLignes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrestationLignesRelationship(org.cocktail.kava.server.metier.EOPrestationLigne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
  }

  public void removeFromPrestationLignesRelationship(org.cocktail.kava.server.metier.EOPrestationLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
  }

  public org.cocktail.kava.server.metier.EOPrestationLigne createPrestationLignesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PrestationLigne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRESTATION_LIGNES_KEY);
    return (org.cocktail.kava.server.metier.EOPrestationLigne) eo;
  }

  public void deletePrestationLignesRelationship(org.cocktail.kava.server.metier.EOPrestationLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRESTATION_LIGNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrestationLignesRelationships() {
    Enumeration objects = prestationLignes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrestationLignesRelationship((org.cocktail.kava.server.metier.EOPrestationLigne)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPrestation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrestation createEOPrestation(EOEditingContext editingContext, String prestApplyTva
, NSTimestamp prestDate
, String prestLibelle
, Integer prestNumero
, java.math.BigDecimal prestTotalHt
, java.math.BigDecimal prestTotalTtc
, java.math.BigDecimal prestTotalTva
			) {
    EOPrestation eo = (EOPrestation) createAndInsertInstance(editingContext, _EOPrestation.ENTITY_NAME);    
		eo.setPrestApplyTva(prestApplyTva);
		eo.setPrestDate(prestDate);
		eo.setPrestLibelle(prestLibelle);
		eo.setPrestNumero(prestNumero);
		eo.setPrestTotalHt(prestTotalHt);
		eo.setPrestTotalTtc(prestTotalTtc);
		eo.setPrestTotalTva(prestTotalTva);
    return eo;
  }

  
	  public EOPrestation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrestation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrestation creerInstance(EOEditingContext editingContext) {
	  		EOPrestation object = (EOPrestation)createAndInsertInstance(editingContext, _EOPrestation.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPrestation localInstanceIn(EOEditingContext editingContext, EOPrestation eo) {
    EOPrestation localInstance = (eo == null) ? null : (EOPrestation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrestation#localInstanceIn a la place.
   */
	public static EOPrestation localInstanceOf(EOEditingContext editingContext, EOPrestation eo) {
		return EOPrestation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrestation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrestation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrestation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrestation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrestation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrestation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrestation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrestation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
