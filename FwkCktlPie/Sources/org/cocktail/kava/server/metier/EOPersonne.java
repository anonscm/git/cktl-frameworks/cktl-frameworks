// EOPersonne.java
// 

package org.cocktail.kava.server.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPersonne extends _EOPersonne {
	public EOPersonne() {
		super();
	}
	
	/* retourne le libellé le plus adéquat pour la personne */
	public String libelle() {
		if(isPersonneMorale()) {
			return persNomPrenom();
		}
		else {
			return individuUlr().civiliteNomPrenom();
		}
	}
	
	public String emailPersonne() {
		
		String email = null;
		EOQualifier qualPers =  new EOKeyValueQualifier(EOVEmailIndividu.PERSONNE_KEY,EOQualifier.QualifierOperatorEqual,this);
		NSArray data = EOVEmailIndividu.fetchAll(this.editingContext(), qualPers);
		if(data.count()>0) {
			EOVEmailIndividu first = (EOVEmailIndividu)data.objectAtIndex(0);
			email = first.cemEmail()+"@"+first.cemDomaine();
		}
		return email;
	}
	
	public String telProPersonne() {
		NSArray<EOPersonneTelephone> tels = this.personneTelephones();
		String type = null;
		String numeros = "";
		for(EOPersonneTelephone telephone : tels) {
			try {
				type = telephone.typeTelRelationship().cTypeTel();
				if(type!=null && !type.equals("PRV") && !type.equals("PAR")) {
					numeros = numeros + telephone.noTelephone()+" | ";
				}
			}
			catch(Exception e) { 
				e.printStackTrace();
			}
		}
		return numeros;
	}
	
	public boolean isPersonneMorale() {
		return "STR".equalsIgnoreCase(persType());
	}
	
	
	public EOAdresse adresseFacturation() {
		return getAdresseDuType(EOTypeAdresse.TYPE_ADR_FACTURATION);
	}
	
	public EOAdresse adresseLivraison() {
		return getAdresseDuType(EOTypeAdresse.TYPE_ADR_LIVRAISON);
	}
	
	public EOAdresse getAdresseDuType(EOTypeAdresse typeAdr) {
		if(typeAdr!=null) {
			return getAdresseDuType(typeAdr.tadrCode());
		}
		else {
			return null;
		}
	}
	
	
	public EOAdresse getAdresseDuType(String tadrCode) {
		EOQualifier qualType = new EOKeyValueQualifier(
									EORepartPersonneAdresse.TADR_CODE_KEY, 
									EOQualifier.QualifierOperatorEqual, 
									tadrCode
									);
		NSArray rpt = EOQualifier.filteredArrayWithQualifier(repartPersonneAdresses(), qualType);
		if(rpt.count()>0) {
			return ((EORepartPersonneAdresse)rpt.objectAtIndex(0)).adresse();
		}
		else {
			return null;
		}
	}
	
	public EORptPersonneAdresse getRptPersonneAdresse(EOTypeAdresse typeAdr) {
		EOQualifier qualType = new EOKeyValueQualifier(
														EORptPersonneAdresse.TYPE_ADRESSE_KEY, 
														EOQualifier.QualifierOperatorEqual, 
														typeAdr
													);
		
		EOQualifier qualFinal = new EOAndQualifier(new NSArray(new Object[]{qualType,EORptPersonneAdresse.QUALIFIER_RPA_VALIDE}));
		
		NSArray rptPersAdresses = EOQualifier.filteredArrayWithQualifier(this.rptPersonneAdresses(), qualFinal);
		if(rptPersAdresses.count()>0) {
			return (EORptPersonneAdresse)rptPersAdresses.objectAtIndex(0);
		}
		else {
			return null;
		}
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
