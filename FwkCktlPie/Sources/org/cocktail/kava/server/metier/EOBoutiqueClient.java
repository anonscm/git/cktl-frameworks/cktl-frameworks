

// EOBoutiqueClient.java
// 

package org.cocktail.kava.server.metier;


import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOBoutiqueClient extends _EOBoutiqueClient
{
    public EOBoutiqueClient() {
        super();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    public static boolean isClientBoutique(EOPersonne personne, EOBoutique boutique) {
    	if(personne==null || boutique==null) {
    		return false;
    	}
    	EOQualifier qualifier = new EOKeyValueQualifier(PERSONNE_KEY,EOQualifier.QualifierOperatorEqual,personne);
    	NSArray<EOBoutiqueClient> bcArray = EOQualifier.filteredArrayWithQualifier(boutique.boutiqueClients(), qualifier);
    	return bcArray.count() > 0;
    }
    
    
    
    
    
    public Number getPersId() {
    	NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
    	if(dico!=null) {
    		return (Number)dico.objectForKey( EOBoutiqueClient.PERS_ID_KEY );
    	}
    	else {
    		return null;
    	}
    
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
