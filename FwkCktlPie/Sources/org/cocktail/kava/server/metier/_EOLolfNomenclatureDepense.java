/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLolfNomenclatureDepense.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOLolfNomenclatureDepense extends  FwkPieRecord {
	public static final String ENTITY_NAME = "LolfNomenclatureDepense";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.Lolf_Nomenclature_Depense";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "lolfId";

	public static final String LOLF_ABREVIATION_KEY = "lolfAbreviation";
	public static final String LOLF_CODE_KEY = "lolfCode";
	public static final String LOLF_FERMETURE_KEY = "lolfFermeture";
	public static final String LOLF_LIBELLE_KEY = "lolfLibelle";
	public static final String LOLF_NIVEAU_KEY = "lolfNiveau";
	public static final String LOLF_ORDRE_AFFICHAGE_KEY = "lolfOrdreAffichage";
	public static final String LOLF_OUVERTURE_KEY = "lolfOuverture";
	public static final String LOLF_PERE_KEY = "lolfPere";

// Attributs non visibles
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String LOLF_TYPE_KEY = "lolfType";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String LOLF_ABREVIATION_COLKEY = "LOLF_ABREVIATION";
	public static final String LOLF_CODE_COLKEY = "LOLF_CODE";
	public static final String LOLF_FERMETURE_COLKEY = "LOLF_FERMETURE";
	public static final String LOLF_LIBELLE_COLKEY = "LOLF_LIBELLE";
	public static final String LOLF_NIVEAU_COLKEY = "LOLF_NIVEAU";
	public static final String LOLF_ORDRE_AFFICHAGE_COLKEY = "LOLF_ORDRE_AFFICHAGE";
	public static final String LOLF_OUVERTURE_COLKEY = "LOLF_OUVERTURE";
	public static final String LOLF_PERE_COLKEY = "LOLF_PERE";

	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String LOLF_TYPE_COLKEY = "LOLF_TYPE";
	public static final String TYET_ID_COLKEY = "tyet_id";


	// Relationships
	public static final String LOLF_NOMENCLATURE_TYPE_KEY = "lolfNomenclatureType";
	public static final String ORGAN_ACTION_DEPS_KEY = "organActionDeps";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public String lolfAbreviation() {
    return (String) storedValueForKey(LOLF_ABREVIATION_KEY);
  }

  public void setLolfAbreviation(String value) {
    takeStoredValueForKey(value, LOLF_ABREVIATION_KEY);
  }

  public String lolfCode() {
    return (String) storedValueForKey(LOLF_CODE_KEY);
  }

  public void setLolfCode(String value) {
    takeStoredValueForKey(value, LOLF_CODE_KEY);
  }

  public NSTimestamp lolfFermeture() {
    return (NSTimestamp) storedValueForKey(LOLF_FERMETURE_KEY);
  }

  public void setLolfFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, LOLF_FERMETURE_KEY);
  }

  public String lolfLibelle() {
    return (String) storedValueForKey(LOLF_LIBELLE_KEY);
  }

  public void setLolfLibelle(String value) {
    takeStoredValueForKey(value, LOLF_LIBELLE_KEY);
  }

  public Integer lolfNiveau() {
    return (Integer) storedValueForKey(LOLF_NIVEAU_KEY);
  }

  public void setLolfNiveau(Integer value) {
    takeStoredValueForKey(value, LOLF_NIVEAU_KEY);
  }

  public Integer lolfOrdreAffichage() {
    return (Integer) storedValueForKey(LOLF_ORDRE_AFFICHAGE_KEY);
  }

  public void setLolfOrdreAffichage(Integer value) {
    takeStoredValueForKey(value, LOLF_ORDRE_AFFICHAGE_KEY);
  }

  public NSTimestamp lolfOuverture() {
    return (NSTimestamp) storedValueForKey(LOLF_OUVERTURE_KEY);
  }

  public void setLolfOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, LOLF_OUVERTURE_KEY);
  }

  public Integer lolfPere() {
    return (Integer) storedValueForKey(LOLF_PERE_KEY);
  }

  public void setLolfPere(Integer value) {
    takeStoredValueForKey(value, LOLF_PERE_KEY);
  }

  public org.cocktail.kava.server.metier.EOLolfNomenclatureType lolfNomenclatureType() {
    return (org.cocktail.kava.server.metier.EOLolfNomenclatureType)storedValueForKey(LOLF_NOMENCLATURE_TYPE_KEY);
  }
  
	public void setLolfNomenclatureType(org.cocktail.kava.server.metier.EOLolfNomenclatureType value) {
		takeStoredValueForKey(value,LOLF_NOMENCLATURE_TYPE_KEY);
	}


  public void setLolfNomenclatureTypeRelationship(org.cocktail.kava.server.metier.EOLolfNomenclatureType value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOLolfNomenclatureType oldValue = lolfNomenclatureType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_NOMENCLATURE_TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_TYPE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.application.serveur.eof.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray organActionDeps() {
    return (NSArray)storedValueForKey(ORGAN_ACTION_DEPS_KEY);
  }
  
  //ICI
  public void setOrganActionDeps(NSArray values) {
	  takeStoredValueForKey(values,ORGAN_ACTION_DEPS_KEY);
  }
  

  public NSArray organActionDeps(EOQualifier qualifier) {
    return organActionDeps(qualifier, null, false);
  }

  public NSArray organActionDeps(EOQualifier qualifier, boolean fetch) {
    return organActionDeps(qualifier, null, fetch);
  }

  public NSArray organActionDeps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOOrganActionDep.LOLF_NOMENCLATURE_DEPENSE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOOrganActionDep.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organActionDeps();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganActionDepsRelationship(org.cocktail.kava.server.metier.EOOrganActionDep object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_ACTION_DEPS_KEY);
  }

  public void removeFromOrganActionDepsRelationship(org.cocktail.kava.server.metier.EOOrganActionDep object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_ACTION_DEPS_KEY);
  }

  public org.cocktail.kava.server.metier.EOOrganActionDep createOrganActionDepsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("OrganActionDep");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_ACTION_DEPS_KEY);
    return (org.cocktail.kava.server.metier.EOOrganActionDep) eo;
  }

  public void deleteOrganActionDepsRelationship(org.cocktail.kava.server.metier.EOOrganActionDep object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_ACTION_DEPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganActionDepsRelationships() {
    Enumeration objects = organActionDeps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganActionDepsRelationship((org.cocktail.kava.server.metier.EOOrganActionDep)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOLolfNomenclatureDepense avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOLolfNomenclatureDepense createEOLolfNomenclatureDepense(EOEditingContext editingContext, String lolfAbreviation
, String lolfCode
, String lolfLibelle
, Integer lolfNiveau
, NSTimestamp lolfOuverture
			) {
    EOLolfNomenclatureDepense eo = (EOLolfNomenclatureDepense) createAndInsertInstance(editingContext, _EOLolfNomenclatureDepense.ENTITY_NAME);    
		eo.setLolfAbreviation(lolfAbreviation);
		eo.setLolfCode(lolfCode);
		eo.setLolfLibelle(lolfLibelle);
		eo.setLolfNiveau(lolfNiveau);
		eo.setLolfOuverture(lolfOuverture);
    return eo;
  }

  
	  public EOLolfNomenclatureDepense localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLolfNomenclatureDepense)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLolfNomenclatureDepense creerInstance(EOEditingContext editingContext) {
	  		EOLolfNomenclatureDepense object = (EOLolfNomenclatureDepense)createAndInsertInstance(editingContext, _EOLolfNomenclatureDepense.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOLolfNomenclatureDepense localInstanceIn(EOEditingContext editingContext, EOLolfNomenclatureDepense eo) {
    EOLolfNomenclatureDepense localInstance = (eo == null) ? null : (EOLolfNomenclatureDepense)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOLolfNomenclatureDepense#localInstanceIn a la place.
   */
	public static EOLolfNomenclatureDepense localInstanceOf(EOEditingContext editingContext, EOLolfNomenclatureDepense eo) {
		return EOLolfNomenclatureDepense.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLolfNomenclatureDepense fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLolfNomenclatureDepense fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLolfNomenclatureDepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLolfNomenclatureDepense)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLolfNomenclatureDepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLolfNomenclatureDepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLolfNomenclatureDepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLolfNomenclatureDepense)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLolfNomenclatureDepense fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLolfNomenclatureDepense eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLolfNomenclatureDepense ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLolfNomenclatureDepense fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
