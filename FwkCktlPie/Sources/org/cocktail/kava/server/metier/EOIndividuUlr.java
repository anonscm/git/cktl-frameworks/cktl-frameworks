

// EOIndividuUlr.java
// 

package org.cocktail.kava.server.metier;


import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSValidation;

public class EOIndividuUlr extends _EOIndividuUlr
{
	
	
	public static final EOSortOrdering NOM_USUEL_ORDERING = EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	
	public static final EOQualifier QUALIFIER_VALIDE = EOQualifier.qualifierWithQualifierFormat( EOIndividuUlr.TEM_VALIDE_KEY + " = 'O'",null );
	
    public EOIndividuUlr() {
        super();
    }

    
    public String civiliteNomPrenom() {
    	return cCivilite()+" "+prenomNom();
    }
    
    public String prenomNom() {
    	return prenom()+" "+nomUsuel();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
