package org.cocktail.kava.server.metier;


public class EOBoutiqueType extends _EOBoutiqueType {
	
	
	public static final String TYPE_SEMINAIRE_COLLOQUE 	= "SC";
	public static final String TYPE_VENTE 			   	= "VE";
	public static final String TYPE_DIFFUSEUR 			= "DF";
	public static final String TYPE_FORMATION_CONTINUE = "FC";
	
}
