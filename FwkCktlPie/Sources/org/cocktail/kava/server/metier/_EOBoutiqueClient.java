/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBoutiqueClient.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBoutiqueClient extends  FwkPieRecord {
	public static final String ENTITY_NAME = "BoutiqueClient";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.boutique_client";



	// Attributes


	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";

// Attributs non visibles
	public static final String BOUTIQUE_ID_KEY = "boutiqueId";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "DATE_DEBUT";
	public static final String DATE_FIN_COLKEY = "DATE_FIN";

	public static final String BOUTIQUE_ID_COLKEY = "BOUTIQUE_ID";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String BOUTIQUE_KEY = "boutique";
	public static final String FOURNIS_ULRS_KEY = "fournisUlrs";
	public static final String PERSONNE_KEY = "personne";
	public static final String STRUCTURE_ULRS_KEY = "structureUlrs";



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutique boutique() {
    return (org.cocktail.kava.server.metier.EOBoutique)storedValueForKey(BOUTIQUE_KEY);
  }
  
	public void setBoutique(org.cocktail.kava.server.metier.EOBoutique value) {
		takeStoredValueForKey(value,BOUTIQUE_KEY);
	}


  public void setBoutiqueRelationship(org.cocktail.kava.server.metier.EOBoutique value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOBoutique oldValue = boutique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BOUTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BOUTIQUE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPersonne personne() {
    return (org.cocktail.kava.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }
  
	public void setPersonne(org.cocktail.kava.server.metier.EOPersonne value) {
		takeStoredValueForKey(value,PERSONNE_KEY);
	}


  public void setPersonneRelationship(org.cocktail.kava.server.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public NSArray fournisUlrs() {
    return (NSArray)storedValueForKey(FOURNIS_ULRS_KEY);
  }
  
  //ICI
  public void setFournisUlrs(NSArray values) {
	  takeStoredValueForKey(values,FOURNIS_ULRS_KEY);
  }
  

  public NSArray fournisUlrs(EOQualifier qualifier) {
    return fournisUlrs(qualifier, null);
  }

  public NSArray fournisUlrs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = fournisUlrs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToFournisUlrsRelationship(org.cocktail.kava.server.metier.EOFournisUlr object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FOURNIS_ULRS_KEY);
  }

  public void removeFromFournisUlrsRelationship(org.cocktail.kava.server.metier.EOFournisUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULRS_KEY);
  }

  public org.cocktail.kava.server.metier.EOFournisUlr createFournisUlrsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FournisUlr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FOURNIS_ULRS_KEY);
    return (org.cocktail.kava.server.metier.EOFournisUlr) eo;
  }

  public void deleteFournisUlrsRelationship(org.cocktail.kava.server.metier.EOFournisUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULRS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFournisUlrsRelationships() {
    Enumeration objects = fournisUlrs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFournisUlrsRelationship((org.cocktail.kava.server.metier.EOFournisUlr)objects.nextElement());
    }
  }

  public NSArray structureUlrs() {
    return (NSArray)storedValueForKey(STRUCTURE_ULRS_KEY);
  }
  
  //ICI
  public void setStructureUlrs(NSArray values) {
	  takeStoredValueForKey(values,STRUCTURE_ULRS_KEY);
  }
  

  public NSArray structureUlrs(EOQualifier qualifier) {
    return structureUlrs(qualifier, null);
  }

  public NSArray structureUlrs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = structureUlrs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToStructureUlrsRelationship(org.cocktail.kava.server.metier.EOStructureUlr object) {
    addObjectToBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
  }

  public void removeFromStructureUlrsRelationship(org.cocktail.kava.server.metier.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
  }

  public org.cocktail.kava.server.metier.EOStructureUlr createStructureUlrsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("StructureUlr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, STRUCTURE_ULRS_KEY);
    return (org.cocktail.kava.server.metier.EOStructureUlr) eo;
  }

  public void deleteStructureUlrsRelationship(org.cocktail.kava.server.metier.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllStructureUlrsRelationships() {
    Enumeration objects = structureUlrs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteStructureUlrsRelationship((org.cocktail.kava.server.metier.EOStructureUlr)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOBoutiqueClient avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBoutiqueClient createEOBoutiqueClient(EOEditingContext editingContext			) {
    EOBoutiqueClient eo = (EOBoutiqueClient) createAndInsertInstance(editingContext, _EOBoutiqueClient.ENTITY_NAME);    
    return eo;
  }

  
	  public EOBoutiqueClient localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBoutiqueClient)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBoutiqueClient creerInstance(EOEditingContext editingContext) {
	  		EOBoutiqueClient object = (EOBoutiqueClient)createAndInsertInstance(editingContext, _EOBoutiqueClient.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBoutiqueClient localInstanceIn(EOEditingContext editingContext, EOBoutiqueClient eo) {
    EOBoutiqueClient localInstance = (eo == null) ? null : (EOBoutiqueClient)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBoutiqueClient#localInstanceIn a la place.
   */
	public static EOBoutiqueClient localInstanceOf(EOEditingContext editingContext, EOBoutiqueClient eo) {
		return EOBoutiqueClient.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBoutiqueClient fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBoutiqueClient fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBoutiqueClient eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBoutiqueClient)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBoutiqueClient fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBoutiqueClient fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBoutiqueClient eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBoutiqueClient)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBoutiqueClient fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBoutiqueClient eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBoutiqueClient ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBoutiqueClient fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
