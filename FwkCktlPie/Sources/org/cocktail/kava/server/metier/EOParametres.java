

// EOParametres.java
// 

package org.cocktail.kava.server.metier;


import com.webobjects.foundation.NSValidation;

public class EOParametres extends _EOParametres
{
	public static final String PARAM_ADRESSE_SERVICE_FACTURE="ADRESSE_SERVICE_FACTURE";

	public static final String PARAM_AUTORISE_CREATION_CLIENT="AUTORISE_CREATION_CLIENT";
	public static final String PARAM_AUTORISE_CREATION_RIB="AUTORISE_CREATION_RIB";
	public static final String PARAM_AUTORISE_PI_FROM_COMMANDE="AUTORISE_PI_FROM_COMMANDE";
	public static final String PARAM_CRYPT_PASSWORD_COMPTE_CLIENT="CRYPT_PASSWORD_COMPTE_CLIENT";
	public static final String PARAM_CTRL_ORGAN_DEST="CTRL_ORGAN_DEST";
	public static final String PARAM_MODE_PAIEMENT_PI="MODE_PAIEMENT_PI";
	public static final String PARAM_MODE_RECOUVREMENT_PI="MODE_RECOUVREMENT_PI";
	public static final String PARAM_RECETTE_IDEM_TAP_ID="RECETTE_IDEM_TAP_ID";
	public static final String PARAM_SIXID_PIE_DEVIS="SIXID_PIE_DEVIS";
	public static final String PARAM_SIXID_PIE_FACTURE="SIXID_PIE_FACTURE";
	public static final String PARAM_SIXID_PIE_FACTURE_ANGLAIS="SIXID_PIE_FACTURE_ANGLAIS";
	public static final String PARAM_WS_PRESTATION="WS_PRESTATION";
	public static final String PARAM_WS_PRESTATION_PASSWORD="WS_PRESTATION_PASSWORD";
	public static final String PARAM_CONFIG_URL_APP_WEB="APP_URL_WEB";

    public EOParametres() {
        super();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
