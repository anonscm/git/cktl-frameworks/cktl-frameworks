

// EOCatalogueResponsable.java
// 

package org.cocktail.kava.server.metier;


import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueResponsable;
import org.cocktail.kava.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOCatalogueResponsable extends _EOCatalogueResponsable
{
    public EOCatalogueResponsable() {
        super();
    }
    
    
	public static EOCatalogueResponsable newObject(EOEditingContext ec) {
		EOCatalogueResponsable object = (EOCatalogueResponsable) Factory.instanceForEntity(ec, EOCatalogueResponsable.ENTITY_NAME);
		ec.insertObject(object);
		return object;
	}

	public static EOCatalogueResponsable newObject(EOEditingContext ec, EOCatalogue catalogue, EOUtilisateur utilisateur) {
		EOCatalogueResponsable object = newObject(ec);
		object.setCatalogueRelationship(catalogue);
		object.setUtilisateurRelationship(utilisateur);
		return object;
	}

	
	
	public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
