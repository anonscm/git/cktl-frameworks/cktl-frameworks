// EOTypePublic.java
// 

package org.cocktail.kava.server.metier;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOTypePublic extends _EOTypePublic {
	public static final String	TYPU_ID_KEY	= "typuId";
	
    public static final int TYPE_INTERNE_INT = 1;
    public static final int TYPE_EXTERNE_INTRANET_INT = 2;
    public static final int TYPE_EXTERNE_PUBLIC_INT = 3;
    public static final int TYPE_EXTERNE_PRIVE_INT = 4;
    public static final int TYPE_FORMATION_EXTERNE_INT = 5;
    public static final java.lang.Integer TYPE_INTERNE = new Integer(1);
    public static final java.lang.Integer TYPE_EXTERNE_INTRANET = new Integer(2);
    public static final java.lang.Integer TYPE_EXTERNE_PUBLIC = new Integer(3);
    public static final java.lang.Integer TYPE_EXTERNE_PRIVE = new Integer(4);
    public static final java.lang.Integer TYPE_FORMATION_EXTERNE = new Integer(5);

	public EOTypePublic() {
		super();
	}

	public Integer typuId() {
		try {
			NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
			return (Integer) dico.objectForKey(TYPU_ID_KEY);
		}
		catch (Exception e) {
			return new Integer(-1);
		}
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
