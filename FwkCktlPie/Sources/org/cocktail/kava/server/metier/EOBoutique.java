/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.metier;

import java.util.Enumeration;
import java.util.Iterator;

import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.server.factory.Factory;
import org.cocktail.kava.server.finder.FinderTypeEtat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOBoutique extends _EOBoutique {

	private static final String OUI = "O";
	private static final String NON = "N";
	
    public EOBoutique() {
        super();
    }

    /** retourne l'intitulé de la boutique dans la locale choisie */
    public String getLibelleForLocale(String locale) {
    	if(locale.equals("fr")) {
    		return boutiqueLibelleFr();
    	}
    	else {
    		String lib = boutiqueLibelleEn();
    		if(lib==null) {
    			lib = boutiqueLibelleFr();
    		}
    		return lib;
    	}
    }
    
	private final Integer unite = new Integer(1);
	

	public boolean isDiffuseur() {
		boolean isDiffuseur = boutiqueType().btCode().equals(EOBoutiqueType.TYPE_DIFFUSEUR);
		return isDiffuseur;
	}
	
	public boolean isVente() {
		return boutiqueType().btCode().equals(EOBoutiqueType.TYPE_VENTE);
	}
	
    public boolean isColloqueSeminaire() {
    	return boutiqueType().btCode().equals( EOBoutiqueType.TYPE_SEMINAIRE_COLLOQUE );
    }
    
    public boolean isFormationContinue()
    {
    	return boutiqueType().btCode().equals(EOBoutiqueType.TYPE_FORMATION_CONTINUE);
    }
    
	public static EOBoutique createNewInstance(EOEditingContext ec) {
		EOBoutique object = (EOBoutique)Factory.instanceForEntity(ec, EOBoutique.ENTITY_NAME);
		EOTypeEtat etatValide  = FinderTypeEtat.typeEtatValide(ec);
		object.setTypeEtat(etatValide);
		object.setBoutiqueValClient(OUI);
		object.setBoutiqueValPrestataire(OUI);
		object.setBoutiqueValCloture(OUI);
		return object;
	}	
	


	public boolean isPaiementWebPossible() {
		return FwkPieRecord.OUI.equals( boutiquePaiementWeb() );
	}

	public void setPaiementWebPossible(boolean vf) {
		setBoutiquePaiementWeb( vf ? FwkPieRecord.OUI : FwkPieRecord.NON );
	}
	
	
	// TODO: adapter au cas boutiques privées sans que ce soit.
	/** Concerne les délégués commandes pour les CRI Diffuseurs; individu est il reference comme delegue quelque part */
	public boolean peutPasserCommande(EOIndividuUlr individu) {
		
		if(!isDiffuseur()) {
			return true;
		} 
		
		NSMutableArray<EOQualifier> arrayQual = new NSMutableArray<EOQualifier>();
		arrayQual.addObject( new EOKeyValueQualifier(EOVDelegueCommandeBoutique.INDIVIDU_ULR_KEY,EOQualifier.QualifierOperatorEqual,individu) );
		arrayQual.addObject( new EOKeyValueQualifier(EOVDelegueCommandeBoutique.BOUTIQUE_KEY,EOQualifier.QualifierOperatorEqual,this) );
		
		NSArray<EOVDelegueCommandeBoutique> data = EOVDelegueCommandeBoutique.fetchAll(editingContext(), new EOAndQualifier(arrayQual));
		
		
		return data.count()>0;
	}
	
    public boolean archiverBoutiqueEtCatalogues(boolean saveChanges) {
    	
     	NSArray cataloguesValides = getListeCatalogues();
     	Iterator<EOCatalogue> catIterator = cataloguesValides.iterator(); 
     	EOTypeEtat etatAnnule = FinderTypeEtat.typeEtatAnnule( editingContext() );
     	
     	EOCatalogue catalogue = null;
     	while(catIterator.hasNext()) {
     		catalogue = catIterator.next();
     		catalogue.setTypeEtat(etatAnnule);
     	} 
     	
     	setTypeEtat(etatAnnule);
     	boolean success = true;
     	if(saveChanges) {
     		try {
     			editingContext().saveChanges();
     		}
     		catch(Exception e) {
     			editingContext().revert();
     			e.printStackTrace();
     			success = false;
     		}
     	}
     	return success;
    }
    
    
    public NSArray<EOCatalogue> getListeCatalogues() {
    	return getListeCatalogues(null);
    }
    
    public NSArray<EOCatalogue> getListeCatalogues(EOQualifier additionalQualifier) {
    	
       	EOQualifier qualBou = new EOKeyValueQualifier( EOCatalogue.BOUTIQUES_CATALOGUES_KEY+"."+EOBoutiqueCatalogue.BOUTIQUE_KEY,
				   									   EOQualifier.QualifierOperatorEqual,
				   									   this);
       	EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(editingContext());
       	EOQualifier qualValide = new EOKeyValueQualifier( EOCatalogue.TYPE_ETAT_KEY,
					  									  EOQualifier.QualifierOperatorEqual,
					  									  etatValide);
   	
       	NSMutableArray<EOQualifier> qualArray = new NSMutableArray<EOQualifier>();
       	qualArray.add(qualBou);
       	qualArray.add(qualValide);
       	if(additionalQualifier!=null) {
       		qualArray.add(additionalQualifier);
       	}
       	
       	EOQualifier qualCatalogues = new EOAndQualifier(qualArray);
       	return EOCatalogue.fetchAll(editingContext(), qualCatalogues);
    }
	
    
    public NSArray<EOCatalogue> getCataloguesValables() {
		EOEditingContext ec = editingContext();
    	EOQualifier qualBou = EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.BOUTIQUES_CATALOGUES_KEY+"."+EOBoutiqueCatalogue.BOUTIQUE_KEY+"=%@", new NSArray<EOBoutique>(this)
				);
		EOQualifier qualDate = EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.CAT_DATE_FIN_KEY+" > %@ or "+EOCatalogue.CAT_DATE_FIN_KEY+" = nil",new NSArray<NSTimestamp>(new NSTimestamp())
				);
		EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(ec);
		EOQualifier qualValidite = EOQualifier.qualifierWithQualifierFormat(
					EOCatalogue.TYPE_ETAT_KEY+"=%@", new NSArray<EOTypeEtat>(etatValide)
					);
		NSMutableArray<EOQualifier> listQuals = new NSMutableArray<EOQualifier>();
		listQuals.addObject(qualBou);
		listQuals.addObject(qualDate);
		listQuals.addObject(qualValidite);

		EOAndQualifier qualifier = new EOAndQualifier(listQuals);
		return EOCatalogue.fetchAll(ec, qualifier, new NSArray(EOCatalogue.SORT_LIBELLE));
    }
    
    
	/* l'individu est responsable s'il est directement responsable de la boutique ou d'un des catalogues constituants la boutique */
	public boolean checkResponsableBoutique(Integer persId) {
		System.out.println("boutique.checkResponsableBoutique");
		boolean isResp = this.individuResponsable().persId().intValue() == persId.intValue();
		if(isResp) {
			System.out.println("IS RESPONSABLE");
			return true;
		}
		
		isResp = this.jefyAdminUtilisateur().persId().intValue() == persId.intValue();
		if(isResp) {
			System.out.println("IS JEFYADMRESPONSABLE");
			return true;
		}
		
		
		EOUtilisateur util = EOUtilisateur.fetchByKeyValue(editingContext(), EOUtilisateur.PERS_ID_KEY, persId);
		if(util!=null) {
			boolean voir = util.checkDroitFonction(EOFonction.DROIT_VOIR_PRESTATIONS) || util.checkDroitFonction(EOFonction.DROIT_VOIR_TOUTES_LES_PRESTATIONS);
			if(voir) {
				System.out.println("IS DROIT_VOIR_PRESTATIONS");
				return true;
			}
		}

		EOQualifier qualBoutique = new EOKeyValueQualifier(EOVEmailResponsableCatalogue.BOUTIQUE_KEY, EOQualifier.QualifierOperatorEqual,this);
		EOQualifier qualPersId = new EOKeyValueQualifier(EOVEmailResponsableCatalogue.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual,persId);
		
		EOQualifier qualResp = new EOAndQualifier( new NSArray( new EOQualifier[]{ qualBoutique, qualPersId } ) );
		
		NSArray values = EOVEmailResponsableCatalogue.fetchAll(editingContext(), qualResp);
		
		return values.count() > 0;
	}

	

	
	
	public final Integer unite() {
		return unite;
	}
	
	public boolean isArticleMultiple() {
		return !isArticleUnique() && !isArticleExclusif();
	}
	
	public boolean isArticleMultipleExclusif() {
		return !isArticleUnique() && isArticleExclusif();
	}
	
	
	
	
	public boolean isArticleUniqueExclusif() {
		return isArticleUnique() && isArticleExclusif();
	}
	
	public boolean isArticleExclusif() {
		return OUI.equals( boutiqueArticleExclusif() );
	}
	
	public boolean isArticleUnique() {
		return OUI.equals( boutiqueArticleUnique() );
	}
	
	public boolean isOptionUnique() {
		return OUI.equals( boutiqueOptionUnique() );
	}
	

    
    public boolean isAutoValPrestaClient() {
    	return boutiqueValClient().equals(OUI);
    }
    
    public boolean isAutoValPrestaPrestataire() {
    	return boutiqueValPrestataire().equals(OUI);
    }

	public boolean isAutoCloturePrestation() {
		return boutiqueValCloture().equals(OUI);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public NSArray<EOVEmailResponsableCatalogue> responsablesCatalogues() {
		NSMutableArray<EOVEmailResponsableCatalogue> array = new NSMutableArray<EOVEmailResponsableCatalogue>();
		NSArray<EOVEmailResponsableCatalogue> vEmailResp = vEmailResponsableCatalogues();
		NSArray<Integer> tmp;
		EOVEmailResponsableCatalogue enumElement;
		for (Enumeration<EOVEmailResponsableCatalogue> e = vEmailResp.objectEnumerator() ; e.hasMoreElements() ;) {
			enumElement = (EOVEmailResponsableCatalogue)e.nextElement();
			tmp = (NSArray)array.valueForKey( EOVEmailResponsableCatalogue.PERS_ID_KEY );
	       if( !tmp.contains( enumElement.persId() ) ) {
	    	   array.addObject( enumElement );
	       }
		 }
		return array;
	}
	
	
	
	public Integer pkBoutiqueId() {
		@SuppressWarnings("rawtypes")
		NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
		return (Integer)dico.objectForKey(BOUTIQUE_ID_KEY);
	}
	
	
	/* retourne le nombre d'inscriptions avec au moins une facture payee */
	public int countPaidRegistrations() {
		return getPaidOrPartiallyPaidRegistrations().count();
	}
	
	/* retourne les inscriptions ayant au moins une facture payee */
	@SuppressWarnings("unchecked")
	public NSArray<EOBoutiqueInscription> getPaidOrPartiallyPaidRegistrations() {
		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
		
		array.add( new EOKeyValueQualifier(EOBoutiqueInscription.BOUTIQUE_KEY,SEL_EQUAL, this) );
		array.add( new EOKeyValueQualifier(
						EOBoutiqueInscription.PRESTATION_KEY+"."+EOPrestation.FACTURE_PAPIERS_KEY+"."+EOFacturePapier.FAP_DATE_REGLEMENT_KEY,
						SEL_NEQUAL, 
						NSKeyValueCoding.NullValue)
		);
		array.add( new EOKeyValueQualifier(
						EOBoutiqueInscription.PRESTATION_KEY+"."+EOPrestation.FACTURE_PAPIERS_KEY+"."+EOFacturePapier.FAP_REFERENCE_REGLEMENT_KEY,
						SEL_NEQUAL, 
						NSKeyValueCoding.NullValue)
		);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOBoutiqueInscription.ENTITY_NAME, new EOAndQualifier(array), null, true, true, null);
		return editingContext().objectsWithFetchSpecification(fs);
	}
	
	
	
	
	/** Retourne true si toutes les places disponibles sont prises, false sinon */
	public boolean isToutesPrestationsPrises() {
		if(nombrePrestations()==null) {
			return false;
		}
		
		if(boutiqueInscriptions().count()>=nombrePrestations().intValue()) {
			return true;
		}
		else {
			return false;
		}
	}

	/** Retourne true si toutes les places disponibles sont prises ET payees, false sinon */
	public boolean isToutesPrestationsPrisesEtPayees() {
		
		if(nombrePrestations()==null) { return false; }
		
		int nbPlaces = nombrePrestations().intValue();
		@SuppressWarnings("unchecked")
		NSArray<EOBoutiqueInscription> inscs = boutiqueInscriptions();
		int countPaid = 0;
		for(EOBoutiqueInscription insc : inscs) {
			if(insc.prestation().aAuMoinsUneFacturePayee() ) {
				countPaid++;
			}
			if(countPaid>=nbPlaces) {
				return true;
			}
		}
		return false;
	}
	
	/** retourne true si l'application doit prendre en compte dans le calcul des places diponibles que les inscriptions/achats payes */
	public boolean isLimitationAuxPrestaPayees() {
		return FwkPieRecord.OUI.equals(limitationPrestPayees());
	}
	
	
	public boolean hasCataloguesValables() {
		return getCataloguesValables().count()>0;
	}
	
	@SuppressWarnings("unchecked")
	public static NSArray<EOBoutique> fetchAllValides(EOEditingContext ec) {
		EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(ec);
		EOQualifier qualValide = new EOKeyValueQualifier(TYPE_ETAT_KEY, EOQualifier.QualifierOperatorEqual, etatValide);
		return fetchAll(ec, qualValide);
	}
	
	
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
