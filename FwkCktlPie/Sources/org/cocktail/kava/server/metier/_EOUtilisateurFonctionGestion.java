/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurFonctionGestion.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOUtilisateurFonctionGestion extends  FwkPieRecord {
	public static final String ENTITY_NAME = "UtilisateurFonctionGestion";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.utilisateur_fonct_gestion";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ufgId";


// Attributs non visibles
	public static final String GES_CODE_KEY = "gesCode";
	public static final String UF_ORDRE_KEY = "ufOrdre";
	public static final String UFG_ID_KEY = "ufgId";

//Colonnes dans la base de donnees

	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String UF_ORDRE_COLKEY = "uf_ORDRE";
	public static final String UFG_ID_COLKEY = "ufg_id";


	// Relationships
	public static final String GESTION_KEY = "gestion";
	public static final String UTILISATEUR_FONCTION_KEY = "utilisateurFonction";



	// Accessors methods
  public org.cocktail.kava.server.metier.EOGestion gestion() {
    return (org.cocktail.kava.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }
  
	public void setGestion(org.cocktail.kava.server.metier.EOGestion value) {
		takeStoredValueForKey(value,GESTION_KEY);
	}


  public void setGestionRelationship(org.cocktail.kava.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateurFonction utilisateurFonction() {
    return (org.cocktail.kava.server.metier.EOUtilisateurFonction)storedValueForKey(UTILISATEUR_FONCTION_KEY);
  }
  
	public void setUtilisateurFonction(org.cocktail.kava.server.metier.EOUtilisateurFonction value) {
		takeStoredValueForKey(value,UTILISATEUR_FONCTION_KEY);
	}


  public void setUtilisateurFonctionRelationship(org.cocktail.kava.server.metier.EOUtilisateurFonction value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateurFonction oldValue = utilisateurFonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_FONCTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOUtilisateurFonctionGestion avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOUtilisateurFonctionGestion createEOUtilisateurFonctionGestion(EOEditingContext editingContext			) {
    EOUtilisateurFonctionGestion eo = (EOUtilisateurFonctionGestion) createAndInsertInstance(editingContext, _EOUtilisateurFonctionGestion.ENTITY_NAME);    
    return eo;
  }

  
	  public EOUtilisateurFonctionGestion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurFonctionGestion)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateurFonctionGestion creerInstance(EOEditingContext editingContext) {
	  		EOUtilisateurFonctionGestion object = (EOUtilisateurFonctionGestion)createAndInsertInstance(editingContext, _EOUtilisateurFonctionGestion.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOUtilisateurFonctionGestion localInstanceIn(EOEditingContext editingContext, EOUtilisateurFonctionGestion eo) {
    EOUtilisateurFonctionGestion localInstance = (eo == null) ? null : (EOUtilisateurFonctionGestion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOUtilisateurFonctionGestion#localInstanceIn a la place.
   */
	public static EOUtilisateurFonctionGestion localInstanceOf(EOEditingContext editingContext, EOUtilisateurFonctionGestion eo) {
		return EOUtilisateurFonctionGestion.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateurFonctionGestion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateurFonctionGestion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurFonctionGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurFonctionGestion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurFonctionGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurFonctionGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurFonctionGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurFonctionGestion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateurFonctionGestion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurFonctionGestion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurFonctionGestion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurFonctionGestion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
