/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArticleLocalise.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOArticleLocalise extends  FwkPieRecord {
	public static final String ENTITY_NAME = "ArticleLocalise";
	public static final String ENTITY_TABLE_NAME = "JEFY_CATALOGUE.ARTICLE_LOCALISE";



	// Attributes


	public static final String ART_DESCRIPTION_KEY = "artDescription";
	public static final String ART_LIBELLE_KEY = "artLibelle";
	public static final String ART_REFERENCE_KEY = "artReference";
	public static final String C_LANGUE_KEY = "cLangue";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";

//Colonnes dans la base de donnees
	public static final String ART_DESCRIPTION_COLKEY = "ART_DESCRIPTION";
	public static final String ART_LIBELLE_COLKEY = "ART_LIBELLE";
	public static final String ART_REFERENCE_COLKEY = "ART_REFERENCE";
	public static final String C_LANGUE_COLKEY = "C_LANGUE";

	public static final String ART_ID_COLKEY = "ART_ID";


	// Relationships
	public static final String ARTICLE_KEY = "article";



	// Accessors methods
  public String artDescription() {
    return (String) storedValueForKey(ART_DESCRIPTION_KEY);
  }

  public void setArtDescription(String value) {
    takeStoredValueForKey(value, ART_DESCRIPTION_KEY);
  }

  public String artLibelle() {
    return (String) storedValueForKey(ART_LIBELLE_KEY);
  }

  public void setArtLibelle(String value) {
    takeStoredValueForKey(value, ART_LIBELLE_KEY);
  }

  public String artReference() {
    return (String) storedValueForKey(ART_REFERENCE_KEY);
  }

  public void setArtReference(String value) {
    takeStoredValueForKey(value, ART_REFERENCE_KEY);
  }

  public String cLangue() {
    return (String) storedValueForKey(C_LANGUE_KEY);
  }

  public void setCLangue(String value) {
    takeStoredValueForKey(value, C_LANGUE_KEY);
  }

  public org.cocktail.kava.server.metier.EOArticle article() {
    return (org.cocktail.kava.server.metier.EOArticle)storedValueForKey(ARTICLE_KEY);
  }
  
	public void setArticle(org.cocktail.kava.server.metier.EOArticle value) {
		takeStoredValueForKey(value,ARTICLE_KEY);
	}


  public void setArticleRelationship(org.cocktail.kava.server.metier.EOArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOArticle oldValue = article();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_KEY);
    }
  }
  

/**
 * Créer une instance de EOArticleLocalise avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOArticleLocalise createEOArticleLocalise(EOEditingContext editingContext, String artLibelle
, String artReference
, String cLangue
, org.cocktail.kava.server.metier.EOArticle article			) {
    EOArticleLocalise eo = (EOArticleLocalise) createAndInsertInstance(editingContext, _EOArticleLocalise.ENTITY_NAME);    
		eo.setArtLibelle(artLibelle);
		eo.setArtReference(artReference);
		eo.setCLangue(cLangue);
    eo.setArticleRelationship(article);
    return eo;
  }

  
	  public EOArticleLocalise localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArticleLocalise)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArticleLocalise creerInstance(EOEditingContext editingContext) {
	  		EOArticleLocalise object = (EOArticleLocalise)createAndInsertInstance(editingContext, _EOArticleLocalise.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOArticleLocalise localInstanceIn(EOEditingContext editingContext, EOArticleLocalise eo) {
    EOArticleLocalise localInstance = (eo == null) ? null : (EOArticleLocalise)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOArticleLocalise#localInstanceIn a la place.
   */
	public static EOArticleLocalise localInstanceOf(EOEditingContext editingContext, EOArticleLocalise eo) {
		return EOArticleLocalise.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOArticleLocalise fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOArticleLocalise fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArticleLocalise eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArticleLocalise)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArticleLocalise fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArticleLocalise fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArticleLocalise eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArticleLocalise)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOArticleLocalise fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArticleLocalise eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArticleLocalise ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArticleLocalise fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
