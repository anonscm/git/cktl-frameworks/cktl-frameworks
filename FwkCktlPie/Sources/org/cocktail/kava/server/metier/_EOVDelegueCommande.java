/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVDelegueCommande.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOVDelegueCommande extends  FwkPieRecord {
	public static final String ENTITY_NAME = "VDelegueCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_RECETTE.V_DELEGUE_COMMANDE";



	// Attributes


	public static final String ASS_ID_KEY = "assId";
	public static final String C_STRUCTURE_CLIENT_KEY = "cStructureClient";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PERS_ID_CLIENT_KEY = "persIdClient";
	public static final String PERS_ID_DELEGUE_KEY = "persIdDelegue";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ASS_ID_COLKEY = "ASS_ID";
	public static final String C_STRUCTURE_CLIENT_COLKEY = "C_STRUCTURE_CLIENT";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PERS_ID_CLIENT_COLKEY = "PERS_ID_CLIENT";
	public static final String PERS_ID_DELEGUE_COLKEY = "PERS_ID_DELEGUE";



	// Relationships
	public static final String INDIVIDU_ULRS_KEY = "individuUlrs";
	public static final String STRUCTURE_ULRS_KEY = "structureUlrs";



	// Accessors methods
  public Integer assId() {
    return (Integer) storedValueForKey(ASS_ID_KEY);
  }

  public void setAssId(Integer value) {
    takeStoredValueForKey(value, ASS_ID_KEY);
  }

  public String cStructureClient() {
    return (String) storedValueForKey(C_STRUCTURE_CLIENT_KEY);
  }

  public void setCStructureClient(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_CLIENT_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer persIdClient() {
    return (Integer) storedValueForKey(PERS_ID_CLIENT_KEY);
  }

  public void setPersIdClient(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CLIENT_KEY);
  }

  public Integer persIdDelegue() {
    return (Integer) storedValueForKey(PERS_ID_DELEGUE_KEY);
  }

  public void setPersIdDelegue(Integer value) {
    takeStoredValueForKey(value, PERS_ID_DELEGUE_KEY);
  }

  public NSArray individuUlrs() {
    return (NSArray)storedValueForKey(INDIVIDU_ULRS_KEY);
  }
  
  //ICI
  public void setIndividuUlrs(NSArray values) {
	  takeStoredValueForKey(values,INDIVIDU_ULRS_KEY);
  }
  

  public NSArray individuUlrs(EOQualifier qualifier) {
    return individuUlrs(qualifier, null);
  }

  public NSArray individuUlrs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = individuUlrs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToIndividuUlrsRelationship(org.cocktail.kava.server.metier.EOIndividuUlr object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INDIVIDU_ULRS_KEY);
  }

  public void removeFromIndividuUlrsRelationship(org.cocktail.kava.server.metier.EOIndividuUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INDIVIDU_ULRS_KEY);
  }

  public org.cocktail.kava.server.metier.EOIndividuUlr createIndividuUlrsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("IndividuUlr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INDIVIDU_ULRS_KEY);
    return (org.cocktail.kava.server.metier.EOIndividuUlr) eo;
  }

  public void deleteIndividuUlrsRelationship(org.cocktail.kava.server.metier.EOIndividuUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INDIVIDU_ULRS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllIndividuUlrsRelationships() {
    Enumeration objects = individuUlrs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteIndividuUlrsRelationship((org.cocktail.kava.server.metier.EOIndividuUlr)objects.nextElement());
    }
  }

  public NSArray structureUlrs() {
    return (NSArray)storedValueForKey(STRUCTURE_ULRS_KEY);
  }
  
  //ICI
  public void setStructureUlrs(NSArray values) {
	  takeStoredValueForKey(values,STRUCTURE_ULRS_KEY);
  }
  

  public NSArray structureUlrs(EOQualifier qualifier) {
    return structureUlrs(qualifier, null);
  }

  public NSArray structureUlrs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = structureUlrs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToStructureUlrsRelationship(org.cocktail.kava.server.metier.EOStructureUlr object) {
    addObjectToBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
  }

  public void removeFromStructureUlrsRelationship(org.cocktail.kava.server.metier.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
  }

  public org.cocktail.kava.server.metier.EOStructureUlr createStructureUlrsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("StructureUlr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, STRUCTURE_ULRS_KEY);
    return (org.cocktail.kava.server.metier.EOStructureUlr) eo;
  }

  public void deleteStructureUlrsRelationship(org.cocktail.kava.server.metier.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllStructureUlrsRelationships() {
    Enumeration objects = structureUlrs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteStructureUlrsRelationship((org.cocktail.kava.server.metier.EOStructureUlr)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOVDelegueCommande avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVDelegueCommande createEOVDelegueCommande(EOEditingContext editingContext, Integer assId
, String cStructureClient
, Integer exeOrdre
, Integer persIdClient
, Integer persIdDelegue
			) {
    EOVDelegueCommande eo = (EOVDelegueCommande) createAndInsertInstance(editingContext, _EOVDelegueCommande.ENTITY_NAME);    
		eo.setAssId(assId);
		eo.setCStructureClient(cStructureClient);
		eo.setExeOrdre(exeOrdre);
		eo.setPersIdClient(persIdClient);
		eo.setPersIdDelegue(persIdDelegue);
    return eo;
  }

  
	  public EOVDelegueCommande localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVDelegueCommande)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVDelegueCommande creerInstance(EOEditingContext editingContext) {
	  		EOVDelegueCommande object = (EOVDelegueCommande)createAndInsertInstance(editingContext, _EOVDelegueCommande.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOVDelegueCommande localInstanceIn(EOEditingContext editingContext, EOVDelegueCommande eo) {
    EOVDelegueCommande localInstance = (eo == null) ? null : (EOVDelegueCommande)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVDelegueCommande#localInstanceIn a la place.
   */
	public static EOVDelegueCommande localInstanceOf(EOEditingContext editingContext, EOVDelegueCommande eo) {
		return EOVDelegueCommande.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVDelegueCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVDelegueCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVDelegueCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVDelegueCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVDelegueCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVDelegueCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVDelegueCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVDelegueCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVDelegueCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVDelegueCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVDelegueCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVDelegueCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
