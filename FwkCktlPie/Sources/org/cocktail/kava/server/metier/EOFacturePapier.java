// EOFacturePapier.java
// 

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.AFinder;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.kava.server.finder.FinderTypeApplication;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXThreadStorage;

public class EOFacturePapier extends _EOFacturePapier {
	public static final String FAP_ID_KEY = "fapId";

	public static final String FAP_TOTAL_HT_LIVE_KEY = "fapTotalHtLive";
	public static final String FAP_TOTAL_TTC_LIVE_KEY = "fapTotalTtcLive";
	public static final String FAP_TOTAL_TVA_LIVE_KEY = "fapTotalTvaLive";

	public static final String SEQ_EOFACTUREPAPIER_ENTITY_NAME = "SeqEOFacturePapier";
	public static final String MODE_FC = "modeFC";
	private String cacheRefPaiement;

	public EOFacturePapier() {
		super();
	}

	public Integer numCommandePaybox() {
		return new Integer(StringCtrl.get0Int(this.fapId().intValue(), 10));
	}

	public Integer fapId() {
		try {
			NSDictionary dico = EOUtilities.primaryKeyForObject(
					editingContext(), this);
			return (Integer) dico.objectForKey(FAP_ID_KEY);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire
	 * (peu importe son etape actuelle de validation)
	 * 
	 * @return true ou false
	 */
	public boolean isValidablePrest() {
		if (organ() == null || tauxProrata() == null || typeCreditRec() == null
				|| lolfNomenclatureRecette() == null || planComptable() == null
				|| modeRecouvrement() == null) {
			return false;
		}
		return true;
	}

	public boolean isValidableClient() {
		if (!typePublic().typeApplication().equals(
				FinderTypeApplication
						.typeApplicationPrestationInterne(editingContext()))) {
			return true;
		}
		return true;
	}

	public void setFapRemiseGlobale(BigDecimal aValue) {
		if (aValue != null && aValue.equals(fapRemiseGlobale())) {
			return;
		}
		super.setFapRemiseGlobale(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				((EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i))
						.updateTotaux();
			}
		}
	}

	/**
	 * @param aValue
	 *            (O/N) applique ou non la tva, et met a jour les lignes de la
	 *            facture papier, et recalcule le total ttc
	 */
	public void setFapApplyTva(String aValue) {
		if (aValue != null && aValue.equals(fapApplyTva())) {
			return;
		}
		super.setFapApplyTva(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				EOFacturePapierLigne flig = (EOFacturePapierLigne) facturePapierLignes()
						.objectAtIndex(i);
				if ("N".equalsIgnoreCase(fapApplyTva())) {
					flig.setTvaRelationship(null);
					flig.setFligArtTtc(flig.fligArtHt());
				} else {
					flig.setTvaRelationship(flig.tvaInitial());
					flig.setFligArtTtc(flig.fligArtTtcInitial());
				}
			}
		}
	}

	public BigDecimal fapTotalHtLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) facturePapierLignes().valueForKey(
				"@sum." + EOFacturePapierLigne.FLIG_TOTAL_HT_KEY);
	}

	public BigDecimal fapTotalTtcLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) facturePapierLignes().valueForKey(
				"@sum." + EOFacturePapierLigne.FLIG_TOTAL_TTC_KEY);
	}

	public BigDecimal fapTotalTvaLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return new BigDecimal(0.0);
		}
		return (BigDecimal) facturePapierLignes().valueForKey(
				"@sum." + EOFacturePapierLigne.FLIG_TOTAL_TVA_KEY);
	}

	public void updateTotaux() {
		Boolean isModeFC = (Boolean) ERXThreadStorage
				.valueForKey(EOFacturePapier.MODE_FC);
		if (!isModeFC) {
			setFapTotalHt(fapTotalHtLive());
			setFapTotalTva(fapTotalTvaLive());
			setFapTotalTtc(fapTotalTtcLive());
		}
	}

	/*
	 * // If you add instance variables to store property values you // should
	 * add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the
	 * superclass). private void writeObject(java.io.ObjectOutputStream out)
	 * throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws
	 * java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public String getCacheRefPaiement() {
		if (cacheRefPaiement == null) {
			cacheRefPaiement = "VAL_MANU";
		}
		return cacheRefPaiement;
	}

	public void setCacheRefPaiement(String cacheRefPaiement) {
		this.cacheRefPaiement = cacheRefPaiement;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave()
			throws NSValidation.ValidationException {
			updateTotaux();
	}

	public Integer creerFapId(EOEditingContext ec) {
		return AFinder.clePrimairePour(ec, EOFacturePapier.ENTITY_NAME,
				EOFacturePapier.FAP_ID_KEY, SEQ_EOFACTUREPAPIER_ENTITY_NAME,
				true);
	}
}
