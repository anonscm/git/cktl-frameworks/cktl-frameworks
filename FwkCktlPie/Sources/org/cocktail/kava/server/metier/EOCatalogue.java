
// EOCatalogue.java
// 

package org.cocktail.kava.server.metier;

import java.util.ArrayList;
import java.util.Iterator;

import org.cocktail.application.serveur.eof.EOCodeMarche;
import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.server.finder.FinderTypeEtat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.qualifiers.ERXInQualifier;

public class EOCatalogue extends _EOCatalogue {
	
	public static final String PK_CAT_ID  = "pkCatId";
	public static final EOSortOrdering SORT_LIBELLE = EOSortOrdering.sortOrderingWithKey(CAT_LIBELLE_KEY, EOSortOrdering.CompareAscending);
	
	public static final NSTimestampFormatter FORMAT_DATE =new NSTimestampFormatter("%m/%d/%Y");
	
	
	public EOCatalogue() {
		super();
	}
	
	
	public String numeroEtLibelle() {
		return cataloguePrestation().catNumero().intValue() + " - " + catLibelle();
	}
	
	
	public boolean checkResponsableCatalogue(Integer persId) {
		System.out.println("checkResponsableCatalogue : "+persId);
		EOQualifier qualCat = new EOKeyValueQualifier(EOVEmailResponsableCatalogue.CATALOGUE_KEY, EOQualifier.QualifierOperatorEqual,this);
		EOQualifier qualPersId = new EOKeyValueQualifier(EOVEmailResponsableCatalogue.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual,persId);
		
		EOQualifier qualResp = new EOAndQualifier( new NSArray( new EOQualifier[]{ qualCat, qualPersId } ) );
		
		NSArray values = EOVEmailResponsableCatalogue.fetchAll(editingContext(), qualResp);
		
		return values.count() > 0;
	}

	
	
	/** permet d'archiver/annuler le catalogue */
	public void archiver(boolean archiverArticles,boolean saveChanges) {
		
		EOTypeEtat etatAnnule = FinderTypeEtat.typeEtatAnnule( editingContext() );
		setTypeEtat(etatAnnule);
		if(archiverArticles) {
			archiverArticles(false);
		}
		if(saveChanges) {
			editingContext().saveChanges();
		}
	}
	
	/** permet d'archiver tous les articles du catalogue, avec ou sans saveChanges */
	public void archiverArticles(boolean saveChanges) {
		
		NSArray<EOCatalogueArticle> cArticles = catalogueArticles();
		for(EOCatalogueArticle item : cArticles) {
			item.archiver(false);
		}
		if(saveChanges) {
			editingContext().saveChanges();
		}
	}
	
	
	public String getLibelleEn() {
		EOCatalogueLocalise locEn = getCatalogueLocaliseEn();
		if(locEn!=null) {
			return locEn.catLibelle();
		}
		else {
			return null;
		}
	}
	
	public String getCommentaireEn() {
		EOCatalogueLocalise locEn = getCatalogueLocaliseEn();
		if(locEn!=null) {
			return locEn.catCommentaire();
		}
		else {
			return null;
		}
	}
	
	public EOCatalogueLocalise getCatalogueLocaliseEn() {
		
		EOQualifier qualEn = EOQualifier.qualifierWithQualifierFormat(EOCatalogueLocalise.C_LANGUE_KEY+"='en'",null);
		NSArray results = EOQualifier.filteredArrayWithQualifier(cataloguesLocalises(),qualEn);
		if(results.count()>0) {
			return (EOCatalogueLocalise)results.lastObject();
		}
		else {
			return null;
		}
	}
	

	public NSArray<EOTypePublic> typesPublicAffectes() {
		return (NSArray<EOTypePublic>)cataloguePublics().valueForKey(EOCataloguePublic.TYPE_PUBLIC_KEY);
	}

	public void setTypesPublicAffectes(NSArray<EOTypePublic> selectedValues) {
		
		boolean newCreation = globalID().isTemporary();
		NSMutableArray<EOTypePublic> arrayAdd = new NSMutableArray<EOTypePublic>();
		
		if(newCreation) {
			arrayAdd.addObjectsFromArray(selectedValues);
		}
		else {
			NSArray<EOTypePublic> existing = typesPublicAffectes();
			NSMutableArray<EOTypePublic> arrayRemove = new NSMutableArray<EOTypePublic>();	
			
			for(EOTypePublic item : selectedValues) {
				if(!existing.contains(item)) {
					arrayAdd.add(item);
				}
			}
			
			for(EOTypePublic item : existing) {
				if(!selectedValues.contains(item)) {
					arrayRemove.add(item);
				}
			}

			ERXInQualifier qualType = new ERXInQualifier(EOCataloguePublic.TYPE_PUBLIC_KEY,arrayRemove);
			NSArray<EOCataloguePublic> catPubRemove = EOQualifier.filteredArrayWithQualifier(this.cataloguePublics(), qualType);
		
			for(EOCataloguePublic typePubRemove : catPubRemove) {
				typePubRemove.removeObjectFromBothSidesOfRelationshipWithKey(this, EOCataloguePublic.CATALOGUE_KEY);
				typePubRemove.removeObjectFromBothSidesOfRelationshipWithKey(typePubRemove, EOCataloguePublic.TYPE_PUBLIC_KEY);
				editingContext().deleteObject(typePubRemove);
			}
		}
		
		// insertion des nouveaux types public a affecter reellement
		for(EOTypePublic typePub : arrayAdd) {
			EOCataloguePublic.newObject(editingContext(), this, typePub);
		}
		
	}
	
	
	/* retire des responsables catalogue l'utilisateur util */
	public void removeFromResponsables(EOUtilisateur util) {
		NSArray<EOCatalogueResponsable> resps = catalogueResponsables();
		NSMutableArray<EOCatalogueResponsable> array = new NSMutableArray<EOCatalogueResponsable>(resps);
		
		for(EOCatalogueResponsable resp : array) {
			if(resp.utilisateur().equals(util)) {
				resp.removeObjectFromBothSidesOfRelationshipWithKey(resp.catalogue(), EOCatalogueResponsable.CATALOGUE_KEY);
				resp.removeObjectFromBothSidesOfRelationshipWithKey(resp.utilisateur(), EOCatalogueResponsable.UTILISATEUR_KEY);
				editingContext().deleteObject( resp );
			}
		}
	}
	
	public String chaineDateDebut() {
		NSTimestamp deb = this.catDateDebut();
		if(deb!=null) {
			return FORMAT_DATE.format(deb); 
		}
		else {
			return null;
		}
	}
	
	public String chaineDateFin() {
		NSTimestamp fin = this.catDateFin();
		if(fin!=null) {
			return FORMAT_DATE.format(fin);
		}
		else {
			return null;
		}
	}
	
	
	
	public Number pkCatId() {
		NSDictionary pk = EOUtilities.primaryKeyForObject(editingContext(), this);
		return (Number)pk.valueForKey(CAT_ID_KEY);
	}
	
	
	/* Affecte le code marché value au catalogue mais aussi aux articles du catalogue */
	public void setAffectationCodeMarche(EOCodeMarche value) {
		NSArray catArticles = catalogueArticles();
		Iterator<EOCatalogueArticle> iCatArticles = catArticles.iterator();
		EOArticle article;
		while(iCatArticles.hasNext()) {
			System.out.println("Article->CodeMarche");
			article = ((EOCatalogueArticle)iCatArticles.next()).article();
			article.setCodeMarcheRelationship(value);
		}
		
		setCodeMarcheRelationship(value);
	}
	
	
	public EOCodeMarche affectationCodeMarche() {
		return codeMarche();
	}
	
	
	
	
	public void updateArticlesInfos() {
		
		
		@SuppressWarnings("unchecked")
		NSArray<EOCatalogueArticle> cArticles = catalogueArticles();
		
		EOCataloguePrestation catP = cataloguePrestation();
		EOArticlePrestation artP;
		for(EOCatalogueArticle item : cArticles) {
			artP = item.article().articlePrestation();
			if(catP.planComptableRecette()!=null && 
					!catP.planComptableRecette().equals(artP.planComptableRecette())) {
				artP.setPlanComptableRecetteRelationship(catP.planComptableRecette());
				System.out.println("update pconum_rec");
			}
			
			if(catP.planComptableDepense()!=null && 
					!catP.planComptableDepense().equals(artP.planComptableDepense())) {
				artP.setPlanComptableDepenseRelationship(catP.planComptableDepense());
				System.out.println("update pconum_dep");
			}
		}
	}
	

	public Object valueForKey(java.lang.String arg0) {
		Object obj = null;
		try {
			obj = super.valueForKey(arg0);
		}
		catch (Throwable e) {
			try {
				int i = 0;
				for (NSArray list = (com.webobjects.foundation.NSArray) cataloguePrestationWebs().valueForKey(arg0); (obj == null || obj
						.equals(NullValue))
						&& list.count() > i; i++)
					obj = list.objectAtIndex(i);
			}
			catch (Throwable e2) {
				obj = NullValue;
			}
		}
		return obj;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		
		ArrayList<String> list = new ArrayList<String>();
		if(catDateDebut()==null) {
			list.add("Date de début");
		}
		
		if(fournisUlr()==null) {
			list.add("Fournisseur de catalogue");
		}
		
		if(catLibelle()==null) {
			list.add("Libellé");
		}
		
		if(catCommentaire()==null) {
			list.add("Description du catalogue");
		}
		
		
		if(cataloguePublics()==null || cataloguePublics().count()==0) {
			list.add("Les types de publics");
		}
		
		EOCataloguePrestation catPresta = cataloguePrestation();
		if(catPresta==null) {
			list.add("Les informations budgétaires");
		}
		else {
			if(catPresta.planComptableRecette()==null) {
				list.add("L'imputation recette");
			}
			if(catPresta.organRecette()==null) {
				list.add("Ligne budgétaire recette");
			}
			if(catPresta.lolfNomenclatureRecette()==null) {
				list.add("Action LOLF recette");
			}
		}
		
		NSArray<EOCatalogueResponsable> resp = catalogueResponsables();
		if(resp==null || resp.count()==0) {
			list.add("Les résponsables du catalogue");
		}
		
		if(list.size()>0) {
			String msg = "Informations manquantes : \n"+list.toString();
			throw new NSValidation.ValidationException(msg);
		}
		
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
