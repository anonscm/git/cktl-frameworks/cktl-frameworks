/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrestationBascule.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPrestationBascule extends  FwkPieRecord {
	public static final String ENTITY_NAME = "PrestationBascule";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_bascule";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "presbId";

	public static final String PREST_ID_ORIGINE_BIS_KEY = "prestIdOrigineBis";

// Attributs non visibles
	public static final String PRESB_ID_KEY = "presbId";
	public static final String PREST_ID_DESTINATION_KEY = "prestIdDestination";
	public static final String PREST_ID_ORIGINE_KEY = "prestIdOrigine";

//Colonnes dans la base de donnees
	public static final String PREST_ID_ORIGINE_BIS_COLKEY = "PREST_ID_ORIGINE";

	public static final String PRESB_ID_COLKEY = "PRESB_ID";
	public static final String PREST_ID_DESTINATION_COLKEY = "PREST_ID_DESTINATION";
	public static final String PREST_ID_ORIGINE_COLKEY = "PREST_ID_ORIGINE";


	// Relationships
	public static final String PRESTATION_DESTINATION_KEY = "prestationDestination";
	public static final String PRESTATION_ORIGINE_KEY = "prestationOrigine";



	// Accessors methods
  public Integer prestIdOrigineBis() {
    return (Integer) storedValueForKey(PREST_ID_ORIGINE_BIS_KEY);
  }

  public void setPrestIdOrigineBis(Integer value) {
    takeStoredValueForKey(value, PREST_ID_ORIGINE_BIS_KEY);
  }

  public org.cocktail.kava.server.metier.EOPrestation prestationDestination() {
    return (org.cocktail.kava.server.metier.EOPrestation)storedValueForKey(PRESTATION_DESTINATION_KEY);
  }
  
	public void setPrestationDestination(org.cocktail.kava.server.metier.EOPrestation value) {
		takeStoredValueForKey(value,PRESTATION_DESTINATION_KEY);
	}


  public void setPrestationDestinationRelationship(org.cocktail.kava.server.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPrestation oldValue = prestationDestination();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_DESTINATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_DESTINATION_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPrestation prestationOrigine() {
    return (org.cocktail.kava.server.metier.EOPrestation)storedValueForKey(PRESTATION_ORIGINE_KEY);
  }
  
	public void setPrestationOrigine(org.cocktail.kava.server.metier.EOPrestation value) {
		takeStoredValueForKey(value,PRESTATION_ORIGINE_KEY);
	}


  public void setPrestationOrigineRelationship(org.cocktail.kava.server.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPrestation oldValue = prestationOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_ORIGINE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPrestationBascule avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrestationBascule createEOPrestationBascule(EOEditingContext editingContext			) {
    EOPrestationBascule eo = (EOPrestationBascule) createAndInsertInstance(editingContext, _EOPrestationBascule.ENTITY_NAME);    
    return eo;
  }

  
	  public EOPrestationBascule localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrestationBascule)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrestationBascule creerInstance(EOEditingContext editingContext) {
	  		EOPrestationBascule object = (EOPrestationBascule)createAndInsertInstance(editingContext, _EOPrestationBascule.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPrestationBascule localInstanceIn(EOEditingContext editingContext, EOPrestationBascule eo) {
    EOPrestationBascule localInstance = (eo == null) ? null : (EOPrestationBascule)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrestationBascule#localInstanceIn a la place.
   */
	public static EOPrestationBascule localInstanceOf(EOEditingContext editingContext, EOPrestationBascule eo) {
		return EOPrestationBascule.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrestationBascule fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrestationBascule fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrestationBascule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrestationBascule)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrestationBascule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrestationBascule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrestationBascule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrestationBascule)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrestationBascule fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrestationBascule eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrestationBascule ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrestationBascule fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
