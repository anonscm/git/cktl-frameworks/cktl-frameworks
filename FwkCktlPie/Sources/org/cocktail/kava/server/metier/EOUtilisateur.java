

// EOUtilisateur.java
// 

package org.cocktail.kava.server.metier;


import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.server.finder.FinderTypeEtat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOUtilisateur extends _EOUtilisateur
{
    public EOUtilisateur() {
        super();
    }
    
    
    
    public Integer getPkUtlOrdre() {
    	NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
    	if(dico!=null) {
    		return (Integer)dico.objectForKey("utlOrdre");
    	}
    	return null;
    }
    

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    
	public boolean canUseFonction(String fonIdInterne, EOExercice exercice) {
		NSArray utilFonctions = utilisateurFonctions();
		
		if (utilFonctions.count()==0) {
			System.out.println(":-( L'utilisateur n'a pas de fonctions jefy !");
			return false;
		}
		EOQualifier qualFon = new EOKeyValueQualifier(
									EOUtilisateurFonction.FONCTION_KEY+"."+EOFonction.FON_ID_INTERNE_KEY,
									EOQualifier.QualifierOperatorEqual,
									fonIdInterne
								);
		
		NSArray<EOUtilisateurFonction> tmp = EOQualifier.filteredArrayWithQualifier(utilFonctions, qualFon);
		
		// pas d'exercice demandé on remonte le droit tel quel.
		if(exercice==null) {
			return tmp.count() > 0;
		}
		
		// si un exercice est demandé on poursuit sur l'exercice.
		EOUtilisateurFonction utilFonc = null;
		if(tmp.count()>0) {
			utilFonc = tmp.lastObject();
		}
		else {
			return false;
		}
		
		EOQualifier qualFuncEx = EOQualifier.qualifierWithQualifierFormat(
								EOUtilisateurFonctionExercice.UTILISATEUR_FONCTION_KEY + "=%@ and "+
								EOUtilisateurFonctionExercice.EXERCICE_KEY + "=%@",
								new NSArray(new Object[]{utilFonc,exercice}));
		
		return EOUtilisateurFonctionExercice.fetchAll(editingContext(), qualFuncEx).count() > 0;
	}

    
    
    public boolean checkDroitGestionCatalogues() {
    	return checkDroitFonction(EOUtilisateurFonction.FON_ID_GESTION_CATALOGUES);
    }
    
    /*TODO : tenir compte de l'exercice */ 
    public boolean checkDroitFonction(String fonIdInterne) {
    	return canUseFonction(fonIdInterne, null);
    	/*
		EOFonction fonction = EOFonction.fetchByKeyValue(editingContext(), EOFonction.FON_ID_INTERNE_KEY, fonctionId);
		NSArray utilFonctions = utilisateurFonctions();
		EOQualifier qualDroitBoutique = new EOKeyValueQualifier(EOUtilisateurFonction.FONCTION_KEY, EOQualifier.QualifierOperatorEqual, fonction);
		utilFonctions = EOQualifier.filteredArrayWithQualifier(utilFonctions, qualDroitBoutique);
		return utilFonctions.count() > 0;
    	*/
    }
    
    
    
    public static NSArray<EOUtilisateur> getFileteredUtilisateurs(String aInput,EOEditingContext ec) {
		String input = "*"+aInput+"*";
		
		EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(ec);
		
		EOQualifier qualUtl = EOQualifier.qualifierWithQualifierFormat(
								EOUtilisateur.PERSONNE_KEY+"."+EOPersonne.PERS_NOMPTR_KEY+" caseInsensitiveLike %@ or "+
								EOUtilisateur.PERSONNE_KEY+"."+EOPersonne.PERS_LIBELLE_KEY+" caseInsensitiveLike %@",
								new NSArray<String>(new String[]{input,input})
								);
																			
		
		EOQualifier qualValide = new EOKeyValueQualifier(EOUtilisateur.TYPE_ETAT_KEY,EOQualifier.QualifierOperatorEqual,etatValide);
		
		EOQualifier qualUtl1 = new EOKeyValueQualifier(
				EOUtilisateur.PERSONNE_KEY+"."+EOPersonne.PERS_NOMPTR_KEY,EOQualifier.QualifierOperatorCaseInsensitiveLike,input
				);
		
		EOQualifier qualUtl2 = new EOKeyValueQualifier(
				EOUtilisateur.PERSONNE_KEY+"."+EOPersonne.PERS_LIBELLE_KEY,EOQualifier.QualifierOperatorCaseInsensitiveLike,input
				);
				
		return EOUtilisateur.fetchAll(ec, qualUtl);
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
