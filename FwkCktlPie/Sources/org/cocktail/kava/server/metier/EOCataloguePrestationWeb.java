// EOCataloguePrestationWeb.java
// 

package org.cocktail.kava.server.metier;

import com.webobjects.foundation.NSValidation;

public class EOCataloguePrestationWeb extends _EOCataloguePrestationWeb {
	public EOCataloguePrestationWeb() {
		super();
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 * 
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public java.lang.Object valueForKey(java.lang.String arg0) {
		java.lang.Object obj = null;
		try {
			obj = super.valueForKey(arg0);
		}
		catch (java.lang.Throwable e) {
			try {
				if (arg0 != null && arg0.equals(catCle()) && catValeur() != null && !"".equals(catValeur()))
					return catValeur();
				else
					return null;
			}
			catch (Throwable e2) {
				return null;
			}

		}
		return obj;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
