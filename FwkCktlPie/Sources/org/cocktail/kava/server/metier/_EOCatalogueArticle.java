/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCatalogueArticle.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOCatalogueArticle extends  FwkPieRecord {
	public static final String ENTITY_NAME = "CatalogueArticle";
	public static final String ENTITY_TABLE_NAME = "jefy_catalogue.catalogue_article";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "caarId";

	public static final String CAAR_DOC_ID_KEY = "caarDocId";
	public static final String CAAR_DOC_REFERENCE_KEY = "caarDocReference";
	public static final String CAAR_PRIX_HT_KEY = "caarPrixHt";
	public static final String CAAR_PRIX_TTC_KEY = "caarPrixTtc";
	public static final String CAAR_REFERENCE_KEY = "caarReference";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String CAAR_ID_KEY = "caarId";
	public static final String CAT_ID_KEY = "catId";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CAAR_DOC_ID_COLKEY = "CAAR_DOC_ID";
	public static final String CAAR_DOC_REFERENCE_COLKEY = "CAAR_DOC_REFERENCE";
	public static final String CAAR_PRIX_HT_COLKEY = "CAAR_PRIX_HT";
	public static final String CAAR_PRIX_TTC_COLKEY = "CAAR_PRIX_TTC";
	public static final String CAAR_REFERENCE_COLKEY = "CAAR_REFERENCE";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String CAAR_ID_COLKEY = "CAAR_ID";
	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String ARTICLE_KEY = "article";
	public static final String ARTICLES_LOCALISES_KEY = "articlesLocalises";
	public static final String CATALOGUE_KEY = "catalogue";
	public static final String TVA_KEY = "tva";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public Integer caarDocId() {
    return (Integer) storedValueForKey(CAAR_DOC_ID_KEY);
  }

  public void setCaarDocId(Integer value) {
    takeStoredValueForKey(value, CAAR_DOC_ID_KEY);
  }

  public String caarDocReference() {
    return (String) storedValueForKey(CAAR_DOC_REFERENCE_KEY);
  }

  public void setCaarDocReference(String value) {
    takeStoredValueForKey(value, CAAR_DOC_REFERENCE_KEY);
  }

  public java.math.BigDecimal caarPrixHt() {
    return (java.math.BigDecimal) storedValueForKey(CAAR_PRIX_HT_KEY);
  }

  public void setCaarPrixHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAAR_PRIX_HT_KEY);
  }

  public java.math.BigDecimal caarPrixTtc() {
    return (java.math.BigDecimal) storedValueForKey(CAAR_PRIX_TTC_KEY);
  }

  public void setCaarPrixTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAAR_PRIX_TTC_KEY);
  }

  public String caarReference() {
    return (String) storedValueForKey(CAAR_REFERENCE_KEY);
  }

  public void setCaarReference(String value) {
    takeStoredValueForKey(value, CAAR_REFERENCE_KEY);
  }

  public org.cocktail.kava.server.metier.EOArticle article() {
    return (org.cocktail.kava.server.metier.EOArticle)storedValueForKey(ARTICLE_KEY);
  }
  
	public void setArticle(org.cocktail.kava.server.metier.EOArticle value) {
		takeStoredValueForKey(value,ARTICLE_KEY);
	}


  public void setArticleRelationship(org.cocktail.kava.server.metier.EOArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOArticle oldValue = article();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOCatalogue catalogue() {
    return (org.cocktail.kava.server.metier.EOCatalogue)storedValueForKey(CATALOGUE_KEY);
  }
  
	public void setCatalogue(org.cocktail.kava.server.metier.EOCatalogue value) {
		takeStoredValueForKey(value,CATALOGUE_KEY);
	}


  public void setCatalogueRelationship(org.cocktail.kava.server.metier.EOCatalogue value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCatalogue oldValue = catalogue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTva tva() {
    return (org.cocktail.kava.server.metier.EOTva)storedValueForKey(TVA_KEY);
  }
  
	public void setTva(org.cocktail.kava.server.metier.EOTva value) {
		takeStoredValueForKey(value,TVA_KEY);
	}


  public void setTvaRelationship(org.cocktail.kava.server.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTva oldValue = tva();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.application.serveur.eof.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray articlesLocalises() {
    return (NSArray)storedValueForKey(ARTICLES_LOCALISES_KEY);
  }
  
  //ICI
  public void setArticlesLocalises(NSArray values) {
	  takeStoredValueForKey(values,ARTICLES_LOCALISES_KEY);
  }
  

  public NSArray articlesLocalises(EOQualifier qualifier) {
    return articlesLocalises(qualifier, null);
  }

  public NSArray articlesLocalises(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = articlesLocalises();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToArticlesLocalisesRelationship(org.cocktail.kava.server.metier.EOArticleLocalise object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_LOCALISES_KEY);
  }

  public void removeFromArticlesLocalisesRelationship(org.cocktail.kava.server.metier.EOArticleLocalise object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_LOCALISES_KEY);
  }

  public org.cocktail.kava.server.metier.EOArticleLocalise createArticlesLocalisesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ArticleLocalise");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLES_LOCALISES_KEY);
    return (org.cocktail.kava.server.metier.EOArticleLocalise) eo;
  }

  public void deleteArticlesLocalisesRelationship(org.cocktail.kava.server.metier.EOArticleLocalise object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_LOCALISES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllArticlesLocalisesRelationships() {
    Enumeration objects = articlesLocalises().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteArticlesLocalisesRelationship((org.cocktail.kava.server.metier.EOArticleLocalise)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCatalogueArticle avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCatalogueArticle createEOCatalogueArticle(EOEditingContext editingContext, java.math.BigDecimal caarPrixHt
, java.math.BigDecimal caarPrixTtc
, String caarReference
, org.cocktail.kava.server.metier.EOArticle article			) {
    EOCatalogueArticle eo = (EOCatalogueArticle) createAndInsertInstance(editingContext, _EOCatalogueArticle.ENTITY_NAME);    
		eo.setCaarPrixHt(caarPrixHt);
		eo.setCaarPrixTtc(caarPrixTtc);
		eo.setCaarReference(caarReference);
    eo.setArticleRelationship(article);
    return eo;
  }

  
	  public EOCatalogueArticle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCatalogueArticle)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCatalogueArticle creerInstance(EOEditingContext editingContext) {
	  		EOCatalogueArticle object = (EOCatalogueArticle)createAndInsertInstance(editingContext, _EOCatalogueArticle.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOCatalogueArticle localInstanceIn(EOEditingContext editingContext, EOCatalogueArticle eo) {
    EOCatalogueArticle localInstance = (eo == null) ? null : (EOCatalogueArticle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCatalogueArticle#localInstanceIn a la place.
   */
	public static EOCatalogueArticle localInstanceOf(EOEditingContext editingContext, EOCatalogueArticle eo) {
		return EOCatalogueArticle.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCatalogueArticle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCatalogueArticle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCatalogueArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCatalogueArticle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCatalogueArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCatalogueArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCatalogueArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCatalogueArticle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCatalogueArticle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCatalogueArticle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCatalogueArticle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCatalogueArticle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
