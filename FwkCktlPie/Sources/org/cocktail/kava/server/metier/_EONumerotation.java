/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONumerotation.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EONumerotation extends  FwkPieRecord {
	public static final String ENTITY_NAME = "Numerotation";
	public static final String ENTITY_TABLE_NAME = "JEFY_RECETTE.NUMEROTATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "numId";

	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String NUM_ID_KEY = "numId";
	public static final String NUM_NUMERO_KEY = "numNumero";
	public static final String TNU_ID_KEY = "tnuId";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String NUM_ID_COLKEY = "NUM_ID";
	public static final String NUM_NUMERO_COLKEY = "NUM_NUMERO";
	public static final String TNU_ID_COLKEY = "TNU_ID";



	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_KEY = "toGestion";
	public static final String TO_TYPE_NUMEROTATION_KEY = "toTypeNumerotation";



	// Accessors methods
  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer numId() {
    return (Integer) storedValueForKey(NUM_ID_KEY);
  }

  public void setNumId(Integer value) {
    takeStoredValueForKey(value, NUM_ID_KEY);
  }

  public Integer numNumero() {
    return (Integer) storedValueForKey(NUM_NUMERO_KEY);
  }

  public void setNumNumero(Integer value) {
    takeStoredValueForKey(value, NUM_NUMERO_KEY);
  }

  public Integer tnuId() {
    return (Integer) storedValueForKey(TNU_ID_KEY);
  }

  public void setTnuId(Integer value) {
    takeStoredValueForKey(value, TNU_ID_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOExercice toExercice() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }
  
	public void setToExercice(org.cocktail.fwkcktldroitsutils.common.metier.EOExercice value) {
		takeStoredValueForKey(value,TO_EXERCICE_KEY);
	}


  public void setToExerciceRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.server.metier.EOGestion toGestion() {
    return (org.cocktail.fwkcktldepense.server.metier.EOGestion)storedValueForKey(TO_GESTION_KEY);
  }
  
	public void setToGestion(org.cocktail.fwkcktldepense.server.metier.EOGestion value) {
		takeStoredValueForKey(value,TO_GESTION_KEY);
	}


  public void setToGestionRelationship(org.cocktail.fwkcktldepense.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.server.metier.EOGestion oldValue = toGestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GESTION_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypeNumerotation toTypeNumerotation() {
    return (org.cocktail.kava.server.metier.EOTypeNumerotation)storedValueForKey(TO_TYPE_NUMEROTATION_KEY);
  }
  
	public void setToTypeNumerotation(org.cocktail.kava.server.metier.EOTypeNumerotation value) {
		takeStoredValueForKey(value,TO_TYPE_NUMEROTATION_KEY);
	}


  public void setToTypeNumerotationRelationship(org.cocktail.kava.server.metier.EOTypeNumerotation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypeNumerotation oldValue = toTypeNumerotation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_NUMEROTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_NUMEROTATION_KEY);
    }
  }
  

/**
 * Créer une instance de EONumerotation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONumerotation createEONumerotation(EOEditingContext editingContext, Integer numId
, Integer numNumero
, Integer tnuId
, org.cocktail.kava.server.metier.EOTypeNumerotation toTypeNumerotation			) {
    EONumerotation eo = (EONumerotation) createAndInsertInstance(editingContext, _EONumerotation.ENTITY_NAME);    
		eo.setNumId(numId);
		eo.setNumNumero(numNumero);
		eo.setTnuId(tnuId);
    eo.setToTypeNumerotationRelationship(toTypeNumerotation);
    return eo;
  }

  
	  public EONumerotation localInstanceIn(EOEditingContext editingContext) {
	  		return (EONumerotation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONumerotation creerInstance(EOEditingContext editingContext) {
	  		EONumerotation object = (EONumerotation)createAndInsertInstance(editingContext, _EONumerotation.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EONumerotation localInstanceIn(EOEditingContext editingContext, EONumerotation eo) {
    EONumerotation localInstance = (eo == null) ? null : (EONumerotation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONumerotation#localInstanceIn a la place.
   */
	public static EONumerotation localInstanceOf(EOEditingContext editingContext, EONumerotation eo) {
		return EONumerotation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONumerotation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONumerotation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONumerotation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONumerotation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONumerotation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONumerotation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONumerotation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONumerotation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONumerotation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONumerotation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONumerotation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONumerotation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
