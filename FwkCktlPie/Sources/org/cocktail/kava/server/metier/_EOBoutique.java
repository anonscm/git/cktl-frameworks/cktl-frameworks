/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBoutique.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBoutique extends  FwkPieRecord {
	public static final String ENTITY_NAME = "Boutique";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.boutique";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "boutiqueId";

	public static final String BOUTIQUE_ARTICLE_EXCLUSIF_KEY = "boutiqueArticleExclusif";
	public static final String BOUTIQUE_ARTICLE_UNIQUE_KEY = "boutiqueArticleUnique";
	public static final String BOUTIQUE_CODE_KEY = "boutiqueCode";
	public static final String BOUTIQUE_DESCRIPTION_EN_KEY = "boutiqueDescriptionEn";
	public static final String BOUTIQUE_DESCRIPTION_FR_KEY = "boutiqueDescriptionFr";
	public static final String BOUTIQUE_EMAIL_CONTACT_KEY = "boutiqueEmailContact";
	public static final String BOUTIQUE_ENTETE_ADRESSE_KEY = "boutiqueEnteteAdresse";
	public static final String BOUTIQUE_LIBELLE_EN_KEY = "boutiqueLibelleEn";
	public static final String BOUTIQUE_LIBELLE_FR_KEY = "boutiqueLibelleFr";
	public static final String BOUTIQUE_OPTION_UNIQUE_KEY = "boutiqueOptionUnique";
	public static final String BOUTIQUE_PAIEMENT_WEB_KEY = "boutiquePaiementWeb";
	public static final String BOUTIQUE_URL_LOGOUT_KEY = "boutiqueUrlLogout";
	public static final String BOUTIQUE_VAL_CLIENT_KEY = "boutiqueValClient";
	public static final String BOUTIQUE_VAL_CLOTURE_KEY = "boutiqueValCloture";
	public static final String BOUTIQUE_VAL_PRESTATAIRE_KEY = "boutiqueValPrestataire";
	public static final String DELAIS_PAIEMENT_JOURS_KEY = "delaisPaiementJours";
	public static final String LIMITATION_PREST_PAYEES_KEY = "limitationPrestPayees";
	public static final String NOMBRE_PRESTATIONS_KEY = "nombrePrestations";

// Attributs non visibles
	public static final String BT_CODE_KEY = "btCode";
	public static final String NO_INDIVIDU_RESP_KEY = "noIndividuResp";
	public static final String UTL_ORDRE_RESP_FONC_KEY = "utlOrdreRespFonc";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String BOUTIQUE_ID_KEY = "boutiqueId";

//Colonnes dans la base de donnees
	public static final String BOUTIQUE_ARTICLE_EXCLUSIF_COLKEY = "BOUTIQUE_ARTICLE_EXCLUSIF";
	public static final String BOUTIQUE_ARTICLE_UNIQUE_COLKEY = "BOUTIQUE_ARTICLE_UNIQUE";
	public static final String BOUTIQUE_CODE_COLKEY = "BOUTIQUE_CODE";
	public static final String BOUTIQUE_DESCRIPTION_EN_COLKEY = "BOUTIQUE_DESCRIPTION_EN";
	public static final String BOUTIQUE_DESCRIPTION_FR_COLKEY = "BOUTIQUE_DESCRIPTION_FR";
	public static final String BOUTIQUE_EMAIL_CONTACT_COLKEY = "BOUTIQUE_EMAIL_CONTACT";
	public static final String BOUTIQUE_ENTETE_ADRESSE_COLKEY = "BOUTIQUE_ENTETE_ADRESSE";
	public static final String BOUTIQUE_LIBELLE_EN_COLKEY = "BOUTIQUE_LIBELLE_EN";
	public static final String BOUTIQUE_LIBELLE_FR_COLKEY = "BOUTIQUE_LIBELLE_FR";
	public static final String BOUTIQUE_OPTION_UNIQUE_COLKEY = "BOUTIQUE_OPTION_UNIQUE";
	public static final String BOUTIQUE_PAIEMENT_WEB_COLKEY = "BOUTIQUE_PAIEMENT_WEB";
	public static final String BOUTIQUE_URL_LOGOUT_COLKEY = "BOUTIQUE_URL_LOGOUT";
	public static final String BOUTIQUE_VAL_CLIENT_COLKEY = "BOUTIQUE_VAL_CLIENT";
	public static final String BOUTIQUE_VAL_CLOTURE_COLKEY = "BOUTIQUE_VAL_CLOTURE";
	public static final String BOUTIQUE_VAL_PRESTATAIRE_COLKEY = "BOUTIQUE_VAL_PRESTATAIRE";
	public static final String DELAIS_PAIEMENT_JOURS_COLKEY = "DELAIS_PAIEMENT_JOURS";
	public static final String LIMITATION_PREST_PAYEES_COLKEY = "LIMITATION_PREST_PAYEES";
	public static final String NOMBRE_PRESTATIONS_COLKEY = "NOMBRE_PRESTATIONS";

	public static final String BT_CODE_COLKEY = "BT_CODE";
	public static final String NO_INDIVIDU_RESP_COLKEY = "NO_INDIVIDU_RESP";
	public static final String UTL_ORDRE_RESP_FONC_COLKEY = "UTL_ORDRE_RESP_FONC";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String BOUTIQUE_ID_COLKEY = "BOUTIQUE_ID";


	// Relationships
	public static final String BOUTIQUE_CLIENTS_KEY = "boutiqueClients";
	public static final String BOUTIQUE_INSCRIPTIONS_KEY = "boutiqueInscriptions";
	public static final String BOUTIQUE_TYPE_KEY = "boutiqueType";
	public static final String INDIVIDU_RESPONSABLE_KEY = "individuResponsable";
	public static final String JEFY_ADMIN_UTILISATEUR_KEY = "jefyAdminUtilisateur";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String V_EMAIL_RESPONSABLE_CATALOGUES_KEY = "vEmailResponsableCatalogues";



	// Accessors methods
  public String boutiqueArticleExclusif() {
    return (String) storedValueForKey(BOUTIQUE_ARTICLE_EXCLUSIF_KEY);
  }

  public void setBoutiqueArticleExclusif(String value) {
    takeStoredValueForKey(value, BOUTIQUE_ARTICLE_EXCLUSIF_KEY);
  }

  public String boutiqueArticleUnique() {
    return (String) storedValueForKey(BOUTIQUE_ARTICLE_UNIQUE_KEY);
  }

  public void setBoutiqueArticleUnique(String value) {
    takeStoredValueForKey(value, BOUTIQUE_ARTICLE_UNIQUE_KEY);
  }

  public String boutiqueCode() {
    return (String) storedValueForKey(BOUTIQUE_CODE_KEY);
  }

  public void setBoutiqueCode(String value) {
    takeStoredValueForKey(value, BOUTIQUE_CODE_KEY);
  }

  public String boutiqueDescriptionEn() {
    return (String) storedValueForKey(BOUTIQUE_DESCRIPTION_EN_KEY);
  }

  public void setBoutiqueDescriptionEn(String value) {
    takeStoredValueForKey(value, BOUTIQUE_DESCRIPTION_EN_KEY);
  }

  public String boutiqueDescriptionFr() {
    return (String) storedValueForKey(BOUTIQUE_DESCRIPTION_FR_KEY);
  }

  public void setBoutiqueDescriptionFr(String value) {
    takeStoredValueForKey(value, BOUTIQUE_DESCRIPTION_FR_KEY);
  }

  public String boutiqueEmailContact() {
    return (String) storedValueForKey(BOUTIQUE_EMAIL_CONTACT_KEY);
  }

  public void setBoutiqueEmailContact(String value) {
    takeStoredValueForKey(value, BOUTIQUE_EMAIL_CONTACT_KEY);
  }

  public String boutiqueEnteteAdresse() {
    return (String) storedValueForKey(BOUTIQUE_ENTETE_ADRESSE_KEY);
  }

  public void setBoutiqueEnteteAdresse(String value) {
    takeStoredValueForKey(value, BOUTIQUE_ENTETE_ADRESSE_KEY);
  }

  public String boutiqueLibelleEn() {
    return (String) storedValueForKey(BOUTIQUE_LIBELLE_EN_KEY);
  }

  public void setBoutiqueLibelleEn(String value) {
    takeStoredValueForKey(value, BOUTIQUE_LIBELLE_EN_KEY);
  }

  public String boutiqueLibelleFr() {
    return (String) storedValueForKey(BOUTIQUE_LIBELLE_FR_KEY);
  }

  public void setBoutiqueLibelleFr(String value) {
    takeStoredValueForKey(value, BOUTIQUE_LIBELLE_FR_KEY);
  }

  public String boutiqueOptionUnique() {
    return (String) storedValueForKey(BOUTIQUE_OPTION_UNIQUE_KEY);
  }

  public void setBoutiqueOptionUnique(String value) {
    takeStoredValueForKey(value, BOUTIQUE_OPTION_UNIQUE_KEY);
  }

  public String boutiquePaiementWeb() {
    return (String) storedValueForKey(BOUTIQUE_PAIEMENT_WEB_KEY);
  }

  public void setBoutiquePaiementWeb(String value) {
    takeStoredValueForKey(value, BOUTIQUE_PAIEMENT_WEB_KEY);
  }

  public String boutiqueUrlLogout() {
    return (String) storedValueForKey(BOUTIQUE_URL_LOGOUT_KEY);
  }

  public void setBoutiqueUrlLogout(String value) {
    takeStoredValueForKey(value, BOUTIQUE_URL_LOGOUT_KEY);
  }

  public String boutiqueValClient() {
    return (String) storedValueForKey(BOUTIQUE_VAL_CLIENT_KEY);
  }

  public void setBoutiqueValClient(String value) {
    takeStoredValueForKey(value, BOUTIQUE_VAL_CLIENT_KEY);
  }

  public String boutiqueValCloture() {
    return (String) storedValueForKey(BOUTIQUE_VAL_CLOTURE_KEY);
  }

  public void setBoutiqueValCloture(String value) {
    takeStoredValueForKey(value, BOUTIQUE_VAL_CLOTURE_KEY);
  }

  public String boutiqueValPrestataire() {
    return (String) storedValueForKey(BOUTIQUE_VAL_PRESTATAIRE_KEY);
  }

  public void setBoutiqueValPrestataire(String value) {
    takeStoredValueForKey(value, BOUTIQUE_VAL_PRESTATAIRE_KEY);
  }

  public Integer delaisPaiementJours() {
    return (Integer) storedValueForKey(DELAIS_PAIEMENT_JOURS_KEY);
  }

  public void setDelaisPaiementJours(Integer value) {
    takeStoredValueForKey(value, DELAIS_PAIEMENT_JOURS_KEY);
  }

  public String limitationPrestPayees() {
    return (String) storedValueForKey(LIMITATION_PREST_PAYEES_KEY);
  }

  public void setLimitationPrestPayees(String value) {
    takeStoredValueForKey(value, LIMITATION_PREST_PAYEES_KEY);
  }

  public Integer nombrePrestations() {
    return (Integer) storedValueForKey(NOMBRE_PRESTATIONS_KEY);
  }

  public void setNombrePrestations(Integer value) {
    takeStoredValueForKey(value, NOMBRE_PRESTATIONS_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutiqueType boutiqueType() {
    return (org.cocktail.kava.server.metier.EOBoutiqueType)storedValueForKey(BOUTIQUE_TYPE_KEY);
  }
  
	public void setBoutiqueType(org.cocktail.kava.server.metier.EOBoutiqueType value) {
		takeStoredValueForKey(value,BOUTIQUE_TYPE_KEY);
	}


  public void setBoutiqueTypeRelationship(org.cocktail.kava.server.metier.EOBoutiqueType value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOBoutiqueType oldValue = boutiqueType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BOUTIQUE_TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BOUTIQUE_TYPE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOIndividuUlr individuResponsable() {
    return (org.cocktail.kava.server.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_RESPONSABLE_KEY);
  }
  
	public void setIndividuResponsable(org.cocktail.kava.server.metier.EOIndividuUlr value) {
		takeStoredValueForKey(value,INDIVIDU_RESPONSABLE_KEY);
	}


  public void setIndividuResponsableRelationship(org.cocktail.kava.server.metier.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOIndividuUlr oldValue = individuResponsable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_RESPONSABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_RESPONSABLE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur jefyAdminUtilisateur() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(JEFY_ADMIN_UTILISATEUR_KEY);
  }
  
	public void setJefyAdminUtilisateur(org.cocktail.kava.server.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,JEFY_ADMIN_UTILISATEUR_KEY);
	}


  public void setJefyAdminUtilisateurRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = jefyAdminUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, JEFY_ADMIN_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, JEFY_ADMIN_UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.application.serveur.eof.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray boutiqueClients() {
    return (NSArray)storedValueForKey(BOUTIQUE_CLIENTS_KEY);
  }
  
  //ICI
  public void setBoutiqueClients(NSArray values) {
	  takeStoredValueForKey(values,BOUTIQUE_CLIENTS_KEY);
  }
  

  public NSArray boutiqueClients(EOQualifier qualifier) {
    return boutiqueClients(qualifier, null, false);
  }

  public NSArray boutiqueClients(EOQualifier qualifier, boolean fetch) {
    return boutiqueClients(qualifier, null, fetch);
  }

  public NSArray boutiqueClients(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOBoutiqueClient.BOUTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOBoutiqueClient.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = boutiqueClients();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToBoutiqueClientsRelationship(org.cocktail.kava.server.metier.EOBoutiqueClient object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
  }

  public void removeFromBoutiqueClientsRelationship(org.cocktail.kava.server.metier.EOBoutiqueClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutiqueClient createBoutiqueClientsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BoutiqueClient");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BOUTIQUE_CLIENTS_KEY);
    return (org.cocktail.kava.server.metier.EOBoutiqueClient) eo;
  }

  public void deleteBoutiqueClientsRelationship(org.cocktail.kava.server.metier.EOBoutiqueClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBoutiqueClientsRelationships() {
    Enumeration objects = boutiqueClients().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBoutiqueClientsRelationship((org.cocktail.kava.server.metier.EOBoutiqueClient)objects.nextElement());
    }
  }

  public NSArray boutiqueInscriptions() {
    return (NSArray)storedValueForKey(BOUTIQUE_INSCRIPTIONS_KEY);
  }
  
  //ICI
  public void setBoutiqueInscriptions(NSArray values) {
	  takeStoredValueForKey(values,BOUTIQUE_INSCRIPTIONS_KEY);
  }
  

  public NSArray boutiqueInscriptions(EOQualifier qualifier) {
    return boutiqueInscriptions(qualifier, null, false);
  }

  public NSArray boutiqueInscriptions(EOQualifier qualifier, boolean fetch) {
    return boutiqueInscriptions(qualifier, null, fetch);
  }

  public NSArray boutiqueInscriptions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOBoutiqueInscription.BOUTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOBoutiqueInscription.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = boutiqueInscriptions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToBoutiqueInscriptionsRelationship(org.cocktail.kava.server.metier.EOBoutiqueInscription object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BOUTIQUE_INSCRIPTIONS_KEY);
  }

  public void removeFromBoutiqueInscriptionsRelationship(org.cocktail.kava.server.metier.EOBoutiqueInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_INSCRIPTIONS_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutiqueInscription createBoutiqueInscriptionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BoutiqueInscription");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BOUTIQUE_INSCRIPTIONS_KEY);
    return (org.cocktail.kava.server.metier.EOBoutiqueInscription) eo;
  }

  public void deleteBoutiqueInscriptionsRelationship(org.cocktail.kava.server.metier.EOBoutiqueInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_INSCRIPTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBoutiqueInscriptionsRelationships() {
    Enumeration objects = boutiqueInscriptions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBoutiqueInscriptionsRelationship((org.cocktail.kava.server.metier.EOBoutiqueInscription)objects.nextElement());
    }
  }

  public NSArray vEmailResponsableCatalogues() {
    return (NSArray)storedValueForKey(V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }
  
  //ICI
  public void setVEmailResponsableCatalogues(NSArray values) {
	  takeStoredValueForKey(values,V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }
  

  public NSArray vEmailResponsableCatalogues(EOQualifier qualifier) {
    return vEmailResponsableCatalogues(qualifier, null, false);
  }

  public NSArray vEmailResponsableCatalogues(EOQualifier qualifier, boolean fetch) {
    return vEmailResponsableCatalogues(qualifier, null, fetch);
  }

  public NSArray vEmailResponsableCatalogues(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue.BOUTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = vEmailResponsableCatalogues();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToVEmailResponsableCataloguesRelationship(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue object) {
    addObjectToBothSidesOfRelationshipWithKey(object, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }

  public void removeFromVEmailResponsableCataloguesRelationship(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }

  public org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue createVEmailResponsableCataloguesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VEmailResponsableCatalogue");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
    return (org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue) eo;
  }

  public void deleteVEmailResponsableCataloguesRelationship(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllVEmailResponsableCataloguesRelationships() {
    Enumeration objects = vEmailResponsableCatalogues().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteVEmailResponsableCataloguesRelationship((org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOBoutique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBoutique createEOBoutique(EOEditingContext editingContext, String boutiqueArticleExclusif
, String boutiqueArticleUnique
, String boutiqueEmailContact
, String boutiqueEnteteAdresse
, String boutiqueOptionUnique
, String boutiquePaiementWeb
, String boutiqueValClient
, String boutiqueValCloture
, String boutiqueValPrestataire
, org.cocktail.kava.server.metier.EOBoutiqueType boutiqueType, org.cocktail.kava.server.metier.EOIndividuUlr individuResponsable, org.cocktail.kava.server.metier.EOUtilisateur jefyAdminUtilisateur, org.cocktail.application.serveur.eof.EOTypeEtat typeEtat			) {
    EOBoutique eo = (EOBoutique) createAndInsertInstance(editingContext, _EOBoutique.ENTITY_NAME);    
		eo.setBoutiqueArticleExclusif(boutiqueArticleExclusif);
		eo.setBoutiqueArticleUnique(boutiqueArticleUnique);
		eo.setBoutiqueEmailContact(boutiqueEmailContact);
		eo.setBoutiqueEnteteAdresse(boutiqueEnteteAdresse);
		eo.setBoutiqueOptionUnique(boutiqueOptionUnique);
		eo.setBoutiquePaiementWeb(boutiquePaiementWeb);
		eo.setBoutiqueValClient(boutiqueValClient);
		eo.setBoutiqueValCloture(boutiqueValCloture);
		eo.setBoutiqueValPrestataire(boutiqueValPrestataire);
    eo.setBoutiqueTypeRelationship(boutiqueType);
    eo.setIndividuResponsableRelationship(individuResponsable);
    eo.setJefyAdminUtilisateurRelationship(jefyAdminUtilisateur);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOBoutique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBoutique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBoutique creerInstance(EOEditingContext editingContext) {
	  		EOBoutique object = (EOBoutique)createAndInsertInstance(editingContext, _EOBoutique.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBoutique localInstanceIn(EOEditingContext editingContext, EOBoutique eo) {
    EOBoutique localInstance = (eo == null) ? null : (EOBoutique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBoutique#localInstanceIn a la place.
   */
	public static EOBoutique localInstanceOf(EOEditingContext editingContext, EOBoutique eo) {
		return EOBoutique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBoutique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBoutique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBoutique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBoutique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBoutique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBoutique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBoutique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBoutique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBoutique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBoutique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBoutique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBoutique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
