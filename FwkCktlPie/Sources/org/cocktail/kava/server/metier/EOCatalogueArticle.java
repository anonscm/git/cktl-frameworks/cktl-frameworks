
// EOCatalogueArticle.java
// 

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.server.finder.FinderTypeEtat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCatalogueArticle extends _EOCatalogueArticle {
	
	private Number caarId;
	
	private static final EOSortOrdering SORT_LIBELLE = EOSortOrdering
			.sortOrderingWithKey(EOCatalogueArticle.ARTICLE_KEY + "."
					+ EOArticle.ART_LIBELLE_KEY, 
					EOSortOrdering.CompareAscending);

	public static NSArray sortedCatalogueArticlesByLibelle(NSArray catArticles) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(catArticles,
				new NSArray(SORT_LIBELLE));
	}
	
	
	
	/** permet d'archiver/annuler l'article */
	public void archiver(boolean saveChanges) {
		EOTypeEtat etatAnnule = FinderTypeEtat.typeEtatAnnule( editingContext() );
		setTypeEtat(etatAnnule);
		if(saveChanges) {
			editingContext().saveChanges();
		}
	}
	
	/* retourne la cle primaire */
	public Number getPkCaarId() {
		if(caarId==null) {
		NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(),this);
			if(dico!=null) {
				caarId = (Number)dico.objectForKey(EOCatalogueArticle.CAAR_ID_KEY);
			}
		}
		return caarId;
	}
	
	
	
	public EOCatalogueArticle() {
		super();
	}

	
	
	public EOArticleLocalise articleLocaliseEn() {
		return  articleLocaliseForLangue("en");
	}
	
	// gestion des libelles localises
	
	public String artLibelleLocalise(String cLangue) {
		String libelle = null;
		if(cLangue==null || cLangue.equals("fr")) {
			libelle = article().artLibelle();
		}
		else {
			EOArticleLocalise localise = articleLocaliseForLangue(cLangue);
			if(localise!=null) {
				libelle = localise.artLibelle();
			}
			else {
				libelle = article().artLibelle();
			}
		}
		
		return libelle;
	}
	
	
	public EOArticleLocalise articleLocaliseForLangue(String cLangue) {
		NSArray<EOArticleLocalise> localises = articlesLocalises();
		EOQualifier qualLangue = EOQualifier.qualifierWithQualifierFormat(EOArticleLocalise.C_LANGUE_KEY+"='"+cLangue+"'",null);
		NSArray<EOArticleLocalise> filtered = EOQualifier.filteredArrayWithQualifier(localises, qualLangue);
		if(filtered.count()>0) {
			return (EOArticleLocalise)filtered.lastObject();
		}
		else {
			return null;
		}
	}
	
	
	public String artReferenceLocalise(String cLangue) {
		String value = null;
		if(cLangue==null || cLangue.equals("fr")) {
			value = caarReference();
		}
		else {
			EOArticleLocalise localise = articleLocaliseForLangue(cLangue);
			if(localise!=null) {
				value = localise.artReference();
			}
			else {
				value =  caarReference();
			}
		}
		
		return value;
	}
	
	
	
	/** Retourne les prestationLigne issues de prestations valides */
	@SuppressWarnings("unchecked")
	public NSArray<EOPrestationLigne> getPrestationLignesDansPrestaValide() {
		
		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
		EOEditingContext ec = editingContext();
		EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(ec);
		
		array.add(
				new EOKeyValueQualifier(EOPrestationLigne.CATALOGUE_ARTICLE_KEY, FwkPieRecord.SEL_EQUAL, this)
		);
		array.add(
				new EOKeyValueQualifier(EOPrestationLigne.PRESTATION_KEY+"."+EOPrestation.TYPE_ETAT_KEY, FwkPieRecord.SEL_EQUAL, etatValide)
		);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOPrestationLigne.ENTITY_NAME, new EOAndQualifier(array), null, true, true, null);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	/** Retourne les prestation_ligne issues de prestations dont au moins une facture est payee (normalement la seule en mode colloque) */
	@SuppressWarnings("unchecked")
	public NSArray<EOPrestationLigne> getPrestationLignesDansPrestaPayees() {
		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
		EOEditingContext ec = editingContext();
		EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(ec);
		
		array.add(
				new EOKeyValueQualifier(EOPrestationLigne.CATALOGUE_ARTICLE_KEY, FwkPieRecord.SEL_EQUAL, this)
		);
		array.add(
				new EOKeyValueQualifier(EOPrestationLigne.PRESTATION_KEY+"."+EOPrestation.TYPE_ETAT_KEY, FwkPieRecord.SEL_EQUAL, etatValide)
		);
		
		array.add( new EOKeyValueQualifier(
				EOPrestationLigne.PRESTATION_KEY+"."+EOPrestation.FACTURE_PAPIERS_KEY+"."+EOFacturePapier.FAP_DATE_REGLEMENT_KEY, 
				FwkPieRecord.SEL_NEQUAL, 
				NSKeyValueCoding.NullValue)
		);
		
		array.add( new EOKeyValueQualifier(
				EOPrestationLigne.PRESTATION_KEY+"."+EOPrestation.FACTURE_PAPIERS_KEY+"."+EOFacturePapier.FAP_REFERENCE_REGLEMENT_KEY, 
				FwkPieRecord.SEL_NEQUAL, 
				NSKeyValueCoding.NullValue)
		);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOPrestationLigne.ENTITY_NAME, new EOAndQualifier(array), null, true, true, null);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		
		if(article().typeArticle()==null) {
			throw new NSValidation.ValidationException("Il faut indiquer le type d'article/option.");
		}
		
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave()
			throws NSValidation.ValidationException {

	}

	/**
     * Initialise la creation d'un catalogueArticle
     * 
     * @param ec
     * @param catalogue
     * @param article
     */
    public static EOCatalogueArticle creerNouveauCatalogueArticle(
                    EOEditingContext ec, EOCatalogue catalogue, EOArticle article) {
            if (ec == null) {
                    throw new NSValidation.ValidationException(
                                    "Erreur de creation d'une nouvelle instance de CatalogueArticle. EOEditingContext absent.");
            }
            if (catalogue == null) {
                    throw new NSValidation.ValidationException(
                                    "Erreur de creation d'une nouvelle instance de CatalogueArticle. EOCatalogue absent.");
            }
            if (article == null) {
                    throw new NSValidation.ValidationException(
                                    "Erreur de creation d'une nouvelle instance de CatalogueArticle. EOArticle absent.");
            }
            EOCatalogueArticle catArticle = EOCatalogueArticle.creerInstance(ec);
            catArticle.setArticleRelationship(article);
            catArticle.setCatalogueRelationship(catalogue);
            catArticle.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
            // Imputation dépense (si prestation interne) factorisée par défaut pour
            // tous les articles du catalogues
            if (catArticle.catalogue().cataloguePrestation().planComptableDepense() != null) {
                    catArticle
                                    .article()
                                    .articlePrestation()
                                    .setPlanComptableDepenseRelationship(
                                                    catArticle.catalogue().cataloguePrestation()
                                                                    .planComptableDepense());
            }
            // Imputation recette factorisée par défaut pour tous les articles du
            // catalogues
            if (catArticle.catalogue().cataloguePrestation().planComptableRecette()!=null) {
                    catArticle
                                    .article()
                                    .articlePrestation()
                                    .setPlanComptableRecetteRelationship(
                                                    catArticle.catalogue().cataloguePrestation()
                                                                    .planComptableRecette());
            }
            catArticle.setCaarPrixHt(new BigDecimal(0.0));
            catArticle.setCaarPrixTtc(new BigDecimal(0.0));
            catArticle.setCaarReference("");
            return catArticle;
    
    }
}
