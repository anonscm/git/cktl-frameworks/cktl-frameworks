/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPiDepRec.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPiDepRec extends  FwkPieRecord {
	public static final String ENTITY_NAME = "PiDepRec";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.pi_dep_rec";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdrId";

	public static final String D_CREATION_KEY = "dCreation";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String PDR_ID_KEY = "pdrId";
	public static final String PEF_ID_KEY = "pefId";
	public static final String REC_ID_KEY = "recId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String PDR_ID_COLKEY = "PDR_ID";
	public static final String PEF_ID_COLKEY = "PEF_ID";
	public static final String REC_ID_COLKEY = "REC_ID";


	// Relationships
	public static final String DEPENSE_BUDGET_KEY = "depenseBudget";
	public static final String PI_ENG_FAC_KEY = "piEngFac";
	public static final String RECETTE_KEY = "recette";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public org.cocktail.kava.server.metier.EODepenseBudget depenseBudget() {
    return (org.cocktail.kava.server.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_KEY);
  }
  
	public void setDepenseBudget(org.cocktail.kava.server.metier.EODepenseBudget value) {
		takeStoredValueForKey(value,DEPENSE_BUDGET_KEY);
	}


  public void setDepenseBudgetRelationship(org.cocktail.kava.server.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EODepenseBudget oldValue = depenseBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPiEngFac piEngFac() {
    return (org.cocktail.kava.server.metier.EOPiEngFac)storedValueForKey(PI_ENG_FAC_KEY);
  }
  
	public void setPiEngFac(org.cocktail.kava.server.metier.EOPiEngFac value) {
		takeStoredValueForKey(value,PI_ENG_FAC_KEY);
	}


  public void setPiEngFacRelationship(org.cocktail.kava.server.metier.EOPiEngFac value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPiEngFac oldValue = piEngFac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PI_ENG_FAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PI_ENG_FAC_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EORecette recette() {
    return (org.cocktail.kava.server.metier.EORecette)storedValueForKey(RECETTE_KEY);
  }
  
	public void setRecette(org.cocktail.kava.server.metier.EORecette value) {
		takeStoredValueForKey(value,RECETTE_KEY);
	}


  public void setRecetteRelationship(org.cocktail.kava.server.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EORecette oldValue = recette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPiDepRec avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPiDepRec createEOPiDepRec(EOEditingContext editingContext, NSTimestamp dCreation
			) {
    EOPiDepRec eo = (EOPiDepRec) createAndInsertInstance(editingContext, _EOPiDepRec.ENTITY_NAME);    
		eo.setDCreation(dCreation);
    return eo;
  }

  
	  public EOPiDepRec localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPiDepRec)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPiDepRec creerInstance(EOEditingContext editingContext) {
	  		EOPiDepRec object = (EOPiDepRec)createAndInsertInstance(editingContext, _EOPiDepRec.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPiDepRec localInstanceIn(EOEditingContext editingContext, EOPiDepRec eo) {
    EOPiDepRec localInstance = (eo == null) ? null : (EOPiDepRec)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPiDepRec#localInstanceIn a la place.
   */
	public static EOPiDepRec localInstanceOf(EOEditingContext editingContext, EOPiDepRec eo) {
		return EOPiDepRec.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPiDepRec fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPiDepRec fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPiDepRec eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPiDepRec)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPiDepRec fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPiDepRec fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPiDepRec eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPiDepRec)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPiDepRec fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPiDepRec eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPiDepRec ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPiDepRec fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
