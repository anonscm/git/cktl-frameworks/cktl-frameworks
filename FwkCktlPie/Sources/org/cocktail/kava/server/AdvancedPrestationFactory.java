package org.cocktail.kava.server;

import org.cocktail.application.serveur.eof.EODevise;
import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.kava.server.factory.FactoryPrestationLigne;
import org.cocktail.kava.server.finder.FinderDevise;
import org.cocktail.kava.server.finder.FinderFacturePapier;
import org.cocktail.kava.server.finder.FinderModeRecouvrement;
import org.cocktail.kava.server.finder.FinderParametreBudget;
import org.cocktail.kava.server.finder.FinderParametreJefyAdmin;
import org.cocktail.kava.server.finder.FinderTauxProrata;
import org.cocktail.kava.server.finder.FinderTypeCreditRec;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.finder.FinderTypePublic;
import org.cocktail.kava.server.metier.EOBoutique;
import org.cocktail.kava.server.metier.EOBoutiqueInscription;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOIndividuUlr;
import org.cocktail.kava.server.metier.EOModeRecouvrement;
import org.cocktail.kava.server.metier.EOPersonne;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EOTypePublic;
import org.cocktail.kava.server.metier.EOUtilisateur;
import org.cocktail.kava.server.procedures.ApiPrestation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;



public class AdvancedPrestationFactory {
	
	
	// Code du type credit recette par defaut
	public static final String DEFAULT_TCD_CODE_RECETTE = "02";
	
	private EOEditingContext editingContext;
	
	private EOTypeEtat etatAnnule = null;
	
	public AdvancedPrestationFactory(EOEditingContext edc) {
		editingContext = edc;
		etatAnnule = FinderTypeEtat.typeEtatAnnule(editingContext);
	}
	
	
	/* version simplifiée pour la création de presta externes depuis le web avec saveChanges auto
	 * */
	public EOPrestation creerPrestationExterne( 
										  EOBoutique boutique,	
										  NSArray<ClientCatalogueArticle> listeArticles,
			  							  EOFournisUlr fournisClient,
			  							  EOPersonne personneClient,
			  							  EOIndividuUlr contactFournis,
			  							  EOUtilisateur pieUtilisateur,
			  							  String libellePrestation,
			  							  String commentaireClient
			  							) throws Exception {
		
		EOTypePublic typePublic  = FinderTypePublic.typePublicExterneIntranet(editingContext);
		return  createPrestationExterne( 
										boutique,
										null, // catalogue
										listeArticles,
										fournisClient,
										personneClient,
										contactFournis,
										null, // modeRecouvrement
										pieUtilisateur,
										libellePrestation,
										commentaireClient,
										typePublic,
										null, // typeCreditRecette
										null, //exercice
										true
										);
	}
	
	/* version simplifiée pour la création de presta externes depuis le web avec saveChanges auto
	 * */
	public EOPrestation creerPrestationExterne( 
										  EOBoutique boutique,	
										  NSArray<ClientCatalogueArticle> listeArticles,
			  							  EOFournisUlr fournisClient,
			  							  EOPersonne personneClient,
			  							  EOIndividuUlr contactFournis,
			  							  EOUtilisateur pieUtilisateur,
			  							  String libellePrestation,
			  							  String commentaireClient,
			  							  boolean saveChanges
			  							) throws Exception {
		
		EOTypePublic typePublic  = FinderTypePublic.typePublicExterneIntranet(editingContext);
		return  createPrestationExterne( 
										boutique,
										null, //catalogue
										listeArticles,
										fournisClient,
										personneClient,
										contactFournis,
										null, // modeRecouvrement
										pieUtilisateur,
										libellePrestation,
										commentaireClient,
										typePublic,
										null, // typeCreditRecette
										null, //exercice
										saveChanges
										);
	}
	
	
	// Version pour DIL, avec passage de typeCredit en plus
	
	public EOPrestation creerPrestationExterne( 
			  EOBoutique boutique,	
			  EOCatalogue catalogue,
			  NSArray<ClientCatalogueArticle> listeArticles,
			  EOFournisUlr fournisClient,
			  EOPersonne personneClient,
			  EOIndividuUlr contactFournis,
			  EOUtilisateur pieUtilisateur,
			  EOTypeCredit typeCreditRec,
			  String libellePrestation,
			  String commentaireClient,
			  boolean saveChanges
			) throws Exception {

EOTypePublic typePublic  = FinderTypePublic.typePublicExterneIntranet(editingContext);
return  createPrestationExterne( 
			boutique,
			catalogue,
			listeArticles,
			fournisClient,
			personneClient,
			contactFournis,
			null, // modeRecouvrement
			pieUtilisateur,
			libellePrestation,
			commentaireClient,
			typePublic,
			typeCreditRec, // typeCreditRecette
			null, //exercice
			saveChanges
			);
}
	
	
	/* effectue d'office un saveChanges pour garder la compat. avec PieWeb
	 * */
	public EOPrestation createPrestationExterne( EOBoutique boutique,
												 NSArray<ClientCatalogueArticle> listeArticles,
												 EOFournisUlr fournisClient,
												 EOPersonne personneClient,
												 EOIndividuUlr contactFournis,
												 EOModeRecouvrement modeRecouvrement,
												 EOUtilisateur pieUtilisateur,
												 String libellePrestation,
												 String commentaireClient,
												 EOTypePublic typePublic,
												 EOTypeCredit typeCreditRecette,
												 EOExercice exercice
										  		) throws Exception {
					return createPrestationExterne( 			 boutique,
																 null, // catalogue
																 listeArticles,
																 fournisClient,
																 personneClient,
																 contactFournis,
																 modeRecouvrement,
																 pieUtilisateur,
																 libellePrestation,
																 commentaireClient,
																 typePublic,
																 typeCreditRecette,
																 exercice,
																 true
														  		);
	}
	
	/* Permet de creer une prestation externe avec les parametres passes en arguments
	 * si  typeCreditRecette, typePublic, modeRecouvrement ou exercice sont passes a nul, les valeurs 
	 * par defauts seront employees pour ces arguments 
	 * */
	public EOPrestation createPrestationExterne( EOBoutique boutique,
												 EOCatalogue catalogue,
												 NSArray<ClientCatalogueArticle> listeArticles,
												 EOFournisUlr fournisClient,
												 EOPersonne personneClient,
												 EOIndividuUlr contactFournis,
												 EOModeRecouvrement modeRecouvrement,
												 EOUtilisateur pieUtilisateur,
												 String libellePrestation,
												 String commentaireClient,
												 EOTypePublic typePublic,
												 EOTypeCredit typeCreditRecette,
												 EOExercice exercice,
												 boolean saveChanges
										  		) throws Exception {
		
		EOPrestation prestation = new EOPrestation();
		
		editingContext.insertObject(prestation);
		
		if(catalogue!=null) {
			prestation.setCatalogueRelationship(catalogue);
		}
		
		NSTimestamp maintenant = new NSTimestamp();

		EOTypeCredit typeCredRecette = typeCreditRecette;
		if(typeCredRecette==null) {
			typeCredRecette = defaultTypeCreditRecette(editingContext);
			if(typeCredRecette==null) { throw new PieException("Type credit recette introuvable (code 02) !"); }
		}

		EOExercice exer = exercice;
		if(exer==null){
			exer = findCurrentExercice(editingContext);
			if(exer==null) { throw new PieException("Impossible de determiner l'exercice courant !"); }
		}
		
		EOModeRecouvrement modeRecouvre = modeRecouvrement;
		if(modeRecouvre==null) {
			modeRecouvre = FinderModeRecouvrement.modeRecouvrementPrestationExterne(editingContext, exer);
		}
		
		EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(editingContext);
		 
		
		Integer prestaNum = EOPrestation.generatePrestaNumero(exer, editingContext);
		
		prestation.setPrestLibelle( libellePrestation );
		System.out.println("setPrestCommentairePrest : "+commentaireClient);
		prestation.setPrestCommentairePrest( commentaireClient );
		prestation.setPrestDate( maintenant );
		prestation.setTypeCreditRec( typeCredRecette );
		prestation.setTypePublic( typePublic );
		prestation.setTypeEtat( etatValide );
		prestation.setExercice( exer );
		prestation.setPersonne( personneClient );
		prestation.setFournisUlr( fournisClient );
		prestation.setModeRecouvrement(modeRecouvre);
		prestation.setUtilisateur(pieUtilisateur);
		
		prestation.setIndividuUlr(contactFournis);
		
		
		/*
		if(personneClient.isPersonneMorale()) {
			prestation.setIndividuUlr( pieUtilisateur.personne().individuUlr() );
		}
		else {
			prestation.setIndividuUlr( personneClient.individuUlr() );
		}
		*/
		
		prestation.setPrestNumero( prestaNum );
		EOPrestationLigne prestaLigne = null;
		IPrestationElement elemCommande;
		EOCatalogueArticle catArticle;
		
		NSMutableArray<EOPrestationLigne> createdPrestaLignes = new NSMutableArray<EOPrestationLigne>();
		
		for(int i = 0; i < listeArticles.count(); i++) {
			
			elemCommande = (IPrestationElement)listeArticles.objectAtIndex(i);
			catArticle = elemCommande.catalogueArticle(); 
			
			
			// Pour le premier article on recupere les infos budgetaires propres a la prestation.
			if(i==0) {
				prestation.setPrestApplyTva("O");
				prestation.setFournisUlrPrest(catArticle.catalogue().fournisUlr());
				prestation.setOrgan( catArticle.catalogue().cataloguePrestation().organRecette() );
				prestation.setLolfNomenclatureRecette( catArticle.catalogue().cataloguePrestation().lolfNomenclatureRecette() );
				prestation.setTauxProrata(
						FinderTauxProrata.findDefault(
													editingContext, 
													catArticle.catalogue().cataloguePrestation().organRecette(),
													exercice )
						);
			}
			
			prestaLigne = new EOPrestationLigne();
			editingContext.insertObject(prestaLigne);
			
			prestaLigne.setPrligDescription(elemCommande.catalogueArticle().article().artLibelle());
			prestaLigne.setPrligDate( maintenant );
			prestaLigne.setPlanComptable(catArticle.article().articlePrestation().planComptableRecette());
			prestaLigne.setCatalogueArticle( catArticle );
			
			prestaLigne.setPrligArtHt( catArticle.caarPrixHt() );
			prestaLigne.setPrligArtTtcInitial( catArticle.caarPrixTtc() );
			prestaLigne.setPrligArtTtc( catArticle.caarPrixTtc() );
			prestaLigne.setTva(catArticle.tva());
			
			prestaLigne.setPrligReference(catArticle.caarReference());
			prestaLigne.setTypeArticle(catArticle.article().typeArticle());
			prestaLigne.setPrestation(prestation);
			
			// Pas de reference au catalogue, car presta multi-catalogue
			//prestation.setCatalogue(catArticle.catalogue());
			
			prestaLigne.setPrligQuantite( elemCommande.quantite() );
			prestaLigne.setPrligQuantiteReste( elemCommande.quantite() );
			prestaLigne.setPrligTotalHt( prestaLigne.prligTotalResteHt() );
			prestaLigne.setPrligTotalTtc( prestaLigne.prligTotalResteTtc() );
			
			// Gestion des options et remises :
			// TODO : verifier si les options ne sont pas en double
			NSArray<IPrestationElement> options = elemCommande.options();
			
			EOCatalogueArticle catalogueArtOption = null;
			EOPrestationLigne prestaLigneOption = null;
			for(IPrestationElement uneOption : options) {
				if(uneOption.quantite()!=null) {
					catalogueArtOption = uneOption.catalogueArticle();
					prestaLigneOption = FactoryPrestationLigne.newObject(editingContext, prestaLigne, catalogueArtOption);
					prestaLigneOption.setPrligQuantite( uneOption.quantite() );
					prestaLigneOption.setPrligQuantiteReste( uneOption.quantite() );
					
					prestaLigneOption.setPrligTotalHt( prestaLigneOption.prligTotalResteHt() );
					prestaLigneOption.setPrligTotalTtc( prestaLigneOption.prligTotalResteTtc() );
					
					prestaLigneOption.setPrligDescription(catalogueArtOption.article().artLibelle());
					prestaLigneOption.setPrligDate( maintenant );
					prestaLigneOption.setPlanComptable(catalogueArtOption.article().articlePrestation().planComptableRecette());
					prestaLigneOption.setPrligArtHt( catalogueArtOption.caarPrixHt() );
					prestaLigneOption.setPrligArtTtcInitial( catalogueArtOption.caarPrixTtc() );
					prestaLigneOption.setPrligArtTtc( catalogueArtOption.caarPrixTtc() );
					prestaLigneOption.setTva(catalogueArtOption.tva());
					prestaLigneOption.setPrligReference(catalogueArtOption.caarReference());
					prestaLigneOption.setTypeArticle(catalogueArtOption.article().typeArticle());
					prestaLigneOption.setPrestation(prestation);

					createdPrestaLignes.addObject(prestaLigneOption);
				}
			}
						
			createdPrestaLignes.addObject(prestaLigne);
		}
		
		prestation.setPrestationLignes(createdPrestaLignes);
		
		
		if(personneClient!=null && boutique!=null) {
			EOBoutiqueInscription boutiqueInsc = EOBoutiqueInscription.createEOBoutiqueInscription(editingContext, boutique.localInstanceIn(editingContext), personneClient.localInstanceIn(editingContext));
			boutiqueInsc.setPrestationRelationship(prestation);
		}
		
		if(saveChanges) {
			try {
				editingContext.saveChanges();
			} catch (java.lang.Throwable e) {
				e.printStackTrace();
				editingContext.revert();
				return null;
			}
		}
		return prestation;
	}
	
	
	/* permet de valider la prestation cote client */
	 public void validerPrestationClient(boolean saveChanges, EOPrestation prestation) throws Exception {
		
		if(prestation == null) { return; }
	
		if(prestation.prestDateValideClient() != null) { return; }
     
		EOTypeEtat etatAnnule = FinderTypeEtat.typeEtatAnnule(editingContext);
		 
		if( prestation.typeEtat().equals(etatAnnule) ) {
			throw new PieException("La prestation " + prestation.prestNumero() + " est archivée, impossible de la valider !");
		}
		 
		if( !prestation.isValidableClient(editingContext) ) {
			throw new PieException("Il manque des informations budgétaires côté client, impossible de valider la prestation " + prestation.prestNumero() + " !");
		}
		 
		prestation.setPrestDateValideClient( new NSTimestamp() );
		
		if(saveChanges) {
			editingContext.saveChanges();
		}
	 }
	
	 /* permet de valider la prestation cote prestataire */
	 public void validerPrestationPrestataire(boolean saveChanges, EOPrestation prestation) throws Exception {
        
		 if(prestation == null) { return; }
        
        if(prestation.prestDateValidePrest() != null) { return; }
        
        if( prestation.typeEtat().equals(etatAnnule) ) {
            throw new Exception("La prestation " + prestation.prestNumero() + " est archivée, impossible de la valider !");
        } 
        else {
        	validerPrestationClient(false, prestation);
            prestation.setPrestDateValidePrest( new NSTimestamp() );
            if(saveChanges) {
            	editingContext.saveChanges();
            }
        }
    } 

	public void cloturerPrestation(boolean saveChanges, EOPrestation prestation) throws Exception {
		
		if(prestation==null) { return; }
		
		if(prestation.prestDateCloture() !=null) { return; }
		
		if(prestation.typeEtat().equals(etatAnnule)) {
			throw new Exception("La prestation " + prestation.prestNumero() + " est archivée, impossible de la valider !");
		}
		
		validerPrestationPrestataire(false, prestation);
		prestation.setPrestDateCloture(new NSTimestamp());
		
		if(saveChanges) {
			editingContext.saveChanges();
		}
	}
	 
	public EOFacturePapier creerFacturePapier(_CktlBasicDataBus dataBus, EOPrestation prestation, EOUtilisateur utilisateur) 
	throws Exception {
		 
		try {
			ApiPrestation.genereFacturePapier(dataBus, dataBus.editingContext(), prestation, utilisateur);
		} catch (java.lang.Exception e) {
			if (e.getMessage() == null || e.getMessage().indexOf("Plus aucun reste") < 0)
				throw e;
		}
		
		NSArray list = FinderFacturePapier.find(editingContext, prestation);
		if (list == null) {
			throw new Exception("Problème : aucune facture n'a été générée pour cette prestation.");
		}
		else {
			return (EOFacturePapier)list.objectAtIndex(0);
		}
	}
	 
	public void validerFactureClient(EOEditingContext ec, EOPrestation presta) {
		 
	}
		
	public static EOTypeCredit defaultTypeCreditRecette(EOEditingContext ec) {
		
		return FinderTypeCreditRec.find(ec,DEFAULT_TCD_CODE_RECETTE);
	}
	
	public static String fileNameDevisPapier(EOPrestation prestation,String nomActePapier) {
		
		if(prestation!=null && prestation.fournisUlr()!=null) {
			StringBuffer buffer = new StringBuffer();
			buffer.append(nomActePapier).append("_").append( prestation.prestNumero().intValue() );
			buffer.append('_').append( prestation.fournisUlr().adrNom().replaceAll(" ", "-") );
			buffer.append('_').append( prestation.prestNumero() );
			buffer.append(".pdf");
			return buffer.toString();
		}
		else {
			return null;
		}
	}
	
	
	/* finder simplifie a l'extreme permettant de retrouver une unique facture pour une prestation */
	public static EOFacturePapier findFacturePapierForPrestation(EOPrestation prestation) {
		EOFacturePapier facture = null;
		if(prestation!=null) {
			EOTypeEtat etatValide = FinderTypeEtat.typeEtatValide(prestation.editingContext());
			EOQualifier qualFPValides = new EOKeyValueQualifier(EOFacturePapier.TYPE_ETAT_KEY,EOQualifier.QualifierOperatorEqual,etatValide);
			NSArray<EOFacturePapier> listFactures = prestation.facturePapiers();
			listFactures = EOQualifier.filteredArrayWithQualifier(listFactures, qualFPValides);
			if(listFactures.count()>0) {
				facture = (EOFacturePapier)listFactures.objectAtIndex(0);
			}
		}
		return facture;
	}
	
	// Quelques methodes statiques helpers. 
	public static EOExercice findCurrentExercice(EOEditingContext ec) {
		//return (EOExercice)DBHelper.fetchOneRow(ec, EOExercice.ENTITY_NAME, EOExercice.EXE_STAT_KEY, "O");
		try {
			//return EOExercice.fetchByKeyValue(ec, EOExercice.EXE_STAT_KEY, "O");
			//Si il y a plusieurs exercices ouverts en même temps, on retourne le dernier
			@SuppressWarnings("unchecked")			
			NSArray<EOExercice> tmp = EOExercice.fetchAll(ec,  EOExercice.EXE_STAT_KEY, "O", new NSArray<EOSortOrdering>(new EOSortOrdering(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareAscending)));
			if ((tmp!=null)&&(tmp.size()>0)){				
				return tmp.lastObject();
			}
			return null;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getParamBudget(EOEditingContext ec, String parKey, EOExercice exercice) {
		try {
			return FinderParametreBudget.find(ec, parKey, exercice).bparValue();
		}
		catch (Exception e) {
			return null;
		}
	}

	   public static EODevise getDeviseEnCours(EOEditingContext editingContext, EOExercice exercice) {
	       String devCode = FinderParametreJefyAdmin.find(editingContext, "DEVISE_DEV_CODE", exercice);
	       if (devCode != null) {
	           return FinderDevise.find(editingContext, devCode);
	       }
	       return null;
	   }	
}
