/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.procedures;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

public class Api {

	/**
	 * Appele la procedure de creation de la facture<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param facture
	 *            NSDictionary a passer a la procedure
	 */
	public static void insFacture(_CktlBasicDataBus dataBus, NSDictionary facture) throws Exception {
		if (facture == null) {
			throw new Exception("Facture a enregistrer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiInsFacture", facture)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de modification de la facture<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param facture
	 *            NSDictionary a passer a la procedure
	 */
	public static void updFacture(_CktlBasicDataBus dataBus, NSDictionary facture) throws Exception {
		if (facture == null) {
			throw new Exception("Facture a modifier null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiUpdFacture", facture)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de suppression de la facture<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param facture
	 *            NSDictionary a passer a la procedure
	 */
	public static void delFacture(_CktlBasicDataBus dataBus, NSDictionary facture) throws Exception {
		if (facture == null) {
			throw new Exception("Facture a supprimer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiDelFacture", facture)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de creation de la recette papier<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recettePapier
	 *            NSDictionary a passer a la procedure
	 */
	public static void insRecettePapier(_CktlBasicDataBus dataBus, NSDictionary recettePapier) throws Exception {
		if (recettePapier == null) {
			throw new Exception("Recette papier a enregistrer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiInsRecettePapier", recettePapier)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de modification de la recette papier<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recettePapier
	 *            NSDictionary a passer a la procedure
	 */
	public static void updRecettePapier(_CktlBasicDataBus dataBus, NSDictionary recettePapier) throws Exception {
		if (recettePapier == null) {
			throw new Exception("Recette papier a modifier null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiUpdRecettePapier", recettePapier)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de suppression de la recette papier<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recettePapier
	 *            NSDictionary a passer a la procedure
	 */
	public static void delRecettePapier(_CktlBasicDataBus dataBus, NSDictionary recettePapier) throws Exception {
		if (recettePapier == null) {
			throw new Exception("Recette papier a supprimer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiDelRecettePapier", recettePapier)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de creation de la recette<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recette
	 *            NSDictionary a passer a la procedure
	 */
	public static void insRecette(_CktlBasicDataBus dataBus, NSDictionary recette) throws Exception {
		if (recette == null) {
			throw new Exception("Recette a enregistrer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiInsRecette", recette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de suppression de la recette<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recette
	 *            NSDictionary a passer a la procedure
	 */
	public static void delRecette(_CktlBasicDataBus dataBus, NSDictionary recette) throws Exception {
		if (recette == null) {
			throw new Exception("Recette a supprimer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiDelRecette", recette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de creation de la facture-recette<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param factureRecette
	 *            NSDictionary a passer a la procedure
	 */
	public static NSDictionary insFactureRecette(EOEditingContext ec, _CktlBasicDataBus dataBus, NSDictionary factureRecette) throws Exception {
		try {
			if (factureRecette == null) {
				throw new Exception("FactureRecette a enregistrer null!!");
			}
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiInsFactureRecette", factureRecette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de modification de la facture-recette<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param factureRecette
	 *            NSDictionary a passer a la procedure
	 */
	public static void updFactureRecette(_CktlBasicDataBus dataBus, NSDictionary factureRecette) throws Exception {
		if (factureRecette == null) {
			throw new Exception("FactureRecette a modifier null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiUpdFactureRecette", factureRecette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de suppression de la facture-recette<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param factureRecette
	 *            NSDictionary a passer a la procedure
	 */
	public static void delFactureRecette(_CktlBasicDataBus dataBus, NSDictionary factureRecette) throws Exception {
		if (factureRecette == null) {
			throw new Exception("FactureRecette a supprimer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiDelFactureRecette", factureRecette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}
	
	/**
	 * Appele la procedure de creation de la reduction<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recette
	 *            NSDictionary a passer a la procedure
	 */
	public static NSDictionary insReduction(_CktlBasicDataBus dataBus, NSDictionary recette) throws Exception {
		if (recette == null) {
			throw new Exception("Recette a enregistrer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiInsReduction", recette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	/**
	 * Appele la procedure de suppression de la reduction<BR>
	 * 
	 * @param dataBus
	 *            _CktlBasicDataBus servant a gerer les transactions
	 * @param recette
	 *            NSDictionary a passer a la procedure
	 */
	public static void delReduction(_CktlBasicDataBus dataBus, NSDictionary recette) throws Exception {
		if (recette == null) {
			throw new Exception("Recette a supprimer null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiDelReduction", recette)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

}