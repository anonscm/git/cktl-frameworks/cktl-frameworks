package org.cocktail.kava.server;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.metier.EOArticle;
import org.cocktail.kava.server.metier.EOArticleLocalise;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.FwkPieRecord;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.qualifiers.ERXInQualifier;

public class ClientCatalogueArticle implements IPrestationElement {

	private EOCatalogueArticle catalogueArticle = null;
	private Number quantite = null;
	private NSMutableArray<ClientCatalogueArticle> userChoosenOptions = null;
	private NSMutableArray<ClientCatalogueArticle> clientCatalogueArticlesOption = null;
	
	public ClientCatalogueArticle(EOCatalogueArticle article, Number quantite) {
		this.catalogueArticle = article;
		this.quantite = quantite;
		_init();
	}
	
	public ClientCatalogueArticle(EOCatalogueArticle article) {
		this.catalogueArticle = article;
		_init();
	}	
	
	public ClientCatalogueArticle() {
	}
	
	private void _init() {
		userChoosenOptions = new NSMutableArray<ClientCatalogueArticle>();
	}
	
	
	
	public boolean isStockLimite() {
		Integer qte = catalogueArticle.article().articlePrestation().artpQteDispo();
		if((qte==null) || (qte!=null && qte.intValue()<0)) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public int getQtePrevue() {
		Integer qte = catalogueArticle.article().articlePrestation().artpQteDispo();
		System.out.println("getQtePrevue="+qte);
		if(qte==null) {	return -1; }
		else { return qte.intValue(); }
	}
	
	public int getQteReservee() {
		NSArray<EOPrestationLigne> prestaLignes = catalogueArticle.getPrestationLignesDansPrestaValide();
		int cpt = 0;
		for(EOPrestationLigne pl : prestaLignes) {
			cpt += pl.prligQuantite().intValue();
		}
		System.out.println("getQteReservee="+cpt);
		return cpt;
	}
	
	public int getQtePayee() {
		NSArray<EOPrestationLigne> prestaLignes = catalogueArticle.getPrestationLignesDansPrestaPayees();
		int cpt = 0;
		for(EOPrestationLigne pl : prestaLignes) {
			cpt += pl.prligQuantite().intValue();
		}
		System.out.println("getQtePayee="+cpt);
		return cpt;
	}
	
	public boolean isPrestationSansPaiementPossible() {
		return getQtePrevue()>0 && getQteReservee()<=getQtePrevue() && getQtePayee()<getQteReservee();
	}
	
	public boolean isPrestationAvecPaiementPossible() {
		return (getQtePrevue()==-1) || (getQtePrevue() - getQteReservee() > 0);
	}
	
	
	public boolean isArticleDisponible() {
		if(!isStockLimite()) {
			return true;
		}
		else {
			return isPrestationAvecPaiementPossible();
		}
	}
	
	
	// gestion des libelles localises
	
	public String artLibelleLocalise(String cLangue) {
		return catalogueArticle.artLibelleLocalise(cLangue);
	}
	
	public String artReferenceLocalise(String cLangue) {
		return catalogueArticle.artReferenceLocalise(cLangue);
	}
	
	
	public EOArticleLocalise articleLocaliseForLangue(String cLangue) {
		return catalogueArticle.articleLocaliseForLangue(cLangue);
	}
	
	

	public EOCatalogueArticle getCatalogueArticle() {
		return catalogueArticle;
	}

	public void setCatalogueArticle(EOCatalogueArticle article) {
		this.catalogueArticle = article;
	}

	public Number getQuantite() {
		return quantite;
	}

	public void setQuantite(Number qte) {
		this.quantite = qte;
		if(qte==null) {
			this.quantite = new Integer(0);
		}
	}
	
	
	public BigDecimal calculateSousTotal() {
		if(quantite!=null && catalogueArticle!=null && catalogueArticle.caarPrixTtc()!=null) {
			return new BigDecimal( quantite.doubleValue() * catalogueArticle.caarPrixTtc().doubleValue() );
		}
		else {
			return null;
		}
	}
	
	public int caarId() {
		return catalogueArticle.getPkCaarId().intValue(); 
	}
	
	/* les options d'un articles ce sont ses enfants 
	 * TODO: Voir si on ne peut pas ameliorer les fetchs ici.
	 * */
	public NSMutableArray<ClientCatalogueArticle> listeOptions() {
		
		if(clientCatalogueArticlesOption==null) {
			
			clientCatalogueArticlesOption = new NSMutableArray();
			
			NSArray<EOArticle> articlesOption = catalogueArticle.article().articles();
		
			ERXInQualifier qual = new ERXInQualifier( EOCatalogueArticle.ARTICLE_KEY, articlesOption );
			//NSArray catalogueArticlesOptions = DBHelper.fetchData(catalogueArticle.editingContext(),EOCatalogueArticle.ENTITY_NAME, qual);
			NSArray<EOCatalogueArticle> catalogueArticlesOptions = EOCatalogueArticle.fetchAll(catalogueArticle.editingContext(), qual);
			
			
			for(Enumeration<EOCatalogueArticle> enumCatArts = catalogueArticlesOptions.objectEnumerator(); enumCatArts.hasMoreElements();) {
				clientCatalogueArticlesOption.addObject(new ClientCatalogueArticle((EOCatalogueArticle)enumCatArts.nextElement()));
			}
		}
		
		return clientCatalogueArticlesOption;
	}
	
	
	public void addUserChoosenOption(ClientCatalogueArticle ccArticle) {
		userChoosenOptions.addObject(ccArticle);
	}

	public NSMutableArray<ClientCatalogueArticle> getUserChoosenOptions() {
		return userChoosenOptions;
	}

	public void setUserChoosenOptions(NSMutableArray<ClientCatalogueArticle> userChoosenOptions) {
		this.userChoosenOptions = userChoosenOptions;
	}
	
	
	// IPrestationElement implementation
	public EOCatalogueArticle catalogueArticle() {
		return catalogueArticle;
	}

	public NSArray<ClientCatalogueArticle> options() {
		return userChoosenOptions.immutableClone();
	}
	
	//TODO: verifier ce qui se passe avec les bigDecimal ici (conversion de int vers BigDecimal)
	public BigDecimal quantite() {
		if(getQuantite()!=null) {
			return new BigDecimal( getQuantite().intValue() );
		}
		else {
			return null;
		}
	}
	
	
	public String toString () {
		StringBuffer buf = new StringBuffer();
		buf.append( "["+catalogueArticle.article().artLibelle()+" - id="+catalogueArticle.getPkCaarId()+" - qte="+quantite+"]" );
		if(userChoosenOptions!=null && userChoosenOptions.count()>0) {
			buf.append( "\nOptions:\n");
			for(int i=0;i<userChoosenOptions.count();i++) {
				buf.append("\t").append(  ((ClientCatalogueArticle)userChoosenOptions.objectAtIndex(0)).toString() ).append("\n");
			}
		}
		
		return buf.toString();
	}



	
	
}
