
/*
Copyright Cocktail (Consortium) 1995-2009

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.


This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package org.cocktail.kava.server;

import java.io.IOException;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;

/**
* Classe permettant de transformer un objet en bloc xml. Utilise la classe CktlXMLWriter.
* 
* @author rprin
*/
public class CktlXMLSerialize {

	/**
	 * Tableau contenant les relations à exporter (dans le cas d'un EOEnterpriseObject). Les autres relations seront ignorées. Chaque relation
	 * doit être indiquée en texte (minuscules) avec son arborescence à partir de l'objet ( par exemple commande/engage/todetailcde/ pour un
	 * objet Commande). Ne pas oublier le "/" à la fin de chaque relation.
	 */
	private NSArray		relationsToExport;

	/**
	 * Objet servant à écrire dans le flux xml.
	 */
	private CktlXMLWriter	myCktlXMLWriter;

	/**
	 * Indique si on souhaite échapper les caractères & et <.
	 */
	public boolean		wantToEscapeSpecialsChars	= true;

	/**
	 * Constructeur.
	 * 
	 * @param pCktlXMLWriter
	 *            L'objet dans lequel on écrit le xml.
	 * @param prelationsToExport
	 *            Tableau contenant les relations à exporter (dans le cas d'un EOEnterpriseObject). Les autres relations seront ignorées. Chaque
	 *            relation doit être indiquée en texte avec son arborescence complête ( par exemple Commande/engage/toDetailCde).
	 */
	public CktlXMLSerialize(CktlXMLWriter pCktlXMLWriter, NSArray prelationsToExport) {
		super();
		myCktlXMLWriter = pCktlXMLWriter;
		setRelationsToExport(prelationsToExport);
	}

	/**
	 * Transforme un objet en bloc XML en créant uniquement des noeuds (pas d'attributs). Tous les noms des noeuds seront créés en minuscules,
	 * pour respecter les préconisations XML. <br>
	 * Pour les NSDictionary, le nom du noeud sera la clé (en minuscules). Pour les EOEnterpriseObject, le nom du noeud sera le nom de l'attribut
	 * ou de la relation (toujours en minuscules)
	 * 
	 * @param obj
	 *            Objet à transformer en xml (NSDictionary, NSArray ou EOEnterpriseObject).
	 * @param tag
	 *            Nom du noeud à créer
	 * @return
	 */
	public void objectToXml(Object obj, String tag) throws IOException {
		serializeObjectForTag(obj, tag, "");
	}

	private void serializeObjectForTag(Object obj, String tag, String arboPere) throws IOException {
		String arbo = arboPere + tag.toLowerCase() + "/";
		if (obj instanceof NSTimestamp) {
			serializeNStimestampForTag((NSTimestamp) obj, tag);
		}
		else
			if (obj instanceof Integer) {
				serializeIntegerForTag((Integer) obj, tag);
			}
			else
				if (obj instanceof Number) {
					serializeNumberForTag((Number) obj, tag);
				}
				else
					if ((obj instanceof NSArray)) {
						serializeNSArrayForTag((NSArray) obj, tag, arboPere);
					}
					else
						if ((obj instanceof NSDictionary)) {
							serializeNSDictionaryForTag((NSDictionary) obj, tag, arbo);
						}
						else
							if ((obj instanceof EOEnterpriseObject)) {
								serializeEOEnterpriseObjectForTag((EOEnterpriseObject) obj, tag, arbo);
							}
							else
								if (obj != null) {
									encodeValueInNode(obj, tag);
								}
								else {
									encodeValueInNode(null, tag);
								}
	}

	private void serializeNSDictionaryForTag(NSDictionary obj, String tag, String arbo) throws IOException {
		NSArray lesKeys = obj.allKeys();
		int nbItems = lesKeys.count();
		myCktlXMLWriter.startElement(tag);
		for (int i = 0; i < nbItems; i++) {
			serializeObjectForTag(obj.valueForKey((String) lesKeys.objectAtIndex(i)), (String) lesKeys.objectAtIndex(i), arbo);
		}
		myCktlXMLWriter.endElement();
	}

	private void serializeNSArrayForTag(NSArray obj, String tagname, String arbo) throws IOException {
		int nbItems = obj.count();
		for (int i = 0; i < nbItems; i++) {
			serializeObjectForTag(obj.objectAtIndex(i), tagname, arbo);
		}
	}

	private void serializeEOEnterpriseObjectForTag(EOEnterpriseObject obj, String tag, String arbo) throws IOException {
		String tagname, keyname;
		myCktlXMLWriter.startElement(tag);
		NSArray lesattr = obj.attributeKeys();
		for (int i = 0; i < lesattr.count(); i++) {
			keyname = (String) lesattr.objectAtIndex(i);
			tagname = keyname.toLowerCase();
			serializeObjectForTag(obj.valueForKey(keyname), tagname, arbo);
		}
		// relations
		NSArray lesrel = obj.toOneRelationshipKeys();
		for (int i = 0; i < lesrel.count(); i++) {
			keyname = (String) lesrel.objectAtIndex(i);
			tagname = keyname.toLowerCase();

			if (this.relationsToExport.indexOfObject(arbo + tagname + "/") != NSArray.NotFound) {
				serializeObjectForTag(obj.valueForKey(keyname), tagname, arbo);
			}
		}

		lesrel = obj.toManyRelationshipKeys();
		for (int i = 0; i < lesrel.count(); i++) {
			keyname = (String) lesrel.objectAtIndex(i);
			tagname = keyname.toLowerCase();
			if (this.relationsToExport.indexOfObject(arbo + tagname + "/") != NSArray.NotFound) {
				serializeObjectForTag(obj.valueForKey(keyname), tagname, arbo);
			}
		}
		myCktlXMLWriter.endElement();
	}

	private void serializeNStimestampForTag(NSTimestamp d, String tag) throws IOException {
		encodeValueInNode((new NSTimestampFormatter("%Y-%m-%d %H:%M")).format(d), tag);
	}

	private void serializeIntegerForTag(Integer n, String tag) throws IOException {
		encodeValueInNode(n, tag);
	}

	private void serializeNumberForTag(Number n, String tag) throws IOException {
		encodeValueInNode(n, tag);
	}

	private void encodeValueInNode(Object val, String tag) throws IOException {
		if (val == null) {
			myCktlXMLWriter.writeElement(tag, "");
			return;
		}
		if (this.wantToEscapeSpecialsChars) {
			myCktlXMLWriter.writeElement(tag, StringTool.ifNull(escapeSpecialChars(val.toString()), ""));
		}
		else {
			myCktlXMLWriter.writeElement(tag, StringTool.ifNull(val, ""));
		}
	}

	private String escapeSpecialChars(String val) {
		val = val.replaceAll("&", "&amp;");
		val = val.replaceAll("<", "&lt;");
		val = StringTool.replace(val, "\n", CktlXMLWriter.SpecChars.br);
		return val;
	}

	/**
	 * @param array
	 */
	public void setRelationsToExport(NSArray prelationsToExport) {
		relationsToExport = prelationsToExport;
		// Transformer les chemins des relations en minuscules
		NSMutableArray tmp = new NSMutableArray(relationsToExport);
		for (int i = 0; i < tmp.count(); i++) {
			tmp.replaceObjectAtIndex(((String) tmp.objectAtIndex(i)).toLowerCase(), i);
		}
		relationsToExport = tmp.immutableClone();
	}

}
