package org.cocktail.fwkcktlnomenclaturesguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature;
import org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclatureObjet;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.foundation.ERXStringUtilities;

public class ObjetsNomenclatureTableView extends CktlAjaxWOComponent {
	
	private static final long serialVersionUID = 7157615152950591802L;
	
	private static final String BINDING_editingContext = "editingContext";
	private static final String BINDING_selectedNomenclature = "selectedNomenclature";
	private static final String BINDING_wantRefresh = "wantRefresh";
	private static final String BINDING_persId = "persId";
	
	public final static String VISIBLE = "VISIBLE";
	public final static String NON_VISIBLE = "NON_VISIBLE";
	private String visibilite = VISIBLE;

	private static final String ACTION_AJOUTER = "ACTION_AJOUTER";
	private static final String ACTION_EDITER = "ACTION_EDITER";
	private String typeAction = ACTION_AJOUTER;
	
	private static String CODE_OBJET_DEBUT = "ETAB_";

	private EONomenclatureObjet currentNomenclatureObjet;
	private EONomenclatureObjet selectedNomenclatureObjet;
	private EOArrayDataSource nomenclatureObjetsDataSource;
	private ERXDisplayGroup<EONomenclatureObjet> nomenclatureObjetsDisplayGroup;
	
	private NSTimestamp debutDateValidite;
	private NSTimestamp finDateValidite;
	private NSTimestamp dateCreation;
	private NSTimestamp dateModification;
	private String codeObjet;
	private String libelleCourt;
	private String libelleCourtCustom;
	private String libelleLong;
	private String libelleLongCustom;
	private String valeur;
	private String position;
	private Boolean personnalisee;
	private String origine;
	
	
    public ObjetsNomenclatureTableView(WOContext context) {
        super(context);
    }
    
    @Override
    public EOEditingContext edc() {
    	return (EOEditingContext) valueForBinding(BINDING_editingContext);
    }
	
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	
    	if(nomenclatureObjetsDataSource != null && nomenclatureObjetsDisplayGroup != null) {
	    	if(getWantRefreshObjet()) {
				nomenclatureObjetsDataSource.setArray(nomenclatureObjets());
				nomenclatureObjetsDisplayGroup.fetch();
		    	setWantRefreshObjet(Boolean.FALSE);
	    	}
    	}
    	
    	super.appendToResponse(response, context);
    }
    
	public EONomenclature getSelectedNomenclature() {
		return (EONomenclature) valueForBinding(BINDING_selectedNomenclature);
	}

	public Boolean getWantRefreshObjet() {
		return booleanValueForBinding(BINDING_wantRefresh, null);
	}

	public Integer getPersId() {
		return valueForIntegerBinding(BINDING_persId, 0);
	}

	public void setWantRefreshObjet(Boolean wantRefreshObjet) {
		setValueForBinding(wantRefreshObjet, BINDING_wantRefresh);
	}
    
    public String containerAjoutObjetNomenclatureId() {
    	return getComponentId() + "_containerAjoutObjetNomenclature";
    }
	
	public String containerTableViewId() {
		return getComponentId() + "_ContainerObjetTableView";
	}
    
    public String objetsTableViewId() {
    	return getComponentId() + "_ObjetsTableView";
    }
    
    public String AjouterObjetNomenclatureWindowId() {
    	return getComponentId() + "_AjouterObjetNomenclatureWindow";
    }

	public EONomenclatureObjet getCurrentNomenclatureObjet() {
		return currentNomenclatureObjet;
	}

	public void setCurrentNomenclatureObjet(EONomenclatureObjet currentNomenclatureObjet) {
		this.currentNomenclatureObjet = currentNomenclatureObjet;
	}

	public EOArrayDataSource getNomenclatureObjetsDataSource() {
		return nomenclatureObjetsDataSource;
	}

	public void setNomenclatureObjetsDataSource(
			EOArrayDataSource nomenclatureObjetsDataSource) {
		this.nomenclatureObjetsDataSource = nomenclatureObjetsDataSource;
	}
	
	public NSArray<EONomenclatureObjet> nomenclatureObjets() {
		EONomenclature selectedNomenclature = getSelectedNomenclature();
		
		if(selectedNomenclature != null) {
			int taille = selectedNomenclature.objets().size();
			Boolean positionsOk = false;
			
			for(int i=0; i<taille; i++) {
				if(!positionsOk && selectedNomenclature.objets().get(i).position() != 0) {
					positionsOk = true;
				}
				//System.out.println("["+selectedNomenclature.objets().get(i).libelleLong()+" : "+selectedNomenclature.objets().get(i).position()+"]");
			}
			
			if(!positionsOk) {
				for(int i=0; i<taille; i++) {
					selectedNomenclature.objets().get(i).setPosition(i);
				}
			}
			return selectedNomenclature.objets();
		}
		return null;
	}
	
	public EOArrayDataSource nomenclatureObjetsDataSource() {
		if(nomenclatureObjetsDataSource == null) {
    		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(EONomenclatureObjet.ENTITY_NAME);
    		nomenclatureObjetsDataSource = new EOArrayDataSource(classDescription, edc());
    		nomenclatureObjetsDataSource.setArray(nomenclatureObjets());
    	}
    	return nomenclatureObjetsDataSource;
	}

	public ERXDisplayGroup<EONomenclatureObjet> getNomenclatureObjetsDisplayGroup() {		
		if(nomenclatureObjetsDisplayGroup == null) {
			nomenclatureObjetsDisplayGroup = new ERXDisplayGroup<EONomenclatureObjet>();
			nomenclatureObjetsDisplayGroup.setDataSource(nomenclatureObjetsDataSource());
			nomenclatureObjetsDisplayGroup.setDelegate(new NomenclatureObjetDisplayGroupDelegate());
			nomenclatureObjetsDisplayGroup.setSortOrderings(EONomenclatureObjet.POSITION.ascs());
			nomenclatureObjetsDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			nomenclatureObjetsDisplayGroup.fetch();
    	}
		return nomenclatureObjetsDisplayGroup;
	}

	public void setNomenclatureObjetsDisplayGroup(
			ERXDisplayGroup<EONomenclatureObjet> nomenclatureObjetsDisplayGroup) {
		this.nomenclatureObjetsDisplayGroup = nomenclatureObjetsDisplayGroup;
	}
	
	public EONomenclatureObjet getSelectedNomenclatureObjet() {
		return selectedNomenclatureObjet;
	}

	public void setSelectedNomenclatureObjet(EONomenclatureObjet selectedNomenclatureObjet) {
		this.selectedNomenclatureObjet = selectedNomenclatureObjet;
	}

	public class NomenclatureObjetDisplayGroupDelegate {
		
		public void displayGroupDidChangeSelection(WODisplayGroup displayGroup) {
			ERXDisplayGroup<EONomenclatureObjet> _displayGroup = (ERXDisplayGroup<EONomenclatureObjet>) displayGroup;
			if(_displayGroup.selectedObject() != null) {
					setSelectedNomenclatureObjet(_displayGroup.selectedObject());
			}
		}
		
	}
	
	public Boolean canAdd() {
		return getSelectedNomenclature().completable();
	}

	public Boolean canEdit() {
		if(getSelectedNomenclatureObjet().personnalisee()) {
			return true;
		}
		return getSelectedNomenclature().modifiable();
	}

	public Boolean canRemove() {
		if(getSelectedNomenclatureObjet().personnalisee()) {
			return true;
		}
		return getSelectedNomenclature().modifiable();
	}
	
	public NSTimestamp getDebutDateValidite() {
		return debutDateValidite;
	}

	public void setDebutDateValidite(NSTimestamp debutDateValidite) {
		this.debutDateValidite = debutDateValidite;
	}

	public String getCodeObjet() {
		return codeObjet;
	}

	public void setCodeObjet(String codeObjet) {
		this.codeObjet = codeObjet;
	}

	public String getLibelleCourt() {
		return libelleCourt;
	}

	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}

	public String getLibelleCourtCustom() {
		return libelleCourtCustom;
	}

	public void setLibelleCourtCustom(String libelleCourtCustom) {
		this.libelleCourtCustom = libelleCourtCustom;
	}

	public String getLibelleLong() {
		return libelleLong;
	}

	public void setLibelleLong(String libelleLong) {
		this.libelleLong = libelleLong;
	}

	public String getLibelleLongCustom() {
		return libelleLongCustom;
	}

	public void setLibelleLongCustom(String libelleLongCustom) {
		this.libelleLongCustom = libelleLongCustom;
	}

	public String getValeur() {
		return valeur;
	}

	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
	  this.position = position;
	}
	
	public Boolean getPersonnalisee() {
		return personnalisee;
	}
	
	public void setPersonnalisee(Boolean personnalisee) {
		this.personnalisee = personnalisee;
	}
	
	public String getOrigine() {
		return origine;
	}

	public void setOrigine(String origine) {
		this.origine = origine;
	}

	public NSTimestamp getFinDateValidite() {
		return finDateValidite;
	}

	public void setFinDateValidite(NSTimestamp finDateValidite) {
		this.finDateValidite = finDateValidite;
	}

	public NSTimestamp getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(NSTimestamp dateCreation) {
		this.dateCreation = dateCreation;
	}

	public NSTimestamp getDateModification() {
		return dateModification;
	}

	public void setDateModification(NSTimestamp dateModification) {
		this.dateModification = dateModification;
	}

	public String getVisibilite() {
		return visibilite;
	}

	public void setVisibilite(String visibilite) {
		this.visibilite = visibilite;
	}
	
	public Boolean getIsVisibleSelected() {
		return ERXStringUtilities.stringEqualsString(getVisibilite(), VISIBLE);
	}

	public void setIsVisibleSelected(Boolean isVisibleSelected) {
		if(isVisibleSelected) {
			setVisibilite(VISIBLE);
		}
	}
	
	public Boolean getIsNonVisibleSelected() {
		return ERXStringUtilities.stringEqualsString(getVisibilite(), NON_VISIBLE);
	}

	public void setIsNonVisibleSelected(Boolean isNonVisibleSelected) {
		if(isNonVisibleSelected) {
			setVisibilite(NON_VISIBLE);
		}
	}
	
	public Boolean isAjout() {
		return ERXStringUtilities.stringEqualsString(typeAction, ACTION_AJOUTER);
	}
	
	public Boolean isEdit() {
		return ERXStringUtilities.stringEqualsString(typeAction, ACTION_EDITER);
	}
	
	public Boolean isTypeValeurString() {
		return ERXStringUtilities.stringEqualsString(EONomenclature.TYPE_VALEUR_STRING, getSelectedNomenclature().typeValeurs());
	}
	
	public Boolean isTypeValeurFloat() {
		return ERXStringUtilities.stringEqualsString(EONomenclature.TYPE_VALEUR_FLOAT, getSelectedNomenclature().typeValeurs());
	}
	
	public Boolean isTypeValeurInteger() {
		return ERXStringUtilities.stringEqualsString(EONomenclature.TYPE_VALEUR_INTEGER, getSelectedNomenclature().typeValeurs());
	}
	
	public void reset() {
		typeAction = ACTION_AJOUTER;
		codeObjet = CODE_OBJET_DEBUT;
		libelleCourt = null;
		libelleCourtCustom = null;
		libelleLong = null;
		libelleLongCustom = null;
		valeur = null;
		position = null;		
		debutDateValidite = null;
		finDateValidite = null;
		setVisibilite(VISIBLE);
	}
	
	public void initialiserValeurs() {
		
		typeAction = ACTION_EDITER;
		codeObjet = getSelectedNomenclatureObjet().code();
		
		if(ERXStringUtilities.stringEqualsString(EONomenclature.TYPE_VALEUR_STRING, getSelectedNomenclature().typeValeurs())) {
			libelleCourt = getSelectedNomenclatureObjet().libelleCourt();
			libelleCourtCustom = getSelectedNomenclatureObjet().libelleCourtCustom();
			libelleLong = getSelectedNomenclatureObjet().libelleLong();
			libelleLongCustom = getSelectedNomenclatureObjet().libelleLongCustom();
		} else {
			valeur = getSelectedNomenclatureObjet().valeur();
		}
		position = getSelectedNomenclatureObjet().position().toString();
		personnalisee = getSelectedNomenclatureObjet().personnalisee();
		origine = getSelectedNomenclatureObjet().origine();
		debutDateValidite = getSelectedNomenclatureObjet().dDebutValidite();
		finDateValidite = getSelectedNomenclatureObjet().dFinValidite();
		
		if(getSelectedNomenclatureObjet().visible()) {
			setVisibilite(VISIBLE);
		} else {
			setVisibilite(NON_VISIBLE);
		}
	}
	
	public WOActionResults validerNomenclatureObjet() {
		
		if(isAjout()) {
			if(getDebutDateValidite()!= null && 
			!ERXStringUtilities.stringIsNullOrEmpty(getCodeObjet()) &&
			(!ERXStringUtilities.stringIsNullOrEmpty(getLibelleCourt()) || 
			!ERXStringUtilities.stringIsNullOrEmpty(getLibelleCourtCustom()) || 
			!ERXStringUtilities.stringIsNullOrEmpty(getLibelleLong()) ||
			!ERXStringUtilities.stringIsNullOrEmpty(getLibelleLongCustom()))) {
				
				String code = getCodeObjet();
				String code2 = ERXStringUtilities.stringByTruncatingStringToByteLengthInEncoding(code, CODE_OBJET_DEBUT.length(), "UTF8");

				if(code.length()>CODE_OBJET_DEBUT.length()) {
					
					if(!ERXStringUtilities.stringEqualsString(code2, CODE_OBJET_DEBUT)) {
						code = CODE_OBJET_DEBUT + code;
					}
					
					EONomenclature nomenclature = getSelectedNomenclature();
					EONomenclatureObjet	nomenclatureObjet = EONomenclatureObjet.creerInstance(edc());
					nomenclatureObjet.addNomenclatureObjet(getCodeObjet(), getLibelleCourt(), getLibelleCourtCustom(), 
							getLibelleLong(), getLibelleLongCustom(), getValeur(), getDebutDateValidite(), getFinDateValidite(), 
							nomenclature, getPersId(), nomenclature.objets().size(), getIsVisibleSelected());

					nomenclature.addToObjetsRelationship(nomenclatureObjet);

					nomenclatureObjetsDataSource.insertObject(nomenclatureObjet);
					getNomenclatureObjetsDisplayGroup().fetch();
					
					try {
						edc().saveChanges();
						NSNotificationCenter.defaultCenter().postNotification("resetNomenclatures", null);
					} catch (ValidationException e) {
						mySession().addSimpleErrorMessage("Erreur", e);
						edc().revert();
						return doNothing();
					}
					
					CktlAjaxWindow.close(context(), AjouterObjetNomenclatureWindowId());
					
				} else {
					mySession().addSimpleErrorMessage("Erreur", "Veuillez compl&eacute;ter le champ code.");
				}
			} else {
				mySession().addSimpleErrorMessage("Erreur", "Un des champs &agrave; remplir est vide.");
			}
			
		} else if(isEdit()){
			getSelectedNomenclatureObjet().editNomenclatureObjet(getLibelleCourtCustom(), getLibelleLongCustom(), getValeur(), getFinDateValidite(), getPersId(), getIsVisibleSelected());
			try {
				edc().saveChanges();
				NSNotificationCenter.defaultCenter().postNotification("resetNomenclatures", null);
			} catch (ValidationException e) {
				mySession().addSimpleErrorMessage("Erreur", e);
				edc().revert();
				return doNothing();
			}
			CktlAjaxWindow.close(context(), AjouterObjetNomenclatureWindowId());
		}
		
		return doNothing();
	}
		
	
	public WOActionResults supprimerObjetNomenclature() {
		
		for(Object nomencObj : nomenclatureObjetsDataSource.fetchObjects()) {
			EONomenclatureObjet nomenclatureObjet = (EONomenclatureObjet) nomencObj;
			if(nomenclatureObjet.position() > getSelectedNomenclatureObjet().position()) {
				nomenclatureObjet.setPosition(nomenclatureObjet.position() -1, getPersId());
			}
		}
		
		selectedNomenclatureObjet.setNomenclatureRelationship(null);
		edc().deleteObject(selectedNomenclatureObjet);
		
		try {
            edc().saveChanges();
            NSNotificationCenter.defaultCenter().postNotification("resetNomenclatures", null);
        } catch (ValidationException e) {
        	mySession().addSimpleErrorMessage("Erreur", e);
            edc().revert();
            return doNothing();
        }
		
		return doNothing();
	}
	
	public WOActionResults decrementerPosition() {
		int currentPosition = getSelectedNomenclatureObjet().position();
		
		if(currentPosition > 0) {
			int i=0;
			int taille = nomenclatureObjetsDataSource.fetchObjects().size();
			Boolean trouve = false;
			
			while(!trouve && i<taille) {
				if(((EONomenclatureObjet) nomenclatureObjetsDataSource.fetchObjects().get(i)).position() == currentPosition-1) {
					trouve = true;
					((EONomenclatureObjet) nomenclatureObjetsDataSource.fetchObjects().get(i)).setPosition(currentPosition, getPersId());
					getSelectedNomenclatureObjet().setPosition(currentPosition -1, getPersId());
				}
				i++;
			}
			
			try {
	            edc().saveChanges();
	            nomenclatureObjetsDisplayGroup.fetch();
	        } catch (ValidationException e) {
	            mySession().addSimpleErrorMessage("Erreur", e);
	            edc().revert();
	            return doNothing();
	        }
		}
		return doNothing();
	}
	
	public WOActionResults incrementerPosition() {
		
		int currentPosition = getSelectedNomenclatureObjet().position();
		int taille = nomenclatureObjetsDataSource.fetchObjects().size();
		
		if(currentPosition < taille) {
			
			int i=0;
			Boolean trouve = false;
			
			while(!trouve && i<taille) {
				if(((EONomenclatureObjet) nomenclatureObjetsDataSource.fetchObjects().get(i)).position() == currentPosition+1) {
					trouve = true;
					((EONomenclatureObjet) nomenclatureObjetsDataSource.fetchObjects().get(i)).setPosition(currentPosition, getPersId());
					getSelectedNomenclatureObjet().setPosition(currentPosition +1, getPersId());
				}
				i++;
			}
			
			try {
	            edc().saveChanges();
	            nomenclatureObjetsDisplayGroup.fetch();
	        } catch (ValidationException e) {
	            mySession().addSimpleErrorMessage("Erreur", e);
	            edc().revert();
	            return doNothing();
	        }
		}
		return doNothing();
	}
    
}