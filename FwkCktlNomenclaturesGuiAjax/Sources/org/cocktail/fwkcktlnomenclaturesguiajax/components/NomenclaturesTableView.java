package org.cocktail.fwkcktlnomenclaturesguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;

public class NomenclaturesTableView extends CktlAjaxWOComponent {
	
	private static final long serialVersionUID = 8681277063950150243L;
	
	private static final String BINDING_filter = "filter";
	private static final String BINDING_editingContext = "editingContext";
	private static final String BINDING_selectedNomenclature = "selectedNomenclature";
	
	private EONomenclature currentNomenclature;
	private EOArrayDataSource nomenclaturesDataSource;
	private ERXDisplayGroup<EONomenclature> nomenclaturesDisplayGroup;
	
    public NomenclaturesTableView(WOContext context) {
        super(context);
    }
    
    @Override
    public EOEditingContext edc() {
    	return (EOEditingContext) valueForBinding(BINDING_editingContext);
    }
    
    public String filter() {
    	return stringValueForBinding(BINDING_filter, "");
    }
    
    public String selectedNomenclature() {
    	return stringValueForBinding(BINDING_selectedNomenclature, "");
    }
    
    public String nomenclaturesTableViewId() {
    	return getComponentId() + "_nomenclaturesTableView";
    }
    
    public NSArray<EONomenclature> nomenclatures() {
    	return EONomenclature.nomenclaturesWithFilter(filter(), edc());
    }
    
    public EOArrayDataSource nomenclaturesDataSource() {
    	if(nomenclaturesDataSource == null) {
    		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(EONomenclature.ENTITY_NAME);
    		nomenclaturesDataSource = new EOArrayDataSource(classDescription, edc());
    		nomenclaturesDataSource.setArray(nomenclatures());
    	}
    	return nomenclaturesDataSource;
    }
    
    public ERXDisplayGroup<EONomenclature> nomenclaturesDisplayGroup() {
    	if(nomenclaturesDisplayGroup == null) {
    		nomenclaturesDisplayGroup = new ERXDisplayGroup<EONomenclature>();
    		nomenclaturesDisplayGroup.setNumberOfObjectsPerBatch(5);
    		nomenclaturesDisplayGroup.setDataSource(nomenclaturesDataSource());
    		nomenclaturesDisplayGroup.setDelegate(new NomenclaturesDisplayGroupDelegate());
    		nomenclaturesDisplayGroup.setSortOrderings(EONomenclature.LIBELLE.ascs());
    		nomenclaturesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
    		nomenclaturesDisplayGroup.fetch();
    	}
    	return nomenclaturesDisplayGroup;
    }


	public EONomenclature currentNomenclature() {
		return currentNomenclature;
	}


	public void setCurrentNomenclature(EONomenclature currentNomenclature) {
		this.currentNomenclature = currentNomenclature;
	}

	public class NomenclaturesDisplayGroupDelegate {

		public void displayGroupDidChangeSelection(WODisplayGroup displayGroup) {
			ERXDisplayGroup<EONomenclature> _displayGroup = (ERXDisplayGroup<EONomenclature>) displayGroup;
			if(_displayGroup.selectedObject() != null) {
				if(hasBinding(BINDING_selectedNomenclature)) {
					setValueForBinding(_displayGroup.selectedObject(), BINDING_selectedNomenclature);
					
				}
			}
		}

	}
    
}