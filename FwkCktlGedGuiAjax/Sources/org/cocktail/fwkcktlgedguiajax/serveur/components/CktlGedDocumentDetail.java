package org.cocktail.fwkcktlgedguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsAttachment;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsUrl;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.attachment.model.ERAttachment;

public class CktlGedDocumentDetail extends CktlAjaxWOComponent {

	private static String BINDING_gedDocument = "gedDocument";
	protected static final String BINDING_TYPES = "types";
	protected static final String BINDING_FILTRE_TYPES = "filtreTypes";

	private EODocument gedDocument;
	protected NSArray<EOTypeDocument> types;
	protected EOTypeDocument unType;

	public CktlGedDocumentDetail(WOContext context) {
        super(context);
    }

	/**
	 * @return the gedDocument
	 */
	public EODocument gedDocument() {
		return (EODocument)valueForBinding(BINDING_gedDocument);
	}

	/**
	 * @param gedDocument the gedDocument to set
	 */
	public void setGedDocument(EODocument gedDocument) {
		this.gedDocument = gedDocument;
	}
	/**
	 * @return the types
	 */
	public NSArray<EOTypeDocument> types() {
		if (types == null) {
			if (hasBinding(BINDING_TYPES)) {
				types = (NSArray<EOTypeDocument>)valueForBinding(BINDING_TYPES);
			} else {
				types = EOTypeDocument.fetchAll(edc());
			}
			if (types != null && types.count()>0 && hasBinding(BINDING_FILTRE_TYPES)) {
				EOKeyValueQualifier qual = new EOKeyValueQualifier(EOTypeDocument.TD_STR_ID_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"+(String)valueForBinding(BINDING_FILTRE_TYPES)+"*");
				types = EOQualifier.filteredArrayWithQualifier(types, qual);
			}
		}
		return types;
	}
	
	/**
	 * @param types the types to set
	 */
	public void setTypes(NSArray<EOTypeDocument> types) {
		this.types = types;
	}

	/**
	 * @return the unType
	 */
	public EOTypeDocument unType() {
		return unType;
	}

	/**
	 * @param unType the unType to set
	 */
	public void setUnType(EOTypeDocument unType) {
		this.unType = unType;
	}

	public String famille() {
		String famille = null;
		EODocAsAttachment doc = (EODocAsAttachment)gedDocument();
		ERAttachment attachment = doc.toErAttachment();
		if (attachment != null) {
			famille = attachment.configurationName();
		}
		return famille;
	}

	public String url() {
		String url = null;
		EODocAsUrl doc = (EODocAsUrl)gedDocument();
		url = doc.toStockageUrl().surlUrl();
		return url;
	}

	public void setUrl(String url) {
	    EODocAsUrl doc = (EODocAsUrl)gedDocument();
	    doc.toStockageUrl().setSurlUrl(url);
	}

}