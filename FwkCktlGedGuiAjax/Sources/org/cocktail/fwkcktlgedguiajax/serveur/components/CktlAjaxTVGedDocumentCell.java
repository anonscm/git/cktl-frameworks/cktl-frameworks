package org.cocktail.fwkcktlgedguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTVCell;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;

import com.webobjects.appserver.WOContext;

public class CktlAjaxTVGedDocumentCell extends CktlAjaxTVCell {
	
	private EODocument gedDocument;
	private String famille;
	
    public CktlAjaxTVGedDocumentCell(WOContext context) {
        super(context);
    }
    
    @Override
    public void reset() {
    	super.reset();
    	gedDocument = null;
    }

	/**
	 * @return the gedDocument
	 */
	public EODocument gedDocument() {
		return gedDocument;
	}

	/**
	 * @param gedDocument the gedDocument to set
	 */
	public void setGedDocument(EODocument gedDocument) {
		this.gedDocument = gedDocument;
	}

	/**
	 * @return the famille
	 */
	public String famille() {
		return famille;
	}

	/**
	 * @param famille the famille to set
	 */
	public void setFamille(String famille) {
		this.famille = famille;
	}

}