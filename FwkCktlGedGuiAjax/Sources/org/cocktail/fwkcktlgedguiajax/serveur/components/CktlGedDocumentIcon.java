package org.cocktail.fwkcktlgedguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsAttachment;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsUrl;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;

import com.webobjects.appserver.WOContext;

import er.attachment.components.ERAttachmentIcon;
import er.attachment.model.ERAttachment;

public class CktlGedDocumentIcon extends CktlAjaxWOComponent {

	private static final String BINDING_gedDocument = "gedDocument";
	private static final String BINDING_famille = "famille";
	private static final String BINDING_SHOW_FILENAME = "showFileName";
	private EODocument gedDocument;
	private String famille, configurationName;
	private String title, target;
	
	public CktlGedDocumentIcon(WOContext context) {
        super(context);
    }

	@Override
	public void reset() {
		super.reset();
		gedDocument = null;
	}
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return false;
    }
    @Override
    public boolean isStateless() {
        return true;
    }
	/**
	 * @return the gedDocument
	 */
	public EODocument gedDocument() {
		return (EODocument)valueForBinding(BINDING_gedDocument);
	}

	/**
	 * @param gedDocument the gedDocument to set
	 */
	public void setGedDocument(EODocument gedDocument) {
		this.gedDocument = gedDocument;
		setValueForBinding(gedDocument, BINDING_gedDocument);
	}

	public ERAttachment attachment() {
		ERAttachment attachment = null;
		
		if (gedDocument() != null && gedDocument().isFile()) {
			EODocAsAttachment gedDocAsAttachment = (EODocAsAttachment)gedDocument();
			attachment = gedDocAsAttachment.toErAttachment();
		}
		
		return attachment;
	}
	public String iconPath() {
		String iconPath = null;
		
		if (attachment() != null) {
			iconPath = ERAttachmentIcon.iconPath(attachment(), valueForBinding("size"));
		}
		return iconPath;
	}

	public String gedDocumentUrl() {
		String gedDocumentUrl = null;
		
		if (gedDocument() != null && gedDocument().isUrl()) {
			EODocAsUrl gedDocAsUrl = (EODocAsUrl)gedDocument();
			gedDocumentUrl = gedDocAsUrl.toStockageUrl().surlUrl();
		}
		
		return gedDocumentUrl;
	}

	/**
	 * @return the famille
	 */
	public String famille() {
		return (String)valueForBinding(BINDING_famille);
	}

	/**
	 * @param famille the famille to set
	 */
	public void setFamille(String famille) {
		this.famille = famille;
		setValueForBinding(famille, BINDING_famille);
	}

	/**
	 * @return the configurationName
	 */
	public String configurationName() {
		return (String)valueForBinding("configurationName");
//		return configurationName;
	}

	/**
	 * @param configurationName the configurationName to set
	 */
	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
		setValueForBinding(configurationName, "configurationName");
	}

	/**
	 * @return the title
	 */
	public String title() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the target
	 */
	public String target() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}
	
	public boolean showFileName() {
	    return booleanValueForBinding(BINDING_SHOW_FILENAME, Boolean.FALSE);
	}
	
}