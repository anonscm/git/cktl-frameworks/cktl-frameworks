package org.cocktail.fwkcktlgedguiajax.serveur.components;

import java.io.File;
import java.io.IOException;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;
import org.cocktail.fwkcktlgedguiajax.serveur.controlers.CktlGedCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUploadProgress;
import er.extensions.foundation.ERXStringUtilities;

public class CktlGedDocumentGestion extends CktlAjaxWOComponent {

	protected static final String BINDING_editingContext = "editingContext";
	protected static final String BINDING_UTILISATEUR = "utilisateur";
	protected static final String BINDING_TYPES = "types";
	protected static final String BINDING_FILTRE_TYPES = "filtreTypes";
	protected static final String BINDING_FAMILLE = "famille";
	protected static final String BINDING_DOCUMENT = "document";
	protected static final String BINDING_ACTION_AJOUTER = "ajouter";
	protected static final String BINDING_ACTION_ANNULER = "annuler";
  protected static final String BINDING_WANT_REFRESH = "wantRefresh";

	protected EOEditingContext edtCtx;
	protected ApplicationUser utilisateur;
	protected NSArray<EOTypeDocument> types;
	protected EOTypeDocument unType, leType;
	protected String famille;
	protected EODocument cktlDocument;

	protected boolean isRBFichierChecked = true;
	protected boolean isRBURLChecked = false;

//	public NSData _data;
	public String _finalFilePath, _filePath;
	public AjaxUploadProgress _uploadProgress;
	protected String url;
	private String objet;
	private String commentaire;
	private String motsClefs;

	public CktlGedDocumentGestion(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
	  if(wantRefresh()) {
	   
	    setLeType(null);
	    setMotsClefs(null);
	    setObjet(null);
	    setUrl(null);
	    setCommentaire(null);
	    setCktlDocument(null);
	   
	    setWantRefresh(false);
	    
	  }
	  super.appendToResponse(response, context);
	}
	
	@Override
	public void reset() {
		super.reset();
		edtCtx = null;
		isRBFichierChecked = true;
		isRBURLChecked = false;
//		_data = null;
		_finalFilePath = null;
		_filePath = null;
		_uploadProgress = null;
		url = null;
		famille = null;
		types = null;
		unType = null;
		leType = null;
		cktlDocument = null;
		objet = null;
		commentaire = null;
		motsClefs = null;
	}

	public Boolean wantRefresh() {
	  return booleanValueForBinding(BINDING_WANT_REFRESH, false);
	}
	
	public void setWantRefresh(Boolean wantRefresh) {
	  setValueForBinding(wantRefresh, BINDING_WANT_REFRESH);
	}
	
	public WOActionResults annuler() {
		WOActionResults result = null;
		if (hasBinding(BINDING_ACTION_ANNULER)) {
			result = performParentAction((String) valueForBinding(BINDING_ACTION_ANNULER));
		}
		return result;
	}

	public WOActionResults valider() {
		WOActionResults result = null;
		try {
			ApplicationUser utilisateur = new ApplicationUser(edtCtx(), utilisateur().getPersId());
			File file = null;
			if (_finalFilePath != null) {
				file = new File(_finalFilePath);
			}
			EODocument document = CktlGedCtrl.deposerUnDocument(
			                edtCtx(), utilisateur, famille(), leType(), url(), file, _filePath, null, new NSTimestamp(), objet, commentaire, motsClefs, famille());
			setCktlDocument(document);
			if (hasBinding(BINDING_ACTION_AJOUTER)) {
				result = performParentAction((String) valueForBinding(BINDING_ACTION_AJOUTER));
			}
		} catch (IOException e) {
			CktlWebApplication.log.error(e.getMessage(), e);
			mySession().addSimpleErrorMessage("Ged", "Une erreur est survenue lors de l'enregistrement du fichier");
		}
		return result;
	}

	public String finalFileName() {
		return _finalFilePath == null ? "" : _finalFilePath;
	}

	public WOActionResults uploadSucceded() {
		return null;
	}

	public String failedFunction() {
		String failedFunction = null;
		failedFunction = "alert('Pour une raison technique, le serveur de gestion de documents est actuellement indisponible.Veuillez re-essayer plus tard.')";
		return failedFunction;
	}

	public WOActionResults uploadFinished() {
		System.out.println("FileUploadExample.uploadFinished: FINISHED!");
		return null;
	}

	public WOActionResults finishedAction() {
		return null;
	}

	public WOActionResults selectionnerFichier() {
		isRBURLChecked = false;
		isRBFichierChecked = true;
		url = null;
		return null;
	}

	public WOActionResults selectionnerURL() {
		isRBURLChecked = true;
		isRBFichierChecked = false;
//		_data = null;
		_finalFilePath = null;
		_filePath = null;
		return null;
	}

	public boolean isSupportFichier() {
		return isRBFichierChecked;
	}

	public boolean isSupportURL() {
		return isRBURLChecked;
	}

	/**
	 * @return the url
	 */
	public String url() {
		if (url == null) {
			url = "http://";
		}
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the types
	 */
	public NSArray<EOTypeDocument> types() {
		if (types == null) {
			if (hasBinding(BINDING_TYPES)) {
				types = (NSArray<EOTypeDocument>) valueForBinding(BINDING_TYPES);
			}
			else {
				types = EOTypeDocument.fetchAll(edtCtx());
			}
			if (types != null && types.count() > 0 && hasBinding(BINDING_FILTRE_TYPES)) {
				EOKeyValueQualifier qual = new EOKeyValueQualifier(EOTypeDocument.TD_STR_ID_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + (String) valueForBinding(BINDING_FILTRE_TYPES) + "*");
				types = EOQualifier.filteredArrayWithQualifier(types, qual);
			}
		}
		return types;
	}

	/**
	 * @param types the types to set
	 */
	public void setTypes(NSArray<EOTypeDocument> types) {
		this.types = types;
	}

	/**
	 * @return the utilisateur
	 */
	public ApplicationUser utilisateur() {
		if (utilisateur == null) {
			utilisateur = (ApplicationUser) valueForBinding(BINDING_UTILISATEUR);
		}
		return utilisateur;
	}

	/**
	 * @param utilisateur the utilisateur to set
	 */
	public void setUtilisateur(ApplicationUser utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @return the famille
	 */
	public String famille() {
		return (String) valueForBinding(BINDING_FAMILLE);
	}

	/**
	 * @param famille the famille to set
	 */
	public void setFamille(String famille) {
		this.famille = famille;
	}

	/**
	 * @return the unType
	 */
	public EOTypeDocument unType() {
		return unType;
	}

	/**
	 * @param unType the unType to set
	 */
	public void setUnType(EOTypeDocument unType) {
		this.unType = unType;
	}

	/**
	 * @return the leType
	 */
	public EOTypeDocument leType() {
		return leType;
	}

	/**
	 * @param leType the leType to set
	 */
	public void setLeType(EOTypeDocument leType) {
		this.leType = leType;
	}

	/**
	 * @return the edtCtx
	 */
	public EOEditingContext edtCtx() {
		if (edtCtx == null) {
			if (hasBinding(BINDING_editingContext)) {
				edtCtx = (EOEditingContext) valueForBinding(BINDING_editingContext);
			}
		}
		return edtCtx;
	}

	/**
	 * @param edtCtx the edtCtx to set
	 */
	public void setEdtCtx(EOEditingContext edtCtx) {
		this.edtCtx = edtCtx;
	}

	public boolean isBtnValiderDisabled() {
		boolean isBtnValiderDisabled = true;

		isBtnValiderDisabled = (leType() == null || getObjet() == null ||
				(isSupportFichier() && (_finalFilePath == null || ERXStringUtilities.stringIsNullOrEmpty(_finalFilePath))) ||
				(isSupportURL() && ERXStringUtilities.stringIsNullOrEmpty(url())));

		return isBtnValiderDisabled;
	}

	/**
	 * @return the isRBFichierChecked
	 */
	public boolean isRBFichierChecked() {
		return isRBFichierChecked;
	}

	/**
	 * @param isRBFichierChecked the isRBFichierChecked to set
	 */
	public void setIsRBFichierChecked(boolean isRBFichierChecked) {
		this.isRBFichierChecked = isRBFichierChecked;
	}

	/**
	 * @return the isRBURLChecked
	 */
	public boolean isRBURLChecked() {
		return isRBURLChecked;
	}

	/**
	 * @param isRBURLChecked the isRBURLChecked to set
	 */
	public void setIsRBURLChecked(boolean isRBURLChecked) {
		this.isRBURLChecked = isRBURLChecked;
	}

	/**
	 * @return the cktlDocument
	 */
	public EODocument cktlDocument() {
		return (EODocument) valueForBinding(BINDING_DOCUMENT);
	}

	/**
	 * @param cktlDocument the cktlDocument to set
	 */
	public void setCktlDocument(EODocument cktlDocument) {
		this.cktlDocument = cktlDocument;
		setValueForBinding(cktlDocument, BINDING_DOCUMENT);
	}

	public String getFormId() {
		return getComponentId() + "_FormGedDocumentGestion";
	}

	public String getContainerMenuId() {
		return getComponentId() + "_ContainerCktlGedDocumentGestionMenu";
	}

	public String getContainerTypeSupportId() {
		return getComponentId() + "_ContainerTypeSupportDocument";
	}

	public String getPopupTypeId() {
		return getComponentId() + "_PopUpTypes";
	}

	public String getRBFichierId() {
		return getComponentId() + "_RBFichier";
	}

	public String getRBURLid() {
		return getComponentId() + "_RBURL";
	}

	public String getTFUrlId() {
		return getComponentId() + "_TFUrl";
	}

	public String getFileUploadId() {
		return getComponentId() + "_FileUpload";
	}

	public String commentaireId() {
	    return getComponentId() + "_Commentaire";
	}
	
	public String objetId() {
	    return getComponentId() + "_Objet";
	}
	
	public String motsClefsId() {
	    return getComponentId() + "_MotsClefs";
	}
	
	public String getJSContainerMenuUpdate() {
		return getContainerMenuId() + "Update";
	}

	public String getJSContainerMenuUpdateInFunction() {
		return "function oc(){" + getContainerMenuId() + "Update();}";
	}

	public String succeededFunction() {
		return "function() { " + getContainerMenuId() + "Update(); }";
	}
	
	public String getCommentaire() {
        return commentaire;
    }
	
	public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
	
	public String getMotsClefs() {
        return motsClefs;
    }
	
	public void setMotsClefs(String motsClefs) {
        this.motsClefs = motsClefs;
    }
	
	public String getObjet() {
        return objet;
    }
	
	public void setObjet(String objet) {
        this.objet = objet;
    }
	
}