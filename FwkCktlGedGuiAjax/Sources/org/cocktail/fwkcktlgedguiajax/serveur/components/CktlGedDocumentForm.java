package org.cocktail.fwkcktlgedguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

/**
 * 
 * @author jlafourc
 *
 */
public class CktlGedDocumentForm extends CktlAjaxWOComponent {

	
	private static final long serialVersionUID = 4101968329070861232L;

	private EOTypeDocument currentType;
	
    /**
     * @param context paramètre obligatoire 
     */
	public CktlGedDocumentForm(WOContext context) {
        super(context);
    }

	public EOTypeDocument getCurrentType() {
		return currentType;
	}

	public void setCurrentType(EOTypeDocument currentType) {
		this.currentType = currentType;
	}
    
	public String succededFunction() {
		return "function() { " + updateContainerID() + "Update(); }";
	}
	
}