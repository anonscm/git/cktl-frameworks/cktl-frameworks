package org.cocktail.fwkcktlgedguiajax.serveur.controlers;

import java.io.File;
import java.io.IOException;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsAttachment;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsUrl;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOStockageUrl;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.attachment.model.ERAttachment;
import er.attachment.processors.ERAttachmentProcessor;
import er.extensions.foundation.ERXProperties;

public class CktlGedCtrl {

	public CktlGedCtrl() {
		super();
	}

	public static EODocument deposerUnDocument(EOEditingContext edc, ApplicationUser utilisateur, 
	        String famille, EOTypeDocument type, String url, File uploadedFile, String filePath, String mimeType,
	        NSTimestamp dateCreation, String objet, String commentaire, String motsClefs, String configurationName) 
	throws IOException {
	    EODocument cktlDocument = null;
	    if (uploadedFile != null) {
	        String storageType = ERXProperties.stringForKey("er.attachment." + configurationName + ".storageType");
	        int width = ERXProperties.intForKeyWithDefault("er.attachment." + configurationName + ".width", -1);
	        int height = ERXProperties.intForKeyWithDefault("er.attachment." + configurationName + ".height", -1);
	        // ERAttachment attachment = ERAttachmentProcessor.processorForType("file").process(edc, uploadedFile, filePath, mimeType, famille, utilisateur.getPersId().toString());			
	        ERAttachment attachment = ERAttachmentProcessor.processorForType(storageType).process(edc, uploadedFile, filePath, mimeType, width, height, configurationName, utilisateur.getPersId().toString());
	        cktlDocument = EODocAsAttachment.creer(edc, utilisateur.getPersId(), type, attachment);
	    } else {
	        EOStockageUrl stockageUrl = EOStockageUrl.creer(edc, url);
	        cktlDocument = EODocAsUrl.creer(edc, utilisateur.getPersId(), type, stockageUrl);
	    }
	    cktlDocument.setDCreation(dateCreation);
	    cktlDocument.setCommentaire(commentaire);
	    cktlDocument.setMotsClefs(motsClefs);
	    cktlDocument.setObjet(objet);
	    return cktlDocument;
	}
	
	
	public static boolean supprimerUnDocument(EODocument document) {
		boolean suppression = true;
		try {
			document.delete();
		} catch (Exception e) {
			suppression = false;
		}
		return suppression;
	}
	
	public static boolean archiverUnDocument(EODocument document) {
		boolean archivage = true;
		// TODO A coder : Regles a appliquer pour l'archivage du document?
		return archivage;
	}
}
