package org.cocktail.fwkcktldepenseguiajax.serveur.filtre;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreBean;
import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreConverter;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier._IDepenseExtourneCredits;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

public class EnveloppeExtourneFiltreConverter implements CktlDepenseFiltreConverter<Collection<EOQualifier>> {

	/**
	 * Constructeur par défaut.
	 */
	public EnveloppeExtourneFiltreConverter() {
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<EOQualifier> convert(List<CktlDepenseFiltreBean> filtres) {
		List<EOQualifier> qualsFiltres = new ArrayList<EOQualifier>();
		for (CktlDepenseFiltreBean currentFiltre : filtres) {
			if (!currentFiltre.isEmpty()) {
				EOQualifier filtreQualBuilt = buildFiltre(currentFiltre);
				if (filtreQualBuilt != null) {
					qualsFiltres.add(filtreQualBuilt);
				}
			}
		}
		return qualsFiltres;
	}

	private EOQualifier buildFiltre(CktlDepenseFiltreBean filtre) {
		EOQualifier filtreQual = null;
		FiltresEnveloppeExtourne currentFiltreKey = FiltresEnveloppeExtourne.valueOf(filtre.getKey());
		String filtreValue = filtre.getValue();
		switch (currentFiltreKey) {
		case UB:
			filtreQual = buildFiltreUb(filtreValue);
			break;
		case CR:
			filtreQual = buildFiltreCr(filtreValue);
			break;
		case SEULEMENT_DISPONIBLE:
			Boolean seulementDisponibleAsBoolean = Boolean.valueOf(filtreValue);
			if (Boolean.TRUE.equals(seulementDisponibleAsBoolean)) {
				filtreQual = buildFiltreSeulementDisponible();
			}
			break;
		default:
		}
		return filtreQual;
	}

	private EOQualifier buildFiltreUb(String ub) {
		return ERXQ.is(_IDepenseExtourneCredits.TO_ORGAN.append(EOOrgan.ORG_UB).key(), ub);
	}

	private EOQualifier buildFiltreCr(String cr) {
		return ERXQ.is(_IDepenseExtourneCredits.TO_ORGAN.append(EOOrgan.ORG_CR).key(), cr);
	}

	private EOQualifier buildFiltreSeulementDisponible() {
		return ERXQ.lessThan(_IDepenseExtourneCredits.VEC_MONTANT_BUD_DISPO_REEL_KEY, BigDecimal.ZERO);
	}
}
