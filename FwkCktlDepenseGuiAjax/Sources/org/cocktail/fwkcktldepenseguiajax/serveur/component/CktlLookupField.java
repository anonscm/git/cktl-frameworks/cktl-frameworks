/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * Composant qui permet de filtrer une liste (balise html SELECT). Ajouter le composant et insérer la liste à l'intérieur.
 * 
 * @binding pattern permet de définir une expression réguglière de recherche (cette expression doit contenir <value> aux pour prendre en compte la
 *          valeur du champ récupérée.
 * @author rprin
 */
public class CktlLookupField extends WOComponent {

	private static final long serialVersionUID = 1L;
	public String filterText;

	public CktlLookupField(WOContext context) {
		super(context);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public boolean isStateless() {
		return true;
	}

	public void reset() {
		filterText = null;
	}

	/*
	 * public void appendToResponse(WOResponse res, WOContext ctx) { StringBuffer str = new StringBuffer();
	 * 
	 * str.append("<script type=\"text/javascript\">\n// <![CDATA[\n"); str.append("var filt"); str.append(selectName());
	 * str.append("=new filterlist(document."); str.append(formName()); str.append("."); str.append(selectName()); str.append(");\n");
	 * str.append("filt"); str.append(selectName()); str.append(".set(document."); str.append(formName()); str.append("."); str.append(fieldName());
	 * str.append(".value"); str.append(");\n// ]]>\n</script>\n");
	 * 
	 * res.appendContentString(String.valueOf(str)); super.appendToResponse(res, ctx); }
	 */

	public String filtre() {
		StringBuffer str = new StringBuffer();

		str.append("if ( document.getElementById('" + selectId() + "')!=null ) {\n");
		str.append("filt");
		str.append(selectId());
		str.append("=new filterlist2(document.getElementById('");
		str.append(selectId());
		str.append("'));\n");
		str.append("};\n");
		str.append("filt");
		str.append(selectId());
		str.append(".set(");
		str.append(getCompiledPattern());
		/*
		 * str.append("document.getElementById('"); str.append(fieldId()); str.append("'"); str.append(").value,");
		 */
		str.append(",");
		if (optionAttributetoTest() != null) {
			str.append("'" + optionAttributetoTest() + "'");
		}
		else {
			str.append("null");
		}
		str.append(");");
		return String.valueOf(str);
	}

	private String getCompiledPattern() {
		String pattern = "'" + optionPatternTest() + "'";
		String value = "document.getElementById('" +
				fieldId() +
				"'" +
				").value";
		if (pattern == null || pattern.equals("'null'")) {
			pattern = value;
		}
		else {
			pattern = pattern.replaceAll("<value>", "'+" + value + "+'");
		}
		return pattern;
	}

	public String onKeyUp() {
		String res = "filt" + selectId() + ".set(" + getCompiledPattern() + ",";
		if (optionAttributetoTest() != null) {
			res += ("'" + optionAttributetoTest() + "'");
		}
		else {
			res += ("null");
		}
		res += ");";
		return res;
		//return "filt" + selectId() + ".set(this.value, "+   +");";
	}

	public String fieldId() {
		return (String) valueForBinding("fieldId");
	}

	public String selectId() {
		return (String) valueForBinding("selectId");
	}

	public String optionAttributetoTest() {
		return (String) valueForBinding("optionAttributetoTest");
	}

	public String optionPatternTest() {
		return (String) valueForBinding("pattern");
	}

}
