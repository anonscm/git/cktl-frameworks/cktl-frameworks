/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXArrayUtilities;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @binding unInventaire
 * @binding ajouterInventairesAction
 * @binding supprimerInventaireAction
 * @binding planComptables
 * @binding factoryDepenseControlePlanComptable
 * @binding unDepenseCtrlPlanComptable
 * @binding isInventaireModifiable
 */
public class CktlRepartitionDepensePlanComptable extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_depenseBudgets = "depenseBudgets";
	private static final String BINDING_unDepenseBudget = "unDepenseBudget";
	private static final String BINDING_factoryDepenseControlePlanComptable = "factoryDepenseControlePlanComptable";
	private static final String BINDING_planComptables = "planComptables";
	private static final String BINDING_ajouterInventairesAction = "ajouterInventairesAction";
	private static final String BINDING_supprimerInventairesAction = "supprimerInventaireAction";
	private static final String BINDING_unInventaire = "unInventaire";
	private static final String BINDING_showInventaire = "showInventaire";
	private static final String BINDING_isInventaireModifiable = "isInventaireModifiable";
	private static final String BINDING_unDepenseCtrlPlanComptable = "unDepenseCtrlPlanComptable";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";
	private static final String BINDING_codesNomenclatureDepense = "codesNomenclatureDepense";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";
	private static final String DEFAULT_codesNomenclatureDepense = "";

	private static final String LIBELLE_COMPTES_DERIVES = "Comptes derivés des codes achats";
	private static final String LIBELLE_COMPTES_NON_DERIVES = "Autres comptes";
	
	private _IDepenseBudget unDepenseBudget;
	private _IDepenseControlePlanComptable unDepenseCtrlPlanComptable;
	private EOPlanComptable unPlanComptable;
	private String filterText;
	//	private EOInventaire unInventaire = null;
	private boolean modificationEnCours;
	
	public CktlRepartitionDepensePlanComptable(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<_IDepenseBudget> getDepenseBudgets() {
		return (NSArray<_IDepenseBudget>) valueForBinding(BINDING_depenseBudgets);
	}

	public _IDepenseBudget getUnDepenseBudget() {
		return unDepenseBudget;
	}

	public void setUnDepenseBudget(_IDepenseBudget unDepenseBudget) {
		this.unDepenseBudget = unDepenseBudget;
		if (hasBinding(BINDING_unDepenseBudget)) {
			setValueForBinding(unDepenseBudget, BINDING_unDepenseBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOPlanComptable> getLesPlanComptables() {
		NSArray<EOPlanComptable> res = ERXArrayUtilities.sortedArraySortedWithKey((NSArray<EOPlanComptable>) valueForBinding(BINDING_planComptables), EOPlanComptable.PCO_NUM_KEY);
		NSMutableArray<EOPlanComptable> comptesDerives = new NSMutableArray<EOPlanComptable>();
		NSMutableArray<EOPlanComptable> comptesNonDerives = new NSMutableArray<EOPlanComptable>();
		NSArray<EOCodeExer> codesNomenclatureDepense = (NSArray<EOCodeExer>) valueForBinding(BINDING_codesNomenclatureDepense);
		for (EOPlanComptable eoPlanComptable : res) {
			if (ERXArrayUtilities.arrayContainsAnyObjectFromArray(codesNomenclatureDepense, eoPlanComptable.getCodesExerDerives())) {
				comptesDerives.add(eoPlanComptable);
			} else {
				comptesNonDerives.add(eoPlanComptable);
			}
		}
		return ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(comptesDerives, comptesNonDerives);
	}

	public _IFactoryDepenseControlePlanComptable getFactoryDepenseControlePlanComptable() {
		return (_IFactoryDepenseControlePlanComptable) valueForBinding(BINDING_factoryDepenseControlePlanComptable);
	}

	public _IDepenseControlePlanComptable getUnDepenseCtrlPlanComptable() {
		return unDepenseCtrlPlanComptable;
	}

	public void setUnDepenseCtrlPlanComptable(_IDepenseControlePlanComptable unDepenseCtrlPlanComptable) {
		this.unDepenseCtrlPlanComptable = unDepenseCtrlPlanComptable;
		setValueForBinding(unDepenseCtrlPlanComptable, BINDING_unDepenseCtrlPlanComptable);
	}

	public EOPlanComptable getSelection() {
		return getUnDepenseCtrlPlanComptable().planComptable();
	}

	public void setSelection(EOPlanComptable selection) {
		NSArray<? extends _IDepenseControlePlanComptable> res = getLesDepenseCtrlPlanComptables();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOPlanComptable> actions = (NSArray<EOPlanComptable>) getLesDepenseCtrlPlanComptables().valueForKeyPath(_IDepenseControlePlanComptable._PLAN_COMPTABLE_KEY);
			if (!actions.containsObject(selection)) {
				if (getUnDepenseCtrlPlanComptable() != null) {
					getUnDepenseCtrlPlanComptable().setPlanComptableRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnDepenseCtrlPlanComptable() != null) {
			getFactoryDepenseControlePlanComptable()._supprimer(getUnDepenseBudget().editingContext(), getUnDepenseCtrlPlanComptable());
		}
		return null;
	}

	public NSArray<? extends _IDepenseControlePlanComptable> getLesDepenseCtrlPlanComptables() {
		EOEditingContext edc = getUnDepenseBudget().editingContext();
		NSArray<? extends _IDepenseControlePlanComptable> res = getUnDepenseBudget().depenseControlePlanComptables();
		if (res == null || res.count() < 1) {
			getFactoryDepenseControlePlanComptable()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControlePlanComptable(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnDepenseBudget().exercice(), getUnDepenseBudget());
			res = getUnDepenseBudget().depenseControlePlanComptables();
		}
		else {
			if (getLesPlanComptables() != null && getLesPlanComptables().count() > 0) {
				_IDepenseControlePlanComptable ctrlePlanComptableVide = null;
				EOQualifier qual = new EOKeyValueQualifier(_IDepenseControlePlanComptable._PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, null);
				NSArray<? extends _IDepenseControlePlanComptable> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrlePlanComptableVide = (_IDepenseControlePlanComptable) arrayVides.lastObject();
				}
				//				qual = EOQualifier.qualifierWithQualifierFormat("planComptable != nil", null);
				qual = new EONotQualifier(qual);
				NSArray<? extends _IDepenseControlePlanComptable> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum.pourcentage");
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrlePlanComptableVide == null) {
						getFactoryDepenseControlePlanComptable()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControlePlanComptable(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnDepenseBudget().exercice(),
								getUnDepenseBudget());
						res = getUnDepenseBudget().depenseControlePlanComptables();
					}
					else {
						ctrlePlanComptableVide.setDpcoTtcSaisie(getUnDepenseBudget().restantTtcControlePlanComptable());
						ctrlePlanComptableVide.setPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrlePlanComptableVide != null) {
						Enumeration<? extends _IDepenseControlePlanComptable> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrlePlanComptableVide = (_IDepenseControlePlanComptable) enumArrayVides.nextElement();
							getFactoryDepenseControlePlanComptable()._supprimer(edc, ctrlePlanComptableVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnPlanComptable(EOPlanComptable unPlanComptable) {
		this.unPlanComptable = unPlanComptable;
	}

	public EOPlanComptable getUnPlanComptable() {
		return unPlanComptable;
	}

	public boolean isInventaireIncompletEtObligatoire() {
		boolean isInventaireIncompletEtObligatoire = true;
		//EODepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		_IDepenseControlePlanComptable dcpc = getUnDepenseCtrlPlanComptable();
		if (dcpc.isInventaireObligatoire() && dcpc.isGood()) {
			isInventaireIncompletEtObligatoire = false;
		}
		return isInventaireIncompletEtObligatoire;
	}

	public EOInventaire getUnInventaire() {
		return (EOInventaire) valueForBinding(BINDING_unInventaire);
	}

	public void setUnInventaire(EOInventaire unInventaire) {
		setValueForBinding(unInventaire, BINDING_unInventaire);
	}

	public BigDecimal getUnInventaireMontant() {
		return getUnInventaire().lidMontant();
	}

	public void setUnInventaireMontant(BigDecimal unMontant) {
		if (modificationEnCours == false && unMontant != null) {
			BigDecimal oldValue = getUnInventaireMontant();
			if ((oldValue != null && oldValue.floatValue() != unMontant.floatValue()) ||
					(oldValue == null && unMontant != null)) {
				getUnInventaire().setLidMontant(unMontant);
				modificationEnCours = true;
			}
		}
	}

	public BigDecimal getUnPreInventaireMontant() {
		NSArray<EOPreDepenseControlePlanComptableInventaire> res = ((EOPreDepenseControlePlanComptable) getUnDepenseCtrlPlanComptable()).preDepenseControlePlanComptableInventaires(new EOKeyValueQualifier(EOPreDepenseControlePlanComptableInventaire.INVENTAIRE_KEY, EOQualifier.QualifierOperatorEqual,
				getUnInventaire()), false);
		if (res.count() > 0) {
			return ((EOPreDepenseControlePlanComptableInventaire) res.objectAtIndex(0)).pdpinMontantBudgetaire();
		}
		return null;
	}

	public void setUnPreInventaireMontant(BigDecimal unMontant) {
		if (modificationEnCours == false && unMontant != null) {
			BigDecimal oldValue = getUnPreInventaireMontant();
			if ((oldValue != null && oldValue.floatValue() != unMontant.floatValue()) ||
					(oldValue == null && unMontant != null)) {
				NSArray<EOPreDepenseControlePlanComptableInventaire> res = ((EOPreDepenseControlePlanComptable) getUnDepenseCtrlPlanComptable()).preDepenseControlePlanComptableInventaires(new EOKeyValueQualifier(EOPreDepenseControlePlanComptableInventaire.INVENTAIRE_KEY,
						EOQualifier.QualifierOperatorEqual, getUnInventaire()), false);
				if (res.count() > 0) {
					((EOPreDepenseControlePlanComptableInventaire) res.objectAtIndex(0)).setPdpinMontantBudgetaire(unMontant);
				}
				modificationEnCours = true;
			}
		}
	}

	public WOActionResults ajouterInventaires() {
		return performParentAction((String) valueForBinding(BINDING_ajouterInventairesAction));
	}

	public WOActionResults supprimerInventaire() {
		return performParentAction((String) valueForBinding(BINDING_supprimerInventairesAction));
	}

	@Override
	public WOActionResults doNothing() {
		super.doNothing();
		modificationEnCours = false;
		return null;
	}

	public boolean isAjouterInventaireDisabled() {
		boolean isAjouterInventaireDisabled = true;
		//		EODepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		_IDepenseControlePlanComptable dcpc = getUnDepenseCtrlPlanComptable();
		if (dcpc != null && dcpc.isInventairesPossibles()) {
			isAjouterInventaireDisabled = false;
		}
		return isAjouterInventaireDisabled;
	}

	public Boolean showInventaire() {
		return booleanValueForBinding(BINDING_showInventaire, Boolean.TRUE) && getUnDepenseCtrlPlanComptable().isInventaireAffichable();
	}

	public Boolean isInventaireModifiable() {
		return booleanValueForBinding(BINDING_isInventaireModifiable, Boolean.TRUE);
	}

	@Override
	public String updateContainerID() {
		if (super.updateContainerID() != null) {
			return super.updateContainerID();
		}
		return getComponentId();
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}
	
	public String getCodesNomenclatureDepense() {
		return stringValueForBinding(BINDING_codesNomenclatureDepense, DEFAULT_codesNomenclatureDepense);
	}
	
	public boolean getObjetARepartirGroupeKeyPath() {
		NSArray<EOCodeExer> codesExerFromArticles = (NSArray<EOCodeExer>) valueForBinding(BINDING_codesNomenclatureDepense);
		NSArray<EOCodeExer> codesExerFromPlanCo = unPlanComptable.getCodesExerDerives();
		return ERXArrayUtilities.arrayContainsAnyObjectFromArray(codesExerFromArticles, codesExerFromPlanCo);
	}

	public String getObjetARepartirGroupeLabel() {
		return (getObjetARepartirGroupeKeyPath()) ? LIBELLE_COMPTES_DERIVES : LIBELLE_COMPTES_NON_DERIVES;
	}
}