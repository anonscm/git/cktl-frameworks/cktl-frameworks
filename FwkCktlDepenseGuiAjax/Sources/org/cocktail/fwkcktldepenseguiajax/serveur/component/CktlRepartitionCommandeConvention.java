/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class CktlRepartitionCommandeConvention extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_commandeBudgets = "commandeBudgets";
	private static final String BINDING_unCommandeBudget = "unCommandeBudget";
	private static final String BINDING_unCommandeCtrlConvention = "unCommandeCtrlConvention";
	private static final String BINDING_factoryCommandeControleConvention = "factoryCommandeControleConvention";
	private static final String BINDING_conventions = "conventions";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private EOCommandeBudget unCommandeBudget;
	//private EOCommandeControleConvention unCommandeCtrlConvention;
	private EOConvention unConvention;
	private String filterText;

	public CktlRepartitionCommandeConvention(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCommandeBudget> getCommandeBudgets() {
		return (NSArray<EOCommandeBudget>) valueForBinding(BINDING_commandeBudgets);
	}

	public EOCommandeBudget getUnCommandeBudget() {
		return unCommandeBudget;
	}

	public void setUnCommandeBudget(EOCommandeBudget unCommandeBudget) {
		this.unCommandeBudget = unCommandeBudget;
		if (hasBinding(BINDING_unCommandeBudget)) {
			setValueForBinding(unCommandeBudget, BINDING_unCommandeBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOConvention> getLesConventions() {
		return (NSArray<EOConvention>) valueForBinding(BINDING_conventions);
	}

	public FactoryCommandeControleConvention getFactoryCommandeControleConvention() {
		return (FactoryCommandeControleConvention) valueForBinding(BINDING_factoryCommandeControleConvention);
	}

	public EOCommandeControleConvention getUnCommandeCtrlConvention() {
		return (EOCommandeControleConvention) valueForBinding(BINDING_unCommandeCtrlConvention);
	}

	public void setUnCommandeCtrlConvention(EOCommandeControleConvention unCommandeCtrlConvention) {
		setValueForBinding(unCommandeCtrlConvention, BINDING_unCommandeCtrlConvention);
	}

	public EOConvention getSelection() {
		return getUnCommandeCtrlConvention().convention();
	}

	public void setSelection(EOConvention selection) {
		NSArray<EOCommandeControleConvention> res = getLesCommandeCtrlConventions();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOConvention> actions = (NSArray<EOConvention>) getLesCommandeCtrlConventions().valueForKeyPath(EOCommandeControleConvention.CONVENTION_KEY);
			if (!actions.containsObject(selection)) {
				if (getUnCommandeCtrlConvention() != null) {
					getUnCommandeCtrlConvention().setConventionRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnCommandeCtrlConvention() != null) {
			getFactoryCommandeControleConvention().supprimer(getUnCommandeBudget().editingContext(), getUnCommandeCtrlConvention());
		}
		return null;
	}

	public NSArray<EOCommandeControleConvention> getLesCommandeCtrlConventions() {
		EOEditingContext edc = getUnCommandeBudget().editingContext();
		NSArray<EOCommandeControleConvention> res = getUnCommandeBudget().commandeControleConventions();
		if (res == null || res.count() < 1) {
			getFactoryCommandeControleConvention().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControleConvention(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnCommandeBudget().exercice(), getUnCommandeBudget());
			res = getUnCommandeBudget().commandeControleConventions();
		}
		else {
			if (getLesConventions() != null && getLesConventions().count() > 0) {
				EOCommandeControleConvention ctrleConventionVide = null;
				//				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  "typeConvention = nil", null);
				//FIXME tester !
				EOQualifier qual = new EOKeyValueQualifier(EOCommandeControleConvention.CONVENTION_KEY, EOQualifier.QualifierOperatorEqual, null);
				NSArray<EOCommandeControleConvention> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleConventionVide = (EOCommandeControleConvention) arrayVides.lastObject();
				}
				//				qual = EOQualifier.qualifierWithQualifierFormat("convention != nil", null);
				qual = new EONotQualifier(qual);
				NSArray<EOCommandeControleConvention> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum." + EOCommandeControleConvention.CCON_POURCENTAGE_KEY);
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleConventionVide == null) {
						getFactoryCommandeControleConvention().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControleConvention(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnCommandeBudget().exercice(),
								getUnCommandeBudget());
						res = getUnCommandeBudget().commandeControleConventions();
					}
					else {
						ctrleConventionVide.setCconTtcSaisie(getUnCommandeBudget().restantTtcControleConvention());
						ctrleConventionVide.setCconPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleConventionVide != null) {
						Enumeration<EOCommandeControleConvention> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleConventionVide = (EOCommandeControleConvention) enumArrayVides.nextElement();
							getFactoryCommandeControleConvention().supprimer(edc, ctrleConventionVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnConvention(EOConvention unConvention) {
		this.unConvention = unConvention;
	}

	public EOConvention getUnConvention() {
		return unConvention;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

}