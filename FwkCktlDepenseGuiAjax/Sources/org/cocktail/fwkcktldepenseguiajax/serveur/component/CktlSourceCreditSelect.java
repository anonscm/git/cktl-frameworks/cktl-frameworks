/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlSourceCreditSelectCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

public class CktlSourceCreditSelect extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_ACTION_AJOUTER_CALLBACK = "actionAjouterCallback";
	public static final String BINDING_ACTION_ANNULER_CALLBACK = "actionAnnulerCallback";

	public CktlSourceCreditSelect(WOContext context) {
		super(context);
	}

	public String getContainerBoutonId() {
		return getComponentId() + "_containerBoutons";
	}

	public CktlSourceCreditSelectCtrl ctrl() {
		return (CktlSourceCreditSelectCtrl) valueForBinding(BINDING_ctrl);
	}

	public String getSourcesModalPanelId() {
		return getComponentId() + "_sourcesModalPanel";
	}

	public WOActionResults ajouterLesSourcesSelectionnees() {
		WOActionResults result = null;
		try {
			ctrl().ajouterLesSourcesSelectionnees();
			//ctrl().ajouterLesSourcesSelectionnees(edc(), getLesSourcesUtilisateurSelectionnees());
			if (hasBinding(BINDING_ACTION_AJOUTER_CALLBACK)) {
				result = (WOActionResults) valueForBinding(BINDING_ACTION_AJOUTER_CALLBACK);
			}
			if (!StringCtrl.isEmpty(updateContainerID())) {
				AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
			}
			CktlAjaxWindow.close(context(), getSourcesModalPanelId());

		} catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e);
		}

		return result;

	}

	public WOActionResults annuler() {
		WOActionResults result = null;
		try {
			ctrl().annuler();
			if (hasBinding(BINDING_ACTION_ANNULER_CALLBACK)) {
				result = (WOActionResults) valueForBinding(BINDING_ACTION_ANNULER_CALLBACK);
			}

			//			if (!StringCtrl.isEmpty(updateContainerID())) {
			//				AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
			//			}
		} catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e);
		} finally {
			CktlAjaxWindow.close(context(), getSourcesModalPanelId());
		}

		return result;
	}
}