/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepenseguiajax.serveur.FwkCktlDepenseGuiAjax;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSNumberFormatter;

import er.ajax.AjaxUtils;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Composant générique pour gérer des répartitions entre des objets de type A et plusieurs objets de type B avec montants et pourcentages. Plusieurs
 * objets de type A peuvent être regroupés au niveau de l'interface. Exemple d'utilisation : Répartition entre les engagements et les actions. Il est
 * possible d'insérer un contenu à lintérieur du composant (componentContent) pour ajouter des info pour chaque répartition. Exemple (les inventaires
 * pour les repartitions par planComptable).
 * 
 * @binding updateContainerID ID du container Ajax à mettre à jour lors d'une action
 * @binding isRepartitionGood Indique au composant si la répartition en cours est valide (affichage différent suivant le cas)
 * @binding objetsDuGroupe les objets permettant de regrouper les répartitions (par exemples les commandeBudgets)
 * @binding unObjetDuGroupe Settable. Instance de l'objet dans la répétition
 * @binding objetDuGroupeDisplayString Libellé du groupe.
 * @binding repartitionsExistantes Tableau renvoyant les répartitions déjà saisies.
 * @binding uneRepartitionExistante Settable. Instance de l'objet dans la répétition.
 * @binding objetsARepartir NSArray renvoyant l'ensemble des objets qui peuvent être répartis (par exemple les actions lolf)
 * @binding unObjetARepartir Settable. Instance de l'objet dans la liste deroulante.
 * @binding objetARepartirDisplayString Libelle de l'objet à répartir
 * @binding objetARepartirSelectionne Objet à répartir sélectionné dans la liste déroulante.
 * @binding repartPourcentageFormatter NSNumberFormatter du textfield pourcentage.
 * @binding isRepartPourcentageDisabled Boolean Permet de desactiver la saisie du pourcentage.
 * @binding repartPourcentage Pourcentage saisie.
 * @binding repartMontantFormatter NSNumberFormatter du textfield montant.
 * @binding isRepartMontantDisabled Boolean Permet de desactiver la saisie du pourcentage.
 * @binding repartMontant Montant saisi.
 * @binding filterText Filtre pour le champ de recherche
 * @binding supprimerRepartAction Nom d'une methode dans le composant qui retourne un WOActionResults. Appelée lors de l'appel à l suppression de la
 *          répartition
 * @binding repartitionLibelle Libelle à afficher comme titre du bloc contenant les répartitions
 * @binding id facultatif
 * @binding groupeTitre Libellé de la colonne des groupes. ATTENTION : Si vide, la colonne n'est pas affichée, les groupes n'apparaitront pas.
 * @binding repartMontantTitre Libellé de la colonne des montants (par défaut Montant)
 * @binding repartPourcentageTitre Libellé de la colonne des pourcentage (par défaut Pourcentage)
 * @binding objetsARepartirTitre Libellé de la colonne des objets à répartir (par défaut vide)
 * @binding isObjetARepartirModifiable facultatif
 * @binding objetARepartirGroupeKeyPath chemin vers la valeur qui change pour créer des groupes dans la liste déroulante des objets à répartir.
 * @binding objetARepartirGroupeLabel Chaine utilisée comme label pour le groupe
 * @binding visible Permet de masquer le composant si besoin
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlRepartitionForm extends ACktlDepenseGuiAjaxComponent {
	private static final long serialVersionUID = 1L;

	private static final String BINDING_isRepartitionGood = "isRepartitionGood";
	private static final String BINDING_repartitionsExistantes = "repartitionsExistantes";
	private static final String BINDING_objetsDuGroupe = "objetsDuGroupe";
	private static final String BINDING_objetDuGroupeDisplayString = "objetDuGroupeDisplayString";
	private static final String BINDING_objetsARepartir = "objetsARepartir";
	private static final String BINDING_objetARepartirDisplayString = "objetARepartirDisplayString";
	private static final String BINDING_objetARepartirSelectionne = "objetARepartirSelectionne";
	private static final String BINDING_repartPourcentageFormatter = "repartPourcentageFormatter";
	private static final String BINDING_isRepartPourcentageDisabled = "isRepartPourcentageDisabled";
	private static final String BINDING_repartPourcentage = "repartPourcentage";
	private static final String BINDING_repartMontantFormatter = "repartMontantFormatter";
	private static final String BINDING_isRepartMontantDisabled = "isRepartMontantDisabled";
	private static final String BINDING_repartMontant = "repartMontant";
	private static final String BINDING_supprimerRepartAction = "supprimerRepartAction";
	private static final String BINDING_unObjetDuGroupe = "unObjetDuGroupe";
	private static final String BINDING_uneRepartitionExistante = "uneRepartitionExistante";
	//	private static final String BINDING_repartitionLibelle = "repartitionLibelle";
	private static final String BINDING_filterText = "filterText";

	private static final String BINDING_unObjetARepartir = "unObjetARepartir";
	//private static final String BINDING_grouperObjetsARepartir = "grouperObjetsARepartir";
	private static final String BINDING_objetARepartirGroupeKeyPath = "objetARepartirGroupeKeyPath";
	private static final String BINDING_objetARepartirGroupeLabel = "objetARepartirGroupeLabel";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";
	private static final String BINDING_isObjetARepartirModifiable = "isObjetARepartirModifiable";
	private static final String BINDING_visible = "visible";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private NSKeyValueCoding unObjetARepartir;

	private Integer indexObjetDuGroupe;
	private Integer indexRepartitionExistante;

	private BigDecimal leMontant;
	private BigDecimal leRepartPourcentage;

	private boolean modificationEnCours;

	public CktlRepartitionForm(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, FwkCktlDepenseGuiAjax.FRAMEWORK_NAME, "scripts/filterlist2.js");

	}

	public String getFormId() {
		return getComponentId() + "_form";
	}

	public String getRepartPopUpId() {
		return getComponentId() + "_" + getIndexObjetDuGroupe() + "_" + getIndexRepartitionExistante() + "_repartPopup";
	}

	public String getRepartFieldId() {
		return getComponentId() + "_" + getIndexObjetDuGroupe() + "_" + getIndexRepartitionExistante() + "_repartField";
	}

	public String getRepartPourcentageId() {
		return getComponentId() + "_" + getIndexObjetDuGroupe() + "_" + getIndexRepartitionExistante() + "_repartPourcentageField";
	}

	public String getRepartMontantId() {
		return getComponentId() + "_" + getIndexObjetDuGroupe() + "_" + getIndexRepartitionExistante() + "_repartMontantField";
	}

	public String getFilterText() {
		return (String) valueForBinding(BINDING_filterText);
	}

	public void setFilterText(String filterText) {
		setValueForBinding(filterText, BINDING_filterText);
	}

	public WOActionResults supprimerRepart() {
		try {
			if (hasBinding(BINDING_supprimerRepartAction)) {
				return performParentAction((String) valueForBinding(BINDING_supprimerRepartAction));
			}
		} catch (Exception e) {
			mySession().addSimpleErrorMessage("Erreur lors de la suppression de la répartition", e);
		}
		return null;
	}

	/**
	 * @return TRUE si la répartition saisie est correcte.
	 */
	public Boolean isRepartitionGood() {
		return (Boolean) valueForBinding(BINDING_isRepartitionGood);
	}

	/**
	 * @return Les répartitions existantes.
	 */
	@SuppressWarnings("unchecked")
	public NSArray<? extends NSKeyValueCoding> getRepartitionsExistantes() {
		return (NSArray<? extends NSKeyValueCoding>) valueForBinding(BINDING_repartitionsExistantes);
	}

	/**
	 * @return un tableau contenant les objets du groupe (dans le cas où les répartitions sont regroupées). Par exemple dans le cas de la répartition
	 *         des actions pour les engagements, les objets du groupe seront les engagements de la commande. Les répartitions seront les répartitions
	 *         existantes par engagement.
	 */
	@SuppressWarnings("unchecked")
	public NSArray<? extends NSKeyValueCoding> getObjetsDuGroupe() {
		return (NSArray<? extends NSKeyValueCoding>) valueForBinding(BINDING_objetsDuGroupe);
	}

	public NSKeyValueCoding getUneRepartitionExistanteItem() {
		return (NSKeyValueCoding) valueForBinding(BINDING_uneRepartitionExistante);
	}

	public void setUneRepartitionExistanteItem(NSKeyValueCoding uneRepartitionExistanteItem) {
		setValueForBinding(uneRepartitionExistanteItem, BINDING_uneRepartitionExistante);
	}

	public NSKeyValueCoding getUnObjetDuGroupe() {
		return (NSKeyValueCoding) valueForBinding(BINDING_unObjetDuGroupe);
	}

	public void setUnObjetDuGroupe(NSKeyValueCoding unObjetDuGroupe) {
		setValueForBinding(unObjetDuGroupe, BINDING_unObjetDuGroupe);
	}

	public Integer getIndexObjetDuGroupe() {
		return indexObjetDuGroupe;
	}

	public void setIndexObjetDuGroupe(Integer indexObjetDuGroupe) {
		this.indexObjetDuGroupe = indexObjetDuGroupe;
	}

	public Integer getIndexRepartitionExistante() {
		return indexRepartitionExistante;
	}

	public void setIndexRepartitionExistante(Integer indexRepartitionExistante) {
		this.indexRepartitionExistante = indexRepartitionExistante;
	}

	/**
	 * @return Le libellé du groupe à afficher.
	 */
	public String getLibelleForGroupe() {
		return geObjetDuGroupeDisplayString();
		//		return (String) getUnObjetDuGroupe().valueForKey(geObjetDuGroupeDisplayString());
	}

	public String geObjetDuGroupeDisplayString() {
		return (String) valueForBinding(BINDING_objetDuGroupeDisplayString);
	}

	public Boolean afficherGroupeLibelle() {
		return (afficherGroupeHeader() && !ERXStringUtilities.stringIsNullOrEmpty(getLibelleForGroupe()) && (indexRepartitionExistante == 0));
	}

	public Boolean afficherGroupeHeader() {
		return getGroupeTitre() != null;
	}

	/**
	 * @return Les objets qui peuvent être sélectionnés (ce qui sera affiché dans la liste déroulante).
	 */
	@SuppressWarnings("unchecked")
	public NSArray<? extends NSKeyValueCoding> getObjetsARepartir() {
		return (NSArray<? extends NSKeyValueCoding>) valueForBinding(BINDING_objetsARepartir);
	}

	public NSKeyValueCoding getUnObjetARepartir() {
		return unObjetARepartir;
	}

	public void setUnObjetARepartir(NSKeyValueCoding unObjetARepartir) {
		this.unObjetARepartir = unObjetARepartir;
		setValueForBinding(unObjetARepartir, BINDING_unObjetARepartir);
	}

	public String unObjetARepartirDisplayString() {
		return (String) valueForBinding(BINDING_objetARepartirDisplayString);
	}

	public NSKeyValueCoding getObjetARepartirSelectionne() {
		return (NSKeyValueCoding) valueForBinding(BINDING_objetARepartirSelectionne);
	}

	public void setObjetARepartirSelectionne(NSKeyValueCoding objetARepartirSelectionne) {
		setValueForBinding(objetARepartirSelectionne, BINDING_objetARepartirSelectionne);
	}

	public NSNumberFormatter getRepartPourcentageFormatter() {
		return (NSNumberFormatter) valueForBinding(BINDING_repartPourcentageFormatter);
	}

	public Boolean isRepartPourcentageDisabled() {
		return (isObjetARepartirNotModifiable() || getObjetARepartirSelectionne() == null || booleanValueForBinding(BINDING_isRepartPourcentageDisabled, Boolean.FALSE));
	}

	public BigDecimal getRepartPourcentage() {
		return (BigDecimal) valueForBinding(BINDING_repartPourcentage);
	}

	//
	//	public void setRepartPourcentage(BigDecimal value) {
	//		setValueForBinding(value, BINDING_repartPourcentage);
	//	}

	public NSNumberFormatter repartMontantFormatter() {
		return (NSNumberFormatter) valueForBinding(BINDING_repartMontantFormatter);
	}

	public Boolean isRepartMontantDisabled() {
		return (isObjetARepartirNotModifiable() || getObjetARepartirSelectionne() == null || booleanValueForBinding(BINDING_isRepartMontantDisabled, Boolean.FALSE));
	}

	public BigDecimal getRepartMontant() {
		return (BigDecimal) valueForBinding(BINDING_repartMontant);
	}

	public void setRepartMontant(BigDecimal value) {
		if (!modificationEnCours && value != null) {
			if (getUneRepartitionExistanteItem() != null) {
				BigDecimal oldValue = getRepartMontant();
				if (oldValue != null && value != null && oldValue.floatValue() != value.floatValue() ||
						oldValue == null && value != null) {
					modificationEnCours = true;
					this.leMontant = value;
					if (leMontant != null) {
						//getUnEngageCtrlAction().setEactTtcSaisie(leMontant);
						setValueForBinding(value, BINDING_repartMontant);
					}
				}
			}
		}
	}

	public void setRepartPourcentage(BigDecimal value) {
		if (!modificationEnCours && value != null) {
			if (getUneRepartitionExistanteItem() != null) {
				BigDecimal oldValue = getRepartPourcentage();
				if (oldValue != null && value != null && oldValue.floatValue() != value.floatValue() ||
						oldValue == null && value != null) {
					modificationEnCours = true;
					this.leRepartPourcentage = value;
					if (leRepartPourcentage != null) {
						//getUnEngageCtrlAction().setEactTtcSaisie(leMontant);
						setValueForBinding(value, BINDING_repartPourcentage);
					}
				}
			}
		}
	}
	@Override
	public WOActionResults doNothing() {
		super.doNothing();
		modificationEnCours = false;
		return null;
	}

	@Override
	public String updateContainerID() {
		if (super.updateContainerID() != null) {
			return super.updateContainerID();
		}
		return getComponentId();
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

	public Boolean isObjetARepartirModifiable() {
		if (getObjetARepartirSelectionne() == null) {
			return Boolean.TRUE;
		}

		return booleanValueForBinding(BINDING_isObjetARepartirModifiable, Boolean.TRUE);
	}

	public Boolean isObjetARepartirNotModifiable() {
		return !isObjetARepartirModifiable();
	}

	public Object objetARepartirGroupeKeyPath() {
		if (hasBinding(BINDING_objetARepartirGroupeKeyPath)) {
			return valueForBinding(BINDING_objetARepartirGroupeKeyPath);
		}
		return null;
	}

	public String objetARepartirGroupeLabel() {
		return stringValueForBinding(BINDING_objetARepartirGroupeLabel, null);
	}

	public Boolean showRepartition() {
		return booleanValueForBinding(BINDING_visible, Boolean.TRUE);
	}

}