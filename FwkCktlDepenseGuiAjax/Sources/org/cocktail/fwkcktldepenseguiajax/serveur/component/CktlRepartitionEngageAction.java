/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @binding engageBudgets Les engageBudgets
 * @binding actions Les actions qui peuvent être affectées
 * @binding factoryEngagementControleAction instance de la factory
 * @binding editingContext facultatif
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlRepartitionEngageAction extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_engageBudgets = "engageBudgets";
	private static final String BINDING_actions = "actions";
	//private static final String BINDING_engageCtrlActions = "engageCtrlActions";
	private static final String BINDING_factoryEngagementControleAction = "factoryEngagementControleAction";

	private EOEngagementBudget unEngagementBudget;
	private EOEngagementControleAction unEngageCtrlAction;
	private EOTypeAction actionSelectionnee;
	private EOTypeAction unTypeAction;
	private String filterText;

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	public CktlRepartitionEngageAction(WOContext context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOEngagementBudget> getEngageBudgets() {
		return (NSArray<EOEngagementBudget>) valueForBinding(BINDING_engageBudgets);
	}

	public EOEngagementBudget getUnEngageBudget() {
		return unEngagementBudget;
	}

	public void setUnEngageBudget(EOEngagementBudget unEngageBudget) {
		unEngagementBudget = unEngageBudget;
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOTypeAction> getLesActions() {
		return (NSArray<EOTypeAction>) valueForBinding(BINDING_actions);
	}

	public FactoryEngagementControleAction getFactoryEngagementControleAction() {
		return (FactoryEngagementControleAction) valueForBinding(BINDING_factoryEngagementControleAction);
	}

	public EOEngagementControleAction getUnEngageCtrlAction() {
		return unEngageCtrlAction;
	}

	public void setUnEngageCtrlAction(EOEngagementControleAction unEngageCtrlAction) {
		this.unEngageCtrlAction = unEngageCtrlAction;
	}

	public EOTypeAction getActionSelectionnee() {
		return actionSelectionnee;
	}

	public void setActionSelectionnee(EOTypeAction actionSelectionnee) {
		this.actionSelectionnee = actionSelectionnee;
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnEngageCtrlAction() != null) {
			getFactoryEngagementControleAction().supprimer(edc(), getUnEngageCtrlAction());
		}
		return null;
	}

	public NSArray<EOEngagementControleAction> getLesEngageCtrlActions() {
		//	EOEngagementBudget commande = laCommande;
		EOEditingContext edc = getUnEngageBudget().editingContext();
		//f (commande.isRepartitionMarcheHorsMarcheGood()) {

		NSArray<EOEngagementControleAction> res = getUnEngageBudget().engagementControleActions();
		//lesCommandeControleActions = getUneCmdeBudgetForAction().commandeControleActions();
		if (res == null || res.count() < 1) {
			//getFactoryEngagementControleAction().creer(ed, eactHtSaisie,  eactTvaSaisie, eactTtcSaisie, eactMontantBudgetaire, typeAction, exercice, engagementBudget)
			getFactoryEngagementControleAction().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnEngageBudget().restantTtcControleAction(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnEngageBudget().exercice(), getUnEngageBudget());
			res = getUnEngageBudget().engagementControleActions();
		}
		else {
			if (getLesActions() != null && getLesActions().count() > 0) {
				EOEngagementControleAction ctrleActionVide = null;
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeAction = nil", null);
				NSArray<EOEngagementControleAction> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleActionVide = (EOEngagementControleAction) arrayVides.lastObject();
				}
				qual = EOQualifier.qualifierWithQualifierFormat("typeAction != nil", null);
				NSArray<EOEngagementControleAction> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum.pourcentage");
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleActionVide == null) {
						getFactoryEngagementControleAction().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnEngageBudget().restantTtcControleAction(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnEngageBudget().exercice(), getUnEngageBudget());
						res = getUnEngageBudget().engagementControleActions();
					}
					else {
						ctrleActionVide.setEactTtcSaisie(getUnEngageBudget().restantTtcControleAction());
						ctrleActionVide.setPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleActionVide != null) {
						Enumeration<EOEngagementControleAction> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleActionVide = (EOEngagementControleAction) enumArrayVides.nextElement();
							getFactoryEngagementControleAction().supprimer(edc, ctrleActionVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		//}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

	public void setUnTypeAction(EOTypeAction unTypeAction) {
		this.unTypeAction = unTypeAction;
	}

	public EOTypeAction getUnTypeAction() {
		return unTypeAction;
	}

}