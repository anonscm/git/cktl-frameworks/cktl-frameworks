/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class ACktlDepenseGuiAjaxComponent extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;

	public static final Logger LOG = Logger.getLogger(ACktlDepenseGuiAjaxComponent.class);

	/** PersId de l'utilisateur en cours. */
	public final static String BINDING_utilisateurPersId = "utilisateurPersId";

	public final static String BINDING_utilisateur = "utilisateur";

	/**
	 * Facultatif. EditingContext fourni par le composant parent. (Si non specifie, le defaultEditingContext de la session est utilise).
	 */
	public static final String BINDING_editingContext = "editingContext";

	/**
	 * Facultatif. Indique si le composant est en lecture seule. Par defaut TRUE.
	 */
	public static final String BINDING_isReadOnly = "isReadOnly";

	/**
	 * Facultatif. Indique si le composant est en mode edition. Par defaut FALSE.
	 */
	public static final String BINDING_isEditing = "isEditing";
	public static final String BINDING_exercice = "exercice";
	public static final String BINDING_ctrl = "ctrl";

	public static final Boolean IS_READ_ONLY_DEFAULT = Boolean.TRUE;
	public static final Boolean IS_EDITING_DEFAULT = Boolean.FALSE;

	private String containerErreurMsgId;
	private String erreurSaisieMessage;

	private Boolean isEditing = IS_EDITING_DEFAULT;

	public ACktlDepenseGuiAjaxComponent(WOContext context) {
		super(context);
	}

	public ICktlDepenseGuiAjaxSession cktlDepenseGuiAjaxSession() {
		return (ICktlDepenseGuiAjaxSession) mySession();
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("starting appendToResponse : " + getComponentId());
		}
		super.appendToResponse(response, context);
		//		if (hasBinding(BINDING_cssFilename)) {
		//			String filename = (String) valueForBinding(BINDING_cssFilename);
		//			String fwkName = null;
		//			if (hasBinding(BINDING_cssFwkName)) {
		//				fwkName = (String) valueForBinding(BINDING_cssFwkName);
		//				if (!("app".equalsIgnoreCase(fwkName))) {
		//					fwkName = NSPathUtilities.stringByDeletingPathExtension(fwkName);
		//					fwkName = NSPathUtilities.stringByAppendingPathExtension(fwkName, "framework");
		//				}
		//			}
		//			CktlAjaxUtils.addStylesheetResourceInHead(context, response, fwkName, filename);
		//		}
	}

	/**
	 * @return Le binding <i>editingContext</i> s'il est renseigne, sinon l'editingContext de la session. Cette methode peut etre surchargee pour
	 *         renvoyer un nestedEditingContext.
	 */
	public EOEditingContext edc() {
		if (hasBinding(BINDING_editingContext)) {
			return (EOEditingContext) valueForBinding(BINDING_editingContext);
		}
		return mySession().defaultEditingContext();
	}

	public Integer getUtilisateurPersId() {
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}

	public String getContainerErreurMsgId() {
		if (containerErreurMsgId == null) {
			containerErreurMsgId = getComponentId() + "_containerErreurMsg";
		}
		return containerErreurMsgId;
	}

	public String getErreurSaisieMessage() {
		String tmp = erreurSaisieMessage;
		return tmp;
	}

	public void setErreurSaisieMessage(String erreurSaisieMessage) {
		this.erreurSaisieMessage = erreurSaisieMessage;
	}

	public Boolean isReadOnly() {
		return (Boolean) (hasBinding(BINDING_isReadOnly) ? valueForBinding(BINDING_isReadOnly) : IS_READ_ONLY_DEFAULT);
	}

	public EOExercice exercice() {
		return (EOExercice) valueForBinding(BINDING_exercice);
	}

	public Boolean isEditing() {
		if (hasBinding(BINDING_isEditing)) {
			return (Boolean) valueForBinding(BINDING_isEditing);
		}
		return isEditing;
	}

	public void setIsEditing(Boolean value) {
		if (hasBinding(BINDING_isEditing) && canSetValueForBinding(BINDING_isEditing)) {
			setValueForBinding(value, BINDING_isEditing);
		}
		isEditing = value;
	}

	public EOUtilisateur utilisateurInContext(EOEditingContext ed) {
		EOUtilisateur user = (EOUtilisateur) valueForBinding(BINDING_utilisateur);
		if (user == null) {
			throw new RuntimeException("binding utilisateur null");
		}
		if (!user.editingContext().equals(ed)) {
			user = (EOUtilisateur) ed.faultForGlobalID(ed.globalIDForObject(user), ed);
		}
		return user;

	}

}
