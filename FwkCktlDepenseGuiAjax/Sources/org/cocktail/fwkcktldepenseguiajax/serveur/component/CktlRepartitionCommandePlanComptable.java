/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXArrayUtilities;

public class CktlRepartitionCommandePlanComptable extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_commandeBudgets = "commandeBudgets";
	private static final String BINDING_unCommandeBudget = "unCommandeBudget";
	private static final String BINDING_factoryCommandeControlePlanComptable = "factoryCommandeControlePlanComptable";
	private static final String BINDING_planComptables = "planComptables";
	//	private static final String BINDING_ajouterInventairesAction = "ajouterInventairesAction";
	//	private static final String BINDING_supprimerInventairesAction = "supprimerInventaireAction";
	//	private static final String BINDING_unInventaire = "unInventaire";
	//	private static final String BINDING_showInventaire = "showInventaire";
	//	private static final String BINDING_isInventaireModifiable = "isInventaireModifiable";
	private static final String BINDING_unCommandeCtrlPlanComptable = "unCommandeCtrlPlanComptable";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";
	private static final String BINDING_codesNomenclatureDepense = "codesNomenclatureDepense";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";
	private static final String DEFAULT_codesNomenclatureDepense = "";
	
	
	private static final String LIBELLE_COMPTES_DERIVES = "Comptes dérivés des codes achats";
	private static final String LIBELLE_COMPTES_NON_DERIVES = "Autres comptes";

	private EOCommandeBudget unCommandeBudget;
	private EOCommandeControlePlanComptable unCommandeCtrlPlanComptable;
	private EOPlanComptable unPlanComptable;
	private String filterText;

	//	private EOInventaire unInventaire = null;
	//private boolean modificationEnCours;

	public CktlRepartitionCommandePlanComptable(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCommandeBudget> getCommandeBudgets() {
		return (NSArray<EOCommandeBudget>) valueForBinding(BINDING_commandeBudgets);
	}

	public EOCommandeBudget getUnCommandeBudget() {
		return unCommandeBudget;
	}

	public void setUnCommandeBudget(EOCommandeBudget unCommandeBudget) {
		this.unCommandeBudget = unCommandeBudget;
		if (hasBinding(BINDING_unCommandeBudget)) {
			setValueForBinding(unCommandeBudget, BINDING_unCommandeBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOPlanComptable> getLesPlanComptables() {
		NSArray<EOPlanComptable> res = ERXArrayUtilities.sortedArraySortedWithKey((NSArray<EOPlanComptable>) valueForBinding(BINDING_planComptables), EOPlanComptable.PCO_NUM_KEY);
		NSMutableArray<EOPlanComptable> comptesDerives = new NSMutableArray<EOPlanComptable>();
		NSMutableArray<EOPlanComptable> comptesNonDerives = new NSMutableArray<EOPlanComptable>();
		NSArray<EOCodeExer> codesNomenclatureDepense = (NSArray<EOCodeExer>) valueForBinding(BINDING_codesNomenclatureDepense);
		for (EOPlanComptable eoPlanComptable : res) {
			if (ERXArrayUtilities.arrayContainsAnyObjectFromArray(codesNomenclatureDepense, eoPlanComptable.getCodesExerDerives())) {
				comptesDerives.add(eoPlanComptable);
			} else {
				comptesNonDerives.add(eoPlanComptable);
			}
		}
		return ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(comptesDerives, comptesNonDerives);
	}

	public FactoryCommandeControlePlanComptable getFactoryCommandeControlePlanComptable() {
		return (FactoryCommandeControlePlanComptable) valueForBinding(BINDING_factoryCommandeControlePlanComptable);
	}

	public EOCommandeControlePlanComptable getUnCommandeCtrlPlanComptable() {
		return unCommandeCtrlPlanComptable;
	}

	public void setUnCommandeCtrlPlanComptable(EOCommandeControlePlanComptable unCommandeCtrlPlanComptable) {
		this.unCommandeCtrlPlanComptable = unCommandeCtrlPlanComptable;
		setValueForBinding(unCommandeCtrlPlanComptable, BINDING_unCommandeCtrlPlanComptable);
	}

	public EOPlanComptable getSelection() {
		return getUnCommandeCtrlPlanComptable().planComptable();
	}

	public void setSelection(EOPlanComptable selection) {
		NSArray<EOCommandeControlePlanComptable> res = getLesCommandeCtrlPlanComptables();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOPlanComptable> actions = (NSArray<EOPlanComptable>) getLesCommandeCtrlPlanComptables().valueForKeyPath(EOCommandeControlePlanComptable.PLAN_COMPTABLE_KEY);
			if (!actions.containsObject(selection)) {
				if (getUnCommandeCtrlPlanComptable() != null) {
					getUnCommandeCtrlPlanComptable().setPlanComptableRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnCommandeCtrlPlanComptable() != null) {
			getFactoryCommandeControlePlanComptable().supprimer(getUnCommandeBudget().editingContext(), getUnCommandeCtrlPlanComptable());
		}
		return null;
	}

	public NSArray<EOCommandeControlePlanComptable> getLesCommandeCtrlPlanComptables() {
		EOEditingContext edc = getUnCommandeBudget().editingContext();
		NSArray<EOCommandeControlePlanComptable> res = getUnCommandeBudget().commandeControlePlanComptables();
		if (res == null || res.count() < 1) {
			getFactoryCommandeControlePlanComptable().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControlePlanComptable(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnCommandeBudget().exercice(), getUnCommandeBudget());
			res = getUnCommandeBudget().commandeControlePlanComptables();
		}
		else {
			if (getLesPlanComptables() != null && getLesPlanComptables().count() > 0) {
				EOCommandeControlePlanComptable ctrlePlanComptableVide = null;
				//				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  "typePlanComptable = nil", null);
				EOQualifier qual = new EOKeyValueQualifier(EOCommandeControlePlanComptable.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, null);
				NSArray<EOCommandeControlePlanComptable> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrlePlanComptableVide = (EOCommandeControlePlanComptable) arrayVides.lastObject();
				}
				//				qual = EOQualifier.qualifierWithQualifierFormat("planComptable != nil", null);
				qual = new EONotQualifier(qual);
				NSArray<EOCommandeControlePlanComptable> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum." + EOCommandeControlePlanComptable.CPCO_POURCENTAGE_KEY);
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrlePlanComptableVide == null) {
						getFactoryCommandeControlePlanComptable().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControlePlanComptable(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnCommandeBudget().exercice(),
								getUnCommandeBudget());
						res = getUnCommandeBudget().commandeControlePlanComptables();
					}
					else {
						ctrlePlanComptableVide.setCpcoTtcSaisie(getUnCommandeBudget().restantTtcControlePlanComptable());
						ctrlePlanComptableVide.setCpcoPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrlePlanComptableVide != null) {
						Enumeration<EOCommandeControlePlanComptable> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrlePlanComptableVide = (EOCommandeControlePlanComptable) enumArrayVides.nextElement();
							getFactoryCommandeControlePlanComptable().supprimer(edc, ctrlePlanComptableVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnPlanComptable(EOPlanComptable unPlanComptable) {
		this.unPlanComptable = unPlanComptable;
	}

	public EOPlanComptable getUnPlanComptable() {
		return unPlanComptable;
	}

	@Override
	public WOActionResults doNothing() {
		super.doNothing();
		//	modificationEnCours = false;
		return null;
	}

	@Override
	public String updateContainerID() {
		if (super.updateContainerID() != null) {
			return super.updateContainerID();
		}
		return getComponentId();
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}
	
	public String getCodesNomenclatureDepense() {
		return stringValueForBinding(BINDING_codesNomenclatureDepense, DEFAULT_codesNomenclatureDepense);
	}
	
	public boolean getObjetARepartirGroupeKeyPath() {
		NSArray<EOCodeExer> codesExerFromArticles = (NSArray<EOCodeExer>) valueForBinding(BINDING_codesNomenclatureDepense);
		NSArray<EOCodeExer> codesExerFromPlanCo = unPlanComptable.getCodesExerDerives();
		return ERXArrayUtilities.arrayContainsAnyObjectFromArray(codesExerFromArticles, codesExerFromPlanCo);
	}

	public String getObjetARepartirGroupeLabel() {
		return (getObjetARepartirGroupeKeyPath()) ? LIBELLE_COMPTES_DERIVES : LIBELLE_COMPTES_NON_DERIVES;
	}
}