/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class CktlRepartitionCommandeAction extends ACktlDepenseGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	private static final String BINDING_commandeBudgets = "commandeBudgets";
	private static final String BINDING_unCommandeBudget = "unCommandeBudget";
	private static final String BINDING_actions = "actions";
	private static final String BINDING_factoryCommandeControleAction = "factoryCommandeControleAction";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private EOCommandeBudget unCommandeBudget;
	private EOCommandeControleAction unCommandeCtrlAction;
	private EOTypeAction unTypeAction;
	private String filterText;

	public CktlRepartitionCommandeAction(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCommandeBudget> getCommandeBudgets() {
		return (NSArray<EOCommandeBudget>) valueForBinding(BINDING_commandeBudgets);
	}

	public EOCommandeBudget getUnCommandeBudget() {
		return unCommandeBudget;
	}

	public void setUnCommandeBudget(EOCommandeBudget unCommandeBudget) {
		this.unCommandeBudget = unCommandeBudget;
		if (hasBinding(BINDING_unCommandeBudget)) {
			setValueForBinding(unCommandeBudget, BINDING_unCommandeBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOTypeAction> getLesActions() {
		return (NSArray<EOTypeAction>) valueForBinding(BINDING_actions);
	}

	public FactoryCommandeControleAction getFactoryCommandeControleAction() {
		return (FactoryCommandeControleAction) valueForBinding(BINDING_factoryCommandeControleAction);
	}

	public EOCommandeControleAction getUnCommandeCtrlAction() {
		return unCommandeCtrlAction;
	}

	public void setUnCommandeCtrlAction(EOCommandeControleAction unCommandeCtrlAction) {
		this.unCommandeCtrlAction = unCommandeCtrlAction;
	}

	public EOTypeAction getActionSelectionnee() {
		return getUnCommandeCtrlAction().typeAction();
	}

	public void setActionSelectionnee(EOTypeAction actionSelectionnee) {
		NSArray<EOCommandeControleAction> res = getLesCommandeCtrlActions();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOTypeAction> actions = (NSArray<EOTypeAction>) getLesCommandeCtrlActions().valueForKeyPath(EOCommandeControleAction.TYPE_ACTION_KEY);
			if (!actions.containsObject(actionSelectionnee)) {
				if (getUnCommandeCtrlAction() != null) {
					getUnCommandeCtrlAction().setTypeActionRelationship(actionSelectionnee);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnCommandeCtrlAction() != null) {
			getFactoryCommandeControleAction().supprimer(getUnCommandeBudget().editingContext(), getUnCommandeCtrlAction());
		}
		return null;
	}

	public NSArray<EOCommandeControleAction> getLesCommandeCtrlActions() {
		EOEditingContext edc = getUnCommandeBudget().editingContext();
		NSArray<EOCommandeControleAction> res = getUnCommandeBudget().commandeControleActions();
		if (res == null || res.count() < 1) {
			getFactoryCommandeControleAction().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControleAction(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnCommandeBudget().exercice(), getUnCommandeBudget());
			res = getUnCommandeBudget().commandeControleActions();
		}
		else {
			if (getLesActions() != null && getLesActions().count() > 0) {
				EOCommandeControleAction ctrleActionVide = null;
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeAction = nil", null);
				NSArray<EOCommandeControleAction> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleActionVide = (EOCommandeControleAction) arrayVides.lastObject();
				}
				qual = EOQualifier.qualifierWithQualifierFormat("typeAction != nil", null);
				NSArray<EOCommandeControleAction> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum." + EOCommandeControleAction.CACT_POURCENTAGE_KEY);
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleActionVide == null) {
						getFactoryCommandeControleAction().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControleAction(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnCommandeBudget().exercice(),
								getUnCommandeBudget());
						res = getUnCommandeBudget().commandeControleActions();
					}
					else {
						ctrleActionVide.setCactTtcSaisie(getUnCommandeBudget().restantTtcControleAction());
						ctrleActionVide.setCactPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleActionVide != null) {
						Enumeration<EOCommandeControleAction> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleActionVide = (EOCommandeControleAction) enumArrayVides.nextElement();
							getFactoryCommandeControleAction().supprimer(edc, ctrleActionVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnTypeAction(EOTypeAction unTypeAction) {
		this.unTypeAction = unTypeAction;
	}

	public EOTypeAction getUnTypeAction() {
		return unTypeAction;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}
}