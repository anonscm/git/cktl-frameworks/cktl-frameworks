/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.util.List;

import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepenseguiajax.serveur.FwkCktlDepenseGuiAjax;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlTypeAchatSelectionCtrl;
import org.cocktail.fwkcktldepenseguiajax.serveur.enums.ETypeMarche;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;

public class CktlTypeAchatSelection extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String TYPE_ACHAT_GROUP_RADIO_NAME = "TypesAchat";
	private static final String TYPE_ACHAT_MARCHE_ID = "MarcheId";
	private static final String TYPE_ACHAT_HORS_MARCHE_ID = "HorsMarcheId";
	public static final String BINDING_hideCodeNomenclatures = "hideCodeNomenclatures";

	public CktlTypeAchatSelection(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, FwkCktlDepenseGuiAjax.FRAMEWORK_NAME, "scripts/filterlist2.js");
	}

	public CktlTypeAchatSelectionCtrl ctrl() {
		return (CktlTypeAchatSelectionCtrl) valueForBinding(BINDING_ctrl);
	}

	public ETypeMarche typeMarcheIsMarche() {
		return ETypeMarche.MARCHE;
	}

	public ETypeMarche typeMarcheIsHorsMarche() {
		return ETypeMarche.HORS_MARCHE;
	}

	public ETypeMarche getTypeMarcheSelectionne() {
		return ctrl().getTypeAchatTO().getTypeMarcheSelectionne();
	}

	public void setTypeMarcheSelectionne(ETypeMarche typeMarcheSelectionne) {
		ctrl().getTypeAchatTO().setTypeMarcheSelectionne(typeMarcheSelectionne);
	}

	public boolean isTypeAchatMarche() {
		return ctrl().isTypeAchatMarche();
	}

	public boolean isTypeAchatHorsMarche() {
		return ctrl().isTypeAchatHorsMarche();
	}

	public boolean isTypeAchatMarcheDisabled() {
		return ctrl().isTypeAchatDisabled(ETypeMarche.MARCHE);
	}

	public boolean isTypeAchatHorsMarcheDisabled() {
		return ctrl().isTypeAchatDisabled(ETypeMarche.HORS_MARCHE);
	}

	public String getTypeAchatGroupRadioName() {
		return TYPE_ACHAT_GROUP_RADIO_NAME;
	}

	public String getTypeAchatMarcheId() {
		return getComponentId() + TYPE_ACHAT_MARCHE_ID;
	}

	public String getTypeAchatHorsMarcheId() {
		return getComponentId() + TYPE_ACHAT_HORS_MARCHE_ID;
	}

	public String getTypeAchatMarcheLabel() {
		return "for=" + getTypeAchatMarcheId();
	}

	public String getTypeAchatHorsMarcheLabel() {
		return "for=" + getTypeAchatHorsMarcheId();
	}

	public List<EOAttribution> getAttributions() {
		return ctrl().attributions();
	}

	public String getCurrentAttributionLibelle() {
		EOAttribution currentAttribution = ctrl().getCurrentAttribution();
		if (currentAttribution == null) {
			return null;
		}
		String libelle = currentAttribution.libelle();
		if (currentAttribution.fournisseur() != null) {
			libelle = StringCtrl.cut(currentAttribution.fournisseur().fouNom(), 20) + " " + libelle;
		}
		return libelle;
	}

	public String getCurrentCodeNomenclatureLibelle() {
		return ctrl().getCurrentCodeNomenclature().libelle();
	}

	public String getContainerCodeNomenclatureId() {
		return getComponentId() + "_codesNomenclature";
	}

	public String getAttributionPopupId() {
		return getComponentId() + "_attributionField";
	}

	public String getAttributionFieldId() {
		return getComponentId() + "_attributionPopup";
	}

	public String getCodeNomenclatureFieldId() {
		return getComponentId() + "_codesNomenclatureField";
	}

	public String getCodeNomenclaturePopupId() {
		return getComponentId() + "_codesNomenclaturePopup";
	}

	public WOActionResults selectAttribution() {
		ctrl().selectAttribution();
		return null;
	}

	public WOActionResults selectCodeNomenclature() {
		ctrl().selectCodeNomenclature();
		return null;
	}

	public Boolean hideCodeNomenclatures() {
		return booleanValueForBinding(BINDING_hideCodeNomenclatures, Boolean.FALSE);
	}

	public Boolean afficheCodeNomenclatures() {
		return !hideCodeNomenclatures() && isTypeAchatHorsMarche();
	}
}