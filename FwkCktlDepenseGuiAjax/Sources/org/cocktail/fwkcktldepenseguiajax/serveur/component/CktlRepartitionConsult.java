/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSNumberFormatter;

import er.ajax.AjaxUtils;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Permet de gérer l'affichage de répartitions
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlRepartitionConsult extends ACktlDepenseGuiAjaxComponent {
	private static final long serialVersionUID = 1L;

	private static final String BINDING_repartitionsExistantes = "repartitionsExistantes";
	private static final String BINDING_objetsDuGroupe = "objetsDuGroupe";
	private static final String BINDING_objetDuGroupeDisplayString = "objetDuGroupeDisplayString";
	private static final String BINDING_unObjetDuGroupe = "unObjetDuGroupe";

	private static final String BINDING_uneRepartitionExistante = "uneRepartitionExistante";
	private static final String BINDING_uneRepartitionExistanteDisplayStringKeyPath = "uneRepartitionExistanteDisplayStringKeyPath";
	private static final String BINDING_uneRepartitionExistantePourcentageKeyPath = "uneRepartitionExistantePourcentageKeyPath";
	private static final String BINDING_uneRepartitionExistanteMontantKeyPath = "uneRepartitionExistanteMontantKeyPath";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String BINDING_repartPourcentageFormatter = "repartPourcentageFormatter";
	private static final String BINDING_repartMontantFormatter = "repartMontantFormatter";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private Integer indexObjetDuGroupe;
	private Integer indexRepartitionExistante;

	public CktlRepartitionConsult(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlDepenseGuiAjax.framework", "scripts/filterlist2.js");
	}

	/**
	 * @return Les répartitions existantes.
	 */
	@SuppressWarnings("unchecked")
	public NSArray<? extends NSKeyValueCodingAdditions> getRepartitionsExistantes() {
		return (NSArray<? extends NSKeyValueCodingAdditions>) valueForBinding(BINDING_repartitionsExistantes);
	}

	/**
	 * @return un tableau contenant les objets du groupe (dans le cas où les répartitions sont regroupées). Par exemple dans le cas de la répartition
	 *         des actions pour les engagements, les objets du groupe seront les engagements de la commande. Les répartitions seront les répartitions
	 *         existantes par engagement.
	 */
	@SuppressWarnings("unchecked")
	public NSArray<? extends NSKeyValueCodingAdditions> getObjetsDuGroupe() {
		return (NSArray<? extends NSKeyValueCodingAdditions>) valueForBinding(BINDING_objetsDuGroupe);
	}

	public NSKeyValueCodingAdditions getUneRepartitionExistanteItem() {
		return (NSKeyValueCodingAdditions) valueForBinding(BINDING_uneRepartitionExistante);
	}

	public void setUneRepartitionExistanteItem(NSKeyValueCodingAdditions uneRepartitionExistanteItem) {
		setValueForBinding(uneRepartitionExistanteItem, BINDING_uneRepartitionExistante);
	}

	public NSKeyValueCodingAdditions getUnObjetDuGroupe() {
		return (NSKeyValueCodingAdditions) valueForBinding(BINDING_unObjetDuGroupe);
	}

	public void setUnObjetDuGroupe(NSKeyValueCodingAdditions unObjetDuGroupe) {
		setValueForBinding(unObjetDuGroupe, BINDING_unObjetDuGroupe);
	}

	public Integer getIndexObjetDuGroupe() {
		return indexObjetDuGroupe;
	}

	public void setIndexObjetDuGroupe(Integer indexObjetDuGroupe) {
		this.indexObjetDuGroupe = indexObjetDuGroupe;
	}

	public Integer getIndexRepartitionExistante() {
		return indexRepartitionExistante;
	}

	public void setIndexRepartitionExistante(Integer indexRepartitionExistante) {
		this.indexRepartitionExistante = indexRepartitionExistante;
	}

	/**
	 * @return Le libellé du groupe à afficher.
	 */
	public String getLibelleForGroupe() {
		return geObjetDuGroupeDisplayString();
		//		return (String) getUnObjetDuGroupe().valueForKey(geObjetDuGroupeDisplayString());
	}

	public String geObjetDuGroupeDisplayString() {
		return (String) valueForBinding(BINDING_objetDuGroupeDisplayString);
	}

	public Boolean afficherGroupeLibelle() {
		return (afficherGroupeHeader() && !ERXStringUtilities.stringIsNullOrEmpty(getLibelleForGroupe()) && (indexRepartitionExistante == 0));
	}

	public Boolean afficherGroupeHeader() {
		return getGroupeTitre() != null;
	}

	public NSNumberFormatter getRepartPourcentageFormatter() {
		return (NSNumberFormatter) valueForBinding(BINDING_repartPourcentageFormatter);
	}

	public NSNumberFormatter repartMontantFormatter() {
		return (NSNumberFormatter) valueForBinding(BINDING_repartMontantFormatter);
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

	public String getObjetRepartiDisplayString() {
		return (String) getUneRepartitionExistanteItem().valueForKeyPath(stringValueForBinding(BINDING_uneRepartitionExistanteDisplayStringKeyPath, null));
	}

	public BigDecimal getObjetRepartiPourcentage() {
		return (BigDecimal) getUneRepartitionExistanteItem().valueForKeyPath(stringValueForBinding(BINDING_uneRepartitionExistantePourcentageKeyPath, null));
	}

	public BigDecimal getObjetRepartiMontant() {
		return (BigDecimal) getUneRepartitionExistanteItem().valueForKeyPath(stringValueForBinding(BINDING_uneRepartitionExistanteMontantKeyPath, null));
	}

}
