/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlSourceSelectionCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Composant de selection d'une source de credit.
 * 
 * @binding ctrl
 * @binding isMultiSelection (par defaut false)
 */
public class CktlSourceSelection extends ACktlSourceSelection {

	private static final long serialVersionUID = 1L;

	private static final String RADIO_CREDITS_PREFERES_ID = "rbCreditsPreferesId";
	private static final String RADIO_CREDITS_DISPONIBLES_ID = "rbCreditsDisponiblesId";
	private static final String RADIO_CREDITS_CONVENTION_ID = "rbCreditsConventionId";
	private static final String RADIO_CREDITS_TOUS_ID = "rbCreditsTousId";

	private static final String BINDING_isMultiSelection = "isMultiSelection";

	public CktlSourceSelection(WOContext context) {
		super(context);
	}

	public String radioCreditsPreferesId() {
		return getComponentId() + RADIO_CREDITS_PREFERES_ID;
	}

	public String radioCreditsDisponiblesId() {
		return getComponentId() + RADIO_CREDITS_DISPONIBLES_ID;
	}

	public String radioCreditsParConventionId() {
		return getComponentId() + RADIO_CREDITS_CONVENTION_ID;
	}

	public String radioCreditsTousId() {
		return getComponentId() + RADIO_CREDITS_TOUS_ID;
	}

	@Override
	public CktlSourceSelectionCtrl ctrl() {
		return (CktlSourceSelectionCtrl) super.ctrl();
	}

	public String getConventionPopUpId() {
		return getComponentId() + "_conventionPopup";
	}

	public String getConventionFieldId() {
		return getComponentId() + "_conventionField";
	}

	public String getSourcesUtilisateurContainerId() {
		return getComponentId() + "_sourcesUtilisateur";
	}

	public String getContainerSourcesId() {
		return getComponentId() + "_containerSources";
	}

	public Boolean isMultiSelection() {
		return booleanValueForBinding(BINDING_isMultiSelection, Boolean.FALSE);
	}

	public String containerRadiosId() {
		return getComponentId() + "_radios";
	}

	public String onCompleteSourceUtilisateur() {
		//return "function (oC) {Element.hide('busy1');}";
		return "function (oC) {Element.hide('busy1');}";
	}

	public String getApresAffichageElements() {
		return containerRadiosId() + "Update();";
	}

	public String onChangeFiltre() {
		return "var keyCode=this.getAttribute('keyCode');if (keyCode=='entree') {$('" + filtreSourceUtilisateurButtonId() + "').click();}; this.setAttribute('keyCode','');return true;";
	}

	public String filtreSourceUtilisateurButtonId() {
		return getComponentId() + "filtreButton";
	}

	public String onKeypressFiltre() {
		return "var keyCode = event.which ? event.which : event.keyCode;if (keyCode==13) {this.setAttribute('keyCode','entree');}";
	}

	public WOActionResults actionPref() {
		if (ctrl().isAfficherCreditsPreferes()) {
			return supprimerSourcePreferee();
		}
		return ajouterUneSourceAuxPreferes();
	}

	public String typePref() {
		if (ctrl().isAfficherCreditsPreferes()) {
			return "etoileGrise";
		}
		return "etoile";
	}

	public String titlePref() {
		if (ctrl().isAfficherCreditsPreferes()) {
			return "Supprimer des favoris";
		}
		return "Ajouter aux favoris";
	}

	public String updateContainerIdForPref() {
		if (ctrl().isAfficherCreditsPreferes()) {
			return getContainerSourcesId();
		}
		return null;
	}

}