/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.ACktlSourceSelectionCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe abstraite pour les composants de selection de sources de crédit.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class ACktlSourceSelection extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private EOConvention uneConvention = null, uneConventionSelectionnee = null;

	public ACktlSourceSelection(WOContext context) {
		super(context);
		//initialiseComponent();
	}

	public EOEditingContext getEditingContext() {
		return edc();
	}

	public WOActionResults ajouterUneSourceAuxPreferes() {
		ctrl().ajouterUneSourceAuxPreferes();
		return null;
	}

	public WOActionResults supprimerSourcePreferee() {
		ctrl().supprimerSourcePreferee();
		return null;
	}

	protected EOExercice getExercice() {
		return exercice();
	}

	public WOActionResults filtrerSourcesUtilisateur() {
		ctrl().filtrerSourcesUtilisateur();
		return null;

	}

	public WOActionResults filtrer1SourcesUtilisateur() {
		ctrl().filtrer1SourcesUtilisateur();
		return null;
	}

	public EOConvention getUneConvention() {
		return uneConvention;
	}

	public void setUneConvention(EOConvention uneConvention) {
		this.uneConvention = uneConvention;
	}

	public EOConvention getUneConventionSelectionnee() {
		return uneConventionSelectionnee;
	}

	public void setUneConventionSelectionnee(EOConvention uneConventionSelectionnee) {
		this.uneConventionSelectionnee = uneConventionSelectionnee;
	}

	public ACktlSourceSelectionCtrl ctrl() {
		return (ACktlSourceSelectionCtrl) valueForBinding(BINDING_ctrl);
	}

}
