/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleMarche;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Permet d'afficher les informations concernant un lot (marché, code nomenclatures). Le composant fonctionne avec soit une commande, soit un
 * depenseCtrlMarche, soit un engagementCtrlMarche, soit une commande
 * 
 * @binding commande Un objet {@link EOCommande}.
 * @binding depenseControleMarche (prioritaire) Un objet {@link _IDepenseControleMarche}.
 * @binding engageControleMarche Un objet {@link EOEngagementControleMarche}
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlDepenseLotView extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_COMMANDE = "commande";
	public static final String BINDING_depenseControleMarche = "depenseControleMarche";
	public static final String BINDING_engageControleMarche = "engageControleMarche";

	public EOLotNomenclature unLotNomenclature;

	public CktlDepenseLotView(WOContext arg0) {
		super(arg0);
	}

	public EOCommande getCommande() {
		return (EOCommande) valueForBinding(BINDING_COMMANDE);
	}

	public _IDepenseControleMarche getDepenseCtrlMarche() {
		return (_IDepenseControleMarche) valueForBinding(BINDING_depenseControleMarche);
	}

	public EOEngagementControleMarche getEngageCtrlMarche() {
		return (EOEngagementControleMarche) valueForBinding(BINDING_engageControleMarche);
	}

	public Boolean hasAttribution() {
		return getAttribution() != null;
	}

	public EOAttribution getAttribution() {
		if (getDepenseCtrlMarche() != null) {
			return getDepenseCtrlMarche().attribution();
		}
		if (getEngageCtrlMarche() != null) {
			return getEngageCtrlMarche().attribution();
		}
		if (getCommande() != null) {
			return getCommande().attribution();
		}
		return null;
	}

	public EOExercice getExercice() {
		if (getDepenseCtrlMarche() != null) {
			return getDepenseCtrlMarche().exercice();
		}
		if (getEngageCtrlMarche() != null) {
			return getEngageCtrlMarche().exercice();
		}
		if (getCommande() != null) {
			return getCommande().exercice();
		}
		return null;
	}

	public String expansionText() {
		return "D\u00E9tail des codes nomenclatures du lot";
	}

	public String expansionId() {
		return getComponentId() + "_expansion";
	}

	public NSArray<EOLotNomenclature> lotNomenclatures() {
		if (getAttribution() != null) {

			return getAttribution().lot().lotNomenclatures(new EOKeyValueQualifier(EOLotNomenclature.CODE_EXER_KEY + "." + EOCodeExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, getExercice()), new NSArray<EOSortOrdering>(EOLotNomenclature.SORT_CODE_EXER_LIBELLE_ASC), false);
		}
		return NSArray.emptyArray();
	}
}
