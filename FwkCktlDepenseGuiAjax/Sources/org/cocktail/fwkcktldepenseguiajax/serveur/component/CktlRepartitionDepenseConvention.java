/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class CktlRepartitionDepenseConvention extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_depenseBudgets = "depenseBudgets";
	private static final String BINDING_unDepenseBudget = "unDepenseBudget";
	private static final String BINDING_factoryDepenseControleConvention = "factoryDepenseControleConvention";
	private static final String BINDING_conventions = "conventions";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private _IDepenseBudget unDepenseBudget;
	private _IDepenseControleConvention unDepenseCtrlConvention;
	private EOConvention unConvention;
	private String filterText;

	public CktlRepartitionDepenseConvention(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<_IDepenseBudget> getDepenseBudgets() {
		return (NSArray<_IDepenseBudget>) valueForBinding(BINDING_depenseBudgets);
	}

	public _IDepenseBudget getUnDepenseBudget() {
		return unDepenseBudget;
	}

	public void setUnDepenseBudget(_IDepenseBudget unDepenseBudget) {
		this.unDepenseBudget = unDepenseBudget;
		if (hasBinding(BINDING_unDepenseBudget)) {
			setValueForBinding(unDepenseBudget, BINDING_unDepenseBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOConvention> getLesConventions() {
		return (NSArray<EOConvention>) valueForBinding(BINDING_conventions);
	}

	public _IFactoryDepenseControleConvention getFactoryDepenseControleConvention() {
		return (_IFactoryDepenseControleConvention) valueForBinding(BINDING_factoryDepenseControleConvention);
	}

	public _IDepenseControleConvention getUnDepenseCtrlConvention() {
		return unDepenseCtrlConvention;
	}

	public void setUnDepenseCtrlConvention(_IDepenseControleConvention unDepenseCtrlConvention) {
		this.unDepenseCtrlConvention = unDepenseCtrlConvention;
	}

	public EOConvention getSelection() {
		return getUnDepenseCtrlConvention().convention();
	}

	public void setSelection(EOConvention selection) {
		NSArray<? extends _IDepenseControleConvention> res = getLesDepenseCtrlConventions();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOConvention> actions = (NSArray<EOConvention>) getLesDepenseCtrlConventions().valueForKeyPath(_IDepenseControleConvention._CONVENTION_KEY);
			if (!actions.containsObject(selection)) {
				if (getUnDepenseCtrlConvention() != null) {
					getUnDepenseCtrlConvention().setConventionRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnDepenseCtrlConvention() != null) {
			getFactoryDepenseControleConvention()._supprimer(getUnDepenseBudget().editingContext(), getUnDepenseCtrlConvention());
		}
		return null;
	}

	public NSArray<? extends _IDepenseControleConvention> getLesDepenseCtrlConventions() {
		EOEditingContext edc = getUnDepenseBudget().editingContext();
		NSArray<? extends _IDepenseControleConvention> res = getUnDepenseBudget().depenseControleConventions();
		if (res == null || res.count() < 1) {
			getFactoryDepenseControleConvention()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleConvention(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnDepenseBudget().exercice(), getUnDepenseBudget());
			res = getUnDepenseBudget().depenseControleConventions();
		}
		else {
			if (getLesConventions() != null && getLesConventions().count() > 0) {
				_IDepenseControleConvention ctrleConventionVide = null;
				EOQualifier qual = new EOKeyValueQualifier(_IDepenseControleConvention._CONVENTION_KEY, EOQualifier.QualifierOperatorEqual, null);
				NSArray<? extends _IDepenseControleConvention> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleConventionVide = (_IDepenseControleConvention) arrayVides.lastObject();
				}
				//				qual = EOQualifier.qualifierWithQualifierFormat("convention != nil", null);
				qual = new EONotQualifier(qual);
				NSArray<? extends _IDepenseControleConvention> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum.pourcentage");
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleConventionVide == null) {
						getFactoryDepenseControleConvention()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleConvention(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnDepenseBudget().exercice(),
								getUnDepenseBudget());
						res = getUnDepenseBudget().depenseControleConventions();
					}
					else {
						ctrleConventionVide.setDconTtcSaisie(getUnDepenseBudget().restantTtcControleConvention());
						ctrleConventionVide.setPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleConventionVide != null) {
						Enumeration<? extends _IDepenseControleConvention> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleConventionVide = (_IDepenseControleConvention) enumArrayVides.nextElement();
							getFactoryDepenseControleConvention()._supprimer(edc, ctrleConventionVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnConvention(EOConvention unConvention) {
		this.unConvention = unConvention;
	}

	public EOConvention getUnConvention() {
		return unConvention;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

}