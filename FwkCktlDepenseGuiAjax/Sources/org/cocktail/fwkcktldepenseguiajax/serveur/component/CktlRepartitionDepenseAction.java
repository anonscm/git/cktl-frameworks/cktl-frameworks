/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org> Composant qui permet de gérer les répartitions par action lolf des objets de type
 *         _IDepenseBudget.
 * @binding unDepenseBudget settable. Permet de stocker le _IDepenseBudget en cours.
 * @binding depenseBudgets NSArray<_IDepenseBudget> les depenseBudgets à repartir
 * @binding actions NSArray<EOTypeAction> Les actions disponibles pour la repartition
 * @binding factoryDepenseControleAction _IFactoryDepenseControleAction instance de la factory utilisable pour les methode de suppression, affectation
 *          des repartitions.
 * @binding updateContainerID
 * @binding onObserveSuccess
 * @binding isRepartitionGood Boolean. Doit renvoyer true si la répartition globale est correcte
 * @binding repartMontantFormatter Formatter pour le champ de saisie du montant
 * @binding repartPourcentageFormatter Formatter pour le champ de saisie du pourcentage
 */
public class CktlRepartitionDepenseAction extends ACktlDepenseGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	private static final String BINDING_depenseBudgets = "depenseBudgets";
	private static final String BINDING_unDepenseBudget = "unDepenseBudget";
	private static final String BINDING_actions = "actions";
	private static final String BINDING_factoryDepenseControleAction = "factoryDepenseControleAction";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private _IDepenseBudget unDepenseBudget;
	private _IDepenseControleAction unDepenseCtrlAction;
	private EOTypeAction unTypeAction;
	private String filterText;

	public CktlRepartitionDepenseAction(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<_IDepenseBudget> getDepenseBudgets() {
		return (NSArray<_IDepenseBudget>) valueForBinding(BINDING_depenseBudgets);
	}

	public _IDepenseBudget getUnDepenseBudget() {
		return unDepenseBudget;
	}

	public void setUnDepenseBudget(_IDepenseBudget unDepenseBudget) {
		this.unDepenseBudget = unDepenseBudget;
		if (hasBinding(BINDING_unDepenseBudget)) {
			setValueForBinding(unDepenseBudget, BINDING_unDepenseBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOTypeAction> getLesActions() {
		return (NSArray<EOTypeAction>) valueForBinding(BINDING_actions);
	}

	public _IFactoryDepenseControleAction getFactoryDepenseControleAction() {
		return (_IFactoryDepenseControleAction) valueForBinding(BINDING_factoryDepenseControleAction);
	}

	public _IDepenseControleAction getUnDepenseCtrlAction() {
		return unDepenseCtrlAction;
	}

	public void setUnDepenseCtrlAction(_IDepenseControleAction unDepenseCtrlAction) {
		this.unDepenseCtrlAction = unDepenseCtrlAction;
	}

	public EOTypeAction getActionSelectionnee() {
		return getUnDepenseCtrlAction().typeAction();
	}

	public void setActionSelectionnee(EOTypeAction actionSelectionnee) {
		NSArray<? extends _IDepenseControleAction> res = getLesDepenseCtrlActions();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOTypeAction> actions = (NSArray<EOTypeAction>) getLesDepenseCtrlActions().valueForKeyPath(_IDepenseControleAction._TYPE_ACTION_KEY);
			if (!actions.containsObject(actionSelectionnee)) {
				if (getUnDepenseCtrlAction() != null) {
					getUnDepenseCtrlAction().setTypeActionRelationship(actionSelectionnee);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnDepenseCtrlAction() != null) {
			getFactoryDepenseControleAction().supprimer(getUnDepenseBudget().editingContext(), getUnDepenseCtrlAction());
		}
		return null;
	}

	public NSArray<? extends _IDepenseControleAction> getLesDepenseCtrlActions() {
		EOEditingContext edc = getUnDepenseBudget().editingContext();
		NSArray<? extends _IDepenseControleAction> res = getUnDepenseBudget().depenseControleActions();
		if (res == null || res.count() < 1) {
			getFactoryDepenseControleAction().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleAction(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnDepenseBudget().exercice(), getUnDepenseBudget());
			res = getUnDepenseBudget().depenseControleActions();
		}
		else {
			if (getLesActions() != null && getLesActions().count() > 0) {
				_IDepenseControleAction ctrleActionVide = null;
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeAction = nil", null);
				NSArray<? extends _IDepenseControleAction> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleActionVide = (_IDepenseControleAction) arrayVides.lastObject();
				}
				qual = EOQualifier.qualifierWithQualifierFormat("typeAction != nil", null);
				NSArray<? extends _IDepenseControleAction> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum.pourcentage");
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleActionVide == null) {
						getFactoryDepenseControleAction().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleAction(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnDepenseBudget().exercice(), getUnDepenseBudget());
						res = getUnDepenseBudget().depenseControleActions();
					}
					else {
						ctrleActionVide.setDactTtcSaisie(getUnDepenseBudget().restantTtcControleAction());
						ctrleActionVide.setPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleActionVide != null) {
						Enumeration<? extends _IDepenseControleAction> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleActionVide = (_IDepenseControleAction) enumArrayVides.nextElement();
							getFactoryDepenseControleAction().supprimer(edc, ctrleActionVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnTypeAction(EOTypeAction unTypeAction) {
		this.unTypeAction = unTypeAction;
	}

	public EOTypeAction getUnTypeAction() {
		return unTypeAction;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}
}
