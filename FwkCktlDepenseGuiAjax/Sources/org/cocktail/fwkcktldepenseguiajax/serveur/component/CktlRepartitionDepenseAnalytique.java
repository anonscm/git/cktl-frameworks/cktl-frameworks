/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class CktlRepartitionDepenseAnalytique extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_depenseBudgets = "depenseBudgets";
	private static final String BINDING_unDepenseBudget = "unDepenseBudget";
	private static final String BINDING_factoryDepenseControleAnalytique = "factoryDepenseControleAnalytique";
	private static final String BINDING_codeAnalytiques = "codeAnalytiques";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private _IDepenseBudget unDepenseBudget;
	private _IDepenseControleAnalytique unDepenseCtrlAnalytique;
	private EOCodeAnalytique unCodeAnalytique;
	private String filterText;

	public CktlRepartitionDepenseAnalytique(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<_IDepenseBudget> getDepenseBudgets() {
		return (NSArray<_IDepenseBudget>) valueForBinding(BINDING_depenseBudgets);
	}

	public _IDepenseBudget getUnDepenseBudget() {
		return unDepenseBudget;
	}

	public void setUnDepenseBudget(_IDepenseBudget unDepenseBudget) {
		this.unDepenseBudget = unDepenseBudget;
		if (hasBinding(BINDING_unDepenseBudget)) {
			setValueForBinding(unDepenseBudget, BINDING_unDepenseBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCodeAnalytique> getLesCodeAnalytiques() {
		return (NSArray<EOCodeAnalytique>) valueForBinding(BINDING_codeAnalytiques);
	}

	public _IFactoryDepenseControleAnalytique getFactoryDepenseControleAnalytique() {
		return (_IFactoryDepenseControleAnalytique) valueForBinding(BINDING_factoryDepenseControleAnalytique);
	}

	public _IDepenseControleAnalytique getUnDepenseCtrlAnalytique() {
		return unDepenseCtrlAnalytique;
	}

	public void setUnDepenseCtrlAnalytique(_IDepenseControleAnalytique unDepenseCtrlAnalytique) {
		this.unDepenseCtrlAnalytique = unDepenseCtrlAnalytique;
	}

	public EOCodeAnalytique getSelection() {
		return getUnDepenseCtrlAnalytique().codeAnalytique();
	}

	public void setSelection(EOCodeAnalytique selection) {
		NSArray<? extends _IDepenseControleAnalytique> res = getLesDepenseCtrlAnalytiques();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOCodeAnalytique> actions = (NSArray<EOCodeAnalytique>) getLesDepenseCtrlAnalytiques().valueForKeyPath(_IDepenseControleAnalytique._CODE_ANALYTIQUE_KEY);
			if (!actions.containsObject(selection)) {
				if (getUnDepenseCtrlAnalytique() != null) {
					getUnDepenseCtrlAnalytique().setCodeAnalytiqueRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnDepenseCtrlAnalytique() != null) {
			getFactoryDepenseControleAnalytique()._supprimer(getUnDepenseBudget().editingContext(), getUnDepenseCtrlAnalytique());
		}
		return null;
	}

	public NSArray<? extends _IDepenseControleAnalytique> getLesDepenseCtrlAnalytiques() {
		EOEditingContext edc = getUnDepenseBudget().editingContext();
		NSArray<? extends _IDepenseControleAnalytique> res = getUnDepenseBudget().depenseControleAnalytiques();
		if (res == null || res.count() < 1) {
			getFactoryDepenseControleAnalytique()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleAnalytique(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnDepenseBudget().exercice(), getUnDepenseBudget());
			res = getUnDepenseBudget().depenseControleAnalytiques();
		}
		else {
			if (getLesCodeAnalytiques() != null && getLesCodeAnalytiques().count() > 0) {
				_IDepenseControleAnalytique ctrleAnalytiqueVide = null;
				EOQualifier qual = new EOKeyValueQualifier(_IDepenseControleAnalytique._CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, null);
				NSArray<? extends _IDepenseControleAnalytique> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleAnalytiqueVide = (_IDepenseControleAnalytique) arrayVides.lastObject();
				}
				qual = new EONotQualifier(qual);
				NSArray<? extends _IDepenseControleAnalytique> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum.pourcentage");
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleAnalytiqueVide == null) {
						getFactoryDepenseControleAnalytique()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleAnalytique(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnDepenseBudget().exercice(),
								getUnDepenseBudget());
						res = getUnDepenseBudget().depenseControleAnalytiques();
					}
					else {
						ctrleAnalytiqueVide.setDanaTtcSaisie(getUnDepenseBudget().restantTtcControleAnalytique());
						ctrleAnalytiqueVide.setPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleAnalytiqueVide != null) {
						Enumeration<? extends _IDepenseControleAnalytique> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleAnalytiqueVide = (_IDepenseControleAnalytique) enumArrayVides.nextElement();
							getFactoryDepenseControleAnalytique()._supprimer(edc, ctrleAnalytiqueVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnCodeAnalytique(EOCodeAnalytique unCodeAnalytique) {
		this.unCodeAnalytique = unCodeAnalytique;
	}

	public EOCodeAnalytique getUnCodeAnalytique() {
		return unCodeAnalytique;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

}
