/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleMarche;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Permet de gérer l'affichage (uniquement) des répartition depense/marches.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlRepartitionDepenseMarche extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_depenseBudgets = "depenseBudgets";
	private static final String BINDING_unDepenseBudget = "unDepenseBudget";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";
	private static final String BINDING_isConsultation = "isConsultation";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant HT";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private _IDepenseBudget unDepenseBudget;
	private _IDepenseControleMarche unDepenseCtrlMarche;

	public CktlRepartitionDepenseMarche(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<_IDepenseBudget> getDepenseBudgets() {
		return (NSArray<_IDepenseBudget>) valueForBinding(BINDING_depenseBudgets);
	}

	public _IDepenseBudget getUnDepenseBudget() {
		return unDepenseBudget;
	}

	public void setUnDepenseBudget(_IDepenseBudget unDepenseBudget) {
		this.unDepenseBudget = unDepenseBudget;
		if (hasBinding(BINDING_unDepenseBudget)) {
			setValueForBinding(unDepenseBudget, BINDING_unDepenseBudget);
		}
	}

	public _IDepenseControleMarche getUnDepenseCtrlMarche() {
		return unDepenseCtrlMarche;
	}

	public void setUnDepenseCtrlMarche(_IDepenseControleMarche unDepenseCtrlMarche) {
		this.unDepenseCtrlMarche = unDepenseCtrlMarche;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

	public Boolean isConsultation() {
		return booleanValueForBinding(BINDING_isConsultation, Boolean.TRUE);
	}

	public String getUneRepartitionExistanteDisplayStringKeyPath() {
		return EODepenseControleMarche.ATTRIBUTION_KEY + "." + EOAttribution.LIBELLE_KEY;
	}

	public String getUneRepartitionExistanteMontantKeyPath() {
		return EODepenseControleMarche.DMAR_HT_SAISIE_KEY;
	}

	public String getUneRepartitionExistantePourcentageKeyPath() {
		return EODepenseControleMarche.DMAR_POURCENTAGE;
	}

}
