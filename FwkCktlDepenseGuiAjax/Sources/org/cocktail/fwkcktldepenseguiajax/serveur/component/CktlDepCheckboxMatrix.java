package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.woextensions.WOCheckboxMatrix;

public class CktlDepCheckboxMatrix extends WOCheckboxMatrix {
	protected boolean condition = true;
	protected String updateContainerID = null;

	public CktlDepCheckboxMatrix(WOContext context) {
		super(context);
	}

	public boolean condition() {
		Boolean cond = (Boolean) valueForBinding("condition");

		if (cond == null) {
			cond = Boolean.TRUE;
		}
		return cond.booleanValue();
	}

	public void setCondition(boolean condition) {
		setValueForBinding(Boolean.valueOf(condition), "condition");
	}

	public WOActionResults submit() {
		return performParentAction((String) valueForBinding("action"));
	}

	public String checkBoxID() {
		String checkBoxID = wrapperElementID + "_" + index;
		return checkBoxID;
	}

	/**
	 * @return the updateContainerID
	 */
	public String updateContainerID() {
		return updateContainerID;
	}

	/**
	 * @param updateContainerID the updateContainerID to set
	 */
	public void setUpdateContainerID(String updateContainerID) {
		this.updateContainerID = updateContainerID;
	}

}