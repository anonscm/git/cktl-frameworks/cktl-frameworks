/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlFournisseurSelectCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

public class CktlFournisseurSelect extends ACktlDepenseGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_onSelectAction = "onSelectAction";
	public static final String BINDING_selection = "selection";

	private CktlFournisseurSelectCtrl ctrl;

	public CktlFournisseurSelect(WOContext context) {
		super(context);
	}

	public String getTfRechercheFournisseurId() {
		return getComponentId() + "_RechercheFournisseur";
	}

	public String getCochePersonneMoraleId() {
		return getComponentId() + "_CochePersonneMorale";
	}

	public String getCocheFournisseurInterneId() {
		return getComponentId() + "_CocheFournisseurInterne";
	}

	public CktlFournisseurSelectCtrl ctrl() {
		if (this.ctrl == null) {
			ctrl = (CktlFournisseurSelectCtrl) valueForBinding(BINDING_ctrl);
			if (ctrl == null) {
				this.ctrl = new CktlFournisseurSelectCtrl(this);
			}
		}
		return this.ctrl;
	}

	public WOActionResults onSelectFournisseur() {
		return performParentOnSelectAction();
	}

	public WOActionResults rechercherUnFournisseur() {
		ctrl().rechercherUnFournisseur();
		if (ctrl().getUnFournisseurSelectionne() != null) {
			return performParentOnSelectAction();
		}
		return null;
	}

	public WOActionResults performParentOnSelectAction() {

		if (updateContainerID() != null) {
			AjaxUpdateContainer.safeUpdateContainerWithID(updateContainerID(), context());
		}

		if (hasBinding(BINDING_onSelectAction)) {
			//return performParentAction((String) valueForBinding(BINDING_onSelectAction));
			return (WOActionResults) valueForBinding(BINDING_onSelectAction);
		}
		return null;
	}

	public Boolean isReadOnly() {
		return (Boolean) (hasBinding(BINDING_isReadOnly) ? valueForBinding(BINDING_isReadOnly) : Boolean.FALSE);
	}

	public String getOnBeforeSubmitRecherche() {
		return "function f(oc) {var tfRecherche=document.getElementById('" + getTfRechercheFournisseurId() + "');if (tfRecherche.value.length<3) {alert('Vous devez saisir au moins 3 caracteres.');return false;}return true;}";
	}

	public void setSelection(EOFournisseur unFournisseurSelectionne) {
		setValueForBinding(unFournisseurSelectionne, BINDING_selection);
	}

	public EOFournisseur getSelection() {
		return (EOFournisseur) valueForBinding(BINDING_selection);
	}

	//	public String getComponentOnComplete() {
	//		if (ctrl.getSelectionDone()) {
	//			return "function(oc) {alert('refresh');" + updateContainerID() + "Update();   }";
	//		}
	//		return null;
	//	}

}