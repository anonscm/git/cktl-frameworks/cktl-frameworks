/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepenseguiajax.serveur.beans.SourceCreditsEngagementTO;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlEngagementDirectFormCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class CktlEngagementDirectForm extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_ctrl = "ctrl";

	public CktlEngagementDirectForm(WOContext context) {
		super(context);
	}

	public CktlEngagementDirectFormCtrl ctrl() {
		return (CktlEngagementDirectFormCtrl) valueForBinding(BINDING_ctrl);
	}

	public WOActionResults submitMontantEngagement() {
		try {
			//modificationEnCours = false;
			if (!ctrl().getTypeAchatSelectionCtrl().isTypeAchatMarche()) {
				ctrl().verificationMAPA();
			}
		} catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e);
		}
		return null;
	}

	public WOActionResults submitMontantHtEngagement() {
		try {
			ctrl().onModifierHtOuTva();

			//modificationEnCours = false;
			if (!ctrl().getTypeAchatSelectionCtrl().isTypeAchatMarche()) {
				ctrl().verificationMAPA();
			}

		} catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e);
		}
		return null;
	}

	public String getMontantContainerId() {
		return getComponentId() + "_montants";
	}

	public String getSourcesContainerId() {
		return getComponentId() + "_sources";
	}

	public String getContainerSourceSelectionneeId() {
		return getComponentId() + "_sourceSelectionnee";
	}

	public WOActionResults supprimerSourceCredit() {
		ctrl().supprimerSourceCredit();
		return null;
	}

	public BigDecimal getSourceCreditDisponible() {
		// TODO est ce la meme chose que de passer p&r un EOSource (organ/typeCredit, exer, txProrata) puis de fetcher le budget grace a la source ?
		return ctrl().getSourceCreditTO().getBudgetExecCredit().bdxcDisponible();
	}

	public WOActionResults ajouterCreditsCallback() {
		ctrl().ajouterCreditsCallback();
		return null;
	}

	public WOActionResults annulerCreditsCallback() {
		ctrl().annulerCreditsCallback();
		return null;
	}

	public String popupProrataNoSelectionString() {
		if (cktlDepenseGuiAjaxSession().preselectionTauxProrata().booleanValue()) {
			return null;
		}
		return "-";
	}

	public boolean isSourceCreditSelectionnee() {
		boolean sourceSelectionnee = false;
		SourceCreditsEngagementTO sourceTO = ctrl().getSourceCreditTO();
		if (sourceTO != null && sourceTO.getBudgetExecCredit() != null) {
			sourceSelectionnee = true;
		}
		return sourceSelectionnee;
	}

	public Boolean isMontantsDisabled() {
		Boolean enabled = (ctrl().getSourceCreditTO().isComplete() && ctrl().getTypeAchatTO().isComplete());
		return !enabled.booleanValue();
	}

	public String getOnCompleteUpdateContainerID() {
		return "function(oc){" + updateContainerID() + "Update();}";
	}

}