/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlEngagementsListeCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;

/**
 * @binding displayGroup liste des engagements a afficher sous la forme d'un display group
 * @binding onSelect action appelee lorsqu'un engagement est selectionne
 * @binding filtresManager gestionnaire de filtres.
 * @binding filtrer action filtrer.
 * @author flagouey
 */
public class CktlEngagementListe extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;

	private static final String BINDING_DISPLAY_GROUP = "displayGroup";
	private static final String BINDING_ON_SELECT = "onSelect";
	private static final String BINDING_FILTRES_MANAGER = "filtresManager";
	private static final String BINDING_FILTRER = "filtrer";

	private CktlEngagementsListeCtrl engagementListeCtrl;

	public CktlEngagementListe(WOContext context) {
		super(context);
		this.engagementListeCtrl = new CktlEngagementsListeCtrl();
	}

	public String getFiltreContainerId() {
		return getComponentId() + "_filtres";
	}

	public String getEngagementsContainerId() {
		return getComponentId() + "_engagements";
	}

	public String getTbId() {
		return getComponentId() + "_tb";
	}

	public WODisplayGroup engagementsDisplayGroup() {
		return (WODisplayGroup) valueForBinding(BINDING_DISPLAY_GROUP);
	}

	public String onFiltreComplete() {
		if (valueForBinding(BINDING_updateContainerID) != null) {
			return "function(oc){" + getEngagementsContainerId() + "Update();"
					+ valueForBinding(BINDING_updateContainerID) + "Update();}";
		}
		return "";
	}

	public Boolean hasEngagements() {
		WODisplayGroup engagementsDg = engagementsDisplayGroup();
		return Boolean.valueOf(engagementsDg != null && engagementsDg.displayedObjects().count() > 0);
	}

	public CktlEngagementsListeCtrl getEngagementsListeCtrl() {
		return engagementListeCtrl;
	}

	public void setEngagementsListeCtrl(CktlEngagementsListeCtrl engagementListeCtrl) {
		this.engagementListeCtrl = engagementListeCtrl;
	}

}