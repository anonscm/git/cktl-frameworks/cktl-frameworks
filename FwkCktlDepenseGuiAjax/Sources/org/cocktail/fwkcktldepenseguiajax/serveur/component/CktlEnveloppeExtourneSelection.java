/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.util.List;

import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreBean;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier._IDepenseExtourneCredits;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlEnveloppeExtourneSelectionCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * @author flagouey
 *
 */
public class CktlEnveloppeExtourneSelection extends ACktlDepenseGuiAjaxComponent {

	public static final String BINDING_NIVEAU_ORGAN_PRESELECTION = "niveauOrganPreselection";

	private static final long serialVersionUID = 1L;

	private static final String NIVEAU_ORGAN_GROUP_RADIO_NAME = "niveauOrgan";
	private static final String PREFIX_FILTRE = "Filtre";
	private static final String NIVEAU_UB_ID = "NiveauUBId";
	private static final String NIVEAU_CR_ID = "NiveauCRId";
	private static final String CONTAINER_LISTE_ENVELOPPES_ID = "ListeEnveloppesId";

    public CktlEnveloppeExtourneSelection(WOContext context) {
        super(context);
    }

	public CktlEnveloppeExtourneSelectionCtrl controller() {
		return (CktlEnveloppeExtourneSelectionCtrl) valueForBinding(BINDING_ctrl);
	}

	public WOActionResults selectNiveauOrgan() {
		controller().selectNiveauOrgan();
		return null;
	}

	public String getNiveauOrganGroupRadioName() {
		return NIVEAU_ORGAN_GROUP_RADIO_NAME;
	}

	public Integer getNiveauUBValue() {
		return EOOrgan.ORG_NIV_2;
	}

	public Integer getNiveauCRValue() {
		return EOOrgan.ORG_NIV_3;
	}

	public String getNiveauUBId() {
		return getComponentId() + NIVEAU_UB_ID;
	}

	public String getNiveauCRId() {
		return getComponentId() + NIVEAU_CR_ID;
	}

	public String getContainerListeEnveloppesId() {
		return getComponentId() + CONTAINER_LISTE_ENVELOPPES_ID;
	}

	public boolean isListeEnveloppesEmpty() {
		return isListeEnveloppesEmpty(controller().getListeEnveloppes());
	}

	private boolean isListeEnveloppesEmpty(List<_IDepenseExtourneCredits> listeEnveloppes) {
		return listeEnveloppes == null || listeEnveloppes.isEmpty();
	}

	public String getFiltreUBId() {
		return getComponentId() + PREFIX_FILTRE + CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne.UB.toString() + "Id";
	}

	public CktlDepenseFiltreBean filtreUB() {
		return controller().getFiltresManagerAsMap().get(CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne.UB);
	}

	public String getFiltreCRId() {
		return getComponentId() + PREFIX_FILTRE + CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne.CR.toString() + "Id";
	}

	public CktlDepenseFiltreBean filtreCR() {
		return controller().getFiltresManagerAsMap().get(CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne.CR);
	}

	public String getFiltreSeulementDisponibleId() {
		return getComponentId() + PREFIX_FILTRE + CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne.SEULEMENT_DISPONIBLE.toString() + "Id";
	}

	public CktlDepenseFiltreBean filtreSeulementDisponible() {
		return controller().getFiltresManagerAsMap().get(CktlEnveloppeExtourneSelectionCtrl.FiltresEnveloppeExtourne.SEULEMENT_DISPONIBLE);
	}

	public boolean isFiltreUBEnabled() {
		return EOOrgan.ORG_NIV_2.equals(controller().getNiveauOrganSelected());
	}

	public boolean isFiltreCREnabled() {
		return EOOrgan.ORG_NIV_3.equals(controller().getNiveauOrganSelected());
	}

	public String labelNiveauUB() {
		return EOOrgan.ORG_NIV_2_LIB;
	}

	public String labelNiveauCR() {
		return EOOrgan.ORG_NIV_3_LIB;
	}

}