/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class CktlRepartitionCommandeAnalytique extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_commandeBudgets = "commandeBudgets";
	private static final String BINDING_unCommandeBudget = "unCommandeBudget";
	private static final String BINDING_factoryCommandeControleAnalytique = "factoryCommandeControleAnalytique";
	private static final String BINDING_codeAnalytiques = "codeAnalytiques";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private EOCommandeBudget unCommandeBudget;
	private EOCommandeControleAnalytique unCommandeCtrlAnalytique;
	private EOCodeAnalytique unCodeAnalytique;
	private String filterText;

	public CktlRepartitionCommandeAnalytique(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCommandeBudget> getCommandeBudgets() {
		return (NSArray<EOCommandeBudget>) valueForBinding(BINDING_commandeBudgets);
	}

	public EOCommandeBudget getUnCommandeBudget() {
		return unCommandeBudget;
	}

	public void setUnCommandeBudget(EOCommandeBudget unCommandeBudget) {
		this.unCommandeBudget = unCommandeBudget;
		if (hasBinding(BINDING_unCommandeBudget)) {
			setValueForBinding(unCommandeBudget, BINDING_unCommandeBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCodeAnalytique> getLesCodeAnalytiques() {
		return (NSArray<EOCodeAnalytique>) valueForBinding(BINDING_codeAnalytiques);
	}

	public FactoryCommandeControleAnalytique getFactoryCommandeControleAnalytique() {
		return (FactoryCommandeControleAnalytique) valueForBinding(BINDING_factoryCommandeControleAnalytique);
	}

	public EOCommandeControleAnalytique getUnCommandeCtrlAnalytique() {
		return unCommandeCtrlAnalytique;
	}

	public void setUnCommandeCtrlAnalytique(EOCommandeControleAnalytique unCommandeCtrlAnalytique) {
		this.unCommandeCtrlAnalytique = unCommandeCtrlAnalytique;
	}

	public EOCodeAnalytique getSelection() {
		return getUnCommandeCtrlAnalytique().codeAnalytique();
	}

	public void setSelection(EOCodeAnalytique selection) {
		NSArray<EOCommandeControleAnalytique> res = getLesCommandeCtrlAnalytiques();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOCodeAnalytique> actions = (NSArray<EOCodeAnalytique>) getLesCommandeCtrlAnalytiques().valueForKeyPath(EOCommandeControleAnalytique.CODE_ANALYTIQUE_KEY);
			if (!actions.containsObject(selection)) {
				if (getUnCommandeCtrlAnalytique() != null) {
					getUnCommandeCtrlAnalytique().setCodeAnalytiqueRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnCommandeCtrlAnalytique() != null) {
			getFactoryCommandeControleAnalytique().supprimer(getUnCommandeBudget().editingContext(), getUnCommandeCtrlAnalytique());
		}
		return null;
	}

	public NSArray<EOCommandeControleAnalytique> getLesCommandeCtrlAnalytiques() {
		EOEditingContext edc = getUnCommandeBudget().editingContext();
		NSArray<EOCommandeControleAnalytique> res = getUnCommandeBudget().commandeControleAnalytiques();
		if (res == null || res.count() < 1) {
			getFactoryCommandeControleAnalytique().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControleAnalytique(), BigDecimal.valueOf(0), BigDecimal.valueOf(100), null, getUnCommandeBudget().exercice(), getUnCommandeBudget());
			res = getUnCommandeBudget().commandeControleAnalytiques();
		}
		else {
			if (getLesCodeAnalytiques() != null && getLesCodeAnalytiques().count() > 0) {
				EOCommandeControleAnalytique ctrleAnalytiqueVide = null;
				//				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  "typeAnalytique = nil", null);
				EOQualifier qual = new EOKeyValueQualifier(EOCommandeControleAnalytique.CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, null);
				NSArray<EOCommandeControleAnalytique> arrayVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleAnalytiqueVide = (EOCommandeControleAnalytique) arrayVides.lastObject();
				}
				//				qual = EOQualifier.qualifierWithQualifierFormat("codeAnalytique != nil", null);
				qual = new EONotQualifier(qual);
				NSArray<EOCommandeControleAnalytique> arrayNonVides = EOQualifier.filteredArrayWithQualifier(res, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum." + EOCommandeControleAnalytique.CANA_POURCENTAGE_KEY);
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleAnalytiqueVide == null) {
						getFactoryCommandeControleAnalytique().creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnCommandeBudget().restantTtcControleAnalytique(), BigDecimal.valueOf(0), BigDecimal.valueOf(100).subtract(pourcentageTotal), null, getUnCommandeBudget().exercice(),
								getUnCommandeBudget());
						res = getUnCommandeBudget().commandeControleAnalytiques();
					}
					else {
						ctrleAnalytiqueVide.setCanaTtcSaisie(getUnCommandeBudget().restantTtcControleAnalytique());
						ctrleAnalytiqueVide.setCanaPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleAnalytiqueVide != null) {
						Enumeration<EOCommandeControleAnalytique> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleAnalytiqueVide = (EOCommandeControleAnalytique) enumArrayVides.nextElement();
							getFactoryCommandeControleAnalytique().supprimer(edc, ctrleAnalytiqueVide);
						}
						res = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return res;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnCodeAnalytique(EOCodeAnalytique unCodeAnalytique) {
		this.unCodeAnalytique = unCodeAnalytique;
	}

	public EOCodeAnalytique getUnCodeAnalytique() {
		return unCodeAnalytique;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

}