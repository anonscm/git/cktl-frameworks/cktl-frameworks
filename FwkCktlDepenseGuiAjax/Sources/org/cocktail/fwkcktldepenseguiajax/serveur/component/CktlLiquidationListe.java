/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.util.Map;

import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNumberFormatter;

/**
 * Composant d'affichage d'une liste de liquidations.
 * 
 * @binding displayGroup displaygroup contenant les EODepenseBudget à afficher.
 * @binding filtresMap Map contenant les valeurs des filtres.
 * @binding selectionMultipleEnabled
 * @binding actionAppliquerFiltres
 * @binding actionOnSelect
 * @binding updateContainerIDs NSArray de composant a rafraichir suite à une requête Ajax.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlLiquidationListe extends ACktlDepenseGuiAjaxComponent {

	public static final String FILTRE_FOURNISSEUR = "fournisseur";
	public static final String FILTRE_UB = "ub";
	public static final String FILTRE_PCO = "pco";
	public static final String FILTRE_NO_FACTURE = "noFacture";

	private static final long serialVersionUID = 1L;

	private static final String AJAX_UPD_CONTROL_CENTER = "_ajaxUpdatesControlCenter";
	private static final String RESULTATS_CONTAINER_ID = "_resultatsContainerId";

	private static final String BINDING_displayGroup = "displayGroup";
	private static final String BINDING_filtresMap = "filtresMap";
	private static final String BINDING_selectionMultipleEnabled = "selectionMultipleEnabled";
	private static final String BINDING_actionAppliquerFiltres = "actionAppliquerFiltres";
	private static final String BINDING_actionOnSelect = "actionOnSelect";
	private static final String BINDING_monnaieFormatter = "monnaieFormatter";
	private static final String BINDING_updateContainerIDs = "updateContainerIDs";

	private EODepenseBudget item;

	public CktlLiquidationListe(WOContext context) {
		super(context);
	}

	// ------------------------------------------
	// actions
	// ------------------------------------------

	// ------------------------------------------
	// proprietes calculees
	// ------------------------------------------
	public NSArray<String> containersToUpdateList() {
		NSMutableArray<String> containersToUpdateId = new NSMutableArray<String>(updateContainerIDs());
		containersToUpdateId.add(getResultatsContainerId());
		return containersToUpdateId;
	}

	public String getAjaxUpdatesControlCenterId() {
		return getComponentId() + AJAX_UPD_CONTROL_CENTER;
	}

	public String getResultatsContainerId() {
		return getComponentId() + RESULTATS_CONTAINER_ID;
	}

	public String getFiltreContainerId() {
		return getComponentId() + "_filtres";
	}

	public String getFiltreUB() {
		return getValeurFiltre(FILTRE_UB);
	}

	public void setFiltreUB(String filtreUB) {
		getFiltresMap().put(FILTRE_UB, filtreUB);
	}

	public String getFiltreFournisseur() {
		return getValeurFiltre(FILTRE_FOURNISSEUR);
	}

	public void setFiltreFournisseur(String filtreFournisseur) {
		getFiltresMap().put(FILTRE_FOURNISSEUR, filtreFournisseur);
	}

	public String getFiltreNoFacture() {
		return getValeurFiltre(FILTRE_NO_FACTURE);
	}

	public void setFiltreNoFacture(String filtreNoFacture) {
		getFiltresMap().put(FILTRE_NO_FACTURE, filtreNoFacture);
	}

	public String getFiltrePco() {
		return getValeurFiltre(FILTRE_PCO);
	}

	public void setFiltrePco(String filtrePco) {
		getFiltresMap().put(FILTRE_PCO, filtrePco);
	}

	public NSNumberFormatter monnaieFormatter() {
		// sette un formatter par defaut a la rigueur
		NSNumberFormatter monnaieFormatter = null;
		if (hasBinding(BINDING_monnaieFormatter)) {
			monnaieFormatter = monnaieFormatterProvided();
		}
		return monnaieFormatter;
	}

	protected String getValeurFiltre(String cleFiltre) {
		String valeurResult = "";
		Map<String, String> filtres = getFiltresMap();
		String valeurMap = filtres.get(cleFiltre);
		if (valeurMap != null) {
			valeurResult = valeurMap;
		}
		return valeurResult;
	}

	// ------------------------------------------
	// bindings
	// ------------------------------------------
	public WODisplayGroup displayGroup() {
		return (WODisplayGroup) valueForBinding(BINDING_displayGroup);
	}

	public boolean isSelectionMultipleEnabled() {
		return booleanValueForBinding(BINDING_selectionMultipleEnabled, Boolean.FALSE);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getFiltresMap() {
		return (Map<String, String>) valueForBinding(BINDING_filtresMap);
	}

	public WOActionResults appliquerFiltres() {
		WOActionResults result = null;
		if (hasBinding(BINDING_actionAppliquerFiltres)) {
			result = (WOActionResults) valueForBinding(BINDING_actionAppliquerFiltres);
		}
		return result;
	}

	public WOActionResults onSelect() {
		return (WOActionResults) valueForBinding(BINDING_actionOnSelect);
	}

	public NSNumberFormatter monnaieFormatterProvided() {
		return (NSNumberFormatter) valueForBinding(BINDING_monnaieFormatter);
	}

	@SuppressWarnings("unchecked")
	public NSArray<String> updateContainerIDs() {
		return (NSArray<String>) valueForBinding(BINDING_updateContainerIDs);
	}

	// ------------------------------------------
	// getter - setter
	// ------------------------------------------
	public EODepenseBudget getItem() {
		return item;
	}

	public void setItem(EODepenseBudget item) {
		this.item = item;
	}
}