/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.component;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @binding depenseBudgets
 * @binding unDepenseBudget
 * @binding codeExers
 * @binding factoryDepenseControleHorsMarche
 * @binding typeAchat
 */
public class CktlRepartitionDepenseNomenclature extends ACktlDepenseGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_depenseBudgets = "depenseBudgets";
	private static final String BINDING_unDepenseBudget = "unDepenseBudget";
	private static final String BINDING_factoryDepenseControleHorsMarche = "factoryDepenseControleHorsMarche";
	private static final String BINDING_codeExers = "codeExers";
	private static final String BINDING_typeAchat = "typeAchat";

	private static final String BINDING_groupeTitre = "groupeTitre";
	private static final String BINDING_repartMontantTitre = "repartMontantTitre";
	private static final String BINDING_repartPourcentageTitre = "repartPourcentageTitre";
	private static final String BINDING_objetsARepartirTitre = "objetsARepartirTitre";

	private static final String DEFAULT_groupeTitre = null;
	private static final String DEFAULT_repartMontantTitre = "Montant TTC";
	private static final String DEFAULT_repartPourcentageTitre = "Pourcentage";
	private static final String DEFAULT_objetsARepartirTitre = "";

	private _IDepenseBudget unDepenseBudget;
	private _IDepenseControleHorsMarche unDepenseCtrlHorsMarche;
	private EOCodeExer unCodeExer;
	private String filterText;

	public CktlRepartitionDepenseNomenclature(WOContext arg0) {
		super(arg0);
	}

	@SuppressWarnings("unchecked")
	public NSArray<_IDepenseBudget> getDepenseBudgets() {
		return (NSArray<_IDepenseBudget>) valueForBinding(BINDING_depenseBudgets);
	}

	public _IDepenseBudget getUnDepenseBudget() {
		return unDepenseBudget;
	}

	public void setUnDepenseBudget(_IDepenseBudget unDepenseBudget) {
		this.unDepenseBudget = unDepenseBudget;
		if (hasBinding(BINDING_unDepenseBudget)) {
			setValueForBinding(unDepenseBudget, BINDING_unDepenseBudget);
		}
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOCodeExer> getLesCodeExers() {
		return (NSArray<EOCodeExer>) valueForBinding(BINDING_codeExers);
	}

	public _IFactoryDepenseControleHorsMarche getFactoryDepenseControleHorsMarche() {
		return (_IFactoryDepenseControleHorsMarche) valueForBinding(BINDING_factoryDepenseControleHorsMarche);
	}

	public EOTypeAchat getTypeAchat() {
		return (EOTypeAchat) valueForBinding(BINDING_typeAchat);
	}

	public _IDepenseControleHorsMarche getUnDepenseCtrlHorsMarche() {
		return unDepenseCtrlHorsMarche;
	}

	public void setUnDepenseCtrlHorsMarche(_IDepenseControleHorsMarche unDepenseCtrlHorsMarche) {
		this.unDepenseCtrlHorsMarche = unDepenseCtrlHorsMarche;
	}

	public EOCodeExer getCodeExerSelection() {
		return getUnDepenseCtrlHorsMarche().codeExer();
	}

	public void setCodeExerSelection(EOCodeExer selection) {
		NSArray<? extends _IDepenseControleHorsMarche> res = getLesDepenseCtrlHorsMarches();
		if (res != null) {
			@SuppressWarnings("unchecked")
			NSArray<EOCodeExer> existants = (NSArray<EOCodeExer>) getLesDepenseCtrlHorsMarches().valueForKeyPath(_IDepenseControleHorsMarche._CODE_EXER_KEY);
			if (!existants.containsObject(selection)) {
				if (getUnDepenseCtrlHorsMarche() != null) {
					getUnDepenseCtrlHorsMarche().setCodeExerRelationship(selection);
				}
			}
		}
	}

	public WOActionResults supprimerRepartition() throws Exception {
		if (getUnDepenseCtrlHorsMarche() != null) {
			getFactoryDepenseControleHorsMarche()._supprimer(getUnDepenseBudget().editingContext(), getUnDepenseCtrlHorsMarche());
		}
		return null;
	}

	public NSArray<? extends _IDepenseControleHorsMarche> getLesDepenseCtrlHorsMarches() {
		EOEditingContext edc = getUnDepenseBudget().editingContext();
		NSArray<? extends _IDepenseControleHorsMarche> lesDepenseControleHorsMarches = getUnDepenseBudget().depenseControleHorsMarches();
		if (lesDepenseControleHorsMarches == null || lesDepenseControleHorsMarches.count() < 1) {
			getFactoryDepenseControleHorsMarche()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleHorsMarche(), getUnDepenseBudget().depMontantBudgetaire(), BigDecimal.valueOf(100), null,
					getTypeAchat(),
					getUnDepenseBudget().exercice(), getUnDepenseBudget());
			lesDepenseControleHorsMarches = getUnDepenseBudget().depenseControleHorsMarches();
		}
		else {
			if (getLesCodeExers() != null && getLesCodeExers().count() > 0) {
				_IDepenseControleHorsMarche ctrleHorsMarcheVide = null;
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("codeExer = nil", null);
				NSArray<? extends _IDepenseControleHorsMarche> arrayVides = EOQualifier.filteredArrayWithQualifier(lesDepenseControleHorsMarches, qual);
				if (arrayVides != null && arrayVides.count() > 0) {
					ctrleHorsMarcheVide = (_IDepenseControleHorsMarche) arrayVides.lastObject();
				}
				qual = EOQualifier.qualifierWithQualifierFormat("codeExer != nil", null);
				NSArray<? extends _IDepenseControleHorsMarche> arrayNonVides = EOQualifier.filteredArrayWithQualifier(lesDepenseControleHorsMarches, qual);
				BigDecimal pourcentageTotal = (BigDecimal) arrayNonVides.valueForKeyPath("@sum.pourcentage");
				if (pourcentageTotal.floatValue() < 100) {
					if (ctrleHorsMarcheVide == null) {
						getFactoryDepenseControleHorsMarche()._creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), getUnDepenseBudget().restantTtcControleHorsMarche(), getUnDepenseBudget().depMontantBudgetaire(),
								BigDecimal.valueOf(100).subtract(
										pourcentageTotal),
								null, getTypeAchat(), getUnDepenseBudget().exercice(), getUnDepenseBudget());
						lesDepenseControleHorsMarches = getUnDepenseBudget().depenseControleHorsMarches();
					}
					else {
						ctrleHorsMarcheVide.setDhomTtcSaisie(getUnDepenseBudget().restantTtcControleHorsMarche());
						ctrleHorsMarcheVide.setPourcentage(BigDecimal.valueOf(100).subtract(pourcentageTotal));
					}
				}
				else if (pourcentageTotal.floatValue() >= 100) {
					if (ctrleHorsMarcheVide != null) {
						Enumeration<? extends _IDepenseControleHorsMarche> enumArrayVides = arrayVides.objectEnumerator();
						while (enumArrayVides.hasMoreElements()) {
							ctrleHorsMarcheVide = (_IDepenseControleHorsMarche) enumArrayVides.nextElement();
							getFactoryDepenseControleHorsMarche()._supprimer(edc, ctrleHorsMarcheVide);
						}
						lesDepenseControleHorsMarches = arrayNonVides;
					}
				}
				setFilterText(null);
			}
		}
		return lesDepenseControleHorsMarches;
	}

	/**
	 * @return the filterText
	 */
	public String getFilterText() {
		return filterText;
	}

	/**
	 * @param filterText the filterText to set
	 */
	public void setFilterText(String filterText) {
		this.filterText = filterText;
	}

	public void setUnCodeExer(EOCodeExer unCodeExer) {
		this.unCodeExer = unCodeExer;
	}

	public EOCodeExer getUnCodeExer() {
		return unCodeExer;
	}

	public String getRepartMontantTitre() {
		return stringValueForBinding(BINDING_repartMontantTitre, DEFAULT_repartMontantTitre);
	}

	public String getRepartPourcentageTitre() {
		return stringValueForBinding(BINDING_repartPourcentageTitre, DEFAULT_repartPourcentageTitre);
	}

	public String getObjetsARepartirTitre() {
		return stringValueForBinding(BINDING_objetsARepartirTitre, DEFAULT_objetsARepartirTitre);
	}

	public String getGroupeTitre() {
		return stringValueForBinding(BINDING_groupeTitre, DEFAULT_groupeTitre);
	}

	public Object objetARepartirGroupeKeyPath() {
		return (unCodeExer != null ? unCodeExer.getPere() : null);
	}

}
