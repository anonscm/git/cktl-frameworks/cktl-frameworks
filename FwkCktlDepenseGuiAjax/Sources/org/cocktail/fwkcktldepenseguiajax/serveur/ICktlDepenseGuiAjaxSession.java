package org.cocktail.fwkcktldepenseguiajax.serveur;

import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNumberFormatter;

public interface ICktlDepenseGuiAjaxSession {

	Boolean preselectionTauxProrata();

	NSNumberFormatter getMonnaieFormatter();

	EOUtilisateur utilisateurInContext(EOEditingContext ed);

	_CktlBasicDataBus dataBus();
}
