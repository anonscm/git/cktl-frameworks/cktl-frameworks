/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.finder.FinderTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.TypeAchatConfiguration;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.TypeAchatTO;
import org.cocktail.fwkcktldepenseguiajax.serveur.enums.ETypeMarche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class CktlTypeAchatSelectionCtrl extends ACktlDepenseGuiAjaxComponentFreeCtrl {

	private String filterTextAttribution;
	private String filterTextCodeNomenclature;;

	private TypeAchatTO typeAchatTO;
	private TypeAchatConfiguration typeAchatConfiguration;
	private Map<ETypeMarche, Boolean> etatsTypesMarcheCache;

	/* Variable d'iteration. */
	private EOAttribution currentAttribution;
	private EOCtrlSeuilMAPA currentCodeNomenclature;

	public CktlTypeAchatSelectionCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
		super(session, edc, exercice);
		this.filterTextAttribution = "";
		this.etatsTypesMarcheCache = new HashMap<ETypeMarche, Boolean>();
	}

	public void synchroniseTypeAchatFromTypeMarche() {
		ETypeMarche typeMarche = getTypeAchatTO().getTypeMarcheSelectionne();
		switch (typeMarche) {
		case HORS_MARCHE:
			getTypeAchatTO().setTypeAchat(FinderTypeAchat.getTypeAchat(edc(), EOTypeAchat.MARCHE_NON_FORMALISE));
			break;
		case MONOPOLE:
			getTypeAchatTO().setTypeAchat(FinderTypeAchat.getTypeAchat(edc(), EOTypeAchat.MONOPOLE));
			break;
		case TROISCMP:
			getTypeAchatTO().setTypeAchat(FinderTypeAchat.getTypeAchat(edc(), EOTypeAchat.TROIS_CMP));
			break;
		case SANS_ACHAT:
			getTypeAchatTO().setTypeAchat(FinderTypeAchat.getTypeAchat(edc(), EOTypeAchat.SANS_ACHAT));
			break;

		case MARCHE:
			getTypeAchatTO().setTypeAchat(null);
			break;

		default:
			break;
		}
	}

	public WOActionResults selectTypeAchat() {
		synchroniseTypeAchatFromTypeMarche();
		if (isTypeAchatMarche() && typeAchatConfiguration.isAttributionUnique()) {
			typeAchatTO.setAttributionSelectionnee(typeAchatConfiguration.getAttributions().get(0));
			typeAchatTO.setCodeNomenclatureSelectionne(null);
		}
		else {
			typeAchatTO.setAttributionSelectionnee(null);
		}
		return null;
	}

	public WOActionResults selectAttribution() {
		return null;
	}

	public WOActionResults selectCodeNomenclature() {
		return null;
	}

	public boolean isTypeAchatMarche() {
		return ETypeMarche.MARCHE.equals(typeAchatTO.getTypeMarcheSelectionne());
	}

	public boolean isTypeAchatHorsMarche() {
		return ETypeMarche.HORS_MARCHE.equals(typeAchatTO.getTypeMarcheSelectionne());
	}

	public boolean isTypeAchatDisabled(ETypeMarche typeMarche) {
		if (!etatsTypesMarcheCache.containsKey(typeMarche)) {
			etatsTypesMarcheCache.put(typeMarche, Boolean.valueOf(typeAchatConfiguration.isDisabled(typeMarche)));
		}
		return etatsTypesMarcheCache.get(typeMarche).booleanValue();
	}

	public void updateConfiguration(TypeAchatConfiguration configuration) {
		this.etatsTypesMarcheCache.clear();
		this.typeAchatConfiguration = configuration;

		updateTypeMarcheSelectionne(configuration.getAttributions());
		updateCodeNomenclatureSelectionne(configuration.getCodesNomenclatures());
	}

	private void updateTypeMarcheSelectionne(List<EOAttribution> attributions) {
		this.typeAchatTO.setTypeMarcheSelectionne(
				attributions.isEmpty() ? ETypeMarche.HORS_MARCHE : ETypeMarche.MARCHE);
		if (attributions.isEmpty()) {
			this.typeAchatTO.setTypeAchat(FinderTypeAchat.getTypeAchat(edc(), EOTypeAchat.MARCHE_NON_FORMALISE));
		}
		else {
			this.typeAchatTO.setTypeAchat(null);
		}
	}

	private void updateCodeNomenclatureSelectionne(List<EOCtrlSeuilMAPA> codesNomenclatures) {
		if (codesNomenclatures != null && codesNomenclatures.size() == 1) {
			this.typeAchatTO.setCodeNomenclatureSelectionne(codesNomenclatures.get(0));
		}
	}

	/* Getters / Setters */

	public String getFilterTextAttribution() {
		return filterTextAttribution;
	}

	public void setFilterTextAttribution(String filterTextAttribution) {
		this.filterTextAttribution = filterTextAttribution;
	}

	public EOAttribution getCurrentAttribution() {
		return currentAttribution;
	}

	public void setCurrentAttribution(EOAttribution currentAttribution) {
		this.currentAttribution = currentAttribution;
	}

	public TypeAchatTO getTypeAchatTO() {
		return typeAchatTO;
	}

	public void setTypeAchatTO(TypeAchatTO typeAchatTO) {
		this.typeAchatTO = typeAchatTO;
	}

	public List<EOAttribution> attributions() {
		return typeAchatConfiguration.getAttributions();
	}

	public NSArray<EOCtrlSeuilMAPA> codesNomenclatures() {
		//TODO utiliser du cache pour eviter de recreer un NSArray a chaque appel
		return new NSArray<EOCtrlSeuilMAPA>(typeAchatConfiguration.getCodesNomenclatures());
	}

	public String getFilterTextCodeNomenclature() {
		return filterTextCodeNomenclature;
	}

	public void setFilterTextCodeNomenclature(String filterTextCodeNomenclature) {
		this.filterTextCodeNomenclature = filterTextCodeNomenclature;
	}

	public EOCtrlSeuilMAPA getCurrentCodeNomenclature() {
		return currentCodeNomenclature;
	}

	public void setCurrentCodeNomenclature(EOCtrlSeuilMAPA currentCodeNomenclature) {
		this.currentCodeNomenclature = currentCodeNomenclature;
	}

	public void reset() {
		setTypeAchatTO(new TypeAchatTO());
	}

}
