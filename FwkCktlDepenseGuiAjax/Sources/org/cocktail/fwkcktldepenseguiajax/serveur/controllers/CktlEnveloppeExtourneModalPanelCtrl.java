package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;

import com.webobjects.eocontrol.EOEditingContext;

public class CktlEnveloppeExtourneModalPanelCtrl extends ACktlDepenseGuiAjaxComponentFreeCtrl {

	private CktlEnveloppeExtourneSelectionCtrl enveloppeExtourneSelectionCtrl;

	public CktlEnveloppeExtourneModalPanelCtrl(
			ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
		super(session, edc, exercice);
		this.enveloppeExtourneSelectionCtrl = new CktlEnveloppeExtourneSelectionCtrl(session, edc, exercice);
		enveloppeExtourneSelectionCtrl.filtrer();
	}

	public void valider() {
	}

	public void annuler() {
	}

	// Getters - Setters
	public CktlEnveloppeExtourneSelectionCtrl getEnveloppeExtourneSelectionCtrl() {
		return enveloppeExtourneSelectionCtrl;
	}

	public void setEnveloppeExtourneSelectionCtrl(CktlEnveloppeExtourneSelectionCtrl enveloppeExtourneSelectionCtrl) {
		this.enveloppeExtourneSelectionCtrl = enveloppeExtourneSelectionCtrl;
	}

}
