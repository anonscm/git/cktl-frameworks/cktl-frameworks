/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import org.cocktail.fwkcktldepense.server.finder.FinderBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.process.ProcessPreference;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Controleur dont doivent heriter les controleurs des composants de selection des sources de credit.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class ACktlSourceSelectionCtrl extends ACktlDepenseGuiAjaxComponentFreeCtrl {

	private NSArray<NSDictionary<String, Object>> lesSourcesUtilisateur = null;
	private NSArray<NSDictionary<String, Object>> lesSourcesUtilisateurAAfficher = null;
	private NSMutableDictionary<String, Object> bdgsSourcesUtilisateur = new NSMutableDictionary<String, Object>();
	private NSArray<EOConvention> lesConventions = null;
	private boolean isAfficherCreditsPreferesDisabled = false;
	private NSDictionary<String, Object> uneSourceUtilisateur = null;

	private String filterTextConventions = null;

	private static Integer RADIO_PREFERES = Integer.valueOf(1);
	private static Integer RADIO_DISPOS = Integer.valueOf(2);
	private static Integer RADIO_PAR_CONVENTION = Integer.valueOf(3);
	private static Integer RADIO_TOUS = Integer.valueOf(4);

	private Integer selectedRadioFiltre = RADIO_DISPOS;

	public ACktlSourceSelectionCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
		super(session, edc, exercice);
		//init();
	}

	protected void init() {
		// Recuperation des credits preferes si il y en a sinon les credits dispo
		EOEditingContext ed = edc();
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();

		bindings.setObjectForKey(session().utilisateurInContext(ed), "utilisateur");
		bindings.setObjectForKey(getExercice(), "exercice");
		bindings.setObjectForKey(Boolean.TRUE, "lignesPreferees");

		lesSourcesUtilisateur = getRawRowBudgets(ed, bindings);
		if (lesSourcesUtilisateur.count() == 0) {
			setSelectedRadioFiltre(RADIO_DISPOS);
			bindings.removeObjectForKey("lignesPreferees");
			bindings.setObjectForKey(Boolean.TRUE, "creditsDispo");
			lesSourcesUtilisateur = getRawRowBudgets(ed, bindings);
		}
		else {
			setSelectedRadioFiltre(RADIO_PREFERES);
		}
		lesSourcesUtilisateurAAfficher = lesSourcesUtilisateur;
	}

	public NSMutableDictionary<String, Object> getBdgsSourcesUtilisateur() {
		return bdgsSourcesUtilisateur;
	}

	public void setBdgsSourcesUtilisateur(NSMutableDictionary<String, Object> bdgsSourcesUtilisateur) {
		this.bdgsSourcesUtilisateur = bdgsSourcesUtilisateur;
	}

	public NSArray<NSDictionary<String, Object>> getLesSourcesUtilisateur() {
		if (lesSourcesUtilisateur == null) {
			init();
		}

		return lesSourcesUtilisateur;
	}

	protected NSArray<NSDictionary<String, Object>> getRawRowBudgets(EOEditingContext ed, NSMutableDictionary<String, Object> bindings) {
		return FinderBudget.getRawRowBudgets(ed, bindings);
	}

	public void filtrerSourcesUtilisateur() {
		NSMutableDictionary<String, Object> bdgs = getBdgsSourcesUtilisateur();
		if (bdgs.count() > 0) {
			EOConvention conv = (EOConvention) bdgs.objectForKey("convention");
			if (conv != null) {
				EOEditingContext ed = edc();
				EOExercice exercice = getExercice();
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
				bindings.setObjectForKey(exercice, "exercice");
				bindings.setObjectForKey(conv, "convention");
				bindings.setObjectForKey(session().utilisateurInContext(ed), "utilisateur");
				lesSourcesUtilisateur = getRawRowBudgets(ed, bindings);
				lesSourcesUtilisateurAAfficher = lesSourcesUtilisateur;
			}
			else {
				String ub = (String) bdgs.objectForKey("organUb");
				String cr = (String) bdgs.objectForKey("organCr");
				String sscr = (String) bdgs.objectForKey("organSsCr");
				String tc = (String) bdgs.objectForKey("tcdCode");

				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				if (ub != null && ub.equals("") == false) {
					EOQualifier ubQual = ERXQ.like("ORGANUB", ub + "*");

					qualifiers.addObject(ubQual);
				}
				if (cr != null && cr.equals("") == false) {
					EOQualifier crQual = ERXQ.likeInsensitive("ORGANCR", cr + "*");
					qualifiers.addObject(crQual);
				}
				if (sscr != null && sscr.equals("") == false) {
					EOQualifier sscrQual = ERXQ.likeInsensitive("ORGANSSCR", sscr + "*");
					qualifiers.addObject(sscrQual);
				}
				if (tc != null && tc.equals("") == false) {
					EOQualifier tcQual = ERXQ.like("TYPECREDIT", tc + "*");
					qualifiers.addObject(tcQual);
				}
				EOAndQualifier qualFiltre = new EOAndQualifier(qualifiers);
				lesSourcesUtilisateurAAfficher = EOQualifier.filteredArrayWithQualifier(lesSourcesUtilisateur, qualFiltre);
			}
		}
		else {
			filtrer1SourcesUtilisateur();
		}
	}

	public void filtrer1SourcesUtilisateur() {
		EOEditingContext ed = edc();

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(session().utilisateurInContext(ed), "utilisateur");
		bindings.setObjectForKey(getExercice(), "exercice");
		lesSourcesUtilisateur = null;

		if (!isAfficherCreditsParConvention() && !isAfficherTousCredits()) {
			getBdgsSourcesUtilisateur().removeAllObjects();
			filterTextConventions = null;
			if (isAfficherCreditsPreferes()) {
				bindings.setObjectForKey(Boolean.TRUE, "lignesPreferees");
				lesSourcesUtilisateur = getRawRowBudgets(ed, bindings);
				if (lesSourcesUtilisateur.count() == 0) {
					setSelectedRadioFiltre(RADIO_DISPOS);
				}
			}
			if (isAfficherCreditsDisponibles()) {
				bindings.removeObjectForKey("lignesPreferees");
				bindings.setObjectForKey(Boolean.TRUE, "creditsDispo");
				lesSourcesUtilisateur = getRawRowBudgets(ed, bindings);
			}
		}
		else {
			lesSourcesUtilisateur = getRawRowBudgets(ed, bindings);
		}
		lesSourcesUtilisateurAAfficher = lesSourcesUtilisateur;

	}

	public void trierDispoSourcesUtilisateur() {
		NSArray<NSDictionary<String, Object>> sources = getLesSourcesUtilisateur();
		EOSortOrdering sortDispo = EOSortOrdering.sortOrderingWithKey(EOBudgetExecCredit.BDXC_DISPONIBLE_KEY, EOSortOrdering.CompareDescending);
		lesSourcesUtilisateur = EOSortOrdering.sortedArrayUsingKeyOrderArray(sources, new NSArray<EOSortOrdering>(sortDispo));
	}

	public void setLesSourcesUtilisateur(NSArray<NSDictionary<String, Object>> lesSourcesUtilisateur) {
		this.lesSourcesUtilisateur = lesSourcesUtilisateur;
	}

	public NSArray<NSDictionary<String, Object>> getLesSourcesUtilisateurAAfficher() {
		if (lesSourcesUtilisateurAAfficher == null) {
			lesSourcesUtilisateurAAfficher = getLesSourcesUtilisateur();
			filtrer1SourcesUtilisateur();
		}
		return lesSourcesUtilisateurAAfficher;
	}

	public void setLesSourcesUtilisateurAAfficher(NSArray<NSDictionary<String, Object>> lesSourcesUtilisateurAAfficher) {
		this.lesSourcesUtilisateurAAfficher = lesSourcesUtilisateurAAfficher;
	}

	public NSArray<EOConvention> getLesConventions() {
		if (lesConventions == null) {
			EOEditingContext ed = edc();
			EOExercice exercice = FinderExercice.getExercicePourDate(ed, new NSTimestamp());
			NSDictionary<String, Object> bindings = new NSDictionary<String, Object>(exercice, "exercice");
			lesConventions = FinderConvention.getConventions(ed, bindings);
		}
		return lesConventions;
	}

	public void setLesConventions(NSArray<EOConvention> lesConventions) {
		this.lesConventions = lesConventions;
	}

	public boolean isAfficherCreditsPreferesDisabled() {
		return isAfficherCreditsPreferesDisabled;
	}

	public boolean isAfficherCreditsPreferes() {
		return RADIO_PREFERES.equals(getSelectedRadioFiltre());
		//return isAfficherCreditsPreferes;
	}

	public void setIsAfficherCreditsPreferes(boolean isAfficherCreditsPreferes) {
		if (isAfficherCreditsPreferes) {
			setSelectedRadioFiltre(RADIO_PREFERES);
		}
		//this.isAfficherCreditsPreferes = isAfficherCreditsPreferes;
	}

	public boolean isAfficherCreditsParConvention() {
		return RADIO_PAR_CONVENTION.equals(getSelectedRadioFiltre());
	}

	public void setIsAfficherCreditsParConvention(boolean isAfficherCreditsParConvention) {
		if (isAfficherCreditsParConvention) {
			setSelectedRadioFiltre(RADIO_PAR_CONVENTION);
		}
		//this.isAfficherCreditsParConvention = isAfficherCreditsParConvention;
	}

	public boolean isAfficherCreditsDisponibles() {
		return RADIO_DISPOS.equals(getSelectedRadioFiltre());
		//return isAfficherCreditsDisponibles;
	}

	public void setIsAfficherCreditsDisponibles(boolean isAfficherCreditsDisponibles) {
		if (isAfficherCreditsDisponibles) {
			setSelectedRadioFiltre(RADIO_DISPOS);
		}
		//this.isAfficherCreditsDisponibles = isAfficherCreditsDisponibles;
	}

	public boolean isAfficherTousCredits() {
		return RADIO_TOUS.equals(getSelectedRadioFiltre());
		//return isAfficherTousCredits;
	}

	public void setIsAfficherTousCredits(boolean isAfficherTousCredits) {
		if (isAfficherTousCredits) {
			setSelectedRadioFiltre(RADIO_TOUS);
		}
		//this.isAfficherTousCredits = isAfficherTousCredits;
	}

	public boolean isRechercherParConvention() {
		return isAfficherCreditsParConvention();
	}

	public void ajouterUneSourceAuxPreferes() {
		NSDictionary<String, Object> uneSource = getUneSourceUtilisateur();
		EOEditingContext edc = edc();
		NSArray<EOBudgetExecCredit> credits = FinderBudget.getBudgets(edc, new NSArray<NSDictionary<String, Object>>(uneSource));
		if (credits != null && credits.count() > 0) {
			@SuppressWarnings("unchecked")
			NSArray<EOOrgan> organs = (NSArray<EOOrgan>) credits.valueForKeyPath("organ");
			ProcessPreference.insererOrgansPreferes(session().dataBus(), session().utilisateurInContext(edc), organs);
			isAfficherCreditsPreferesDisabled = false;
			filtrer1SourcesUtilisateur();
		}

	}

	public void supprimerSourcePreferee() {
		NSDictionary<String, Object> uneSource = getUneSourceUtilisateur();
		EOEditingContext edc = edc();
		NSArray<EOBudgetExecCredit> credits = FinderBudget.getBudgets(edc, new NSArray<NSDictionary<String, Object>>(uneSource));
		if (credits != null && credits.count() > 0) {
			@SuppressWarnings("unchecked")
			NSArray<EOOrgan> organs = (NSArray<EOOrgan>) credits.valueForKeyPath("organ");
			ProcessPreference.supprimerOrgansPreferes(session().dataBus(), session().utilisateurInContext(edc), organs);
			filtrer1SourcesUtilisateur();
		}
	}

	public String getFilterTextConventions() {
		return filterTextConventions;
	}

	public void setFilterTextConventions(String filterTextConventions) {
		this.filterTextConventions = filterTextConventions;
	}

	public NSDictionary<String, Object> getUneSourceUtilisateur() {
		return uneSourceUtilisateur;
	}

	public void setUneSourceUtilisateur(NSDictionary<String, Object> uneSource) {
		this.uneSourceUtilisateur = uneSource;
	}

	public boolean isEligible() {
		boolean isEligible = false;
		NSDictionary<String, Object> laSource = getUneSourceUtilisateur();
		if (laSource != null) {
			Double disponible = (Double) laSource.objectForKey("DISPONIBLE");
			Double isProrata = (Double) laSource.objectForKey("ISPRORATA");
			Double isDroit = (Double) laSource.objectForKey("DROIT");
			if ((disponible.doubleValue() > 0) &&
					(isProrata.doubleValue() > 0) && isDroit.doubleValue() == 1) {
				isEligible = true;
			}
		}
		return isEligible;
	}

	public Integer getSelectedRadioFiltre() {
		return selectedRadioFiltre;
	}

	public void setSelectedRadioFiltre(Integer selectedRadio) {
		this.selectedRadioFiltre = selectedRadio;
	}

}
