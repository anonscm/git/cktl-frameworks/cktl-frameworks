/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.component.ACktlDepenseGuiAjaxComponent;

import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;

public class ACktlDepenseGuiAjaxComponentCtrl {

	private ACktlDepenseGuiAjaxComponent myComponent;
	private WODisplayGroup content;

	private EOEditingContext edc;
	private ICktlDepenseGuiAjaxSession session;
	private EOExercice exercice;

	public ACktlDepenseGuiAjaxComponentCtrl(ACktlDepenseGuiAjaxComponent component) {
		this.myComponent = component;
		this.content = new WODisplayGroup();
	}

	public ACktlDepenseGuiAjaxComponentCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
		this.edc = edc;
		this.session = session;
		this.exercice = exercice;
		this.content = new WODisplayGroup();
	}

	public ACktlDepenseGuiAjaxComponentCtrl() {
	}

	protected EOEditingContext edc() {
		return (myComponent != null ? myComponent.edc() : edc);
	}

	public ACktlDepenseGuiAjaxComponent getMyComponent() {
		return myComponent;
	}

	public void setMyComponent(ACktlDepenseGuiAjaxComponent myComponent) {
		this.myComponent = myComponent;
	}

	public WODisplayGroup getContent() {
		return content;
	}

	public ICktlDepenseGuiAjaxSession getSession() {
		return session;
	}

	public EOExercice getExercice() {
		return exercice;
	}

}
