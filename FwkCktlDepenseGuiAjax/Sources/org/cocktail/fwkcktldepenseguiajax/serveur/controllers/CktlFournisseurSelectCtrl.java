/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.finder.FinderFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepenseguiajax.serveur.component.CktlFournisseurSelect;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class CktlFournisseurSelectCtrl extends ACktlDepenseGuiAjaxComponentCtrl {
	private Map<String, String> filtres = new HashMap<String, String>();
	private Boolean isRechercheFournisseurDisabled = Boolean.FALSE;
	private Boolean isRechercheFournisseurInterneChecked = Boolean.FALSE;
	private Boolean isRecherchePersonneMoraleChecked = Boolean.TRUE;
	private Boolean isTfFournisseurDisabled = Boolean.FALSE;
	private Boolean isCocheFournisseurInterneDisabled = Boolean.FALSE;
	private Boolean isCochePersonneMoraleDisabled = Boolean.FALSE;
	private Boolean isPopupFournisseursDisabled = Boolean.FALSE;
	//private Boolean isAfficherListeFournisseur = Boolean.FALSE;
	private String rechercheRapideStr;
	//	private EOEditingContext edc;

	private NSDictionary<String, Object> selectedFournisseur;
	private NSDictionary<String, Object> unFournisseur;
	private NSArray<NSDictionary<String, Object>> fournisseurs;
	private EOFournisseur unFournisseurSelectionne;

	private CktlFournisseurListeCtrl fournisseurListCtrl;

	//	private Boolean selectionDone = Boolean.FALSE;

	public CktlFournisseurSelectCtrl(CktlFournisseurSelect view) {
		super(view);
		fournisseurListCtrl = new CktlFournisseurListeCtrl(edc());
	}

	public Boolean isAfficherRechercheFournisseur() {
		boolean isAfficherRechercheFournisseur = false;
		//		if (getUnFournisseurSelectionne() == null && isCmdeMarche == false && (fournisseurs == null || fournisseurs.count() == 0)) {
		//			isAfficherRechercheFournisseur = true;
		//		}
		if ((fournisseurs == null || fournisseurs.count() == 0)) {
			isAfficherRechercheFournisseur = true;
		}
		return isAfficherRechercheFournisseur;
	}

	public Boolean isRechercheFournisseurDisabled() {
		return isRechercheFournisseurDisabled;
	}

	public Boolean isTfFournisseurDisabled() {
		return isTfFournisseurDisabled;
	}

	public String getRechercheRapideStr() {
		EOFournisseur fournisseur = getUnFournisseurSelectionne();
		if (fournisseur != null) {
			rechercheRapideStr = fournisseur.libelle();
		}
		return rechercheRapideStr;
	}

	public void setRechercheRapideStr(String rechercheRapide) {
		this.rechercheRapideStr = rechercheRapide;
	}

	public Map<String, String> getFiltres() {
		return filtres;
	}

	public void setFiltres(Map<String, String> filtres) {
		this.filtres = filtres;
	}

	public Boolean isCocheFournisseurInterneDisabled() {
		return isCocheFournisseurInterneDisabled;
	}

	public void setIsCocheFournisseurInterneDisabled(Boolean isCocheFournisseurInterneDisabled) {
		this.isCocheFournisseurInterneDisabled = isCocheFournisseurInterneDisabled;
	}

	public Boolean isRechercheFournisseurInterneChecked() {
		return isRechercheFournisseurInterneChecked;
	}

	public void setIsRechercheFournisseurInterneChecked(Boolean isRechercheFournisseurInterne) {
		this.isRechercheFournisseurInterneChecked = isRechercheFournisseurInterne;
	}

	public Boolean isRecherchePersonneMoraleChecked() {
		return isRecherchePersonneMoraleChecked;
	}

	public void setIsRecherchePersonneMoraleChecked(Boolean isRecherchePersonneMoraleChecked) {
		this.isRecherchePersonneMoraleChecked = isRecherchePersonneMoraleChecked;
	}

	public Boolean isCochePersonneMoraleDisabled() {
		return isCochePersonneMoraleDisabled;
	}

	public void setIsCochePersonneMoraleDisabled(Boolean isCochePersonneMoraleDisabled) {
		this.isCochePersonneMoraleDisabled = isCochePersonneMoraleDisabled;
	}

	public Boolean isAfficherPopupFournisseurs() {
		return false;
		//return (fournisseurs != null && fournisseurs.count() > 1);
	}

	public NSArray<NSDictionary<String, Object>> getFournisseurs() {
		return fournisseurs;
	}

	public void setFournisseurs(NSArray<NSDictionary<String, Object>> fournisseurs) {
		this.fournisseurs = fournisseurs;
		getFournisseurListCtrl().setFournisseurs(fournisseurs);
	}

	public Boolean isPopupFournisseursDisabled() {
		return isPopupFournisseursDisabled;
	}

	public void setIsPopupFournisseursDisabled(Boolean isPopupFournisseursDisabled) {
		this.isPopupFournisseursDisabled = isPopupFournisseursDisabled;
	}

	public NSDictionary<String, Object> getSelectedFournisseur() {
		return selectedFournisseur;
	}

	public void setSelectedFournisseur(NSDictionary<String, Object> selectedFournisseur) {
		this.selectedFournisseur = selectedFournisseur;
		if (selectedFournisseur != null) {
			String fouCode = (String) selectedFournisseur.objectForKey("CODE");
			EOFournisseur leFournisseur = FinderFournisseur.getFournisseur(edc(), fouCode);
			setUnFournisseurSelectionne(leFournisseur);
			setFournisseurs(null);
		}
		else {

			//unFournisseurSelectionne = null;
			setUnFournisseurSelectionne(null);
		}
	}

	public Boolean isAfficherListeFournisseur() {
		return getFournisseurs() != null && getFournisseurs().count() > 1;
	}

	public NSDictionary<String, Object> getUnFournisseur() {
		return unFournisseur;
	}

	public void setUnFournisseur(NSDictionary<String, Object> unFournisseur) {
		this.unFournisseur = unFournisseur;
	}

	public EOFournisseur getUnFournisseurSelectionne() {
		if (unFournisseurSelectionne == null) {
			unFournisseurSelectionne = getView().getSelection();
		}
		return unFournisseurSelectionne;
	}

	public void setUnFournisseurSelectionne(EOFournisseur unFournisseurSelectionne) {
		if ((this.unFournisseurSelectionne != null && this.unFournisseurSelectionne.equals(unFournisseurSelectionne) == false) ||
				(this.unFournisseurSelectionne == null && unFournisseurSelectionne != null)) {
			this.unFournisseurSelectionne = unFournisseurSelectionne;
		}
		getView().setSelection(unFournisseurSelectionne);
		//	setSelectionDone(true);
	}

	public EOFournis getFournis() {
		if (getSelectedFournisseur() == null) {
			return null;
		}
		EOFournis fournis = EOFournis.fetchByKeyValue(getUnFournisseurSelectionne().editingContext(), EOFournis.FOU_ORDRE_KEY, getUnFournisseurSelectionne().fouOrdre());
		return fournis;
	}

	public void rechercherUnFournisseur() {
		//	setSelectionDone(false);
		Boolean pm = null, fi = null;
		if (isRecherchePersonneMoraleChecked()) {
			pm = Boolean.TRUE;
		}
		else {
			pm = Boolean.FALSE;
		}

		if (isRechercheFournisseurInterneChecked()) {
			fi = Boolean.TRUE;
		}
		else {
			fi = Boolean.FALSE;
		}
		setSelectedFournisseur(null);
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(getRechercheRapideStr(), "rechercheGlobale");
		bindings.setObjectForKey(pm, "personneMorale");
		if (fi != null) {
			bindings.setObjectForKey(fi, "fournisseurInterne");
		}
		EOEditingContext ed = edc();
		int nbreFournisseurs = FinderFournisseur.getRawRowCountFournisseurs(ed, bindings);
		if (nbreFournisseurs > 0) {
			setFournisseurs(FinderFournisseur.getRawRowFournisseurs(ed, bindings));
			if (getFournisseurs().count() == 1) {
				setSelectedFournisseur(getFournisseurs().lastObject());
			}
		}
		else {
			//FIXME adapter 
			//session.setAlertMessage("Aucun fournisseur ne correspond à cette recherche.");
		}

	}

	public boolean isAfficherLeFournisseur() {
		boolean isAfficherLeFournisseur = false;
		if (getUnFournisseurSelectionne() != null) {
			isAfficherLeFournisseur = true;
		}
		return isAfficherLeFournisseur;
	}

	public CktlFournisseurListeCtrl getFournisseurListCtrl() {
		return fournisseurListCtrl;
	}

	public void setFournisseurListCtrl(CktlFournisseurListeCtrl fournisseurListCtrl) {
		this.fournisseurListCtrl = fournisseurListCtrl;
	}

	//	public Boolean getSelectionDone() {
	//		return selectionDone;
	//	}
	//
	//	public void setSelectionDone(Boolean selectionDone) {
	//		this.selectionDone = selectionDone;
	//	}

	public CktlFournisseurSelect getView() {
		return (CktlFournisseurSelect) getMyComponent();
	}
}
