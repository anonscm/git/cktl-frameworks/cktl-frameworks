/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktldepense.server.finder.FinderAttribution;
import org.cocktail.fwkcktldepense.server.finder.FinderCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.finder.FinderProrata;
import org.cocktail.fwkcktldepense.server.finder.FinderTva;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.util.Calculs;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.MontantsEngagementTO;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.SourceCreditsEngagementTO;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.TypeAchatConfiguration;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.TypeAchatTO;
import org.cocktail.fwkcktldepenseguiajax.serveur.enums.ETypeMarche;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author flagouey $LastChangedBy: rprin $ $Date: 2012-07-09 16:19:05 +0200 (lun., 09 juil. 2012) $
 */
public class CktlEngagementDirectFormCtrl extends ACktlDepenseGuiAjaxComponentFreeCtrl implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Calculs serviceCalculs = Calculs.getInstance();

	private static final Set<ETypeMarche> TYPES_MARCHE = EnumSet.of(ETypeMarche.MARCHE, ETypeMarche.HORS_MARCHE);

	/* Controllers. */
	private CktlTypeAchatSelectionCtrl typeAchatSelectionCtrl;
	private CktlSourceCreditSelectCtrl sourceCreditSelectCtrl;

	/* Variables iteration. */
	private EOTauxProrata currentTauxProrata;

	/* Attributs. */
	private SourceCreditsEngagementTO sourceCreditTO;
	private String engLibelle;
	private MontantsEngagementTO montantsEngagementTO;

	private NSArray<EOTva> lesTauxTVA;

	private EOTva unTauxTVA;

	/**
	 * Constructeur par défaut.
	 */
	public CktlEngagementDirectFormCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
		super(session, edc, exercice);
		initTypeAchatSelectionCtrl();
		initSourceCreditsSelectionCtrl();
		reset();
	}

	private void initTypeAchatSelectionCtrl() {
		this.typeAchatSelectionCtrl = new CktlTypeAchatSelectionCtrl(session(), edc(), getExercice());

		//this.typeAchatSelectionCtrl.updateConfiguration(defaultTypeAchatConfiguration());
	}

	private void initSourceCreditsSelectionCtrl() {
		this.sourceCreditSelectCtrl = new CktlSourceCreditSelectCtrl(session(), edc(), getExercice());
	}

	public void updateTypeAchat(EOEditingContext editingContext, EOExercice exercice, EOFournisseur fournisseur) {
		List<EOAttribution> attributions = listerAttributionsValides(editingContext, fournisseur);
		List<EOCtrlSeuilMAPA> codesNomenclatures = listerSeuilsMAPA(editingContext, exercice);

		TypeAchatConfiguration configuration = buildTypeAchatConfiguration(attributions, codesNomenclatures);
		typeAchatSelectionCtrl.updateConfiguration(configuration);
	}

	public void ajouterCreditsCallback() {
		EOBudgetExecCredit creditSelectionne = getSourceCreditSelectCtrl().getSourceCreditsSelectionnee();
		this.sourceCreditTO = convertBudget(creditSelectionne);
	}

	public void annulerCreditsCallback() {
	}

	public void supprimerSourceCredit() {
		this.sourceCreditTO = new SourceCreditsEngagementTO();
	}

	private TypeAchatConfiguration defaultTypeAchatConfiguration() {
		TypeAchatConfiguration defaultTypeAchatConfig = new TypeAchatConfiguration();
		for (ETypeMarche currentTypeMarche : TYPES_MARCHE) {
			defaultTypeAchatConfig.add(currentTypeMarche, false);
		}
		return defaultTypeAchatConfig;
	}

	private List<EOAttribution> listerAttributionsValides(EOEditingContext editingContext, EOFournisseur fournisseur) {
		if (fournisseur == null) {
			return Collections.emptyList();
		}

		Map<String, Object> criteriaMap = new HashMap<String, Object>();
		criteriaMap.put(FinderAttribution.KEY_DATE, new Date());
		criteriaMap.put(FinderAttribution.KEY_FOURNISSEUR, fournisseur);
		return FinderAttribution.getAttributionsValides(editingContext, criteriaMap);
	}

	private List<EOCtrlSeuilMAPA> listerSeuilsMAPA(EOEditingContext editingContext, EOExercice exercice) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(exercice, FinderCtrlSeuilMAPA.BINDING_EXERCICE);
		bindings.setObjectForKey(EOTypeAchat.fetchByKeyValue(editingContext, EOTypeAchat.TYPA_LIBELLE_KEY, EOTypeAchat.MARCHE_NON_FORMALISE), FinderCtrlSeuilMAPA.BINDING_TYPE_ACHAT);
		return FinderCtrlSeuilMAPA.getCtrlSeuilMAPAValides(editingContext, bindings);
	}

	//FIXME Passer ca en statique dans TypeAchatConfiguration ???
	private TypeAchatConfiguration buildTypeAchatConfiguration(List<EOAttribution> attributions,
			List<EOCtrlSeuilMAPA> codesNomenclatures) {
		//TODO gerer ca sous forme de builder.

		// tous les types d'achat sont actifs par defaut.
		TypeAchatConfiguration typeAchatConfiguration = defaultTypeAchatConfiguration();

		// gestion attributions
		if (attributions.isEmpty()) {
			typeAchatConfiguration.changerEtat(EnumSet.of(ETypeMarche.MARCHE), false);
		}
		else {
			typeAchatConfiguration.setAttributions(attributions);
		}

		// on met a jour les codes nomenclatures
		typeAchatConfiguration.setCodesNomenclatures(codesNomenclatures);

		return typeAchatConfiguration;
	}

	private SourceCreditsEngagementTO convertBudget(EOBudgetExecCredit credit) {
		SourceCreditsEngagementTO sourceTO = new SourceCreditsEngagementTO();
		sourceTO.setBudgetExecCredit(credit);
		List<EOTauxProrata> creditTauxProrataList = credit.getTauxProratas(edc());
		if (creditTauxProrataList != null && creditTauxProrataList.size() > 0) {
			EOTauxProrata tauxProrataParDefaut = creditTauxProrataList.get(0);
			sourceTO.setTauxProrataSelectionne(tauxProrataParDefaut);
		}

		sourceTO.setTauxProrataDisponibles(loadTauxProrata(credit));

		return sourceTO;
	}

	public NSArray<EOTauxProrata> loadTauxProrata(EOBudgetExecCredit credit) {
		EOOrgan creditOrgan = credit.organ();
		if (creditOrgan == null) {
			return NSArray.emptyArray();
		}

		// TODO idealement utilisee un Map ici
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(getExercice(), FinderProrata.BINDING_EXERCICE);
		bindings.setObjectForKey(creditOrgan, FinderProrata.BINDING_ORGAN);

		return FinderProrata.getTauxProratasValides(edc(), bindings);
	}

	public CktlTypeAchatSelectionCtrl getTypeAchatSelectionCtrl() {
		return typeAchatSelectionCtrl;
	}

	public TypeAchatTO getTypeAchatTO() {
		return this.getTypeAchatSelectionCtrl().getTypeAchatTO();
	}

	public SourceCreditsEngagementTO getSourceCreditTO() {
		return sourceCreditTO;
	}

	public EOTauxProrata getCurrentTauxProrata() {
		return currentTauxProrata;
	}

	public void setCurrentTauxProrata(EOTauxProrata currentTauxProrata) {
		this.currentTauxProrata = currentTauxProrata;
	}

	public void reset() {
		sourceCreditTO = new SourceCreditsEngagementTO();
		montantsEngagementTO = new MontantsEngagementTO();
		setEngLibelle(null);
		typeAchatSelectionCtrl.reset();
		typeAchatSelectionCtrl.updateConfiguration(defaultTypeAchatConfiguration());

	}

	public Boolean isliquidable() {
		return getSourceCreditTO().isComplete() && getTypeAchatTO().isComplete() && getMontantsEngagementTO().isComplete() && !StringCtrl.isEmpty(getEngLibelle());
	}

	public String getEngLibelle() {
		return engLibelle;
	}

	public void setEngLibelle(String engLibelle) {
		this.engLibelle = engLibelle;
	}

	public MontantsEngagementTO getMontantsEngagementTO() {
		return montantsEngagementTO;
	}

	public void setMontantsEngagementTO(MontantsEngagementTO montantsEngagementTO) {
		this.montantsEngagementTO = montantsEngagementTO;
	}

	public NSArray<EOTva> lesTauxTVA() {
		if (lesTauxTVA == null) {
			lesTauxTVA = FinderTva.getTvas(edc());
			if (lesTauxTVA.count() > 0) {
				getMontantsEngagementTO().setTva(lesTauxTVA.objectAtIndex(0));
			}
		}
		return lesTauxTVA;
	}

	public EOTva getUnTauxTVA() {
		return unTauxTVA;
	}

	public void setUnTauxTVA(EOTva unTauxTVA) {
		this.unTauxTVA = unTauxTVA;
	}

	public void verificationMAPA() throws Exception {
		if (getTypeAchatTO().getAttributionSelectionnee() == null) {
			FinderCtrlSeuilMAPA.checkDepassementSeuil(edc(), getExercice(), getTypeAchatSelectionCtrl().getTypeAchatTO().getCodeNomenclatureSelectionne(), getMontantsEngagementTO().getMontantHT());
		}
	}

	public CktlSourceCreditSelectCtrl getSourceCreditSelectCtrl() {
		return sourceCreditSelectCtrl;
	}

	public void setSourceCreditSelectCtrl(CktlSourceCreditSelectCtrl sourceCreditSelectCtrl) {
		this.sourceCreditSelectCtrl = sourceCreditSelectCtrl;
	}

	public void onModifierHtOuTva() {
		BigDecimal ht = getMontantsEngagementTO().getMontantHT();
		EOTva tva = getMontantsEngagementTO().getTva();
		BigDecimal ttc = EOTva.calcMontantTtc(edc(), getExercice(), ht, tva);
		getMontantsEngagementTO().setMontantTTC(ttc);

		BigDecimal engMontantBudgetaire = 
				serviceCalculs.calculMontantBudgetaire(ht, ttc.add(ht.negate()), 
						getSourceCreditTO().getTauxProrataSelectionne().tapTaux());
		getMontantsEngagementTO().setMontantBudgetaire(engMontantBudgetaire);

	}

}
