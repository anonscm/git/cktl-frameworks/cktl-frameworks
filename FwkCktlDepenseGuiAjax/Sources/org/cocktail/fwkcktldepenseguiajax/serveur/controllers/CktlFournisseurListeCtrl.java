/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class CktlFournisseurListeCtrl implements ICktlFournisseurListeCtrl {
	private EOEditingContext edc;
	private NSArray<NSDictionary<String, Object>> fournisseurs;
	private NSDictionary<String, Object> unFournisseur;
	private NSDictionary<String, Object> selectedFournisseur;

	public CktlFournisseurListeCtrl(EOEditingContext edc) {
		this.edc = edc;
	}

	public void selectionnerFournisseur() {
		setSelectedFournisseur(getUnFournisseur());
	}

	public String detailFournisseurAdresse() {
		String detailFournisseurAdresse = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("ADRESSE1") != null && fournisseur.objectForKey("ADRESSE1").equals(NSDictionary.NullValue) == false) {
			detailFournisseurAdresse += fournisseur.objectForKey("ADRESSE1") + " ";
		}
		if (fournisseur.objectForKey("ADRESSE2") != null && fournisseur.objectForKey("ADRESSE2").equals(NSDictionary.NullValue) == false) {
			detailFournisseurAdresse += fournisseur.objectForKey("ADRESSE2") + " ";
		}

		return detailFournisseurAdresse;
	}

	public String detailFournisseurCodePostal() {
		String detailFournisseurCodePostal = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("CODEPOSTAL") != null && fournisseur.objectForKey("CODEPOSTAL").equals(NSDictionary.NullValue) == false) {
			detailFournisseurCodePostal += fournisseur.objectForKey("CODEPOSTAL");
		}

		return detailFournisseurCodePostal;
	}

	public String detailFournisseurVille() {
		String detailFournisseurVille = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("VILLE") != null && fournisseur.objectForKey("VILLE").equals(NSDictionary.NullValue) == false) {
			detailFournisseurVille += fournisseur.objectForKey("VILLE");
		}

		return detailFournisseurVille;
	}

	public String detailFournisseurBP() {
		String detailFournisseurBP = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("BP") != null && fournisseur.objectForKey("BP").equals(NSDictionary.NullValue) == false) {
			detailFournisseurBP += fournisseur.objectForKey("BP");
		}

		return detailFournisseurBP;
	}

	public String detailFournisseurPays() {
		String detailFournisseurPays = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("PAYS") != null && fournisseur.objectForKey("PAYS").equals(NSDictionary.NullValue) == false) {
			detailFournisseurPays += fournisseur.objectForKey("PAYS");
		}

		return detailFournisseurPays;
	}

	public String detailFournisseurTels() {
		String detailFournisseurTels = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("TELEPHONES") != null && fournisseur.objectForKey("TELEPHONES").equals(NSDictionary.NullValue) == false) {
			detailFournisseurTels += fournisseur.objectForKey("TELEPHONES");
		}

		return detailFournisseurTels;
	}

	public String detailFournisseurFaxs() {
		String detailFournisseurFaxs = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("FAXS") != null && fournisseur.objectForKey("FAXS").equals(NSDictionary.NullValue) == false) {
			detailFournisseurFaxs += fournisseur.objectForKey("FAXS");
		}

		return detailFournisseurFaxs;
	}

	public String detailFournisseurRibs() {
		String detailFournisseurRibs = "";
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur.objectForKey("RIBS") != null && fournisseur.objectForKey("RIBS").equals(NSDictionary.NullValue) == false) {
			detailFournisseurRibs += fournisseur.objectForKey("RIBS");
		}

		return detailFournisseurRibs;
	}

	public NSArray<NSDictionary<String, Object>> getFournisseurs() {
		return fournisseurs;
	}

	public void setFournisseurs(NSArray<NSDictionary<String, Object>> fournisseurs) {
		this.fournisseurs = fournisseurs;
	}

	public NSDictionary<String, Object> getUnFournisseur() {
		return unFournisseur;
	}

	public void setUnFournisseur(NSDictionary<String, Object> unFournisseur) {
		this.unFournisseur = unFournisseur;
	}

	public NSDictionary<String, Object> getSelectedFournisseur() {
		return selectedFournisseur;
	}

	public void setSelectedFournisseur(NSDictionary<String, Object> selectedFournisseur) {
		this.selectedFournisseur = selectedFournisseur;
	}

	public String siret() {
		String siret = null;
		NSDictionary<String, Object> fournisseur = getUnFournisseur();
		if (fournisseur != null) {
			if (fournisseur.objectForKey("SIRET").equals(NSDictionary.NullValue) == false) {
				siret = (String) fournisseur.objectForKey("SIRET");
			}
		}
		return siret;
	}

	public EOFournis getUnFournis() {
		EOFournis fournis = EOFournis.fetchByKeyValue(edc, EOFournis.FOU_CODE_KEY, getUnFournisseur().valueForKey("CODE"));
		return fournis;
	}

}
