package org.cocktail.fwkcktldepenseguiajax.serveur.controllers;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreBean;
import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltresManager;
import org.cocktail.fwkcktldepense.server.finder.FinderExtourneCredits;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier._IDepenseExtourneCredits;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.filtre.EnveloppeExtourneFiltreConverter;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNumberFormatter;

import er.extensions.eof.ERXEOControlUtilities;

public class CktlEnveloppeExtourneSelectionCtrl extends ACktlDepenseGuiAjaxComponentFreeCtrl {

	public enum FiltresEnveloppeExtourne {
		UB, CR, SEULEMENT_DISPONIBLE
	}

	public static final String FILTRE_UB_LABEL = "UB : ";
	public static final String FILTRE_CR_LABEL = "CR : ";
	public static final String FILTRE_SEULEMENT_DISPONIBLE_LABEL = "Seulement Disponible : ";

	private static final CktlDepenseFiltreBean FILTRE_UB =
			new CktlDepenseFiltreBean(FiltresEnveloppeExtourne.UB.toString(), FILTRE_UB_LABEL);
	private static final CktlDepenseFiltreBean FILTRE_CR =
			new CktlDepenseFiltreBean(FiltresEnveloppeExtourne.CR.toString(), FILTRE_CR_LABEL);
	private static final CktlDepenseFiltreBean FILTRE_SEULEMENT_DISPONIBLE =
			new CktlDepenseFiltreBean(
					FiltresEnveloppeExtourne.SEULEMENT_DISPONIBLE.toString(),
					FILTRE_SEULEMENT_DISPONIBLE_LABEL,
					Boolean.TRUE.toString());

	private boolean showNiveauOrganSelection;
	private Integer niveauOrganSelected;
	private List<_IDepenseExtourneCredits> listeEnveloppes;
	private _IDepenseExtourneCredits enveloppeVarIteration;
	private _IDepenseExtourneCredits enveloppeSelectionnee;

	//TODO moche mais ca simplifie temporairement le developpement (sinon il faut une vue par type de filtres)
	private Map<FiltresEnveloppeExtourne, CktlDepenseFiltreBean> filtresManagerAsMap;
	private CktlDepenseFiltresManager filtresManager;
	private EnveloppeExtourneFiltreConverter filtresConverter;

	public CktlEnveloppeExtourneSelectionCtrl(
			ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
		super(session, edc, exercice);

		this.showNiveauOrganSelection = true;
		this.niveauOrganSelected = EOOrgan.ORG_NIV_2; // UB
		this.filtresManagerAsMap = new HashMap<FiltresEnveloppeExtourne, CktlDepenseFiltreBean>();
		this.filtresManager = new CktlDepenseFiltresManager();
		this.filtresConverter = new EnveloppeExtourneFiltreConverter();

		initFiltres();
		//initListeEnveloppes();
	}

	public void selectNiveauOrgan() {
		resetFiltres();
		chargerListeEnveloppes(buildFiltres());
	}

	public void filtrer() {
		chargerListeEnveloppes(buildFiltres());
	}

	public Collection<CktlDepenseFiltreBean> listefiltres() {
		return filtresManagerAsMap.values();
	}

	public EOQualifier enrichirQualifiers(EOQualifier qualifier) {
		return qualifier;
	}

	//	private void initListeEnveloppes() {
	//		chargerListeEnveloppes(buildFiltres());
	//	}

	private void chargerListeEnveloppes(EOQualifier qualifiers) {
		EOQualifier qualifierEnrichi = enrichirQualifiers(qualifiers);
		this.listeEnveloppes = getListeEnveloppes(edc(), getExercice(), niveauOrganSelected, qualifierEnrichi);
	}

	private void initFiltres() {
		// init une map statique
		filtresManagerAsMap.put(FiltresEnveloppeExtourne.UB, FILTRE_UB);
		filtresManagerAsMap.put(FiltresEnveloppeExtourne.CR, FILTRE_CR);
		filtresManagerAsMap.put(FiltresEnveloppeExtourne.SEULEMENT_DISPONIBLE, FILTRE_SEULEMENT_DISPONIBLE);
	}

	private void resetFiltres() {
		for (CktlDepenseFiltreBean currentFiltre : filtresManagerAsMap.values()) {
			currentFiltre.clear();
		}
	}

	private EOQualifier buildFiltres() {
		filtresManager.clear();
		filtresManager.addAll(listefiltres());
		return new EOAndQualifier(new NSMutableArray<EOQualifier>(this.filtresManager.convert(filtresConverter)));
	}

	private List<_IDepenseExtourneCredits> getListeEnveloppes(
			EOEditingContext editingContext, EOExercice exercice, Integer niveauOrgan, EOQualifier qualifiers) {
		List<_IDepenseExtourneCredits> listeEnveloppes = Collections.emptyList();
		if (niveauOrgan == null || exercice == null) {
			return listeEnveloppes;
		}

		if (EOOrgan.ORG_NIV_2.equals(niveauOrgan)) {
			listeEnveloppes = FinderExtourneCredits.instance().findExtourneCreditsDisponiblesPourNiveauUb(
					editingContext, session().utilisateurInContext(editingContext), exercice, qualifiers);
		}
		else if (EOOrgan.ORG_NIV_3.equals(niveauOrgan)) {
			listeEnveloppes = FinderExtourneCredits.instance().findExtourneCreditsDisponiblesPourNiveauCr(
					editingContext, session().utilisateurInContext(editingContext), exercice, qualifiers);
		}

		//on force un refresh en cas de modif par un autre utilisateur.
		for (_IDepenseExtourneCredits obj : listeEnveloppes) {
			ERXEOControlUtilities.refaultObject((EOEnterpriseObject) obj);
		}
		return listeEnveloppes;
	}

	public NSNumberFormatter monnaieFormatter() {
		return session().getMonnaieFormatter();
	}

	// Getters / Setters.
	public Integer getNiveauOrganSelected() {
		return niveauOrganSelected;
	}

	public void setNiveauOrganSelected(Integer niveauOrganSelected) {
		this.niveauOrganSelected = niveauOrganSelected;
	}

	public List<_IDepenseExtourneCredits> getListeEnveloppes() {
		return listeEnveloppes;
	}

	public void setListeEnveloppes(List<_IDepenseExtourneCredits> listeEnveloppes) {
		this.listeEnveloppes = listeEnveloppes;
	}

	public _IDepenseExtourneCredits getEnveloppeSelectionnee() {
		return enveloppeSelectionnee;
	}

	public void setEnveloppeSelectionnee(_IDepenseExtourneCredits enveloppeSelectionnee) {
		this.enveloppeSelectionnee = enveloppeSelectionnee;
	}

	public _IDepenseExtourneCredits getEnveloppeVarIteration() {
		return enveloppeVarIteration;
	}

	public void setEnveloppeVarIteration(_IDepenseExtourneCredits enveloppeVarIteration) {
		this.enveloppeVarIteration = enveloppeVarIteration;
	}

	public EnveloppeExtourneFiltreConverter getFiltresConverter() {
		return filtresConverter;
	}

	public Map<FiltresEnveloppeExtourne, CktlDepenseFiltreBean> getFiltresManagerAsMap() {
		return filtresManagerAsMap;
	}

	public boolean isShowNiveauOrganSelection() {
		return showNiveauOrganSelection;
	}

	public void setShowNiveauOrganSelection(boolean showNiveauOrganSelection) {
		this.showNiveauOrganSelection = showNiveauOrganSelection;
	}
}
