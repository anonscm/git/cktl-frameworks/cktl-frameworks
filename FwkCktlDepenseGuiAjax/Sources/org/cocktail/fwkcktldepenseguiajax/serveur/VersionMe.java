package org.cocktail.fwkcktldepenseguiajax.serveur;

/**
 * Cette classe ne doit pas heriter d'une classe autre que Object pour être lancee sans classpath. Logiquement vous ne devez modifier que les
 * constantes statiques des nº de version, la date et les commentaires
 */
public final class VersionMe extends Object {
	// Nom de l'appli
	static final String APPLICATIONFINALNAME = "FwkCktlDepenseGuiAjax";
	public static final String APPLICATIONINTERNALNAME = "FwkCktlDepenseGuiAjax";
	public static final String APPLICATION_STRID = "FWKCKTLDEPENSEGUIAJAX";

	// Version appli
	public static final long SERIALVERSIONUID = 2106;

	public static final int VERSIONNUMMAJ = 2;
	public static final int VERSIONNUMMIN = 1;
	public static final int VERSIONNUMPATCH = 0;
	public static final int VERSIONNUMBUILD = 6;
	public static final int VERSIONNUMRC = 0;

	public static final String VERSIONDATE = "08/01/2013";
	public static final String COMMENT = null;

	/***
	 * Ne pas modifier cette methode, elle est utilisee pour recuperer le nº de version formate a partir d'une tache ant.
	 */
	public static void main(String[] args) {
		System.out.println("" + VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD);
	}

	@SuppressWarnings("unused")
	public static String appliVersion() {
		String appliVersion = VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD;
		if (VERSIONNUMRC > 0) {
			appliVersion += "_rc" + VERSIONNUMRC;
		}

		return appliVersion;
	}

	public static String htmlAppliVersion() {
		String htmlAppliVersion = "<b>Version " + appliVersion();
		htmlAppliVersion += " du " + VERSIONDATE + "</b>";
		return htmlAppliVersion;
	}

	public static String txtAppliVersion() {
		return "Version " + appliVersion() + " du " + VERSIONDATE;
	}

}