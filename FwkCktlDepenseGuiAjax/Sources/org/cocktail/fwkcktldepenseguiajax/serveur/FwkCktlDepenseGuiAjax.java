package org.cocktail.fwkcktldepenseguiajax.serveur;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjax;

import er.extensions.ERXFrameworkPrincipal;

public class FwkCktlDepenseGuiAjax extends ERXFrameworkPrincipal {
	public static final Logger LOG = Logger.getLogger(FwkCktlPersonneGuiAjax.class);
	public static final String FRAMEWORK_NAME = "FwkCktlDepenseGuiAjax.framework";

	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlDepenseGuiAjax.class);
	}

	@Override
	public void didFinishInitialization() {
		super.didFinishInitialization();

	}

	@Override
	public void finishInitialization() {

	}
}