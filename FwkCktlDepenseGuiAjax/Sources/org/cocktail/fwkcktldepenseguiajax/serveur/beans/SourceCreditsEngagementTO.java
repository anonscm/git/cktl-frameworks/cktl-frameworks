package org.cocktail.fwkcktldepenseguiajax.serveur.beans;

import java.io.Serializable;

import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;

import com.webobjects.foundation.NSArray;

/**
 * @author flagouey
 */
public class SourceCreditsEngagementTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private EOBudgetExecCredit budgetExecCredit;
	private EOTauxProrata tauxProrataSelectionne;
	private NSArray<EOTauxProrata> tauxProrataDisponibles;

	public SourceCreditsEngagementTO() {
	}

	public EOBudgetExecCredit getBudgetExecCredit() {
		return budgetExecCredit;
	}

	public void setBudgetExecCredit(EOBudgetExecCredit budgetExecCredit) {
		this.budgetExecCredit = budgetExecCredit;
	}

	public EOTauxProrata getTauxProrataSelectionne() {
		return tauxProrataSelectionne;
	}

	public void setTauxProrataSelectionne(EOTauxProrata tauxProrataSelectionne) {
		this.tauxProrataSelectionne = tauxProrataSelectionne;
	}

	public NSArray<EOTauxProrata> getTauxProrataDisponibles() {
		return tauxProrataDisponibles;
	}

	public void setTauxProrataDisponibles(NSArray<EOTauxProrata> tauxProrataDisponibles) {
		this.tauxProrataDisponibles = tauxProrataDisponibles;
	}

	public Boolean isComplete() {
		return getBudgetExecCredit() != null && getTauxProrataSelectionne() != null;
	}

}
