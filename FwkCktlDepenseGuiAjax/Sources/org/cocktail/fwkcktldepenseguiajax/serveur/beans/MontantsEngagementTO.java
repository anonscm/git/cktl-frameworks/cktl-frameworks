package org.cocktail.fwkcktldepenseguiajax.serveur.beans;

import java.io.Serializable;
import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier.EOTva;

public class MontantsEngagementTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private EOTva tva;
	private BigDecimal montantHT;
	private BigDecimal montantTTC;
	private BigDecimal montantBudgetaire;

	public MontantsEngagementTO() {
		montantHT = BigDecimal.ZERO;
		montantTTC = BigDecimal.ZERO;
		montantBudgetaire = BigDecimal.ZERO;
	}

	public EOTva getTva() {
		return tva;
	}

	public void setTva(EOTva tva) {
		this.tva = tva;
	}

	public BigDecimal getMontantHT() {
		return montantHT;
	}

	public void setMontantHT(BigDecimal montantHT) {
		this.montantHT = montantHT;
	}

	public BigDecimal getMontantTTC() {
		return montantTTC;
	}

	public void setMontantTTC(BigDecimal montantTTC) {
		this.montantTTC = montantTTC;
	}

	public BigDecimal getMontantBudgetaire() {
		return montantBudgetaire;
	}

	public void setMontantBudgetaire(BigDecimal montantBudgetaire) {
		this.montantBudgetaire = montantBudgetaire;
	}

	public Boolean isComplete() {
		return getMontantTTC() != null && getMontantTTC().compareTo(BigDecimal.ZERO) != 0;
	}

}
