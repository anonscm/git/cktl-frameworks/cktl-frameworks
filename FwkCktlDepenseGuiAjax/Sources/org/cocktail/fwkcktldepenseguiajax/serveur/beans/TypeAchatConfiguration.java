/*
 * $License$
 */
package org.cocktail.fwkcktldepenseguiajax.serveur.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepenseguiajax.serveur.enums.ETypeMarche;

/**
 *
 * @author flagouey
 * $LastChangedBy: rprin $
 * $Date: 2012-07-11 16:29:28 +0200 (mer., 11 juil. 2012) $
 *
 */
public class TypeAchatConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	/* Liste types marche disponibles. */
	private Set<TypeAchatConfigurationBean> typesMarchesDisponibles;

	/* Liste type attribution. */
	private List<EOAttribution> attributions;

	/* Liste codes nomenclature. */
	private List<EOCtrlSeuilMAPA> codesNomenclatures;

	public TypeAchatConfiguration() {
		this.typesMarchesDisponibles = new HashSet<TypeAchatConfiguration.TypeAchatConfigurationBean>();
		this.attributions = new ArrayList<EOAttribution>();
		this.codesNomenclatures = new ArrayList<EOCtrlSeuilMAPA>();
	}

	public void add(ETypeMarche typeMarche, boolean disabled) {
		this.typesMarchesDisponibles.add(new TypeAchatConfigurationBean(typeMarche));
	}

	public boolean isDisabled(ETypeMarche typeMarche) {
		boolean disabled = false;
		for (TypeAchatConfigurationBean currentBeanConfig : typesMarchesDisponibles) {
			if (currentBeanConfig.typeMarche.equals(typeMarche)) {
				disabled = currentBeanConfig.disabled;
				break;
			}
		}
		return disabled;
	}

	public void changerEtat(Set<ETypeMarche> typesMarche, boolean enable) {
		if (typesMarche == null || typesMarche.isEmpty()) {
			return;
		}

		for (TypeAchatConfigurationBean currentBeanConfig : typesMarchesDisponibles) {
			Iterator<ETypeMarche> iteTypesMarche = typesMarche.iterator();
			while (iteTypesMarche.hasNext()) {
				ETypeMarche currentTypeMarche = iteTypesMarche.next();
				if (currentBeanConfig.typeMarche.equals(currentTypeMarche)) {
					currentBeanConfig.setDisabled(!enable);
					iteTypesMarche.remove();
					break;
				}
			}
		}
	}

	public boolean isAttributionUnique() {
		return attributions != null && attributions.size() == 1;
	}

	public Set<TypeAchatConfigurationBean> getTypesMarchesDisponibles() {
		return typesMarchesDisponibles;
	}

	public List<EOAttribution> getAttributions() {
		return attributions;
	}

	public void setAttributions(List<EOAttribution> attributions) {
		this.attributions = attributions;
	}

	public List<EOCtrlSeuilMAPA> getCodesNomenclatures() {
		return codesNomenclatures;
	}

	public void setCodesNomenclatures(List<EOCtrlSeuilMAPA> codesNomenclatures) {
		this.codesNomenclatures = codesNomenclatures;
	}

	private class TypeAchatConfigurationBean {
		private ETypeMarche typeMarche;
		private boolean disabled;

		public TypeAchatConfigurationBean(ETypeMarche typeMarche) {
			this(typeMarche, false);
		}

		public TypeAchatConfigurationBean(ETypeMarche typeMarche, boolean disabled) {
			this.typeMarche = typeMarche;
			this.disabled = disabled;
		}

		public ETypeMarche getTypeMarche() {
			return typeMarche;
		}

		public void setTypeMarche(ETypeMarche typeMarche) {
			this.typeMarche = typeMarche;
		}

		public boolean isDisabled() {
			return disabled;
		}

		public void setDisabled(boolean disabled) {
			this.disabled = disabled;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + (disabled ? 1231 : 1237);
			result = prime * result	+ ((typeMarche == null) ? 0 : typeMarche.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TypeAchatConfigurationBean other = (TypeAchatConfigurationBean) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (disabled != other.disabled)
				return false;
			if (typeMarche != other.typeMarche)
				return false;
			return true;
		}

		private TypeAchatConfiguration getOuterType() {
			return TypeAchatConfiguration.this;
		}
	}
}
