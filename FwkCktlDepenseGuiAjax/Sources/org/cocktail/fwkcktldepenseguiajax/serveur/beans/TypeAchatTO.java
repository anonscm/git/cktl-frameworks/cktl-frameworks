package org.cocktail.fwkcktldepenseguiajax.serveur.beans;

import java.io.Serializable;

import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepenseguiajax.serveur.enums.ETypeMarche;

public class TypeAchatTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private ETypeMarche typeMarcheSelectionne;
	private EOAttribution attributionSelectionnee;
	private EOCtrlSeuilMAPA codeNomenclatureSelectionne;
	private EOTypeAchat typeAchat;

	public TypeAchatTO() {
	}

	public ETypeMarche getTypeMarcheSelectionne() {
		return typeMarcheSelectionne;
	}

	public void setTypeMarcheSelectionne(ETypeMarche typeMarcheSelectionne) {
		this.typeMarcheSelectionne = typeMarcheSelectionne;
	}

	public EOAttribution getAttributionSelectionnee() {
		return attributionSelectionnee;
	}

	public void setAttributionSelectionnee(EOAttribution attributionSelectionnee) {
		this.attributionSelectionnee = attributionSelectionnee;
	}

	public EOCtrlSeuilMAPA getCodeNomenclatureSelectionne() {
		return codeNomenclatureSelectionne;
	}

	public void setCodeNomenclatureSelectionne(
			EOCtrlSeuilMAPA codeNomenclatureSelectionne) {
		this.codeNomenclatureSelectionne = codeNomenclatureSelectionne;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TypeAchatTO other = (TypeAchatTO) obj;
		if (attributionSelectionnee == null) {
			if (other.attributionSelectionnee != null) {
				return false;
			}
		}
		else if (!attributionSelectionnee.equals(other.attributionSelectionnee)) {
			return false;
		}
		if (codeNomenclatureSelectionne == null) {
			if (other.codeNomenclatureSelectionne != null) {
				return false;
			}
		}
		else if (!codeNomenclatureSelectionne.equals(other.codeNomenclatureSelectionne)) {
			return false;
		}
		if (typeMarcheSelectionne != other.typeMarcheSelectionne) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((attributionSelectionnee == null) ? 0 : attributionSelectionnee.hashCode());
		result = prime
				* result
				+ ((codeNomenclatureSelectionne == null) ? 0 : codeNomenclatureSelectionne.hashCode());
		result = prime
				* result
				+ ((typeMarcheSelectionne == null) ? 0 : typeMarcheSelectionne.hashCode());
		return result;
	}

	public Boolean isComplete() {
		return getAttributionSelectionnee() != null || getCodeNomenclatureSelectionne() != null;
	}

	public EOTypeAchat getTypeAchat() {
		return typeAchat;
	}

	public void setTypeAchat(EOTypeAchat typeAchat) {
		this.typeAchat = typeAchat;
	}
}
