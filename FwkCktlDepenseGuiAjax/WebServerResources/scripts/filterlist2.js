/*==================================================*
 $Id: filterlist.js,v 1.1 2004/02/06 13:05:30 rprin Exp $
 Copyright 2003 Patrick Fitzgerald
 //Modification Rodolphe Prin pour conserver la s?lection lorsqu'on filtre (la s?lection est perdue si le filtre cache toutes les valeurs)
  //Modification Rodolphe Prin pour tenir compte des select avec optGroup
  
 http://www.barelyfitz.com/webdesign/articles/filterlist/

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *==================================================*/



function filterlist2(selectobj) {

    //==================================================
    // PARAMETERS
    //==================================================

    // HTML SELECT object
    // For example, set this to document.myform.myselect
    this.selectobj = selectobj;

    // Flags for regexp matching.
    // "i" = ignore case; "" = do not ignore case
    // You can use the set_ignore_case() method to set this
    this.flags = 'i';

    // Which parts of the select list do you want to match?
    this.match_text = true;
    this.match_value = false;

    // You can set the hook variable to a function that
    // is called whenever the select list is filtered.
    // For example:
    // myfilterlist.hook = function() { }

    // Flag for debug alerts
    // Set to true if you are having problems.
    this.show_debug = false;

    //==================================================
    // METHODS
    //==================================================

    //--------------------------------------------------
    this.init = function() {
      // This method initilizes the object.
      // This method is called automatically when you create the object.
      // You should call this again if you alter the selectobj parameter.

      if (!this.selectobj) return this.debug('selectobj not defined');
      if (!this.selectobj.options) return this.debug('selectobj.options not defined');

      //memoriser les options directement a la racine du select
      this.options0 = this.selectobj.getElementsByTagName('option');
      this.options0copy = new Array();
      var o = -1;
      for (z=0; z < this.options0.length ; z++) {
  	if (this.options0[z]. parentNode== this.selectobj) {
  	    o++;
  	    this.options0copy[o] = document.createElement("option");
  	    this.options0copy[o].value = this.options0[z].value;
  	    this.options0copy[o].appendChild(document.createTextNode(this.options0[z].text));
  	}
      }
      
      //memoriser les optgroup et leurs options
      this.groupes = this.selectobj.getElementsByTagName('optgroup');
      this.optionscopy = new Array();
      if (this.selectobj && this.groupes) {
  	 for (z=0; z < this.groupes.length ; z++) {
  		this.groupescopy= new Array();
  		for (var z=0; z < this.groupes.length; z++) {
  			this.groupescopy[z] = document.createElement("optgroup" );
  			this.groupescopy[z].label = this.groupes[z].label;
  			options = this.groupes[z].getElementsByTagName('option');
  			this.optionscopy[z] = new Array();
  			for (var i=0; i < options.length; i++) {
  			        // Create a new Option
  			        this.optionscopy[z][i] = new Option();

  			        // Set the text for the Option
  			        this.optionscopy[z][i].text = options[i].text;
  			        //alert( this.optionscopy[i].text);

  			        // Set the value for the Option.
  			        // If the value wasn't set in the original select list,
  			        // then use the text.
  			        if (options[i].value) {
  			          this.optionscopy[z][i].value = options[i].value;
  			        } else {
  			          this.optionscopy[z][i].value = options[i].text;
  			        }
  			        
  			        
  			        

  			      }
  			    }
  			
  		}
  	 }
      }
  //  

    //--------------------------------------------------
    this.reselect = function(selectedOption) {
  	 for (var i=0; i < this.selectobj.options.length; i++) {
  	 	if ( this.selectobj.options[i].value == selectedOption) {
  	 		this.selectobj.selectedIndex = i;
  	 		break;
  	 	}
  	 }
    }


    //--------------------------------------------------
    this.reset = function() {
      // This method resets the select list to the original state.
      // It also unselects all of the options.

      this.set('');
    }


    //--------------------------------------------------
    this.set = function(pattern, attributeToTest) {
      // This method removes all of the options from the select list,
      // then adds only the options that match the pattern regexp.
      // It also unselects all of the options.

      var loop=0, index=0, regexp, e, optionSelected;

      if (!this.selectobj) return this.debug('selectobj not defined');
      if (!this.selectobj.options) return this.debug('selectobj.options not defined');

  	if (this.selectobj.selectedIndex>0){
  		optionSelected = this.selectobj.options[this.selectobj.selectedIndex].value;
  	}
  	
  	
  	
  	
  	//supprimer les optgroups
  	
  	var NodeListe = this.selectobj.childNodes;
  	//ou Node.childNodes
  	 while(this.selectobj.hasChildNodes()==true){
  	    var Enfant=NodeListe.item(0);
  	    this.selectobj.removeChild(Enfant);
  	  }
  	
  	
  	
      // Clear the select list so nothing is displayed
  	
      //this.selectobj.options.length = 0;

      // Set up the regular expression.
      // If there is an error in the regexp,
      // then return without selecting any items.
      try {

        // Initialize the regexp
        regexp = new RegExp(pattern, this.flags);

      } catch(e) {

        // There was an error creating the regexp.

        // If the user specified a function hook,
        // call it now, then return
        if (typeof this.hook == 'function') {
          this.hook();
        }

        return;
      }

      // Loop through the entire select list and
      // add the matching items to the select list
      
      
      
      for (loop=0; loop < this.options0copy.length; loop++) {
  	
  	// This is the option that we're currently testing
  	var option = this.options0copy[loop];
  	
  	/////Rod //////////
  	valueToTest = option.text;
  	if (attributeToTest) {
  	  valueToTest = option.getAttribute(attributeToTest);
  	  //alert(valueToTest);
    	}
  	
  	///////////////////
  	
  	// Check if we have a match
//  	if ((this.match_text && regexp.test(option.text)) ||
  		if ((this.match_text && regexp.test(valueToTest)) ||
  		(this.match_value && regexp.test(option.value))) {
  	    
  	    // We have a match, so add this option to the select list
  	    // and increment the index
  	    
  	    this.selectobj.options[index++] =
  		new Option(option.text, option.value, false);
  	    
  	}
      }
      
      
      var match = 0;
      
      for (loop=0; loop < this.optionscopy.length; loop++) {
  	//creer le groupe
  	var optg = document.createElement("optgroup");
  	optg.label = this.groupescopy[loop].label;
  	
  	match = 0;
  	for (x=0; x < this.optionscopy[loop].length; x++) {
  	 // This is the option that we're currently testing
  	      var option = this.optionscopy[loop][x];

  	      // Check if we have a match
  	      if ((this.match_text && regexp.test(option.text)) ||
  	          (this.match_value && regexp.test(option.value))) {
  		  match = 1;
  	        // We have a match, so add this option to the select list
  	        // and increment the index

  		var newOption = document.createElement("option");
  		newOption.value = option.value;
  		newOption.appendChild(document.createTextNode(option.text));
  		optg.appendChild(newOption);  
  		  
  	       // this.selectobj.options[index++] = newOption;

  	      }
  	}
  	if (match >0) {
  	    this.selectobj.appendChild(optg);
  	}
      }


  	//ROD: reselect previous
  	if (optionSelected) {
  		//alert(optionSelected);
  		this.reselect(optionSelected);
  	}
  	


      // If the user specified a function hook,
      // call it now
      if (typeof this.hook == 'function') {
        this.hook();
      }

    }


    //--------------------------------------------------
    this.set_ignore_case = function(value) {
      // This method sets the regexp flags.
      // If value is true, sets the flags to "i".
      // If value is false, sets the flags to "".

      if (value) {
        this.flags = 'i';
      } else {
        this.flags = '';
      }
    }


    //--------------------------------------------------
    this.debug = function(msg) {
      if (this.show_debug) {
        alert('FilterList: ' + msg);
      }
    }


    //==================================================
    // Initialize the object
    //==================================================
    this.init();

}
