/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.FwkCktlEvenementSchedulerUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementTypeCat;
import org.cocktail.fwkcktlevenement.serveur.modele.EORepetition;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.jdbcadaptor.JDBCAdaptor;

import er.extensions.ERXFrameworkPrincipal;
import er.extensions.appserver.ERXApplication;
import er.extensions.concurrency.ERXTaskObjectStoreCoordinatorPool;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

/**
 * 
 * Classe d'initialisation du framework Evenement.<br/>
 * 
 * 
 * <h2>Architecture mise en oeuvre autour de quartz</h2>
 * Actuellement l'architecture prévue est : <strong>1 seul scheduler Quartz par Application.</strong><br/>
 * Autrement dit <u><strong>une seule instance</strong> de l'application doit démarrer un scheduler</u>, ceci est très important.<br/>
 * Si plusieurs instances de quartz sont démarrées pour une même application, les tâches associées aux évènements 
 * seront exécutés plusieurs fois...
 * <br/>
 * Pour démarrer un scheduler Quartz lors du démarrage de l'instance, il faut positionner le paramètre 
 * 
 * <strong><code>EVT_WOINSTANCE_START_QUARTZ = true</code></strong> .
 * 
 * <p>
 *  Les évènements sont reschedulés à chaque redémarrage de l'instance (si le param EVT_WOINSTANCE_START_QUARTZ est à true).
 *  Seuls les évènements concernant l'application sont reschedulés : les évènements sont filtrés selon le paramètre APP_ID.
 *  Si ce comportement par défaut ne convient pas, un déléguée peut être passé : {@link Delegate} pour récupérer
 *  les évènements de manière personnalisée.
 * </p>
 * 
 * <p>
 * Enfin, le scheduler quartz est paramétrable et les propriétés quartz peuvent être directement insérées dans le 
 * fichier Properties de l'application ou du framework.
 * <br/>
 * Exemple :
 * <code>org.quartz.threadPool.threadCount = 3</code><br/>
 * cf <a href="http://www.quartz-scheduler.org/docs/configuration">http://www.quartz-scheduler.org/docs/configuration</a>
 * </p>
 * 
 * <h2>Utilisation du framework</h2>
 * Les méthodes utiles pour gérer des évènements et des taches se trouvent dans la classe <strong>{@link FwkCktlEvenementUtil}</strong>, 
 * celles pour manipuler le scheduler quartz et les triggers quartz sont dans la classe {@link FwkCktlEvenementSchedulerUtil}.
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class FwkCktlEvenementPrincipal extends ERXFrameworkPrincipal {

    public static final Logger LOG = FwkCktlEvenementUtil.LOG;
    public static final String PROP_JOB_STORE_DATA_SOURCE = "org.quartz.jobStore.dataSource";
    public static final String PROP_JOB_STORE_CLASS = "org.quartz.jobStore.class";
    public static final String PROP_JDBC_JOB_STORE_CLASS = "org.quartz.impl.jdbcjobstore.JobStoreTX";
    public static final String PROP_NEW_DATA_SOURCE_NAME = "quartzCktlEvenementDS";
    public static final String PROP_NEW_DATA_SOURCE_PREFIX = "org.quartz.dataSource." + PROP_NEW_DATA_SOURCE_NAME + ".";
//    private static FwkCktlEvenementPrincipal fwkCktlEvenementPrincipal;
    private Scheduler scheduler;
    private EOObjectStoreCoordinator osc;
    private Delegate delegate;

    
    static {
        setUpFrameworkPrincipalClass(FwkCktlEvenementPrincipal.class);
    }
    
    public FwkCktlEvenementPrincipal() {
        setDelegate(new DefaultDelegate());
    }
    
    public static FwkCktlEvenementPrincipal instance() {
        return sharedInstance(FwkCktlEvenementPrincipal.class);
    }

    private void initScheduler() throws SchedulerException, ExceptionOperationImpossible  {
        LOG.info("Mise en cache des objets de base : types d'evenements...");
        EOEvenementType.getTypesEvenement();
        EOEvenementTypeCat.getTypeCategories();
        EOTypeTache.getTypesTache();
        EOEvenementEtat.getEtatsEvenement();
        EORepetition.getRepetitions();
        if (!WOApplication.application().isTerminating() && appInstanceOwnScheduler()) {
            Properties props = new Properties();
            if (PROP_JDBC_JOB_STORE_CLASS.equals(
                            ERXProperties.stringForKeyWithDefault(PROP_JOB_STORE_CLASS,"org.quartz.simpl.RAMJobStore"))) {
                // Si on est dans le cadre d'un jdbcstore, on récupère les propriétés à partir du modèle
                JDBCAdaptor jdbcAdaptor = jdbcAdaptor();
                props.put(PROP_JOB_STORE_DATA_SOURCE, PROP_NEW_DATA_SOURCE_NAME);
                props.put(PROP_NEW_DATA_SOURCE_PREFIX + StdSchedulerFactory.PROP_DATASOURCE_DRIVER,
                                jdbcAdaptor.driverName());
                props.put(PROP_NEW_DATA_SOURCE_PREFIX + StdSchedulerFactory.PROP_DATASOURCE_URL,
                                jdbcAdaptor.connectionURL());
                props.put(PROP_NEW_DATA_SOURCE_PREFIX + StdSchedulerFactory.PROP_DATASOURCE_USER,
                                jdbcAdaptor.username());
                props.put(PROP_NEW_DATA_SOURCE_PREFIX + StdSchedulerFactory.PROP_DATASOURCE_PASSWORD,
                                jdbcAdaptor.password());
            }
            // On ovverride par les propriétés quartz extraites des properties
            props.putAll(userDefinedQuartzProperties());
            SchedulerFactory schFactory = new StdSchedulerFactory(props);
            scheduler = schFactory.getScheduler();
            scheduler.start();
            LOG.info("Scheduler quartz démarré !");
            NSArray<Trigger> triggers = FwkCktlEvenementSchedulerUtil.triggersProgrammes(scheduler);
            FwkCktlEvenementUtil.debugPrintArray("Triggers trouves", triggers, LOG);
            // Lancement de la tâche en background de rescheduling des évènements !
            long period = ERXProperties.longForKeyWithDefault("EVT_RESCHEDULING_DELAY", 20*60) * 1000;
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new SchedulingEvenementTask(), new NSTimestamp(), period);
        }
    }

    /**
     * @return true si l'application doit lancer un scheduler
     */
    public boolean appInstanceOwnScheduler() {
        ERXApplication app = (ERXApplication) WOApplication.application();
        if (!app.isDevelopmentMode()) {
            return ERXProperties.booleanForKeyWithDefault("EVT_WOINSTANCE_START_QUARTZ", false);
        } else {
            return true;
        }
    }

    private Map<String, String> userDefinedQuartzProperties() {
        Map<String, String> quartzProps = new HashMap<String, String>();
        for (Entry<Object, Object> entry : System.getProperties().entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (key.startsWith("org.quartz")) {
                quartzProps.put(key, value);
            }
        }
        return quartzProps;
    }

    private JDBCAdaptor jdbcAdaptor() {
        EOModel model = EOModelGroup.defaultGroup().modelNamed("FwkCktlEvenementQuartz");
        EODatabaseContext dContext = EODatabaseContext.registeredDatabaseContextForModel(model, ERXEC.newEditingContext());
        JDBCAdaptor jdbcAdaptor = (JDBCAdaptor) dContext.adaptorContext().adaptor();
        return jdbcAdaptor;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * @return un nouvel editing context dans la stack eof réservée
     *      à l'exécution de jobs. Ceci permet d'isoler
     *      au maximum les pbs éventuels pouvant arriver lors de l'exécution de jobs.
     */
    public synchronized EOEditingContext newEditingContextForJobs() {
        if (osc == null) {
            osc = ERXTaskObjectStoreCoordinatorPool.objectStoreCoordinator();
        }
        ERXEC ec = (ERXEC)ERXEC.newEditingContext(osc);
        ec.setUseAutoLock(false);
        ec.setCoalesceAutoLocks(false);
        return ec;
    }

    public void setDelegate(Delegate delegate) {
        this.delegate = delegate;
    }

    public String getAppName() {
        return this.delegate.getAppName();
    }

    /**
     * 
     * Délégué par défaut, fetch les évènements ayant un appName égal au paramètre APP_ID
     * de l'application. Doit être suffisant dans 90% des cas. 
     * 
     *
     */
    public static class DefaultDelegate implements Delegate {

        public NSArray<EOEvenement> fetchEvents(EOEditingContext ec, NSTimestamp dateCreation) {
            // Qualifier pour récupérer les évènements correspondants à l'application uniquement en s'appuyant sur la méthode getAppName()
            EOQualifier qual = ERXQ.equals(EOEvenement.APP_NAME_KEY, getAppName());
            if (dateCreation != null)
                qual = ERXQ.and(qual, ERXQ.greaterThanOrEqualTo(EOEvenement.DATE_CREATION_KEY, dateCreation));
            ERXFetchSpecification<EOEvenement> fetchSpec = 
                            new ERXFetchSpecification<EOEvenement>(EOEvenement.ENTITY_NAME, qual, null);
            fetchSpec.setPrefetchingRelationshipKeyPaths(new NSArray<String>(EOEvenement.TACHES_KEY));
            fetchSpec.setIsDeep(true);
            NSArray<EOEvenement> evenements = fetchSpec.fetchObjects(ec);
            return evenements;
        }

        public String getAppName() {
            CktlWebApplication webapp = (CktlWebApplication)WOApplication.application();
            return webapp.config().stringForKey(CktlConfig.CONFIG_APP_ID_KEY);
        }



    }

    public static interface Delegate {

        /**
         * @param ec l'ec utilisé pour fetcher
         * @param dateCreation date de création minimale des évènements
         * @return les évènements avec une date de création strictement supérieure à celle passée. Si la date est nulle
         *         doit retourner tous les évènements.
         */
        public NSArray<EOEvenement> fetchEvents(EOEditingContext ec, NSTimestamp dateCreation);

        /**
         * @return Valeur de l'appName permettant de filtrer les évènements concernant l'application. Cette valeur est 
         *         utilsée lors du fetch des evenements dans la methode fetchEvents(), ainsi que dans les methodes de 
         *         création par defaut d'évènements de l'application par le framework.
         */
        public String getAppName();

    }

    /**
     * Task lancée à intervalles réguliers pour rescheduler les évènements.
     * Ceci est nécessaire pour des évènements rajoutés par d'autres instances ou de façon extérieure. 
     *
     */
    private class SchedulingEvenementTask extends TimerTask {

        private NSTimestamp lastPassage = null;

        @Override
        public void run() {
            rescheduleAllEvents(scheduler, lastPassage);
            lastPassage = new NSTimestamp();
        }

        private void rescheduleAllEvents(Scheduler scheduler, NSTimestamp dateCreation) {
            // On va faire les traitements dans une stack eof à part que l'on va dropper, ceci pour éviter de bouffer
            // de la mémoire par de trop gros fetch, les effets de bords, et surtout le lock de la connex à la base
            EOEditingContext ec = newEditingContextForJobs();
            ec.lock();
            try {
                NSArray<EOEvenement> evenements = delegate.fetchEvents(ec, dateCreation);
                LOG.info("Passage du daemon reschedulant les évènements ");
                LOG.info(evenements.count() + " nouveaux evenements à scheduler");
                for (EOEvenement evenement : evenements) {
                    try {
                        LOG.info("Va programmer les tâches éventuelles corespondants à l'évènement " + evenement);
                        FwkCktlEvenementSchedulerUtil.programmerTachesEvenement(evenement, new NSTimestamp(), scheduler, ec);
                    } catch (Exception e) {
                        LOG.warn("Une erreur est survenue lors du scheduling de l'évènement " + evenement, e);
                    }
                }
            } finally {
                ec.unlock();
                // A garbage collecter rapidos
                ec.dispose();
                ec = null;
            }
        }

    }

    @Override
    public void didFinishInitialization() {
        super.didFinishInitialization();
        try {
            this.initScheduler();
        } catch (ExceptionOperationImpossible e) {
            LOG.error("Probleme concernant le scheduler Quartz !", e);
        } catch (SchedulerException e) {
            LOG.error("Probleme concernant le scheduler Quartz !", e);
        }    
    }

    @Override
    public void finishInitialization() {
        
    }

}
