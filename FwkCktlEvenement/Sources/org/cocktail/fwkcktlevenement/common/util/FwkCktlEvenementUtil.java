/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.common.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.FwkCktlEvenementPrincipal;
import org.cocktail.fwkcktlevenement.FwkCktlEvenementPrincipal.Delegate;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionEvenementCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionEvenementDocumentCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionFrequenceCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionTacheCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionTacheModif;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionTacheSuppression;
import org.cocktail.fwkcktlevenement.common.util.quartz.CronUtilities;
import org.cocktail.fwkcktlevenement.common.util.quartz.FwkCktlEvenementSchedulerUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple;
import org.cocktail.fwkcktlevenement.serveur.modele.EORepetition;
import org.cocktail.fwkcktlevenement.serveur.modele.EORolePersonne;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTache;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.quartz.CronExpression;
import org.quartz.Job;
import org.quartz.Trigger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Classe offrant les methodes statiques utilitaires du framework FwkCktlEvenement.<br/>
 * Les méthodes proposées ici permettent de manipuler des évènements et des taches.<br/>
 * (Voir creerEvenementPonctuel... , creerEvenementPeriodique..., ajouterTacheAlerteMail ou ajouterTacheExecutionApplication ).
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class FwkCktlEvenementUtil {
	/** Separateur d'adresses mail */
	public static final String RECIPIENTS_SEPARATOR		= ", ";
	
	/** fwkcktlevenement logging support */
	public static final Logger LOG = Logger.getLogger("org.cocktail.fwkcktlevenement");
	
	/**
	 * Affichage d'un NSArray dans le log FwkCktlEvenementUtil.fwkCktlEvtLog, avec la priorité DEBUG.
	 * @param name le nom de la variable
	 * @param array le NSArray a logger
	 */
	public static void debugPrintArrayInLog(final String name, final NSArray array) {
		FwkCktlEvenementUtil.printArray(name, array, LOG, Level.DEBUG);
	}
	
	/**
	 * Affichage d'un NSArray dans un log donné, avec la priorité DEBUG.
	 * @param name le nom de la variable
	 * @param array le NSArray a logger
	 * @param log le logger log4J
	 */
	public static void debugPrintArray(final String name, final NSArray array, Logger log) {
		FwkCktlEvenementUtil.printArray(name, array, log, Level.DEBUG);
	}
	
	/**
	 * Affichage d'un NSArray dans un log donné, avec la priorité donnée.
	 * @param name le nom de la variable
	 * @param array le NSArray a logger
	 * @param log le logger log4J
	 * @param priority la priorité du log (constante log4j : INFO, DEBUG, ERROR ...)
	 */
	public static void printArray(final String name, final NSArray array, Logger log, Priority priority) {
		StringBuffer buffer = new StringBuffer();
		if (array == null) {
			buffer.append(name+" == null");
		} else {
			if (array.count() < 1) {
				buffer.append(name+" : vide");
			} else {
				buffer.append(name+" : \n");
				for (int i=0; i<array.count(); i++) {
					buffer.append("\t["+i+"] : "+array.objectAtIndex(i)+"\n");
				}
			}
		}
		buffer.append("\n");
		log.log(priority, buffer.toString());
	}
	/**
	 * 
	 * Affichage d'un EOGenericRecord dans le log FwkCktlEvenementUtil.fwkCktlEvtLog,
	 * avec la priorité DEBUG.
	 * @param name le nom de la variable
	 * @param record EOGenericRecord a afficher
	 */
	public static void debugPrintRecordInLog(final String name, final EOGenericRecord record) {
		FwkCktlEvenementUtil.printRecord(name, record, LOG, Level.DEBUG);
	}
	/**
	 * le log FwkCktlEvenementUtil.fwkCktlEvtLog,
	 * Affichage d'un EOGenericRecord dans un log donné, avec la priorité DEBUG.
	 * @param name le nom de la variable
	 * @param record EOGenericRecord a afficher
	 * @param log le logger log4J
	 */
	public static void debugPrintRecord(final String name, final EOGenericRecord record, Logger log) {
		FwkCktlEvenementUtil.printRecord(name, record, log, Level.DEBUG);
	}
	
	/**
	 * Affichage d'un EOGenericRecord dans un log donné, avec la priorité donnée.
	 * @param name le nom de la variable
	 * @param record EOGenericRecord a afficher
	 * @param log le logger log4J
	 * @param priority la priorité du log
	 */
	public static void printRecord(final String name, final EOGenericRecord record, Logger log, Priority priority) {
		StringBuffer buffer = new StringBuffer("\n");
		if (record == null)
			buffer.append(name+" == null");
		else {
			buffer.append(name+" : \n");
			buffer.append("\t[class] : "+record.getClass()+"\n");
			buffer.append("\t[editingContext] : "+record.editingContext()+"\n");
			NSArray keys = record.allPropertyKeys();
			for (int i=0; i<keys.count(); i++) {
				String key = (String) keys.objectAtIndex(i);
				Object obj = record.valueForKey(key);
				buffer.append("\t[").append(key).append("] : ");
				buffer.append(obj+"\n");
			}
		}
		buffer.append("\n");
		log.log(priority, buffer.toString());
	}
	/**
	 * Affichage d'un EOGenericRecord dans le log FwkCktlEvenementUtil.fwkCktlEvtLog, avec la priorité DEBUG.
	 * @param name le nom de la variable
	 * @param map la map du contenu à logger
	 */
	public static void debugPrintMapInLog(final String name, final Map map) {
		FwkCktlEvenementUtil.printMap(name, map, LOG, Level.DEBUG);
	}
	/**
	 * Affichage d'un EOGenericRecord dans un log donné, avec la priorité DEBUG.
	 * @param name le nom de la variable
	 * @param map la map du contenu à logger
	 * @param log le logger log4J
	 */
	public static void debugPrintMap(final String name, final Map map, Logger log) {
		FwkCktlEvenementUtil.printMap(name, map, log, Level.DEBUG);
	}
	/**
	 * Affichage d'une Map
	 * @param name le nom de la variable
	 * @param map la map du contenu à logger
	 * @param log le logger log4J
	 * @param priority la priorité du log
	 * 
	 */
	public static void printMap(final String name, final Map map, Logger log, Priority priority) {
		StringBuffer buffer = new StringBuffer("\n");
		if (map == null)
			buffer.append(name+" == null");
		else {
			buffer.append(name+" : \n");
			for (Iterator iter = map.keySet().iterator(); iter.hasNext(); ) {
				Object key = iter.next();
				Object obj = map.get(key);
				buffer.append("\t["+key+"] : "+obj);
			}
		}
		buffer.append("\n");
		log.log(priority, buffer.toString());
	}
	/**
     * Refault les objects passes en parametres. 
     */
    public static void invalidateObjects(EOEditingContext ec, NSArray records) {
    	if (records == null)
    		throw new IllegalArgumentException("Liste d'objets metier vide interdite.");
        NSArray listGIDs = ERXEOControlUtilities.globalIDsForObjects(records);
        ec.invalidateObjectsWithGlobalIDs(listGIDs);
    }
	
	/*
	 * Gestions des evenements :
	 */
    
    /*---------- Creation ------------------------*/
	/**
	 * Cree un nouvel evenement pre-initialise, avec l'unique periodicite specifiee. L'appName utilisé est celui donné
	 * par le {@link Delegate} du framework.
	 * NB : Aucune frequence CRON ne sera céée et relié à ce nouvel événement 
	 * si l'un des paramètres 'répétition', 'cronExpression', 'traduction' ou 
	 * 'dateFin' est null.
	 * Exemple d'expression CRON : * 0 14 * * * *  qui se traduit par tous les
	 * jours de tous les mois de toutes les années à 14h00 (et 0 secondes).
	 * @param type Type de l'evenement. Un finder existe.
	 * @param dateHeurePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param repetition Type de repetition (periodicite) de l'evenement. Un finder existe.
	 * @param cronExpression Expression CRON qui specifie la periodicite. {@link CronExpression}
	 * @param traduction Traduction en francais de la periodicite. Ex: "Tous les jours", "Le 1,5,7 du mois".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @return L'evenement cree.
	 * @throws ExceptionEvenementCreation
	 * @see EOEvenementType
	 * @see EORepetition
	 * @see CronExpression
	 */
	public static EOEvenement creerNouvelEvenementPeriodiqueForType(
			final EOEvenementType type,
			final NSTimestamp dateHeurePrevue,
			final String objet,
			final String observations,
			final EORepetition repetition,
			final String cronExpression, 
			final String traduction, 
			final NSTimestamp dateFin,
			final EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {
		
		return FwkCktlEvenementUtil.creerNouvelEvenementPeriodiqueForTypeAndAppName(type, dateHeurePrevue, objet, observations, 
										FwkCktlEvenementPrincipal.instance().getAppName(), repetition, cronExpression, 
										traduction, dateFin, utilisateur, editingContext);
	}

	/**
	 * Cree un nouvel evenement pre-initialise, avec la periodicite specifiee. Parametres sous forme d'ids internes.
	 * L'appName utilisé est celui donné par le {@link Delegate} du framework.
	 * @param typeEvenementIdInterne Id interne du type de l'evenement. Ex: {@link EOEvenementType#ID_INTERNE_REUNION}.
	 * @param dateHeurePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param repetitionIdInterne Id interne du type de repetition (periodicite) de l'evenement. Ex: {@link EORepetition#ID_INTERNE_SEMAINE}.
	 * @param cronExpression Expression CRON qui specifie la periodicite. {@link CronExpression}
	 * @param traduction Traduction en français de la periodicite. Ex: "Tous les jours", "Le 1,5,7 du mois".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @return L'evenement cree.
	 * @throws ExceptionEvenementCreation
	 * @see EOEvenementType
	 * @see EORepetition
	 * @see CronExpression
	 */
	public static EOEvenement creerNouvelEvenementPeriodique(
			final String typeEvenementIdInterne,
			final NSTimestamp dateHeurePrevue,
			final String objet,
			final String observations,
			final String repetitionIdInterne,
			final String cronExpression, 
			final String traduction, 
			final NSTimestamp dateFin,
			EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {

		return FwkCktlEvenementUtil.creerNouvelEvenementPeriodiqueForAppName(typeEvenementIdInterne, dateHeurePrevue, 
													objet, observations, 
													FwkCktlEvenementPrincipal.instance().getAppName(), 
													repetitionIdInterne, cronExpression, traduction, dateFin, 
													utilisateur, editingContext);
	}
	
	/**
	 * Cree un nouvel evenement pre-initialise, sans repetition. 
	 * L'appName utilisé est celui donné par le {@link Delegate} du framework.
	 * @param type Type de l'evenement. Un finder existe : {@link EOEvenementType#findWithIdInterne(String, EOEditingContext)}
	 * @param datePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @return le nouvel evenement inséré dans l'editing context.
	 * @throws ExceptionEvenementCreation 
	 */
	public static EOEvenement creerNouvelEvenementPonctuelForType(
			final EOEvenementType type,
			final NSTimestamp datePrevue,
			final String objet,
			final String observations,
			final EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {
		
		return FwkCktlEvenementUtil.creerNouvelEvenementPonctuelForTypeAndAppName(type, datePrevue, objet, observations, 
										FwkCktlEvenementPrincipal.instance().getAppName(), utilisateur, editingContext);
	}
	
	/**
	 * Cree un nouvel evenement pre-initialise, sans repetition.
	 * L'appName utilisé est celui donné par le {@link Delegate} du framework.
	 * @param typeEvenementIdInterne Id interne du type de l'evenement. Ex: {@link EOEvenementType#ID_INTERNE_REUNION}.
	 * @param datePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @throws ExceptionEvenementCreation 
	 */
	public static EOEvenement creerNouvelEvenementPonctuel(
			final String typeEvenementIdInterne,
			final NSTimestamp datePrevue,
			final String objet,
			final String observations,
			EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {

		return FwkCktlEvenementUtil.creerNouvelEvenementPonctuelForAppName(typeEvenementIdInterne, datePrevue, objet, observations, 
											FwkCktlEvenementPrincipal.instance().getAppName(), utilisateur, editingContext);
	}
	
	/**
	 * Cree un nouvel evenement vierge, avec quelques attribut initialisés par défaut. 
	 * Il ne pourra sans doute pas etre enregistre dans cet etat et pourra etre modifier a l'aide des 
	 * methodes de cet utilitaire.
	 * L'appName est donné par le {@link Delegate} de framework {@link FwkCktlEvenementPrincipal}. (L'implémentation par
	 * défaut déduie cet appName du paramètre APP_ID.)
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @throws ExceptionEvenementCreation 
	 */
	public static EOEvenement creerNouvelEvenement(
			EOPersonne utilisateur, 
			EOEditingContext editingContext) throws ExceptionEvenementCreation {

		if (utilisateur == null)
			throw new IllegalStateException("Un utilisateur doit être fourni à FwkCktlEvenementUtil.");

		String appName = FwkCktlEvenementPrincipal.instance().getAppName();
		
		Date datePrevue = new Date();
		datePrevue = DateUtilities.decaler(datePrevue, 2, Calendar.HOUR_OF_DAY);
		EOEvenement evenement = EOEvenement.creerEvenement(
				EOEvenementType.getTypeEvenementDefault(), 
				new NSTimestamp(datePrevue), 
				"", 
				"",
				appName,
				utilisateur,
				editingContext);
		
		return evenement;
	}
	
	/*---------- Creation pour appName -----------------*/
	
	/**
	 * Cree un nouvel evenement pre-initialise, avec l'unique periodicite specifiee. 
	 * NB : Aucune frequence CRON ne sera céée et relié à ce nouvel événement 
	 * si l'un des paramètres 'répétition', 'cronExpression', 'traduction' ou 
	 * 'dateFin' est null.
	 * Exemple d'expression CRON : * 0 14 * * * *  qui se traduit par tous les
	 * jours de tous les mois de toutes les années à 14h00 (et 0 secondes).
	 * @param type Type de l'evenement. Un finder existe.
	 * @param dateHeurePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param appName appName servant à identifier l'application à laquelle sera rattaché l'évènement.
	 * @param repetition Type de repetition (periodicite) de l'evenement. Un finder existe.
	 * @param cronExpression Expression CRON qui specifie la periodicite. {@link CronExpression}
	 * @param traduction Traduction en francais de la periodicite. Ex: "Tous les jours", "Le 1,5,7 du mois".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @return L'evenement cree.
	 * @throws ExceptionEvenementCreation
	 * @see EOEvenementType
	 * @see EORepetition
	 * @see CronExpression
	 */
	public static EOEvenement creerNouvelEvenementPeriodiqueForTypeAndAppName(
			final EOEvenementType type,
			final NSTimestamp dateHeurePrevue,
			final String objet,
			final String observations,
			final String appName,
			final EORepetition repetition,
			final String cronExpression, 
			final String traduction, 
			final NSTimestamp dateFin,
			final EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {
		
		EOEvenement evenement = 
			FwkCktlEvenementUtil.creerNouvelEvenementPonctuelForTypeAndAppName(type, dateHeurePrevue, objet, observations, appName, utilisateur, editingContext);
		
		if (repetition!=null && cronExpression!=null && traduction!=null) {
			try {
				EOFrequenceCron.creerFrequenceCron(
						evenement, repetition, cronExpression, traduction, dateFin, editingContext);	
			} 
			catch (ExceptionFrequenceCreation e) {
				throw new ExceptionEvenementCreation("Impossible de creer la frequence.", e);
			}
		} else {
			FwkCktlEvenementUtil.LOG.warn("Aucune frequence Cron n'a " +
					"été céée pour ce nouvel événement car l'un des paramètres " +
					"'répétition', 'cronExpression', 'traduction' ou 'dateFin' " +
					"est null");
		}
		
		return evenement;
	}

	/**
	 * Cree un nouvel evenement pre-initialise, avec la periodicite specifiee. Parametres sous forme d'ids internes.
	 * @param typeEvenementIdInterne Id interne du type de l'evenement. Ex: {@link EOEvenementType#ID_INTERNE_REUNION}.
	 * @param dateHeurePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param appName appName servant à identifier l'application à laquelle sera rattaché l'évènement.
	 * @param repetitionIdInterne Id interne du type de repetition (periodicite) de l'evenement. Ex: {@link EORepetition#ID_INTERNE_SEMAINE}.
	 * @param cronExpression Expression CRON qui specifie la periodicite. {@link CronExpression}
	 * @param traduction Traduction en français de la periodicite. Ex: "Tous les jours", "Le 1,5,7 du mois".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @return L'evenement cree.
	 * @throws ExceptionEvenementCreation
	 * @see EOEvenementType
	 * @see EORepetition
	 * @see CronExpression
	 */
	public static EOEvenement creerNouvelEvenementPeriodiqueForAppName(
			final String typeEvenementIdInterne,
			final NSTimestamp dateHeurePrevue,
			final String objet,
			final String observations,
			final String appName,
			final String repetitionIdInterne,
			final String cronExpression, 
			final String traduction, 
			final NSTimestamp dateFin,
			EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {

		EOEvenementType type;
		try {
			type = EOEvenementType.findWithIdInterne(typeEvenementIdInterne, editingContext);
		} catch (Exception e) {
			throw new ExceptionEvenementCreation("Type d'événement introuvable: "+typeEvenementIdInterne);
		}
		EORepetition repetition;
		try {
			repetition = EORepetition.findWithIdInterne(repetitionIdInterne,editingContext);
		} catch (Exception e) {
			throw new ExceptionEvenementCreation("Type de répétition introuvable: "+repetitionIdInterne);
		}
		
		return FwkCktlEvenementUtil.creerNouvelEvenementPeriodiqueForTypeAndAppName(
					type, 
					dateHeurePrevue, 
					objet, 
					observations,
					appName,
					repetition, 
					cronExpression, 
					traduction, 
					dateFin,
					utilisateur,
					editingContext);
	}
	
	/**
	 * Cree un nouvel evenement pre-initialise, sans repetition.
	 * @param type Type de l'evenement. Un finder existe : {@link EOEvenementType#findWithIdInterne(String, EOEditingContext)}
	 * @param datePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param appName appName servant à identifier l'application à laquelle sera rattaché l'évènement.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @return le nouvel evenement inséré dans l'editing context.
	 * @throws ExceptionEvenementCreation 
	 */
	public static EOEvenement creerNouvelEvenementPonctuelForTypeAndAppName(
			final EOEvenementType type,
			final NSTimestamp datePrevue,
			final String objet,
			final String observations,
			final String appName,
			final EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {
		
		if (utilisateur == null)
			throw new IllegalStateException("Un utilisateur doit être fourni au framework FwkCktlEvenement.");

		EOEvenement evenement = EOEvenement.creerEvenement(
				type, 
				datePrevue, 
				objet, 
				observations,
				appName,
				utilisateur,
				editingContext);
		
		return evenement;
	}
	
	/**
	 * Cree un nouvel evenement pre-initialise, sans repetition.
	 * @param typeEvenementIdInterne Id du type de l'evenement. Un finder existe.
	 * @param datePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param appName appName servant à identifier l'application à laquelle sera rattaché l'évènement.
	 * @param utilisateur {@link EOPersonne} pris comme créateur de l'événement.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer l'instance du nouvel evenement.
	 * @throws ExceptionEvenementCreation 
	 */
	public static EOEvenement creerNouvelEvenementPonctuelForAppName(
			final String typeEvenementIdInterne,
			final NSTimestamp datePrevue,
			final String objet,
			final String observations,
			final String appName,
			EOPersonne utilisateur,
			EOEditingContext editingContext) throws ExceptionEvenementCreation {

		EOEvenementType type;
		try {
			type = EOEvenementType.findWithIdInterne(typeEvenementIdInterne, editingContext);
		} catch (Exception e) {
			throw new ExceptionEvenementCreation("Type d'événement introuvable: "+typeEvenementIdInterne);
		}

		EOEvenement evenement = FwkCktlEvenementUtil.creerNouvelEvenementPonctuelForTypeAndAppName(
				type, 
				datePrevue, 
				objet, 
				observations,
				appName,
				utilisateur,
				editingContext);
		
		return evenement;
	}
	
	/*---------- Modification ------------------------*/
	
	/**
	 * Modifie un evenement existant pour l'application. (l'appName est donné par le {@link Delegate} du framework.)
	 * @param evenement Evenement a modifier. Un finder existe.
	 * @param type Type de l'evenement. Un finder existe.
	 * @param dateHeurePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param etat Etat de l'evenement: realise ou prevu. Un finder existe.
	 * @param utilisateur {@link EOPersonne} pris comme modificateur de l'événement.
	 * @see EOEvenementType
	 * @see CronExpression
	 */
	public static void modifierEvenement(
			EOEvenement evenement,
			EOEvenementType type,
			NSTimestamp dateHeurePrevue,
			String objet,
			String observations,
			EOEvenementEtat etat,
			EOPersonne utilisateur) {
		modifierEvenement(
				evenement, 
				type, 
				dateHeurePrevue, 
				objet, 
				observations,
				FwkCktlEvenementPrincipal.instance().getAppName(),
				etat, 
				utilisateur);
	}
	
	/**
	 * Modifie un evenement existant.
	 * @param evenement Evenement a modifier. Un finder existe.
	 * @param type Type de l'evenement. Un finder existe.
	 * @param dateHeurePrevue Date prevue de l'evenement, passe ou futur.
	 * @param objet Objet, intitule, motif de l'evenement
	 * @param observations Observations, remarques facultatives.
	 * @param appName appName servant à identifier l'application à laquelle sera rattaché l'évènement.
	 * @param etat Etat de l'evenement: realise ou prevu. Un finder existe.
	 * @param utilisateur {@link EOPersonne} pris comme modificateur de l'événement.
	 * @see EOEvenementType
	 * @see CronExpression
	 */
	public static void modifierEvenement(
			EOEvenement evenement,
			EOEvenementType type,
			NSTimestamp dateHeurePrevue,
			String objet,
			String observations,
			String appName,
			EOEvenementEtat etat,
			EOPersonne utilisateur) {
		
//		NSTimestamp datePrevue = new NSTimestamp(dateHeurePrevue);
		
		EOEvenement.modifierEvenement(
				evenement, 
				type, 
				dateHeurePrevue, 
				objet, 
				observations,
				appName,
				etat, 
				utilisateur);
		
		FwkCktlEvenementUtil.debugPrintRecordInLog("evenement apres modif", evenement);
	}
	
	/*---------- Suppression ------------------------*/
	
	/**
	 * <p>Supprime un evenement. 
	 * <p>NB: l'evenement ne doit plus posseder de taches pour pouvoir le supprimer. 
	 * @param evenement EOEvenement a supprimer
	 * @throws ExceptionOperationImpossible
	 */
	public static void supprimerEvenement(
	        EOEvenement evenement, EOEditingContext editingContext) throws ExceptionOperationImpossible {

	    if (evenement == null)
	        throw new IllegalArgumentException("Evénement requis.");

	    if (evenement.taches().count() > 0)
	        throw new ExceptionOperationImpossible("L'événement à supprimer possède des tâches.");

	    supprimerFrequences(evenement);
	    supprimerPersonnesConcernees(evenement);
	    supprimerDocuments(evenement);

	    EOEvenement.supprimerEvenement(evenement, editingContext);
	}
	
	/**
	 * <p>Supprime un evenement. 
	 * <p>NB: l'evenement ne doit plus posseder de taches pour pouvoir le supprimer. 
	 * @param evtId Identifiant de l'EOEvenement a supprimer
	 * @throws ExceptionOperationImpossible
	 * @throws ExceptionFinder 
	 */
	public static void supprimerEvenement(
			Number evtId, EOEditingContext editingContext) throws ExceptionOperationImpossible {

		if (evtId == null)
			throw new IllegalArgumentException("Identifiant de l'événement requis.");
		
		supprimerEvenement(EOEvenement.findWithEvtId(evtId,editingContext), editingContext);
	}
	
	/**
	 * <p>Supprime toutes les frequences existantes, de type CRON et simples.
	 * <p>NB: cela ne deprogramme pas les triggers eventuels des taches de cet evenement.
	 * @param evenement EOEvenement dont on veut supprimer toutes les frequences.
	 */
	public static void supprimerFrequences(EOEvenement evenement) {

		if (evenement == null)
			throw new IllegalArgumentException("Evénement requis.");

		FwkCktlEvenementUtil.debugPrintArray("frequencesCrons qui vont etre supprimees", evenement.frequencesCrons(), FwkCktlEvenementUtil.LOG);
		FwkCktlEvenementUtil.debugPrintArray("frequencesSimples qui vont etre supprimees", evenement.frequencesSimples(), FwkCktlEvenementUtil.LOG);
		
		NSArray frequencesCrons = evenement.frequencesCrons().immutableClone();
		NSArray frequencesSimples = evenement.frequencesSimples().immutableClone();
		
		// suppression des frequences CRON existantes
		for (int i=0; i<frequencesCrons.count(); i++) {
			EOFrequenceCron f = (EOFrequenceCron) frequencesCrons.objectAtIndex(i);
			evenement.removeFromFrequencesCronsRelationship(f);
			evenement.editingContext().deleteObject(f);
		}
		// suppression des frequences SIMPLE existantes
		for (int i=0; i<frequencesSimples.count(); i++) {
			EOFrequenceSimple f = (EOFrequenceSimple) frequencesSimples.objectAtIndex(i);
			evenement.removeFromFrequencesSimplesRelationship(f);
			evenement.editingContext().deleteObject(f);
		}
	}
	
	/*---------- Gestion des frequences ------------------------*/

	/**
	 * Cree et ajoute une frequence CRON a l'evenement specifie.
	 * @param evenement EOEvenement conserne. Un finder existe.
	 * @param repetition Type de repetition (periodicite) de l'evenement. Un finder existe.
	 * @param cronExpression Expression CRON qui specifie la periodicite. 
	 * @param traduction Traduction en francais de la frequence. Ex: "Tous les jours", "Le 1,5,7 du mois".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer la frequence CRON.
	 * @return La frequence CRON creee.
	 * @throws ExceptionOperationImpossible
	 */
	public static EOFrequenceCron ajouterFrequenceCron(
			EOEvenement evenement, 
			EORepetition repetition,
			String cronExpression, 
			String traduction, 
			NSTimestamp dateFin,
			EOEditingContext editingContext) throws ExceptionOperationImpossible {

		EOFrequenceCron f;
		try {
			f = EOFrequenceCron.creerFrequenceCron(
					evenement, 
					repetition, 
					cronExpression, 
					traduction, 
					dateFin,
					editingContext);
		} 
		catch (ExceptionFrequenceCreation e) {
			throw new ExceptionOperationImpossible("Impossible de créer la fréquence CRON.", e);
		}
		
		return f;
	}

	   /**
     * Cree et ajoute une frequence CRON a l'evenement specifie.
     * @param evenement EOEvenement conserne. Un finder existe.
     * @param repetition Type de repetition (periodicite) de l'evenement. Un finder existe.
     * @param dateFin Date de fin de repetition ou <code>null</code>.
     * @param editingContext {@link EOEditingContext} utilisé pour créer la frequence CRON.
     * @return La frequence CRON creee.
     * @throws ExceptionOperationImpossible
     */
    public static EOFrequenceCron ajouterFrequenceCronStandard(
            EOEvenement evenement, 
            EORepetition repetition,
            NSTimestamp dateFin,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        EOFrequenceCron f;
        String cronExpression = cronExpression(evenement.datePrevue(), repetition);
        String traduction = repetition.libelleLong();
        try {
            f = EOFrequenceCron.creerFrequenceCron(
                    evenement, 
                    repetition, 
                    cronExpression, 
                    traduction, 
                    dateFin,
                    editingContext);
        } 
        catch (ExceptionFrequenceCreation e) {
            throw new ExceptionOperationImpossible("Impossible de créer la fréquence CRON.", e);
        }
        
        return f;
    }
	
    public static void recalculerFrequenceCronStandard(EOFrequenceCron freq) {
        String cronExpression = cronExpression(freq.evenement().datePrevue(), freq.repetition());
        String traduction = freq.repetition().libelleLong();
        freq.setCronExpression(cronExpression);
        freq.updateSummaryAttributes();
        freq.setTraduction(traduction);
    }
    
    public static String cronExpression(NSTimestamp dateDebut, EORepetition periodicite) {
        CronExpression cronExpression = null;
        try {
            if (periodicite.isQuotidienne())
                cronExpression = CronUtilities.createForDaily(dateDebut);
            else if (periodicite.isHebdomadaire())
                cronExpression = CronUtilities.createForWeekly(null, dateDebut);
            else if (periodicite.isMensuelle())
                cronExpression = CronUtilities.createForMonthly(null, dateDebut);
        } catch (ExceptionOperationImpossible e) {
            throw new NSForwardException(e);
        }
        return cronExpression == null ? null : cronExpression.getCronExpression();
    }
    
	/**
	 * Cree et ajoute une frequence simple a l'evenement specifie.
	 * @param evenement EOEvenement conserne. Un finder existe.
	 * @param repeatCount Nombre de repetition voulu. Utiliser {@link EOFrequenceSimple#REPEAT_INDEFINITLY}
	 * pour repetition infinie.
	 * @param repeatInterval Interval de repetiton en millisecondes.
	 * @param traduction Traduction en francais de la frequence. Ex: "Tous les 14 jours".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param editingContext {@link EOEditingContext} utilisé pour créer la frequence simple.
	 * @return La frequence simple creee.
	 * @throws ExceptionOperationImpossible
	 */
	public static EOFrequenceSimple ajouterFrequenceSimple(
			EOEvenement evenement, 
			Number repeatCount, 
			Number repeatInterval, 
			String traduction, 
			NSTimestamp dateFin,
			EOEditingContext editingContext) throws ExceptionOperationImpossible {

		EOFrequenceSimple f;
		try {
			f = EOFrequenceSimple.creerFrequenceSimple(evenement, repeatCount, repeatInterval, traduction, dateFin, editingContext);
		} 
		catch (ExceptionFrequenceCreation e) {
			throw new ExceptionOperationImpossible("Impossible de créer la fréquence simple.", e);
		}
		
		return f;
		
	}
	
	/*---------- Gestion des documents d'evenement ------------------------*/
	
	/**
	 * Supprime toutes les inscriptions de personnes concernees.
	 * @param evenement EOEvenement dont on veut cupprimer toutes les associations de documents.
	 * @throws ExceptionOperationImpossible 
	 */
	public static int supprimerDocuments(EOEvenement evenement) throws ExceptionOperationImpossible {
		
		if (evenement == null)
			throw new IllegalArgumentException("Evénement requis.");
		
		return EOEvenementDocument.supprimerEvenementDocuments(evenement,evenement.editingContext());
	}
	/**
	 * Associe des references de courriers a un evenement.
	 * @param evenement EOEvenement concerne.
	 * @param couNumeros Identifiant des courriers a associer.
	 * @return Les associations de documents creees, de type {@link EOEvenementDocument}.
	 * @throws ExceptionOperationImpossible 
	 */
	public static NSArray ajouterDocuments(EOEvenement evenement, NSArray couNumeros, EOEditingContext editingContext) throws ExceptionOperationImpossible {

		if (evenement == null)
			throw new IllegalArgumentException("Evénement requis.");
		if (couNumeros == null)
			throw new IllegalArgumentException("Documents requis.");
		
		NSArray crees = new NSArray();
		if (couNumeros!=null && couNumeros.count()>0) {
			try {
				crees = EOEvenementDocument.creerEvenementDocuments(evenement, couNumeros, null, editingContext);
			} 
			catch (ExceptionEvenementDocumentCreation e) {
				throw new ExceptionOperationImpossible("Impossible de créer l'objet métier peremettant d'associer un document.", e);
			}
		}
		
		return crees;
	}
	/**
	 * Associe une reference de courrier a un evenement.
	 * @param evenement EOEvenement concerne.
	 * @param couNumero Reference du courrier.
	 * @return L'association de document creee.
	 * @throws ExceptionOperationImpossible 
	 */
	public static EOEvenementDocument ajouterDocument(EOEvenement evenement, Integer couNumero, EOEditingContext editingContext) throws ExceptionOperationImpossible {

		if (evenement == null)
			throw new IllegalArgumentException("Evénement requis.");
		if (couNumero == null)
			throw new IllegalArgumentException("Référence du document requise.");
		
		EOEvenementDocument ed;
		try {
			ed = EOEvenementDocument.creerEvenementDocument(evenement, couNumero, null, editingContext);
		} 
		catch (ExceptionEvenementDocumentCreation e) {
			throw new ExceptionOperationImpossible("Impossible de créer l'objet métier peremettant d'associer un document.", e);
		}
		
		return ed;
	}
//	/**
//	 * Depose un document dans la GED et l'associe a l'evenement specifie.
//	 * @param evenement EOEvenement concerne.
//	 * @param fileData Donnees brutes du fichier a deposer.
//	 * @param fileName Nom du fichier a deposer. Ex: "licenciement.doc".
//	 * @param rootGed Dossier racine dans la GED dans laquelle sera deposer la fichier.
//	 * @param visibilite Type de visibilite du document. Ex: {@link FacadeEvenement#TYPE_INTERNET}.
//	 * @param persId Identifiant de la personne deposant ce fichier.
//	 * @return L'association de document creee, de type {@link EOEvenementDocument}.
//	 * @throws ExceptionOperationImpossible 
//	 */
//	public satic EOEvenementDocument ajouterDocument(
//			EOEvenement evenement, 
//			NSData fileData,
//			String fileName,
//			String rootGed,
//			Integer visibilite,
//			Integer persId) throws ExceptionOperationImpossible {
//
////		GedTool gt = new GedTool(evenement.editingContext());
////		gt.setRootGed(rootGed);
////		gt.setVisibilite(visibilite);
//
////		Integer couNumero = gt.saveFileIntoGed(fileData, fileName, persId);
//		
//		Integer couNumero = (Integer) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
//						ec,
//						"session.remoteCallDelagate",
//						"clientSideRequestAjouterFicherDansGed",
//						new Class[] {
//								NSData.class, String.class, Number.class }, 
//						new Object[] {
//								fileData, fileName, getUtilisateur().persIdReadOnly() },
//						true);
//		
//		return ajouterDocument(evenement, couNumero);
//	}
//	/**
//	 * Depose un document dans la GED et l'associe a l'evenement specifie.
//	 * @param evenement EOEvenement concerne.
//	 * @param file Fichier a deposer.
//	 * @param rootGed Dossier racine dans la GED dans laquelle sera deposer la fichier.
//	 * @param visibilite Type de visibilite du document. Ex: ????
//	 * @param persId Identifiant de la personne deposant ce fichier.
//	 * @return L'association de document creee, de type {@link EOEvenementDocument}.
//	 * @throws ExceptionOperationImpossible 
//	 */
//	public EOEvenementDocument ajouterDocument(
//			EOEvenement evenement, 
//			File file, 
//			String rootGed,
//			Integer visibilite,
//			Integer persId) throws ExceptionOperationImpossible {
//
//		NSData fileData;
//		try {
//			fileData = new NSData(file.toURL());
//		} 
//		catch (IOException e) {
//			throw new ExceptionOperationImpossible("Impossible de lire le fichier.", e);
//		}
//		
//		return ajouterDocument(evenement, fileData, file.getName(), rootGed, visibilite, persId);
//	}
	
	/*---------- Gestion des personnes concernées ------------------------*/

	/**
	 * Supprime toutes les inscriptions de personnes concernees.
	 * @param evenement EOEvenement dont on veut supprimer toutes les inscriptions de personnes concernees.
	 */
	public static void supprimerPersonnesConcernees(EOEvenement evenement) {

		if (evenement == null)
			throw new IllegalArgumentException("Evénement requis.");

		EOEvenementPersonne.supprimerEvenementPersonnes(evenement, evenement.editingContext());
	}
	/**
	 * Instancie et ajoute une inscription de personne concernee.
	 * @param evenement EOEvenement concerne. Un finder existe.
	 * @param personneConcernee Personne concernee par l'evenement.
	 * @param role Role que joue la personne pour l'evenement.
	 * @param alerter Indique si la personne peut etre prevenue par mail, si bien-sur la fonctionnalite d'alerte est 
	 * cochee par l'utilisateur.
	 * @return L'inscription de la personne concernee creee.
	 * @throws ExceptionOperationImpossible
	 */
	public static EOEvenementPersonne ajouterPersonneConcernee(
			EOEvenement evenement, 
			IPersonne personneConcernee,
			EORolePersonne role, 
			boolean alerter,
			EOEditingContext editingContext) {
		
		EOEvenementPersonne evenementPersonne;
			evenementPersonne = EOEvenementPersonne.creerEvenementPersonne(
					evenement, 
					personneConcernee,
					role, 
					alerter,
					editingContext);
		return evenementPersonne;
	}
	
	/*---------- Gestion des taches d'évènement ------------------------*/
	
	/**
	 * <p>Modifie une tache. 
	 * @param tache {@link EOTache} que l'on veut modifier.
	 * @param type Type de la tache. Un finder existe.
	 * @param className Nom complet de la classe du job a executer.
	 * @param etat Etat souhaite de la tache. Ex: {@link EOTache#ETAT_PROGRAMMATION_DEMANDEE}.
	 * @throws ExceptionOperationImpossible 
	 */
	public static void modifierTache(
			EOTache tache,
			EOTypeTache type, 
			String className, 
			String etat,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {

		try {
			EOTache.modifierTache(tache, type, className, etat, utilisateur);
		} 
		catch (ExceptionTacheModif e) {
			throw new ExceptionOperationImpossible("Impossible de modifier la tâche suivante: "+tache, e);
		}
	}
	/**
	 * <p>Modifie une tache. 
	 * @param tache Tache a modifier.
	 * @param type Type de la tache. Un finder existe. 
	 * @param className Nom de la classe du {@link Job} correspondant au code qui sera appeler par le scheduler Quartz.
	 * @param description Description en francais de ce que fait la tache.
	 * @param sendMailAfterExeBoolean Flag indiquant si un accuse d'execution doit etre envoye par mail.
	 * @param etat Etat de progrmmation de la tache.
	 * @throws ExceptionOperationImpossible
	 */
	public static void modifierTache(
			EOTache tache,
			EOTypeTache type, 
			String className,
			String description,
			boolean sendMailAfterExeBoolean, 
			String etat,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {

		try {
			EOTache.modifierTache(
					tache, 
					type, 
					className, 
					tache.group(), 
					tache.name(), 
					description, 
					(Integer) tache.triggerOffsetInMinutes(), 
					new Boolean(sendMailAfterExeBoolean), 
					etat, 
					utilisateur);
		} 
		catch (ExceptionTacheModif e) {
			throw new ExceptionOperationImpossible("Impossible de modifier la tâche suivante: "+tache, e);
		}
	}


	/**
	 * <p>Modifie une tache. 
	 * @param tache Tache a modifier.
	 * @param tacheExterneUrlFichier URL du fichier .jar contenant le {@link Job} externe a executer.
	 * @param tacheExterneClasse Nom complet de la classe (implementant {@link Job}) representant le code externe a executer.
	 * @throws ExceptionOperationImpossible 
	 */
	public static  void modifierTache(
			EOTache tache,
			String tacheExterneUrlFichier,
			String tacheExterneClasse,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {
		
		try {
			EOTache.modifierTache(
					tache, 
					tacheExterneUrlFichier, 
					tacheExterneClasse, 
					utilisateur);
		} 
		catch (ExceptionTacheModif e) {
			throw new ExceptionOperationImpossible("Impossible de modifier la tâche suivante: "+tache, e);
		}
	}

	/**
	 * <p>Cree et ajoute une tache vierge a un evenement.
	 * <p>NB: un evenement possede au maximum 1 tache de type "alerte par mail".
	 * @param evenement EOEvenement auquel on veut ajouter une tache.
	 * @param type Type de la tache. Un finder existe. 
	 * @return La tache creee.
	 * @throws ExceptionOperationImpossible
	 */
	public static EOTache ajouterTacheVierge(
			EOEvenement evenement, 
			EOTypeTache type,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {

		EOTache f;
		try {
			f = EOTache.creerTacheVierge(
					evenement, 
					type, 
					utilisateur,
					evenement.editingContext());
		} 
		catch (ExceptionTacheCreation e) {
			throw new ExceptionOperationImpossible("Impossible de créer la tâche.", e);
		}
		
		return f;
	}
	/**
	 * <p>Cree et ajoute une tache d'alerte par mail (des personnes concernees) a un evenement.
	 * <p>NB: un evenement possede au maximum 1 tache d'alerte par mail pour l'instant.
	 * @param evenement EOEvenement auquel on veut ajouter une tache d'alerte par mail.
	 * @param triggerOffsetInMinutes Offset (decalage temporel) de declenchement souhaite par rapport a la date d'occurence de 
	 * l'evenement, en minutes.
	 * @return La tache creee.
	 * @throws ExceptionOperationImpossible
	 */
	public static EOTache ajouterTacheAlerteMail(
			EOEvenement evenement,
			Integer triggerOffsetInMinutes,
			EOPersonne utilisateur) {

		EOTache f;
			f = EOTache.creerTacheAlerteMail(
					evenement, 
					triggerOffsetInMinutes, 
					EOTache.ETAT_PROGRAMMATION_DEMANDEE, 
					utilisateur,
					evenement.editingContext());
		return f;
	}
	/**
	 * <p>Cree et ajoute  une nouvelle tache d'execution de tache d'application a un evenement.
	 * @param evenement EOEvenement auquel on veut associer la tache.
	 * @param description Description en francais de ce que fait la tache.
	 * @param nomClasseTacheApplication Nom complet de la classe de la tache a exécuter, <b>doit implementer l'interface {@link Job}</b> de Quartz et être dans le classpath de l'application.
	 * Ex: "org.cocktail.application.job.MonJob".
	 * @param sendMailAfterExecute Indique si un "accuse" d'execution doit etre envoye par mail aux personnes concernees par l'evenement
	 * a l'issu de l'execution de la tache externe.
	 * @param utilisateur Createur de la tache.
	 * @return La {@link EOTache} creee.
	 * @throws ExceptionTacheCreation
	 */
	public static EOTache ajouterTacheExecutionTacheApplication(
			EOEvenement evenement,
			String description,
			String nomClasseTacheApplication,
			boolean sendMailAfterExecute,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {

		EOTache f;
		try {
			f = EOTache.creerTacheExecutionTacheApplication(
					evenement, 
					description, 
//					tacheExterneUrlFichier, 
					nomClasseTacheApplication, 
					sendMailAfterExecute, 
					EOTache.ETAT_PROGRAMMATION_DEMANDEE, 
					utilisateur,
					evenement.editingContext());
		} 
		catch (ExceptionTacheCreation e) {
			throw new ExceptionOperationImpossible("Impossible de créer la tâche d'exécution d'une tâche d'application.", e);
		}
		
		return f;
	}
	/**
	 * <p>Cree et ajoute  une nouvelle tache d'execution de tache externe a un evenement.
	 * @return La {@link EOTache} creee.
	 * @deprecated remplacé par {@link #ajouterTacheExecutionTacheApplication(EOEvenement, String, String, boolean, EOPersonne)}
	 */
	@Deprecated
	public static EOTache ajouterTacheExecutionTacheExterne(
			EOEvenement evenement,
			String description,
			String tacheExterneUrlFichier,
			String tacheExterneClasse,
			boolean sendMailAfterExecute,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {

		EOTache f;
		try {
			f = EOTache.creerTacheExecutionTacheExterne(
					evenement, 
					description, 
					tacheExterneUrlFichier, 
					tacheExterneClasse, 
					sendMailAfterExecute, 
					EOTache.ETAT_PROGRAMMATION_DEMANDEE, 
					utilisateur,
					evenement.editingContext());
		} 
		catch (ExceptionTacheCreation e) {
			throw new ExceptionOperationImpossible("Impossible de créer la tâche d'exécution d'une tâche externe.", e);
		}
		
		return f;
	}
	
	/**
	 * <p>Modifie l'offset de declenchement et l'etat de la tache d'alerte par mail (des personnes concernees) d'un evenement. 
	 * Methode a utiliser par exemple pour demander la deprogrammation ou reprogrammation de cette tache (en preambule a l'appel
	 * de la methode {@link FwkCktlEvenementUtil#programmerTachesEvenement(EOEvenement)}).
	 * <p>NB: un evenement possede au maximum 1 tache d'alerte par mail.
	 * @param tache {@link EOTache} que l'on veut modifier.
	 * @param triggerOffsetInMinutes Offset (decalage temporel) de declenchement souhaite par rapport a la date d'occurence de 
	 * l'evenement, en minutes.
	 * @param etat Etat souhaite de la tache. Ex: {@link EOTache#ETAT_REPROGRAMMATION_DEMANDEE}.
	 * @throws ExceptionOperationImpossible 
	 */
	public static void modifierTacheAlerteMail(
			EOTache tache,
			Integer triggerOffsetInMinutes,
			String etat,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {

		if (tache != null) {
			try {
				EOTache.modifierTacheAlerteMail(tache, triggerOffsetInMinutes, etat, utilisateur);
			} 
			catch (ExceptionTacheModif e) {
				throw new ExceptionOperationImpossible("Impossible de modifier la tâche suivante: "+tache, e);
			}
		}
	}
	
	/**
	 * <p>Marque une tache comme devant etre supprimee. 
	 * <p>NB: La tache sera deprogrammee si necessaire puis supprimee du cote scheduler.
	 * @param tache Tache a supprimer
	 * @throws ExceptionOperationImpossible 
	 */
	public static void supprimerTache(
			EOTache tache,
			EOPersonne utilisateur) throws ExceptionOperationImpossible {
		
//		if (!getEditingContext().insertedObjects().containsObject(tache) && tacheProgrammee(tache)) {
//			throw new ExceptionOperationImpossible(
//					"La tâche à supprimer spécifiée est programmée, elle doit être d'abord déprogrammée.");
//		}
		
		try {
			EOTache.supprimerTache(tache, utilisateur);
		} 
		catch (ExceptionTacheSuppression e) {
			throw new ExceptionOperationImpossible("Impossible de supprimer la tâche suivante: "+tache, e);
		}
	}
	/**
	 * <p>Marque toutes les taches d'un evenement comme devant etre supprimees. 
	 * <p>NB: Chaque tache sera deprogrammee si necessaire puis supprimee du cote scheduler.
	 * @param evenement EOEvenement dont on veut supprimer toutes les taches.
	 * @throws ExceptionOperationImpossible 
	 */
	public static void supprimerTaches(
			EOEvenement evenement, EOPersonne utilisateur) throws ExceptionOperationImpossible {
		
		supprimerTaches(evenement.taches(), utilisateur);
	}
	/**
	 * <p>Marque des taches comme devant etre supprimees (par le serveur).
	 * <p>NB: Chaque tache sera deprogrammee si necessaire puis supprimee du cote scheduler.
	 * @param taches {@link EOTache}s a supprimer
	 * @throws ExceptionOperationImpossible 
	 */
	public static void supprimerTaches(
			NSArray taches, EOPersonne utilisateur) throws ExceptionOperationImpossible {
		
		NSArray objects = taches.immutableClone();
		for (int i=0; i<objects.count(); i++) {
			supprimerTache((EOTache) objects.objectAtIndex(i),utilisateur);
		}
	}
	
	/**
	 * <p>Realise la programmation de toutes les taches associee a un evenement.
	 * <p>La programmation, deprogrammation ou reprogrammation est decidee en fonction de l'etat courant des taches.
	 * @param evenement EOEvenement dont on veut programmer les taches.
	 * @throws ExceptionOperationImpossible
	 */
	@Deprecated
	public static void programmerTachesEvenement(EOEvenement evenement) throws ExceptionOperationImpossible {

		if (evenement == null)
			throw new IllegalArgumentException("EOEvenement requis.");
		if (evenement.taches() == null)
			throw new IllegalArgumentException("L'événement spécifié n'a aucune tâche associée.");
		if (evenement.editingContext().hasChanges())
			throw new IllegalStateException(
					"La programmation des tâches d'un événement ne peut se faire qu'après avoir " +
					"enregistré les modifications en cours.");

		if (evenement.taches().count() > 0) {
			
//			NSDictionary resultat = getFacadeScheduling().programmerTachesEvenement(evenement.evtIdReadOnly(), new NSTimestamp());
//			FwkCktlEvenementUtil.debugPrintMap("resultat de programmerTachesEvenement", resultat, FwkCktlEvenementUtil.fwkCktlEvtLog);

			// invalidates necessaires car des suppressions de taches ont pu etre faites cote serveur (apres deprogrammation eventuelle)
			FwkCktlEvenementUtil.invalidateObjects(evenement.editingContext(), new NSArray(evenement));
			FwkCktlEvenementUtil.invalidateObjects(evenement.editingContext(), evenement.taches());
			
			NSMutableArray<EOTache> tachesImpactees = new NSMutableArray<EOTache>();
//			for (int i = 0; i < resultat.count(); i++) {
//				String tachId = (String) resultat.allKeys().objectAtIndex(i);
//				try {
//					EOTache tache = EOTache.findWithTachId(new Integer(tachId),evenement.editingContext());
//					tachesImpactees.addObject(tache);
//				} 
//				catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
			FwkCktlEvenementUtil.debugPrintArray("tachesImpactees", tachesImpactees,FwkCktlEvenementUtil.LOG);
		}
	}

//    /**
//     * <p>Test si une tache est programmee actuellement dans le scheduler (cad si elle possede des triggers Quartz actifs).
//     * <p>NB: cela n'utilise pas l'attribut {@link Tache#etat()} mais recherche bien s'il existe des triggers Quartz actifs
//     * qui lui sont associes. 
//     * @param tache Tache concernee.
//     * @return <code>true</code> si elle a ete trouvee programmee dans le scheduler Quartz, <code>false</code> sinon.
//     * @throws ExceptionOperationImpossible
//     */
//    public boolean tacheProgrammee(Tache tache) throws ExceptionOperationImpossible {
//    	
//    	if (tache == null)
//    		throw new IllegalArgumentException("Tâche requise.");
//
//    	Boolean scheduled = getFacadeScheduling().tacheExisteDansScheduler(tache, true);
//    	
//    	return scheduled!=null ? scheduled.booleanValue() : false;
//    }
	
    
    /**
     * Recupere la liste de tous les triggers programmes existants dans le scheduler Quartz.
     * @return Une liste d'objets de type {@link Trigger}.
     * @throws ExceptionOperationImpossible
     */
	public static NSArray<Trigger> getCurrentTriggers() throws ExceptionOperationImpossible {
		return FwkCktlEvenementSchedulerUtil.triggersProgrammes(FwkCktlEvenementPrincipal.instance().getScheduler());
	}

}
