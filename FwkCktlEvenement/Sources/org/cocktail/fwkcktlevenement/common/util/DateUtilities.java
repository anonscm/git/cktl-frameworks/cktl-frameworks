/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;


/**
 * <p>Classe utilitaire autour des dates...
 * 
 * <p>ATTENTION: Les NSTimestamp n'ont "aucun" NSTimezone associe, ils travaillent toujours en UTC (cad GMT).
 * Instancier un NSTimestamp avec en parametre un time zone ne fait que "decaler" la valeur initiale (consideree comme
 * etant en GMT) selon ce time zone. Meme si aucun time zone n'est specifie a la creation d'un NSTimestamp, c'est 
 * NSTimezone.defaultTimeZone() qui est utilise.<br>
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2006.
 */
public class DateUtilities 
{

    static public UniteTemps Minutes = new UniteTemps("Minutes", Calendar.MINUTE);
    static public UniteTemps Heures = new UniteTemps("Heures", Calendar.HOUR);
    static public UniteTemps Jours = new UniteTemps("Jours", Calendar.DAY_OF_MONTH);
    static public UniteTemps Semaines = new UniteTemps("Semaines", Calendar.WEEK_OF_MONTH);
    
    static public NSArray<UniteTemps> Unites = new NSArray<UniteTemps>(
            Minutes, Heures, Jours, Semaines
    );
    
    static public class UniteTemps {
        
        String label;
        Integer code;
        
        public UniteTemps(String label, Integer code) {
            super();
            this.label = label;
            this.code = code;
        }
        
        public String getLabel() {
            return label;
        }
        
        public Integer getCode() {
            return code;
        }
        
    }
    
	/**
	 * Retourne la representation litterale lisible d'une date.
	 * @param date La date.
	 * @param includeTime <code>true</code>, pour inclure l'heure sous la forme "HH/mm/ss" dans la representation.
	 * @param includeDayOfWeek <code>true</code>, pour inclure le jour de la semaine abrege.
	 * @return La representation litterale lisible de la date specifie. Ex: "09/11/2006 - 11:34:53".
	 */
	static public String print(final Date date, final boolean includeTime, boolean includeDayOfWeek) {
		StringBuffer format = new StringBuffer("dd/MM/yyyy");
		if (includeDayOfWeek)
			format.insert(0, "E ");
		if (includeTime)
			format.append(" - HH:mm:ss");
		return new SimpleDateFormat(format.toString()).format(date);
	}
	/**
	 * Retourne la representation litterale d'une date pour l'utiliser dans un nom de fichier par exemple.
	 * @param ts La date.
	 * @param includeTime true, pour inclure l'heure sous la forme "HHmmss" dans la representation.
	 * @return La representation litterale de la date specifie. Ex: "20061109_113453" pour "09/11/2006 - 11:34:53".
	 */
	static public String printForFileName(final Date ts, final boolean includeTime) {
		if (ts == null) return "NULL";
		
		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(ts);

		StringBuffer buf = new StringBuffer();
		buf.append(new SimpleDateFormat("yyyyMMdd").format(myCalendar.getTime()));
		if (includeTime) {
			buf.append("_");
			buf.append(new SimpleDateFormat("HHmmss").format(myCalendar.getTime()));
		}
		return buf.toString();
	}
	/**
	 * <p>Ajouter un laps de temps a une date.
	 * 
	 * @param date Date a laquelle ajouter le laps de temps
	 * @param valeur Valeur du laps de temps (ex: 1, -3)
	 * @param unite Unite du laps de temps (ex: Calendar.MONTH, Calendar.DATE)
	 * 
	 * @return La date passee en parametre + le laps de temps specifie. (NB: 31/01/2006 + 1 mois = 28/02/2006)
	 */
	static public NSTimestamp decalerTimestamp(final NSTimestamp date, final int valeur, final int unite) {
		if (date == null) return null;
		
		NSTimestamp ts;
		
		switch (unite) {
			case Calendar.SECOND:
				ts = date.timestampByAddingGregorianUnits(0, 0, 0, 0, 0, valeur);
				break;
	
			case Calendar.MINUTE:
				ts = date.timestampByAddingGregorianUnits(0, 0, 0, 0, valeur, 0);
				break;
	
			case Calendar.HOUR:
			case Calendar.HOUR_OF_DAY:
				ts = date.timestampByAddingGregorianUnits(0, 0, 0, valeur, 0, 0);
				break;
	
			case Calendar.DATE: // similar to DAY_OF_MONTH
			case Calendar.DAY_OF_WEEK:
			case Calendar.DAY_OF_YEAR:
				ts = date.timestampByAddingGregorianUnits(0, 0, valeur, 0, 0, 0);
				break;
				
			case Calendar.WEEK_OF_MONTH:
				ts = date.timestampByAddingGregorianUnits(0, 0, 0, 0, toMinutes(valeur, Calendar.WEEK_OF_MONTH), 0);
				break;
	
			case Calendar.MONTH:
				ts = date.timestampByAddingGregorianUnits(0, valeur, 0, 0, 0, 0);
				break;
	
			case Calendar.YEAR:
				ts = date.timestampByAddingGregorianUnits(valeur, 0, 0, 0, 0, 0);
				break;
				
			default:
				throw new IllegalArgumentException("Unité non supportée: "+unite);
		}
		
//		System.out.println("decaler(NSTimestamp)>>  "+print(date,true)+" + "+valeur+" "+unite+" --> "+print(ts, true));
		return ts;
	}
	/**
	 * <p>Ajouter un laps de temps a une date.
	 * 
	 * @param date Date a laquelle ajouter le laps de temps
	 * @param valeur Valeur du laps de temps (ex: 1, -3)
	 * @param unite Unite du laps de temps (ex: Calendar.MONTH, Calendar.DATE)
	 * 
	 * @return La date passee en parametre + le laps de temps specifie. (NB: 31/01/2006 + 1 mois = 28/02/2006)
	 */
	static public Date decaler(final Date date, final int valeur, final int unite) {
		if (date == null) return null;
		if (valeur == 0) return date;
		Calendar startDate = GregorianCalendar.getInstance();
		startDate.setTime(date);
		startDate.add(unite, valeur);
		Date d = startDate.getTime();
		return d;
	}
//	static public NSTimestamp decaler(final NSTimestamp date, final int valeur, final int unite) {
//		if (date == null) return null;
//		
//		Calendar startDate;
////		if (date instanceof NSTimestamp)
////			startDate = new GregorianCalendar(NSTimeZone.getGMT());
////		else
//			startDate = GregorianCalendar.getInstance();
//		
//		startDate.setTime(date);
//		startDate.add(unite, valeur);
//		NSTimestamp d = new NSTimestamp(startDate.getTime());
//		
////		System.out.println("decaler(Date)>>  "+print(date,true)+" + "+valeur+" "+unite+" --> "+print(date, true));
//		return d;
//	}

	/**
	 * <p>Calcul l'ecart de temps qu'il existe entre deux dates, dans l'unite precisee.
	 * <p>Exemples :
	 * <ul>
	 * <li><code>ecartEntreDates(05/09/2006, 12/05/2007, Calendar.MONTH)</code> renvoie <code>-8</code></li>
	 * <li><code>ecartEntreDates(05/09/2006, 01/05/2007, Calendar.MONTH)</code> renvoie <code>-7</code></li>
	 * <li><code>ecartEntreDates(10/09/2006, 12/09/2006, Calendar.MONTH)</code> renvoie <code>-8</code></li>
	 * <li><code>ecartEntreDates(03/10/2006, 12/09/2006, Calendar.DATE)</code> renvoie <code>21</code></li>
	 * </ul>
	 * 
	 * @param date Date pour laquelle on veut calculer l'ecart.
	 * @param dateDeReference Date par rapport a laquelle est calcule l'ecart de temps.
	 * @param unite Unite souhaitee : Soit Calendar.YEAR, soit Calendar.MONTH soit Calendar.DATE.
	 * 
	 * @return L'ecart trouve entre les deux dates, positif ou negatif.
	 */
	static public int ecart(
			final NSTimestamp date, 
			final NSTimestamp dateDeReference, 
			final int unite) 
	throws IllegalArgumentException {
		if (date==null || dateDeReference==null)
			throw new IllegalArgumentException("Arguments null interdits.");
		if (unite!=Calendar.YEAR && unite!=Calendar.MONTH && unite!=Calendar.DATE)
			throw new IllegalArgumentException("Arguments autoris\u00E9s : Calendar.YEAR, Calendar.MONTH ou Calendar.DATE");
			
		// date inferieure
		Calendar d = GregorianCalendar.getInstance();
//		d.clear();
		d.setTime(dateDeReference.after(date) ? date : dateDeReference);
//		d.setTime(d.getTime());// contournement du bug de Calendar.setTime()

		// date superieure
		Calendar ref = GregorianCalendar.getInstance();
//		ref.clear();
		ref.setTime(dateDeReference.after(date) ? dateDeReference : date);
//		ref.setTime(ref.getTime());// contournement du bug de Calendar.setTime()
		
		// calcul difference
		DateDiff diff = new DateDiff(d, ref);
		diff.calculateDifference();
		int ecart = diff.getFieldOnly(unite);
		
		// signe de la difference
		int ecartSigne = dateDeReference.after(date) ? ecart : ecart*-1;

//		System.out.println("ecart()>>  DateDiff.diff = "+diff.toString());
//		System.out.println("ecart()>>  "+print(date,true)
//				+" - "+print(dateDeReference,true)+" = "+ecartSigne);
		
		return ecartSigne;
	}
	
	/**
	 * Determine le premier jour du mois specifie.
	 * 
	 * @param mois Mois sous la forme "mm/yyyy". Ex: 03/2006 ou 3/2006 pour mars 2006
	 * 
	 * @return Un NSTimestamp egal au premier jour du mois, 0:00:00
	 */
	static public NSTimestamp premierJourDuMois(final String mois) {
		NSTimestampFormatter formatterPeriode = new NSTimestampFormatter("%m/%Y");
		NSTimestamp periode = (NSTimestamp) formatterPeriode.parseObjectInUTC(mois, new ParsePosition(0));
		
		if (periode != null) {
			Calendar dateDeb = GregorianCalendar.getInstance();
//			dateDeb.clear();
			dateDeb.setTime(periode);
//			dateDeb.setTime(dateDeb.getTime());//contournement du bug de Calendar.setTime()
			NSTimestamp debut = new NSTimestamp(dateDeb.getTime());
			
//			System.out.println("getPremierJourDuMois()>>  resultat = "+print(debut,true));
			return debut;
		}
		return null;
	}
	/**
	 * Determine le premier jour du mois specifie, a l'heure specifiee.
	 * @param mois Mois sous la forme "mm/yyyy". Ex: 03/2006 ou 3/2006 pour mars 2006
	 * @param heures 
	 * @param minutes 
	 * @param secondes 
	 * @return Un NSTimestamp egal au premier jour du mois, a l'heure specifiee.
	 * @see #premierJourDuMois(String)
	 */
	static public NSTimestamp premierJourDuMois(final String mois, int heures, int minutes, int secondes) {
		NSTimestamp date = premierJourDuMois(mois);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(GregorianCalendar.HOUR_OF_DAY, heures);
		cal.set(GregorianCalendar.MINUTE, minutes);
		cal.set(GregorianCalendar.SECOND, secondes);
		cal.set(GregorianCalendar.MILLISECOND, 0);
		return new NSTimestamp(cal.getTime());
	}
	
	/**
	 * Determine le dernier jour du mois specifie.
	 *
	 * @param mois Mois sous la forme "mm/yyyy". Ex: 03/2006 ou 3/2006 pour mars 2006
	 * 
	 * @return Un NSTimestamp egal au dernier jour du mois, 23:59:59
	 */
	static public NSTimestamp dernierJourDuMois(final String mois) {
		NSTimestampFormatter formatterPeriode = new NSTimestampFormatter("%m/%Y");
		NSTimestamp periode = (NSTimestamp) formatterPeriode.parseObjectInUTC(mois, new ParsePosition(0));
		
		if (periode != null) {
			Calendar dateFin = GregorianCalendar.getInstance(/*NSTimeZone.defaultTimeZone()*/);
//			dateFin.clear();
			dateFin.setTime(periode);
//			dateFin.setTime(dateFin.getTime());//contournement du bug de Calendar.setTime()
			dateFin.add(Calendar.MONTH, 1);
			dateFin.add(Calendar.SECOND, -1);
			NSTimestamp fin = new NSTimestamp(dateFin.getTime());
			
//			System.out.println("dernierJourDuMois()>>  resultat = "+print(fin,true));
			return fin;
		}
		return null;
	}
	/**
	 * Determine le dernier jour du mois specifie, a l'heure specifiee.
	 * @param mois Mois sous la forme "mm/yyyy". Ex: 03/2006 ou 3/2006 pour mars 2006
	 * @param heures 
	 * @param minutes 
	 * @param secondes 
	 * @return Un NSTimestamp egal au dernier jour du mois, a l'heure specifiee.
	 * @see #dernierJourDuMois(String)
	 */
	static public NSTimestamp dernierJourDuMois(final String mois, int heures, int minutes, int secondes) {
		NSTimestamp date = dernierJourDuMois(mois);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(GregorianCalendar.HOUR_OF_DAY, heures);
		cal.set(GregorianCalendar.MINUTE, minutes);
		cal.set(GregorianCalendar.SECOND, secondes);
		cal.set(GregorianCalendar.MILLISECOND, 0);
		return new NSTimestamp(cal.getTime());
	}
	
	/**
	 * Fournit une nouvelle date a partir de celle specifiee, mais dont l'heure est mise a 0h0m0s0ms.
	 * NB: Le time zone de la date retournee est le meme que celui de la date passee en parametre.
	 * @param date Date de reference.
	 * @return La nouvelle date.
	 */
	static public NSTimestamp premiereHeure(final NSTimestamp date) {
		if (date==null) return null;

		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(date); // le tz du ts est pris en compte
		myCalendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
		myCalendar.set(GregorianCalendar.MINUTE, 0);
		myCalendar.set(GregorianCalendar.SECOND, 0);
		myCalendar.set(GregorianCalendar.MILLISECOND, 0);
		NSTimestamp ts = new NSTimestamp(myCalendar.getTime()); // le tz du nouveau ts sera le meme qu'en entree  

//		System.out.println("premiereHeure()>>  "+print(date,true)
//				+" --> "+print(ts,true));
		return ts;
		
//		Calendar cal = GregorianCalendar.getInstance();
//		cal.clear();
//		cal.setTime(date);
//		cal.setTime(cal.getTime());//contournement d'un bug
//		cal.set(Calendar.HOUR_OF_DAY, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 0);
//		cal.set(Calendar.MILLISECOND, 0);
//		NSTimestamp ts = new NSTimestamp(cal.getTime().getTime());//, NSTimeZone.timeZoneWithName("GMT", true));
	}
	
	/**
	 * Fournit une nouvelle date a partir de celle specifiee, mais dont l'heure est mise a 23h59m59s999ms.
	 * NB: Le time zone de la date retournee est le meme que celui de la date passee en parametre.
	 * @param date Date de reference.
	 * @return La nouvelle date.
	 */
	static public NSTimestamp derniereHeure(final NSTimestamp date) {
		if (date==null) return null;

		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(date); // le tz du ts est pris en compte
		myCalendar.set(GregorianCalendar.HOUR_OF_DAY, 23);
		myCalendar.set(GregorianCalendar.MINUTE, 59);
		myCalendar.set(GregorianCalendar.SECOND, 59);
		myCalendar.set(GregorianCalendar.MILLISECOND, 999);
//		myCalendar.add(Calendar.DAY_OF_MONTH, 1);
//		myCalendar.add(Calendar.MILLISECOND, -1);
		NSTimestamp ts = new NSTimestamp(myCalendar.getTime()); // le tz du nouveau ts sera le meme qu'en entree

//		System.out.println("derniereHeure()>>  "+print(date,true,true)
//				+" --> "+print(ts,true,true));
		return ts;  

//		Calendar cal = GregorianCalendar.getInstance();
//		cal.clear();
//		cal.setTime(date);
//		cal.setTime(cal.getTime());//contournement d'un bug
//		cal.set(Calendar.HOUR_OF_DAY, 23);
//		cal.set(Calendar.MINUTE, 59);
//		cal.set(Calendar.SECOND, 59);
//		cal.set(Calendar.MILLISECOND, 999);
//		NSTimestamp ts = new NSTimestamp(cal.getTime().getTime());//, NSTimeZone.timeZoneWithName("GMT", true));
	}
	
	/**
	 * Traduit en francais une duree exprimee en minutes. Unites gerees: minute, heure, jour, semaine.
	 * @param valeurUniteOrdreEnMinutes Duree positive ou negative exprimee en minutes.
	 * @param inclureAvantApres Indique si "avant ou "apres" doit etre indique dans le resultat. 
	 * @return Exemple: -5*24*60 minutes --> "5 jour(s) avant"; 7*60 minutes --> "7 minutes(s) apres"
	 */
	public static String traduireDuree(int valeurUniteOrdreEnMinutes, boolean inclureAvantApres) {

		String valeur, unite, ordre;

		int val = valeurUniteOrdreEnMinutes;

		if (val < 0) {
			ordre = "avant";
			val = -val;
		}
		else
			ordre = "aprés";

		if (val < 60) {
			valeur = ""+val;
			unite = "minute(s)";
		}
		else {
			val = val / 60; // en heures

			if (val < 24) {
				valeur = ""+val;
				unite = "heure(s)";
			}
			else {
				val = val / 24; // en jours

				if (val < 7) {
					valeur = ""+val;
					unite = "jour(s)";
				}
				else {
					val = val / 7; // en semaines

					valeur = ""+val;
					unite = "semaine(s)";
				}
			}
		}
		return valeur+" "+unite+(inclureAvantApres? " "+ordre : "");
	}


	public static int toMinutes(int nb, int unite) {
	    int result = 0;
	    switch (unite) {
	    case Calendar.MINUTE:
	        result = nb;
	        break;
	    case Calendar.HOUR:
	    case Calendar.HOUR_OF_DAY:
	        result = nb * 60;
	        break;
	    case Calendar.DAY_OF_MONTH:
	    case Calendar.DAY_OF_WEEK:
	        result = nb * 60 * 24;
	        break;
	    case Calendar.WEEK_OF_YEAR:
	    case Calendar.WEEK_OF_MONTH:
	    	result = nb * 7 * 24 * 60;
	    	break;
	    default:
	        throw new IllegalArgumentException("Unité non supportée: "+unite);
	    }
	    return result;
	}
	
	/** 
	 * This class determines the difference between 2 dates not including the dates themselves, i.e. inclusive 
	 * passed as java.utilCalendar.
	 * <p>Owner : Niraj Agarwal
	 * 
	 * <p>Modif : Bertrand Gauthier. Duree comprise entre 2 dates, en incluant ces deux dates.
	 * <p>Exemple: 
	 * <ul> <li>[01/01/2005 --> 31/12/2005] 	<--> 	1 an,		0 mois,		0 jour</li>
	 * 		<li>[01/02/2005 --> 31/03/2005] 	<--> 	0 an,		2 mois,		0 jour</li>
	 * 		<li>[01/01/2005 --> 01/04/2005]		<-->	0 an,		3 mois,		1 jour</li>
	 * 		<li>[11/03/2006 --> 11/03/2006] 	<--> 	0 an,		0 mois,		1 jour</li>
	 * 		<li>[11/03/2005 --> 11/03/2006] 	<--> 	1 an,		0 mois,		1 jour</li>	</ul>
	 */
	static public class DateDiff  
	{
		// The year difference between passed dates
		private int yearDiff = 0;
		// The month difference between passed dates, excluding years
		private int monthDiff = 0;
		// The day difference between passed dates, excluding years and months
		private int dayDiff = 0;
		// Total day difference between passed dates, including years and months 
		private int dayOnly = 0;
		
		// Flag pour affichage
		private boolean nice = false;
		
		private Calendar startDate = null;
		private Calendar endDate = null;
		
		// 1 jour en millisecondes = 24 x 60 x 60 x 1000
		private static final long DAY = 86400000;
		
		/**
		 * Constructeur
		 * @param pStartDate Date de debut sous forme "dd/MM/yyyy"
		 * @param pEndDate Date de fin sous forme "dd/MM/yyyy"
		 * @throws NumberFormatException Si le parseInt echoue
		 */
		public DateDiff(String pStartDate, String pEndDate) throws NumberFormatException {
			startDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			endDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			
			startDate.clear();
			endDate.clear();
			
			startDate.set(
				Integer.parseInt(pStartDate.substring(6)), Integer.parseInt(pStartDate.substring(3,5)), Integer.parseInt(pStartDate.substring(0,2)));
			endDate.set(
				Integer.parseInt(pEndDate.substring(6)), Integer.parseInt(pEndDate.substring(3,5)), Integer.parseInt(pEndDate.substring(0,2)));
			
			//to include the date themselves
			startDate.add(Calendar.DATE, -1);
		}
		
		public DateDiff(Calendar pStartDate, Calendar pEndDate)
		{
			startDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			endDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			
			startDate.clear();
			endDate.clear();
			
			startDate.set(pStartDate.get(Calendar.YEAR), pStartDate.get(Calendar.MONTH), pStartDate.get(Calendar.DATE));
			endDate.set(pEndDate.get(Calendar.YEAR), pEndDate.get(Calendar.MONTH), pEndDate.get(Calendar.DATE));

			//to include the date themselves
			startDate.add(Calendar.DAY_OF_YEAR, -1);
		}
		
		/**
		 * Calule la difference entre les deux dates (incluses dans la difference)
		 */
		public void calculateDifference()
		{
			if( startDate == null || endDate == null || startDate.after(endDate) )
				return;
			
			dayOnly = (int) ((endDate.getTimeInMillis() - startDate.getTimeInMillis()) / DAY);
			
			yearDiff = endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR);
			
			boolean bYearAdjusted = false;
			startDate.add(Calendar.YEAR, yearDiff);
			if( startDate.after(endDate) )
			{
				bYearAdjusted = true;
				startDate.add(Calendar.YEAR, -1 );
				yearDiff--;
			}
			
			monthDiff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH);
			if( bYearAdjusted && monthDiff <= 0 )
				monthDiff = 12 + monthDiff;
			
			startDate.add(Calendar.MONTH, monthDiff);
			if( startDate.after(endDate) )
			{
				startDate.add(Calendar.MONTH, -1 );
				monthDiff--;
			}
			
			dayDiff = endDate.get(Calendar.DAY_OF_YEAR) - startDate.get(Calendar.DAY_OF_YEAR);
			if( dayDiff < 0 )
				dayDiff = 365 + dayDiff;
			
			startDate.add(Calendar.DAY_OF_YEAR, dayDiff);
		}
		
		/**
		 * Accesseur des resultats du calcul de la difference
		 * @param field Calendar.DATE, Calendar.MONTH ou Calendar.YEAR
		 */
		public int getField(final int field) throws IllegalArgumentException {
			if (field!=Calendar.YEAR && field!=Calendar.MONTH && field!=Calendar.DATE)
				throw new IllegalArgumentException("Arguments autoris\u00E9s : Calendar.YEAR, Calendar.MONTH ou Calendar.DATE");
		
			switch (field) {
				case Calendar.DATE:
					return dayDiff;
				case Calendar.MONTH:
					return monthDiff;
				case Calendar.YEAR:
					return yearDiff;
				default:
					return 0;
			}
		}
		
		/**
		 * Accesseur des resultats du calcul de la difference
		 * @param field Calendar.DATE, Calendar.MONTH ou Calendar.YEAR
		 */
		public int getFieldOnly(final int field) throws IllegalArgumentException {
			if (field!=Calendar.YEAR && field!=Calendar.MONTH && field!=Calendar.DATE)
				throw new IllegalArgumentException("Arguments autoris\u00E9s : Calendar.YEAR, Calendar.MONTH ou Calendar.DATE");
		
			switch (field) {
				case Calendar.DATE:
					return getDayOnly();
				case Calendar.MONTH:
					return getMonthOnly();
				case Calendar.YEAR:
					return getYearOnly();
				default:
					return 0;
			}
		}
		
		/**
		 * Retourne le nombre d'annee. Cad 1 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getYear() {
			return yearDiff;
		}
		/**
		 * Retourne le nombre de mois. Cad 7 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getMonth() {
			return monthDiff;
		}
		/**
		 * Retourne le nombre de jours. Cad 12 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getDay() {
			return dayDiff;
		}

		/**
		 * Retourne la difference en nombre de jours.
		 */
		public int getDayOnly() {
			return dayOnly;
		}
		/**
		 * Retourne la difference en nombre de mois entiers.
		 * Cad 19 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getMonthOnly() {
			return yearDiff*12 + monthDiff;
		}
		/**
		 * Retourne la difference en nombre d'annees entieres.
		 * C'est la meme chose que getYear().
		 */
		public int getYearOnly() {
			return getYear();
		}
		
		
		
		//--
		/**
		 * La duree sous forme affichable complete.
		 * <p>Exemples :	
		 * <ul>
		 * 	<li>12ans 4mois 12j</li>
		 * 	<li>6mois</li>
		 * 	<li>1an 15j</li>
		 * 	<li>1j</li>
		 * </ul>
		 */
		public String toString() {
			StringBuffer buf = new StringBuffer();
			if (getYear() > 0) {
				buf.append(new Integer(getYear()).toString());
				if (getYear()==1)
					buf.append(nice ? " an" : "an");
				else
					buf.append(nice ? " ans" : "ans");
			}
			if (getMonth() > 0) {
				buf.append(getYear()>0 ? " " : "");
				buf.append(new Integer(getMonth()).toString());
				buf.append(nice ? " mois" : "mois");
			}
			if (getDay() > 0) {
				buf.append(getYear()>0 || getMonth()>0 ? " " : "");
				buf.append(new Integer(getDay()).toString());
				if (getDay() == 1)
					buf.append(nice ? " jour" : "j");
				else
					buf.append(nice ? " jours" : "j");
			}
			return buf.length()>0 ? buf.toString() : (nice ? "0 jour" : "0j");
		}
		
//		/**
//		 * Test de la classe
//		 * @param args
//		 */
//		public static void main(String args[])
//		{
//			if( args.length != 2 )
//			{
//				System.out.println("\nInvalid usage : ");
//				System.out.println(" java DateDiff [startDate - dd/mm/yyyy] [endDate - dd/mm/yyyy]\n");
//				return;
//			}
//			
//			Calendar sDate = Calendar.getInstance();
//			Calendar eDate = Calendar.getInstance();
//			
//			sDate.set(Integer.parseInt(args[0].substring(6)), Integer.parseInt(args[0].substring(3,5)), Integer.parseInt(args[0].substring(0,2)));
//			eDate.set(Integer.parseInt(args[1].substring(6)), Integer.parseInt(args[1].substring(3,5)), Integer.parseInt(args[1].substring(0,2)));
//			
//			SimpleDateFormat xlsDateFormater = new SimpleDateFormat("dd/MM/yyyy");
//
//			DateDiff dateDiff = new DateDiff(sDate, eDate);
//			//DateDiff dateDiff = new DateDiff(xlsDateFormater.format(sDate.getTime()), xlsDateFormater.format(eDate.getTime()));
//			dateDiff.calculateDifference();
//			
//			System.out.println("\nStart Date : "+xlsDateFormater.format(sDate.getTime()));
//			System.out.println("End Date   : "+xlsDateFormater.format(eDate.getTime()));
//			
//			System.out.println("\nDateDiff : "+dateDiff.getYear()+" Year(s), "+dateDiff.getMonth()+" Month(s), "+dateDiff.getDay()+" Day(s)    ["+dateDiff.getDayOnly()+" days(s)]\n");
//		}
	}
	
	/**
	 * Point d'entree pour tests.
	 * @param arguments
	 * @throws ParseException 
	 */
	static public void main(String[] arguments) throws ParseException {

		NSTimestamp ts = (NSTimestamp) new NSTimestampFormatter("%d/%m/%Y %H:%M:%S").parseObject("18/03/2008 13:22:00");
		System.out.println("DateUtilities.main() ts = "+ts);
//		NSTimeZone.setDefaultTimeZone(NSTimeZone.getGMT());
		
//		NSTimestamp ts = new NSTimestamp(DateFormat.getDateInstance(DateFormat.SHORT).parse("1/12/2006"));
		Date d = DateFormat.getDateInstance(DateFormat.SHORT).parse("11/03/2006");
		
//		System.out.println("DateOperation.main() d = "+d);
//		System.out.println("DateOperation.main() "+DateOperation.print(d, true));
		//Debug.printDate(DateOperation.decalerDate(new Date(), -60, Calendar.MINUTE));
		//Debug.printDate(DateOperation.decalerDate(new NSTimestamp(), -60, Calendar.MINUTE));

		System.out.println();
		
//			System.out.println("NSTimeZone.defaultTimeZone() : "+NSTimeZone.defaultTimeZone());
//			NSTimestamp myNSTimestamp = new NSTimestamp();
//			System.out.println("new NSTimestamp() : "+myNSTimestamp);
//			GregorianCalendar myCalendar = new GregorianCalendar();
//			System.out.println("new GregorianCalendar() : "+myCalendar);
//			System.out.println("myCalendar.getTimeZone() : "+myCalendar.getTimeZone().getID());
//			myCalendar.setTime(myNSTimestamp);
//			System.out.println("setTime myCalendar : "+myCalendar);
//
//			Date date = new Date();
//			System.out.println("DateOperation.main() date = "+date);
//			System.out.println("DateOperation.main() variable = "+DateOperation.decaler(date, 1, Calendar.HOUR));
			
		System.out.println("DateOperation.main() "+new NSTimestampFormatter("dd/MM/yyyy - HH:mm:ss").format(ts));
		System.out.println();
		
		NSTimestamp t = new NSTimestamp();
		System.out.println(t);
		System.out.println(new Date(t.getTime()));
		
	}
	
//	/**
//	 * <p>Cree une nouvelle date a partir d'une autre, dans le time zone specifie.
//	 * 
//	 * @param date Date de reference.
//	 * @param tzName Libelle du time zone demande (ex: "Europe/Paris", "GMT"). Si null ou vide, le time zone par defaut
//	 * est utilise (NSTimeZone.defaultTimeZone()).
//	 * 
//	 * @return La nouvelle date dans le time zone adequat.
//	 */
//	static public NSTimestamp dateWithTimeZone(final NSTimestamp date, final String tzName) {
//		return dateWithTimeZone(date, NSTimeZone.timeZoneWithName(tzName, true));
//	} 
//	/**
//	 * <p>Cree une nouvelle date a partir d'une autre, dans le time zone specifie.
//	 * 
//	 * @param date Date de reference.
//	 * @param tzName Time zone demande. Si null, le time zone par defaut est utilise (NSTimeZone.defaultTimeZone()).
//	 * 
//	 * @return La nouvelle date dans le time zone adequat.
//	 */
//	static public NSTimestamp dateWithTimeZone(final NSTimestamp date, final NSTimeZone timezone) {
//		if (date == null) return null;
//		NSTimeZone tz = NSTimeZone.defaultTimeZone();
//		if (timezone != null) tz = timezone;
//		return new NSTimestamp(date.getTime(), tz);
//	} 
//
//	/**
//	 * <p>Retourne sous forme de chaine de caracteres une date dans le time zone et le format specifies.
//	 * 
//	 * @param date Date voulue..
//	 * @param tzName Libelle du time zone demande (ex: "Europe/Paris", "GMT"). Si null ou vide, le time zone par defaut.
//	 * @param format Libelle du format demande (ex: "%d/%m/%Y" pour avoir un truc du genre 05/06/2006).
//	 * 
//	 * @return La date sous forme de chaine de caracteres, dans le time zone et le format voulus. Ou "" en cas de probleme.
//	 */
//	static public String dateWithTimeZoneAndFormat(final NSTimestamp date, final String tzName, final String format) {
//		NSTimestampFormatter f = new NSTimestampFormatter(format);
//		NSTimestamp ts = dateWithTimeZone(date, tzName);
//		if (ts == null) return "null";
//		return f.format(ts);
//	}
//	/**
//	 * <p>Retourne sous forme de chaine de caracteres une date dans le time zone et le format specifies.
//	 * 
//	 * @param date Date voulue..
//	 * @param tzName Time zone demande. Si null, le time zone par defaut
//	 * @param format Libelle du format demande (ex: "%d/%m/%Y" pour avoir un truc du genre 05/06/2006).
//	 * 
//	 * @return La date sous forme de chaine de caracteres, dans le time zone et le format adequat. Ou "null" en cas de probleme.
//	 */
//	static public String dateWithTimeZoneAndFormat(final NSTimestamp date, final NSTimeZone timezone, final String format) {
//		NSTimestampFormatter f = new NSTimestampFormatter(format);
//		NSTimestamp ts = dateWithTimeZone(date, timezone);
//		if (ts == null) return "null";
//		return f.format(ts);
//	}
//
//	/**
//	 * <p>Retourne sous forme de chaine de caracteres une date dans le time zone specifie, au format 05/06/2006.
//	 * 
//	 * @param date Date concernee.
//	 * @param timezoneName Libelle du time zone demande (ex: "Europe/Paris", "GMT").
//	 * 
//	 * @return La date sous forme de chaine de caracteres.
//	 */
//	public static String dateWithTimeZoneAndShortFormat(final NSTimestamp date, final String timezoneName) {
//		return dateWithTimeZoneAndFormat(date, timezoneName, "%d/%m/%Y");
//	}
//	public static String dateWithTimeZoneAndShortFormat(final NSTimestamp date, final NSTimeZone timezone) {
//		return dateWithTimeZoneAndFormat(date, timezone, "%d/%m/%Y");
//	}
//	public static String dateWithTimeZoneAndLongFormat(final NSTimestamp date, final String timezoneName) {
//		return dateWithTimeZoneAndFormat(date, timezoneName, "%d/%m/%Y - %H:%M:%S - %Z %z");
//	}
//	public static String dateWithTimeZoneAndLongFormat(final NSTimestamp date, final NSTimeZone timezone) {
//		return dateWithTimeZoneAndFormat(date, timezone, "%d/%m/%Y - %H:%M:%S - %Z %z");
//	}
}
