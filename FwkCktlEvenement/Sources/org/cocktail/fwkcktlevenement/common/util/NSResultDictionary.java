/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * <p>Classe utile pour encapsuler le resultat d'un traitement dont on ne peut recuperer 
 * directement l'exception eventuellement rencontree.
 * <p>Ce "resultat" est un dictionnaire contenant soit un objet resultat, 
 * soit les infos necessaires pour reconstruire en partie l'exception rencontree.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2008
 */
public class NSResultDictionary extends NSDictionary 
{
	public static final String VALUE_FOR_RESULT_NULL_AND_OK = 	"NULL_AND_OK";
	public static final String VALUE_FOR_RESULT_OK = 			"OK";
	
	public static final String KEY_FOR_RETURN_VALUE = "returnValue";
	public static final String KEY_FOR_EXCEPTION_MESSAGE = "exceptionMessage";
	public static final String KEY_FOR_EXCEPTION_CLASS_NAME = "exceptionClassName";
	

	/**
	 * Cree un nouveau dictionnaire de resultat a partir d'un dictionnaire classique.
	 * @param dictionary Dictionnaire devant contenir soit une cle {@link NSResultDictionary#KEY_FOR_RETURN_VALUE},
	 * soit les deux cles {@link NSResultDictionary#KEY_FOR_EXCEPTION_CLASS_NAME} et {@link NSResultDictionary#KEY_FOR_EXCEPTION_MESSAGE}.
	 * @return Une nouvelle instance de {@link NSResultDictionary}.
	 */
	public static NSResultDictionary newInstanceFromDictionary(NSDictionary dictionary) {
		
		if (dictionary == null)
			throw new IllegalArgumentException("Un dictionnaire doit être fourni.");
		
		if (dictionary.allKeys().containsObject(KEY_FOR_RETURN_VALUE) && 
				(dictionary.allKeys().containsObject(KEY_FOR_EXCEPTION_MESSAGE) || 
				dictionary.allKeys().containsObject(KEY_FOR_EXCEPTION_CLASS_NAME)))
			throw new IllegalArgumentException("Le dictionnaire ne doit pas contenir à la fois un resultat et une exception.");
		
		if (!dictionary.allKeys().containsObject(KEY_FOR_RETURN_VALUE) && 
				!dictionary.allKeys().containsObject(KEY_FOR_EXCEPTION_MESSAGE) && 
				!dictionary.allKeys().containsObject(KEY_FOR_EXCEPTION_CLASS_NAME))
			throw new IllegalArgumentException("Le dictionnaire spécifié ne contient ni resultat ni exception.");
			
		return new NSResultDictionary(dictionary);
	}
	
	/**
	 * Cree un nouveau dictionnaire de resultat avec l'objet resultat specifie.
	 * @param o Objet resultat, evetuellement <code>null</code>. 
	 * Sera associe a la cle {@link NSResultDictionary#KEY_FOR_RETURN_VALUE} dans le dictionnaire.
	 * @return Une nouvelle instance de {@link NSResultDictionary}.
	 * @see #getResult()
	 * @see #getException()
	 */
	public static NSResultDictionary newInstanceWithResult(Object o) {
		
		if (o == null)
			return new NSResultDictionary(new NSDictionary());
		if (o instanceof Throwable)
			throw new IllegalArgumentException("Exception non permise ici.");
		
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.takeValueForKey(o, KEY_FOR_RETURN_VALUE);
		
		return new NSResultDictionary(dico);
	}
	
	/**
	 * Cree un nouveau dictionnaire de "resultat" a partir de l'exception specifiee.
	 * @param e Exception.
	 * Les cles {@link NSResultDictionary#KEY_FOR_EXCEPTION_CLASS_NAME} et 
	 * {@link NSResultDictionary#KEY_FOR_EXCEPTION_MESSAGE} sont creees dans le dictionnaire. 
	 * Le message de l'exception cree dans le dictionnaire est la concatenation des {@link Throwable#getMessage()} 
	 * de toute la pile des causes de l'exception specifiee (incluse). 
	 * @return Une nouvelle instance de {@link NSResultDictionary}.
	 * @see NSResultDictionary#getException()
	 */
	public static NSResultDictionary newInstanceWithException(Throwable e) {
		
		if (e == null)
			throw new IllegalArgumentException("Exception null interdite.");
		
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.takeValueForKey(Message.createMessage(e), KEY_FOR_EXCEPTION_MESSAGE);
		dico.takeValueForKey(e.getClass().getName(), KEY_FOR_EXCEPTION_CLASS_NAME);
		
		return new NSResultDictionary(dico);
	}
	
	/**
	 * Constructeur.
	 * @param dictionary Contenu du dictionnaire de resultat a construire.
	 */
	protected NSResultDictionary(NSDictionary dictionary) {
		super(dictionary);
	}
	
	/**
	 * Retourne l'objet resultat contenu dans ce dictionnaire.
	 * @return Un objet quelconque. Associe a la cle {@link NSResultDictionary#KEY_FOR_RETURN_VALUE} dans ce dictionnaire.
	 */
	public Object getResult() {
		return valueForKey(KEY_FOR_RETURN_VALUE);
	}
	
	/**
	 * Indique si l'objet resultat contenu dans ce dictionnaire correspond a {@link NSResultDictionary#VALUE_FOR_RESULT_OK}.
	 * @return <code>true</code> si {@link #getResult()} retourne {@link NSResultDictionary#VALUE_FOR_RESULT_OK}.
	 */
	public boolean isResultOk() {
		return getResult()!=null && VALUE_FOR_RESULT_OK.equals(getResult());
	}
	/**
	 * Indique si l'objet resultat contenu dans ce dictionnaire correspond a {@link NSResultDictionary#VALUE_FOR_RESULT_NULL_AND_OK}.
	 * @return <code>true</code> si {@link #getResult()} retourne {@link NSResultDictionary#VALUE_FOR_RESULT_NULL_AND_OK}.
	 */
	public boolean isResultNullAndOk() {
		return getResult()!=null && VALUE_FOR_RESULT_NULL_AND_OK.equals(getResult());
	}

	/**
	 * Retourne l'exception eventuelle contenu dans ce dictionnaire.
	 * @return L'exception ou <code>null</code> si aucune exception presente. 
	 * Associee a la cle {@link NSResultDictionary#KEY_FOR_RETURN_VALUE} dans ce dictionnaire.
	 */
	public Exception getException() {
		
		Exception e=null;
		if (valueForKey(KEY_FOR_EXCEPTION_CLASS_NAME)!=null && valueForKey(KEY_FOR_EXCEPTION_MESSAGE)!=null) {
			e = getExceptionFromClassName(
					valueForKey(KEY_FOR_EXCEPTION_CLASS_NAME).toString(), 
					valueForKey(KEY_FOR_EXCEPTION_MESSAGE).toString());
		}
		return e;
	}



	/**
	 * Instancie une nouvelle {@link Exception} a partir d'une classe et d'un message.
	 * @param className Nom complet de la classe d'exception qui sera instanciee.
	 * @param message Message de l'exception a creer.
	 * @return L'exection instanciee.
	 */
	private Exception getExceptionFromClassName(String className, String message) {
		try {
			Class exceptionClass = Class.forName(className);
			Constructor constructor = exceptionClass.getDeclaredConstructor(new Class[] { String.class });
			return (Exception) constructor.newInstance(new Object[] { message });
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			return new Exception(message);
		} 
		catch (InstantiationException e) {
			e.printStackTrace();
			return new Exception(message);
		} 
		catch (IllegalAccessException e) {
			e.printStackTrace();
			return new Exception(message);
		} 
		catch (SecurityException e) {
			e.printStackTrace();
			return new Exception(message);
		} 
		catch (NoSuchMethodException e) {
			e.printStackTrace();
			return new Exception(message);
		} 
		catch (IllegalArgumentException e) {
			e.printStackTrace();
			return new Exception(message);
		} 
		catch (InvocationTargetException e) {
			e.printStackTrace();
			return new Exception(message);
		}
	}

}
