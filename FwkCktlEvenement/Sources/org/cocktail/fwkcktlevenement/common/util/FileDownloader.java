/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class FileDownloader 
{
	private URL url;
	private URLConnection connection;
	private InputStream inputStream;
	
	/**
	 * 
	 */
	public FileDownloader() {

	}
	
	/**
	 * Ouvre une connexion pointant sur l'URL specifiee.
	 * @param url URL a laquelle se connecter.
	 * @throws IOException
	 */
	public void connectUrl(URL url) throws IOException {
		
		this.connection = url.openConnection();
		
		int fileLength = connection.getContentLength();
		if (fileLength == -1) {
			throw new IOException("Fichier non valide.");
		}
		
		this.inputStream = connection.getInputStream();
		this.url = url;
	}

	/**
	 * Retourne le type du fichier pointe par la connexion courante.
	 * @return le type du fichier pointé par la connexion courante.
	 */
	public String getFileType() {
		String fileType = connection.getContentType();
		System.out.println("fileType = "+fileType);
		return fileType;
	}

	/**
	 * Retourne le nom du fichier pointe par la connexion courante.
	 * @return Le nom du fichier. Ex: "Document.pdf".
	 */
	public String getFileName() {
		String fileName = url.getFile();
		fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
		System.out.println("fileName = "+fileName);
		return fileName;
	}
	
	/**
	 * Fournit le flux d'entree pointant sur l'URL courante.
	 * @return Un {@link InputStream}.
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Telecharge le fichier pointe par l'URL courante.
	 * @param targetFilePath Chemin complet (nom du fichier compris) du fichier a creer sur le disque local.
	 * @return Un objet {@link File} representant le fichier cree sur le disque local.
	 * @throws IOException
	 */
	public File downloadFile(String targetFilePath) throws IOException {
		
		FileOutputStream writenFile = new FileOutputStream(targetFilePath);
		byte[] buff = new byte[1024];
		int l = inputStream.read(buff);
		while (l > 0)
		{
			writenFile.write(buff, 0, l);
			l = inputStream.read(buff);
		}
		writenFile.flush();
		writenFile.close();
		
		return new File(targetFilePath);
	}

}
