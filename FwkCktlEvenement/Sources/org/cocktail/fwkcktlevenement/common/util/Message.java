/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util;


/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class Message 
{
	public static final String TITLE_ERREUR_IMPREVUE 			= "Erreur impr\u00E9vue";
	public static final String MESSAGE_ERREUR_SANS_MESSAGE 		= "Une erreur s'est produite.";
	public static final String DETAILS_ERREUR_SANS_MESSAGE 		= "Aucun.";
	
	
//	/**
//	 * 
//	 * @param parentComponent
//	 * @param exception
//	 * @param messageType
//	 */
//	private static void showExceptionDialog(
//			Component parentComponent, 
//			ExceptionBase exception, 
//			int messageType) {
//		
//		if (exception == null)
//			throw new NullPointerException("Exception requise.");
//		
//		String message = StringOperation.multiline(createMessage(exception));
//		JOptionPane.showMessageDialog(
//				parentComponent, 
//				message.toString(), 
//				exception.getTitle(), 
//				messageType);
//	}
//	/**
//	 * 
//	 * @param parentComponent
//	 * @param exception
//	 * @param messageType
//	 */
//	private static void showExceptionDialog(
//			Component parentComponent, 
//			String title,
//			Throwable exception, 
//			int messageType) {
//		
//		if (exception == null)
//			throw new NullPointerException("Exception requise.");
//		
//		String message = StringOperation.multiline(createMessage(exception));
//		JOptionPane.showMessageDialog(
//				parentComponent, 
//				message.toString(), 
//				TITLE_ERREUR_IMPREVUE, 
//				messageType);
//	}
//	/**
//	 * 
//	 * @param parentComponent
//	 * @param exceptionEvenement
//	 */
//	public static void showExceptionDialog(Component parentComponent, ExceptionBase exception) {
//		showExceptionDialog(parentComponent, exception, JOptionPane.ERROR_MESSAGE);
//	}
//	/**
//	 * 
//	 * @param parentComponent
//	 * @param exceptionEvenement
//	 */
//	public static void showExceptionDialog(Component parentComponent, ExceptionBloquante exception) {
//		showExceptionDialog(parentComponent, exception, JOptionPane.ERROR_MESSAGE);
//	}
//	/**
//	 * 
//	 * @param parentComponent
//	 * @param exceptionEvenement
//	 */
//	public static void showExceptionDialog(Component parentComponent, ExceptionNonBloquante exception) {
//		showExceptionDialog(parentComponent, exception, JOptionPane.WARNING_MESSAGE);
//	}
//	/**
//	 * 
//	 * @param parentComponent
//	 * @param exception
//	 */
//	public static void showExceptionDialog(Component parentComponent, Throwable exception) {
//		if (exception == null)
//			throw new NullPointerException("Exception requise.");
//		
//		if (exception instanceof ExceptionBloquante) {
//			showExceptionDialog(parentComponent, (ExceptionBloquante) exception);
//		}
//		else if (exception instanceof ExceptionNonBloquante) {
//			showExceptionDialog(parentComponent, (ExceptionNonBloquante) exception);
//		}
//		else if (exception instanceof ExceptionBase) {
//			showExceptionDialog(parentComponent, (ExceptionBase) exception);
//		}
//		else {
//			showExceptionDialog(parentComponent, TITLE_ERREUR_IMPREVUE, exception, JOptionPane.ERROR_MESSAGE);
//		}
//	}
		
	/**
	 * 
	 */
	public static String createMessage(Throwable exception) {
		return createMessage(exception, true);
	}	
	/**
	 * 
	 */
	public static String createMessage(Throwable exception, boolean avecSautsDeLigne) {

		if (exception != null) {
			StringBuffer message = new StringBuffer();
			
			if (exception.getMessage() != null)
				message.append(exception.getMessage());
			else 
				message.append(MESSAGE_ERREUR_SANS_MESSAGE);

			message.append(avecSautsDeLigne ? "\n" : " ");
			if (exception.getCause() != null) {
				message.append("Cause:  ["+exception.getCause().getClass().getName()+"]");
				message.append(avecSautsDeLigne ? "\n\t" : " ");
			}
			message.append(createMessage(exception.getCause()));
		
			return message.toString();
		}
		else
			return "";
	}
	
	
//	public static void main(String[] args) {
//		Exception a = new Exception("chagrin d'amour");
//		Exception b = new Exception("9 pastis cul-sec", a);
//		Exception c = new Exception("ivresse sur la voie publique", b);
//		Exception d = new Exception("contravention", c);
//		
//		try {
//			throw d;
//		} 
//		catch (Exception e) {
//			e.printStackTrace();
//			Message.showExceptionDialog(null, d);
//		}
//	}
}
