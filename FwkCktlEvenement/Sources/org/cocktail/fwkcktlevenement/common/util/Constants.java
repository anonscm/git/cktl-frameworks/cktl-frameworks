/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class Constants 
{
	static protected String[] DaysOfWeekListStrings;
	static protected String[] DaysOfWeekListShortStrings;
	static protected String[] DaysOfWeekUsualListStrings;
	static protected String[] DaysOfWeekUsualListShortStrings;
	
	public final static String MONDAY_STRING		= "Lundi";
	public final static String TUESDAY_STRING		= "Mardi";
	public final static String WEDNESDAY_STRING		= "Mercredi";
	public final static String THURSDAY_STRING		= "Jeudi";
	public final static String FRIDAY_STRING		= "Vendredi";
	public final static String SATURDAY_STRING		= "Samedi";
	public final static String SUNDAY_STRING		= "Dimanche";

	public final static String MONDAY_STRING_SHORT		= "Lun";
	public final static String TUESDAY_STRING_SHORT		= "Mar";
	public final static String WEDNESDAY_STRING_SHORT	= "Mer";
	public final static String THURSDAY_STRING_SHORT	= "Jeu";
	public final static String FRIDAY_STRING_SHORT		= "Ven";
	public final static String SATURDAY_STRING_SHORT	= "Sam";
	public final static String SUNDAY_STRING_SHORT		= "Dim";
	
	
	/**
	 * <p>Fournit la liste de tous les noms des jours de la semaine. Ex: GetDaysOfWeekStrings[Calendar.THURSDAY] vaut "Jeudi".
	 * <p>Attention : le premier de la liste se trouve a l'index 1 et correspond a Dimanche ; le dernier en 7 et correspond a Samedi.
	 * @param shortNames 
	 * @return Liste des noms des jours de la semaine. 
	 */
	public final static String[] GetDaysOfWeekStrings(final boolean shortNames) {
		if (DaysOfWeekListStrings==null || DaysOfWeekListShortStrings==null) {
			DaysOfWeekListStrings = new String[8];
			DaysOfWeekListStrings[0] = "";
			DaysOfWeekListStrings[Calendar.SUNDAY] 		= SUNDAY_STRING;
			DaysOfWeekListStrings[Calendar.MONDAY] 		= MONDAY_STRING;
			DaysOfWeekListStrings[Calendar.TUESDAY] 	= TUESDAY_STRING;
			DaysOfWeekListStrings[Calendar.WEDNESDAY] 	= WEDNESDAY_STRING;
			DaysOfWeekListStrings[Calendar.THURSDAY] 	= THURSDAY_STRING;
			DaysOfWeekListStrings[Calendar.FRIDAY] 		= FRIDAY_STRING;
			DaysOfWeekListStrings[Calendar.SATURDAY] 	= SATURDAY_STRING;

			DaysOfWeekListShortStrings = new String[8];
			DaysOfWeekListShortStrings[0] = "";
			DaysOfWeekListShortStrings[Calendar.SUNDAY] 	= SUNDAY_STRING_SHORT;
			DaysOfWeekListShortStrings[Calendar.MONDAY] 	= MONDAY_STRING_SHORT;
			DaysOfWeekListShortStrings[Calendar.TUESDAY] 	= TUESDAY_STRING_SHORT;
			DaysOfWeekListShortStrings[Calendar.WEDNESDAY] 	= WEDNESDAY_STRING_SHORT;
			DaysOfWeekListShortStrings[Calendar.THURSDAY] 	= THURSDAY_STRING_SHORT;
			DaysOfWeekListShortStrings[Calendar.FRIDAY] 	= FRIDAY_STRING_SHORT;
			DaysOfWeekListShortStrings[Calendar.SATURDAY] 	= SATURDAY_STRING_SHORT;
		}
		if (shortNames)
			return DaysOfWeekListShortStrings;
		else
			return DaysOfWeekListStrings;
	}
	/**
	 * <p>Retourne le nom en clair du jour specifie par son numero. 
	 * @param dayField Numero du jour. Ex: {@link Calendar#THURSDAY}.
	 * @param shortName Utiliser <code>true</code> pour avoir le format court des noms des jours.
	 * @return Le nom en clair du jour specifie par son numero. Ex: "Lundi" ou "Lun" pour {@link Calendar#MONDAY}.
	 */
	public final static String GetDayOfWeekString(int dayField, boolean shortName) {
		return GetDaysOfWeekStrings(shortName)[dayField];
	}
	/**
	 * <p>Fournit la liste de tous les noms des jours de la semaine. Ex: GetDaysOfWeekStrings[3] vaut "Jeudi".
	 * <p>Attention : le premier de la liste se trouve a l'index 0 et correspond a Lundi ; le dernier en 6 et correspond a Dimanche.
	 * @param shortNames Utiliser <code>true</code> pour avoir le format court des noms des jours.
	 * @return Liste des noms des jours de la semaine. 
	 */
	public final static String[] GetDaysOfWeekUsualListStrings(boolean shortNames) {
		if (DaysOfWeekUsualListStrings == null) {
			DaysOfWeekUsualListStrings = new String[7];
			DaysOfWeekUsualListStrings[0] 	= shortNames ? MONDAY_STRING_SHORT : 	MONDAY_STRING;
			DaysOfWeekUsualListStrings[1] 	= shortNames ? TUESDAY_STRING_SHORT : 	TUESDAY_STRING;
			DaysOfWeekUsualListStrings[2] 	= shortNames ? WEDNESDAY_STRING_SHORT : WEDNESDAY_STRING;
			DaysOfWeekUsualListStrings[3] 	= shortNames ? THURSDAY_STRING_SHORT : 	THURSDAY_STRING;
			DaysOfWeekUsualListStrings[4] 	= shortNames ? FRIDAY_STRING_SHORT : 	FRIDAY_STRING;
			DaysOfWeekUsualListStrings[5] 	= shortNames ? SATURDAY_STRING_SHORT : 	SATURDAY_STRING;
			DaysOfWeekUsualListStrings[6] 	= shortNames ? SUNDAY_STRING_SHORT : 	SUNDAY_STRING;
		}
		return DaysOfWeekUsualListStrings;
	}
	

	public final static String MONTH_DAY_1_STRINGS	= "1";
	public final static String MONTH_DAY_2_STRINGS	= "2";
	public final static String MONTH_DAY_3_STRINGS	= "3";
	public final static String MONTH_DAY_4_STRINGS	= "4";
	public final static String MONTH_DAY_5_STRINGS	= "5";
	public final static String MONTH_DAY_6_STRINGS	= "6";
	public final static String MONTH_DAY_7_STRINGS	= "7";
	public final static String MONTH_DAY_8_STRINGS	= "8";
	public final static String MONTH_DAY_9_STRINGS	= "9";
	public final static String MONTH_DAY_10_STRINGS	= "10";
	public final static String MONTH_DAY_11_STRINGS	= "11";
	public final static String MONTH_DAY_12_STRINGS	= "12";
	public final static String MONTH_DAY_13_STRINGS	= "13";
	public final static String MONTH_DAY_14_STRINGS	= "14";
	public final static String MONTH_DAY_15_STRINGS	= "15";
	public final static String MONTH_DAY_16_STRINGS	= "16";
	public final static String MONTH_DAY_17_STRINGS	= "17";
	public final static String MONTH_DAY_18_STRINGS	= "18";
	public final static String MONTH_DAY_19_STRINGS	= "19";
	public final static String MONTH_DAY_20_STRINGS	= "20";
	public final static String MONTH_DAY_21_STRINGS	= "21";
	public final static String MONTH_DAY_22_STRINGS	= "22";
	public final static String MONTH_DAY_23_STRINGS	= "23";
	public final static String MONTH_DAY_24_STRINGS	= "24";
	public final static String MONTH_DAY_25_STRINGS	= "25";
	public final static String MONTH_DAY_26_STRINGS	= "26";
	public final static String MONTH_DAY_27_STRINGS	= "27";
	public final static String MONTH_DAY_28_STRINGS	= "28";
	public final static String MONTH_DAY_29_STRINGS	= "29";
	public final static String MONTH_DAY_30_STRINGS	= "30";
	public final static String MONTH_DAY_31_STRINGS	= "31";

	public final static String[] MONTH_DAYS_STRINGS =  new String[] { 
		MONTH_DAY_1_STRINGS,
		MONTH_DAY_2_STRINGS,
		MONTH_DAY_3_STRINGS,
		MONTH_DAY_4_STRINGS,
		MONTH_DAY_5_STRINGS,
		MONTH_DAY_6_STRINGS,
		MONTH_DAY_7_STRINGS,
		MONTH_DAY_8_STRINGS,
		MONTH_DAY_9_STRINGS,
		MONTH_DAY_10_STRINGS,
		MONTH_DAY_11_STRINGS,
		MONTH_DAY_12_STRINGS,
		MONTH_DAY_13_STRINGS,
		MONTH_DAY_14_STRINGS,
		MONTH_DAY_15_STRINGS,
		MONTH_DAY_16_STRINGS,
		MONTH_DAY_17_STRINGS,
		MONTH_DAY_18_STRINGS,
		MONTH_DAY_19_STRINGS,
		MONTH_DAY_20_STRINGS,
		MONTH_DAY_21_STRINGS,
		MONTH_DAY_22_STRINGS,
		MONTH_DAY_23_STRINGS,
		MONTH_DAY_24_STRINGS,
		MONTH_DAY_25_STRINGS,
		MONTH_DAY_26_STRINGS,
		MONTH_DAY_27_STRINGS,
		MONTH_DAY_28_STRINGS,
		MONTH_DAY_29_STRINGS,
		MONTH_DAY_30_STRINGS,
		MONTH_DAY_31_STRINGS
	};

	/**
	 * Genere la liste des numeros de jour du mois de l'annee specifie. 
	 * @param month Ex: {@value java.util.Calendar#FEBRUARY}
	 * @param year Ex: 2007. Ou 0 pour l'annee en cours.
	 * @return Liste de String representant les numeros des jours du mois specifie. 
	 * Ex: { "1", "2", ... , "28" } pour {@value java.util.Calendar#FEBRUARY} si l'annee specifiee n'est pas bissextile.
	 * @see java.util.Calendar
	 */
	public final static String[] GetDaysOfMonthStrings(final int month, final int year) {
		ArrayList days = new ArrayList();
		Calendar calendar = GregorianCalendar.getInstance();
		if (year != 0) calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);	

//		System.out.println("Constants.GetDaysOfMonthStrings() "+calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		for (int i=1; i<=calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
			days.add(new Integer(i).toString());
		}
		return (String[]) days.toArray(new String[] { });
	}
	/**
	 * Genere la liste des numeros de jour du mois de l'annee courante specifie. 
	 * @param month Ex: {@value Calendar#FEBRUARY}
	 * @return Liste de String representant les numeros des jours du mois specifie. 
	 * Ex: { "1", "2", ... , "28" } pour {@value Calendar#FEBRUARY} si l'annee courante n'est pas bissextile.
	 * @see Calendar
	 */
	public final static String[] GetDaysOfMonthStrings(final int month) {
		return GetDaysOfMonthStrings(month, 0);
	}
	
	
	
	
	
	public final static String LUNDI_STRING         = "Lundi";
	public final static String MARDI_STRING         = "Mardi";
	public final static String MERCREDI_STRING     	= "Mercredi";
	public final static String JEUDI_STRING         = "Jeudi";
	public final static String VENDREDI_STRING     	= "Vendredi";
	public final static String SAMEDI_STRING     	= "Samedi";
	public final static String DIMANCHE_STRING     	= "Dimanche";
	
	public final static String[] JOURS_SEMAINE_STRINGS = new String[] { 
		LUNDI_STRING,
		MARDI_STRING,
		MERCREDI_STRING,
		JEUDI_STRING,
		VENDREDI_STRING,
		SAMEDI_STRING,
		DIMANCHE_STRING
	}; 
	
	public final static long LUNDI         = (long) Math.pow(2, 1);
	public final static long MARDI         = (long) Math.pow(2, 2);
	public final static long MERCREDI     = (long) Math.pow(2, 3);
	public final static long JEUDI         = (long) Math.pow(2, 4);
	public final static long VENDREDI     = (long) Math.pow(2, 5);
	public final static long SAMEDI     = (long) Math.pow(2, 6);
	public final static long DIMANCHE     = (long) Math.pow(2, 7);

	public final static long JOUR_MOIS_1     = (long) Math.pow(2, 1);
	public final static long JOUR_MOIS_2     = (long) Math.pow(2, 2);
	public final static long JOUR_MOIS_3     = (long) Math.pow(2, 3);
	public final static long JOUR_MOIS_4     = (long) Math.pow(2, 4);
	public final static long JOUR_MOIS_5     = (long) Math.pow(2, 5);
	public final static long JOUR_MOIS_6     = (long) Math.pow(2, 6);
	public final static long JOUR_MOIS_7     = (long) Math.pow(2, 7);
	public final static long JOUR_MOIS_8     = (long) Math.pow(2, 8);
	public final static long JOUR_MOIS_9     = (long) Math.pow(2, 9);
	public final static long JOUR_MOIS_10     = (long) Math.pow(2, 10);
	public final static long JOUR_MOIS_11     = (long) Math.pow(2, 11);
	public final static long JOUR_MOIS_12     = (long) Math.pow(2, 12);
	public final static long JOUR_MOIS_13     = (long) Math.pow(2, 13);
	public final static long JOUR_MOIS_14     = (long) Math.pow(2, 14);
	public final static long JOUR_MOIS_15     = (long) Math.pow(2, 15);
	public final static long JOUR_MOIS_16     = (long) Math.pow(2, 16);
	public final static long JOUR_MOIS_17     = (long) Math.pow(2, 17);
	public final static long JOUR_MOIS_18     = (long) Math.pow(2, 18);
	public final static long JOUR_MOIS_19     = (long) Math.pow(2, 19);
	public final static long JOUR_MOIS_20     = (long) Math.pow(2, 20);
	public final static long JOUR_MOIS_21     = (long) Math.pow(2, 21);
	public final static long JOUR_MOIS_22     = (long) Math.pow(2, 22);
	public final static long JOUR_MOIS_23     = (long) Math.pow(2, 23);
	public final static long JOUR_MOIS_24     = (long) Math.pow(2, 24);
	public final static long JOUR_MOIS_25     = (long) Math.pow(2, 25);
	public final static long JOUR_MOIS_26     = (long) Math.pow(2, 26);
	public final static long JOUR_MOIS_27     = (long) Math.pow(2, 27);
	public final static long JOUR_MOIS_28     = (long) Math.pow(2, 28);
	public final static long JOUR_MOIS_29     = (long) Math.pow(2, 29);
	public final static long JOUR_MOIS_30     = (long) Math.pow(2, 30);
	public final static long JOUR_MOIS_31     = (long) Math.pow(2, 31);

	public final static String JOUR_MOIS_STRING_1     = "1";
	public final static String JOUR_MOIS_STRING_2     = "2";
	public final static String JOUR_MOIS_STRING_3     = "3";
	public final static String JOUR_MOIS_STRING_4     = "4";
	public final static String JOUR_MOIS_STRING_5     = "5";
	public final static String JOUR_MOIS_STRING_6     = "6";
	public final static String JOUR_MOIS_STRING_7     = "7";
	public final static String JOUR_MOIS_STRING_8     = "8";
	public final static String JOUR_MOIS_STRING_9     = "9";
	public final static String JOUR_MOIS_STRING_10     = "10";
	public final static String JOUR_MOIS_STRING_11     = "11";
	public final static String JOUR_MOIS_STRING_12     = "12";
	public final static String JOUR_MOIS_STRING_13     = "13";
	public final static String JOUR_MOIS_STRING_14     = "14";
	public final static String JOUR_MOIS_STRING_15     = "15";
	public final static String JOUR_MOIS_STRING_16     = "16";
	public final static String JOUR_MOIS_STRING_17     = "17";
	public final static String JOUR_MOIS_STRING_18     = "18";
	public final static String JOUR_MOIS_STRING_19     = "19";
	public final static String JOUR_MOIS_STRING_20     = "20";
	public final static String JOUR_MOIS_STRING_21     = "21";
	public final static String JOUR_MOIS_STRING_22     = "22";
	public final static String JOUR_MOIS_STRING_23     = "23";
	public final static String JOUR_MOIS_STRING_24     = "24";
	public final static String JOUR_MOIS_STRING_25     = "25";
	public final static String JOUR_MOIS_STRING_26     = "26";
	public final static String JOUR_MOIS_STRING_27     = "27";
	public final static String JOUR_MOIS_STRING_28     = "28";
	public final static String JOUR_MOIS_STRING_29     = "29";
	public final static String JOUR_MOIS_STRING_30     = "30";
	public final static String JOUR_MOIS_STRING_31     = "31";
	
	public final static String[] JOURS_MOIS_STRINGS = new String[] { 
		JOUR_MOIS_STRING_1,
		JOUR_MOIS_STRING_2,
		JOUR_MOIS_STRING_3,
		JOUR_MOIS_STRING_4,
		JOUR_MOIS_STRING_5,
		JOUR_MOIS_STRING_6,
		JOUR_MOIS_STRING_7,
		JOUR_MOIS_STRING_8,
		JOUR_MOIS_STRING_9 ,
		JOUR_MOIS_STRING_10  ,
		JOUR_MOIS_STRING_11   ,
		JOUR_MOIS_STRING_12   ,
		JOUR_MOIS_STRING_13 ,
		JOUR_MOIS_STRING_14   ,
		JOUR_MOIS_STRING_15  ,
		JOUR_MOIS_STRING_16  ,
		JOUR_MOIS_STRING_17  ,
		JOUR_MOIS_STRING_18  ,
		JOUR_MOIS_STRING_19 ,
		JOUR_MOIS_STRING_20   ,
		JOUR_MOIS_STRING_21   ,
		JOUR_MOIS_STRING_22  ,
		JOUR_MOIS_STRING_23  ,
		JOUR_MOIS_STRING_24   ,
		JOUR_MOIS_STRING_25   ,
		JOUR_MOIS_STRING_26   ,
		JOUR_MOIS_STRING_27 ,
		JOUR_MOIS_STRING_28   ,
		JOUR_MOIS_STRING_29   ,
		JOUR_MOIS_STRING_30 ,
		JOUR_MOIS_STRING_31
	}; 
	
	public final static String PREMIER     = "PREMIER";
	public final static String DEUXIEME     = "DEUXIEME";
	public final static String TROISIEME     = "TROISIEME";
	public final static String QUATRIEME     = "QUATRIEME";
	public final static String DERNIER     = "DERNIER";


}
