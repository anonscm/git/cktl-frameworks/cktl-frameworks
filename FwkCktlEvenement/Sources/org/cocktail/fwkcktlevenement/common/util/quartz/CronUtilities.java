/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util.quartz;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.quartz.CronExpression;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * Utilitaires de creation d'expression CRON.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 * @see CronExpression
 */
public class CronUtilities 
{
	static public final String SUMMARY_KEY_seconds = "seconds";
	static public final String SUMMARY_KEY_minutes = "minutes";
	static public final String SUMMARY_KEY_hours = "hours";
	static public final String SUMMARY_KEY_daysOfMonth = "daysOfMonth";
	static public final String SUMMARY_KEY_months = "months";
	static public final String SUMMARY_KEY_daysOfWeek = "daysOfWeek";
	static public final String SUMMARY_KEY_lastdayOfWeek = "lastdayOfWeek";
	static public final String SUMMARY_KEY_nearestWeekday = "nearestWeekday";
	static public final String SUMMARY_KEY_NthDayOfWeek = "NthDayOfWeek";
	static public final String SUMMARY_KEY_lastdayOfMonth = "lastdayOfMonth";
	static public final String SUMMARY_KEY_years = "years";

	/**
	 * Cree une expression CRON a partir de d'elements constitutifs.
	 * @param sec secondes
	 * @param min minutes
	 * @param hour heures
	 * @param dayOfMonth jours du mois
	 * @param month mois
	 * @param dayOfWeek jours de la semaine
	 * @param year annees
	 * @return L'expression CRON resultante.
	 */
	public static String makeCronExpression(
			Object sec, 
			Object min, 
			Object hour, 
			Object dayOfMonth, 
			Object month, 
			Object dayOfWeek, 
			Object year) {

		if (sec == null) 
			throw new NullPointerException("Secondes requises.");
		if (min == null) 
			throw new NullPointerException("Minutes requises.");
		if (hour == null) 
			throw new NullPointerException("Heures requises.");
		if (dayOfMonth == null) 
			throw new NullPointerException("Jour de mois requis.");
		if (month == null) 
			throw new NullPointerException("Mois requis.");
		if (dayOfWeek == null) 
			throw new NullPointerException("Jours de la semaine requis.");
//		if (year == null) 
//			throw new NullPointerException("Annees requises.");

		StringBuffer buf = new StringBuffer();
		buf.append(sec);//sec
		buf.append(" ");
		buf.append(min);//min
		buf.append(" ");
		buf.append(hour);//hour
		buf.append(" ");
		buf.append(dayOfMonth);//day of month
		buf.append(" ");
		buf.append(month);//month
		buf.append(" ");
		buf.append(dayOfWeek);//day of week
		if (year != null) {
			buf.append(" ");
			buf.append(year);//year
		}
		
		return buf.toString();
	}


	/**
	 * Cree une expression CRON pour une periodicite quotidienne, a partir d'une date specifiant 
	 * les heures et les minutes. 
	 * @param date Date specifiant les heures et les minutes de l'expression CRON.
	 * @return L'expression CRON resultante. Du type: "0 30 10 ? * *".
	 * @throws ExceptionOperationImpossible 
	 */
	static public CronExpression createForDaily(Date date) throws ExceptionOperationImpossible {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		String expr = makeCronExpression(
				"0", 
				""+cal.get(Calendar.MINUTE), 
				""+cal.get(Calendar.HOUR_OF_DAY), 
				"?",
				"*", 
				"*",
				null);
		
		try {
			return new CronExpression(expr);
		} 
		catch (ParseException pe) {
			throw new ExceptionOperationImpossible("Erreur de parsing de l'expression CRON: "+expr, pe);
		}
	}

	/**
	 * Cree une expression CRON pour une periodicite hebdomadaire, a partir des jours de la semaine
	 * et d'une date specifiant les heures et les minutes. 
	 * @param daysOfWeekFields Liste de numeros de jours de la semaine. 
	 * Ex: { {@link Calendar#MONDAY}, {@link Calendar#WEDNESDAY} }.
	 * @param date Date specifiant les heures et les minutes.
	 * @return L'expression CRON resultante. Du type: "0 30 10 ? * MON,WED".
	 * @throws ExceptionOperationImpossible 
	 */
	static public CronExpression createForWeekly(
			Integer[] daysOfWeekFields,
			Date date) throws ExceptionOperationImpossible {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		String daysofweek = "*";
		if (daysOfWeekFields!=null && daysOfWeekFields.length>0) {
			
			Arrays.sort(daysOfWeekFields);	//indispensable
			StringBuffer buf = new StringBuffer();
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.SUNDAY)) >= 0) 		buf.append("SUN"); //1
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.MONDAY)) >= 0) 		buf.append(",MON");//2
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.TUESDAY)) >= 0) 		buf.append(",TUE");//3
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.WEDNESDAY)) >= 0) 	buf.append(",WED");//4
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.THURSDAY)) >= 0) 	buf.append(",THU");//5
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.FRIDAY)) >= 0)	 	buf.append(",FRI");//6
			if (Arrays.binarySearch(daysOfWeekFields, new Integer(Calendar.SATURDAY)) >= 0) 	buf.append(",SAT");//7
			if (buf.charAt(0) == ',') buf.deleteCharAt(0);
			daysofweek = buf.toString();
		}
		
		String expr = makeCronExpression(
				"0", 
				""+cal.get(Calendar.MINUTE), 
				""+cal.get(Calendar.HOUR_OF_DAY), 
				"?",
				"*", 
				daysofweek,
				null);

		try {
			return new CronExpression(expr);
		}
		catch (ParseException pe) {
			throw new ExceptionOperationImpossible("Erreur de parsing de l'expression CRON: "+expr, pe);
		}
	}

	/**
	 * Cree une expression CRON pour une periodicite mensuelle, a partir des jours du mois
	 * et d'une date specifiant les heures et les minutes. 
	 * @param daysOfMonthFields Liste de numeros de jous du mois. Ex: { 1, 2, 31 }.
	 * @param date Date specifiant les heures et les minutes.
	 * @return L'expression CRON resultante. Du type: "0 30 10 1,2,31 * ?".
	 * @throws ExceptionOperationImpossible 
	 */
	static public CronExpression createForMonthly(
			Integer[] daysOfMonthFields, 
			Date date) throws ExceptionOperationImpossible {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		String daysOfMonth = null;
		if (daysOfMonthFields != null && daysOfMonthFields.length > 0) {
			StringBuffer buf = new StringBuffer();
			for (int i = 0; i < daysOfMonthFields.length; i++) {
				buf.append(daysOfMonthFields[i] + ",");
			}
			if (buf.charAt(buf.length() - 1) == ',') {
				buf.deleteCharAt(buf.length() - 1);
			}
			daysOfMonth = buf.toString();
		} else {
		    daysOfMonth = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		}
		
		String expr = makeCronExpression(
				"0", 
				""+cal.get(Calendar.MINUTE), 
				""+cal.get(Calendar.HOUR_OF_DAY), 
				daysOfMonth,
				"*", 
				"?",
				null);

		try {
			return new CronExpression(expr);
		}
		catch (ParseException pe) {
			throw new ExceptionOperationImpossible("Erreur de parsing de l'expression CRON: "+expr, pe);
		}
	}
	
	/**
	 * Cree une expression CRON pour une periodicite du genre "Nieme jour du mois", 
	 * a partir d'un index, d'un jour du mois et d'une date specifiant les heures et les minutes. 
	 * @param nth Numero d'ordre. Valeurs: 1 pour "le premier", 2, 3 ou 4 ; 5 pour "le dernier".
	 * @param dayOfWeekField Numero du jour de la semaine. Ex: {@link Calendar#WEDNESDAY}.
	 * @param date Date specifiant les heures et les minutes.
	 * @return L'expression CRON resultante. 
	 * Ex: "0 30 10 ? * 4#2" pour "2e mercredi du mois", "0 30 10 ? * 2#L" pour "dernier lundi du mois".
	 * @throws ExceptionOperationImpossible 
	 */
	static public CronExpression createForMonthlyNthDay(
			int nth, 
			int dayOfWeekField, 
			Date date) throws ExceptionOperationImpossible {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		StringBuffer pattern = new StringBuffer();
		pattern.append(dayOfWeekField);// nom abrege du jour non accepte (ex: incorrect="MON#3" --> correct="2#3") 
		if (nth>=1 && nth<=4) {
			pattern.append("#");
			pattern.append(nth);
		}
		else
			pattern.append("L");
		
		String expr = makeCronExpression(
				"0", 
				""+cal.get(Calendar.MINUTE), 
				""+cal.get(Calendar.HOUR_OF_DAY), 
				"?",
				"*", 
				pattern.toString(),
				null);

		try {
			return new CronExpression(expr);
		}
		catch (ParseException pe) {
			throw new ExceptionOperationImpossible("Erreur de parsing de l'expression CRON: "+expr, pe);
		}
	}

	/**
	 * Calcul les occurences d'une expression CRON, occurences situees entre 2 dates.
	 * @param expr Expression CRON de reference.
	 * @param afterDate Date non comprise apres laquelle on commence a calculer.
	 * @param dateMaxi Date butoire non comprise.
	 * @return Liste des occurences trouvees de type {@link Date}. Vide si rien trouve.
	 */
	static public NSArray computeNextValidTimesTill(CronExpression expr, Date afterDate, Date dateMaxi) {
		if (expr == null)
			throw new NullPointerException("Expression CRON requise.");
		if (afterDate == null)
			throw new NullPointerException("Date de reference requise.");
		if (dateMaxi == null)
			throw new NullPointerException("Date maxi requise.");

		NSMutableArray times = new NSMutableArray();
		Date next = expr.getNextValidTimeAfter(afterDate);
		while (next.before(dateMaxi)) {
			times.addObject(next);
			next = expr.getNextValidTimeAfter(next);
		}
		return times;
	}

	/**
	 * Genere un dictionnaire contenant le {@link CronExpression#getExpressionSummary()} 
	 * de l'expression CRON specifiee.
	 * @param expr Expression CRON.
	 * @return Un {@link NSDictionary}.
	 * @see CronExpression#getExpressionSummary()
	 */
	public static NSDictionary getCronExpressionSummaryDictionary(CronExpression expr) {
		NSArray array = NSArray.componentsSeparatedByString(expr.getExpressionSummary(), "\n");
		NSMutableDictionary dico = new NSMutableDictionary();
		for (int i = 0; i < array.count() - 1; i++) {
			String string = (String) array.objectAtIndex(i);
			NSArray keyValue = NSArray.componentsSeparatedByString(string, ":");
			dico.setObjectForKey(((String) keyValue.objectAtIndex(1)).trim(), ((String) keyValue.objectAtIndex(0)).trim());
		}
		return dico.immutableClone();
	}
	
	/**
	 * 
	 * @param expr
	 * @return
	 */
	private Integer[] getCronExpressionDaysOfWeek(CronExpression expr) {
		Integer[] result=null;
		if (getCronExpressionNthDayOfWeek(expr) == null) {
			NSDictionary dico = getCronExpressionSummaryDictionary(expr);
			String value = (String) dico.valueForKey("daysOfWeek");
			NSArray daysOfWeek = NSArray.componentsSeparatedByString(value, ",");
			
			result = new Integer[daysOfWeek.count()];
			for (int i = 0; i < daysOfWeek.count(); i++) {
				result[i] = new Integer((String) daysOfWeek.objectAtIndex(i));
//				System.out.println("CronUtilities.getCronExpressionDaysOfWeek() result[i] = "+result[i]);
			}
		}
		return result;
	}
	/**
	 * 
	 * @param expr
	 * @return
	 */
	private Integer[] getCronExpressionDaysOfMonth(CronExpression expr) {
		Integer[] result=null;
		if (getCronExpressionNthDayOfWeek(expr) == null) {
			NSDictionary dico = getCronExpressionSummaryDictionary(expr);
			String value = (String) dico.valueForKey("daysOfMonth");
			NSArray daysOfWeek = NSArray.componentsSeparatedByString(value, ",");
			
			result = new Integer[daysOfWeek.count()];
			for (int i = 0; i < daysOfWeek.count(); i++) {
				result[i] = new Integer((String) daysOfWeek.objectAtIndex(i));
//				System.out.println("CronUtilities.getCronExpressionDaysOfMonth() result[i] = "+result[i]);
			}
		}
		return result;
	}
	/**
	 * 
	 * @param expr
	 * @return
	 */
	private Integer[] getCronExpressionMonths(CronExpression expr) {
		NSDictionary dico = getCronExpressionSummaryDictionary(expr);
		String value = (String) dico.valueForKey("months");
		Integer[] result=null;
		if (!"*".equals(value) && !"?".equals(value)) {
			NSArray daysOfWeek = NSArray.componentsSeparatedByString(value, ",");
			result = new Integer[daysOfWeek.count()];
			for (int i = 0; i < daysOfWeek.count(); i++) {
				result[i] = new Integer((String) daysOfWeek.objectAtIndex(i));
//				System.out.println("CronUtilities.getCronExpressionMonths() result[i] = "+result[i]);
			}
		}
		return result;
	}
	/**
	 * 
	 * @param expr
	 * @return
	 */
	private Integer getCronExpressionNthDayOfWeek(CronExpression expr) {
		NSDictionary dico = getCronExpressionSummaryDictionary(expr);
		String value = (String) dico.valueForKey("NthDayOfWeek");
		if (!"0".equals(value)) 
			return new Integer(value);
		else
			return null;
	}

	/**
	 * 
	 * @param componentsJoinedByString
	 * @return
	 */
	private static Integer[] componentsSeparatedByStringToIntegers(String componentsJoinedByString) {
    	NSArray names = NSArray.componentsSeparatedByString(componentsJoinedByString, ",");
    	Integer[] ints = new Integer[names.count()];
    	for (int i=0; i<names.count(); i++) {
    		ints[i] = new Integer((String) names.objectAtIndex(i));
    	}
    	return ints;
    }
	
	
//	public static void main(String[] args) throws ParseException {
//		
//		Date now = new Date();
//		int delai = -2*24*60;	//min
////		delai = -30;
//		
//		Calendar cal = GregorianCalendar.getInstance();
//		cal.setTime(now);
//		cal.add(Calendar.MONTH, 12);
//		Date end = cal.getTime();
//		
//		// CRON:   1=dim, 2=lun, 3=mar, 4=mer, 5=jeu, 6=ven, 7=sam
//
//		CronExpression ce = new CronExpression("0 30 10 1,10,20,30 * ?");	
//		System.out.println("CronUtilities.main() ce avant = "+ce);
//		NSArray fireTimes = CronUtilities.computeNextValidTimesTill(ce, now, end);
//		Debug.printArray("fire times avant", fireTimes);
//		
////		ce = new CronExpression("0 30 10 ? * 4,5,6");	
////		System.out.println("CronUtilities.main() ce avant = "+ce);
////		fireTimes = CronUtilities.computeNextValidTimesTill(ce, now, end);
////		Debug.printArray("fire times avant", fireTimes);
////		
////		ce = CronUtilities.shiftCronExpressionForWeekly(ce.getCronExpression(), new Date(), delai);
////		System.out.println("CronUtilities.main() ce apres = "+ce);
////		fireTimes = CronUtilities.computeNextValidTimesTill(ce, now, end);
////		Debug.printArray("fire times apres", fireTimes);
//	}
}
