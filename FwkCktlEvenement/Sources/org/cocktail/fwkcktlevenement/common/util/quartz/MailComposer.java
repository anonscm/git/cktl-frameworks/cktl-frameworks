/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util.quartz;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.quartz.JobDataMap;
import org.quartz.jobs.ee.mail.SendMailJob;

import com.webobjects.foundation.NSArray;


/**
 * <p>Utilitaire pour fabriquer une {@link Map} contenant les informations necessaires
 * a la classe {@link SendMailJob} pour envoyer un mail.
 * <p>
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2008
 * 
 * @see SendMailJob
 */
public class MailComposer 
{
	private static final String PROP_SMTP_HOST 		= SendMailJob.PROP_SMTP_HOST;
	private static final String PROP_RECIPIENT 		= SendMailJob.PROP_RECIPIENT;
	private static final String PROP_CC_RECIPIENT 	= SendMailJob.PROP_CC_RECIPIENT;
	private static final String PROP_SENDER 		= SendMailJob.PROP_SENDER;
	private static final String PROP_REPLY_TO 		= SendMailJob.PROP_REPLY_TO;
	private static final String PROP_SUBJECT 		= SendMailJob.PROP_SUBJECT;
	private static final String PROP_CONTENT_TYPE 	= SendMailJob.PROP_CONTENT_TYPE;
	private static final String PROP_MESSAGE 		= SendMailJob.PROP_MESSAGE;

	private static final String EMPLACEMENT_MESSAGE_HEADER 	= "{0}";
	private static final String EMPLACEMENT_MESSAGE_BODY 	= "{1}";
	private static final String EMPLACEMENT_MESSAGE_FOOTER 	= "{2}";

	private static final String EMPLACEMENT_DELAI_PREVENANCE 				= "{0}";
	private static final String EMPLACEMENT_INFOS_EVENEMENT 				= "{1}";
	private static final String EMPLACEMENT_INFOS_TACHE 					= "{2}";
	private static final String EMPLACEMENT_MESSAGE_PROCHAIN_MAIL			= "{3}";
	private static final String EMPLACEMENT_MESSAGE_MISFIRED				= "{4}";
	private static final String EMPLACEMENT_MESSAGE_PERSONNES_INJOIGNABLES	= "{5}";
	
	public static final String SUBJECT_DEFAULT						= "[EVENEMENT] Alerte automatique";
	public static final String SUBJECT_DEFAULT_ACCUSE_EXEC_TACHE	= "[EVENEMENT] Accuse d'execution d'une tache";
	public static final String MESSAGE_HEADER_DEFAULT	= "Bonjour, <br>";
	public static final String MESSAGE_FOOTER_DEFAULT	= "Bonne journée. <br>";
	
	public static final String SENDER_DEFAULT			= "ne_pas_repondre@cocktail.org";
	public static final String RECIPIENTS_SEPARATOR	= ", ";
	
	
	private String prop_smtp_host;
	private String prop_sender;
	private String prop_reply_to;
	private String prop_content_type = "text/html; charset=utf-8";

	private StringBuffer prop_recipient = new StringBuffer();
	private StringBuffer prop_cc_recipient = new StringBuffer();
	private StringBuffer prop_subject = new StringBuffer();
	private StringBuffer prop_message = new StringBuffer();

	private StringBuffer prop_message_header = new StringBuffer();
	private StringBuffer prop_message_body = new StringBuffer();
	private StringBuffer prop_message_footer = new StringBuffer();

	private StringBuffer adressesMailDestinataires = new StringBuffer();
	private StringBuffer infosDelaiPrevenance = new StringBuffer();
	private StringBuffer infosEvenement = new StringBuffer();
	private StringBuffer infosTache = new StringBuffer();
	private StringBuffer infosMisfired = new StringBuffer();
	private StringBuffer infosPersonnesInjoignables = new StringBuffer();
	private StringBuffer infosProchainMail = new StringBuffer();


	/**
	 * Constructeur.
	 * @param prop_smtp_host Adresse du sevreur de mail.
	 * @param prop_sender Adresse mail utilisee comme emetteur du mail.
	 * @param prop_reply_to Adresse mail utilisee comme adresse de reponse au mail.
	 */
	protected MailComposer(
			String prop_smtp_host,
			String prop_sender,
			String prop_reply_to) {

		this.prop_smtp_host = prop_smtp_host;
		this.prop_sender = prop_sender;
		this.prop_reply_to = prop_reply_to;

		initialize();
	}

	/**
	 * Fournit une nouvelle instance de cette classe.
	 * @param prop_smtp_host Adresse du sevreur de mail.
	 * @param prop_sender Adresse mail utilisee comme emetteur du mail.
	 * @param prop_reply_to Adresse mail utilisee comme adresse de reponse au mail.
	 * @return Une instance de cette classe.
	 */
	public static MailComposer newInstance(
			String prop_smtp_host,
			String prop_sender,
			String prop_reply_to) {
		
		if (prop_smtp_host == null)
			throw new IllegalArgumentException("Paramètre 'prop_smtp_host' requis.");
		if (prop_sender == null)
			throw new IllegalArgumentException("Paramètre 'prop_sender' requis.");
		if (prop_reply_to == null)
			throw new IllegalArgumentException("Paramètre 'prop_reply_to' requis.");
		
		return new MailComposer(prop_smtp_host, prop_sender, prop_reply_to);
	}

	/**
	 * 
	 */
	protected void initialize() {
		createDefaultSubject();
		createDefaultMessage();

	}
	

	/**
	 * Change le sujet courant pour celui par defaut.
	 */
	public void createDefaultSubject() {
		setSubject(SUBJECT_DEFAULT);
	}
	/**
	 * Change le sujet courant pour celui par defaut concernant un accuse d'execution de tache.
	 */
	public void createDefaultSubjectAccuseExcutionTache(Date instant) {
		prop_subject = new StringBuffer(SUBJECT_DEFAULT_ACCUSE_EXEC_TACHE);
		if (instant != null) {
			prop_subject.append(" - ");
			prop_subject.append(DateUtilities.print(instant, true, false));
		}
	}
	/**
	 * Change le sujet courant pour celui specifie.
	 * @param subject Nouveau sujet.
	 */
	public void setSubject(String subject) {
		if (subject == null)
			throw new IllegalArgumentException("Texte requis pour le sujet du mail.");
		prop_subject = new StringBuffer();
		prop_subject.append(subject);
	}

	/**
	 * Change l'entete du corps de message courante pour celle par defaut.
	 */
	public void createDefaultMessageHeader() {
		prop_message_header = new StringBuffer(MESSAGE_HEADER_DEFAULT);
	}
	/**
	 * Change l'entete du corps de message courante pour celle specifiee.
	 * @param header Nouvelle entete.
	 */
	public void setMessageHeader(String header) {
		if (header == null)
			throw new IllegalArgumentException("Texte requis pour le header du message.");
		prop_message_header = new StringBuffer(header);
	}

	/**
	 * Change le pied de page du corps de message courant pour celui par defaut.
	 */
	public void createDefaultMessageFooter() {
		prop_message_footer = new StringBuffer(MESSAGE_FOOTER_DEFAULT);
	}
	/**
	 * Change le pied de page du corps de message courane pour celui specifie.
	 * @param footer Nouveau pied de page.
	 */
	public void setMessageFooter(String footer) {
		if (footer == null)
			throw new IllegalArgumentException("Texte requis pour le footer du message.");
		prop_message_footer = new StringBuffer(footer);
	}

	/**
	 * Change le corps de message courant pour celui par defaut.
	 */
	public void createDefaultMessageBody() {
		prop_message_body = new StringBuffer();
		prop_message_body.append("Ceci est un mail envoyé par l'application de gestion des événements ");
		prop_message_body.append("car vous faites partie des personnes concernées par un événement.");
		prop_message_body.append("<br>");
		prop_message_body.append(EMPLACEMENT_DELAI_PREVENANCE);
		prop_message_body.append(EMPLACEMENT_INFOS_EVENEMENT);
		prop_message_body.append(EMPLACEMENT_INFOS_TACHE);
		prop_message_body.append(EMPLACEMENT_MESSAGE_PROCHAIN_MAIL);
		prop_message_body.append(EMPLACEMENT_MESSAGE_MISFIRED);
		prop_message_body.append(EMPLACEMENT_MESSAGE_PERSONNES_INJOIGNABLES);
	}
	/**
	 * Change le corps de message courant pour celui specifie.
	 * @param messageBody Nouveau corps de message du mail.
	 */
	public void setMessageBody(String messageBody) {
		if (messageBody == null)
			throw new IllegalArgumentException("Texte requis pour le body du message.");
		prop_message_body = new StringBuffer();
		prop_message_body.append(messageBody);
	}

	/**
	 * 
	 */
	protected void createDefaultMessage() {

		createDefaultMessageHeader();
		createDefaultMessageBody();
		createDefaultMessageFooter();

		prop_message = new StringBuffer();
		prop_message.append(EMPLACEMENT_MESSAGE_HEADER);
		prop_message.append("<br>");
		prop_message.append(EMPLACEMENT_MESSAGE_BODY);
		prop_message.append("<br>");
		prop_message.append(EMPLACEMENT_MESSAGE_FOOTER);
	}
	
	/**
	 * Precise les adresses des destinatires du mail.
	 * @param adressesMailDestinataires Adresses mails separees par des virgules. <code>null</code> interdit.
	 */
	public void setAdressesMailDestinataires(String adressesMailDestinataires) {
		if (adressesMailDestinataires == null)
			throw new IllegalArgumentException("Adresses des destinataires du mail requises.");
		this.prop_recipient = new StringBuffer();
		this.prop_recipient.append(adressesMailDestinataires);
	}
	/**
	 * Precise le delai de prevenance (decalage temporel par rapport a la date/heure prevue de l'evenement).
	 * @param delaiPrevenance Nouveau delai de prevenance. Ex: "5 minutes avant" ou <code>null</code>.
	 */
	public void setInfosDelaiPrevenance(String delaiPrevenance) {
		this.infosDelaiPrevenance = new StringBuffer();
		if (delaiPrevenance != null) {
			this.infosDelaiPrevenance.append("Il a été demandé de vous prévenir <b>");
			this.infosDelaiPrevenance.append(delaiPrevenance);
			this.infosDelaiPrevenance.append("</b> l'événement en question.");
			this.infosDelaiPrevenance.append("<br>");
		}
	}
	/**
	 * Precise les infos decrivant l'evenement concerne par l'envoi du mail.
	 * @param infosEvenement Description de l'evenement, ou <code>null</code>.
	 */
	public void setInfosEvenement(String infosEvenement) {
		this.infosEvenement = new StringBuffer();
		this.infosEvenement.append("Evénement qui vous concerne :");
		this.infosEvenement.append("<br>");
		this.infosEvenement.append("<code>");
		this.infosEvenement.append(infosEvenement!=null ? infosEvenement : "Aucune info transmise concernant l'événement.");
		this.infosEvenement.append("</code>");
		this.infosEvenement.append("<br>");
	}
	/**
	 * Precise les infos decrivant la tache donnant lieu a l'envoi du mail.
	 * @param infosTache Description de la tache, ou <code>null</code>.
	 * @param misfired Indique si la date prevue d'execution de la tache a ete manquee. 
	 */
	public void setInfosTache(String infosTache, boolean misfired) {
		this.infosTache = new StringBuffer();
		if (infosTache != null) {
			this.infosTache.append("La tâche exécutée est la suivante :");
			this.infosTache.append("<br>");
			this.infosTache.append("<code>");
			this.infosTache.append(infosTache);
			this.infosTache.append("</code>");
			this.infosTache.append("<br>");
			if (misfired) {
				this.infosTache.append("<b><i>");
				this.infosTache.append("NB: l'exécution de cette tâche n'a pas été déclenchée à l'heure initialement prévue ");
				this.infosTache.append("(misfired trigger).");
				this.infosTache.append("</i></b>");
				this.infosTache.append("<br>");
			}
		}
	}
	/**
	 * Precise si le mail est envoye en retard.
	 * @param misfired <code>true</code> pour indique un retard, <code>false</code> sinon.
	 */
	public void setInfosMisfired(boolean misfired) {
		infosMisfired = new StringBuffer();
		if (misfired) {
			infosMisfired.append("<br>");
			infosMisfired.append("<b>NB: L'application de gestion des événements n'a pas été ");
			infosMisfired.append("en mesure de vous envoyer cette alerte par mail à l'heure prévue. ");
			infosMisfired.append("<br>");
			infosMisfired.append("Veuillez nous en excusez. </b>");
			infosMisfired.append("<br>");
		}
	}

	/**
	 * Precise la liste des personnes auxquelles ne pourra pas etre envoye le mail, 
	 * parce que leur adresse mail est inconnue par exemple.
	 * @param nomsPersonnes Liste de {@link String}s, eventuellement vide. <code>null</code> interdit.
	 */
	public void setInfosPersonnesInjoignables(NSArray nomsPersonnes) {
		if (nomsPersonnes == null)
			throw new IllegalArgumentException("Noms des destinataires injoignables requis.");
		infosPersonnesInjoignables = new StringBuffer();
		if (nomsPersonnes.count() > 0) {
			infosPersonnesInjoignables.append("<br>");
			infosPersonnesInjoignables.append("NB: ");
			infosPersonnesInjoignables.append("les personnes concernées suivantes n'ont pu être ajoutées à la liste ");
			infosPersonnesInjoignables.append("des destinataires de ce mail car leur adresse électronique est inconnue :");
			infosPersonnesInjoignables.append("<br>");
			for (int i=0; i<nomsPersonnes.count(); i++) {
				infosPersonnesInjoignables.append("- ");
				infosPersonnesInjoignables.append(nomsPersonnes.objectAtIndex(i).toString());
				infosPersonnesInjoignables.append("<br>");
			}
		}
	}
	/**
	 * Precise la date de prochain envoi d'un mail. Cas des evenements periodiques.
	 * @param dateProchainMail Date du prochain envoi de mail.
	 */
	public void setInfosProchainMail(Date dateProchainMail) {
		infosProchainMail = new StringBuffer();
		if (dateProchainMail != null) {
			infosProchainMail.append("Prochain mail: ");
			infosProchainMail.append(DateUtilities.print(dateProchainMail, true, true));
			infosProchainMail.append("<br>");
		}	
	}

	/**
	 * Genere une {@link Map} a partir des attributs courants de cet objet, a utiliser pour
	 * remplir une {@link JobDataMap} pour un envoi de mail.
	 * @return Une {@link HashMap}.
	 */
	public Map toPropertiesMap() {
		
		MessageFormat mf = new MessageFormat(prop_message_body.toString().replaceAll("'", "''"));
		String body = mf.format(
				new Object[] { 
					infosDelaiPrevenance, 		//0
					infosEvenement, 			//1
					infosTache, 				//2
					infosProchainMail,			//5
					infosMisfired, 				//3
					infosPersonnesInjoignables	//4
					
		});
		
//		System.out.println("MailComposer.toPropertiesMap() prop_message_body = "+prop_message_body);
//		System.out.println("MailComposer.toPropertiesMap() body = "+body);
		
		mf = new MessageFormat(prop_message.toString().replaceAll("'", "''"));
		String message = mf.format(
				new Object[] { 
					prop_message_header, 	//0
					body, 					//1
					prop_message_footer 	//2
		});
//		System.out.println("MailComposer.toPropertiesMap() prop_message = "+prop_message);
//		System.out.println("MailComposer.toPropertiesMap() message = "+message);
		
		HashMap map = new HashMap();
		
		map.put(PROP_SMTP_HOST, prop_smtp_host.toString());
		map.put(PROP_SENDER, prop_sender.toString());
		map.put(PROP_REPLY_TO, prop_reply_to.toString());
		map.put(PROP_CONTENT_TYPE, prop_content_type.toString());
		
		map.put(PROP_SUBJECT, prop_subject.toString());
		map.put(PROP_RECIPIENT, prop_recipient.toString());
		map.put(PROP_CC_RECIPIENT, prop_cc_recipient.toString());
		
		map.put(PROP_MESSAGE, "<html>"+message+"</html>");
		
		return map;
	}


	/**
	 * Cet objet au format texte pour debug.
	 */
	public String toString() {
		
		StringBuffer buf = new StringBuffer();
		
		buf.append("MailComposer {\n");
		
		buf.append("adressesMailDestinataires = ");
		buf.append(adressesMailDestinataires);
		buf.append("\n");
		buf.append("infosDelaiPrevenance = ");
		buf.append(infosDelaiPrevenance);
		buf.append("\n");
		buf.append("infosEvenement = ");
		buf.append(infosEvenement);
		buf.append("\n");
		buf.append("infosTache = ");
		buf.append(infosTache);
		buf.append("\n");
		buf.append("infosMisfired = ");
		buf.append(infosMisfired);
		buf.append("\n");
		buf.append("infosPersonnesInjoignables = ");
		buf.append(infosPersonnesInjoignables);
		buf.append("\n");
		
		buf.append("prop_recipient = ");
		buf.append(prop_recipient);
		buf.append("\n");
		buf.append("prop_cc_recipient = ");
		buf.append(prop_cc_recipient);
		buf.append("\n");
		buf.append("prop_subject = ");
		buf.append(prop_subject);
		buf.append("\n");
		buf.append("prop_reply_to = ");
		buf.append(prop_reply_to);
		buf.append("\n");
		buf.append("prop_sender = ");
		buf.append(prop_sender);
		buf.append("\n");
		buf.append("prop_smtp_host = ");
		buf.append(prop_smtp_host);
		buf.append("\n");
		buf.append("prop_content_type = ");
		buf.append(prop_content_type);
		buf.append("\n");
		buf.append("prop_message_header = ");
		buf.append(prop_message_header);
		buf.append("\n");
		buf.append("prop_message_body = ");
		buf.append(prop_message_body);
		buf.append("\n");
		buf.append("prop_message_footer = ");
		buf.append(prop_message_footer);
		buf.append("\n");
		buf.append("prop_recipient = ");
		buf.append(prop_message);
		buf.append("\n");
		buf.append("} \n");

		return buf.toString();
	}

	
	
	public static void main(String[] args) {
		MessageFormat mf = new MessageFormat(
				"Ceci est un mail envoyé par l''application de gestion des évènements car vous faites partie des " +
				"personnes concernées par un évènement.<br>{0}<br><br>{1}<br>{2}<br>{3}<br>{4}{5}");
		String res = mf.format(new Object[] { "1", "2", "3", "4", "5", new StringBuffer("6") });
		System.out.println("MailComposer.main() res = "+res);
	}
}
