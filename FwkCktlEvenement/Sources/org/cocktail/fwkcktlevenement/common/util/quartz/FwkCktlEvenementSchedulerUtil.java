/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.common.util.quartz;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTache;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.util.JobDetailUtilities;
import org.cocktail.fwkcktlevenement.serveur.quartz.trigger.util.TriggerUtilities;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * Classe regroupant les methodes utiles à la manipulation du framework Quartz et de son scheduler.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class FwkCktlEvenementSchedulerUtil {

    /**
     * Démarre le scheduler.
     */
    public static Boolean startScheduler(Scheduler scheduler) throws ExceptionOperationImpossible {

        if (scheduler == null)
            throw new IllegalStateException("Le scheduler est null.");

        try {
            if (!scheduler.isStarted())
                scheduler.start();

            FwkCktlEvenementUtil.LOG.info("------- "+new Date()+" : Scheduler demarre -----------------");

            return new Boolean(scheduler.isStarted());
        } 
        catch (SchedulerException e) {
            throw new ExceptionOperationImpossible("Impossible de demarrer le scheduler existant.", e);
        }
    }

    /**
     * Met en pause le scheduler.
     * @param scheduler
     * @return vrai si le scheduler a été mis en pause.
     * @throws ExceptionOperationImpossible
     */
    public static Boolean pauseScheduler(Scheduler scheduler) throws ExceptionOperationImpossible {
        try {
            if (!scheduler.isInStandbyMode())
                scheduler.standby();

            FwkCktlEvenementUtil.LOG.info("------- "+new Date()+" : Scheduler mis en pause -----------------");

            return new Boolean(scheduler.isInStandbyMode());
        } 
        catch (SchedulerException e) {
            throw new ExceptionOperationImpossible("Impossible de mettre en pause le scheduler existant.", e);
        }
    }

    /**
     * @param evenement
     * @param requestTime
     * @param scheduler
     * @param editingContext
     * @throws ExceptionOperationImpossible
     */
    public static void programmerTachesEvenement(
            EOEvenement evenement, 
            NSTimestamp requestTime,
            Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        verifierScheduler(scheduler);
        FwkCktlEvenementUtil.invalidateObjects(editingContext, new NSArray(evenement));// indispensable!!
        FwkCktlEvenementUtil.invalidateObjects(editingContext, evenement.frequencesCrons());// indispensable!!
        FwkCktlEvenementUtil.invalidateObjects(editingContext, evenement.frequencesSimples());// indispensable!!
        FwkCktlEvenementUtil.invalidateObjects(editingContext, evenement.taches());// indispensable!!
        // deprogrammation des taches necessaires
        deprogrammerTaches(evenement.taches(), false, scheduler, editingContext);
        // programmation des taches necessaires
        NSMutableArray tachesProgrammer = new NSMutableArray(evenement.taches());
        programmerTaches(evenement.taches(), requestTime, null, scheduler, editingContext);
    }

    /**
     * @param tache
     * @param requestTime
     * @param extraJobData
     * @param scheduler
     * @param editingContext
     * @throws ExceptionOperationImpossible
     */
    public static void programmerTache(
            EOTache tache, 
            NSTimestamp requestTime, 
            Map extraJobData,
            Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        // Verification du scheduler
        verifierScheduler(scheduler);
        NSMutableArray triggers = new NSMutableArray();
        String tachId = tache.tachIdReadOnly() != null ? tache.tachIdReadOnly().toString() : "TMP";
        // tache de type ALERTE MAIL : programmee une seule fois (car le job associe se reprogrammera lui-meme si necessaire)
        if (tache.type().idInterne().equals(EOTypeTache.ID_INTERNE_ALERTE_MAIL)) {
            Trigger trigger = programmerTacheAlerteMailSiNecessaire(tache, requestTime, scheduler);
            if (trigger != null) {
                triggers.addObject(TriggerUtilities.dictionaryFromTrigger(trigger));
                FwkCktlEvenementUtil.LOG.info("La tâche d'envoi d'alerte mail suivante a été programmée dans le scheduler :\n"+tache.toFullString());
                FwkCktlEvenementUtil.debugPrintArray("Les triggers créés pour cela sont les suivants", triggers, FwkCktlEvenementUtil.LOG);
            }
            else {
                FwkCktlEvenementUtil.LOG.info("La tâche d'envoi d'alerte mail suivante n'a pas eu besoin d'être programmée dans le scheduler :\n"+tache.toFullString());
            }
        } 
        // autres types de tache: autant de triggers programmes qu'il y a de frequences associees a l'evenement
        else {
            JobDetail jobDetail = addJobIfNecessary(tache, scheduler);
            NSArray frequencesCrons = tache.evenement().frequencesCrons();
            NSArray frequencesSimples = tache.evenement().frequencesSimples();
            FwkCktlEvenementUtil.debugPrintArrayInLog("Frequences CRON trouvees pour la tâche "+tachId, frequencesCrons);
            FwkCktlEvenementUtil.debugPrintArrayInLog("Frequences SIMPLE trouvees pour la tâche "+tachId, frequencesSimples);
            if (extraJobData != null) {
                jobDetail.getJobDataMap().putAll(extraJobData);
            }
            // programmation des triggers CRON et simples
            if (frequencesCrons.count() + frequencesSimples.count() > 0) {
                // CRON
                for (int i=0; i<frequencesCrons.count(); i++) {
                    EOFrequenceCron frequenceCron = (EOFrequenceCron) frequencesCrons.objectAtIndex(i);
                    Date nextFireTime = EOFrequenceCron.getOccurenceApres(new NSArray(frequenceCron), requestTime);
                    FwkCktlEvenementUtil.debugPrintRecordInLog("frequenceCron "+i, frequenceCron);
                    FwkCktlEvenementUtil.LOG.info("FwkCktlEvenementSchedulerUtil.programmerTaches() nextFireTime ce cette freq cron = "+nextFireTime);
                    if (nextFireTime != null) {
                        CronTrigger trigger;
                        try {
                            trigger = TriggerUtilities.createCronTrigger(
                                    frequenceCron,
                                    null, //getTimeZone(),
                                    requestTime,
                                    "TRIGGER_"+(i+1)+"_TACH_ID="+tachId,
                                    tache.groupForJobDetail(),
                                    tache.nameForJobDetail(),
                                    EOTache.TACH_ID_KEY+"="+tachId,
                                    tache.triggerOffsetInMinutes());
                        } 
                        catch (ParseException e) {
                            throw new ExceptionOperationImpossible(
                                    "L'expression CRON semble erronee: "+frequenceCron.cronExpression(), e);
                        }
                        try {
                            scheduler.scheduleJob(trigger);
                            triggers.addObject(TriggerUtilities.dictionaryFromTrigger(trigger));
                            // maj flag de programmation
                            tache.setEtat(EOTache.ETAT_PROGRAMMEE);
                            tache.editingContext().saveChanges();
                            FwkCktlEvenementUtil.LOG.info("La tache suivante a ete programmee dans le scheduler :\n"+tache.toFullString());
                            FwkCktlEvenementUtil.debugPrintArrayInLog("Les triggers crees pour cela sont les suivants", triggers);
                        } 
                        catch (SchedulerException e) {
                            throw new ExceptionOperationImpossible(
                                    "Impossible d'ajouter le trigger suivant au scheduler: "+trigger, e);
                        }
                    }
                }
                // Simple
                for (int i=0; i<frequencesSimples.count(); i++) {
                    EOFrequenceSimple frequenceSimple = (EOFrequenceSimple) frequencesSimples.objectAtIndex(i);
                    Date nextFireTime = EOFrequenceSimple.getOccurenceApres(new NSArray(frequenceSimple), requestTime);
                    FwkCktlEvenementUtil.debugPrintRecordInLog("frequenceSimple "+i, frequenceSimple);
                    FwkCktlEvenementUtil.LOG.info("FwkCktlEvenementSchedulerUtil.programmerTaches() nextFireTime ce cette freq simple = "+nextFireTime);
                    // si occurence suivante trouvee, programmation necessaire
                    if (nextFireTime != null) {
                        SimpleTrigger trigger = TriggerUtilities.createSimpleTrigger(
                                frequenceSimple, 
                                nextFireTime,
                                "TRIGGER_"+(frequencesSimples.count()+(i+1))+"_TACH_ID="+tachId,
                                tache.groupForJobDetail(),
                                tache.nameForJobDetail(),
                                EOTache.TACH_ID_KEY+"="+tachId,
                                tache.triggerOffsetInMinutes());
                        try {
                            scheduler.scheduleJob(trigger);
                            triggers.addObject(TriggerUtilities.dictionaryFromTrigger(trigger));
                            // maj flag de programmation
                            tache.setEtat(EOTache.ETAT_PROGRAMMEE);
                            tache.editingContext().saveChanges();
                            FwkCktlEvenementUtil.LOG.info("La tache suivante a ete programmee dans le scheduler :\n"+tache.toFullString());
                            FwkCktlEvenementUtil.debugPrintArrayInLog("Les triggers crees pour cela sont les suivants", triggers);
                        }
                        catch (SchedulerException e) {
                            throw new ExceptionOperationImpossible(
                                    "Impossible d'ajouter le trigger suivant au scheduler: "+trigger, e);
                        }
                    }
                }
            }
            // aucune frequence, programmation d'un trigger a declenchement unique
            else {
                Date nextFireTime = tache.getOccurenceTacheApres(DateUtilities.decaler(requestTime,-1,Calendar.MILLISECOND));
                FwkCktlEvenementUtil.LOG.info("FwkCktlEvenementSchedulerUtil.programmerTaches() nextFireTime sans freq = "+nextFireTime);
                // si occurence suivante trouvee, programmation necessaire
                if (nextFireTime != null) {
                    SimpleTrigger trigger = TriggerUtilities.createOneShotSimpleTrigger(
                            nextFireTime,
                            "TRIGGER_"+(frequencesCrons.count()+frequencesSimples.count()+1)+"_TACH_ID="+tachId,
                            tache.groupForJobDetail(),
                            tache.nameForJobDetail(),
                            EOTache.TACH_ID_KEY+"="+tachId,
                            tache.triggerOffsetInMinutes());
                    try {
                        scheduler.scheduleJob(trigger);

                        triggers.addObject(TriggerUtilities.dictionaryFromTrigger(trigger));

                        // maj flag de programmation
                        tache.setEtat(EOTache.ETAT_PROGRAMMEE);
                        tache.editingContext().saveChanges();

                        FwkCktlEvenementUtil.LOG.info("La tache suivante a ete programmee dans le scheduler :\n"+tache.toFullString());
                        FwkCktlEvenementUtil.debugPrintArrayInLog("Les triggers crees pour cela sont les suivants", triggers);
                    }
                    catch (SchedulerException e) {
                        throw new ExceptionOperationImpossible(
                                "Impossible d'ajouter le trigger suivant au scheduler: "+trigger, e);
                    }
                }

            }
        }

    }

    /**
     * @param taches
     * @param requestTime
     * @param extraJobData
     * @param scheduler
     * @param editingContext
     * @throws ExceptionOperationImpossible
     */
    public static void programmerTaches(
            NSArray<EOTache> taches, 
            NSTimestamp requestTime, 
            Map extraJobData,
            Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible { 
        for (EOTache tache : taches) {
            programmerTache(tache, requestTime, extraJobData, scheduler, editingContext);
        }
    }

    /**
     * @param tache
     * @param verifExist
     * @param scheduler
     * @param editingContext
     * @throws ExceptionOperationImpossible
     */
    public static void deprogrammerTache(
            EOTache tache, 
            boolean verifExist,
            Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        // test presence eventuel de la tâche dans le scheduler
        if (verifExist) {
            Boolean exists = tacheExisteDansScheduler(tache, false, scheduler, editingContext);
            if (!exists.booleanValue()) {
                throw new ExceptionOperationImpossible(
                        "La tâche à déprogrammer suivante n'existe pas dans le scheduler: "+tache);
            }
        }
        boolean successForAllTriggers = true;
        // triggers associes a la tache
        NSArray triggers = getTriggersOfJob(tache, scheduler);
        for (int i = 0; i < triggers.count(); i++) {
            Trigger trigger = (Trigger) triggers.objectAtIndex(i);
            try {
                successForAllTriggers &= unscheduleJob(trigger.getName(), trigger.getGroup(), scheduler);
            } 
            catch (SchedulerException e) {
                throw new ExceptionOperationImpossible("Impossible d'arrêter le trigger suivant: "+trigger, e);
            }
        }
        if (successForAllTriggers) {
            if (!tache.isDeletedEO()) {
                tache.setEtat(EOTache.ETAT_NON_PROGRAMMEE);
                tache.editingContext().saveChanges();
            }
            FwkCktlEvenementUtil.LOG.info("Deprogrammation complete reussie: "+triggers.count()+" triggers arretes.");
            FwkCktlEvenementUtil.debugPrintArrayInLog("Triggers arretes", triggers);
        }
        else 
            FwkCktlEvenementUtil.LOG.info("Deprogrammation non reussie.");

        FwkCktlEvenementUtil.LOG.info("----");
    }


    /**
     * @param taches
     * @param verifExist
     * @param scheduler
     * @param editingContext
     * @throws ExceptionOperationImpossible
     */
    public static void deprogrammerTaches(
            NSArray<EOTache> taches, 
            boolean verifExist,
            Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        for (EOTache tache : taches) {
            deprogrammerTache(tache, verifExist, scheduler, editingContext);
        }
    }

    /**
     * @param taches
     * @param requestTime
     * @param extraJobData
     * @param scheduler
     * @param editingContext
     * @throws ExceptionOperationImpossible
     */
    public static void reprogrammerTaches(
            NSArray<EOTache> taches,
            NSTimestamp requestTime, 
            Map extraJobData,
            Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        FwkCktlEvenementUtil.LOG.info("REPROGRAMMATION des tâches suivantes dans le scheduler: "+taches.componentsJoinedByString(", "));
        deprogrammerTaches(taches, false, scheduler, editingContext);
        programmerTaches(taches, requestTime, extraJobData, scheduler, editingContext);
    }

    /**
     * @param tache
     * @param andScheduled
     * @param scheduler
     * @param editingContext
     * @return Vrai si la tache est programmée dans le scheduler
     * @throws ExceptionOperationImpossible
     */
    public static Boolean tacheExisteDansScheduler(EOTache tache, boolean andScheduled, Scheduler scheduler,
            EOEditingContext editingContext) throws ExceptionOperationImpossible {
        FwkCktlEvenementUtil.LOG.info("TEST DE PRESENCE ");
        FwkCktlEvenementUtil.LOG.info(andScheduled ? "PROGRAMMEE " : "NON PROGRAMMEE ");
        FwkCktlEvenementUtil.LOG.info("de la tache suivante dans le scheduler: " + tache.tachIdReadOnly());

        Boolean resultat;
        if (andScheduled) {
            try {
                Trigger[] triggers = scheduler.getTriggersOfJob(tache.nameForJobDetail(), tache.groupForJobDetail());
                resultat = new Boolean(triggers != null && triggers.length > 0);
            } catch (SchedulerException e) {
                throw new ExceptionOperationImpossible(
                        "Impossible d'obtenir les triggers associés à la tache suivant: " + tache.nameForJobDetail(), e);
            }
        } else {
            try {
                JobDetail detail = scheduler.getJobDetail(tache.nameForJobDetail(), tache.groupForJobDetail());
                resultat = new Boolean(detail != null);
            } catch (SchedulerException e) {
                throw new ExceptionOperationImpossible(
                        "Impossible d'obtenir les détails de la tâche suivant dans le scheduler: " + tache, e);
            }
        }
        return resultat;
    }

    /**
     * @param scheduler
     * @throws ExceptionOperationImpossible
     */
    protected static void verifierScheduler(Scheduler scheduler) throws ExceptionOperationImpossible {
        try {
            if (!scheduler.isStarted())
                throw new ExceptionOperationImpossible("Le scheduler n'est pas démarré.");
        } 
        catch (SchedulerException e1) {
            throw new ExceptionOperationImpossible("Impossible de savoir si le scheduler est démarré ou non.");
        }
    }

    /**
     * Programme la tache d'alerte mail specifiee si c'est necessaire (cad s'il existe une occurence
     * situee apres l'instant de demande de programmation).
     * @param tache Tache d'alerte mail a programmer.
     * @param requestTime Instant de la demande de programmation. 
     * Utilisee comme origine des temps pour chercher l'occurence suivante.
     * @return  Le {@link Trigger} cree pour cette programmation.
     * @throws ExceptionOperationImpossible
     */
    protected static Trigger programmerTacheAlerteMailSiNecessaire(
            EOTache tache, 
            Date requestTime,
            Scheduler scheduler) throws ExceptionOperationImpossible {

        if (!tache.estDuTypeAlerteMail())
            throw new IllegalArgumentException("La tâche spécifiée n'est pas du type "+EOTypeTache.ID_INTERNE_ALERTE_MAIL+".");

        Trigger trigger=null;

        int triggerOffsetInMinutes = tache.triggerOffsetInMinutes()!=null ? tache.triggerOffsetInMinutes().intValue() : 0;

        Date alerteMailStartTime =
            tache.getOccurenceTacheApres(DateUtilities.decaler(requestTime, -1, Calendar.MILLISECOND));

        FwkCktlEvenementUtil.LOG.info("FwkCktlEvenementSchedulerUtil.scheduleJobs() requestTime = "+requestTime);
        FwkCktlEvenementUtil.LOG.info("FwkCktlEvenementSchedulerUtil.scheduleJobs() triggerOffsetInMinutes = "+triggerOffsetInMinutes);
        FwkCktlEvenementUtil.LOG.info("FwkCktlEvenementSchedulerUtil.scheduleJobs() alerteMailStartTime = "+alerteMailStartTime);

        if (alerteMailStartTime!=null /*&& !alerteMailStartTime.before(now)*/) {

            JobDetail jobdetail = JobDetailUtilities.createJobDetail(tache);
            trigger = TriggerUtilities.createTriggerForTacheAlerteMail(tache);
            //			Debug.printMap("jobdetail.getJobDataMap()", jobdetail.getJobDataMap());

            trigger.setStartTime(alerteMailStartTime);

            try {
                scheduler.deleteJob(jobdetail.getName(), jobdetail.getGroup());
                if (scheduler.getTrigger(trigger.getName(), trigger.getGroup()) == null) 
                    scheduler.scheduleJob(jobdetail, trigger);
                else
                    scheduler.rescheduleJob(trigger.getName(), trigger.getGroup(), trigger);

                // maj flag de programmation
                tache.setEtat(EOTache.ETAT_PROGRAMMEE);
                tache.editingContext().saveChanges();
            }
            catch (SchedulerException e) {
                throw new ExceptionOperationImpossible(
                        "Impossible de programmer la tâche d'envoi d'alerte mail dans le scheduler.", 
                        e);
            }
        }

        return trigger;
    }

    /**
     * 
     * @param tache
     * @return
     * @throws ExceptionOperationImpossible 
     */
    private static JobDetail addJobIfNecessary(EOTache tache, Scheduler scheduler) throws ExceptionOperationImpossible {

        if (tache == null)
            throw new NullPointerException("Tache requise.");

        JobDetail jobdetail;
        try {
            jobdetail = scheduler.getJobDetail(tache.nameForJobDetail(), tache.groupForJobDetail());
        } 
        catch (SchedulerException e) {	
            throw new ExceptionOperationImpossible(
                    "Impossible de récupérer le JobDetail correspondant à la tâche suivante: "+tache, e);
        }

        if (jobdetail == null) { // job detail introuvable
            jobdetail = JobDetailUtilities.createJobDetail(tache);
        }

        try {
            scheduler.addJob(jobdetail, true);
        } 
        catch (SchedulerException e1) {
            throw new ExceptionOperationImpossible(
                    "Impossible d'inscrire le JobDetail pour la tâche suivante: "+tache, e1);
        }

        return jobdetail;
    }

    /**
     * 
     * @param triggerName
     * @param groupName
     * @return
     * @throws SchedulerException
     */
    protected static boolean unscheduleJob(String triggerName, 
            String groupName, Scheduler scheduler) throws SchedulerException {
        FwkCktlEvenementUtil.LOG.info("Arret du trigger "+groupName+"."+triggerName+"...");
        boolean ft = scheduler.unscheduleJob(triggerName, groupName);
        FwkCktlEvenementUtil.LOG.info(ft ? "OK" : "NOK");
        return ft;
    }
    /**
     * 
     * @param jobName
     * @param groupName
     * @return
     * @throws SchedulerException
     */
    protected static boolean deleteJob(String jobName, String groupName, 
            Scheduler scheduler) throws SchedulerException {
        FwkCktlEvenementUtil.LOG.info("Suppression de la tâche "+jobName+"...");
        boolean ret = scheduler.deleteJob(jobName, groupName);
        FwkCktlEvenementUtil.LOG.info(ret ? "OK" : "NOK");
        return ret;
    }

    /**
     * Retourn la liste des triggers associes au tache specifie.
     * @param tache
     * @return Une liste d'objets de type {@link Trigger}.
     * @throws ExceptionOperationImpossible 
     */
    public static NSArray getTriggersOfJob(EOTache tache, 
            Scheduler scheduler) throws ExceptionOperationImpossible {

        if (tache == null) 
            throw new NullPointerException("Job requis.");

        Trigger[] triggers;
        try {
            triggers = scheduler.getTriggersOfJob(tache.nameForJobDetail(), tache.groupForJobDetail());
        } 
        catch (SchedulerException e) {
            throw new ExceptionOperationImpossible(
                    "Impossible d'obtenir la liste des triggers de la tâche suivante: "+tache, e);
        }
        if (triggers != null)
            return new NSArray(triggers);
        else 
            return new NSArray();
    }
    
//    /**
//     * @param tache
//     * @throws ExceptionOperationImpossible 
//     */
//    public static void setMisFiredForTriggersOfJob(EOTache tache, 
//            Scheduler scheduler) throws ExceptionOperationImpossible {
//
//        if (tache == null) 
//            throw new NullPointerException("Job requis.");
//
//        NSArray<Trigger> triggers = (NSArray<Trigger>)getTriggersOfJob(tache,scheduler);
//        if (!triggers.isEmpty()) {
//        	for (Trigger trigger : triggers) {
//				trigger.set
//			}
//		}
//    }

    /**
     */
    public static NSArray<Trigger> triggersProgrammes(Scheduler scheduler) throws ExceptionOperationImpossible {

        NSMutableArray<Trigger> array = new NSMutableArray<Trigger>();
        String[] triggerGroups;
        String[] triggersInGroup;
        int i;
        int j;
        try {
            triggerGroups = scheduler.getTriggerGroupNames();
            for (i = 0; i < triggerGroups.length; i++) {
                triggersInGroup = scheduler.getTriggerNames(triggerGroups[i]);
                for (j = 0; j < triggersInGroup.length; j++) {
                    Trigger trigger = scheduler.getTrigger(triggersInGroup[j], triggerGroups[i]);
                    array.addObject(trigger);
                }
            }
            return array.immutableClone();
        }
        catch (SchedulerException e) {
            throw new ExceptionOperationImpossible("Problème avec le scheduler.", e);
        }
    }


}
