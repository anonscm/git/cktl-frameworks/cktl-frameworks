/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.impl;

import org.cocktail.fwkcktlevenement.FwkCktlEvenementPrincipal;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTache;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.JobEvenement;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.util.JobDetailUtilities;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSData;


/**
 * Implementation standard de l'interface {@link JobEvenement}
 * Toute classe destinée à être executée comme job du framework evenement, doit hériter de cette classe et implémenter
 * la methode executeJob().
 * <br/>
 * Par ailleurs, la methode intialize() peut être surchargée dans le cas où certains traitements propres à l'environnement
 * de job doivent être fait. Cette methode initialize() est appelée avant executeJob().
 * <br/>
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 */
public abstract class JobEvenementImpl implements JobEvenement 
{
	public final static String KEY_FOR_MISFIRED = "MISFIRED";
	public final static Integer VALUE_FOR_MISFIRED = new Integer(1);
	
	transient private JobExecutionContext jobexecutioncontext;
	transient private EOEvenement evenement;
	transient private EOTache tache;
	transient private boolean misfired;
	
	transient private NSData resultat;
	
	
	/**
	 * Constructeur par defaut obligatoire.
	 */
	public JobEvenementImpl() {
		
	}

	/**
	 * 
	 * @throws ExceptionOperationImpossible
	 */
	protected void initialize() throws ExceptionOperationImpossible {

		// fetch job et evenement associes
		Number tachId = (Number) getJobexecutioncontext().getMergedJobDataMap().get(EOTache.TACH_ID_KEY);
		if (tachId == null)
			throw new NullPointerException(
					"Référence du Job introuvable dans la MergedJobDataMap. Récupération de l'objet métier impossible.");
        EOEditingContext ec = FwkCktlEvenementPrincipal.instance().newEditingContextForJobs();
        ec.lock();
		try {
			setTache(EOTache.findWithTachId(tachId, ec));
		} 
		catch (Exception e) {
			throw new ExceptionOperationImpossible("Récupération de l'objet métier Job impossible.", e);
		} finally {
		    ec.unlock();
		}
		
		// misfired ?
		Number misfiredValue = 
			(Number) getJobexecutioncontext().getMergedJobDataMap().get(KEY_FOR_MISFIRED);
		if (misfiredValue != null)
			setMisfired(misfiredValue.equals(VALUE_FOR_MISFIRED));
		
	}
	
	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public void execute(JobExecutionContext jobexecutioncontext) throws JobExecutionException {

		setJobexecutioncontext(jobexecutioncontext);

		try {
			initialize();
			resultat = executeJob(jobexecutioncontext);
		}
		catch (ExceptionOperationImpossible e) {
			FwkCktlEvenementUtil.LOG.error("Une erreur s'est produite pendant l'execution du job: "+this+" Message: "+e.getMessage(), e);
			throw new JobExecutionException("Une erreur s'est produite pendant l'execution du job: "+this+" Message: "+e.getMessage(), e);
		}

		// Un fichier resultat peut eventuellement avoir ete transmis par la tache externe.
		// S'il a ete demande de le recuperer, on le GEDifie et l'ajoute a l'evt.
//        if (getTache().tachExterneNomFichierResult()!=null && !"".equals(getTache().tachExterneNomFichierResult()) 
//        		&& resultat!=null) {
//        	
//	    	EOEvenement evenement = tache.evenement();
//	    	try {
//	        	GedTool gt = new GedTool(FwkCktlEvenementPrincipal.instance().newEditingContextForJobs());
////	        	Integer persIdApplication = 
////	        		new Integer(getApplication().getFacadeEvenementApplicationConfigServer().persIdAppClienteGed(true).intValue()); 
//	        	// TODO : récupérer le param PERS_ID_DEPOSANT_GED
//	        	Integer persIdApplication = null;
////	        		getJobexecutioncontext().get(ConfigEvenementInterface.PERS_ID_DEPOSANT_GED); 
//	        	String fileName = 
//	        		getTache().tachExterneNomFichierResult().substring(0, getTache().tachExterneNomFichierResult().indexOf('.')) +
//	        		"_" +
//	        		DateUtilities.printForFileName(new Date(), true) +
//	        		getTache().tachExterneNomFichierResult().substring(getTache().tachExterneNomFichierResult().indexOf('.'));
//	        	// des sous-dossiers seront crees dans la GED : "Evenement X/Tache Y/fichier"
//	        	String path = 
//	        		"Evenement "+getTache().evenement().evtIdReadOnly()+"/"+
//	        		"Tache "+getTache().tachIdReadOnly()+"/"+
//	        		fileName;
//	        	System.out.println("JobEvenementImpl.execute() path GED fichier resultat = "+path);
//	        	// le fichier est depose dans le dossier specifie par le parametre TYPE_ROOT_GED
//                // TODO : récupérer le param TYPE_ROOT_GED
////	        	gt.setRootGed(""+getApplication().getFacadeEvenementApplicationConfigServer().typeRootGed(true));
////	        	gt.setRootGed(""+getJobexecutioncontext().get(ConfigEvenementInterface.TYPE_ROOT_GED));
//
//	        	gt.setVisibilite(new Integer(0));
//	        	Integer couNumero = gt.saveFileIntoGed(
//	        			resultat, 
//	        			path, 
//	        			persIdApplication);
//	        	
////				FacadeEvenement facadeEvenement = new FacadeEvenement(tache.editingContext(), persIdApplication);
//				FwkCktlEvenementUtil.ajouterDocument(evenement, couNumero, tache.editingContext());
////				facadeEvenement.enregistrer();
//				tache.editingContext().saveChanges();
//			}
//	    	catch (Exception e) {
//	    		ExceptionOperationImpossible eoi = new ExceptionOperationImpossible(
//	    				"Depot du fichier resultat '"+getTache().tachExterneNomFichierResult()+"' impossible.", 
//	    				e);
//				eoi.printStackTrace();
//				FwkCktlEvenementUtil.LOG.warn("Depot du fichier resultat '"+getTache().tachExterneNomFichierResult()+"' impossible.", eoi);
//			}
//        }
		
		// envoi eventuel d'un mail : accuse d'execution
		if (getTache().sendMailAfterExeBoolean()) {
			try {
				scheduleEnvoiMailAccuseExecution(jobexecutioncontext);
			} 
			catch (ExceptionOperationImpossible e) {
				FwkCktlEvenementUtil.LOG.warn("Erreur lors de l'envoie du mail d'accusé d'execution du job: "+this, e);
			}
		}
	}
	
//	/**
//	 * @see org.cocktail.evenement.quartz.serveur.job.JobEvenement#executeJob(org.quartz.JobExecutionContext)
//	 */
//	public NSData executeJob(JobExecutionContext jobexecutioncontext) throws JobExecutionException {
//		
//		// 
//		// l'implementation par defaut se tourne les pouces!
//		//
//		
//		return null;
//	}
//	
	/**
	 * Programme l'envoi d'un mail d'accuse d'execution.
	 * @param jobexecutioncontext Contexte d'execution.
	 * @throws ExceptionOperationImpossible 
	 */
	protected void scheduleEnvoiMailAccuseExecution(JobExecutionContext jobexecutioncontext) throws ExceptionOperationImpossible  {
        
		JobDetail jobdetail = JobDetailUtilities.createJobDetailAccuseExecution(getTache(), isMisfired());
		
		// programmer la tâche d'envoi de mail avec un trigger volatile
		Scheduler scheduler = jobexecutioncontext.getScheduler();
		Trigger trigger = TriggerUtils.makeImmediateTrigger(0, 0);
		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		trigger.setName(jobdetail.getName());
		trigger.setJobGroup(Scheduler.DEFAULT_GROUP);
		trigger.setJobName(jobdetail.getName());
		trigger.setVolatility(false);
		try {
			scheduler.addJob(jobdetail, true);
			
			if (scheduler.getTrigger(trigger.getName(), trigger.getGroup()) == null) 
				scheduler.scheduleJob(trigger);
			else
				scheduler.rescheduleJob(trigger.getName(), trigger.getGroup(), trigger);
		} 
		catch (SchedulerException e) {
			throw new ExceptionOperationImpossible(
					"Impossible de programmer la tâche '"+jobdetail.getFullName()+"' " +
					"(accuse d'execution de la tâche '"+jobexecutioncontext.getJobDetail().getFullName()+"') " +
					"dans le scheduler.", 
					e);
		}
	}

	/**
	 * Change la tache associee a ce job
	 * @param evenement La nouvelle tache associee a ce job
	 */
	protected void setTache(EOTache tache) {
		this.tache = tache;
		if (tache.evenement() == null)
			throw new NullPointerException("Anomalie: la tâche n'est associée à aucun événement.");
		this.evenement = tache.evenement();
	}
	/**
	 * Fournit la tache associee a ce job.
	 * @return La tache associee a ce job.
	 */
	protected EOTache getTache() {
		return tache;
	}

	/**
	 * Fournit l'evenement associe a ce job.
	 * @return L'evenement associe a ce job.
	 */
	protected EOEvenement getEvenement() {
		return evenement;
	}

	/**
	 * Retourne le contexte d'execution courant.
	 * @return Le contexte d'execution courant
	 */
	protected JobExecutionContext getJobexecutioncontext() {
		return this.jobexecutioncontext;
	}
	/**
	 * Change le contexte d'execution courant.
	 * @param jobexecutioncontext Le nouveau contexte d'execution.
	 */
	protected void setJobexecutioncontext(JobExecutionContext jobexecutioncontext) {
		if (jobexecutioncontext == null) 
			throw new NullPointerException("Contexte d'exécution requis.");
		this.jobexecutioncontext = jobexecutioncontext;
	}

	/**
	 * Retourne la valeur courante de l'attribut misfired
	 * @return La valeur de l'attribut misfired
	 */
	protected boolean isMisfired() {
		return this.misfired;
	}
	/**
	 * Change la valeur de l'attribut misfired
	 * @param misfired Nouvelle valeur
	 */
	protected void setMisfired(boolean misfired) {
		this.misfired = misfired;
	}
	
}
