/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.impl;

import java.net.URL;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.webobjects.foundation.NSData;

import er.extensions.logging.ERXLogger;

/**
 * <p>Exemple de job d'application executable par le scheduler de l'application.
 * <p>Ce job preleve un fichier sur le disque dur (transmis comme fichier resultat d'execution). 
 * <p>Le fichier resultat est recupere dans {@link JobEvenementImpl} puis depose dans la GED et associe
 * a l'evenement concerne.
 * @see JobEvenementImpl
 */
public class JobGetOneFile extends JobEvenementImpl 
{
    private static Logger _log = ERXLogger.getLogger(JobGetOneFile.class);
    /**
     * URL du fichier résultat à retourner.
     * Aurait pu etre passee via le context.getMergedJobDataMap() plutot qu'en dur.
     */
    private static final String fileUrl = "file:///Users/bgauthie/PresentsAperitif.pdf";
    
    /**
     * Constructeur par defaut obligatoire.
     */
    public JobGetOneFile() {
    	_log.info("JobGetOneFile.JobGetOneFile() ");
    }

	/**
	 * @see JobEvenementImpl#executeJob(JobExecutionContext)
	 */
	public NSData executeJob(JobExecutionContext context) throws JobExecutionException {
		_log.info("JobGetOneFile.executeJob()");
        // on retourne un fichier resultat au format NSData
        NSData data=null;
		try {
			data = new NSData(new URL(fileUrl));
		}
		catch (Exception e) {
			throw new JobExecutionException("Erreur lors de la lecture du fichier '"+fileUrl+"'.", e);
		}
        return data;
	}

}