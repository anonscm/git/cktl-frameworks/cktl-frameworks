/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.MailComposer;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.JobEvenement;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.util.JobDetailUtilities;
import org.cocktail.fwkcktlevenement.serveur.quartz.trigger.util.TriggerUtilities;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXProperties;
import er.javamail.ERJavaMail;


/**
 * <p>Job qui envoie un mail aux personnes concernees par un evenement.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 */
public class JobAlerteMail extends JobEvenementImpl 
{
	transient private NSMutableArray personnesConcerneesSansAdresseMail = new NSMutableArray();
	
	private org.quartz.jobs.ee.mail.SendMailJob sendMailJobDelegate;
	private NSMutableArray adressesMailPersonnesConcernees;
	private Date mailNextFireTime;
	
	/**
	 * Constructeur.
	 */
	public JobAlerteMail() {
		setSendMailJobDelegate(new org.quartz.jobs.ee.mail.SendMailJob());
	}

	/**
	 * 
	 * @return
	 */
	private WOApplication getApplication() {
		return (WOApplication) WOApplication.application();
	}

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public void execute(JobExecutionContext jobexecutioncontext) throws JobExecutionException {
		super.execute(jobexecutioncontext);
	}

	/**
	 * @see JobEvenement#executeJob(JobExecutionContext)
	 */
	public NSData executeJob(JobExecutionContext jobexecutioncontext) throws JobExecutionException {
	    CktlWebApplication app = (CktlWebApplication)WOApplication.application();
		boolean useMail = app.useMail();
		if (useMail) {
			sendMail();
		}
		else {
			FwkCktlEvenementUtil.LOG.warn( 
					"L'envoi de mail n'est pas autorisé par l'application-daemon (paramètre '"
					+"APP_USE_MAIL"+"').");
		}
		
		try {
			rescheduleJob();
		}
		catch (ExceptionOperationImpossible e) {
			throw new JobExecutionException(
					"Impossible de reprogrammer la tâche d'alerte mail à cause d'une erreur.", 
					e); 
		} 
		
		// aucun fichier resultat retourne par ce type de tache
		return null;
	}
	
	/**
	 * Envoie le mail.
	 * @throws JobExecutionException
	 */
	protected void sendMail() throws JobExecutionException {
		getSendMailJobDelegate().execute(getJobexecutioncontext());
	}
	
	/**
	 * 
	 * @throws ExceptionOperationImpossible 
	 */
	protected void initialize() throws ExceptionOperationImpossible {
		super.initialize();
		
		// recherche des personnes concernees a alerter
		NSArray[] resutat = getEvenement().getMailsPersonnesConcernees(true);
		setAdressesMailPersonnesConcernees(new NSMutableArray(resutat[0]));
		setPersonnesConcerneesSansAdresseMail(new NSMutableArray(resutat[1]));

		computeNextFireTime();
		initializeMailContent();
	}

	/**
	 * 
	 */
	protected void initializeMailContent() {

	    CktlConfig conf = ((CktlWebApplication)WOApplication.application()).config();
	    
	    
	    
	    
		String grhumHostMail = ERXProperties.stringForKeyWithDefault("er.javamail.smtpHost", conf.stringForKey("GRHUM_HOST_MAIL"));
		String mailSender = conf.stringForKey("EVT_MAIL_SENDER");
		String mailReplyTo = conf.stringForKey("EVT_MAIL_REPLY_TO");
		
		MailComposer mailComposer = MailComposer.newInstance(grhumHostMail,	mailSender,	mailReplyTo); 
		
		String adressesMails = 
			getAdressesMailPersonnesConcernees().componentsJoinedByString(FwkCktlEvenementUtil.RECIPIENTS_SEPARATOR);

		mailComposer.setAdressesMailDestinataires(adressesMails);
		mailComposer.setInfosPersonnesInjoignables(getPersonnesConcerneesSansAdresseMail());
		mailComposer.setInfosEvenement(getEvenement().getHtmlDescriptionPourMail());
//		mailComposer.setInfosTache(getTache().getHtmlDescriptionPourMail()); pas interessant pour le commun des mortels!!
		mailComposer.setInfosDelaiPrevenance(getTache().triggerOffsetInMinutesToString());
		mailComposer.setInfosProchainMail(getMailNextFireTime());
		mailComposer.setInfosMisfired(isMisfired());
		Map map = mailComposer.toPropertiesMap();

		getJobexecutioncontext().getMergedJobDataMap().putAll(map);
	}
	
	/**
	 * Calcule la date du prochain declenchement de cette alerte
	 */
	protected void computeNextFireTime() {

		Date scheduledFireTime = getJobexecutioncontext().getScheduledFireTime();
		int triggerOffsetInMinutes = getTache().triggerOffsetInMinutes()!=null ? getTache().triggerOffsetInMinutes().intValue() : 0;
		
		setMailNextFireTime(null);
		Date evtNextFireTime = getEvenement().getOccurenceApres(
				DateUtilities.decaler(scheduledFireTime, -1*triggerOffsetInMinutes, Calendar.MINUTE));
		
		// s'il reste des occurences
		if (evtNextFireTime != null) {
			
			setMailNextFireTime(DateUtilities.decaler(evtNextFireTime, triggerOffsetInMinutes, Calendar.MINUTE));
	
//			System.out.println("JobAlerteMail.finishExecution() scheduledFireTime = "+scheduledFireTime);
//			System.out.println("JobAlerteMail.finishExecution() triggerOffsetInMinutes = "+triggerOffsetInMinutes);
//			System.out.println("JobAlerteMail.finishExecution() evtNextFireTime = "+evtNextFireTime);
//			System.out.println("JobAlerteMail.finishExecution() mailNextFireTime = "+getMailNextFireTime());
		}
	}
	
	/**
	 * 
	 * @throws ExceptionOperationImpossible
	 */
	protected void rescheduleJob() throws ExceptionOperationImpossible {

		if (getMailNextFireTime() != null) {

			Scheduler scheduler = getJobexecutioncontext().getScheduler();
			JobDetail jobdetail = JobDetailUtilities.createJobDetail(getTache());
			Trigger trigger = TriggerUtilities.createTriggerForTacheAlerteMail(getTache());//new SimpleTrigger();
	
			trigger.setStartTime(getMailNextFireTime());
	
			try {
				scheduler.deleteJob(jobdetail.getName(), jobdetail.getGroup());
				if (scheduler.getTrigger(trigger.getName(), trigger.getGroup()) == null) 
					scheduler.scheduleJob(jobdetail, trigger);
				else
					scheduler.rescheduleJob(trigger.getName(), trigger.getGroup(), trigger);
			} 
			catch (SchedulerException e) {
				throw new ExceptionOperationImpossible(
						"Impossible de programmer la tâche '"+jobdetail.getFullName()+"' " +
						"(envoi d'alerte mail) dans le scheduler.", 
						e);
			}
		}
	}
	

	/**
	 * Change la valeur de l'attribut sendMailJobDelegate
	 * @param sendMailJobDelegate Nouvelle valeur
	 */
	protected void setSendMailJobDelegate(
			org.quartz.jobs.ee.mail.SendMailJob sendMailJobDelegate) {
		this.sendMailJobDelegate = sendMailJobDelegate;
	}
	/**
	 * Retourne la valeur courante de l'attribut sendMailJobDelegate
	 * @return La valeur de l'attribut sendMailJobDelegate
	 */
	protected org.quartz.jobs.ee.mail.SendMailJob getSendMailJobDelegate() {
		return this.sendMailJobDelegate;
	}

	
	/**
	 * Retourne la valeur courante de l'attribut personnesConcerneesSansAdresseMail
	 * @return La valeur de l'attribut personnesConcerneesSansAdresseMail
	 */
	protected NSMutableArray getPersonnesConcerneesSansAdresseMail() {
		return this.personnesConcerneesSansAdresseMail;
	}
	/**
	 * Change la valeur de l'attribut personnesConcerneesSansAdresseMail
	 * @param personnesConcerneesSansAdresseMail Nouvelle valeur
	 */
	protected void setPersonnesConcerneesSansAdresseMail(
			NSMutableArray personnesConcerneesSansAdresseMail) {
		this.personnesConcerneesSansAdresseMail = personnesConcerneesSansAdresseMail;
	}

	/**
	 * Retourne la valeur courante de l'attribut adressesMailPersonnesConcernees
	 * @return La valeur de l'attribut adressesMailPersonnesConcernees
	 */
	protected NSMutableArray getAdressesMailPersonnesConcernees() {
		return this.adressesMailPersonnesConcernees;
	}
	/**
	 * Change la valeur de l'attribut adressesMailPersonnesConcernees
	 * @param adressesMailPersonnesConcernees Nouvelle valeur
	 */
	protected void setAdressesMailPersonnesConcernees(
			NSMutableArray adressesMailPersonnesConcernees) {
		this.adressesMailPersonnesConcernees = adressesMailPersonnesConcernees;
	}


	/**
	 * Retourne la valeur courante de l'attribut mailNextFireTime
	 * @return La valeur de l'attribut mailNextFireTime
	 */
	protected Date getMailNextFireTime() {
		return this.mailNextFireTime;
	}
	/**
	 * Change la valeur de l'attribut mailNextFireTime
	 * @param mailNextFireTime Nouvelle valeur
	 */
	protected void setMailNextFireTime(Date mailNextFireTime) {
		this.mailNextFireTime = mailNextFireTime;
	}
}
