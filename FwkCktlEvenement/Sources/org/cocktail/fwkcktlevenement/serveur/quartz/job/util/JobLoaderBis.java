/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.cocktail.fwkcktlevenement.common.util.FileDownloader;
import org.quartz.Job;


/**
 * 
 * @author Bertrand
 *
 */
public class JobLoaderBis 
{
	private Class jobClass;
	private FileDownloader fileDownloader;

	/**
	 * Constructeur.
	 */
	public JobLoaderBis() {
		fileDownloader = new FileDownloader();
	}
	
	/**
	 * 
	 * @param jarFile
	 * @param className
	 * @return
	 * @throws ClassNotFoundException
	 */
	public void loadJobClassFromJarFile(
			final JarFile jarFile, 
			final String className) throws ClassNotFoundException {
		
		Class loaded_class = new ClassLoader(Thread.currentThread().getContextClassLoader())  {
            public Class findClass(String name) {
            	System.out.println(".findClass() name = "+name);
                JarEntry loaded_class_entry = jarFile.getJarEntry(name);
                System.out.println("loaded_class_entry = "+loaded_class_entry);
                if (loaded_class_entry == null)
                    return null; 
                try {  
                    InputStream is = jarFile.getInputStream(loaded_class_entry);
                    int available = is.available();
                    byte data[] = new byte[available];
                    is.read(data);
                    return defineClass(name, data, 0, data.length);
                }
                catch (IOException ioe)  {
                    System.out.println("Exception: " + ioe);
                    return(null);
                }
            }
            public Class loadClass(String name) throws ClassNotFoundException {
            	System.out.println(".loadClass() name = "+name);
            	return super.loadClass(name);
            }
            
        }.loadClass(className);

        try { jarFile.close(); }
        catch(IOException ioe) {}
        
        if (loaded_class == null)
        	throw new ClassNotFoundException(
        			"La classe '"+className+"' dans le jar '"+jarFile.getName()+
        			"' n'a pas pu être chargée.");
        
        if (!Job.class.isAssignableFrom(loaded_class))
        	throw new IllegalArgumentException(
        			"La classe '"+className+"' trouvée dans le jar '"+jarFile.getName()+
        			"' n'implemente pas l'interface obligatoire '"+Job.class.getName()+"'.");
        
        this.jobClass = loaded_class;
	}
	/**
	 * 
	 * @param jarFileName
	 * @param className
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void loadJobClassFromJarFile(
			final String jarFileName, 
			final String className) throws IOException, ClassNotFoundException {

		if (jarFileName == null)
			throw new IllegalArgumentException("Le chemin du fichier spécifié est null.");
		if (className == null)
			throw new IllegalArgumentException("Le nom de la classe spécifié est null.");
		
		File file = new File(jarFileName);
		System.out.println("JobEvenementExterneLoader.loadJobClassFromJarFile() file = "+file);
		System.out.println("JobEvenementExterneLoader.loadJobClassFromJarFile() file.isFile() = "+file.isFile());
		System.out.println("JobEvenementExterneLoader.loadJobClassFromJarFile() file.isDirectory() = "+file.isDirectory());

		if (!file.exists()) {
			throw new IllegalArgumentException("Le fichier spécifié est introuvable: "+jarFileName);
		}
		if (file.isDirectory()) {
			throw new IllegalArgumentException("Le fichier spécifié est un répertoire: "+jarFileName);
		}
		
		// si le .jar ne semble pas accessible directement (cas d'une URL par ex), on telecharge le fichier avant de pouvoir le charger
		if (!file.isFile()) 
			file = downloadFile(jarFileName);
		
		loadJobClassFromJarFile(new JarFile(file), className);
	}
	
	/**
	 * 
	 * @param classFile
	 * @param className
	 * @throws ClassNotFoundException
	 * @throws IOException 
	 */
	public void loadJobClassFromClassFile(
			final File classFile, 
			final String className) throws ClassNotFoundException, IOException {

		System.out.println("JobEvenementExterneLoader.loadJobClassFromClassFile() file = "+classFile);
		System.out.println("JobEvenementExterneLoader.loadJobClassFromClassFile() classFile.isFile() = "+classFile.isFile());
		System.out.println("JobEvenementExterneLoader.loadJobClassFromClassFile() classFile.isDirectory() = "+classFile.isDirectory());

		if (!classFile.exists()) {
			throw new IllegalArgumentException("Le fichier spécifié est introuvable: "+classFile);
		}
		if (classFile.isDirectory()) {
			throw new IllegalArgumentException("Le fichier spécifié est un répertoire: "+classFile);
		}
		
		// si le .jar ne semble pas accessible directement (cas d'une URL par ex), on telecharge le fichier avant de pouvoir le charger
		final File file;
		if (!classFile.isFile())
			file = downloadFile(classFile.getPath());
		else
			file = classFile;
		
		Class loaded_class = new ClassLoader()  {
	        public Class findClass(String name) {
	            try {  
	                InputStream is = new FileInputStream(file);
	                int available = is.available();
	                byte data[] = new byte[available];
	                is.read(data);
	                return defineClass(name, data, 0, data.length);
	            }
	            catch (IOException ioe)  {
	                System.out.println("Exception: " + ioe);
	                return(null);
	            }
	        }
	    }.loadClass(className);
        
        if (loaded_class == null)
        	throw new ClassNotFoundException(
        			"La classe '"+className+"' dans le fichier '"+classFile.getPath()+
        			"' n'a pas pu être chargée.");
        
        if (!Job.class.isAssignableFrom(loaded_class))
        	throw new IllegalArgumentException(
        			"La classe '"+className+"' trouvée dans le fichier '"+classFile.getName()+
        			"' n'implemente pas l'interface obligatoire '"+Job.class.getName()+"'.");
        
        this.jobClass = loaded_class;
	}
	/**
	 * 
	 * @param classFileInputStream
	 * @param className
	 * @throws ClassNotFoundException
	 */
	public void loadJobClassFromClassFile(
			final InputStream classFileInputStream, 
			final String className) throws ClassNotFoundException {
		
		Class loaded_class = new ClassLoader()  {
	        public Class findClass(String name) {
	            try {  
	                InputStream is = classFileInputStream;
	                int available = is.available();
	                byte data[] = new byte[available];
	                is.read(data);
	                return defineClass(name, data, 0, data.length);
	            }
	            catch (IOException ioe)  {
	                System.out.println("Exception: " + ioe);
	                return(null);
	            }
	        }
	    }.loadClass(className);
        
        if (loaded_class == null)
        	throw new ClassNotFoundException(
        			"La classe '"+className+"' dans le flux d'entrée spécifié n'a pas pu être chargée.");
        
        if (!Job.class.isAssignableFrom(loaded_class))
        	throw new IllegalArgumentException(
        			"La classe '"+className+"' trouvée dans le flux d'entrée " +
        			"n'implemente pas l'interface obligatoire '"+Job.class.getName()+"'.");
        
        this.jobClass = loaded_class;
	}
	/**
	 * 
	 * @param classFileName
	 * @param className
	 * @throws ClassNotFoundException
	 * @throws IOException 
	 * @throws ClassNotFoundException, IOException
	 */
	public void loadJobClassFromClassFile(
			final String classFileName, 
			final String className) throws ClassNotFoundException, IOException {

		if (classFileName == null)
			throw new IllegalArgumentException("Le chemin du fichier .class spécifié est null.");
		if (className == null)
			throw new IllegalArgumentException("Le nom de la classe spécifié est null.");
//		
//		File file = new File(classFileName);
//		System.out.println("JobEvenementExterneLoader.loadJobClassFromClassFile() file = "+file);
//		System.out.println("JobEvenementExterneLoader.loadJobClassFromClassFile() file.isFile() = "+file.isFile());
//		System.out.println("JobEvenementExterneLoader.loadJobClassFromClassFile() file.isDirectory() = "+file.isDirectory());
//		
//		if (file.isDirectory()) {
//			throw new IllegalArgumentException("Le fichier spécifié est un répertoire.");
//		}
//		
//		// si le .jar ne semble pas accessible directement (cas d'une URL par ex), on telecharge le fichier avant de pouvoir le charger
//		if (!file.isFile()) 
//			file = downloadFile(classFileName);
	
		loadJobClassFromClassFile(new File(classFileName), className);
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private File downloadFile(final String fileName) throws IOException {
		URL url = new URL(fileName);
		fileDownloader.connectUrl(url);
		fileDownloader.getFileName();
		String tmpdir = System.getProperty("java.io.tmpdir", ".");
		System.out.println("JobEvenementExterneLoader.downloadFile() tmpdir = "+tmpdir);
		File file = fileDownloader.downloadFile(tmpdir+File.separator+"job_"+Calendar.getInstance().getTime().hashCode()+".jar");
		System.out.println("JobEvenementExterneLoader.downloadFile() file = "+file);
		System.out.println("JobEvenementExterneLoader.downloadFile() file.isFile() = "+file.isFile());
		System.out.println("JobEvenementExterneLoader.downloadFile() file.isDirectory() = "+file.isDirectory());
		return file;
	}
	
	/**
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Job newInstance() throws InstantiationException, IllegalAccessException {
        return (Job) jobClass.newInstance();
	}
	
	
	
	public static void main(String[] args) {
		
		JobLoaderBis loader = new JobLoaderBis();
		
		try {
			loader.loadJobClassFromJarFile(
					"/tmp/job.jar", 
					"org.cocktail.evenement.quartz.serveur.job.JobHelloWorld");
			Job job = loader.newInstance();
			job.execute(null);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
}
