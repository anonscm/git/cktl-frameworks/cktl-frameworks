/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.util;

import java.util.Date;
import java.util.Map;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.MailComposer;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTache;
import org.quartz.JobDetail;
import org.quartz.jobs.ee.mail.SendMailJob;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2008
 * 
 */
public class JobDetailUtilities 
{
	public static final String KEY_JOB_DETAIL_CLASS_NAME = "className";
	public static final String KEY_JOB_DETAIL_DESCRIPTION = "description";
	public static final String KEY_JOB_DETAIL_FULL_NAME = "fullName";
	public static final String KEY_JOB_DETAIL_GROUP = "group";
	public static final String KEY_JOB_DETAIL_NAME = "name";
	public static final String KEY_JOB_DETAIL_IS_DURABLE = "isDurable";
	public static final String KEY_JOB_DETAIL_IS_STATEFUL = "isStateful";
	public static final String KEY_JOB_DETAIL_IS_VOLATILE = "isVolatile";
	public static final String KEY_JOB_DETAIL_REQUESTS_RECOVERY = "requestsRecovery";


    /**
     * 
     * @param tache
     * @return le {@link JobDetail} créé.
     * @throws ExceptionOperationImpossible 
     */
    public static JobDetail createJobDetail(EOTache tache) throws ExceptionOperationImpossible {

    	if (tache == null) 
    		throw new NullPointerException("Tâche requise.");
    	
    	Class jobClass;
		try {
			jobClass = Class.forName(tache.className());
		} 
		catch (ClassNotFoundException e) {
			throw new ExceptionOperationImpossible(
					"Classe de la tâche inconnue: "+tache.className(), e);
		}
    	
        JobDetail detail = new JobDetail(
        		tache.nameForJobDetail(), 
        		tache.groupForJobDetail(), 
        		jobClass);

        detail.setDescription(EOTache.TACH_ID_KEY+"="+tache.tachIdReadOnly());
        
        detail.setVolatility(false); 
        detail.setDurability(false); 
        detail.setRequestsRecovery(true); 

        // la reference de la tache est inscrite dans la JobDataMap
		detail.getJobDataMap().put(EOTache.TACH_ID_KEY, tache.tachIdReadOnly());
		
    	return detail;
    }
	
	/**
	 * Cree un nouvel objet {@link JobDetail} volatile qui envoie un mail.
	 * @param tache Tache dont on veut programmer un mail d'accuse d'execution.
	 * @param misfired Indique si l'instant de declenchement prevu du trigger a ete rate. 
	 * @return Le {@link JobDetail} cree.
	 * @throws ExceptionOperationImpossible 
	 */
	public static JobDetail createJobDetailAccuseExecution(EOTache tache, boolean misfired) throws ExceptionOperationImpossible {
		
    	if (tache == null) 
    		throw new NullPointerException("Tâche requise.");
    	
        JobDetail detail = new JobDetail(
        		"JOB_ACCUSE_EXE_MAIL_TACH_ID="+tache.tachIdReadOnly(), 
        		tache.groupForJobDetail(), 
        		SendMailJob.class); 

        detail.setDescription(EOTache.TACH_ID_KEY+"="+tache.tachIdReadOnly());
        
        detail.setVolatility(false); 
        detail.setDurability(false); 
        detail.setRequestsRecovery(true); 

        // la reference de la tache est inscrite dans la JobDataMap
		detail.getJobDataMap().put(EOTache.TACH_ID_KEY, tache.tachIdReadOnly());

		// recherche des personnes concernees a alerter
		NSArray[] resutat = tache.evenement().getMailsPersonnesConcernees(true);
		
		// composition du mail
		// TODO : recuperer la config dans le jobContext et le passer en parametre ????
//		String grhumHostMail = getApplication().getFacadeEvenementApplicationConfigServer().grhumHostMail(true);
//		String mailSender = getApplication().getFacadeEvenementApplicationConfigServer().mailSender(true);
//		String mailReplyTo = getApplication().getFacadeEvenementApplicationConfigServer().mailReplyTo(true);
		
		String grhumHostMail = "";// TODO : recuperer la config dans le jobContext et le passer en parametre ????
		String mailSender = "";// TODO : recuperer la config dans le jobContext et le passer en parametre ????
		String mailReplyTo = "";// TODO : recuperer la config dans le jobContext et le passer en parametre ????
		
		MailComposer mc = MailComposer.newInstance(grhumHostMail,	mailSender,	mailReplyTo); 
		mc.createDefaultSubjectAccuseExcutionTache(new Date());
		mc.setAdressesMailDestinataires(resutat[0].componentsJoinedByString(FwkCktlEvenementUtil.RECIPIENTS_SEPARATOR));
		mc.setInfosPersonnesInjoignables(resutat[1]);
		mc.setInfosDelaiPrevenance(tache.triggerOffsetInMinutesToString());
		mc.setInfosEvenement(tache.evenement().getHtmlDescriptionPourMail());
		mc.setInfosTache(tache.getShortHtmlDescription(true), misfired);
		Map map = mc.toPropertiesMap();
		
		detail.getJobDataMap().putAll(map);
		
    	return detail;
	}
//	/**
//	 * Cree un nouvel objet {@link JobDetail} volatile qui envoie un mail.
//	 * @param tache EOTache dont on veut programmer un mail d'accuse d'execution.
//	 * @param misfired Indique si l'instant de declenchement prevu du trigger a ete rate. 
//	 * @return Le {@link JobDetail} cree.
//	 * @throws ExceptionOperationImpossible 
//	 */
//	public static JobDetail createJobDetailAccuseExecution(
//			EOTache tache,
//			boolean misfired, 
//			NSData data) throws ExceptionOperationImpossible {
//
//        if (data != null) {
//	    	Evenement evenement = tache.evenement();
//	    	try {
//	        	GedTool gt = new GedTool(new EOEditingContext());
//	        	Integer couNumero = gt.saveFileIntoGed(data, "test.pdf", new Integer(30626));
//	        	System.out.println("JobDetailUtilities.createJobDetailAccuseExecution() couNumero = "+couNumero);
//	        	
//				FacadeEvenement facadeEvenement = new FacadeEvenement(tache.editingContext(), new Integer(30626));
//				facadeEvenement.ajouterDocument(evenement, couNumero);
//				
//				facadeEvenement.enregistrer();
//			}
//	    	catch (Exception e) {
//				e.printStackTrace();
//			}
//        }
//		
//    	return createJobDetailAccuseExecution(tache, misfired);
//	}

	/**
	 * 
	 * @return
	 */
	private static WOApplication getApplication() {
		return (WOApplication) WOApplication.application(); // TODO : voir l'interet ou bien faire avec le FwkCktlEvtUtil
	}
    
	/**
	 * 
	 * @param jobDetails
	 * @return
	 */
	public static NSArray dictionariesFromJobDetails(NSArray jobDetails) {
		
		if (jobDetails == null)
			throw new NullPointerException("Liste de job details requise.");
		
		NSMutableArray dictionaries = new NSMutableArray();
		for (int i=0; i<jobDetails.count(); i++) {
			JobDetail detail = (JobDetail) jobDetails.objectAtIndex(i);
			dictionaries.addObject(dictionaryFromJobDetail(detail));
		}
		
		return dictionaries.immutableClone();
	}
    
	/**
	 * 
	 * @param jobDetail
	 * @return
	 */
	public static NSDictionary dictionaryFromJobDetail(JobDetail jobDetail) {
		
		if (jobDetail == null) 
			throw new NullPointerException("Trigger requis.");
		
		NSMutableDictionary dico = new NSMutableDictionary();
		
		dico.takeValueForKey(jobDetail.getClass().getName(), KEY_JOB_DETAIL_CLASS_NAME);
		dico.takeValueForKey(jobDetail.getDescription(), KEY_JOB_DETAIL_DESCRIPTION);
		dico.takeValueForKey(jobDetail.getFullName(), KEY_JOB_DETAIL_FULL_NAME);
		dico.takeValueForKey(jobDetail.getGroup(), KEY_JOB_DETAIL_GROUP);
		dico.takeValueForKey(jobDetail.getName(), KEY_JOB_DETAIL_NAME);
		dico.takeValueForKey(new Boolean(jobDetail.isDurable()), KEY_JOB_DETAIL_IS_DURABLE);
		dico.takeValueForKey(new Boolean(jobDetail.isStateful()), KEY_JOB_DETAIL_IS_STATEFUL);
		dico.takeValueForKey(new Boolean(jobDetail.isVolatile()), KEY_JOB_DETAIL_IS_VOLATILE);
		dico.takeValueForKey(new Boolean(jobDetail.requestsRecovery()), KEY_JOB_DETAIL_REQUESTS_RECOVERY);
		
		return dico.immutableClone();
	}
}
