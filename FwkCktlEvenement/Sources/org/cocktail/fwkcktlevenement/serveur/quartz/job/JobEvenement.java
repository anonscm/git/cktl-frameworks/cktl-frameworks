/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */ 
package org.cocktail.fwkcktlevenement.serveur.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.webobjects.foundation.NSData;

/**
 * Interface mere implementee par les taches associes aux evenements.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 * @see Job
 */
public interface JobEvenement extends Job
{
	/**
	 * Methode reprsentant le code execute quand le {@link Job} est appele par le scheduler Quartz.
	 * @param jobexecutioncontext Contexte d'execution fourni par le scheduler Quartz.
	 * @return Fichier resultat eventuel renvoye par le code execute.
	 * @throws JobExecutionException Probleme quelconque. Veiller a ne aps renvoyer d'autres types d'exception que celui-la.
	 */
	public NSData executeJob(JobExecutionContext jobexecutioncontext) throws JobExecutionException;
	
}
