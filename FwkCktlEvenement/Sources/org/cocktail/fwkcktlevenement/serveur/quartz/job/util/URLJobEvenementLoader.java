/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * <p>Chargeur de classe qui recharge bien systematiquement la classe au cas ou elle ait changé.
 * 
 * @author Bertrand
 */
public class URLJobEvenementLoader extends URLClassLoader
{

	/**
	 * Constructeur.
	 */
	public URLJobEvenementLoader(URL[] urls) {
		super(urls);
	}
	/**
	 * Constructeur.
	 */
	public URLJobEvenementLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
	}


	/**
	 * @see java.lang.ClassLoader#loadClass(java.lang.String)
	 */
	public Class loadClass(String className) throws ClassNotFoundException {

		Class loadedClass=null;// = findLoadedClass(className);
		
		try {
			loadedClass = findClass(className);
		} 
		catch (ClassNotFoundException e) {
			
		}

		if (loadedClass == null) {
			loadedClass = super.loadClass(className);
		}

		return loadedClass;
	}
	
//	/**
//	 * <p>Charge une classe specifiee par son nom complet dans le ClassPath.
//	 * <p>NB: la classe doit obligatoirement implementer l'interface {@link JobEvenementExterne}.
//	 * @param className Nom complet de la classe a charger.
//	 * Ex: "org.cocktail.application.serveur.JobHelloWorld".
//	 * @throws ClassNotFoundException
//	 */
//	public void loadJobClassFromClasspath(
//			final String className) throws Throwable {
//		
//		this.jobClass = null;
//		
////		URLJobEvenementLoader loader = new URLJobEvenementLoader(
////				new URL[] { jarFileUrl },
////				WOApplication.application().getClass().getClassLoader());//Thread.currentThread().getContextClassLoader());
//		ClassLoader loader = this.getClass().getClassLoader();
//		Class loaded_class = loader.loadClass(className);
//		
//		if (loaded_class == null)
//			throw new ClassNotFoundException(
//					"La classe '"+className+"' n'a pas pu être chargée dans le classpath.");
//		
//		
//		this.jobClass = loaded_class;
//	}


	/**
	 * 
	 * @param args
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {

		URLJobEvenementLoader loader = new URLJobEvenementLoader(
				new URL[] { new URL("file:///tmp/test.jar") }, 
				URLJobEvenementLoader.class.getClassLoader());

		try {
			Class jobClass = loader.loadClass("org.cocktail.evenement.quartz.serveur.job.impl.JobHelloWorldPlus");
//			JobEvenementExterne job = (JobEvenementExterne) jobClass.newInstance();
//			job.executeJob(null);
		}
		catch (Throwable e) {
			e.printStackTrace();
		}

	}
}
