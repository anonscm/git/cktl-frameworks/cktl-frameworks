/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.job.impl;

import java.net.URL;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.webobjects.foundation.NSData;

import er.extensions.logging.ERXLogger;


/**
 * <p>Exemple de job d'application executable par le scheduler de l'application.
 * <p>Le job affiche un message puis temporise quelques secondes, puis renvoit un fichier preleve sur
 * le disque dur en guise de resultat d'execution (chemin du fichier a adapter). 
 * <p>Le fichier resultat est recupere dans {@link JobEvenementImpl} puis depose dans la GED et associe
 * a l'evenement concerne.
 * 
 * @see JobEvenementImpl
 */
public class JobHelloWorldPlus extends JobEvenementImpl
{
	
    private static Logger _log = ERXLogger.getLogger(JobHelloWorldPlus.class);
    
    /**
     * Constructeur par defaut obligatoire.
     */
    public JobHelloWorldPlus() {
    	_log.info("JobHelloWorldPlus.JobHelloWorldPlus() ");
//    	_log.info("coucou ");
    }

	/**
	 * @see JobEvenementImpl#executeJob(JobExecutionContext)
	 */
	public NSData executeJob(JobExecutionContext context) throws JobExecutionException {
		
		_log.info("JobHelloWorldPlus.executeJob() hello");
		
		// on affiche juste un message
    	if (context != null) {
	        String jobName = context.getJobDetail().getFullName();
	        _log.info("\n\n---------");
	        _log.info("SimpleJob "+jobName+" execute a "+context.getFireTime()+
	        		" (prevu a "+context.getScheduledFireTime()+", fin = "+context.getTrigger().getEndTime()+")");
	        _log.info("Hello world! - " + new Date());
	        _log.info("SimpleJob next fire time: " + context.getTrigger().getNextFireTime());
	        _log.info("utilisateurCreateur = "+context.getMergedJobDataMap().getString("utilisateurCreateur"));
    	}
    	// et une petite temporisation a la con
    	int i=0;
		for (i=0; i<1000000000L; i++) { }
    	_log.info("Fini.");
        _log.info("\n\n---------");    
        
        // on retourne un fichier resultat au format NSData, juste pour l'exemple
        NSData data=null;
		try {
			data = new NSData(new URL("file:///Users/bgauthie/suivi_convention.pdf"));
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
        return data;
	}

}