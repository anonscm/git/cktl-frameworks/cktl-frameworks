/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.trigger.util;

import java.io.Serializable;
import java.util.Calendar;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class TriggerOffset implements Serializable
{
	int offsetValue = 0;
	int offsetUnit = 0;
	

	/**
	 * Constructeur.
	 * @param offsetEnMinutes Decalage en minutes.
	 */
	public TriggerOffset(int offsetEnMinutes) {
		this(offsetEnMinutes, Calendar.MINUTE);
	}
	/**
	 * Constructeur.
	 * @param offsetValue Valeur du decalage.
	 * @param offsetUnit Unite du decalage, ex: {@link Calendar#MINUTE}.
	 */
	public TriggerOffset(int offsetValue, int offsetUnit) {
		this.offsetValue = offsetValue;
		this.offsetUnit = offsetUnit;
	}


	public int getOffsetUnit() {
		return this.offsetUnit;
	}


	public void setOffsetUnit(int offsetUnit) {
		this.offsetUnit = offsetUnit;
	}


	public int getOffsetValue() {
		return this.offsetValue;
	}


	public void setOffsetValue(int offsetValue) {
		this.offsetValue = offsetValue;
	}
	
	/**
	 * Cet objet au format texte.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("TriggerOffset { ");
		buf.append(getOffsetValue());
		buf.append(";");
		buf.append(getOffsetUnit());
		buf.append(" }");
		return buf.toString();
	}

	
	
	
	public static void main(String[] args) {
		int i = 1000*60*60*24*7*50;
		long l = 1000*60*60*24*7*50;
		System.out.println("TriggerOffset.main() i = "+i);
		System.out.println("TriggerOffset.main() l = "+l);
	}
}
