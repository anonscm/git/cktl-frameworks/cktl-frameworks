/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.trigger;

import java.util.Date;

import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.serveur.quartz.trigger.util.TriggerOffset;
import org.quartz.Calendar;
import org.quartz.SimpleTrigger;

import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class SimpleTriggerWithOffset extends SimpleTrigger 
{
	TriggerOffset triggerOffset;
	

//	/**
//	 * Constructeur.
//	 * @param simpleTrigger
//	 * @param triggerOffset Decalage horaire a appliquer a ce trigger.
//	 */
//	public SimpleTriggerWithOffset(SimpleTrigger simpleTrigger, TriggerOffset triggerOffset) {
//		this(triggerOffset);
//		
//		setCalendarName(simpleTrigger.getCalendarName());
//		setDescription(simpleTrigger.getDescription());
//		setFireInstanceId(simpleTrigger.getFireInstanceId());
//		setGroup(simpleTrigger.getGroup());
//		setJobDataMap(simpleTrigger.getJobDataMap());
//		setJobGroup(simpleTrigger.getJobGroup());
//		setJobName(simpleTrigger.getJobName());
//		setMisfireInstruction(simpleTrigger.getMisfireInstruction());
//		setName(simpleTrigger.getName());
//		setPriority(simpleTrigger.getPriority());
//		
//		// NB: 
//		setStartTime(getShiftedTime(simpleTrigger.getStartTime()));
//		setPreviousFireTime(new NSTimestamp());
////		setPreviousFireTime(getShiftedTime(simpleTrigger.getPreviousFireTime()));
////		setNextFireTime(getShiftedTime(simpleTrigger.getNextFireTime()));
//		setEndTime(getShiftedTime(simpleTrigger.getEndTime()));
//	}
	/**
	 * Constructeur.
	 * @param triggerOffset Decalage horaire a appliquer a ce trigger.
	 */
	public SimpleTriggerWithOffset(TriggerOffset triggerOffset) {
		setTriggerOffset(triggerOffset);
	}
	
	/**
	 * Retourne Le decalage horaire de ce trigger.
	 * @return Le decalage horaire de ce trigger.
	 */
	public TriggerOffset getTriggerOffset() {
		return this.triggerOffset;
	}
	/**
	 * Change le decalage horaire de ce trigger.
	 * @param triggerOffset Le nouveau decalage horaire.
	 */
	public void setTriggerOffset(TriggerOffset triggerOffset) {
		this.triggerOffset = triggerOffset;
	}

	/**
	 * 
	 * @see org.quartz.CronTrigger#computeFirstFireTime(org.quartz.Calendar)
	 */
	public Date computeFirstFireTime(Calendar calendar) {
		return getShiftedTime(super.computeFirstFireTime(calendar));
	}

//	/**
//	 * 
//	 * @see org.quartz.CronTrigger#setNextFireTime(java.util.Date)
//	 */
//	public void setNextFireTime(Date nextFireTime) {
//		Date tmp = getShiftedTime(nextFireTime);
////		Date tmp = getOppositeShiftedTime(nextFireTime);
////		tmp = getOppositeShiftedTime(tmp);
//		System.out.println("["+new Date()+"] CronTriggerWithOffset.setNextFireTime() nextFireTime = "+nextFireTime);
//		System.out.println("["+new Date()+"] CronTriggerWithOffset.setNextFireTime() tmp          = "+tmp);
//		super.setNextFireTime(tmp);
//	}
	/**
	 * 
	 * @see org.quartz.CronTrigger#getNextFireTime()
	 */
	public Date getNextFireTime() {
		Date tmp = getShiftedTime(super.getNextFireTime());
//		System.out.println("["+new Date()+"] SimpleTriggerWithOffset.getNextFireTime() super.getNextFireTime() = "+super.getNextFireTime());
//		System.out.println("["+new Date()+"] SimpleTriggerWithOffset.getNextFireTime() tmp                     = "+tmp);
		return tmp;
	}
//	/**
//	 * 
//	 * @see org.quartz.CronTrigger#getPreviousFireTime()
//	 */
//	public Date getPreviousFireTime() {
//		return getShiftedTime(super.getPreviousFireTime());
//	}

	/**
	 * 
	 * @see org.quartz.CronTrigger#updateAfterMisfire(org.quartz.Calendar)
	 */
	public void updateAfterMisfire(Calendar cal) {
		super.updateAfterMisfire(cal);
		super.setNextFireTime(getOppositeShiftedTime(super.getNextFireTime()));
	}

	/**
	 * Decale la date selon le decalage horaire courant.
	 * @param time Date a decaler.
	 * @return La date decalee.
	 */
	protected Date getShiftedTime(Date time) {
		if (triggerOffset != null)
			return DateUtilities.decaler(
					new NSTimestamp(time), 
					triggerOffset.getOffsetValue(), 
					triggerOffset.getOffsetUnit());
		else
			return time;
	}
	/**
	 * Decale la date selon L'OPPOSE du decalage horaire courant.
	 * @param time Date a decaler.
	 * @return La date decalee.
	 */
	protected Date getOppositeShiftedTime(Date time) {
		if (triggerOffset != null)
			return DateUtilities.decaler(
					new NSTimestamp(time), 
					-1*triggerOffset.getOffsetValue(), 
					triggerOffset.getOffsetUnit());
		else
			return time;
	}
	
	
	/**
	 * Cet objet au format texte.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("SimpleTriggerWithOffset { ");
		buf.append("offset=");
		buf.append(getTriggerOffset());
		buf.append(" - ");
		buf.append(super.toString());
		buf.append(" }");
		return buf.toString();
	}

}
