/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.trigger.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron;
import org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple;
import org.cocktail.fwkcktlevenement.serveur.modele.EOTache;
import org.cocktail.fwkcktlevenement.serveur.quartz.trigger.SimpleTriggerWithOffset;
import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.NthIncludedDayTrigger;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class TriggerUtilities 
{
	public static final String KEY_TRIGGER_CLASS_NAME = "className";
	public static final String KEY_TRIGGER_DESCRIPTION = "description";
	public static final String KEY_TRIGGER_END_TIME = "endTime";
	public static final String KEY_TRIGGER_FULL_JOB_NAME = "fullJobName";
	public static final String KEY_TRIGGER_FULL_NAME = "fullName";
	public static final String KEY_TRIGGER_GROUP = "group";
	public static final String KEY_TRIGGER_JOB_GROUP = "jobGroup";
	public static final String KEY_TRIGGER_JOB_NAME = "jobName";
	public static final String KEY_TRIGGER_START_TIME = "startDate";
	public static final String KEY_TRIGGER_MISFIRE_INSTRUCTION = "misfireInstruction";

	public static final String KEY_TRIGGER_PREVIOUS_FIRE_TIME = "previousFireTime";
	public static final String KEY_TRIGGER_NEXT_FIRE_TIME = "nextFireTime";
	
	public static final String KEY_TRIGGER_REPEAT_COUNT = "repeatCount";
	public static final String KEY_TRIGGER_REPEAT_INTERVAL = "repeatInterval";
	
	public static final String KEY_TRIGGER_CRON_EXPRESSION = "cronExpression";
	public static final String KEY_TRIGGER_EXPRESSION_SUMMARY = "expressionSummary";

	public static final String KEY_TRIGGER_INTERVAL_TYPE = "intervalType";
	public static final String KEY_TRIGGER_N = "n";
	public static final String KEY_TRIGGER_NEXT_FIRE_CUTOFF_INTERVAL = "nextFireCutoffInterval";
	
	public static final String CLASS_NAME_SIMPLE_TRIGGER = SimpleTrigger.class.getName();
	public static final String CLASS_NAME_CRON_TRIGGER = CronTrigger.class.getName();
	public static final String CLASS_NAME_NTH_INCLUDED_DAY_TRIGGER = NthIncludedDayTrigger.class.getName();
	
	public static final long ONE_DAY_IN_MILLISECONDS = 24*60*60*60*1000L;
	public static final long ONE_WEEK_IN_MILLISECONDS = 7*ONE_DAY_IN_MILLISECONDS;

	public static final int LAST = 0;
	
	/**
	 * 
	 * @param tache
	 * @return
	 */
	public static Trigger createTriggerForTacheAlerteMail(EOTache tache) {

		Trigger trigger = new SimpleTrigger();
		
		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		trigger.setName("TRIGGER_ALERTE_MAIL"+"_"+EOTache.TACH_ID_KEY+"="+tache.tachIdReadOnly());
//		trigger.setDescription("TACH_ID="+tache.tachIdReadOnly());
		trigger.setDescription("Pour la tâche d'alerte mail N°"+tache.tachIdReadOnly());
		trigger.setVolatility(false);
		
		return trigger;
	}

	/**
	 * 
	 * @param frequenceSimple 
	 * @param startDate 
	 * @param triggerOffsetEnMinutes 
	 * @return 
	 * @throws ParseException 
	 */
	static public SimpleTrigger createSimpleTrigger(
			EOFrequenceSimple frequenceSimple, 
			Date startDate, 
			String name,
			String jobGroup,
			String jobName,
			String description,
			Number triggerOffsetEnMinutes) {

		if (frequenceSimple == null)
			throw new NullPointerException("Frequence simple requise.");
		if (startDate == null)
			throw new NullPointerException("Date de debut requise.");
		
		int offset = triggerOffsetEnMinutes!=null ? triggerOffsetEnMinutes.intValue() : 0;
		
		SimpleTrigger trigger;
		if (offset != 0) 
			trigger = new SimpleTriggerWithOffset(new TriggerOffset(offset));
		else
			trigger = new SimpleTrigger();
		
		trigger.setStartTime(startDate);
		trigger.setEndTime(frequenceSimple.dateFin());
		trigger.setRepeatCount(frequenceSimple.repeatCount().intValue());
		trigger.setRepeatInterval(frequenceSimple.repeatInterval().intValue());

		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		trigger.setName(name);
		trigger.setJobGroup(jobGroup);
		trigger.setJobName(jobName);
		trigger.setDescription(description);
		
		return trigger;
	}

	/**
	 * 
	 * @param frequenceCron
	 * @param startDate
	 * @param triggerOffsetEnMinutes
	 * @return
	 * @throws ParseException 
	 */
	static public CronTrigger createCronTrigger(
			EOFrequenceCron frequenceCron,
			TimeZone triggerTimeZone,
			Date startDate,
			String name,
			String jobGroup,
			String jobName,
			String description,
			Number triggerOffsetEnMinutes) throws ParseException {

		if (frequenceCron == null)
			throw new NullPointerException("Frequence CRON requise.");
		if (startDate == null)
			throw new NullPointerException("Date de debut requise.");
		
		int offset = triggerOffsetEnMinutes!=null ? triggerOffsetEnMinutes.intValue() : 0;

		Date startTime = startDate;
		Date endTime = frequenceCron.dateFin();
		CronExpression cronExpression = new CronExpression(frequenceCron.cronExpression());
		
		CronTrigger trigger = new CronTrigger();
		
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() TimeZone.getDefault() = "+TimeZone.getDefault());
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() startDate = "+startDate);
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() startTime = "+startTime);
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() dateFin = "+frequenceCron.dateFin());
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() endTime = "+endTime);
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() cronExpression = "+frequenceCron.cronExpression());
//		System.out.println("TriggerUtilities.makeCronTriggerFromFrequenceCron() cronExpression = "+cronExpression);

		trigger.setTimeZone(triggerTimeZone!= null ? triggerTimeZone : TimeZone.getDefault());
		trigger.setStartTime(startTime);
		trigger.setEndTime(endTime);
		trigger.setCronExpression(cronExpression);

		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		trigger.setName(name);
		trigger.setJobGroup(jobGroup);
		trigger.setJobName(jobName);
		trigger.setDescription(description);
		
		return trigger;
	}

	/**
	 * 
	 * @param startDate
	 * @param triggerOffsetEnMinutes
	 * @return
	 */
	static public SimpleTrigger createOneShotSimpleTrigger(
			Date startDate, 
			String name,
			String jobGroup,
			String jobName,
			String description,
			Number triggerOffsetEnMinutes) {
		
		if (startDate == null)
			throw new NullPointerException("Date de debut requise.");
		
		int offset = triggerOffsetEnMinutes!=null ? triggerOffsetEnMinutes.intValue() : 0;
		
		SimpleTrigger trigger;
		if (offset != 0) 
			trigger = new SimpleTriggerWithOffset(new TriggerOffset(offset));
		else
			trigger = new SimpleTrigger();
		
		trigger.setStartTime(startDate);

		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		trigger.setName(name);
		trigger.setJobGroup(jobGroup);
		trigger.setJobName(jobName);
		trigger.setDescription(description);
		
		return trigger;
	}
	

	
	/**
	 * 
	 * @param trigger 
	 * @param afterDate 
	 * @param dateMaxi 
	 * @return Ensemble des prochains moments de déclanchement du trigger.
	 */
	static public NSArray computeFireTimesAfter(Trigger trigger, Date afterDate, Date dateMaxi) {
		if (trigger == null)
			throw new NullPointerException("Trigger requis.");
		if (afterDate == null)
			throw new NullPointerException("Date de reference requise.");
		if (dateMaxi == null)
			throw new NullPointerException("Date maxi requise.");
		
		NSMutableArray times = new NSMutableArray();
		Date next = trigger.getFireTimeAfter(afterDate);
		while (next!=null && next.before(dateMaxi)) {
			times.addObject(next);
			next = trigger.getFireTimeAfter(next);
		}
		return times;
	}

	/**
	 * 
	 * @param array
	 * @return une liste à partir du NSArray.
	 */
	public static List listFromArray(NSArray array) {
		if (array == null)
			return null;
		
		ArrayList list = new ArrayList();
		for (int i=0; i<array.count(); i++) {
			list.add(array.objectAtIndex(i));
		}
		
		return list;
	}
	/**
	 * 
	 * @param triggers
	 * @return
	 */
	public static NSArray dictionariesFromTriggers(NSArray triggers) {
		
		if (triggers == null)
			throw new NullPointerException("Liste de triggers requise.");
		
		NSMutableArray dictionaries = new NSMutableArray();
		for (int i=0; i<triggers.count(); i++) {
			Trigger trigger = (Trigger) triggers.objectAtIndex(i);
			dictionaries.addObject(dictionaryFromTrigger(trigger));
		}
		
		return dictionaries.immutableClone();
	}
	/**
	 * 
	 * @param trigger
	 * @return
	 */
	public static NSDictionary dictionaryFromTrigger(Trigger trigger) {
		
		if (trigger == null) 
			throw new NullPointerException("Trigger requis.");
		
		NSMutableDictionary dico = new NSMutableDictionary();
		
		dico.takeValueForKey(trigger.getClass().getName(), KEY_TRIGGER_CLASS_NAME);
		dico.takeValueForKey(trigger.getDescription(), KEY_TRIGGER_DESCRIPTION);
		dico.takeValueForKey(trigger.getEndTime(), KEY_TRIGGER_END_TIME);
		dico.takeValueForKey(trigger.getFullJobName(), KEY_TRIGGER_FULL_JOB_NAME);
		dico.takeValueForKey(trigger.getFullName(), KEY_TRIGGER_FULL_NAME);
		dico.takeValueForKey(trigger.getGroup(), KEY_TRIGGER_GROUP);
		dico.takeValueForKey(trigger.getJobGroup(), KEY_TRIGGER_JOB_GROUP);
		dico.takeValueForKey(trigger.getJobName(), KEY_TRIGGER_JOB_NAME);
		dico.takeValueForKey(new Integer(trigger.getMisfireInstruction()), KEY_TRIGGER_MISFIRE_INSTRUCTION);		
		
		if (trigger.getStartTime() != null)
			dico.takeValueForKey(trigger.getStartTime(), KEY_TRIGGER_START_TIME);
		if (trigger.getPreviousFireTime() != null)
			dico.takeValueForKey(trigger.getPreviousFireTime(), KEY_TRIGGER_PREVIOUS_FIRE_TIME);
		if (trigger.getNextFireTime() != null)
			dico.takeValueForKey(trigger.getNextFireTime(), KEY_TRIGGER_NEXT_FIRE_TIME);

		if (trigger instanceof SimpleTrigger) {
			dico.takeValueForKey(new Integer(((SimpleTrigger)trigger).getRepeatCount()), KEY_TRIGGER_REPEAT_COUNT);
			dico.takeValueForKey(new Long(((SimpleTrigger)trigger).getRepeatInterval()), KEY_TRIGGER_REPEAT_INTERVAL);
		}
		else if (trigger instanceof CronTrigger) {
			dico.takeValueForKey(((CronTrigger)trigger).getCronExpression(), KEY_TRIGGER_CRON_EXPRESSION);
			dico.takeValueForKey(((CronTrigger)trigger).getExpressionSummary(), KEY_TRIGGER_EXPRESSION_SUMMARY);
		}
		else 
			throw new IllegalArgumentException("Type de  trigger non supporté.");
		/*if (CLASS_NAME_NTH_INCLUDED_DAY_TRIGGER.equals(trigger.getClass().getName())) {
			dico.takeValueForKey(new Integer(((NthIncludedDayTrigger)trigger).getIntervalType()), KEY_TRIGGER_INTERVAL_TYPE);
			dico.takeValueForKey(new Integer(((NthIncludedDayTrigger)trigger).getN()), KEY_TRIGGER_N);
			dico.takeValueForKey(new Integer(((NthIncludedDayTrigger)trigger).getNextFireCutoffInterval()), KEY_TRIGGER_NEXT_FIRE_CUTOFF_INTERVAL);
		}*/
		
//		System.out.println("TriggerUtilities.dictionaryFromTrigger() dico = "+dico);
		
		return dico.immutableClone();
	}
	
	
	
	public static void main(String[] args) {
		
		long t = 1205856780000L;
		
		System.out.println("TriggerUtilities.main() NSTimeZone.getDefault() = "+NSTimeZone.getDefault());
		
		System.out.println("TriggerUtilities.main()  = "+new Date(t));
		System.out.println("TriggerUtilities.main()  = "+new NSTimestamp(t));

		NSTimeZone.setDefault(NSTimeZone.getGMT());
		TimeZone.setDefault(NSTimeZone.getGMT());
		
		System.out.println();
		System.out.println("TriggerUtilities.main() TimeZone.getDefault() = "+TimeZone.getDefault());
		System.out.println("TriggerUtilities.main() NSTimeZone.getDefault() = "+NSTimeZone.getDefault());
		System.out.println("TriggerUtilities.main() NSTimeZone.getGMT() = "+NSTimeZone.getGMT());
		System.out.println();
		
		NSTimestamp ts = new NSTimestamp();
		Date d = new Date();
		System.out.println("TriggerUtilities.main() ts = "+ts);
		System.out.println("TriggerUtilities.main() decale ts = "+DateUtilities.decaler(ts, -1, Calendar.MINUTE));
//		System.out.println("TriggerUtilities.main() d = "+d);
//		System.out.println("TriggerUtilities.main() decale d = "+DateUtilities.decaler(d, -1, Calendar.MINUTE));
		
//    	NSTimestamp startDate = new NSTimestamp();
//    	startDate = DateOperation.premierJourDuMois("12/2007", 14,0,0);
////    	startDate = DateOperation.dernierJourDuMois("12/2007", 14,0,0);
//    	System.out.println("CronUtilities.main() startDate = "+DateOperation.print(startDate, true));
//    	NSTimestamp endDate = new NSTimestamp(2015,5,15,1,0,0,NSTimeZone.defaultTimeZone());
//    	System.out.println("CronUtilities.main() endDate   = "+DateOperation.print(endDate, true));

//		CronTrigger trigger;
//		trigger = makeDailyTrigger(startDate, endDate);
//		trigger = makeWeeklyTrigger(startDate, endDate, new int[] { Calendar.MONDAY, Calendar.SATURDAY });
//		trigger = makeMonthlyTrigger(startDate, endDate, new int[] { 11,30 });
//		trigger = makeMonthlyTrigger(startDate, endDate, TriggerUtilities.LAST, Calendar.MONDAY);
//		trigger = makeMonthlyTrigger(startDate, endDate, 1, Calendar.MONDAY);
//		trigger = makeYearlyTrigger(startDate, endDate, new int[] { 05,02 });
//		System.out.println("TriggerUtilities.main() trigger = "+trigger);
//    	Debug.printArray("fire times", TriggerUtilities.computeFireTimesAfter(trigger, startDate, endDate));
//    	System.out.println("["+new Date()+"] main() times = "+TriggerUtils.computeFireTimes(trigger, null, 50).toString().replaceAll(",", "\n"));
    	
	}
	
}
