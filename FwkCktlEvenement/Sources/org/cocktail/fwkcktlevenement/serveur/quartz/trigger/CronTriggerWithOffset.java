/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlevenement.serveur.quartz.trigger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.quartz.CronUtilities;
import org.cocktail.fwkcktlevenement.serveur.quartz.trigger.util.TriggerOffset;
import org.quartz.Calendar;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class CronTriggerWithOffset extends CronTrigger 
{
	TriggerOffset triggerOffset;

	
	/**
	 * Constructeur.
	 * @param triggerOffset Decalage horaire a appliquer a ce trigger.
	 */
	public CronTriggerWithOffset(TriggerOffset triggerOffset) {
		super();
		setTriggerOffset(triggerOffset);
	}

	/**
	 * 
	 * @see org.quartz.CronTrigger#computeFirstFireTime(org.quartz.Calendar)
	 */
	public Date computeFirstFireTime(Calendar calendar) {
		return getShiftedTime(super.computeFirstFireTime(calendar));
	}

//	/**
//	 * 
//	 * @see org.quartz.CronTrigger#setNextFireTime(java.util.Date)
//	 */
//	public void setNextFireTime(Date nextFireTime) {
//		Date tmp = getShiftedTime(nextFireTime);
////		Date tmp = getOppositeShiftedTime(nextFireTime);
////		tmp = getOppositeShiftedTime(tmp);
//		System.out.println("["+new Date()+"] CronTriggerWithOffset.setNextFireTime() nextFireTime = "+nextFireTime);
//		System.out.println("["+new Date()+"] CronTriggerWithOffset.setNextFireTime() tmp          = "+tmp);
//		super.setNextFireTime(tmp);
//	}
	/**
	 * 
	 * @see org.quartz.CronTrigger#getNextFireTime()
	 */
	public Date getNextFireTime() {
		Date tmp = getShiftedTime(super.getNextFireTime());
//		System.out.println("["+new Date()+"] SimpleTriggerWithOffset.getNextFireTime() super.getNextFireTime() = "+super.getNextFireTime());
//		System.out.println("["+new Date()+"] SimpleTriggerWithOffset.getNextFireTime() tmp                     = "+tmp);
		return tmp;
	}
//	/**
//	 * 
//	 * @see org.quartz.CronTrigger#getPreviousFireTime()
//	 */
//	public Date getPreviousFireTime() {
//		return getShiftedTime(super.getPreviousFireTime());
//	}

	/**
	 * 
	 * @see org.quartz.CronTrigger#updateAfterMisfire(org.quartz.Calendar)
	 */
	public void updateAfterMisfire(Calendar cal) {
		super.updateAfterMisfire(cal);
		super.setNextFireTime(getOppositeShiftedTime(super.getNextFireTime()));
	}

	/**
	 * Decale la date selon le decalage horaire courant.
	 * @param time Date a decaler.
	 * @return La date decalee.
	 */
	protected Date getShiftedTime(Date time) {
		if (triggerOffset != null)
			return DateUtilities.decaler(
					new NSTimestamp(time), 
					triggerOffset.getOffsetValue(), 
					triggerOffset.getOffsetUnit()/*,
					time instanceof NSTimestamp ? NSTimeZone.getGMT() : TimeZone.getDefault()*/);
		else
			return time;
	}
	/**
	 * Decale la date selon L'OPPOSE du decalage horaire courant.
	 * @param time Date a decaler.
	 * @return La date decalee.
	 */
	protected Date getOppositeShiftedTime(Date time) {
		if (triggerOffset != null)
			return DateUtilities.decaler(
					new NSTimestamp(time), 
					-1*triggerOffset.getOffsetValue(), 
					triggerOffset.getOffsetUnit()/*,
					time instanceof NSTimestamp ? NSTimeZone.getGMT() : TimeZone.getDefault()*/);
		else
			return time;
	}
	
	
	/**
	 * Change le decalage horaire de ce trigger.
	 * @param triggerOffset Le nouveau decalage horaire.
	 */
	public void setTriggerOffset(TriggerOffset triggerOffset) {
		this.triggerOffset = triggerOffset;
	}
	/**
	 * Retourne Le decalage horaire de ce trigger.
	 * @return Le decalage horaire de ce trigger.
	 */
	public TriggerOffset getTriggerOffset() {
		return this.triggerOffset;
	}
	
	/**
	 * Cet objet au format texte.
	 */
//	public String toString() {
//		StringBuffer buf = new StringBuffer();
//		buf.append("CronTriggerWithOffset { ");
//		buf.append("offset=");
//		buf.append(getTriggerOffset());
//		buf.append(" - ");
//		buf.append(super.toString());
//		buf.append(" }");
//		return buf.toString();
//	}
	
	public static void main(String[] args) throws ParseException, ExceptionOperationImpossible {
		final int offset = -19;
		
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
//		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		
		Date startTime = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").parse("17/03/2008 - 12:00:00");
        System.out.println("CronTriggerWithOffset.main() startTime = "+startTime);
		
		try {
            // Grab the Scheduler instance from the Factory 
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
            
            CronTrigger trigger = new CronTriggerWithOffset(new TriggerOffset(offset));
    		trigger.setTimeZone(TimeZone.getDefault());
    		trigger.setEndTime(null);
    		trigger.setCronExpression(CronUtilities.createForDaily(startTime));
            trigger.setStartTime(startTime);
			trigger.setGroup(Scheduler.DEFAULT_GROUP);
			trigger.setName("TRIGGER");
    		trigger.setDescription("khjkjh");
            System.out.println("CronTriggerWithOffset.main() trigger avant = "+trigger);
            System.out.println("CronTriggerWithOffset.main() trigger.getCronExpression() = "+trigger.getCronExpression());
            
            JobDetail jobDetail = new JobDetail("xxxxx", Scheduler.DEFAULT_GROUP, DumbJob.class);
            
            scheduler.scheduleJob(jobDetail, trigger);

            System.out.println("CronTriggerWithOffset.main() trigger apres = "+trigger);
            
            
            System.out.println("CronTriggerWithOffset.main() getStartTime        = "+trigger.getStartTime());
            System.out.println("CronTriggerWithOffset.main() getPreviousFireTime = "+trigger.getPreviousFireTime());
            System.out.println("CronTriggerWithOffset.main() getNextFireTime     = "+trigger.getNextFireTime());
            System.out.println("CronTriggerWithOffset.main() getEndTime          = "+trigger.getEndTime());
            
            //scheduler.shutdown();

        } 
		catch (SchedulerException se) {
            se.printStackTrace();
        }
	}
	
	static public class DumbJob implements Job {

	    public DumbJob() {
	    }

	    public void execute(JobExecutionContext context)     throws JobExecutionException
	    {
	    	System.err.println();
	    	System.err.println();
			  System.err.println("-----------------");
			  System.err.println(new Date());
			  System.err.println("DumbJob is executing.");
			  System.err.println("DumbJob.execute() trigger = "+context.getTrigger());
			  System.err.println("-----------------");
	    }
	  }
}
