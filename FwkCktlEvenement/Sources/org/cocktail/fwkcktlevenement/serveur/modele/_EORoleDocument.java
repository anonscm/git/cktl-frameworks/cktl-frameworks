/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORoleDocument.java instead.
package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

@SuppressWarnings("all")
public abstract class _EORoleDocument extends  CktlServerRecord {
	public static final String ENTITY_NAME = "FwkEvt_RoleDocument";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EVT_ROLE_DOCUMENT";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "rdocId";

	public static final String ID_INTERNE_KEY = "idInterne";
	public static final String LIBELLE_COURT_KEY = "libelleCourt";
	public static final String LIBELLE_LONG_KEY = "libelleLong";
	public static final String ORDRE_AFFICHAGE_KEY = "ordreAffichage";

	// Non visible attributes
	public static final String RDOC_ID_KEY = "rdocId";

	// Colkeys
	public static final String ID_INTERNE_COLKEY = "RDOC_ID_INTERNE";
	public static final String LIBELLE_COURT_COLKEY = "RDOC_LC";
	public static final String LIBELLE_LONG_COLKEY = "RDOC_LL";
	public static final String ORDRE_AFFICHAGE_COLKEY = "RDOC_ORDRE_AFFICHAGE";

	// Non visible colkeys
	public static final String RDOC_ID_COLKEY = "RDOC_ID";

	// Relationships
	public static final String EVENEMENT_DOCUMENTS_KEY = "evenementDocuments";

	// Create / Init methods

	/**
	 * Creates and inserts a new EORoleDocument with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param idInterne
	 * @param libelleCourt
	 * @param libelleLong
	 * @return EORoleDocument
	 */
	public static EORoleDocument create(EOEditingContext editingContext, String idInterne, String libelleCourt, String libelleLong) {
		EORoleDocument eo = (EORoleDocument) createAndInsertInstance(editingContext);
		eo.setIdInterne(idInterne);
		eo.setLibelleCourt(libelleCourt);
		eo.setLibelleLong(libelleLong);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EORoleDocument.
	 *
	 * @param editingContext
	 * @return EORoleDocument
	 */
	public static EORoleDocument create(EOEditingContext editingContext) {
		EORoleDocument eo = (EORoleDocument) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EORoleDocument localInstanceIn(EOEditingContext editingContext) {
		EORoleDocument localInstance = (EORoleDocument) localInstanceOfObject(editingContext, (EORoleDocument) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EORoleDocument localInstanceIn(EOEditingContext editingContext, EORoleDocument eo) {
		EORoleDocument localInstance = (eo == null) ? null : (EORoleDocument) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String idInterne() {
		return (String) storedValueForKey("idInterne");
	}

	public void setIdInterne(String value) {
		takeStoredValueForKey(value, "idInterne");
	}
	public String libelleCourt() {
		return (String) storedValueForKey("libelleCourt");
	}

	public void setLibelleCourt(String value) {
		takeStoredValueForKey(value, "libelleCourt");
	}
	public String libelleLong() {
		return (String) storedValueForKey("libelleLong");
	}

	public void setLibelleLong(String value) {
		takeStoredValueForKey(value, "libelleLong");
	}
	public Integer ordreAffichage() {
		return (Integer) storedValueForKey("ordreAffichage");
	}

	public void setOrdreAffichage(Integer value) {
		takeStoredValueForKey(value, "ordreAffichage");
	}

	public NSArray evenementDocuments() {
		return (NSArray)storedValueForKey("evenementDocuments");
	}

	public NSArray evenementDocuments(EOQualifier qualifier) {
		return evenementDocuments(qualifier, null, false);
	}

	public NSArray evenementDocuments(EOQualifier qualifier, boolean fetch) {
		return evenementDocuments(qualifier, null, fetch);
	}

	public NSArray evenementDocuments(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument.ROLE_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = evenementDocuments();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToEvenementDocumentsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "evenementDocuments");
	}

	public void removeFromEvenementDocumentsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementDocuments");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument createEvenementDocumentsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_EvenementDocument");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "evenementDocuments");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument) eo;
	}

	public void deleteEvenementDocumentsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementDocuments");
				editingContext().deleteObject(object);
			}

	public void deleteAllEvenementDocumentsRelationships() {
		Enumeration objects = evenementDocuments().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteEvenementDocumentsRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EORoleDocument.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EORoleDocument.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EORoleDocument)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EORoleDocument fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORoleDocument fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EORoleDocument eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EORoleDocument)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EORoleDocument fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORoleDocument fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORoleDocument fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EORoleDocument eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EORoleDocument)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORoleDocument fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORoleDocument fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EORoleDocument eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EORoleDocument ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EORoleDocument createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EORoleDocument.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EORoleDocument.ENTITY_NAME + "' !");
		}
		else {
			EORoleDocument object = (EORoleDocument) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EORoleDocument localInstanceOfObject(EOEditingContext ec, EORoleDocument object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EORoleDocument " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EORoleDocument) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
