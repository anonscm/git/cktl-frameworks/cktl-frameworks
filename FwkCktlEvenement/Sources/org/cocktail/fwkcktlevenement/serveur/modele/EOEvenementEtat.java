/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import java.util.Date;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public class EOEvenementEtat extends _EOEvenementEtat {

    private static final long serialVersionUID = 1L;
    private static NSArray<EOEvenementEtat> etatsEvenement;
    public static final String ID_INTERNE_PREVU = "PREVU";
    public static final String ID_INTERNE_REALISE = "REALISE";

	public EOEvenementEtat() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    /**
     * @see com.webobjects.eocontrol.EOCustomObject#toString()
     */
    public String toString() {
    	return libelleLong();
    }
    /**
     * 
     * @param date
     * @return Faux si l'etat est prévu et que la date est anterieure à maintenant 
     * ou si l'état est realisé et que la date est postèrieure à maintenant.
     */
    public boolean estCoherentAvecDate(Date date) {
    	Date now = new Date();
    	if (ID_INTERNE_PREVU.equals(idInterne()) && date.before(now) ||
    		ID_INTERNE_REALISE.equals(idInterne()) && date.after(now))
    		return false;
    	else
    		return true;
    }

    /*
     * 
     */
    
    /**
	 * Verifie que l'etat specifie est coherent avec la date/heure prevue.
	 * @param etat Etat a valider.
	 * @param datePrevue Date/heure prevue de reference.
	 */
	static public void validerEtat(final EOEvenementEtat etat, final NSTimestamp datePrevue) {
		if (!etat.estCoherentAvecDate(datePrevue))
			throw new IllegalArgumentException(
					"L'état '"+etat+"' spécifié est incohérent avec la date prévue de l'événement.");
	}
	
	/*
	 * Finders
	 */

	/**
	 * Cherche l'état d'évenements selon son identifiant interne.
	 * @param idInterne Id interne de l'etat.
	 * @return Les evts trouves.
	 */
	static public EOEvenementEtat findWithIdInterne(String idInterne, EOEditingContext ec) {
//		return (EOEvenementEtat) super.findGenericRecordWithIdInterne(idInterne);
		return EOEvenementEtat.fetchFirstByKeyValue(ec, ID_INTERNE_KEY, idInterne);
	}

	/**
	 * Cherche un état d'évenement selon sa clé primaire.
	 * @param evteId Identifiant (cle primaire)
	 * @return L'evt trouve.
	 */
	static public EOEvenementEtat findWithEvteId(Number evteId, EOEditingContext ec) {
//		return (EOEvenementEtat) super.findGenericRecordWithEvteId(evteId);
		return EOEvenementEtat.fetchFirstByKeyValue(ec, EVTE_ID_KEY, evteId);
	}
	
	/**
	 * Fournit la liste des etats possibles.
	 * @return Objets de type reel {@link EOEvenementEtat}.
	 */
	@SuppressWarnings("unchecked")
    public static NSArray<EOEvenementEtat> getEtatsEvenement() {
		if (etatsEvenement == null) {
				etatsEvenement = EOEvenementEtat.fetchAll(
				        EOSharedEditingContext.defaultSharedEditingContext(), ERXS.asc(ORDRE_AFFICHAGE_KEY).array());
		}
		return etatsEvenement;
	}
	
	/**
	 * Fournit l'etat par defaut d'un evenement.
	 * @return Objet de type reel {@link EOEvenementEtat}.
	 */
	public static EOEvenementEtat getEtatEvenementDefault() {
	    return ERXArrayUtilities.firstObject(getEtatsEvenement());
	}
	
}
