/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOEvenementExtJuridique extends _EOEvenementExtJuridique {

    public static final String AVIS_FAV = "FAV";
    public static final String AVIS_DEFAV = "DEFAV";

    public EOEvenementExtJuridique() {
        super();
    }

    @Override
    public void awakeFromInsertion(EOEditingContext editingContext) {
        super.awakeFromInsertion(editingContext);
        setEvtAvis(AVIS_FAV);
    }
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    /**
     * @return le responsable juridique
     */
    public IPersonne responsableJuridique() {
        if (evtPersIdRespJuridique() != null) {
            IPersonne personne = EOStructure.fetchFirstByQualifier(
                editingContext(), ERXQ.equals(EOStructure.PERS_ID_KEY, evtPersIdRespJuridique()));
            if (personne == null)
                personne = EOIndividu.fetchFirstByQualifier(
                        editingContext(), ERXQ.equals(EOIndividu.PERS_ID_KEY, evtPersIdRespJuridique()));
            return personne;
        }
        return null;
    }
    
    /**
     * @param personne la personne à rajouter comme responsable juridique
     */
    public void setResponsableJuridique(IPersonne personne) {
            setEvtPersIdRespJuridique(personne.persId());
    }

    /**
     * @param ec un editing context
     * @return les responsables juridiques du référentiel
     */
    @SuppressWarnings("unchecked")
    public static NSArray<IPersonne> fetchResponsablesJuridiquesPossibles(EOEditingContext ec) {
        return EORepartAssociation.getPersonnesWithAssociation(ec, EOAssociation.ASS_CODE_RESP_JURIDIQUE);
    }
    
}
