/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import java.util.Date;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EORepetition extends _EORepetition {
    public static final EOSortOrdering SORT_ASC_LIBELLE_LONG = 
        EOSortOrdering.sortOrderingWithKey(LIBELLE_LONG_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    public static final EOSortOrdering SORT_ASC_ORDRE_AFFICHAGE = 
        EOSortOrdering.sortOrderingWithKey(ORDRE_AFFICHAGE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);

    public static final String ID_INTERNE_AUCUNE = "AUCUNE";
    public static final String ID_INTERNE_JOUR = "JOUR";
    public static final String ID_INTERNE_SEMAINE = "SEMAINE";
    public static final String ID_INTERNE_MOIS = "MOIS";
    public static final String ID_INTERNE_ANNEE = "ANNEE";
    public static final String ID_INTERNE_AVANCEE = "AVANCEE";

    public static final String FIN_REPETITION_JAMAIS = "Jamais";
    public static final String FIN_REPETITION_LE = "Le:";
    private Object[] FINS_REPETITION_DEFAULT = new Object[] {
            FIN_REPETITION_JAMAIS,
            FIN_REPETITION_LE
    };

    private static NSArray<EORepetition> repetitionsPossibles;

    public EORepetition() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {

    }

    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

    }

    public boolean isAucune() {
        return idInterne().equals(ID_INTERNE_AUCUNE);
    }
    public boolean isQuotidienne() {
        return idInterne().equals(ID_INTERNE_JOUR);
    }
    public boolean isHebdomadaire() {
        return idInterne().equals(ID_INTERNE_SEMAINE);
    }
    public boolean isMensuelle() {
        return idInterne().equals(ID_INTERNE_MOIS);
    }

    /*
     * Finders
     */
    /**
     * Recherche selon le seul critere specifie.
     * @param repId Valeur du critere de recherche : cle primaire
     * @return Le(s) objet(s) metier trouve(s)
     * @throws ExceptionFinder Probleme renvoye par le finder.
     */
    public static EORepetition findWithRepId(Number repId, EOEditingContext editingContext) {
        return EORepetition.fetchFirstByKeyValue(editingContext, EORepetition.REP_ID_KEY, repId);
    }

    /**
     * Recherche selon le seul critere specifie.
     * @param idInterne Valeur du critere de recherche : id interne
     * @return Le(s) objet(s) metier trouve(s)
     * @throws ExceptionFinder Probleme renvoye par le finder.
     */
    public static EORepetition findWithIdInterne(String idInterne, EOEditingContext editingContext) {
        return EORepetition.fetchFirstByKeyValue(editingContext, EORepetition.ID_INTERNE_KEY, idInterne);
    }

    /**
     * Fournit la liste des types de repetition possibles.
     * @return Objets de type reel {@link EORepetition}.
     */
    public static NSArray<EORepetition> getRepetitions() {
        if (repetitionsPossibles == null) {
            repetitionsPossibles = EORepetition.fetchAll(EOSharedEditingContext.defaultSharedEditingContext(),
                    ERXS.ascs(EORepetition.ORDRE_AFFICHAGE_KEY, EORepetition.LIBELLE_LONG_KEY));
        }
        return repetitionsPossibles;
    }

    /**
     * Fournit la liste des types de repetition possibles.
     * @return Objets de type reel {@link EORepetition}.
     */
    public static NSArray<EORepetition> getRepetitionsSansAvance() {
        return ERXQ.filtered(getRepetitions(), ERXQ.notEquals(EORepetition.ID_INTERNE_KEY, ID_INTERNE_AVANCEE));
    }

    /**
     * Fournit le type de repetition par defaut.
     * @return
     */
    public static EORepetition getRepetitionDefault(EOEditingContext editingContext) {
        return getRepetitionAucune(editingContext);
    }

    /**
     * Fournit le type correspondant a "aucune repetition".
     * @return
     */
    public static EORepetition getRepetitionAucune(EOEditingContext editingContext) {
        return EORepetition.findWithIdInterne(ID_INTERNE_AUCUNE, editingContext);
    }

    /**
     * Fournit le type correspondant a "tous les jours".
     * @return
     */
    public static EORepetition getRepetitionQuotidienne(EOEditingContext editingContext) {
        return EORepetition.findWithIdInterne(ID_INTERNE_JOUR, editingContext);
    }

    /**
     * Fournit le type correspondant a "toutes les semaines".
     * @return
     */
    public static EORepetition getRepetitionHebdomadaire(EOEditingContext editingContext) {
        return EORepetition.findWithIdInterne(ID_INTERNE_SEMAINE, editingContext);
    }

    /**
     * Fournit le type correspondant a "tous les mois".
     * @return
     */
    public static EORepetition getRepetitionMensuelle(EOEditingContext editingContext) {
        return EORepetition.findWithIdInterne(ID_INTERNE_MOIS, editingContext);
    }

    /**
     * Fournit le type correspondant a un repetition personnalisee (expression CRON manuelle).
     * @return
     */
    public static EORepetition getRepetitionAvancee(EOEditingContext editingContext) {
        return EORepetition.findWithIdInterne(ID_INTERNE_AVANCEE, editingContext);
    }

    /**
     * Fournit la liste des types de fin de repetition possibles.
     * @return
     */
    public Object[] getFinsRepetition() {
        return FINS_REPETITION_DEFAULT;
    }
    /**
     * Fournit le type de fin de repetition par defaut.
     * @return
     */
    public Object getFinRepetitionDefault() {
        return getFinRepetitionJamais();
    }
    /**
     * Fournit le type correspondant a une repetition avec fin.
     * @return le type correspondant a une repetition avec fin.
     */
    public Object getFinRepetitionLe() {
        return FIN_REPETITION_LE;
    }
    /**
     * Fournit le type correspondant a une repetition sans fin.
     * @return le type correspondant a une repetition sans fin.
     */
    public Object getFinRepetitionJamais() {
        return FIN_REPETITION_JAMAIS;
    }
    /**
     * Fournit le type de fin de repetition de l'evenement specifie.
     * @param evenement Evenement voulu ou <code>null</code> pour avoir la valeur par defaut
     * @return le type de fin de repetition de l'evenement specifie.
     */
    public Object getFinRepetition(EOEvenement evenement, EOEditingContext editingContext) {
        if (evenement != null) 
            return getFinRepetition(evenement.dateFinRepetition());
        else
            return getRepetitionDefault(editingContext);
    }
    /**
     * Fournit le type de fin de repetition en fonction de la date de fin de repetition.
     * @param dateFinRepetition Date de fin de repetition de l'evenement, <code>null</code> possible.
     * @return le type de fin de repetition en fonction de la date de fin de repetition.
     */
    public Object getFinRepetition(Date dateFinRepetition) {
        if (dateFinRepetition != null)
            return FIN_REPETITION_LE;
        else
            return FIN_REPETITION_JAMAIS;
    }	
}
