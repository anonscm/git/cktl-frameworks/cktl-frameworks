/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionEvenementPersonneCreation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOEvenementPersonne extends _EOEvenementPersonne {
	public static final String TO_PERSONNE_KEY = "toPersonne";
	static final public String VALUE_ALERTER_OUI   = "OUI";
    static final public String VALUE_ALERTER_NON    = "NON";
    static final public EOQualifier QUALIFIER_ALERTER_OUI = 
        EOQualifier.qualifierWithQualifierFormat("alerter = %@", new NSArray(VALUE_ALERTER_OUI));
    static final public EOQualifier QUALIFIER_ALERTER_NON = 
        EOQualifier.qualifierWithQualifierFormat("alerter isEqualTo null or alerter = %@", new NSArray(VALUE_ALERTER_NON));

    public EOEvenementPersonne() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    /**
	 * @return toStructure() ou bien toIndiviodu() selon le cas.
	 */
	public IPersonne toPersonne() {
		return (toStructure() != null ? toStructure() : toIndividu());
	}

	public void setToPersonne(IPersonne personne) {
	    if (personne.isIndividu())
	        addToToIndividusRelationship((EOIndividu)personne);
	    else
	        addToToStructuresRelationship((EOStructure)personne);
	}
	
	public EOIndividu toIndividu() {
		return (EOIndividu) ((toIndividus() != null && toIndividus().count() > 0) ? toIndividus().objectAtIndex(0) : null);
	}

	public EOStructure toStructure() {
		return (EOStructure) ((toStructures() != null && toStructures().count() > 0) ? toStructures().objectAtIndex(0) : null);
	}

	public boolean alerterBoolean() {
        return VALUE_ALERTER_OUI.equals(alerter());
    }
    public void setAlerterBoolean(boolean value) {
    	setAlerter(value ? VALUE_ALERTER_OUI : VALUE_ALERTER_NON);
    }
    
    /*
     * Services
     */
    /**
	 * Creee une inscription de personne concernee par un evenement.
	 * @param evenement Evenement de reference.
	 * @param personneConcernee Le persID de la Personne concernee par l'evenement
	 * @param role Role que joue la personne dans l'evenement. <code>null</code> possible. Un finder existe. 
	 * @return L'inscription creee.
	 * @throws ExceptionEvenementPersonneCreation 
	 */
    static public EOEvenementPersonne creerEvenementPersonne(
            final EOEvenement evenement, 
            //			final EOPersonne personneConcernee, 
            final IPersonne personneConcernee,
            final EORolePersonne role,
            final boolean alerter,
            EOEditingContext ec) {
        if (evenement == null)
            throw new IllegalArgumentException("L'evenement doit etre fourni.");
        if (personneConcernee == null)
            throw new IllegalArgumentException("La personne concernee doit etre fournie.");
        EOEvenementPersonne evenementPersonne;
        //			evenementPersonne = (EOEvenementPersonne) instanceForEntity();
        evenementPersonne = EOEvenementPersonne.create(ec);
        //		ec.insertObject(evenementPersonne);
        evenementPersonne.setEvenementRelationship(evenement.localInstanceIn(ec));
        //		evenementPersonne.setPersonneRelationship(personneConcernee);
        evenementPersonne.setToPersonne(personneConcernee);
        if (role != null)
            evenementPersonne.setRoleRelationship(role.localInstanceIn(ec));
        evenementPersonne.setAlerterBoolean(alerter);
        return evenementPersonne;
    }

	/**
	 * Supprime une inscription de personne concernee par un evenement.
	 * @param evenementPersonne Inscription a supprimer.
	 */
	static public void supprimerEvenementPersonne(
			EOEvenementPersonne evenementPersonne,
			EOEditingContext ec) {

		if (evenementPersonne == null)
			throw new IllegalArgumentException("L'inscription à supprimer doit être fournie.");

		EOEvenement evenement = evenementPersonne.evenement();
		EORolePersonne role = evenementPersonne.role();
		
		evenement.removeFromEvenementPersonnesRelationship(evenementPersonne);
		if (role != null)
			role.removeFromEvenementPersonnesRelationship(evenementPersonne);
		
		ec.deleteObject(evenementPersonne);
	}
	/**
	 * Supprime toutes les inscriptions de personnes concernees par un evenement.
	 * @param evenement Evenement de reference.
	 * @return Nombre d'inscriptions trouvees et supprimees.
	 */
	static public int supprimerEvenementPersonnes(
			final EOEvenement evenement,
			EOEditingContext ec) {

		if (evenement == null)
			throw new IllegalArgumentException("L'événement doit être fourni.");
		
		NSArray evenementPersonnes = evenement.evenementPersonnes().immutableClone();
		
		int nb = evenementPersonnes.count();
		for (int i=0; i<evenementPersonnes.count(); i++) {
			EOEvenementPersonne object = (EOEvenementPersonne) evenementPersonnes.objectAtIndex(i);
			EOEvenementPersonne.supprimerEvenementPersonne(object,ec);
		}
		
		return nb;
	}
	/**
	 * Supprime les inscriptions des personnes concernees specifiees.
	 * 
	 * @param evenement Evenement de reference.
	 * @param personneConcerneePersId ATTENTION : ne sert pas actuellement !!!
	 * @param role Role que joue la personne dans l'evenement. Un finder existe. 
	 * Si <code>null</code>, role pas pris en compte dans le fetch.
	 * @return Nombre d'inscriptions trouvees et supprimees.
	 */
	static public int supprimerEvenementPersonnes(
			final EOEvenement evenement, 
//			final EOPersonne personneConcernee, 
			final Integer personneConcerneePersId, // Attention ne sert pas actuellement
			final EORolePersonne role,
			EOEditingContext ec) {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement doit être fourni.");
		if (personneConcerneePersId == null)
			throw new IllegalArgumentException("La personne concernée doit être fournie.");
		
		String condStr = "evenement = %@ and persId = %@";
		NSMutableArray params = new NSMutableArray(new Object[] { evenement, personneConcerneePersId });
		if (role != null) {
			condStr += " and role = %@";
			params.addObject(role);
		}
		
		// Actuellement on supprime TOUTES les personnes de l'evenement
		NSArray evenementPersonnes = evenement.evenementPersonnes().immutableClone();

		int nb = evenementPersonnes.count();
		for (int i=0; i<evenementPersonnes.count(); i++) {
			EOEvenementPersonne evenementPersonne = (EOEvenementPersonne) evenementPersonnes.objectAtIndex(i);
			EOEvenementPersonne.supprimerEvenementPersonne(evenementPersonne,ec);
		}
		
		return nb;
	}
}
