/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFrequenceSimple.java instead.
package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

@SuppressWarnings("all")
public abstract class _EOFrequenceSimple extends  CktlServerRecord {
	public static final String ENTITY_NAME = "FwkEvt_FrequenceSimple";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EVT_FREQUENCE_SIMPLE";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "frqsId";

	public static final String DATE_FIN_KEY = "dateFin";
	public static final String REPEAT_COUNT_KEY = "repeatCount";
	public static final String REPEAT_INTERVAL_KEY = "repeatInterval";
	public static final String TRADUCTION_KEY = "traduction";

	// Non visible attributes
	public static final String FRQS_ID_KEY = "frqsId";
	public static final String EVT_ID_KEY = "evtId";
	public static final String REP_ID_KEY = "repId";

	// Colkeys
	public static final String DATE_FIN_COLKEY = "FRQS_DATE_FIN";
	public static final String REPEAT_COUNT_COLKEY = "FRQS_REPEAT_COUNT";
	public static final String REPEAT_INTERVAL_COLKEY = "FRQS_REPEAT_INTERVAL";
	public static final String TRADUCTION_COLKEY = "FRQS_TRADUCTION";

	// Non visible colkeys
	public static final String FRQS_ID_COLKEY = "FRQS_ID";
	public static final String EVT_ID_COLKEY = "EVT_ID";
	public static final String REP_ID_COLKEY = "REP_ID";

	// Relationships
	public static final String EVENEMENT_KEY = "evenement";
	public static final String REPETITION_KEY = "repetition";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOFrequenceSimple with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param repeatCount
	 * @param repeatInterval
	 * @param traduction
	 * @param evenement
	 * @param repetition
	 * @return EOFrequenceSimple
	 */
	public static EOFrequenceSimple create(EOEditingContext editingContext, Integer repeatCount, Integer repeatInterval, String traduction, org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement, org.cocktail.fwkcktlevenement.serveur.modele.EORepetition repetition) {
		EOFrequenceSimple eo = (EOFrequenceSimple) createAndInsertInstance(editingContext);
		eo.setRepeatCount(repeatCount);
		eo.setRepeatInterval(repeatInterval);
		eo.setTraduction(traduction);
		eo.setEvenementRelationship(evenement);
		eo.setRepetitionRelationship(repetition);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOFrequenceSimple.
	 *
	 * @param editingContext
	 * @return EOFrequenceSimple
	 */
	public static EOFrequenceSimple create(EOEditingContext editingContext) {
		EOFrequenceSimple eo = (EOFrequenceSimple) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOFrequenceSimple localInstanceIn(EOEditingContext editingContext) {
		EOFrequenceSimple localInstance = (EOFrequenceSimple) localInstanceOfObject(editingContext, (EOFrequenceSimple) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOFrequenceSimple localInstanceIn(EOEditingContext editingContext, EOFrequenceSimple eo) {
		EOFrequenceSimple localInstance = (eo == null) ? null : (EOFrequenceSimple) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public NSTimestamp dateFin() {
		return (NSTimestamp) storedValueForKey("dateFin");
	}

	public void setDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, "dateFin");
	}
	public Integer repeatCount() {
		return (Integer) storedValueForKey("repeatCount");
	}

	public void setRepeatCount(Integer value) {
		takeStoredValueForKey(value, "repeatCount");
	}
	public Integer repeatInterval() {
		return (Integer) storedValueForKey("repeatInterval");
	}

	public void setRepeatInterval(Integer value) {
		takeStoredValueForKey(value, "repeatInterval");
	}
	public String traduction() {
		return (String) storedValueForKey("traduction");
	}

	public void setTraduction(String value) {
		takeStoredValueForKey(value, "traduction");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement)storedValueForKey("evenement");
	}

	public void setEvenementRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement oldValue = evenement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "evenement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "evenement");
		}
	}
  
	public org.cocktail.fwkcktlevenement.serveur.modele.EORepetition repetition() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EORepetition)storedValueForKey("repetition");
	}

	public void setRepetitionRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EORepetition value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EORepetition oldValue = repetition();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "repetition");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "repetition");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOFrequenceSimple.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOFrequenceSimple.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOFrequenceSimple)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOFrequenceSimple fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFrequenceSimple fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOFrequenceSimple eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOFrequenceSimple)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOFrequenceSimple fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOFrequenceSimple fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOFrequenceSimple fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOFrequenceSimple eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOFrequenceSimple)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOFrequenceSimple fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOFrequenceSimple fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOFrequenceSimple eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOFrequenceSimple ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOFrequenceSimple createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOFrequenceSimple.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOFrequenceSimple.ENTITY_NAME + "' !");
		}
		else {
			EOFrequenceSimple object = (EOFrequenceSimple) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOFrequenceSimple localInstanceOfObject(EOEditingContext ec, EOFrequenceSimple object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOFrequenceSimple " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOFrequenceSimple) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
