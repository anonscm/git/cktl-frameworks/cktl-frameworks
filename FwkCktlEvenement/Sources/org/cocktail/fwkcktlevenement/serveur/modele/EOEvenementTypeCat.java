/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public class EOEvenementTypeCat extends _EOEvenementTypeCat {

    private static NSArray<EOEvenementTypeCat> typeCategories;
    public static final String ID_INTERNE_ADMINISTRATIF = "ADMIN";
    public static final String ID_INTERNE_FINANCIER     = "FINANCIER";
    public static final String ID_INTERNE_JURIDIQUE     = "JURIDIQUE";
    
    public EOEvenementTypeCat() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    /**
     * @return la liste des types d'évènements ordonnées (suivant {@link EOEvenementType#ordreAffichage()}).
     */
    public NSArray<EOEvenementType> evenementTypesOrdered() {
        return super.evenementTypes(null, ERXS.asc(EOEvenementType.ORDRE_AFFICHAGE_KEY).array(), false);
    }
    
    /**
     * Fournit la liste des catégories de types d'evenements possibles.
     * @return Liste d'objets de type {@link EOEvenementTypeCat}.
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EOEvenementTypeCat> getTypeCategories() {
        if (typeCategories == null) {
            typeCategories = EOEvenementTypeCat.fetchAll(EOSharedEditingContext.defaultSharedEditingContext(), 
                        ERXS.ascInsensitives(LIBELLE_LONG_KEY));
        }
        return typeCategories;
    }
    
    
    /**
     * Fournit la catégorie de type d'evenements par defaut.
     * @return Objet de type reel {@link EOEvenementTypeCat}.
     */
    public static EOEvenementTypeCat getTypeCategorieDefault() {
        return ERXQ.filtered(getTypeCategories(), ERXQ.equals(ID_INTERNE_KEY, ID_INTERNE_ADMINISTRATIF)).lastObject();
    }
    
}
