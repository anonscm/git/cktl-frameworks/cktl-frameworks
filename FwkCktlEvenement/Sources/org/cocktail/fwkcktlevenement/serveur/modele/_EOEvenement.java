// _EOEvenement.java
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEvenement.java instead.
package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

@SuppressWarnings("all")
public abstract class _EOEvenement extends  CktlServerRecord {
	public static final String ENTITY_NAME = "FwkEvt_Evenement";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EVT_EVENEMENT";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "evtId";

	public static final String APP_NAME_KEY = "appName";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIF_KEY = "dateModif";
	public static final String DATE_PREVUE_KEY = "datePrevue";
	public static final String EVT_ID_READ_ONLY_KEY = "evtIdReadOnly";
	public static final String OBJET_KEY = "objet";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String PERSID_CREATION_KEY = "persidCreation";
	public static final String PERSID_MODIF_KEY = "persidModif";
	public static final String STATUT_KEY = "statut";

	// Non visible attributes
	public static final String EVT_ID_KEY = "evtId";
	public static final String EVTE_ID_KEY = "evteId";
	public static final String EVTT_ID_KEY = "evttId";

	// Colkeys
	public static final String APP_NAME_COLKEY = "EVT_APP_NAME";
	public static final String DATE_CREATION_COLKEY = "EVT_DATE_CREATION";
	public static final String DATE_MODIF_COLKEY = "EVT_DATE_MODIF";
	public static final String DATE_PREVUE_COLKEY = "EVT_DATE_PREVUE";
	public static final String EVT_ID_READ_ONLY_COLKEY = "EVT_ID";
	public static final String OBJET_COLKEY = "EVT_OBJET";
	public static final String OBSERVATIONS_COLKEY = "EVT_OBSERVATIONS";
	public static final String PERSID_CREATION_COLKEY = "PERSID_CREATION";
	public static final String PERSID_MODIF_COLKEY = "PERSID_MODIF";
	public static final String STATUT_COLKEY = "EVT_STATUT";

	// Non visible colkeys
	public static final String EVT_ID_COLKEY = "EVT_ID";
	public static final String EVTE_ID_COLKEY = "EVTE_ID";
	public static final String EVTT_ID_COLKEY = "EVTT_ID";

	// Relationships
	public static final String ETAT_KEY = "etat";
	public static final String EVENEMENT_DOCUMENTS_KEY = "evenementDocuments";
	public static final String EVENEMENT_EXT_JURIDIQUES_KEY = "evenementExtJuridiques";
	public static final String EVENEMENT_PERSONNES_KEY = "evenementPersonnes";
	public static final String FREQUENCES_CRONS_KEY = "frequencesCrons";
	public static final String FREQUENCES_SIMPLES_KEY = "frequencesSimples";
	public static final String TACHES_KEY = "taches";
	public static final String TYPE_KEY = "type";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOEvenement with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param dateCreation
	 * @param datePrevue
	 * @param persidCreation
	 * @param etat
	 * @param type
	 * @return EOEvenement
	 */
	public static EOEvenement create(EOEditingContext editingContext, NSTimestamp dateCreation, NSTimestamp datePrevue, Integer persidCreation, org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat etat, org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType type) {
		EOEvenement eo = (EOEvenement) createAndInsertInstance(editingContext);
		eo.setDateCreation(dateCreation);
		eo.setDatePrevue(datePrevue);
		eo.setPersidCreation(persidCreation);
		eo.setEtatRelationship(etat);
		eo.setTypeRelationship(type);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOEvenement.
	 *
	 * @param editingContext
	 * @return EOEvenement
	 */
	public static EOEvenement create(EOEditingContext editingContext) {
		EOEvenement eo = (EOEvenement) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOEvenement localInstanceIn(EOEditingContext editingContext) {
		EOEvenement localInstance = (EOEvenement) localInstanceOfObject(editingContext, (EOEvenement) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOEvenement localInstanceIn(EOEditingContext editingContext, EOEvenement eo) {
		EOEvenement localInstance = (eo == null) ? null : (EOEvenement) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String appName() {
		return (String) storedValueForKey("appName");
	}

	public void setAppName(String value) {
		takeStoredValueForKey(value, "appName");
	}
	public NSTimestamp dateCreation() {
		return (NSTimestamp) storedValueForKey("dateCreation");
	}

	public void setDateCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dateCreation");
	}
	public NSTimestamp dateModif() {
		return (NSTimestamp) storedValueForKey("dateModif");
	}

	public void setDateModif(NSTimestamp value) {
		takeStoredValueForKey(value, "dateModif");
	}
	public NSTimestamp datePrevue() {
		return (NSTimestamp) storedValueForKey("datePrevue");
	}

	public void setDatePrevue(NSTimestamp value) {
		takeStoredValueForKey(value, "datePrevue");
	}
	public Integer evtIdReadOnly() {
		return (Integer) storedValueForKey("evtIdReadOnly");
	}

	public void setEvtIdReadOnly(Integer value) {
		takeStoredValueForKey(value, "evtIdReadOnly");
	}
	public String objet() {
		return (String) storedValueForKey("objet");
	}

	public void setObjet(String value) {
		takeStoredValueForKey(value, "objet");
	}
	public String observations() {
		return (String) storedValueForKey("observations");
	}

	public void setObservations(String value) {
		takeStoredValueForKey(value, "observations");
	}
	public Integer persidCreation() {
		return (Integer) storedValueForKey("persidCreation");
	}

	public void setPersidCreation(Integer value) {
		takeStoredValueForKey(value, "persidCreation");
	}
	public Integer persidModif() {
		return (Integer) storedValueForKey("persidModif");
	}

	public void setPersidModif(Integer value) {
		takeStoredValueForKey(value, "persidModif");
	}
	public String statut() {
		return (String) storedValueForKey("statut");
	}

	public void setStatut(String value) {
		takeStoredValueForKey(value, "statut");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat etat() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat)storedValueForKey("etat");
	}

	public void setEtatRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementEtat oldValue = etat();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "etat");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "etat");
		}
	}
  
	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType type() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType)storedValueForKey("type");
	}

	public void setTypeRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType oldValue = type();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "type");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "type");
		}
	}
  
	public NSArray evenementDocuments() {
		return (NSArray)storedValueForKey("evenementDocuments");
	}

	public NSArray evenementDocuments(EOQualifier qualifier) {
		return evenementDocuments(qualifier, null, false);
	}

	public NSArray evenementDocuments(EOQualifier qualifier, boolean fetch) {
		return evenementDocuments(qualifier, null, fetch);
	}

	public NSArray evenementDocuments(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument.EVENEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = evenementDocuments();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToEvenementDocumentsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "evenementDocuments");
	}

	public void removeFromEvenementDocumentsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementDocuments");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument createEvenementDocumentsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_EvenementDocument");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "evenementDocuments");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument) eo;
	}

	public void deleteEvenementDocumentsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementDocuments");
			}

	public void deleteAllEvenementDocumentsRelationships() {
		Enumeration objects = evenementDocuments().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteEvenementDocumentsRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementDocument)objects.nextElement());
		}
	}
	public NSArray evenementExtJuridiques() {
		return (NSArray)storedValueForKey("evenementExtJuridiques");
	}

	public NSArray evenementExtJuridiques(EOQualifier qualifier) {
		return evenementExtJuridiques(qualifier, null, false);
	}

	public NSArray evenementExtJuridiques(EOQualifier qualifier, boolean fetch) {
		return evenementExtJuridiques(qualifier, null, fetch);
	}

	public NSArray evenementExtJuridiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique.EVENEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = evenementExtJuridiques();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToEvenementExtJuridiquesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "evenementExtJuridiques");
	}

	public void removeFromEvenementExtJuridiquesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementExtJuridiques");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique createEvenementExtJuridiquesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_EvenementExtJuridique");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "evenementExtJuridiques");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique) eo;
	}

	public void deleteEvenementExtJuridiquesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementExtJuridiques");
				editingContext().deleteObject(object);
			}

	public void deleteAllEvenementExtJuridiquesRelationships() {
		Enumeration objects = evenementExtJuridiques().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteEvenementExtJuridiquesRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementExtJuridique)objects.nextElement());
		}
	}
	public NSArray evenementPersonnes() {
		return (NSArray)storedValueForKey("evenementPersonnes");
	}

	public NSArray evenementPersonnes(EOQualifier qualifier) {
		return evenementPersonnes(qualifier, null, false);
	}

	public NSArray evenementPersonnes(EOQualifier qualifier, boolean fetch) {
		return evenementPersonnes(qualifier, null, fetch);
	}

	public NSArray evenementPersonnes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne.EVENEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = evenementPersonnes();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToEvenementPersonnesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "evenementPersonnes");
	}

	public void removeFromEvenementPersonnesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementPersonnes");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne createEvenementPersonnesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_EvenementPersonne");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "evenementPersonnes");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne) eo;
	}

	public void deleteEvenementPersonnesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "evenementPersonnes");
			}

	public void deleteAllEvenementPersonnesRelationships() {
		Enumeration objects = evenementPersonnes().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteEvenementPersonnesRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementPersonne)objects.nextElement());
		}
	}
	public NSArray frequencesCrons() {
		return (NSArray)storedValueForKey("frequencesCrons");
	}

	public NSArray frequencesCrons(EOQualifier qualifier) {
		return frequencesCrons(qualifier, null, false);
	}

	public NSArray frequencesCrons(EOQualifier qualifier, boolean fetch) {
		return frequencesCrons(qualifier, null, fetch);
	}

	public NSArray frequencesCrons(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron.EVENEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = frequencesCrons();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToFrequencesCronsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "frequencesCrons");
	}

	public void removeFromFrequencesCronsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "frequencesCrons");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron createFrequencesCronsRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_FrequenceCron");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "frequencesCrons");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron) eo;
	}

	public void deleteFrequencesCronsRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "frequencesCrons");
			}

	public void deleteAllFrequencesCronsRelationships() {
		Enumeration objects = frequencesCrons().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteFrequencesCronsRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceCron)objects.nextElement());
		}
	}
	public NSArray frequencesSimples() {
		return (NSArray)storedValueForKey("frequencesSimples");
	}

	public NSArray frequencesSimples(EOQualifier qualifier) {
		return frequencesSimples(qualifier, null, false);
	}

	public NSArray frequencesSimples(EOQualifier qualifier, boolean fetch) {
		return frequencesSimples(qualifier, null, fetch);
	}

	public NSArray frequencesSimples(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple.EVENEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = frequencesSimples();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToFrequencesSimplesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "frequencesSimples");
	}

	public void removeFromFrequencesSimplesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "frequencesSimples");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple createFrequencesSimplesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_FrequenceSimple");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "frequencesSimples");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple) eo;
	}

	public void deleteFrequencesSimplesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "frequencesSimples");
			}

	public void deleteAllFrequencesSimplesRelationships() {
		Enumeration objects = frequencesSimples().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteFrequencesSimplesRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOFrequenceSimple)objects.nextElement());
		}
	}
	public NSArray taches() {
		return (NSArray)storedValueForKey("taches");
	}

	public NSArray taches(EOQualifier qualifier) {
		return taches(qualifier, null, false);
	}

	public NSArray taches(EOQualifier qualifier, boolean fetch) {
		return taches(qualifier, null, fetch);
	}

	public NSArray taches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlevenement.serveur.modele.EOTache.EVENEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlevenement.serveur.modele.EOTache.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = taches();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToTachesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOTache object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "taches");
	}

	public void removeFromTachesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOTache object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "taches");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOTache createTachesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkEvt_Tache");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "taches");
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOTache) eo;
	}

	public void deleteTachesRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOTache object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "taches");
				editingContext().deleteObject(object);
			}

	public void deleteAllTachesRelationships() {
		Enumeration objects = taches().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteTachesRelationship((org.cocktail.fwkcktlevenement.serveur.modele.EOTache)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOEvenement.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOEvenement.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOEvenement)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOEvenement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEvenement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOEvenement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOEvenement)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOEvenement fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOEvenement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOEvenement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOEvenement eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOEvenement)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOEvenement fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOEvenement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOEvenement eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOEvenement ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOEvenement createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOEvenement.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOEvenement.ENTITY_NAME + "' !");
		}
		else {
			EOEvenement object = (EOEvenement) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOEvenement localInstanceOfObject(EOEditingContext ec, EOEvenement object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOEvenement " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOEvenement) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
