/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionFrequenceCreation;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.quartz.SimpleTrigger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOFrequenceSimple extends _EOFrequenceSimple {

    public static final int REPEAT_INDEFINITLY = -1;
    
    public EOFrequenceSimple() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    /*
     * 
     */

	
	/**
	 * @see com.webobjects.eocontrol.EOCustomObject#toString()
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("FrequenceSimple {");
		buf.append(" repetition=");
		buf.append(repetition());
		buf.append(" ; repeatCount=");
		buf.append(repeatCount());
		buf.append(" ; repeatInterval=");
		buf.append(repeatInterval());
		buf.append(" ; traduction=");
		buf.append(traduction());
		buf.append(" ; dateFin=");
		buf.append(dateFin());
		buf.append(" ; evenement=");
		buf.append(evenement());
		buf.append(" }");
		return buf.toString();
	}

	/**
	 * Cet objet au format texte.
	 */
	public String toFullString() {
		StringBuffer buf = new StringBuffer();
		buf.append("FrequenceSimple {");
		buf.append("\n\t class=");
		buf.append(getClass());
		buf.append("\n\t ec=");
		buf.append(editingContext());
		buf.append("\n\t repetition=");
		buf.append(repetition());
		buf.append("\n\t repeatCount=");
		buf.append(repeatCount());
		buf.append("\n\t repeatInterval=");
		buf.append(repeatInterval());
		buf.append("\n\t traduction=");
		buf.append(traduction());
		buf.append("\n\t dateFin=");
		buf.append(dateFin());
		buf.append("\n\t evenement=");
		buf.append(evenement());
		buf.append("\n}");
		return buf.toString();
	}

	
	/**
	 * Calcule la prochaine occurence (date) d'apres des frequences simples.
	 * @param frequencesSimples Liste des frequences simples.
	 * @param afterTime Date (non comprise) a partir de laquelle calculer les occurences.
	 * @return Prochaine date d'apparition de cet evenement, ou <code>null</code> si aucune trouvee.
	 */
	public static Date getOccurenceApres(NSArray frequencesSimples, Date afterTime) {

		if (frequencesSimples == null)
			throw new NullPointerException("Frequences simples requises.");
		if (afterTime == null)
			throw new NullPointerException("Date mini requise.");
		
		if (frequencesSimples.count() < 1)
			return null;
		
		ArrayList occurences = new ArrayList();
		for (int i = 0; i < frequencesSimples.count(); i++) {
			EOFrequenceSimple freq = (EOFrequenceSimple) frequencesSimples.objectAtIndex(i);
			
			SimpleTrigger trigger = new SimpleTrigger(
					"xxxxxxx", "xxxxxx", freq.repeatCount().intValue(), freq.repeatInterval().intValue());
			trigger.setStartTime(freq.evenement().datePrevue());
			Date nextFireTime = trigger.getFireTimeAfter(afterTime);
//			System.out.println("FrequenceSimple.getOccurenceApres() nextFireTime = "+nextFireTime);
			if (nextFireTime!=null && (freq.dateFin()==null || !nextFireTime.after(freq.dateFin())))
				occurences.add(nextFireTime);
		}
		Date[] dates = (Date[]) occurences.toArray(new Date[0]);
		Arrays.sort(dates, new Comparator() {
			public int compare(Object o1, Object o2) {
				Date date1 = (Date) o1;
				Date date2 = (Date) o2;
				return date1.compareTo(date2);
			}
		});
		
		if (dates.length > 0)
			return dates[0];
		else 
			return null;
	}
	
	/**
	 * Cree et ajoute une frequence simple a l'evenement specifie.
	 * @param evenement Evenement concerne. Un finder existe.
	 * @param repeatCount Nombre de repetition voulu. Utiliser {@link EOFrequenceSimple#REPEAT_INDEFINITLY}
	 * pour repetition infinie.
	 * @param repeatInterval Interval de repetiton en millisecondes.
	 * @param traduction Traduction en francais de la frequence. Ex: "Tous les 14 jours".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @return La frequence simple creee.
	 * @throws ExceptionFrequenceCreation
	 */
	static public EOFrequenceSimple creerFrequenceSimple(
			EOEvenement evenement, 
			Number repeatCount, 
			Number repeatInterval, 
			String traduction, 
			NSTimestamp dateFin,
			EOEditingContext ec) throws ExceptionFrequenceCreation {
		
		EOFrequenceSimple frequence=null;
		try {
//			frequence = (FrequenceSimple) instanceForEntity(ec, FrequenceSimple.ENTITY_NAME);
			frequence = EOFrequenceSimple.create(ec);
		} 
		catch (Exception e) {
			throw new ExceptionFrequenceCreation("Impossible d'instancier la classe "+EOFrequenceSimple.ENTITY_NAME, e);
		}

		frequence.setEvenementRelationship(evenement.localInstanceIn(ec));
		frequence.setTraduction(traduction);
		frequence.setRepeatCount(repeatCount!=null ? new Integer(repeatCount.intValue()) : new Integer(0));
		frequence.setRepeatInterval(repeatInterval!=null ? new Integer(repeatInterval.intValue()) : new Integer(0));
		frequence.setDateFin(dateFin);
		
		evenement.addToFrequencesSimplesRelationship(frequence.localInstanceIn(ec));
		
		return frequence;
	}
	
	/**
	 * Modifie une frequence simple.
	 * @param frequence Frequence a modifier.
	 * @param repeatCount Nombre de repetition voulu. Utiliser {@link EOFrequenceSimple#REPEAT_INDEFINITLY}
	 * pour repetition infinie.
	 * @param repeatInterval Interval de repetiton en millisecondes.
	 * @param traduction Traduction en francais de la frequence. Ex: "Tous les 14 jours".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 */
	static public void modifierFrequenceSimple(
			EOFrequenceSimple frequence, 
			int  repeatCount, 
			int  repeatInterval, 
			String traduction, 
			NSTimestamp dateFin) {

		frequence.setTraduction(traduction);
		frequence.setRepeatCount(new Integer(repeatCount));
		frequence.setRepeatInterval(new Integer(repeatInterval));
		frequence.setDateFin(dateFin);
	}
	
	public static void main(String[] args) {
		Date now = new Date();
		Date startTime = DateUtilities.decaler(now, -5, Calendar.MINUTE);
		System.out.println("FrequenceSimple.main() now       = "+now);
		System.out.println("FrequenceSimple.main() startTime = "+startTime);
		SimpleTrigger trigger = new SimpleTrigger(
				"xxxxxxx", "xxxxxx", 20, 1000*60);
		trigger.setStartTime(startTime);
		Date nextFireTime = trigger.getFireTimeAfter(DateUtilities.decaler(now, -1, Calendar.MILLISECOND));
		//System.out.println("FrequenceSimple.getOccurenceApres() nextFireTime = "+nextFireTime);
	}
}
