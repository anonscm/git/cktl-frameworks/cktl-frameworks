/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTache.java instead.
package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

@SuppressWarnings("all")
public abstract class _EOTache extends  CktlServerRecord {
	public static final String ENTITY_NAME = "FwkEvt_Tache";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EVT_TACHE";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "tachId";

	public static final String CLASS_NAME_KEY = "className";
	public static final String DATA_KEY = "data";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIF_KEY = "dateModif";
	public static final String DESCRIPTION_KEY = "description";
	public static final String ETAT_KEY = "etat";
	public static final String GROUP_KEY = "group";
	public static final String NAME_KEY = "name";
	public static final String SEND_MAIL_AFTER_EXE_KEY = "sendMailAfterExe";
	public static final String TACH_ID_READ_ONLY_KEY = "tachIdReadOnly";
	public static final String TRIGGER_OFFSET_IN_MINUTES_KEY = "triggerOffsetInMinutes";

	// Non visible attributes
	public static final String PERSID_CREATION_KEY = "persidCreation";
	public static final String TACH_ID_KEY = "tachId";
	public static final String EVT_ID_KEY = "evtId";
	public static final String TYTA_ID_KEY = "tytaId";
	public static final String PERSID_MODIF_KEY = "persidModif";

	// Colkeys
	public static final String CLASS_NAME_COLKEY = "TACH_CLASS_NAME";
	public static final String DATA_COLKEY = "TACH_DATA";
	public static final String DATE_CREATION_COLKEY = "TACH_DATE_CREATION";
	public static final String DATE_MODIF_COLKEY = "TACH_DATE_MODIF";
	public static final String DESCRIPTION_COLKEY = "TACH_DESCRIPTION";
	public static final String ETAT_COLKEY = "TACH_ETAT";
	public static final String GROUP_COLKEY = "TACH_GROUP";
	public static final String NAME_COLKEY = "TACH_NAME";
	public static final String SEND_MAIL_AFTER_EXE_COLKEY = "TACH_SEND_MAIL_AFTER_EXE";
	public static final String TACH_ID_READ_ONLY_COLKEY = "TACH_ID";
	public static final String TRIGGER_OFFSET_IN_MINUTES_COLKEY = "TACH_TRIGGER_OFFSET_MINUTES";

	// Non visible colkeys
	public static final String PERSID_CREATION_COLKEY = "PERSID_CREATION";
	public static final String TACH_ID_COLKEY = "TACH_ID";
	public static final String EVT_ID_COLKEY = "EVT_ID";
	public static final String TYTA_ID_COLKEY = "TYTA_ID";
	public static final String PERSID_MODIF_COLKEY = "PERSID_MODIF";

	// Relationships
	public static final String EVENEMENT_KEY = "evenement";
	public static final String TYPE_KEY = "type";
	public static final String UTILISATEUR_CREATION_KEY = "utilisateurCreation";
	public static final String UTILISATEUR_MODIF_KEY = "utilisateurModif";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOTache with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param className
	 * @param dateCreation
	 * @param etat
	 * @param evenement
	 * @param type
	 * @param utilisateurCreation
	 * @return EOTache
	 */
	public static EOTache create(EOEditingContext editingContext, String className, NSTimestamp dateCreation, String etat, org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement, org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache type, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne utilisateurCreation) {
		EOTache eo = (EOTache) createAndInsertInstance(editingContext);
		eo.setClassName(className);
		eo.setDateCreation(dateCreation);
		eo.setEtat(etat);
		eo.setEvenementRelationship(evenement);
		eo.setTypeRelationship(type);
		eo.setUtilisateurCreationRelationship(utilisateurCreation);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOTache.
	 *
	 * @param editingContext
	 * @return EOTache
	 */
	public static EOTache create(EOEditingContext editingContext) {
		EOTache eo = (EOTache) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOTache localInstanceIn(EOEditingContext editingContext) {
		EOTache localInstance = (EOTache) localInstanceOfObject(editingContext, (EOTache) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOTache localInstanceIn(EOEditingContext editingContext, EOTache eo) {
		EOTache localInstance = (eo == null) ? null : (EOTache) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String className() {
		return (String) storedValueForKey("className");
	}

	public void setClassName(String value) {
		takeStoredValueForKey(value, "className");
	}
	public NSData data() {
		return (NSData) storedValueForKey("data");
	}

	public void setData(NSData value) {
		takeStoredValueForKey(value, "data");
	}
	public NSTimestamp dateCreation() {
		return (NSTimestamp) storedValueForKey("dateCreation");
	}

	public void setDateCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dateCreation");
	}
	public NSTimestamp dateModif() {
		return (NSTimestamp) storedValueForKey("dateModif");
	}

	public void setDateModif(NSTimestamp value) {
		takeStoredValueForKey(value, "dateModif");
	}
	public String description() {
		return (String) storedValueForKey("description");
	}

	public void setDescription(String value) {
		takeStoredValueForKey(value, "description");
	}
	public String etat() {
		return (String) storedValueForKey("etat");
	}

	public void setEtat(String value) {
		takeStoredValueForKey(value, "etat");
	}
	public String group() {
		return (String) storedValueForKey("group");
	}

	public void setGroup(String value) {
		takeStoredValueForKey(value, "group");
	}
	public String name() {
		return (String) storedValueForKey("name");
	}

	public void setName(String value) {
		takeStoredValueForKey(value, "name");
	}
	public String sendMailAfterExe() {
		return (String) storedValueForKey("sendMailAfterExe");
	}

	public void setSendMailAfterExe(String value) {
		takeStoredValueForKey(value, "sendMailAfterExe");
	}
	public Integer tachIdReadOnly() {
		return (Integer) storedValueForKey("tachIdReadOnly");
	}

	public void setTachIdReadOnly(Integer value) {
		takeStoredValueForKey(value, "tachIdReadOnly");
	}
	public Integer triggerOffsetInMinutes() {
		return (Integer) storedValueForKey("triggerOffsetInMinutes");
	}

	public void setTriggerOffsetInMinutes(Integer value) {
		takeStoredValueForKey(value, "triggerOffsetInMinutes");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement)storedValueForKey("evenement");
	}

	public void setEvenementRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement oldValue = evenement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "evenement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "evenement");
		}
	}
  
	public org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache type() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache)storedValueForKey("type");
	}

	public void setTypeRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EOTypeTache oldValue = type();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "type");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "type");
		}
	}
  
	public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne utilisateurCreation() {
		return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey("utilisateurCreation");
	}

	public void setUtilisateurCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
		if (value == null) {
			org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = utilisateurCreation();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "utilisateurCreation");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "utilisateurCreation");
		}
	}
  
	public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne utilisateurModif() {
		return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey("utilisateurModif");
	}

	public void setUtilisateurModifRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
		if (value == null) {
			org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = utilisateurModif();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "utilisateurModif");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "utilisateurModif");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOTache.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOTache.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOTache)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOTache fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTache fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOTache eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOTache)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOTache fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOTache fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOTache fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOTache eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOTache)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOTache fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOTache fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOTache eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOTache ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOTache createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOTache.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOTache.ENTITY_NAME + "' !");
		}
		else {
			EOTache object = (EOTache) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOTache localInstanceOfObject(EOEditingContext ec, EOTache object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOTache " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOTache) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
