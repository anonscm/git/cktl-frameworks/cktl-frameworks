/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import javax.annotation.Nullable;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.FwkCktlEvenementPrincipal;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionEvenementCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionEvenementSuppression;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.FwkCktlEvenementSchedulerUtil;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.geide.fwkcktlgedibus.tools.GedTool;
import org.quartz.CronExpression;
import org.quartz.Scheduler;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.validation.ERXValidationFactory;

public class EOEvenement extends _EOEvenement {

    private static final String BadDatePrevue = "BadDatePrevue";
    
    @Inject @Nullable
    private UserInfo userInfo;
    
    public EOEvenement() {
        super();
    }

    @Override
    public void awakeFromInsertion(EOEditingContext editingContext) {
        setDateCreation(new NSTimestamp());
        super.awakeFromInsertion(editingContext);
    }
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    @Override
    public void didUpdate() {
        super.didUpdate();
        if ( FwkCktlEvenementPrincipal.instance().appInstanceOwnScheduler() &&
                editingContext() != null && editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
            Scheduler scheduler = FwkCktlEvenementPrincipal.instance().getScheduler();
            try {
                FwkCktlEvenementUtil.LOG.info("In didUpdate de l'evenement " + this + " - va reprogrammer le(s) job(s) quartz correspondant");
                FwkCktlEvenementSchedulerUtil.programmerTachesEvenement(this, new NSTimestamp(), scheduler, editingContext());
            } catch (ExceptionOperationImpossible e) {
                FwkCktlEvenementUtil.LOG.warn(e);
            }
        }
    }
    
    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        if (datePrevue().before(new NSTimestamp())) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "BadDatePrevue");
        }
        if (userInfo != null && userInfo.persId() != null) {
            setPersidModif(userInfo.persId().intValue());
        }
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        setDateModif(new NSTimestamp());
    }
    
    /**
     * @return la derniere et (seule) frequence cron
     */
    public EOFrequenceCron lastFrequenceCron() {
        return (EOFrequenceCron)frequencesCrons().lastObject();
    }
    
    /**
     * @return l'extension juridique
     */
    public EOEvenementExtJuridique evenementExtJuridique() {
        return (EOEvenementExtJuridique) evenementExtJuridiques().lastObject();
    }
    
    /**
     * @param evtExt
     */
    public void setEvenementExtJuridique(EOEvenementExtJuridique evtExt) {
        deleteAllEvenementExtJuridiquesRelationships();
        addToEvenementExtJuridiquesRelationship(evtExt);
    }
    
    /**
     * @return l'ensemble des {@link EOEvenementPersonne} concernées par l'alerte mail.
	 */
	public NSArray evenementPersonnesAlertables() {
		return EOQualifier.filteredArrayWithQualifier(
				super.evenementPersonnes(), 
				EOEvenementPersonne.QUALIFIER_ALERTER_OUI);
	}
	
	/**
	 * @return all the {@link IPersonne} corresponding to the 
	 * {@link EOEvenementPersonne} list of this {@link EOEvenement}
	 */
	public NSArray personnes() {
		return ((NSArray) evenementPersonnes().valueForKey(EOEvenementPersonne.TO_PERSONNE_KEY));
	}
	
	public void addToPersonnes(NSArray<IPersonne> personnes) {
	    NSArray<Integer> persIds = (NSArray<Integer>) personnes().valueForKey(EOIndividu.PERS_ID_KEY);
	    for (IPersonne personne : personnes) {
	        if (!persIds.contains(personne.persId())) {
	            EOEvenementPersonne evPers = EOEvenementPersonne.creerEvenementPersonne(this, personne, null, true, editingContext());
	            evPers.setPersId(personne.persId());
	            addToEvenementPersonnesRelationship(evPers);
	        }
	    }
	}
	
	public void removeFromPersonnes(IPersonne personne) {
	    NSArray<EOEvenementPersonne> evtPersonnes = 
	        evenementPersonnes(ERXQ.equals(EOEvenementPersonne.PERS_ID_KEY, personne.persId())).immutableClone();
	    for (EOEvenementPersonne evtPersonne : evtPersonnes) {
	        removeFromEvenementPersonnesRelationship(evtPersonne);
	        evtPersonne.delete();
	    }
	}
	
	/**
	 * Retourne la tache d'alerte par mail eventuellement associee a cet evenement.
	 * @return Tache trouvee.
	 */
	public EOTache tacheAlerteMail() {
		// NB: pour l'instant une seule tache d'alerte mail par evenement
		NSArray tachesAlerteMail = EOQualifier.filteredArrayWithQualifier(taches(), EOTache.QUALIFIER_TYPE_TACHE_ALERTE_MAIL);
		return (EOTache) tachesAlerteMail.lastObject();
	}

	
	/**
	 * Retourne la liste des couNumero des documents associes a cet evenement.
	 * @return Liste de {@link Number}
	 */
	public NSArray documentsCouNumeros() {
		return (NSArray) evenementDocuments().valueForKey("couNumero");
	}
	/**
	 * Retourne la liste des URLs et noms des documents associes a cet evenement.
	 * @return Trois listes de {@link String}: 
	 * liste des URLS en 0 ; 
	 * liste des noms de fichier en 1.
	 */
	public NSArray[] documentsUrlsNamesAndDates() {
		NSArray evenementDocuments = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				evenementDocuments(), 
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOEvenementDocument.COU_NUMERO_KEY, EOSortOrdering.CompareAscending)));
		NSMutableArray urls = new NSMutableArray();
		NSMutableArray names = new NSMutableArray();
		NSArray couNumeros = (NSArray) evenementDocuments.valueForKey(EOEvenementDocument.COU_NUMERO_KEY);
		for (int i = 0; i < couNumeros.count(); i++) {
			Number couNumero = (Number) couNumeros.objectAtIndex(i);
			GedTool gedTool = new GedTool(FwkCktlEvenementPrincipal.instance().newEditingContextForJobs());
			String url = (String) gedTool.listUrlFileIntoGed(new Integer(couNumero.intValue())).lastObject();
			String name = (String) gedTool.listNameFileIntoGed((Integer)couNumero, 30626).lastObject();
			urls.addObject(url);
			names.addObject(name);
		}
		return new NSArray[] { urls, names };
	}
	
	/**
	 * Calcule la prochaine occurence (date) de cet evenement , d'apres ses frequences CRON associees.
	 * @param afterTime Date (non comprise) a partir de laquelle calculer les occurences.
	 * @return Prochaine date d'apparition de cet evenement, ou <code>null</code> si aucune trouvee.
	 */
	public Date getOccurenceApresFromFrequencesCron(Date afterTime, int offsetInMinutes) {

		if (afterTime == null)
			throw new NullPointerException("Date mini requise.");
		if (frequencesCrons().count() < 1)
			return null;

		Date datePrevueEvtDecalee = datePrevue();
		Date dateMin=afterTime;
		if (offsetInMinutes != 0)
			datePrevueEvtDecalee = DateUtilities.decaler(datePrevueEvtDecalee, offsetInMinutes, Calendar.MINUTE);
		
		if (dateMin.before(datePrevueEvtDecalee)) 
			dateMin = datePrevueEvtDecalee;
		
		FwkCktlEvenementUtil.debugPrintArray("frequences cron pour getOccurenceSuivanteFromFrequencesCron", frequencesCrons(), FwkCktlEvenementUtil.LOG);
		
		ArrayList occurences = new ArrayList();
		for (int i = 0; i < frequencesCrons().count(); i++) {
			EOFrequenceCron freq = (EOFrequenceCron) frequencesCrons().objectAtIndex(i);
			Date nextFireTime;
			try {
				CronExpression expr = new CronExpression(freq.cronExpression());
				nextFireTime = expr.getNextValidTimeAfter(afterTime);
				//System.out.println("Evenement.getOccurenceSuivanteFromFrequencesCron() nextFireTime = "+nextFireTime);
				if (nextFireTime != null)
					occurences.add(nextFireTime);
			} 
			catch (ParseException e) {
				throw new IllegalStateException("L'expression CRON est invalide dans la frequence suivante: "+freq);
			}
		}
		Date[] dates = (Date[]) occurences.toArray(new Date[0]);
		Arrays.sort(dates, new Comparator() {
			public int compare(Object o1, Object o2) {
				Date date1 = (Date) o1;
				Date date2 = (Date) o2;
				return date1.compareTo(date2);
			}
		});
		
		if (dates.length > 0)
			return dates[0];
		else 
			return null;
	}
	
	/**
	 * Calcule la prochaine occurence (date) de cet evenement , d'apres ses frequences CRON et simples associees.
	 * @param afterTime Date (non comprise) a partir de laquelle calculer les occurences.
	 * @return Prochaine date d'apparition de cet evenement, ou <code>null</code> si aucune trouvee.
	 */
	public Date getOccurenceApres(Date afterTime) {

		Date next;
		if (frequencesCrons().count() + frequencesSimples().count() > 0) {
			
//			Date nextFromCrons = getOccurenceApresFromFrequencesCron(afterTime);
//			Date nextFromSimples = getOccurenceApresFromFrequencesSimples(afterTime);
			Date nextFromCrons = EOFrequenceCron.getOccurenceApres(frequencesCrons(), afterTime);
			Date nextFromSimples = EOFrequenceSimple.getOccurenceApres(frequencesSimples(), afterTime);
			
			if (nextFromCrons!=null && nextFromSimples!=null) {
				next = nextFromCrons.before(nextFromSimples) ? nextFromCrons : nextFromSimples;
			}
			else if (nextFromCrons == null) {
				next = nextFromSimples;
			}
			else {
				next = nextFromCrons;
			}
		}
		else {
			next = afterTime.before(datePrevue()) ? datePrevue() : null;
		}
		
		return next;
	}

	/**
	 * Retourne la date de fin de repetition de cet evenement (via sa frequence). 
	 * En realite un evenement peut posseder plusieurs repetitions differentes (via ses frequences CRONs ou SIMPLEs)
	 * mais pour l'instant on decide de ne considerer qu'une seule repetition (celle correspondant a la premiere 
	 * frequence CRON ou frequence simple trouvee).
	 * @return La date de fin de repetition ou <code>null</code> si aucune trouvee.
	 */
	public Date dateFinRepetition() {
		Date dateFin=null;
		if (frequencesCrons()!=null && frequencesCrons().count()>0)
			dateFin = ((EOFrequenceCron) frequencesCrons().objectAtIndex(0)).dateFin();
		if (frequencesSimples()!=null && frequencesSimples().count()>0) {
			Date tmp = ((EOFrequenceSimple) frequencesSimples().objectAtIndex(0)).dateFin();
			dateFin = dateFin!=null && dateFin.after(tmp) ? dateFin : tmp;
		}
		return dateFin;
	}
	
	/**
	 * <p>Fabrique la liste des adresses mail des personnes concernees par cete evenement, et la liste des personnes dont l'adresse
	 * mail n'a pas pu etre trouvee.
	 * <p>Si une personne possede plusieurs adresses mail (cad plusieurs <code>Compte</code>s dans GRHUM), elles peuvent etre
	 * toutes ajoutees a la liste.
	 * @param inclureToutesLesAdresses Si <code>true</code>, toutes les adresses d'une meme personne seront ajoutee a la liste.
	 * Sinon, seule la premiere trouvee le sera.
	 * @return 2 objets : 
	 * [0] = {@link NSArray} des adresses mails, ex d'adresse:"bobby.joe@cocktail.org" ; 
	 * [1] = {@link NSArray} des noms des personnes concernees dont l'adresse mail n'a pas ete trouvee, 
	 * ex de nom: "Bobby Joe (persId=12345)"
	 * @throws ExceptionOperationImpossible
	 */
	public NSArray[] getMailsPersonnesConcernees(boolean inclureToutesLesAdresses) throws ExceptionOperationImpossible {

	    // recherche des personnes concernees a alerter
	    NSArray<IPersonne> personnesConcerneesAlertables = 
	        (NSArray<IPersonne>) evenementPersonnesAlertables().valueForKey(EOEvenementPersonne.TO_PERSONNE_KEY);

	    if (personnesConcerneesAlertables.count() < 1)
	        throw new ExceptionOperationImpossible("Aucune personne concernée à alerter n'a été trouvée.");

	    NSArray[] resultat = new NSArray[2];
	    NSMutableArray<String> personnesConcerneesSansAdresseMail = new NSMutableArray<String>();
	    NSMutableArray<String> toutesLesAdressesMail = new NSMutableArray<String>();
	    for (IPersonne personne : personnesConcerneesAlertables) {
	        // Si structure, on prend les aliases mail de la structure
	        if (personne.isStructure()) {
	            EOStructure structure = (EOStructure)personne;
	            if (!structure.aliases().isEmpty()) {
	                if (!inclureToutesLesAdresses)
	                    toutesLesAdressesMail.addObject((String)structure.aliases().objectAtIndex(0));
	                else
	                    toutesLesAdressesMail.addObjectsFromArray(structure.aliases());
	            } else {
	                personnesConcerneesSansAdresseMail.addObject(
	                        structure.persLibelle()+" "+structure.persLc()+" (persId="+structure.persId()+")");
	            }
	        } else {
	            NSArray<EOCompte> comptes = (NSArray<EOCompte>) personne.toComptes(null);
	            for (EOCompte compte : comptes) {
	                // ajout de l'adresse mail trouvee pour la personne (si pas deja ajoutee)
	                if (compte.toCompteEmail() != null && compte.toCompteEmail().getEmailFormatte()!=null 
	                        && !toutesLesAdressesMail.containsObject(
	                                compte.toCompteEmail().getEmailFormatte())) {
	                    toutesLesAdressesMail.addObject(
	                            compte.toCompteEmail().getEmailFormatte());
	                }
	                if (!inclureToutesLesAdresses)
	                    break;
	            }
	            // si aucune adresse trouve pour cette personne
	            if (comptes.count() < 1) {
	                personnesConcerneesSansAdresseMail.addObject(
	                        personne.persLibelle()+" "+personne.persLc()+" (persId="+personne.persId()+")");
	            }
	        }

	    }
	    if (toutesLesAdressesMail.count() < 1) {
	        throw new ExceptionOperationImpossible("Aucune adresse mail trouvée pour les personnes concernées.");
	    }

	    resultat[0] = toutesLesAdressesMail.immutableClone();
	    resultat[1] = personnesConcerneesSansAdresseMail.immutableClone();

	    return resultat;
	}
	
	/**
	 * 
	 * @param etat
	 * @return l'ensemble des taches dans l'état donné.
	 */
	public NSArray tachesDansEtat(String etat) {
		EOQualifier qualifier = 
			EOQualifier.qualifierWithQualifierFormat("etat = %@", new NSArray(etat));
		return EOQualifier.filteredArrayWithQualifier(taches(), qualifier);
	}
	
	public EOPersonne utilisateurCreation() {
	    return EOPersonne.fetchFirstByQualifier(editingContext(), ERXQ.equals(EOPersonne.PERS_ID_KEY, persidCreation()));
	}
	
	public EOPersonne utilisateurModif() {
	    return EOPersonne.fetchFirstByQualifier(editingContext(), ERXQ.equals(EOPersonne.PERS_ID_KEY, persidModif()));
	}
	
    /**
     * 
     * @return la desscription en HTML
     */
    public String getHtmlDescriptionPourMail() {
    	
		StringBuffer buf = new StringBuffer();
		
		// generalites
		buf.append("<b>N°: </b>");
		buf.append(evtIdReadOnly());
		buf.append("<br>");
		buf.append("<b>Date prévue: </b>");
		buf.append(DateUtilities.print(datePrevue(), true, true));
		buf.append("<br>");
		buf.append("<b>Objet: </b>");
		buf.append(objet());
		buf.append("<br>");
		buf.append("<b>Observations: </b>");
		buf.append(observations());
		buf.append("<br>");
		buf.append("<b>Type d'événement: </b>");
		buf.append(type().libelleLong());
		buf.append("<br>");
		buf.append("Etat: ");
		buf.append(etat().libelleLong());
		buf.append("<br>");
		buf.append("Créé par: ");
		buf.append(utilisateurCreation().persLc()+" "+utilisateurCreation().persLibelle());
		buf.append("<br>");
		if (utilisateurModif()!=null && !utilisateurCreation().equals(utilisateurModif())) {
			buf.append("Modifié par: ");
			buf.append(utilisateurModif().persLc()+" "+utilisateurModif().persLibelle());
			buf.append("<br>");
		}

		// personnes concernees
		buf.append("----");
		buf.append("<br>");
		buf.append("<b>Personnes concernées déclarées: </b>");
		if (evenementPersonnes().count() > 0) {
			buf.append("<br>");
			for (int i = 0; i < evenementPersonnes().count(); i++) {
				EOEvenementPersonne ep = (EOEvenementPersonne) evenementPersonnes().objectAtIndex(i);
				buf.append("   - ");
				buf.append(ep.toPersonne().persLc()+" "+ep.toPersonne().persLibelle());
				if (ep.role() != null) {
					buf.append(" (");
					buf.append(ep.role().libelleLong());
					buf.append(")");
				}
				buf.append("<br>");
			}
		}
		else {
			buf.append("Aucune. ");
			buf.append("<br>");
		}
		
		// Périodicité
		buf.append("----");
		buf.append("<br>");
		buf.append("<b>Périodicité de l'événement: </b>");
		if (frequencesCrons().count() > 0) {
			buf.append("<br>");
			for (int i = 0; i < frequencesCrons().count(); i++) {
				EOFrequenceCron fc = (EOFrequenceCron) frequencesCrons().objectAtIndex(i);
				buf.append("   - ");
				buf.append(fc.traduction()!=null ? fc.traduction() : "Aucune traduction disponible");
				buf.append(" ");
				buf.append("(Fin: ");
				if (fc.dateFin() != null) {
					buf.append(DateUtilities.print(fc.dateFin(), true, true));
					buf.append(".");
				}
				else {
					buf.append("Jamais.");
				}
				buf.append(")<br>");
			}
		}
		else {
			buf.append("Aucune. ");
			buf.append("<br>");
		}
		
		// documents
		NSArray[] arrays = documentsUrlsNamesAndDates();
		NSArray documentsUrls = arrays[0];
		NSArray documentsNames = arrays[1];
		buf.append("----");
		buf.append("<br>");
		buf.append("<b>Documents associés: </b>");
		if (documentsUrls.count() > 0) {
			buf.append("<br>");
			for (int i = 0; i < documentsUrls.count(); i++) {
				buf.append("<a href=\"");
				buf.append(documentsUrls.objectAtIndex(i));
				buf.append("\">");
				buf.append(documentsNames.objectAtIndex(i));
				buf.append("</a><br>");
			}
		}
		else {
			buf.append("Aucun. ");
			buf.append("<br>");
		}
		
		return buf.toString();
    }
	
	/**
	 * @see com.webobjects.eocontrol.EOCustomObject#toString()
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Evenement {");
		buf.append(" date=");
		buf.append(datePrevue());
		buf.append(" ; objet=");
		buf.append(objet());
		buf.append(" ; observ=");
		buf.append(observations());
		buf.append(" ; type=");
		buf.append(type());
		buf.append(" ; etat=");
		buf.append(etat());
		buf.append(" ; statut=");
		buf.append(statut());
		buf.append(" }");
		return buf.toString();
	}

	/**
	 * Cet objet au format texte.
	 */
	public String toFullString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Evenement {");
		buf.append("\n\t ec=");
		buf.append(editingContext());
		buf.append("\n\t date=");
		buf.append(datePrevue());
		buf.append("\n\t objet=");
		buf.append(objet());
		buf.append("\n\t observ=");
		buf.append(observations());
		buf.append("\n\t type=");
		buf.append(type());
		buf.append("\n\t etat=");
		buf.append(etat());
		buf.append("\n\t statut=");
		buf.append(statut());
		buf.append("\n}");
		return buf.toString();
	}

    
    /*
     * Finders
     */
    /**
	 * Renvoie l'evenment ayant l'identifiant evtId.
	 * @param evtId
	 * @return l'evenement correspondant à l'identifiant.
	 * @throws ExceptionFinder 
	 */
	static public EOEvenement findWithEvtId(Number evtId, EOEditingContext ec) {
		return EOEvenement.fetchFirstByKeyValue(ec, EOEvenement.EVT_ID_KEY, evtId);
	}

	/*
	 * Factories
	 */
	/**
	 * Cree un nouvel evenement.
	 * @param type Type de l'evenement. Un finder existe.
	 * @param datePrevue Date/heure prevue de l'evenement.
	 * @param objet Objet/intitule de l'evenement.
	 * @param observations Observations/remarques eventuelles.
	 * @param utilisateur Createur de l'evenement.
	 * @param ec EditingContext de création
	 * @return L'evenement cree.
	 * @throws ExceptionEvenementCreation
	 */
	static public EOEvenement creerEvenement(
			final EOEvenementType type,
			final NSTimestamp datePrevue,
			final String objet,
			final String observations,
			final String appName,
			final EOPersonne utilisateur,
			EOEditingContext ec) throws ExceptionEvenementCreation {

		if (type == null)
			throw new IllegalArgumentException("Le type de l'evenement doit etre fourni.");
		if (datePrevue == null)
			throw new IllegalArgumentException("La date prevue de l'evenement doit etre fournie.");
		if (objet == null)
			throw new IllegalArgumentException("L'objet de l'evenement doit etre fourni.");
		if (utilisateur == null)
			throw new IllegalArgumentException("L'utilisateur createur de l'evenement doit etre fourni.");
		
		
		EOEvenement evenement;
		try {
//			evenement = (EOEvenement) instanceForEntity();
			evenement = EOEvenement.create(ec);
		} 
		catch (Exception e) {
			throw new ExceptionEvenementCreation("Impossible d'instancier la classe "+EOEvenement.ENTITY_NAME, e);
		}
		
//		ec.insertObject(evenement);
		
		evenement.setDatePrevue(datePrevue);
		evenement.setTypeRelationship(type.localInstanceIn(ec));
		evenement.setObjet(objet);
		evenement.setAppName(appName);
		evenement.setObservations(observations);
		try {
			if (datePrevue.before(new NSTimestamp())) {
			    EOEvenementEtat etat = EOEvenementEtat.findWithIdInterne(EOEvenementEtat.ID_INTERNE_REALISE, ec);
				evenement.setEtatRelationship(etat);
			}else {
			    EOEvenementEtat etat = EOEvenementEtat.findWithIdInterne(EOEvenementEtat.ID_INTERNE_PREVU, ec);
				evenement.setEtatRelationship(etat);
			}
		} catch (Exception e) {
			throw new ExceptionEvenementCreation("Impossible d'initialiser l'état de l'événement.", e);
		}
			
		evenement.setPersidCreation(utilisateur.persId());
		evenement.setPersidModif(utilisateur.persId());
		evenement.setDateCreation(new NSTimestamp());
		
		return evenement;
	}
	
	/**
	 * <p>Supprime un evenement.
	 * <p> Les documents, personnes, frequences CRON et simples associes sont supprimes implicitement
	 * car on a coche "delete rule: cascade" et "owns destination" dans le modele.
	 * <p>Attention: par contre, si l'evenement possede encore des taches, la suppresion sera refusee.
	 * @param evenement EOEvenement a supprimer.
	 * @throws ExceptionEvenementSuppression 
	 */
	static public void supprimerEvenement(
			final EOEvenement evenement,
			EOEditingContext ec) {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement à supprimer doit être fourni.");
		
		if (evenement.taches().count() > 0)
			throw new ValidationException("L'evenement à supprimer possède encore des tâches, supprimez-les d'abord.");
			
		evenement.setTypeRelationship(null);
		evenement.setEtatRelationship(null);
		evenement.setPersidCreation(null);
		evenement.setPersidModif(null);
		
		ec.deleteObject(evenement);
	}
	/**
	 * <p>Supprime des evenements.
	 * <p> Les documents, personnes, frequences CRON et simples associes sont supprimes implicitement
	 * car on a coche "delete rule: cascade" et "owns destination" dans le modele.
	 * <p>Attention: par contre, si un evenement possede encore des taches, la suppresion sera refusee.
	 * @param evenements EOEvenements a supprimer.
	 * @throws ExceptionEvenementSuppression 
	 */
	static public void supprimerEvenements(
			final NSArray evenements,
			EOEditingContext ec) throws ExceptionEvenementSuppression {

		if (evenements == null)
			throw new IllegalArgumentException("La liste d'evenements doit être fournie.");
		
		for (int i=0; i<evenements.count(); i++) {
			EOEvenement evenement = (EOEvenement) evenements.objectAtIndex(i);
			EOEvenement.supprimerEvenement(evenement,ec);
		}
	}
	

	/**
	 * Modifie un evenement existant, sans repercussion dans la base.
	 * @param evenement EOEvenement a modifier.
	 * @param type Type de l'evenement. Un finder existe.
	 * @param datePrevue Date/heure prevue de l'evenement.
	 * @param objet Objet/intitule de l'evenement.
	 * @param observations Observations/remarques eventuelles.
	 * @param etat Etat de l'evenement. Un finder existe.
	 * @param utilisateur Createur de l'evenement {@link EOPersonne}.
	 */
	static public void modifierEvenement(
			final EOEvenement evenement,
			final EOEvenementType type,
			final NSTimestamp datePrevue,
			final String objet,
			final String observations,
			final String appName,
			final EOEvenementEtat etat,
			final EOPersonne utilisateur) {

		if (evenement == null)
			throw new IllegalArgumentException("Evenement a modifier requis.");
		if (type == null)
			throw new IllegalArgumentException("Le type de l'evenement doit etre fourni.");
		if (datePrevue == null)
			throw new IllegalArgumentException("La date prevue de l'evenement doit etre fournie.");
		if (objet == null)
			throw new IllegalArgumentException("L'objet de l'evenement doit etre fourni.");
		if (etat == null)
			throw new IllegalArgumentException("L'etat de l'evenement doit etre fourni.");
		if (utilisateur == null)
			throw new IllegalArgumentException("L'utilisateur modificateur de l'evenement doit etre fourni.");

		EOEvenementEtat.validerEtat(etat, datePrevue);
		
		evenement.setDatePrevue(datePrevue);
		evenement.setTypeRelationship(type);
		evenement.setObjet(objet);
		evenement.setObservations(observations);
		evenement.setAppName(appName);
		evenement.setEtatRelationship(etat);
		
		evenement.setPersidModif(utilisateur.persId());
		evenement.setDateModif(new NSTimestamp());
	}
	
	public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
	
}
