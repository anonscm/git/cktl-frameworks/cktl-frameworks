/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionFrequenceCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.CronUtilities;
import org.quartz.CronExpression;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOFrequenceCron extends _EOFrequenceCron {

    public EOFrequenceCron() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
	 * @see com.webobjects.eocontrol.EOCustomObject#validateForSave()
	 */
	public void validateForSave() throws ValidationException {
		this.validateObjectMetier();
        validateBeforeTransactionSave();
		super.validateForSave();
	}
	
    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	if (!CronExpression.isValidExpression(cronExpression()))
    		throw new NSValidation.ValidationException("L'expression CRON suivante n'est pas valide : "+cronExpression());
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
       
    }

    
    /**
	 * @see com.webobjects.eocontrol.EOCustomObject#toString()
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("FrequenceCron {");
		buf.append(" repetition=");
		buf.append(repetition());
		buf.append(" ; cronExpression=");
		buf.append(cronExpression());
		buf.append(" ; traduction=");
		buf.append(traduction());
		buf.append(" ; dateFin=");
		buf.append(dateFin());
		buf.append(" ; evenement=");
		buf.append(evenement());
		buf.append(" }");
		return buf.toString();
	}

	/**
	 * Cet objet au format texte.
	 */
	public String toFullString() {
		StringBuffer buf = new StringBuffer();
		buf.append("FrequenceCron {");
		buf.append("\n\t class=");
		buf.append(getClass());
		buf.append("\n\t ec=");
		buf.append(editingContext());
		buf.append("\n\t repetition=");
		buf.append(repetition());
		buf.append("\n\t cronExpression=");
		buf.append(cronExpression());
		buf.append("\n\t traduction=");
		buf.append(traduction());
		buf.append("\n\t dateFin=");
		buf.append(dateFin());
		buf.append("\n\t evenement=");
		buf.append(evenement());
		buf.append("\n}");
		return buf.toString();
	}

	/**
     * 
     */
    public void updateSummaryAttributes() {
		NSDictionary dico;
		try {
			dico = CronUtilities.getCronExpressionSummaryDictionary(new CronExpression(cronExpression()));
			setCronExpressionSeconds((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_seconds)); 
			setCronExpressionMinutes((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_minutes)); 
			setCronExpressionHours((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_hours)); 
			setCronExpressionDaysofmonth((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_daysOfMonth)); 
			setCronExpressionMonths((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_months));  
			setCronExpressionDaysofweek((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_daysOfWeek)); 
			setCronExpressionLastdayofweek((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_lastdayOfWeek)); 
			setCronExpressionNearestweekday((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_nearestWeekday)); 
			setCronExpressionNthdayofweek((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_NthDayOfWeek)); 
			setCronExpressionLastdayofmonth((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_lastdayOfMonth)); 
			setCronExpressionYears((String) dico.valueForKey(CronUtilities.SUMMARY_KEY_years)); 
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }

	/**
	 * Calcule la prochaine occurence (date) d'apres des frequences CRON.
	 * @param frequencesCrons Liste des frequences CRON
	 * @param afterTime Date (non comprise) a partir de laquelle calculer les occurences.
	 * @return Prochaine date d'apparition de cet evenement, ou <code>null</code> si aucune trouvee.
	 */
	public static Date getOccurenceApres(NSArray frequencesCrons, Date afterTime) {

		if (frequencesCrons == null)
			throw new NullPointerException("Frequences CRON requises.");
		if (afterTime == null)
			throw new NullPointerException("Date mini requise.");
		
		if (frequencesCrons.count() < 1)
			return null;

		ArrayList occurences = new ArrayList();
		for (int i = 0; i < frequencesCrons.count(); i++) {
			EOFrequenceCron freq = (EOFrequenceCron) frequencesCrons.objectAtIndex(i);
			Date nextFireTime;
			try {
				CronExpression expr = new CronExpression(freq.cronExpression());
				nextFireTime = expr.getNextValidTimeAfter(afterTime);
				//System.out.println("FrequenceCron.getOccurenceApres() nextFireTime = "+nextFireTime);
				if (nextFireTime!=null && (freq.dateFin()==null || !nextFireTime.after(freq.dateFin())))
					occurences.add(nextFireTime);
			} 
			catch (ParseException e) {
				throw new IllegalStateException("L'expression CRON est invalide dans la frequence suivante: "+freq);
			}
		}
		Date[] dates = (Date[]) occurences.toArray(new Date[0]);
		Arrays.sort(dates, new Comparator() {
			public int compare(Object o1, Object o2) {
				Date date1 = (Date) o1;
				Date date2 = (Date) o2;
				return date1.compareTo(date2);
			}
		});
		
		if (dates.length > 0)
			return dates[0];
		else 
			return null;
	}
	
	/*
	 * Factory
	 */
	
	/**
	 * Cree et ajoute une frequence CRON a l'evenement specifie.
	 * @param evenement Evenement concerne. Un finder existe.
	 * @param repetition Type de repetition (periodicite) de l'evenement. Un finder existe.
	 * @param cronExpression Expression CRON qui specifie la periodicite. 
	 * @param traduction Traduction en français de la frequence. Ex: "Tous les jours", "Le 1,5,7 du mois".
	 * @param dateFin Date de fin de repetition ou <code>null</code>.
	 * @param ec {@link EOEditingContext} utilisé pour créer la frequence CRON.
	 * @return La frequence CRON creee.
	 * @throws ExceptionFrequenceCreation
	 */
	static public EOFrequenceCron creerFrequenceCron(
			EOEvenement evenement, 
			EORepetition repetition,
			String cronExpression, 
			String traduction, 
			NSTimestamp dateFin,
			EOEditingContext ec) throws ExceptionFrequenceCreation {

		if (evenement == null)
			throw new NullPointerException("Evénement requis.");
		if (repetition == null)
			throw new NullPointerException("Type de répétition requis.");
		if (cronExpression == null)
			throw new NullPointerException("Expression CRON requise.");
		if (!CronExpression.isValidExpression(cronExpression))
			throw new ExceptionFrequenceCreation("Expression CRON invalide: "+cronExpression);
		if (traduction == null)
			throw new NullPointerException("Traduction en clair requise.");
		
		EOFrequenceCron frequence=null;
		try {
//			frequence = (FrequenceCron) instanceForEntity(ec, FrequenceCron.ENTITY_NAME);
			frequence = EOFrequenceCron.create(ec);
		} 
		catch (Exception e) {
			throw new ExceptionFrequenceCreation("Impossible d'instancier la classe "+EOFrequenceCron.ENTITY_NAME, e);
		}
		
		frequence.setEvenementRelationship(evenement.localInstanceIn(ec));
		frequence.setRepetitionRelationship(repetition.localInstanceIn(ec));
		frequence.setTraduction(traduction);
		frequence.setDateFin(dateFin);
		
		frequence.setCronExpression(cronExpression);
		frequence.updateSummaryAttributes();
		
		return frequence;
	}

}
