/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

import org.cocktail.fwkcktlevenement.common.exception.ExceptionEvenementDocumentCreation;

public class EOEvenementDocument extends _EOEvenementDocument {

    public EOEvenementDocument() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    /*
     * Factories
     */
    /**
	 * Cree une nouvelle association de document a un evenement.
	 * @param evenement Evenement concerne.
	 * @param couNumero Reference vers un document GED.
	 * @param role Role que joue le document dans l'evenement. <code>null</code> possible. Un finder existe. 
	 * @return L'association creee.
	 * @throws ExceptionEvenementDocumentCreation 
	 */
	static public EOEvenementDocument creerEvenementDocument(
			final EOEvenement evenement, 
			final Integer couNumero,
			final EORoleDocument role,
			EOEditingContext ec) throws ExceptionEvenementDocumentCreation {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement doit etre fourni.");
		if (couNumero == null)
			throw new IllegalArgumentException("La reference du document GED doit etre fournie.");
		
		
		EOEvenementDocument evenementDocument;
		try {
//			evenementDocument = (EvenementDocument) instanceForEntity();
			evenementDocument = EOEvenementDocument.create(ec);
		} 
		catch (Exception e) {
			throw new ExceptionEvenementDocumentCreation(
					"Impossible d'instancier la classe "+EOEvenementDocument.ENTITY_NAME, e);
		}
		
//		ec.insertObject(evenementDocument);
		
		evenementDocument.setEvenementRelationship(evenement.localInstanceIn(ec));
		evenementDocument.setCouNumero(couNumero);
		evenementDocument.setRoleRelationship(role.localInstanceIn(ec));
		
		return evenementDocument;
	}
	/**
	 * Cree de nouvelles associations de documents a un evenement.
	 * @param evenement Evenement concerne.
	 * @param couNumeros Liste de 'couNumero' (references vers des documents GED), de type {@link Number}
	 * @param role Role que joue les documents dans l'evenement. <code>null</code> possible. Un finder existe.
	 * @return Les associations creees, de type {@link EOEvenementDocument}.
	 * @throws ExceptionEvenementDocumentCreation 
	 */
	static public NSArray<EOEvenementDocument> creerEvenementDocuments(
			final EOEvenement evenement, 
			final NSArray<Integer> couNumeros,
			final EORoleDocument role,
			EOEditingContext ec) throws ExceptionEvenementDocumentCreation {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement doit etre fourni.");
		if (couNumeros==null || couNumeros.count()<1)
			throw new IllegalArgumentException("Une liste de references GED non vide doit etre fournie.");
		
		NSMutableArray<EOEvenementDocument> array = new NSMutableArray<EOEvenementDocument>();
		for (int i=0; i<couNumeros.count(); i++) {
			Integer couNumero = couNumeros.get(i);
			EOEvenementDocument ed = EOEvenementDocument.creerEvenementDocument(evenement, couNumero, role, ec);
			array.addObject(ed);
		}

		return array.immutableClone();
	}

	/**
	 * Supprime une association de document a un evenement.
	 * @param evenementDocument Association a supprimer.
	 */
	static public void supprimerEvenementDocument(
			final EOEvenementDocument evenementDocument,
			EOEditingContext ec) {

		if (evenementDocument == null)
			throw new IllegalArgumentException("L'objet a supprimer doit etre fourni.");
		
		
		EOEvenement evenement = evenementDocument.evenement();
		EORoleDocument role = evenementDocument.role();
		
		evenement.removeFromEvenementDocumentsRelationship(evenementDocument);
		if (role != null)
			role.removeFromEvenementDocumentsRelationship(evenementDocument);
		
		evenementDocument.setEvenementRelationship(null);
		evenementDocument.setRoleRelationship(null);
		
		ec.deleteObject(evenementDocument);
	}
	/**
	 * Supprime une association de document a un evenement.
	 * @param evenement Evenement concerne.
	 * @param couNumero Reference vers un document GED.
	 * @param role Role que joue le document dans l'evenement. Un finder existe. 
	 * Si <code>null</code>, role pas pris en compte dans la recherche.
	 * @return Nombre d'associations trouvees et supprimees.
	 */
	static public int supprimerEvenementDocuments(
			final EOEvenement evenement, 
			final Number couNumero,
			final EORoleDocument role,
			EOEditingContext ec) {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement doit etre fourni.");
		if (couNumero == null)
			throw new IllegalArgumentException("La reference du document GED doit etre fournie.");
		
		EOQualifier qualifier=null;
		if (role != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("role = %@", new NSArray(role));
		}
		
		NSArray evenementDocuments = 
			EOQualifier.filteredArrayWithQualifier(evenement.evenementDocuments().immutableClone(), qualifier);
		int nb = evenementDocuments.count();
		for (int i=0; i<evenementDocuments.count(); i++) {
			EOEvenementDocument evenementDocument = (EOEvenementDocument) evenementDocuments.objectAtIndex(i);
			EOEvenementDocument.supprimerEvenementDocument(evenementDocument,ec);
		}
		
		return nb;
	}
	/**
	 * Supprime toutes les associations de documents a un evenement.
	 * @param evenement Evenement concerne.
	 * @return Nombre d'associations trouvees et supprimees.
	 */
	static public int supprimerEvenementDocuments(
			final EOEvenement evenement,
			EOEditingContext ec) {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement doit etre fourni.");
		
		NSArray evenementDocuments = evenement.evenementDocuments().immutableClone();
		
		for (int i=0; i<evenementDocuments.count(); i++) {
			EOEvenementDocument evenementDocument = (EOEvenementDocument) evenementDocuments.objectAtIndex(i);
			EOEvenementDocument.supprimerEvenementDocument(evenementDocument,ec);
		}
		
		return evenementDocuments.count();
	}
	/**
	 * Supprime toutes les associations de documents a un évenement, sauf celle specifiee.
	 * @param evenement Evenement concerne.
	 * @param couNumero Reference vers le document GED a ne pas supprimer.
	 * @return Nombre d'associations trouvees et supprimees.
	 */
	static public int supprimerEvenementDocumentsSauf(
			final EOEvenement evenement,
			final Number couNumero,
			EOEditingContext ec) {

		if (evenement == null)
			throw new IllegalArgumentException("L'evenement doit etre fourni.");
		
		NSArray evenementDocuments = evenement.evenementDocuments().immutableClone();
		
		for (int i=0; i<evenementDocuments.count(); i++) {
			EOEvenementDocument evenementDocument = (EOEvenementDocument) evenementDocuments.objectAtIndex(i);
			EOEvenementDocument.supprimerEvenementDocument(evenementDocument,ec);
		}
		
		return evenementDocuments.count();
	}
    

}
