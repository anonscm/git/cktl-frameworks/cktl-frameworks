/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public class EOEvenementType extends _EOEvenementType {
	
	public static final String ID_INTERNE_TRAITER_COURRIER 		= "TRAITER_COURRIER";
	public static final String ID_INTERNE_REUNION 				= "REUNION";
	public static final String ID_INTERNE_SIGNER_PIECE 			= "SIGNER_PIECE";
	public static final String ID_INTERNE_REALISER_FACTURE 		= "REALISER_FACTURE";
	public static final String ID_INTERNE_PERCEVOIR_SOMME 		= "PERCEVOIR_SOMME";
	public static final String ID_INTERNE_VERSER_SOMME 			= "VERSER_SOMME";
	public static final String ID_INTERNE_REMETTRE_RAPPORT 		= "REMETTRE_RAPPORT";
	public static final String ID_INTERNE_ETAPE_SIGNATURE 		= "ETAPE_SIGNATURE";
	public static final String ID_INTERNE_AUTRE 				= "AUTRE";

    private static NSArray typesEvenement;
    
	public EOEvenementType() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    /**
     * @see com.webobjects.eocontrol.EOCustomObject#toString()
     */
    public String toString() {
    	return libelleLong();
    }
    /*
     * Finders
     */
    /**
	 * Recherche selon le seul critere specifie.
	 * @param evttId Valeur du critere de recherche : cle primaire
	 * @return Le(s) objet(s) metier trouve(s)
	 * @throws ExceptionFinder Probleme renvoye par le finder.
	 */
	public static EOEvenementType findWithEvttId(Number evttId, EOEditingContext ec) {
		return EOEvenementType.fetchFirstByKeyValue(ec, EOEvenementType.EVTT_ID_KEY, evttId);
	}

	/**
	 * Recherche selon le seul critere specifie.
	 * @param idInterne Valeur du critere de recherche : id interne du type
	 * @return Le(s) objet(s) metier trouve(s)
	 * @throws ExceptionFinder Probleme renvoye par le finder.
	 */
	public static EOEvenementType findWithIdInterne(String idInterne, EOEditingContext ec) {
		return EOEvenementType.fetchFirstByKeyValue(ec, EOEvenementType.ID_INTERNE_KEY, idInterne);
	}
	
	/**
	 * Fournit la liste des types d'evenement possibles.
	 * @return Liste d'objets de type reel {@link EOEvenementType}.
	 */
	@SuppressWarnings("unchecked")
    public static NSArray<EOEvenementType> getTypesEvenement() {
		if (typesEvenement == null) {
				typesEvenement = EOEvenementType.fetchAll(EOSharedEditingContext.defaultSharedEditingContext(), 
				        ERXS.ascInsensitives(ORDRE_AFFICHAGE_KEY, LIBELLE_LONG_KEY));
		}
		return typesEvenement;
	}
	
	/**
	 * Fournit le type d'evenement par defaut.
	 * @return Objet de type reel {@link EOEvenementType}.
	 */
	public static EOEvenementType getTypeEvenementDefault() {
	    return ERXArrayUtilities.firstObject(getTypesEvenement());
	}
    
}
