/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import java.util.Calendar;
import java.util.Date;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlevenement.FwkCktlEvenementPrincipal;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionTacheCreation;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionTacheModif;
import org.cocktail.fwkcktlevenement.common.exception.ExceptionTacheSuppression;
import org.cocktail.fwkcktlevenement.common.util.DateUtilities;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.FwkCktlEvenementSchedulerUtil;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.JobEvenement;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.impl.JobAlerteMail;
import org.quartz.Job;
import org.quartz.Scheduler;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXProperties;
import er.extensions.validation.ERXValidationFactory;

public class EOTache extends _EOTache {
	private static final long serialVersionUID = 6938711069929024218L;
	
	public static final String ETAT_NON_PROGRAMMEE 				= "Non programmée";
	public static final String ETAT_PROGRAMMATION_DEMANDEE 		= "Programmation demandée";
	public static final String ETAT_PROGRAMMEE 					= "Programmée";
	public static final String ETAT_REPROGRAMMATION_DEMANDEE 	= "Reprogrammation demandée";
	public static final String ETAT_DEPROGRAMMATION_DEMANDEE 	= "Déprogrammation demandée";
	public static final String ETAT_DEPROG_PUIS_SUPPR_DEMANDEE 	= "Déprogrammation puis suppression demandées";
	
	public static final String[] ETATS = new String[] {
		ETAT_NON_PROGRAMMEE, 
		ETAT_PROGRAMMATION_DEMANDEE, 
		ETAT_PROGRAMMEE, 
		ETAT_REPROGRAMMATION_DEMANDEE,
		ETAT_DEPROGRAMMATION_DEMANDEE, 
		ETAT_DEPROG_PUIS_SUPPR_DEMANDEE
	};
	
	public static final String NAME_TACHE_ALERTE_MAIL = "TACHE_ALERTE_MAIL";
	public static final String NAME_TACHE_EXEC_TACHE_APPLICATION = "TACHE_APPLICATION";
	
	public static final String DESCRIPTION_TACHE_ALERTE_MAIL = 
		"Alerte par mail des personnes concernées par l'événement";
	public static final String DESCRIPTION_TACHE_EXEC_TACHE_EXTERNE = 
		"Exécution d'une tâche externe spécifiée par un fichier .jar et un nom de classe";
	
	public static final String CLASSNAME_TACHE_ALERTE_MAIL = JobAlerteMail.class.getName();
	public static final String CLASSNAME_TACHE_EXEC_TACHE_APPLICATION = 
		"org.cocktail.evenement.quartz.serveur.job.impl.JobExecutionTacheExterne";
	
	public static final String URL_FICHIER_TACHE_EXTERNE_AUCUN	= "(Fichier .jar de l'application côté serveur)";
	public static final String URL_FICHIER_TACHE_EXTERNE_DEFAULT	= "file:///serveur/chemin/vers/le/fichier.jar";

	public static final String CLASSE_TACHE_EXTERNE_DEFAULT = "org.cocktail.classe.implementant.org.quartz.Job";
	
	public static final String SEND_MAIL_AFTER_EXECUTE_OUI      = "OUI";
	public static final String SEND_MAIL_AFTER_EXECUTE_NON      = "NON";

	public static final EOQualifier QUALIFIER_TYPE_TACHE_ALERTE_MAIL = 
		EOQualifier.qualifierWithQualifierFormat("type.idInterne = %@", new NSArray<String>(EOTypeTache.ID_INTERNE_ALERTE_MAIL));
	public static final EOQualifier QUALIFIER_TYPE_TACHE_EXEC_TACHE_APPLICATION = 
		EOQualifier.qualifierWithQualifierFormat("type.idInterne = %@", new NSArray<String>(EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION));
	
	public static String PROPERTY_TACHE_EMAIL_CLASS_NAME = "org.cocktail.fwkcktlevenement.tacheemail.classname";
	
    public EOTache() {
        super();
    }

    @Override
    public void awakeFromInsertion(EOEditingContext editingContext) {
        super.awakeFromInsertion(editingContext);
        setDateCreation(new NSTimestamp());
        this.setEtat(ETAT_NON_PROGRAMMEE);
    }
    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    @Override
    public void didInsert() {
        super.didInsert();
     // Appelée lorsque la tâche a été créée -> on la programme
        if (FwkCktlEvenementPrincipal.instance().appInstanceOwnScheduler() && 
                editingContext() != null && editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
            Scheduler scheduler = FwkCktlEvenementPrincipal.instance().getScheduler();
            try {
                FwkCktlEvenementUtil.LOG.info("In didInsert de la tache " + this + " - va programmer le job quartz correspondant");
                FwkCktlEvenementSchedulerUtil.programmerTache(this, new NSTimestamp(), null, scheduler, editingContext());
            } catch (ExceptionOperationImpossible e) {
                FwkCktlEvenementUtil.LOG.warn(e);
            }
        }
    }
    
    @Override
    public void didDelete(EOEditingContext ec) {
        super.didDelete(ec);
        // Appelée lorsque la tâche a été supprimé -> on la déprogramme
        if (FwkCktlEvenementPrincipal.instance().appInstanceOwnScheduler() &&
                ec != null && ec.parentObjectStore() instanceof EOObjectStoreCoordinator) {
            Scheduler scheduler = FwkCktlEvenementPrincipal.instance().getScheduler();
            try {
                FwkCktlEvenementUtil.LOG.info("In didDelete de la tache " + this + " - va deprogrammer le job quartz correspondant");
                FwkCktlEvenementSchedulerUtil.deprogrammerTache(this, false, scheduler, ec);
            } catch (ExceptionOperationImpossible e) {
                FwkCktlEvenementUtil.LOG.warn(e);
            }
        }
    }
    
    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        // Si la tâche à délencher se retrouve dans la tranche horaire interdite
        int heureDebut = 0;
        int heureFin = 0;
        String paramEvtTranche = "EVT_TRANCHE_HORAIRE_INTERDITE";
        try {
            NSArray<String> tranche = ERXProperties.componentsSeparatedByString(paramEvtTranche, "-");
            heureDebut = Integer.valueOf(tranche.objectAtIndex(0));
            heureFin = Integer.valueOf(tranche.objectAtIndex(1));
        } catch (Exception e) {
            log.warn("Impossible de déterminer la tranche horaire interdite, param EVT_TRANCHE_HORAIRE_INTERDITE " + 
                    ERXProperties.stringForKey(paramEvtTranche), e);
            return;
        }
        Date dateTrigger =
            DateUtilities.decaler(evenement().datePrevue(), triggerOffsetInMinutes(), Calendar.MINUTE);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateTrigger);
        cal.set(Calendar.HOUR_OF_DAY, heureDebut);
        cal.set(Calendar.MINUTE, 0);
        Date dDebutInterdit = cal.getTime();
        cal.setTime(dateTrigger);
        // Si l'heure de début est supérieure à l'heure de fin c'est que l'heure
        // de fin est le jour d'après
        if (heureDebut > heureFin) {
            cal.add(Calendar.DAY_OF_MONTH, +1);
        }
        cal.set(Calendar.HOUR_OF_DAY, heureFin);
        cal.set(Calendar.MINUTE, 0);
        Date dFinInterdit = cal.getTime();
        if (dDebutInterdit.before(dateTrigger) && dateTrigger.before(dFinInterdit)) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "BadDateTrigger");
        }

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    public boolean estDuTypeAlerteMail() {
    	return type()!=null && EOTypeTache.ID_INTERNE_ALERTE_MAIL.equals(type().idInterne());
    }
    public boolean estDuTypeExecutionTacheApplication() {
    	return type()!=null && EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION.equals(type().idInterne());
    }
    
    /**
     * 
     * @param etat
     * @return vrai si la tache est dans l'état donnée.
     */
    public boolean estDansEtat(String etat) {
    	if (etat == null)
    		throw new IllegalArgumentException("Etat null interdit.");
    	if (etat() == null) return false;
    	return etat().equals(etat); 
    }
    
    /**
     * 
     * @return
     */
    public String groupForJobDetail() {
    	return group();
    }
    /**
     * 
     * @return
     */
    public String nameForJobDetail() {
    	return /*name()+"_"+Tache.TACH_ID_KEY+"="+*/""+tachIdReadOnly();
    }
    /**
     * 
     * @return
     */
    public String nameForTrigger() {
    	return name()+"_"+EOTache.TACH_ID_KEY+"="+tachIdReadOnly();
    }
    
	/**
	 * Calcule la premiere occurence (date) de l'evenement (associe a cette tache) situee apres la date specifiee. 
	 * Ici, la difference avec le calcul standard de l'occurence suivante d'un evenement (dans la classe {@link EOEvenement})  
	 * reside dans la prise en compte de l'offset eventuel de cette tache. En effet un offset positif (= apres la date prevue
	 * de l'evt) peut faire louper une occurence si on utilise {@link EOEvenement#getOccurenceApres(Date)} avec
	 * evt.datePrevue() <= afterTime < evt.datePrevue()+offset
	 * @param afterTime Date (non comprise) a partir de laquelle sont considerees les occurences possibles.
	 * @return Prochaine date d'apparition de l'evenement associe a cette tache, ou <code>null</code> si aucune occurence trouvee.
	 */
	public Date getOccurenceEvenementApres(Date afterTime) {

		if (afterTime == null)
			throw new IllegalArgumentException("Date mini requise.");

		// Une tache peut avoir un "trigger offset" cad un decalage demande par rapport a la date prevue de son evenement associe
		int triggerOffsetInMinutes = triggerOffsetInMinutes()!=null ? triggerOffsetInMinutes().intValue() : 0;
		
		if (triggerOffsetInMinutes > 0) {
			// Si cet offset est positif (cad declenchement demande apres la date prevue de l'evt), 
			// on recule systematiquement la date de test de la meme valeur (+1 ms) pour ne pas louper une tache 
			// prevue APRES la date prevue de l'evt
			afterTime = DateUtilities.decaler(afterTime, -1*triggerOffsetInMinutes, Calendar.MINUTE);
			afterTime = DateUtilities.decaler(afterTime, -1, Calendar.MILLISECOND);
		}
		
		return evenement().getOccurenceApres(afterTime);
	}
	/**
	 * Calcule la premiere occurence (date) de la tache, situee apres la date specifiee. 
	 * @param afterTime Date (non comprise) a partir de laquelle sont considerees les occurences possibles.
	 * @return Prochaine date d'apparition de cette tache, ou <code>null</code> si aucune occurence trouvee.
	 */
	public Date getOccurenceTacheApres(Date afterTime) {
		Date occurenceAlerte=null;
		Date occurenceEvenement=afterTime;
		do {
			occurenceEvenement = getOccurenceEvenementApres(occurenceEvenement);
	    	occurenceAlerte = appliquerTriggerOffset(occurenceEvenement);
	    	if (occurenceAlerte!=null && occurenceAlerte.after(afterTime))
	    		return occurenceAlerte;
		} 
		while (occurenceEvenement != null);
		
		return occurenceAlerte;
	}

	/**
	 * Applique le decalage temporel de declenchement courant a la date specifiee.
	 * @param date Date de reference.
	 * @return La date impactee.
	 */
	public Date appliquerTriggerOffset(Date date) {
		if (date == null) return null;
		int triggerOffsetInMinutes = triggerOffsetInMinutes()!=null ? triggerOffsetInMinutes().intValue() : 0;
		return DateUtilities.decaler(date, triggerOffsetInMinutes, Calendar.MINUTE);
	}

    /**
     * 
     * @return
     */
    public String getHtmlDescriptionPourMail() {
    	
		StringBuffer buf = new StringBuffer();

		buf.append("<b>N°: </b>");
		buf.append(tachIdReadOnly());
		buf.append("<br>");
		buf.append("<b>Type: </b>");
		buf.append(type().libelleCourt());
		buf.append("<br>");
		buf.append("<b>Description: </b>");
		buf.append(description());
		buf.append("<br>");
		buf.append("Créée par: ");
		buf.append(utilisateurCreation().persLc()+" "+utilisateurCreation().persLibelle());
		buf.append("<br>");
		if (utilisateurModif() != null && !utilisateurCreation().equals(utilisateurModif())) {
			buf.append("Modifiée par: ");
			buf.append(utilisateurModif().persLc()+" "+utilisateurModif().persLibelle());
			buf.append("<br>");
		}
		buf.append("Accusé d'exécution par mail demandé: ");
		buf.append(sendMailAfterExeBoolean() ? "Oui" : "Non");
		buf.append("<br>");
		if (triggerOffsetInMinutes()!=null && triggerOffsetInMinutes().intValue()!=0) {
			buf.append("Décalage de déclenchement: ");
			buf.append(triggerOffsetInMinutesToString());
			buf.append("<br>");
		}
		
		return buf.toString();
    }
    
	/**
	 * @see com.webobjects.eocontrol.EOCustomObject#toString()
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Tache {");
		buf.append(" className=");
		buf.append(className());
		buf.append(" ; tachId=");
		buf.append(tachIdReadOnly());
		buf.append(" ; group=");
		buf.append(group());
		buf.append(" ; name=");
		buf.append(name());
		buf.append(" ; evenement=");
		buf.append(evenement());
		buf.append(" ; description=");
		buf.append(description());
		buf.append(" ; triggerOffsetInMinutes=");
		buf.append(triggerOffsetInMinutes());
		buf.append(" ; sendMailAfterExecute=");
		buf.append(sendMailAfterExe());
		buf.append(" ; etat=");
		buf.append(etat());
		buf.append(" }");
		return buf.toString();
	}

	/**
	 * Cet objet au format texte.
	 */
	public String toFullString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Tache {");
		buf.append("\n\t class=");
		buf.append(getClass());
		buf.append("\n\t ec=");
		buf.append(editingContext());
		buf.append("\n\t className=");
		buf.append(className());
		buf.append("\n\t tachId=");
		buf.append(tachIdReadOnly());
		buf.append("\n\t group=");
		buf.append(group());
		buf.append("\n\t name=");
		buf.append(name());
		buf.append("\n\t evenement=");
		buf.append(evenement());
		buf.append("\n\t description=");
		buf.append(description());
		buf.append("\n\t triggerOffsetInMinutes=");
		buf.append(triggerOffsetInMinutes());
		buf.append("\n\t sendMailAfterExecute=");
		buf.append(sendMailAfterExe());
		buf.append("\n\t etat=");
		buf.append(etat());
		buf.append("\n}");
		return buf.toString();
	}

    /**
     * 
     * @return
     */
    public String getShortHtmlDescription(boolean includeType) {
    	
		StringBuffer buf = new StringBuffer();

		if (includeType) {
			buf.append("<b>Type: </b>");
			buf.append(type().libelleCourt());
			buf.append("<br>");
		}
		buf.append("<b>Description: </b>");
		buf.append("<i>");
		buf.append(description());
		buf.append("</i>");
		buf.append("<br>");
		buf.append("<b>Accusé exéc.: </b>");
		buf.append(sendMailAfterExeBoolean() ? "Avec" : "Sans");
		buf.append("<br>");
		if (triggerOffsetInMinutes()!=null && triggerOffsetInMinutes().intValue()!=0) {
			buf.append("<b>Retard déclench.: </b>");
			buf.append(triggerOffsetInMinutesToString());
			buf.append("<br>");
		}
		
		return buf.toString().replaceAll("(<br>)*\\z", ""); // les <br> inutiles en fin sont enleves
    }


    public boolean sendMailAfterExeBoolean() {
        return sendMailAfterExe()==null || SEND_MAIL_AFTER_EXECUTE_OUI.equals(sendMailAfterExe());
    }
    public void setSendMailAfterExeBoolean(boolean aValue) {
    	setSendMailAfterExe(
    			aValue ? SEND_MAIL_AFTER_EXECUTE_OUI : SEND_MAIL_AFTER_EXECUTE_NON);
    }
    
    
    /**
	 * 
	 */
    public String triggerOffsetInMinutesToString() {

    	if (triggerOffsetInMinutes()==null || triggerOffsetInMinutes().intValue()==0)
    		return null;

    	return DateUtilities.traduireDuree(triggerOffsetInMinutes().intValue(), true);
	}

    /*
     * Finders
     */
	/**
	 * 
	 * @param ec
	 * @param gidTache
	 * @return la tache
	 */
	static public EOTache tacheFromGlobalId(EOEditingContext ec, EOGlobalID gidTache) {
		
    	if (gidTache == null)
    		throw new NullPointerException("Global ID de la tâche requis.");
    	
//    	System.out.println("["+new Date()+"] Job.jobFromGlobalId() gidTache = "+gidTache);
    	
    	EOTache tache = (EOTache) ec.objectForGlobalID(gidTache);
    	if (tache == null)
    		throw new IllegalStateException("Tâche introuvable avec ce Global ID : "+gidTache);
    	
    	return tache;
	}
	
	/**
	 * 
	 * @param tachId
	 * @return
	 */
	static public EOTache findWithTachId(Number tachId, EOEditingContext editingContext) {
		return EOTache.fetchFirstByKeyValue(editingContext, EOTache.TACH_ID_KEY, tachId);
	}

//	/**
//	 * Recherche selon le seul critere specifie.
//	 * @param idInterne Valeur du critere de recherche : id interne
//	 * @return Le(s) objet(s) metier trouve(s)
//	 */
//	static public EOTache findWithIdInterne(String idInterne, EOEditingContext editingContext) {
//		EOTache.fetchFirstByKeyValue(editingContext, EOTache.???, idInterne);
//	}
	
	/*
	 * Factories
	 */
	/**
	 * Cree une nouvelle tache et l'associe a un evenement.
	 * @param evenement Evenement concerne.
	 * @param type Type de la tache. Un finder existe.
	 * @param className Nom complet de la classe representant le {@link JobEvenement}
	 * @param group Nom du groupe qui sera repris pour creer le trigger Quartz.
	 * @param name Nom qui sera repris pour creer le trigger Quartz.
	 * @param description Description en francais de ce que fait la tache.
	 * @param data Pas utilise.
	 * @param triggerOffsetInMinutes Decalage de declenchement eventuel, en minutes. 
	 * Ex: -5 pour "5 minutes avant" la date/heure prevue de l'evenement associe.
	 * @param sendMailAfterExecute Flag indiquant si un mail sera envoye aux personnes concernees par l'evenement
	 * a l'issu de l'execution de la tache.
	 * @param tacheExterneUrlFichier URL eventuelle du fichier .jar ("file://fichier_sur_serveur_appli ou "http://...")
	 * contenant la classe permettant d'executer un processus externe.
	 * @param nomClasseTacheApplication Nom complet de la classe situee dans le classpath de l'application, representant les traitements à effectuer et implementant {@link Job}.
	 * @param etat Etat de programmation de la tache. Ex: {@link Tache#ETAT_PROGRAMMATION_DEMANDEE}.
	 * @param utilisateur Createur de la tache. Un finder existe.
	 * @return La tache creee.
	 * @throws ExceptionTacheCreation
	 */
	protected static EOTache creerTache(
			EOEvenement evenement,
			EOTypeTache type,
			String className,
			String group,
			String name,
			String description,
			NSData data,
			Integer triggerOffsetInMinutes,
			boolean sendMailAfterExecute,
//			String tacheExterneUrlFichier,
//			String nomClasseTacheApplication,
			String etat,
			EOPersonne utilisateur,
			EOEditingContext ec) {

		if (evenement == null)
			throw new NullPointerException("Evenement requis.");
		if (type == null)
			throw new NullPointerException("Type de tâche requis.");
		if (className == null)
			throw new NullPointerException("Nom complet de la classe de la tâche requis.");
		if (group == null)
			throw new NullPointerException("Groupe de la tâche requis.");
		if (name == null)
			throw new NullPointerException("Nom de la tâche requis.");
		if (etat == null)
			throw new NullPointerException("Etat de la tâche requis.");
		if (utilisateur == null)
			throw new NullPointerException("Utilisateur requis.");

		// NB: une seule alerte mail possible pour l'instant
//		if (TypeTacheConstants.ID_INTERNE_ALERTE_MAIL.equals(type.idInterne()) && evenement.tacheAlerteMail() != null) {
//			throw new ValidationException("L'événement spécifié possède déjà une tâche d'alerte par mail.");
//		}
		
		EOTache tache=null;
		tache = EOTache.create(ec);

		tache.setClassName(className);
		tache.setDescription(description);
		tache.setGroup(group);
		tache.setName(name);
		tache.setData(data);
		tache.setTriggerOffsetInMinutes(triggerOffsetInMinutes!=null ? triggerOffsetInMinutes : new Integer(0));
		tache.setSendMailAfterExeBoolean(sendMailAfterExecute);
		tache.setEtat(etat);
		
		tache.setTypeRelationship(type.localInstanceIn(ec));
		tache.setEvenementRelationship(evenement.localInstanceIn(ec));

//		tache.setTacheExterneUrlFichier(tacheExterneUrlFichier);
//		tache.setTacheExterneClasse(nomClasseTacheApplication);
		
		tache.setUtilisateurCreationRelationship(utilisateur.localInstanceIn(ec));
		tache.setDateCreation(new NSTimestamp());
		
		return tache;
	}

	/**
	 * <p>Cree et ajoute une tache vierge a un evenement.
	 * <p>NB: un evenement possede au maximum 1 tache de type "alerte par mail".
	 * @param evenement Evenement auquel on veut ajouter une tache.
	 * @param type Type de la tache. Un finder existe.
	 * @param utilisateur Createur de la tache. 
	 * @return La tache creee.
	 * @throws ExceptionTacheCreation
	 */
	static public EOTache creerTacheVierge(
			EOEvenement evenement,
			EOTypeTache type,
			EOPersonne utilisateur,
			EOEditingContext ec) throws ExceptionTacheCreation {

		if (type.estAlerteMail()) {
			return EOTache.creerTacheAlerteMail(
					evenement,
					new Integer(-5), 
					EOTache.ETAT_NON_PROGRAMMEE,
					utilisateur, 
					ec);
		}
		else if (type.estExecuterTacheApplication()) {
			return EOTache.creerTacheExecutionTacheApplication(
					evenement, 
					EOTache.DESCRIPTION_TACHE_EXEC_TACHE_EXTERNE, 
//					EOTache.URL_FICHIER_TACHE_EXTERNE_DEFAULT, 
					EOTache.CLASSE_TACHE_EXTERNE_DEFAULT, 
					false, 
					EOTache.ETAT_NON_PROGRAMMEE, 
					utilisateur,
					ec);
		}
		else {
			throw new ExceptionTacheCreation("Type de tâche non géré: "+type);
		}
	}

	/**
	 * <p>Cree une nouvelle tache d'alerte mail (aux personnes concernees par l'evenement specifie).
	 * <b>NB: une seule tache d'alerte mail autorisee par evenement pour l'instant.</b>  
	 * @param evenement Evenement auquel on veut associer la tache.
	 * @param triggerOffsetInMinutes Decalage de declenchement eventuel.
	 * @param utilisateur Createur de la tache.
	 * @return La {@link EOTache} creee.
	 * @throws ExceptionTacheCreation
	 */
	static public EOTache creerTacheAlerteMail(
			EOEvenement evenement,
			Integer triggerOffsetInMinutes,
			String etat,
			EOPersonne utilisateur,
			EOEditingContext ec) {
		
		return creerTacheAlerteMail(evenement, triggerOffsetInMinutes, etat, utilisateur, ec, getTacheEmailClassName());
		
	}
	
	/**
	 * <p>Cree une nouvelle tache d'alerte mail (aux personnes concernees par l'evenement specifie).
	 * <b>NB: une seule tache d'alerte mail autorisee par evenement pour l'instant.</b>  
	 * @param evenement Evenement auquel on veut associer la tache.
	 * @param triggerOffsetInMinutes Decalage de declenchement eventuel.
	 * @param utilisateur Createur de la tache.
	 * @return La {@link EOTache} creee.
	 * @throws ExceptionTacheCreation
	 */
	static public EOTache creerTacheAlerteMail(
			EOEvenement evenement,
			Integer triggerOffsetInMinutes,
			String etat,
			EOPersonne utilisateur,
			EOEditingContext ec,
			String tacheClassName) {
		
		if (evenement == null)
			throw new IllegalArgumentException("Evenement requis pour creer la tache d'alerte par mail.");
		
		EOTypeTache type=null;
			type = EOTypeTache.findWithIdInterne(EOTypeTache.ID_INTERNE_ALERTE_MAIL,ec);
		return creerTache(
				evenement, 
				type,
				tacheClassName,
				Scheduler.DEFAULT_GROUP, 
				EOTache.NAME_TACHE_ALERTE_MAIL, 
				EOTache.DESCRIPTION_TACHE_ALERTE_MAIL, 
				null,
				triggerOffsetInMinutes,
				false,
//				EOTache.URL_FICHIER_TACHE_EXTERNE_AUCUN,
//				EOTache.CLASSNAME_TACHE_ALERTE_MAIL,
				etat,
				utilisateur,
				ec);
	}

	/**
	 * <p>Cree une nouvelle tache d'execution de tache propre a une application.
	 * @param evenement Evenement auquel on veut associer la tache.
	 * @param description Description en francais de ce que fait la tache.
	 * @param nomClasseTacheApplication Nom complet de la classe de la tache a exécuter, <b>doit implementer l'interface {@link Job}</b> de Quartz et se trouver dans le classpath de l'application.
	 * Ex: "org.cocktail.application.job.MonJob".
	 * @param sendMailAfterExecute Indique si un "accuse" d'execution doit etre envoye par mail aux personnes concernees par l'evenement
	 * a l'issu de l'execution de la tache externe.
	 * @param etat Exemple: {@link EOTache#ETAT_NON_PROGRAMMEE}
	 * @param utilisateur Createur de la tache.
	 * @return La {@link EOTache} creee.
	 * @throws ExceptionTacheCreation
	 */
	static public EOTache creerTacheExecutionTacheApplication(
			EOEvenement evenement,
			String description,
			String nomClasseTacheApplication,
			boolean sendMailAfterExecute,
			String etat,
			EOPersonne utilisateur,
			EOEditingContext ec) throws ExceptionTacheCreation {
		
		if (evenement == null)
			throw new IllegalArgumentException("Evenement requis pour creer la tache d'alerte par mail.");
		
		EOTypeTache type=null;
		try {
			type = EOTypeTache.findWithIdInterne(EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION,ec);
		}
		catch (Exception e) {
			throw new ExceptionTacheCreation(
					"Impossible d'obtenir le type de tâche '"+EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION+"'.", e);
		}
		
		return creerTache(
				evenement, 
				type,
				nomClasseTacheApplication,
				Scheduler.DEFAULT_GROUP, 
				EOTache.NAME_TACHE_EXEC_TACHE_APPLICATION+"("+nomClasseTacheApplication+")", 
				description, 
				null,
				null,
				sendMailAfterExecute,
//				EOTache.URL_FICHIER_TACHE_EXTERNE_AUCUN,
//				nomClasseTacheApplication,
				etat,
				utilisateur,
				ec);
	}
	
	/**
	 * <p>Cree une nouvelle tache d'execution de tache externe.
	 * @return La {@link EOTache} creee.
	 * @deprecated replaced by: {@link EOTache#creerTacheExecutionTacheApplication(EOEvenement, String, String, boolean, String, EOPersonne, EOEditingContext)}
	 */
	@Deprecated
	static public EOTache creerTacheExecutionTacheExterne(
			EOEvenement evenement,
			String description,
			String tacheExterneUrlFichier,
			String tacheExterneClasse,
			boolean sendMailAfterExecute,
			String etat,
			EOPersonne utilisateur,
			EOEditingContext ec) throws ExceptionTacheCreation {
		
		if (evenement == null)
			throw new IllegalArgumentException("Evenement requis pour creer la tache d'alerte par mail.");
		
		EOTypeTache type=null;
		try {
			type = EOTypeTache.findWithIdInterne(EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION,ec);
		}
		catch (Exception e) {
			throw new ExceptionTacheCreation(
					"Impossible d'obtenir le type de tâche '"+EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION+"'.", e);
		}
		
		return creerTache(
				evenement, 
				type,
				EOTache.CLASSNAME_TACHE_EXEC_TACHE_APPLICATION,
				Scheduler.DEFAULT_GROUP, 
				EOTache.NAME_TACHE_EXEC_TACHE_APPLICATION, 
				description, 
				null,
				null,
				sendMailAfterExecute,
//				tacheExterneUrlFichier,
//				tacheExterneClasse,
				etat,
				utilisateur,
				ec);
	}

	/**
	 * <p>Suppression d'une tache.
	 * <p> La suppression proprement dite n'est pas faite ici mais sera faite cote serveur car la tache doit etre 
	 * deprogrammee si necessaire du scheduler Quartz avant. ELle est donc simplements marquee comme devant etre 
	 * deprogrammee puis supprimee.
	 * @param tache Tache a supprimer.
	 * @throws ExceptionTacheSuppression
	 * @see EOTache#ETAT_DEPROG_PUIS_SUPPR_DEMANDEE
	 */
	static public void supprimerTache(
			EOTache tache,
			EOPersonne utilisateur) throws ExceptionTacheSuppression {
		
		if (tache == null)
			throw new NullPointerException("Tâche à supprimer requise.");

		try {
			modifierTache(
					tache, 
					tache.type(), 
					tache.className(), 
					EOTache.ETAT_DEPROG_PUIS_SUPPR_DEMANDEE, 
					utilisateur);
		} 
		catch (ExceptionTacheModif e) {
			throw new ExceptionTacheSuppression("Modification de la tâche impossible.", e);
		}
	}

	/**
	 * Modification de certains attributs d'une tache existante.
	 * @param tache Tache a modifier.
	 * @param type Type de la tache. Un finder existe.
	 * @param className Nom complet de la classe representant le {@link JobEvenement}
	 * @param etat Etat de programmation de la tache. Ex: {@link EOTache#ETAT_PROGRAMMATION_DEMANDEE}.
	 * @param utilisateur Modificateur de la tache. Un finder existe.
	 * @throws ExceptionTacheModif
	 */
	static public void modifierTache(
			EOTache tache,
			EOTypeTache type,
			String className,
			String etat,
			EOPersonne utilisateur) throws ExceptionTacheModif {

		if (tache == null)
			throw new NullPointerException("Tâche à modifier requise.");
		
		modifierTache(
				tache, 
				type, 
				className, 
				tache.group(), 
				tache.name(), 
				tache.description(), 
				(Integer) tache.triggerOffsetInMinutes(), 
				new Boolean(tache.sendMailAfterExeBoolean()), 
				etat, 
				utilisateur);
		
	}
	/**
	 * Modification de certains attributs d'une tache existante.
	 * @param tache Tache a modifier.
	 * @param type Type de la tache. Un finder existe.
	 * @param className Nom complet de la classe representant le {@link JobEvenement}
	 * @param group Nom du groupe qui sera repris pour creer le trigger Quartz.
	 * @param name Nom qui sera repris pour creer le trigger Quartz.
	 * @param description Description en francais de ce que fait la tache.
	 * @param triggerOffsetInMinutes Decalage de declenchement eventuel, en minutes. 
	 * Ex: -5 pour "5 minutes avant" la date/heure prevue de l'evenement associe.
	 * @param sendMailAfterExecute Flag indiquant si un mail sera envoye aux personnes concernees par l'evenement
	 * a l'issu de l'execution de la tache.
	 * @param etat Etat de programmation de la tache. Ex: {@link EOTache#ETAT_PROGRAMMATION_DEMANDEE}.
	 * @param utilisateur Modificateur de la tache. Un finder existe.
	 * @throws ExceptionTacheModif 
	 */
	static public void modifierTache(
			EOTache tache,
			EOTypeTache type,
			String className,
			String group,
			String name,
			String description,
			Integer triggerOffsetInMinutes,
			Boolean sendMailAfterExecute,
			String etat,
			EOPersonne utilisateur) throws ExceptionTacheModif {
		
		if (tache == null)
			throw new NullPointerException("Tâche à modifier requise.");
		if (type == null)
			throw new NullPointerException("Type de la tâche requis.");
		if (className == null)
			throw new NullPointerException("Nom complet de la classe de la tâche requis.");
		if (group == null)
			throw new NullPointerException("Groupe de la tâche requis.");
		if (name == null)
			throw new NullPointerException("Nom de la tâche requis.");
		if (etat == null)
			throw new NullPointerException("Etat de la tâche requis.");
		if (utilisateur == null)
			throw new NullPointerException("Utilisateur requis.");

		// NB: une seule alerte mail possible pour l'instant
		if (EOTypeTache.ID_INTERNE_ALERTE_MAIL.equals(type.idInterne()) && !tache.estDuTypeAlerteMail()
				&& tache.evenement().tacheAlerteMail() != null) {
			throw new ExceptionTacheModif(
					"Type de tâche refusé car l'événement associé à la tâche possède déjà une tâche d'alerte par mail.");
		}
		
		
		tache.setClassName(className);
		tache.setDescription(description);
		tache.setGroup(group);
		tache.setName(name);
		tache.setTriggerOffsetInMinutes(triggerOffsetInMinutes);
		tache.setSendMailAfterExeBoolean(sendMailAfterExecute!=null ? sendMailAfterExecute.booleanValue() : false);
		tache.setEtat(etat);
		
		tache.setTypeRelationship(type);
		
		tache.setUtilisateurModifRelationship(utilisateur);
		tache.setDateModif(new NSTimestamp());
	}

	/**
	 * Modification de certains attributs d'une tache existante.
	 * @param tache Tache a modifier.
	 * @param tacheExterneUrlFichier URL eventuelle du fichier .jar ("file://fichier_sur_serveur_appli ou "http://...")
	 * contenant la classe permettant d'executer un processus externe.
	 * @param tacheExterneClasse Nom complet de la classe situee dans le .jar implementant {@link Job}.
	 * @param utilisateur Modificateur de la tache. Un finder existe.
	 * @throws ExceptionTacheModif 
	 */
	@Deprecated
	static public void modifierTache(
			EOTache tache,
			String tacheExterneUrlFichier,
			String tacheExterneClasse,
			EOPersonne utilisateur) throws ExceptionTacheModif {
		
//		tache.setTacheExterneUrlFichier(tacheExterneUrlFichier.toString());
//		tache.setTacheExterneClasse(tacheExterneClasse);
		
		tache.setUtilisateurModifRelationship(utilisateur);
		tache.setDateModif(new NSTimestamp());
	}
	

	/**
	 * Modification de certains attributs d'une tache existante de type "alerte mail".
	 * @param tache Tache a modifier.
	 * @param triggerOffsetInMinutes Decalage de declenchement eventuel, en minutes. 
	 * Ex: -5 pour "5 minutes avant" la date/heure prevue de l'evenement associe.
	 * @param etat Etat de programmation de la tache. Ex: {@link EOTache#ETAT_PROGRAMMATION_DEMANDEE}.
	 * @param utilisateur Modificateur de la tache. Un finder existe.
	 * @throws ExceptionTacheModif 
	 */
	static public void modifierTacheAlerteMail(
			EOTache tache,
			Integer triggerOffsetInMinutes,
			String etat,
			EOPersonne utilisateur) throws ExceptionTacheModif {
		
		if (tache == null)
			throw new NullPointerException("Tâche à modifier requise.");
		if (utilisateur == null)
			throw new NullPointerException("Utilisateur requis.");
		if (etat == null)
			throw new NullPointerException("Etat de la tâche requis.");
		
		if (!tache.estDuTypeAlerteMail())
			throw new IllegalArgumentException(
					"Le type de la tâche n'est pas correct. Type attendu: Alerte mail. Type spécifié: "+tache.type());
		
		tache.setTriggerOffsetInMinutes(triggerOffsetInMinutes);
		tache.setEtat(etat);
		
		tache.setUtilisateurModifRelationship(utilisateur);
		tache.setDateModif(new NSTimestamp());
	}
	
	/**
	 * Fournit la liste des etats de programmation d'une tache possibles.
	 * @return Objets de type {@link String}
	 */
	public static String[] getEtatsTache() {
		return EOTache.ETATS;
	}
	/**
	 * Fournit l'etat de programmation par defaut d'une tache.
	 * @return Objet de type {@link String}.
	 */
	public static String getEtatTacheDefault() {
		return EOTache.ETAT_NON_PROGRAMMEE;
	}
	
	public static String getTacheEmailClassName() {
		return ERXProperties.stringForKey(PROPERTY_TACHE_EMAIL_CLASS_NAME);
	}
	
}
