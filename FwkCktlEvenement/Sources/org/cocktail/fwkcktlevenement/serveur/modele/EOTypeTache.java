/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlevenement.serveur.modele;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOTypeTache extends _EOTypeTache {
	private static final long serialVersionUID = 6075654599847704002L;
	
	public static final String ID_INTERNE_ALERTE_MAIL = 		"ALERTE_MAIL";
	public static final String ID_INTERNE_EXEC_TACHE_APPLICATION =	"EXEC_TACHE_APPLICATION";
	
	public static final String VISIBILITE_PUBLIC =	"PUBLIC";
	public static final String VISIBILITE_PRIVE =	"PRIVE";

	
	public static final EOSortOrdering SORT_ASC_LIBELLE_LONG = 
		EOSortOrdering.sortOrderingWithKey("libelleLong", EOSortOrdering.CompareCaseInsensitiveAscending);
	public static final EOSortOrdering SORT_ASC_ORDRE_AFFICHAGE = 
		EOSortOrdering.sortOrderingWithKey("ordreAffichage", EOSortOrdering.CompareCaseInsensitiveAscending);
	
	public static final EOQualifier QUALIFIER_EXEC_TACHE_APPLICATION = 
		EOQualifier.qualifierWithQualifierFormat("idInterne = %@", new NSArray<String>(EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION));
	
	public static final EOQualifier QUALIFIER_TYPE_TACHE_PUBLIC = ERXQ.is(VISIBILITE_KEY, VISIBILITE_PUBLIC);
	
	private static NSArray<EOTypeTache> typesTache;
	
    public EOTypeTache() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    /**
	 * 
	 * @return
	 */
	public boolean estAlerteMail() {
		return EOTypeTache.ID_INTERNE_ALERTE_MAIL.equals(this.idInterne());
	}
	public boolean estExecuterTacheApplication() {
		return EOTypeTache.ID_INTERNE_EXEC_TACHE_APPLICATION.equals(this.idInterne());
	}
	
	@Deprecated
	public boolean estExecuterTacheExterne() {
		return estExecuterTacheApplication();
	}

	 /**
	  * Cet objet au format texte.
	  */
	 public String toString() {
		 return libelleCourt();
	 }
	 
	 /*
	  * Finders
	  */
	 /**
	 * Recherche selon le seul critere specifie.
	 * @param tytaId Valeur du critere de recherche : cle primaire
	 * @return Le(s) objet(s) metier trouve(s)
	 * @throws ExceptionFinder Probleme renvoye par le finder.
	 */
	public static EOTypeTache findWithTytaId(Number tytaId, EOEditingContext editingContext) {
		return EOTypeTache.fetchFirstByKeyValue(editingContext, EOTypeTache.TYTA_ID_KEY, tytaId);
	}

	/**
	 * Recherche selon le seul critere specifie.
	 * @param idInterne Valeur du critere de recherche : id interne du type
	 * @return Le(s) objet(s) metier trouve(s)
	 * @throws ExceptionFinder Probleme renvoye par le finder.
	 */
	public static EOTypeTache findWithIdInterne(String idInterne, EOEditingContext editingContext) {
		return EOTypeTache.fetchFirstByKeyValue(editingContext, EOTypeTache.ID_INTERNE_KEY, idInterne);
	}

	/**
     * Fournit la liste des types de taches publics.
     * @return liste des types de taches publics {@link EOTypeTache}.
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EOTypeTache> getTypesTache() {
        if (typesTache == null) {
            typesTache = EOTypeTache.fetchAll(
                        EOSharedEditingContext.defaultSharedEditingContext(), 
                        QUALIFIER_TYPE_TACHE_PUBLIC,
                        ERXS.asc(ORDRE_AFFICHAGE_KEY).array());
        }
        return typesTache;
    }
	
}
