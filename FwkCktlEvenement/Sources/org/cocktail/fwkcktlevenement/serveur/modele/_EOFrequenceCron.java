/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFrequenceCron.java instead.
package org.cocktail.fwkcktlevenement.serveur.modele;

import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

@SuppressWarnings("all")
public abstract class _EOFrequenceCron extends  CktlServerRecord {
	public static final String ENTITY_NAME = "FwkEvt_FrequenceCron";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EVT_FREQUENCE_CRON";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "frqcId";

	public static final String CRON_EXPRESSION_KEY = "cronExpression";
	public static final String CRON_EXPRESSION_DAYSOFMONTH_KEY = "cronExpressionDaysofmonth";
	public static final String CRON_EXPRESSION_DAYSOFWEEK_KEY = "cronExpressionDaysofweek";
	public static final String CRON_EXPRESSION_HOURS_KEY = "cronExpressionHours";
	public static final String CRON_EXPRESSION_LASTDAYOFMONTH_KEY = "cronExpressionLastdayofmonth";
	public static final String CRON_EXPRESSION_LASTDAYOFWEEK_KEY = "cronExpressionLastdayofweek";
	public static final String CRON_EXPRESSION_MINUTES_KEY = "cronExpressionMinutes";
	public static final String CRON_EXPRESSION_MONTHS_KEY = "cronExpressionMonths";
	public static final String CRON_EXPRESSION_NEARESTWEEKDAY_KEY = "cronExpressionNearestweekday";
	public static final String CRON_EXPRESSION_NTHDAYOFWEEK_KEY = "cronExpressionNthdayofweek";
	public static final String CRON_EXPRESSION_SECONDS_KEY = "cronExpressionSeconds";
	public static final String CRON_EXPRESSION_YEARS_KEY = "cronExpressionYears";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String TRADUCTION_KEY = "traduction";

	// Non visible attributes
	public static final String REP_ID_KEY = "repId";
	public static final String FRQC_ID_KEY = "frqcId";
	public static final String EVT_ID_KEY = "evtId";

	// Colkeys
	public static final String CRON_EXPRESSION_COLKEY = "FRQC_CRON_EXPRESSION";
	public static final String CRON_EXPRESSION_DAYSOFMONTH_COLKEY = "FRQC_DAYSOFMONTH";
	public static final String CRON_EXPRESSION_DAYSOFWEEK_COLKEY = "FRQC_DAYSOFWEEK";
	public static final String CRON_EXPRESSION_HOURS_COLKEY = "FRQC_HOURS";
	public static final String CRON_EXPRESSION_LASTDAYOFMONTH_COLKEY = "FRQC_LASTDAYOFMONTH";
	public static final String CRON_EXPRESSION_LASTDAYOFWEEK_COLKEY = "FRQC_LASTDAYOFWEEK";
	public static final String CRON_EXPRESSION_MINUTES_COLKEY = "FRQC_MINUTES";
	public static final String CRON_EXPRESSION_MONTHS_COLKEY = "FRQC_MONTHS";
	public static final String CRON_EXPRESSION_NEARESTWEEKDAY_COLKEY = "FRQC_NEARESTWEEKDAY";
	public static final String CRON_EXPRESSION_NTHDAYOFWEEK_COLKEY = "FRQC_NTHDAYOFWEEK";
	public static final String CRON_EXPRESSION_SECONDS_COLKEY = "FRQC_SECONDS";
	public static final String CRON_EXPRESSION_YEARS_COLKEY = "FRQC_YEARS";
	public static final String DATE_FIN_COLKEY = "FRQC_DATE_FIN";
	public static final String TRADUCTION_COLKEY = "FRQC_TRADUCTION";

	// Non visible colkeys
	public static final String REP_ID_COLKEY = "REP_ID";
	public static final String FRQC_ID_COLKEY = "FRQC_ID";
	public static final String EVT_ID_COLKEY = "EVT_ID";

	// Relationships
	public static final String EVENEMENT_KEY = "evenement";
	public static final String REPETITION_KEY = "repetition";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOFrequenceCron with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cronExpression
	 * @param cronExpressionDaysofmonth
	 * @param cronExpressionDaysofweek
	 * @param cronExpressionHours
	 * @param cronExpressionLastdayofmonth
	 * @param cronExpressionLastdayofweek
	 * @param cronExpressionMinutes
	 * @param cronExpressionMonths
	 * @param cronExpressionNearestweekday
	 * @param cronExpressionNthdayofweek
	 * @param cronExpressionSeconds
	 * @param cronExpressionYears
	 * @param traduction
	 * @param evenement
	 * @param repetition
	 * @return EOFrequenceCron
	 */
	public static EOFrequenceCron create(EOEditingContext editingContext, String cronExpression, String cronExpressionDaysofmonth, String cronExpressionDaysofweek, String cronExpressionHours, String cronExpressionLastdayofmonth, String cronExpressionLastdayofweek, String cronExpressionMinutes, String cronExpressionMonths, String cronExpressionNearestweekday, String cronExpressionNthdayofweek, String cronExpressionSeconds, String cronExpressionYears, String traduction, org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement, org.cocktail.fwkcktlevenement.serveur.modele.EORepetition repetition) {
		EOFrequenceCron eo = (EOFrequenceCron) createAndInsertInstance(editingContext);
		eo.setCronExpression(cronExpression);
		eo.setCronExpressionDaysofmonth(cronExpressionDaysofmonth);
		eo.setCronExpressionDaysofweek(cronExpressionDaysofweek);
		eo.setCronExpressionHours(cronExpressionHours);
		eo.setCronExpressionLastdayofmonth(cronExpressionLastdayofmonth);
		eo.setCronExpressionLastdayofweek(cronExpressionLastdayofweek);
		eo.setCronExpressionMinutes(cronExpressionMinutes);
		eo.setCronExpressionMonths(cronExpressionMonths);
		eo.setCronExpressionNearestweekday(cronExpressionNearestweekday);
		eo.setCronExpressionNthdayofweek(cronExpressionNthdayofweek);
		eo.setCronExpressionSeconds(cronExpressionSeconds);
		eo.setCronExpressionYears(cronExpressionYears);
		eo.setTraduction(traduction);
		eo.setEvenementRelationship(evenement);
		eo.setRepetitionRelationship(repetition);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOFrequenceCron.
	 *
	 * @param editingContext
	 * @return EOFrequenceCron
	 */
	public static EOFrequenceCron create(EOEditingContext editingContext) {
		EOFrequenceCron eo = (EOFrequenceCron) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOFrequenceCron localInstanceIn(EOEditingContext editingContext) {
		EOFrequenceCron localInstance = (EOFrequenceCron) localInstanceOfObject(editingContext, (EOFrequenceCron) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOFrequenceCron localInstanceIn(EOEditingContext editingContext, EOFrequenceCron eo) {
		EOFrequenceCron localInstance = (eo == null) ? null : (EOFrequenceCron) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cronExpression() {
		return (String) storedValueForKey("cronExpression");
	}

	public void setCronExpression(String value) {
		takeStoredValueForKey(value, "cronExpression");
	}
	public String cronExpressionDaysofmonth() {
		return (String) storedValueForKey("cronExpressionDaysofmonth");
	}

	public void setCronExpressionDaysofmonth(String value) {
		takeStoredValueForKey(value, "cronExpressionDaysofmonth");
	}
	public String cronExpressionDaysofweek() {
		return (String) storedValueForKey("cronExpressionDaysofweek");
	}

	public void setCronExpressionDaysofweek(String value) {
		takeStoredValueForKey(value, "cronExpressionDaysofweek");
	}
	public String cronExpressionHours() {
		return (String) storedValueForKey("cronExpressionHours");
	}

	public void setCronExpressionHours(String value) {
		takeStoredValueForKey(value, "cronExpressionHours");
	}
	public String cronExpressionLastdayofmonth() {
		return (String) storedValueForKey("cronExpressionLastdayofmonth");
	}

	public void setCronExpressionLastdayofmonth(String value) {
		takeStoredValueForKey(value, "cronExpressionLastdayofmonth");
	}
	public String cronExpressionLastdayofweek() {
		return (String) storedValueForKey("cronExpressionLastdayofweek");
	}

	public void setCronExpressionLastdayofweek(String value) {
		takeStoredValueForKey(value, "cronExpressionLastdayofweek");
	}
	public String cronExpressionMinutes() {
		return (String) storedValueForKey("cronExpressionMinutes");
	}

	public void setCronExpressionMinutes(String value) {
		takeStoredValueForKey(value, "cronExpressionMinutes");
	}
	public String cronExpressionMonths() {
		return (String) storedValueForKey("cronExpressionMonths");
	}

	public void setCronExpressionMonths(String value) {
		takeStoredValueForKey(value, "cronExpressionMonths");
	}
	public String cronExpressionNearestweekday() {
		return (String) storedValueForKey("cronExpressionNearestweekday");
	}

	public void setCronExpressionNearestweekday(String value) {
		takeStoredValueForKey(value, "cronExpressionNearestweekday");
	}
	public String cronExpressionNthdayofweek() {
		return (String) storedValueForKey("cronExpressionNthdayofweek");
	}

	public void setCronExpressionNthdayofweek(String value) {
		takeStoredValueForKey(value, "cronExpressionNthdayofweek");
	}
	public String cronExpressionSeconds() {
		return (String) storedValueForKey("cronExpressionSeconds");
	}

	public void setCronExpressionSeconds(String value) {
		takeStoredValueForKey(value, "cronExpressionSeconds");
	}
	public String cronExpressionYears() {
		return (String) storedValueForKey("cronExpressionYears");
	}

	public void setCronExpressionYears(String value) {
		takeStoredValueForKey(value, "cronExpressionYears");
	}
	public NSTimestamp dateFin() {
		return (NSTimestamp) storedValueForKey("dateFin");
	}

	public void setDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, "dateFin");
	}
	public String traduction() {
		return (String) storedValueForKey("traduction");
	}

	public void setTraduction(String value) {
		takeStoredValueForKey(value, "traduction");
	}

	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement)storedValueForKey("evenement");
	}

	public void setEvenementRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement oldValue = evenement();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "evenement");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "evenement");
		}
	}
  
	public org.cocktail.fwkcktlevenement.serveur.modele.EORepetition repetition() {
		return (org.cocktail.fwkcktlevenement.serveur.modele.EORepetition)storedValueForKey("repetition");
	}

	public void setRepetitionRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EORepetition value) {
		if (value == null) {
			org.cocktail.fwkcktlevenement.serveur.modele.EORepetition oldValue = repetition();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "repetition");
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, "repetition");
		}
	}
  

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOFrequenceCron.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOFrequenceCron.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOFrequenceCron)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOFrequenceCron fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFrequenceCron fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOFrequenceCron eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOFrequenceCron)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOFrequenceCron fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOFrequenceCron fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOFrequenceCron fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOFrequenceCron eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOFrequenceCron)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOFrequenceCron fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOFrequenceCron fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOFrequenceCron eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOFrequenceCron ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOFrequenceCron createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOFrequenceCron.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOFrequenceCron.ENTITY_NAME + "' !");
		}
		else {
			EOFrequenceCron object = (EOFrequenceCron) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOFrequenceCron localInstanceOfObject(EOEditingContext ec, EOFrequenceCron object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOFrequenceCron " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOFrequenceCron) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
