-- Ensemble des requêtes à effectuer sur Grhum pour modifier les tables du fwk Evenement déjà installée en dev 
-- avant la version diffusée dans le patch Grhum

alter table grhum.evt_tache drop column TACH_EXT_URL_FICHIER;

alter table grhum.evt_tache drop column TACH_EXT_CLASSE;

alter table grhum.evt_tache drop column TACH_EXT_NOM_FICHIER_RESULT;

INSERT INTO GRHUM.EVT_TYPE_TACHE (TYTA_ID, TYTA_ID_INTERNE, TYTA_ORDRE_AFFICHAGE, TYTA_VISIBILITE, TYTA_LC, TYTA_LL)
VALUES (GRHUM.EVT_TYPE_TACHE_SEQ.nextval, 'EXEC_TACHE_APPLICATION', 1000, 'PRIVE', 
'Tache d''application', 
'Tâche d''execution de traitements propres à une application. Le nom de la classe à executer est enregistré dans la tache.');


















