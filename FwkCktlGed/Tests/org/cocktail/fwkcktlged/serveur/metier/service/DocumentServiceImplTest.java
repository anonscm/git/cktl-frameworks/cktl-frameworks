package org.cocktail.fwkcktlged.serveur.metier.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.cocktail.fwkcktlged.serveur.metier.Configuration;
import org.cocktail.fwkcktlged.serveur.metier.Document;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsAttachment;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.eocontrol.EOEditingContext;
import com.wounit.rules.MockEditingContext;

import er.attachment.model.ERAttachment;
import er.attachment.model.ERFileAttachment;
import er.attachment.processors.ERAttachmentProcessor;
import er.extensions.foundation.ERXProperties;

public class DocumentServiceImplTest {

    private DocumentServiceImpl documentService;
    @Rule
    public MockEditingContext mockEditingContext = new MockEditingContext("FwkCktlGed");
    
    @Before
    public void setUp() {
        ERXProperties.setStringForKey("file", "er.attachment.conf.storageType");
        ERXProperties.setStringForKey("100", "er.attachment.conf.width");
        ERXProperties.setStringForKey("100", "er.attachment.conf.height");
        ERXProperties.setStringForKey("/tmp", "er.attachment.conf.file.filesystemPath");
        documentService = new DocumentServiceImpl(mockEditingContext, Configuration.configFromProperties("conf"));
        ERAttachmentProcessor<ERAttachment> attachmentProcessor = new ERAttachmentProcessor<ERAttachment>() {

            @Override
            public ERAttachment _process(EOEditingContext editingContext, File uploadedFile, String recommendedFileName, String mimeType, String configurationName, String ownerID, boolean pendingDelete) throws IOException {
                ERAttachment attachment = mockEditingContext.createSavedObject(ERFileAttachment.class);
                attachment.setWebPath(recommendedFileName);
                return attachment;
            }

            @Override
            public InputStream attachmentInputStream(ERAttachment attachment) throws IOException {
                return null;
            }

            @Override
            public String attachmentUrl(ERAttachment attachment, WORequest request, WOContext context) {
                return "http://test";
            }

            @Override
            public void deleteAttachment(ERAttachment attachment) throws IOException {
                
            }
            
        };
        documentService.setAttachmentProcessor(attachmentProcessor);


    }
    
    @Test
    public void testCreerDocument() {
        byte[] data = new byte[1];
        String fileName = "test.pdf";
        Document doc = documentService.creerFileDocument("typedoc", fileName, data, 99);
        assertNotNull(doc);
        assertTrue(doc instanceof EODocAsAttachment);
        assertTrue(((EODocAsAttachment)doc).toErAttachment().fileName().endsWith(".pdf"));
    }
    
    @Test
    public void testUrlDocument() {
        WOContext testContext = Mockito.mock(WOContext.class);
        byte[] data = new byte[1];
        String fileName = "test.pdf";
        Document doc = documentService.creerFileDocument("typedoc", fileName, data, 99);
        String url = documentService.urlDocument((EODocAsAttachment) doc, testContext);
        assertEquals("http://test", url);
    }

}
