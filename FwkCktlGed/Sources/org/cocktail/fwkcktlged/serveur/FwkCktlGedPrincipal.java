/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlged.serveur;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSharedEditingContext;

import er.attachment.ERAttachmentRequestHandler;
import er.attachment.ERAttachmentRequestHandler.Delegate;
import er.attachment.model.ERFileAttachment;
import er.extensions.ERXFrameworkPrincipal;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

public class FwkCktlGedPrincipal extends ERXFrameworkPrincipal{

    public static final Logger LOG = Logger.getLogger(FwkCktlGedPrincipal.class);
    
    static {
        setUpFrameworkPrincipalClass(FwkCktlGedPrincipal.class);
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public void didFinishInitialization() {
        super.didFinishInitialization();
        loadModelToFixBug();
        String delegateClass = ERXProperties.stringForKey("ATTACHMENT_REQ_HANDLER_DELEGATE");
        ERAttachmentRequestHandler reqHandler = 
            ((ERAttachmentRequestHandler) WOApplication.application().requestHandlerForKey(ERAttachmentRequestHandler.REQUEST_HANDLER_KEY));
        Delegate delegate = null;
        if (delegateClass == null) 
            delegate = new BasicRequestHandlerDelegate();
        else {
            try {
                Class clazz = Class.forName(delegateClass);
                delegate = (Delegate)clazz.newInstance();
            } catch (ClassNotFoundException e) {
                LOG.warn("La classe " + delegateClass + " n'a pas pu etre chargee", e);
                delegate = new BasicRequestHandlerDelegate();
            } catch (InstantiationException e) {
                LOG.warn("La classe " + delegateClass + " n'a pas pu etre chargee", e);
                delegate = new BasicRequestHandlerDelegate();
            } catch (IllegalAccessException e) {
                LOG.warn("La classe " + delegateClass + " n'a pas pu etre chargee", e);
                delegate = new BasicRequestHandlerDelegate();
            }
        }
        reqHandler.setDelegate(delegate);
    }
    
    /**
     * Chargement du modèle ERAttachment pour éviter le bug suivant :
     * http://lists.apple.com/archives/webobjects-dev/2011/Apr/msg00004.html
     * qui a lieu lorsqu'on traverse toErAttachment qui pointe vers une entité abstraite d'un autre modèle.
     */
    private void loadModelToFixBug() {
        EOEditingContext ec = EOSharedEditingContext.defaultSharedEditingContext();
        try {
            ec.lock();
            EOModel modelAttachment = EOUtilities.entityForClass(ec, ERFileAttachment.class).model();
            EOModel modelGed = EOUtilities.entityForClass(ec, EODocument.class).model();
            for (EOModel model : Arrays.asList(modelAttachment, modelGed)) {
                EODatabaseContext.registeredDatabaseContextForModel(model, ec);
            }
        } catch (Exception e) {
            LOG.error("Impossible de charger le modèle ERAttachment !", e);
        } finally {
            ec.unlock();
        }
    }
    
    @Override
    public void finishInitialization() {
        // TODO Auto-generated method stub
        
    }

}
