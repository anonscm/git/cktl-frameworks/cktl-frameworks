/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlged.serveur.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

import er.attachment.model.ERAttachment;

public class EODocAsAttachment extends _EODocAsAttachment {
	private static final String MSG_ERATTACHMENT_OBLIGATOIRE = "La référence à un objet ERAttachment est obligatoire";

	public EODocAsAttachment() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();

		try {
			if (toErAttachment() == null) {
				throw new NSValidation.ValidationException(MSG_ERATTACHMENT_OBLIGATOIRE);
			}

		} catch (Exception e) {
			throw new NSValidation.ValidationException(dressExceptionMsg(e));
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		//si erreur lors de l'ajout d'un document ajouter la propriété er.extensions.ERXEnterpriseObject.applyRestrictingQualifierOnInsert=false à votre application
		super.awakeFromInsertion(ec);
		EODelegueStockage delegueStockage = EODelegueStockage.fetchByKeyValue(ec, EODelegueStockage.DS_STR_ID_KEY, EODelegueStockage.DSSTR_ID_ERATTACHMENT);
		// setDsId(Integer.valueOf(delegueStockage.primaryKey()));
		setToDelegueStockageRelationship(delegueStockage);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	@Override
	public Boolean isFile() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isUrl() {
		return Boolean.FALSE;
	}

	/**
	 * @param edc Obligatoire
	 * @param persId facultatif lors de la création
	 * @param typeDocument facultatif lors de la création
	 * @param attachment facultatif lors de la création
	 * @return Le nouvel objet créé et inséré dans l'editingContext
	 */
	public static EODocAsAttachment creer(EOEditingContext edc, Integer persId, EOTypeDocument typeDocument, ERAttachment attachment) {
		EODocAsAttachment obj = EODocAsAttachment.creerInstance(edc);
		obj.setPersId(persId);
		obj.setToTypeDocumentRelationship(typeDocument);
		obj.setToErAttachmentRelationship(attachment);
		return obj;
	}

	/**
	 * Supprime l'objet et ses objets liés
	 */
	public void delete() {
		if (toErAttachment() != null) {
			ERAttachment erAttachment = toErAttachment();
			setToErAttachmentRelationship(null);
			editingContext().deleteObject(erAttachment);
			editingContext().deleteObject(this);
		}
	}

}
