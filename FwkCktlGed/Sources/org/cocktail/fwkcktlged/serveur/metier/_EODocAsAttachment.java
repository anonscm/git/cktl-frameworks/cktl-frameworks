/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODocAsAttachment.java instead.
package org.cocktail.fwkcktlged.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODocAsAttachment extends org.cocktail.fwkcktlged.serveur.metier.EODocument {
	public static final String ENTITY_NAME = "FwkCktlGed_DocAsAttachment";
	public static final String ENTITY_TABLE_NAME = "cktl_ged.DOCUMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "docId";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String MOTS_CLEFS_KEY = "motsClefs";
	public static final String OBJET_KEY = "objet";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles
	public static final String DOC_ID_KEY = "docId";
	public static final String DS_ID_KEY = "dsId";
	public static final String DOC_EXTERNAL_ID_KEY = "docExternalId";
	public static final String TD_ID_KEY = "tdId";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String MOTS_CLEFS_COLKEY = "MOTS_CLEFS";
	public static final String OBJET_COLKEY = "objet";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String DOC_ID_COLKEY = "DOC_ID";
	public static final String DS_ID_COLKEY = "DS_ID";
	public static final String DOC_EXTERNAL_ID_COLKEY = "DOC_EXTERNAL_ID";
	public static final String TD_ID_COLKEY = "TD_ID";


	// Relationships
	public static final String TO_DELEGUE_STOCKAGE_KEY = "toDelegueStockage";
	public static final String TO_ER_ATTACHMENT_KEY = "toErAttachment";
	public static final String TO_TYPE_DOCUMENT_KEY = "toTypeDocument";



	// Accessors methods
  public er.attachment.model.ERAttachment toErAttachment() {
    return (er.attachment.model.ERAttachment)storedValueForKey("toErAttachment");
  }

  public void setToErAttachmentRelationship(er.attachment.model.ERAttachment value) {
    if (value == null) {
    	er.attachment.model.ERAttachment oldValue = toErAttachment();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toErAttachment");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toErAttachment");
    }
  }
  

  public static EODocAsAttachment createFwkCktlGed_DocAsAttachment(EOEditingContext editingContext, NSTimestamp dCreation
, String objet
, Integer persId
, org.cocktail.fwkcktlged.serveur.metier.EODelegueStockage toDelegueStockage, er.attachment.model.ERAttachment toErAttachment, org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument toTypeDocument) {
    EODocAsAttachment eo = (EODocAsAttachment) createAndInsertInstance(editingContext, _EODocAsAttachment.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setObjet(objet);
		eo.setPersId(persId);
    eo.setToDelegueStockageRelationship(toDelegueStockage);
    eo.setToErAttachmentRelationship(toErAttachment);
    eo.setToTypeDocumentRelationship(toTypeDocument);
    return eo;
  }

  
	  public EODocAsAttachment localInstanceIn(EOEditingContext editingContext) {
	  		return (EODocAsAttachment)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODocAsAttachment creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param eoeditingcontext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODocAsAttachment creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODocAsAttachment object = (EODocAsAttachment)createAndInsertInstance(editingContext, _EODocAsAttachment.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODocAsAttachment localInstanceIn(EOEditingContext editingContext, EODocAsAttachment eo) {
    EODocAsAttachment localInstance = (eo == null) ? null : (EODocAsAttachment)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EODocAsAttachment#localInstanceIn a la place.
   */
	public static EODocAsAttachment localInstanceOf(EOEditingContext editingContext, EODocAsAttachment eo) {
		return EODocAsAttachment.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throw IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EODocAsAttachment fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODocAsAttachment fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODocAsAttachment eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODocAsAttachment)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODocAsAttachment fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODocAsAttachment fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODocAsAttachment eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODocAsAttachment)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EODocAsAttachment fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODocAsAttachment eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODocAsAttachment ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODocAsAttachment fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
