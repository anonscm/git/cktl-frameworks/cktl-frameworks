package org.cocktail.fwkcktlged.serveur.metier;

public interface Document {

    void setCommentaire(String value);
    void setMotsClefs(String value);
    void setObjet(String value);
    void setPersId(Integer value);
    
}
