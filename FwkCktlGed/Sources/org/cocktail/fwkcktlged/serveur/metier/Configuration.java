package org.cocktail.fwkcktlged.serveur.metier;

import er.extensions.foundation.ERXProperties;

/**
 * Représente une configuration de type erattachment : type de stockage, nom de la config, 
 * largeur, hauteur dans le cas d'une image.
 * 
 * @author Alexis Tual
 *
 */
public class Configuration {

    private String name;
    private String storageType;
    private int width;
    private int height;
    
    /**
     * @param storageType le type de stockage
     * @param name le nom de la config
     * @param width la largeur dans le cas d'une image
     * @param height la hauteur dans le cas d'une image
     */
    public Configuration(String storageType, String name, int width, int height) {
        this.name = name;
        this.storageType = storageType;
        this.width = width;
        this.height = height;
    }

    public String getStorageType() {
        return storageType;
    }
    
    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }
    
    public String getName() {
        return name;
    }
    
    /**
     * @param configName le nom de la config
     * @return une configuration à partir des propriétés renseignées
     */
    public static Configuration configFromProperties(String configName) {
        String storageType = ERXProperties.stringForKey(paramForConfig(configName, "storageType"));
        int width = ERXProperties.intForKeyWithDefault(paramForConfig(configName, "width"), -1);
        int height = ERXProperties.intForKeyWithDefault(paramForConfig(configName, "height"), -1);
        return new Configuration(storageType, configName, width, height);
    }
    
    private static String paramForConfig(String configuration, String param) {
        return "er.attachment." + configuration + "." + param;
    }
    
}
