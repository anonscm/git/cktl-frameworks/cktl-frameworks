/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlged.serveur.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EODocAsUrl extends _EODocAsUrl {
	private static final String MSG_STOCKAGEURL_OBLIGATOIRE = "Une référence à un objet de type EOStockageUrl est obligatoire";

	public EODocAsUrl() {
		super();
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		EODelegueStockage delegueStockage = EODelegueStockage.fetchByKeyValue(ec, EODelegueStockage.DS_STR_ID_KEY, EODelegueStockage.DSSTR_ID_STOCKAGE_URL);
		// setDsId(Integer.valueOf(delegueStockage.primaryKey()));
		setToDelegueStockageRelationship(delegueStockage);
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		try {
			if (toStockageUrl() == null) {
				throw new NSValidation.ValidationException(MSG_STOCKAGEURL_OBLIGATOIRE);
			}

		} catch (Exception e) {
			throw new NSValidation.ValidationException(dressExceptionMsg(e));
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	@Override
	public Boolean isFile() {
		return Boolean.FALSE;
	}

	@Override
	public Boolean isUrl() {
		return Boolean.TRUE;
	}

	/**
	 * @param edc Obligatoire
	 * @param persId facultatif lors de la création
	 * @param typeDocument facultatif lors de la création
	 * @param stockageUrl facultatif lors de la création
	 * @return Le nouvel objet créé et inséré dans l'editingContext
	 */
	public static EODocAsUrl creer(EOEditingContext edc, Integer persId, EOTypeDocument typeDocument, EOStockageUrl stockageUrl) {
		EODocAsUrl obj = EODocAsUrl.creerInstance(edc);
		obj.setPersId(persId);
		obj.setToTypeDocumentRelationship(typeDocument);
		obj.setToStockageUrlRelationship(stockageUrl);
		return obj;
	}

	/**
	 * Supprime l'objet et ses objets liés
	 */
	public void delete() {
		if (toStockageUrl() != null) {
			EOStockageUrl stockageUrl = toStockageUrl();
			setToStockageUrlRelationship(null);
			editingContext().deleteObject(stockageUrl);
			editingContext().deleteObject(this);
		}
	}
}
