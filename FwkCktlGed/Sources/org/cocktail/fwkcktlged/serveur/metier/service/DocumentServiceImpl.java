package org.cocktail.fwkcktlged.serveur.metier.service;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlged.serveur.metier.Configuration;
import org.cocktail.fwkcktlged.serveur.metier.Document;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsAttachment;
import org.cocktail.fwkcktlged.serveur.metier.EODocAsUrl;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;

import com.google.common.io.Files;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.attachment.model.ERAttachment;
import er.attachment.processors.ERAttachmentProcessor;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXTimestampUtilities;

/**
 * Implémentation par défaut de {@link DocumentService}.
 * Une instance de {@link DocumentServiceImpl} par ec et configuration erattachment 
 * c'est à dire, ne pas utiliser une même instance pour de {@link DocumentServiceImpl} 
 * pour créer des documents de type différent (exemple : contrat et factures...).
 * 
 * @see DocumentService
 * 
 * @author Alexis Tual
 *
 */
public class DocumentServiceImpl implements DocumentService {

    private static final Logger LOG = Logger.getLogger(DocumentServiceImpl.class);

    private ERAttachmentProcessor<ERAttachment> attachmentProcessor;
    private EOEditingContext editingContext;
    private Configuration configuration;
    
    /**
     * @param editingContext un editing context
     * @param configuration une configuration
     */
    public DocumentServiceImpl(EOEditingContext editingContext, Configuration configuration) {
        this.editingContext = editingContext;
        this.configuration = configuration;
    }
    
    /**
     * {@inheritDoc}
     */
    public Document creerFileDocument(String typeDocumentStr, String fileName, byte[] data, Integer persId) {
        EODocument cktlDocument = null;
        ERAttachment attachment;
        try {
            File tmpFile = createTmpFile(fileName, data);
            attachment = getAttachmentProcessor().process(editingContext, tmpFile, null, null, 
                    configuration.getWidth(), 
                    configuration.getHeight(), 
                    configuration.getName(), persId.toString());
        }
        catch (IOException e) {
            LOG.error(e);
            throw new RuntimeException("Impossible de créer le document dans la nouvelle GED");
        }
        EOTypeDocument typeDocument = EOTypeDocument.fetchFirstByQualifier(
                editingContext, ERXQ.equals(EOTypeDocument.TD_STR_ID_KEY, typeDocumentStr));
        cktlDocument = EODocAsAttachment.creer(editingContext, persId, typeDocument, attachment);
        cktlDocument.setDCreation(ERXTimestampUtilities.today());
        return cktlDocument;
    }

    /**
     * {@inheritDoc}
     */
    public String urlDocument(Document document, WOContext context) {
        if (document instanceof EODocAsAttachment) {
            return getAttachmentProcessor().attachmentUrl(((EODocAsAttachment) document).toErAttachment(), context.request(), context);
        } else if (document instanceof EODocAsUrl) {
            return ((EODocAsUrl) document).toStockageUrl().surlUrl();
        } else {
            throw new RuntimeException("Type de document inconnu : " + document);
        }
    }

    private ERAttachmentProcessor<ERAttachment> getAttachmentProcessor() {
        if (attachmentProcessor == null) {
            setAttachmentProcessor(ERAttachmentProcessor.processorForType(configuration.getStorageType()));
        }
        return attachmentProcessor;
    }
    
    public void setAttachmentProcessor(ERAttachmentProcessor<ERAttachment> attachmentProcessor) {
        this.attachmentProcessor = attachmentProcessor;
    }
    
    private File createTmpFile(String fileName, byte[] data) throws IOException {
        File tmpDir = Files.createTempDir();
        File tmpFile = new File(tmpDir, fileName);
        FileUtils.writeByteArrayToFile(tmpFile, data);
        return tmpFile;
    }

}
