package org.cocktail.fwkcktlged.serveur.metier.service;

import org.cocktail.fwkcktlged.serveur.metier.Document;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;

import com.webobjects.appserver.WOContext;

/**
 * Services pour créer des documents.
 * Permet d'éviter d'utiliser les composants graphiques et de masquer la complexité
 * de l'implémentation de la ged.
 * 
 * @author Alexis Tual
 *
 */
public interface DocumentService {

    /**
     * @param typeDocumentStr le type de document, doit correspondre à CKTL_GED.TYPE_DOCUMENT.TD_STR_ID
     * @param fileName le nom du fichier à enregistrer
     * @param data les données binaires
     * @param persId le créateur du fichier
     * @return une instance d'EODocument
     */
    Document creerFileDocument(String typeDocumentStr, String fileName, byte[] data, Integer persId);

    /**
     * @param document un {@link EODocument}
     * @param context un {@link WOContext}
     * @return l'url générée correspondant à un document
     */
    String urlDocument(Document document, WOContext context);
    
}
