--
-- Patch DDL de CKTL_GED du 20/08/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : CKTL_GED
-- Numero de version : 1.0.0.1
-- Date de publication : 01/09/2011
-- Auteur(s) : Emmanuel GEZE
-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

Insert into CKTL_GED.TYPE_DOCUMENT (TD_ID,TD_STR_ID,TD_LIBELLE) values (CKTL_GED.TYPE_DOCUMENT_seq.nextval,'org.cocktail.ged.typedocument.gfc.acte.autre','Autre type');
Insert into CKTL_GED.TYPE_DOCUMENT (TD_ID,TD_STR_ID,TD_LIBELLE) values (CKTL_GED.TYPE_DOCUMENT_seq.nextval,'org.cocktail.ged.typedocument.gfc.acte.contrat','Contrat');
Insert into CKTL_GED.TYPE_DOCUMENT (TD_ID,TD_STR_ID,TD_LIBELLE) values (CKTL_GED.TYPE_DOCUMENT_seq.nextval,'org.cocktail.ged.typedocument.gfc.acte.avenant','Avenant');
Insert into CKTL_GED.TYPE_DOCUMENT (TD_ID,TD_STR_ID,TD_LIBELLE) values (CKTL_GED.TYPE_DOCUMENT_seq.nextval,'org.cocktail.ged.typedocument.gfc.acte.devis','Devis');
Insert into CKTL_GED.TYPE_DOCUMENT (TD_ID,TD_STR_ID,TD_LIBELLE) values (CKTL_GED.TYPE_DOCUMENT_seq.nextval,'org.cocktail.ged.typedocument.gfc.acte.facture','Facture');

COMMIT;

