--
-- Patch DDL de CKTL_GED du 20/08/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : CKTL_GED
-- Numero de version : 1.0.0.2
-- Date de publication : 09/11/2011
-- Auteur(s) : Alexis Tual
-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- La table doit être vidée avant
-- DELETE FROM CKTL_GED.DOCUMENT;
ALTER TABLE CKTL_GED.DOCUMENT ADD OBJET VARCHAR2(255) NOT NULL;
ALTER TABLE CKTL_GED.DOCUMENT ADD COMMENTAIRE VARCHAR2(800);
ALTER TABLE CKTL_GED.DOCUMENT ADD MOTS_CLEFS VARCHAR2(255);
ALTER TABLE CKTL_GED.DOCUMENT ADD D_CREATION DATE NOT NULL;


INSERT INTO CKTL_GED.DB_VERSION VALUES(CKTL_GED.DB_VERSION_SEQ.NEXTVAL, '1.0.0.2',to_date('09/11/2011','DD/MM/YYYY'),NULL,'Ajout de metadata');


COMMIT;

