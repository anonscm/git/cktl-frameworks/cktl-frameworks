
SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--


whenever sqlerror exit sql.sqlcode ;


CREATE USER CKTL_GED IDENTIFIED BY cktl_ged
DEFAULT TABLESPACE DATA_GRHUM
TEMPORARY TABLESPACE TEMP
QUOTA UNLIMITED ON DATA_GRHUM
QUOTA UNLIMITED ON INDX_GRHUM
;

GRANT CREATE SESSION TO CKTL_GED;
GRANT CREATE TABLE TO CKTL_GED;
GRANT CREATE SYNONYM TO CKTL_GED;
GRANT CREATE VIEW TO CKTL_GED;
GRANT CREATE PROCEDURE TO CKTL_GED;
GRANT CREATE SEQUENCE TO CKTL_GED;
GRANT CREATE TRIGGER TO CKTL_GED;
GRANT UNLIMITED TABLESPACE TO CKTL_GED;

grant select, references on grhum.personne to CKTL_GED;


PROMPT Le mot de passe du user est cktl_ged
PROMPT il est vivement recommandé de le changer
PROMPT Alter user CKTL_GED identified by "unvraimotdepasse";
