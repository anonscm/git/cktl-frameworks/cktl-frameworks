/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;
 
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Panel;
import java.util.Iterator;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;



/**
 * Affiche une fenêtre de légendes de couleur.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CouleurLegendeDialog extends JFrame {
	JComponent myContentPane;


	/**
	 * @param arg0 Titre de la fenêtre.
	 * @param colorListeBg Dictionaire contenant en clé la couleur et en valeur le libellé à afficher à coté de la couleur, pour les couleurs de fond.
	 * @param colorListeFg  Dictionaire contenant en clé la couleur et en valeur le libellé à afficher à coté de la couleur, pour les couleurs de texte.
	 * @throws HeadlessException
	 */
	public CouleurLegendeDialog(String arg0, Map colorListeBg, Map colorListeFg) throws HeadlessException {
		super(arg0);
		
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int)this.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int)this.getGraphicsConfiguration().getBounds().getHeight();

		myContentPane = createUI(colorListeBg, colorListeFg);
		myContentPane.setOpaque(true);
		this.setContentPane(myContentPane);
		this.setResizable(true);
		this.setLocation((x/2)-((int)this.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)this.getContentPane().getMinimumSize().getHeight()/2)));
		this.pack();		
	}
	

	/**
	 * Créer et remplir le panneau d'affichage. 
	 * 
	 * @param colorListe Dictionaire contenant en clé la couleur et en valeur le libellé à afficher à coté de la couleur.
	 * @return
	 */
	private JPanel createUI(Map colorListeBg, Map colorListeFg) {
		String  label;
		Color lacle;
		
		
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		panel.add(Box.createRigidArea(new Dimension(200,2))); 
		//panel.setBorder(BorderFactory.createEmptyBorder(20,20,10,20));
		
		
		
		
		//Couleurs de fond
		
		if (( colorListeBg!=null )  && (!colorListeBg.keySet().isEmpty())  ) {
			Box boxBg = Box.createVerticalBox();
			boxBg.setBorder(BorderFactory.createTitledBorder("Couleurs de fond")  );
			for (Iterator e = colorListeBg.keySet().iterator()  ; e.hasNext() ;) {
				lacle = (Color)e.next();
				label = (String) colorListeBg.get(lacle);
				//Ajout de la ligne 
				boxBg.add(createLineBg(lacle, label));
				//Ajout d'un séparateur vertical entre deux lignes			
				boxBg.add(Box.createRigidArea(new Dimension(150,5)) ); 
			}			
			panel.add(boxBg);
		}
		panel.add(Box.createRigidArea(new Dimension(150,5)) );
		
		//Couleurs de texte
		if (( colorListeFg!=null )  && (!colorListeFg.keySet().isEmpty())  ) {
			Box boxFg = Box.createVerticalBox();
			boxFg.setBorder(BorderFactory.createTitledBorder("Couleurs de texte")  );			
			for (Iterator e = colorListeFg.keySet().iterator()  ; e.hasNext() ;) {
				lacle = (Color)e.next();
				label = (String) colorListeFg.get(lacle);
				//Ajout de la ligne 
				boxFg.add(createLineFg(lacle, label));
				//Ajout d'un séparateur vertical entre deux lignes			
				boxFg.add(Box.createRigidArea(new Dimension(150,5)) ); 
			}	
			panel.add(boxFg);	
		}
				
		
		//Ajout d'une zone étirable en bas de panel
		panel.add(Box.createVerticalGlue());

		//Add a vertical spacer that also guarantees us a minimum width:
		//panel.add(Box.createRigidArea(new Dimension(150,10)));

		return panel;
	}
	
	
	/**
	 * 
	 * Crée une ligne avec un bloc de couleur plus une légende. 
	 * 
	 * @param color
	 * @param label
	 * @return
	 */
	private Box createLineBg(Color color,String label) {
		Panel tmpPanel;
		JLabel tmpLabel;
		Box tmpBox;
		//Box contenant la couleur			
		Box tmpBox2 = Box.createVerticalBox();	
		//Créer la zone avec couleur
		tmpPanel = new Panel();
		tmpPanel.setBackground( color );

		//On fixe la taille de la box
		tmpBox2.setMaximumSize(new Dimension(40,20));
		//Ajout d'une box pour avoir une largeur minimale
		tmpBox2.add(Box.createRigidArea(new Dimension(40,1)));
			
		//tmpBox2.setBorder(BorderFactory.createLineBorder(Color.decode("#000000"),2));
			
		//On ajoute la couleur dans la box			
		tmpBox2.add(tmpPanel);

		//creation d'un libelle
		tmpLabel =  new JLabel(label);
		tmpLabel.setHorizontalAlignment(SwingConstants.LEFT);

		//Box representant une ligne
		tmpBox = Box.createHorizontalBox();	
		//Marge gauche fixe	+ hauteur
		tmpBox.add(Box.createRigidArea(new Dimension(5,20)));
		//On ajoute la zone couleur dans la ligne
		tmpBox.add(tmpBox2);
//		On ajoute un separateur vertical entre couleur et label dans la box
		tmpBox.add(Box.createRigidArea(new Dimension(5,20))); //separateur
		//Ajouter le libellé dans la box
		tmpBox.add(tmpLabel);
		//Ajout d'une zone elastique à droite						
		tmpBox.add(Box.createHorizontalGlue() );
		
		return tmpBox;		
	}
	


	/**
	 * 
	 * Crée une ligne avec un texte en couleur plus une légende. 
	 * 
	 * @param color
	 * @param label
	 * @return
	 */
	private Box createLineFg(Color color,String label) {
		JLabel tmpLabel, tmpLabel2;
		Box tmpBox;
		//Box contenant la couleur			
		Box tmpBox2 = Box.createVerticalBox();	

		//Créer le texte avec couleur
		tmpLabel2 = new JLabel("texte", SwingConstants.LEFT);
		tmpLabel2.setForeground(color);
		//tmpLabel2.setBorder(BorderFactory.createLineBorder(Color.decode("#000000"),2));
		
		//On fixe la taille de la box
		tmpBox2.setMaximumSize(new Dimension(40,20));
		//Ajout d'une box pour avoir une largeur minimale
		tmpBox2.add(Box.createRigidArea(new Dimension(40,1)));
			
		//tmpBox2.setBorder(BorderFactory.createLineBorder(Color.decode("#000000"),2));
			
		Box tmpBox3 = Box.createHorizontalBox();	
		//On ajoute le texte dans la box			
		tmpBox3.add(tmpLabel2);
		tmpBox3.add(Box.createHorizontalGlue());
		
		tmpBox2.add(tmpBox3);
		

		//creation d'un libelle
		tmpLabel =  new JLabel(label);
		tmpLabel.setHorizontalAlignment(SwingConstants.LEFT);

		//Box representant une ligne
		tmpBox = Box.createHorizontalBox();	
		//Marge gauche fixe	+ hauteur
		tmpBox.add(Box.createRigidArea(new Dimension(5,20)));
		//On ajoute la zone couleur dans la ligne
		tmpBox.add(tmpBox2);
//		On ajoute un separateur vertical entre couleur et label dans la box
		tmpBox.add(Box.createRigidArea(new Dimension(5,20))); //separateur
		//Ajouter le libellé dans la box
		tmpBox.add(tmpLabel);
		//Ajout d'une zone elastique à droite						
		tmpBox.add(Box.createHorizontalGlue() );
		
		return tmpBox;		
	}
	

/*

	public static void main(String[] args) {
		try {
			Hashtable colorsBg = new Hashtable();
			colorsBg.put(Color.decode( "#FF0000" ), new String("Fond rouge"));
			colorsBg.put(Color.decode( "#00FF00" ), new String("Fond vert "));
			colorsBg.put(Color.decode( "#0000FF" ), new String("Fond bleu"));			
			
			Hashtable colorsFg = new Hashtable();
			colorsFg.put(Color.decode( "#FF0000" ), new String("Texte rouge "));
			colorsFg.put(Color.decode( "#00FF00" ), new String("Texte vert "));
			colorsFg.put(Color.decode( "#0000FF" ), new String("Texte bleu"));			

			CouleurLegendeDialog tmpCouleurLegendeDialog = new CouleurLegendeDialog("Test de légendes",colorsBg, colorsFg);
			tmpCouleurLegendeDialog.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/	

}
