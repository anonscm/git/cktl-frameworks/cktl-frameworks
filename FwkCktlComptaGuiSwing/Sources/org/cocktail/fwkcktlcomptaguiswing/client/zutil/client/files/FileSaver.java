package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files;

import java.awt.Component;
import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;

import org.cocktail.fwkcktlcompta.common.exception.DefaultClientException;

public class FileSaver {

	/**
	 * Ouvre une boite de dialogue d'enregistrements pour une liste de fichiers.
	 * 
	 * @param owner
	 * @param defaultDirectory
	 * @param files
	 * @throws Exception
	 * @return Le répertoire dans lequel on été enregistrés les fichiers
	 */
	public static final File saveFiles(Component owner, String defaultDirectory, List<File> files) throws Exception {
			File dir = new File(defaultDirectory);
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setDialogTitle("Sélectionnez le répertoire de destination ...");
			fileChooser.setCurrentDirectory(dir);
			if (fileChooser.showSaveDialog(owner) == JFileChooser.APPROVE_OPTION) {
				dir = fileChooser.getCurrentDirectory();
				if (dir.exists()) {
					//On copie les fichiers
					File dest;
					for (File f : files) {
						dest = new File(dir, f.getName());
						org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZFileUtil.fileCopy(f, dest);
					}
				}
				else {
					throw new DefaultClientException("Le répertoire " + dir.getAbsolutePath() + " n'existe pas.");
				}
			}
		return dir;
	}
}
