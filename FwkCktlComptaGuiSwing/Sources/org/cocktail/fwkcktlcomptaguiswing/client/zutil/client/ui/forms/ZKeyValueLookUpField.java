/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZKeyValueLookUpField extends ZTextField {
    private final Map _keyValues;
    private JLabel libelle;


    /**
     * @param keyValues Map avec en clés ce qui est suceptible d'être saisi par l'utilisateur et en valeur les libellés associés. Toutes les clés et valeurs doivent être des String.
     */
    public ZKeyValueLookUpField(final Map keyValues) {
        super();
        _keyValues = keyValues;
        buildLibelleValue();
        this.add(libelle);
        addDocumentListener(new PcoNumFieldListener());
    }

    /**
     * @param provider
     */
    public ZKeyValueLookUpField(IZTextFieldModel provider, final Map keyValues) {
        super(provider);
        _keyValues = keyValues;
        buildLibelleValue();
        this.add(libelle);
        addDocumentListener(new PcoNumFieldListener());
    }

    private void buildLibelleValue() {
        libelle = new JLabel();
    }

    public final void updateLibelle() {
        if (  getMyTexfield().getText() !=null &&  _keyValues.get( getMyTexfield().getText()   ) != null ) {
            libelle.setText(  (String) _keyValues.get( getMyTexfield().getText()   )  );
        }
        else {
            libelle.setText(  null );
        }
    }

    /**
     * @see org.cocktail.zutil.client.ui.forms.ZTextField#updateData()
     */
    public void updateData() {
        super.updateData();
        updateLibelle();
    }


    private final class PcoNumFieldListener implements DocumentListener {

        /**
         * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
         */
        public void changedUpdate(final DocumentEvent e) {
            updateLibelle();
        }

        /**
         * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
         */
        public void insertUpdate(final DocumentEvent e) {
            updateLibelle();
        }

        /**
         * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
         */
        public void removeUpdate(final DocumentEvent e) {
            updateLibelle();
        }

    }



}
