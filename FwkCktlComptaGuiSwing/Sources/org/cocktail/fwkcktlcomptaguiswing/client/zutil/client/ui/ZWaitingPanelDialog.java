/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.Dialog;
import java.awt.Frame;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZWaitingPanelDialog extends JDialog {

	private ZWaitingPanel myZWaitingPanel;

	public ZWaitingPanel getWaitingPanel() {
		return myZWaitingPanel;
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener) {
		myZWaitingPanel = new ZWaitingPanel(listener);
		setContentPane(myZWaitingPanel);
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, ImageIcon img) {
		myZWaitingPanel = new ZWaitingPanel(listener, img);
		setContentPane(myZWaitingPanel);
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Dialog dial) {
		super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener);
		setContentPane(myZWaitingPanel);
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Dialog dial, ImageIcon img) {
		super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener, img);
		setContentPane(myZWaitingPanel);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int) getGraphicsConfiguration().getBounds().getWidth();
		int y = (int) getGraphicsConfiguration().getBounds().getHeight();

		setResizable(false);
		setLocation((x / 2) - ((int) getContentPane().getMinimumSize().getWidth() / 2), ((y / 2) - ((int) getContentPane().getMinimumSize().getHeight() / 2)));
		pack();
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Frame dial) {
		super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener);
		setContentPane(myZWaitingPanel);
	}

	public ZWaitingPanelDialog(ZWaitingPanel.ZWaitingPanelListener listener, Frame dial, ImageIcon img) {
		super(dial);
		myZWaitingPanel = new ZWaitingPanel(listener, img);
		setContentPane(myZWaitingPanel);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int) getGraphicsConfiguration().getBounds().getWidth();
		int y = (int) getGraphicsConfiguration().getBounds().getHeight();

		setResizable(false);
		setLocation((x / 2) - ((int) getContentPane().getMinimumSize().getWidth() / 2), ((y / 2) - ((int) getContentPane().getMinimumSize().getHeight() / 2)));
		pack();
	}

	public static ZWaitingPanelDialog getWindow(ZWaitingPanel.ZWaitingPanelListener listener, ImageIcon img) {

		ZWaitingPanelDialog win = new ZWaitingPanelDialog(listener, img);
		win.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int) win.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int) win.getGraphicsConfiguration().getBounds().getHeight();

		win.setResizable(false);
		win.setLocation((x / 2) - ((int) win.getContentPane().getMinimumSize().getWidth() / 2), ((y / 2) - ((int) win.getContentPane().getMinimumSize().getHeight() / 2)));
		win.pack();
		return win;
	}

	public static ZWaitingPanelDialog getWindow(ZWaitingPanel.ZWaitingPanelListener listener) {
		ZWaitingPanelDialog win = new ZWaitingPanelDialog(listener);
		win.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int) win.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int) win.getGraphicsConfiguration().getBounds().getHeight();

		win.setResizable(false);
		win.setLocation((x / 2) - ((int) win.getContentPane().getMinimumSize().getWidth() / 2), ((y / 2) - ((int) win.getContentPane().getMinimumSize().getHeight() / 2)));
		win.pack();
		return win;
	}

	public void setTopText(String s) {
		getWaitingPanel().setGlobalTaskDescription(s);
	}

	public void setBottomText(String s) {
		getWaitingPanel().setCurrentTaskDescription(s);
	}

	public final JProgressBar getMyProgressBar() {
		return getWaitingPanel().getMyProgressBar();
	}

	public void setImg(ImageIcon img) {
		myZWaitingPanel.setIcon(img);
	}

}
