/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Cette classe fournit les methodes pour la gestion des flots
 * de donnees. Elle ne contient que des methodes statiques et
 * aucune instance de cette classe ne doit etre creee. 
 */
public class StreamCtrl {
  /**
   * La taille du tampont utilise pour le transfert de document.
   */
  public static final int BUFFER_SIZE = 1024;

  /**
   * <i>Il n'est pas necessaire de creer un objet de la classe
   * <code>StreamCtrl</code>, car toutes ses methodes sont statiques !</i>
   */
  public StreamCtrl() {
    // Ce constructeur est laisse juste pour pouvoir
    // ajouter un commentaire JavaDoc.
  }

  /**
   * Lit une chaine de caractere a partir d'un flux. Le le flux commence
   * par la taille de la chaine (un entier) suivi des caracteres de la
   * chaine.
   * 
   * <p>Cette methode ne peut etre utilisee qu'en parallele avec la methode
   * <code>writeStringToStream</code>. Une chaine ne peut etre lue avec
   * cette methode seulement si elle a ete ecrite avec
   * <code>writeStringToStream</code>.</p>
   * 
   * @param dataIn Le flux a partir duquel la chaine est lue. 
   * @exception IOException
   * 
   * @see #writeStringToStream(String, DataOutputStream) 
   */
  public static String readStringFromStream(DataInputStream dataIn)
    throws IOException
  {
    byte[] bytes;
    int i = dataIn.readInt();
    if (i > 0) {
      bytes = new byte[i];
      dataIn.read(bytes);
      return new String(bytes);
    }
    return "";
  }

  /**
   * Ecrit une chaine de caracteres dans un flux. On ecrit
   * la longeur de la chaine (un entier) suivi des ses caracteres.
   *   
   * <p>Cette methode ne peut etre utilisee qu'en parallele avec la methode
   * <code>readStringFromStream</code>. Si une chaine est ecrite avec
   * cette methode alors elle doit etre lue avec
   * <code>readStringFromStream</code>.</p>
   * 
   * @param aString La chaine a enregistrer dans un flux. 
   * @param dataOut Le flux.
   * @exception IOException
   * 
   * @see #readStringFromStream(DataInputStream) 
   */
  public static void writeStringToStream(String aString, DataOutputStream dataOut)
    throws IOException
  {
    if ((aString == null) || (aString.length() == 0)) {
      dataOut.writeInt(0);
    } else {
      byte[] bytes = aString.getBytes();
      dataOut.writeInt(bytes.length);
      dataOut.write(bytes, 0, bytes.length);
    }
  }

  /**
   * Enregistre les donnees envoyees via le flux <code>content</code>
   * dans le fichier avec le chemin <code>filePath</code>. L'enregistrement
   * est termine lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee.
   * 
   * @throws IOException
   */
  public static void saveContentToFile(InputStream content,
                                       String filePath)
    throws IOException
  {
    saveContentToFile(content, filePath, -1);
  }

  /**
   * Enregistre les donnees envoyees via le flux <code>content</code>
   * dans le fichier avec le chemin <code>filePath</code>. L'enregistrement
   * est termine lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee ou lorsque la nombre total des octets
   * envoyes via le flux atteint <code>contentSize</code>.
   * 
   * @throws IOException
   */
  public static void saveContentToFile(InputStream content,
                                       String filePath,
                                       long contentSize)
    throws IOException
  {
    FileOutputStream out = null;
    if (filePath != null) out = new FileOutputStream(filePath);
    writeContentToStream(content, out, contentSize);
    if (out != null) {
      out.flush();
      forceClose(out);
    }
  }
  
  /**
   * Lit les donnees a partir de flux <code>content</code> et les
   * ecrit dans le flux <code>out</code>. L'ecriture est termine
   * lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee ou lorsque la nombre total des octets
   * envoyes via le flux <code>out</code> atteint <code>contentSize</code>.
   * 
   * @throws IOException
   */
  public static void writeContentToStream(InputStream content,
                                          OutputStream out,
                                          long contentSize)
    throws IOException
  {
    long bytesTotal;
    int bytesRead;
    byte bBuffer[] = new byte[BUFFER_SIZE];
    bytesTotal = 0;
    do {
      bytesRead = content.read(bBuffer, 0, BUFFER_SIZE);
      if (bytesRead > 0) {
        if (out != null) out.write(bBuffer, 0, bytesRead);
        bytesTotal += bytesRead;
      }
      // Si on connait la taille des donnees, on peut s'arreter explicitement
      if ((contentSize >= 0) && (bytesTotal >= contentSize))
        break;
    } while(bytesRead > 0);
    if (out != null) out.flush();
  }

  
  /**
   * Lit les donnees a partir de flux <code>content</code> et les
   * ecrit dans le flux <code>out</code>. L'ecriture est termine
   * lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee.
   * 
   * @throws IOException
   */
  public static void writeContentToStream(InputStream content,
                                          OutputStream out)
    throws IOException
  {
    writeContentToStream(content, out, -1);
  }

  /**
   * Ferme le flux d'entree <code>stream</code> et ignore les erreurs
   * si elles se produissent.
   */
  public static void forceClose(InputStream stream) {
    if (stream != null)
      try { stream.close(); } catch(Throwable e) { }
  }

  /**
   * Ferme le flux de sortie <code>stream</code> et ignore les erreurs
   * si elles se produissent.
   */
  public static void forceClose(OutputStream stream) {
    if (stream != null)
      try { stream.close(); } catch(Throwable e) { }
  }

  /**
   * Ferme le socket de commnuication <code>socket</code> et ignore
   * les erreurs si elles se produissent.
   */
  public static void forceClose(Socket socket) {
    if (socket != null)
      try { socket.close(); } catch(Throwable e) { }
  }
  

  
  
  
  
}
