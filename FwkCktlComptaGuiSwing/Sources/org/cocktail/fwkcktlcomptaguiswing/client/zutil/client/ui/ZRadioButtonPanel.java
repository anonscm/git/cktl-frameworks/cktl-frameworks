/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZRadioButtonPanel extends JPanel implements IZDataComponent {

	private Map buttonList;
	private String borderTitle;
	private Object initialSelectedObject;
	private ButtonGroup group; 
	private int orientation;
	
	
	

	/**
	 * Crï¿½e un panel contenant plusieurs radios buttons, sous forme de colonne.
	 * @param objectList Map contenant en clï¿½ l'objet associï¿½ au bouton et en valeur le label du bouton.
	 * @param title Titre de la bordure entourant le panel. Si le titre est null, il n'y a pas de bordure dessinï¿½e.
	 * @param pselectedObject L'objet prï¿½selectionnï¿½.
	 */
	public ZRadioButtonPanel(Map objectList, String title,Object pselectedObject) {
		super();
		orientation = SwingConstants.VERTICAL;
		group = new ButtonGroup();
		borderTitle = title;
		buttonList = objectList;
		initialSelectedObject = pselectedObject;
		buildContent();
	}
	
	/**
	 * Crï¿½e un panel contenant plusieurs radios buttons.
	 * @param objectList Map contenant en clï¿½ l'objet associï¿½ au bouton et en valeur le label du bouton.
	 * @param title Titre de la bordure entourant le panel. Si le titre est null, il n'y a pas de bordure dessinï¿½e.
	 * @param pselectedObject L'objet prï¿½selectionnï¿½.
	 * @param orientation (SwingConstants.VERTICAL ou SwingConstants.HORIZONTAL) 
	 */
	public ZRadioButtonPanel(Map objectList, String title,Object pselectedObject, int orientation) {
		super();
		this.orientation = orientation;
		group = new ButtonGroup();
		borderTitle = title;
		buttonList = objectList;
		initialSelectedObject = pselectedObject;
		buildContent();
	}	
	
	
	private void buildContent() {
		Box boxBg = Box.createVerticalBox();
		Component vide;
		
		if (orientation == SwingConstants.VERTICAL) {
		    boxBg = Box.createVerticalBox();
		    vide = Box.createRigidArea(new Dimension(1,4));
		}
		else {
		    boxBg = Box.createHorizontalBox();
		    vide = Box.createRigidArea(new Dimension(4,1));		    
		}
		
		String label;
		Object lacle;
		if (borderTitle!=null) {
			boxBg.setBorder(BorderFactory.createTitledBorder(borderTitle));	
		}
		for (Iterator e = buttonList.keySet().iterator()  ; e.hasNext() ;) {
			lacle = (Object)e.next();
			label = (String) buttonList.get(lacle);
			//Ajout de la ligne 
			boxBg.add(buildSingleContent(lacle, label, (lacle.equals(initialSelectedObject))));
			//Ajout d'un sï¿½parateur			
			boxBg.add(vide ); 
		}			
		this.add(boxBg);
	}
	

	private Box buildSingleContent(Object lacle, String label, boolean selected) {
		Box tmpBox;
		//System.out.println(new Boolean(selected));
		ZRadioButton tmpZRadioButton = new ZRadioButton(lacle, label);
		//tmpZRadioButton.setAlignmentX(  SwingConstants.LEFT );
		//tmpZRadioButton.setModel(  );
		//tmpZRadioButton.getModel();
		group.add(tmpZRadioButton);  
		//Box representant une ligne
		tmpBox = Box.createHorizontalBox();
		tmpBox.setAlignmentX(  SwingConstants.LEFT );			
		//Ajouter le libellï¿½ dans la box
		tmpBox.add(tmpZRadioButton);
		//Ajout d'une zone elastique ï¿½ droite						
		tmpBox.add(Box.createHorizontalGlue() );	
		if (selected) {
			tmpZRadioButton.getModel().setSelected(true);
		}
		return tmpBox;	
	}



	/**
	 * @return
	 */
	public Object getSelectedObject() {
//		ButtonModel tmpButtonModel = group.getSelection();		
		return ((ZRadioButton.ZRadioButtonModel)group.getSelection()).getAssociatedObject();
		
		/*
		Object lacle;
		for (Iterator e = buttonList.keySet().iterator()  ; e.hasNext() ;) {
			lacle = (Object)e.next();
			for (Enumeration iter =  group.getElements() ; iter.hasMoreElements() ;) {
				if (lacle ==  ((ZRadioButton) iter.nextElement()).getAssociatedObject()) {
					return lacle;
				}
			}
		}		
		return null;
		
		*/
	}

	public void setselectedObject(Object sel) {
	    Enumeration i = group.getElements();
	    while (i.hasMoreElements()) {
            ZRadioButton.ZRadioButtonModel element  = (ZRadioButton.ZRadioButtonModel) ((AbstractButton)i.nextElement()).getModel();
            if (element.getAssociatedObject().equals(sel)) {
                element.setSelected(true);
            }
        }
	}


    /**
     * @return Returns the group.
     */
    public ButtonGroup getGroup() {
        return group;
    }
    
    
    public void updateData() {
	    Enumeration i = group.getElements();
	    while (i.hasMoreElements()) {
            ZRadioButton element  = (ZRadioButton) i.nextElement();
            element.updateData();
        }        
    }
    
    public ZRadioButton getButtonByName(final String name) {
	    Enumeration i = group.getElements();
	    while (i.hasMoreElements()) {
            ZRadioButton element  = (ZRadioButton) i.nextElement();
            if (name.equals(element.getName())) {
                return element;
            }
        }          
	    return null;
    }
    
    public ZRadioButton getButtonByObject(final Object obj) {
	    Enumeration i = group.getElements();
	    while (i.hasMoreElements()) {
            ZRadioButton element  = (ZRadioButton) i.nextElement();
            if (obj.equals(    ((ZRadioButton.ZRadioButtonModel)element.getModel()).getAssociatedObject()   )) {
                return element;
            }
        }          
	    return null;
    }    
    
}
