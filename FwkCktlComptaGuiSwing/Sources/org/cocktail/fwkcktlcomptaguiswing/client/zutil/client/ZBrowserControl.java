/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZBrowserControl {
    // Used to identify the windows platform.
    private static final String WIN_ID = "Windows";
    // The default system browser under windows.
    private static final String WIN_PATH = "rundll32";
    // The flag to display a url.
    private static final String WIN_FLAG = "url.dll,FileProtocolHandler";
    // The default browser under unix.
    private static final String UNIX_PATH = "netscape";
    // The flag to display a url.
    private static final String UNIX_FLAG = "-remote openURL";

    /**
     * Display a file in the system browser. If you want to display a file, you
     * must include the absolute path name.
     * 
     * @param url
     *            the file's url (the url must start with either "http://" or
     *            "file://").
     * @throws Exception
     */
    public static void displayURL(String url) throws Exception {
        boolean windows = isWindowsPlatform();
        String cmd = null;
        if (windows) {
            // cmd = 'rundll32 url.dll,FileProtocolHandler http://...'
            cmd = WIN_PATH + " " + WIN_FLAG + " " + url;
             Runtime.getRuntime().exec(cmd);
        } else {
            // Under Unix, Netscape has to be running for the "-remote"
            // command to work. So, we try sending the command and
            // check for an exit value. If the exit command is 0,
            // it worked, otherwise we need to start the browser.
            // cmd = 'netscape -remote openURL(http://www.javaworld.com)'
            cmd = UNIX_PATH + " " + UNIX_FLAG + "(" + url + ")";
            Process p = Runtime.getRuntime().exec(cmd);
            
                // wait for exit code -- if it's 0, command worked,
                // otherwise we need to start the browser up.
                int exitCode = p.waitFor();
                if (exitCode != 0) {
                    // Command failed, start up the browser
                    // cmd = 'netscape http://www.javaworld.com'
                    cmd = UNIX_PATH + " " + url;
                    p = Runtime.getRuntime().exec(cmd);
                }
        }
    }

    /**
     * Try to determine whether this application is running under Windows or
     * some other platform by examing the "os.name" property.
     * 
     * @return true if this application is running under a Windows OS
     */
    public static boolean isWindowsPlatform() {
        String os = System.getProperty("os.name");
        if (os != null && os.startsWith(WIN_ID))
            return true;
        
        return false;
    }

}
