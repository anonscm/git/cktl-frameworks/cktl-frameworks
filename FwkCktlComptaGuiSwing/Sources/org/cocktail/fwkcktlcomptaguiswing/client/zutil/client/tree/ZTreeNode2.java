/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.tree;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import com.webobjects.eocontrol.EOQualifier;

public abstract class ZTreeNode2 implements TreeNode {
    protected ZTreeNode2 _parentNode;
    protected final LinkedList childs = new LinkedList();
    private EOQualifier _qualifier;
    private boolean _isMatchingQualifier;    
    
    
    static public final Enumeration EMPTY_ENUMERATION = new Enumeration() {
        public boolean hasMoreElements() {
            return false;
        }

        public Object nextElement() {
            throw new NoSuchElementException("Plus d'element");
        }
    };
    

    
    public ZTreeNode2(final ZTreeNode2 parentNode) {
        setParent(parentNode);
        if (parentNode != null) {
            setQualifier(parentNode.getQualifier());
//            refreshMatchQualifier();
        }        
        
        
    }
    
    public boolean isLeaf() {
        return getChildCount() == 0;
    }

    public Enumeration children() {
        return Collections.enumeration(childs);
    }

    public TreeNode getParent() {
        return _parentNode;
    }

    public TreeNode getChildAt(int childIndex) {
        return (TreeNode) childs.get(childIndex);
    }

    public int getIndex(final TreeNode node) {
        return childs.indexOf(node);
    }
    
    public int getChildCount() {
        return childs.size();
    }
    
    
    /**
     * Modifie le parent du noeud et met à jour les enfants.
     * @param newParent
     */
    public void setParent(final ZTreeNode2 newParent) {
        if (_parentNode != null) {
            _parentNode.removeChild(this);
        }
        _parentNode = newParent;
        if (_parentNode != null) {
            _parentNode.addChild(this);
        }
    }

    
    
    
    private void addChild(ZTreeNode2 node2) {
        childs.add(node2);
    }

    public void removeChild(ZTreeNode2 node2) {
        childs.remove(node2);
    }    
    
    

    public void refreshMatchQualifier() {
        _isMatchingQualifier = ((_qualifier == null || getAssociatedObject()==null) ? true : _qualifier.evaluateWithObject(getAssociatedObject()));
    }
    
    
    /**
     * Indique si le noeud rentre dans la condition du qualifier.
     * @return
     */
    public final boolean isMatchingQualifier() {
        return _isMatchingQualifier;
    }



    /**
     * Permet d'affecter un qualifier au noeud. 
     * @see ZTreeNode2#isMatchingQualifier()
     * @param qual
     */
    public final void setQualifier(EOQualifier qual) {
        _qualifier = qual;
        refreshMatchQualifier();
//        invalidateNode();        
    }

    
    public final void setQualifierRecursive(EOQualifier qual) {
        _qualifier = qual;
        refreshMatchQualifier();
        final Enumeration enumeration = children(); 
        while (enumeration.hasMoreElements()) {
            final ZTreeNode2 element = (ZTreeNode2) enumeration.nextElement();
            element.setQualifierRecursive(qual);
        }
    }
        
    public EOQualifier getQualifier() {
        return _qualifier;
    }

    
    public void invalidateNode() {
        refreshChilds();
    }
    
    
    public abstract Object getAssociatedObject();
    
    protected abstract void refreshChilds();
    
    
    
    public TreePath buildTreePath() {
        TreeNode tmp = this;
        TreePath path = null;
        if (tmp != null) {
            final LinkedList nodes = new LinkedList();
            while (tmp != null) {
                nodes.add(0, tmp);
                tmp = tmp.getParent();
            }
            path = new TreePath(nodes.toArray());
        }
        return path;
    }    
    
}
