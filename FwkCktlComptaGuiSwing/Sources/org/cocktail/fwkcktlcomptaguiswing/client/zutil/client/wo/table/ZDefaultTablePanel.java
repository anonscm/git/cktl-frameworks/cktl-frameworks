/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table;

import javax.swing.ListSelectionModel;

public class ZDefaultTablePanel extends ZTablePanel {
    protected IZDefaultTablePanelMdl _ctrl;
    public ZDefaultTablePanel(IZDefaultTablePanelMdl listener) {
        super(listener);
        _ctrl = listener;
        initColumns();
        initGUI();
        getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);        
    }
    
    /**
     * Initialise les colonnes.
     *
     */
    protected void initColumns() {
        colsMap.clear();
        for (int i = 0; i < _ctrl.getColumnKeys().length; i++) {
            final String array_element = _ctrl.getColumnKeys()[i];
            final ZEOTableModelColumn col = new ZEOTableModelColumn(myDisplayGroup,array_element , _ctrl.getColumnTitles()[i],100);
            colsMap.put(array_element ,col);
        }        
    }
    
    
    
    
    public interface IZDefaultTablePanelMdl extends IZTablePanelMdl {
        /** renvoie la liste de clés en String NSValueCoding pour les colonnes */
        public String[] getColumnKeys();
        
        /** renvoie une liste de Titres en String pour les colonnes */
        public String[] getColumnTitles();
        
    }

}
