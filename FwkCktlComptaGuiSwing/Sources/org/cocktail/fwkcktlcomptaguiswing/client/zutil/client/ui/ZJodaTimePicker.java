package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import org.joda.time.LocalDate;

public class ZJodaTimePicker extends ZDatePickerPanel {
	private static final long serialVersionUID = 1L;

	public LocalDate getLocalDate() {
		return new LocalDate(getSelectedDate());
	}

	public void open(LocalDate ladate) {
		if (ladate == null) {
			ladate = new LocalDate();
		}
		updateForDate(ladate.toDate());
	}
}
