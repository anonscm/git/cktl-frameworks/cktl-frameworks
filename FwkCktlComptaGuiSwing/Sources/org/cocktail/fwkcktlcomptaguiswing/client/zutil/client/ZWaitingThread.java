/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Permet d'avoir un outil pour executer des taches ï¿½ intervalle prï¿½cis (communication avec le serveur, etc.)
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZWaitingThread extends Thread {

    protected final int intervalle;
    protected final int delai;
    private Timer beatTimer;
    private Timer waitingTimer;
    private BeatTask beatTask;
    private WaitingTask waitingTask;
    private final IZHeartBeatListener _listener;
    private MainTaskThread mainTaskThread;
    private BeatTaskThread beatTaskThread;
    
    private final Integer stateLock=new Integer(0);
    
    
    /**
     * @param delay Delai en ms avant lequel va s'executer la tache
     * @param period Intervalle d'execution de la tï¿½che en ms
     * @param listener Listener qui vous permet de spï¿½cifier quelles sont les tï¿½ches a executer. 
     * @see IZHeartBeatListener
     */
    public ZWaitingThread(final int delay, final int period, final IZHeartBeatListener listener) {
        super();
        delai = delay;
        intervalle = period;
        _listener = listener;
    }
    
    
//    private boolean isMainTaskFinished() {
//        return _listener.isMainTaskFinished();
//    }
//    
    
    public void run() {
        try {
            waitingTimer = new Timer();
            waitingTask = new WaitingTask();
            waitingTimer.scheduleAtFixedRate(waitingTask, 500, 500);
            
            beatTaskThread = new BeatTaskThread();
            mainTaskThread = new MainTaskThread();
            
            mainTaskThread.setDaemon(true);
            mainTaskThread.setPriority(Thread.currentThread().getPriority()-1);
            
            beatTaskThread.start();
            mainTaskThread.start();
            
            
        } catch (Exception e) {
            e.printStackTrace();
            _listener.onException(e);
        } finally {
            _listener.onFinally();
        }

    }

    /**
     * @see java.lang.Thread#interrupt()
     */
    public void interrupt() {
        if (mainTaskThread != null) {
            mainTaskThread.interrupt();
        }
        
        if (beatTimer != null) {
            beatTimer.cancel();
        }
        if (waitingTimer != null) {
            waitingTimer.cancel();
        }        
        if (beatTaskThread != null) {
            beatTaskThread.interrupt();
        }
        super.interrupt();
    }
    
    

    
    /**
     * Tache a executer a intervalle specifie par appelant.
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    private class BeatTask extends TimerTask {
        /**
         * 
         */
        public BeatTask() {
        }

        /**
         * @see java.util.TimerTask#run()
         */
        public void run() {
            Thread.yield();
            try {
                _listener.onBeat();
                _listener.afterBeat();
                if (_listener.isMainTaskFinished()) {
                    this.cancel();
                }                
            } catch (Exception e) {
                _listener.onException(e);               
            }
        }
    }  
    
    /**
     * Tache a executer pour maintenir les differents threads.
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    private class WaitingTask extends TimerTask {
        /**
         * 
         */
        public WaitingTask() {
        }
        
        /**
         * @see java.util.TimerTask#run()
         */
        public void run() {
            Thread.yield();
            try {
                if (_listener.isMainTaskFinished()) {
                    synchronized (getStateLock()) {
                        getStateLock().notifyAll();
                    }                   
                }                
            } catch (Exception e) {
                _listener.onException(e);               
            }
        }
    }  
    
    
    
    
    public Integer getStateLock( ){
        return stateLock;
    }
    
    
    
    /**
     * Tache principale (appelï¿½e une seule fois)
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    private class MainTaskThread extends Thread {
        /**
         * 
         */
        public MainTaskThread() {
        }
        
        /**
         * @see java.util.TimerTask#run()
         */
        public void run() {
            Thread.yield();
            try {
                _listener.mainTaskStart();
                
                synchronized (getStateLock()) {
                    getStateLock().wait();
//                    System.out.println("Tache princpale terminee");
                }
                
                
            } catch (Exception e) {
                _listener.onException(e);               
            }
        }
    }    
    
    /**
     * Tache qui permet de gï¿½rer l'ï¿½volution de la tache princpale. 
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    private class BeatTaskThread extends Thread {
        /**
         * 
         */
        public BeatTaskThread() {
        }
        
        /**
         * @see java.util.TimerTask#run()
         */
        public void run() {
            Thread.yield();
            try {
                beatTimer = new Timer();
                beatTask = new BeatTask();
                beatTimer.scheduleAtFixedRate(beatTask, delai, intervalle);
            } catch (Exception e) {
                _listener.onException(e);               
            }
        }
    }    
    
    /**
     * Interface ï¿½ implï¿½menter pour utiliser le timer.
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     * @see DefaultZHeartBeatListener
     */
    public interface IZHeartBeatListener {
        /** Appelï¿½ ï¿½ chaque intervalle de temps*/
        public void onBeat();
        /** Appelï¿½ une fois l'execution de onBeat() effectuee s'il n'y a pas eu d'exception*/
        public void afterBeat();
        /** Appelï¿½ dans la clause finally */
        public void onFinally();
        /** Appelï¿½ dans la clause catch */
        public void onException(Exception e);
        
        /** Implï¿½menter cette mï¿½thode pour lancer le traitement de la tï¿½che principale */
        public void mainTaskStart();
        
        /** Implï¿½menter cette mï¿½thode pour indiquer au thread si la tï¿½che principale est terminï¿½e */
        public boolean isMainTaskFinished();
        
        
    }
    
    
    /**
     * Implementation par defaut le l'interface ZHeartBeatListener. Toutes les mï¿½thodes sont vides.
     * Surchargez les mï¿½thodes que vous souhaitez utiliser. 
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public abstract static class DefaultZHeartBeatListener implements IZHeartBeatListener {
        public void onBeat() {
        }
        public void afterBeat() {
        }
        public void onFinally() {
        }
        public void onException(Exception e) {
        }
    }
    

}
