package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.basic.BasicTableUI;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

public class FileListTablePanel extends ZTablePanel implements DragSourceListener, DragGestureListener {
	public static DataFlavor FILEFLAVOR = DataFlavor.javaFileListFlavor;

	public ZFileTransferHandler transferHandler;
	private DragSource dragSource = DragSource.getDefaultDragSource();

	public FileListTablePanel(IFileListTablePanelMdl listener) {
		super(listener);
		transferHandler = new ZFileTransferHandler();
		final ZEOTableModelColumn nomFichier = new ZEOTableModelColumn(myDisplayGroup, IFileListTablePanelMdl.NOM_FICHIER_KEY, "Nom", 200);
		colsMap.clear();
		colsMap.put(IFileListTablePanelMdl.NOM_FICHIER_KEY, nomFichier);
	}

	public void updateData() throws Exception {
		super.updateData();
	}

	public void initGUI() {
		super.initGUI();
		myEOTable.setTransferHandler(transferHandler);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		myEOTable.getTableHeader().setReorderingAllowed(false);
		dragSource.createDefaultDragGestureRecognizer(myEOTable, DnDConstants.ACTION_COPY, this);
		myEOTable.setUI(new MyTableUI());
	}

	/**
	 * @see java.awt.dnd.DragSourceListener#dragEnter(java.awt.dnd.DragSourceDragEvent)
	 */
	public void dragEnter(DragSourceDragEvent dsde) {
	}

	/**
	 * @see java.awt.dnd.DragSourceListener#dragOver(java.awt.dnd.DragSourceDragEvent)
	 */
	public void dragOver(DragSourceDragEvent dsde) {

	}

	/**
	 * @see java.awt.dnd.DragSourceListener#dropActionChanged(java.awt.dnd.DragSourceDragEvent)
	 */
	public void dropActionChanged(DragSourceDragEvent dsde) {

	}

	/**
	 * @see java.awt.dnd.DragSourceListener#dragDropEnd(java.awt.dnd.DragSourceDropEvent)
	 */
	public void dragDropEnd(DragSourceDropEvent dsde) {

	}

	/**
	 * @see java.awt.dnd.DragSourceListener#dragExit(java.awt.dnd.DragSourceEvent)
	 */
	public void dragExit(DragSourceEvent dse) {
	}

	/**
	 * @see java.awt.dnd.DragGestureListener#dragGestureRecognized(java.awt.dnd.DragGestureEvent)
	 */
	public void dragGestureRecognized(DragGestureEvent dge) {
		if (selectedObjects() == null) {
			return;
		}
		final FileSelection transferable = new FileSelection(selectedObjects());
		dge.startDrag(DragSource.DefaultCopyDrop, transferable, this);
	}

	public class FileSelection extends Vector implements Transferable {
		final static int FILE = 0;
		final static int STRING = 1;
		DataFlavor flavors[] = {
				DataFlavor.javaFileListFlavor, DataFlavor.stringFlavor
		};

		public FileSelection(NSArray files) {
			for (int i = 0; i < files.count(); i++) {
				NSKeyValueCoding array_element = (NSKeyValueCoding) files.objectAtIndex(i);
				addElement(array_element.valueForKey(IFileListTablePanelMdl.FICHIER_KEY));
			}
		}

		/* Returns the array of flavors in which it can provide the data. */
		public synchronized DataFlavor[] getTransferDataFlavors() {
			return flavors;
		}

		/* Returns whether the requested flavor is supported by this object. */
		public boolean isDataFlavorSupported(final DataFlavor flavor) {
			System.out.println("FileSelection.isDataFlavorSupported()");
			boolean b = false;
			b |= flavor.equals(flavors[FILE]);
			b |= flavor.equals(flavors[STRING]);
			return (b);
		}

		/**
		 * If the data was requested in the "java.lang.String" flavor, return the String representing the selection.
		 */
		public synchronized Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException, IOException {
			if (flavor.equals(flavors[FILE])) {
				return this;
			}
			else if (flavor.equals(flavors[STRING])) {
				return ((File) elementAt(0)).getAbsolutePath();
			}
			else {
				throw new UnsupportedFlavorException(flavor);
			}
		}
	}

	public final class ZFileTransferHandler extends TransferHandler {

		public ZFileTransferHandler() {
			System.out.println("ZFileTransferHandler.ZFileTransferHandler()");
		}

		/**
		 * @see javax.swing.TransferHandler#exportDone(javax.swing.JComponent, java.awt.datatransfer.Transferable, int)
		 */
		protected void exportDone(JComponent source, Transferable data, int action) {
			super.exportDone(source, data, action);
			//                System.out.println("ZFileTransferHandler.exportDone()");
		}

		/**
		 * @see javax.swing.TransferHandler#createTransferable(javax.swing.JComponent)
		 */
		protected Transferable createTransferable(JComponent c) {
			//                System.out.println("ZFileTransferHandler.createTransferable()");
			return super.createTransferable(c);
		}

		public boolean importData(JComponent c, Transferable t) {
			return false;
		}

		protected boolean hasFileFlavor(DataFlavor[] flavors) {
			//                System.out.println("ZFileTransferHandler.hasFileFlavor()");
			for (int i = 0; i < flavors.length; i++) {
				if (FILEFLAVOR.equals(flavors[i])) {
					return true;
				}
			}
			return false;
		}
	}

	public class MyTableUI extends BasicTableUI {

		protected MouseInputListener createMouseInputListener() {
			return new MyMouseInputHandler();
		}

		/*
		 * This class listens for selections on table rows. It prevents table row selections when mouse is dragged (default behaviour for multi row
		 * interval selections) and instead fires row drag (DND) events.
		 */
		class MyMouseInputHandler extends MouseInputHandler {

			private boolean ignoreDrag = false;

			public void mousePressed(MouseEvent e) {

				// Cancel the selected area if the user
				// clicks twice, otherwise if the user had
				// selected all of the cells they couldn't
				// de-select them with the mouse
				if (e.getClickCount() == 2) {
					table.clearSelection();
				}

				// Always set this to true - it will be
				// set if the user has clicked on a cell
				// that is already selected.
				ignoreDrag = true;

				// Found out whether the user has clicked
				// on a cell that it is already selected
				Point origin = e.getPoint();
				int row = table.rowAtPoint(origin);
				int column = table.columnAtPoint(origin);
				if (row != -1 && column != -1) {
					if (table.isCellSelected(row, column)) {
						// The cell is selected, so we
						// need to ignore the mouse drag
						// events completely.
						ignoreDrag = true;

					}
					else {
						// Only perform the usual mouse pressed
						// operation of the user has not clicked
						// on a cell that is already selected.
						super.mousePressed(e);
					}
				}
			}

			public void mouseDragged(MouseEvent e) {
				if (!ignoreDrag) {
					// We only call this method when a user has NOT clicked on a cell that is already selected 
					super.mouseDragged(e);
				}
				else {

					// Start a table row drag operation
					table.getTransferHandler().exportAsDrag(table, e, DnDConstants.ACTION_COPY);

				}
			}

			public void mouseReleased(MouseEvent e) {
				if (ignoreDrag) {
					// When clicking on an already selected row and releasing mouse => clear selection on table
					table.clearSelection();
				}

				super.mousePressed(e);
			}
		}

	}

	public interface IFileListTablePanelMdl extends IZTablePanelMdl {
		public static final String NOM_FICHIER_KEY = "FILE_NAME";
		public static final String FICHIER_KEY = "FILE";
	}
}

