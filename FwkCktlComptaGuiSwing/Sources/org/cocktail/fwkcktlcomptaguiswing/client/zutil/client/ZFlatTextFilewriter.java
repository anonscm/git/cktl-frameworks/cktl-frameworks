/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;


/**
 * Moteur pour la generation des fichiers textes d'exports. 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZFlatTextFilewriter extends FileWriter {
        /** un espace*/
        protected String SPACER=" ";
        /** retour a la ligne*/
        protected String CRLF="\r\n";
        
        protected String colSeparator;
        
        /**
         * @param file
         * @throws IOException
         */
        public ZFlatTextFilewriter(File file) throws IOException {
            this(file, "");
        }
        
        public ZFlatTextFilewriter(File file, String colSeparator) throws IOException {
        	super(file);
        	this.colSeparator= colSeparator; 
        }
        
        /** Ecrit la ligne d'entete de fichier */
        public abstract void writeHeader(final Map headerMap) throws Exception;
        
        /** Ecrit une ligne (hors entete / bas de fichier*/
        public abstract void writeLine(final Map lineMap) throws Exception;        
        
        /** Ecrit la ligne bas de fichier*/
        public abstract void writeFooter(final Map footerMap) throws Exception;
        
        /** Ecrit toutes les lignes de detail (appelle writeLine)*/
        public void writeLines(final ArrayList lines) throws Exception {
            Iterator iter = lines.iterator();
            while (iter.hasNext()) {
                Map element = (Map) iter.next();
                writeLine(element);
            }
        }
        
        
        public String encodeStringAlignLeft(final String value, final int nbCharacters) throws Exception {
    	    String s=value;
    	    if (s==null) {
    	        s="";
    	    }
    	    if (value.length()>nbCharacters) {
    	        throw new Exception("La longueur de la chaine est plus grande que l'espace qui lui est reserve (" + value + " depasse "+ nbCharacters +" caracteres.)");
    	    }
    		for(;s.length() < nbCharacters; s = s+SPACER);
    		return s + colSeparator;            
        }        
        public String encodeStringAlignLeft(final String value, final int nbCharacters, final String spacer) throws Exception {
    	    String s=value;
    	    if (s==null) {
    	        s="";
    	    }
    	    if (value.length()>nbCharacters) {
    	        throw new Exception("La longueur de la chaine est plus grande que l'espace qui lui est reserve (" + value + " depasse "+ nbCharacters +" caracteres.)");
    	    }
    		for(;s.length() < nbCharacters; s = s+spacer);
    		return s+ colSeparator;            
        }        
        
        public String encodeStringAlignRight(final String value, final int nbCharacters) throws Exception {
    	    String s=value;
    	    if (s==null) {
    	        s="";
    	    }
    	    if (value.length()>nbCharacters) {
    	        throw new Exception("La longueur de la chaine est plus grande que l'espace qui lui est reserve (" + value + " depasse "+ nbCharacters +" caracteres.)");
    	    }
    		for(;s.length() < nbCharacters; s = SPACER+s);
    		return s+ colSeparator;            
        }        
        public String encodeStringAlignRight(final String value, final int nbCharacters, final String spacer) throws Exception {
    	    String s=value;
    	    if (s==null) {
    	        s="";
    	    }
    	    if (value.length()>nbCharacters) {
    	        throw new Exception("La longueur de la chaine est plus grande que l'espace qui lui est reserve (" + value + " depasse "+ nbCharacters +" caracteres.)");
    	    }
    		for(;s.length() < nbCharacters; s = spacer+s);
    		return s+ colSeparator;            
        }        
        
        
        
        
        public abstract String encodeNumber(Object value, final int nbCharacters) throws Exception;
        public abstract String encodeDate(final Date value, final int nbCharacters) throws Exception;        
        
        
        
        
    }
