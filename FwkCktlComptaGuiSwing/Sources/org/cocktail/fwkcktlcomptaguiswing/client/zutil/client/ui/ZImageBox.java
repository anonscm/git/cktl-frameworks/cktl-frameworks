/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.border.BevelBorder;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;

public class ZImageBox extends ZAbstractSwingPanel implements DropTargetListener {
	public static DataFlavor FILEFLAVOR = DataFlavor.javaFileListFlavor;;
	private ZImageView imageView;
	private IZImageBoxMdl _mdl;

	public ZImageBox(IZImageBoxMdl mdl) {
		super(new BorderLayout());
		_mdl = mdl;

		imageView = new ZImageView(_mdl, ZImageView.SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER);

		add(imageView, BorderLayout.CENTER);
		setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		new DropTarget(this, this);
	}

	public void updateData() throws Exception {
		super.updateData();

		imageView.updateData();
	}

	public void dragEnter(DropTargetDragEvent dtde) {
		imageView.setBorder(BorderFactory.createLineBorder(Color.RED));
	}

	public void dragExit(DropTargetEvent dte) {
		imageView.setBorder(null);
	}

	public void dragOver(DropTargetDragEvent dtde) {

	}

	public void dropActionChanged(DropTargetDragEvent dtde) {

	}

	public void drop(DropTargetDropEvent evt) {
		if (_mdl.canDrop()) {
			try {
				final Transferable t = evt.getTransferable();
				ZLogger.verbose(t);
				if (t.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
					evt.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

					ZLogger.verbose("class=" + t.getTransferData(DataFlavor.javaFileListFlavor).getClass().getName());
					final List s = (List) t.getTransferData(DataFlavor.javaFileListFlavor);
					evt.getDropTargetContext().dropComplete(true);
					ZLogger.verbose(" drop list = " + s);
					final File f = (File) s.get(0);
					_mdl.setValue(f);
					try {
						updateData();
					} catch (Exception e) {
						//Pas de probleme normalement
						e.printStackTrace();
					}
				}
				else {
					evt.rejectDrop();
				}
			} catch (IOException e) {
				evt.rejectDrop();
			} catch (UnsupportedFlavorException e) {
				evt.rejectDrop();
			}
		}
		imageView.setBorder(null);
	}

	public interface IZImageBoxMdl extends ZImageView.IZImageMdl {
		public void setValue(File f);

		/** doit renvoyer true quand on pêut dropper une image */
		public boolean canDrop();
	}

	public ZImageView getImageView() {
		return imageView;
	}

}
