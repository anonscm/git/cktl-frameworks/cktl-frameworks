/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;

import org.cocktail.fwkcktlwebapp.common.CktlDockClient;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZEOApplication extends EOApplication {
	private EOEditingContext editingContext;
	public EOClientResourceBundle resourceBundle;

	/**
	 *
	 */
	public ZEOApplication() {
		super();
		editingContext = new EOEditingContext();
		resourceBundle = new EOClientResourceBundle();
	}

	public EOEditingContext editingContext() {
		return editingContext;
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public final String getIpAdress() {
		try {
			final String s = java.net.InetAddress.getLocalHost().getHostAddress();
			final String s2 = java.net.InetAddress.getLocalHost().getHostName();
			return s + " / " + s2;
		} catch (UnknownHostException e) {
			return "Machine inconnue";

		}
	}

	/**
	 * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	 */
	public final class MyByteArrayOutputStream extends ByteArrayOutputStream {
		private PrintStream out;

		public MyByteArrayOutputStream() {
			this(System.out);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			super();
			this.out = out;
		}

		public synchronized void write(final int b) {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(final byte[] b, final int off, final int len) {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}

	/**
	 * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	 */
	public final class MyFileOutputStream extends FileOutputStream {
		private final PrintStream out;

		public MyFileOutputStream(PrintStream out, File file) throws FileNotFoundException {
			super(file);
			this.out = out;
		}

		public synchronized void write(final int b) throws IOException {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(final byte[] b, final int off, final int len) throws IOException {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}

	public String getCASUserName() {
		final NSDictionary arguments = arguments();
		final String dockPort = (String) arguments.valueForKey("LRAppDockPort");
		final String dockTicket = (String) arguments.valueForKey("LRAppDockTicket");
		String uname = null;
		if (dockPort != null && dockTicket != null) {
			uname = CktlDockClient.getNetID(null, dockPort, dockTicket);
		}
		return uname;
	}

	public String getJREVersion() {
		return System.getProperty("java.version");
	}

}
