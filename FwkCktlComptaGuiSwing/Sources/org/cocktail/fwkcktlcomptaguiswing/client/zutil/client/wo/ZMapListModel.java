/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo;

import java.util.Map;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZListModel;

public class ZMapListModel extends ZListModel {
	protected final Map map;

	public ZMapListModel(Map _map) {
		super();
		map = _map;
	}

	//    /**
	//     * renvoi l'affichage de l'objet
	//     * 
	//     * @see javax.swing.ListModel#getElementAt(int)
	//     * @see ZMapListModel#getObjectAt(int)
	//     */
	//    public Object getElementAt(int index) {
	//        final Object obj = getObjectAt(index);
	//        if (obj==null) {
	//            return null;
	//        }
	//        return displayFormat.format(obj);
	//    }

	//    protected boolean isElementMatchesFilter(Object element, String _filter) {
	////        System.out.println("ZEOListModel.isElementMatchesFilter()");
	//        
	//        
	//        return super.isElementMatchesFilter(displayFormat.format(element), _filter);
	//    }

	/**
	 * @param index
	 * @return L'objet dans la liste (pas son affichage)
	 */
	public Object getObjectAt(int index) {
		return map.get(getElementAt(index));
	}

	public void updateData() {
		super.updateData(map.keySet());
	}

}
