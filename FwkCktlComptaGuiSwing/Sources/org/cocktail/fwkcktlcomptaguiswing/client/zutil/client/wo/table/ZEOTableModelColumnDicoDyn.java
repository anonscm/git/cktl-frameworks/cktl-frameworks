/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table;

import java.util.Map;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * Descriptif d'une colonne dont les valeurs ï¿½ afficher sont rï¿½cupï¿½rï¿½es depuis un HashMap, dont les clï¿½s sont les EOEnterpriseObjects
 * contenues dans le displaygroup. Cette classe est utile pour afficher/modifier des valeurs en lien avec les lignes du displaygroup. Pour faire une
 * colonne qui contient des cases ï¿½ cocher pour permetre ï¿½ l'utilisateur de sï¿½lectionner des lignes, crï¿½er une instance de cette en passant un
 * HashMap contenant en clï¿½ les objets du displayGroup et en valeur Boolean.TRUE ou Boolean.FALSE.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZEOTableModelColumnDicoDyn extends ZEOTableModelColumn {

	//private final Map dico;
	private IZEOTableModelColumnDicoListener listener;

	/**
	 * @param attName
	 * @param vTitle
	 * @param vpreferredWidth
	 * @param checkedDico Dictionnaire contenant en clï¿½ un enregistrement du displaygroup et en valeur un Boolean. La synchronisation de ce dico
	 *            doit etre effectuee manuellement.
	 */
	public ZEOTableModelColumnDicoDyn(EODisplayGroup vDg, String vTitle, int vpreferredWidth, IZEOTableModelColumnDicoListener _listener) {
		super(vDg, vTitle, vTitle, vpreferredWidth);
		listener = _listener;
	}

	public Object getValueAtRow(final int row) {
		return listener.checkDico().get(getMyDg().displayedObjects().objectAtIndex(row));
	}

	public void setValueAtRow(final Object value, final int row) {
		if (getMyModifier() != null) {
			getMyModifier().setValueAtRow(value, row);
		}
		listener.checkDico().put(getMyDg().displayedObjects().objectAtIndex(row), value);
	}

	/**
	 * Change la valeurs de toutes les lignes pour cette colonne. Cette mï¿½thode ne fait pas appel ï¿½ fireTableDataChanged() du model, ï¿½ vous de
	 * l'implï¿½menter...
	 * 
	 * @param newVal
	 */
	public void setValueForAllRows(final Object newVal) {
		final NSArray res = getMyDg().displayedObjects();
		for (int i = 0; i < res.count(); i++) {
			setValueAtRow(newVal, i);
			//			dico.put(res.objectAtIndex(i),newVal);
		}
	}

	public interface IZEOTableModelColumnDicoListener {
		/**
		 * Dictionnaire contenant en clé un enregistrement du displaygroup et en valeur un Boolean. La synchronisation de ce dico doit etre effectuee
		 * manuellement.
		 * 
		 * @return
		 */
		public Map checkDico();
	}
}
