/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.tree;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZTree extends JTree {

	/**
	 * 
	 */
	public ZTree() {
		super();
	}

	/**
	 * @param value
	 */
	public ZTree(Object[] value) {
		super(value);
	}

	/**
	 * @param value
	 */
	public ZTree(Hashtable value) {
		super(value);
	}

	/**
	 * @param value
	 */
	public ZTree(Vector value) {
		super(value);
	}

	/**
	 * @param newModel
	 */
	public ZTree(TreeModel newModel) {
		super(newModel);
	}

	/**
	 * @param root
	 */
	public ZTree(TreeNode root) {
		super(root);
	}

	/**
	 * @param root
	 * @param asksAllowsChildren
	 */
	public ZTree(TreeNode root, boolean asksAllowsChildren) {
		super(root, asksAllowsChildren);
	}
	
	
	public void enableToolTips(boolean allow) {
		if (allow) {
			ToolTipManager.sharedInstance().registerComponent(this);
		}
		else {
			ToolTipManager.sharedInstance().unregisterComponent(this);	
		}
		
	}
	
	
//	
//	public void expandAll(boolean expand) {
//		ZTreeNode root = (ZTreeNode)getModel().getRoot();
//    
//		// Traverse tree from root
//		expandAll(new TreePath(root), expand);
//	}
//	
//	private void expandAll(TreePath parent, boolean expand) {
//		ZTreeNode node = (ZTreeNode)parent.getLastPathComponent();
//		if (node.getChildCount() >= 0) {
//			ArrayList tmpList=node.getMyChilds();
//			if (tmpList!=null) {
//				for (Iterator e=tmpList.iterator() ; e.hasNext(); ) {
//					ZTreeNode n = (ZTreeNode)e.next();
//					TreePath path = parent.pathByAddingChild(n);
//					expandAll(path, expand);
//				}
//			}
//		}
//    
//		
//		if (expand) {
//			expandPath(parent);
//		} else {
//			collapsePath(parent);
//		}
//	}
//	
	

	public void expandAllObjectsAtLevel(final int level, final boolean expand ) {
		TreeNode root = (TreeNode)getModel().getRoot();
		// Traverse tree from root
		expandAllObjectsAtLevel(new TreePath(root), level, expand, 0);		
	}
	
	private void expandAllObjectsAtLevel(TreePath parent, final int level,final boolean expand, final int currentLevel) {
		TreeNode node = (TreeNode)parent.getLastPathComponent();
		if (currentLevel<level) {
			if (node.getChildCount() >= 0) {
                Enumeration enumeration = node.children();
                while (enumeration.hasMoreElements()) {
                    TreeNode element = (TreeNode) enumeration.nextElement();
                    TreePath path = parent.pathByAddingChild(element);
                    expandAllObjectsAtLevel(path, level, expand,currentLevel+1);
                }
//                
//                
//				ArrayList tmpList=node. ;
//				if (tmpList!=null) {
//					for (Iterator e=tmpList.iterator() ; e.hasNext(); ) {
//						ZTreeNode n = (ZTreeNode)e.next();
//						TreePath path = parent.pathByAddingChild(n);
//						expandAllObjectsAtLevel(path, level, expand,currentLevel+1);
//					}
//				}
			}
		}
    
		if (currentLevel==level  ) {
			if (expand) {
				expandPath(parent);
			} else {
				collapsePath(parent);
			}
		}
	}		
	
//	
//	public void expandAllObjectsWithClass(Class theClass, boolean expand ) {
//		TreeNode root = (TreeNode)getModel().getRoot();
//		// Traverse tree from root
//		expandAllObjectsWithClass(new TreePath(root), theClass, expand);		
//	}
//    
    
    
//	
//	private void expandAllObjectsWithClass(TreePath parent, Class theClass ,boolean expand) {
//		TreeNode node = (TreeNode)parent.getLastPathComponent();
//		if (node.getChildCount() >= 0) {
//			ArrayList tmpList=node.getMyChilds();
//			if (tmpList!=null) {
//				for (Iterator e=tmpList.iterator() ; e.hasNext(); ) {
//					TreeNode n = (TreeNode)e.next();
//					TreePath path = parent.pathByAddingChild(n);
//					expandAllObjectsWithClass(path, theClass, expand);
//				}
//			}
//		}
//    
//		if (node.getMyObject().getClass().equals(theClass)  ) {
//			if (expand) {
//				expandPath(parent);
//			} else {
//				collapsePath(parent);
//			}
//		}
//	}	
//	
//	
	

}
