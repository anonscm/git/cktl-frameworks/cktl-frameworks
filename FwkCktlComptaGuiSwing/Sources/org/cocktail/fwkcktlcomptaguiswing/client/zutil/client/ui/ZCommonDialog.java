/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * Classe abstraite dont doivent heriter les fenetres de l'application.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZCommonDialog extends JDialog {
	public static final int MROK = 1;
	public static final int MRCANCEL = 0;
	protected int modalResult;
	private BusyGlassPanel myBusyGlassPanel;
	private Point lastPos;

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZCommonDialog(Dialog owner, String title, boolean modal)
			throws HeadlessException {
		super(owner, title, modal);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZCommonDialog(Frame owner, String title, boolean modal)
			throws HeadlessException {
		super(owner, title, modal);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}

	/**
	 *
	 */
	public int getModalResult() {
		return modalResult;
	}

	/**
	 * @param i
	 */
	public void setModalResult(int i) {
		modalResult = i;
	}

	public void onOkClick() {
		setModalResult(MROK);
		setVisible(false);
	}

	public void onCancelClick() {
		setModalResult(MRCANCEL);
		setVisible(false);
	}

	public void onCloseClick() {
		setModalResult(MRCANCEL);
		setVisible(false);
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	public int open() {
		setModalResult(MRCANCEL);
		centerWindow();
		setVisible(true);
		//en modal la fenetre reste active jusqu'au closeWindow...
		return getModalResult();
	}

	public final void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2), ((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

	}

	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

	public void maximize() {
		lastPos = getLocation();
		this.setVisible(false);
		this.setLocation(0, 0);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(screenSize.width, screenSize.height);
		this.setVisible(true);
	}

	public void normal() {
		if (lastPos != null) {
			this.setLocation(lastPos);
		}
		this.setVisible(false);
		this.setLocation(0, 0);
		this.setSize(getContentPane().getPreferredSize());
		this.setVisible(true);
	}

}
