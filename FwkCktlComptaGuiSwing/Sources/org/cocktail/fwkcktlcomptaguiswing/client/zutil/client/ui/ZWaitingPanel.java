/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;



/**
 *
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZWaitingPanel extends JPanel {
	private String globalTaskDescription;
	private String currentTaskDescription;

	private JProgressBar myProgressBar;
	private JLabel globalTaskLabel;
	private JLabel currentTaskLabel;

	private ImageIcon icon;

	private ZWaitingPanelListener myListener;

	/**
	 *
	 */
	public ZWaitingPanel(ZWaitingPanelListener listener ) {
		super();
		myListener = listener;
		initComponent();

	}
	public ZWaitingPanel(ZWaitingPanelListener listener, ImageIcon img) {
		super();
		myListener = listener;
		icon = img;
		initComponent();
	}

	private void initComponent() {
		myProgressBar = new JProgressBar(0, 100);
		myProgressBar.setValue(0);
		myProgressBar.setStringPainted(true);
		myProgressBar.setString("");
		myProgressBar.setIndeterminate(true);
		myProgressBar.setBorder(BorderFactory.createEmptyBorder());

		globalTaskLabel = new JLabel();
		globalTaskLabel.setAlignmentX(  (float) 0.5  );
		currentTaskLabel = new JLabel();
		currentTaskLabel.setAlignmentX(  (float) 0.5  );

		Box globalTaskBlock = Box.createHorizontalBox();
		globalTaskBlock.add(Box.createGlue());
		globalTaskBlock.add(globalTaskLabel);
		globalTaskBlock.add(Box.createGlue());

		Box currentTaskBlock = Box.createHorizontalBox();
		currentTaskBlock.add(Box.createGlue());
		currentTaskBlock.add(currentTaskLabel);
		currentTaskBlock.add(Box.createGlue());

//		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		Box mainBox = Box.createHorizontalBox();
		mainBox.add(Box.createRigidArea(new Dimension(2,100)));
		Box contentBox = Box.createVerticalBox();
		contentBox.add(Box.createRigidArea(new Dimension(400,2)));
		contentBox.add(globalTaskBlock);
		contentBox.add(myProgressBar);
		contentBox.add(currentTaskBlock);
		contentBox.add(Box.createRigidArea(new Dimension(400,2)));
		mainBox.add(contentBox);
		mainBox.add(Box.createRigidArea(new Dimension(2,100)));

		setLayout(new BorderLayout());
		if (icon != null) {
		    JLabel labelIcon = new JLabel(icon);
		    //Permet d'afficher les gif animes
		    icon.setImageObserver(labelIcon);
		    JPanel panelLeft = new JPanel(new BorderLayout());
		    panelLeft.setBorder(BorderFactory.createEmptyBorder(4,5,4,10));
		    panelLeft.add(labelIcon, BorderLayout.CENTER);
		    add(panelLeft, BorderLayout.WEST);
		}

		if (myListener != null && myListener.cancelAction() != null) {
		    final ArrayList list = new ArrayList();
		    list.add(myListener.cancelAction());
		    List<Component> buttons  = ZUiUtil.getButtonListFromActionList(list);

		    JPanel p = new JPanel(new BorderLayout());
		    p.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		    p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		    p.add(ZUiUtil.buildBoxLine(buttons), BorderLayout.EAST);
		    add(p, BorderLayout.SOUTH);
		}

		this.add(mainBox,BorderLayout.CENTER);
	}





	private void refresh() {
		globalTaskLabel.setText(globalTaskDescription);
		currentTaskLabel.setText(currentTaskDescription);
//		paintAll(this.getGraphics());
	}


	/**
	 * @param string
	 */
	public void setCurrentTaskDescription(String string) {
		currentTaskDescription = string;
		refresh();

	}

	/**
	 * @param string
	 */
	public void setGlobalTaskDescription(String string) {
		globalTaskDescription = string;
		refresh();
	}




    public JProgressBar getMyProgressBar() {
        return myProgressBar;
    }
    public ImageIcon getIcon() {
        return icon;
    }
    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    public interface ZWaitingPanelListener {
        public Action cancelAction();
    }
}
