/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of PrestationJC.
 *
 * PrestationJC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * PrestationJC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * Panel comportant des champs de saisie spécialement pour représenter un message (email).
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZMailModelPanel extends ZAbstractSwingPanel {
	
	private final String C_LABELFROM="Expéditeur";
	private final String C_LABELTO="Destinataire";
	private final String C_LABELCC="Cc";
	private final String C_LABELSUBJECT="Objet";
	
	private final int C_FIELDHEIGHT=20;
	private final int C_FIELDWIDTH=350;
	private final int C_LABELWIDTH=85;
	
	private String borderTitle;
	

	private JTextField fromJTextField;
	private JTextField toJTextField;
	private JTextField ccJTextField;
	private JTextField subjectJTextField;
	private JTextArea bodyJTextarea;
	private JScrollPane bodyJScrollPane;
	
	
	
	/**
	 * 
	 */
	public ZMailModelPanel() {
		super();
		initGUI();
	}
	
	
	
	private void initGUI() {
		this.setAlignmentX(LEFT_ALIGNMENT);
		if (borderTitle!=null) {
			this.setBorder(BorderFactory.createTitledBorder(borderTitle));	
		}
		Box box1 = Box.createHorizontalBox();
		box1.setAlignmentX(LEFT_ALIGNMENT);
		Box boxBg = Box.createVerticalBox();
		boxBg.setAlignmentX(LEFT_ALIGNMENT);
		boxBg.add(buildFrom());	
		boxBg.add(buildTo());	
		boxBg.add(buildCc());	
		boxBg.add(buildSubject());
		boxBg.add(buildBody());
		box1.add(boxBg);
		box1.add(Box.createGlue());	
		this.add(box1);
	}
		
	private JPanel buildFrom() {
		JPanel tmp = new JPanel();
		
		Box boxTmp = Box.createHorizontalBox();
		JLabel label = new JLabel(C_LABELFROM);
		label.setAlignmentX(LEFT_ALIGNMENT);
		label.setPreferredSize(new Dimension(C_LABELWIDTH,C_FIELDHEIGHT));
		label.setSize(label.getPreferredSize());
		boxTmp.add(label);
		boxTmp.add(Box.createRigidArea(new Dimension(10,C_FIELDHEIGHT)));
		fromJTextField = new JTextField( getFrom() );
		fromJTextField.setMinimumSize(new Dimension(C_FIELDWIDTH,C_FIELDHEIGHT));
		fromJTextField.setPreferredSize(fromJTextField.getMinimumSize());
		fromJTextField.setSize(fromJTextField.getPreferredSize());
		fromJTextField.setAlignmentX(LEFT_ALIGNMENT);
		fromJTextField.setEnabled(true);
		boxTmp.add(fromJTextField);
		tmp.add(boxTmp);
		return tmp;
	}
	
	private JPanel buildTo() {
		JPanel tmp = new JPanel();
		Box boxTmp = Box.createHorizontalBox();
		JLabel label = new JLabel(C_LABELTO);
		label.setAlignmentX(LEFT_ALIGNMENT);
		label.setPreferredSize(new Dimension(C_LABELWIDTH,C_FIELDHEIGHT));
		label.setSize(label.getPreferredSize());
		boxTmp.add(label);
		boxTmp.add(Box.createRigidArea(new Dimension(10,C_FIELDHEIGHT)));
		toJTextField = new JTextField( getTo() );
		toJTextField.setMinimumSize(new Dimension(C_FIELDWIDTH,C_FIELDHEIGHT));
		toJTextField.setPreferredSize(toJTextField.getMinimumSize());
		toJTextField.setSize(toJTextField.getPreferredSize());
		toJTextField.setAlignmentX(LEFT_ALIGNMENT);
		toJTextField.setEnabled(true);
		boxTmp.add(toJTextField);
		tmp.add(boxTmp);
		return tmp;
	}	
	
	private JPanel buildCc() {
		final JPanel tmp = new JPanel();
		final Box boxTmp = Box.createHorizontalBox();
		final JLabel label = new JLabel(C_LABELCC);
		label.setAlignmentX(LEFT_ALIGNMENT);
		label.setPreferredSize(new Dimension(C_LABELWIDTH,C_FIELDHEIGHT));
		label.setSize(label.getPreferredSize());
		boxTmp.add(label);
		boxTmp.add(Box.createRigidArea(new Dimension(10,C_FIELDHEIGHT)));
		final JTextField lccJTextField = new JTextField( getCc() );
		lccJTextField.setMinimumSize(new Dimension(C_FIELDWIDTH,C_FIELDHEIGHT));
		lccJTextField.setPreferredSize(lccJTextField.getMinimumSize());
		lccJTextField.setSize(lccJTextField.getPreferredSize());
		lccJTextField.setAlignmentX(LEFT_ALIGNMENT);
		lccJTextField.setEnabled(true);
		boxTmp.add(lccJTextField);
		tmp.add(boxTmp);
		return tmp;
	}		
	
	
	private JPanel buildSubject() {
		final JPanel tmp = new JPanel();
        final Box boxTmp = Box.createHorizontalBox();
        final JLabel label = new JLabel(C_LABELSUBJECT);
		label.setAlignmentX(LEFT_ALIGNMENT);
		label.setPreferredSize(new Dimension(C_LABELWIDTH,C_FIELDHEIGHT));
		label.setSize(label.getPreferredSize());
		boxTmp.add(label);
		boxTmp.add(Box.createRigidArea(new Dimension(10,C_FIELDHEIGHT)));
		subjectJTextField = new JTextField( getSubject() );
		subjectJTextField.setMinimumSize(new Dimension(C_FIELDWIDTH,C_FIELDHEIGHT));
		subjectJTextField.setPreferredSize(subjectJTextField.getMinimumSize());
		subjectJTextField.setSize(subjectJTextField.getPreferredSize());
		subjectJTextField.setAlignmentX(LEFT_ALIGNMENT);
		subjectJTextField.setEnabled(true);
		boxTmp.add(subjectJTextField);
		tmp.add(boxTmp);
		return tmp;
	}	
		
		
	private JPanel buildBody() {
        final JPanel tmp = new JPanel();
        final Box boxTmp = Box.createHorizontalBox();		
		bodyJTextarea = new JTextArea(getBody());
		bodyJTextarea.setFont(bodyJTextarea.getFont().deriveFont((float)11));
		bodyJTextarea.setEnabled(true);
		bodyJScrollPane = new JScrollPane(bodyJTextarea);
		bodyJScrollPane.setPreferredSize(new Dimension(460,180));
		boxTmp.add(bodyJScrollPane);
		tmp.add(boxTmp);
		return tmp;
	}	
		
	public String getBody() {
		if (bodyJTextarea!=null) {
			return  bodyJTextarea.getText() ;	
		}
        return null;
	}

	/**
	 * @return
	 */
	public String getCc() {
		if (ccJTextField!=null) {
			return  ccJTextField.getText() ;	
		}
        return null;
	}



	/**
	 * @return
	 */
	public String getSubject() {
		if (subjectJTextField!=null) {
			return  subjectJTextField.getText() ;	
		}
        return null;
	}

	/**
	 * @return
	 */
	public String getTo() {
		if (toJTextField!=null) {
			return  toJTextField.getText() ;	
		}
        return null;
	}

	/**
	 * @param string
	 */
	public void setBody(String string) {
		if (bodyJTextarea!=null) {
			bodyJTextarea.setText(string) ;
			bodyJTextarea.setCaretPosition(0);	
		}
	}

	/**
	 * @param string
	 */
	public void setCc(String string) {
		if (ccJTextField!=null) {
			ccJTextField.setText(string) ;	
		}
	}



	/**
	 * @param string
	 */
	public void setSubject(String string) {
		if (subjectJTextField!=null) {
			subjectJTextField.setText(string) ;	
		}
	}

	/**
	 * @param string
	 */
	public void setTo(String string) {
		if (toJTextField!=null) {
			toJTextField.setText(string) ;	
		}
	}

	/**
	 * @return
	 */
	public String getFrom() {
		if (toJTextField!=null) {
			return  toJTextField.getText() ;	
		}
        return null;
	}

	/**
	 * @param string
	 */
	public void setFrom(String string) {
		if (fromJTextField!=null) {
			fromJTextField.setText(string) ;	
		}
	}



    public void updateData() throws Exception {
        
    }








}
