/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo;

import java.text.Format;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * Classe pour crÃ©er un modÃ©le pour les listes dÃ©roulantes.<br>
 * Ce modÃ©le permet d'affecter un tableau d'objets respectant l'interface
 * NSKeyValueCoding (par exmple un NSArray d'EOEnterpriseObjects ou
 * d'EOGenericRecord), en spÃ©cifiant la clÃ© Ã© utiliser pour afficher les
 * valeurs dans la liste dÃ©roulante.<br>
 * <br>
 * Les Exemple:<br>
 * <code>
 * ...<br>
 * 		NSArray res = fetchDataCatalogues(currentFournis); //res contient un tableau d'EOGenericRecord<br>
 * 		ZEOComboBoxModel myZEOComboBoxModel = new ZEOComboBoxModel(res, "catLibelle");  //le combobox affichera le contenu de l'attribut catLibelle<br>
 * 		ldCatalogues.setModel(myZEOComboBoxModel); //ldCatalogue est un JComboBox<br>
 * 		if ((res!=null) && (res.count()>0)) {<br>
 * 			myZEOComboBoxModel.setSelectedEObject(res.objectAtIndex(0));<br>
 * 		}<br>
 * 		<br>
 * ...<br>
 * </code>
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZEOComboBoxModel extends DefaultComboBoxModel {
	private ArrayList myObjects;
	private final String myDisplayKey;
	private String nullValue;
	private final Format formatDisplay;

	/**
	 * Contructeur du modÃ©le.
	 * 
	 * @param lesObjets
	 *            Tableau contenant des objets implÃ©mentant l'interface
	 *            NSKeyValueCoding.
	 * @param displayKey
	 *            Nom de la propriÃ©tÃ© qui doit permettre d'afficher le
	 *            libellÃ© de chaque Ã©lÃ©ment de la liste dÃ©roulante (en
	 *            NSKeyValueCoding). Sera affichÃ©e avec valueForKey(). Si null,
	 *            la valeur affichÃ©e sera obtenue via un toString().
	 * @param labelForNullValue
	 *            si non null, une valeur sera affichÃ©e en dÃ©but de modele et
	 *            getSelectedEObject renverra null si selectionnÃ©.
	 * @param formatPattern
	 */
	public ZEOComboBoxModel(NSArray lesObjets, String displayKey, String labelForNullValue, Format format) {
		super();
		myDisplayKey = displayKey;
		nullValue = labelForNullValue;
		formatDisplay = format;
		updateListWithData(lesObjets);
	}

	/**
	 * Renvoie l'objet correspondant Ã© la sÃ©lection.
	 */
	public NSKeyValueCoding getSelectedEObject() {
		if (super.getSelectedItem() == null) {
			return null;
		}
		final int i = super.getIndexOf(super.getSelectedItem());
		if (i == -1) {
			return null;
		}
		return (NSKeyValueCoding) myObjects.get(i);
	}

	/**
	 * Met aï¿½ jour les donnees.
	 * 
	 * @param lesObjets
	 *            Tableau contenant des objets implÃ©mentant l'interface
	 *            NSKeyValueCoding.
	 */

	public final void updateListWithData(final NSArray lesObjets) {
		//nettoyer
		this.removeAllElements();

		if (lesObjets != null) {

			myObjects = new ArrayList();

			//Gerer l'affichage ou non d'un element null
			if (nullValue != null) {
				myObjects.add(null);
				this.addElement(nullValue);
			}

			//Gerer l'affichage des infos
			for (int i = 0; i < lesObjets.objects().length; i++) {
				myObjects.add(lesObjets.objects()[i]);
			}

			//		System.out.println("myObjects : " + myObjects);

			//		Object tmp;
			for (int i = 0; i < lesObjets.count(); i++) {
				final Object tmp = lesObjets.objectAtIndex(i);
				//			if ( !(tmp instanceof NSKeyValueCoding) ) {
				//				throw new ZUtilException ("L'objet ne gere pas l'interface NSKeyValueCoding.");
				//			}
				if (myDisplayKey == null) {
					this.addElement(((NSKeyValueCoding) tmp).toString());
				}
				else {
					final Object obj = ((NSKeyValueCoding) tmp).valueForKey(myDisplayKey);
					if (formatDisplay == null) {
						this.addElement(obj.toString());
					}
					else {
						this.addElement(formatDisplay.format(obj));
					}

				}
			}
		}

		//		System.out.println("nb obj dans la liste"+ this.getSize() );
		//		System.out.println("-------------------");
	}

	/**
	 * Definit quel est l'element qui doit etre selectionne dans la liste
	 * deroulante.
	 * 
	 * @param selectedObject
	 */
	public void setSelectedEObject(final NSKeyValueCoding selectedObject) {
		if (selectedObject == null) {
			if (nullValue == null) {
				super.setSelectedItem(null);
			}
			else {
				super.setSelectedItem(nullValue);
			}
		}
		else {
			if (formatDisplay == null) {
				super.setSelectedItem(selectedObject.valueForKey(myDisplayKey).toString());
			}
			else {
				super.setSelectedItem(formatDisplay.format(selectedObject.valueForKey(myDisplayKey)));
			}

		}
	}

	public final ArrayList getMyObjects() {
		return myObjects;
	}

	public String getNullValue() {
		return nullValue;
	}

	public void setNullValue(String nullValue) {
		this.nullValue = nullValue;
	}

}
