/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table;

import java.text.Format;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

import org.cocktail.fwkcktlcompta.common.exception.DefaultClientException;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.eointerface.EODisplayGroup;

/**
 * Modele de colonne de table pour afficher/ modifier directement la relation d'un EO. A utiliser avec les relations 1/1.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZEOTableModelColumnComboBoxWithRelation extends ZEOTableModelColumn {
	private EODisplayGroup _objectsToList;
	private String _attributeForList;
	private final String _relationName;
	private String _nullLabel;
	private Format _formatPattern;
	private ZEOComboBoxModel _comboboxModel;
	private final JComboBox _myComboBox;

	/**
	 * Crï¿½e un modele de colonne basï¿½ sur une relation d'enterpriseobject, avec gestion d'une combobox.
	 * 
	 * @param dg Displaygroup contenant les enterpriseObjects des rows
	 * @param attName Nom de l'attribut associï¿½ ï¿½ la colonne
	 * @param vTitle Titre de la colonne
	 * @param objectsToList DisplayGroup contenant les objets qui seront listï¿½s dans la combobox
	 * @param attributeForList Attribut utilispour rï¿½cupï¿½rer les valeurs ï¿½ afficher dans la combobox
	 * @param relationName Nom de la relation entre l'eoenterpriseObject affichï¿½ dans la table et l'eoenterpriseObjet affichï¿½ dans la combobox
	 * @param nullLabel Valeur ï¿½ associer ï¿½ l'objet null ï¿½ afficher dans la combobox. Si null, l'option nulle ne sera pas proposï¿½e dans la
	 *            combobox.
	 * @param formatPattern Format ï¿½ appliquer pour afficher la valeur dans la combobox
	 * @param vPreferredWidth Taille par dï¿½faut de la colonne
	 * @throws DefaultClientException
	 */
	public ZEOTableModelColumnComboBoxWithRelation(
			final EODisplayGroup dg,
			final String attName,
			final String vTitle, final EODisplayGroup objectsToList, final String attributeForList, final String relationName, final String nullLabel, final Format formatPattern, int vPreferredWidth) throws DefaultClientException {
		super(dg, attName, vTitle, vPreferredWidth);
		_relationName = relationName;

		_myComboBox = new JComboBox();
		if (objectsToList != null) {
			setTableCellEditor(new DefaultCellEditor(_myComboBox));
			updateComboWithData(objectsToList, attributeForList, nullLabel, formatPattern);
		}
	}

	public final void updateComboWithData(final EODisplayGroup objectsToList, final String attributeForList, final String nullLabel, final Format formatPattern) throws DefaultClientException {
		try {
			_objectsToList = objectsToList;
			_attributeForList = attributeForList;
			_nullLabel = nullLabel;
			_formatPattern = formatPattern;
			_comboboxModel = new ZEOComboBoxModel(_objectsToList.displayedObjects(), _attributeForList, _nullLabel, _formatPattern);
			_myComboBox.setModel(_comboboxModel);

		} catch (Exception e) {
			throw new DefaultClientException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTableModelColumn#getValueAtRow(int)
	 */
	public Object getValueAtRow(final int row) {
		return ((EOEnterpriseObject) (getMyDg().displayedObjects().objectAtIndex(row))).valueForKeyPath(getAttributeName());
	}

	/**
	 * Si un modifier est afefctï¿½ ï¿½ la colonne, la mï¿½thode setValueAtRow du modifier est appelï¿½e, avec l'EOEnterprisObject comme parametre.
	 * 
	 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn#setValueAtRow(java.lang.Object, int)
	 */
	public void setValueAtRow(final Object value, final int row) {
		if (getMyModifier() != null) {
			getMyModifier().setValueAtRow(_comboboxModel.getSelectedEObject(), row);
		}
		else {

			final EOEnterpriseObject obj = ((EOEnterpriseObject) getMyDg().displayedObjects().objectAtIndex(row));
			//Nettoyer la relation si deja presente
			if ((obj.valueForKey(_relationName) != null) && (obj.valueForKey(_relationName) != EOKeyValueCoding.NullValue)) {
				obj.removeObjectFromBothSidesOfRelationshipWithKey((EOEnterpriseObject) obj.valueForKey(_relationName), _relationName);
			}
			if (value != null) {
				obj.addObjectToBothSidesOfRelationshipWithKey((EOEnterpriseObject) _comboboxModel.getSelectedEObject(), _relationName);
			}

		}
	}

}
