/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.util.StringTokenizer;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;



/**
* classe regroupant des utilitaires de chaines.
* @author Rodolphe PRIN - rodolphe.prin at cocktail.org
*/
public abstract class ZStringUtil {
    
    private static final String CONST_DELIM = "'- ()\t\n\r\f";
    private static final String CONST_VIDE = "";
    private static final String CONST_NULL = "null";
    public static final String DECIMALCHARS = "01234567889.";
    public static final String[] IGNOREWORDSFORCAPITALIZE = {"au","aux","ce","ces","de","des","du","en","et","la","le","les","ne","nos","on","or","ou","où","par","pas","pour","puis","qq.","qqch.","qqn","que","qui","quoi","sa","sauf","se","ses","si","sur","te","tu","un","une","vs","ça","çà"};

    
    public static boolean estVide(String s) {
        if ((s==null) || (s.trim().length()==0)|| (s==CONST_NULL)) {
            return true;
        }
        return false;
    }

    /**
     * Renvoie true si la chaine s est égale à null ou à "". Sinon renvoie false.
     * 
     * @param s Chaine à tester
     */
    public static boolean isEmpty(String s) {
        if ((s==null) || (s.trim().length()==0)|| (s==CONST_NULL)) {
            return true;
        }
        return false;
    }
    

    /**
     * Vérifie si une chaine pstr est vide (ou nulle) et si c'est le cas la méthode renvoie la chaine defaultStr, sinon elle renvoie pstr.
     * 
     * 
     * @param pstr Chaine à tester
     * @param defaultStr Chaine de remplacement
     */
    public static String ifNull(String pstr, String defaultStr) {
        if ((pstr==null) || (pstr.trim().length()==0) || (pstr==CONST_NULL) ) {
            return defaultStr;
        }
        return pstr;
    }
    
    /**
     * Vérifie si la représentation chaine d'un objet pstr est vide (ou nulle) et si c'est le cas la méthode renvoie la chaine defaultStr, sinon elle renvoie pstr.
     * Prend en compte NSKeyValueCoding.NullValue 
     * 
     * @param pstr Objet à tester
     * @param defaultStr Chaine de remplacement
     */    
    public static String ifNull(Object pstr, String defaultStr) {
        String strtemp = null;
        if ( (pstr == NSKeyValueCoding.NullValue) || (pstr==null) ) {
            strtemp = null;
        }
        else {
            strtemp = pstr.toString();  
        }
        
        if ((strtemp==null) || (strtemp.trim().length()==0)|| (pstr==CONST_NULL)) {
            return defaultStr;
        }
        return strtemp;
    }
    
    
    
    /**
     * Vérifie si la chaine pstr est vide (ou nulle) et si c'est le cas la méthode renvoie la chaine "". 
     * Appelle {@link org.cocktail.fwkcktlwebapp.common.util.StringCtrl#normalize}.
     * 
     * @param pstr Chaine à tester
     */    
    public static String ifNull(String pstr ) {
        /*
        if ((pstr==null) || (pstr.trim().length()==0)) {
            return "";
        }
        else {
            return pstr;
        }
        */
        return ifNull(pstr,CONST_VIDE);
        //return StringCtrl.normalize(pstr);
    }
    


    /**
     * 
     * @param pstr
     * @param appendStr
     * @return Si la chaine pstr n'est pas nulle, renvoie la chaine pstr + appendstr, sinon revoie une chaine vide 
     */
    public static String ifNotNullAppend(Object pstr, String appendStr) {
        String strtemp = null;
        if ( (pstr == NSKeyValueCoding.NullValue) || (pstr==null) ) {
            strtemp = null;
        }
        else {
            strtemp = pstr.toString();  
        }
        
        if ((strtemp==null) || (strtemp.trim().length()==0)) {
            return CONST_VIDE;
        }
        return strtemp +appendStr;
    }

    

    public static String reduitVisibleChars(String s, int nbVisibleChars) {
        if (s.length()<= nbVisibleChars) {
            return s;
        }
        return s.substring(0,nbVisibleChars-3)+"...";
    }

    public static String chaineSansCaracteresSpeciauxUpper(String str) {
        String retour = str.toLowerCase();
        retour = retour.replaceAll("é","e");
        retour = retour.replaceAll("è","e");
        retour = retour.replaceAll("ê","e");
        retour = retour.replaceAll("ë","e");

        retour = retour.replaceAll( "à","a");
        retour = retour.replaceAll( "â","a");

        retour = retour.replaceAll( "î","i");
        retour = retour.replaceAll( "ï","i");

        retour = retour.replaceAll( "ô","o");
        retour = retour.replaceAll( "ö","o");
        retour = retour.replaceAll( "ò","o");

        retour = retour.replaceAll( "ü","u");
        retour = retour.replaceAll( "û","u");
        retour = retour.replaceAll( "ù","u");
        
        retour = retour.replaceAll( "ç","c");        
        
        retour = retour.replaceAll("\n", " ");
        retour = retour.replaceAll("\r", "");
        retour = retour.replaceAll("\t", "");
        
        retour = retour.toUpperCase();
        return retour;
    }


    /**
    * FORMATTER TELEPHONE : On formate le numéro en mettant des '.' entre chaque chiffre. Supprime les autres caracteres
     */
    public static String formaterTelephone(String phone) {
        int digCount = 0;
        StringBuffer sb = new StringBuffer();

        phone = ifNull(phone);
        for(int i=0; i<phone.length(); i++) {
          if ((digCount == 2) && (sb.length() < 14)) {
            sb.append(".");
            digCount = 0;
          }
          if (isBasicDigit(phone.charAt(i))) {
            sb.append(phone.charAt(i));
            digCount++;
          }
        }
        return sb.toString();        
              
    }
    

    /**
     * @param c
     * @return
     */
    private static boolean isBasicDigit(char c) {
        int numVal = Character.getNumericValue(c);
        return ((Character.getNumericValue('0') <= numVal) &&
                (numVal <= Character.getNumericValue('9')));

        //return false;
    }

    /**
      * Renvoie uniquement les caractères numériques contenus dans une chaine
     */
    public static String keepOnlyNumChars(String num) {
        StringBuffer res= new StringBuffer();
        for (int i=0;i<num.length();i++) {
            if (isBasicDigit(num.charAt(i)) ) {
                res.append(num.substring(i,i+1));
            }
        }
        return res.toString();
    }


    /**
     * Formate une chaine de caractères sur la longueur spécifiée, en ajoutant au besoin des caractères au début ou à la fin de la chaine. 
     * Si la chaine original est trop longue, elle est coupée. 
     * 
     * 
     * @param str La chaine originale
     * @param zeroChar Le caractère à ajouter au début ou à la fin de la chaine originale.
     * @param length Longueur de la chaine à renvoyer.
     * @param inFront True si on veut ajouter les charctères au début, false si c'est à la fin.
     * @return La chaine modifiée
     */
    public static final String extendWithChars(final String string, final String addChars, final int length, final boolean inFront) {
        String s=string;
        if (s==null) {
            s=CONST_VIDE;
        }
        for(;s.length() < length; s = ((inFront)?(addChars+s):(s+addChars)));
        return s;
    }        
    
    public static final String extendWithChars(final Object obj, final String addChars, final int length, final boolean inFront) {
        return extendWithChars((obj==null ? CONST_VIDE : obj.toString()),addChars, length, inFront);
    }   
    
    
    
    /**
     * Coupe la chaine <i>s</i> en laissant au maximum <i>maxLen</i> caracteres.
     * La chaine est inchangee si sa longeur ne depasse pas <i>maxLen</i>.
     * S'il le faut, les caracteres sont ellimines a la fin de la chaine.
     * 
     */
    public static String cut(String s, int maxLen) {
      return cut(s, maxLen, false);
    }

    /**
     * Coupe la chaine <i>s</i> en laissant au maximum <i>maxLen</i> caracteres.
     * La chaine est inchangee si sa longeur ne depasse pas <i>maxLen</i>.
     * La valeur inFront indique si les caracteres doivent etre ellimines
     * au debut (<i>true</i>) ou a la fin de la chaine (<i>false</i>) ou au de.
     * 
     */
    public static String cut(String s, int maxLen, boolean inFront) {
      if ((s == null) || (s.length() <= maxLen)) return s;
      if (inFront) return s.substring(s.length()-maxLen);
    return s.substring(0, maxLen);
    }
  
    
    
    
    /**
     * Renvoie une chaine de caractères avec la première lettre en majuscule et les autres en minuscules.
     * @param aString
     * @return La chaine modifiée.
     */
    public static String capitalizedString(String aString) {
        if (CONST_VIDE.equals(aString))   return CONST_VIDE;
        String debut = (aString.substring(0,1)).toUpperCase();
        String fin = (aString.substring(1,aString.length())).toLowerCase();
        return debut.concat(fin);
    }
    
    
    /**
     * Renvoie une chaine avec tous les mots ayant leur première lettre en majuscule.<br>
     * Par exemple : <hr>
     * La chaine "ACHATS D'ETUDES ET PRESTATIONS DE SERVICES" est transformée en
     * "Achats d'Etudes et Prestations de Services"
     * <hr>
     * Les mots sont récupérés en se basant sur les séparateurs "' ()\t\n\r\f".<br>
     * Les mots d'un caractère sont mis en minuscule.<br>
     * Les mots qui se trouvent dans le tableau IGNOREWORDSFORCAPITALIZE sont également mis en minuscules.<br> 
     * 
     * @param aString Chaine à transformer.
     * 
     */
    public static String capitalizedWords(String aString, String delimiters) {
        if (aString == null || aString.length()==0)   return aString;
        boolean ignoreWord;
        String delim = (delimiters == null ? CONST_DELIM : delimiters);
        StringTokenizer stok = new StringTokenizer(aString, delim, true);
        String words[] = new String[stok.countTokens()];
        //récupérer les mots
        for(int i = 0; i < words.length; i++) {
           words[i] = stok.nextToken();
        }
        //On met une majuscule en début de chaque mot
        for(int i = 0; i < words.length; i++) {
            //on ignore les caractères de délimitation
            if ( delim.indexOf(words[i]) < 0  ) {
                //On passe le mot en minuscule
                words[i] = words[i].toLowerCase();
                //si le mot est dans la liste des mots à ignorer, ben on l'ignore -;)
                //Pareil si on a une seule lettre
                ignoreWord=false;
                if (words[i].length()==1) {
                    ignoreWord = true;   
                }
                else {
                    for (int j = 0; j < IGNOREWORDSFORCAPITALIZE.length; j++) {
                        if (IGNOREWORDSFORCAPITALIZE[j].equals( words[i] )) {
                            ignoreWord=true;
                        }
                    }
                }
                if ( !ignoreWord  ) {
                    words[i] = words[i].substring(0,1).toUpperCase() + words[i].substring(1,words[i].length()); 
                }
            }
        }
        
        StringBuffer buf = new StringBuffer();
        for(int i = 0; i < words.length; i++) {
            buf.append(words[i]);
        }
        
        return buf.toString();
    }


    /**
     * Remplace une sous-chaine par une autre.
     * @param s Chaine entière.
     * @param what Sous-chaine à remplacer.
     * @param byWhat Sous-chaine de remplacement.
     * @return La chaine modifiée.
     */
    public static String replaceStringByAnother(String s, String what, String byWhat) {
        StringBuffer sb;
        int i;
    
//        if ((s == null) || (what == null)) return s;
        sb = new StringBuffer();
        if (byWhat == null) byWhat = CONST_VIDE;
        do {
          i = s.indexOf(what);
          if (i >= 0) {
            sb.append(s.substring(0, i));
            sb.append(byWhat);
            s = s.substring(i+what.length());
          }
        } while(i != -1);
        sb.append(s);
        return sb.toString();
        //return StringCtrl.replace(str, replaceWhat, byWhat);
    }
    
    
    
    
    /**
     * Teste si la chaine de caracteres est "acceptable". Elle l'est si tous
     * les caracteres de la chaine sont acceptables (<i>isAcceptChar</i>) :
     * les lettres "de base", les chiffres et les caracteres acceptes par defaut.
     * 
     * @see #isAcceptChar(char)
     */
    public static boolean isAcceptBasicString(String aString) {
      for(int i=0; i<aString.length(); i++) {
        if (!isAcceptChar(aString.charAt(i))) return false;
      }
      return true;
    }   
    
    /**
     * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
     * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
     * (<i>isBasicDigit</i>) ou un des caracteres <i>acceptChars</i>.
     * 
     * @see #isBasicLetter(char)
     * @see #isBasicDigit(char)
     * @see #isAcceptChar(char) 
     */
    public static boolean isAcceptChar(char c, String acceptChars) {
      boolean rep = isBasicLetter(c);
      if (!rep) rep = isBasicDigit(c);
      if ((!rep) && (acceptChars != null)) {
        for(int i=0; i<acceptChars.length(); i++)
          if (c == acceptChars.charAt(i)) return true;
      }
      return rep;
    }

    /**
     * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
     * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
     * (<i>isBasicDigit</i>) ou un des caracteres supplementaires acceptes par
     * defaut (<i>defaultAcceptChars</i>).
     * 
     * @see #isBasicLetter(char)
     * @see #isBasicDigit(char)
     * @see #isAcceptChar(char, String)
     * @see #defaultAcceptChars() 
     */
    public static boolean isAcceptChar(char c) {
      return isAcceptChar(c, defaultAcceptChars());
    }
    
    /**
     * Test si le caractere <i>c</i> est une lettre "de base" (a-z, A-Z).
     * Il ne doit pas etre une lettre accentue, une chiffre ou un autre caractere
     * special. 
     */
    public static boolean isBasicLetter(char c) {
      int numVal = Character.getNumericValue(c);
      return (((Character.getNumericValue('a') <= numVal) &&
               (numVal <= Character.getNumericValue('z'))) ||
              ((Character.getNumericValue('A') <= numVal) &&
               (numVal <= Character.getNumericValue('Z'))));
    }
    

    /**
     * Retourne la liste des caracteres acceptes par defaut comme caracteres
     * legales.
     * 
     * <p>Cette implementation renvoie la chaine "._-".
     * 
     * @see #isAcceptChar(char)
     */
    public static String defaultAcceptChars() {
      return "._-";
    }
    
	public static String toBasicString(String s, String acceptChars, char charToReplace) {
		if (s == null || s.length() == 0)
			return s;
		StringBuffer newStr = new StringBuffer();
		for (int i = 0; i < s.length(); i++)
			if (isAcceptChar(s.charAt(i), acceptChars))
				newStr.append(s.charAt(i));
			else
				newStr.append(charToReplace);

		return newStr.toString();
	}

	public static String toBasicString(String aString) {
		return toBasicString(aString, defaultAcceptChars(), '_');
	}    

    /**
     * Effectue un trim (suppression des espaces de début et de fin) sur toutes les valeurs du dico, si ce sont des chaines.
     * 
     */
    public static void trimAllValuesInDic(NSMutableDictionary dictionary) {
        NSArray keys = dictionary.allKeys();
        for (int i = 0; i < keys.count(); i++) {
            //NSLog.out.appendln(dictionary.valueForKey((String)keys.objectAtIndex(i) ).getClass().getName());
            if ( dictionary.valueForKey((String)keys.objectAtIndex(i) ) instanceof java.lang.String    ) {
                ((String)dictionary.valueForKey((String)keys.objectAtIndex(i) )).trim();    
            }
            
        }       
    }

    /**
     * Convertie le numero number en une chaine de caracteres. S'il le faut,
     * les "0" sont ajoutes au debut de la chaine pour qu'elle ait la longeur
     * <i>digits</i>.
     * 
     * @see #extendWithChars(String, String, int, boolean)
     */
    public static String get0Int(int number, int digits) {
      String s = String.valueOf(number);
      return extendWithChars(s, "0", digits, true);
    }

    /**
     * Copie la chaine passée en paramètre dans le presse-papiers.
     * @param str
     */
    public static final void copyToClipboard(final String str) {
        StringSelection ss = new StringSelection(str);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
    }
    

    /**
     * Concatene deux chaines, en rajoutant un éventuel séparateur entre les deux.
     * @param str1
     * @param str2
     * @param separateur
     * @return La nouvelle chaine, concatenation des deux
     */
    public static final String concatStr(final String str1, final String str2, final String separateur) {
        if (str1==null ||str1.length()==0 ) {
            return str2;
        }
        if (str2==null ||str2.length()==0) {
            return str1;            
        }
        return str1.concat(separateur==null ? CONST_VIDE : separateur).concat(str2);
    }    
    
    
    
}





    
    

