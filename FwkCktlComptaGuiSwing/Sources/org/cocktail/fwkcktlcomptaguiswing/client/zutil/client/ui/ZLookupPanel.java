/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZListModel;

/**
 * Permet d'afficher une liste avec un champ de filtre dynamique au dessus.
 */
public class ZLookupPanel extends ZAbstractSwingPanel {

	private final JTextField lookupField;
	private JList list;
	private final IZLookupPanelModel model;
	private final IZLookupPanelListener listener;

	private Dialog currentDialog;
	private Object lastSelectedObject;

	public interface IZLookupPanelModel {
		/** Modele qui contient les donnÃ©es Ã  afficher */
		public ZListModel getDataModel();

		/** Filtre les donnÃ©es Ã  afficher */
		public void filter(String text);

		public Collection getDatas();

		public String getTitle();
	}

	public interface IZLookupPanelListener {
		public void onTextHasChanged();

		public void onDbClick();
	}

	public ZLookupPanel(final IZLookupPanelModel aModel, final IZLookupPanelListener aListener) {
		super();
		model = aModel;
		listener = aListener;
		lookupField = new JTextField();
		lookupField.getDocument().addDocumentListener(new MyDocumentListener());

		list = new JList(model.getDataModel());
		list.addMouseListener(new MyMouseListener());
		//        list.setCellRenderer(new TabListCellRenderer());

		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		list.setFocusable(false);

		String family = "Courier";
		int style = Font.PLAIN;
		int size = 11;
		Font font = new Font(family, style, size);
		//        g.setFont(font);        

		list.setFont(font);

		final Action downAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (list.getSelectedIndex() < list.getModel().getSize() - 1) {
					list.setSelectedIndex(list.getSelectedIndex() + 1);
				}
			}
		};

		final Action upAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (list.getSelectedIndex() > 0) {
					list.setSelectedIndex(list.getSelectedIndex() - 1);
				}
			}
		};

		final Action enterAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				dbClick();
			}
		};

		final KeyStroke down = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, false);
		final KeyStroke up = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, false);
		final KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false);

		lookupField.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(down, "DOWN");
		lookupField.getActionMap().put("DOWN", downAction);
		lookupField.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(up, "UP");
		lookupField.getActionMap().put("UP", upAction);
		lookupField.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(enter, "ENTER");
		lookupField.getActionMap().put("ENTER", enterAction);

		setLayout(new BorderLayout());
		final JScrollPane listScroller = new JScrollPane(list);
		add(lookupField, BorderLayout.NORTH);
		add(listScroller, BorderLayout.CENTER);

	}

	public void initGUI() {

	}

	private final void textHasChanged() {
		final Object selection = list.getSelectedValue();
		// Filtrer la liste
		model.filter(lookupField.getText());

		// Metre a jour la selection
		if (model.getDataModel().getSize() > 0) {
			final int n = model.getDataModel().indexOf(selection);
			if (n >= 0) {
				list.setSelectedValue(selection, true);
			}
			else {
				list.setSelectedIndex(0);
			}
		}
		else {
			list.setSelectedValue(null, false);
		}

		// Informer les copains
		if (listener != null) {
			listener.onTextHasChanged();
		}
	}

	private final void dbClick() {
		if (listener != null) {
			listener.onDbClick();
		}
		lastSelectedObject = getSelectedObject();
		// System.out.println(getSelectedObject());

		if (getCurrentDialog() != null) {
			getCurrentDialog().setVisible(false);
			getCurrentDialog().dispose();
		}
	}

	public Object getSelectedObject() {
		return list.getSelectedValue();

	}

	private final class MyDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent e) {
			textHasChanged();

		}

		public void insertUpdate(DocumentEvent e) {
			textHasChanged();

		}

		public void removeUpdate(DocumentEvent e) {
			textHasChanged();

		}

	}

	private final class MyMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent evt) {
			super.mouseClicked(evt);
			if (evt.getClickCount() == 2) {
				dbClick();
			}
		}
	}

	public static class DefaultLookupPanelModel implements IZLookupPanelModel {
		private final ZListModel dataModel = new ZListModel();
		private final ArrayList list = new ArrayList();

		public DefaultLookupPanelModel() {
		}

		public ZListModel getDataModel() {
			return dataModel;
		}

		public void filter(String text) {
			getDataModel().setFilterText(text);
		}

		public Collection getDatas() {
			return list;
		}

		public String getTitle() {
			return "Sélectionnez une valeur";
		}

	}

	public Dialog CreateDialog(final Window owner) {
		final JDialog dial2;
		if (owner instanceof Dialog) {
			dial2 = new JDialog((Dialog) owner);
		}
		else {
			dial2 = new JDialog((Frame) owner);
		}

		setCurrentDialog(dial2);

		// initGUI();
		dial2.setContentPane(this);

		// Ajouter la gestion de la touche echap pour fermer la fenetre
		final KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
		final Action escapeAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				dial2.setVisible(false);
			}
		};
		dial2.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
		dial2.getRootPane().getActionMap().put("ESCAPE", escapeAction);
		dial2.getRootPane().setFocusable(true);
		dial2.setResizable(true);
		dial2.setTitle(model.getTitle());
		dial2.setModal(true);
		// dial2.setUndecorated(true);

		dial2.pack();
		// dial2.show();
		return dial2;
	}

	public Object showInDialog(final Component friend) {
		lastSelectedObject = null;
		lookupField.requestFocusInWindow();

		final Point p = friend.getLocationOnScreen();
		p.setLocation(p.getX() + friend.getSize().getWidth(), p.getY());
		getCurrentDialog().setLocation(p);
		textHasChanged();
		getCurrentDialog().setVisible(true);

		return lastSelectedObject;
	}

	public Dialog getCurrentDialog() {
		return currentDialog;
	}

	public void setCurrentDialog(Dialog dialog) {
		this.currentDialog = dialog;
	}

	public void updateData() {
		model.getDataModel().addAll(model.getDatas());
	}

	//    private class TabListCellRenderer extends JLabel implements ListCellRenderer {
	//        protected final Border m_noFocusBorder = new EmptyBorder(1, 1, 1, 1);
	//
	//        protected FontMetrics m_fm = null;
	////        private final int nbCols;
	//        
	//        
	//
	//        public TabListCellRenderer() {
	//            super();
	////            nbCols = aNbCols;
	//            setOpaque(true);
	//            setBorder(m_noFocusBorder);
	//        }
	//
	//        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
	//            setText(value.toString());
	//
	//            setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
	//            setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
	//
	//            setFont(list.getFont());
	//            setBorder((cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder") : m_noFocusBorder);
	//
	//            return this;
	//        }
	//
	//        public void paint(Graphics g) {
	//            m_fm = g.getFontMetrics();
	//
	//            g.setColor(getBackground());
	//            g.fillRect(0, 0, getWidth(), getHeight());
	//            getBorder().paintBorder(this, g, 0, 0, getWidth(), getHeight());
	//
	//            
	//            
	//            g.setColor(getForeground());
	//            g.setFont(getFont());
	//            Insets insets = getInsets();
	//            int x = insets.left;
	//            int y = insets.top + m_fm.getAscent();
	//
	//            StringTokenizer st = new StringTokenizer(getText(), "\t");
	//            
	//            while (st.hasMoreTokens()) {
	//                String str = st.nextToken();
	//                g.drawString(str, x, y);
	//
	//                // insert distance for each tab
	//                x += m_fm.stringWidth(str) + 50;
	//
	//                if (!st.hasMoreTokens())
	//                    break;
	//            }
	//        }
	//
	//    }

	//    
	// public static void main(String s[]) {
	// final JFrame frame = new JFrame();
	// final ZLookupPanel panel;
	// final Dialog dialog;
	// final JButton button = new JButton();
	// final DefaultLookupPanelModel defaultLookupPanelModel = new
	// DefaultLookupPanelModel();
	//
	// panel = new ZLookupPanel(defaultLookupPanelModel, null);
	//        
	// defaultLookupPanelModel.getDataModel().add("Ted");
	// defaultLookupPanelModel.getDataModel().add("Rodeur");
	// defaultLookupPanelModel.getDataModel().add("Geo");
	// defaultLookupPanelModel.getDataModel().add("Guwann");
	// defaultLookupPanelModel.getDataModel().add("Zorck");
	//        
	// dialog = panel.CreateDialog(frame);
	//
	// JPanel panel2 = new JPanel(new FlowLayout());
	//
	// final Action a = new AbstractAction("Clicmi") {
	//
	// public void actionPerformed(ActionEvent e) {
	// panel.showInDialog(button);
	// }
	//
	// };
	//
	// button.setAction(a);
	//
	// button.setPreferredSize(new Dimension(80, 20));
	// panel2.add(button);
	// panel2.add(new JPanel());
	//
	// frame.setContentPane(panel2);
	// frame.addWindowListener(new WindowAdapter() {
	// public void windowClosing(WindowEvent e) {
	// System.exit(0);
	// }
	// });
	// frame.pack();
	// frame.setVisible(true);
	// }
}
