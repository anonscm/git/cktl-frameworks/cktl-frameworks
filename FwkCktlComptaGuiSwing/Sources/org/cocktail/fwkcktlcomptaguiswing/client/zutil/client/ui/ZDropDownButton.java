/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZDropDownButton extends JButton {

    private JPopupMenu menu=new JPopupMenu();
    
    /**
     * 
     */
    public ZDropDownButton() {
        super();
        initSubObjects();
    }

    /**
     * @param text
     */
    public ZDropDownButton(String text) {
        super(text);
        initSubObjects();
    }

    /**
     * @param a
     */
    public ZDropDownButton(Action a) {
        super(a);
        setHorizontalTextPosition(SwingConstants.LEADING);
        setHorizontalAlignment(SwingConstants.LEFT);
        initSubObjects();
    }

    /**
     * @param icon
     */
    public ZDropDownButton(Icon icon) {
        super(icon);
        initSubObjects();
    }

    /**
     * @param text
     * @param icon
     */
    public ZDropDownButton(String text, Icon icon) {
        super(text, icon);
        setHorizontalTextPosition(SwingConstants.LEADING);
        setHorizontalAlignment(SwingConstants.LEFT);
        initSubObjects();
    }

    
    public void addMenuItem(JMenuItem menuItem) {
        menu.add(menuItem);
    }
    
    public void addActionItem(Action action) {
        menu.add(new JMenuItem(action));
    }
    
    public void addActions(Collection actions) {
        for (Iterator iter = actions.iterator(); iter.hasNext();) {
            final Action element = (Action) iter.next();
            final JMenuItem item = new JMenuItem(element);
            item.setToolTipText(null);
            addMenuItem(item);
        }
    }
    
    public JPopupMenu getMenu() {
        return menu;
    }
    
    public void setPopupMenu(JPopupMenu popupMenu) {
        menu = popupMenu;
    }
    private final void initSubObjects() {
        addActionListener(new MyActionListener());
    }
    
    private final class MyActionListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            menu.show(ZDropDownButton.this,0, ZDropDownButton.this.getHeight());
        }
    }
}
