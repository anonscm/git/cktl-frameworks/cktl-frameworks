/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;



import javax.swing.JRadioButton;
import javax.swing.JToggleButton;

/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZRadioButton extends JRadioButton {
    


	public ZRadioButton(Object obj, String libelle) {
		super();
		this.setName(libelle);
		this.setToolTipText(libelle);
		this.setModel( new ZRadioButtonModel(obj, libelle)  );
		updateData();
	}


	
	
	public void updateData() {
	    setText(((ZRadioButtonModel)getModel()).getLibelle());
	}



	public class ZRadioButtonModel extends JToggleButton.ToggleButtonModel {
		private Object associatedObject;
		private String _libelle;
		
		
		public ZRadioButtonModel(Object obj, String libelle) {
			setAssociatedObject(obj);
			_libelle=libelle;
		}
		
		/**
		 * @return
		 */
		public Object getAssociatedObject() {
			return associatedObject;
		}

		/**
		 * @param object
		 */
		public void setAssociatedObject(Object object) {
			associatedObject = object;
		}		
		
        public String getLibelle() {
            return _libelle;
        }
        public void setLibelle(String libelle) {
            this._libelle = libelle;
        }
	}


}
