/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.text.JTextComponent;

import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZDatePickerPanel;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZDatePickerField extends ZTextField {
	//	private TimeZone myTimeZone = TimeZone.getTimeZone("GMT");
	private TimeZone myTimeZone = TimeZone.getDefault();
	private JDialog datePickerDialog;
	//	private Dialog myParentDialog;
	private Action actionDateSelect;

	private final IZDatePickerFieldModel datePickerFieldModel;

	/**
	 * @param provider
	 */
	public ZDatePickerField(IZDatePickerFieldModel provider, DateFormat format, TimeZone aTimeZone, ImageIcon icon) {
		super(provider);
		setFormat(format);
		if (aTimeZone != null) {
			myTimeZone = aTimeZone;
		}
		((DateFormat) getFormat()).setTimeZone(myTimeZone);
		this.add(buildButton(icon));
		datePickerFieldModel = provider;
		getMyTexfield().addFocusListener(new ZDateFieldListener());
		if (format != null) {
			final String s = format.format(ZDateUtil.nowAsDate());
			getMyTexfield().setColumns(s.length());
		}

	}

	private void onButtonClick() {
		showDatePickerPanel(datePickerFieldModel.getParentWindow());
	}

	private JButton buildButton(ImageIcon icon) {
		actionDateSelect = new AbstractAction("...") {
			public void actionPerformed(ActionEvent e) {
				onButtonClick();
			}
		};
		actionDateSelect.putValue(AbstractAction.SMALL_ICON, icon);
		actionDateSelect.putValue(AbstractAction.SHORT_DESCRIPTION, "Afficher le calendrier");
		JButton res = new JButton(actionDateSelect);
		res.setHorizontalAlignment(SwingConstants.CENTER);
		res.setText(null);
		res.setPreferredSize(new Dimension(icon.getIconWidth() + 2, getMyTexfield().getPreferredSize().height));
		res.setMinimumSize(res.getPreferredSize());
		res.setMaximumSize(res.getPreferredSize());
		res.setFocusPainted(false);

		return res;
	}

	/**
	 * Affiche un datePicker
	 * 
	 * @param parentWindow
	 * @param dateText
	 */
	private void showDatePickerPanel(Window parentWindow) {
		Date ladate = null;
		if ((getMyTexfield().getText() != null) && ((getMyTexfield().getText().length() > 0))) {
			try {
				ladate = ((DateFormat) getFormat()).parse(getMyTexfield().getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
		}

		ZDatePickerPanel datePickerPanel = new ZDatePickerPanel();
		ComponentAdapter myComponentAdapter = new ComponentAdapter() {
			public void componentHidden(final ComponentEvent evt) {
				final Date dt = ((ZDatePickerPanel) evt.getSource()).getDate();
				if (null != dt) {
					getMyTexfield().setText(getFormat().format(dt));
				}
				datePickerDialog.dispose();
			}
		};
		datePickerPanel.addComponentListener(myComponentAdapter);

		datePickerPanel.open(ladate);
		final Point p = getMyTexfield().getLocationOnScreen();
		p.setLocation(p.getX(), p.getY() - 1 + getMyTexfield().getSize().getHeight());
		if (parentWindow instanceof JDialog) {
			datePickerDialog = new JDialog((JDialog) parentWindow, true);
		}
		else {
			datePickerDialog = new JDialog((JFrame) parentWindow, true);
		}

		//Ajouter la gestion de la touche echap pour fermer la fenetre		
		KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
		Action escapeAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				datePickerDialog.setVisible(false);
			}
		};
		datePickerDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
		datePickerDialog.getRootPane().getActionMap().put("ESCAPE", escapeAction);
		datePickerDialog.getRootPane().setFocusable(true);

		datePickerDialog.setResizable(false);
		datePickerDialog.setUndecorated(true);
		datePickerDialog.getContentPane().add(datePickerPanel);
		datePickerDialog.setLocation(p);
		datePickerDialog.pack();
		datePickerDialog.setVisible(true);
	}

	private class ZDateFieldListener implements FocusListener {
		public void focusGained(FocusEvent e) {
			((JTextComponent) e.getComponent()).selectAll();
		}

		/**
		 * Methode appelee lorsque le focus quitte le champ. Elle est implementee pour realiser le controle de la saisie.
		 */
		public void focusLost(FocusEvent e) {
			dateStringControl((JTextComponent) e.getComponent());
		}

		private void dateStringControl(JTextComponent textField) {
			String dateEntree, dateResult;
			// Si l'utilisateur n'a rien saisi
			if (textField.getText().equals(""))
				return;
			// Controle de la date saisie
			dateEntree = textField.getText();
			dateResult = ZDateUtil.dateCompletion(dateEntree);
			if (dateResult == null) {
				textField.requestFocus();
				textField.selectAll();
			}
			else {
				if (!dateResult.equals(textField.getText())) {
					textField.setText(dateResult);
				}
			}
		}
	}

	public interface IZDatePickerFieldModel extends IZTextFieldModel {
		public Window getParentWindow();
	}

	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		actionDateSelect.setEnabled(enabled);
	}

}
