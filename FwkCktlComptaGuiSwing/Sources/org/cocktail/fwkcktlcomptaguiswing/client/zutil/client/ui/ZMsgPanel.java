/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.text.html.HTMLEditorKit;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZStringUtil;

public class ZMsgPanel extends ZAbstractSwingPanel {

	private final String CSS_DEFAULT_STYLE = "body, table, td, th, dd, dt {font-family: " + getFont().getName() + ", Helvetica, Arial, sans-serif;font-size: 12pt;}";

	private static final Dimension BTSIZE = new Dimension(95, 24);
	private static final Dimension PANEL_SIZE = new Dimension(450, 210);

	public static final int MR_NONE = 0;
	public static final int MR_OK = 1;
	public static final int MR_YES = 2;
	public static final int MR_CANCEL = 3;
	public static final int MR_NO = 4;

	public final static String BTLABEL_YES = "Oui";
	public final static String BTLABEL_NO = "Non";
	public final static String BTLABEL_CANCEL = "Annuler";
	public final static String BTLABEL_OK = "Ok";

	private List<Action> actions = new ArrayList(3);
	private List buttons = new ArrayList(3);

	protected int _mrResult = MR_NONE;
	protected JDialog dial;

	private final JPanel jPanelLeft;
	private final JLabel jLabelImg;
	private final JPanel jPanelBottom;
	private final JEditorPane jTextPane = new JEditorPane();

	private final static HTMLEditorKit hEditorKit = new HTMLEditorKit();

	private final Action ACTIONOK = new AbstractAction(BTLABEL_OK) {
		public void actionPerformed(ActionEvent e) {
			_mrResult = MR_OK;
			dial.setVisible(false);
		}
	};

	private final Action ACTIONCANCEL = new AbstractAction(BTLABEL_CANCEL) {
		public void actionPerformed(ActionEvent e) {
			_mrResult = MR_CANCEL;
			dial.setVisible(false);
		}
	};

	private final Action ACTIONYES = new AbstractAction(BTLABEL_YES) {
		public void actionPerformed(ActionEvent e) {
			_mrResult = MR_YES;
			dial.setVisible(false);
		}
	};
	private final Action ACTIONNO = new AbstractAction(BTLABEL_NO) {
		public void actionPerformed(ActionEvent e) {
			_mrResult = MR_NO;
			dial.setVisible(false);
		}
	};

	private JPanel jPanelRight = new JPanel(new BorderLayout());;
	private JScrollPane jPanelText;

	/**
	 * This method initializes
	 */
	public ZMsgPanel() {
		super();
		jLabelImg = new JLabel();
		jPanelLeft = new JPanel(new BorderLayout());
		jPanelLeft.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
		jPanelLeft.add(jLabelImg, java.awt.BorderLayout.CENTER);
		jPanelText = new JScrollPane(jTextPane);
		jPanelText.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		jTextPane.setFocusable(true);
		jTextPane.setEditable(false);
		jTextPane.setEditorKit(hEditorKit);
		hEditorKit.getStyleSheet().addRule(CSS_DEFAULT_STYLE);

		jPanelBottom = new JPanel(new FlowLayout());
		jPanelBottom.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		this.setLayout(new BorderLayout());
		this.setSize(new java.awt.Dimension(431, 220));
		this.add(getJPanelLeft(), java.awt.BorderLayout.WEST);
		this.add(getJPanelBottom(), java.awt.BorderLayout.SOUTH);
		this.add(getJTextPane(), java.awt.BorderLayout.CENTER);
		this.add(getJPanelRight(), java.awt.BorderLayout.EAST);
		setPreferredSize(PANEL_SIZE);
	}

	private JPanel getJPanelLeft() {

		return jPanelLeft;
	}

	private JPanel getJPanelBottom() {
		return jPanelBottom;
	}

	private JScrollPane getJTextPane() {

		return jPanelText;
	}

	protected void buildButtons() {
		buttons = ZUiUtil.getButtonListFromActionList(actions, BTSIZE.width, BTSIZE.height);
		for (Iterator iter = buttons.iterator(); iter.hasNext();) {
			final JButton element = (JButton) iter.next();
			element.setHorizontalAlignment(JButton.CENTER);
		}
		jPanelBottom.add(ZUiUtil.buildGridLine(buttons));
	}

	public static final void showDialogOk(final Window parentWindow, final String title, final String msg, final Icon icon) {
		final ZMsgPanel msgPanel = new ZMsgPanel();
		msgPanel.jTextPane.setText(msg != null ? msg.replaceAll("\n", "<br>") : null);
		//        msgPanel.jTextPane.setFocusable(false);
		msgPanel.jLabelImg.setIcon(icon);
		msgPanel.actions.add(msgPanel.ACTIONOK);
		msgPanel.buildButtons();
		msgPanel.dial = ZUiUtil.createDialog(parentWindow, title, true, msgPanel);
		msgPanel.dial.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(0));
		ZUiUtil.centerWindow(msgPanel.dial);
		InitialFocusSetter.setInitialFocus(msgPanel.dial, msgPanel.dial.getRootPane().getDefaultButton());
		msgPanel.dial.setVisible(true);
	}

	public static final void showDialogOkExt(final Window parentWindow, final String title, final String msg, final Icon icon, final String extMsg, final Icon copyIcon) {
		final ZMsgPanel msgPanel = new ZMsgPanel();

		//        if (msg != null && msg.startsWith("<html>")) {
		//            msgPanel.jTextPane.setEditorKit(hEditorKit);
		//        }

		msgPanel.jTextPane.setText(msg != null ? msg.replaceAll("\n", "<br>") : null);
		//        msgPanel.jTextPane.setFocusable(false);
		msgPanel.jLabelImg.setIcon(icon);

		msgPanel.actions.add(msgPanel.ACTIONOK);
		msgPanel.buildButtons();

		msgPanel.getJPanelRight().setLayout(new BorderLayout());
		final Action actionCopy = new AbstractAction() {
			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(final ActionEvent e) {
				ZStringUtil.copyToClipboard(extMsg);
			}
		};
		actionCopy.putValue(AbstractAction.SMALL_ICON, copyIcon);
		actionCopy.putValue(AbstractAction.SHORT_DESCRIPTION, "Copier le message affiché dans le presse-papiers.\n " +
				"Vous pouvez ainsi le coller dans une fenètre de message e-mail.");
		msgPanel.getJPanelRight().add(ZUiUtil.getButtonFromAction(actionCopy, 50, 26), BorderLayout.NORTH);
		msgPanel.getJPanelRight().add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		msgPanel.getJPanelRight().setBorder(BorderFactory.createEmptyBorder(5, 4, 5, 4));

		msgPanel.dial = ZUiUtil.createDialog(parentWindow, title, true, msgPanel);
		msgPanel.dial.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(0));
		ZUiUtil.centerWindow(msgPanel.dial);
		InitialFocusSetter.setInitialFocus(msgPanel.dial, msgPanel.dial.getRootPane().getDefaultButton());
		//        ZLogger.verbose("ici=" + msgPanel.dial);

		msgPanel.dial.setVisible(true);
	}

	public static final boolean showDialogOkCancel(final Window parentWindow, final String title, final String msg, final Icon icon) {
		final ZMsgPanel msgPanel = new ZMsgPanel();
		msgPanel.jTextPane.setText(msg != null ? msg.replaceAll("\n", "<br>") : null);
		//        msgPanel.jTextPane.setFocusable(false);
		msgPanel.jLabelImg.setIcon(icon);
		msgPanel.actions.add(msgPanel.ACTIONOK);
		msgPanel.actions.add(msgPanel.ACTIONCANCEL);
		msgPanel.buildButtons();
		msgPanel.dial = ZUiUtil.createDialog(parentWindow, title, true, msgPanel);
		msgPanel.dial.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		ZUiUtil.centerWindow(msgPanel.dial);
		InitialFocusSetter.setInitialFocus(msgPanel.dial, msgPanel.dial.getRootPane().getDefaultButton());
		msgPanel.dial.setVisible(true);
		return msgPanel._mrResult == MR_OK;
	}

	public static final int showDialogYesNo(final Window parentWindow, final String title, final String msg, final Icon icon, final String defaultRep) {
		final ZMsgPanel msgPanel = new ZMsgPanel();
		msgPanel.jTextPane.setText(msg != null ? msg.replaceAll("\n", "<br>") : null);
		//        msgPanel.jTextPane.setFocusable(false);
		msgPanel.jLabelImg.setIcon(icon);
		msgPanel.actions.add(msgPanel.ACTIONYES);
		msgPanel.actions.add(msgPanel.ACTIONNO);
		msgPanel.buildButtons();
		msgPanel.dial = ZUiUtil.createDialog(parentWindow, title, true, msgPanel);
		msgPanel.dial.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		if (defaultRep != null) {
			if (BTLABEL_YES.equals(defaultRep)) {
				msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(0));

			}
			else if (BTLABEL_NO.equals(defaultRep)) {
				msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(1));
			}
		}
		ZUiUtil.centerWindow(msgPanel.dial);
		InitialFocusSetter.setInitialFocus(msgPanel.dial, msgPanel.dial.getRootPane().getDefaultButton());
		msgPanel.dial.setVisible(true);
		return msgPanel._mrResult;
	}

	/**
	 * Affiche une boite de dialogue avec boutons Oui, Non, Annuler
	 *
	 * @param parentWindow
	 * @param title
	 * @param msg
	 * @param icon
	 * @param defaultRep
	 * @return
	 */
	public static final int showDialogYesNoCancel(final Window parentWindow, final String title, final String msg, final Icon icon, final String defaultRep) {
		final ZMsgPanel msgPanel = new ZMsgPanel();
		msgPanel.jTextPane.setText(msg != null ? msg.replaceAll("\n", "<br>") : null);
		//        msgPanel.jTextPane.setFocusable(false);
		msgPanel.jLabelImg.setIcon(icon);
		msgPanel.actions.add(msgPanel.ACTIONYES);
		msgPanel.actions.add(msgPanel.ACTIONNO);
		msgPanel.actions.add(msgPanel.ACTIONCANCEL);
		msgPanel.buildButtons();
		msgPanel.dial = ZUiUtil.createDialog(parentWindow, title, true, msgPanel);
		msgPanel.dial.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		if (defaultRep != null) {
			if (BTLABEL_YES.equals(defaultRep)) {
				msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(0));
			}
			else if (BTLABEL_NO.equals(defaultRep)) {
				msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(1));
			}
			else if (BTLABEL_CANCEL.equals(defaultRep)) {
				msgPanel.dial.getRootPane().setDefaultButton((JButton) msgPanel.buttons.get(2));
			}
		}
		ZUiUtil.centerWindow(msgPanel.dial);
		InitialFocusSetter.setInitialFocus(msgPanel.dial, msgPanel.dial.getRootPane().getDefaultButton());
		msgPanel.dial.setVisible(true);
		return msgPanel._mrResult;
	}

	private JPanel getJPanelRight() {
		return jPanelRight;
	}

	private static class InitialFocusSetter {
		public static void setInitialFocus(Window w, Component c) {
			w.addWindowListener(new FocusSetter(c));
		}

		public static class FocusSetter extends WindowAdapter {
			final Component initComp;

			FocusSetter(Component c) {
				initComp = c;
			}

			public void windowOpened(WindowEvent e) {
				initComp.requestFocusInWindow();
				e.getWindow().removeWindowListener(this);
			}
		}
	}

	public void updateData() throws Exception {

	}
}
