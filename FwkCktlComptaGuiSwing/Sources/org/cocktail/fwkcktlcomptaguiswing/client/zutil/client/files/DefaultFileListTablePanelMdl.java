package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files.FileListTablePanel.IFileListTablePanelMdl;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class DefaultFileListTablePanelMdl implements IFileListTablePanelMdl {
	protected final String FILE_NAME_KEY = IFileListTablePanelMdl.NOM_FICHIER_KEY;
	protected final String FILE_KEY = IFileListTablePanelMdl.FICHIER_KEY;
	private final EOSortOrdering SORT_FILE_NAME = EOSortOrdering.sortOrderingWithKey(FILE_NAME_KEY, EOSortOrdering.CompareAscending);
	private final NSMutableArray files = new NSMutableArray();
	private Map filePreparationMap = new HashMap();

	public NSArray getData() throws Exception {
		return files;
	}

	public void onDbClick() {

	}

	public void selectionChanged() {

	}

	public void clear() {
		files.removeAllObjects();
		filePreparationMap.clear();
	}

	public NSMutableDictionary addFile(File file) {
		NSMutableDictionary dic = new NSMutableDictionary();
		dic.takeValueForKey(file.getName(), FILE_NAME_KEY);
		dic.takeValueForKey(file, FILE_KEY);
		files.addObject(dic);
		sort();
		return dic;
	}

	public void sort() {
		EOSortOrdering.sortArrayUsingKeyOrderArray(files, new NSArray(new Object[] {
				SORT_FILE_NAME
		}));
	}

	public NSMutableArray getFiles() {
		return (NSMutableArray) files.valueForKey(FILE_KEY);
	}

	public Map getFilePreparationMap() {
		return filePreparationMap;
	}
}