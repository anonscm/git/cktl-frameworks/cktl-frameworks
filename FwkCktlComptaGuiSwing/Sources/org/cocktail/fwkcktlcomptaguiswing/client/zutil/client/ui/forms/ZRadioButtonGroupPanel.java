/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.IZDataComponent;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZRadioButtonGroupPanel extends JPanel implements IZDataComponent, ItemListener {
	private IZRadioButtonGroupModel myModel;
	private ButtonGroup myButtonGroup;
	private HashMap buttonsValues;

	public ZRadioButtonGroupPanel(IZRadioButtonGroupModel _model) {
		super();
		myModel = _model;
		buttonsValues = new HashMap();
		removeAll();
		setLayout(new FlowLayout());

		myButtonGroup = new ButtonGroup();

		final Object[] objs = myModel.getValues().keySet().toArray();
		for (int i = 0; i < objs.length; i++) {
			final Object object = objs[i];
			final JRadioButton bt = buildButtonForValue(object);
			//            bt.addActionListener(this);
			bt.addItemListener(this);
			myButtonGroup.add(bt);
			buttonsValues.put(myModel.getValues().get(object), bt);
			add(bt);
		}
	}

	public void updateData() {
		if (myModel.getValue() != null) {
			if (((JRadioButton) buttonsValues.get(myModel.getValue())) != null) {
				((JRadioButton) buttonsValues.get(myModel.getValue())).setSelected(true);
			}
		}
	}

	/**
	 * @param Value
	 * @return
	 */
	protected JRadioButton buildButtonForValue(Object Value) {
		String s = getValueToDisplay(Value);
		JRadioButton b = new JRadioButton(s);
		b.setActionCommand(s);
		return b;
	}

	private String getValueToDisplay(Object value) {
		if (value == null) {
			return null;
		}
		return value.toString();
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	//    public void actionPerformed(ActionEvent e) {
	//        final Object obj = myModel.getValues().get(e.getActionCommand());
	//        myModel.setValue( obj );
	//    }    

	public interface IZRadioButtonGroupModel {
		/**
		 * @return En cle une chaine a afficher, en valeurs l'objet represente par le bouton.
		 */
		public HashMap getValues();

		public Object getValue();

		public void setValue(Object value);

	}

	/**
	 * @see javax.swing.JComponent#setEnabled(boolean)
	 */
	public void setEnabled(boolean enabled) {

		Iterator iter = buttonsValues.values().iterator();
		while (iter.hasNext()) {
			JRadioButton element = (JRadioButton) iter.next();
			element.setEnabled(enabled);
		}
		super.setEnabled(enabled);
	}

	public ButtonGroup getMyButtonGroup() {
		return myButtonGroup;
	}

	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			myModel.setValue(myModel.getValues().get(((AbstractButton) e.getSource()).getText()));
		}
	}

}
