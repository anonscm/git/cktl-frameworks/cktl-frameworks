/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZTextFileViewerPanel extends ZAbstractSwingPanel {
    private JTextArea textArea;
    private IZTextFileViewerPanelModel model;
    private JToolBar toolbar;


    /**
     *
     */
    public ZTextFileViewerPanel(IZTextFileViewerPanelModel aModel) {
        super();
        model = aModel;
    }


    public void initUI() {
        setLayout(new BorderLayout());
        textArea = new JTextArea();
        toolbar = new JToolBar();
        Iterator iter = model.getActionList().iterator();
        while (iter.hasNext()) {
            Action element = (Action) iter.next();
            toolbar.add(element);
        }
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(0,4,0,4));
        p.add(new JScrollPane(textArea), BorderLayout.CENTER);
        add(p, BorderLayout.CENTER);
        add(toolbar, BorderLayout.NORTH);
   }

    public void updateData() {
        textArea.setText(model.stringBuffer().toString());
    }



    public interface IZTextFileViewerPanelModel {
        public StringBuffer stringBuffer();
        public ArrayList getActionList();
    }





    public JTextArea getTextArea() {
        return textArea;
    }



}
