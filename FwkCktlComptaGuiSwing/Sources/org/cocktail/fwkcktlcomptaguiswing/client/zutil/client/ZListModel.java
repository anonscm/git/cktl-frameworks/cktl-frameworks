/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

import javax.swing.AbstractListModel;

/**
 * Modele de liste, utile quand on a besoin de filtrer la liste.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZListModel extends AbstractListModel {
    private final ArrayList fullList;
    private final ArrayList filteredList;
    String filterText;

    public ZListModel() {
        fullList = new ArrayList();
        filteredList = new ArrayList();
    }

    public ZListModel(final int initialCapacity) {
        fullList = new ArrayList(initialCapacity);
        filteredList = new ArrayList(initialCapacity);
    }

    protected void updateFilteredList() {            
//        System.out.println("ZListModel.updateFilteredList()");
        filteredList.clear();
        final String _filter = getFilterText();
        if (_filter == null || _filter.length() == 0) {
            filteredList.addAll(fullList);
        } else {
            final Iterator iter = fullList.iterator();
            while (iter.hasNext()) {
                final Object element =  iter.next();
                if (isElementMatchesFilter(element, _filter)) {
                    filteredList.add(element);
//                    System.out.println("ZListModel.updateFilteredList() - dans la liste");
                }
            }
        }
        fireContentsChanged(this, 0, filteredList.size() - 1);
    }

    protected boolean isElementMatchesFilter(final Object element, final String _filter) {
        if (_filter == null || _filter.length() == 0) {
            return true;
        }
        final String filtre = getFilterText().toUpperCase();
        final String listElt = ((String)element).toUpperCase();
        final String regFiltre = ".*" + filtre + ".*";
        return (element != null && (listElt.startsWith(filtre) || listElt.matches(regFiltre)) );
    }

    public void add(int index, Object element) {
        fullList.add(index, element);
        updateFilteredList();
    }

    public boolean add(Object o) {
        boolean res = fullList.add(o);
        updateFilteredList();
        return res;
    }

    public boolean addAll(Collection c) {
        boolean res = fullList.addAll(c);
        updateFilteredList();
        return res;
    }

    public boolean addAll(int index, Collection c) {
        boolean res = fullList.addAll(index, c);
        updateFilteredList();
        return res;
    }

    public void clear() {
        fullList.clear();
        filteredList.clear();
    }

    public int indexOf(Object elem) {
        return filteredList.indexOf(elem);
    }

    public ListIterator listIterator() {
        return filteredList.listIterator();
    }

    public Object remove(int index) {
        final Object obj = fullList.remove(index);
        updateFilteredList();
        return obj;
    }

    public boolean remove(Object o) {
        boolean res = fullList.remove(o);
        updateFilteredList();
        return res;
    }

    public boolean removeAll(Collection c) {
        boolean res = fullList.removeAll(c);
        updateFilteredList();
        return res;
    }

    public int size() {
        return filteredList.size();
    }

    public int getSize() {
        return filteredList.size();
    }

    public Object getElementAt(int index) {
//        System.out.println("ZListModel.getElementAt()");
        try {
            return filteredList.get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
    
    
    

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
        updateFilteredList();
    }
    
    
    public void updateData(Collection datas) {
        clear();
        addAll(datas);
    }

}
