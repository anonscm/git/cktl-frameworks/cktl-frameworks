/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.TableSorter;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZCommonDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZActionField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZEOSrchDialog extends ZCommonDialog implements ZEOTable.ZEOTableListener {
	private TableSorter myTableSorter;
	//	protected	ApplicationClient	myApp;
	//	protected int modalResult;
	//	private Window upperWindow;
	protected ZEOTableModel myTableModel;
	protected ZEOTable myEOTable;
	private String topMessage;
	protected ZActionField filterTextField;
	private EODisplayGroup _myDg;
	private Vector _myCols;
	private AbstractAction srchAction;
	protected JLabel tooltipIcon;

	/**
	 * @param win Dialog parent.
	 * @param title Titre à afficher dans la barre de titre
	 * @param dg DisplayGroup contenant les objets métiers
	 * @param cols Vector contenant des objets ZEOTableModelColumn
	 * @param message Message texte à afficher en haut de la fenêtre de sélection
	 */
	public ZEOSrchDialog(Dialog win, String title, EODisplayGroup dg, Vector cols, String message, AbstractAction action, EOEditingContext ec) {
		super(win, title, true);
		_myDg = dg;
		_myCols = cols;
		setModal(true);
		//		upperWindow = win;
		//		myApp = (ApplicationClient)EOApplication.sharedApplication();
		srchAction = action;
		topMessage = message;
		initGUI();
	}

	public ZEOSrchDialog(Frame win, String title, EODisplayGroup dg, Vector cols, String message, AbstractAction action) {
		super(win, title, true);
		_myDg = dg;
		_myCols = cols;
		setModal(true);
		//		upperWindow = win;
		//		myApp = (ApplicationClient)EOApplication.sharedApplication();
		srchAction = action;
		topMessage = message;
		initGUI();
	}

	protected ZEOSrchDialog(Frame win, String title) {
		super(win, title, true);
		//		myApp = (ApplicationClient)EOApplication.sharedApplication();
		//		upperWindow = win;
	}

	protected ZEOSrchDialog(Dialog win, String title) {
		super(win, title, true);
		//		myApp = (ApplicationClient)EOApplication.sharedApplication();
		//		upperWindow = win;
	}

	protected void initDialog(EODisplayGroup dg, Vector cols, String message, AbstractAction action) {
		_myDg = dg;
		_myCols = cols;
		setModal(true);

		//		myApp = (ApplicationClient)EOApplication.sharedApplication();
		srchAction = action;
		topMessage = message;
		initGUI();
	}

	private void initGUI() {
		initTableModel();
		initTable();
		JPanel mainPanel = new JPanel();
		JPanel tmpPanel;
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		tmpPanel = buildTopPanel();
		if (tmpPanel != null) {
			mainPanel.add(tmpPanel, BorderLayout.PAGE_START);
		}
		tmpPanel = buildRightPanel();
		if (tmpPanel != null) {
			mainPanel.add(tmpPanel, BorderLayout.LINE_END);
		}
		tmpPanel = buildBottomPanel();
		if (tmpPanel != null) {
			mainPanel.add(tmpPanel, BorderLayout.PAGE_END);
		}
		tmpPanel = buildCenterPanel();
		if (tmpPanel != null) {
			mainPanel.add(tmpPanel, BorderLayout.CENTER);
		}
		this.setContentPane(mainPanel);
		this.pack();
		this.validate();
	}

	protected void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

	}

	protected void initTableModel() {
		myTableModel = new ZEOTableModel(_myDg, _myCols);
		myTableSorter = new TableSorter(myTableModel);
	}

	protected JPanel buildTitle() {
		if (topMessage != null) {
			JPanel title = new JPanel(new BorderLayout());
			title.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			JLabel msgLabel = new JLabel(topMessage);
			title.add(msgLabel, BorderLayout.LINE_START);
			title.add(new JPanel());
			return title;
		}
		return null;
	}

	protected JPanel buildFilterBox() {
		if (srchAction != null) {
			JPanel filterPanel = new JPanel();
			filterPanel.setLayout(new BorderLayout());
			filterPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			filterTextField = new ZActionField(srchAction);
			//			filterTextField = new ZLabelActionField("Rechercher", srchAction);
			filterTextField.getMyTexfield().setColumns(40);
			filterPanel.add(filterTextField, BorderLayout.LINE_START);
			filterPanel.add(new JPanel(), BorderLayout.CENTER);
			return filterPanel;
		}
		return null;
	}

	protected JPanel buildButtonsPanel() {
		Action actionOk = new AbstractAction("Ok") {
			public void actionPerformed(ActionEvent e) {
				onOkClick();
			}
		};
		actionOk.putValue(AbstractAction.SMALL_ICON, getImageIconOk());

		Action actionCancel = new AbstractAction("Annuler") {
			public void actionPerformed(ActionEvent e) {
				onCancelClick();
			}
		};
		actionCancel.putValue(AbstractAction.SMALL_ICON, getImageIconCancel());

		JButton btOk = new JButton(actionOk);
		JButton btCancel = new JButton(actionCancel);
		Dimension btSize = new Dimension(95, 24);
		btOk.setMinimumSize(btSize);
		btCancel.setMinimumSize(btSize);
		btOk.setPreferredSize(btSize);
		btCancel.setPreferredSize(btSize);

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(btOk);
		buttonPanel.add(btCancel);
		return buttonPanel;
	}

	/**
	 * Construit le panel du haut (titre, boite de sélection)
	 * 
	 * @return
	 */
	protected JPanel buildTopPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		Box box = Box.createVerticalBox();

		JPanel tmpPanel1 = buildTitle();
		if (tmpPanel1 != null) {
			Box box1 = Box.createHorizontalBox();
			box1.add(tmpPanel1);
			box1.add(Box.createHorizontalGlue());
			box.add(box1);
		}

		JPanel tmpPanel2 = buildFilterBox();
		if (tmpPanel2 != null) {
			Box box1 = Box.createHorizontalBox();
			if (tooltipIcon != null) {
				box1.add(tooltipIcon);
			}
			box1.add(tmpPanel2);
			box1.add(Box.createHorizontalGlue());
			box.add(box1);
		}
		box.add(Box.createVerticalGlue());
		panel.add(box);
		return panel;
	}

	protected JPanel buildBottomPanel() {
		return buildButtonsPanel();
	}

	protected JPanel buildRightPanel() {
		return null;
	}

	protected JPanel buildCenterPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JScrollPane myJscrollPane = new JScrollPane(myEOTable);
		panel.add(myJscrollPane, BorderLayout.CENTER);
		return panel;
	}

	/**
	 * Ouvre la fenetre en modal et renvoie le résultat (via getModalResult).
	 */
	public int open() {
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setModalResult(MRCANCEL);
		centerWindow();
		setVisible(true);
		//en modal la fenetre reste active jusqu'au closeWindow...
		return getModalResult();
	}

	/**
	 *
	 */
	public int getModalResult() {
		return modalResult;
	}

	//	/**
	//	 * @param window
	//	 */
	//	public void setUpperWindow(Window window) {
	//		upperWindow = window;
	//	}

	public NSArray getSelectedEOObjects() {
		return myTableModel.getSelectedEOObjects();
	}

	public EOEnterpriseObject getSelectedEOObject() {
		return (EOEnterpriseObject) get_myDg().selectedObject();
	}

	public void setSelectionMode(int selMode) {
		myEOTable.setSelectionMode(selMode);
	}

	/**
	 * @return
	 */
	public EODisplayGroup get_myDg() {
		return _myDg;
	}

	/**
	 * @param group
	 */
	public void set_myDg(EODisplayGroup group) {
		_myDg = group;
	}

	public String getFilterText() {
		return filterTextField.getMyTexfield().getText();
	}

	public void onDbClick() {
		onOkClick();
	}

	public void onSelectionChanged() {
		return;

	}

	public final ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public abstract ImageIcon getImageIconOk();

	public abstract ImageIcon getImageIconCancel();

}
