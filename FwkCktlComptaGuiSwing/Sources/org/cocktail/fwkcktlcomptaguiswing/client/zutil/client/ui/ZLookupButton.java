/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Collection;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZListModel;

public class ZLookupButton extends JButton {

	private final IZLookupButtonModel model;
	private final IZLookupButtonListener listener;
	private ZLookupPanel panel;
	protected ZListModel listModel;

	public ZLookupButton(IZLookupButtonModel _model, IZLookupButtonListener _listener) {
		super();
		listener = _listener;
		model = _model;
		listModel = new ZListModel();
	}

	public void initGUI() {
		final MyLookupPanelModel lookupPanelModel = new MyLookupPanelModel();
		panel = new ZLookupPanel(lookupPanelModel, listener);
		panel.setPreferredSize(model.getPreferredSize());
		this.setAction(new ActionClick());
	}

	public void updateData() {
		panel.updateData();
	}

	public interface IZLookupButtonModel {
		public Collection getDatas();

		public Dimension getPreferredSize();

		public String getActionDescription();

		public ImageIcon getIcon();

		public Window getMyWindow();

		public String getTitle();
	}

	public interface IZLookupButtonListener extends ZLookupPanel.IZLookupPanelListener {
		/** Appelé lorsque l'utilisateur a sélectionné une nouvelle valeur */
		public void setNewValue(Object res);

	}

	private final class MyLookupPanelModel implements ZLookupPanel.IZLookupPanelModel {

		public ZListModel getDataModel() {
			return getListModel();
		}

		public void filter(String text) {
			getDataModel().setFilterText(text);
		}

		public Collection getDatas() {
			return model.getDatas();
		}

		public String getTitle() {
			return model.getTitle();
		}
	}

	private class ActionClick extends AbstractAction {

		public ActionClick() {
			putValue(TOOL_TIP_TEXT_KEY, model.getActionDescription());
			putValue(Action.SMALL_ICON, model.getIcon());
		}

		public void actionPerformed(ActionEvent e) {
			final Dialog dial = panel.CreateDialog(model.getMyWindow());
			final Object res = panel.showInDialog(ZLookupButton.this);
			if (res != null) {
				listener.setNewValue(res);
			}
			dial.dispose();

		}

	}

	public ZListModel getListModel() {
		return listModel;
	}

}
