/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;

public class ZImageView extends JLabel implements IZDataComponent, Scrollable {
	public static final int SCALE_MODE_NEVER = 0;
	public static final int SCALE_MODE_ALWAYS = 1;
	public static final int SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER = 2;
	private final IZImageMdl _mdl;
	private BufferedImage image;
	private int scaleMode = SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER;

	/**
	 * @param mdl
	 * @param scMode
	 *            Scale mode : SCALE_MODE_NEVER, SCALE_MODE_ALWAYS,
	 *            SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER
	 */
	public ZImageView(IZImageMdl mdl, int scMode) {
		super();
		_mdl = mdl;
		setScaleMode(scMode);
		setHorizontalAlignment(SwingConstants.CENTER);
		setVerticalAlignment(SwingConstants.CENTER);
		setFont(getFont().deriveFont((float) 15));
		setForeground(Color.RED);
		setBackground(getBackground());

	}

	public ZImageView(IZImageMdl mdl) {
		this(mdl, SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER);
	}

	public void updateData() {
		image = _mdl.getImage();
		if (image == null) {
			setPreferredSize(getSize());
			setText(_mdl.getTxtForNoImage());
		}
		else {
			setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
			setText(null);
		}
		repaint();
	}

	private void paintImageWithScaleNever(Graphics g) {
		//taille du viewport
		int width = getWidth();
		int height = getHeight();

		//taille reelle de l'image
		double imageWidth = image.getWidth();
		double imageHeight = image.getHeight();

		//position
		double x = (width - imageWidth) / 2;
		double y = (height - imageHeight) / 2;

		g.drawImage(image, (int) Math.max(Math.round(x), 0), (int) Math.max(Math.round(y), 0), null);
	}

	private void paintImageWithScaleAlways(Graphics g) {
		//taille du viewport
		int width = getWidth();
		int height = getHeight();

		//taille reelle de l'image
		double imageWidth = image.getWidth();
		double imageHeight = image.getHeight();

		//echelle
		double wScale = width / imageWidth;
		double hScale = height / imageHeight;

		//on s'adapte à la plus petite echelle
		double scale = Math.min(wScale, hScale);

		//nouvelle taille de l'image
		double newWidth = scale * imageWidth;
		double newHeight = scale * imageHeight;

		//position
		double x = (width - newWidth) / 2;
		double y = (height - newHeight) / 2;

		g.drawImage(image, (int) Math.round(x), (int) Math.round(y), (int) Math.round(newWidth), (int) Math.round(newHeight), null);
	}

	private void paintImageWithScaleWhenBigger(Graphics g) {
		//taille du viewport
		int width = getWidth();
		int height = getHeight();

		//taille reelle de l'image
		double imageWidth = image.getWidth();
		double imageHeight = image.getHeight();

		//echelle
		double wScale = width / imageWidth;
		double hScale = height / imageHeight;

		//on s'adapte à la plus petite echelle
		double scale = Math.min(wScale, hScale);

		//On n'agrandit pas les images plus petites
		if (scale > 1) {
			scale = 1;
		}
		//nouvelle taille de l'image
		double newWidth = scale * imageWidth;
		double newHeight = scale * imageHeight;

		//position
		double x = (width - newWidth) / 2;
		double y = (height - newHeight) / 2;

		g.drawImage(image, (int) Math.round(x), (int) Math.round(y), (int) Math.round(newWidth), (int) Math.round(newHeight), null);
	}

	protected void paintComponent(Graphics g) {
		if (image != null) {
			if (getScaleMode() == SCALE_MODE_NEVER) {
				paintImageWithScaleNever(g);
			}
			else if (getScaleMode() == SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER) {
				paintImageWithScaleWhenBigger(g);
			}
			else {
				paintImageWithScaleAlways(g);
			}
		}
		else {
			super.paintComponent(g);
		}
	}

	public final int getScaleMode() {
		return scaleMode;
	}

	public final void setScaleMode(int scaleMode) {
		this.scaleMode = scaleMode;
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 10;
	}

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 1;
	}

	public interface IZImageMdl {
		public BufferedImage getImage();

		public String getTxtForNoImage();
	}

}
