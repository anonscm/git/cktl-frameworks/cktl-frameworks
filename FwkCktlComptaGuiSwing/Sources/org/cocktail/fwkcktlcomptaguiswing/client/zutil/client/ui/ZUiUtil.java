/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZUiUtil {

    /**
     * Contsruit un JPanel de titre.
     *
     * @param aTitle
     * @param textColor
     * @param bgColor
     * @param leftIcon
     * @param rightIcon
     * @return
     */
    public static final JPanel buildTitlePanel(final String aTitle, final Color textColor, final Color bgColor, final ImageIcon leftIcon, final ImageIcon rightIcon) {
        return buildTitlePanel(new JLabel(aTitle), textColor, bgColor, leftIcon, rightIcon);
    }

    public static final JPanel buildTitlePanel(final JLabel aTitle, final Color textColor, final Color bgColor, final ImageIcon leftIcon, final ImageIcon rightIcon) {
        final Color tmptextColor = textColor;
        final Color tmpstartColor = bgColor;

        final JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBackground(tmpstartColor);
        mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

        final JLabel txt = aTitle;
        txt.setHorizontalAlignment(SwingConstants.LEFT);
        txt.setFont(txt.getFont().deriveFont(Font.BOLD));
        txt.setForeground(tmptextColor);

        final JPanel txtPanel = new JPanel(new BorderLayout());
        txtPanel.setBackground(mainPanel.getBackground());
        txtPanel.add(txt, BorderLayout.CENTER);
        if (leftIcon != null) {
            final JLabel l = new JLabel(leftIcon);
            l.setBorder(BorderFactory.createEmptyBorder(1, 3, 1, 5));
            txtPanel.add(l, BorderLayout.WEST);
        }
        if (rightIcon != null) {
            final JLabel l = new JLabel(rightIcon);
            l.setBorder(BorderFactory.createEmptyBorder(1, 5, 1, 3));
            txtPanel.add(new JLabel(rightIcon), BorderLayout.EAST);
        }

        //        mainPanel.setMaximumSize(new Dimension(10,50));
        mainPanel.add(txtPanel);

        final JPanel borderedPanel = new JPanel(new BorderLayout());
        borderedPanel.setBorder(BorderFactory.createLineBorder(tmpstartColor, 1));
        borderedPanel.add(mainPanel, BorderLayout.CENTER);

        return borderedPanel;
    }

    /**
     * Integre le Component dans un JPanel avec un titre.
     *
     * @param aTitle
     * @param textColor
     * @param startColor
     * @param comp
     * @param leftIcon
     * @param rightIcon
     * @return
     */
    public static final JPanel encloseInPanelWithTitle(final String aTitle, final Color textColor, final Color startColor, final Component comp, final ImageIcon leftIcon, final ImageIcon rightIcon) {
        final JPanel p = new JPanel(new BorderLayout());
        p.add(buildTitlePanel(aTitle, textColor, startColor, leftIcon, rightIcon), BorderLayout.NORTH);
        p.add(comp, BorderLayout.CENTER);
        return p;
    }

    /**
     * Construit un JPanel constitue d'une colonne de composants de meme hauteur, separes par l'espace de hauteur 20px
     *
     * @param comps La liste des Component a integrer dans le panel
     * @return un JPanel
     */
    public static final JPanel buildGridColumn(final List<? extends Component> comps) {
        return buildGridColumn(comps, 20);
    }

    /**
     * Construit un JPanel constitue d'une colonne de composants de meme hauteur, separes par l'espace de hauteur vgap.
     * @param comps La liste des Component a integrer dans le panel
     * @param vgap Separation verticale en pixels entre chaque cellules.
     * @return Le JPanel
     */
    public static final JPanel buildGridColumn(final List<? extends Component> comps, final int vgap) {
        final JPanel p = new JPanel();
        final Iterator iterator = comps.iterator();
        final GridLayout thisLayout = new GridLayout(comps.size(), 1);
        thisLayout.setVgap(vgap);
        p.setLayout(thisLayout);
        while (iterator.hasNext()) {
            final Component c = (Component) iterator.next();
            p.add(c);
        }
        return p;
    }

    /**
     * Construit un JPanel constitue d'une ligne de composants de meme largeur, separes par l'espace de largeur 20px.
     * @param comps La liste des Component a integrer dans le panel
     * @return Le JPanel
     */
    public static JPanel buildGridLine(final List<? extends Component> comps) {
        return buildGridLine(comps, 20);
    }

    /**
     * Construit un JPanel constitue d'une ligne de composants de meme largeur, separes par l'espace de largeur hgap.
     * @param comps La liste des Component a integrer dans le panel
     * @param hgap Separation horinzontale en pixels entre chaque cellules.
     * @return
     */
    public static JPanel buildGridLine(final List<? extends Component> comps, final int hgap) {
        final JPanel p = new JPanel();
        final Iterator iterator = comps.iterator();
        final GridLayout thisLayout = new GridLayout(1, comps.size());
        thisLayout.setHgap(hgap);
        p.setLayout(thisLayout);
        while (iterator.hasNext()) {
            final Component c = (Component) iterator.next();
            p.add(c);
        }
        return p;
    }

    /**
     * Construit une ligne alignee en fonction de l'orientation specifiee.
     * @param comps un array de Component
     * @param orientation Orientation ({@link BorderLayout#WEST}, {@link BorderLayout#EAST}
     * @return un JPanel
     */
    public static JComponent buildBoxLine(final Component[] comps, String orientation) {
        final Box box = Box.createHorizontalBox();
        for (int i = 0; i < comps.length; i++) {
            final Component component = comps[i];
            box.add(component);
        }

        final JPanel p = new JPanel(new BorderLayout());
        p.add(box, orientation);
        p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return p;
    }

    /**
     * Construit une Box horizontale a partir des Component passÃ©s en parametre.
     * @param comps
     * @return
     */
    public static Box buildBoxLine(final Component[] comps) {
        final Box box = Box.createHorizontalBox();
        for (Component component : comps) {
            box.add(component);
        }
        return box;
    }

    /**
     * Construit une Box horizontale Ã  partir des Component passÃ©s en parametre.
     * @param comps
     * @return
     */
    public static Box buildBoxLine(final List<? extends Component> comps) {
        final Box box = Box.createHorizontalBox();
        for (Component component : comps) {
            box.add(component);
        }
        return box;
    }

    public static JPanel buildFlowLine(final Component[] comps) {
        final JPanel p = new JPanel();
        for (int i = 0; i < comps.length; i++) {
            final Component component = comps[i];
            p.add(component);
        }
        return p;
    }

    /**
     * Construit une Box verticale Ã  partir des Component passÃ©s en parametre.
     *
     * @param comps
     * @return
     */
    public static Box buildBoxColumn(final Component[] comps) {
        final Box box = Box.createVerticalBox();
        for (int i = 0; i < comps.length; i++) {
            final Component component = comps[i];
            box.add(component);
        }
        return box;
    }

    /**
     * Construit une Box verticale à partir des composants passés en parametre.
     * @param comps
     * @return
     */
    public static Box buildBoxColumn(final List<? extends Component> comps) {
        final Box box = Box.createVerticalBox();
        for (Component component : comps) {
            box.add(component);
        }
        return box;
    }

    /**
     * Construit un formulaire à partir d'une liste de tableaux de composants (typiquement chaque tableau contient un
     * libellé et un champ et représente une ligne du formulaire)
     */
    public static JPanel buildForm(final List<Component[]> lignes) {
    	final JPanel p = new JPanel();
    	GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints constraint = new GridBagConstraints();
        p.setLayout(gridbag);
        constraint.fill = GridBagConstraints.BOTH;
        constraint.weightx = 1.0;
        constraint.insets = new Insets(4,4,4,4);
        constraint.anchor = GridBagConstraints.WEST;

        for (int i = 0; i < lignes.size(); i++) {
            final Component[] ligne = (Component[]) lignes.get(i);
            constraint.gridwidth = GridBagConstraints.RELATIVE;
            for (int j = 0; j < ligne.length; j++) {
            	final Component comp = ligne[j];
            	if (j == ligne.length-1) {
            		//on indique qu'on est à la fin de la ligne
            		constraint.gridwidth = GridBagConstraints.REMAINDER;
            	}
            	gridbag.setConstraints(comp, constraint);
            	p.add(comp);
			}
        }
    	return p;
    }

    /**
     * @return Une bordure vide avec une marge (15 en marge verticale, et 10 en marge horizontale)
     */
    public final static Border createMargin() {
        return BorderFactory.createEmptyBorder(15, 10, 15, 10);
    }

    public static final List<Component> getButtonListFromActionList(final List<? extends Action> actions) {
        return getButtonListFromActionList(actions, 97, 25);
    }

    public static final JButton getButtonFromAction(final Action action) {
        return getButtonFromAction(action, 97, 25);
    }

    /**
     * Construit un bouton de 20 pixels par 20, sans libelle.
     *
     * @param action
     * @return
     */
    public static final JButton getSmallButtonFromAction(final Action action) {
        final JButton res = getButtonFromAction(action, 20, 20);
        res.setHorizontalAlignment(SwingConstants.CENTER);
        res.setText(null);
        res.setMinimumSize(res.getPreferredSize());
        res.setMaximumSize(res.getPreferredSize());
        res.setFocusPainted(false);
        return res;
    }

    /**
     * Construit une liste de JButton Ã  partir d'une liste d'actions.
     *
     * @param actions
     * @param minWidth
     * @param minHeight
     * @return
     */
    public static final List<Component> getButtonListFromActionList(final List<? extends Action> actions, final int minWidth, final int minHeight) {
        final List res = new ArrayList(actions.size());
        for (Action element : actions) {
            if (element != null) {
                res.add(getButtonFromAction(element, minWidth, minHeight));
            }
        }
        return res;
    }

    /**
     * Construit un bouton normal partir d'une action
     * @param action
     * @param minWidth
     * @param minHeight
     * @return
     */
    public static final JButton getButtonFromAction(final Action action, final int minWidth, final int minHeight) {
        final JButton button = new JButton(action);
        button.setHorizontalAlignment(JButton.LEFT);
        button.setMinimumSize(new Dimension(minWidth, minHeight));
        button.setPreferredSize(new Dimension(minWidth, minHeight));
        return button;
    }

    /**
     * Construit un panel avec un panel en haut, un panel en bas et un separateur entre les deux.
     * @param compTop
     * @param compBottom
     * @return
     */
    public final static JSplitPane buildVerticalSplitPane(final Component compTop, final Component compBottom, final double proportionalLocation, final double resizeWeight) {
        final JSplitPane split1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, compTop, compBottom);
        split1.setDividerLocation(proportionalLocation);
        split1.setResizeWeight(resizeWeight);
        split1.setBorder(BorderFactory.createEmptyBorder());
        return split1;
    }

    public final static JSplitPane buildVerticalSplitPane(final Component compTop, final Component compBottom) {
        return buildVerticalSplitPane(compTop, compBottom, 0.5, 0.5);
    }

    /**
     * Construit un panel avec un panel Ã  gauche, un panel Ã  droite et un separateur entre les deux.
     *
     * @param compLeft
     * @param compRight
     * @param proportionalLocation
     * @return
     */
    public final static JSplitPane buildHorizontalSplitPane(final Component compLeft, final Component compRight, final double proportionalLocation, final double resizeWeight) {
        final JSplitPane split1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, compLeft, compRight);
        split1.setDividerLocation(proportionalLocation);
        split1.setResizeWeight(resizeWeight);
        split1.setBorder(BorderFactory.createEmptyBorder());
        return split1;
    }

    public final static JSplitPane buildHorizontalSplitPane(final Component compLeft, final Component compRight) {
        return buildHorizontalSplitPane(compLeft, compRight, 0.5, 0.5);
    }

    /**
     * Renvoie un JDialog contenant le Container passÃ© en parametre. Le dialog est ensuite prÃªt Ã  etre affochÃ©.
     * @param owner
     * @param title
     * @param modal
     * @param mainContainer
     * @return
     */
    public static final JDialog createDialog(final Window owner, final String title, final boolean modal, final Container mainContainer) {
        if (owner instanceof Dialog) {
            return createDialog((Dialog) owner, title, modal, mainContainer);
        }
        return createDialog((Frame) owner, title, modal, mainContainer);
    }

    private static final JDialog createDialog(final Frame owner, final String title, final boolean modal, final Container mainContainer) {
        final JDialog win = new JDialog(owner, title, modal);
        win.setContentPane(mainContainer);
        win.pack();
        ZUiUtil.centerWindow(win);
        return win;
    }

    private static final JDialog createDialog(final Dialog owner, final String title, final boolean modal, final Container mainContainer) {
        final JDialog win = new JDialog(owner, title, modal);
        win.setContentPane(mainContainer);
        win.pack();
        ZUiUtil.centerWindow(win);
        return win;
    }

    /**
     * Centre la fenetre win au centre de l'écran.
     * @param win
     */
    public static final void centerWindow(final Window win) {
        int screenWidth = (int) win.getGraphicsConfiguration().getBounds().getWidth();
        int screenHeight = (int) win.getGraphicsConfiguration().getBounds().getHeight();
        win.setLocation((screenWidth / 2) - ((int) win.getSize().getWidth() / 2), ((screenHeight / 2) - ((int) win.getSize().getHeight() / 2)));
    }

    /**
     * Centre une fenêtre (win) par rapport à une autre (parentWin).
     * @param win
     * @param parentWin
     */
    public static final void centerWindowInWindow(final Window win, final Window parentWin) {
        int parentAbscisse = parentWin.getX();
        int parentOrdonnee = parentWin.getY();
        int xLocation = parentAbscisse + ((int) parentWin.getWidth() / 2);
        int yLocation = parentOrdonnee + ((int) parentWin.getHeight() / 2);
        win.setLocation(xLocation - ((int) win.getWidth() / 2), yLocation - ((int) win.getHeight() / 2));
    }

    /**
     * centre la fenetre par rapport à sa fenetre parente
     * @param win
     */
    public static final void centerWindowInContainer(final Window win) {
        int xLocation = win.getParent().getX() + ((int) win.getParent().getWidth() / 2);
        int yLocation = win.getParent().getY() + ((int) win.getParent().getHeight() / 2);
        win.setLocation(xLocation - ((int) win.getWidth() / 2), yLocation - ((int) win.getHeight() / 2));
    }

    /**
     * Renvoie la liste des fenetres et sous-fenetres visibles à partir de win.
     * @param win
     * @param list
     * @return
     */
    public static LinkedList visibleWindows(final Window win) {
        final LinkedList list = new LinkedList();

        list.add(win);
        final Window[] array = win.getOwnedWindows();
        for (int i = 0; i < array.length; i++) {
            Window window = array[i];
            if (window.isVisible()) {
                list.addAll(visibleWindows(window));
            }
        }
        return list;
    }

    /**
     * Renvoie la fenetre active parmis une liste de fenetres (passez Frame.getFrames pour toutes les fenetres de l'appli)
     * @param windows
     * @return
     */
    public static Window getActiveWindow(Window[] windows) {
        Window result = null;
        for (int i = 0; i < windows.length; i++) {
            Window window = windows[i];
            if (window.isActive()) {
                result = window;
            } else {
                Window[] ownedWindows = window.getOwnedWindows();
                if (ownedWindows != null) {
                    result = getActiveWindow(ownedWindows);
                }
            }
        }
        return result;
    }


    /**
     * Inclue c dans un panel en l'orientant (rempli le vide en utilisant un new JPanel(new BorderLayout()).
     *
     * @param c Component à placer
     * @param orientation Orientation a utiliser BorderLayout.WEST, BorderLayout.EAST, BorderLayout.SOUTH, BorderLayout.NORTH (pas de controle effectue).
     * @return
     */
    public static JPanel alignInNewPanel(final Component c, final String orientation) {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(c, orientation);
        panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return panel;
    }

    /**
     * Affecte une bordure au composant. Une ligne de couleur plus claire par rapport au background du composant.
     * @param c
     */
    public static void setSimpleLineBorder(final JComponent c) {
        c.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder( c.getBackground().brighter() ), BorderFactory.createEmptyBorder(4,2,0,4)     ));
    }

    /**
     *
     * @param group
     * @return Le bouton sélectionné dans un group de bouton
     */
    public static JRadioButton getSelectedRadioButton(ButtonGroup group) {
        for (Enumeration e=group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();
            if (b.getModel() == group.getSelection()) {
                return b;
            }
        }
        return null;
    }
}
