/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


/**
 * Panel avec titre + texte + image.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZCommentPanel extends ZAbstractSwingPanel {
    private final JLabel title = new JLabel();
    private final JLabel  comment = new JLabel();
//    private final JTextArea  comment = new JTextArea();
    private final JLabel img = new JLabel();
    private final Color _bgColor;
    private final Color _txtColor;
    private final String _imgPosition;
    
    public ZCommentPanel(final String aTitle, final String aComment, final ImageIcon aImageIcon) {
        this(aTitle, aComment, aImageIcon, Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.EAST);
    }
    
    /**
     * Crée un panel de commentaire
     * 
     * @param aTitle
     * @param aComment
     * @param aImageIcon
     * @param bgColor
     * @param txtColor
     * @param imgPosition Utiliser BorderLayout.EAST ou BorderLayout.WEST 
     */
    public ZCommentPanel(final String aTitle, final String aComment, final ImageIcon aImageIcon, final Color bgColor, final Color txtColor, final String imgPosition) {
        super();
        _bgColor = bgColor;
        _txtColor = txtColor;
        _imgPosition = imgPosition;
        initGUI();
        setTitleText(aTitle);
        setCommentText(aComment);
        setIcon(aImageIcon);        
    }
    
    
    
    

    
    protected void initGUI() {
		//Le titre
		title.setFont(getFont().deriveFont((float)12).deriveFont(Font.BOLD));

		comment.setFont(getFont().deriveFont((float)11));
//		comment.setEditable(false);
		comment.setAlignmentX(SwingConstants.LEFT);
        comment.setBorder(BorderFactory.createEmptyBorder(0,6,0,6));
        comment.setForeground(_txtColor);
        comment.setBackground(_bgColor);
		
        
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createMatteBorder(0,0,5,0, _bgColor));
        setForeground(_txtColor);
        
		setBackground(_bgColor);
		setMinimumSize(getPreferredSize());
        
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBackground(_bgColor);
		tmp.add(title, BorderLayout.NORTH);
        tmp.add(comment, BorderLayout.CENTER);
        add(tmp,BorderLayout.CENTER);
        
        img.setVerticalTextPosition(SwingConstants.TOP);
        tmp.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        if (BorderLayout.EAST.equals(_imgPosition)) {
            img.setHorizontalTextPosition(SwingConstants.RIGHT);
        }
        else {
            img.setHorizontalTextPosition(SwingConstants.LEFT);
        }
		add(img, _imgPosition);
    }
    
    
    public String getCommentText() {
        return comment.getText();
    }
    
    public void setCommentText(final String s) {
        comment.setText(s);
    }
    
    public void setTitleText(final String s) {
        title.setText(s);
    }
    
    public void setIcon(ImageIcon i) {
        img.setIcon(i);
    }
	        
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
    }

    public void updateData() throws Exception {
        
    }
	
}
