/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ZSplashScreen extends JFrame {

    private final ImageIcon _image;
    private final String _labelTop;
    private final String _labelBottom;
    private JPanel splashPanel;
    
    /**
     * @param title
     * @throws HeadlessException
     */
    public ZSplashScreen(String title, ImageIcon image, String topText, String bottomText) throws HeadlessException {
        super(title);
        _image = image;
        _labelTop = topText;
        _labelBottom = bottomText;
        splashPanel = buildPanel();
        setSize(new Dimension(320,240));
        setUndecorated(true);
        setContentPane(splashPanel);
        this.pack();
        centerWindow();
    }

    private JPanel buildPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        if (_labelTop!=null) {
            final JLabel north = new JLabel(_labelTop);
            panel.add(north, BorderLayout.NORTH);
        }
        if (_labelBottom!=null) {
            final JLabel south = new JLabel(_labelBottom);
            panel.add(south, BorderLayout.SOUTH);
        }
        
        final JLabel center = new JLabel(_image);
        panel.add(center, BorderLayout.CENTER);
        
        return panel;
    }
    
    public void centerWindow() {
        int screenWidth = (int)this.getGraphicsConfiguration().getBounds().getWidth();
        int screenHeight = (int)this.getGraphicsConfiguration().getBounds().getHeight();
        this.setLocation((screenWidth/2)-((int)this.getSize().getWidth()/2), ((screenHeight/2)-((int)this.getSize().getHeight()/2)));
        
    }       
   
    
}
