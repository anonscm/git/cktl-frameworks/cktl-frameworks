/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo;

import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class CRIWaitingFrame2 extends JFrame {
	// Titre de la fenetre, texte d'intro, texte du message
	String title, intro, message;
	JComponent myContentPane;
	// Les labels pour les textes : resp. intro et message
	JLabel sujet, texte;

	public CRIWaitingFrame2(String aTitle, String anIntro, String aMessage) {
		super(aTitle);

		// Initialisations
		title = aTitle;
		message = aMessage;
		intro = anIntro;
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int) this.getGraphicsConfiguration().getBounds().getHeight();

		myContentPane = createUI();
		myContentPane.setOpaque(true);
		this.setContentPane(myContentPane);
		this.setResizable(false);
		this.setLocation((x / 2) - ((int) this.getContentPane().getMinimumSize().getWidth() / 2), ((y / 2) - ((int) this.getContentPane().getMinimumSize().getHeight() / 2)));

		//Display the window.
		this.pack();
		this.setVisible(true);
		this.paintAll(this.getGraphics());
	}

	/**
	 * Constructeur pour ne pas l'afficher directement.
	 */
	public CRIWaitingFrame2(String aTitle, String anIntro, String aMessage, boolean hide) {
		super(aTitle);

		// Initialisations
		title = aTitle;
		message = aMessage;
		intro = anIntro;
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// Recuperer les dimensions de l'ecran
		int x = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int y = (int) this.getGraphicsConfiguration().getBounds().getHeight();

		myContentPane = createUI();
		myContentPane.setOpaque(true);
		this.setContentPane(myContentPane);
		this.setResizable(false);
		this.setLocation((x / 2) - ((int) this.getContentPane().getMinimumSize().getWidth() / 2), ((y / 2) - ((int) this.getContentPane().getMinimumSize().getHeight() / 2)));
		this.pack();
		if (!hide) {
			setVisible(true);
		}
	}

	public void showTheFrame() {
		//Display the window.
		this.setVisible(true);
		this.paintAll(this.getGraphics());
		validate();
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	}

	public void hideTheFrame() {
		this.setVisible(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	private JPanel createUI() {
		//Create the labels.
		sujet = new JLabel(intro);
		texte = new JLabel(message);
		sujet.setLabelFor(texte);

		//Create the panel we'll return and set up the layout.
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 10, 20));
		sujet.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		texte.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		//Add the labels to the content pane.
		panel.add(sujet);
		panel.add(Box.createVerticalStrut(5)); //extra space
		panel.add(texte);

		//Add a vertical spacer that also guarantees us a minimum width:
		panel.add(Box.createRigidArea(new Dimension(150, 10)));

		return panel;
	}

	public void setTitle(String aTitle) {
		title = aTitle;
		this.setTitle(title);
		this.paintAll(this.getGraphics());
	}

	public void setIntro(String anIntro) {
		intro = anIntro;
		sujet.setText(intro);
		this.paintAll(this.getGraphics());
	}

	public void setMessage(String aMessage) {
		message = aMessage;
		texte.setText(message);
		this.paintAll(this.getGraphics());
		validate();
	}
}
