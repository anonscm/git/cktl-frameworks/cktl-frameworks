/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZToolBar extends JToolBar {
    public static final int SEPARATOR_WIDTH = 40;
    public static final int SEPARATOR_HEIGHT = 40;
    public static final Dimension HORIZONTAL_SEPARATOR_DIMENSION = new Dimension(SEPARATOR_WIDTH, 0);
    public static final Dimension VERTICAL_SEPARATOR_DIMENSION = new Dimension(0, SEPARATOR_HEIGHT);

    private int buttonsHorizontalTextPosition = SwingConstants.TRAILING;
    private int buttonsVerticalTextPosition = SwingConstants.CENTER;
    private Boolean buttonsHideLabel = Boolean.FALSE;


    /**
     *
     */
    public ZToolBar() {
        this(0);
    }

    /**
     * @param orientation
     */
    public ZToolBar(int orientation) {
        this(null,orientation);
    }

    /**
     * @param name
     */
    public ZToolBar(String name) {
        this(name, 0);
    }

    public ZToolBar(final String name, final int orientation) {
        this(name, 0, null);
    }

    /**
     * @param name
     * @param orientation
     */
    public ZToolBar(String name, int orientation, Collection collection) {
        super(name, orientation);
//        setLayout(new BorderLayout());
        setFloatable(false);
        setRollover(true);
        setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        addAll(collection);
        setOrientation(orientation);
    }


    public final void addAll(Collection collection) {
        if (collection != null) {
            final Iterator iter = collection.iterator();
            while (iter.hasNext()) {
                final Object obj = iter.next();
                if (obj==null) {
                    addSeparator( getOrientation()==HORIZONTAL  ? HORIZONTAL_SEPARATOR_DIMENSION : VERTICAL_SEPARATOR_DIMENSION);
                }
                else if (obj instanceof Action) {
                    add((Action)obj);
                }
                else if (obj instanceof Component) {
                    add((Component)obj) ;
                }
            }
        }
    }


    protected JButton createActionComponent(Action action) {
        final JButton button = super.createActionComponent(action);
        button.putClientProperty("hideActionText", Boolean.FALSE);
        button.setHorizontalTextPosition(getButtonsHorizontalTextPosition());
        button.setVerticalTextPosition(getButtonsVerticalTextPosition());
        return button;
    }


    /**
     * @see javax.swing.JToolBar#setOrientation(int)
     */
    public void setOrientation(int o) {
        if (o==JToolBar.HORIZONTAL) {
            setButtonsHorizontalTextPosition(SwingConstants.TRAILING);
            setButtonsVerticalTextPosition(SwingConstants.CENTER);
        }
        else {
            setButtonsHorizontalTextPosition(SwingConstants.CENTER);
            setButtonsVerticalTextPosition(SwingConstants.BOTTOM);
        }
        super.setOrientation(o);
    }


    public Boolean getButtonsHideLabel() {
        return buttonsHideLabel;
    }
    public void setButtonsHideLabel(Boolean buttonsHideLabel) {
        this.buttonsHideLabel = buttonsHideLabel;
    }
    public int getButtonsHorizontalTextPosition() {
        return buttonsHorizontalTextPosition;
    }
    public void setButtonsHorizontalTextPosition(int buttonsHorizontalTextPosition) {
        this.buttonsHorizontalTextPosition = buttonsHorizontalTextPosition;
    }
    public int getButtonsVerticalTextPosition() {
        return buttonsVerticalTextPosition;
    }
    public void setButtonsVerticalTextPosition(int buttonsVerticalTextPosition) {
        this.buttonsVerticalTextPosition = buttonsVerticalTextPosition;
    }
}
