/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZDefaultTablePanel.IZDefaultTablePanelMdl;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable.ZEOTableListener;

public class ZAffectationPanel extends ZAbstractSwingPanel {

	protected final IAffectationPanelMdl _mdl;

	private final ZDefaultTablePanel leftPanel;
	private final ZDefaultTablePanel rightPanel;
	protected final JToolBar leftToolBar;
	protected final ZFormPanel leftSrchFilter;

	private final String FILTERKEY = "srchstr";
	private final Map filterMap = new HashMap();
	private final IZTextFieldModel leftFilterSrchMdl = new ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);

	public ZAffectationPanel(IAffectationPanelMdl mdl) {
		super();
		setLayout(new BorderLayout());
		_mdl = mdl;

		leftPanel = new ZDefaultTablePanel(_mdl.getLeftPanelMdl());
		rightPanel = new ZDefaultTablePanel(_mdl.getRightPanelMdl());

		leftPanel.getMyEOTable().addListener(new ZEOTableListener() {
			public void onDbClick() {
				_mdl.actionLeftToRight().actionPerformed(null);
			}

			public void onSelectionChanged() {
				_mdl.actionLeftToRight().setEnabled(leftPanel.selectedObjects().count() > 0);
			}

		});

		rightPanel.getMyEOTable().addListener(new ZEOTableListener() {
			public void onDbClick() {
				_mdl.actionRightToLeft().actionPerformed(null);
			}

			public void onSelectionChanged() {
				_mdl.actionRightToLeft().setEnabled(rightPanel.selectedObjects().count() > 0);
			}

		});

		if (_mdl.displayLeftFilter()) {

			leftSrchFilter = ZFormPanel.buildLabelField("Filtrer", leftFilterSrchMdl);
			((ZTextField) leftSrchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);

			leftToolBar = new JToolBar(SwingConstants.HORIZONTAL);
			leftToolBar.setFloatable(false);
			leftToolBar.setRollover(true);
			leftToolBar.add(leftSrchFilter);

			((ZTextField) leftSrchFilter.getMyFields().get(0)).getMyTextComponent().getDocument().addDocumentListener(
					new DocumentListener() {

						public void changedUpdate(DocumentEvent e) {
							_mdl.filterChanged();
						}

						public void insertUpdate(DocumentEvent e) {
							_mdl.filterChanged();
						}

						public void removeUpdate(DocumentEvent e) {
							_mdl.filterChanged();
						}

					});

			//            leftToolBar.add(leftSrchFilter);
		}
		else {
			leftSrchFilter = null;
			leftToolBar = null;
		}

		buildPanel();

		_mdl.actionLeftToRight().setEnabled(false);
		_mdl.actionRightToLeft().setEnabled(false);

	}

	protected void buildPanel() {

		final JPanel px = new JPanel(new BorderLayout());
		if (leftToolBar != null) {
			px.add(leftToolBar, BorderLayout.NORTH);
		}
		px.add(leftPanel, BorderLayout.CENTER);

		final JPanel p1 = new JPanel(new BorderLayout());
		p1.add(ZUiUtil.encloseInPanelWithTitle(_mdl.getLeftLibelle(), null, _mdl.getLeftTitleBgColor(), px, null, null), BorderLayout.CENTER);
		p1.add(buildButtonsPanel(), BorderLayout.EAST);

		final JPanel p7 = new JPanel(new BorderLayout());
		p7.add(ZUiUtil.buildHorizontalSplitPane(p1, ZUiUtil.encloseInPanelWithTitle(_mdl.getRightLibelle(), null, _mdl.getRightTitleBgColor(), rightPanel, null, null)), BorderLayout.CENTER);

		add(p7, BorderLayout.CENTER);
	}

	public void updateData() throws Exception {
		super.updateData();
		leftPanel.updateData();
		rightPanel.updateData();
	}

	/**
	 * @return le panel qui contient les boutons de transfert des objets de gauche à droite. Par defaut un bouton "droite" et un bouton "gauche"
	 */
	protected JComponent buildButtonsPanel() {
		final ArrayList list = new ArrayList();
		final JButton b1 = ZUiUtil.getButtonFromAction(_mdl.actionLeftToRight());
		final JButton b2 = ZUiUtil.getButtonFromAction(_mdl.actionRightToLeft());
		b1.setHorizontalAlignment(SwingConstants.CENTER);
		b2.setHorizontalAlignment(SwingConstants.CENTER);
		list.add(b1);
		list.add(b2);

		final JPanel p0 = new JPanel(new BorderLayout());
		p0.add(ZUiUtil.buildGridColumn(list), BorderLayout.NORTH);
		p0.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		p0.setBorder(BorderFactory.createEmptyBorder(50, 6, 2, 5));
		return p0;
	}

	public interface IAffectationPanelMdl {
		public IZDefaultTablePanelMdl getLeftPanelMdl();

		public boolean displayLeftFilter();

		public void filterChanged();

		public IZDefaultTablePanelMdl getRightPanelMdl();

		public Action actionRightToLeft();

		public Action actionLeftToRight();

		public String getLeftLibelle();

		public String getRightLibelle();

		public Color getLeftTitleBgColor();

		public Color getRightTitleBgColor();

	}

	public final ZDefaultTablePanel getLeftPanel() {
		return leftPanel;
	}

	public final ZDefaultTablePanel getRightPanel() {
		return rightPanel;
	}

	public final ZFormPanel getLeftSrchFilter() {
		return leftSrchFilter;
	}

	public String getLeftFilterString() {
		if (leftSrchFilter == null) {
			return null;
		}
		return ((ZTextField) leftSrchFilter.getMyFields().get(0)).getMyTextComponent().getText();

	}

}
