/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client;

import java.util.StringTokenizer;

public class ZVersion implements Comparable {
	private int m_major;
	private int m_minor;
	private int m_micro;

	/**
	 * Crée un nouvel objet.
	 * 
	 * @param version Chaine du type xxx.yyy.zzz . Si la chaine se termine par _nnn, ce n'est pas pris en compte.
	 * @return une instance de ZVersion
	 * @throws NumberFormatException
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	public static ZVersion getVersion(String version) throws NumberFormatException, IllegalArgumentException {
		if (version == null) {
			throw new NullPointerException("version");
		}

		//        if (version.indexOf("_")>-1 ) {
		//            version = version.substring(0, version.indexOf("_"));
		//        }
		//        if (version.indexOf("-")>-1 ) {
		//            version = version.substring(0, version.indexOf("-"));
		//        }

		version = cleanVersionStr(version);

		final StringTokenizer tokenizer = new StringTokenizer(version, ".");
		final String[] levels = new String[tokenizer.countTokens()];
		for (int i = 0; i < levels.length; i++) {
			levels[i] = tokenizer.nextToken();
		}

		int major = -1;
		if (0 < levels.length) {
			major = Integer.parseInt(levels[0]);
		}

		int minor = 0;
		if (1 < levels.length) {
			minor = Integer.parseInt(levels[1]);
		}

		int micro = 0;
		if (2 < levels.length) {
			micro = Integer.parseInt(levels[2]);
		}

		return new ZVersion(major, minor, micro);
	}

	public ZVersion(final int major, final int minor, final int micro) {
		m_major = major;
		m_minor = minor;
		m_micro = micro;
	}

	public int getMajor() {
		return m_major;
	}

	public int getMinor() {
		return m_minor;
	}

	public int getMicro() {
		return m_micro;
	}

	/**
	 * Compare deux versions.
	 * 
	 * @param other
	 * @return true si les deux versions sont egales.
	 */
	public boolean equals(final ZVersion other) {
		if (other == null)
			return false;

		boolean isEqual = (getMajor() == other.getMajor());

		if (isEqual) {
			isEqual = (getMinor() == other.getMinor());
		}

		if (isEqual) {
			isEqual = (getMicro() == other.getMicro());
		}

		return isEqual;
	}

	public boolean equals(final Object other) {
		boolean isEqual = false;

		if (other instanceof ZVersion) {
			isEqual = equals((ZVersion) other);
		}

		return isEqual;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public boolean complies(final ZVersion other) {
		if (other == null)
			return false;

		if (other.m_major == -1) {
			return true;
		}
		if (m_major != other.m_major) {
			return false;
		} else if (m_minor < other.m_minor) {
			return false;
		} else if (m_minor == other.m_minor && m_micro < other.m_micro) {
			return false;
		} else {
			return true;
		}
	}

	public String toString() {
		return m_major + "." + m_minor + "." + m_micro;
	}

	public int compareTo(Object o) {
		if (o == null)
			throw new NullPointerException("o");

		ZVersion other = (ZVersion) o;
		int val = 0;

		if (getMajor() < other.getMajor())
			val = -1;
		if (0 == val && getMajor() > other.getMajor())
			val = 1;

		if (0 == val && getMinor() < other.getMinor())
			val = -1;
		if (0 == val && getMinor() > other.getMinor())
			val = 1;

		if (0 == val && getMicro() < other.getMicro())
			val = -1;
		if (0 == val && getMicro() > other.getMicro())
			val = 1;

		return val;
	}

	/**
	 * Compare 2 numéros de version donnés sous la forme de tableaux de int. Chaque numéro de version peut contenir autant d'éléments que l'on veut.<br>
	 * 2 numéros null ou vides sont considérés égaux.<br>
	 * 1 numéro null est considéré inférieur à l'autre non null.<br>
	 * 1 numéro vide est considéré inférieur à l'autre non vide.<br>
	 * <b>ATTENTION:<b> 1.2.0 est considéré égal à 1.2 !!!
	 * 
	 * @param version
	 * @param anotherVersion
	 * @return Un entier négatif, zéro, ou un entier positif si le premier argument est inférieur, égal à, ou supérieur au second.
	 */
	public static int compare(final int[] version, final int[] anotherVersion) {
		if (version == null && anotherVersion == null) {
			return 0;
		}
		if (version == null) {
			return -1;
		}
		if (anotherVersion == null) {
			return 1;
		}
		for (int i = 0; i < version.length && i < anotherVersion.length; i++) {
			if (version[i] < anotherVersion[i]) {
				return -1;
			}
			if (version[i] > anotherVersion[i]) {
				return 1;
			}
		}
		if (version.length < anotherVersion.length) {
			for (int i = version.length; i < anotherVersion.length; i++) {
				if (anotherVersion[i] > 0) {
					return -1;
				}
			}
		}
		if (version.length > anotherVersion.length) {
			for (int i = anotherVersion.length; i < version.length; i++) {
				if (version[i] > 0) {
					return 1;
				}
			}
		}
		return 0;
	}

	/**
	 * @param str Une chaine indiquant un n° de version (du type 1.5.6.7)
	 * @return Un tableau d'int avec tous les numeros de la version
	 */
	public static int[] strToIntArray(String str) {
		String clstr = cleanVersionStr(str);
		String[] r = clstr.split(".");
		int[] res = new int[(r.length)];
		for (int i = 0; i < r.length; i++) {
			String string = r[i];
			res[i] = (new Integer(string)).intValue();
		}
		return res;
	}

	/**
	 * Nettoie (enleve les caratere nons numeriques (et non .)
	 * 
	 * @param s
	 * @return
	 */
	private static String cleanVersionStr(final String s) {
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < s.length() && (isBasicDigit(s.charAt(i)) || '.' == s.charAt(i)); i++) {
			res.append(s.substring(i, i + 1));
		}
		return res.toString();
	}

	private static boolean isBasicDigit(char c) {
		int numVal = Character.getNumericValue(c);
		return ((Character.getNumericValue('0') <= numVal) && (numVal <= Character.getNumericValue('9')));
	}

}
