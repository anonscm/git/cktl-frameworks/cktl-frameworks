/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.TransferHandler;


/**
 * panneau qui permet d'afficher la représentation d'un fichier. Implémente le drag & drop.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZFileBox extends ZAbstractSwingPanel  {
    public static DataFlavor  FILEFLAVOR = DataFlavor.javaFileListFlavor;;
    private IZFileBoxModel model;
    private ZSingleFileView iconLabel;
    private ZFileTransferHandler transferHandler;
//    private DataFlavor[] supportedDataFlavors;
    
    
    
    /**
     * 
     */
    public ZFileBox(IZFileBoxModel pmodel) {
        super();
        model = pmodel;
        initGUI();
    }

    /**
     * @param isDoubleBuffered
     */
    public ZFileBox(boolean isDoubleBuffered, IZFileBoxModel pmodel) {
        super(isDoubleBuffered);
        model = pmodel;
        initGUI();
    }

    public void initGUI() {
//        supportedDataFlavors = new DataFlavor[]{FILEFLAVOR};
        transferHandler = new ZFileTransferHandler();
        
        iconLabel = new ZSingleFileView();
        iconLabel.setTransferHandler(transferHandler);
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                JComponent c = (JComponent)e.getSource();
                TransferHandler th = c.getTransferHandler();
                th.exportAsDrag(c, e, TransferHandler.COPY);
            }
        };
        iconLabel.addMouseListener(ml);
        
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createLoweredBevelBorder());
        add(iconLabel, BorderLayout.CENTER);
    }


    public void updateData() {
        if (model.getFile()==null || model.getFile().getName()==null) {
            iconLabel.setIcon( null );
            iconLabel.setText("Fichier non généré");
        }
        else {
            iconLabel.setIcon( model.getIcon() );
            iconLabel.setText(model.getFile().getName());
        }
    }
    
    public interface IZFileBoxModel{
        public File getFile();
        /**
         * @return
         */
        public String getText();
        public Icon getIcon();
    }

    
     
    
    
    
    public final class ZFileTransferHandler extends TransferHandler {
        

        public ZFileTransferHandler() {
        }

        /**
         * @see javax.swing.TransferHandler#exportDone(javax.swing.JComponent, java.awt.datatransfer.Transferable, int)
         */
        protected void exportDone(JComponent source, Transferable data, int action) {
            super.exportDone(source, data, action);
        }
        
        
        /**
         * @see javax.swing.TransferHandler#createTransferable(javax.swing.JComponent)
         */
        protected Transferable createTransferable(JComponent c) {
            return super.createTransferable(c);
        }
        
        public boolean importData(JComponent c, Transferable t) {
//            if (hasFileFlavor(t.getTransferDataFlavors())) {
//                try {
//                    File f = (File) t.getTransferData(FILEFLAVOR);
//                    return true;
//                } catch (UnsupportedFlavorException ufe) {
//                } catch (IOException ioe) {
//                }
//            }
            return false;
        }

        protected boolean hasFileFlavor(DataFlavor[] flavors) {
//            System.out.println("ZFileTransferHandler.hasFileFlavor()");
            for (int i = 0; i < flavors.length; i++) {
                if (FILEFLAVOR.equals(flavors[i])) {
                    return true;
                }
            }
            return false;
        }
    }    
    
    
    
    private final class ZSingleFileView extends JPanel implements DragSourceListener, DragGestureListener  {
        private JLabel labelIcon;
        private JLabel labelName;
        private DragSource dragSource = DragSource.getDefaultDragSource();
        
        /**
         * 
         */
        public ZSingleFileView() {
            super();
            dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this);
            labelIcon = new JLabel();
            labelIcon.setVerticalAlignment(SwingConstants.CENTER);
            labelIcon.setHorizontalAlignment(SwingConstants.CENTER);
            labelName = new JLabel();
            labelName.setVerticalAlignment(SwingConstants.CENTER);
            labelName.setHorizontalAlignment(SwingConstants.CENTER);            
            setLayout(new BorderLayout());
            setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
            add(labelIcon, BorderLayout.CENTER);
            add(labelName, BorderLayout.SOUTH);
        }

        public void setText(final String text) {
            labelName.setText(text);
        }
        
        public void setIcon(final Icon icon) {
            labelIcon.setIcon(icon);
        }
        
        
        /**
         * @see java.awt.dnd.DragSourceListener#dragEnter(java.awt.dnd.DragSourceDragEvent)
         */
        public void dragEnter(DragSourceDragEvent dsde) {
        }

        /**
         * @see java.awt.dnd.DragSourceListener#dragOver(java.awt.dnd.DragSourceDragEvent)
         */
        public void dragOver(DragSourceDragEvent dsde) {
            
        }

        /**
         * @see java.awt.dnd.DragSourceListener#dropActionChanged(java.awt.dnd.DragSourceDragEvent)
         */
        public void dropActionChanged(DragSourceDragEvent dsde) {
            
        }

        /**
         * @see java.awt.dnd.DragSourceListener#dragDropEnd(java.awt.dnd.DragSourceDropEvent)
         */
        public void dragDropEnd(DragSourceDropEvent dsde) {
            
        }

        /**
         * @see java.awt.dnd.DragSourceListener#dragExit(java.awt.dnd.DragSourceEvent)
         */
        public void dragExit(DragSourceEvent dse) {
        }

        /**
         * @see java.awt.dnd.DragGestureListener#dragGestureRecognized(java.awt.dnd.DragGestureEvent)
         */
        public void dragGestureRecognized(DragGestureEvent dge) {
            if (model.getFile()==null) {
                return;
            }
            final FileSelection transferable = new FileSelection(model.getFile());
            dge.startDrag(DragSource.DefaultCopyDrop, transferable, this);
        }           
        
        
        
    }
    
    
    
    public class FileSelection extends Vector implements Transferable {
        final static int FILE = 0;
        final static int STRING = 1;
        DataFlavor flavors[] = { DataFlavor.javaFileListFlavor, DataFlavor.stringFlavor };

        public FileSelection(File file) {
            addElement(file);
        }

        /* Returns the array of flavors in which it can provide the data. */
        public synchronized DataFlavor[] getTransferDataFlavors() {
            return flavors;
        }

        /* Returns whether the requested flavor is supported by this object. */
        public boolean isDataFlavorSupported(final DataFlavor flavor) {
            boolean b = false;
            b |= flavor.equals(flavors[FILE]);
            b |= flavor.equals(flavors[STRING]);
            return (b);
        }

        /**
         * If the data was requested in the "java.lang.String" flavor, return
         * the String representing the selection.
         */
        public synchronized Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (flavor.equals(flavors[FILE])) {
                return this;
            } else if (flavor.equals(flavors[STRING])) {
                return ((File) elementAt(0)).getAbsolutePath();
            } else {
                throw new UnsupportedFlavorException(flavor);
            }
        }
    }    
    
    
    
    
    
    
    
}
