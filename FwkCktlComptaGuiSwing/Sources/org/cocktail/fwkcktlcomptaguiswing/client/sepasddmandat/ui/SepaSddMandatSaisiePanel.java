/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZActionField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZStringDatePickerField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SepaSddMandatSaisiePanel extends ZComptaSwingPanel {
	private static final long serialVersionUID = 1L;

	private static final int SEARCH_PERSONNE_FIELD_LENGTH = 30;

	private ISepaSddMandatSaisiePanelListener myListener;
	private final JLabel refAppliCreation;
	private final ZTextField libelle;
	private final ZTextField commentaire;
	private final JLabel rum;
	private final ZStringDatePickerField dateCreationMandat;
	private final ZStringDatePickerField dateSignatureMandat;
	private final ZActionField debiteurPersonne;
	private final ZActionField tiersDebiteurPersonne;
	private final JComboBox creancierCompte;
	private final JComboBox debiteurRib;
	private final JComboBox debiteurAdresse;
	private final JComboBox typePrelevement;

	public SepaSddMandatSaisiePanel(ISepaSddMandatSaisiePanelListener listener) {
		super();
		myListener = listener;

		refAppliCreation = new JLabel();
		libelle = new ZTextField(new MandatLibelleModel(myListener.getValues(), EOSepaSddMandat.LIBELLE_KEY));
		libelle.getMyTexfield().setColumns(30);
		commentaire = new ZTextField(new DefaultTextFieldModel(myListener.getValues(), EOSepaSddMandat.COMMENTAIRE_KEY));
		commentaire.getMyTexfield().setColumns(30);

		rum = new JLabel();
		creancierCompte = new JComboBox(listener.getParamCompteModel());

		MyDatePickerFieldModel dateCreationMdl = new MyDatePickerFieldModel(myListener.getValues(), EOSepaSddMandat.D_MANDAT_CREATION_KEY);

		dateCreationMandat = new ZStringDatePickerField(dateCreationMdl, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		MyDatePickerFieldModel dateSignatureMdl = new MyDatePickerFieldModel(myListener.getValues(), EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY);
		dateSignatureMandat = new ZStringDatePickerField(dateSignatureMdl, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));

		debiteurPersonne = initDebiteurPersonneField(myListener.actionSelectDebiteur());
		tiersDebiteurPersonne = initTiersDebiteurPersonneField(myListener.actionSelectTiersDebiteur());
		debiteurRib = new JComboBox(listener.getDebiteurRibModel());
		debiteurAdresse = new JComboBox(listener.getDebiteurAdresseModel());
		typePrelevement = new JComboBox(listener.getTypePrelevementModel());
	}

	private ZActionField initDebiteurPersonneField(Action selectDebiteur) {
		ZActionField debiteurPersonneField = new ZActionField(
				new PersonneLibelleModel(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY), selectDebiteur);
		debiteurPersonneField.getMyTexfield().setColumns(SEARCH_PERSONNE_FIELD_LENGTH);
		debiteurPersonneField.getMyTexfield().setEnabled(false);
		return debiteurPersonneField;
	}

	private ZActionField initTiersDebiteurPersonneField(Action selectTiersDebiteur) {
		ZActionField tiersDebiteurPersonneField = new ZActionField(
				new PersonneLibelleModel(EOSepaSddMandat.TO_TIERS_DEBITEUR_PERSONNE_KEY), selectTiersDebiteur);
		tiersDebiteurPersonneField.getMyTexfield().setColumns(SEARCH_PERSONNE_FIELD_LENGTH);
		tiersDebiteurPersonneField.getMyTexfield().setEnabled(false);
		return tiersDebiteurPersonneField;
	}

	private final class PersonneLibelleModel implements ZTextField.IZTextFieldModel {
		private String key;

		public PersonneLibelleModel(String keyPersonne) {
			this.key = keyPersonne;
		}

		public Object getValue() {
			if (myListener.getValues().get(key) != null) {
				return ((EOGrhumPersonne) myListener.getValues().get(key)).getNomAndPrenomAndCode();
			}
			return null;
		}

		public void setValue(Object value) {
		}
	}

	private final class MandatLibelleModel extends DefaultTextFieldModel {
		public MandatLibelleModel(Map filter, String key) {
			super(filter, key);
		}
	}

	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildForm(), BorderLayout.CENTER);
		this.add(buildBottom(), BorderLayout.SOUTH);
	}

	private final JComponent buildForm() {
		return new SepaSddMandatInfosPanel.Builder()
			.provenance(refAppliCreation)
			.compteCreancier(creancierCompte)
			.libelle(libelle)
			.commentaires(commentaire)
			.rum(rum)
			.debiteur(debiteurPersonne)
			.adresse(debiteurAdresse)
			.coordonneesBancaires(debiteurRib)
			.tiersDebiteur(tiersDebiteurPersonne)
			.date(dateCreationMandat)
			.dateSignatureDebiteur(dateSignatureMandat)
			.build();
	}

	private final JPanel buildBottom() {
		ArrayList<Action> a = new ArrayList<Action>();
		a.add(myListener.actionSave());
		a.add(myListener.actionCancel());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		refAppliCreation.setText((String) myListener.getValues().get(EOSepaSddMandat.REF_APPLI_CREATION_KEY));
		libelle.updateData();
		commentaire.updateData();
		rum.setText((String) myListener.getValues().get(EOSepaSddMandat.RUM_KEY));
		debiteurPersonne.updateData();
		dateCreationMandat.updateData();
		dateSignatureMandat.updateData();
		tiersDebiteurPersonne.updateData();
		applyConstraints();
	}

	private void applyConstraints() {
		SepaSddUiConstraints constraints = myListener.uiConstraints();
		myListener.actionSelectDebiteur().setEnabled(constraints.isDebiteurMandatSelectionEnabled());
	}

	/**
	 * @author rprin
	 */
	public interface ISepaSddMandatSaisiePanelListener {

		Action actionCancel();

		Action actionSave();

		boolean objectHasEcheanciers();

		ComboBoxModel getTypePrelevementModel();

		Action actionSelectDebiteur();

		Action actionSelectTiersDebiteur();

		ComboBoxModel getParamCompteModel();

		ComboBoxModel getDebiteurRibModel();

		ComboBoxModel getDebiteurAdresseModel();

		Map getValues();

		SepaSddUiConstraints uiConstraints();
	}

	private class MyDatePickerFieldModel extends DefaultTextFieldModel implements IZDatePickerFieldModel {
		public MyDatePickerFieldModel(final Map filter, final String key) {
			super(filter, key);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

}
