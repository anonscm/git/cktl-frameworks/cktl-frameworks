/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.math.BigDecimal;

import javax.swing.Action;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.TracableColFactory;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;

import com.webobjects.foundation.NSArray;

/**
 * Affiche une liste d'echeanciers pour un mandat SDD SEPA.
 *
 * @author Rodolphe PRIN
 */
public class SepaSddEcheancierListPanel extends ZComptaSwingTablePanel {

	private static final long serialVersionUID = 1L;
	private final ISepaSDDEcheancierListPanelToolbarListener myEcheancierToolbarListener;
	private final SepaSddEcheancierPriseEnChargeCellRenderer sepaSddEcheancierPriseEnChargeCellRenderer = new SepaSddEcheancierPriseEnChargeCellRenderer();

	public SepaSddEcheancierListPanel(IZKarukeraTablePanelListener listener, ISepaSDDEcheancierListPanelToolbarListener echeancierToolbarListener) {
		super(listener);

		myEcheancierToolbarListener = echeancierToolbarListener;

		ZEOTableModelColumn colOrigine = new ZEOTableModelColumn(myDisplayGroup, EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY + "." + EOSepaSddOrigine.TYPE_AND_LIBELLE_COMPLET_OF_ORIGINE_KEY, "Origine",
				60);
		colOrigine.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn montant = new ZEOTableModelColumn(myDisplayGroup, EOSepaSddEcheancier.MONTANT_KEY, "Montant", 10);
		montant.setAlignment(SwingConstants.RIGHT);
		montant.setColumnClass(BigDecimal.class);
		montant.setFormatDisplay(ZConst.FORMAT_DECIMAL);

		ZEOTableModelColumn nbEcheances = new ZEOTableModelColumn(myDisplayGroup, EOSepaSddEcheancier.NB_ECHEANCES_KEY, "Nombre échéances", 10);
		nbEcheances.setAlignment(SwingConstants.CENTER);
		nbEcheances.setColumnClass(Integer.class);

		ZEOTableModelColumn numeroEcrPriseEnCharge = new ZEOTableModelColumnWithProvider("Prise en charge", new NumeroPriseEnChargeProvider(), 10);
		numeroEcrPriseEnCharge.setAlignment(SwingConstants.CENTER);
		numeroEcrPriseEnCharge.setTableCellRenderer(sepaSddEcheancierPriseEnChargeCellRenderer);

		ZEOTableModelColumn modifiePar = TracableColFactory.sharedInstance().getModifiePar(myDisplayGroup);
		ZEOTableModelColumn modifieLe = TracableColFactory.sharedInstance().getModifieLe(myDisplayGroup);

		colsMap.clear();
		colsMap.put(colOrigine.getAttributeName(), colOrigine);
		colsMap.put(nbEcheances.getAttributeName(), nbEcheances);
		colsMap.put(montant.getAttributeName(), montant);
		colsMap.put(numeroEcrPriseEnCharge.getAttributeName(), numeroEcrPriseEnCharge);
		colsMap.put(modifieLe.getAttributeName(), modifieLe);
		colsMap.put(modifiePar.getAttributeName(), modifiePar);

	}

	public NSArray selectedObjects() {
		return myDisplayGroup.selectedObjects();
	}

	public final JToolBar buildToolBarSepaSDDEcheancierList() {
		JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);

		//Initialiser les boutons à partir des iactions
		myToolBar.add(myEcheancierToolbarListener.myActionAddEcheancier());
		myToolBar.add(myEcheancierToolbarListener.myActionModifyEcheancier());
		myToolBar.add(myEcheancierToolbarListener.myActionDeleteEcheancier());
		myToolBar.add(myEcheancierToolbarListener.myActionImprimerEcheancier());
		//Rod 30/10/2013  :on ne permet pas la prise en charge comptable de l'échéancier à partir d'ici
		//Pour les factures issues de PIE, c'est géré lors du visa du titre
		//myToolBar.add(myEcheancierToolbarListener.myActionViserEcheancier());

		myToolBar.addSeparator();
		return myToolBar;
	}

	public interface ISepaSDDEcheancierListPanelToolbarListener {
		Action myActionDeleteEcheancier();

		Action myActionViserEcheancier();

		Action myActionAddEcheancier();

		Action myActionModifyEcheancier();

		Action myActionImprimerEcheancier();
	}

	public final class NumeroPriseEnChargeProvider implements ZEOTableModelColumnProvider {

	    private static final String MSG_ECRITURE_ECHEANCIER_MANUELLE = "Gestion manuelle"; 
	    
		public NumeroPriseEnChargeProvider() {
			super();
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			final ISepaSddEcheancier echeancier = (ISepaSddEcheancier) myDisplayGroup.displayedObjects().objectAtIndex(row);
			String libelle = "";
			if (echeancier.toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
			    libelle =  EcritureDetailHelper.getSharedInstance().getNumeroEcritureAvecExerciceEtIndex(
			        SepaSddEcheancierHelper.getSharedInstance().getLastEcritureDetailPriseEnCharge(echeancier));
			} else {
			    libelle = MSG_ECRITURE_ECHEANCIER_MANUELLE;
			}
			
			return libelle;
		}
	}
}
