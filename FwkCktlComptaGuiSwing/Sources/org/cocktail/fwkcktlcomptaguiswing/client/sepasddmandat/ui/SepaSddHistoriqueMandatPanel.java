/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.TracableColFactory;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;

/**
 * Panel permettant d'afficher l'historique des modifications d'un mandat SEPA SDD.
 *
 * @author Franck Bordinat - franck.bordinat at asso-cocktail.com
 */
public final class SepaSddHistoriqueMandatPanel extends ZComptaSwingPanel {
	private static final long serialVersionUID = 1L;
	private final ISepaSddHistoriqueMandatPanelCtrl myCtrl;
	private final SepaSddHistoriqueMandatListTablePanel sepaSddHistoriqueMandatListTablePanel;


	/**
	 * @param sepaSddHistoriqueMandatPanelCtrl
	 */
	public SepaSddHistoriqueMandatPanel(ISepaSddHistoriqueMandatPanelCtrl sepaSddHistoriqueMandatPanelCtrl) {
		super();
		myCtrl = sepaSddHistoriqueMandatPanelCtrl;
		sepaSddHistoriqueMandatListTablePanel = new SepaSddHistoriqueMandatListTablePanel(myCtrl.sepaSddHistoriqueMandatListMdl());
		sepaSddHistoriqueMandatListTablePanel.initGUI();

		setLayout(new BorderLayout());

		add(buildCenterPanel(), BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);
	}


    private JPanel buildCenterPanel() {

        JPanel p = new JPanel(new BorderLayout());
        p.add(encloseInPanelWithTitle("Historique des modifications", null, ZConst.BG_COLOR_TITLE, sepaSddHistoriqueMandatListTablePanel, null, null), BorderLayout.CENTER);
        return p;
    }


	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myCtrl.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public final class SepaSddHistoriqueMandatListTablePanel extends ZTablePanel {
		private static final long serialVersionUID = 1L;
		public static final String D_CREATION_KEY = EOSepaSddMandatHisto.D_CREATION_KEY;
		public static final String LIBELLE_KEY = EOSepaSddMandatHisto.LIBELLE_KEY;
		public static final String RUM_KEY = EOSepaSddMandatHisto.RUM_KEY;
		public static final String C_TYPE_PRELEVEMENT_KEY = EOSepaSddMandatHisto.C_TYPE_PRELEVEMENT_KEY;
		public static final String CREANCIER_NOM_KEY = EOSepaSddMandatHisto.CREANCIER_NOM_KEY;
		public static final String CREANCIER_ICS_KEY = EOSepaSddMandatHisto.CREANCIER_ICS_KEY;
		public static final String DEBITEUR_NOM_KEY = EOSepaSddMandatHisto.DEBITEUR_NOM_KEY;
		public static final String DEBITEUR_BIC_KEY = EOSepaSddMandatHisto.DEBITEUR_BIC_KEY;
		public static final String DEBITEUR_IBAN_KEY = EOSepaSddMandatHisto.DEBITEUR_IBAN_KEY;

		public SepaSddHistoriqueMandatListTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn dmandatcreation = new ZEOTableModelColumn(myDisplayGroup, D_CREATION_KEY, "Date", 70);
			dmandatcreation.setAlignment(SwingConstants.LEFT);
			dmandatcreation.setFormatDisplay(ZConst.DATEISO_FORMAT_DATEHEURE);

			final ZEOTableModelColumn libelle = new ZEOTableModelColumn(myDisplayGroup, LIBELLE_KEY, "Libellé", 60);

			final ZEOTableModelColumn rum = new ZEOTableModelColumn(myDisplayGroup, RUM_KEY, "RUM", 60);
			rum.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn typePrelevement = new ZEOTableModelColumn(myDisplayGroup, C_TYPE_PRELEVEMENT_KEY, "Type de prélèvement", 60);
			typePrelevement.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn creancierNom = new ZEOTableModelColumn(myDisplayGroup, CREANCIER_NOM_KEY, "Nom du créancier", 60);

			final ZEOTableModelColumn creancierICS = new ZEOTableModelColumn(myDisplayGroup, CREANCIER_ICS_KEY, "ICS du créancier", 60);
			final ZEOTableModelColumn etat = new ZEOTableModelColumn(myDisplayGroup, EOSepaSddMandatHisto.TO_TYPE_ETAT_KEY + "." + EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, "Etat", 60);

			final ZEOTableModelColumn debiteur = new ZEOTableModelColumn(myDisplayGroup, DEBITEUR_NOM_KEY, "Nom du débiteur", 60);

			final ZEOTableModelColumn bic = new ZEOTableModelColumn(myDisplayGroup, DEBITEUR_BIC_KEY, "BIC du débiteur", 60);
			bic.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn iban = new ZEOTableModelColumn(myDisplayGroup, DEBITEUR_IBAN_KEY, "IBAN du débiteur", 60);
			ZEOTableModelColumn modifiePar = TracableColFactory.sharedInstance().getModifiePar(myDisplayGroup);


			colsMap.clear();
			colsMap.put(D_CREATION_KEY, dmandatcreation);
			colsMap.put(modifiePar.getAttributeName(), modifiePar);
			colsMap.put(LIBELLE_KEY, libelle);
			colsMap.put(RUM_KEY, rum);
			colsMap.put(C_TYPE_PRELEVEMENT_KEY, typePrelevement);
			colsMap.put(CREANCIER_NOM_KEY, creancierNom);
			colsMap.put(CREANCIER_ICS_KEY, creancierICS);
			colsMap.put(DEBITEUR_NOM_KEY, debiteur);
			colsMap.put(DEBITEUR_BIC_KEY, bic);
			colsMap.put(DEBITEUR_IBAN_KEY, iban);
			colsMap.put(etat.getAttributeName(), etat);

		}

	}

	public interface ISepaSddHistoriqueMandatPanelCtrl {
		ZTablePanel.IZTablePanelMdl sepaSddHistoriqueMandatListMdl();

		Action actionClose();

	}


	public SepaSddHistoriqueMandatListTablePanel getSepaSddMandatListTablePanel() {
		return sepaSddHistoriqueMandatListTablePanel;
	}

	/**
	 * @return l'EOSepaSddMandatHisto sélectionné
	 */
	public EOSepaSddMandatHisto selectedSepaSddMandatHisto() {
		return (EOSepaSddMandatHisto) sepaSddHistoriqueMandatListTablePanel.selectedObject();
	}

	/**
	 * @return NSArray les EOSepaSddMandatHisto sélectionnés
	 */
	public NSArray selectedSepaSddMandatHistos() {
		return sepaSddHistoriqueMandatListTablePanel.selectedObjects();
	}


	public void initGUI() {

	}

	public void updateData() throws Exception {
		sepaSddHistoriqueMandatListTablePanel.updateData();
	}
}
