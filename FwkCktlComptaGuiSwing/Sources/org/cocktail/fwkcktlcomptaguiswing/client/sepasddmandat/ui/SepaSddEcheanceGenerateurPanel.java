package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.text.Format;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZNumberField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZStringDatePickerField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;

public class SepaSddEcheanceGenerateurPanel extends ZComptaSwingPanel {

	private static final long serialVersionUID = 1L;

	private static final String LIBELLE_MONTANT = "Montant";
	private static final String LIBELLE_NB_ECHEANCES = "Nombre d'échéances";
	private static final String LIBELLE_DATE_1ERE_ECHEANCE = "Date de la première échéance";
	private static final String LIBELLE_MONTANT_1ERE_ECHEANCE = "Montant de la première échéance";

	private ISepaSddEcheanceGenerateurPanelListener listener;

	private final ZNumberField montantEcheancier;
	private final ZNumberField nbEcheances;
	private final ZStringDatePickerField datePremiereEcheance;
	private final ZNumberField montantPremiereEcheance;

	public SepaSddEcheanceGenerateurPanel(ISepaSddEcheanceGenerateurPanelListener listener) {
		super();
		this.listener = listener;

		this.montantEcheancier = ZNumberField.monetaire(listener.values(), EOSepaSddEcheancier.MONTANT_KEY);
		this.nbEcheances = new ZNumberField(
				new ZNumberField.IntegerFieldModel(listener.values(), EOSepaSddEcheancier.NB_ECHEANCES_KEY),
				new Format[] {
					ZConst.FORMAT_ENTIER_BRUT
				},
				ZConst.FORMAT_ENTIER_BRUT);
		this.datePremiereEcheance = new ZStringDatePickerField(
				new MyDatePickerFieldModel(listener.values(), EOSepaSddEcheancier.D_PREMIERE_ECHEANCE_PREVUE_KEY),
				ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		this.montantPremiereEcheance = ZNumberField.monetaire(listener.values(), ISepaSddEcheancier.MONTANT_PREMIERE_ECHEANCE_KEY);
	}

	public void prepareGuiOnOpen() throws Exception {
		initGUI();
		updateData();
	}

	@Override
	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildMainPanel(), BorderLayout.CENTER);
		this.add(buildActionsPrincipalesPanel(), BorderLayout.SOUTH);
	}

	@Override
	public void updateData() throws Exception {
		datePremiereEcheance.updateData();
		nbEcheances.updateData();
		montantEcheancier.updateData();
		montantPremiereEcheance.updateData();
	}

	private final JPanel buildMainPanel() {
		final JPanel panelInfosGenerateur = new JPanel(new BorderLayout());
		panelInfosGenerateur.add(buildInfosPanel(), BorderLayout.CENTER);
		return panelInfosGenerateur;
	}

	private final JPanel buildInfosPanel() {
		return new Builder()
				.montantEcheancier(getMontantEcheancier())
				.nbEcheances(getNbEcheances())
				.datePremiereEcheance(getDatePremiereEcheance())
				.montantPremiereEcheance(getMontantPremiereEcheance())
				.build();
	}

	private final JPanel buildActionsPrincipalesPanel() {
		List<Action> actions = new ArrayList<Action>();
		actions.add(listener.actionGenerer());
		actions.add(listener.actionCancel());

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(actions));
		return p;
	}

	public ZNumberField getMontantEcheancier() {
		return montantEcheancier;
	}

	public ZNumberField getNbEcheances() {
		return nbEcheances;
	}

	public ZNumberField getMontantPremiereEcheance() {
		return montantPremiereEcheance;
	}

	public ZStringDatePickerField getDatePremiereEcheance() {
		return datePremiereEcheance;
	}

	public static class Builder {
		private List<Component[]> formulaire = new ArrayList<Component[]>();

		public Builder montantEcheancier(Component montant) {
			formulaire.add(addLabel(LIBELLE_MONTANT, montant));
			return this;
		}

		public Builder nbEcheances(Component nbEcheances) {
			formulaire.add(addLabel(LIBELLE_NB_ECHEANCES, nbEcheances));
			return this;
		}

		public Builder datePremiereEcheance(Component datePremiereEcheance) {
			formulaire.add(addLabel(LIBELLE_DATE_1ERE_ECHEANCE, datePremiereEcheance));
			return this;
		}

		public Builder montantPremiereEcheance(Component montantPremiereEcheance) {
			formulaire.add(addLabel(LIBELLE_MONTANT_1ERE_ECHEANCE, montantPremiereEcheance));
			return this;
		}

		public JPanel build() {
			final JPanel panel = new JPanel(new BorderLayout());
			panel.add(ZUiUtil.buildForm(formulaire), BorderLayout.NORTH);
			panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
			return panel;
		}

		private Component[] addLabel(String label, Component value) {
			return new Component[] {new JLabel(label), value};
		}
	}

	private final class MyDatePickerFieldModel extends DefaultTextFieldModel implements IZDatePickerFieldModel {
		public MyDatePickerFieldModel(final Map filter, final String key) {
			super(filter, key);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	public interface ISepaSddEcheanceGenerateurPanelListener {
		Map<String, Object> values();

		// action
		Action actionCancel();
		Action actionGenerer();
	}
}
