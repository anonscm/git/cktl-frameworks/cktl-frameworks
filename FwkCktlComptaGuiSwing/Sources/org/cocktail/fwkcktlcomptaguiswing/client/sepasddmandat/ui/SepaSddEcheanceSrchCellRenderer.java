package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZHtmlUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableCellRenderer;

public class SepaSddEcheanceSrchCellRenderer extends ZEOTableCellRenderer {
	private static final long serialVersionUID = 1L;

	public SepaSddEcheanceSrchCellRenderer() {

	}

	public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		Component res = null;
		res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
		final EOSepaSddEcheance obj = (EOSepaSddEcheance) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
		if (obj.etatAsEnum().equals(Etat.ANNULE)) {
			((JLabel) res).setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + ((JLabel) res).getText() + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX);
		}
		if (isSelected) {
			res.setBackground(table.getSelectionBackground());
			res.setForeground(table.getSelectionForeground());
		}
		else {
			res.setBackground(table.getBackground());
			res.setForeground(table.getForeground());
		}

		return res;
	}
}