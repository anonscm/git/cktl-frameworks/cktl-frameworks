package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.io.Serializable;

/**
 *	Declare les contraintes UI que les fenetres IHM associées aux prelevements SEPA doivent respecter.
 *	i.e desactivation de certains champs, etc.
 */
public class SepaSddUiConstraints implements Serializable {

	/** Serial version ID */
	private static final long serialVersionUID = 1L;

	private boolean origineEcheancierSelectionEnabled;
	private boolean montantPremiereEcheanceEnabled;
	private boolean debiteurMandatSelectionEnabled;

	public SepaSddUiConstraints() {
		this.origineEcheancierSelectionEnabled = true;
		this.montantPremiereEcheanceEnabled = true;
		this.debiteurMandatSelectionEnabled = true;
	}

	public boolean isOrigineEcheancierSelectionEnabled() {
		return origineEcheancierSelectionEnabled;
	}

	public void setOrigineEcheancierSelectionEnabled(boolean origineEcheancierSelectionEnabled) {
		this.origineEcheancierSelectionEnabled = origineEcheancierSelectionEnabled;
	}

	public boolean isDebiteurMandatSelectionEnabled() {
		return debiteurMandatSelectionEnabled;
	}

	public void setDebiteurMandatSelectionEnabled(
			boolean debiteurMandatSelectionEnabled) {
		this.debiteurMandatSelectionEnabled = debiteurMandatSelectionEnabled;
	}

	public boolean isMontantPremiereEcheanceEnabled() {
		return montantPremiereEcheanceEnabled;
	}

	public void setMontantPremiereEcheanceEnabled(
			boolean montantPremiereEcheanceEnabled) {
		this.montantPremiereEcheanceEnabled = montantPremiereEcheanceEnabled;
	}

}
