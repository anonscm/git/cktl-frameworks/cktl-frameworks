package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;

public final class SepaSddMandatInfosPanel extends ZComptaSwingPanel {

	/** Serial version Id.*/
	private static final long serialVersionUID = 1L;

	public static final String LABEL_CPTE_CREANCIER = "Compte créancier : ";
	public static final String LABEL_REFERENCE_APPLICATION_CREATION = "Provenance : ";
	public static final String LABEL_LIBELLE = "Libellé (apparait sur le mandat papier) : ";
	public static final String LABEL_COMMENTAIRES = "Commentaires (n'apparait pas sur le mandat papier) : ";
	public static final String LABEL_RUM = "RUM (Référence Unique du Mandat) : ";
	public static final String LABEL_ORIGINE = "Origine du mandat : ";
	public static final String LABEL_DEBITEUR = "Débiteur (celui qui sera réellement prélevé) : ";
	public static final String LABEL_TIERS_DEBITEUR = "Tiers Débiteur : ";
	public static final String LABEL_ADRESSE = "Adresse : ";
	public static final String LABEL_COORDONNEES_BANCAIRES = "Coordonnées bancaires : ";
	public static final String LABEL_DATE = "Date du mandat SDD : ";
	public static final String LABEL_DATE_SIGNATURE_DEBITEUR = "Date de signature par le débiteur : ";
	public static final String LABEL_TYPE_PRELEVELEMENT = "Type de prélèvement : ";

	private SepaSddMandatInfosPanel(Builder builder) {
		super();
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(builder.build(), BorderLayout.CENTER);
	}

	@Override
	public void initGUI() {
	}

	@Override
	public void updateData() throws Exception {
	}

	public static class Builder {
		private List<Component[]> formulaire = new ArrayList<Component[]>();

		public Builder provenance(Component referenceApplicationCreation) {
			formulaire.add(addLabel(LABEL_REFERENCE_APPLICATION_CREATION, referenceApplicationCreation));
			return this;
		}

		public Builder compteCreancier(Component compteCreancier) {
			formulaire.add(addLabel(LABEL_CPTE_CREANCIER, compteCreancier));
			return this;
		}

		public Builder libelle(Component libelle) {
			formulaire.add(addLabel(LABEL_LIBELLE, libelle));
			return this;
		}

		public Builder commentaires(Component commentaire) {
			formulaire.add(addLabel(LABEL_COMMENTAIRES, commentaire));
			return this;
		}

		public Builder rum(Component rum) {
			formulaire.add(addLabel(LABEL_RUM, rum));
			return this;
		}

		public Builder debiteur(Component debiteur) {
			formulaire.add(addLabel(LABEL_DEBITEUR, debiteur));
			return this;
		}

		public Builder tiersDebiteur(Component tiersDebiteur) {
			formulaire.add(addLabel(LABEL_TIERS_DEBITEUR, tiersDebiteur));
			return this;
		}
		public Builder adresse(Component adresse) {
			formulaire.add(addLabel(LABEL_ADRESSE, adresse));
			return this;
		}

		public Builder coordonneesBancaires(Component coordonneesBancaires) {
			formulaire.add(addLabel(LABEL_COORDONNEES_BANCAIRES, coordonneesBancaires));
			return this;
		}

		public Builder date(Component date) {
			formulaire.add(addLabel(LABEL_DATE, date));
			return this;
		}

		public Builder dateSignatureDebiteur(Component dateSignatureDebiteur) {
			formulaire.add(addLabel(LABEL_DATE_SIGNATURE_DEBITEUR, dateSignatureDebiteur));
			return this;
		}

		public Builder typePrelevement(Component typePrelevement) {
			formulaire.add(addLabel(LABEL_TYPE_PRELEVELEMENT, typePrelevement));
			return this;
		}

		public JPanel build() {
			final JPanel panel = new JPanel(new BorderLayout());
			panel.add(ZUiUtil.buildForm(formulaire), BorderLayout.NORTH);
			panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

			return panel;
		}
		private Component[] addLabel(String label, Component value) {
			return new Component[] {
					new JLabel(label), value
			};
		}
	}

}
