package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZNumberField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZStringDatePickerField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;

public class SepaSddEcheanceSaisiePanel extends ZComptaSwingPanel {

	private static final long serialVersionUID = 1L;

	private static final String LIBELLE_MONTANT = "Montant";
	private static final String LIBELLE_DATE_PREV = "Date prévisionnelle de l'échéance";
	private static final String LIBELLE_ETAT = "Etat";

	private ISepaSddEcheanceSaisiePanelListener listener;

	// formulaire echeance
	private final JLabel etatEcheance;
	private final ZNumberField montantEcheance;
	private final ZStringDatePickerField datePrevisionnelleEcheance;


	public SepaSddEcheanceSaisiePanel(ISepaSddEcheanceSaisiePanelListener listener) {
		super();
		this.listener = listener;
		this.etatEcheance = new JLabel();
		this.montantEcheance = ZNumberField.monetaire(listener.values(), EOSepaSddEcheance.MONTANT_KEY);
		this.datePrevisionnelleEcheance = new ZStringDatePickerField(
				new MyDatePickerFieldModel(listener.values(), EOSepaSddEcheance.D_PREVUE_KEY),
				ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
	}

	public void prepareGuiOnOpen() throws Exception {
		initGUI();
		updateData();
	}

	@Override
	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildMainPanel(), BorderLayout.CENTER);
		this.add(buildActionsPrincipalesPanel(), BorderLayout.SOUTH);
	}

	@Override
	public void updateData() throws Exception {
		etatEcheance.setText(listener.values().get(EOSepaSddEcheance.ETAT_KEY).toString());
		datePrevisionnelleEcheance.updateData();
		montantEcheance.updateData();
	}

	private final JPanel buildMainPanel() {

		final JPanel panelEcheance = new JPanel(new BorderLayout());
		panelEcheance.add(buildFormPanel(), BorderLayout.CENTER);

		return panelEcheance;
	}

	private final JPanel buildFormPanel() {
		final ArrayList<Component[]> list = new ArrayList<Component[]>();
		list.add(new Component[] {
				new JLabel(LIBELLE_ETAT), etatEcheance
		});
		list.add(new Component[] {
				new JLabel(LIBELLE_DATE_PREV), datePrevisionnelleEcheance
		});
		list.add(new Component[] {
				new JLabel(LIBELLE_MONTANT), montantEcheance
		});

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildActionsPrincipalesPanel() {
		List<Action> actions = new ArrayList<Action>();
		actions.add(listener.actionValider());
		actions.add(listener.actionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(actions));
		return p;
	}

	public ZNumberField getMontantEcheance() {
		return montantEcheance;
	}

	public JLabel getEtatEcheance() {
		return etatEcheance;
	}


	public ZStringDatePickerField getDatePrevisionnelleEcheance() {
		return datePrevisionnelleEcheance;
	}

	private class MyDatePickerFieldModel extends DefaultTextFieldModel implements IZDatePickerFieldModel {
		public MyDatePickerFieldModel(final Map filter, final String key) {
			super(filter, key);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	public interface ISepaSddEcheanceSaisiePanelListener {

		Map<String, Object> values();

		// action
		Action actionClose();
		Action actionValider();
	}
}
