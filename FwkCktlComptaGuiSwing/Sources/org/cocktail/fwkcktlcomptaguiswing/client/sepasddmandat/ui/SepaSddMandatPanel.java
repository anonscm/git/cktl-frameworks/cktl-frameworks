/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.CommonFilterPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.CommonFilterPanel.ICommonSrchCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheancierListPanel.ISepaSDDEcheancierListPanelToolbarListener;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZHtmlUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;

/**
 * Panel permettant de rechercher et d'afficher des mandats SEPA SDD.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class SepaSddMandatPanel extends ZComptaSwingPanel {
	private static final long serialVersionUID = 1L;
	private final ISepaSddMandatPanelCtrl myCtrl;
	private final SepaSddMandatListTablePanel sepaSddMandatListTablePanel;
	private final SepaSddEcheancierListPanel sepaSddEcheancierListTablePanel;
	private final SepaSddEcheanceListPanel sepaSddEcheanceListTablePanel;
	private final FilterPanel filterPanel;

	/**
	 * @param editingContext
	 */
	public SepaSddMandatPanel(ISepaSddMandatPanelCtrl ctrl, ISepaSDDEcheancierListPanelToolbarListener echeancierToolbarListener) {
		super();
		myCtrl = ctrl;
		sepaSddMandatListTablePanel = new SepaSddMandatListTablePanel(myCtrl.sepaSddMandatMdl());
		sepaSddMandatListTablePanel.initGUI();

		sepaSddEcheancierListTablePanel = new SepaSddEcheancierListPanel(myCtrl.sepaSddEcheancierMdl(), echeancierToolbarListener);
		sepaSddEcheancierListTablePanel.initGUI();

		sepaSddEcheanceListTablePanel = new SepaSddEcheanceListPanel(myCtrl.sepaSddEcheanceMdl());
		sepaSddEcheanceListTablePanel.initGUI();

		filterPanel = new FilterPanel(myCtrl.panelFilterCtrl());

		setLayout(new BorderLayout());

		add(buildCenterPanel(), BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);
		add(encloseInPanelWithTitle("Critères de recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
	}

	private JPanel buildCenterPanel() {

		final JPanel resultPanel = new JPanel(new GridLayout(3, 1, 5, 5));

		JPanel panelMandat = new JPanel(new BorderLayout());
		panelMandat.add(buildToolBarSepaSDDMandatList(), BorderLayout.NORTH);
		panelMandat.add(sepaSddMandatListTablePanel, BorderLayout.CENTER);

		JPanel panelEcheancier = new JPanel(new BorderLayout());
		panelEcheancier.add(sepaSddEcheancierListTablePanel.buildToolBarSepaSDDEcheancierList(), BorderLayout.NORTH);
		panelEcheancier.add(sepaSddEcheancierListTablePanel, BorderLayout.CENTER);

		JPanel panelEcheances = new JPanel(new BorderLayout());
		panelEcheances.add(sepaSddEcheanceListTablePanel.buildToolBarSepaSDDEcheanceList(), BorderLayout.NORTH);
		panelEcheances.add(sepaSddEcheanceListTablePanel, BorderLayout.CENTER);

		resultPanel.add(encloseInPanelWithTitle("Mandats SDD", null, ZConst.BG_COLOR_TITLE, panelMandat, null, null));
		resultPanel.add(encloseInPanelWithTitle("Echéanciers", null, ZConst.BG_COLOR_TITLE, panelEcheancier, null, null));
		resultPanel.add(encloseInPanelWithTitle("Echéances", null, ZConst.BG_COLOR_TITLE, panelEcheances, null, null));
		return resultPanel;
	}

	/**
	 * @see org.cocktail.ZComptaSwingPanel.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		sepaSddMandatListTablePanel.updateData();
		sepaSddEcheancierListTablePanel.updateData();
		sepaSddEcheanceListTablePanel.updateData();
	}


	public void updateDataEcheancier() throws Exception {
		sepaSddEcheancierListTablePanel.updateData();
	}

	public final JToolBar buildToolBarSepaSDDMandatList() {
		JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);
		myToolBar.add(myCtrl.actionNouveau());
		myToolBar.add(myCtrl.actionModifier());
		myToolBar.add(myCtrl.actionSupprimer());
		myToolBar.add(myCtrl.actionImprimerMandat());
		myToolBar.add(myCtrl.actionValiderMandat());
		myToolBar.addSeparator();
		myToolBar.add(myCtrl.actionHistorique());
		myToolBar.addSeparator();
		myToolBar.add(myCtrl.ActionRefreshData());
		myToolBar.addSeparator();
		final JCheckBox cb = new JCheckBox(myCtrl.actionAfficherMandatsAnnules());
		myToolBar.add(cb);
		return myToolBar;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myCtrl.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public final class FilterPanel extends CommonFilterPanel {
		private static final long serialVersionUID = 1L;
		public final ZFormPanel rum;
		public final ZFormPanel debiteur;
		//public final ZFormPanel filterTypeOrigine;

		public FilterPanel(ICommonSrchCtrl ctrl) {
			super(ctrl);
			setLayout(new BorderLayout());
			rum = ZFormPanel.buildLabelField("RUM", new ZTextField.DefaultTextFieldModel(ctrl.filters(), EOSepaSddMandat.RUM_KEY));
			debiteur = ZFormPanel.buildLabelField("debiteur", new ZTextField.DefaultTextFieldModel(ctrl.filters(), EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY + "." + EOGrhumPersonne.NOM_AND_PRENOM_KEY));
			//filterTypeOrigine = ZFormPanel.buildLabelComboBoxField("Type origine", myCtrl.getTypeOrigineModel(), ctrl.filters(), EOSepaSddOrigineType.TYPE_NOM_KEY, null);

			((ZTextField) rum.getMyFields().get(0)).getMyTexfield().setColumns(20);
			((ZTextField) debiteur.getMyFields().get(0)).getMyTexfield().setColumns(20);

			rum.setDefaultAction(ctrl.actionSrch());
			debiteur.setDefaultAction(ctrl.actionSrch());

			final Component separator = Box.createRigidArea(new Dimension(4, 1));
			final ArrayList comps = new ArrayList();

			comps.add(ZUiUtil.buildBoxLine(new Component[] {
					separator, rum, separator, debiteur
			}));

			add(ZUiUtil.buildBoxLine(new Component[] {
					ZUiUtil.buildBoxColumn(comps)
			}), BorderLayout.WEST);
			add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

			add(new JButton(ctrl.actionSrch()), BorderLayout.EAST);

		}

		public void updateData() throws Exception {
			rum.updateData();
		}
	}

	public final class SepaSddMandatListTablePanel extends ZTablePanel {
		private static final long serialVersionUID = 1L;
		public static final String RUM_KEY = EOSepaSddMandat.RUM_KEY;
		public static final String D_MANDAT_CREATION_KEY = EOSepaSddMandat.D_MANDAT_CREATION_KEY;
		public static final String DEBITEUR_PERSONNE_KEY = EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY + "." + EOGrhumPersonne.NOM_AND_PRENOM_KEY;
		public static final String DEBITEUR_RIB = EOSepaSddMandat.TO_DEBITEUR_RIB_KEY + "." + EOGrhumRib.RIBCOMPLET_KEY;
		public static final String DEBITEUR_VILLE = EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY + "." + EOGrhumAdresse.CP_ET_VILLE_KEY;
		public static final String ETAT = EOSepaSddMandat.TO_TYPE_ETAT_KEY + "." + EOJefyAdminTypeEtat.TYET_LIBELLE_KEY;

		private SepaSddMandatListCellRenderer cellRenderer = new SepaSddMandatListCellRenderer();
		private SepaSddMandatEtatListCellRenderer cellEtatRenderer = new SepaSddMandatEtatListCellRenderer();

		public SepaSddMandatListTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn rum = new ZEOTableModelColumn(myDisplayGroup, RUM_KEY, "RUM", 60);
			rum.setAlignment(SwingConstants.CENTER);
			rum.setTableCellRenderer(cellRenderer);

			final ZEOTableModelColumn dmandatcreation = new ZEOTableModelColumn(myDisplayGroup, D_MANDAT_CREATION_KEY, "Date mandat", 60);
			dmandatcreation.setAlignment(SwingConstants.CENTER);
			dmandatcreation.setFormatDisplay(ZConst.DATEISO_FORMAT_DATESHORT);
			dmandatcreation.setTableCellRenderer(cellRenderer);

			final ZEOTableModelColumn debiteur = new ZEOTableModelColumn(myDisplayGroup, DEBITEUR_PERSONNE_KEY, "Débiteur", 60);
			debiteur.setTableCellRenderer(cellRenderer);

			final ZEOTableModelColumn rib = new ZEOTableModelColumn(myDisplayGroup, DEBITEUR_RIB, "Coordonnées bancaires", 60);
			rib.setAlignment(SwingConstants.CENTER);
			rib.setTableCellRenderer(cellRenderer);

			final ZEOTableModelColumn ville = new ZEOTableModelColumn(myDisplayGroup, DEBITEUR_VILLE, "Ville", 60);
			ville.setTableCellRenderer(cellRenderer);

			final ZEOTableModelColumn etat = new ZEOTableModelColumn(myDisplayGroup, ETAT, "Etat", 60);
			etat.setAlignment(SwingConstants.CENTER);
			etat.setTableCellRenderer(cellEtatRenderer);


			colsMap.clear();
			colsMap.put(RUM_KEY, rum);
			colsMap.put(D_MANDAT_CREATION_KEY, dmandatcreation);
			colsMap.put(DEBITEUR_PERSONNE_KEY, debiteur);
			colsMap.put(DEBITEUR_RIB, rib);
			colsMap.put(DEBITEUR_VILLE, ville);
			colsMap.put(ETAT, etat);
		}

		private class SepaSddMandatListCellRenderer extends ZEOTableCellRenderer {
			private static final long serialVersionUID = 1L;

			public SepaSddMandatListCellRenderer() {

			}

			public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
				Component res = null;
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
				final EOSepaSddMandat obj = (EOSepaSddMandat) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
				if (SepaSddMandatHelper.getSharedInstance().isAnnule(obj)) {
					((JLabel) res).setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + ((JLabel) res).getText() + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX);
				}


				return res;
			}
		}

		private class SepaSddMandatEtatListCellRenderer extends SepaSddMandatListCellRenderer {
			private static final long serialVersionUID = 1L;

			public SepaSddMandatEtatListCellRenderer() {

			}

			public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
				Component res = null;
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
				final EOSepaSddMandat obj = (EOSepaSddMandat) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
				//	res.setBackground(table.getBackground());
				//	res.setForeground(table.getForeground());

				if (SepaSddMandatHelper.getSharedInstance().isValide(obj)) {
					res.setBackground(ZConst.BGCOL_EQUILIBRE);
					res.setForeground(table.getForeground());
				}

				return res;
			}
		}

		private int getPreferredRowHeight(int rowIndex) {
			int height = myEOTable.getRowHeight();
			TableCellRenderer renderer = myEOTable.getCellRenderer(rowIndex, 3);
			Component comp = myEOTable.prepareRenderer(renderer, rowIndex, 3);
			int h = comp.getPreferredSize().height;
			height = Math.max(height, h);
			return height;
		}

		private void packRows(int start, int end) {
			for (int r = start; r < end; r++) {
				int h = getPreferredRowHeight(r);
				if (myEOTable.getRowHeight(r) != h) {
					myEOTable.setRowHeight(r, h);
				}
			}
		}

		/**
		 * Modifie la hauteur des rows en fonction du contenu.
		 */
		public void packRows() {
			if (myEOTable.getRowCount() > 0) {
				packRows(0, myEOTable.getRowCount());
			}

		}

		@Override
		public void updateData() throws Exception {
			super.updateData();
			packRows();
		}

	}

	public interface ISepaSddMandatPanelCtrl {
		public ZTablePanel.IZTablePanelMdl sepaSddMandatMdl();

		public Action actionAfficherMandatsAnnules();

		public Action actionImprimerMandat();

		public IZKarukeraTablePanelListener sepaSddEcheancierMdl();

		public IZKarukeraTablePanelListener sepaSddEcheanceMdl();

		public Action actionNouveau();

		public Action ActionRefreshData();

		public Action actionModifier();

		public Action actionSupprimer();

		public Action actionValiderMandat();

		public Action actionHistorique();

		public Action actionClose();

		public ICommonSrchCtrl panelFilterCtrl();

		public ComboBoxModel getTypeOrigineModel();

	}

	public void initGUI() {
		//obsolete

	}

	public final SepaSddMandatListTablePanel getSepaSddMandatListTablePanel() {
		return sepaSddMandatListTablePanel;
	}

	public final SepaSddEcheancierListPanel getSepaSddEcheancierListTablePanel() {
		return sepaSddEcheancierListTablePanel;
	}

	public final SepaSddEcheanceListPanel getSepaSddEcheanceListTablePanel() {
		return sepaSddEcheanceListTablePanel;
	}

	public EOSepaSddMandat selectedSepaSddMandat() {
		return (EOSepaSddMandat) sepaSddMandatListTablePanel.selectedObject();
	}

	public EOSepaSddEcheancier selectedSepaSddEcheancier() {
		return (EOSepaSddEcheancier) sepaSddEcheancierListTablePanel.selectedObject();
	}

	public NSArray selectedSepaSddMandats() {
		return sepaSddMandatListTablePanel.selectedObjects();
	}

	public void forceNewMandatSelection(ISepaSddMandat mandat) {
		if (mandat == null) {
			return;
		}
		getSepaSddMandatListTablePanel().getMyEOTable().forceNewSelectionOfObjects(new NSArray(new ISepaSddMandat[] {mandat}));
		getSepaSddMandatListTablePanel().getMyEOTable().scrollToSelection();
	}

	public void forceNewEcheancierSelection(ISepaSddEcheancier echeancier) {
		if (echeancier == null) {
			return;
		}
		getSepaSddEcheancierListTablePanel().getMyEOTable().forceNewSelectionOfObjects(new NSArray(new ISepaSddEcheancier[] {echeancier}));
		getSepaSddEcheancierListTablePanel().getMyEOTable().scrollToSelection();
	}

	public FilterPanel getFilterPanel() {
		return filterPanel;
	}

}
