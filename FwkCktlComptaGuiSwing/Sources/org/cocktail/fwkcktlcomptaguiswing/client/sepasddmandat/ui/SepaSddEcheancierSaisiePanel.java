package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZLabelTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZPanelNbTotal;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheanceListPanel.ISepaSDDEcheanceListPanelToolbarListener;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.IZDataCompModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZActionField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZLabel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOUtilities;


public class SepaSddEcheancierSaisiePanel extends ZComptaSwingPanel {

	private static final long serialVersionUID = 1L;

	private static final String LIBELLE_ORIGINE = "Origine de l'échéancier : ";
	private static final String LIBELLE_MONTANT = "Montant";
	private static final String LIBELLE_NB_ECHEANCES = "Nombre d'échéances";
	private static final String LIBELLE_DATE_1ERE_ECHEANCE = "Date de la première échéance";
	private static final String LIBELLE_MONTANT_1ERE_ECHEANCE = "Montant de la première échéance";

	private ISepaSddEcheancierSaisiePanelListener listener;

	// infos echeanciers
	private final ZActionField origineEcheancier;

	// liste echeances
	private final SepaSddEcheanceListPanel sepaSddEcheanceListTablePanel;

	private ZPanelNbTotal panelTotal1;

	public SepaSddEcheancierSaisiePanel(ISepaSddEcheancierSaisiePanelListener listener,
			ISepaSDDEcheanceListPanelToolbarListener echeanceToolbarListener) {
		super();
		this.listener = listener;

		this.origineEcheancier = new ZActionField(new OrigineLibelleModel(), listener.actionSelectOrigine());
		this.origineEcheancier.getMyTexfield().setColumns(50);
		this.origineEcheancier.getMyTexfield().setEnabled(false);

		sepaSddEcheanceListTablePanel = new SepaSddEcheanceListPanel(listener.sepaSddEcheanceMdl(), echeanceToolbarListener);
		sepaSddEcheanceListTablePanel.initGUI();
	}

	public void prepareGuiOnOpen() throws Exception {
		initGUI();
		updateData();
	}

	public ISepaSddEcheance selectedSepaSddEcheance() {
		return (ISepaSddEcheance) sepaSddEcheanceListTablePanel.selectedObject();
	}

	public final SepaSddEcheanceListPanel getSepaSddEcheanceListTablePanel() {
		return sepaSddEcheanceListTablePanel;
	}

	@Override
	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildMainPanel(), BorderLayout.CENTER);
		this.add(buildActionsPrincipalesPanel(), BorderLayout.SOUTH);
	}

	@Override
	public void updateData() throws Exception {
		origineEcheancier.updateData();
		sepaSddEcheanceListTablePanel.updateData();
		panelTotal1.updateData();
		//	labelMontantAPayer.updateData();
		//	labelNombreEcheancesPrevues.updateData();

		applyConstraints();

		//FIXME si le recouvrement a deja debuter, bloquer certaines actions
	}


	private void applyConstraints() {
		SepaSddUiConstraints constraints = listener.uiConstraints();
		listener.actionSelectOrigine().setEnabled(constraints.isOrigineEcheancierSelectionEnabled());
	}

	private final JPanel buildMainPanel() {
		buildTotalPanel();

		final JPanel resultPanel = new JPanel(new BorderLayout());

		JPanel panelMandatInfos = buildMandatInfosPanel(listener);

		JPanel panelEcheances = new JPanel(new BorderLayout());
		panelEcheances.add(sepaSddEcheanceListTablePanel.buildToolBarSepaSDDEcheanceList(), BorderLayout.NORTH);
		panelEcheances.add(sepaSddEcheanceListTablePanel, BorderLayout.CENTER);
		panelEcheances.add(panelTotal1, BorderLayout.SOUTH);

		JPanel panelEcheancier = new JPanel(new BorderLayout());
		panelEcheancier.add(buildEcheancierPanel(), BorderLayout.NORTH);
		panelEcheancier.add(encloseInPanelWithTitle("Echéances", null, ZConst.BG_COLOR_TITLE, panelEcheances, null, null), BorderLayout.CENTER);

		resultPanel.add(encloseInPanelWithTitle("Mandat", null, ZConst.BG_COLOR_TITLE, panelMandatInfos, null, null), BorderLayout.NORTH);
		resultPanel.add(encloseInPanelWithTitle("Echéancier", null, ZConst.BG_COLOR_TITLE, panelEcheancier, null, null), BorderLayout.CENTER);
		return resultPanel;
	}

	private final JPanel buildTotalPanel() {
		panelTotal1 = new ZPanelNbTotal("Total");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());
		return panelTotal1;
	}

	private final class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return listener.echeancierMontantAPayer();
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return listener.echeancierNbEcheances();
		}

		public void setValue(Object value) {
			return;
		}
	}

	public ZPanelNbTotal getPanelTotal1() {
		return panelTotal1;
	}

	private final JPanel buildMandatInfosPanel(ISepaSddEcheancierSaisiePanelListener listener) {
		return new SepaSddMandatInfosPanel.Builder()
				.provenance(new JLabel(listener.mandatReferenceAppCreation()))
				.libelle(new JLabel(listener.mandatLibelle()))
				.commentaires(new JLabel(listener.mandatCommentaire()))
				.rum(new JLabel(listener.mandatRum()))
				.debiteur(new JLabel(listener.mandatDebiteurNomPrenom()))
				.tiersDebiteur(new JLabel(listener.mandatTiersDebiteurNomPrenom()))
				.coordonneesBancaires(new JLabel(listener.mandatDebiteurBicIban()))
				.date(new JLabel(listener.mandatDate()))
				.dateSignatureDebiteur(new JLabel(listener.mandatDateSignatureDebiteur()))
				.typePrelevement(new JLabel(listener.mandatTypePrelevement()))
				.build();
	}

	private final JPanel buildEcheancierPanel() {
		return new Builder()
				.origine(origineEcheancier)
				.build();
	}

	private final JPanel buildActionsPrincipalesPanel() {
		List<Action> actions = new ArrayList<Action>();
		actions.add(listener.actionSave());
		actions.add(listener.actionCancel());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(actions));
		return p;
	}

	public static class Builder {
		private List<Component[]> formulaire = new ArrayList<Component[]>();

		public Builder origine(Component selectOrigine) {
			formulaire.add(addLabel(LIBELLE_ORIGINE, selectOrigine));
			return this;
		}

		public Builder montantEcheancier(Component montant) {
			formulaire.add(addLabel(LIBELLE_MONTANT, montant));
			return this;
		}

		public Builder nbEcheances(Component nbEcheances) {
			formulaire.add(addLabel(LIBELLE_NB_ECHEANCES, nbEcheances));
			return this;
		}

		public Builder datePremiereEcheance(Component datePremiereEcheance) {
			formulaire.add(addLabel(LIBELLE_DATE_1ERE_ECHEANCE, datePremiereEcheance));
			return this;
		}

		public Builder montantPremiereEcheance(Component montantPremiereEcheance) {
			formulaire.add(addLabel(LIBELLE_MONTANT_1ERE_ECHEANCE, montantPremiereEcheance));
			return this;
		}

		public JPanel build() {
			final JPanel panel = new JPanel(new BorderLayout());
			panel.add(ZUiUtil.buildForm(formulaire), BorderLayout.NORTH);
			panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
			return panel;
		}

		private Component[] addLabel(String label, Component value) {
			return new Component[] {
					new JLabel(label), value
			};
		}
	}

	private final class OrigineLibelleModel implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			if (listener.values().get(ISepaSddEcheancierSaisiePanelListener.ORIGINE_KEY) != null) {
				String origineLibelle = new StringBuilder(listener.origineType().typeNom())
					.append(" : ")
					.append(listener.origineEntity().getOrigineLibelleComplet())
						.append(" (" + listener.origineEntity().origineDebiteurNomComplet() + ")")
					.toString();
				return origineLibelle;
			}

			return null;
		}

		public void setValue(Object value) {

		}
	}

	private final class MontantAPayerModel implements IZDataCompModel {
		public Object getValue() {
			return listener.echeancierMontantAPayer();
		}

		public void setValue(Object value) {
		}
	}

	private final class NombreEcheancesPrevuesModel implements IZDataCompModel {
		public Object getValue() {
			return listener.echeancierNbEcheances();
		}

		public void setValue(Object value) {
		}
	}

	public interface ISepaSddEcheancierSaisiePanelListener {
		static final String ORIGINE_KEY = "origine";
		static final String ORIGINE_TYPE_KEY = "origineType";

		Map<String, Object> values();

		String mandatCommentaire();

		// attributs types
		ISepaSddOrigine echeancierOrigine();

		BigDecimal echeancierMontantAPayer();

		Integer echeancierNbEcheances();

		String mandatReferenceAppCreation();
		String mandatLibelle();
		String mandatRum();
		String mandatDebiteurNomPrenom();
		String mandatTiersDebiteurNomPrenom();
		String mandatDebiteurBicIban();
		String mandatDate();
		String mandatDateSignatureDebiteur();
		String mandatTypePrelevement();

		ISepaSddOrigineEntity origineEntity();
		ISepaSddOrigineType origineType();

		// action
		Action actionCancel();
		Action actionSave();
		Action actionSelectOrigine();

		// ui
		IZKarukeraTablePanelListener sepaSddEcheanceMdl();
		SepaSddUiConstraints uiConstraints();
	}
}
