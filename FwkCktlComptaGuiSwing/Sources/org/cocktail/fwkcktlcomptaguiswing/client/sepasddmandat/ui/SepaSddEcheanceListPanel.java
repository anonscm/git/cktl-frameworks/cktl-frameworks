/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.Action;
import javax.swing.JToolBar;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.TracableColFactory;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;

/**
 * Affiche une liste d'échéances pour un échéancier de mandat SDD SEPA.
 * 
 * @author Franck Bordinat
 */
public class SepaSddEcheanceListPanel extends ZComptaSwingTablePanel {

	private static final long serialVersionUID = 1L;
	private final SepaSddEcheanceSrchCellRenderer sepaSddEcheanceSrchCellRenderer = new SepaSddEcheanceSrchCellRenderer();
	private final SepaSddEcheanceEtatCellRenderer sepaSddEcheanceEtatCellRenderer = new SepaSddEcheanceEtatCellRenderer();
	private final ISepaSDDEcheanceListPanelToolbarListener myEcheanceToolbarListener;
	private List<ZEOTableModelColumn> zeoTableModelColumnDisponibles = new ArrayList<ZEOTableModelColumn>();;

	public SepaSddEcheanceListPanel(IZKarukeraTablePanelListener listener) {
		this(listener, new DefaultEcheanceListPanelToolbarListener());
	}

	public SepaSddEcheanceListPanel(IZKarukeraTablePanelListener listener, ISepaSDDEcheanceListPanelToolbarListener echeanceToolbarListener) {
		super(listener);

		myEcheanceToolbarListener = echeanceToolbarListener;

		ZEOTableModelColumn montant = SepaSddEcheanceColFactory.sharedInstance().getMontant(myDisplayGroup);
		ZEOTableModelColumn dateprevue = SepaSddEcheanceColFactory.sharedInstance().getDatePrevue(myDisplayGroup);
		ZEOTableModelColumn etat = SepaSddEcheanceColFactory.sharedInstance().getEtat(myDisplayGroup);
		ZEOTableModelColumn typeOp = SepaSddEcheanceColFactory.sharedInstance().getType(myDisplayGroup);
		ZEOTableModelColumn modifiePar = TracableColFactory.sharedInstance().getModifiePar(myDisplayGroup);
		ZEOTableModelColumn modifieLe = TracableColFactory.sharedInstance().getModifieLe(myDisplayGroup);
		ZEOTableModelColumn datePrelev = SepaSddEcheanceColFactory.sharedInstance().getDatePrelev(myDisplayGroup);

		zeoTableModelColumnDisponibles.add(montant);
		zeoTableModelColumnDisponibles.add(dateprevue);
		zeoTableModelColumnDisponibles.add(etat);
		zeoTableModelColumnDisponibles.add(datePrelev);
		zeoTableModelColumnDisponibles.add(typeOp);
		zeoTableModelColumnDisponibles.add(modifiePar);
		zeoTableModelColumnDisponibles.add(modifieLe);

		for (ZEOTableModelColumn col : this.getZeoTableModelColumnDisponibles()) {
			col.setTableCellRenderer(getTableCellRenderer());
		}
		etat.setTableCellRenderer(sepaSddEcheanceEtatCellRenderer);
		colsMap.clear();
		colsMap.put(dateprevue.getAttributeName(), dateprevue);
		colsMap.put(montant.getAttributeName(), montant);
		colsMap.put(etat.getAttributeName(), etat);
		colsMap.put(datePrelev.getAttributeName(), datePrelev);
		colsMap.put(typeOp.getAttributeName(), typeOp);
		colsMap.put(modifieLe.getAttributeName(), modifieLe);
		colsMap.put(modifiePar.getAttributeName(), modifiePar);

	}

	public List<ZEOTableModelColumn> getZeoTableModelColumnDisponibles() {
		return zeoTableModelColumnDisponibles;
	}

	public NSArray selectedObjects() {
		return myDisplayGroup.selectedObjects();
	}

	public final JToolBar buildToolBarSepaSDDEcheanceList() {
		JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);

		//Initialiser les boutons à partir des iactions
		for (Action action : myEcheanceToolbarListener.actionsDisponibles()) {
			myToolBar.add(action);
		}
		myToolBar.addSeparator();

		return myToolBar;
	}

	public interface ISepaSDDEcheanceListPanelToolbarListener {
		List<Action> actionsDisponibles();

		Action myActionDeleteEcheance();

		//		Action myActionAddEcheance();

		Action myActionModifyEcheance();

		Action myActionGenerateEcheances();
	}

	private static final class DefaultEcheanceListPanelToolbarListener implements ISepaSDDEcheanceListPanelToolbarListener {
		public List<Action> actionsDisponibles() {
			return Collections.emptyList();
		}

		public Action myActionDeleteEcheance() {
			return null;
		}

		public Action myActionAddEcheance() {
			return null;
		}

		public Action myActionModifyEcheance() {
			return null;
		}

		public Action myActionGenerateEcheances() {
			return null;
		}

	}

	public IZEOTableCellRenderer getTableCellRenderer() {
		return sepaSddEcheanceSrchCellRenderer;
	}
}
