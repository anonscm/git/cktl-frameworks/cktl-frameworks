package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;

public class SepaSddEcheanceColFactory {
	public static SepaSddEcheanceColFactory sharedInstance() {
		return new SepaSddEcheanceColFactory();
	}

	//	public ZEOTableModelColumn getNumero(EODisplayGroup displayGroup) {
	//		ZEOTableModelColumn numero = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.NUMERO_KEY, "N°", 10);
	//		numero.setAlignment(SwingConstants.CENTER);
	//		numero.setColumnClass(Integer.class);
	//		return numero;
	//	}
	//
	//	public ZEOTableModelColumn getNumeroSurNombre(EODisplayGroup displayGroup) {
	//		ZEOTableModelColumn numero = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.NUMANDTOTAL_KEY, "N°", 10);
	//		numero.setAlignment(SwingConstants.CENTER);
	//		numero.setColumnClass(Integer.class);
	//		return numero;
	//	}

	public ZEOTableModelColumn getMontant(EODisplayGroup displayGroup) {
		ZEOTableModelColumn montant = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.MONTANT_KEY, "Montant", 40);
		montant.setAlignment(SwingConstants.RIGHT);
		montant.setColumnClass(BigDecimal.class);
		montant.setFormatDisplay(ZConst.FORMAT_DECIMAL);
		return montant;
	}

	public ZEOTableModelColumn getDatePrevue(EODisplayGroup displayGroup) {
		ZEOTableModelColumn dateprevue = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.D_PREVUE_KEY, "Date échéance", 40);
		dateprevue.setAlignment(SwingConstants.CENTER);
		dateprevue.setFormatDisplay(ZConst.DATEISO_FORMAT_DATESHORT);
		return dateprevue;
	}

	public ZEOTableModelColumn getDatePrelev(EODisplayGroup displayGroup) {
		ZEOTableModelColumn datePrelev = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.D_PRELEVE_KEY, "Date prélèvement", 40);
		datePrelev.setAlignment(SwingConstants.CENTER);
		datePrelev.setFormatDisplay(ZConst.DATEISO_FORMAT_DATESHORT);
		return datePrelev;
	}

	public ZEOTableModelColumn getEtat(EODisplayGroup displayGroup) {
		ZEOTableModelColumn etat = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.ETAT_KEY, "Etat", 20);
		etat.setAlignment(SwingConstants.CENTER);
		return etat;
	}

	public ZEOTableModelColumn getOrigine(EODisplayGroup displayGroup) {
		ZEOTableModelColumn colOrigine = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY + "." + EOSepaSddOrigine.TYPE_AND_LIBELLE_COMPLET_OF_ORIGINE_KEY, "Origine",
				90);
		colOrigine.setAlignment(SwingConstants.LEFT);
		return colOrigine;
	}

	public ZEOTableModelColumn getDebiteur(EODisplayGroup displayGroup) {
		ZEOTableModelColumn colDebiteur = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY + "." + EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY + "." + EOGrhumPersonne.NOM_AND_PRENOM_KEY, "Débiteur", 90);
		colDebiteur.setAlignment(SwingConstants.LEFT);
		return colDebiteur;
	}

	public ZEOTableModelColumn getRib(EODisplayGroup displayGroup) {
		ZEOTableModelColumn colRib = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY + "." + EOSepaSddMandat.TO_DEBITEUR_RIB_KEY + "." + EOGrhumRib.RIBCOMPLET_KEY, "Rib", 60);
		colRib.setAlignment(SwingConstants.CENTER);
		return colRib;
	}

	public ZEOTableModelColumn getType(EODisplayGroup displayGroup) {
		ZEOTableModelColumn colType = new ZEOTableModelColumn(displayGroup, EOSepaSddEcheance.SDD_TYPE_OP_KEY, "Type", 20);
		colType.setAlignment(SwingConstants.CENTER);
		return colType;
	}


}
