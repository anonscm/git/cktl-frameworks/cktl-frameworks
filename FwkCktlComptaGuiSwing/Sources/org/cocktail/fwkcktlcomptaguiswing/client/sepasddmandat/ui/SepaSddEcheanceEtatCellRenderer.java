package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZHtmlUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableCellRenderer;

final class SepaSddEcheanceEtatCellRenderer extends ZEOTableCellRenderer {
	public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		Component res = null;
		res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
		final EOSepaSddEcheance obj = (EOSepaSddEcheance) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
		res.setBackground(table.getBackground());
		res.setForeground(table.getForeground());

		if (obj.etatAsEnum().equals(Etat.ANNULE)) {
			((JLabel) res).setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + ((JLabel) res).getText() + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX);
		}
		else if (obj.etatAsEnum().equals(Etat.PRELEVE)) {
			res.setBackground(ZConst.BGCOL_EQUILIBRE);
		}
		else if (obj.etatAsEnum().equals(Etat.REJETE)) {
			res.setBackground(ZConst.BGCOL_DESEQUILIBRE);
		}

		return res;
	}
}