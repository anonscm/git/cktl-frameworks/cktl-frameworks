package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui;

import java.awt.Component;

import javax.swing.JTable;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableCellRenderer;

final class SepaSddEcheancierPriseEnChargeCellRenderer extends ZEOTableCellRenderer {
    
	public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		Component res = null;
		res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
		final EOSepaSddEcheancier echeancier = (EOSepaSddEcheancier) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
		res.setBackground(table.getBackground());
		res.setForeground(table.getForeground());

		if (SepaSddEcheancierHelper.getSharedInstance().isEcheancierPrisEnCharge(echeancier)) {
			res.setBackground(ZConst.BGCOL_EQUILIBRE);
		} else if (!echeancier.toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
		    res.setBackground(ZConst.BGCOLOR_YELLOW);
		}

		return res;
	}
}