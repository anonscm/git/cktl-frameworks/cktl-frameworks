package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.client.sepasdd.helpers.SepaSddOrigineClientHelper;
import org.cocktail.fwkcktlcompta.common.exception.DataCheckException;
import org.cocktail.fwkcktlcompta.common.exception.DefaultClientException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.exceptions.InvalidEcheancierException;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.TriEcheancesParDatePrevue;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheanceRule;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheancierRule;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheanceListPanel.ISepaSDDEcheanceListPanelToolbarListener;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheancierSaisiePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheancierSaisiePanel.ISepaSddEcheancierSaisiePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddUiConstraints;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddorigine.ctrl.SepaSddOrigineSelectCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZCommonDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZMsgPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class SepaSddEcheancierSaisieCtrl extends CommonCtrl {

	private static final String ERR_AUCUN_ECHEANCIER_SELECTIONNE = "Aucun échéancier n'est sélectionné.";
	private static final String ERR_ECHEANCE_SUPPRIMABLE = "Cette échéance ne peut-être supprimée";
	private static final String ERR_ECHEANCE_MODIFIABLE = "Cette échéance ne peut-être modifiée";
	private static final String ERR_AUCUNE_ECHEANCE_SELECTIONNEE = "Aucune échéance n'est sélectionnée.";
	private static final String ERR_ORIGINE_ECHEANCIER_OBLIGATOIRE = "L'origine de l'échéancier est obligatoire.";
	private static final String ERR_AUCUNES_ECHEANCES = "L'échéancier doit contenir au moins une échéance. Merci de générer les échéances.";
	private static final String ERR_ECHEANCES_NON_SUPPRIMABLES = "Action annulée car l'une des échéances n'est pas supprimable.";
	private static final String TITLE_SUPPRESSION_ECHEANCE = "Supprimer échéance";
	private static final String MSG_SUPPRESSION_ECHEANCE = "Etes-vous sûr de vouloir supprimer cette échéance ?";
	private static final String MSG_REPORTER_ECHEANCE = "Essayer de reporter le montant sur l'échéance suivante en attente.";

	private static final String TITLE = "Saisie d'un échéancier SEPA SDD";
	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 700);

	private SepaSddEcheancierSaisiePanel sepaSddEcheancierSaisiePanel;
	private SepaSddUiConstraints uiConstraints;
	private SepaSddEcheanceListMdl sepaSddEcheanceMdl;
	private IGenerateurEcheancier generateurEcheancier;

	private ISepaSddEcheancier currentEcheancier;
	private Map<String, Object> currentDico;

	private Action actionSelectOrigine = new ActionSelectOrigine();
	private Action actionCancel = new ActionCancel();
	private Action actionSave = new ActionSave();
	private Action actionAjoutEcheance = new ActionAddEcheance();
	private Action actionModificationEcheance = new ActionModifyEcheance();
	private Action actionSuppressionEcheance = new ActionDeleteEcheance();
	private Action actionGenerationEcheances = new ActionGenererEcheances();

	/**
	 * Constructeur.
	 *
	 * @param editingContext editing Context associe a ce controlleur.
	 */
	public SepaSddEcheancierSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);
		this.currentDico = new HashMap<String, Object>();
		this.generateurEcheancier = null;
		this.sepaSddEcheanceMdl = new SepaSddEcheanceListMdl();
		this.sepaSddEcheancierSaisiePanel = new SepaSddEcheancierSaisiePanel(
				new SepaSddEcheancierSaisiePanelListener(),
				new SepaSDDEcheanceListPanelToolbarListener());
		this.uiConstraints = new SepaSddUiConstraints();
	}

	public final int openDialogModify(Window parent, ISepaSddEcheancier echeancier) {
		return openDialogModify(parent, echeancier, generateurEcheancier, uiConstraints, null, null);
	}

	public final int openDialogModify(Window parent, ISepaSddEcheancier echeancier, IGenerateurEcheancier generateurEcheancier,
			SepaSddUiConstraints uiConstraints, ISepaSddOrigineEntity origineEntity, String codeOrigineType) {
		final ZKarukeraDialog win = createDialog(parent, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			this.currentEcheancier = echeancier;

			if (generateurEcheancier != null) {
				this.generateurEcheancier = generateurEcheancier;
			}

			if (uiConstraints != null) {
				this.uiConstraints = uiConstraints;
			}

			if (origineEntity != null && codeOrigineType != null) {
				ISepaSddOrigineType origineType = SepaSddOrigineClientHelper.getSharedInstance()
						.fetchOrigineTypeValide(getEditingContext(), codeOrigineType);
				ISepaSddOrigine origineRefApplication = SepaSddOrigineHelper.getSharedInstance()
						.creerOuRecupererOrigine(getEditingContext(), origineType, origineEntity);
				this.currentEcheancier.setToSepaSddOrigineRelationship(origineRefApplication);
			}

			updateDicoWithObject();

			sepaSddEcheancierSaisiePanel.prepareGuiOnOpen();
			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);

		checkSaisieOrigine();
		checkSaisieEcheances();
	}

	private final void checkSaisieOrigine() throws Exception {
		ISepaSddOrigineType origineType = (ISepaSddOrigineType) currentDico.get(ISepaSddEcheancierSaisiePanelListener.ORIGINE_TYPE_KEY);
		ISepaSddOrigineEntity origineEntity = (ISepaSddOrigineEntity) currentDico.get(ISepaSddEcheancierSaisiePanelListener.ORIGINE_KEY);

		if (origineType == null || origineEntity == null) {
			throw new DataCheckException(ERR_ORIGINE_ECHEANCIER_OBLIGATOIRE);
		}
	}

	private final void checkSaisieEcheances() throws Exception {
		if (!SepaSddEcheancierHelper.getSharedInstance().hasEcheances(getCurrentEcheancier())) {
			throw new DataCheckException(ERR_AUCUNES_ECHEANCES);
		}
	}

	private final void checkEcheancesSupprimables() throws Exception {
		SepaSddEcheancierRule.getSharedInstance().checkEcheancesSupprimables(getCurrentEcheancier());
	}

	private final void validerSaisie() {
		try {
			setWaitCursor(true);
			checkSaisie();
			updateObjectWithDico();

			getCurrentEcheancier().setToModificateurPersonne(myApp.appUserInfo().getGrhumPersonne());
			boolean goon = true;
			StringBuilder msg = new StringBuilder();
			if (SepaSddEcheancierRule.getSharedInstance().isPremiereEcheanceDateEcheanceAnterieureADatePremiereEcheanceSurMemeMandatPlusDelai(getEditingContext(), getCurrentEcheancier(), msg)) {
				goon = showConfirmationDialog("Confirmation", msg.toString() + "\nSouhaitez-vous enregistrer cet échéancier ? ", ZMsgPanel.BTLABEL_NO);
			}

			if (goon) {
				if (getEditingContext().hasChanges()) {
					getEditingContext().saveChanges();
				}
				getMyDialog().onOkClick();
				//ne pas faire de revert lors d'une exception, sinon on perd l'editingContext sur le currentObject
				getEditingContext().revert();
			}

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		}
 finally {
			setWaitCursor(false);
		}
	}

	private void validerEcheancier(IGenerateurEcheancier generateurEcheancier, ISepaSddEcheancier echeancier) throws InvalidEcheancierException {
		try {
			generateurEcheancier.validerEcheancier(echeancier);
		} catch (InvalidEcheancierException iee) {
			if (echeancier.echeances() != null) {
				Iterator<ISepaSddEcheance> iteEcheances = echeancier.echeances().iterator();
				while (iteEcheances.hasNext()) {
					getEditingContext().deleteObject(iteEcheances.next().toEnterpriseObject());
					iteEcheances.remove();
				}
			}
			throw iee;
		}
	}

	private void updateObjectWithDico() throws Exception {

	}

	private void updateObjectWithOrigine() throws Exception {
		currentEcheancier.setToSepaSddOrigineRelationship(prepareOrigine(currentEcheancier, currentDico));
	}

	private ISepaSddOrigine prepareOrigine(ISepaSddEcheancier echeancier, Map<String, Object> valeursSaisies) throws Exception {
		ISepaSddOrigine origine = echeancier.toSepaSddOrigine();
		ISepaSddOrigineType origineType = (ISepaSddOrigineType) valeursSaisies.get(ISepaSddEcheancierSaisiePanelListener.ORIGINE_TYPE_KEY);
		ISepaSddOrigineEntity origineEntity = (ISepaSddOrigineEntity) valeursSaisies.get(ISepaSddEcheancierSaisiePanelListener.ORIGINE_KEY);

		if (origine == null || !(origine.toSepaSddOrigineType().equals(origineType) && origine.toEntity().equals(origineEntity))) {
			origine = SepaSddOrigineHelper.getSharedInstance().creerOuRecupererOrigine(getEditingContext(), origineType, origineEntity);
		}
		return origine;
	}

	private void updateDicoWithObject() {
		DateConversionUtil dcu = new DateConversionUtil();
		currentDico.clear();

		currentDico.put(EOSepaSddMandat.REF_APPLI_CREATION_KEY, currentEcheancier.mandat().refAppliCreation());
		currentDico.put(EOSepaSddMandat.LIBELLE_KEY, currentEcheancier.mandat().libelle());
		currentDico.put(EOSepaSddMandat.COMMENTAIRE_KEY, currentEcheancier.mandat().commentaire());
		currentDico.put(EOSepaSddMandat.RUM_KEY, currentEcheancier.mandat().rum());
		currentDico.put(ISepaSddMandat.DEBITEUR_NOM_PRENOM, currentEcheancier.mandat().nomPrenomDebiteur());
		currentDico.put(ISepaSddMandat.DEBITEUR_BIC_IBAN, currentEcheancier.mandat().bicIbanDebiteur());
		currentDico.put(ISepaSddMandat.TIERS_DEBITEUR_NOM_PRENOM, currentEcheancier.mandat().tiersDebiteurNomPrenom());
		currentDico.put(EOSepaSddMandat.D_MANDAT_CREATION_KEY, currentEcheancier.mandat().dMandatCreation());
		currentDico.put(EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY, currentEcheancier.mandat().dMandatSignature());
		currentDico.put(EOSepaSddMandat.C_TYPE_PRELEVEMENT_KEY, currentEcheancier.mandat().cTypePrelevement());

		if (currentEcheancier.toSepaSddOrigine() != null) {
			updateDicoWithOrigine(currentDico, currentEcheancier.toSepaSddOrigine());
		}

		currentDico.put(EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY, currentEcheancier.toSepaSddOrigine());
		currentDico.put(EOSepaSddEcheancier.MONTANT_KEY, currentEcheancier.montantAPayer());
		currentDico.put(EOSepaSddEcheancier.NB_ECHEANCES_KEY, currentEcheancier.nombreEcheancesPrevues());
		currentDico.put(EOSepaSddEcheancier.D_PREMIERE_ECHEANCE_PREVUE_KEY, dcu.parseDateSilent(currentEcheancier.datePremiereEcheance()));
		currentDico.put(EOSepaSddEcheancier.MONTANT_PREMIERE_ECHEANCE_KEY, currentEcheancier.montantPremiereEcheance());
	}

	private void updateDicoWithOrigine(Map<String, Object> dico, ISepaSddOrigine sepaSddOrigine) {
		ISepaSddOrigineEntity entiteOrigine = sepaSddOrigine.toEntity();
		dico.put(ISepaSddEcheancierSaisiePanelListener.ORIGINE_TYPE_KEY, sepaSddOrigine.toSepaSddOrigineType());
		dico.put(ISepaSddEcheancierSaisiePanelListener.ORIGINE_KEY, entiteOrigine);
		dico.put(ISepaSddEcheancier.MONTANT_KEY, entiteOrigine.origineMontant());
	}

	protected void onCancel() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	@Override
	public String title() {
		return TITLE;
	}

	@Override
	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	@Override
	public ZAbstractSwingPanel mainPanel() {
		return sepaSddEcheancierSaisiePanel;
	}

	public ISepaSddEcheancier getCurrentEcheancier() {
		return currentEcheancier;
	}

	private ISepaSddEcheance getSelectedEcheanceSdd() {
		return sepaSddEcheancierSaisiePanel.selectedSepaSddEcheance();
	}

	private void onModifyEcheance() {
		setWaitCursor(true);
		try {
			if (currentEcheancier == null) {
				throw new DefaultClientException(ERR_AUCUN_ECHEANCIER_SELECTIONNE);
			}

			if (getSelectedEcheanceSdd() != null) {

				if (!getSelectedEcheanceSdd().estModifiable()) {
					throw new DefaultClientException(ERR_ECHEANCE_MODIFIABLE);
				}

				final SepaSddEcheanceSaisieCtrl echeanceSaisieCtrl = new SepaSddEcheanceSaisieCtrl(getEditingContext());
				getSelectedEcheanceSdd().setToModificateurPersonne(myApp.appUserInfo().getGrhumPersonne());
				echeanceSaisieCtrl.openDialogModify(getMyDialog(), getSelectedEcheanceSdd());

				mainPanel().updateData();
			}

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}

	}

	private void onAddEcheance() {
		setWaitCursor(true);
		try {
			if (currentEcheancier == null) {
				throw new DefaultClientException(ERR_AUCUN_ECHEANCIER_SELECTIONNE);
			}

	
			ISepaSddEcheance echeance = SepaSddEcheancierHelper.getSharedInstance().creerNouvelleEcheance(currentEcheancier, new LocalDate(),
					BigDecimal.ZERO, myApp.appUserInfo().getGrhumPersonne());
			
			final SepaSddEcheanceSaisieCtrl echeanceSaisieCtrl = new SepaSddEcheanceSaisieCtrl(getEditingContext());
			echeanceSaisieCtrl.openDialogModify(getMyDialog(), echeance);

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			try {
				mainPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}
	
	private void onDeleteEcheance() {
		setWaitCursor(true);
		try {
			if (getSelectedEcheanceSdd() == null) {
				throw new DefaultClientException(ERR_AUCUNE_ECHEANCE_SELECTIONNEE);
			}
			SepaSddEcheanceRule.getSharedInstance().checkEcheanceSupprimable(getSelectedEcheanceSdd());

			if (showConfirmationDialog(TITLE_SUPPRESSION_ECHEANCE, MSG_SUPPRESSION_ECHEANCE, ZMsgPanel.BTLABEL_NO)) {
				SepaSddEcheancierHelper.getSharedInstance().annulerEcheance(getSelectedEcheanceSdd());
				if (showConfirmationDialog(TITLE_SUPPRESSION_ECHEANCE, MSG_REPORTER_ECHEANCE, ZMsgPanel.BTLABEL_YES)) {
					SepaSddEcheancierHelper.getSharedInstance().reporteMontantEcheanceSurSuivanteEnAttente(getSelectedEcheanceSdd());
				}


				updateDicoWithObject();
				updateEcheancierGui();
			}
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onGenererEcheances() {
		setWaitCursor(true);
		try {

			checkEcheancesSupprimables();
			checkSaisieOrigine();

			SepaSddEcheanceGenerateurCtrl echeanceGenerateurCtrl = new SepaSddEcheanceGenerateurCtrl(getEditingContext());
			echeanceGenerateurCtrl.openDialogModify(getMyDialog(), getCurrentEcheancier());

			updateDicoWithObject();
			updateEcheancierGui();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			try {
				mainPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	private void selectionnerOrigine() {
		setWaitCursor(true);
		try {
			SepaSddOrigineSelectCtrl sepaSddOrigineSelectCtrl = new SepaSddOrigineSelectCtrl(
					getEditingContext(), getCurrentEcheancier().mandat().clientPersonne());
			int mr = sepaSddOrigineSelectCtrl.openDialog(getMyDialog(), true);
			if (mr == ZCommonDialog.MROK) {
				ISepaSddOrigineEntity sddOrigineEntity = sepaSddOrigineSelectCtrl.getSelectedSepaSddOrigineEntity();
				EOSepaSddOrigineType origineType = sepaSddOrigineSelectCtrl.getSelectedSepaSddOrigineType();
				currentDico.put(ISepaSddEcheancierSaisiePanelListener.ORIGINE_KEY, sddOrigineEntity);
				currentDico.put(ISepaSddEcheancierSaisiePanelListener.ORIGINE_TYPE_KEY, origineType);
				updateObjectWithOrigine();
				mainPanel().updateData();
			}

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void updateEcheancierGui() throws Exception {
		refreshActionsToolbarEcheancier();
		mainPanel().updateData();
	}

	private final void refreshActionsToolbarEcheancier() {
		if (getSelectedEcheanceSdd() != null) {
			actionModificationEcheance.setEnabled(getSelectedEcheanceSdd().estModifiable());
			actionSuppressionEcheance.setEnabled(getSelectedEcheanceSdd().estAnnulable());
			actionGenerationEcheances.setEnabled(true);
		}
	}

	/**************************/
	/**** CLASSES INTERNES ****/
	/**************************/
	public final class ActionSave extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionSave() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}
	}

	public final class ActionCancel extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionCancel() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		public void actionPerformed(ActionEvent e) {
			onCancel();
		}
	}

	private final class SepaSddEcheancierSaisiePanelListener implements SepaSddEcheancierSaisiePanel.ISepaSddEcheancierSaisiePanelListener {

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionSave() {
			return actionSave;
		}

		public Action actionSelectOrigine() {
			return actionSelectOrigine;
		}

		public Map<String, Object> values() {
			return currentDico;
		}

		public ISepaSddOrigine echeancierOrigine() {
			return (ISepaSddOrigine) values().get(EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY);
		}

		public BigDecimal echeancierMontantAPayer() {
			return currentEcheancier.montantAPayer();
		}

		public Integer echeancierNbEcheances() {
			return currentEcheancier.nombreEcheancesPrevues();
		}

		public String mandatLibelle() {
			String libelle = (String) values().get(EOSepaSddMandat.LIBELLE_KEY);
			if (libelle == null) {
				return ZConst.LIBELLE_NON_DEFINI;
			}

			return libelle;
		}

		public String mandatReferenceAppCreation() {
			String refAppCreation = (String) values().get(EOSepaSddMandat.REF_APPLI_CREATION_KEY);
			if (refAppCreation == null) {
				return ZConst.LIBELLE_NON_DEFINI;
			}
			return refAppCreation;
		}

		public String mandatRum() {
			return (String) values().get(EOSepaSddMandat.RUM_KEY);
		}

		public ISepaSddOrigineEntity origineEntity() {
			return (ISepaSddOrigineEntity) values().get(SepaSddEcheancierSaisiePanel.ISepaSddEcheancierSaisiePanelListener.ORIGINE_KEY);
		}

		public ISepaSddOrigineType origineType() {
			return (ISepaSddOrigineType) values().get(SepaSddEcheancierSaisiePanel.ISepaSddEcheancierSaisiePanelListener.ORIGINE_TYPE_KEY);
		}

		public String mandatDebiteurNomPrenom() {
			return (String) values().get(ISepaSddMandat.DEBITEUR_NOM_PRENOM);
		}

		public String mandatDebiteurBicIban() {
			return (String) values().get(ISepaSddMandat.DEBITEUR_BIC_IBAN);
		}

		public String mandatDate() {
			return ZConst.DATEISO_FORMAT_DATESHORT.format(
			    (String) values().get(EOSepaSddMandat.D_MANDAT_CREATION_KEY));
		}

		public String mandatDateSignatureDebiteur() {
		    if (values().get(EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY) == null) {
	            return ZConst.LIBELLE_NON_DEFINI;
	        }
	        return ZConst.DATEISO_FORMAT_DATESHORT.format(
	            (String) values().get(EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY));
		}

		public String mandatTypePrelevement() {
			return (String) values().get(EOSepaSddMandat.C_TYPE_PRELEVEMENT_KEY);
		}

		public IZKarukeraTablePanelListener sepaSddEcheanceMdl() {
			return sepaSddEcheanceMdl;
		}

		public SepaSddUiConstraints uiConstraints() {
			return uiConstraints;
		}

		public String mandatTiersDebiteurNomPrenom() {
			if (values().get(ISepaSddMandat.TIERS_DEBITEUR_NOM_PRENOM) == null) {
				return ZConst.LIBELLE_NON_DEFINI;
			}
			return (String) values().get(ISepaSddMandat.TIERS_DEBITEUR_NOM_PRENOM);
		}

		public String mandatCommentaire() {
			return (String) values().get(EOSepaSddMandat.COMMENTAIRE_KEY);
		}
	}

	// TODO a mutualiser avec celle de SepaSddMandatCtrl
	private final class SepaSddEcheanceListMdl implements IZKarukeraTablePanelListener {

		public void selectionChanged() {
			refreshActionsToolbarEcheancier();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		/**
		 * Retourne la liste des echeances associees à l'échéancier selectionné.
		 */
		public NSArray getData() throws Exception {
			ISepaSddEcheancier echeancierSelectionne = getCurrentEcheancier();
			if (echeancierSelectionne == null) {
				return new NSArray();
			}

			List<ISepaSddEcheance> echeances = echeancierSelectionne.echeances();
			Collections.sort(echeances, new TriEcheancesParDatePrevue());
			return WebObjectConversionUtil.asRawNSArray(echeances);
		}

		public void onDbClick() {
			onModifyEcheance();
		}
	}

	private final class SepaSDDEcheanceListPanelToolbarListener implements ISepaSDDEcheanceListPanelToolbarListener {
		private List<Action> actionsDisponibles = new ArrayList<Action>();

		public SepaSDDEcheanceListPanelToolbarListener() {
			actionsDisponibles.add(actionAjoutEcheance);
			actionsDisponibles.add(actionModificationEcheance);
			actionsDisponibles.add(actionSuppressionEcheance);
			actionsDisponibles.add(actionGenerationEcheances);
		}
		
		public Action myActionAddEcheance() {
			return actionAjoutEcheance;
		}

		public Action myActionModifyEcheance() {
			return actionModificationEcheance;
		}

		public Action myActionDeleteEcheance() {
			return actionSuppressionEcheance;
		}

		public Action myActionGenerateEcheances() {
			return actionGenerationEcheances;
		}

		public List<Action> actionsDisponibles() {
			return actionsDisponibles;
		}
	}
	
	private final class ActionAddEcheance extends AbstractAction {
		/** Serial version ID.*/
		private static final long serialVersionUID = 1L;

		public ActionAddEcheance() {
			super("Ajouter une échéance");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter une échéance");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onAddEcheance();
		}
	}

	private final class ActionDeleteEcheance extends AbstractAction {
		/** Serial version ID.*/
		private static final long serialVersionUID = 1L;

		public ActionDeleteEcheance() {
			super("Supprimer l'échéance");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'échéance");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onDeleteEcheance();
		}
	}

	private final class ActionModifyEcheance extends AbstractAction {
		/** Serial version ID.*/
		private static final long serialVersionUID = 1L;

		public ActionModifyEcheance() {
			super("Modifier l'échéance");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier l'échéance");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDIT_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onModifyEcheance();
		}
	}

	private final class ActionGenererEcheances extends AbstractAction {
		/** Serial version ID.*/
		private static final long serialVersionUID = 1L;

		public ActionGenererEcheances() {
			super("Générer écheances");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Générer les échéances");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onGenererEcheances();
		}
	}

	private final class ActionSelectOrigine extends AbstractAction {
		/** Serial version ID.*/
		private static final long serialVersionUID = 1L;

		public ActionSelectOrigine() {
			super("Sélectionner une origine");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
		}

		public void actionPerformed(ActionEvent e) {
			selectionnerOrigine();
		}
	}

	public SepaSddUiConstraints getUiConstraints() {
		return uiConstraints;
	}

	public void setUiConstraints(SepaSddUiConstraints uiConstraints) {
		this.uiConstraints = uiConstraints;
	}

	public IGenerateurEcheancier getGenerateurEcheancier() {
		return generateurEcheancier;
	}

	public void setGenerateurEcheancier(IGenerateurEcheancier generateurEcheancier) {
		this.generateurEcheancier = generateurEcheancier;
	}

}
