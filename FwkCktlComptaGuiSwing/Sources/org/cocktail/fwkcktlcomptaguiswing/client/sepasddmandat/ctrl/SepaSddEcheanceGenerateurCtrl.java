package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.exception.DataCheckException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierConstant;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierPremiereEcheanceFixe;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheanceGenerateurPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;

public class SepaSddEcheanceGenerateurCtrl extends CommonCtrl {

	private static final String TITLE = "Générer les échéances";
	private static final Dimension WINDOW_DIMENSION = new Dimension(450, 250);

	private SepaSddEcheanceGenerateurPanel sepaSddEcheanceGenerateurPanel;

	private ISepaSddEcheancier currentEcheancier;
	private Map<String, Object> currentDico;

	private Action actionClose = new ActionClose();
	private Action actionGenerer = new ActionGenerer();

	/**
	 * Constructeur.
	 *
	 * @param editingContext editing Context associe a ce controlleur.
	 */
	public SepaSddEcheanceGenerateurCtrl(EOEditingContext editingContext) {
		super(editingContext);
		this.currentDico = new HashMap<String, Object>();
		this.sepaSddEcheanceGenerateurPanel = new SepaSddEcheanceGenerateurPanel(
				new SepaSddEcheanceGenerateurPanelListener());
	}

	public final int openDialogModify(Window parent, ISepaSddEcheancier echeancier) {
		final ZKarukeraDialog win = createDialog(parent, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			this.currentEcheancier = echeancier;
			updateDicoWithObject();

			sepaSddEcheanceGenerateurPanel.prepareGuiOnOpen();
			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);

		if (currentDico.get(ISepaSddEcheancier.MONTANT_KEY) == null) {
			throw new DataCheckException("Le montant de l'échéancier est obligatoire.");
		} else if (currentDico.get(ISepaSddEcheancier.D_PREMIERE_ECHEANCE_PREVUE_KEY) == null) {
			throw new DataCheckException("La date de première échéance est obligatoire.");
		}

		Integer nbEcheances = (Integer) currentDico.get(ISepaSddEcheancier.NB_ECHEANCES_KEY);
		String cTypePrelevement = currentEcheancier.mandat().cTypePrelevement();
		if ("P".equals(cTypePrelevement) && nbEcheances != 1) {
			throw new DataCheckException("Il ne peut y avoir qu'une seule échéance pour les prélèvements ponctuels");
		}
	}

	private final void validerSaisie() {
		try {
			checkSaisie();
			genererEcheances();
			getCurrentEcheancier().setToModificateurPersonne(myApp.appUserInfo().getGrhumPersonne());
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void updateDicoWithObject() {
		Integer nbJoursMaxAvantBasculeAuMoisSuivant = Integer.valueOf((String) ServerCallCompta.clientSideRequestGetConfig(getEditingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT));
		Integer numeroJourDePrelevement = Integer.valueOf((String) ServerCallCompta.clientSideRequestGetConfig(getEditingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_NUMERO_JOUR));
		LocalDate datePremiereEcheance = SepaSddEcheancierHelper.getSharedInstance().calculeDatePremiereEcheanceAvecDateDuJour(nbJoursMaxAvantBasculeAuMoisSuivant, numeroJourDePrelevement);

		currentDico.clear();
		currentDico.put(ISepaSddEcheancier.MONTANT_KEY, currentEcheancier.toSepaSddOrigine().toEntity().origineMontant());
		currentDico.put(ISepaSddEcheancier.D_PREMIERE_ECHEANCE_PREVUE_KEY, datePremiereEcheance);
	}

	protected void onCancel() {
		getMyDialog().onCancelClick();
	}

	@Override
	public String title() {
		return TITLE;
	}

	@Override
	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	@Override
	public ZAbstractSwingPanel mainPanel() {
		return sepaSddEcheanceGenerateurPanel;
	}

	public ISepaSddEcheancier getCurrentEcheancier() {
		return currentEcheancier;
	}

	private void genererEcheances() {
		try {
			DateConversionUtil dcu = new DateConversionUtil();

			BigDecimal montantAPayer = (BigDecimal) currentDico.get(ISepaSddEcheancier.MONTANT_KEY);
			Integer nbEcheances = (Integer) currentDico.get(ISepaSddEcheancier.NB_ECHEANCES_KEY);
			LocalDate datePremiereEcheance = (LocalDate) currentDico.get(ISepaSddEcheancier.D_PREMIERE_ECHEANCE_PREVUE_KEY);
			BigDecimal montantPremiereEcheance = (BigDecimal) currentDico.get(ISepaSddEcheancier.MONTANT_PREMIERE_ECHEANCE_KEY);

			// choix du generateur d'echeances
			Integer persId = myApp.appUserInfo().getPersId();
			IGenerateurEcheancier generateur;
			if (montantPremiereEcheance == null) {
				generateur = new SepaSddGenerateurEcheancierConstant(
						montantAPayer, nbEcheances, datePremiereEcheance, persId, myApp.appUserInfo().getGrhumPersonne());
			} else {
				generateur = new SepaSddGenerateurEcheancierPremiereEcheanceFixe(
						montantAPayer, nbEcheances, datePremiereEcheance, montantPremiereEcheance, persId,
						myApp.appUserInfo().getGrhumPersonne());
			}

			SepaSddEcheancierHelper.getSharedInstance().initialiserEcheances(currentEcheancier, generateur);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**************************/
	/**** CLASSES INTERNES ****/
	/**************************/
	public final class ActionGenerer extends AbstractAction {
		public ActionGenerer() {
			super("Générer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}
	}

	private final class SepaSddEcheanceGenerateurPanelListener implements SepaSddEcheanceGenerateurPanel.ISepaSddEcheanceGenerateurPanelListener {
		public Action actionCancel() {
			return actionClose;
		}

		public Action actionGenerer() {
			return actionGenerer;
		}

		public Map<String, Object> values() {
			return currentDico;
		}
	}
}
