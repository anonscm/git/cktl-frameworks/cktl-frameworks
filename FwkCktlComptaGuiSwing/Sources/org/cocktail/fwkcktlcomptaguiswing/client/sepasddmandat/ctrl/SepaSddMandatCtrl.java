/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;

import org.cocktail.fwkcktlcompta.client.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.client.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.client.metier.finders.ZFinder;
import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta;
import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallPrint;
import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallsepaSdd;
import org.cocktail.fwkcktlcompta.client.sepasdd.helpers.SepaSddOrigineClientHelper;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.exception.DefaultClientException;
import org.cocktail.fwkcktlcompta.common.helpers.EcritureHelper;
import org.cocktail.fwkcktlcompta.common.helpers.EmargementHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl.ISepaSddMandatCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.origines.OrigineComplements;
import org.cocktail.fwkcktlcompta.common.util.CktlConfigUtil;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcompta.common.util.ZListUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonSrchCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.CommonFilterPanel.ICommonSrchCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheancierListPanel.ISepaSDDEcheancierListPanelToolbarListener;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddMandatPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddMandatPanel.ISepaSddMandatPanelCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddUiConstraints;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZCommonDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZMsgPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SepaSddMandatCtrl extends CommonSrchCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Gestion des mandats SEPA SDD";
	private static final String TITLE_SUPPRESSION_ECHEANCIER = "Supprimer un échéancier";
	private static final String TITLE_SUPPRESSION_MANDAT = "Supprimer un mandat";
	private static final String MSG_SUPPRESSION_ECHEANCIER = "Etes-vous sûr de vouloir supprimer cet échéancier ?";
	private static final String MSG_SUPPRESSION_MANDAT = "Etes-vous sûr de vouloir supprimer ce mandat ?";
	private static final String ERR_AUCUN_MANDAT_SELECTIONNE = "Aucun mandat n'est sélectionné.";
	private static final String ERR_AUCUN_ECHEANCIER_SELECTIONNE = "Aucun échéancier n'est sélectionné.";
	private static final String INFO_MANDATS_RESTREINTS_AU_DEBITEUR = "Seuls les mandats existant pour %s sont affichés";
	private static final String INFO_AUCUN_MANDAT_TROUVE = "Aucun mandat n'a été trouvé pour ce débiteur (%s)";
	private static final String COMBO_TYPE_ORIGINE_NULL_LABEL = "Tous";
	private static final int NOMBRE_MAXIMUM_DE_MANDATS_RETOURNES = 100;
	private static final String ERR_AUCUN_MANDAT_ANNULE = "Le mandat est ANNULE";

	private final SepaSddMandatPanel sepaSddMandatPanel;
	private final SepaSddMandatPanelCtrl sepaSddMandatPanelCtrl;

	private final SepaSddMandatListMdl sepaSddMandatMdl;
	private final SepaSddEcheancierListMdl sepaSddEcheancierMdl;
	private final SepaSddEcheanceListMdl sepaSddEcheanceMdl;
	private final FilterPanelCtrl filterPanelCtrl;

	private final Action actionModifier = new ActionModifier();
	private final Action actionNouveau = new ActionNouveau();
	private final Action actionSupprimer = new ActionSupprimer();
	private final Action actionHistorique = new ActionHistorique();
	private final Action actionImprimerMandat = new ActionImprimerMandat();
	private final Action actionValiderMandat = new ActionValiderMandat();
	private final Action actionRefreshData = new ActionRefreshData();
	private final ActionAfficherMandatsAnnules actionAfficherMandatsAnnules = new ActionAfficherMandatsAnnules();

	private final Action actionAddEcheancier = new ActionAddEcheancier();
	private final Action actionDeleteEcheancier = new ActionDeleteEcheancier();
	private final Action actionModifyEcheancier = new ActionModifyEcheancier();
	private final Action actionImprimerEcheancier = new ActionImprimerEcheancier();
	private final Action actionViserEcheancier = new ActionViserEcheancier();

	private DefaultComboBoxModel comboEtatsModel;
	private ZEOComboBoxModel typeOrigineModel;

	private ISepaSddOrigine origineRefApplication;
	private Map<OrigineComplements, Object> origineComplements;
	private EOQualifier filtreAdditionnel;
	private Boolean prelevementSepaEnabled;
	private boolean hasDroitValiderMandat;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public SepaSddMandatCtrl(EOEditingContext editingContext) {
		super(editingContext);
		revertChanges();
		prelevementSepaEnabled = CktlConfigUtil.getBooleanValueForConfig((String) ServerCallCompta.clientSideRequestGetConfig(getEditingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_ENABLED));

		if (!prelevementSepaEnabled) {
			showWarningDialog("Les prélèvements SEPA ne sont pas activés, vous ne pourrez pas créer de nouveaux mandats ni créer de remises de fichiers de prélèvements SEPA. (Cf. paramètre de l'application org.cocktail.gfc.sepasddmandat.enabled).");
		}

		hasDroitValiderMandat = myApp.appUserInfo().isFonctionAutoriseeByActionID("SSDDVAL");

		comboEtatsModel = new DefaultComboBoxModel();
		comboEtatsModel.addElement(EOJefyAdminTypeEtat.fetchByKeyValue(editingContext, EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOJefyAdminTypeEtat.ETAT_EN_ATTENTE));
		comboEtatsModel.addElement(EOJefyAdminTypeEtat.fetchByKeyValue(editingContext, EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOJefyAdminTypeEtat.ETAT_EN_COURS));
		comboEtatsModel.addElement(EOJefyAdminTypeEtat.fetchByKeyValue(editingContext, EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOJefyAdminTypeEtat.ETAT_SUPPRIME));

		NSArray typeOrigines = SepaSddOrigineClientHelper.getSharedInstance().fetchOrigineTypesValides(getEditingContext());
		typeOrigineModel = new ZEOComboBoxModel(typeOrigines, EOSepaSddOrigineType.TYPE_NOM_KEY, COMBO_TYPE_ORIGINE_NULL_LABEL, null);

		sepaSddMandatMdl = new SepaSddMandatListMdl();
		sepaSddEcheancierMdl = new SepaSddEcheancierListMdl();
		sepaSddEcheanceMdl = new SepaSddEcheanceListMdl();

		sepaSddMandatPanelCtrl = new SepaSddMandatPanelCtrl();
		filterPanelCtrl = new FilterPanelCtrl();
		sepaSddMandatPanel = new SepaSddMandatPanel(
				sepaSddMandatPanelCtrl,
				new SepaSDDEcheancierListPanelToolbarListener());
		filtreAdditionnel = null;
	}

	public int openDialog(Window dial, boolean modal, Boolean preloadData) {
		int res = ZCommonDialog.MRCANCEL;
		ZCommonDialog win = createDialog(dial, modal);
		try {
			mainPanel().onBeforeShow();
			if (preloadData) {
				mainPanel().updateData();
			}
			onAfterUpdateDataBeforeOpen();
			win.open();
			res = win.getModalResult();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
			setWaitCursor(false);
		}
		return res;
	}

	public int openDialog(Window dial, boolean modal, ISepaSddOrigineEntity origineEntity, String codeOrigineType, Map<OrigineComplements, Object> origineComplements) {
		try {
			ISepaSddOrigineType origineType = SepaSddOrigineClientHelper.getSharedInstance()
					.fetchOrigineTypeValide(getEditingContext(), codeOrigineType);

			setOrigineComplements(origineComplements);
			setOrigineRefApplication(SepaSddOrigineHelper.getSharedInstance()
					.creerOuRecupererOrigine(getEditingContext(), origineType, origineEntity));

			if (((EOSepaSddOrigine) getOrigineRefApplication()).isNewObject()) {
				getEditingContext().saveChanges();
			}

			if (existeMandatsAssociesAuDebiteur(getEditingContext(), getOrigineRefApplication())) {
				filtreAdditionnel = ISepaSddMandatCtrl.getSharedInstance().filtreMandatsPourDebiteurOuTiersDebiteur(getOrigineRefApplication().toEntity().origineDebiteurPersonne());
				showInfoDialog(String.format(INFO_MANDATS_RESTREINTS_AU_DEBITEUR, origineEntity.origineDebiteurNomComplet()));
			} else {
				showInfoDialog(String.format(INFO_AUCUN_MANDAT_TROUVE, origineEntity.origineDebiteurNomComplet()));
			}

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
		setWaitCursor(true);
		return openDialog(dial, modal, Boolean.TRUE);
	}

	protected boolean existeMandatsAssociesAuDebiteur(EOEditingContext ec, ISepaSddOrigine origine) throws Exception {
		if (!SepaSddOrigineHelper.getSharedInstance().isOrigineDebiteurDefini(origine)) {
			return false;
		}

		return ISepaSddMandatCtrl.getSharedInstance().mandatsPourDebiteurOuTiersDebiteur(ec, origine.toEntity().origineDebiteurPersonne()).count() > 0;
	}

	protected NSMutableArray buildFilterQualifiers() throws Exception {
		NSMutableArray quals = new NSMutableArray();

		if (filtreAdditionnel != null) {
			quals.addObject(filtreAdditionnel);
		}

		if (filters.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY + "." + EOGrhumPersonne.NOM_AND_PRENOM_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY + "." + EOGrhumPersonne.PERS_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filters.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY + "." + EOGrhumPersonne.NOM_AND_PRENOM_KEY) + "*"));
		}

		if (filters.get(EOSepaSddMandat.RUM_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier(EOSepaSddMandat.RUM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filters.get(EOSepaSddMandat.RUM_KEY) + "*"));
		}
		if (!actionAfficherMandatsAnnules.isSelected()) {
			quals.addObject(new EOKeyValueQualifier(EOSepaSddMandat.TO_TYPE_ETAT_KEY + "." + EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorNotEqual, EOJefyAdminTypeEtat.ETAT_ANNULE));
		}

		return quals;
	}

	public void onAfterUpdateDataBeforeOpen() {
		try {
			super.onAfterUpdateDataBeforeOpen();
			refreshActionsMandat();
			refreshActionsEcheancier();
			ISepaSddEcheancier echeancier = SepaSddOrigineHelper.getSharedInstance().premierEcheancierPourOrigine(getOrigineRefApplication());
			if (echeancier != null) {
				sepaSddMandatPanel.forceNewMandatSelection(echeancier.mandat());
				sepaSddMandatPanel.forceNewEcheancierSelection(echeancier);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final NSArray getData() throws Exception {
		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOSepaSddMandat.D_MANDAT_CREATION_KEY, EOSortOrdering.CompareDescending);
		EOFetchSpecification spec = new EOFetchSpecification(EOSepaSddMandat.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers()), new NSArray(sort1));
		spec.setFetchLimit(NOMBRE_MAXIMUM_DE_MANDATS_RETOURNES);
		NSArray res = getEditingContext().objectsWithFetchSpecification(spec);
		SepaSddMandatHelper.getSharedInstance().invalidateMandatsEtSesEcheances(res, getEditingContext());
		if (res.count() >= NOMBRE_MAXIMUM_DE_MANDATS_RETOURNES) {
			showInfoDialog("Seuls les " + NOMBRE_MAXIMUM_DE_MANDATS_RETOURNES + " mandats ont été retournés. Affinez vos critères de recherche si besoin.");
		}

		return res;
	}

	private void onModifyMandat() {
		getEditingContext().revert();
		onModifyMandat(getSelectedMandatSdd(), null);
	}


	private void onModifyMandat(EOSepaSddMandat mandat, EOSepaSddOrigine origine) {
		//FIXME ajouter verification des droits
		setWaitCursor(true);
		try {
			if (mandat == null) {
				throw new DefaultClientException(ERR_AUCUN_MANDAT_SELECTIONNE);
			}

			final SepaSddMandatSaisieCtrl mandatSaisieCtrl = new SepaSddMandatSaisieCtrl(getEditingContext());
			mandat.setToModificateurRelationship(myApp.appUserInfo().getGrhumPersonne());
			int res = mandatSaisieCtrl.openDialogModify(getMyDialog(), mandat);

			if (res == ZCommonDialog.MROK) {
				filters.put(EOSepaSddMandat.RUM_KEY, mandat.rum());
				sepaSddMandatPanel.getFilterPanel().updateData();
			}


			sepaSddMandatPanel.getSepaSddMandatListTablePanel().updateData();
			sepaSddMandatPanel.forceNewMandatSelection(mandat);

			if (res == ZCommonDialog.MROK) {
				if (mandat.toSepaSddEcheanciers() == null || mandat.toSepaSddEcheanciers().count() == 0) {
					if (showConfirmationDialog(
							"Création d'un échéancier",
							"Le mandat a bien été enregistré. Souhaitez-vous associer un échéancier à ce mandat ?",
							ZMsgPanel.BTLABEL_YES)) {
						onAddEcheancier(mandat, origine, null, null);
						sepaSddMandatPanel.getSepaSddMandatListTablePanel().fireSeletedTableRowUpdated();
					}
				}
			}
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}


	}

	private void onAddMandat() {
		getEditingContext().revert();
		EOSepaSddMandat sepaSddMandat = EOSepaSddMandat.creerInstance(getEditingContext());
		sepaSddMandat.setPersIdCreation(myApp.appUserInfo().getPersId());
		sepaSddMandat.setPersIdModification(myApp.appUserInfo().getPersId());
		sepaSddMandat.setToModificateurRelationship(myApp.appUserInfo().getGrhumPersonne());
		sepaSddMandat.setDMandatCreation(new DateConversionUtil().formatDateWithoutTimeISO(new LocalDate()));
		sepaSddMandat.setNumero(SepaSddMandatHelper.NUMERO_TEMP);
		sepaSddMandat.setRum(SepaSddMandatHelper.RUM_TEMP);
		sepaSddMandat.setToTypeEtatRelationship(EOJefyAdminTypeEtat.fetchByKeyValue(getEditingContext(), EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOJefyAdminTypeEtat.ETAT_A_VALIDER));
		sepaSddMandat.setRefAppliCreation(FwkCktlComptaMoteurCtrl.getSharedInstance().getRefApplicationCreation());

		Map<OrigineComplements, Object> origineCpltsLocale = getOrigineComplements();
		if (origineCpltsLocale != null) {
			sepaSddMandat.setToDebiteurPersonneRelationship((EOGrhumPersonne) origineCpltsLocale.get(OrigineComplements.TO_DEBITEUR_PERSONNE));
			sepaSddMandat.setToDebiteurAdresseRelationship((EOGrhumAdresse) origineCpltsLocale.get(OrigineComplements.TO_DEBITEUR_ADRESSE));
			sepaSddMandat.setToDebiteurRibRelationship((EOGrhumRib) origineCpltsLocale.get(OrigineComplements.TO_DEBITEUR_RIB));
		}

		onModifyMandat(sepaSddMandat, null);
	}

	private void onDeleteMandat() {
		setWaitCursor(true);
		try {
			ISepaSddMandat mandat = getSelectedMandatSdd();
			if (mandat == null) {
				throw new DefaultClientException(ERR_AUCUN_MANDAT_SELECTIONNE);
			}

			if (showConfirmationDialog(TITLE_SUPPRESSION_MANDAT, MSG_SUPPRESSION_MANDAT, ZMsgPanel.BTLABEL_NO)) {
				SepaSddMandatHelper.getSharedInstance().supprimerMandat(mandat, getEditingContext());
				getEditingContext().saveChanges();
				onMandatSelectionneHasChanged();
			}
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onHistorique() {
		setWaitCursor(true);
		try {
			if (getSelectedMandatSdd() == null) {
				throw new DefaultClientException("Aucun mandat n'est sélectionnée.");
			}

			final SepaSddHistoriqueMandatCtrl sepaSddHistoriqueMandatCtrl = new SepaSddHistoriqueMandatCtrl(getEditingContext());
			sepaSddHistoriqueMandatCtrl.openDialog(getMyDialog(), getSelectedMandatSdd());

			sepaSddMandatPanel.getSepaSddMandatListTablePanel().fireSeletedTableRowUpdated();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}

	}

	private void onValiderMandat() {
		setWaitCursor(true);
		try {
			if (!hasDroitValiderMandat) {
				throw new DefaultClientException("Vous n'avez pas les droits nécessaires pour valider les mandats SEPA SDD.");
			}

			if (getSelectedMandatSdd() == null) {
				throw new DefaultClientException("Aucun mandat n'est sélectionnée.");
			}

			SepaSddMandatHelper.getSharedInstance().validerMandat(getEditingContext(), getSelectedMandatSdd());

			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			sepaSddMandatPanel.getSepaSddMandatListTablePanel().fireSeletedTableRowUpdated();
		}
	}

	private void onAddEcheancier() {
		setWaitCursor(true);
		try {
			getEditingContext().revert();
			onAddEcheancier(getSelectedMandatSdd(), null, null, null);
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onAddEcheancier(EOSepaSddMandat mandat, EOSepaSddOrigine origine, IGenerateurEcheancier generateurEcheancier, SepaSddUiConstraints uiConstraints) {
		try {
			if (mandat == null) {
				throw new DefaultClientException(ERR_AUCUN_MANDAT_SELECTIONNE);
			}
			if (SepaSddMandatHelper.getSharedInstance().isAnnule(mandat)) {
				throw new DefaultClientException(ERR_AUCUN_MANDAT_ANNULE);
			}

			ISepaSddEcheancier echeancier = EOSepaSddEcheancier.creerInstance(getEditingContext());

			echeancier = new SepaSddEcheancierHelper.BuilderEcheancier(echeancier, myApp.appUserInfo().getPersId(), myApp.appUserInfo().getGrhumPersonne())
					.mandat(mandat)
					.build();

			final SepaSddEcheancierSaisieCtrl echeancierSaisieCtrl = new SepaSddEcheancierSaisieCtrl(getEditingContext());

			ISepaSddOrigineEntity origineEntity = null;
			String codeOrigineType = null;
			if (getOrigineRefApplication() != null) {
				origineEntity = getOrigineRefApplication().toEntity();
				codeOrigineType = getOrigineRefApplication().toSepaSddOrigineType().typeCode();
			}
			int res = echeancierSaisieCtrl.openDialogModify(getMyDialog(), echeancier, generateurEcheancier, uiConstraints, origineEntity, codeOrigineType);
			if (res == ZKarukeraDialog.MROK) {
				onMandatSelectionneHasChanged();
				if (echeancier != null) {
					sepaSddMandatPanel.getSepaSddEcheancierListTablePanel().updateData();
					sepaSddMandatPanel.forceNewEcheancierSelection(echeancier);
					onEcheancierSelectionneHasChanged();
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
		}
	}

	private void onDeleteEcheancier() {
		getEditingContext().revert();
		if (getSelectedEcheancierSdd() == null) {
			return;
		}
		setWaitCursor(true);
		try {
			if (showConfirmationDialog(TITLE_SUPPRESSION_ECHEANCIER, MSG_SUPPRESSION_ECHEANCIER, ZMsgPanel.BTLABEL_NO)) {
				SepaSddEcheancierHelper.getSharedInstance().supprimerEcheancier(getSelectedEcheancierSdd());
				getEditingContext().deleteObject(getSelectedEcheancierSdd());
				getEditingContext().saveChanges();
			}
		} catch (Exception e) {
			setWaitCursor(false);
			getEditingContext().revert();
			showErrorDialog(e);
			CktlEOControlUtilities.refaultObject(getSelectedEcheancierSdd());

		} finally {
			setWaitCursor(false);
			onMandatSelectionneHasChanged();
		}

	}

	private void onModifyEcheancier() {
		getEditingContext().revert();
		onModifyEcheancier(getSelectedEcheancierSdd());
	}

	private void onModifyEcheancier(EOSepaSddEcheancier echeancier) {
		setWaitCursor(true);
		try {
			//FIXME ajouter verification des droits

			if (echeancier == null) {
				throw new DefaultClientException(ERR_AUCUN_ECHEANCIER_SELECTIONNE);
			}

			final SepaSddEcheancierSaisieCtrl echeancierSaisieCtrl = new SepaSddEcheancierSaisieCtrl(getEditingContext());
			//verifier que l'origine existe
			if (getSelectedEcheancierSdd().toSepaSddOrigine() == null || getSelectedEcheancierSdd().toSepaSddOrigine().toEntity() == null) {
				throw new Exception("Origine introuvable pour l'échéancier (type origine=" + getSelectedEcheancierSdd().toSepaSddOrigine().toSepaSddOrigineType().typeCode() + " / id origine=" + getSelectedEcheancierSdd().toSepaSddOrigine().origineId() + ")");
			}

			echeancierSaisieCtrl.openDialogModify(getMyDialog(), getSelectedEcheancierSdd());

			sepaSddMandatPanel.getSepaSddEcheancierListTablePanel().fireSelectedTableRowUpdated();
			onEcheancierSelectionneHasChanged();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private EOSepaSddMandat getSelectedMandatSdd() {
		return sepaSddMandatPanel.selectedSepaSddMandat();
	}

	private EOSepaSddEcheancier getSelectedEcheancierSdd() {
		return sepaSddMandatPanel.selectedSepaSddEcheancier();
	}

	private final void refreshActionsMandat() {

		actionNouveau.setEnabled(prelevementSepaEnabled);
		actionModifier.setEnabled(getSelectedMandatSdd() != null);
		actionSupprimer.setEnabled(getSelectedMandatSdd() != null);
		actionHistorique.setEnabled(getSelectedMandatSdd() != null);
		actionImprimerMandat.setEnabled(getSelectedMandatSdd() != null);
		actionValiderMandat.setEnabled(getSelectedMandatSdd() != null && SepaSddMandatHelper.getSharedInstance().isAValider(getSelectedMandatSdd()));
	}

	private final void refreshActionsEcheancier() {
		actionAddEcheancier.setEnabled(getSelectedMandatSdd() != null && !SepaSddMandatHelper.getSharedInstance().isAnnule(getSelectedMandatSdd()));
		boolean echeancierSelected = getSelectedEcheancierSdd() != null;
		actionDeleteEcheancier.setEnabled(echeancierSelected);
		actionModifyEcheancier.setEnabled(echeancierSelected);
		actionImprimerEcheancier.setEnabled(echeancierSelected);
		actionViserEcheancier.setEnabled(echeancierSelected && SepaSddEcheancierHelper.getSharedInstance().getLastEcritureDetailPriseEnCharge(getSelectedEcheancierSdd()) == null);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractSwingPanel mainPanel() {
		return sepaSddMandatPanel;
	}

	public String title() {
		return TITLE;
	}

	private final class SepaSddMandatPanelCtrl implements ISepaSddMandatPanelCtrl {

		public Action actionClose() {
			return actionClose;
		}

		public ICommonSrchCtrl panelFilterCtrl() {
			return filterPanelCtrl;
		}

		public ComboBoxModel getEtatModel() {
			return comboEtatsModel;
		}

		public Action actionModifier() {
			return actionModifier;
		}

		public IZTablePanelMdl sepaSddMandatMdl() {
			return sepaSddMandatMdl;
		}

		public Action actionNouveau() {
			return actionNouveau;
		}

		public Action actionSupprimer() {
			return actionSupprimer;
		}

		public Action actionHistorique() {
			return actionHistorique;
		}

		public IZKarukeraTablePanelListener sepaSddEcheancierMdl() {
			return sepaSddEcheancierMdl;
		}

		public IZKarukeraTablePanelListener sepaSddEcheanceMdl() {
			return sepaSddEcheanceMdl;
		}

		public ComboBoxModel getTypeOrigineModel() {
			return typeOrigineModel;
		}

		public Action actionValiderMandat() {
			return actionValiderMandat;
		}

		public Action ActionRefreshData() {
			return actionRefreshData;
		}

		public Action actionImprimerMandat() {
			return actionImprimerMandat;
		}

		public Action actionAfficherMandatsAnnules() {
			return actionAfficherMandatsAnnules;
		}
	}

	private final class SepaSDDEcheancierListPanelToolbarListener implements ISepaSDDEcheancierListPanelToolbarListener {

		public Action myActionDeleteEcheancier() {
			return actionDeleteEcheancier;
		}

		public Action myActionAddEcheancier() {
			return actionAddEcheancier;
		}

		public Action myActionModifyEcheancier() {
			return actionModifyEcheancier;
		}

		public Action myActionImprimerEcheancier() {
			return actionImprimerEcheancier;
		}

		public Action myActionViserEcheancier() {
			return actionViserEcheancier;
		}

	}


	private final class SepaSddMandatListMdl implements IZTablePanelMdl {

		public NSArray getData() throws Exception {
			return SepaSddMandatCtrl.this.getData();
		}

		public void onDbClick() {
			onModifyMandat();

		}

		public void selectionChanged() {
			onMandatSelectionneHasChanged();
		}


	}

	private final class SepaSddEcheancierListMdl implements IZKarukeraTablePanelListener {

		public void selectionChanged() {
			onEcheancierSelectionneHasChanged();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		/**
		 * Retourne la liste des echeanciers associes au mandat selectionné.
		 */
		public NSArray getData() throws Exception {
			NSArray echeanciers = new NSArray();

			EOSepaSddMandat mandatSelectionne = getSelectedMandatSdd();
			if (mandatSelectionne != null) {
				echeanciers = mandatSelectionne.getEcheanciersTries(true);
			}
			return echeanciers;
		}

		public void onDbClick() {
			onModifyEcheancier();
		}

	}

	private final class SepaSddEcheanceListMdl implements IZKarukeraTablePanelListener {

		public void selectionChanged() {
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		/**
		 * Retourne la liste des echeances associees à l'échéancier selectionné.
		 */
		public NSArray getData() throws Exception {
			EOSepaSddEcheancier echeancierSelectionne = getSelectedEcheancierSdd();
			if (echeancierSelectionne == null) {
				return NSArray.EmptyArray;
			}
			NSArray res = echeancierSelectionne.getEcheancesTrieesParDateEcheanceASC(true);
			ZFinder.invalidateEOObjectsInEditingContext(getEditingContext(), res);
			return res;
		}

		public void onDbClick() {
		}

	}

	private final class FilterPanelCtrl implements ICommonSrchCtrl {
		private Action myActionSrch = new ActionSrch() {
			public void actionPerformed(ActionEvent e) {
				super.actionPerformed(e);

			};
		};

		public Action actionSrch() {
			return myActionSrch;
		}

		public Map filters() {
			return filters;
		}

	}

	private final class ActionModifier extends AbstractAction {
		public ActionModifier() {
			super("Modifier");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le mandat");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDIT_16));
		}

		public void actionPerformed(ActionEvent e) {
			onModifyMandat();
		}
	}

	private final class ActionNouveau extends AbstractAction {
		public ActionNouveau() {
			super("Nouveau");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un mandat");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
		}

		public void actionPerformed(ActionEvent e) {
			onAddMandat();
		}
	}

	private final class ActionSupprimer extends AbstractAction {
		public ActionSupprimer() {
			super("Supprimer");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer un mandat");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
		}

		public void actionPerformed(ActionEvent e) {
			onDeleteMandat();
		}
	}

	private final class ActionHistorique extends AbstractAction {
		public ActionHistorique() {
			super("Voir l'historique");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir l'historique des modifications");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			onHistorique();
		}
	}

	private final class ActionImprimerMandat extends AbstractAction {
		public ActionImprimerMandat() {
			super("Imprimer le mandat");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le mandat");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			onImprimerMandat();
		}
	}

	private final class ActionValiderMandat extends AbstractAction {
		public ActionValiderMandat() {
			super("Valider");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider le mandat");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CHECKED_16));
		}

		public void actionPerformed(ActionEvent e) {
			onValiderMandat();
		}
	}

	private final class ActionRefreshData extends AbstractAction {

		public ActionRefreshData() {
			super("Rafraîchir les données");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Rafraîchir les données");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onRefreshData();
		}
	}

	private final class ActionAddEcheancier extends AbstractAction {

		public ActionAddEcheancier() {
			super("Nouvel Echéancier");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un échéancier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onAddEcheancier();
		}
	}

	private final class ActionDeleteEcheancier extends AbstractAction {
		public ActionDeleteEcheancier() {
			super("Supprimer l'échéancier");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'échéancier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onDeleteEcheancier();
		}
	}

	private final class ActionModifyEcheancier extends AbstractAction {
		public ActionModifyEcheancier() {
			super("Modifier l'échéancier");

			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier l'échéancier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDIT_16));

			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onModifyEcheancier();
		}
	}

	private final class ActionImprimerEcheancier extends AbstractAction {
		public ActionImprimerEcheancier() {
			super("Imprimer l'échéancier");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer l'échéancier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			onImprimerEcheancier();
		}
	}

	private final class ActionViserEcheancier extends AbstractAction {
		public ActionViserEcheancier() {
			super("Prendre en charge l'échéancier au niveau comptable");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Prendre en charge l'échéancier au niveau comptable");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CHECKED_16));
		}

		public void actionPerformed(ActionEvent e) {
			onPrendreEnChargeEcheancier();
		}
	}

	private void onMandatSelectionneHasChanged() {
		setWaitCursor(true);
		try {
			getEditingContext().revert();
			refreshActionsMandat();
			sepaSddMandatPanel.getSepaSddEcheancierListTablePanel().updateData();
			refreshActionsEcheancier();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onPrendreEnChargeEcheancier() {
		if (SepaSddEcheancierHelper.getSharedInstance().isEcheancierPrisEnCharge(getSelectedEcheancierSdd())) {
			showInfoDialog("L'échéancier est déjà pris en charge dans la comptabilité.");
			return;
		}

		setWaitCursor(true);
		try {
			getEditingContext().revert();


			EOJefyAdminExercice exerciceTresorerie = EOJefyAdminExercice.fetchByKeyValue(getEditingContext(), IJefyAdminExercice.I_EXE_TYPE_KEY, IJefyAdminExercice.EXE_TYPE_TRESORERIE);

			if (exerciceTresorerie == null) {
				throw new Exception("Impossible de récupérer l'exercice de trésorerie ");
			}

			EOComptabilite comptabilite = EOComptabilite.fetchFirstByQualifier(getEditingContext(), null);
			EOJefyAdminUtilisateur util = EOJefyAdminUtilisateur.fetchByKeyValue(getEditingContext(), EOJefyAdminUtilisateur.PERSONNE_KEY, myApp.appUserInfo().getGrhumPersonne());

			NSDictionary res = ServerCallsepaSdd.clientSideRequestPriseEnChargeEcheancier(getEditingContext(), getSelectedEcheancierSdd(), comptabilite, exerciceTresorerie, util, ZDateUtil.now());

			NSArray ecrituresGenerees = NSArray.EmptyArray;
			NSArray emargementsGenerees = NSArray.EmptyArray;

			if (res.valueForKey("ecrituresGeneresGids") != null) {
				ecrituresGenerees = CktlEOControlUtilities.faultsFromGlobalIds(getEditingContext(), (NSArray) res.valueForKey("ecrituresGeneresGids"), EOEcriture.class);
			}
			if (res.valueForKey("emargementsGenerees") != null) {
				emargementsGenerees = CktlEOControlUtilities.faultsFromGlobalIds(getEditingContext(), (NSArray) res.valueForKey("emargementsGeneresGids"), EOEcriture.class);
			}
			String msg ="";
			List<Integer> numerosEcritures = EcritureHelper.getSharedInstance().getNumerosList(ecrituresGenerees);
			if (numerosEcritures.size() > 0) {
				 msg = "Ecritures générées : " + ZListUtil.toCommaSeparatedString(numerosEcritures) + "\n";
			}
			List<Integer> numerosEmargements = EmargementHelper.getSharedInstance().getNumerosList(emargementsGenerees);
			if (numerosEcritures.size() > 0) {
				msg = "Emargements générés : " + ZListUtil.toCommaSeparatedString(numerosEmargements) + "\n";
			}

			if (msg.length()>0) {
				showInfoDialog(msg);
			}

			String warnings = (String) res.valueForKey("warnings");
			if (warnings != null && warnings.length() > 0) {
				showWarningDialog(warnings);
			}

			String erreurs = (String) res.valueForKey("erreurs");
			if (erreurs != null && erreurs.length() > 0) {
				throw new Exception(erreurs);
			}

			sepaSddMandatPanel.getSepaSddEcheanceListTablePanel().updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			refreshActionsEcheancier();
			setWaitCursor(false);
		}

	}

	private void onEcheancierSelectionneHasChanged() {
		setWaitCursor(true);
		try {
			getEditingContext().revert();
			refreshActionsEcheancier();
			sepaSddMandatPanel.getSepaSddEcheanceListTablePanel().updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onRefreshData() {
		setWaitCursor(true);
		try {
		NSArray mandats = sepaSddMandatPanel.getSepaSddMandatListTablePanel().getMyDisplayGroup().allObjects();
		SepaSddMandatHelper.getSharedInstance().invalidateMandatsEtSesEcheances(mandats, getEditingContext());
			sepaSddMandatPanel.getSepaSddMandatListTablePanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onCacherMandatsAnnules() {
		setWaitCursor(true);
		try {
			NSArray mandats = sepaSddMandatPanel.getSepaSddMandatListTablePanel().getMyDisplayGroup().allObjects();
			SepaSddMandatHelper.getSharedInstance().invalidateMandatsEtSesEcheances(mandats, getEditingContext());
			sepaSddMandatPanel.getSepaSddMandatListTablePanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onImprimerMandat() {
		setWaitCursor(true);
		try {
			if (getSelectedMandatSdd() == null) {
				throw new DefaultClientException(ERR_AUCUN_MANDAT_SELECTIONNE);
			}
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.takeValueForKey("'" + getSelectedMandatSdd().rum() + "'", ISepaSddMandat.PRINT_RUM_KEY);

			imprimer(ServerCallPrint.JASPERFILENAME_MANDAT_PRELEVEMENT_SEPA,
					ServerCallPrint.PDF_MANDAT_PRELEVEMENT_SEPA,
					parametres);

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onImprimerEcheancier() {
		try {
			setWaitCursor(true);
			if (getSelectedEcheancierSdd() == null) {
				throw new DefaultClientException(ERR_AUCUN_ECHEANCIER_SELECTIONNE);
			}

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.takeValueForKey(getSelectedEcheancierSdd().id(), ISepaSddEcheancier.IECHE_ID_KEY);
			parametres.takeValueForKey(SepaSddEcheancierHelper.getSharedInstance().getLibelleMandatEtEcheancier(getSelectedEcheancierSdd()), ISepaSddEcheancier.LIBELLE_COMPLET_KEY);
			imprimer(ServerCallPrint.JASPERFILENAME_ECHEANCIER_PRELEVEMENT_SEPA,
					ServerCallPrint.PDF_ECHEANCIER_PRELEVEMENT_SEPA,
					parametres);
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void imprimer(String jasperName, String pdfName, NSDictionary parametres) throws Exception {
		String pdfFilePath = ServerCallPrint.clientSideRequestPrintAndWait(
				getEditingContext(),
				getMyApp().getTemporaryDir(),
				jasperName,
				pdfName,
				null,
				parametres);

		if (pdfFilePath != null) {
			OperatingSystemHelper.openPdfFile(pdfFilePath);
		}
	}

	public ISepaSddOrigine getOrigineRefApplication() {
		return origineRefApplication;
	}

	public void setOrigineRefApplication(ISepaSddOrigine origineRefApplication) {
		this.origineRefApplication = origineRefApplication;
	}

	public Map<OrigineComplements, Object> getOrigineComplements() {
		return origineComplements;
	}

	public void setOrigineComplements(
			Map<OrigineComplements, Object> origineComplements) {
		this.origineComplements = origineComplements;
	}

	private final class ActionAfficherMandatsAnnules extends AbstractAction {
		private boolean _isSelected = false;

		public ActionAfficherMandatsAnnules() {
			this.putValue(AbstractAction.NAME, "Afficher les mandats annulés");
		}

		public void actionPerformed(ActionEvent e) {
			final JCheckBox cb = (JCheckBox) e.getSource();
			_isSelected = cb.isSelected();
			try {
				sepaSddMandatPanel.getSepaSddMandatListTablePanel().updateData();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

		public boolean isSelected() {
			return _isSelected;
		}

	}
}
