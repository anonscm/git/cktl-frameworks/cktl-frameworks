/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.exception.DataCheckException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance.Etat;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheanceSaisiePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * @author Franck Bordinat - franck.bordinat at asso-cocktail.fr
 */
public class SepaSddEcheanceSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'une échéance SEPA SDD";
	private static final Dimension WINDOW_DIMENSION = new Dimension(450, 250);

	private Map<String, Object> currentDico;

	private SepaSddEcheanceSaisiePanel sepaSddEcheanceSaisiePanel;

	private ActionClose actionClose = new ActionClose();
	private ActionValider actionValider = new ActionValider();


	private ISepaSddEcheance currentObject;

	/**
	 * @param editingContext
	 */
	public SepaSddEcheanceSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);
		currentDico = new HashMap<String, Object>();
		sepaSddEcheanceSaisiePanel = new SepaSddEcheanceSaisiePanel(new SepaSddEcheanceSaisiePanelListener());
	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);

		if (currentDico.get(EOSepaSddEcheance.D_PREVUE_KEY) == null) {
			throw new DataCheckException("La date de l'échéance est obligatoire.");
		}

		if (currentDico.get(EOSepaSddEcheance.MONTANT_KEY) == null) {
			throw new DataCheckException("Le montant de l'échéance est obligatoire.");
		}

	}

	@Override
	protected void onClose() {
		getMyDialog().onCancelClick();
	}

	/**
     *
     */
	private final void validerSaisie() {
		try {
			checkSaisie();
			updateObjectWithDico();

			currentObject.setToModificateurPersonne(myApp.appUserInfo().getGrhumPersonne());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

			getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
					getEditingContext().globalIDForObject((EOEnterpriseObject) currentObject)
			}));

			getMyDialog().onOkClick();

			//ne pas faire de revert lors d'une exception, sinon on perd l'editingContext sur le currentObject
			getEditingContext().revert();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public final int openDialogModify(Window dial, ISepaSddEcheance selectedObject) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			currentObject = selectedObject;
			updateDicoWithObject();

			sepaSddEcheanceSaisiePanel.initGUI();
			sepaSddEcheanceSaisiePanel.updateData();
			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private final class SepaSddEcheanceSaisiePanelListener implements SepaSddEcheanceSaisiePanel.ISepaSddEcheanceSaisiePanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map<String, Object> values() {
			return currentDico;
		}



	}

	private void updateObjectWithDico() {
		DateConversionUtil dcu = new DateConversionUtil();
		currentObject.setEtatEnum((Etat) currentDico.get(EOSepaSddEcheance.ETAT_KEY));
		currentObject.setDatePrevue(dcu.formatDateWithoutTimeISO((LocalDate) currentDico.get(EOSepaSddEcheance.D_PREVUE_KEY)));
		currentObject.setMontantAPayer((BigDecimal) currentDico.get(EOSepaSddEcheance.MONTANT_KEY));
	}

	private void updateDicoWithObject() {
		DateConversionUtil dcu = new DateConversionUtil();
		currentDico.clear();
		currentDico.put(EOSepaSddEcheance.ETAT_KEY, currentObject.etatAsEnum());
		currentDico.put(EOSepaSddEcheance.D_PREVUE_KEY, dcu.parseDateSilent(currentObject.dPrevue()));
		currentDico.put(EOSepaSddEcheance.MONTANT_KEY, currentObject.montantAPayer());

		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractSwingPanel mainPanel() {
		return sepaSddEcheanceSaisiePanel;
	}

	public String title() {
		return TITLE;
	}
}
