/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumRepartPersonneAdresse;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.exception.DataCheckException;
import org.cocktail.fwkcktlcompta.common.finders.FinderGrhumRib;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddParamHelper;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.PersonneSrchDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.PersonneSrchDialog.IPersonneSrchDialogDelegate;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddMandatSaisiePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddUiConstraints;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SepaSddMandatSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'un mandat SEPA SDD";
	private static final String WARN_CHANGEMENT_DEBITEUR_IMPOSSIBLE_CAUSE_MANDAT_SIGNE
		= "Impossible de modifier le débiteur car ce mandat a été signé.";
	private static final String WARN_CHANGEMENT_DEBITEUR_IMPOSSIBLE_CAUSE_ECHEANCIER_EXISTE
		= "Impossible de modifier le débiteur car au moins un échéancier SEPA existe.\nAu besoin, supprimez les échéanciers.";

	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 700);
	private static final DateConversionUtil DATE_CONVERSION_UTIL = new DateConversionUtil();

	private Map<String, Object> currentDico;

	private SepaSddMandatSaisiePanel sepaSddMandatSaisiePanel;
	private SepaSddUiConstraints sepaSddUiConstraints;

	private ZEOComboBoxModel debiteurRibModel;
	private ZEOComboBoxModel debiteurAdresseModel;
	private ZEOComboBoxModel paramCompteModel;
	private ZEOComboBoxModel typePrelevementModel;

	private Action actionCancel = new ActionCancel();
	private Action actionSave = new ActionSave();
	private Action actionSelectDebiteur = new ActionSelectDebiteur();
	private Action actionSelectTiersDebiteur = new ActionSelectTiersDebiteur();

	private EOSepaSddMandat currentObject;
	private ISepaSddOrigine currentOrigine;
	private IGenerateurEcheancier generateurEcheancier;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public SepaSddMandatSaisieCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		NSArray params = SepaSddParamHelper.getSharedInstance().getAllParamsValides(getEditingContext());
		if (params.count() == 0) {
			throw new Exception("Les paramètres SEPA ne sont pas remplis. Cf. table " + EOSepaSddParam.ENTITY_TABLE_NAME);
		}
		paramCompteModel = new ZEOComboBoxModel(params, EOSepaSddParam.LIBELLE_KEY, null, null);

		typePrelevementModel = new ZEOComboBoxModel(SepaSddMandatHelper.getSharedInstance().typePrelevementPossibles(), SepaSddMandatHelper.TYPE_PRELEVEMENT_LIBELLE_KEY, null, null);
		debiteurRibModel = new ZEOComboBoxModel(new NSArray(), EOGrhumRib.BIC_IBAN_KEY, null, null);
		debiteurAdresseModel = new ZEOComboBoxModel(new NSArray(), EOGrhumRepartPersonneAdresse.LIBELLE_COMPLET_KEY, null, null);
		currentDico = new HashMap<String, Object>();
		currentOrigine = null;
		sepaSddMandatSaisiePanel = new SepaSddMandatSaisiePanel(new SepaSddMandatSaisiePanelListener());
		sepaSddUiConstraints = new SepaSddUiConstraints();
		generateurEcheancier = null;
	}

	public final class ActionSave extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionSave() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}
	}

	public final class ActionCancel extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionCancel() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		public void actionPerformed(ActionEvent e) {
			onCancel();
		}
	}

	public final class ActionSelectDebiteur extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionSelectDebiteur() {
			super("Sélectionner un débiteur");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
		}

		public void actionPerformed(ActionEvent e) {
			selectionnerDebiteur();
		}
	}

	public final class ActionSelectTiersDebiteur extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionSelectTiersDebiteur() {
			super("Sélectionner un tiers débiteur");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
		}

		public void actionPerformed(ActionEvent e) {
			selectionnerTiersDebiteur();
		}
	}

	private final class DebiteurSrchDelegate implements IPersonneSrchDialogDelegate {
		public NSArray getData(EOEditingContext editingContext, String filter) {
			if (filter == null || filter.trim().length() == 0) {
				return NSArray.EmptyArray;
			}
			NSMutableArray res = new NSMutableArray();
			res.addObjectsFromArray(EOGrhumPersonne.fetchDebiteurs(getEditingContext(), filter, 100));
			res.addObjectsFromArray(EOGrhumPersonne.fetchDebiteurs(getEditingContext(), filter + "*", 50));
			res.addObjectsFromArray(EOGrhumPersonne.fetchDebiteurs(getEditingContext(), "*" + filter + "*", 50));

			return NSArrayCtrl.getDistinctsOfNSArray(res.immutableClone());
		}
	}

	private final class TiersDebiteurSrchDelegate implements IPersonneSrchDialogDelegate {
		public NSArray getData(EOEditingContext editingContext, String filter) {
			if (filter == null || filter.trim().length() == 0) {
				return NSArray.EmptyArray;
			}
			NSMutableArray res = new NSMutableArray();
			res.addObjectsFromArray(EOGrhumPersonne.fetchTiersDebiteurs(getEditingContext(), filter, 100));
			res.addObjectsFromArray(EOGrhumPersonne.fetchTiersDebiteurs(getEditingContext(), filter + "*", 50));
			res.addObjectsFromArray(EOGrhumPersonne.fetchTiersDebiteurs(getEditingContext(), "*" + filter + "*", 50));
			return NSArrayCtrl.getDistinctsOfNSArray(res.immutableClone());
		}
	}

	private void selectionnerDebiteur() {
		if (checkDateSignatureExiste(currentObject)) {
			showWarningDialog(WARN_CHANGEMENT_DEBITEUR_IMPOSSIBLE_CAUSE_MANDAT_SIGNE);
			return;
		}

		if (checkEcheanciersExistent(currentObject)) {
			showWarningDialog(WARN_CHANGEMENT_DEBITEUR_IMPOSSIBLE_CAUSE_ECHEANCIER_EXISTE);
			return;
		}
		PersonneSrchDialog personneSrchDialog = new PersonneSrchDialog(
				sepaSddMandatSaisiePanel.getMyDialog(), "Sélection d'un débiteur", getEditingContext(),
				new DebiteurSrchDelegate());
		if (personneSrchDialog.open() == personneSrchDialog.MROK) {
			try {
				selectionnerDebiteur((EOGrhumPersonne) personneSrchDialog.getSelectedEOObject());
				sepaSddMandatSaisiePanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		personneSrchDialog.dispose();
	}

	private boolean checkEcheanciersExistent(ISepaSddMandat mandat) {
		return SepaSddMandatHelper.getSharedInstance().hasEcheanciers(mandat);
	}

	private boolean checkDateSignatureExiste(ISepaSddMandat mandat) {
		return SepaSddMandatHelper.getSharedInstance().estSigne(mandat);
	}

	private void selectionnerDebiteur(IGrhumPersonne personne) {
		currentDico.put(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY, personne);
		updateDebiteurAdresseModel();
		updateDebiteurRibModel();
	}

	private void updateDebiteurRibModel() {
		if (currentDico.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY) != null) {
			NSArray ribs = FinderGrhumRib.fetchRibsValidesPourPersonne(getEditingContext(), (EOGrhumPersonne) currentDico.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY));

			debiteurRibModel.updateListWithData(ribs);
			if (ribs.count() == 1) {
				debiteurRibModel.setSelectedEObject((NSKeyValueCoding) ribs.objectAtIndex(0));
			} else {
				if (ribs.indexOfObject(currentDico.get(EOSepaSddMandat.TO_DEBITEUR_RIB_KEY)) != NSArray.NotFound) {
					debiteurRibModel.setSelectedEObject((NSKeyValueCoding) currentDico.get(EOSepaSddMandat.TO_DEBITEUR_RIB_KEY));
				}
			}
		} else {
			debiteurRibModel.setSelectedEObject(null);
		}
	}

	private void updateDebiteurAdresseModel() {
		if (currentDico.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY) != null) {
			NSArray rpas = ((EOGrhumPersonne) currentDico.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY)).toRepartPersonneAdressesValides();
			debiteurAdresseModel.updateListWithData(rpas);
			if (rpas.count() == 1) {
				debiteurAdresseModel.setSelectedEObject((NSKeyValueCoding) rpas.objectAtIndex(0));
			}
			else {
				if (currentDico.get(EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY) != null) {
					NSArray res = EOQualifier.filteredArrayWithQualifier(rpas, new EOKeyValueQualifier(EOGrhumRepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, currentDico.get(EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY)));
					if (!res.isEmpty()) {
						debiteurAdresseModel.setSelectedEObject((NSKeyValueCoding) res.objectAtIndex(0));
					}
					else {
						debiteurAdresseModel.setSelectedEObject(null);
					}
				}
				else {
					debiteurAdresseModel.setSelectedEObject(null);
				}
			}

		}
		else {
			debiteurAdresseModel.setSelectedEObject(null);
		}
	}

	private void selectionnerTiersDebiteur() {
		PersonneSrchDialog personneSrchDialog = new PersonneSrchDialog(
				sepaSddMandatSaisiePanel.getMyDialog(), "Sélection d'un tiers débiteur", getEditingContext(),
				new TiersDebiteurSrchDelegate());
		if (personneSrchDialog.open() == personneSrchDialog.MROK) {
			try {
				selectionnerTiersDebiteur((EOGrhumPersonne) personneSrchDialog.getSelectedEOObject());
				sepaSddMandatSaisiePanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		personneSrchDialog.dispose();
	}

	private void selectionnerTiersDebiteur(IGrhumPersonne tiersDebiteur) {
		currentDico.put(EOSepaSddMandat.TO_TIERS_DEBITEUR_PERSONNE_KEY, tiersDebiteur);
	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);

		currentDico.put(EOSepaSddMandat.TO_SEPA_SDD_PARAM_KEY, paramCompteModel.getSelectedEObject());
		currentDico.put(EOSepaSddMandat.TO_DEBITEUR_RIB_KEY, debiteurRibModel.getSelectedEObject());
		currentDico.put(EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY, debiteurAdresseModel.getSelectedEObject() == null ? null : ((EOGrhumRepartPersonneAdresse) debiteurAdresseModel.getSelectedEObject()).toAdresse());
		currentDico.put(EOSepaSddMandat.C_TYPE_PRELEVEMENT_KEY, typePrelevementModel.getSelectedEObject().valueForKey(SepaSddMandatHelper.TYPE_PRELEVEMENT_CODE_KEY));

		if (currentDico.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY) == null) {
			throw new DataCheckException("Le débiteur est obligatoire");
		}
		if (currentDico.get(EOSepaSddMandat.D_MANDAT_CREATION_KEY) == null) {
			throw new DataCheckException("La date de création du mandat est obligatoire");
		}
		if (currentDico.get(EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY) == null) {
			throw new DataCheckException("L'adresse du débiteur est obligatoire");
		}
		if (currentDico.get(EOSepaSddMandat.TO_DEBITEUR_RIB_KEY) == null) {
			throw new DataCheckException("Les coordonnées bancaires du débiteur sont obligatoires");
		}
	}

	protected void onCancel() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	/**
     *
     */
	private final void validerSaisie() {
		try {
			setWaitCursor(true);
			checkSaisie();
			updateObjectWithDico();

			currentObject.setToModificateurRelationship(myApp.appUserInfo().getGrhumPersonne());
			SepaSddMandatHelper.getSharedInstance().prepareBeforeSave(currentObject);
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

			getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
					getEditingContext().globalIDForObject(currentObject)
			}));
			getMyDialog().onOkClick();

			//ne pas faire de revert lors d'une exception, sinon on perd l'editingContext sur le currentObject
			getEditingContext().revert();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		}
 finally {
			setWaitCursor(false);
		}
	}

	protected void openCreationEcheancierDialog() {
		ISepaSddEcheancier echeancier = EOSepaSddEcheancier.creerInstance(getEditingContext());
		ISepaSddOrigineEntity origineEntity = null;
		ISepaSddOrigineType origineType = null;

		if (currentOrigine != null) {
			origineType = currentOrigine.toSepaSddOrigineType();
			origineEntity = currentOrigine.toEntity();
		}

		echeancier = new SepaSddEcheancierHelper.BuilderEcheancier(echeancier, myApp.appUserInfo().getPersId(), myApp.appUserInfo().getGrhumPersonne())
				.mandat(currentObject)
				.origine(origineType, origineEntity)
				.build();

		final SepaSddEcheancierSaisieCtrl echeancierSaisieCtrl = new SepaSddEcheancierSaisieCtrl(getEditingContext());
		echeancierSaisieCtrl.openDialogModify(getMyDialog(), echeancier, generateurEcheancier, sepaSddUiConstraints, null, null);
	}

	private final class SepaSddMandatSaisiePanelListener implements SepaSddMandatSaisiePanel.ISepaSddMandatSaisiePanelListener {
		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionSave() {
			return actionSave;
		}

		public Map getValues() {
			return currentDico;
		}

		public DefaultComboBoxModel getDebiteurRibModel() {
			return debiteurRibModel;
		}

		public Action actionSelectDebiteur() {
			return actionSelectDebiteur;
		}

		public Action actionSelectTiersDebiteur() {
			return actionSelectTiersDebiteur;
		}

		public DefaultComboBoxModel getParamCompteModel() {
			return paramCompteModel;
		}

		public DefaultComboBoxModel getDebiteurAdresseModel() {
			return debiteurAdresseModel;
		}

		public ComboBoxModel getTypePrelevementModel() {
			return typePrelevementModel;
		}

		public boolean objectHasEcheanciers() {
			return currentObject.toSepaSddEcheanciers().count() > 0;
		}

		public SepaSddUiConstraints uiConstraints() {
			return sepaSddUiConstraints;
		}
	}

	public final int openDialogModify(Window dial, EOSepaSddMandat selectedObject) {
		return openDialogModify(dial, selectedObject, null, null, null);
	}

	public final int openDialogModify(Window dial, EOSepaSddMandat selectedObject, ISepaSddOrigine sepaSddOrigine,
			SepaSddUiConstraints uiConstraints, IGenerateurEcheancier generateurEcheancier) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			if (uiConstraints != null) {
				this.sepaSddUiConstraints = uiConstraints;
			}
			this.currentOrigine = sepaSddOrigine;
			this.generateurEcheancier = generateurEcheancier;
			this.currentObject = selectedObject;

			updateDicoWithObject();
			if (currentObject.toDebiteurPersonne() != null) {
				updateDebiteurAdresseModel();
				updateDebiteurRibModel();
			}

			sepaSddMandatSaisiePanel.initGUI();
			sepaSddMandatSaisiePanel.updateData();
			sepaSddMandatSaisiePanel.setMyDialog(win);

			res = win.open();

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private void updateObjectWithDico() {
		DateConversionUtil dcu = new DateConversionUtil();

		currentObject.setLibelle((String) currentDico.get(EOSepaSddMandat.LIBELLE_KEY));
		currentObject.setCommentaire((String) currentDico.get(EOSepaSddMandat.COMMENTAIRE_KEY));
		currentObject.setRum((String) currentDico.get(EOSepaSddMandat.RUM_KEY));
		currentObject.setDMandatCreation(dcu.formatDateWithoutTimeISO((LocalDate) currentDico.get(EOSepaSddMandat.D_MANDAT_CREATION_KEY)));
		currentObject.setDMandatSignature(null);
		if (currentDico.get(EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY) != null) {
			currentObject.setDMandatSignature(dcu.formatDateWithoutTimeISO((LocalDate) currentDico.get(EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY)));
		}
		currentObject.setCTypePrelevement((String) currentDico.get(EOSepaSddMandat.C_TYPE_PRELEVEMENT_KEY));
		currentObject.setToDebiteurPersonneRelationship((EOGrhumPersonne) currentDico.get(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY));
		currentObject.setToDebiteurAdresseRelationship((EOGrhumAdresse) currentDico.get(EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY));
		currentObject.setToDebiteurRibRelationship((EOGrhumRib) currentDico.get(EOSepaSddMandat.TO_DEBITEUR_RIB_KEY));
		currentObject.setToSepaSddParamRelationship((EOSepaSddParam) currentDico.get(EOSepaSddMandat.TO_SEPA_SDD_PARAM_KEY));
		currentObject.setToTiersCreancierPersonneRelationship((EOGrhumPersonne) currentDico.get(EOSepaSddMandat.TO_TIERS_CREANCIER_PERSONNE_KEY));
		currentObject.setToTiersDebiteurPersonneRelationship((EOGrhumPersonne) currentDico.get(EOSepaSddMandat.TO_TIERS_DEBITEUR_PERSONNE_KEY));
		currentObject.setToTypeEtatRelationship((EOJefyAdminTypeEtat) currentDico.get(EOSepaSddMandat.TO_TYPE_ETAT_KEY));
	}

	private void updateDicoWithObject() {
		currentDico.clear();

		currentDico.put(EOSepaSddMandat.REF_APPLI_CREATION_KEY, currentObject.refAppliCreation());
		currentDico.put(EOSepaSddMandat.LIBELLE_KEY, currentObject.libelle());
		currentDico.put(EOSepaSddMandat.RUM_KEY, currentObject.rum());
		currentDico.put(EOSepaSddMandat.D_MANDAT_CREATION_KEY, DATE_CONVERSION_UTIL.parseDateSilent(currentObject.dMandatCreation()));
		currentDico.put(EOSepaSddMandat.D_MANDAT_SIGNATURE_KEY, DATE_CONVERSION_UTIL.parseDateSilent(currentObject.dMandatSignature()));
		currentDico.put(EOSepaSddMandat.C_TYPE_PRELEVEMENT_KEY, currentObject.cTypePrelevement());
		currentDico.put(EOSepaSddMandat.TO_DEBITEUR_PERSONNE_KEY, currentObject.toDebiteurPersonne());
		currentDico.put(EOSepaSddMandat.TO_DEBITEUR_RIB_KEY, currentObject.toDebiteurRib());
		currentDico.put(EOSepaSddMandat.TO_DEBITEUR_ADRESSE_KEY, currentObject.toDebiteurAdresse());
		currentDico.put(EOSepaSddMandat.TO_SEPA_SDD_PARAM_KEY, currentObject.toSepaSddParam());
		currentDico.put(EOSepaSddMandat.COMMENTAIRE_KEY, currentObject.commentaire());
		currentDico.put(EOSepaSddMandat.TO_TIERS_CREANCIER_PERSONNE_KEY, currentObject.toTiersCreancierPersonne());
		currentDico.put(EOSepaSddMandat.TO_TIERS_DEBITEUR_PERSONNE_KEY, currentObject.toTiersDebiteurPersonne());
		currentDico.put(EOSepaSddMandat.TO_TYPE_ETAT_KEY, currentObject.toTypeEtat());

		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractSwingPanel mainPanel() {
		return sepaSddMandatSaisiePanel;
	}

	public String title() {
		return TITLE;
	}
}
