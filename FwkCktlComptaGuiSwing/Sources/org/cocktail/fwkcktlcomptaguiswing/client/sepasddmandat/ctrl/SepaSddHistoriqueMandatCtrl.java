/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl;

import java.awt.Dimension;
import java.awt.Window;

import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandatHisto;
import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHistoHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonSrchCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.DetailsTableCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddHistoriqueMandatPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddHistoriqueMandatPanel.ISepaSddHistoriqueMandatPanelCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Franck Bordinat - franck.bordinat at asso-cocktail.com
 */
public class SepaSddHistoriqueMandatCtrl extends CommonSrchCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Historique des modifications d'un mandat SEPA SDD";

	private final SepaSddHistoriqueMandatPanel sepaSddHistoriqueMandatPanel;
	private final SepaSddHistoriqueMandatPanelCtrl sepaSddHistoriqueMandatPanelCtrl;

	private final SepaSddHistoriqueMandatListMdl sepaSddHistoriqueMandatListMdl;

	private EOSepaSddMandat currentObject;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public SepaSddHistoriqueMandatCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		sepaSddHistoriqueMandatListMdl = new SepaSddHistoriqueMandatListMdl();

		sepaSddHistoriqueMandatPanelCtrl = new SepaSddHistoriqueMandatPanelCtrl();
		sepaSddHistoriqueMandatPanel = new SepaSddHistoriqueMandatPanel(sepaSddHistoriqueMandatPanelCtrl);
	}


	protected NSMutableArray buildFilterQualifiers() throws Exception {
		return null;
	}

	protected final NSArray getData() throws Exception {
		return null;
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractSwingPanel mainPanel() {
		return sepaSddHistoriqueMandatPanel;
	}

	public String title() {
		return TITLE;
	}

	private void openDetailsDialog() {
		setWaitCursor(true);
		try {
			final DetailsTableCtrl ctrl = new DetailsTableCtrl(sepaSddHistoriqueMandatPanel.getSepaSddMandatListTablePanel());
			ctrl.openDialogModify(getMyDialog());
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final class SepaSddHistoriqueMandatPanelCtrl implements ISepaSddHistoriqueMandatPanelCtrl {

		public Action actionClose() {
			return actionClose;
		}

		public IZTablePanelMdl sepaSddHistoriqueMandatListMdl() {
			return sepaSddHistoriqueMandatListMdl;
		}

	}

	private final class SepaSddHistoriqueMandatListMdl implements IZTablePanelMdl {

		public NSArray getData() throws Exception {
			NSArray modifications = new NSArray();

			EOSepaSddMandat mandatSelectionne = currentObject;
			if (mandatSelectionne != null) {
				modifications = SepaSddMandatHistoHelper.getSharedInstance().getListeModifications(mandatSelectionne);
			}

			return modifications;
		}

		public void onDbClick() {
			openDetailsDialog();
		}

		public void selectionChanged() {
		}

	}

	public final int openDialog(Window dial, EOSepaSddMandat selectedObject) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			currentObject = selectedObject;
			sepaSddHistoriqueMandatPanel.initGUI();
			sepaSddHistoriqueMandatPanel.updateData();
			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

}
