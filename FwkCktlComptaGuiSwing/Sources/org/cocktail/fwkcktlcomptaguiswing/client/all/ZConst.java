/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
/* ZConst.java created by rprin on Tue 23-Sep-2003 */
package org.cocktail.fwkcktlcomptaguiswing.client.all;

import java.awt.Color;

/**
 * Classe contennat des constantes pour l'application.
 */
public abstract class ZConst extends org.cocktail.fwkcktlcompta.common.ZConst {

	public static final int MAX_WINDOW_HEIGHT = 705;
	public static final int MAX_WINDOW_WIDTH = 980;

	/** Couleur pour un élément sélectionné de table */
	public static final Color COLTBSELECTED = new Color(200, 191, 225);
	public static final Color COULEUR_TEXTE_ONGLET_SELECTIONNE = Color.white;
	public static final Color COULEUR_TEXTE_ONGLET_DESELECTIONNE = Color.black;
	public static final Color COULEUR_TB_FONT = new Color(235, 235, 235);

	public static final Color COULEUR_EXERCICE_PREPARATION = Color.decode("#6FB1F4");
	public static final Color COULEUR_EXERCICE_OUVERT = Color.decode("#FFFFFF");
	public static final Color COULEUR_EXERCICE_RESTREINT = Color.decode("#F2C75F");
	public static final Color COULEUR_EXERCICE_CLOS = Color.decode("#F47B83");
	public static final Color BGCOLOR_YELLOW = Color.YELLOW;

	public static final int EXE_EXERCICE_2007 = 2007;

	public static final Color BGCOL_EQUILIBRE = Color.decode("#A0FF99");
	public static final Color BGCOL_DESEQUILIBRE = Color.decode("#FFA099");


	/** Couleur fond Texte en haut de fenetre */
	public static final Color COL_BGD_VIEWTOP = new Color(255, 255, 255);

	public static final Color BG_COLOR_TITLE = Color.decode("#DAC4DA");

	public static final Color BGCOLOR_CREDIT = Color.decode("#D0FFD0");
	public static final Color BGCOLOR_DEBIT = Color.decode("#FFD0D0");
	public static final Color BGCOLOR_POSTIT = Color.decode("#F8F65B");

	/**
	 * Entites à charger par l'application client au lancement (contournement du bug reentered responseToMessage()
	 */
	public static final String[] ENTITIES_TO_LOAD = new String[] {
			"Attribution",
			"Autorisation",
			"AutorisationGestion",
			"Balance",
			"Bordereau",
			"BordereauBrouillard",
			"BordereauInfo",
			"BordereauRejet",
			"Brouillard",
			"CaisseDetail",
			"Cheque",
			"ChequeBrouillard",
			"ChequeDetailEcriture",
			"Comptabilite",
			"ComptabiliteExercice",
			"CompteBancaire",
			"Depense",
			"Domaine",
			"Ecriture",
			"EcritureAnalytique",
			"EcritureDetail",
			"Emargement",
			"EmargementDetail",
			"Exercice",
			"Fonction",
			"Fournisseur",
			"Gestion",
			"GestionExercice",
			"IndividuUlr",
			"Lot",
			"Mandat",
			"MandatBrouillard",
			"MandatDetailEcriture",
			"Marche",
			"ModePaiement",
			"ModeRecouvrement",
			"Numerotation",
			"OperationRapprochement",
			"OperationTresorerie",
			"OperationTresorerieEcriture",
			"OrdreDePaiement",
			"OrdreDePaiementBrouillard",
			"OrdrePaiementDetailEcriture",
			"Organ",
			"Origine",
			"Paiement",
			"Parametre",
			"Personne",
			"PlanAnalytique",
			"PlanComptable",
			"PlanComptableExer",
			"PlancoCredit",
			"PlancoRelance",
			"PlancoVisa",
			"Reimputation",
			"Retenue",
			"Recette",
			"RecetteInfo",
			"RecetteRelance",
			"Rib",
			"Taux",
			"Titre",
			"TitreBrouillard",
			"TitreDetailEcriture",
			"TitreResteRecouvrerLight",
			"TypeBordereau",
			"TypeBrouillard",
			"TypeCredit",
			"TypeEmargement",
			"TypeJournal",
			"TypeNumerotation",
			"TypeOperation",
			"TypeOperationTresor",
			"TypeOrigineBordereau",
			"TypeRelance",
			"TypeRetenue",
			"TypeVirement",
			"Utilisateur",
			"UtilisateurGestion",
			"VirementFichier",
			"VirementParamBdf",
			"VirementParamEtebac",
			"VirementParamVint"

	};

}
