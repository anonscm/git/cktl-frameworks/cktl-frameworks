package org.cocktail.fwkcktlcomptaguiswing.client.all;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;

public interface IConnectedUserInfos {
	public Integer getPersId();

	public String getName();

	public boolean isFonctionAutoriseeByActionID(String fonctionId);

	public Integer getCurrentExerciceAsInt();

	public EOGrhumPersonne getGrhumPersonne();

}
