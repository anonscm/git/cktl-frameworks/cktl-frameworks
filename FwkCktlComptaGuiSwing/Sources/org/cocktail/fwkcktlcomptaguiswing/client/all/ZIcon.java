/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all;

import javax.swing.ImageIcon;

import com.webobjects.eoapplication.client.EOClientResourceBundle;

/**
 * Classe permettant de récupérer les icones de l'application.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZIcon {
	private static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();

	public static final String ICON_EXCLAMATION_14 = "exclamation_14";
	public static final String ICON_INFORMATION_14 = "information_14";
	public static final String ICON_LOCKED_14 = "locked_14";
	public static final String ICON_WARNING_14 = "warning_14";

	public static final String ICON_CANCEL_16 = "button_cancel_16";
	public static final String ICON_ERROR_16 = "error_16";
	public static final String ICON_OK_16 = "button_ok_16";
	public static final String ICON_PRINT_16 = "fileprint_16";
	public static final String ICON_USERS_16 = "config-users_16";
	public static final String ICON_SETTINGS_16 = "settings_16";
	public static final String ICON_MONTH_16 = "month_16";
	public static final String ICON_LOGOUT_16 = "logout_16";
	public static final String ICON_MAIL_16 = "mail_send_16";
	public static final String ICON_FIND_16 = "find_16";
	public static final String ICON_EXIT_16 = "exit_16";
	public static final String ICON_ACROBAT_16 = "acroread_16";
	public static final String ICON_7DAYS_16 = "7days_16";
	public static final String ICON_NEW_16 = "filenew_16";
	public static final String ICON_SAVE_16 = "filesave_16";
	public static final String ICON_OPEN_16 = "fileopen_16";
	public static final String ICON_LOCK_16 = "lock_16";
	public static final String ICON_EXECUTABLE_16 = "executable_16";
	public static final String ICON_CLOSE_16 = "fileclose_16";
	public static final String ICON_EXERCICES_16 = "listedoc16";
	public static final String ICON_ADD_16 = "plus16";
	public static final String ICON_DELETE_16 = "moins16";
	public static final String ICON_LEFT_16 = "leftarrow_16";
	public static final String ICON_RIGHT_16 = "rightarrow_16";
	public static final String ICON_PLUS_16 = "plus_16";
	public static final String ICON_MOINS_16 = "moins_16";
	public static final String ICON_TRAITEMENT_16 = "misc_16";
	public static final String ICON_EDIT_16 = "edit_16";
	public static final String ICON_EDITDELETE_16 = "editdelete_16";
	public static final String ICON_SIGNATURE_16 = "signature_16";
	public static final String ICON_REDLED_16 = "redled_16";
	public static final String ICON_GREENLED_16 = "greenled_16";
	public static final String ICON_UNDO_16 = "undo_16";
	public static final String ICON_XEDIT_16 = "xedit_16";
	public static final String ICON_ECRITURE_16 = "xedit_16";
	public static final String ICON_EMARGMENT_16 = "emargement_16";
	public static final String ICON_JOURNAL_16 = "journal_16";
	public static final String ICON_ODP_16 = "odp_16";
	public static final String ICON_PAIEMENTS_16 = "odp_16";
	public static final String ICON_REMOVE_16 = "remove_16";
	public static final String ICON_MODIF_16 = "modif_16";
	public static final String ICON_ADMINISTRATION_16 = "administration_16";
	public static final String ICON_INFORMATION_16 = "info_16";
	public static final String ICON_PLAY_16 = "play_16";
	public static final String ICON_EXPORTDGCP_16 = "exportdgcp_16";
	public static final String ICON_VIEWTEXT_16 = "view_text_16";
	public static final String ICON_DOWNLOAD_16 = "download_16";
	public static final String ICON_EXCEL_16 = "excel_16";
	public static final String ICON_WIZARD_16 = "wizard_16";
	public static final String ICON_RELOAD_16 = "reload_16";
	public static final String ICON_RELANCE_16 = "relance_16";
	public static final String ICON_JUMELLES_16 = "jumelles";
	public static final String ICON_LOCKB_16 = "lockb_16";
	public static final String ICON_UNLOCK_16 = "unlock_16";
	public static final String ICON_DUPLIQUE_16 = "duplique_16";
	public static final String ICON_CHECKED_16 = "checked_16x16";

	public static final String ICON_INFORMATION_32 = "info_32";
	public static final String ICON_QUESTION_32 = "question_32";
	public static final String ICON_ERROR_32 = "error_32";
	public static final String ICON_WARNING_32 = "warning_32";
	public static final String ICON_EDITCOPY_32 = "editcopy_32";
	public static final String ICON_RUN_32 = "run_32";
	public static final String ICON_TEXTFILE_32 = "textfile_32";
	public static final String ICON_PDFFILE_32 = "pdffile_32";
	public static final String ICON_EXPORTDGCP_32 = "exportdgcp_32";
	public static final String ICON_CHECKED_32 = "checked_32";
	public static final String ICON_EMARGEMENT_32 = "emargement_32";

	public static final String ICON_SANDGLASS = "sandglass";

	public static final String ICON_PASSWORD_64 = "password_64";

	public static final String ICON_ASSISTANT_80 = "assistant_80";

	public static ImageIcon getIconForName(String name) {
		return (ImageIcon) resourceBundle.getObject(name);
	}

}
