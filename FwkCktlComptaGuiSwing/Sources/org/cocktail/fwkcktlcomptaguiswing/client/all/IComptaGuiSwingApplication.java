package org.cocktail.fwkcktlcomptaguiswing.client.all;

import java.awt.Window;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Cette interface doit etre implémentée par les classes Application des applis JC qui utilisent ce framework.
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public interface IComptaGuiSwingApplication {

	public EOEditingContext editingContext();

	public IConnectedUserInfos appUserInfo();

	public void showErrorDialog(Exception e, Window parentWindow);

	public boolean showConfirmationDialog(String titre, String message, String defaultRep, Window parentWindow);

	public void showInfoDialog(String text, Window parentWindow);

	public void showinfoDialogFonctionNonImplementee(Window parentWindow);

	public Window activeWindow();

	public String getApplicationFullName();

	public String getJREVersion();

	public String getTemporaryDir();
}
