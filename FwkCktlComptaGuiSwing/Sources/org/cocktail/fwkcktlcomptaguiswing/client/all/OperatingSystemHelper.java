package org.cocktail.fwkcktlcomptaguiswing.client.all;

import java.io.File;
import java.lang.reflect.Method;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZVersion;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;


public class OperatingSystemHelper {

	public enum OS {
		WINDOWS("Win"),
		MAC_OS_X("Mac OS X"),
		LINUX("Linux");

		private String name;
		private OS(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	/** Ouvre un fichier (ou lance un programme externe) */
	public static void openPdfFile(String filePath) throws Exception {
		openFile(filePath);
	}

	/** Ouvre un fichier (ou lance un programme externe) */
	public static void openFile(String filePath) throws Exception {
		try {
			String platform = System.getProperties().getProperty("os.name");
			File aFile = new File(filePath);
			Runtime runtime = Runtime.getRuntime();
			ZLogger.info("Ouverture du fichier " + aFile);
			if (platform.startsWith(OS.WINDOWS.getName())) {
				if ("Windows NT".equals(platform)) {
					System.out.println("Plateforme " + platform + ", utilisation de cmd.exe /c");
					runtime.exec("cmd.exe /C " + aFile);
				} else {
					runtime.exec(new String[] {
							"rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\""
					});
				}
			} else if (platform.startsWith(OS.MAC_OS_X.getName())) {
				Runtime.getRuntime().exec("open " + aFile);
			} else if (platform.startsWith(OS.LINUX.getName())) {
				final ZVersion curJre = ZVersion.getVersion(System.getProperty("java.version"));
				final ZVersion min = ZVersion.getVersion("1.6.0");

				if (curJre.compareTo(min) >= 0) {
					//On passe par la reflection pour eviter les erreurs de compatibilité en java 1.5
					@SuppressWarnings("rawtypes")
					Class desktopClass = Class.forName("java.awt.Desktop");
					Method getDesktop = desktopClass.getDeclaredMethod("getDesktop", (Class[]) null);
					Method open = desktopClass.getDeclaredMethod("open", new Class[] {
							File.class
					});
					Object desktop = getDesktop.invoke(desktopClass, (Object[]) null);
					open.invoke(desktop, new Object[] {
							aFile
					});

				} else {
					throw new Exception("Utilisez java 1.6 au niveau du client pour pouvoir ouvrir des fichiers sur une plateforme Linux.");
				}
			} else {
				System.out.println("Plateforme non supportee.");
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
