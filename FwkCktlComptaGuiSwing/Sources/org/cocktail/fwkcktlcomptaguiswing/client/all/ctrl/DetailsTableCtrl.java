package org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.DetailsTablePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.DetailsTablePanel.DetailsPanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.IZTablePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;

public class DetailsTableCtrl extends CommonCtrl {

	private static final Dimension DEFAULT_WINDOW_DIMENSION = new Dimension(900, 700);
	private static final String DEFAULT_TITLE = "Détails";

	private Action actionCancel;

	private String title;
	private Dimension dimension;
	private DetailsTablePanel detailsPanel;
	private IZTablePanel tablePanelSource;

	public DetailsTableCtrl(IZTablePanel tablePanelSource) {
		this.title = DEFAULT_TITLE;
		this.dimension = DEFAULT_WINDOW_DIMENSION;
		this.detailsPanel = new DetailsTablePanel(new DetailsPanelListenerImpl());
		this.actionCancel =  new ActionCancel();
		this.tablePanelSource = tablePanelSource;
	}

	@Override
	public String title() {
		return title;
	}

	@Override
	public Dimension defaultDimension() {
		return dimension;
	}

	@Override
	public ZAbstractSwingPanel mainPanel() {
		return detailsPanel;
	}

	public final int openDialogModify(Window parent) {
		final ZKarukeraDialog win = createDialog(parent, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {

			detailsPanel.prepareGuiOnOpen();

			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private void onCancel() {
		getMyDialog().onCancelClick();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public final class ActionCancel extends AbstractAction {
		/** Serial version Id. */
		private static final long serialVersionUID = 1L;

		public ActionCancel() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onCancel();
		}
	}

	private class DetailsPanelListenerImpl implements DetailsPanelListener {
		public Action actionCancel() {
			return actionCancel;
		}

		public IZTablePanel tablePanelSource() {
			return tablePanelSource;
		}
	}
}
