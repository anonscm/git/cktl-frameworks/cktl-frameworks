/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.CommonDialogs;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZCommonDialog;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class CommonCtrl implements ICommonCtrl {
	public static final int MROK = 1;
	public static final int MRCANCEL = 0;
	public final NSArray NSARRAYVIDE = new NSArray();
	protected static final IComptaGuiSwingApplication myApp = (IComptaGuiSwingApplication) EOApplication.sharedApplication();
	protected EOEditingContext _editingContext;
	private ZKarukeraDialog myDialog;
	protected Action actionClose = new ActionClose();

	public CommonCtrl() {
		super();
	}

	/**
	 * Utilisez ce constructeur pour utiliser un nestedEditingContext.
	 *
	 * @param localEditingContext
	 */
	public CommonCtrl(EOEditingContext editingContext) {
		super();
		setEditingContext(editingContext);
	}

	/**
	 * @return l'editingContext défini dans l'application.
	 */
	public final EOEditingContext getDefaultEditingContext() {
		return myApp.editingContext();
	}

	/**
	 * Renvoie l'editing context du composant. Par défaut, renvoie getDefaultEditingContext. A Surcharger si besoin d'un autre... Si un editingContext
	 * particulier a été défini, ce dernier est renvoyé.
	 */
	public EOEditingContext getEditingContext() {
		return (_editingContext == null ? getDefaultEditingContext() : _editingContext);
	}

	/*
	 * protected final EOUtilisateur getUtilisateur() { return myApp.appUserInfo().getUtilisateur(); }
	 *
	 * protected final EOComptabilite getComptabilite() { return myApp.appUserInfo().getCurrentComptabilite(); }
	 *
	 * public final EOExercice getExercice() { return myApp.appUserInfo().getCurrentExercice(); }
	 *
	 * protected final Date getDateJourneeComptable() throws DefaultClientException { return myApp.getDateJourneeComptable(); }
	 *
	 * protected final Date getDateFinCurrentExercice() { return myApp.getDateFinCurrentExercice(); }
	 *
	 *
	 *
	 * protected final void checkExerciceClos() throws DefaultClientException { checkExerciceClos(myApp.appUserInfo().getCurrentExercice()); }
	 *
	 * protected final void checkExerciceClos(final EOExercice exercice) throws DefaultClientException { if (exercice.estClos()) { throw new
	 * DefaultClientException("L'exercice " + exercice.exeExercice().intValue() +
	 * " est clos. Il est interdit de saisir des opérations sur cet exercice."); } }
	 */

	public final EOJefyAdminExercice getExercice() {
		return EOJefyAdminExercice.fetchByKeyValue(getDefaultEditingContext(), EOJefyAdminExercice.EXE_EXERCICE_KEY, myApp.appUserInfo().getCurrentExerciceAsInt());
	}

	/**
	 * Assure que les modifications sur l'editingContext sont bien annulées.
	 */
	protected final void revertChanges() {
		if (getEditingContext() != null) {
			if (getEditingContext().hasChanges()) {
				//		        ZLogger.verbose("Annulation des modification suivantes");
				//		        ZLogger.verbose(getEditingContext());
				getEditingContext().revert();
			}
			//		    else {
			////		        ZLogger.verbose("Pas de modifications à annuler");
			//		    }
		}
	}

	/**
	 * Spécifie l'editing context à utiliser pour ce panel.
	 *
	 * @param context
	 */
	public void setEditingContext(final EOEditingContext context) {
		_editingContext = context;
	}

	/**
	 * @return
	 */
	public ZKarukeraDialog getMyDialog() {
		return myDialog;
	}

	/**
	 * @param dialog
	 */
	public void setMyDialog(final ZKarukeraDialog dialog) {
		myDialog = dialog;
	}

	public void setWaitCursor(final boolean bool) {
		if (getMyDialog() != null) {
			getMyDialog().setWaitCursor(bool);
		}
	}

	public final void showErrorDialog(final Exception e) {
		setWaitCursor(false);
		CommonDialogs.showErrorDialog(getMyDialog(), e);
	}

	public final void showWarningDialog(final String s) {
		CommonDialogs.showWarningDialog(getMyDialog(), s);
	}

	public final boolean showConfirmationDialog(final String titre, final String message, final String defaultRep) {
		return CommonDialogs.showConfirmationDialog(getMyDialog(), titre, message, defaultRep);
	}

	public final int showConfirmationCancelDialog(final String titre, final String message, final String defaultRep) {
		return CommonDialogs.showConfirmationCancelDialog(getMyDialog(), titre, message, defaultRep);
	}

	public final void showInfoDialog(final String text) {
		CommonDialogs.showInfoDialog(getMyDialog(), text);
	}

	public final void showinfoDialogFonctionNonImplementee() {
		myApp.showinfoDialogFonctionNonImplementee(getMyDialog());
	}

	public IComptaGuiSwingApplication getMyApp() {
		return myApp;
	}

	public abstract String title();

	public abstract Dimension defaultDimension();

	public abstract ZAbstractSwingPanel mainPanel();

	/**
	 * Cree un dialog pour ce controleur
	 *
	 * @param parentWindow Dialog ou Frame parente
	 * @param modal
	 * @return
	 */
	public ZKarukeraDialog createDialog(final Window parentWindow, boolean modal) {
		return createDialog(parentWindow, title(), modal);
	}

	public ZKarukeraDialog createDialog(final Window parentWindow, String title, boolean modal) {
		final ZKarukeraDialog win;
		if (parentWindow instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) parentWindow, title, modal);
		}
		else {
			win = new ZKarukeraDialog((Frame) parentWindow, title, modal);
		}
		setMyDialog(win);

		mainPanel().setPreferredSize(defaultDimension());
		win.setContentPane(mainPanel());
		win.pack();
		return win;
	}

	/**
	 * Cree et ouvre une dialog pour ce controleur. Renvoie le code associe au bouton Ok ou Annuler ou autre.
	 *
	 * @param dial Window parente
	 * @param modal
	 * @return Cf ZCommonDialog.MROK et ZCommonDialog.MRCANCEL
	 */
	public int openDialog(Window dial, boolean modal) {
		int res = ZCommonDialog.MRCANCEL;
		ZCommonDialog win = createDialog(dial, modal);
		try {
			mainPanel().onBeforeShow();
			mainPanel().updateData();
			onAfterUpdateDataBeforeOpen();
			win.open();
			res = win.getModalResult();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
			setWaitCursor(false);
		}
		return res;
	}

	/**
	 * Methode a surcharger pour effectuer des traitements juste avant l'ouverture d'une fenetre mais apres que les donnees aient été initialisées.
	 * Par exemple pour préselectionner une information. A surcharger si besoin.
	 */
	public void onAfterUpdateDataBeforeOpen() {
		setWaitCursor(false);
	}

	protected class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onClose();
		}
	}

	protected void onClose() {
		getMyDialog().onCloseClick();
	}

}
