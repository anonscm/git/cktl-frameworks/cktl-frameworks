package org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl;

import java.awt.Window;

import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;

public interface ICommonCtrl {

	Window getMyDialog();

	IComptaGuiSwingApplication getMyApp();

	void showErrorDialog(Exception e1);

}
