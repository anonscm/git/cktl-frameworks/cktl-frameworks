/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;

public class ZPanelNbTotal extends JPanel {
	private ZLabelTextField total;
	private ZLabelTextField nb;
	private String _title;

	public ZPanelNbTotal(String title) {
		super();
		_title = title;
		initGUI();
	}

	public void initGUI() {
		total = new ZLabelTextField("Total ");
		total.getMyTexfield().setColumns(10);
		total.setMyFormat(ZConst.FORMAT_DISPLAY_NUMBER);
		total.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
		total.getMyTexfield().setEditable(false);
		total.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
		total.getMyTexfield().setBackground(Color.WHITE);

		nb = new ZLabelTextField("Nombre ");
		nb.getMyTexfield().setColumns(10);
		nb.setMyFormat(ZConst.FORMAT_ENTIER);
		nb.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
		nb.getMyTexfield().setEditable(false);
		nb.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
		nb.getMyTexfield().setBackground(Color.WHITE);

		Box boxLine1 = Box.createHorizontalBox();
		boxLine1.add(nb);
		boxLine1.add(Box.createRigidArea(new Dimension(2, 2)));
		boxLine1.add(total);
		boxLine1.add(Box.createRigidArea(new Dimension(2, 2)));

		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		if (_title != null) {
			JLabel lab = new JLabel(_title);
			lab.setFont(getFont().deriveFont(Font.BOLD));
			this.add(lab, BorderLayout.LINE_START);
		}
		this.add(Box.createGlue(), BorderLayout.CENTER);
		this.add(boxLine1, BorderLayout.LINE_END);

	}

	public void updateData() {
		total.updateData();
		nb.updateData();

	}

	public void setTotalProvider(ZLabelTextField.IZLabelTextFieldModel prov) {
		total.setMyProvider(prov);
	}

	public void setNbProvider(ZLabelTextField.IZLabelTextFieldModel prov) {
		nb.setMyProvider(prov);
	}

	public void setBackgroundColor(Color color) {
		nb.getMyTexfield().setBackground(color);
		total.getMyTexfield().setBackground(color);
	}

}
