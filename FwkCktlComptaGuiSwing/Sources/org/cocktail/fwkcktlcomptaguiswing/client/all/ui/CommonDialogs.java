/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.Window;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.cocktail.fwkcktlcompta.common.exception.UserActionException;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZMsgPanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;

public final class CommonDialogs {
	private static final IComptaGuiSwingApplication myApp = (IComptaGuiSwingApplication) EOApplication.sharedApplication();

	/**
	 * Affiche une boite de dialogue avec un icone Information et un bouton Ok
	 * 
	 * @see ZMsgPanel#showDialogOk(Window, String, String, javax.swing.Icon)
	 */
	public static final void showInfoDialog(final Window parentWindow, final String text) {
		ZMsgPanel.showDialogOk((parentWindow == null ? myApp.activeWindow() : parentWindow), "Information", text, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_32));
	}

	/**
	 * Affiche une boite de dialogue avec un icone Warning et un bouton Ok
	 * 
	 * @see ZMsgPanel#showDialogOk(Window, String, String, javax.swing.Icon)
	 */
	public static final void showWarningDialog(final Window parentWindow, final String text) {
		ZMsgPanel.showDialogOk((parentWindow == null ? myApp.activeWindow() : parentWindow), "Attention", text, ZIcon.getIconForName(ZIcon.ICON_WARNING_32));
	}

	/**
	 * Affiche une boite de dialogue de confirmation avec les boutons Oui et Non.
	 * 
	 * @see ZMsgPanel#showDialogYesNo(Window, String, String, javax.swing.Icon, String)
	 */
	public static final boolean showConfirmationDialog(final Window parentWindow, final String titre, final String message, final String defaultRep) {
		return (ZMsgPanel.showDialogYesNo((parentWindow == null ? myApp.activeWindow() : parentWindow), titre, message, ZIcon.getIconForName(ZIcon.ICON_QUESTION_32), defaultRep) == ZMsgPanel.MR_YES);
	}

	/**
	 * @param parentWindow La fenetre parente (proprietaire)
	 * @param titre Le titre de la fenetre
	 * @param message Le message a afficher
	 * @param defaultRep Le libelle du bouton a selectionner par defaut ({@link ZMsgPanel#BTLABEL_YES}, {@link ZMsgPanel#BTLABEL_NO},
	 *            {@link ZMsgPanel#BTLABEL_CANCEL})
	 * @return Le code du bouton clique par l'utilisateur ({@link ZMsgPanel#MR_YES}, {@link ZMsgPanel#MR_NO}, {@link ZMsgPanel#MR_CANCEL})
	 * @see ZMsgPanel#showDialogYesNoCancel(Window, String, String, javax.swing.Icon, String)
	 */
	public static final int showConfirmationCancelDialog(final Window parentWindow, final String titre, final String message, final String defaultRep) {
		return (ZMsgPanel.showDialogYesNoCancel((parentWindow == null ? myApp.activeWindow() : parentWindow), titre, message, ZIcon.getIconForName(ZIcon.ICON_QUESTION_32), defaultRep));
	}

	/**
	 * Affiche une boite de dialogue d'erreur
	 * 
	 * @param parentWindow La fenetre parente (proprietaire)
	 * @param e L'exception a afficher
	 */
	public static final void showErrorDialog(final Window parentWindow, final Exception e) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		e.printStackTrace(pw);

		if (!(e instanceof UserActionException)) {
			e.printStackTrace();
		}

		String text = e.getMessage();
		if ((text == null) || (text.trim().length() == 0)) {
			ZLogger.error("Impossible de recuperer le message de l'exception");
			e.printStackTrace();
			text = sw.toString();
			if (text == null || (text.trim().length() == 0)) {
				text = "Une erreur est survenue. Impossible de recuperer le message, il doit etre accessible dans la console...";
			}
		}
		else {
			if (text.contains("20001 -- msg:")) {
				int index = text.indexOf("20001 -- msg:");
				text = text.substring(index + 13);
			}
			String[] msgs = text.split("ORA-20001:");
			if (msgs.length > 0) {
				text = "";
			}
			for (int i = 0; i < msgs.length; i++) {
				String string = msgs[i];
				text = text.concat(string);
			}
			//			if (msgs.length > 1) {
			//				text = msgs[1].split("\n")[0];
			//			}
		}

		text = text.replaceAll("\n", "<br>");

		try {
			Window win = parentWindow;
			if (win == null) {
				win = myApp.activeWindow();
			}
			if (win == null) {
				EODialogs.runInformationDialog("Erreur", text);
			}
			else {
				//                ZLogger.verbose("win==" + win);
				ZMsgPanel.showDialogOkExt(win, "Erreur", text, ZIcon.getIconForName(ZIcon.ICON_ERROR_32), buildExtendedMsg(e), ZIcon.getIconForName(ZIcon.ICON_EDITCOPY_32));
			}

		} catch (Exception e1) {
			e1.printStackTrace();
			//On peut avoir une exception ici notamment liee e un timeout du server
			//Dans ce cas, l'appli peut etre dans l'impossibilite d'instancier
			// le panneau de message
			EODialogs.runInformationDialog("Erreur", text);
		}

	}

	/**
	 * @param e L'exception
	 * @return Une chaine contenant le message de l'exception e + d'autres informations utiles sur l'environnement d'execution.
	 */
	private static final String buildExtendedMsg(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		e.printStackTrace(pw);

		try {
			String text = e.getMessage();
			StringWriter sw2 = new StringWriter();
			sw2.write("Rapport d'erreur - " + ZDateUtil.nowAsDate() + "\n");
			sw2.write("--------------------------------\n");
			sw2.write("Application = " + myApp.getApplicationFullName() + "\n");
			sw2.write("Utilisateur = " + myApp.appUserInfo().getName() + "\n");
			sw2.write("Systeme = " + System.getProperty("os.name") + " " + System.getProperty("os.version") + "\n");
			sw2.write("JVM = " + myApp.getJREVersion() + "\n");
			sw2.write("--------------------------------\n");
			sw2.write("--------- Message court -------\n");
			sw2.write("--------------------------------\n");
			sw2.write(text + "\n");
			sw2.write("--------------------------------\n");
			sw2.write("--------- Message long -------\n");
			sw2.write("--------------------------------\n");
			sw2.write(sw.toString());

			return sw2.toString();
		} catch (Exception e1) {
			return sw.toString();
		}
	}

}
