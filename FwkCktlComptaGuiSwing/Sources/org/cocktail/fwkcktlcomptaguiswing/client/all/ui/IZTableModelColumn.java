package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.text.Format;

public interface IZTableModelColumn {

	/**
	 * @return Le titre de la colonne.
	 */
	String getTitle();

	/**
	 * Retourne la valeur pour l'index de ligne indiqué.
	 *
	 * @param row l'index de la ligne.
	 * @return La valeur que la table va afficher.
	 */
	Object getValueAtRow(final int row);

	/**
	 * @return Le formatter utilise en affichage.
	 */
	Format getFormatDisplay();
}
