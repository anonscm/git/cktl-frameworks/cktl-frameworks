/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOSrchDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN
 */
public class PersonneSrchDialog extends ZEOSrchDialog {

	private static final long serialVersionUID = 1L;
	private EOEditingContext editingContext;
	private EODisplayGroup myDg;
	private IPersonneSrchDialogDelegate srchDelegate;

	protected static final IComptaGuiSwingApplication myApp = (IComptaGuiSwingApplication) EOApplication.sharedApplication();

	/**
	 * @param win
	 * @param title
	 * @param dg
	 * @param cols
	 * @param message
	 * @param action
	 * @param ec
	 */
	public PersonneSrchDialog(Dialog win, String title, EOEditingContext ec, IPersonneSrchDialogDelegate delegate) {
		super(win, title);
		editingContext = ec;
		srchDelegate = delegate;
		initDialog();
	}

	protected void initDialog() {
		myDg = new EODisplayGroup();

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, EOGrhumPersonne.DERIV_FOU_CODE_KEY, "Code", 10);
		col1.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg, EOGrhumPersonne.PERS_LIBELLE_KEY, "Nom", 30);
		col2.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg, EOGrhumPersonne.PERS_LC_KEY, "Prénom", 30);
		col3.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDg, EOGrhumPersonne.DERIV_FOU_TYPE_KEY, "Type", 5);
		col3.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDg, EOGrhumPersonne.DERIV_RIBS_VALIDE_KEY, "Rib", 40);

		Vector cols = new Vector(5, 0);
		cols.add(col2);
		cols.add(col3);
		cols.add(col1);
		cols.add(col4);
		cols.add(col5);

		super.initDialog(myDg, cols, "Cherchez et sélectionnez une personne", new ActionSrch());

	}

	private void updateData() {
		setWaitCursor(true);
		try {
			NSArray res = srchDelegate.getData(editingContext, getFilterText());
			myDg.setObjectArray(res);
			myEOTable.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			myApp.showErrorDialog(e, this);
		} finally {
			setWaitCursor(false);
		}
	}

	private class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher une personne");
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			updateData();
		}

	}

	public ImageIcon getImageIconCancel() {
		return ZIcon.getIconForName(ZIcon.ICON_CANCEL_16);
	}

	public ImageIcon getImageIconOk() {
		return ZIcon.getIconForName(ZIcon.ICON_OK_16);
	}

	@Override
	protected JPanel buildBottomPanel() {
		return super.buildBottomPanel();
	}

	/**
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public interface IPersonneSrchDialogDelegate {
		/**
		 * @param filter Une chaine pour filtrer les résultats
		 * @return Un tableau contenant les résultats de la recherche
		 */
		public NSArray getData(EOEditingContext editingContext, String filter);
	}
}
