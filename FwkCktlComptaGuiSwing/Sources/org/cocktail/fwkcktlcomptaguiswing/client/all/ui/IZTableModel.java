package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.util.List;

public interface IZTableModel {

	/**
	 * @return la definition des colonnes constituant cette table.
	 */
	public List<IZTableModelColumn> getColumns();

}
