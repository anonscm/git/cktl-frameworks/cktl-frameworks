package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

public interface IZTablePanel {

	int selectedRowIndex();
	IZTable getTable();
	IZTableModel getTableModel();

}
