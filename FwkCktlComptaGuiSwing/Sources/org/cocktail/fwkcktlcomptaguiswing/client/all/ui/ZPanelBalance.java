/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.math.BigDecimal;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;


/**
 * Panel qui permet d'afficher un total debit et un total credits. Implémentez le provider pour fournir les données à afficher.
 * Si les totaux sont égaux, ils sont affichés en vert, sinon ils sont affichés en rouge.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZPanelBalance extends JPanel {
	private static final Color COL_EQUILIBRE=Color.decode("#000000");
	private static final Color COL_NONEQUILIBRE=Color.decode("#000000");
	public static final Color BGCOL_EQUILIBRE=Color.decode("#A0FF99");
    public static final Color BGCOL_DESEQUILIBRE=Color.decode("#FFA099");	
	
	private ZTextField debits;
	private ZTextField credits;
	
	private ZTextField soldeDebits;
	private ZTextField soldeCredits;
	
	private IZPanelBalanceProvider _myProvider;
	private boolean showSoldes;
	
	
	
	/**
	 * Crée un panel de total debits/crédits sans les soldes.
	 * @param myProvider Les provider des debits et crédits à afficher.
	 */
	public ZPanelBalance(IZPanelBalanceProvider myProvider) {
		super();
		_myProvider = myProvider;
		showSoldes = false;
	}
	
	/**
	 * Crée un panel de total debits/crédits
	 * @param myProvider Les provider des debits et crédits à afficher.
	 * @param withSoldes Affiche les soldes de chaque coté
	 */
	public ZPanelBalance(IZPanelBalanceProvider myProvider, boolean withSoldes) {
		super();
		_myProvider = myProvider;
		showSoldes = withSoldes;
	}
		


	/**
	 * Met à jour l'affichage d'apres les valeurs fournies par les providers.
	 */
	public void updateData() {
		debits.updateData();
		credits.updateData();
		if (showSoldes) {
		    soldeCredits.updateData();
		    soldeDebits.updateData();
		}
		updateColors();
	}
	
	
	public boolean isEquilibre() {
		return (debits.getMyTexfield().getText().equals( credits.getMyTexfield().getText() ));
	}
	
	
	
	private void updateColors() {
		//Si debit==credit, on ecrit en vert
		if (isEquilibre()) {
			debits.getMyTexfield().setForeground(COL_EQUILIBRE);
			credits.getMyTexfield().setForeground(COL_EQUILIBRE);
			debits.getMyTexfield().setBackground(BGCOL_EQUILIBRE);
			credits.getMyTexfield().setBackground(BGCOL_EQUILIBRE);
		}
		//sinon on ecrit en rouge
		else {
			debits.getMyTexfield().setForeground(COL_NONEQUILIBRE);
			credits.getMyTexfield().setForeground(COL_NONEQUILIBRE);
			debits.getMyTexfield().setBackground(BGCOL_DESEQUILIBRE);
			credits.getMyTexfield().setBackground(BGCOL_DESEQUILIBRE);
		}
	}	
	
	
	
	private ZTextField buildFieldDebit() {
	    ZTextField tmp = new ZTextField();
	    tmp.getMyTexfield().setColumns(10);
	    tmp.setFormat( ZConst.FORMAT_DISPLAY_NUMBER);
	    tmp.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
	    tmp.getMyTexfield().setEditable(false);
	    tmp.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
	    
	    tmp.getMyTexfield().setBackground(this.getBackground().brighter());
//	    tmp.getMyTexfield().setBackground(Color.WHITE);		
	    tmp.setMyModel(new DebitsProvider());
		return tmp;
	}
	
	private ZTextField buildFieldCredit() {
	    ZTextField tmp = new ZTextField();
	    tmp.getMyTexfield().setColumns(10);
	    tmp.setFormat(ZConst.FORMAT_DISPLAY_NUMBER);
	    tmp.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
	    tmp.getMyTexfield().setEditable(false);
	    tmp.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
	    tmp.getMyTexfield().setBackground(this.getBackground().brighter());
//	    tmp.getMyTexfield().setBackground(Color.WHITE);
		tmp.setMyModel(new CreditsProvider());
		return tmp;
	}
	
	private ZTextField buildFieldCreditSolde() {
	    ZTextField tmp = new ZTextField();
	    tmp.getMyTexfield().setColumns(10);
	    tmp.setFormat(ZConst.FORMAT_DISPLAY_NUMBER);
	    tmp.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
	    tmp.getMyTexfield().setEditable(false);
	    tmp.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
	    tmp.getMyTexfield().setBackground(this.getBackground().brighter());
	    tmp.setMyModel(new SoldeCreditsProvider());
		return tmp;
	}
	
	private ZTextField buildFieldDebitSolde() {
	    ZTextField tmp = new ZTextField();
	    tmp.getMyTexfield().setColumns(10);
	    tmp.setFormat(ZConst.FORMAT_DISPLAY_NUMBER);
	    tmp.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
	    tmp.getMyTexfield().setEditable(false);
	    tmp.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
	    tmp.getMyTexfield().setBackground(this.getBackground().brighter());
//	    tmp.getMyTexfield().setBackground(Color.WHITE);
	    tmp.setMyModel(new SoldeDebitsProvider());
		return tmp;
	}	
		
	
	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		debits = buildFieldDebit();
		credits = buildFieldCredit();
		if (showSoldes) {
			soldeDebits = buildFieldDebitSolde();
			soldeCredits = buildFieldCreditSolde();
		}
		
		Box boxLine1 = Box.createHorizontalBox();
		if (showSoldes) {
		    boxLine1.add(new ZLabeledComponent("Solde :",soldeDebits,ZLabeledComponent.LABELONLEFT,-1));
			boxLine1.add(Box.createHorizontalStrut(2));
		}
		boxLine1.add(new ZLabeledComponent("Débits :",debits,ZLabeledComponent.LABELONLEFT,-1));
		boxLine1.add(Box.createHorizontalStrut(2));
		boxLine1.add(new ZLabeledComponent("Crédits :",credits,ZLabeledComponent.LABELONLEFT,-1));
		boxLine1.add(Box.createHorizontalStrut(2));
		if (showSoldes) {
			boxLine1.add(new ZLabeledComponent("Solde :",soldeCredits,ZLabeledComponent.LABELONLEFT,-1));
			boxLine1.add(Box.createHorizontalStrut(2));
		}
		
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createEmptyBorder(4, 2, 2, 2));
//		this.add(Box.createHorizontalGlue(), BorderLayout.CENTER);
		this.add(boxLine1, BorderLayout.CENTER);
		System.out.println("ZPanelBalance.initGUI()");
	}	
	
	
	
	

	private final class DebitsProvider implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return get_myProvider().getDebitValue();
		}

		/**
		 * Ne fait rien, on est en readlonly.
		 */
		public void setValue(Object value) {
			return;
		}
	}
	
	private final class CreditsProvider implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return get_myProvider().getCreditValue();
		}

		/**
		 * Ne fait rien, on est en readlonly.
		 */
		public void setValue(Object value) {
			return;
		}
	}	
	
	private final class SoldeCreditsProvider implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return get_myProvider().getCreditValue().add(get_myProvider().getDebitValue().negate())  ;
		}

		/**
		 * Ne fait rien, on est en readlonly.
		 */
		public void setValue(Object value) {
			return;
		}
	}	
	
	private final class SoldeDebitsProvider implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return get_myProvider().getDebitValue().add(get_myProvider().getCreditValue().negate())  ;
		}

		/**
		 * Ne fait rien, on est en readlonly.
		 */
		public void setValue(Object value) {
			return;
		}
	}		
	
	
	public interface IZPanelBalanceProvider {
		public BigDecimal getDebitValue();
		public BigDecimal getCreditValue();
	}
	
	/**
	 * @return
	 */
	public IZPanelBalanceProvider get_myProvider() {
		return _myProvider;
	}

	/**
	 * @param provider
	 */
	public void set_myProvider(IZPanelBalanceProvider provider) {
		_myProvider = provider;
	}


}
