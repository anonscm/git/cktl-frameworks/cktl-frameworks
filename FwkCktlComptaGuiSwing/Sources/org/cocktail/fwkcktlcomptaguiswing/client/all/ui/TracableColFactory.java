package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.entities.ITracable;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;

/**
 * Factory pour la creation de colonnes liées à la tracabilité (date de modif, personne ayant effectué la modif).
 * 
 * @author rprin
 */
public class TracableColFactory {

	private static TracableColFactory sharedInstance = new TracableColFactory();

	public static TracableColFactory sharedInstance() {
		return sharedInstance;
	}


	public ZEOTableModelColumn getModifiePar(EODisplayGroup displayGroup) {
		ZEOTableModelColumn col = new ZEOTableModelColumn(displayGroup, ITracable.TO_MODIFICATEUR_KEY + "." + EOGrhumPersonne.NOM_AND_PRENOM_KEY, "Modifié par", 10);
		col.setAlignment(SwingConstants.LEFT);
		return col;
	}

	public ZEOTableModelColumn getModifieLe(EODisplayGroup displayGroup) {
		ZEOTableModelColumn col = new ZEOTableModelColumn(displayGroup, ITracable.D_MODIFICATION_KEY, "Modifié le", 10);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(ZConst.DATEISO_FORMAT_DATESHORT);
		return col;
	}

	public ZEOTableModelColumn getModifieLeDateHeure(EODisplayGroup displayGroup) {
		ZEOTableModelColumn col = new ZEOTableModelColumn(displayGroup, ITracable.D_MODIFICATION_KEY, "Modifié le", 15);
		col.setAlignment(SwingConstants.LEFT);
		col.setFormatDisplay(ZConst.DATEISO_FORMAT_DATEHEURE);
		return col;
	}

}
