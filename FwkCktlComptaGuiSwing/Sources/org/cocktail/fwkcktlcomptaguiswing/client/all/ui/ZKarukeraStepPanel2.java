/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;


	
/**
 * Classe abstraite décrivant un panel de l'assistant.
 */	
public abstract class ZKarukeraStepPanel2 extends ZComptaSwingPanel  {
	private final int topHeight = 60;
	private final int bottomHeight = 40;
	private ZStepListener _myListener;
    private static final Dimension BUTTONS_DIMENSION = new Dimension(120,20);
	

    
    public ZKarukeraStepPanel2(ZStepListener listener) {
        super();
        _myListener = listener;
    }
	
	
    public abstract JPanel getCenterPanel();
    public abstract String getTitle();
    public abstract String getCommentaire();    
	
	
	/**
	 * Crée le panneau avec commentaire.
	 * 
	 * @return
	 */
	private final JPanel createCommentaireUI() {
		//Le titre
		JLabel titre = new JLabel(getTitle() );
		titre.setFont(getFont().deriveFont((float)12).deriveFont(Font.BOLD));
		
		//Le blabla
		JLabel tmpTextArea = new JLabel(getCommentaire());
		Color bgcol = Color.decode("#FFFFFF");
		tmpTextArea.setFont(getFont().deriveFont((float)11));
		tmpTextArea.setAlignmentX(SwingConstants.LEFT);
		
		JPanel tmpJPanel = new JPanel(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createMatteBorder(5,5,5,5, bgcol));
		tmpJPanel.setBackground(bgcol);
		
		tmpJPanel.setPreferredSize(new Dimension(1,topHeight));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
//		tmpJPanel.setMaximumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)), BorderLayout.LINE_START);		
		tmpJPanel.add(titre, BorderLayout.PAGE_START);
		tmpJPanel.add(tmpTextArea, BorderLayout.CENTER);
		
		return tmpJPanel;
	}
	
	
	
	private JPanel buildDefaultButtonsPanel() {
		JButton prevButton;
		JButton nextButton;
		JButton closeButton;
//		JButton actionButton1;
		
		Box tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createRigidArea(new Dimension(2,bottomHeight)));
		tmpBox.add(Box.createHorizontalGlue());
	
		//Bouton prev
		if (_myListener.actionPrev()!=null) {
			prevButton = new JButton(_myListener.actionPrev());
			prevButton.setPreferredSize(BUTTONS_DIMENSION);
			prevButton.setMinimumSize(BUTTONS_DIMENSION);
			prevButton.setHorizontalAlignment(SwingConstants.LEFT);
			prevButton.setHorizontalTextPosition(SwingConstants.RIGHT);
			
			if (_myListener.actionPrev().isVisible()) {
				tmpBox.add(prevButton);
				//separateur
				tmpBox.add( Box.createRigidArea(new Dimension(25,30)) );
			}
		}
		
		if (_myListener.actionNext()!=null) {
			nextButton = new JButton(_myListener.actionNext());
			nextButton.setMinimumSize(BUTTONS_DIMENSION);
			nextButton.setPreferredSize(BUTTONS_DIMENSION);
			nextButton.setHorizontalAlignment(SwingConstants.LEFT);
			nextButton.setHorizontalTextPosition(SwingConstants.RIGHT);
			if (_myListener.actionNext().isVisible()) {
				tmpBox.add(nextButton);
				//separateur
				tmpBox.add( Box.createRigidArea(new Dimension(25,30)) );
			}
		}


		if (_myListener.actionSpecial1()!=null) {
			JButton button1 = new JButton(_myListener.actionSpecial1());
			button1.setMinimumSize(BUTTONS_DIMENSION);
			button1.setPreferredSize(BUTTONS_DIMENSION);
			button1.setHorizontalAlignment(SwingConstants.LEFT);
			button1.setHorizontalTextPosition(SwingConstants.RIGHT);
			tmpBox.add(button1 );
			//separateur
			tmpBox.add( Box.createRigidArea(new Dimension(25,30)) );				
		}


		//Bouton Fermer
		if (_myListener.actionClose()!=null) {
			closeButton = new JButton(_myListener.actionClose());
			closeButton.setPreferredSize(BUTTONS_DIMENSION);
			closeButton.setMinimumSize(BUTTONS_DIMENSION);
			closeButton.setHorizontalAlignment(SwingConstants.LEFT);
			closeButton.setHorizontalTextPosition(SwingConstants.RIGHT);
			if (_myListener.actionClose().isVisible()) {
				tmpBox.add(closeButton );
				tmpBox.add( Box.createRigidArea(new Dimension(25,30)) );
			}			
		}
			

		

		JPanel tmpJPanel = new JPanel(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		tmpJPanel.setPreferredSize(new Dimension(1, bottomHeight));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(new JPanel(), BorderLayout.CENTER);	
		tmpJPanel.add(tmpBox, BorderLayout.LINE_END);
		return tmpJPanel;		
		
	}
	
	
	
	
	
	/**
	 * Crée les boutons d'action. Par defaut, fait appel à buildDefaultButtonsPanel. 
	 * Dérivez cette méthode pour personnaliser.
	 * 
	 * @return
	 */
	protected JPanel createActionButtonsUI() {
		return buildDefaultButtonsPanel();
	}
	
	
	/**
	 * Méthode à appeler pour contruire le panel.
	 * 
	 */
	public void initGUI() {
		this.setOpaque(true);
		this.setLayout(new BorderLayout());		
		//le haut
		Box tmpBox = Box.createVerticalBox();
		tmpBox.add(createCommentaireUI() );		
		tmpBox.add(new JSeparator());

		Box tmpBoxBottom = Box.createVerticalBox();
		tmpBoxBottom.add(new JSeparator());
		tmpBoxBottom.add(createActionButtonsUI() );		
		
		this.add(tmpBox, BorderLayout.NORTH);
		this.add(getCenterPanel(), BorderLayout.CENTER);
		this.add(tmpBoxBottom, BorderLayout.SOUTH);
	}
		
		
	public interface ZStepListener {
	    public ZStepAction actionPrev();
	    public ZStepAction actionNext();
	    public ZStepAction actionClose();
	    public ZStepAction actionSpecial1();
	    public ZStepAction actionSpecial2();

	    
	    
	}
		
	public static abstract class ZStepAction extends AbstractAction {
	    public abstract boolean isVisible();
	}

	
	

}	
