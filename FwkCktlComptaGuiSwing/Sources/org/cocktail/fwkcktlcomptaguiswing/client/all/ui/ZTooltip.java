/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;

/**
 * Les différents tooltips d'aide.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class ZTooltip {
	public static final String TOOLTIP_SELECTCOMPTES_TITLE = "Sélection d'un ou plusieurs comptes";
	public static final String TOOLTIP_SELECTCOMPTES_CONTENT = "Vous pouvez sélectionner des comptes de la facon suivante : <ul>" +
			"<li><b>4012</b> : le compte 4012 uniquement</li>" +
			"<li><b>4012,421</b> : le compte 4012 + le compte 421</li>" +
			"<li><b>4*</b> : tous les comptes de la classe 4</li>" +
			"<li><b>44*</b> : tous les comptes du chapître 44</li>" +
			"<li>etc.</li>" +
			"<li><b>18-221</b> : tous les comptes entre le 18 et le 221</li>" +
			"<li><b>40*,441-449</b> : tous les comptes du chapître 40 + tous les comptes<br>entre le 441 et le 449</li>" +
			"</ul>";

	public static final String TOOLTIP_SELECTFOURNIS_TITLE = "Recherche d'un fournisseur";
	public static final String TOOLTIP_SELECTFOURNIS_CONTENT = "Vous pouvez indiquer :</b>" +
			"<ul><li>une partie de son nom</li><li>son code</li>" +
			"<li>une partie de sa ville</li>" +
			"<li>son code postal</li></ul>";
	public static final String TOOLTIP_AGREGATS_TITLE = "Sélection d'un niveau";
	public static final String TOOLTIP_AGREGATS_CONTENT = "Les niveaux incluent le niveau <b>agrégé (Etablissement + SACDs)</b>, <br>" +
			"le niveau <b>Etablissement</b>, chaque <b>SACD</b>, le niveau <b>UB</b> et les regroupements de codes gestion que vous <br>" +
			"pouvez gérer dans l'administration des codes gestions.<br>" +
			"Si un niveau n'est pas disponible, c'est vraisemblablement car vous ne disposez pas de suffisamment de droits <br>" +
			"(par exemple si le niveau Etablissement n'est pas disponible, c'est qu'il vous manque le droit sur au moins un des <br>" +
			"codes gestions de l'établissement pour cette édition).";

	public static final String TOOLTIP_IMPRODP_TITLE = "Impression des ordres de paiement";
	public static final String TOOLTIP_IMPRODP_CONTENT = "Vous pouvez imprimer plusieurs ordres de paiement en une <br>" +
			"seule fois en effectuant une sélection multiple dans la liste<br>" +
			"<i>N.B.: Si vous sélectionnez à la fois des OPs visés et des OPs non visés, <br>" +
			"deux fichiers différents seront générés.</i>";

	public static final String TOOLTIP_EMARGEMENTPLSUISEURSECRITURES_TITLE = "Sélection de plusieurs écritures";
	public static final String TOOLTIP_EMARGEMENTPLSUISEURSECRITURES_CONTENT = "Pour sélectionner plusieurs écritures, utilisez la touche <strong>Ctrl</strong> de votre " +
			"clavier<br>et cliquez sur la ligne que vous souhaitez ajouter/retirer.";

	public static final JLabel getTooltip_AGREGAT() {
		return createTooltipLabel(TOOLTIP_AGREGATS_TITLE, TOOLTIP_AGREGATS_CONTENT);
	}

	public static final JLabel getTooltip_SELECTCOMPTES() {
		return createTooltipLabel(TOOLTIP_SELECTCOMPTES_TITLE, TOOLTIP_SELECTCOMPTES_CONTENT);
	}

	public static final JLabel getTooltip_SELECTFOURNIS() {
		return createTooltipLabel(TOOLTIP_SELECTFOURNIS_TITLE, TOOLTIP_SELECTFOURNIS_CONTENT);
	}

	public static final JLabel getTooltip_IMPRODP() {
		return createTooltipLabel(TOOLTIP_IMPRODP_TITLE, TOOLTIP_IMPRODP_CONTENT);
	}

	public static final JLabel getTooltip_EMARGEMENTPLSUISEURSECRITURES() {
		return createTooltipLabel(TOOLTIP_EMARGEMENTPLSUISEURSECRITURES_TITLE, TOOLTIP_EMARGEMENTPLSUISEURSECRITURES_CONTENT);
	}

	public static final JLabel createTooltipLabel(final String title, final String htmlText) {
		JLabel infoTip = new JLabel(ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16)) {
			public Point getToolTipLocation(MouseEvent event) {
				return new Point(0, 0);
				//                    return new Point(getWidth()/2, getHeight());
			}
		};

		infoTip.setToolTipText("<html>" +
				"<table cellpadding=4><tr style=\"background-color: #83C0DE\"><td><b>" +
				title +
				"</b></td></tr><tr><td>" +
				htmlText +
				"</td></tr></table></html>");
		return infoTip;
	}

}
