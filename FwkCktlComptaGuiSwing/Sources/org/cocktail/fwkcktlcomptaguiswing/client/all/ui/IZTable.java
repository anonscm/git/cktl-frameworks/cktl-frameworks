package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

public interface IZTable {

	/**
	 * Renvoie l'index du 'row' dans le model à partir de son index 'idx' dans la table de la vue.
	 * Ces deux index peuvent être désynchronisés lorsque l'utilisateur trie la vue.
	 *
	 * @param idx index de la ligne dans la table (vue).
	 * @return la correspondance de cet index au niveau du model.
	 */
	int getRowIndexInModel(final int idx);

}
