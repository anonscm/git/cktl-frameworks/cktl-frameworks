/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.border.Border;

import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZKarukeraPanel extends ZAbstractSwingPanel implements ZIUIComponent {
	//	private static final Color DEFAULTTITLESTARTCOLOR=Color.decode("#E5C3F3");
	//	private static final Color DEFAULTTITLESTARTCOLOR=Color.decode("#D4D0F3");
	//	private static final Color DEFAULTTITLESTARTCOLOR=Color.decode("#C3C3C3");
	//	private static final Color DEFAULTTITLESTARTCOLOR=Color.decode("#DAC4DA");
	//	private static final Color DEFAULTTITLEENDCOLOR=Color.decode("#B89DC3");
	//	private static final Color DEFAULTTITLETEXTCOLOR=Color.decode("#000000");
	private static final int TOOLTIPSINITIALDELAY = 100;

	protected IComptaGuiSwingApplication myApp;

	public abstract void initGUI();

	public abstract void updateData() throws Exception;

	private EOEditingContext _editingContext;

	private ZKarukeraDialog myDialog;

	public ZKarukeraPanel() {
		super();
		myApp = (IComptaGuiSwingApplication) EOApplication.sharedApplication();
		ToolTipManager.sharedInstance().setInitialDelay(TOOLTIPSINITIALDELAY);
	}

	//	/**
	//	 * Utilisez ce constructeur  pour utiliser un nestedEditingContext.
	//	 * @param localEditingContext
	//	 */
	//	public ZKarukeraPanel(final EOEditingContext editingContext) {
	//		super();
	//		myApp = (ApplicationClient)EOApplication.sharedApplication();
	//		setEditingContext(_editingContext);
	//		ToolTipManager.sharedInstance().setInitialDelay(TOOLTIPSINITIALDELAY);
	//	}

	/**
	 * @return l'editingContext défini dans l'application.
	 */
	public final EOEditingContext getDefaultEditingContext() {
		return myApp.editingContext();
	}

	/**
	 * Renvoie l'editing context du composant. Par défaut, renvoie getDefaultEditingContext. A Surcharger si besoin d'un autre... Si un editingContext
	 * particulier a été défini, ce dernier est renvoyé.
	 */
	public EOEditingContext getEditingContext() {
		return (_editingContext == null ? getDefaultEditingContext() : _editingContext);
	}

	/**
	 * Crée et renvoie un panneau contenant un titre.
	 * 
	 * @param aTitle Le titre à créer.
	 * @param leftIcon Icone à afficher à gauche du titre
	 * @param rightIcon Icone à afficher à droite du titre
	 */
	public static final JPanel buildTitlePanel(final String aTitle, final Color textColor, final Color startColor, final Color endColor, final ImageIcon leftIcon, final ImageIcon rightIcon) {

		return ZUiUtil.buildTitlePanel(aTitle, textColor, startColor, leftIcon, rightIcon);
	}

	/**
	 * Met le component à l'intérieur d'un panel avec un titre.
	 * 
	 * @param aTitle
	 * @param textColor
	 * @param bgColor
	 * @param comp
	 * @param leftIcon Icone à afficher à gauche du titre
	 * @param rightIcon Icone à afficher à droite du titre
	 * @return
	 */
	public static final JPanel encloseInPanelWithTitle(final String aTitle, final Color textColor, final Color bgColor, final Component comp, final ImageIcon leftIcon, final ImageIcon rightIcon) {
		final JPanel p = new JPanel(new BorderLayout());
		//	    p.add(buildTitlePanel(aTitle, textColor, bgColor, endColor, leftIcon, rightIcon), BorderLayout.NORTH);
		p.add(ZUiUtil.buildTitlePanel(aTitle, textColor, bgColor, leftIcon, rightIcon), BorderLayout.NORTH);
		p.add(comp, BorderLayout.CENTER);
		return p;
	}

	/**
	 * Spécifie l'editing context à utiliser pour ce panel.
	 * 
	 * @param context
	 */
	public void setEditingContext(final EOEditingContext context) {
		_editingContext = context;
	}

	/**
	 * @return
	 */
	public ZKarukeraDialog getMyDialog() {
		return myDialog;
	}

	/**
	 * @param dialog
	 */
	public void setMyDialog(final ZKarukeraDialog dialog) {
		myDialog = dialog;
	}

	public void setWaitCursor(final boolean bool) {
		getMyDialog().setWaitCursor(bool);
	}

	/**
	 * Contsruit et renvoie une ligne (composant comp aligné à gauche, et espace vide à droite)
	 * 
	 * @param comp
	 * @return
	 */
	public static JPanel buildLine(final Component comp) {
		final JPanel p = new JPanel(new BorderLayout());
		p.add(comp, BorderLayout.WEST);
		p.add(new JPanel(), BorderLayout.CENTER);
		return p;
	}

	/**
	 * Contsruit et renvoie un panel sous forme construit en ligne à partir des composants passés en paramètre.
	 * 
	 * @param comps
	 * @return
	 */
	public static JPanel buildLine(final Component[] comps) {
		final JPanel p = new JPanel(new BorderLayout());
		final Component vide = Box.createRigidArea(new Dimension(4, 1));
		final Box box = Box.createHorizontalBox();
		for (int i = 0; i < comps.length; i++) {
			final Component component = comps[i];
			box.add(component);
			box.add(vide);
		}
		p.add(box, BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	public static JPanel buildLine(final ArrayList comps) {
		final JPanel p = new JPanel(new BorderLayout());
		final Component vide = Box.createRigidArea(new Dimension(4, 1));
		final Box box = Box.createHorizontalBox();
		for (int i = 0; i < comps.size(); i++) {
			final Component component = (Component) comps.get(i);
			box.add(component);
			box.add(vide);
		}
		p.add(box, BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	/**
	 * @param comps
	 * @return Un panel horizontal constitué des éléments passés en paramètres.
	 */
	public static JPanel buildHorizontalPanelOfComponent(final Component[] comps) {
		final GridLayout thisLayout = new GridLayout(1, comps.length);
		thisLayout.setVgap(20);
		thisLayout.setHgap(20);
		final JPanel p = new JPanel(thisLayout);
		for (int i = 0; i < comps.length; i++) {
			p.add(comps[i]);
		}
		return p;
	}

	public static JPanel buildHorizontalPanelOfComponent(final ArrayList comps) {
		final JPanel p = new JPanel();
		final Iterator iterator = comps.iterator();
		final GridLayout thisLayout = new GridLayout(1, comps.size());
		thisLayout.setVgap(20);
		thisLayout.setHgap(20);
		p.setLayout(thisLayout);
		while (iterator.hasNext()) {
			final Component c = (Component) iterator.next();
			p.add(c);
		}
		return p;
	}

	public static JPanel buildVerticalPanelOfComponent(final Component[] comps) {
		final GridLayout thisLayout = new GridLayout(comps.length, 1);
		thisLayout.setVgap(5);
		thisLayout.setHgap(20);
		final JPanel p = new JPanel(thisLayout);
		for (int i = 0; i < comps.length; i++) {
			p.add(comps[i]);
		}
		return p;
	}

	public static JPanel buildVerticalPanelOfComponents(final ArrayList comps) {
		final JPanel p = new JPanel();
		final Iterator iterator = comps.iterator();
		final GridLayout thisLayout = new GridLayout(comps.size(), 1);
		thisLayout.setVgap(20);
		p.setLayout(thisLayout);
		while (iterator.hasNext()) {
			final Component c = (Component) iterator.next();
			p.add(c);
		}
		return p;
	}

	public static JPanel buildVerticalPanelOfComponents(final ArrayList comps, int vGap, int hGap) {
		final JPanel p = new JPanel();
		final Iterator iterator = comps.iterator();
		final GridLayout thisLayout = new GridLayout(comps.size(), 1);
		thisLayout.setVgap(vGap);
		thisLayout.setHgap(hGap);
		p.setLayout(thisLayout);
		while (iterator.hasNext()) {
			final Component c = (Component) iterator.next();
			p.add(c);
		}
		return p;
	}

	public static JPanel buildVerticalSeparator(final int height) {
		final Component b = Box.createVerticalStrut(height);
		final JPanel p = new JPanel(new BorderLayout());
		p.add(b, BorderLayout.NORTH);
		return p;
	}

	public static final ArrayList getButtonListFromActionList(final ArrayList actions) {
		return getButtonListFromActionList(actions, 97, 25);
	}

	public static final JButton getButtonFromAction(final Action action) {
		return getButtonFromAction(action, 97, 25);
	}

	public static final ArrayList getButtonListFromActionList(final ArrayList actions, final int minWidth, final int minHeight) {
		final ArrayList res = new ArrayList(actions.size());
		final Iterator iterator = actions.iterator();
		while (iterator.hasNext()) {
			final Action element = (Action) iterator.next();
			if (element != null) {
				res.add(getButtonFromAction(element, minWidth, minHeight));
			}
		}
		return res;
	}

	public static final JButton getButtonFromAction(final Action action, final int minWidth, final int minHeight) {
		final JButton button = new JButton(action);
		button.setHorizontalAlignment(JButton.LEFT);
		button.setMinimumSize(new Dimension(minWidth, minHeight));
		button.setPreferredSize(new Dimension(minWidth, minHeight));
		return button;
	}

	public final static Box buildColumn(final Component[] comps) {
		final Component vide = Box.createRigidArea(new Dimension(1, 4));
		final Box box = Box.createVerticalBox();
		for (int i = 0; i < comps.length; i++) {
			final Component component = comps[i];
			box.add(component);
			box.add(vide);
		}
		box.add(Box.createVerticalGlue());
		return box;
	}

	public static final JPanel buildVerticalPanelOfButtonsFromActions(final Action[] actions) {
		final JPanel p = new JPanel();
		final GridLayout thisLayout = new GridLayout(actions.length, 1);
		thisLayout.setVgap(20);
		p.setLayout(thisLayout);
		for (int i = 0; i < actions.length; i++) {
			final Action element = actions[i];
			final JButton button = new JButton(element);
			button.setHorizontalAlignment(JButton.LEFT);
			p.add(button);
		}
		return p;
	}

	/**
	 * @param actions
	 * @return Une box contenant les boutons construits à partir des actions.
	 */
	public static Box buildHorizontalButtonsFromActions(final ArrayList actions) {
		final Box tmpBox = Box.createHorizontalBox();
		//Remplacer la ligne suivante par un
		//Box.createHorizontalStrut(2);
		tmpBox.add(Box.createRigidArea(new Dimension(2, 40)));
		tmpBox.add(Box.createGlue());
		final Iterator iterator = actions.iterator();
		while (iterator.hasNext()) {
			final Action element = (Action) iterator.next();
			final JButton button = new JButton(element);
			button.setMinimumSize(new Dimension(100, 30));
			button.setPreferredSize(new Dimension(100, 30));
			button.setSize(button.getPreferredSize());
			tmpBox.add(button);
			tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
		}
		return tmpBox;
	}

	public final static JSplitPane buildVerticalSplitPane(final Component compTop, final Component compBottom) {
		final JSplitPane split1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, compTop, compBottom);
		split1.setDividerLocation(0.5);
		split1.setResizeWeight(0.5);
		split1.setBorder(BorderFactory.createEmptyBorder());
		return split1;
	}

	/**
	 * @return Une bordure vide avec une marge.
	 */
	public final static Border createMargin() {
		return BorderFactory.createEmptyBorder(15, 10, 15, 10);
	}

	public final void showErrorDialog(final Exception e) {
		myApp.showErrorDialog(e, getMyDialog());
	}

	public final boolean showConfirmationDialog(final String titre, final String message, final String defaultRep) {
		return myApp.showConfirmationDialog(titre, message, defaultRep, getMyDialog());
	}

	public final void showInfoDialog(final String text) {
		myApp.showInfoDialog(text, getMyDialog());
	}

	public final void showinfoDialogFonctionNonImplementee() {
		myApp.showinfoDialogFonctionNonImplementee(getMyDialog());
	}

	/**
	 * Affiche une boite de saisie simple et renvoie la chaine saisie.
	 * 
	 * @param atitle
	 * @param aLabel
	 * @return
	 */
	public static final String askValueInDialog(final Dialog owner, final String atitle, final String aLabel, int maxChars) {
		final ZKarukeraDialog dial = new ZKarukeraDialog(owner, atitle, true);
		final JPanel panel = new JPanel(new BorderLayout());

		final JLabel label = new JLabel(aLabel);
		final ZTextField textField = new ZTextField();
		textField.getMyTexfield().setColumns(10);
		textField.setMaxChars(maxChars);

		final JPanel panel2 = buildLine(new Component[] {
				label, textField
		});
		panel2.setBorder(BorderFactory.createEmptyBorder(30, 10, 30, 10));
		panel.add(panel2, BorderLayout.NORTH);
		panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		final StringWriter res = new StringWriter();

		final Action actionOk = new AbstractAction("Ok") {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				res.write(textField.getMyTexfield().getText());
				dial.setVisible(false);
			};
		};

		final Action actionCancel = new AbstractAction("Annuler") {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				dial.setVisible(false);
			};
		};

		actionOk.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
		actionCancel.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));

		final ArrayList actions = new ArrayList(2);
		actions.add(actionOk);
		actions.add(actionCancel);
		final ArrayList buttons = getButtonListFromActionList(actions);
		final JPanel bottomPanel = new JPanel();
		bottomPanel.add(buildHorizontalPanelOfComponent(buttons));
		panel.add(bottomPanel, BorderLayout.SOUTH);

		dial.setContentPane(panel);
		dial.pack();
		dial.centerWindow();
		dial.setVisible(true);
		return res.toString();
	}

	public final static JPanel createCommentaireUI(Font defFont, final String title, final String comment) {
		//Le titre
		final JLabel titre = new JLabel(title);
		titre.setFont(defFont.deriveFont((float) 12).deriveFont(Font.BOLD));

		//Le blabla
		JTextArea tmpTextArea = new JTextArea(comment);
		//tmpTextArea.setMargin(new Insets(3,3,3,3));
		Color bgcol = Color.decode("#FFFFFF");
		tmpTextArea.setFont(defFont.deriveFont((float) 11));
		tmpTextArea.setEditable(false);
		tmpTextArea.setAlignmentX(SwingConstants.LEFT);
		//      tmpTextArea.setBackground(bgcol);
		//      tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)));

		JPanel tmpJPanel = new JPanel();
		tmpJPanel.setLayout(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, bgcol));
		tmpJPanel.setBackground(bgcol);

		tmpJPanel.setPreferredSize(new Dimension(1, 40));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
		//      tmpJPanel.setMaximumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(Box.createRigidArea(new Dimension(1, 40)), BorderLayout.LINE_START);
		tmpJPanel.add(titre, BorderLayout.PAGE_START);
		tmpJPanel.add(tmpTextArea, BorderLayout.CENTER);
		return tmpJPanel;
	}

	protected final void setSimpleLineBorder(final JComponent c) {

		ZUiUtil.setSimpleLineBorder(c);
	}
}
