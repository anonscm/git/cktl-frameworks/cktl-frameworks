/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.TableSorter;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZComptaSwingTablePanel extends ZComptaSwingPanel implements IZTablePanel, ZEOTable.ZEOTableListener {
	private static final long serialVersionUID = 1L;

	protected IZKarukeraTablePanelListener myListener;

	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected final EODisplayGroup myDisplayGroup = new EODisplayGroup();
	protected TableSorter myTableSorter;
	protected HashMap<String, ZEOTableModelColumn> colsMap = new LinkedHashMap<String, ZEOTableModelColumn>();

	public ZComptaSwingTablePanel(IZKarukeraTablePanelListener listener) {
		super();
		myListener = listener;
	}

	protected void initTableModel() {
		myTableModel = new ZEOTableModel(myDisplayGroup, colsMap.values());
		myTableSorter = new TableSorter(myTableModel);
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		//	myEOTable.setMyRenderer(myListener.getTableRenderer());
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public void initGUI() {
		initTableModel();
		initTable();
		setLayout(new BorderLayout());
		add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.ZComptaSwingPanel.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myDisplayGroup.setObjectArray(myListener.getData());
		myEOTable.updateData();
	}

	public static interface IZKarukeraTablePanelListener {
		public void selectionChanged();

		public IZEOTableCellRenderer getTableRenderer();

		public NSArray getData() throws Exception;

		public void onDbClick();
		//        public void onNewSelection();

	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		myListener.onDbClick();
	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.selectionChanged();
	}

	/**
	 * @return
	 */
	public EOEnterpriseObject selectedObject() {
		return (EOEnterpriseObject) myDisplayGroup.selectedObject();
	}

	public NSArray selectedObjects() {
		return myDisplayGroup.selectedObjects();
	}

	public int selectedRowIndex() {
		return myEOTable.getSelectedRow();
	}

	/**
	 * Annule la saisie en cours dans la celule si celle-ci est en mode édition.
	 */
	public void cancelCellEditing() {
		myEOTable.cancelCellEditing();
	}

	public boolean isEditing() {
		return myEOTable.isEditing();
	}

	/**
	 * Termine l'édition en cours dans la cellule éventuellement en mode edition.
	 *
	 * @return True si l'édition a bien été validée.
	 */
	public boolean stopCellEditing() {
		return myEOTable.stopCellEditing();
	}

	public EODisplayGroup getMyDisplayGroup() {
		return myDisplayGroup;
	}

	/**
     *
     */
	public void fireTableDataChanged() {
		myTableModel.fireTableDataChanged();
	}

	public void fireTableCellUpdated(final int row, final int col) {
		myEOTable.getDataModel().fireTableCellUpdated(row, myEOTable.convertColumnIndexToModel(col));
	}

	public void fireTableRowUpdated(final int row) {
		myEOTable.getDataModel().fireTableRowUpdated(row);
	}

	public void fireSelectedTableRowUpdated() {
		fireTableRowUpdated(selectedRowIndex());
	}

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	/** IZtable methods definitions */
	public IZTable getTable() {
		return getMyEOTable();
	}

	public IZTableModel getTableModel() {
		return getMyTableModel();
	}

	/**
	 * @param columnModelIndex
	 * @return
	 */
	public TableColumn getTableColumnAtIndex(int columnModelIndex) {
		final Enumeration<TableColumn> enumeration = myEOTable.getColumnModel().getColumns();
		for (; enumeration.hasMoreElements();) {
			final TableColumn col = (TableColumn) enumeration.nextElement();
			if (col.getModelIndex() == columnModelIndex) {
				return col;
			}
		}
		return null;
	}

	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

}
