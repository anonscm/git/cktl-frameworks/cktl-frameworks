/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.IZDataComponent;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZLabelField extends JPanel implements IZDataComponent {
	public static final int LABELONLEFT = 0;
	public static final int LABELONTOP = 1;
	private JLabel myLabel;
	private int labelOrientation = 0;

	public abstract Component getContentComponent();

	/**
	 * @return
	 */
	public JLabel getMyLabel() {
		return myLabel;
	}

	protected void initObject(String label, int aLabelOrientation, int aPreferedWidth) {
		labelOrientation = aLabelOrientation;
		myLabel = new JLabel(label);
		final Component vide;
		//		myTexfield.setPreferredSize(preferredSize)
		Box zcontainer;
		if (aLabelOrientation == LABELONLEFT) {
			zcontainer = Box.createHorizontalBox();
			vide = Box.createRigidArea(new Dimension(4, 1));
		}
		else {
			zcontainer = Box.createVerticalBox();
			vide = Box.createRigidArea(new Dimension(1, 2));
		}
		myLabel.setFocusable(false);
		zcontainer.add(myLabel);
		//		on ajoute un espace entre le label et le textfield
		zcontainer.add(vide);
		zcontainer.add(getContentComponent());
		this.add(zcontainer);
		this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

	}

	/**
	 * @return
	 */
	public int getLabelOrientation() {
		return labelOrientation;
	}

	/**
	 * @param i
	 */
	public void setLabelOrientation(int i) {
		labelOrientation = i;
	}

}
