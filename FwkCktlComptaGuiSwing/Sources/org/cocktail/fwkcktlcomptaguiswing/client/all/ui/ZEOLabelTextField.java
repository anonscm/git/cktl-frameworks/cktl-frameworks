/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;



import com.webobjects.eocontrol.EOEnterpriseObject;


/**
 * Permet de définir un champ avec label qui affiche et modifie les valeur d'un champ spcécifique 
 * d'un EOEnterpriseObject (la lecture et l'écriture des valeurs s'effectue via valueForKey et setValueForKey).
 */
public class ZEOLabelTextField extends ZLabelTextField {
	private String myAttributeName;
	private IZEOLabelTextFieldModel myProvider;

	public ZEOLabelTextField(String label, String attName, IZEOLabelTextFieldModel aProvider) {
		super(label, aProvider);
		myAttributeName = attName;		
		myProvider = aProvider;
	}
	
	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField#initObject(java.lang.String, int, int)
	 */
	protected void initObject(String label,int aLabelOrientation,int aPreferedWidth) {
		super.initObject(label, aLabelOrientation, aPreferedWidth);
	}


	protected String getValueToDisplay() {
		Object val;
//		System.out.println("attname: "+myAttributeName);
//		System.out.println("provider: "+myProvider);
//		System.out.println("eo: "+myProvider.getEo());
		
		if ( myAttributeName.indexOf(".")>0 ) {
			val = myProvider.getEo().valueForKeyPath(  myAttributeName  ) ;
		}
		else {
			val = myProvider.getEo().valueForKey(  myAttributeName  ) ;
		}	
		if (val!=null) {
			if (getMyFormat()!=null) {
				return getMyFormat().format(val);					
			}
            return val.toString();
		}
        return null;
	}
	



	
	/**
	 * Interface qui représente l'objet qui va fournir les données au ZEOLabelTextField.
	 */
	public interface IZEOLabelTextFieldModel extends IZLabelTextFieldModel {
		/**
		 * Doit renvoyer l'enterpriseObjet contenant les champs à afficher.
		 */
		public EOEnterpriseObject getEo();
		
	}
	
	
	
	
	
	
	
	

}
