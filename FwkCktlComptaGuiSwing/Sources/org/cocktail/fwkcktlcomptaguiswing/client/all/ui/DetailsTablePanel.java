package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIDefaults;

import org.cocktail.fwkcktlcompta.common.util.BooleanFormat;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;

public class DetailsTablePanel extends ZComptaSwingPanel {

	/** Serial version UID. */
	private static final long serialVersionUID = 1L;

	private static final String EMPTY_STRING = "";
	private static final String VALEUR_INCONNUE = "Non disponible";
	private static final Map<Class<?>, Format> DEFAULT_FORMATTERS = new HashMap<Class<?>, Format>();
	static {
		DEFAULT_FORMATTERS.put(Boolean.class, new BooleanFormat());
	}

	private DetailsPanelListener listener;

	public DetailsTablePanel(DetailsPanelListener listener) {
		this.listener = listener;
	}

	@Override
	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildMainPanel(), BorderLayout.CENTER);
		this.add(buildActionsPrincipalesPanel(), BorderLayout.SOUTH);
	}

	@Override
	public void updateData() throws Exception {
	}

	public void prepareGuiOnOpen() throws Exception {
		initGUI();
		updateData();
	}

	private final JPanel buildMainPanel() {
		IZTablePanel tablePanel = listener.tablePanelSource();
		IZTableModel tableModel = tablePanel.getTableModel();

		int firstSelectedRowIndex = tablePanel.selectedRowIndex();
		int rowIndexWithinModelContext = tablePanel.getTable().getRowIndexInModel(firstSelectedRowIndex);

		Builder builder = new Builder();
		List<IZTableModelColumn> columnsModel = tableModel.getColumns();
		for (IZTableModelColumn columnModel : columnsModel) {
			Object detailsValue = columnModel.getValueAtRow(rowIndexWithinModelContext);
			Format formatter = columnModel.getFormatDisplay();
			builder.details(columnModel.getTitle(), prepareDisplayValue(detailsValue, formatter));
		}

		return builder.build();
	}

	private String prepareDisplayValue(Object value, Format formatter) {
		String detailsValueToDisplay = null;

		if (value == null || EMPTY_STRING.equals(value)) {
			detailsValueToDisplay = VALEUR_INCONNUE;
		} else {
			if (formatter != null) {
				detailsValueToDisplay = formatter.format(value);
			} else if (DEFAULT_FORMATTERS.containsKey(value.getClass())) {
				detailsValueToDisplay = DEFAULT_FORMATTERS.get(value.getClass()).format(value);
			} else {
				detailsValueToDisplay = value.toString();
			}
		}

		return detailsValueToDisplay;
	}

	private final JPanel buildActionsPrincipalesPanel() {
		List<Action> actions = new ArrayList<Action>();
		actions.add(listener.actionCancel());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(actions));
		return p;
	}

	public static class Builder {
		private List<Component[]> details = new ArrayList<Component[]>();


		public Builder details(String detailsTitle, String detailsValue) {
			details.add(wrapWithLabel(detailsTitle, wrapText(detailsValue)));
			return this;
		}

		public JPanel build() {
			final JPanel panel = new JPanel(new BorderLayout());
			panel.add(ZUiUtil.buildForm(details), BorderLayout.NORTH);
			panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

			return panel;
		}

		private Component wrapText(String value) {
			MultilineLabel wrappedValue = new MultilineLabel(value);

			return wrappedValue;
		}

		private Component[] wrapWithLabel(String label, Component value) {
			return new Component[] {
					labelize(label), value
			};
		}

		private JLabel labelize(String label) {
			JLabel wrappedLabel = new JLabel(label);
			Font font = wrappedLabel.getFont();
			Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
			wrappedLabel.setFont(boldFont);

			return wrappedLabel;
		}

		/**
	     * Sets the text component transparent. It will call setOpaque(false) and also set client property for certain L&Fs
	     * in case the L&F doesn't respect the opaque flag.
	     * @param component the text component to be set to transparent.
	     */
	    public void setComponentTransparent(JComponent component) {
	        component.setOpaque(false);

	        // add this for the Synthetica
	        component.putClientProperty("Synthetica.opaque", false);
	        // 	add this for Nimbus to disable all the painting of a component in Nimbus
	        component.putClientProperty("Nimbus.Overrides.InheritDefaults", false);
	        component.putClientProperty("Nimbus.Overrides", new UIDefaults());
	    }
	}

	public interface DetailsPanelListener {
		Action actionCancel();
		IZTablePanel tablePanelSource();
	}

}
