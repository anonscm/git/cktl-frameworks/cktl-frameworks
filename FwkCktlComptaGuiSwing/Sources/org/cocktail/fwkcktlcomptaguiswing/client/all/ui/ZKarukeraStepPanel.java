/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe abstraite décrivant un panel de l'assistant.
 */
public abstract class ZKarukeraStepPanel extends ZComptaSwingPanel {
	private final int topHeight = 60;
	private final int bottomHeight = 40;
	private final Dimension BUTTONS_DIMENSION = new Dimension(120, 30);

	public abstract boolean isPrevVisible();

	public abstract boolean isNextVisible();

	public abstract boolean isPrevEnabled();

	public abstract boolean isNextEnabled();

	public abstract boolean isCloseEnabled();

	public abstract boolean isCloseVisible();

	public abstract void onPrev();

	public abstract void onNext();

	public abstract void onClose();

	public abstract void onDisplay();

	public abstract String getCommentaire();

	public abstract String getTitle();

	public abstract AbstractAction specialAction1();

	//	private JButton prevButton;
	//	private JButton nextButton;
	//	private JButton closeButton;
	//	private JButton actionButton1;

	private Action prevAction;
	private Action nextAction;
	private Action closeAction;

	//	private Action action1Action;

	/**
	 * Renvoie la taille du panel.
	 * 
	 * @return
	 */
	public abstract Dimension getPanelDimension();

	/**
	 * Renvoie le panel à afficher dans la zone de contenu.
	 * 
	 * @return
	 */
	public abstract JPanel getContentPanel();

	/**
	 * Cette méthode doit permettre d'effectuer des vérifications concernant la validation de la saisie et etre appelée par onNext(). Elle peut
	 * également etre appellée à partir d'un autre endroit pour vérifier la saisie et eventuellement effectuer des traiements avant de quitter
	 * l'écran.
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean valideSaisie() throws Exception;

	/**
	 * Crée le panneau avec commentaire.
	 * 
	 * @return
	 */
	private JPanel createCommentaireUI() {
		//Le titre
		JLabel titre = new JLabel(getTitle());
		titre.setFont(getFont().deriveFont((float) 12).deriveFont(Font.BOLD));

		//Le blabla
		JTextArea tmpTextArea = new JTextArea(getCommentaire());
		//tmpTextArea.setMargin(new Insets(3,3,3,3));
		Color bgcol = Color.decode("#FFFFFF");
		tmpTextArea.setFont(getFont().deriveFont((float) 11));
		tmpTextArea.setEditable(false);
		tmpTextArea.setAlignmentX(SwingConstants.LEFT);
		//		tmpTextArea.setBackground(bgcol);
		//		tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)));

		JPanel tmpJPanel = new JPanel();
		tmpJPanel.setLayout(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, bgcol));
		tmpJPanel.setBackground(bgcol);

		tmpJPanel.setPreferredSize(new Dimension(1, 40));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
		//		tmpJPanel.setMaximumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(Box.createRigidArea(new Dimension(1, 40)), BorderLayout.LINE_START);
		tmpJPanel.add(titre, BorderLayout.PAGE_START);
		tmpJPanel.add(tmpTextArea, BorderLayout.CENTER);
		return tmpJPanel;
	}

	private JPanel buildDefaultButtonsPanel() {
		JButton prevButton;
		JButton nextButton;
		JButton closeButton;
		//		JButton actionButton1;

		Box tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createRigidArea(new Dimension(2, bottomHeight)));
		tmpBox.add(Box.createHorizontalGlue());

		prevAction = new AbstractAction("Précédent") {
			public void actionPerformed(ActionEvent e) {
				onPrev();
			}
		};
		prevAction.setEnabled(this.isPrevEnabled());
		prevAction.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
		//Bouton prev
		prevButton = new JButton(prevAction);
		//		prevButton.setFont(this.getFont().deriveFont(this.getFont().getStyle() + Font.BOLD));
		//		prevButton.setText(this.buttonLabels[0]);
		prevButton.setPreferredSize(BUTTONS_DIMENSION);
		prevButton.setMinimumSize(BUTTONS_DIMENSION);
		prevButton.setHorizontalAlignment(SwingConstants.LEFT);
		prevButton.setHorizontalTextPosition(SwingConstants.RIGHT);
		//		prevButton.setSize(prevButton.getPreferredSize());
		if (isPrevVisible()) {
			tmpBox.add(prevButton);
			//separateur
			tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
		}

		nextAction = new AbstractAction("Suivant") {
			public void actionPerformed(ActionEvent e) {
				onNext();
			}
		};
		nextAction.setEnabled(this.isNextEnabled());
		nextAction.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
		nextButton = new JButton(nextAction);
		nextButton.setMinimumSize(BUTTONS_DIMENSION);
		nextButton.setPreferredSize(BUTTONS_DIMENSION);
		nextButton.setHorizontalAlignment(SwingConstants.LEFT);
		nextButton.setHorizontalTextPosition(SwingConstants.RIGHT);
		if (isNextVisible()) {
			tmpBox.add(nextButton);
			//separateur
			tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
		}

		if (specialAction1() != null) {
			JButton button1 = new JButton(specialAction1());
			button1.setMinimumSize(BUTTONS_DIMENSION);
			button1.setPreferredSize(BUTTONS_DIMENSION);
			button1.setHorizontalAlignment(SwingConstants.LEFT);
			button1.setHorizontalTextPosition(SwingConstants.RIGHT);
			tmpBox.add(button1);
			//separateur
			tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
		}

		//Bouton Fermer
		closeAction = new AbstractAction("Fermer") {
			public void actionPerformed(ActionEvent e) {
				onClose();
			}
		};
		closeAction.setEnabled(this.isCloseEnabled());
		closeAction.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		//Bouton prev
		closeButton = new JButton(closeAction);
		//		prevButton.setFont(this.getFont().deriveFont(this.getFont().getStyle() + Font.BOLD));
		//		prevButton.setText(this.buttonLabels[0]);
		closeButton.setPreferredSize(BUTTONS_DIMENSION);
		closeButton.setMinimumSize(BUTTONS_DIMENSION);
		//		closeButton.setSize(prevButton.getPreferredSize());
		closeButton.setHorizontalAlignment(SwingConstants.LEFT);
		closeButton.setHorizontalTextPosition(SwingConstants.RIGHT);

		if (isCloseVisible()) {
			tmpBox.add(closeButton);
			tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
		}

		//		tmpBox.add(Box.createGlue());

		JPanel tmpJPanel = new JPanel();
		tmpJPanel.setLayout(new BorderLayout());
		//		tmpJPanel.setBorder(BorderFactory.createMatteBorder(5,5,5,5, bgcol));
		//		tmpJPanel.setBackground(bgcol);

		tmpJPanel.setPreferredSize(new Dimension(1, bottomHeight));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(new JPanel(), BorderLayout.CENTER);
		tmpJPanel.add(tmpBox, BorderLayout.LINE_END);
		return tmpJPanel;

	}

	/**
	 * Crée les boutons d'action. Par defaut, fait appel à buildDefaultButtonsPanel. Dérivez cette méthode pour personnaliser.
	 * 
	 * @return
	 */
	protected JPanel createActionButtonsUI() {
		return buildDefaultButtonsPanel();
	}

	/*
	 * private JPanel createActionButtonsUI() {
	 * 
	 * Box tmpBox = Box.createHorizontalBox(); tmpBox.add(Box.createRigidArea(new Dimension(2,bottomHeight))); tmpBox.add(Box.createHorizontalGlue());
	 * 
	 * prevAction = new AbstractAction("Précédent"){ public void actionPerformed(ActionEvent e){ onPrev(); } };
	 * prevAction.setEnabled(this.isPrevEnabled()); prevAction.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16)); //Bouton
	 * prev prevButton = new JButton(prevAction); // prevButton.setFont(this.getFont().deriveFont(this.getFont().getStyle() + Font.BOLD)); //
	 * prevButton.setText(this.buttonLabels[0]); prevButton.setPreferredSize(BUTTONS_DIMENSION); prevButton.setMinimumSize(BUTTONS_DIMENSION);
	 * 
	 * prevButton.setSize(prevButton.getPreferredSize()); if (isPrevVisible()) { tmpBox.add(prevButton); //separateur tmpBox.add(
	 * Box.createRigidArea(new Dimension(25,30)) ); }
	 * 
	 * nextAction = new AbstractAction("Suivant"){ public void actionPerformed(ActionEvent e){ onNext(); } };
	 * nextAction.setEnabled(this.isNextEnabled()); nextAction.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16)); //Bouton
	 * prev nextButton = new JButton(nextAction); // prevButton.setFont(this.getFont().deriveFont(this.getFont().getStyle() + Font.BOLD)); //
	 * prevButton.setText(this.buttonLabels[0]); nextButton.setMinimumSize(BUTTONS_DIMENSION); nextButton.setPreferredSize(BUTTONS_DIMENSION);
	 * 
	 * // prevButton.setPreferredSize(BUTTONS_DIMENSION); // prevButton.setMinimumSize(BUTTONS_DIMENSION);
	 * 
	 * 
	 * prevButton.setSize(prevButton.getPreferredSize()); if (isNextVisible()) { tmpBox.add(nextButton); //separateur tmpBox.add(
	 * Box.createRigidArea(new Dimension(25,30)) ); }
	 * 
	 * 
	 * 
	 * //Bouton Fermer closeAction = new AbstractAction("Fermer"){ public void actionPerformed(ActionEvent e){ onClose(); } };
	 * closeAction.setEnabled(this.isCloseEnabled()); closeAction.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
	 * //Bouton prev closeButton = new JButton(closeAction); // prevButton.setFont(this.getFont().deriveFont(this.getFont().getStyle() + Font.BOLD));
	 * // prevButton.setText(this.buttonLabels[0]); closeButton.setPreferredSize(BUTTONS_DIMENSION); closeButton.setMinimumSize(BUTTONS_DIMENSION);
	 * 
	 * prevButton.setSize(prevButton.getPreferredSize()); if (isCloseVisible()) { tmpBox.add(closeButton ); tmpBox.add( Box.createRigidArea(new
	 * Dimension(25,30)) ); }
	 * 
	 * // tmpBox.add(Box.createGlue());
	 * 
	 * 
	 * //On cale le bloc des boutons à droite JPanel tmpJPanel = new JPanel(); tmpJPanel.add(Box.createHorizontalGlue(), BorderLayout.CENTER);
	 * tmpJPanel.add(tmpBox, BorderLayout.LINE_END); return tmpJPanel; }
	 */

	/**
	 * Méthode à appeler pour contruire le panel.
	 */
	public void initGUI() {
		/*
		 * this.setOpaque(true); this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS)); Box tmpBoxHor = Box.createHorizontalBox(); //Permet de fixer la
		 * taille minimale en hauteur tmpBoxHor.add(Box.createRigidArea( new Dimension(1, getPanelDimension().height))); Box tmpBox =
		 * Box.createVerticalBox(); tmpBox.add(createCommentaireUI() ); tmpBox.add(new JSeparator());
		 * 
		 * 
		 * 
		 * 
		 * 
		 * //Permet de mettre un séparateur + fixer taille minimale en largeur tmpBox.add(Box.createRigidArea( new
		 * Dimension(getPanelDimension().width,5))); Box tmpBoxVert = Box.createHorizontalBox(); tmpBoxVert.add(getContentPanel()); //Fixer la hauteur
		 * du panel de contenu tmpBoxVert.add(Box.createRigidArea(new Dimension(1,getPanelDimension().height-72))); tmpBox.add(tmpBoxVert);
		 * tmpBox.add(Box.createRigidArea(new Dimension(getPanelDimension().width,5))); tmpBox.add( new JSeparator()); //Les boutons OK et annuler
		 * tmpBox.add(createActionButtonsUI() ); tmpBoxHor.add(tmpBox); this.add(tmpBoxHor);
		 */
		this.setOpaque(true);
		this.setLayout(new BorderLayout());

		Box tmpBox = Box.createVerticalBox();
		tmpBox.setPreferredSize(new Dimension(getPanelDimension().width, topHeight));
		tmpBox.setMinimumSize(tmpBox.getPreferredSize());
		//		tmpBox.setMaximumSize(tmpBox.getPreferredSize());
		tmpBox.add(createCommentaireUI());
		tmpBox.add(new JSeparator());

		Box tmpBoxBottom = Box.createVerticalBox();
		tmpBoxBottom.setPreferredSize(new Dimension(getPanelDimension().width, bottomHeight));
		tmpBoxBottom.setMinimumSize(tmpBoxBottom.getPreferredSize());
		tmpBoxBottom.add(new JSeparator());
		tmpBoxBottom.add(createActionButtonsUI());

		this.add(tmpBox, BorderLayout.PAGE_START);
		this.add(getContentPanel(), BorderLayout.CENTER);
		this.add(tmpBoxBottom, BorderLayout.PAGE_END);
		this.setPreferredSize(getPanelDimension());

	}

	public abstract interface ZKarukeraStepListener {
		public void onNext();

		public void onPrev();

		public void onClose();

		public Dimension getPanelDimension();

		public EOEditingContext getParentEditingContext();

		public AbstractAction specialAction1();
	}

	/**
	 * @return
	 */
	public Action getCloseAction() {
		return closeAction;
	}

	/**
	 * @return
	 */
	public Action getNextAction() {
		return nextAction;
	}

	/**
	 * @return
	 */
	public Action getPrevAction() {
		return prevAction;
	}

	/**
	 * @param action
	 */
	public void setCloseAction(Action action) {
		closeAction = action;
	}

	/**
	 * @param action
	 */
	public void setNextAction(Action action) {
		nextAction = action;
	}

	/**
	 * @param action
	 */
	public void setPrevAction(Action action) {
		prevAction = action;
	}
	/**
     * 
     */

}
