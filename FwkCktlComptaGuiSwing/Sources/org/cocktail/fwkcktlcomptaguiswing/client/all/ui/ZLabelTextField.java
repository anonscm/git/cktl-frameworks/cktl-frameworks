/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.Color;
import java.awt.Component;
import java.text.Format;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZLabelTextField extends ZLabelField {
	private JTextField myTexfield;

	protected IZLabelTextFieldModel myProvider;
	private Format myFormat;
	private ArrayList documentListeners;
	
	
	public ZLabelTextField(String label, IZLabelTextFieldModel aProvider) {
		super();
		initObject(label, 0, -1);
		setMyProvider(aProvider);
	}
	
	public ZLabelTextField(String label) {
		super();
		initObject(label, 0, -1);
	}	

	public final boolean isEmpty() {
	    return (getMyTexfield().getText()==null || getMyTexfield().getText().length()==0);
	}


	/**
	 * @return La valeur construite à partir du contenu du champ texte. Si la valeur saisie ne respecte pas le format spécifié, null est renvoyé.
	 */
	protected Object getInnerValue() {
		String txt = getMyTexfield().getText(); 
		if (txt==null) {
			return null;
		}
        if (getMyFormat()==null) {
        	return txt; 
        }
        try {
        	return getMyFormat().parseObject(txt);
        } catch (ParseException e) {
        	return null;
        }
	}
	
	/**
     * @see org.cocktail.zutil.client.ui.ZLabelField#initObject(java.lang.String, int, int)
     */
    protected void initObject(String label, int aLabelOrientation, int aPreferedWidth) {
	    documentListeners = new ArrayList();
		myTexfield = new JTextField();
		addDocumentListener(new InnerDocumentListener()) ;		
        super.initObject(label, aLabelOrientation, aPreferedWidth);
    }



		public void addDocumentListener(DocumentListener aDocumentListener) {
		    documentListeners.add(aDocumentListener);
		    getMyTexfield().getDocument().addDocumentListener(aDocumentListener);
		}
		
		public void removeDocumentListener(DocumentListener aDocumentListener) {
		    documentListeners.remove(aDocumentListener);
		    getMyTexfield().getDocument().removeDocumentListener(aDocumentListener);
		}
		
		
		
	
	
	/**
	 * Surchargez éventuellement cette méthode
	 * @return La valeur à afficher (par défaut toString(), sauf si un format est défini).
	 */
	protected String getValueToDisplay() {
		Object val = myProvider.getValue();
		if (val!=null) {
			if (myFormat!=null) {
				return myFormat.format(val);					
			}
            return val.toString();
		}
        return null;
				
	}

	protected void disableAllDocumentsListener() {
	    Iterator iter = documentListeners.iterator();
	    while (iter.hasNext()) {
            DocumentListener element = (DocumentListener) iter.next();
            getMyTexfield().getDocument().removeDocumentListener(element);
        }
	}
	
	protected void enableAllDocumentsListener() {
	    Iterator iter = documentListeners.iterator();
	    while (iter.hasNext()) {
            DocumentListener element = (DocumentListener) iter.next();
            getMyTexfield().getDocument().addDocumentListener(element);
        }
	}

	
	/**
	 * Remplace la chaine de caractère affichée par la valeur renvoyée par getValueToDisplay.
	 * Appeler cette methode pour mettre à jour les informations sans notification.  
	 */
	public final void updateData() {
	    //Virer les documentsListener
	    disableAllDocumentsListener();
		getMyTexfield().setText(getValueToDisplay());
		enableAllDocumentsListener();
	}

	/**
	 * Remplace la chaine de caractère affichée par la valeur renvoyée par getValueToDisplay.
	 * Appeler cette methode pour mettre à jour les informations avec notification.  
	 */
	public final void updateDataAndNotify() {
	    getMyTexfield().setText(getValueToDisplay());
	}
	

	/**
	 * @return
	 */
	public Format getMyFormat() {
		return myFormat;
	}

	/**
	 * @param format
	 */
	public void setMyFormat(Format format) {
		myFormat = format;
	}
	
	

	
	public JTextField getMyTexfield() {
		return myTexfield;
	}

	
	public interface IZLabelTextFieldModel {
		/**
		 * Doit renvoyer l'objet à afficher.
		 */
		public Object getValue();
		
		/**
		 * Doit traiter la mise à jour d'un objet.
		 */
		public void setValue(Object value);
	}
		


	
	protected void notifyDataChanged() {
		if (myProvider!=null) {
			myProvider.setValue(  getInnerValue() );	
		}
	}
	
	
	
	/**
	 * Pour répondre au saisies sur le champ texte.
	 */
	private class InnerDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			notifyDataChanged();
		}

		public void insertUpdate(DocumentEvent e) {
			notifyDataChanged();			
		}

		public void removeUpdate(DocumentEvent e) {
			notifyDataChanged();
		}
	}
	
	
	

	/**
	 * @return
	 */
	public IZLabelTextFieldModel getMyProvider() {
		return myProvider;
	}

	/**
	 * @param provider
	 */
	public void setMyProvider(IZLabelTextFieldModel provider) {
		myProvider = provider;
	}

    /**
     * @see org.cocktail.zutil.client.ui.ZLabelField#getContentComponent()
     */
    public Component getContentComponent() {
        return getMyTexfield();
    }

    /**
     * Supprime la bordure autour du textfield et met un fond blanc.
     */
    public void setUIReadOnly() {
		myTexfield.setBorder(BorderFactory.createEmptyBorder());
		myTexfield.setBackground(Color.WHITE);	        
    }
    
    
    
    
    
}
