/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.client.all.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZCommonDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;

/**
 * Classe abstraite dont doivent hériter les boites de dialoques de l'application.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZKarukeraDialog extends ZCommonDialog {
	protected IComptaGuiSwingApplication myApp;

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZKarukeraDialog(Dialog owner, String title, boolean modal)
			throws HeadlessException {
		super(owner, title, modal);
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZKarukeraDialog(Frame owner, String title, boolean modal)
			throws HeadlessException {
		super(owner, title, modal);
	}

	/**
	 * @param actions
	 * @return Une box contenant les boutons construits à partir des actions.
	 */
	public static Component buildHorizontalButtonsFromActions(List<Action> actions) {
		final JPanel bPanel = new JPanel(new BorderLayout());
		final JPanel box = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(actions));
		box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		bPanel.add(new JSeparator(), BorderLayout.NORTH);
		bPanel.add(box, BorderLayout.EAST);
		return bPanel;
	}

	/**
	 * Construit un panel de boutons à partir d'une liste d'actions.
	 *
	 * @param actions
	 * @return
	 */
	public static JPanel buildVerticalPanelOfButtonsFromActions(ArrayList actions) {
		JPanel p = new JPanel();
		Iterator iterator = actions.iterator();
		GridLayout thisLayout = new GridLayout(actions.size(), 1);
		thisLayout.setVgap(20);
		p.setLayout(thisLayout);
		while (iterator.hasNext()) {
			Action element = (Action) iterator.next();
			if (element != null) {
				JButton button = new JButton(element);
				button.setHorizontalAlignment(JButton.LEFT);
				p.add(button);
			}
			else {
				p.add(new JPanel(new BorderLayout()));
			}
		}
		return p;
	}

	public static JPanel buildVerticalPanelOfButtonsFromActions(Action[] actions) {
		JPanel p = new JPanel();
		GridLayout thisLayout = new GridLayout(actions.length, 1);
		thisLayout.setVgap(20);
		p.setLayout(thisLayout);
		for (int i = 0; i < actions.length; i++) {
			Action element = actions[i];
			JButton button = new JButton(element);
			button.setHorizontalAlignment(JButton.LEFT);
			p.add(button);
		}
		return p;
	}

	public final void showErrorDialog(Exception e) {
		myApp.showErrorDialog(e, this);
	}

	public final boolean showConfirmationDialog(String titre, String message, String defaultRep) {
		return myApp.showConfirmationDialog(titre, message, defaultRep, this);
	}

	public final void showInfoDialog(String text) {
		myApp.showInfoDialog(text, this);
	}

	public final void showinfoDialogFonctionNonImplementee() {
		myApp.showinfoDialogFonctionNonImplementee(this);
	}

	/**
	 * Permet de spécifier le composant qui doit recevoir le focus lors de l'ouverture d'une fenetre.<br>
	 * Usage : InitialFocusSetter.setInitialFocus(myWindow, myComponent);
	 */
	public static class InitialFocusSetter {
		public static void setInitialFocus(final Window w, final Component c) {
			w.addWindowListener(new FocusSetter(c));
		}

		public static class FocusSetter extends WindowAdapter {
			final Component initComp;

			FocusSetter(Component c) {
				initComp = c;
			}

			public void windowOpened(WindowEvent e) {
				initComp.requestFocusInWindow();
				e.getWindow().removeWindowListener(this);
			}
		}
	}
}
