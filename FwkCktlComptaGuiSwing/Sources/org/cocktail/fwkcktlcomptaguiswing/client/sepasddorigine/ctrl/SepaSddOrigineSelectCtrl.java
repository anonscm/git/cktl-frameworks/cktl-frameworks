package org.cocktail.fwkcktlcomptaguiswing.client.sepasddorigine.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.client.sepasdd.helpers.SepaSddOrigineClientHelper;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineFilters;
import org.cocktail.fwkcktlcompta.common.sepasdd.origines.CommonSepaSddOrigineEntityCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonSrchCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddorigine.ui.SepaSddOrigineSelectPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddorigine.ui.SepaSddOrigineSelectPanel.ISepaSddOrigineSelectPanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author rprin
 */
public class SepaSddOrigineSelectCtrl extends CommonSrchCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Recherche d'une origine pour l'échéancier du mandat SDD";
	private static final int NOMBRE_MAXIMUM_D_ORIGINES_RETOURNEES = 100;

	private SepaSddOrigineSelectPanel mainPanel;
	private SepaSddOrigineFilters filters;

	private MySepaSddOrigineSelectPanelListener sepaSddOrigineSelectPanelListener;
	private MySepaSddOrigineListPanelListener sepaSddOrigineListPanelListener;
	private Action actionOk;
	private Action actionCancel;
	private ZEOComboBoxModel typeOrigineModel;

	private ISepaSddOrigineEntity selectedSepaSddOrigineEntity = null;
	private final Map<String, ? extends CommonSepaSddOrigineEntityCtrl> originesCtrlMap;

	public SepaSddOrigineSelectCtrl(EOEditingContext editingContext, IGrhumPersonne personne) throws Exception {
		super(editingContext);

		this.filters = initOrigineFilters(personne);


		NSArray typeOrigines = SepaSddOrigineClientHelper.getSharedInstance().fetchOrigineTypesValides(getEditingContext());
		originesCtrlMap = SepaSddOrigineClientHelper.getSharedInstance().buildOriginesCtrl(typeOrigines);
		typeOrigineModel = new ZEOComboBoxModel(typeOrigines, EOSepaSddOrigineType.TYPE_NOM_KEY, null, null);

		actionOk = initActionOK();
		actionCancel = initActionCancel();

		sepaSddOrigineListPanelListener = new MySepaSddOrigineListPanelListener();
		sepaSddOrigineSelectPanelListener = new MySepaSddOrigineSelectPanelListener();
		mainPanel = new SepaSddOrigineSelectPanel(sepaSddOrigineSelectPanelListener);
	}

	@Override
	protected NSArray getData() throws Exception {
		//Appeler le fetch en fonction du type d'origine selectionne
		EOSepaSddOrigineType typeOrigine = (EOSepaSddOrigineType) typeOrigineModel.getSelectedEObject();
		try {
			if (typeOrigine != null) {
				CommonSepaSddOrigineEntityCtrl ctrl = originesCtrlMap.get(typeOrigine.typeNom());
				return ctrl.fetchSpec(getEditingContext(), filters, NOMBRE_MAXIMUM_D_ORIGINES_RETOURNEES);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

		return NSArray.EmptyArray;
	}

	@Override
	public String title() {
		return TITLE;
	}

	@Override
	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	@Override
	public ZComptaSwingPanel mainPanel() {
		return mainPanel;
	}

	private void onOkClick() {
		setWaitCursor(true);
		try {
			selectedSepaSddOrigineEntity = (ISepaSddOrigineEntity) mainPanel.getListPanel().selectedObject();
			getMyDialog().onOkClick();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
		
	}

	private void onCancelClick() {
		setWaitCursor(true);
		try {
			selectedSepaSddOrigineEntity = null;
			getMyDialog().onCancelClick();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private Action initActionOK() {
		Action actionOK = new AbstractAction("Ok") {
			public void actionPerformed(ActionEvent e) {
				onOkClick();
			}
		};
		actionOK.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));

		return actionOK;
	}

	private SepaSddOrigineFilters initOrigineFilters(IGrhumPersonne personne) {
		SepaSddOrigineFilters filters = new SepaSddOrigineFilters();
		filters.put(SepaSddOrigineFilters.DEBITEUR_PERSONNE_KEY, personne);
		return filters;
	}

	private Action initActionCancel() {
		Action actionCancel = new AbstractAction("Annuler") {
				public void actionPerformed(ActionEvent e) {
					onCancelClick();
				}
			};
		actionCancel.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));

		return actionCancel;
	}

	private final class MySepaSddOrigineSelectPanelListener implements ISepaSddOrigineSelectPanelListener {

		public SepaSddOrigineFilters getFilters() {
			return filters;
		}

		public Action actionSrch() {
			return actionSrch;
		}

		public Action actionAnnuler() {
			return actionCancel;
		}

		public Action actionOk() {
			return actionOk;
		}

		public IZKarukeraTablePanelListener getSepaSddOrigineListPanelListener() {
			return sepaSddOrigineListPanelListener;
		}

		public ComboBoxModel getTypeOrigineModel() {
			return typeOrigineModel;
		}

	}

	private final class MySepaSddOrigineListPanelListener implements IZKarukeraTablePanelListener {

		public void selectionChanged() {

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return SepaSddOrigineSelectCtrl.this.getData();
		}

		public void onDbClick() {
			onOkClick();
		}

	}

	public ISepaSddOrigineEntity getSelectedSepaSddOrigineEntity() {
		return selectedSepaSddOrigineEntity;
	}

	@Override
	protected NSMutableArray buildFilterQualifiers() throws Exception {
		return null;
	}

	public EOSepaSddOrigineType getSelectedSepaSddOrigineType() {
		return (EOSepaSddOrigineType) typeOrigineModel.getSelectedEObject();
	}

	public void onSrch() {
		setWaitCursor(true);
		try {
			mainPanel.getListPanel().updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}
}
