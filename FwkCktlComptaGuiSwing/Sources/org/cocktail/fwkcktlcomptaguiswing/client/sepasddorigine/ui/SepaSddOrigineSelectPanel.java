package org.cocktail.fwkcktlcomptaguiswing.client.sepasddorigine.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.text.Format;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineFilters;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZNumberField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;

public class SepaSddOrigineSelectPanel extends ZComptaSwingPanel {

	private static final long serialVersionUID = 1L;
	private ISepaSddOrigineSelectPanelListener myListener;

	private SepaSddOrigineListPanel listPanel;

	private ZFormPanel filterTypeOrigine;
	private ZFormPanel filterExercice;
	private ZFormPanel filterNumero;
	private ZFormPanel filterObjet;
	private ZFormPanel filterMontant;

	public SepaSddOrigineSelectPanel(ISepaSddOrigineSelectPanelListener myListener) {
		super();
		this.myListener = myListener;
		listPanel = new SepaSddOrigineListPanel(myListener.getSepaSddOrigineListPanelListener());

		listPanel.initGUI();

		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		add(ZComptaSwingPanel.encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, buildTopPanel(), null, null), BorderLayout.NORTH);
		add(ZComptaSwingPanel.encloseInPanelWithTitle("Liste des origines possibles", null, ZConst.BG_COLOR_TITLE, listPanel, null, null), BorderLayout.CENTER);
		add(ZComptaSwingPanel.encloseInPanelWithTitle("", null, ZConst.BG_COLOR_TITLE, buildBottomPanel(), null, null), BorderLayout.SOUTH);

	}

	private final JPanel buildTopPanel() {
		filterTypeOrigine = ZFormPanel.buildLabelField("Type origine", new JComboBox(myListener.getTypeOrigineModel()));
		filterExercice = ZFormPanel.buildLabelField("Exercice", new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.getFilters(), SepaSddOrigineFilters.EXERCICE_KEY), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT));

		filterNumero = ZFormPanel.buildFourchetteNumberFields("<= Numéro <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), SepaSddOrigineFilters.NUMERO_MIN), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), SepaSddOrigineFilters.NUMERO_MAX), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER);
		filterObjet = ZFormPanel.buildLabelField("Objet", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), SepaSddOrigineFilters.OBJET_KEY));
		filterMontant = ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), SepaSddOrigineFilters.MONTANT_MIN), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), SepaSddOrigineFilters.MONTANT_MAX), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_DECIMAL);

		filterExercice.setDefaultAction(myListener.actionSrch());
		filterNumero.setDefaultAction(myListener.actionSrch());
		filterObjet.setDefaultAction(myListener.actionSrch());
		filterMontant.setDefaultAction(myListener.actionSrch());
		
		((ZTextField) filterExercice.getMyFields().get(0)).getMyTexfield().setColumns(4);
		((ZTextField) filterNumero.getMyFields().get(0)).getMyTexfield().setColumns(4);
		((ZTextField) filterObjet.getMyFields().get(0)).getMyTexfield().setColumns(12);
		((ZTextField) filterMontant.getMyFields().get(0)).getMyTexfield().setColumns(4);

		JPanel p = new JPanel(new BorderLayout());
		final Component separator = Box.createRigidArea(new Dimension(4, 1));
		final List<Component> comps = new ArrayList<Component>();

		comps.add(ZUiUtil.buildBoxLine(new Component[] {
				filterTypeOrigine
		}));
		
		comps.add(ZUiUtil.buildBoxLine(new Component[] {
				filterExercice, separator, filterNumero, separator, filterObjet, filterMontant
		}));

		p.add(ZUiUtil.buildBoxLine(new Component[] {
				ZUiUtil.buildBoxColumn(comps)
		}), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		p.add(new JButton(myListener.actionSrch()), BorderLayout.EAST);
		return p;
	}

	private final JPanel buildBottomPanel() {
		JButton btOk = new JButton(myListener.actionOk());
		JButton btCancel = new JButton(myListener.actionAnnuler());
		Dimension btSize = new Dimension(95, 24);
		btOk.setMinimumSize(btSize);
		btCancel.setMinimumSize(btSize);
		btOk.setPreferredSize(btSize);
		btCancel.setPreferredSize(btSize);

		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		p.add(btOk);
		p.add(btCancel);
		return p;
	}

	@Override
	public void updateData() throws Exception {
		filterExercice.updateData();
		filterNumero.updateData();
		filterObjet.updateData();
		filterMontant.updateData();
		listPanel.updateData();
	}

	public interface ISepaSddOrigineSelectPanelListener {
		IZKarukeraTablePanelListener getSepaSddOrigineListPanelListener();
		ComboBoxModel getTypeOrigineModel();
		SepaSddOrigineFilters getFilters();

		Action actionSrch();
		Action actionAnnuler();
		Action actionOk();
	}

	public SepaSddOrigineListPanel getListPanel() {
		return listPanel;
	}

	@Override
	public void initGUI() {
		// obsolete
	}

}
