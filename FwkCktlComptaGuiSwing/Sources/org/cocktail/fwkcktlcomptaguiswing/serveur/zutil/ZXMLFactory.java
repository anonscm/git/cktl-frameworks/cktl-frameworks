/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil;


import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;

/**
 * Offre des mï¿½thodes pour serialiser des objets en XML.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZXMLFactory {


 	private static final String CONSTANT_1 = "\n"; //$NON-NLS-1$




    /**
 	 * Effectue une requete SQL et ajoute le rï¿½sultat au flux XML. Toutes les balises seront en minuscules.
 	 *
	 * @param pCktlXMLWriter
	 * @param eoContext Editing Context
	 * @param modelName Obligatoire.
	 * @param sqlQuery Requete SQL
	 * @param tagName Nom de la balise ï¿½ utiliser pour chaque enregistrement rï¿½cupï¿½rï¿½.
	 * @return le nombre d'enregistrements rï¿½cupï¿½rï¿½s par la requete sql.
	 */
	public static int addResultFromSQLQuery(CktlXMLWriter pCktlXMLWriter, EOEditingContext eoContext, final String modelName, final String sqlQuery, final String tagName) throws Exception {
		NSArray res = EOUtilities.rawRowsForSQL( eoContext, modelName,sqlQuery, null);
		addAfterSerialize(pCktlXMLWriter, res, tagName, false);
		if (res==null) {
		    return 0;
		}
		return res.count();
	}


	public static int addResultFromSQLQuery(CktlXMLWriter pCktlXMLWriter, EOEditingContext eoContext, final String modelName, final String sqlQuery, final String tagName, final boolean flat, final boolean escapeCarriageReturn) throws Exception {
		NSArray res = EOUtilities.rawRowsForSQL( eoContext, modelName,sqlQuery, null);
        if (Thread.currentThread().isInterrupted()) {
            return -1;
        }
		addAfterSerialize(pCktlXMLWriter, res, tagName, flat, escapeCarriageReturn);
		if (res==null) {
		    return 0;
		}
		return res.count();
	}



	/**
	 * Serialize l'objet passï¿½ en parametre et l'ajoute au flux XML.
	 *
	 * @param pCktlXMLWriter
	 * @param tagName Nom de la balise ï¿½ utiliser pour chaque enregistrement rï¿½cupï¿½rï¿½.
	 */
	public static void addAfterSerialize(CktlXMLWriter pCktlXMLWriter, Object objectToSerialize, final String tagName, final boolean flat) throws Exception {
	    addAfterSerialize(pCktlXMLWriter, objectToSerialize, tagName, flat, false);
	}
	public static void addAfterSerialize(CktlXMLWriter pCktlXMLWriter, Object objectToSerialize, final String tagName, final boolean flat, final boolean escapeCarriageReturn) throws Exception {
	    ZXMLSerialize tmpZXMLSerialize;
	    if (flat){
			tmpZXMLSerialize= new ZXMLSerializeFlat(pCktlXMLWriter  );
	    }
	    else {
			tmpZXMLSerialize= new ZXMLSerialize(pCktlXMLWriter, null  );
	    }

	    if (!escapeCarriageReturn) {
	        tmpZXMLSerialize.getEscapeChars().remove(CONSTANT_1);
	    }
	    tmpZXMLSerialize.objectToXml( objectToSerialize, tagName.toLowerCase());
	}




	/**
	 * Effectue un fetch dans l'eoeditingcontext et serialize le resultat.
	 *
	 * @param pCktlXMLWriter
	 * @param ec
	 * @param entityName
	 * @param qual
	 * @param sortOrderings
	 * @param relationsToExport Les chemins des relations ï¿½ exporter.
	 * @throws Exception
	 */
	public static void addAfterFetch(CktlXMLWriter pCktlXMLWriter, EOEditingContext ec, final String entityName, final EOQualifier qual, final NSArray sortOrderings, NSArray relationsToExport) throws Exception {
		EOFetchSpecification spec = new EOFetchSpecification(entityName,qual,sortOrderings,true,true,null);
		spec.setRefreshesRefetchedObjects(true);
		NSArray res = ec.objectsWithFetchSpecification(spec);

		ZXMLSerialize tmpZXMLSerialize= new ZXMLSerialize(pCktlXMLWriter, relationsToExport  );
		tmpZXMLSerialize.objectToXml( res, entityName.toLowerCase());

	}














}
