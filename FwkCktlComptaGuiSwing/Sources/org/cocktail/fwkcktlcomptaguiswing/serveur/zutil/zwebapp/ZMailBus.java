/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.zwebapp;

import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import com.webobjects.foundation.NSData;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZMailBus extends CktlMailBus {
	private CktlConfig config;

	/**
	 * @param arg0
	 */
	public ZMailBus(CktlConfig arg0) {
		super(arg0);
		config = arg0;
	}

	public boolean sendMail(String from, String to, String cc, String subject,
			String msgText, String[] filenames, NSData[] filedatas) {
		boolean success = false;
		byte[][] fData = new byte[filedatas.length][];
		String[] fNames = new String[filedatas.length];
		// System.out.println("Envoi de mail TO: "+to);
		// System.out.println("Envoi de mail CC: "+cc);
		// to=new String("astockus@univ-lr.fr");
		// cc = null;
		try {
			if (filenames.length != filedatas.length) {
				System.out.println("Le nombre de fichiers ne correspond pas au nombre des noms de fichiers.");
			}

			CktlMailMessage mailBus = new CktlMailMessage(config.stringForKey("GRHUM_HOST_MAIL"));

			for (int i = 0; i < fNames.length; i++) {
				NSData file = (NSData) filedatas[i];
				fData[i] = file.bytes(0, file.length());
				fNames[i] = StringCtrl.normalize((i < filenames.length ? filenames[i] : "fichier_" + i));
			}
			if (filenames.length > 0) {
				mailBus.initMessage(from, to, subject, msgText, fNames, fData);
			}
			else {
				mailBus.initMessage(from, to, subject, msgText);
			}
			mailBus.addCCs(CktlMailMessage.toArray(cc));
			mailBus.send();
			success = true;
		} catch (Throwable mex) {
			mex.printStackTrace();
		}
		return success;
	}

}
