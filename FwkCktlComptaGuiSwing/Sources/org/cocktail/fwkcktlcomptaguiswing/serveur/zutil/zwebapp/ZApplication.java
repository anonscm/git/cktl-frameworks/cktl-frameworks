/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.zwebapp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.ZStringUtil;
import org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.logging.ZBufferedLogger;
import org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.logging.ZLogger;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlLog.ICktlLogWriter;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl;

import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.jdbcadaptor.JDBCContext;

/**
 * Rassemble des methodes pour unifier le comportement des classes Applications. Necessite le frmk CRIWebApp3 ou +
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org - rodolphe.prin at cocktail.org
 */
public abstract class ZApplication extends CktlWebApplication {
	public NSNumberFormatter appMonnaieFormatter;
	public NSNumberFormatter app2DecimalesFormatter;
	public NSMutableDictionary appParametres;
	private NSMutableDictionary appParametresConfig;

	protected final static String CONST_SAUT_URL = "SAUT_URL";
	protected final static String CONST_TMP_LOG_DIR = "TMP_LOG_DIR";

	/**
	 * Refererence au sharedEditingContext de l'application.
	 */
	public EOSharedEditingContext mySharedEditingContext;
	public boolean isInitialisationErreur;
	private boolean testMode;
	private String emailForTest;

	private MyFileOutputStream logOutputStreamOut;
	private MyFileOutputStream logOutputStreamErr;
	private File serverLogFile;

	/**
	 * Reference a l'objet qui gere l'authentification SAUT des clients. Il faut faire appel a initSautClient pour l'initialiser.
	 */
	protected SAUTClient sautClient;

	/**
	 * Doit renvoyer une chaine de caracteres identifiant la version.
	 * 
	 * @return
	 */
	public abstract String versionTxt();

	protected abstract boolean showSqlLogs();

	/**
	 * Methode qui sera appelee par initApplication. Utiliser les exceptions pour renvoyer des erreurs. Les erreurs apparaitront dans le log
	 * d'initialisation. Utiliser myLog.append(String) pour ecrire des informations dans le log.
	 * 
	 * @throws Exception
	 */
	protected abstract void initApplicationSpecial();

	private ZMailBus zMailBus;
	private PrintStream prinStreamOut;
	private PrintStream prinStreamErr;

	/**
	 * Initialise l'application.
	 * 
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#initApplication()
	 */
	public final void initApplication() {

		try {
			initLogs();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erreur initialisation fichier log : " + e.getMessage());
		}

		System.out.println(serverLogFile.getAbsolutePath());
		System.out.println(new Date());
		System.out.println("Lancement de l'application serveur " + this.name() + "...");

		isInitialisationErreur = false;
		appParametresConfig = new NSMutableDictionary();

		//
		ZBufferedLogger.logBigTitle("INITIALISATION", ZLogger.LVL_NONE);
		afficherVersion();

		config().takeValueForKey(config().valueForKey("DEFAULT_NS_TIMEZONE"), "DEFAULT_TIME_ZONE");
		super.initApplication();

		zMailBus = new ZMailBus(config());

		if (showSqlLogs()) {
			ZBufferedLogger.warning("Affichage des logs SQL : actif");
			//			NSLog.setAllowedDebugLevel(NSLog.DebugLevelCritical)
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupSQLGeneration);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupDatabaseAccess);
			//			NSLog.allowDebugLoggingForGroups(-1);
		}
		else {
			ZBufferedLogger.warning("Affichage des logs SQL : desactive");
		}

		//		rawLogVersionInfos();
		rawLogModelInfos();
		isInitialisationErreur = !checkModel();

		initTimeZones();
		initFormatters();
		initApplicationSpecial();

		//		EOModel mdl = 
		//		
		//		dicoBdConnexionServerName.takeValueForKey( EOModelCtrl.bdConnexionServerName(tmpEOModel) , tmpEOModel.name());
		//		dicoBdConnexionServerId.takeValueForKey( bdConnexionServerId(tmpEOModel) , tmpEOModel.name());

		if (isInitialisationErreur) {
			ZBufferedLogger.error("");
			ZBufferedLogger.error("IL Y A EU DES ERREURS D'INITIALISATION :-(");
			ZBufferedLogger.error("");
		}

		testMode = (config().valueForKey("ZTEST_MODE") != null && "1".equals(((String) config().valueForKey("ZTEST_MODE")).trim()));
		if (testMode) {
			ZBufferedLogger.logTitle("Mode TEST actif", ZLogger.LVL_INFO);
			ZBufferedLogger.info("Verifiez que vous etes sur une base de donnees de test");
			emailForTest = config().valueForKey("ZEMAIL_FOR_TEST").toString();
			emailForTest = StringCtrl.normalize(emailForTest);
			if (ZStringUtil.isEmailValid(emailForTest)) {
				ZBufferedLogger.info("Tous les mails generes par l'application seront envoyes a l'adresse \"" + emailForTest + "\" .");
			}
			else {
				ZBufferedLogger.warning("L'adresse email specifiee pour les tests (parametre ZEMAIL_FOR_TEST) n'est pas une adresse email valide = \"" + emailForTest + "\".  " +
						"L'envoi des mail risque de provoquer des erreurs ");
				isInitialisationErreur = true;
			}
		}

		if (ZStringUtil.isEmailValid(this.contactMail())) {
			if ((config().valueForKey("SEND_MAIL_TO_ADMIN_AT_STARTUP") != null) && ("1".equals(config().valueForKey("SEND_MAIL_TO_ADMIN_AT_STARTUP")))) {
				String lebod = "L'application " + this.name() + " a été lancée à " + (new NSTimestamp()).toString();
				lebod = lebod + "\nsur " + webserverConnectURL();
				if (isInitialisationErreur) {
					lebod = lebod + "\n\n" + "Il y a eu des erreurs lors de l'initialisation, reportez-vous au debut du fichier de log pour plus d'informations.\n\n";
				}
				if (isTestMode()) {
					lebod = lebod + "\n\nL'application est en mode TEST";
				}
				else {
					lebod = lebod + "\n\nL'application n'est pas en mode TEST";
				}

				lebod = lebod + "\n\n Log de l'initialisation";
				lebod = lebod + "\n------------------------------ \n";
				lebod = lebod + ZBufferedLogger.getLog();

				String subj;
				subj = "Lancement de l'application " + this.name() + " sur le serveur " + host();
				if (isInitialisationErreur) {
					subj = subj + " : ERREURS";
				}
				else {
					subj = subj + " : OK";
				}

				if (!sendMailToAdmin(subj, lebod)) {
					ZBufferedLogger.warning("L'envoi du mail de notification de demarrage a l'administrateur a echoue. ");
				}
			}
			else {
				ZBufferedLogger.info("Pas d'envoi de mail de notification de demarrage a l'administrateur");
			}
		}
		else {
			ZBufferedLogger.warning("L'adresse email specifiee pour l'administrateur de l'application (parametre ADMIN_MAIL) n'est pas une adresse email valide = \"" + this.contactMail() + "\"  ");
			isInitialisationErreur = true;
		}
		ZBufferedLogger.logBigTitle("INITIALISATION TERMINEE", ZLogger.LVL_INFO);

		//	appLog.trace(""+isInitialisationErreur);
	}

	/**
	 * Vï¿½rifie si la connexion ï¿½ la base de donnï¿½es est active.
	 */
	public void checkBdConnection() {
		if (CktlDataBus.isDatabaseConnected()) {
			ZBufferedLogger.logSuccess("La connexion a la base de donnees est active", ZLogger.LVL_INFO);
		}
		else {
			ZBufferedLogger.logFailed("La connexion a la base de donnees n'est pas active !!", ZLogger.LVL_INFO);
			isInitialisationErreur = true;
		}
	}

	/**
	 * Initialise les formatters ï¿½ utiliser.
	 * 
	 * @see appMonnaieFormatter
	 * @see app2DecimalesFormatter
	 */
	public void initFormatters() {
		//Crï¿½er le numberFormatter
		this.appMonnaieFormatter = new NSNumberFormatter();
		this.appMonnaieFormatter.setDecimalSeparator(",");
		// this.appMonnaieFormatter.setThousandSeparator("\u0020");
		this.appMonnaieFormatter.setThousandSeparator(" ");

		this.appMonnaieFormatter.setHasThousandSeparators(true);
		this.appMonnaieFormatter.setPattern("#,##0.00;0,00;-#,##0.00");

		this.app2DecimalesFormatter = new NSNumberFormatter();
		this.app2DecimalesFormatter.setDecimalSeparator(",");
		// this.appMonnaieFormatter.setThousandSeparator("\u0020");
		this.app2DecimalesFormatter.setThousandSeparator(" ");

		this.app2DecimalesFormatter.setHasThousandSeparators(true);
		this.app2DecimalesFormatter.setPattern("#,##0.00;0,00;-#,##0.00");
	}

	public CktlDataBus getDbManager() {
		return this.dataBus();
	}

	public NSNumberFormatter getAppMonnaieFormatter() {
		return this.appMonnaieFormatter;
	}

	public NSNumberFormatter app2DecimalesFormatter() {
		return this.app2DecimalesFormatter;
	}

	//
	//	/**
	//	 * Renvoie l'url de connexion ï¿½ la base de donnï¿½es.
	//	 *
	//	 * @return
	//	 */
	//	public String bdConnexionName(EOModel model) {
	//		return bdConnexionUrl(model);
	//	}
	//
	//	/**
	//	 * Renvoie l'url de connexion ï¿½ la base de donnï¿½es pour le modele spï¿½cifiï¿½ en parametre.
	//	 *
	//	 * @param model
	//	 * @return
	//	 */
	//	public String bdConnexionUrl(EOModel model) {
	//		NSDictionary vDico = model.connectionDictionary();
	//		String url = (String)vDico.valueForKey("URL");
	//
	//		if (url==null) {
	//		    System.out.println();
	//		    System.out.println("ATTENTION : URL de connexion a la base de donnees nulle!!!!");
	//		    System.out.println();
	//		    url = "n/a";
	//		}
	//		return url;
	//	}

	//
	//	/**
	//	 * Renvoie le user base de donnï¿½es
	//	 * @return
	//	 */
	//	public String bdConnexionUser(EOModel model) {
	//		NSDictionary vDico = model.connectionDictionary();
	//		return (String)vDico.valueForKey("username");
	//	}
	//
	//	/**
	//	 * Renvoie le serverid de la base de donnï¿½es (par exemple gest).
	//	 * @return
	//	 */
	//	public String bdConnexionServerId(EOModel model) {
	//		String url = bdConnexionUrl(model);
	//		String serverUrl=null;
	//		String serverName=null;
	//		String serverBdName=null;
	////		appLog.trace("URL", url);
	//		//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
	//		//On sï¿½pare la partie jdbc de la partie server
	//
	//		String[] parts = url.split("@");
	//		if ( parts.length > 1 ) {
	//			serverUrl = parts[1];
	//			parts = serverUrl.split(":");
	//			if (parts.length>0) {
	//				serverName = parts[0];
	//				if (parts.length>1) {
	//					serverBdName = parts[parts.length - 1];
	//				}
	//			}
	//		}
	//		return serverBdName;
	//	}
	//
	//	/**
	//	 * Renvoie le serverid de la base de donnï¿½es (par exemple jane).
	//	 * @return
	//	 */
	//	public String bdConnexionServerName(EOModel model) {
	//		String url = bdConnexionUrl(model);
	//		String serverUrl=null;
	//		String serverName=null;
	//		String serverBdName=null;
	////		appLog.trace("URL", url);
	//		//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
	//		//On sï¿½pare la partie jdbc de la partie server
	//		String[] parts = url.split("@");
	//		if ( parts.length > 1 ) {
	//			serverUrl = parts[1];
	//			parts = serverUrl.split(":");
	//			if (parts.length>0) {
	//				serverName = parts[0];
	//				if (parts.length>1) {
	//					serverBdName = parts[parts.length - 1];
	//				}
	//			}
	//		}
	//		return serverName;
	//	}
	//

	/**
	 * Verifie si tous les parametres necessaires a l'application sont bien presents et initialises. La liste des parametres doit etre fournie par la
	 * methode requiredParams.
	 * 
	 * @see requiredParams
	 */
	//	protected boolean checkRequiredParams() {
	//		boolean missingParams=false;
	//        ZBufferedLogger.logTitle("Verification des parametres GRHUM / .config ", ZLogger.LVL_INFO);
	//		for (int i=0;i<requiredParams().count();i++) {
	//			if ( (config().valueForKey( (String)requiredParams().objectAtIndex(i) )==null   ) ||  StringCtrl.isEmpty( config().valueForKey( (String)requiredParams().objectAtIndex(i) ).toString()  )  ) {
	//                ZBufferedLogger.logFailed("Le parametre " + (String)requiredParams().objectAtIndex(i)+" est absent ou vide. Initialisez-le dans la table GRHUMPARAMETRES ou bien dans le fichier "+this.name()+".config.", ZLogger.LVL_WARNING);
	//				missingParams=true;
	//			}
	//			else {
	//				//On memorise dans le parametre dans le cache
	//				appParametresConfig.takeValueForKey(config().valueForKey( (String)requiredParams().objectAtIndex(i) ), (String)requiredParams().objectAtIndex(i));
	//                ZBufferedLogger.logKeyValue((String)requiredParams().objectAtIndex(i),config().valueForKey( (String)requiredParams().objectAtIndex(i)).toString(), ZLogger.LVL_WARNING );
	//			}
	//		}
	//        ZBufferedLogger.info("");
	//		return !missingParams;
	//	}

	/**
	 * Affiche les proprietes systeme java
	 */
	public void displaySystemProperties() {
		if ((config().stringForKey("DISPLAY_SYSTEM_PROPERTIES") != null) && (("1".equals(config().stringForKey("DISPLAY_SYSTEM_PROPERTIES"))))) {
			ZBufferedLogger.logTitle("Proprietes systeme", ZLogger.LVL_INFO);
			ZBufferedLogger.info(System.getProperties());
		}
	}

	/**
	 * Initialise le TimeZone a utiliser pour l'application.
	 */
	protected void initTimeZones() {
		//appLog().trace(new NSArray(TimeZone.getAvailableIDs()));
		ZBufferedLogger.logTitle("Initialisation du NSTimeZone", ZLogger.LVL_INFO);
		ZBufferedLogger.logKeyValue("NSTimeZone par defaut recupere sur le systeme (avant initialisation)", NSTimeZone.defaultTimeZone(), ZLogger.LVL_INFO);
		String tz = config().valueForKey("DEFAULT_NS_TIMEZONE").toString();

		//		config().stringForKey("DEFAULT_TIME_ZONE")
		if (ZStringUtil.isEmpty(tz)) {
			ZBufferedLogger.logFailed("Le parametre DEFAULT_NS_TIMEZONE n'est pas defini dans le fichier .config.", ZLogger.LVL_WARNING);
			TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
			NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
		}
		else {
			NSTimeZone ntz = NSTimeZone.timeZoneWithName(tz, false);
			if (ntz == null) {
				ZBufferedLogger.logFailed("Le parametre DEFAULT_NS_TIMEZONE defini dans le fichier .config n'est pas valide (" + tz + ")", ZLogger.LVL_WARNING);
				TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
				NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
			}
			else {
				TimeZone.setDefault(ntz);
				NSTimeZone.setDefaultTimeZone(ntz);
			}
		}
		ZBufferedLogger.logKeyValue("NSTimeZone par defaut utilise dans l'application", NSTimeZone.defaultTimeZone(), ZLogger.LVL_INFO);
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		ZBufferedLogger.logKeyValue("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone", ntf.defaultParseTimeZone(), ZLogger.LVL_INFO);
		ZBufferedLogger.logKeyValue("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone", ntf.defaultFormatTimeZone(), ZLogger.LVL_INFO);
		ZBufferedLogger.info("");
	}

	/**
	 * Indique si on est en mode test. Ce mode permet de rediriger tous les emails vers une seule adresse. Initilisï¿½ dans le fichier de config.
	 * 
	 * @return
	 */
	public boolean isTestMode() {
		return testMode;
	}

	/**
	 * Mappe la methode CktlMailBus#sendmail, en verifiant si on est en mode de test. Si tel est le cas, le mail n'est pas envoye aux destinataires
	 * specifies par mailto et mailcc, mais a l'adresse specifiee dans le fichier de config pour les tests.
	 * 
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public boolean sendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) {
		if (isTestMode()) {
			String preamble = "Vous recevez ce message car l'adresse \"" + config().valueForKey("ZEMAIL_FOR_TEST") + "\" est l'adresse de test specifiee dans le fichier de configuration de l'application " + this.name() + ".\n\n";
			preamble = preamble + "Normalement le message devrait etre adresse a :\n";
			preamble = preamble + "To : " + mailTo + "\n";
			preamble = preamble + "Cc : " + mailCC + "\n";
			preamble = preamble + "------- Message original -------\n";
			mailBody = preamble + mailBody;

			mailTo = getEmailRedirectionForTest();
			mailCC = "";
		}
		mailBody = mailBody + "\n\n" + "---------------------------------------------------\n"
				+ "Ce message a été généré automatiquement par l'application " + this.name();
		return mailBus().sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody);
	}

	public boolean sendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, String[] names, NSData[] files) {
		if (isTestMode()) {
			String preamble = "Vous recevez ce message car l'adresse \"" + config().valueForKey("ZEMAIL_FOR_TEST") + "\" est l'adresse de test spécifiée dans le fichier de configuration de l'application " + this.name() + ".\n\n";
			preamble = preamble + "Normalement le message devrait être adressé à :\n";
			preamble = preamble + "To : " + mailTo + "\n";
			preamble = preamble + "Cc : " + mailCC + "\n";
			preamble = preamble + "------- Message original -------\n";
			mailBody = preamble + mailBody;

			mailTo = getEmailRedirectionForTest();
			mailCC = "";
		}
		mailBody = mailBody + "\n\n" + "---------------------------------------------------\n"
				+ "Ce message a été généré automatiquement par l'application " + this.name();
		return zMailBus.sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody, names, files);
	}

	/**
	 * Renvoie l'adresse email a utiliser pour rediriger tous les mails dans le mode TEST.
	 * 
	 * @return
	 */
	public String getEmailRedirectionForTest() {
		return config().valueForKey("ZEMAIL_FOR_TEST").toString();
	}

	/**
	 * Envoi un email ï¿½ l'administrateur de l'application.
	 * 
	 * @param mailSubject
	 * @param mailBody
	 * @return
	 */
	public boolean sendMailToAdmin(String mailSubject, String mailBody) {
		String preamble = "Vous recevez ce message car l'adresse \"" + this.contactMail() +
				"\" est enregistrée comme adresse d'administrateur dans le fichier de configuration de l'application " + this.name() + ".";
		preamble = preamble + "\n" + "---------------------------------------------------\n\n";
		mailBody = preamble + mailBody;
		mailBody = mailBody + "\n\n" + "---------------------------------------------------\n"
				+ "Ce message a été généré automatiquement par l'application " + this.name();
		return mailBus().sendMail(this.contactMail(), this.contactMail(), null, mailSubject, mailBody);
	}

	public String hrefContactMail() {
		return "mailto:" + contactMail();
	}

	/**
	 * Renvoie une chaine avec la date et l'heure locale (normalement).
	 * 
	 * @return
	 */
	public String currentLocalIsoDateTime() {
		return (new NSTimestampFormatter("%Y-%m-%d %H:%M:%S")).format(new NSTimestamp());
	}

	/**
	 * Renvoie la date et l'heure GMT.
	 * 
	 * @return
	 */
	public String currentGMTIsoDateTime() {
		return (new NSTimestamp()).toString();
	}

	/**
	 * Renvoie les infos de connexion de la base de donnï¿½es (pour l'affichage). Tient compte du paramï¿½tre SHOWBDCONNEXIONSERVER et du paramï¿½tre
	 * SHOWBDCONNEXIONSERVERID.
	 * 
	 * @return
	 */
	public String showBdConnexionInfo() {
		String info = "";
		if ((config().valueForKey("SHOWBDCONNEXIONSERVER") != null) && ("1".equals(config().valueForKey("SHOWBDCONNEXIONSERVER")))) {
			//			info = (String)dicoBdConnexionServerName.valueForKey( mainModelName() );
			info = EOModelCtrl.bdConnexionServerName(((EOModel) EOModelCtrl.getModelsDico().objectForKey(mainModelName())));
		}
		if (info.length() > 0) {
			info = "@" + info;
		}
		if ((config().valueForKey("SHOWBDCONNEXIONSERVERID") != null) && ("1".equals(config().valueForKey("SHOWBDCONNEXIONSERVERID")))) {
			//			info = (String)dicoBdConnexionServerId.valueForKey( mainModelName() ) + info;
			info = EOModelCtrl.bdConnexionServerId(((EOModel) EOModelCtrl.getModelsDico().objectForKey(mainModelName()))) + info;
		}

		if (info.length() == 0) {
			info = "Information masquee";
		}
		return info;
	}

	/**
	 * Renvoie la version du runtime Java utilisï¿½.
	 * 
	 * @return
	 */
	public String getJREVersion() {
		return System.getProperty("java.version");
	}

	public void afficherVersion() {
		ZBufferedLogger.info(versionTxt());
	}

	/**
	 * Renvoie l'url racine pour pour les fichiers Javascript (valeur du parametre GRHUM HTML_JSCRIPT_ROOT).
	 * 
	 * @return
	 */
	public String getHtmlJscriptRoot() {
		return config().stringForKey("HTML_JSCRIPT_ROOT");
	}

	/**
	 * Renvoie l'url racine pour pour les fichiers Image (valeur du parametre GRHUM HTML_IMAGES_ROOT).
	 * 
	 * @return
	 */
	public String getHtmlImagesRoot() {
		return config().stringForKey("HTML_IMAGES_ROOT");
	}

	/**
	 * Renvoie le lien html pour le script overlib_mini.js.
	 * 
	 * @return
	 */
	public String getJsOverlibSrc() {
		return "<script TYPE=\"text/javascript\" language=\"javascript\" src=\"" + getHtmlJscriptRoot() + "/overlib_mini.js" + "\"></script>";
	}

	/**
	 * Initialise le client SAUT et met un message dans le log.
	 */
	protected void initSautClient() {
		try {
			sautClient = new SAUTClient(config().stringForKey(CONST_SAUT_URL));
		} catch (RuntimeException e) {
			ZBufferedLogger.logFailed("L'initialisation du SAUTClient a echoue.", ZLogger.LVL_WARNING);
			sautClient = null;
		}
	}

	/**
	 * Renvoie l'alias de l'application dï¿½fini dans le fichier de config.
	 * 
	 * @return
	 */
	public String getAppAlias() {
		return config().stringForKey("APP_ALIAS");
	}

	/**
	 * @return Un parametre defini dans le fichier de config ou dans GRHUM.PARAMETRE
	 */
	public NSMutableDictionary getAppParametresConfig() {
		return appParametresConfig;
	}

	public EODatabaseContext getDatabaseContext() {
		return CktlDataBus.databaseContext();
	}

	public EOAdaptorChannel getAdaptorChannel() {
		return getDatabaseContext().availableChannel().adaptorChannel();
	}

	/**
	 * Utile pour rï¿½cupï¿½rer la connection vers la base de donnï¿½es.
	 * 
	 * @return
	 */
	public EOAdaptorContext getAdaptorContext() {
		return getAdaptorChannel().adaptorContext();
	}

	/**
	 * @return La connection JDBC vers la base de donnï¿½es.
	 */
	public Connection getJDBCConnection() {
		return ((JDBCContext) getAdaptorContext()).connection();
	}

	/**
	 * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	 */
	public final class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		public MyByteArrayOutputStream() {
			this(System.out);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}

		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}

	/**
	 * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	 */
	public final class MyFileOutputStream extends FileOutputStream {
		protected PrintStream out;

		public MyFileOutputStream(PrintStream out, File file) throws FileNotFoundException {
			super(file);
			this.out = out;
		}

		public synchronized void write(int b) throws IOException {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) throws IOException {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}

	private final void initLogs() throws Exception {
		serverLogFile = createLogFile();

		// redirection des logs
		logOutputStreamOut = new MyFileOutputStream(System.out, serverLogFile);
		logOutputStreamErr = new MyFileOutputStream(System.out, serverLogFile);

		prinStreamOut = new PrintStream(logOutputStreamOut, true);
		prinStreamErr = new PrintStream(logOutputStreamErr, true);

		System.setOut(prinStreamOut);
		System.setErr(prinStreamErr);

		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);

		CktlLog.setMyLRLogWriter(new CktlLogWriter());

	}

	private final File createLogFile() {
		String tag = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String fileName = getLogPrefix() + tag + ".txt";

		File rep = new File(System.getProperty("java.io.tmpdir"));

		if (config().stringForKey(CONST_TMP_LOG_DIR) != null && !StringCtrl.isEmpty(config().stringForKey(CONST_TMP_LOG_DIR))) {
			File tmpRep = new File(config().stringForKey(CONST_TMP_LOG_DIR));
			try {
				if (!tmpRep.exists()) {
					throw new Exception("Le dossier specifie pour le parametre TMP_LOG_DIR (" + tmpRep.getAbsolutePath() + ") n'existe pas, utilisation du dossier par  defaut (" + rep.getAbsolutePath() + ").");
				}
				if (!tmpRep.isDirectory()) {
					throw new Exception("Le dossier specifie pour le parametre TMP_LOG_DIR (" + tmpRep.getAbsolutePath() + ") n'existe pas, utilisation du dossier par  defaut (" + rep.getAbsolutePath() + ").");
				}
				if (!tmpRep.canWrite()) {
					throw new Exception("Impossible d'ecrire dans le dossier specifie pour le parametre TMP_LOG_DIR (" + tmpRep.getAbsolutePath() + "), utilisation du dossier par  defaut (" + rep.getAbsolutePath() + ").");
				}
				rep = tmpRep;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		FileFilter logFileFilter = new LogFileFilter();

		if (rep.exists()) {
			File[] fichiers = rep.listFiles(logFileFilter);
			for (int i = 0; i < fichiers.length; i++) {
				File file = fichiers[i];
				System.out.println("Suppression du fichier de log : " + file.getAbsolutePath());
				file.delete();
			}
		}
		File file = new File(rep, fileName);
		return file;
	}

	/**
	 * Filtre qui selectionne des fichiers dont la date de modifiaction est anterieurs a 15 jours.
	 */
	private class LogFileFilter implements FileFilter {
		/**
		 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
		 */
		public boolean accept(File pathname) {
			GregorianCalendar date = new GregorianCalendar();
			date.add(GregorianCalendar.DAY_OF_YEAR, -15);
			return (pathname.getName().startsWith(getLogPrefix()) && pathname.lastModified() <= date.getTimeInMillis());
		}

	}

	public abstract String getLogPrefix();

	public File getServerLogFile() {
		return serverLogFile;
	}

	/** Nom final de l'application ex. Maracuja - comptabilitÃ© */
	protected abstract String getApplicationFinalName();

	/** Nom interne de l'application ex. Maracuja */
	protected abstract String getApplicationInternalName();

	//    
	//    /**
	//     * @return Le n° de version de WebObjects (le dernier chiffre est le n° du build)
	//     */
	//    public String getWOVersion() {
	//        
	//        try {
	//            final InputStream is = resourceManager().inputStreamForResourceNamed("version.plist", "JavaWebObjects", null);
	//            final NSData data = new NSData(is,1024);
	//            final Object plist = NSPropertyListSerialization.propertyListFromData(data, null);
	//            final String list = NSPropertyListSerialization.stringFromPropertyList(plist);
	//            final NSDictionary dico = NSPropertyListSerialization.dictionaryForString(list);
	//            ZLogger.verbose(dico);
	//            return (String) dico.valueForKey("CFBundleShortVersionString") + "." + (String) dico.valueForKey("BuildVersion");
	//        }
	//        catch (Exception e) {
	//            e.printStackTrace();
	//            return null;
	//        }
	//    }

	/**
	 * @return Une nouvelle connexion JDBC
	 * @throws Une exception dans le cas ou il y ai un probleme lors de la creation
	 */
	public Connection getNewJDBCConnection(boolean autoCommit, boolean readOnly) throws Exception {
		ZLogger.debug("Creation d'une nouvelle connexion JDBC");
		NSDictionary dicoConnection;
		dicoConnection = null;
		Connection conn = null;
		dicoConnection = EOModelGroup.defaultGroup().modelNamed(mainModelName()).connectionDictionary();
		conn = DriverManager.getConnection(dicoConnection.valueForKey("URL").toString(), dicoConnection.valueForKey("username").toString(), dicoConnection.valueForKey("password").toString());
		conn.setAutoCommit(autoCommit);
		conn.setReadOnly(readOnly);
		ZLogger.debug("Nouvelle connexion JDBC cree : " + conn);
		return conn;
	}

	//    
	//    
	//    private static NSMutableDictionary modelsDico = new NSMutableDictionary();
	//	
	//    
	//	public static void scanModels() {
	//		modelsDico.removeAllObjects();
	//		final EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
	//		for (int i = 0; i < vModelGroup.models().count(); i++) {
	//			final EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
	//			modelsDico.takeValueForKey(tmpEOModel, tmpEOModel.name());
	//		}
	//	}
	//	
	//	public static NSMutableDictionary getModelsDico() {
	//		return modelsDico;
	//	}
	//	
	//	public void rawLogModelInfos() {
	//		final NSMutableDictionary mdlsDico = getModelsDico(); 
	//		
	//		final StringBuffer sb = new StringBuffer("Informations modeles\n--------------------\n\n");
	//		final Enumeration mdls = mdlsDico.keyEnumerator();
	//		while (mdls.hasMoreElements()) {
	//			final String mdlName = (String) mdls.nextElement();
	//			sb.append("  > Modele ").append(mdlName).append(" :\n");
	//			sb.append("    * Connexion base de donnees : ").append(EOModelCtrl.bdConnexionUrl((EOModel) mdlsDico.objectForKey(mdlName))).append("\n");
	//			sb.append("    * Instance : ").append(EOModelCtrl.bdConnexionServerId((EOModel) mdlsDico.objectForKey(mdlName))).append("\n");
	//			sb.append("    * User base de donnees : ").append(EOModelCtrl.bdConnexionUser((EOModel) mdlsDico.objectForKey(mdlName))).append("\n");
	//			sb.append("    * Chemin : ").append(((EOModel) mdlsDico.objectForKey(mdlName)).pathURL()).append("\n");
	//			sb.append("\n");			
	//			
	//		}
	//		sb.append("\n");
	//		CktlLog.rawLog(sb.toString());
	//	}
	//	
	//	
	private class CktlLogWriter implements ICktlLogWriter {
		public void appendln(String s) {
			ZBufferedLogger.out(s);

		}
	}

}
