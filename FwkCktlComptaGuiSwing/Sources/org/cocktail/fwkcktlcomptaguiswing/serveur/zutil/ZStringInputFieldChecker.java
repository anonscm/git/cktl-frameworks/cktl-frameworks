/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil;

/**
 * Vï¿½rification des saisies utilisateur.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZStringInputFieldChecker {

	private static final String CONSTANT_SAUT_URL = ""; //$NON-NLS-1$

	public static void check(String lib, Object val, boolean required, Integer minLength, Integer maxLength) throws Exception {
		String input = CONSTANT_SAUT_URL;
		if (val != null) {
			input = val.toString();
		}

		if ((required) && (input.length() == 0)) {
			throw new Exception("Le champ de saisie \"" + lib + "\" est obligatoire");
		}

		if (minLength != null) {
			if (input.length() < minLength.intValue()) {
				throw new Exception("Le champ de saisie \"" + lib + "\" doit faire au minimum " + minLength + " caractère(s)");
			}
		}

		if (maxLength != null) {
			if (input.length() > maxLength.intValue()) {
				throw new Exception("Le champ de saisie \"" + lib + "\" doit faire au maximum " + maxLength + " caractère(s)");
			}
		}
	}

}
