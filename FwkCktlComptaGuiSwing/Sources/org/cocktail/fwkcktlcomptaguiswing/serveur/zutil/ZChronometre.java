/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZChronometre {
	private static final String CONSTANT_0 = "["; //$NON-NLS-1$

    public static final String DEFAULTDATEFORMAT="HH:mm:ss ";
	
	private long startTime=0;
	
	private long endTime=0;
	
	private long lastTop=0;
	
	private long timeSinceLastTop=0;
	
	private long totalTime=0;
	
	private String dateFormat=DEFAULTDATEFORMAT;

	private SimpleDateFormat mySimpleDateFormat;
	
	private String name;
	
	private boolean silentMode;
	
	/**
	 * 
	 */
	public ZChronometre(String pname) {
		super();
		silentMode = false;
		reset();
		name = pname;
	}
	
	public ZChronometre(String pname, boolean psilentMode) {
		super();
		silentMode = psilentMode;
		reset();
		name = pname;
	}	
	
	public final void reset() {
		startTime = 0;
		endTime=0;
		lastTop=0;
		timeSinceLastTop=0;
		totalTime=0;
		mySimpleDateFormat = new SimpleDateFormat(dateFormat);
	}
	
	public void start(String mess) {
		startTime = (new Date()).getTime();
		lastTop = startTime;
		logStart(mess);
	}
	
	public void start() {
		start(null);
	}
	
	public void top(String mess) {
		Date now = new Date();
		timeSinceLastTop = now.getTime() - lastTop;
		lastTop = now.getTime();
		logTop(mess);		
	}
	public void top() {
		top(null);
	}	
	
	
	public long getTimeSinceLastTop() {
		return (new Date()).getTime() - lastTop;
	}
	
	
	public void stop(String mess) {
		top(mess);
		endTime = lastTop;
		totalTime = endTime - startTime;
		logStop(mess);	
	}
	
	public long getTotalTime() {
		return totalTime;
	}
	
	
	private void log() {
		if (!silentMode) {
			System.out.print( CONSTANT_0+ name +"] ");
			System.out.print( mySimpleDateFormat.format(new Date(lastTop)));	
		}
	}
	
	private void logStop(String mess) {
		if (!silentMode) {
			if (mess!=null) {
				System.out.print(mess);		
			} 
			else {
				System.out.print("Arret");
			}
			System.out.print(" / Duree totale mesuree : ");
			System.out.print( String.valueOf( totalTime+" ms")  );
			System.out.println();
		}				
	}
	
	private void logStart(String mess) {
		if (!silentMode) {
			log();
			if (mess!=null) {
				System.out.print(mess);		
			} else {
				System.out.print("Demarrage");
			}
			System.out.println();				
		}		
	}	

	private void logTop(String mess) {
		if (!silentMode) {
			log();
			if (mess!=null) {
				System.out.print(" "+mess);		
			}		
			System.out.print(" / Duree depuis le dernier top : ");
			System.out.print(String.valueOf( timeSinceLastTop+" ms")  );
			System.out.println();		
			System.out.println();				
		}
	}	
	
	/**
	 * @return
	 */
	public String getDateFormat() {
		return dateFormat;
	}

	/**
	 * @param string
	 */
	public void setDateFormat(String string) {
		dateFormat = string;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}


}
