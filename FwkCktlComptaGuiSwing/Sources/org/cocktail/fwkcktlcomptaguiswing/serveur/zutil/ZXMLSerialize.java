/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil;
import java.io.IOException;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;


/**
 * Classe permettant de transformer un objet en bloc xml. Utilise la classe CktlXMLWriter.
 *
 * <h3>Exemple</h3>
 *
 * L'exemple de code suivant illustre la creation d'un document XML :
 *
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * 		//...
 * 		//On considere que l'objet myCktlXMLWriter est initialise et que la methode startDocument a ete appelee
 * 		//...
 * 		ZXMLSerialize tmpZXMLSerialize= new ZXMLSerialize(new NSArray(myCktlXMLWriter, new Object[]{"commande/codeMarche/", "commande/engage/", "commande/toAgent/", "commande/toAttribution/","commande/toDetailCde/","commande/toFournis/","commande/toInfosCde/", "commande/todetailcde/toarticle/"}));
 *		tmpZXMLSerialize.objectToXml(tmpCommande, "commande");
 *		//...
 *		//On considere que la methode myCktlXMLWriter.endDocument est appelee
 *		//...
 *
 </pre></td></tr></table>
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZXMLSerialize {

	protected static final Format FORMAT_DATESHORT=new SimpleDateFormat("dd/MM/yyyy");
	/**
	 * Le format a utiliser pour les nombres. On utilise le format US car C'est ce que prend Jasper en entreee.
	 */
	protected static final NumberFormat FORMAT_NUMBERS=NumberFormat.getNumberInstance(Locale.US);


	/**
	 * Tableau contenant les relations ï¿½ exporter (dans le cas d'un EOEnterpriseObject). Les autres relations seront ignorï¿½es.
	 * Chaque relation doit etre indiquee en texte (minuscules) avec son arborescence a partir de l'objet ( par exemple commande/engage/todetailcde/ pour un objet Commande).
	 * Ne pas oublier le "/" ï¿½ la fin de chaque relation.
	 *
	 */
	private NSArray relationsToExport;

	/**
	 * Objet servant a ecrire dans le flux xml.
	 */
	protected CktlXMLWriter myCktlXMLWriter;


	/**
	 * Indique si on souhaite echapper les caracteres & et <.
	 */
	public boolean wantToEscapeSpecialsChars=true;

	public boolean wantToEscapeCarriageReturn=true;

	private final HashMap escapeChars = new HashMap();



	/**
	 * Constructeur.
	 * @param pCktlXMLWriter L'objet dans lequel on ecrit le xml.
	 * @param prelationsToExport Tableau contenant les relations a exporter (dans le cas d'un EOEnterpriseObject). Les autres relations seront ignorees. Chaque relation doit etre indiquee en texte avec son arborescence complete ( par exemple Commande/engage/toDetailCde).
	 */
	public ZXMLSerialize(CktlXMLWriter pCktlXMLWriter, NSArray prelationsToExport) {
		super();

		escapeChars.put("&", CktlXMLWriter.SpecChars.amp);
		escapeChars.put("<", CktlXMLWriter.SpecChars.lt);
		escapeChars.put(">", CktlXMLWriter.SpecChars.gt);
		escapeChars.put("\"", "&quot;");
		escapeChars.put("\n", CktlXMLWriter.SpecChars.br);

		FORMAT_NUMBERS.setGroupingUsed(false);
		myCktlXMLWriter = pCktlXMLWriter;
		relationsToExport = prelationsToExport;
		//Transformer les chemins des relations en minuscules
		NSMutableArray tmp = new NSMutableArray( relationsToExport);
		for (int i=0;i<tmp.count();i++) {
			tmp.replaceObjectAtIndex(((String)tmp.objectAtIndex(i)).toLowerCase(), i)  ;
		}
		relationsToExport = tmp.immutableClone() ;
	}


	/**
	 * Transforme un objet en bloc XML en creant uniquement des noeuds (pas d'attributs). Tous les noms des noeuds seront crees en minuscules, pour respecter les preconisations XML.<br>
	 * Pour les NSDictionary, le nom du noeud sera la cle (en minuscules).
	 * Pour les EOEnterpriseObject, le nom du noeud sera le nom de l'attribut ou de la relation (toujours en minuscules)
	 *
	 * @param obj Objet a transformer en xml (NSDictionary, NSArray, EOEnterpriseObject ou autre).
	 * @param tag Nom du noeud a creer
	 * @return
	 */
	public void objectToXml(Object obj, String tag) throws IOException {
		serializeObjectForTag(obj,tag, "");
	}

	protected void serializeObjectForTag(Object obj, String tag, String arboPere) throws IOException {
		String arbo = arboPere+tag.toLowerCase() +"/";
		if (obj instanceof NSTimestamp) {
			serializeNStimestampForTag((NSTimestamp) obj, tag);
		}
		else if (obj instanceof Number) {
			serializeNumberForTag((Number) obj, tag);
		}
		else if ((obj instanceof NSArray)) {
			serializeNSArrayForTag((NSArray) obj, tag , arboPere);
		}
		else if ((obj instanceof NSDictionary)) {
			serializeNSDictionaryForTag((NSDictionary) obj, tag , arbo);
		}
		else if ((obj instanceof Map)) {
			serializeMapForTag((Map) obj, tag , arbo);
		}
		else if ((obj instanceof EOEnterpriseObject)) {
			serializeEOEnterpriseObjectForTag((EOEnterpriseObject) obj, tag, arbo);
		}
		else if (obj != null) {
			encodeValueInNode(obj , tag);
		}
		else {
			encodeValueInNode(null , tag);
		}
	}



	protected void serializeNSDictionaryForTag(NSDictionary obj, String tag, String arbo) throws IOException {
		NSArray lesKeys = obj.allKeys();
		int nbItems = lesKeys.count();
		myCktlXMLWriter.startElement(tag);
		for (int i=0;i<nbItems;i++) {
			serializeObjectForTag(obj.valueForKey( (String)lesKeys.objectAtIndex(i) ) , ((String)lesKeys.objectAtIndex(i)).toLowerCase(), arbo );
		}
		myCktlXMLWriter.endElement();
	}
	
	protected void serializeMapForTag(Map obj, String tag, String arbo) throws IOException {
		Object[] lesKeys = obj.keySet().toArray();
		int nbItems = lesKeys.length;
		myCktlXMLWriter.startElement(tag);
		for (int i=0;i<nbItems;i++) {
			serializeObjectForTag(obj.get( (String)lesKeys[i] ) , ((String)lesKeys[i]).toLowerCase(), arbo );
		}
		myCktlXMLWriter.endElement();
	}


	protected void serializeNSArrayForTag(NSArray obj, String tagname, String arbo) throws IOException {
		int nbItems = obj.count();
		for (int i=0;i<nbItems;i++) {
//			System.out.println(obj.objectAtIndex(i).getClass().getName());
			serializeObjectForTag(obj.objectAtIndex(i), tagname , arbo );
		}
	}


	private void serializeEOEnterpriseObjectForTag(EOEnterpriseObject obj, String tag, String arbo) throws IOException {
		String tagname;
		String keyname;
		myCktlXMLWriter.startElement(tag);
		NSArray lesattr = obj.attributeKeys();
		for (int i=0; i <lesattr.count();i++) {
			keyname = (String) lesattr.objectAtIndex(i);
			tagname = keyname.toLowerCase();
			serializeObjectForTag(obj.valueForKey(keyname), tagname , arbo);
		}
		NSArray lesrel = obj.toOneRelationshipKeys();
//		NSLog.out.appendln("Relations : "+lesrel);




		for (int i=0; i <lesrel.count();i++) {
			keyname = (String) lesrel.objectAtIndex(i);
			tagname = keyname.toLowerCase();
//			NSLog.out.appendln(this.relationsToExport);

			if ( this.relationsToExport.indexOfObject(arbo+tagname+"/")>=0 ) {
				serializeObjectForTag(obj.valueForKey(keyname), tagname, arbo );
//				NSLog.out.appendln(arbo+tagname+"/" + " : relation incluse");
			}
//			else {
//				NSLog.out.appendln(arbo+tagname+"/" + " : relation exclue");
//			}
		}

		lesrel = obj.toManyRelationshipKeys();
//		NSLog.out.appendln("Relations : "+lesrel);
		for (int i=0; i <lesrel.count();i++) {
			keyname = (String) lesrel.objectAtIndex(i);
			tagname = keyname.toLowerCase();
			if ( this.relationsToExport.indexOfObject(arbo+tagname+"/")>=0 ) {
				serializeObjectForTag(obj.valueForKey(keyname), tagname, arbo );
//				NSLog.out.appendln(arbo+tagname+"/" + " : relation incluse");
			}
//			else {
//				NSLog.out.appendln(arbo+tagname+"/" + " : relation exclue");
//			}

		}
		myCktlXMLWriter.endElement();
	}

	protected void serializeNStimestampForTag(NSTimestamp d, String tag) throws IOException {
		serializeDateForTag(d, tag);
	}

	protected void serializeDateForTag(Date d, String tag) throws IOException {
		encodeValueInNode(FORMAT_DATESHORT.format(d), tag);
	}

	protected void serializeNumberForTag(Number n, String tag) throws IOException {
	    encodeValueInNode( FORMAT_NUMBERS.format(n), tag);
	}

	protected void encodeValueInNode(Object val, String tag) throws IOException {
		if (val==null || EOKeyValueCoding.NullValue.equals( val )) {
			myCktlXMLWriter.writeElement(tag, "");
			return;
		}
		if (this.wantToEscapeSpecialsChars) {
			myCktlXMLWriter.writeElement(tag, ZStringUtil.ifNull(escapeSpecialChars(val.toString()), "")) ;
		}
		else {
			myCktlXMLWriter.writeElement(tag, ZStringUtil.ifNull(val, "")) ;
		}
	}




	protected String escapeSpecialChars(final String val) {
	    String tmpStr = val;
	    final Iterator iter = getEscapeChars().keySet().iterator();
	    while (iter.hasNext()) {
            final String element = (String) iter.next();
            tmpStr = tmpStr.replaceAll(element, (String) getEscapeChars().get(element));
        }
		return tmpStr;
	}


	/**
	 *
	 * @return Une map avec en clï¿½ les caractï¿½res ï¿½ remplacer, et en valeur les codes sgml ? qui remplacent.
	 */
    public HashMap getEscapeChars() {
        return escapeChars;
    }
}
