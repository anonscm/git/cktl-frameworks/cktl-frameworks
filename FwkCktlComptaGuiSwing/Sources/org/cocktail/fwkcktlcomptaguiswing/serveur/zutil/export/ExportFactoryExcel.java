/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.export;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.fwkcktlcomptaguiswing.serveur.zutil.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ExportFactoryExcel extends ExportFactory {
	public static final String SHEETNAME = "SHEETNAME";
	public static final String SHEETNAME_DEFAULT = "Export";

	private HSSFWorkbook _workbook;

	/**
	 * @param keyNames
	 * @param headers
	 * @param values
	 * @param formats
	 */
	public ExportFactoryExcel(NSArray keyNames, NSArray headers, NSArray values, HashMap formats) {
		super(keyNames, headers, values, formats);
	}

	/**
	 * @param ec
	 * @param modelName
	 * @param sqlQuery
	 * @param headers
	 * @param keyNames
	 * @param keysToSum
	 */
	public ExportFactoryExcel(EOEditingContext ec, String modelName, String sqlQuery, NSArray headers, NSArray keyNames, NSArray keysToSum) {
		super(ec, modelName, sqlQuery, headers, keyNames, keysToSum);
	}

	/**
	 * @param ec
	 * @param fetchSpecification
	 * @param headers
	 * @param keyNames
	 */
	public ExportFactoryExcel(EOEditingContext ec, EOFetchSpecification fetchSpecification, NSArray postSorts, NSArray headers, NSArray keyNames) {
		super(ec, fetchSpecification, postSorts, headers, keyNames);
	}

	private void writeRow(final HSSFSheet sheet, final int rowNum, final Object[] objs) throws Exception {
		final HSSFRow row = sheet.createRow(rowNum);
		for (int i = 0; i < objs.length; i++) {
			final Object object = objs[i];
			final HSSFCell cell = row.createCell(i);
			if (object instanceof Number) {
				cell.setCellValue(((Number) object).doubleValue());
			}
			else if (object instanceof Date) {
				//ZLogger.verbose("date...");
				HSSFCellStyle cellStyle = _workbook.createCellStyle();
				HSSFDataFormat df = _workbook.createDataFormat();
				cellStyle.setDataFormat(df.getFormat("dd/mm/yyyy"));
				cell.setCellValue((Date) object);
				cell.setCellStyle(cellStyle);
			}
			else {
				cell.setCellValue((object == null || object instanceof NSKeyValueCoding.Null ? "" : object.toString()));
			}
		}
	}

	private void writeFooter(final HSSFSheet sheet, final int rowNum, final Object[] objs) throws Exception {
		final HSSFCellStyle footerStyle = _workbook.createCellStyle();
		final HSSFFont boldFont = _workbook.createFont();
		boldFont.setFontName(HSSFFont.FONT_ARIAL);
		boldFont.setFontHeightInPoints((short) 10);
		boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		footerStyle.setFont(boldFont);

		final HSSFRow row = sheet.createRow(rowNum);
		for (int i = 0; i < objs.length; i++) {
			final Object object = objs[i];
			final HSSFCell cell = row.createCell(i);
			if (object instanceof Number) {
				cell.setCellValue(((Number) object).doubleValue());
			}
			else if (object instanceof Date) {
				ZLogger.verbose("date...");
				HSSFCellStyle cellStyle = _workbook.createCellStyle();
				HSSFDataFormat df = _workbook.createDataFormat();
				cellStyle.setDataFormat(df.getFormat("dd/mm/yyyy"));
				cell.setCellValue((Date) object);
				cell.setCellStyle(cellStyle);
			}
			else if (object != null && object.toString().startsWith("=")) {
				ZLogger.verbose("formule : " + object.toString());
				//on enleve le "="
				cell.setCellFormula(object.toString().substring(1));
				ZLogger.verbose("formule inseree = " + cell.getCellFormula());
				cell.setCellStyle(footerStyle);
			}
			else {
				cell.setCellValue((object == null ? "" : object.toString()));
			}
		}
	}

	/**
	 * @throws Exception
	 * @see org.cocktail.zutil.server.export.ExportFactory#export(java.util.HashMap)
	 */
	public ByteArrayOutputStream export(HashMap parametres) throws Exception {
		System.out.println("ExportFactoryExcel.exportToExcel()");
		final ByteArrayOutputStream stream = new ByteArrayOutputStream();
		final String sheetName = (String) parametres.get(SHEETNAME);

		final HSSFWorkbook workbook = new HSSFWorkbook();
		_workbook = workbook;
		final HSSFSheet sheet = workbook.createSheet(sheetName);

		final HSSFCellStyle headerStyle = workbook.createCellStyle();
		final HSSFFont boldFont = workbook.createFont();
		boldFont.setFontName(HSSFFont.FONT_ARIAL);
		boldFont.setFontHeightInPoints((short) 10);
		boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		headerStyle.setFont(boldFont);
		//		headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		//		headerStyle.setFillBackgroundColor(HSSFColor.RED.index);

		//        WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
		//        WritableCellFormat headerFormat = new WritableCellFormat(boldFont);
		//        headerFormat.setBackground(Colour.GREY_25_PERCENT);

		final HSSFRow row = sheet.createRow(0);
		for (int i = 0; i < getMyDataSource().getHeaders().length; i++) {
			final HSSFCell cell = row.createCell(i);
			cell.setCellStyle(headerStyle);
			cell.setCellValue(getMyDataSource().getHeaders()[i]);
		}

		for (int i = 0; i < getMyDataSource().getRowCount(); i++) {
			writeRow(sheet, i + 1, getMyDataSource().getObjectsForRow(i));
		}
		writeFooter(sheet, getMyDataSource().getRowCount() + 1, getMyDataSource().getFooters());

		workbook.write(stream);
		stream.close();
		return stream;
	}

}
