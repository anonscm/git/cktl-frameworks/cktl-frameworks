/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlcomptaguiswing.serveur.zutil;
import java.io.IOException;

import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;


/**
 * 
 *   
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZXMLSerializeFlat extends ZXMLSerialize {


	/**
     * @param pCktlXMLWriter
     * @param prelationsToExport
     */
    public ZXMLSerializeFlat(CktlXMLWriter pCktlXMLWriter) {
        super(pCktlXMLWriter, null);
    }

    
	public void objectToXml(NSArray obj, String tag) throws IOException {
		serializeObjectForTag(obj,tag, "");
	}	

    
    protected void encodeValueInNode(Object val, String tag) throws IOException {
        String tmp = " "+tag+"=";
		if (val==null || EOKeyValueCoding.NullValue.equals( val )) {
			tmp = tmp+"\"\"";
			return;
		}
        if (this.wantToEscapeSpecialsChars) {
            tmp = tmp+"\""+ ZStringUtil.ifNull(escapeSpecialChars(val.toString())) +"\"";
        } 
        else {
            tmp = tmp+"\""+ ZStringUtil.ifNull(val.toString()) +"\"";
        }
		myCktlXMLWriter.writeString(tmp);
	}



	protected void serializeNSDictionaryForTag(NSDictionary obj, String tag, String arbo) throws IOException {
		NSArray lesKeys = obj.allKeys();
		int nbItems = lesKeys.count();
		myCktlXMLWriter.writeString("<"+tag + " ");
		for (int i=0;i<nbItems;i++) {
			serializeObjectForTag(obj.valueForKey( (String)lesKeys.objectAtIndex(i) ) , ((String)lesKeys.objectAtIndex(i)).toLowerCase(), arbo );
		}
		myCktlXMLWriter.writeString("/>");
	}




}
