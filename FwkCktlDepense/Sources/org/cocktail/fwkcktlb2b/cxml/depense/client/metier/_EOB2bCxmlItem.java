// _EOB2bCxmlItem.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOB2bCxmlItem.java instead.
package org.cocktail.fwkcktlb2b.cxml.depense.client.metier;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOB2bCxmlItem extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleB2bCxmlItem";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.B2B_CXML_ITEM";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bciId";

	public static final String CLASSIFICATION_KEY = "classification";
	public static final String CLASSIFICATION_DOM_KEY = "classificationDom";
	public static final String COMMENTS_KEY = "comments";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DESCRIPTION_KEY = "description";
	public static final String MANUFACTURER_NAME_KEY = "manufacturerName";
	public static final String MANUFACTURER_PART_ID_KEY = "manufacturerPartID";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String SUPPLIER_PART_AUXILIARY_ID_KEY = "supplierPartAuxiliaryId";
	public static final String SUPPLIER_PART_ID_KEY = "supplierPartId";
	public static final String UNIT_OF_MEASURE_KEY = "unitOfMeasure";
	public static final String UNIT_PRICE_KEY = "unitPrice";
	public static final String URL_KEY = "url";

// Attributs non visibles
	public static final String BCI_ID_KEY = "bciId";
	public static final String FBCP_ID_KEY = "fbcpId";
	public static final String ART_ID_KEY = "artId";

//Colonnes dans la base de donnees
	public static final String CLASSIFICATION_COLKEY = "classification";
	public static final String CLASSIFICATION_DOM_COLKEY = "classification_Dom";
	public static final String COMMENTS_COLKEY = "comments";
	public static final String DATE_CREATION_COLKEY = "date_creation";
	public static final String DESCRIPTION_COLKEY = "description";
	public static final String MANUFACTURER_NAME_COLKEY = "manufacturer_Name";
	public static final String MANUFACTURER_PART_ID_COLKEY = "Manufacturer_Part_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_creation";
	public static final String SUPPLIER_PART_AUXILIARY_ID_COLKEY = "SUPPLIER_PART_AUXILIARY_ID";
	public static final String SUPPLIER_PART_ID_COLKEY = "SUPPLIER_PART_ID";
	public static final String UNIT_OF_MEASURE_COLKEY = "Unit_Of_Measure";
	public static final String UNIT_PRICE_COLKEY = "UNIT_PRICE";
	public static final String URL_COLKEY = "url";

	public static final String BCI_ID_COLKEY = "BCI_ID";
	public static final String FBCP_ID_COLKEY = "FBCP_ID";
	public static final String ART_ID_COLKEY = "ART_ID";


	// Relationships
	public static final String TO_ARTICLE_KEY = "toArticle";
	public static final String TO_FOURNIS_B2B_CXML_PARAM_KEY = "toFournisB2bCxmlParam";



	// Accessors methods
  public String classification() {
    return (String) storedValueForKey(CLASSIFICATION_KEY);
  }

  public void setClassification(String value) {
    takeStoredValueForKey(value, CLASSIFICATION_KEY);
  }

  public String classificationDom() {
    return (String) storedValueForKey(CLASSIFICATION_DOM_KEY);
  }

  public void setClassificationDom(String value) {
    takeStoredValueForKey(value, CLASSIFICATION_DOM_KEY);
  }

  public String comments() {
    return (String) storedValueForKey(COMMENTS_KEY);
  }

  public void setComments(String value) {
    takeStoredValueForKey(value, COMMENTS_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public String description() {
    return (String) storedValueForKey(DESCRIPTION_KEY);
  }

  public void setDescription(String value) {
    takeStoredValueForKey(value, DESCRIPTION_KEY);
  }

  public String manufacturerName() {
    return (String) storedValueForKey(MANUFACTURER_NAME_KEY);
  }

  public void setManufacturerName(String value) {
    takeStoredValueForKey(value, MANUFACTURER_NAME_KEY);
  }

  public String manufacturerPartID() {
    return (String) storedValueForKey(MANUFACTURER_PART_ID_KEY);
  }

  public void setManufacturerPartID(String value) {
    takeStoredValueForKey(value, MANUFACTURER_PART_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public String supplierPartAuxiliaryId() {
    return (String) storedValueForKey(SUPPLIER_PART_AUXILIARY_ID_KEY);
  }

  public void setSupplierPartAuxiliaryId(String value) {
    takeStoredValueForKey(value, SUPPLIER_PART_AUXILIARY_ID_KEY);
  }

  public String supplierPartId() {
    return (String) storedValueForKey(SUPPLIER_PART_ID_KEY);
  }

  public void setSupplierPartId(String value) {
    takeStoredValueForKey(value, SUPPLIER_PART_ID_KEY);
  }

  public String unitOfMeasure() {
    return (String) storedValueForKey(UNIT_OF_MEASURE_KEY);
  }

  public void setUnitOfMeasure(String value) {
    takeStoredValueForKey(value, UNIT_OF_MEASURE_KEY);
  }

  public java.math.BigDecimal unitPrice() {
    return (java.math.BigDecimal) storedValueForKey(UNIT_PRICE_KEY);
  }

  public void setUnitPrice(java.math.BigDecimal value) {
    takeStoredValueForKey(value, UNIT_PRICE_KEY);
  }

  public String url() {
    return (String) storedValueForKey(URL_KEY);
  }

  public void setUrl(String value) {
    takeStoredValueForKey(value, URL_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOArticle toArticle() {
    return (org.cocktail.fwkcktldepense.client.metier.EOArticle)storedValueForKey(TO_ARTICLE_KEY);
  }

  public void setToArticleRelationship(org.cocktail.fwkcktldepense.client.metier.EOArticle value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOArticle oldValue = toArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ARTICLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOFournisB2bCxmlParam toFournisB2bCxmlParam() {
    return (org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOFournisB2bCxmlParam)storedValueForKey(TO_FOURNIS_B2B_CXML_PARAM_KEY);
  }

  public void setToFournisB2bCxmlParamRelationship(org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOFournisB2bCxmlParam value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOFournisB2bCxmlParam oldValue = toFournisB2bCxmlParam();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_B2B_CXML_PARAM_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_B2B_CXML_PARAM_KEY);
    }
  }
  

  public static EOB2bCxmlItem createFwkCaramboleB2bCxmlItem(EOEditingContext editingContext, String classification
, String classificationDom
, NSTimestamp dateCreation
, String description
, String manufacturerName
, String manufacturerPartID
, String supplierPartId
, String unitOfMeasure
, java.math.BigDecimal unitPrice
, org.cocktail.fwkcktldepense.client.metier.EOArticle toArticle, org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOFournisB2bCxmlParam toFournisB2bCxmlParam) {
    EOB2bCxmlItem eo = (EOB2bCxmlItem) createAndInsertInstance(editingContext, _EOB2bCxmlItem.ENTITY_NAME);    
		eo.setClassification(classification);
		eo.setClassificationDom(classificationDom);
		eo.setDateCreation(dateCreation);
		eo.setDescription(description);
		eo.setManufacturerName(manufacturerName);
		eo.setManufacturerPartID(manufacturerPartID);
		eo.setSupplierPartId(supplierPartId);
		eo.setUnitOfMeasure(unitOfMeasure);
		eo.setUnitPrice(unitPrice);
    eo.setToArticleRelationship(toArticle);
    eo.setToFournisB2bCxmlParamRelationship(toFournisB2bCxmlParam);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOB2bCxmlItem.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOB2bCxmlItem.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOB2bCxmlItem localInstanceIn(EOEditingContext editingContext) {
	  		return (EOB2bCxmlItem)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOB2bCxmlItem localInstanceIn(EOEditingContext editingContext, EOB2bCxmlItem eo) {
    EOB2bCxmlItem localInstance = (eo == null) ? null : (EOB2bCxmlItem)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOB2bCxmlItem#localInstanceIn a la place.
   */
	public static EOB2bCxmlItem localInstanceOf(EOEditingContext editingContext, EOB2bCxmlItem eo) {
		return EOB2bCxmlItem.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOB2bCxmlItem fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOB2bCxmlItem fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOB2bCxmlItem eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOB2bCxmlItem)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOB2bCxmlItem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOB2bCxmlItem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOB2bCxmlItem eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOB2bCxmlItem)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouv̩, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouv̩.
	   */
	  public static EOB2bCxmlItem fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOB2bCxmlItem eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOB2bCxmlItem ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOB2bCxmlItem fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
