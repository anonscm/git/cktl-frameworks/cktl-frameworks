/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOInventaire.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOInventaire extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleInventaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_INVENTAIRE";


//Attribute Keys
	public static final ERXKey<String> CLIC_NUM_COMPLET = new ERXKey<String>("clicNumComplet");
	public static final ERXKey<java.math.BigDecimal> LID_MONTANT = new ERXKey<java.math.BigDecimal>("lidMontant");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commande");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> DEPENSE_CONTROLE_PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable>("depenseControlePlanComptable");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organ");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("typeCredit");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "invcId";

	public static final String CLIC_NUM_COMPLET_KEY = "clicNumComplet";
	public static final String LID_MONTANT_KEY = "lidMontant";

//Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String INVC_ID_KEY = "invcId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CLIC_NUM_COMPLET_COLKEY = "CLIC_NUM_COMPLET";
	public static final String LID_MONTANT_COLKEY = "LID_MONTANT";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY = "depenseControlePlanComptable";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
	public String clicNumComplet() {
	 return (String) storedValueForKey(CLIC_NUM_COMPLET_KEY);
	}

	public void setClicNumComplet(String value) {
	 takeStoredValueForKey(value, CLIC_NUM_COMPLET_KEY);
	}

	public java.math.BigDecimal lidMontant() {
	 return (java.math.BigDecimal) storedValueForKey(LID_MONTANT_KEY);
	}

	public void setLidMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, LID_MONTANT_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande commande() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
	}

	public void setCommandeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commande();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable depenseControlePlanComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable)storedValueForKey(DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
	}

	public void setDepenseControlePlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable oldValue = depenseControlePlanComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organ() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
	}

	public void setOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organ();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
	}

	public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = typeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
	 }
	}


	/**
	* Créer une instance de EOInventaire avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOInventaire createEOInventaire(EOEditingContext editingContext						, org.cocktail.fwkcktldepense.server.metier.EOCommande commande				, org.cocktail.fwkcktldepense.server.metier.EOOrgan organ		, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit					) {
	 EOInventaire eo = (EOInventaire) EOUtilities.createAndInsertInstance(editingContext, _EOInventaire.ENTITY_NAME);	 
								 eo.setCommandeRelationship(commande);
						 eo.setOrganRelationship(organ);
				 eo.setPlanComptableRelationship(planComptable);
				 eo.setTypeCreditRelationship(typeCredit);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOInventaire creerInstance(EOEditingContext editingContext) {
		EOInventaire object = (EOInventaire)EOUtilities.createAndInsertInstance(editingContext, _EOInventaire.ENTITY_NAME);
  		return object;
		}

	

  public EOInventaire localInstanceIn(EOEditingContext editingContext) {
    EOInventaire localInstance = (EOInventaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOInventaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOInventaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOInventaire> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOInventaire> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOInventaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOInventaire> objectsForRecherche(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOCommande commandeBinding,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan organBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCreditBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOInventaire.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (commandeBinding != null)
			bindings.takeValueForKey(commandeBinding, "commande");
		  if (organBinding != null)
			bindings.takeValueForKey(organBinding, "organ");
		  if (typeCreditBinding != null)
			bindings.takeValueForKey(typeCreditBinding, "typeCredit");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
