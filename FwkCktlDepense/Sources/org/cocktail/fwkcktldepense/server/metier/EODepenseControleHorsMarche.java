/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.finder.FinderInfosReversement;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EODepenseControleHorsMarche extends _EODepenseControleHorsMarche implements _IDepenseControleHorsMarche, IDepenseControle {
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	private BigDecimal pourcentage;
	public static String DHOM_POURCENTAGE = "pourcentage";
	private BigDecimal maxReversement;

	public EODepenseControleHorsMarche() {
		super();
		setPourcentage(new BigDecimal(0.0));
		maxReversement = new BigDecimal(0.0);
	}
	public void setMontantHT(BigDecimal montant) {
		setDhomHtSaisie(montant);
	}
	public void setMontantTTC(BigDecimal montant) {
		setDhomTtcSaisie(montant);
	}
	public void setMontantTVA(BigDecimal montant) {
		setDhomTvaSaisie(montant);
	}
	public void setMontantBudgetaire(BigDecimal montant) {
		setDhomMontantBudgetaire(montant);
	}
	public BigDecimal getMontantHT() {
		return dhomHtSaisie();
	}
	public BigDecimal getMontantTTC() {
		return dhomTtcSaisie();
	}
	public BigDecimal getMontantTVA() {
		return dhomTvaSaisie();
	}
	public BigDecimal getMontantBudgetaire() {
		return dhomMontantBudgetaire();
	}
	public BigDecimal getPourcentage() {
		return pourcentage();
	}

	public BigDecimal getSommeRepartition() {
		return FinderInfosReversement.getSumHorsMarche(editingContext(), this);
	}

	public String getLabelCode() {
		return "codeExer";
	}
	
	public Object getCode() {
		return this.codeExer();
	}

	public void setMaxReversement(BigDecimal value) {
		if (value == null)
			value = new BigDecimal(0.0);
		maxReversement = value;
	}

	public BigDecimal maxReversement() {
		if (maxReversement == null)
			setMaxReversement(new BigDecimal(0.0));
		return maxReversement;
	}

	public BigDecimal montantTtc() {
		return dhomTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && !depenseBudget().isReversement() && aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && depenseBudget().isReversement() && aValue.floatValue() > 0)
			aValue = new BigDecimal(0.0);
		aValue = arrondirLeMontant(aValue);

		if (depenseBudget() != null && depenseBudget().depTtcSaisie() != null && depenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(depenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (depenseBudget() != null) {
			NSArray depenseControleHorsMarches = depenseBudget().depenseControleHorsMarches();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EODepenseControleHorsMarche.CODE_EXER_KEY + " != nil", null);
			depenseControleHorsMarches = EOQualifier.filteredArrayWithQualifier(depenseControleHorsMarches, qual);
			BigDecimal total = depenseBudget().computeSumForKey(depenseControleHorsMarches, EODepenseControleHorsMarche.DHOM_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (depenseBudget() != null)
			depenseBudget().corrigerMontantHorsMarches();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDhomHtSaisie(BigDecimal aValue) {
		super.setDhomHtSaisie(arrondirLeMontant(aValue));
	}

	public void setDhomTvaSaisie(BigDecimal aValue) {
		super.setDhomTvaSaisie(arrondirLeMontant(aValue));
	}

	public void setDhomTtcSaisie(BigDecimal aValue) {
		super.setDhomTtcSaisie(arrondirLeMontant(aValue));
	}

	public void setDhomMontantBudgetaire(BigDecimal aValue) {
		super.setDhomMontantBudgetaire(arrondirLeMontant(aValue));
	}

	protected BigDecimal arrondirLeMontant(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		return aValue;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (dhomHtSaisie() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomHtSaisieManquant);
		if (dhomTvaSaisie() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomTvaSaisieManquant);
		if (dhomTtcSaisie() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomTtcSaisieManquant);
		if (dhomMontantBudgetaire() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.exerciceManquant);
		if (depenseBudget() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.depenseBudgetManquant);

		if (!dhomHtSaisie().abs().add(dhomTvaSaisie().abs()).equals(dhomTtcSaisie().abs()))
			setDhomTvaSaisie(dhomTtcSaisie().subtract(dhomHtSaisie()));
		if (!dhomMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDhomMontantBudgetaire(calculMontantBudgetaire());
		if (codeExer() != null && codeExer().codeMarche().cmNiveau().intValue() != EOCodeExer.niveauEngageable())
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.codeExerNiveauPasCoherent);
		if (codeExer() != null && !codeExer().exercice().equals(exercice()))
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.codeExerExercicePasCoherent);
		if (codeExer() != null && !depenseBudget().exercice().equals(exercice()))
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.depenseBudgetExercicePasCoherent);
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(dhomHtSaisie(), dhomTvaSaisie(), depenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (codeExer() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.codeExerManquant);
		if (typeAchat() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.typeAchatManquant);
	}
	public boolean isNull() {
		return false;
	}
}
