/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPreference.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPreference extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreference";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_PREFERENCE";


//Attribute Keys
	public static final ERXKey<String> PREF_DEFAULT_VALUE = new ERXKey<String>("prefDefaultValue");
	public static final ERXKey<String> PREF_DESCRIPTION = new ERXKey<String>("prefDescription");
	public static final ERXKey<String> PREF_KEY = new ERXKey<String>("prefKey");
	public static final ERXKey<Integer> PREF_PERSONNALISABLE = new ERXKey<Integer>("prefPersonnalisable");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeApplication> TYPE_APPLICATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeApplication>("typeApplication");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prefId";

	public static final String PREF_DEFAULT_VALUE_KEY = "prefDefaultValue";
	public static final String PREF_DESCRIPTION_KEY = "prefDescription";
	public static final String PREF_KEY_KEY = "prefKey";
	public static final String PREF_PERSONNALISABLE_KEY = "prefPersonnalisable";

//Attributs non visibles
	public static final String PREF_ID_KEY = "prefId";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String PREF_DEFAULT_VALUE_COLKEY = "PREF_DEFAULT_VALUE";
	public static final String PREF_DESCRIPTION_COLKEY = "PREF_DESCRIPTION";
	public static final String PREF_KEY_COLKEY = "PREF_KEY";
	public static final String PREF_PERSONNALISABLE_COLKEY = "PREF_PERSONNALISABLE";

	public static final String PREF_ID_COLKEY = "PREF_ID";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";


	// Relationships
	public static final String TYPE_APPLICATION_KEY = "typeApplication";



	// Accessors methods
	public String prefDefaultValue() {
	 return (String) storedValueForKey(PREF_DEFAULT_VALUE_KEY);
	}

	public void setPrefDefaultValue(String value) {
	 takeStoredValueForKey(value, PREF_DEFAULT_VALUE_KEY);
	}

	public String prefDescription() {
	 return (String) storedValueForKey(PREF_DESCRIPTION_KEY);
	}

	public void setPrefDescription(String value) {
	 takeStoredValueForKey(value, PREF_DESCRIPTION_KEY);
	}

	public String prefKey() {
	 return (String) storedValueForKey(PREF_KEY_KEY);
	}

	public void setPrefKey(String value) {
	 takeStoredValueForKey(value, PREF_KEY_KEY);
	}

	public Integer prefPersonnalisable() {
	 return (Integer) storedValueForKey(PREF_PERSONNALISABLE_KEY);
	}

	public void setPrefPersonnalisable(Integer value) {
	 takeStoredValueForKey(value, PREF_PERSONNALISABLE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplication() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
	}

	public void setTypeApplicationRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeApplication value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeApplication oldValue = typeApplication();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
	 }
	}


	/**
	* Créer une instance de EOPreference avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPreference createEOPreference(EOEditingContext editingContext								, String prefKey
							, Integer prefPersonnalisable
					, org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplication					) {
	 EOPreference eo = (EOPreference) EOUtilities.createAndInsertInstance(editingContext, _EOPreference.ENTITY_NAME);	 
											eo.setPrefKey(prefKey);
									eo.setPrefPersonnalisable(prefPersonnalisable);
						 eo.setTypeApplicationRelationship(typeApplication);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreference creerInstance(EOEditingContext editingContext) {
		EOPreference object = (EOPreference)EOUtilities.createAndInsertInstance(editingContext, _EOPreference.ENTITY_NAME);
  		return object;
		}

	

  public EOPreference localInstanceIn(EOEditingContext editingContext) {
    EOPreference localInstance = (EOPreference)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreference>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreference fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreference fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreference> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreference eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreference)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreference> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreference eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreference)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreference fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreference eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreference ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreference fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
