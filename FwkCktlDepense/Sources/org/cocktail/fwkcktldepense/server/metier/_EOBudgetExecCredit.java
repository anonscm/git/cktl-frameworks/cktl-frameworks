/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOBudgetExecCredit.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOBudgetExecCredit extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleBudgetExecCredit";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_BUDGET_EXEC_CREDIT";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> BDXC_DISPONIBLE = new ERXKey<java.math.BigDecimal>("bdxcDisponible");
	public static final ERXKey<java.math.BigDecimal> BDXC_ENGAGEMENTS = new ERXKey<java.math.BigDecimal>("bdxcEngagements");
	public static final ERXKey<java.math.BigDecimal> BDXC_MANDATS = new ERXKey<java.math.BigDecimal>("bdxcMandats");
	public static final ERXKey<java.math.BigDecimal> BDXC_OUVERTS = new ERXKey<java.math.BigDecimal>("bdxcOuverts");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organ");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("typeCredit");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdxcId";

	public static final String BDXC_DISPONIBLE_KEY = "bdxcDisponible";
	public static final String BDXC_ENGAGEMENTS_KEY = "bdxcEngagements";
	public static final String BDXC_MANDATS_KEY = "bdxcMandats";
	public static final String BDXC_OUVERTS_KEY = "bdxcOuverts";

//Attributs non visibles
	public static final String BDXC_ID_KEY = "bdxcId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String BDXC_DISPONIBLE_COLKEY = "$attribute.columnName";
	public static final String BDXC_ENGAGEMENTS_COLKEY = "BDXC_ENGAGEMENTS";
	public static final String BDXC_MANDATS_COLKEY = "BDXC_MANDATS";
	public static final String BDXC_OUVERTS_COLKEY = "BDXC_OUVERTS";

	public static final String BDXC_ID_COLKEY = "BDXC_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
	public java.math.BigDecimal bdxcDisponible() {
	 return (java.math.BigDecimal) storedValueForKey(BDXC_DISPONIBLE_KEY);
	}

	public void setBdxcDisponible(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BDXC_DISPONIBLE_KEY);
	}

	public java.math.BigDecimal bdxcEngagements() {
	 return (java.math.BigDecimal) storedValueForKey(BDXC_ENGAGEMENTS_KEY);
	}

	public void setBdxcEngagements(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BDXC_ENGAGEMENTS_KEY);
	}

	public java.math.BigDecimal bdxcMandats() {
	 return (java.math.BigDecimal) storedValueForKey(BDXC_MANDATS_KEY);
	}

	public void setBdxcMandats(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BDXC_MANDATS_KEY);
	}

	public java.math.BigDecimal bdxcOuverts() {
	 return (java.math.BigDecimal) storedValueForKey(BDXC_OUVERTS_KEY);
	}

	public void setBdxcOuverts(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, BDXC_OUVERTS_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organ() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
	}

	public void setOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organ();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
	}

	public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = typeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
	 }
	}


	/**
	* Créer une instance de EOBudgetExecCredit avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOBudgetExecCredit createEOBudgetExecCredit(EOEditingContext editingContext				, java.math.BigDecimal bdxcDisponible
							, java.math.BigDecimal bdxcEngagements
							, java.math.BigDecimal bdxcMandats
							, java.math.BigDecimal bdxcOuverts
					, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOOrgan organ		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit					) {
	 EOBudgetExecCredit eo = (EOBudgetExecCredit) EOUtilities.createAndInsertInstance(editingContext, _EOBudgetExecCredit.ENTITY_NAME);	 
							eo.setBdxcDisponible(bdxcDisponible);
									eo.setBdxcEngagements(bdxcEngagements);
									eo.setBdxcMandats(bdxcMandats);
									eo.setBdxcOuverts(bdxcOuverts);
						 eo.setExerciceRelationship(exercice);
				 eo.setOrganRelationship(organ);
				 eo.setTypeCreditRelationship(typeCredit);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetExecCredit creerInstance(EOEditingContext editingContext) {
		EOBudgetExecCredit object = (EOBudgetExecCredit)EOUtilities.createAndInsertInstance(editingContext, _EOBudgetExecCredit.ENTITY_NAME);
  		return object;
		}

	

  public EOBudgetExecCredit localInstanceIn(EOEditingContext editingContext) {
    EOBudgetExecCredit localInstance = (EOBudgetExecCredit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetExecCredit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetExecCredit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBudgetExecCredit> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetExecCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetExecCredit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetExecCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetExecCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBudgetExecCredit> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetExecCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetExecCredit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetExecCredit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetExecCredit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetExecCredit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetExecCredit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit> objectsForRecherche(EOEditingContext ec,
														Integer cleBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan organBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCreditBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOBudgetExecCredit.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (cleBinding != null)
			bindings.takeValueForKey(cleBinding, "cle");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (organBinding != null)
			bindings.takeValueForKey(organBinding, "organ");
		  if (typeCreditBinding != null)
			bindings.takeValueForKey(typeCreditBinding, "typeCredit");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
