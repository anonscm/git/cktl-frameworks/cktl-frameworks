/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOXMLCommande.java instead.
package org.cocktail.fwkcktldepense.server.metier.xml;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOXMLCommande extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleXMLCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.XML_COMMANDE";


//Attribute Keys
	public static final ERXKey<Integer> ATTRIBUTION = new ERXKey<Integer>("attribution");
	public static final ERXKey<Integer> EXERCICE = new ERXKey<Integer>("exercice");
	public static final ERXKey<String> FOURNISSEUR = new ERXKey<String>("fournisseur");
	public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
	public static final ERXKey<Integer> NUMERO = new ERXKey<Integer>("numero");
	public static final ERXKey<Integer> PRESTATION_ID = new ERXKey<Integer>("prestationId");
	public static final ERXKey<String> REFERENCE = new ERXKey<String>("reference");
	public static final ERXKey<String> TYPE_ACHAT = new ERXKey<String>("typeAchat");
	public static final ERXKey<String> TYPE_APPLICATION = new ERXKey<String>("typeApplication");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle> ARTICLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle>("articles");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "commId";

	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String LIBELLE_KEY = "libelle";
	public static final String NUMERO_KEY = "numero";
	public static final String PRESTATION_ID_KEY = "prestationId";
	public static final String REFERENCE_KEY = "reference";
	public static final String TYPE_ACHAT_KEY = "typeAchat";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";

//Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String ULT_ORDRE_KEY = "ultOrdre";

//Colonnes dans la base de donnees
	public static final String ATTRIBUTION_COLKEY = "ATT_ORDRE";
	public static final String EXERCICE_COLKEY = "EXE_ORDRE";
	public static final String FOURNISSEUR_COLKEY = "FOU_CODE";
	public static final String LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String NUMERO_COLKEY = "COMM_NUMERO";
	public static final String PRESTATION_ID_COLKEY = "PRESTATION_ID";
	public static final String REFERENCE_COLKEY = "COMM_REFERENCE";
	public static final String TYPE_ACHAT_COLKEY = "TYPA_LIBELLE";
	public static final String TYPE_APPLICATION_COLKEY = "TYAP_LIBELLE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String ULT_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ARTICLES_KEY = "articles";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public Integer attribution() {
	 return (Integer) storedValueForKey(ATTRIBUTION_KEY);
	}

	public void setAttribution(Integer value) {
	 takeStoredValueForKey(value, ATTRIBUTION_KEY);
	}

	public Integer exercice() {
	 return (Integer) storedValueForKey(EXERCICE_KEY);
	}

	public void setExercice(Integer value) {
	 takeStoredValueForKey(value, EXERCICE_KEY);
	}

	public String fournisseur() {
	 return (String) storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseur(String value) {
	 takeStoredValueForKey(value, FOURNISSEUR_KEY);
	}

	public String libelle() {
	 return (String) storedValueForKey(LIBELLE_KEY);
	}

	public void setLibelle(String value) {
	 takeStoredValueForKey(value, LIBELLE_KEY);
	}

	public Integer numero() {
	 return (Integer) storedValueForKey(NUMERO_KEY);
	}

	public void setNumero(Integer value) {
	 takeStoredValueForKey(value, NUMERO_KEY);
	}

	public Integer prestationId() {
	 return (Integer) storedValueForKey(PRESTATION_ID_KEY);
	}

	public void setPrestationId(Integer value) {
	 takeStoredValueForKey(value, PRESTATION_ID_KEY);
	}

	public String reference() {
	 return (String) storedValueForKey(REFERENCE_KEY);
	}

	public void setReference(String value) {
	 takeStoredValueForKey(value, REFERENCE_KEY);
	}

	public String typeAchat() {
	 return (String) storedValueForKey(TYPE_ACHAT_KEY);
	}

	public void setTypeAchat(String value) {
	 takeStoredValueForKey(value, TYPE_ACHAT_KEY);
	}

	public String typeApplication() {
	 return (String) storedValueForKey(TYPE_APPLICATION_KEY);
	}

	public void setTypeApplication(String value) {
	 takeStoredValueForKey(value, TYPE_APPLICATION_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle> articles() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle>)storedValueForKey(ARTICLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle> articles(EOQualifier qualifier) {
	 return articles(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle> articles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle> results;
			   results = articles();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToArticlesRelationship(org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
	}
	
	public void removeFromArticlesRelationship(org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle createArticlesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle) eo;
	}
	
	public void deleteArticlesRelationship(org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllArticlesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle> objects = articles().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteArticlesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOXMLCommande avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOXMLCommande createEOXMLCommande(EOEditingContext editingContext				, Integer attribution
							, Integer exercice
							, String fournisseur
							, String libelle
							, Integer numero
							, Integer prestationId
							, String reference
							, String typeAchat
							, String typeApplication
					, org.cocktail.fwkcktldepense.server.metier.xml.EOXMLUtilisateur utilisateur					) {
	 EOXMLCommande eo = (EOXMLCommande) EOUtilities.createAndInsertInstance(editingContext, _EOXMLCommande.ENTITY_NAME);	 
							eo.setAttribution(attribution);
									eo.setExercice(exercice);
									eo.setFournisseur(fournisseur);
									eo.setLibelle(libelle);
									eo.setNumero(numero);
									eo.setPrestationId(prestationId);
									eo.setReference(reference);
									eo.setTypeAchat(typeAchat);
									eo.setTypeApplication(typeApplication);
						 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOXMLCommande creerInstance(EOEditingContext editingContext) {
		EOXMLCommande object = (EOXMLCommande)EOUtilities.createAndInsertInstance(editingContext, _EOXMLCommande.ENTITY_NAME);
  		return object;
		}

	

  public EOXMLCommande localInstanceIn(EOEditingContext editingContext) {
    EOXMLCommande localInstance = (EOXMLCommande)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOXMLCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOXMLCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOXMLCommande> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOXMLCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOXMLCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOXMLCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOXMLCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOXMLCommande> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOXMLCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOXMLCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOXMLCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOXMLCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOXMLCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOXMLCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
