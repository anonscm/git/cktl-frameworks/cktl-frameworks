/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleConventionException;
import org.cocktail.fwkcktldepense.server.finder.FinderInfosReversement;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EODepenseControleConvention extends _EODepenseControleConvention implements _IDepenseControleConvention, IDepenseControle {
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	private BigDecimal pourcentage;
	public static String DCON_POURCENTAGE = "pourcentage";
	private BigDecimal maxReversement;

	public EODepenseControleConvention() {
		super();
		setPourcentage(new BigDecimal(0.0));
		maxReversement = new BigDecimal(0.0);
	}

	public void setMontantHT(BigDecimal montant) {
		setDconHtSaisie(montant);
	}
	public void setMontantTTC(BigDecimal montant) {
		setDconTtcSaisie(montant);
	}
	public void setMontantTVA(BigDecimal montant) {
		setDconTvaSaisie(montant);
	}
	public void setMontantBudgetaire(BigDecimal montant) {
		setDconMontantBudgetaire(montant);
	}
	public BigDecimal getMontantHT() {
		return dconHtSaisie();
	}
	public BigDecimal getMontantTTC() {
		return dconTtcSaisie();
	}
	public BigDecimal getMontantTVA() {
		return dconTvaSaisie();
	}
	public BigDecimal getMontantBudgetaire() {
		return dconMontantBudgetaire();
	}
	public BigDecimal getPourcentage() {
		return pourcentage();
	}

	public BigDecimal getSommeRepartition() {
		return FinderInfosReversement.getSumConvention(editingContext(), this);
	}
	
	public String getLabelCode() {
		return "convention";
	}

	public Object getCode() {
		return this.convention();
	}

	public void setMaxReversement(BigDecimal value) {
		if (value == null)
			value = new BigDecimal(0.0);
		maxReversement = value;
	}

	public BigDecimal maxReversement() {
		if (maxReversement == null)
			setMaxReversement(new BigDecimal(0.0));
		return maxReversement;
	}

	public BigDecimal montantTtc() {
		return dconTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && !depenseBudget().isReversement() && aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && depenseBudget().isReversement() && aValue.floatValue() > 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		if (depenseBudget() != null && depenseBudget().depTtcSaisie() != null && depenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(depenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (depenseBudget() != null) {
			NSArray depenseControleConventions = depenseBudget().depenseControleConventions();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EODepenseControleConvention.CONVENTION_KEY + " != nil", null);
			depenseControleConventions = EOQualifier.filteredArrayWithQualifier(depenseControleConventions, qual);
			BigDecimal total = depenseBudget().computeSumForKey(depenseControleConventions, EODepenseControleConvention.DCON_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (depenseBudget() != null)
			depenseBudget().corrigerMontantConventions();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDconHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDconHtSaisie(aValue);
	}

	public void setDconTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDconTvaSaisie(aValue);
	}

	public void setDconTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDconTtcSaisie(aValue);
	}

	public void setDconMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDconMontantBudgetaire(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (dconHtSaisie() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.dconHtSaisieManquant);
		if (dconTvaSaisie() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.dconTvaSaisieManquant);
		if (dconTtcSaisie() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.dconTtcSaisieManquant);
		if (dconMontantBudgetaire() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.dconMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.exerciceManquant);
		if (depenseBudget() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.depenseBudgetManquant);

		if (!dconHtSaisie().abs().add(dconTvaSaisie().abs()).equals(dconTtcSaisie().abs()))
			setDconTvaSaisie(dconTtcSaisie().subtract(dconHtSaisie()));
		if (!dconMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDconMontantBudgetaire(calculMontantBudgetaire());

		if (!depenseBudget().exercice().equals(exercice()))
			throw new DepenseControleConventionException(DepenseControleConventionException.depenseBudgetExercicePasCoherent);
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(dconHtSaisie(), dconTvaSaisie(), depenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (convention() == null)
			throw new DepenseControleConventionException(DepenseControleConventionException.conventionManquant);
	}

	public boolean isNull() {
		return false;
	}
}
