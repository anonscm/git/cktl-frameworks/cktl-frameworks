/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.FinderDevise;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOTva extends _EOTva
{
	public static final String ETAT_VALIDE = "VALIDE";
	public static final String ETAT_ANNULE = "ANNULE";

	private static Calculs serviceCalcul = Calculs.getInstance();
	
	public EOTva() {
		super();
	}

	/**
	 * Attention cette methode ne renvoit pas le taux de TVA au sens usuel du terme (19.6) mais renvoit 0.196
	 * 
	 * @param ht
	 * @param ttc
	 * @return
	 */
	@Deprecated
	public static BigDecimal calcTauxTVA(BigDecimal ht, BigDecimal ttc) {
		return serviceCalcul.calculTauxTva(ttc, ht);
	}

	@Deprecated
	public BigDecimal montantTva(BigDecimal ht, int arrondi) {
		return serviceCalcul.calculMontantTVAaPartirHT(tvaTaux(), ht, arrondi);
	}

	@Deprecated
	public BigDecimal montantTvaAvecTtc(BigDecimal ttc, int arrondi) {
		return serviceCalcul.calculMontantTVAaPartirTTC(tvaTaux(), ttc, arrondi);
	}

	@Deprecated
	public BigDecimal montantHtAvecTtc(BigDecimal ttc, int arrondi) {
		return serviceCalcul.calculHTaPartirTTC(tvaTaux(), ttc, arrondi);
	}

	@Deprecated
	public BigDecimal montantTtcAvecHt(BigDecimal ht, int arrondi) {
		return serviceCalcul.calculTTCaPartirHT(tvaTaux(), ht, arrondi);
	}

	/**
	 * Tient compte de la devise paramétrée pour récupérer le nombre de decimales pour l'arrondi.
	 * 
	 * @param edc
	 * @param exercice
	 * @param montantHt
	 * @param tva
	 * @return
	 */
	public static BigDecimal calcMontantTtc(EOEditingContext edc, EOExercice exercice, BigDecimal montantHt, EOTva tva) {
		int arrondi = FinderDevise.getDecimales(edc, exercice);
		if (tva == null) {
			return montantHt.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		}
		return tva.montantTtcAvecHt(montantHt, arrondi);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		throw new FactoryException(ENTITY_NAME + " : " + FactoryException.messageErreur);
	}

}
