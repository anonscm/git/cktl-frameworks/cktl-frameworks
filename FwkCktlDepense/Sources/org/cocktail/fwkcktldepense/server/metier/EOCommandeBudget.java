/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;
import java.util.Date;

import org.cocktail.fwkcktldepense.server.FwkCktlDepenseHelper;
import org.cocktail.fwkcktldepense.server.exception.CommandeBudgetException;
import org.cocktail.fwkcktldepense.server.factory.Factory;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeAction;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCommandeBudget extends _EOCommandeBudget
{

	private static Calculs serviceCalcul = Calculs.getInstance();

	private static final long serialVersionUID = 1L;
	private Number cbudIdProc;
	private BigDecimal minBudgetaire;

	private NSArray<EOTypeAction> arrayActions;
	private NSArray<EOCodeAnalytique> arrayAnalytiques;
	private NSArray<EOConvention> arrayConventions;
	private NSArray<EOPlanComptable> arrayPlanComptables;

	public EOCommandeBudget() {
		super();
		cbudIdProc = null;
		minBudgetaire = new BigDecimal(0.0);
		arrayActions = null;
		arrayAnalytiques = null;
		arrayConventions = null;
		arrayPlanComptables = null;
	}

	public BigDecimal minBudgetaire() {
		return minBudgetaire;
	}

	public BigDecimal restantHtControleAction() {
		return cbudHtSaisie().subtract(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControleAction() {
		return cbudTtcSaisie().subtract(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_TTC_SAISIE_KEY));
	}

	public BigDecimal restantHtControleAnalytique() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleAnalytique() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleConvention() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleConvention() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleHorsMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleHorsMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControlePlanComptable() {
		return cbudHtSaisie().subtract(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControlePlanComptable() {
		return cbudTtcSaisie().subtract(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_TTC_SAISIE_KEY));
	}

	public NSArray<EOTypeAction> getTypeActionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayActions == null && source() != null && utilisateur != null)
			arrayActions = FinderTypeAction.getTypeActions(ed, source(), utilisateur);
		if (arrayActions == null)
			arrayActions = NSArray.emptyArray();

		if (arrayActions.count() == 0)
			return arrayActions;

		/*
		 * if (commandeControleActions()!=null && commandeControleActions().count()>0) { NSMutableArray resultats=new NSMutableArray(arrayActions);
		 * for (int i=0; i<commandeControleActions().count(); i++) { EOTypeAction unTypeAction =
		 * ((EOCommandeControleAction)commandeControleActions().objectAtIndex(i)).typeAction(); if (unTypeAction != null)
		 * resultats.removeObject(unTypeAction); } return resultats; }
		 */
		return arrayActions;
	}

	public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques == null && source() != null && utilisateur != null)
			arrayAnalytiques = FinderCodeAnalytique.getCodeAnalytiques(ed, source().organ(), source().exercice());
		if (arrayAnalytiques == null)
			arrayAnalytiques = NSArray.emptyArray();

		if (arrayAnalytiques.count() == 0)
			return arrayAnalytiques;

		return arrayAnalytiques;
	}

	public NSArray<EOConvention> getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null && source() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(source().organ(), "organ");
			bindings.setObjectForKey(source().typeCredit(), "typeCredit");
			bindings.setObjectForKey(source().exercice(), "exercice");
			bindings.setObjectForKey(new Boolean(true), "montants");

			NSTimestamp dateClotureConventionMax = new NSTimestamp(new FwkCktlDepenseHelper().corrigeDateSelonExercice(new Date(), this.exercice().exeExercice()));
			bindings.setObjectForKey(dateClotureConventionMax, "conDateClotureMax");

			arrayConventions = FinderConvention.getConventions(ed, bindings);
		}
		if (arrayConventions == null)
			arrayConventions = NSArray.emptyArray();

		if (arrayConventions.count() == 0)
			return arrayConventions;

		return arrayConventions;
	}

	public NSArray<EOPlanComptable> getPlanComptablesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayPlanComptables == null && source() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(source().typeCredit(), "typeCredit");
			bindings.setObjectForKey(utilisateur, "utilisateur");
			bindings.setObjectForKey(source().exercice(), "exercice");
			arrayPlanComptables = FinderPlanComptable.getPlanComptables(ed, bindings);
		}
		if (arrayPlanComptables == null)
			arrayPlanComptables = NSArray.emptyArray();

		if (arrayPlanComptables.count() == 0)
			return arrayPlanComptables;

		return arrayPlanComptables;
	}

	public boolean isControleActionGood() {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControleAction.TYPE_ACTION_KEY + "=nil", null);
		NSArray<EOCommandeControleAction> array = EOQualifier.filteredArrayWithQualifier(commandeControleActions(), qual);
		if (array != null && array.count() > 0)
			return false;

		if (computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_POURCENTAGE_KEY).floatValue() != 100)
			return false;
		if (restantHtControleAction().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControleAction().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (cbudMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isControleAnalytiqueGood() {
		float sommePourcentage = computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_POURCENTAGE_KEY).
				floatValue();
		if (sommePourcentage < 0 || sommePourcentage > 100)
			return false;
		return true;
	}

	public boolean isControleConventionGood() {
		float sommePourcentage = computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_POURCENTAGE_KEY).
				floatValue();
		if (sommePourcentage < 0 || sommePourcentage > 100)
			return false;

		return true;
	}

	public boolean isControleHorsMarcheGood() {
		if (restantHtControleHorsMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControleHorsMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (cbudMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isControleMarcheGood() {
		if (restantHtControleMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControleMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (cbudMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isControlePlanComptableGood() {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControlePlanComptable.PLAN_COMPTABLE_KEY + "=nil", null);
		NSArray<EOCommandeControlePlanComptable> array = EOQualifier.filteredArrayWithQualifier(commandeControlePlanComptables(), qual);
		if (array != null && array.count() > 0)
			return false;

		if (computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_POURCENTAGE_KEY).floatValue() != 100)
			return false;
		if (restantHtControlePlanComptable().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControlePlanComptable().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (cbudMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isSupprimable() {
		EOEngagementBudget engage = getEngagement();
		if (engage == null)
			return true;
		if (engage.engMontantBudgetaire().compareTo(engage.engMontantBudgetaireReste()) == 0)
			return true;
		return false;
	}

	public void setCommandeRelationship(EOCommande value) {
		super.setCommandeRelationship(value);
		creationCtrlMarcheHorsMarche();
	}

	public EOSource source() {
		return new EOSource(organ(), typeCredit(), tauxProrata(), exercice());
	}

	public void setSource(EOSource source) {
		setOrganRelationship(source.organ());
		setTypeCreditRelationship(source.typeCredit());
		setTauxProrataRelationship(source.tauxProrata());
		setExerciceRelationship(source.exercice());

		EOEngagementBudget engage = getEngagement();
		if (engage == null)
			minBudgetaire = new BigDecimal(0.0);
		else
			minBudgetaire = engage.engMontantBudgetaire().subtract(engage.engMontantBudgetaireReste());
	}

	public EOEngagementBudget getEngagement() {
		if (organ() == null || typeCredit() == null || tauxProrata() == null || exercice() == null || commande().fournisseur() == null)
			return null;
		if (commande() == null)
			return null;
		if (commande().commandeEngagements() == null || commande().commandeEngagements().count() == 0)
			return null;

		NSMutableArray<EOQualifier> arrayQualifier = new NSMutableArray<EOQualifier>();
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("organ=%@", new NSArray<Object>(organ())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit=%@", new NSArray<Object>(typeCredit())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("tauxProrata=%@", new NSArray<Object>(tauxProrata())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray<Object>(exercice())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("fournisseur=%@", new NSArray<Object>(commande().fournisseur())));

		NSArray<EOCommandeEngagement> larray = EOQualifier.filteredArrayWithQualifier(commande().commandeEngagements(), new EOAndQualifier(arrayQualifier));

		if (larray.count() == 0)
			return null;

		return larray.objectAtIndex(0).engagementBudget();
		//		return (EOEngagementBudget) larray.objectAtIndex(0);
	}

	private void creationCtrlMarcheHorsMarche() {
		FactoryCommandeControleHorsMarche factoryCommandeControleHorsMarche = new FactoryCommandeControleHorsMarche();
		FactoryCommandeControleMarche factoryCommandeControleMarche = new FactoryCommandeControleMarche();

		if (editingContext() == null)
			return;

		if (commande() == null) {
			for (int i = commandeControleHorsMarches().count() - 1; i >= 0; i--)
				factoryCommandeControleHorsMarche.supprimer(editingContext(),
						(EOCommandeControleHorsMarche) commandeControleHorsMarches().objectAtIndex(i));
			for (int i = commandeControleMarches().count() - 1; i >= 0; i--)
				factoryCommandeControleMarche.supprimer(editingContext(),
						(EOCommandeControleMarche) commandeControleMarches().objectAtIndex(i));
			return;
		}

		if (commande().articles() == null || commande().articles().count() == 0)
			return;

		if (cbudHtSaisie() == null)
			setCbudHtSaisie(new BigDecimal(0.0));
		if (cbudTtcSaisie() == null)
			setCbudTtcSaisie(new BigDecimal(0.0));
		if (cbudTvaSaisie() == null)
			setCbudTvaSaisie(cbudTtcSaisie().subtract(cbudHtSaisie()));
		if (cbudMontantBudgetaire() == null)
			setCbudMontantBudgetaire(new BigDecimal(0.0));

		// si c'est un marche
		if (((EOArticle) commande().articles().objectAtIndex(0)).attribution() != null) {
			if (commandeControleMarches().count() != 0)
				return;
			factoryCommandeControleMarche.creer(editingContext(), cbudHtSaisie(), cbudTvaSaisie(), cbudTtcSaisie(),
					cbudMontantBudgetaire(), ((EOArticle) commande().articles().objectAtIndex(0)).attribution(), exercice(), this);
			return;
		}

		// hors marche
		for (int i = 0; i < commande().articles().count(); i++) {
			EOTypeAchat typeAchat = ((EOArticle) commande().articles().objectAtIndex(i)).typeAchat();
			EOCodeExer codeExer = FinderCodeExer.getCodeExerNiveau2(((EOArticle) commande().articles().objectAtIndex(i)).codeExer());

			NSMutableArray<EOQualifier> arrayQualifier = new NSMutableArray<EOQualifier>();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.TYPE_ACHAT_KEY + "=%@",
					new NSArray<Object>(typeAchat)));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.CODE_EXER_KEY + "=%@",
					new NSArray<Object>(codeExer)));

			if (EOQualifier.filteredArrayWithQualifier(commandeControleHorsMarches(), new EOAndQualifier(arrayQualifier)).count() > 0)
				continue;

			factoryCommandeControleHorsMarche.creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0),
					new BigDecimal(0.0), codeExer, typeAchat, exercice(), this);
		}
	}

	public void setCbudIdProc(Number value) {
		cbudIdProc = value;
	}

	public Number cbudIdProc() {
		return cbudIdProc;
	}

	public void mettreAJourMontants() {
		if (commandeControleHorsMarches() != null && commandeControleHorsMarches().count() > 0) {
			setCbudHtSaisie(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_HT_SAISIE_KEY));
			setCbudTtcSaisie(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_TTC_SAISIE_KEY));
		}
		else {
			if (commandeControleMarches() != null && commandeControleMarches().count() > 0) {
				setCbudHtSaisie(computeSumForKey(commandeControleMarches(), EOCommandeControleMarche.CMAR_HT_SAISIE_KEY));
				setCbudTtcSaisie(computeSumForKey(commandeControleMarches(), EOCommandeControleMarche.CMAR_TTC_SAISIE_KEY));
			}
			else {
				setCbudHtSaisie(new BigDecimal(0.0));
				setCbudTtcSaisie(new BigDecimal(0.0));
			}
		}

		setCbudTvaSaisie(cbudTtcSaisie().subtract(cbudHtSaisie()));
		if (tauxProrata() != null)
			setCbudMontantBudgetaire(calculMontantBugetaire());
		else
			setCbudMontantBudgetaire(Calculs.ZERO);
	}

	public void setCbudHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setCbudHtSaisie(aValue);
		corrigerMontant();
	}

	public void setCbudTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setCbudTvaSaisie(aValue);
		corrigerMontant();
	}

	public void setCbudTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setCbudTtcSaisie(aValue);
		corrigerMontant();
	}

	public void setCbudMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setCbudMontantBudgetaire(aValue);
		corrigerMontant();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws CommandeBudgetException {
		if (cbudHtSaisie() == null)
			throw new CommandeBudgetException(CommandeBudgetException.cbudHtSaisieManquant);
		if (cbudTvaSaisie() == null)
			throw new CommandeBudgetException(CommandeBudgetException.cbudTvaSaisieManquant);
		if (cbudTtcSaisie() == null)
			throw new CommandeBudgetException(CommandeBudgetException.cbudTtcSaisieManquant);
		if (cbudMontantBudgetaire() == null)
			throw new CommandeBudgetException(CommandeBudgetException.cbudMontantBudgetaireManquant);

		if (commande() == null)
			throw new CommandeBudgetException(CommandeBudgetException.commandeManquant);
		if (exercice() == null)
			throw new CommandeBudgetException(CommandeBudgetException.exerciceManquant);
		if (tauxProrata() == null)
			throw new CommandeBudgetException(CommandeBudgetException.tauxProrataManquant);
		if (organ() == null)
			throw new CommandeBudgetException(CommandeBudgetException.organManquant);
		if (typeCredit() == null)
			throw new CommandeBudgetException(CommandeBudgetException.typeCreditManquant);

		if (!cbudHtSaisie().abs().add(cbudTvaSaisie().abs()).equals(cbudTtcSaisie().abs()))
			setCbudTvaSaisie(cbudTtcSaisie().subtract(cbudHtSaisie()));
		if (cbudTvaSaisie().floatValue() < 0.0)
			throw new CommandeBudgetException(CommandeBudgetException.cbudMontantNegatif);
		if( !cbudMontantBudgetaire().equals(calculMontantBugetaire()))
			setCbudMontantBudgetaire(calculMontantBugetaire());

		if (!commande().exercice().equals(exercice()))
			throw new CommandeBudgetException(CommandeBudgetException.commandeExercicePasCoherent);
	}

	private BigDecimal calculMontantBugetaire() {
		return serviceCalcul.calculMontantBudgetaire(cbudHtSaisie(), cbudTvaSaisie(), tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (cbudHtSaisie().floatValue() == 0.0 && cbudTtcSaisie().floatValue() == 0.0)
			throw new CommandeBudgetException(CommandeBudgetException.ligneNonUtilisee);

		corrigerMontant();

		// on verifie que les montants de la commandeBudget ne sont pas inferieur a ceux des commandeControle et des articleBudget
		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_HT_SAISIE_KEY)) != 0 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_TVA_SAISIE_KEY)) != 0 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_TTC_SAISIE_KEY)) != 0 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_MONTANT_BUDGETAIRE_KEY)) != 0)
			throw new CommandeBudgetException(CommandeBudgetException.montantsCommandeControleActionIncoherent);

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_HT_SAISIE_KEY)) == -1 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_TVA_SAISIE_KEY)) == -1 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_TTC_SAISIE_KEY)) == -1 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_MONTANT_BUDGETAIRE_KEY)) == -1)
			throw new CommandeBudgetException(CommandeBudgetException.montantsCommandeControleAnalytiqueIncoherent);

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_HT_SAISIE_KEY)) == -1 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_TVA_SAISIE_KEY)) == -1 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_TTC_SAISIE_KEY)) == -1 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_MONTANT_BUDGETAIRE_KEY)) == -1)
			throw new CommandeBudgetException(CommandeBudgetException.montantsCommandeControleConventionIncoherent);

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_HT_SAISIE_KEY)) != 0 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_TVA_SAISIE_KEY)) != 0 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_TTC_SAISIE_KEY)) != 0 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_MONTANT_BUDGETAIRE_KEY)) != 0)
			throw new CommandeBudgetException(CommandeBudgetException.montantsCommandeControlePlanComptableIncoherent);

		if (commandeControleHorsMarches() != null && commandeControleHorsMarches().count() > 0) {

			if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_HT_SAISIE_KEY)) != 0 ||
					cbudTvaSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_TVA_SAISIE_KEY)) != 0 ||
					cbudTtcSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_TTC_SAISIE_KEY)) != 0 ||
					cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_MONTANT_BUDGETAIRE_KEY)) != 0) {

				System.out.println("bud : " + cbudMontantBudgetaire() + ", cn : " + computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_MONTANT_BUDGETAIRE_KEY));

				for (int i = 0; i < commandeControleHorsMarches().count(); i++)
					System.out.println("  budgetaire : " + ((EOCommandeControleHorsMarche) commandeControleHorsMarches().objectAtIndex(i)).chomMontantBudgetaire());

				throw new CommandeBudgetException(CommandeBudgetException.montantsCommandeControleHorsMarcheIncoherent);
			}

			if (commandeControleMarches() != null && commandeControleMarches().count() > 0)
				throw new CommandeBudgetException(CommandeBudgetException.controleMarcheHorsMarcheIncoherent);
		}
	}

	protected BigDecimal computeSumForKey(NSArray<? extends EOEnterpriseObject> eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	public boolean isComplet() {
		if (organ() == null)
			return false;
		if (tauxProrata() == null)
			return false;
		if (typeCredit() == null)
			return false;
		if (exercice() == null)
			return false;

		if (isControleActionGood() == false) {
			return false;
		}
		if (isControlePlanComptableGood() == false) {
			return false;
		}

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_HT_SAISIE_KEY)) != 0 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_TVA_SAISIE_KEY)) != 0 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_TTC_SAISIE_KEY)) != 0 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_MONTANT_BUDGETAIRE_KEY)) != 0)
			return false;

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_HT_SAISIE_KEY)) == -1 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_TVA_SAISIE_KEY)) == -1 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_TTC_SAISIE_KEY)) == -1 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_MONTANT_BUDGETAIRE_KEY)) == -1)
			return false;

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_HT_SAISIE_KEY)) == -1 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_TVA_SAISIE_KEY)) == -1 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_TTC_SAISIE_KEY)) == -1 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_MONTANT_BUDGETAIRE_KEY)) == -1)
			return false;

		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_HT_SAISIE_KEY)) != 0 ||
				cbudTvaSaisie().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_TVA_SAISIE_KEY)) != 0 ||
				cbudTtcSaisie().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_TTC_SAISIE_KEY)) != 0 ||
				cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_MONTANT_BUDGETAIRE_KEY)) != 0)
			return false;

		if (commandeControleHorsMarches() != null && commandeControleHorsMarches().count() > 0) {
			if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_HT_SAISIE_KEY)) != 0 ||
					cbudTvaSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_TVA_SAISIE_KEY)) != 0 ||
					cbudTtcSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_TTC_SAISIE_KEY)) != 0 ||
					cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_MONTANT_BUDGETAIRE_KEY)) != 0)
				return false;

			if (commandeControleMarches() != null && commandeControleMarches().count() > 0)
				return false;
		}
		else {
			if (commandeControleMarches() == null || commandeControleMarches().count() == 0)
				return false;

			if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleMarches(), EOCommandeControleMarche.CMAR_HT_SAISIE_KEY)) != 0 ||
					cbudTvaSaisie().compareTo(computeSumForKey(commandeControleMarches(), EOCommandeControleMarche.CMAR_TVA_SAISIE_KEY)) != 0 ||
					cbudTtcSaisie().compareTo(computeSumForKey(commandeControleMarches(), EOCommandeControleMarche.CMAR_TTC_SAISIE_KEY)) != 0 ||
					cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleMarches(), EOCommandeControleMarche.CMAR_MONTANT_BUDGETAIRE_KEY)) != 0)
				return false;
		}

		// verification des pourcentages
		if (new BigDecimal(100.0).compareTo(computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_POURCENTAGE_KEY)) != 0)
			return false;
		if (new BigDecimal(100.0).compareTo(computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_POURCENTAGE_KEY)) != 0)
			return false;

		return true;
	}

	public EOCommandeBudget creer(EOEditingContext ed) throws CommandeBudgetException {
		EOCommandeBudget newCommandeBudget = (EOCommandeBudget) Factory.instanceForEntity(ed, EOCommandeBudget.ENTITY_NAME);
		ed.insertObject(newCommandeBudget);
		return newCommandeBudget;
	}

	public EOCommandeControleConvention containsConvention(EOConvention convention) {
		NSArray<EOCommandeControleConvention> commandeControleConventions = commandeControleConventions();

		if (convention == null)
			return null;

		if (commandeControleConventions != null && commandeControleConventions.count() > 0) {
			EOQualifier qualSources = EOQualifier.qualifierWithQualifierFormat("convention=%@", new NSArray<Object>(convention));
			NSArray<EOCommandeControleConvention> array = EOQualifier.filteredArrayWithQualifier(commandeControleConventions, qualSources);

			if (array != null && array.count() > 0)
				return (EOCommandeControleConvention) array.objectAtIndex(0);
		}

		return null;
	}

	protected void corrigerMontant() {
		// correction des controleurs
		corrigerMontantActions();
		corrigerMontantAnalytiques();
		corrigerMontantConventions();
		corrigerMontantHorsMarches();
		corrigerMontantMarches();
		corrigerMontantPlanComptables();
	}

	protected void corrigerMontantActions() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (commandeControleActions() == null || commandeControleActions().count() == 0)
			return;

		htReste = cbudHtSaisie();
		ttcReste = cbudTtcSaisie();
		budgetaireReste = cbudMontantBudgetaire();
		sommePourcentage = computeSumForKey(commandeControleActions(), EOCommandeControleAction.CACT_POURCENTAGE_KEY);

		for (int i = 0; i < commandeControleActions().count(); i++) {
			EOCommandeControleAction commande = (EOCommandeControleAction) commandeControleActions().objectAtIndex(i);

			boolean reste = (i == commandeControleActions().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, commande.cactPourcentage(), cbudHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, commande.cactPourcentage(), cbudTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, commande.cactPourcentage(), cbudMontantBudgetaire(), reste);

			// on met a jour les montants
			if (commande.cactHtSaisie() == null || commande.cactHtSaisie().floatValue() != ht.floatValue())
				commande.setCactHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (commande.cactTvaSaisie() == null || commande.cactTvaSaisie().floatValue() != tva.floatValue())
				commande.setCactTvaSaisie(tva);
			if (commande.cactTtcSaisie() == null || commande.cactTtcSaisie().floatValue() != ttc.floatValue())
				commande.setCactTtcSaisie(ttc);
			if (commande.cactMontantBudgetaire() == null || commande.cactMontantBudgetaire().floatValue() != budgetaire.floatValue())
				commande.setCactMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantAnalytiques() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (commandeControleAnalytiques() == null || commandeControleAnalytiques().count() == 0)
			return;

		htReste = cbudHtSaisie();
		ttcReste = cbudTtcSaisie();
		budgetaireReste = cbudMontantBudgetaire();
		sommePourcentage = computeSumForKey(commandeControleAnalytiques(), EOCommandeControleAnalytique.CANA_POURCENTAGE_KEY);

		for (int i = 0; i < commandeControleAnalytiques().count(); i++) {
			EOCommandeControleAnalytique commande = (EOCommandeControleAnalytique) commandeControleAnalytiques().objectAtIndex(i);

			boolean reste = (i == commandeControleAnalytiques().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, commande.canaPourcentage(), cbudHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, commande.canaPourcentage(), cbudTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, commande.canaPourcentage(), cbudMontantBudgetaire(), reste);

			// on met a jour les montants
			if (commande.canaHtSaisie() == null || commande.canaHtSaisie().floatValue() != ht.floatValue())
				commande.setCanaHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (commande.canaTvaSaisie() == null || commande.canaTvaSaisie().floatValue() != tva.floatValue())
				commande.setCanaTvaSaisie(tva);
			if (commande.canaTtcSaisie() == null || commande.canaTtcSaisie().floatValue() != ttc.floatValue())
				commande.setCanaTtcSaisie(ttc);
			if (commande.canaMontantBudgetaire() == null || commande.canaMontantBudgetaire().floatValue() != budgetaire.floatValue())
				commande.setCanaMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantConventions() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (commandeControleConventions() == null || commandeControleConventions().count() == 0)
			return;

		htReste = cbudHtSaisie();
		ttcReste = cbudTtcSaisie();
		budgetaireReste = cbudMontantBudgetaire();
		sommePourcentage = computeSumForKey(commandeControleConventions(), EOCommandeControleConvention.CCON_POURCENTAGE_KEY);

		for (int i = 0; i < commandeControleConventions().count(); i++) {
			EOCommandeControleConvention commande = (EOCommandeControleConvention) commandeControleConventions().objectAtIndex(i);

			boolean reste = (i == commandeControleConventions().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, commande.cconPourcentage(), cbudHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, commande.cconPourcentage(), cbudTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, commande.cconPourcentage(), cbudMontantBudgetaire(), reste);

			// on met a jour les montants
			if (commande.cconHtSaisie() == null || commande.cconHtSaisie().floatValue() != ht.floatValue())
				commande.setCconHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (commande.cconTvaSaisie() == null || commande.cconTvaSaisie().floatValue() != tva.floatValue())
				commande.setCconTvaSaisie(tva);
			if (commande.cconTtcSaisie() == null || commande.cconTtcSaisie().floatValue() != ttc.floatValue())
				commande.setCconTtcSaisie(ttc);
			if (commande.cconMontantBudgetaire() == null || commande.cconMontantBudgetaire().floatValue() != budgetaire.floatValue())
				commande.setCconMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantHorsMarches() {
		if (tauxProrata() == null)
			return;
		if (commandeControleHorsMarches() == null || commandeControleHorsMarches().count() == 0)
			return;
		if (cbudHtSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(), EOCommandeControleHorsMarche.CHOM_HT_SAISIE_KEY)) == 0
				&& cbudTtcSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(),
						EOCommandeControleHorsMarche.CHOM_TTC_SAISIE_KEY)) == 0
				&& cbudMontantBudgetaire().compareTo(computeSumForKey(commandeControleHorsMarches(),
						EOCommandeControleHorsMarche.CHOM_MONTANT_BUDGETAIRE_KEY)) == 0
				&& cbudTvaSaisie().compareTo(computeSumForKey(commandeControleHorsMarches(),
						EOCommandeControleHorsMarche.CHOM_TVA_SAISIE_KEY)) == 0)
			return;

		BigDecimal budgetaireReste, budgetaire;

		budgetaireReste = cbudMontantBudgetaire();

		System.out.println("\n\n\n\n\n\ncorriger\n\n");
		System.out.println("budgetaireReste" + budgetaireReste);

		// pour les montants hors marches on a pas de pourcentage, donc on corrige que le budgetaire
		for (int i = 0; i < commandeControleHorsMarches().count(); i++) {
			System.out.println("budgetaireReste" + budgetaireReste);
			EOCommandeControleHorsMarche commande = (EOCommandeControleHorsMarche) commandeControleHorsMarches().objectAtIndex(i);

			// a priori ya plus besoin de cette ligne 
			//commande.setChomTvaSaisie(commande.chomTtcSaisie().subtract(commande.chomHtSaisie()));

			if (i == commandeControleHorsMarches().count() - 1)
				budgetaire = budgetaireReste;
			else {
				budgetaire = serviceCalcul.calculMontantBudgetaire(commande.chomHtSaisie(), commande.chomTvaSaisie(), tauxProrata().tapTaux());
				// on verifie que le montant calcule ne depasse pas le reste
				if (budgetaire.compareTo(budgetaireReste) == 1)
					budgetaire = budgetaireReste;
			}

			System.out.println("budgetaire" + budgetaire);

			commande.setChomMontantBudgetaire(budgetaire);
			budgetaireReste = budgetaireReste.subtract(budgetaire);

			System.out.println("-->budgetaireReste" + budgetaireReste);
		}
	}

	@Deprecated
	protected void corrigerMontantMarches() {
	}

	protected void corrigerMontantPlanComptables() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (commandeControlePlanComptables() == null || commandeControlePlanComptables().count() == 0)
			return;

		htReste = cbudHtSaisie();
		ttcReste = cbudTtcSaisie();
		budgetaireReste = cbudMontantBudgetaire();
		sommePourcentage = computeSumForKey(commandeControlePlanComptables(), EOCommandeControlePlanComptable.CPCO_POURCENTAGE_KEY);

		for (int i = 0; i < commandeControlePlanComptables().count(); i++) {
			EOCommandeControlePlanComptable commande = (EOCommandeControlePlanComptable) commandeControlePlanComptables().objectAtIndex(i);

			boolean reste = (i == commandeControlePlanComptables().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, commande.cpcoPourcentage(), cbudHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, commande.cpcoPourcentage(), cbudTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, commande.cpcoPourcentage(), cbudMontantBudgetaire(), reste);

			// on met a jour les montants
			if (commande.cpcoHtSaisie() == null || commande.cpcoHtSaisie().floatValue() != ht.floatValue())
				commande.setCpcoHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (commande.cpcoTvaSaisie() == null || commande.cpcoTvaSaisie().floatValue() != tva.floatValue())
				commande.setCpcoTvaSaisie(tva);
			if (commande.cpcoTtcSaisie() == null || commande.cpcoTtcSaisie().floatValue() != ttc.floatValue())
				commande.setCpcoTtcSaisie(ttc);
			if (commande.cpcoMontantBudgetaire() == null || commande.cpcoMontantBudgetaire().floatValue() != budgetaire.floatValue())
				commande.setCpcoMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	private BigDecimal calculeMontant(BigDecimal sommePourcentage, BigDecimal montantReste, BigDecimal pourcentage, BigDecimal montant, boolean reste) {
		BigDecimal calcul;

		// si c'est le dernier et que le pourcentage est egal a 100 -> on met le reste 
		if (sommePourcentage.floatValue() >= 100.0 && reste)
			return montantReste;

		// on calcule le montant par rapport au pourcentage
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();

		calcul = pourcentage.multiply(montant).divide(new BigDecimal(100.0), arrondi, BigDecimal.ROUND_HALF_UP);

		// on verifie que le montant calcule ne depasse pas le reste
		if (calcul.compareTo(montantReste) == 1)
			calcul = montantReste;

		return calcul;
	}
}
