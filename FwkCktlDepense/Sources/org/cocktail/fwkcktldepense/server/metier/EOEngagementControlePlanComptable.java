

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControlePlanComptableException;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;



public class EOEngagementControlePlanComptable extends _EOEngagementControlePlanComptable
{
	private BigDecimal pourcentage;
	public static String EPCO_POURCENTAGE="pourcentage";

    public EOEngagementControlePlanComptable() {
        super();
        setPourcentage(new BigDecimal(0.0));
    }

    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

    	if (engagementBudget()!=null && engagementBudget().engTtcSaisie()!=null && engagementBudget().engTtcSaisie().floatValue()!=0.0)
    		setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(engagementBudget().engTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
    	pourcentage=aValue;

    	if (engagementBudget()!=null) {
    		NSArray engageControlePlancos = engagementBudget().engagementControlePlanComptables();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControlePlanComptable.PLAN_COMPTABLE_KEY+" != nil", null);
    		engageControlePlancos = EOQualifier.filteredArrayWithQualifier(engageControlePlancos, qual);
    		BigDecimal total=engagementBudget().computeSumForKey(engageControlePlancos, EOEngagementControlePlanComptable.EPCO_POURCENTAGE);
    		if (total.floatValue()>100.0)
    	        pourcentage=new BigDecimal(100.0).subtract(total.subtract(aValue));    			
    	}

        if (engagementBudget()!=null)
        	engagementBudget().corrigerMontantActions();
    }

    public BigDecimal pourcentage() {
    	return pourcentage;
    }

    public void setEpcoHtSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEpcoHtSaisie(aValue);
    }

    public void setEpcoTvaSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEpcoTvaSaisie(aValue);
    }

    public void setEpcoTtcSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEpcoTtcSaisie(aValue);
    }

    public void setEpcoMontantBudgetaire(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEpcoMontantBudgetaire(aValue);
    }

    public void setEpcoMontantBudgetaireReste(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEpcoMontantBudgetaireReste(aValue);
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        if (epcoHtSaisie()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoHtSaisieManquant);
        if (epcoTvaSaisie()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoTvaSaisieManquant);
        if (epcoTtcSaisie()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoTtcSaisieManquant);
        if (epcoMontantBudgetaire()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoMontantBudgetaireManquant);
        if (epcoMontantBudgetaireReste()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoMontantBudgetaireResteManquant);
        if (epcoDateSaisie()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoDateSaisieManquant);
        if (planComptable()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.planComptableManquant);
        if (exercice()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.exerciceManquant);
        if (engagementBudget()==null)
            throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.engagementBudgetManquant);
        
        if (!epcoHtSaisie().abs().add(epcoTvaSaisie().abs()).equals(epcoTtcSaisie().abs()))
        	setEpcoTvaSaisie(epcoTtcSaisie().subtract(epcoHtSaisie()));
//        if (!epcoHtSaisie().abs().add(epcoTvaSaisie().abs()).equals(epcoTtcSaisie().abs()))
//        	throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoTtcSaisiePasCoherent);
//        if (!epcoMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(epcoHtSaisie(), epcoTvaSaisie())))
//        	setEpcoMontantBudgetaire(engagementBudget().tauxProrata().montantBudgetaire(epcoHtSaisie(), epcoTvaSaisie()));
//        if (!epcoMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(epcoHtSaisie(), epcoTvaSaisie())))
//        	throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.epcoMontantBudgetairePasCoherent);

        if (epcoTvaSaisie().floatValue()<0.0 || epcoHtSaisie().floatValue()<0.0 || epcoTtcSaisie().floatValue()<0.0 ||
        		epcoMontantBudgetaire().floatValue()<0.0 || epcoMontantBudgetaireReste().floatValue()<0.0)
        	throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.montantsNegatifs);
        
        if (!engagementBudget().exercice().equals(exercice()))
        	throw new EngagementControlePlanComptableException(EngagementControlePlanComptableException.engageBudgetExercicePasCoherent);
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    }
}
