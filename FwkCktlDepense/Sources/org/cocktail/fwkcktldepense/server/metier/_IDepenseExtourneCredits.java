package org.cocktail.fwkcktldepense.server.metier;

import er.extensions.eof.ERXKey;

public interface _IDepenseExtourneCredits extends _ISourceCredit {

	public static final String VEC_MONTANT_BUD_CONSOMME_KEY = "vecMontantBudConsomme";
	public static final String VEC_MONTANT_BUD_DISPONIBLE_KEY = "vecMontantBudDisponible";
	public static final String VEC_MONTANT_BUD_INITIAL_KEY = "vecMontantBudInitial";
	public static final String VEC_MONTANT_BUD_DISPO_REEL_KEY = "vecMontantBudDispoReel";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";

	//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_CONSOMME = new ERXKey<java.math.BigDecimal>("vecMontantBudConsomme");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_DISPONIBLE = new ERXKey<java.math.BigDecimal>("vecMontantBudDisponible");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_INITIAL = new ERXKey<java.math.BigDecimal>("vecMontantBudInitial");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_DISPO_REEL = new ERXKey<java.math.BigDecimal>("vecMontantBudDispoReel");

	// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> TO_ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("toOrgan");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TO_TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("toTypeCredit");

	public java.math.BigDecimal vecMontantBudConsomme();

	public void setVecMontantBudConsomme(java.math.BigDecimal value);

	public java.math.BigDecimal vecMontantBudDisponible();

	public void setVecMontantBudDisponible(java.math.BigDecimal value);

	public java.math.BigDecimal vecMontantBudDispoReel();

	public java.math.BigDecimal vecMontantBudInitial();

	public void setVecMontantBudInitial(java.math.BigDecimal value);

	public org.cocktail.fwkcktldepense.server.metier.EOExercice toExercice();

	public void setToExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value);

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan toOrgan();

	public void setToOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value);

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit toTypeCredit();

	public void setToTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value);

	public java.math.BigDecimal vecMontantHt();

	public void setVecMontantHt(java.math.BigDecimal value);

	public java.math.BigDecimal vecMontantTtc();

	public String libelleOrgan();
}
