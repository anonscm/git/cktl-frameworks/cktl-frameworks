/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEngagementBudget.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEngagementBudget extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_BUDGET";


//Attribute Keys
	public static final ERXKey<NSTimestamp> ENG_DATE_SAISIE = new ERXKey<NSTimestamp>("engDateSaisie");
	public static final ERXKey<java.math.BigDecimal> ENG_HT_SAISIE = new ERXKey<java.math.BigDecimal>("engHtSaisie");
	public static final ERXKey<String> ENG_LIBELLE = new ERXKey<String>("engLibelle");
	public static final ERXKey<java.math.BigDecimal> ENG_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("engMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> ENG_MONTANT_BUDGETAIRE_RESTE = new ERXKey<java.math.BigDecimal>("engMontantBudgetaireReste");
	public static final ERXKey<Integer> ENG_NUMERO = new ERXKey<Integer>("engNumero");
	public static final ERXKey<java.math.BigDecimal> ENG_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("engTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> ENG_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("engTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> COMMANDE_ENGAGEMENTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>("commandeEngagements");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> DEPENSE_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("depenseBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> ENGAGEMENT_CONTROLE_ACTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction>("engagementControleActions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> ENGAGEMENT_CONTROLE_ANALYTIQUES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique>("engagementControleAnalytiques");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> ENGAGEMENT_CONTROLE_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>("engagementControleConventions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> ENGAGEMENT_CONTROLE_HORS_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche>("engagementControleHorsMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> ENGAGEMENT_CONTROLE_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche>("engagementControleMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> ENGAGEMENT_CONTROLE_PLAN_COMPTABLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable>("engagementControlePlanComptables");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("fournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organ");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> PRE_DEPENSE_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>("preDepenseBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata> TAUX_PRORATA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata>("tauxProrata");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeApplication> TYPE_APPLICATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeApplication>("typeApplication");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("typeCredit");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "engId";

	public static final String ENG_DATE_SAISIE_KEY = "engDateSaisie";
	public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
	public static final String ENG_LIBELLE_KEY = "engLibelle";
	public static final String ENG_MONTANT_BUDGETAIRE_KEY = "engMontantBudgetaire";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_KEY = "engMontantBudgetaireReste";
	public static final String ENG_NUMERO_KEY = "engNumero";
	public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
	public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";

//Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ENG_DATE_SAISIE_COLKEY = "ENG_DATE_SAISIE";
	public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
	public static final String ENG_LIBELLE_COLKEY = "ENG_LIBELLE";
	public static final String ENG_MONTANT_BUDGETAIRE_COLKEY = "ENG_MONTANT_BUDGETAIRE";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ENG_MONTANT_BUDGETAIRE_RESTE";
	public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
	public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
	public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String COMMANDE_ENGAGEMENTS_KEY = "commandeEngagements";
	public static final String DEPENSE_BUDGETS_KEY = "depenseBudgets";
	public static final String ENGAGEMENT_CONTROLE_ACTIONS_KEY = "engagementControleActions";
	public static final String ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY = "engagementControleAnalytiques";
	public static final String ENGAGEMENT_CONTROLE_CONVENTIONS_KEY = "engagementControleConventions";
	public static final String ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY = "engagementControleHorsMarches";
	public static final String ENGAGEMENT_CONTROLE_MARCHES_KEY = "engagementControleMarches";
	public static final String ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY = "engagementControlePlanComptables";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String ORGAN_KEY = "organ";
	public static final String PRE_DEPENSE_BUDGETS_KEY = "preDepenseBudgets";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public NSTimestamp engDateSaisie() {
	 return (NSTimestamp) storedValueForKey(ENG_DATE_SAISIE_KEY);
	}

	public void setEngDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, ENG_DATE_SAISIE_KEY);
	}

	public java.math.BigDecimal engHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(ENG_HT_SAISIE_KEY);
	}

	public void setEngHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ENG_HT_SAISIE_KEY);
	}

	public String engLibelle() {
	 return (String) storedValueForKey(ENG_LIBELLE_KEY);
	}

	public void setEngLibelle(String value) {
	 takeStoredValueForKey(value, ENG_LIBELLE_KEY);
	}

	public java.math.BigDecimal engMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_KEY);
	}

	public void setEngMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal engMontantBudgetaireReste() {
	 return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public void setEngMontantBudgetaireReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public Integer engNumero() {
	 return (Integer) storedValueForKey(ENG_NUMERO_KEY);
	}

	public void setEngNumero(Integer value) {
	 takeStoredValueForKey(value, ENG_NUMERO_KEY);
	}

	public java.math.BigDecimal engTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(ENG_TTC_SAISIE_KEY);
	}

	public void setEngTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ENG_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal engTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(ENG_TVA_SAISIE_KEY);
	}

	public void setEngTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ENG_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = fournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organ() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
	}

	public void setOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organ();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
	}

	public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.server.metier.EOTauxProrata value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTauxProrata oldValue = tauxProrata();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplication() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
	}

	public void setTypeApplicationRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeApplication value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeApplication oldValue = typeApplication();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
	}

	public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = typeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>)storedValueForKey(COMMANDE_ENGAGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements(EOQualifier qualifier) {
	 return commandeEngagements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements(EOQualifier qualifier, boolean fetch) {
	 return commandeEngagements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeEngagements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
	}
	
	public void removeFromCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement createCommandeEngagementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_ENGAGEMENTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement) eo;
	}
	
	public void deleteCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeEngagementsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> objects = commandeEngagements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeEngagementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)storedValueForKey(DEPENSE_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets(EOQualifier qualifier) {
	 return depenseBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets(EOQualifier qualifier, boolean fetch) {
	 return depenseBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
	}
	
	public void removeFromDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget createDepenseBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget) eo;
	}
	
	public void deleteDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> objects = depenseBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseBudgetsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> engagementControleActions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction>)storedValueForKey(ENGAGEMENT_CONTROLE_ACTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> engagementControleActions(EOQualifier qualifier) {
	 return engagementControleActions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> engagementControleActions(EOQualifier qualifier, boolean fetch) {
	 return engagementControleActions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> engagementControleActions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = engagementControleActions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEngagementControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
	}
	
	public void removeFromEngagementControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction createEngagementControleActionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction) eo;
	}
	
	public void deleteEngagementControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEngagementControleActionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction> objects = engagementControleActions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEngagementControleActionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> engagementControleAnalytiques() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique>)storedValueForKey(ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> engagementControleAnalytiques(EOQualifier qualifier) {
	 return engagementControleAnalytiques(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> engagementControleAnalytiques(EOQualifier qualifier, boolean fetch) {
	 return engagementControleAnalytiques(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> engagementControleAnalytiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = engagementControleAnalytiques();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEngagementControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public void removeFromEngagementControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique createEngagementControleAnalytiquesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique) eo;
	}
	
	public void deleteEngagementControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEngagementControleAnalytiquesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> objects = engagementControleAnalytiques().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEngagementControleAnalytiquesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>)storedValueForKey(ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions(EOQualifier qualifier) {
	 return engagementControleConventions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions(EOQualifier qualifier, boolean fetch) {
	 return engagementControleConventions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = engagementControleConventions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
	}
	
	public void removeFromEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention createEngagementControleConventionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention) eo;
	}
	
	public void deleteEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEngagementControleConventionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> objects = engagementControleConventions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEngagementControleConventionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> engagementControleHorsMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche>)storedValueForKey(ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> engagementControleHorsMarches(EOQualifier qualifier) {
	 return engagementControleHorsMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> engagementControleHorsMarches(EOQualifier qualifier, boolean fetch) {
	 return engagementControleHorsMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> engagementControleHorsMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = engagementControleHorsMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEngagementControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public void removeFromEngagementControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche createEngagementControleHorsMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche) eo;
	}
	
	public void deleteEngagementControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEngagementControleHorsMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> objects = engagementControleHorsMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEngagementControleHorsMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> engagementControleMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche>)storedValueForKey(ENGAGEMENT_CONTROLE_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> engagementControleMarches(EOQualifier qualifier) {
	 return engagementControleMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> engagementControleMarches(EOQualifier qualifier, boolean fetch) {
	 return engagementControleMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> engagementControleMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = engagementControleMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEngagementControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_MARCHES_KEY);
	}
	
	public void removeFromEngagementControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche createEngagementControleMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche) eo;
	}
	
	public void deleteEngagementControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEngagementControleMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> objects = engagementControleMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEngagementControleMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> engagementControlePlanComptables() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable>)storedValueForKey(ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> engagementControlePlanComptables(EOQualifier qualifier) {
	 return engagementControlePlanComptables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> engagementControlePlanComptables(EOQualifier qualifier, boolean fetch) {
	 return engagementControlePlanComptables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> engagementControlePlanComptables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = engagementControlePlanComptables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEngagementControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public void removeFromEngagementControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable createEngagementControlePlanComptablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable) eo;
	}
	
	public void deleteEngagementControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEngagementControlePlanComptablesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> objects = engagementControlePlanComptables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEngagementControlePlanComptablesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)storedValueForKey(PRE_DEPENSE_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets(EOQualifier qualifier) {
	 return preDepenseBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets(EOQualifier qualifier, boolean fetch) {
	 return preDepenseBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
	}
	
	public void removeFromPreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget createPreDepenseBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget) eo;
	}
	
	public void deletePreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> objects = preDepenseBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseBudgetsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOEngagementBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEngagementBudget createEOEngagementBudget(EOEditingContext editingContext				, NSTimestamp engDateSaisie
							, java.math.BigDecimal engHtSaisie
							, String engLibelle
							, java.math.BigDecimal engMontantBudgetaire
							, java.math.BigDecimal engMontantBudgetaireReste
									, java.math.BigDecimal engTtcSaisie
							, java.math.BigDecimal engTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur		, org.cocktail.fwkcktldepense.server.metier.EOOrgan organ		, org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata		, org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplication		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit		, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EOEngagementBudget eo = (EOEngagementBudget) EOUtilities.createAndInsertInstance(editingContext, _EOEngagementBudget.ENTITY_NAME);	 
							eo.setEngDateSaisie(engDateSaisie);
									eo.setEngHtSaisie(engHtSaisie);
									eo.setEngLibelle(engLibelle);
									eo.setEngMontantBudgetaire(engMontantBudgetaire);
									eo.setEngMontantBudgetaireReste(engMontantBudgetaireReste);
											eo.setEngTtcSaisie(engTtcSaisie);
									eo.setEngTvaSaisie(engTvaSaisie);
						 eo.setExerciceRelationship(exercice);
				 eo.setFournisseurRelationship(fournisseur);
				 eo.setOrganRelationship(organ);
				 eo.setTauxProrataRelationship(tauxProrata);
				 eo.setTypeApplicationRelationship(typeApplication);
				 eo.setTypeCreditRelationship(typeCredit);
				 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngagementBudget creerInstance(EOEditingContext editingContext) {
		EOEngagementBudget object = (EOEngagementBudget)EOUtilities.createAndInsertInstance(editingContext, _EOEngagementBudget.ENTITY_NAME);
  		return object;
		}

	

  public EOEngagementBudget localInstanceIn(EOEditingContext editingContext) {
    EOEngagementBudget localInstance = (EOEngagementBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngagementBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngagementBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEngagementBudget> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEngagementBudget> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngagementBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> objectsForRecherche(EOEditingContext ec,
														String attLibelleBinding,
														String cmCodeBinding,
														String cmLibBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur createurBinding,
														NSTimestamp dateDebutBinding,
														NSTimestamp dateFinBinding,
														String dppNumeroFactureBinding,
														String engLibelleBinding,
														Integer engNumeroBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														String fouCodeBinding,
														String fouNomBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														String marLibelleBinding,
														java.math.BigDecimal maxHTBinding,
														java.math.BigDecimal maxTTCBinding,
														java.math.BigDecimal minHTBinding,
														java.math.BigDecimal minTTCBinding,
														String organCrBinding,
														String organSsCrBinding,
														String organUbBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplicationBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOEngagementBudget.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (attLibelleBinding != null)
			bindings.takeValueForKey(attLibelleBinding, "attLibelle");
		  if (cmCodeBinding != null)
			bindings.takeValueForKey(cmCodeBinding, "cmCode");
		  if (cmLibBinding != null)
			bindings.takeValueForKey(cmLibBinding, "cmLib");
		  if (createurBinding != null)
			bindings.takeValueForKey(createurBinding, "createur");
		  if (dateDebutBinding != null)
			bindings.takeValueForKey(dateDebutBinding, "dateDebut");
		  if (dateFinBinding != null)
			bindings.takeValueForKey(dateFinBinding, "dateFin");
		  if (dppNumeroFactureBinding != null)
			bindings.takeValueForKey(dppNumeroFactureBinding, "dppNumeroFacture");
		  if (engLibelleBinding != null)
			bindings.takeValueForKey(engLibelleBinding, "engLibelle");
		  if (engNumeroBinding != null)
			bindings.takeValueForKey(engNumeroBinding, "engNumero");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (marLibelleBinding != null)
			bindings.takeValueForKey(marLibelleBinding, "marLibelle");
		  if (maxHTBinding != null)
			bindings.takeValueForKey(maxHTBinding, "maxHT");
		  if (maxTTCBinding != null)
			bindings.takeValueForKey(maxTTCBinding, "maxTTC");
		  if (minHTBinding != null)
			bindings.takeValueForKey(minHTBinding, "minHT");
		  if (minTTCBinding != null)
			bindings.takeValueForKey(minTTCBinding, "minTTC");
		  if (organCrBinding != null)
			bindings.takeValueForKey(organCrBinding, "organCr");
		  if (organSsCrBinding != null)
			bindings.takeValueForKey(organSsCrBinding, "organSsCr");
		  if (organUbBinding != null)
			bindings.takeValueForKey(organUbBinding, "organUb");
		  if (typeApplicationBinding != null)
			bindings.takeValueForKey(typeApplicationBinding, "typeApplication");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
