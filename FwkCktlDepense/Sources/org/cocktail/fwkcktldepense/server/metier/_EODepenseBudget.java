/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EODepenseBudget.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EODepenseBudget extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleDepenseBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_BUDGET";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> DEP_HT_SAISIE = new ERXKey<java.math.BigDecimal>("depHtSaisie");
	public static final ERXKey<java.math.BigDecimal> DEP_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("depMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> DEP_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("depTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DEP_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("depTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> DEPENSE_BUDGET_REVERSEMENT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("depenseBudgetReversement");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> DEPENSE_CONTROLE_ACTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction>("depenseControleActions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> DEPENSE_CONTROLE_ANALYTIQUES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique>("depenseControleAnalytiques");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> DEPENSE_CONTROLE_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>("depenseControleConventions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> DEPENSE_CONTROLE_HORS_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche>("depenseControleHorsMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> DEPENSE_CONTROLE_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche>("depenseControleMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> DEPENSE_CONTROLE_PLAN_COMPTABLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable>("depenseControlePlanComptables");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> DEPENSE_PAPIER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepensePapier>("depensePapier");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation> REIMPUTATIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation>("reimputations");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> REVERSEMENTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("reversements");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata> TAUX_PRORATA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata>("tauxProrata");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> TO_EXTOURNE_LIQ_DEFS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef>("toExtourneLiqDefs");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> TO_EXTOURNE_LIQ_N1S = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>("toExtourneLiqN1s");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> TO_EXTOURNE_LIQ_NS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>("toExtourneLiqNs");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";

//Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DEP_ID_REVERSEMENT_KEY = "depIdReversement";
	public static final String DPP_ID_KEY = "dppId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TAP_ID_KEY = "tapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_HT_SAISIE_COLKEY = "DEP_HT_SAISIE";
	public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "DEP_MONTANT_BUDGETAIRE";
	public static final String DEP_TTC_SAISIE_COLKEY = "DEP_TTC_SAISIE";
	public static final String DEP_TVA_SAISIE_COLKEY = "DEP_TVA_SAISIE";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DEP_ID_REVERSEMENT_COLKEY = "DEP_ID_REVERSEMENT";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_BUDGET_REVERSEMENT_KEY = "depenseBudgetReversement";
	public static final String DEPENSE_CONTROLE_ACTIONS_KEY = "depenseControleActions";
	public static final String DEPENSE_CONTROLE_ANALYTIQUES_KEY = "depenseControleAnalytiques";
	public static final String DEPENSE_CONTROLE_CONVENTIONS_KEY = "depenseControleConventions";
	public static final String DEPENSE_CONTROLE_HORS_MARCHES_KEY = "depenseControleHorsMarches";
	public static final String DEPENSE_CONTROLE_MARCHES_KEY = "depenseControleMarches";
	public static final String DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY = "depenseControlePlanComptables";
	public static final String DEPENSE_PAPIER_KEY = "depensePapier";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String REIMPUTATIONS_KEY = "reimputations";
	public static final String REVERSEMENTS_KEY = "reversements";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TO_EXTOURNE_LIQ_DEFS_KEY = "toExtourneLiqDefs";
	public static final String TO_EXTOURNE_LIQ_N1S_KEY = "toExtourneLiqN1s";
	public static final String TO_EXTOURNE_LIQ_NS_KEY = "toExtourneLiqNs";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public java.math.BigDecimal depHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_HT_SAISIE_KEY);
	}

	public void setDepHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal depMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
	}

	public void setDepMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal depTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_TTC_SAISIE_KEY);
	}

	public void setDepTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal depTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_TVA_SAISIE_KEY);
	}

	public void setDepTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget depenseBudgetReversement() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_REVERSEMENT_KEY);
	}

	public void setDepenseBudgetReversementRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseBudget oldValue = depenseBudgetReversement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_REVERSEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_REVERSEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepensePapier depensePapier() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepensePapier)storedValueForKey(DEPENSE_PAPIER_KEY);
	}

	public void setDepensePapierRelationship(org.cocktail.fwkcktldepense.server.metier.EODepensePapier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepensePapier oldValue = depensePapier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_PAPIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_PAPIER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
	}

	public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.server.metier.EOTauxProrata value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTauxProrata oldValue = tauxProrata();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> depenseControleActions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction>)storedValueForKey(DEPENSE_CONTROLE_ACTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> depenseControleActions(EOQualifier qualifier) {
	 return depenseControleActions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> depenseControleActions(EOQualifier qualifier, boolean fetch) {
	 return depenseControleActions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> depenseControleActions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseControleActions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ACTIONS_KEY);
	}
	
	public void removeFromDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ACTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction createDepenseControleActionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_ACTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction) eo;
	}
	
	public void deleteDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ACTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseControleActionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction> objects = depenseControleActions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseControleActionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> depenseControleAnalytiques() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique>)storedValueForKey(DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> depenseControleAnalytiques(EOQualifier qualifier) {
	 return depenseControleAnalytiques(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> depenseControleAnalytiques(EOQualifier qualifier, boolean fetch) {
	 return depenseControleAnalytiques(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> depenseControleAnalytiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseControleAnalytiques();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public void removeFromDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique createDepenseControleAnalytiquesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique) eo;
	}
	
	public void deleteDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseControleAnalytiquesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique> objects = depenseControleAnalytiques().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseControleAnalytiquesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>)storedValueForKey(DEPENSE_CONTROLE_CONVENTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions(EOQualifier qualifier) {
	 return depenseControleConventions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions(EOQualifier qualifier, boolean fetch) {
	 return depenseControleConventions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseControleConventions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_CONVENTIONS_KEY);
	}
	
	public void removeFromDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_CONVENTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention createDepenseControleConventionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_CONVENTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention) eo;
	}
	
	public void deleteDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_CONVENTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseControleConventionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> objects = depenseControleConventions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseControleConventionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> depenseControleHorsMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche>)storedValueForKey(DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> depenseControleHorsMarches(EOQualifier qualifier) {
	 return depenseControleHorsMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> depenseControleHorsMarches(EOQualifier qualifier, boolean fetch) {
	 return depenseControleHorsMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> depenseControleHorsMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseControleHorsMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public void removeFromDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche createDepenseControleHorsMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche) eo;
	}
	
	public void deleteDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseControleHorsMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche> objects = depenseControleHorsMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseControleHorsMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> depenseControleMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche>)storedValueForKey(DEPENSE_CONTROLE_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> depenseControleMarches(EOQualifier qualifier) {
	 return depenseControleMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> depenseControleMarches(EOQualifier qualifier, boolean fetch) {
	 return depenseControleMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> depenseControleMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseControleMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_MARCHES_KEY);
	}
	
	public void removeFromDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche createDepenseControleMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche) eo;
	}
	
	public void deleteDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseControleMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche> objects = depenseControleMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseControleMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> depenseControlePlanComptables() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable>)storedValueForKey(DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> depenseControlePlanComptables(EOQualifier qualifier) {
	 return depenseControlePlanComptables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> depenseControlePlanComptables(EOQualifier qualifier, boolean fetch) {
	 return depenseControlePlanComptables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> depenseControlePlanComptables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseControlePlanComptables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public void removeFromDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable createDepenseControlePlanComptablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable) eo;
	}
	
	public void deleteDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseControlePlanComptablesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> objects = depenseControlePlanComptables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseControlePlanComptablesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> reimputations() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation>)storedValueForKey(REIMPUTATIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> reimputations(EOQualifier qualifier) {
	 return reimputations(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> reimputations(EOQualifier qualifier, boolean fetch) {
	 return reimputations(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> reimputations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputation.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputation.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputations();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
	}
	
	public void removeFromReimputationsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputation createReimputationsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputation.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputation) eo;
	}
	
	public void deleteReimputationsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputation> objects = reimputations().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> reversements() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)storedValueForKey(REVERSEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> reversements(EOQualifier qualifier) {
	 return reversements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> reversements(EOQualifier qualifier, boolean fetch) {
	 return reversements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> reversements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reversements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReversementsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REVERSEMENTS_KEY);
	}
	
	public void removeFromReversementsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REVERSEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget createReversementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REVERSEMENTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget) eo;
	}
	
	public void deleteReversementsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REVERSEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReversementsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> objects = reversements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReversementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> toExtourneLiqDefs() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef>)storedValueForKey(TO_EXTOURNE_LIQ_DEFS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> toExtourneLiqDefs(EOQualifier qualifier) {
	 return toExtourneLiqDefs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> toExtourneLiqDefs(EOQualifier qualifier, boolean fetch) {
	 return toExtourneLiqDefs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> toExtourneLiqDefs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef.TO_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toExtourneLiqDefs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToExtourneLiqDefsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_DEFS_KEY);
	}
	
	public void removeFromToExtourneLiqDefsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_DEFS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef createToExtourneLiqDefsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_DEFS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef) eo;
	}
	
	public void deleteToExtourneLiqDefsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_DEFS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToExtourneLiqDefsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> objects = toExtourneLiqDefs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToExtourneLiqDefsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqN1s() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)storedValueForKey(TO_EXTOURNE_LIQ_N1S_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqN1s(EOQualifier qualifier) {
	 return toExtourneLiqN1s(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqN1s(EOQualifier qualifier, boolean fetch) {
	 return toExtourneLiqN1s(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqN1s(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq.FWK_CARAMBOLE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toExtourneLiqN1s();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToExtourneLiqN1sRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_N1S_KEY);
	}
	
	public void removeFromToExtourneLiqN1sRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_N1S_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq createToExtourneLiqN1sRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_N1S_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq) eo;
	}
	
	public void deleteToExtourneLiqN1sRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_N1S_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToExtourneLiqN1sRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> objects = toExtourneLiqN1s().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToExtourneLiqN1sRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqNs() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)storedValueForKey(TO_EXTOURNE_LIQ_NS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqNs(EOQualifier qualifier) {
	 return toExtourneLiqNs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqNs(EOQualifier qualifier, boolean fetch) {
	 return toExtourneLiqNs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> toExtourneLiqNs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq.TO_DEPENSE_BUDGET_N_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toExtourneLiqNs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToExtourneLiqNsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_NS_KEY);
	}
	
	public void removeFromToExtourneLiqNsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_NS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq createToExtourneLiqNsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_NS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq) eo;
	}
	
	public void deleteToExtourneLiqNsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_NS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToExtourneLiqNsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> objects = toExtourneLiqNs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToExtourneLiqNsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EODepenseBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EODepenseBudget createEODepenseBudget(EOEditingContext editingContext				, java.math.BigDecimal depHtSaisie
							, java.math.BigDecimal depMontantBudgetaire
							, java.math.BigDecimal depTtcSaisie
							, java.math.BigDecimal depTvaSaisie
							, org.cocktail.fwkcktldepense.server.metier.EODepensePapier depensePapier		, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata		, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EODepenseBudget eo = (EODepenseBudget) EOUtilities.createAndInsertInstance(editingContext, _EODepenseBudget.ENTITY_NAME);	 
							eo.setDepHtSaisie(depHtSaisie);
									eo.setDepMontantBudgetaire(depMontantBudgetaire);
									eo.setDepTtcSaisie(depTtcSaisie);
									eo.setDepTvaSaisie(depTvaSaisie);
								 eo.setDepensePapierRelationship(depensePapier);
				 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setTauxProrataRelationship(tauxProrata);
				 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepenseBudget creerInstance(EOEditingContext editingContext) {
		EODepenseBudget object = (EODepenseBudget)EOUtilities.createAndInsertInstance(editingContext, _EODepenseBudget.ENTITY_NAME);
  		return object;
		}

	

  public EODepenseBudget localInstanceIn(EOEditingContext editingContext) {
    EODepenseBudget localInstance = (EODepenseBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepenseBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepenseBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EODepenseBudget> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EODepenseBudget> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepenseBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepenseBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepenseBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepenseBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
