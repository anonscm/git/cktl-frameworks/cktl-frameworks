/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeBudget.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeBudget extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_BUDGET";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> CBUD_HT_SAISIE = new ERXKey<java.math.BigDecimal>("cbudHtSaisie");
	public static final ERXKey<java.math.BigDecimal> CBUD_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("cbudMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> CBUD_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("cbudTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> CBUD_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("cbudTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commande");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> COMMANDE_CONTROLE_ACTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction>("commandeControleActions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> COMMANDE_CONTROLE_ANALYTIQUES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique>("commandeControleAnalytiques");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> COMMANDE_CONTROLE_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention>("commandeControleConventions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> COMMANDE_CONTROLE_HORS_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche>("commandeControleHorsMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> COMMANDE_CONTROLE_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche>("commandeControleMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> COMMANDE_CONTROLE_PLAN_COMPTABLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable>("commandeControlePlanComptables");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organ");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata> TAUX_PRORATA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata>("tauxProrata");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("typeCredit");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cbudId";

	public static final String CBUD_HT_SAISIE_KEY = "cbudHtSaisie";
	public static final String CBUD_MONTANT_BUDGETAIRE_KEY = "cbudMontantBudgetaire";
	public static final String CBUD_TTC_SAISIE_KEY = "cbudTtcSaisie";
	public static final String CBUD_TVA_SAISIE_KEY = "cbudTvaSaisie";

//Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String COMM_ID_KEY = "commId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CBUD_HT_SAISIE_COLKEY = "CBUD_HT_SAISIE";
	public static final String CBUD_MONTANT_BUDGETAIRE_COLKEY = "CBUD_MONTANT_BUDGETAIRE";
	public static final String CBUD_TTC_SAISIE_COLKEY = "CBUD_TTC_SAISIE";
	public static final String CBUD_TVA_SAISIE_COLKEY = "CBUD_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String COMMANDE_CONTROLE_ACTIONS_KEY = "commandeControleActions";
	public static final String COMMANDE_CONTROLE_ANALYTIQUES_KEY = "commandeControleAnalytiques";
	public static final String COMMANDE_CONTROLE_CONVENTIONS_KEY = "commandeControleConventions";
	public static final String COMMANDE_CONTROLE_HORS_MARCHES_KEY = "commandeControleHorsMarches";
	public static final String COMMANDE_CONTROLE_MARCHES_KEY = "commandeControleMarches";
	public static final String COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY = "commandeControlePlanComptables";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
	public java.math.BigDecimal cbudHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CBUD_HT_SAISIE_KEY);
	}

	public void setCbudHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CBUD_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal cbudMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(CBUD_MONTANT_BUDGETAIRE_KEY);
	}

	public void setCbudMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CBUD_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal cbudTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CBUD_TTC_SAISIE_KEY);
	}

	public void setCbudTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CBUD_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal cbudTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CBUD_TVA_SAISIE_KEY);
	}

	public void setCbudTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CBUD_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande commande() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
	}

	public void setCommandeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commande();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organ() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
	}

	public void setOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organ();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
	}

	public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.server.metier.EOTauxProrata value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTauxProrata oldValue = tauxProrata();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
	}

	public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = typeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> commandeControleActions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction>)storedValueForKey(COMMANDE_CONTROLE_ACTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> commandeControleActions(EOQualifier qualifier) {
	 return commandeControleActions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> commandeControleActions(EOQualifier qualifier, boolean fetch) {
	 return commandeControleActions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> commandeControleActions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeControleActions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ACTIONS_KEY);
	}
	
	public void removeFromCommandeControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ACTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction createCommandeControleActionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_ACTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction) eo;
	}
	
	public void deleteCommandeControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ACTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeControleActionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction> objects = commandeControleActions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeControleActionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> commandeControleAnalytiques() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique>)storedValueForKey(COMMANDE_CONTROLE_ANALYTIQUES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> commandeControleAnalytiques(EOQualifier qualifier) {
	 return commandeControleAnalytiques(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> commandeControleAnalytiques(EOQualifier qualifier, boolean fetch) {
	 return commandeControleAnalytiques(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> commandeControleAnalytiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeControleAnalytiques();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public void removeFromCommandeControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique createCommandeControleAnalytiquesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique) eo;
	}
	
	public void deleteCommandeControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeControleAnalytiquesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique> objects = commandeControleAnalytiques().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeControleAnalytiquesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> commandeControleConventions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention>)storedValueForKey(COMMANDE_CONTROLE_CONVENTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> commandeControleConventions(EOQualifier qualifier) {
	 return commandeControleConventions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> commandeControleConventions(EOQualifier qualifier, boolean fetch) {
	 return commandeControleConventions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> commandeControleConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeControleConventions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_CONVENTIONS_KEY);
	}
	
	public void removeFromCommandeControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_CONVENTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention createCommandeControleConventionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_CONVENTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention) eo;
	}
	
	public void deleteCommandeControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_CONVENTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeControleConventionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> objects = commandeControleConventions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeControleConventionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> commandeControleHorsMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche>)storedValueForKey(COMMANDE_CONTROLE_HORS_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> commandeControleHorsMarches(EOQualifier qualifier) {
	 return commandeControleHorsMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> commandeControleHorsMarches(EOQualifier qualifier, boolean fetch) {
	 return commandeControleHorsMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> commandeControleHorsMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeControleHorsMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public void removeFromCommandeControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche createCommandeControleHorsMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche) eo;
	}
	
	public void deleteCommandeControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeControleHorsMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> objects = commandeControleHorsMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeControleHorsMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> commandeControleMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche>)storedValueForKey(COMMANDE_CONTROLE_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> commandeControleMarches(EOQualifier qualifier) {
	 return commandeControleMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> commandeControleMarches(EOQualifier qualifier, boolean fetch) {
	 return commandeControleMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> commandeControleMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeControleMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_MARCHES_KEY);
	}
	
	public void removeFromCommandeControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche createCommandeControleMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche) eo;
	}
	
	public void deleteCommandeControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeControleMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche> objects = commandeControleMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeControleMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> commandeControlePlanComptables() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable>)storedValueForKey(COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> commandeControlePlanComptables(EOQualifier qualifier) {
	 return commandeControlePlanComptables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> commandeControlePlanComptables(EOQualifier qualifier, boolean fetch) {
	 return commandeControlePlanComptables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> commandeControlePlanComptables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeControlePlanComptables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public void removeFromCommandeControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable createCommandeControlePlanComptablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable) eo;
	}
	
	public void deleteCommandeControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeControlePlanComptablesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> objects = commandeControlePlanComptables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeControlePlanComptablesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCommandeBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeBudget createEOCommandeBudget(EOEditingContext editingContext				, java.math.BigDecimal cbudHtSaisie
							, java.math.BigDecimal cbudMontantBudgetaire
							, java.math.BigDecimal cbudTtcSaisie
							, java.math.BigDecimal cbudTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCommande commande		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOOrgan organ		, org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit					) {
	 EOCommandeBudget eo = (EOCommandeBudget) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeBudget.ENTITY_NAME);	 
							eo.setCbudHtSaisie(cbudHtSaisie);
									eo.setCbudMontantBudgetaire(cbudMontantBudgetaire);
									eo.setCbudTtcSaisie(cbudTtcSaisie);
									eo.setCbudTvaSaisie(cbudTvaSaisie);
						 eo.setCommandeRelationship(commande);
				 eo.setExerciceRelationship(exercice);
				 eo.setOrganRelationship(organ);
				 eo.setTauxProrataRelationship(tauxProrata);
				 eo.setTypeCreditRelationship(typeCredit);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeBudget creerInstance(EOEditingContext editingContext) {
		EOCommandeBudget object = (EOCommandeBudget)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeBudget.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeBudget localInstanceIn(EOEditingContext editingContext) {
    EOCommandeBudget localInstance = (EOCommandeBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeBudget> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeBudget> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
