/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.finder.FinderInfosReversement;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.util.Calculs;
import org.hamcrest.core.IsInstanceOf;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EODepenseControleAnalytique extends _EODepenseControleAnalytique implements _IDepenseControleAnalytique, IDepenseControle {
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	private BigDecimal pourcentage;
	public static String DANA_POURCENTAGE = "pourcentage";
	private BigDecimal maxReversement;

	public EODepenseControleAnalytique() {
		super();
		setPourcentage(new BigDecimal(0.0));
		maxReversement = new BigDecimal(0.0);
	}
	public void setMontantHT(BigDecimal montant) {
		setDanaHtSaisie(montant);
	}
	public void setMontantTTC(BigDecimal montant) {
		setDanaTtcSaisie(montant);
	}
	public void setMontantTVA(BigDecimal montant) {
		setDanaTvaSaisie(montant);
	}
	public void setMontantBudgetaire(BigDecimal montant) {
		setDanaMontantBudgetaire(montant);
	}
	public BigDecimal getMontantHT() {
		return danaHtSaisie();
	}
	public BigDecimal getMontantTTC() {
		return danaTtcSaisie();
	}
	public BigDecimal getMontantTVA() {
		return danaTvaSaisie();
	}
	public BigDecimal getMontantBudgetaire() {
		return danaMontantBudgetaire();
	}
	public BigDecimal getPourcentage() {
		return pourcentage();
	}

	public BigDecimal getSommeRepartition() {
		return FinderInfosReversement.getSumAnalytique(editingContext(), this);
	}

	public String getLabelCode() {
		return "codeAnalytique";
	}
	
	public Object getCode() {
		return this.codeAnalytique();
	}

	public void setMaxReversement(BigDecimal value) {
		if (value == null)
			value = new BigDecimal(0.0);
		maxReversement = value;
	}

	public BigDecimal maxReversement() {
		if (maxReversement == null)
			setMaxReversement(new BigDecimal(0.0));
		return maxReversement;
	}

	public BigDecimal montantTtc() {
		return danaTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && !depenseBudget().isReversement() && aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && depenseBudget().isReversement() && aValue.floatValue() > 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		if (depenseBudget() != null && depenseBudget().depTtcSaisie() != null && depenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(depenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (depenseBudget() != null) {
			NSArray depenseControleAnalytiques = depenseBudget().depenseControleAnalytiques();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EODepenseControleAnalytique.CODE_ANALYTIQUE_KEY + " != nil", null);
			depenseControleAnalytiques = EOQualifier.filteredArrayWithQualifier(depenseControleAnalytiques, qual);
			BigDecimal total = depenseBudget().computeSumForKey(depenseControleAnalytiques, EODepenseControleAnalytique.DANA_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (depenseBudget() != null)
			depenseBudget().corrigerMontantAnalytiques();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDanaHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDanaHtSaisie(aValue);
	}

	public void setDanaTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDanaTvaSaisie(aValue);
	}

	public void setDanaTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDanaTtcSaisie(aValue);
	}

	public void setDanaMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDanaMontantBudgetaire(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (danaHtSaisie() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.danaHtSaisieManquant);
		if (danaTvaSaisie() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.danaTvaSaisieManquant);
		if (danaTtcSaisie() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.danaTtcSaisieManquant);
		if (danaMontantBudgetaire() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.danaMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.exerciceManquant);
		if (depenseBudget() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.depenseBudgetManquant);

		if (!danaHtSaisie().abs().add(danaTvaSaisie().abs()).equals(danaTtcSaisie().abs()))
			setDanaTvaSaisie(danaTtcSaisie().subtract(danaHtSaisie()));
		if (!danaMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDanaMontantBudgetaire(calculMontantBudgetaire());
		if (!depenseBudget().exercice().equals(exercice()))
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.depenseBudgetExercicePasCoherent);
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(danaHtSaisie(), danaTvaSaisie(),depenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (codeAnalytique() == null)
			throw new DepenseControleAnalytiqueException(DepenseControleAnalytiqueException.codeAnalytiqueManquant);
	}
	public boolean isNull() {
		return false;
	}
}
