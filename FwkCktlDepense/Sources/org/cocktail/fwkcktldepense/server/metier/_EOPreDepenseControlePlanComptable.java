/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPreDepenseControlePlanComptable.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPreDepenseControlePlanComptable extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseControlePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> DPCO_HT_SAISIE = new ERXKey<java.math.BigDecimal>("dpcoHtSaisie");
	public static final ERXKey<java.math.BigDecimal> DPCO_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("dpcoMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> DPCO_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("dpcoTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DPCO_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("dpcoTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail> ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail>("ecritureDetail");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> PRE_DEPENSE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>("preDepenseBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire>("preDepenseControlePlanComptableInventaires");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdpcoId";

	public static final String DPCO_HT_SAISIE_KEY = "dpcoHtSaisie";
	public static final String DPCO_MONTANT_BUDGETAIRE_KEY = "dpcoMontantBudgetaire";
	public static final String DPCO_TTC_SAISIE_KEY = "dpcoTtcSaisie";
	public static final String DPCO_TVA_SAISIE_KEY = "dpcoTvaSaisie";

//Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PDEP_ID_KEY = "pdepId";
	public static final String PDPCO_ID_KEY = "pdpcoId";

//Colonnes dans la base de donnees
	public static final String DPCO_HT_SAISIE_COLKEY = "PDPCO_HT_SAISIE";
	public static final String DPCO_MONTANT_BUDGETAIRE_COLKEY = "PDPCO_MONTANT_BUDGETAIRE";
	public static final String DPCO_TTC_SAISIE_COLKEY = "PDPCO_TTC_SAISIE";
	public static final String DPCO_TVA_SAISIE_COLKEY = "PDPCO_TVA_SAISIE";

	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PDEP_ID_COLKEY = "PDEP_ID";
	public static final String PDPCO_ID_COLKEY = "PDPCO_ID";


	// Relationships
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String PRE_DEPENSE_BUDGET_KEY = "preDepenseBudget";
	public static final String PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES_KEY = "preDepenseControlePlanComptableInventaires";



	// Accessors methods
	public java.math.BigDecimal dpcoHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_HT_SAISIE_KEY);
	}

	public void setDpcoHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal dpcoMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public void setDpcoMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal dpcoTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_TTC_SAISIE_KEY);
	}

	public void setDpcoTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal dpcoTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_TVA_SAISIE_KEY);
	}

	public void setDpcoTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail ecritureDetail() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
	}

	public void setEcritureDetailRelationship(org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail oldValue = ecritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget preDepenseBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget)storedValueForKey(PRE_DEPENSE_BUDGET_KEY);
	}

	public void setPreDepenseBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget oldValue = preDepenseBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRE_DEPENSE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PRE_DEPENSE_BUDGET_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> preDepenseControlePlanComptableInventaires() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire>)storedValueForKey(PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> preDepenseControlePlanComptableInventaires(EOQualifier qualifier) {
	 return preDepenseControlePlanComptableInventaires(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> preDepenseControlePlanComptableInventaires(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControlePlanComptableInventaires(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> preDepenseControlePlanComptableInventaires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire.PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControlePlanComptableInventaires();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControlePlanComptableInventairesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES_KEY);
	}
	
	public void removeFromPreDepenseControlePlanComptableInventairesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire createPreDepenseControlePlanComptableInventairesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire) eo;
	}
	
	public void deletePreDepenseControlePlanComptableInventairesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_INVENTAIRES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControlePlanComptableInventairesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> objects = preDepenseControlePlanComptableInventaires().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControlePlanComptableInventairesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPreDepenseControlePlanComptable avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPreDepenseControlePlanComptable createEOPreDepenseControlePlanComptable(EOEditingContext editingContext				, java.math.BigDecimal dpcoHtSaisie
							, java.math.BigDecimal dpcoMontantBudgetaire
							, java.math.BigDecimal dpcoTtcSaisie
							, java.math.BigDecimal dpcoTvaSaisie
							, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable		, org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget preDepenseBudget					) {
	 EOPreDepenseControlePlanComptable eo = (EOPreDepenseControlePlanComptable) EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseControlePlanComptable.ENTITY_NAME);	 
							eo.setDpcoHtSaisie(dpcoHtSaisie);
									eo.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
									eo.setDpcoTtcSaisie(dpcoTtcSaisie);
									eo.setDpcoTvaSaisie(dpcoTvaSaisie);
								 eo.setExerciceRelationship(exercice);
				 eo.setPlanComptableRelationship(planComptable);
				 eo.setPreDepenseBudgetRelationship(preDepenseBudget);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreDepenseControlePlanComptable creerInstance(EOEditingContext editingContext) {
		EOPreDepenseControlePlanComptable object = (EOPreDepenseControlePlanComptable)EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseControlePlanComptable.ENTITY_NAME);
  		return object;
		}

	

  public EOPreDepenseControlePlanComptable localInstanceIn(EOEditingContext editingContext) {
    EOPreDepenseControlePlanComptable localInstance = (EOPreDepenseControlePlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreDepenseControlePlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreDepenseControlePlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreDepenseControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreDepenseControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreDepenseControlePlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseControlePlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseControlePlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseControlePlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
