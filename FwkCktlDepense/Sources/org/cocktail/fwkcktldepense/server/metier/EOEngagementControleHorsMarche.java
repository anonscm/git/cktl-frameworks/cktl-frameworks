

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleHorsMarcheException;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;




public class EOEngagementControleHorsMarche extends _EOEngagementControleHorsMarche
{
	private BigDecimal pourcentage;
	public static String EHOM_POURCENTAGE="pourcentage";

    public EOEngagementControleHorsMarche() {
        super();
        setPourcentage(new BigDecimal(0.0));
    }

    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

    	if (engagementBudget()!=null && engagementBudget().engTtcSaisie()!=null && engagementBudget().engTtcSaisie().floatValue()!=0.0)
    		setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(engagementBudget().engTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
    	pourcentage=aValue;

    	if (engagementBudget()!=null) {
    		NSArray engageControleHorsMarches = engagementBudget().engagementControleHorsMarches();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControleHorsMarche.CODE_EXER_KEY+" != nil", null);
    		engageControleHorsMarches = EOQualifier.filteredArrayWithQualifier(engageControleHorsMarches, qual);
    		BigDecimal total=engagementBudget().computeSumForKey(engageControleHorsMarches, EOEngagementControleHorsMarche.EHOM_POURCENTAGE);
    		if (total.floatValue()>100.0)
    	        pourcentage=new BigDecimal(100.0).subtract(total.subtract(aValue));    			
    	}

        if (engagementBudget()!=null)
        	engagementBudget().corrigerMontantActions();
    }

    public BigDecimal pourcentage() {
    	return pourcentage;
    }

    public void setEhomHtSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEhomHtSaisie(aValue);
    }
    
    public void setEhomHtReste(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEhomHtReste(aValue);
    }

    public void setEhomTvaSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEhomTvaSaisie(aValue);
    }

    public void setEhomTtcSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEhomTtcSaisie(aValue);
    }

    public void setEhomMontantBudgetaire(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEhomMontantBudgetaire(aValue);
    }

    public void setEhomMontantBudgetaireReste(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEhomMontantBudgetaireReste(aValue);
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        if (ehomHtSaisie()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomHtSaisieManquant);
        if (ehomHtReste()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomHtResteManquant);
        if (ehomTvaSaisie()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomTvaSaisieManquant);
        if (ehomTtcSaisie()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomTtcSaisieManquant);
        if (ehomMontantBudgetaire()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomMontantBudgetaireManquant);
        if (ehomMontantBudgetaireReste()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomMontantBudgetaireResteManquant);
        if (ehomDateSaisie()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomDateSaisieManquant);
        if (codeExer()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.codeExerManquant);
        if (typeAchat()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.typeAchatManquant);
        if (exercice()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.exerciceManquant);
        if (engagementBudget()==null)
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.engagementBudgetManquant);
        
        if (!ehomHtSaisie().abs().add(ehomTvaSaisie().abs()).equals(ehomTtcSaisie().abs()))
        	setEhomTvaSaisie(ehomTtcSaisie().subtract(ehomHtSaisie()));
//        if (!ehomHtSaisie().abs().add(ehomTvaSaisie().abs()).equals(ehomTtcSaisie().abs()))
//        	throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomTtcSaisiePasCoherent);
//        if (!ehomMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(ehomHtSaisie(), ehomTvaSaisie())))
//        	setEhomMontantBudgetaire(engagementBudget().tauxProrata().montantBudgetaire(ehomHtSaisie(), ehomTvaSaisie()));        
//        if (!ehomMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(ehomHtSaisie(), ehomTvaSaisie())))
//        	throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.ehomMontantBudgetairePasCoherent);

        if (ehomTvaSaisie().floatValue()<0.0 || ehomHtSaisie().floatValue()<0.0 || ehomTtcSaisie().floatValue()<0.0 ||
        		ehomMontantBudgetaire().floatValue()<0.0 || ehomMontantBudgetaireReste().floatValue()<0.0)
        	throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.montantsNegatifs);

        if (!engagementBudget().exercice().equals(exercice()))
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.engageBudgetExercicePasCoherent);

        if (codeExer().codeMarche().cmNiveau().intValue()!=EOCodeExer.niveauEngageable())
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.codeExerNiveauPasCoherent);
        if (!codeExer().exercice().equals(exercice()))
            throw new EngagementControleHorsMarcheException(EngagementControleHorsMarcheException.codeExerExercicePasCoherent);
            
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    }
}
