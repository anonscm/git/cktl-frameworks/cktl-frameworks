package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public interface _IDepenseBudget {

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";

	public NSArray<EOTypeAction> getTypeActionsPossibles(EOEditingContext edc, EOUtilisateur utilisateur);

	//public EOSource source();

	public _ISourceCredit getSource();

	public NSArray<EOPlanComptable> getPlanComptablesPossibles(EOEditingContext edc, EOUtilisateur utilisateur);

	public NSArray<EOConvention> getConventionsPossibles(EOEditingContext edc, EOUtilisateur utilisateur);

	public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext edc, EOUtilisateur utilisateur);

	public EOEngagementBudget engagementBudget();

	public EODepensePapier depensePapier();

	public void setTauxProrata(EOTauxProrata unTauxDeProrataSelectionne);

	public EOTauxProrata tauxProrata();

	public BigDecimal depHtSaisie();

	public void setDepHtSaisie(BigDecimal depHtSaisie);

	public BigDecimal depTtcSaisie();

	public BigDecimal depTvaSaisie();

	public void setDepTtcSaisie(BigDecimal depTtcSaisie);

	public NSArray<? extends _IDepenseControleHorsMarche> depenseControleHorsMarches();

	public BigDecimal depMontantBudgetaire();

	public BigDecimal restantTtcControleHorsMarche();

	public NSArray<? extends _IDepenseControleAction> depenseControleActions();

	public BigDecimal restantTtcControleAction();

	public NSArray<? extends _IDepenseControlePlanComptable> depenseControlePlanComptables();

	public BigDecimal restantTtcControlePlanComptable();

	public NSArray<? extends _IDepenseControleConvention> depenseControleConventions();

	public BigDecimal restantTtcControleConvention();

	public NSArray<? extends _IDepenseControleAnalytique> depenseControleAnalytiques();

	public BigDecimal restantTtcControleAnalytique();

	public boolean isSupprimable();

	public boolean isReversable();

	public boolean isControleHorsMarcheGood();

	public boolean isControleMarcheGood();

	public boolean isControleActionGood();

	public boolean isControlePlanComptableGood();

	public boolean isControleConventionGood();

	public boolean isControleAnalytiqueGood();

	public void setDepHtSaisieSansCalcul(BigDecimal dppHtInitial);

	public void setDepTtcSaisieSansCalcul(BigDecimal dppTtcInitial);

	public EOEditingContext editingContext();

	public Boolean isReimputable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray<EOOrgan> organs);

	public EOExercice exercice();

	public NSArray<? extends _IDepenseControleMarche> depenseControleMarches();

	public void setDepMontantBudgetaire(BigDecimal aValue);

	public void resetCodeAnalytiquesPossibles();

	public void resetCodeExersPossibles();

	public void resetConventionsPossibles();

	public void resetPlanComptablesPossibles();

	public void resetTypeActionsPossibles();

	public void setSource(_ISourceCredit source);

	/**
	 * Met à jour les informations stockées au niveau de la depense à partir de celle de la source affectée à la dépense.
	 */
	public void updateFromSource();

	public ESourceCreditType getSourceTypeCredit();

	public void setSourceTypeCredit(ESourceCreditType sourceTypeCredit);

	/**
	 * @return true s'il s'agit d'une liquidation sur extourne
	 */
	public Boolean isSurExtourne();

	/**
	 * @return true s'il s'agit d'une liquidation d'extourne
	 */
	public Boolean isExtourne();

}
