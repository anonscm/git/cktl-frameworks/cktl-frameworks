/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEcritureDetailAEmarger.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEcritureDetailAEmarger extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEcritureDetailAEmarger";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_ECRITURE_DETAIL_A_EMARGER";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> ECD_CREDIT = new ERXKey<java.math.BigDecimal>("ecdCredit");
	public static final ERXKey<java.math.BigDecimal> ECD_DEBIT = new ERXKey<java.math.BigDecimal>("ecdDebit");
	public static final ERXKey<String> ECD_LIBELLE = new ERXKey<String>("ecdLibelle");
	public static final ERXKey<java.math.BigDecimal> ECD_MONTANT = new ERXKey<java.math.BigDecimal>("ecdMontant");
	public static final ERXKey<java.math.BigDecimal> ECD_RESTE_EMARGER = new ERXKey<java.math.BigDecimal>("ecdResteEmarger");
	public static final ERXKey<String> ECD_SENS = new ERXKey<String>("ecdSens");
	public static final ERXKey<Integer> ECR_NUMERO = new ERXKey<Integer>("ecrNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail> ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail>("ecritureDetail");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOGestion> GESTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOGestion>("gestion");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> MODE_PAIEMENT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOModePaiement>("modePaiement");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");

	// Attributes


	public static final String ECD_CREDIT_KEY = "ecdCredit";
	public static final String ECD_DEBIT_KEY = "ecdDebit";
	public static final String ECD_LIBELLE_KEY = "ecdLibelle";
	public static final String ECD_MONTANT_KEY = "ecdMontant";
	public static final String ECD_RESTE_EMARGER_KEY = "ecdResteEmarger";
	public static final String ECD_SENS_KEY = "ecdSens";
	public static final String ECR_NUMERO_KEY = "ecrNumero";

//Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String ECD_CREDIT_COLKEY = "ECD_CREDIT";
	public static final String ECD_DEBIT_COLKEY = "ECD_DEBIT";
	public static final String ECD_LIBELLE_COLKEY = "ECD_LIBELLE";
	public static final String ECD_MONTANT_COLKEY = "ECD_MONTANT";
	public static final String ECD_RESTE_EMARGER_COLKEY = "ECD_RESTE_EMARGER";
	public static final String ECD_SENS_COLKEY = "ECD_SENS";
	public static final String ECR_NUMERO_COLKEY = "ECR_NUMERO";

	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String GESTION_KEY = "gestion";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
	public java.math.BigDecimal ecdCredit() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_CREDIT_KEY);
	}

	public void setEcdCredit(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_CREDIT_KEY);
	}

	public java.math.BigDecimal ecdDebit() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_DEBIT_KEY);
	}

	public void setEcdDebit(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_DEBIT_KEY);
	}

	public String ecdLibelle() {
	 return (String) storedValueForKey(ECD_LIBELLE_KEY);
	}

	public void setEcdLibelle(String value) {
	 takeStoredValueForKey(value, ECD_LIBELLE_KEY);
	}

	public java.math.BigDecimal ecdMontant() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_MONTANT_KEY);
	}

	public void setEcdMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_MONTANT_KEY);
	}

	public java.math.BigDecimal ecdResteEmarger() {
	 return (java.math.BigDecimal) storedValueForKey(ECD_RESTE_EMARGER_KEY);
	}

	public void setEcdResteEmarger(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ECD_RESTE_EMARGER_KEY);
	}

	public String ecdSens() {
	 return (String) storedValueForKey(ECD_SENS_KEY);
	}

	public void setEcdSens(String value) {
	 takeStoredValueForKey(value, ECD_SENS_KEY);
	}

	public Integer ecrNumero() {
	 return (Integer) storedValueForKey(ECR_NUMERO_KEY);
	}

	public void setEcrNumero(Integer value) {
	 takeStoredValueForKey(value, ECR_NUMERO_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail ecritureDetail() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
	}

	public void setEcritureDetailRelationship(org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail oldValue = ecritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOGestion gestion() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
	}

	public void setGestionRelationship(org.cocktail.fwkcktldepense.server.metier.EOGestion value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOGestion oldValue = gestion();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOModePaiement modePaiement() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
	}

	public void setModePaiementRelationship(org.cocktail.fwkcktldepense.server.metier.EOModePaiement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOModePaiement oldValue = modePaiement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}


	/**
	* Créer une instance de EOEcritureDetailAEmarger avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEcritureDetailAEmarger createEOEcritureDetailAEmarger(EOEditingContext editingContext										, java.math.BigDecimal ecdMontant
							, java.math.BigDecimal ecdResteEmarger
							, String ecdSens
							, org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail ecritureDetail		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOGestion gestion		, org.cocktail.fwkcktldepense.server.metier.EOModePaiement modePaiement		, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable					) {
	 EOEcritureDetailAEmarger eo = (EOEcritureDetailAEmarger) EOUtilities.createAndInsertInstance(editingContext, _EOEcritureDetailAEmarger.ENTITY_NAME);	 
													eo.setEcdMontant(ecdMontant);
									eo.setEcdResteEmarger(ecdResteEmarger);
									eo.setEcdSens(ecdSens);
								 eo.setEcritureDetailRelationship(ecritureDetail);
				 eo.setExerciceRelationship(exercice);
				 eo.setGestionRelationship(gestion);
				 eo.setModePaiementRelationship(modePaiement);
				 eo.setPlanComptableRelationship(planComptable);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcritureDetailAEmarger creerInstance(EOEditingContext editingContext) {
		EOEcritureDetailAEmarger object = (EOEcritureDetailAEmarger)EOUtilities.createAndInsertInstance(editingContext, _EOEcritureDetailAEmarger.ENTITY_NAME);
  		return object;
		}

	

  public EOEcritureDetailAEmarger localInstanceIn(EOEditingContext editingContext) {
    EOEcritureDetailAEmarger localInstance = (EOEcritureDetailAEmarger)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEcritureDetailAEmarger fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEcritureDetailAEmarger fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEcritureDetailAEmarger> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcritureDetailAEmarger eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcritureDetailAEmarger)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcritureDetailAEmarger fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcritureDetailAEmarger fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEcritureDetailAEmarger> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcritureDetailAEmarger eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcritureDetailAEmarger)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEcritureDetailAEmarger fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcritureDetailAEmarger eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcritureDetailAEmarger ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcritureDetailAEmarger fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger> objectsForRecherche(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOModePaiement modePaiementBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOEcritureDetailAEmarger.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (modePaiementBinding != null)
			bindings.takeValueForKey(modePaiementBinding, "modePaiement");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
