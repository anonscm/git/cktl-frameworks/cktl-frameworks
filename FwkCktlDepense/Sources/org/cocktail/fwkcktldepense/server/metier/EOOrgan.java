/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOOrgan extends _EOOrgan {

	public static final Integer ORG_NIV_0 = Integer.valueOf(0);
	public static final Integer ORG_NIV_1 = Integer.valueOf(1);
	public static final Integer ORG_NIV_2 = Integer.valueOf(2);
	public static final Integer ORG_NIV_3 = Integer.valueOf(3);
	public static final Integer ORG_NIV_4 = Integer.valueOf(4);
	public static final int ORG_NIV_MAX = EOOrgan.ORG_NIV_4.intValue();

	public static final String ORG_NIV_0_LIB = "UNIVERSITE";
	public static final String ORG_NIV_1_LIB = "ETABLISSEMENT";
	public static final String ORG_NIV_2_LIB = "UB";
	public static final String ORG_NIV_3_LIB = "CR";
	public static final String ORG_NIV_4_LIB = "SOUS CR";

	public static final Map<Integer, String> NIV_LIB_MAP = new HashMap<Integer, String>();
	public static final Map<String, Integer> LIB_NIV_MAP = new HashMap<String, Integer>();
	static {
		NIV_LIB_MAP.put(ORG_NIV_0, ORG_NIV_0_LIB);
		NIV_LIB_MAP.put(ORG_NIV_1, ORG_NIV_1_LIB);
		NIV_LIB_MAP.put(ORG_NIV_2, ORG_NIV_2_LIB);
		NIV_LIB_MAP.put(ORG_NIV_3, ORG_NIV_3_LIB);
		NIV_LIB_MAP.put(ORG_NIV_4, ORG_NIV_4_LIB);

		LIB_NIV_MAP.put(ORG_NIV_0_LIB, ORG_NIV_0);
		LIB_NIV_MAP.put(ORG_NIV_1_LIB, ORG_NIV_1);
		LIB_NIV_MAP.put(ORG_NIV_2_LIB, ORG_NIV_2);
		LIB_NIV_MAP.put(ORG_NIV_3_LIB, ORG_NIV_3);
		LIB_NIV_MAP.put(ORG_NIV_4_LIB, ORG_NIV_4);
	};

	public EOOrgan() {
		super();
	}

	public String sourceLibelle() {
		String libelle = "";

		if (orgUb() != null)
			libelle = orgUb();
		if (orgCr() != null)
			libelle = libelle + "/" + orgCr();
		if (orgSouscr() != null)
			libelle = libelle + "/" + orgSouscr();

		return libelle;
	}

	public boolean isCodeAnalytiqueObligatoire() {
		if (typeEtatCanalObligatoire() != null) {
			if (typeEtatCanalObligatoire().tyetLibelle().equals(EOTypeEtat.DEPENSES) || typeEtatCanalObligatoire().tyetLibelle().equals(EOTypeEtat.OUI))
				return true;
			return false;
		}

		if (organPere() == null)
			return false;

		return organPere().isCodeAnalytiqueObligatoire();
	}

	public boolean isConventionObligatoire() {
		if (typeEtatConventionObligatoire() != null) {
			 return isConventionObligatoirePourDepense();
		}

		if (organPere() == null) {
			return false;
		}

		return organPere().isConventionObligatoire();
	}

	protected boolean isConventionObligatoirePourDepense() {
		return typeEtatConventionObligatoire() != null
				&& (EOTypeEtat.DEPENSES.equals(typeEtatConventionObligatoire().tyetLibelle())
						|| EOTypeEtat.OUI.equals(typeEtatConventionObligatoire().tyetLibelle()));
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		throw new FactoryException(ENTITY_NAME + " : " + FactoryException.messageErreur);
	}

	public NSArray<EOTauxProrata> tauxProratasPourExerciceTries(EOExercice exercice) {
		if (organProratas() == null || organProratas().count() == 0)
			return NSArray.emptyArray();

		return (NSArray) EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(organProratas(),
				EOQualifier.qualifierWithQualifierFormat(EOOrganProrata.EXERCICE_KEY + "=%@", new NSArray(exercice))), new NSArray(new Object[] {
				EOOrganProrata.SORT_ORP_PRIORITE_ASC
		})).
				valueForKeyPath(EOOrganProrata.TAUX_PRORATA_KEY);

	}

	public Boolean isUB() {
		return ORG_NIV_2.equals(orgNiveau());
	}

	public Boolean isCR() {
		return ORG_NIV_3.equals(orgNiveau());
	}

}
