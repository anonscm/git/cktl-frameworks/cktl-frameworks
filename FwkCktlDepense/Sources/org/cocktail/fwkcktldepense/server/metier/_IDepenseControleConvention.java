package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

public interface _IDepenseControleConvention {

	public final static String _CONVENTION_KEY = "convention";

	public void setDconTtcSaisie(BigDecimal restantTtcControleConvention);

	public void setPourcentage(BigDecimal subtract);

	public EOConvention convention();

	public void setConventionRelationship(EOConvention uneConventionSelectionnee);

	public BigDecimal pourcentage();

	public BigDecimal dconHtSaisie();

	public BigDecimal dconTtcSaisie();

	public BigDecimal dconMontantBudgetaire();

	public void setMontantTtc(BigDecimal uneDepenseCtrlConventionMontant);

	public BigDecimal montantTtc();
}
