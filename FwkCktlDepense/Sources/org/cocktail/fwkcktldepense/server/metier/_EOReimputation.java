/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOReimputation.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOReimputation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputation";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION";


//Attribute Keys
	public static final ERXKey<NSTimestamp> REIM_DATE = new ERXKey<NSTimestamp>("reimDate");
	public static final ERXKey<String> REIM_LIBELLE = new ERXKey<String>("reimLibelle");
	public static final ERXKey<Integer> REIM_NUMERO = new ERXKey<Integer>("reimNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> DEPENSE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("depenseBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> REIMPUTATION_ACTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction>("reimputationActions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> REIMPUTATION_ANALYTIQUES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique>("reimputationAnalytiques");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> REIMPUTATION_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget>("reimputationBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> REIMPUTATION_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention>("reimputationConventions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> REIMPUTATION_HORS_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche>("reimputationHorsMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> REIMPUTATION_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche>("reimputationMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> REIMPUTATION_NEW_ACTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction>("reimputationNewActions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> REIMPUTATION_NEW_ANALYTIQUES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique>("reimputationNewAnalytiques");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> REIMPUTATION_NEW_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget>("reimputationNewBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> REIMPUTATION_NEW_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention>("reimputationNewConventions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> REIMPUTATION_NEW_HORS_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche>("reimputationNewHorsMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> REIMPUTATION_NEW_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche>("reimputationNewMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> REIMPUTATION_NEW_PLANCOS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco>("reimputationNewPlancos");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> REIMPUTATION_PLANCOS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco>("reimputationPlancos");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "reimId";

	public static final String REIM_DATE_KEY = "reimDate";
	public static final String REIM_LIBELLE_KEY = "reimLibelle";
	public static final String REIM_NUMERO_KEY = "reimNumero";

//Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String REIM_ID_KEY = "reimId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String REIM_DATE_COLKEY = "REIM_DATE";
	public static final String REIM_LIBELLE_COLKEY = "REIM_LIBELLE";
	public static final String REIM_NUMERO_COLKEY = "REIM_NUMERO";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_BUDGET_KEY = "depenseBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String REIMPUTATION_ACTIONS_KEY = "reimputationActions";
	public static final String REIMPUTATION_ANALYTIQUES_KEY = "reimputationAnalytiques";
	public static final String REIMPUTATION_BUDGETS_KEY = "reimputationBudgets";
	public static final String REIMPUTATION_CONVENTIONS_KEY = "reimputationConventions";
	public static final String REIMPUTATION_HORS_MARCHES_KEY = "reimputationHorsMarches";
	public static final String REIMPUTATION_MARCHES_KEY = "reimputationMarches";
	public static final String REIMPUTATION_NEW_ACTIONS_KEY = "reimputationNewActions";
	public static final String REIMPUTATION_NEW_ANALYTIQUES_KEY = "reimputationNewAnalytiques";
	public static final String REIMPUTATION_NEW_BUDGETS_KEY = "reimputationNewBudgets";
	public static final String REIMPUTATION_NEW_CONVENTIONS_KEY = "reimputationNewConventions";
	public static final String REIMPUTATION_NEW_HORS_MARCHES_KEY = "reimputationNewHorsMarches";
	public static final String REIMPUTATION_NEW_MARCHES_KEY = "reimputationNewMarches";
	public static final String REIMPUTATION_NEW_PLANCOS_KEY = "reimputationNewPlancos";
	public static final String REIMPUTATION_PLANCOS_KEY = "reimputationPlancos";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public NSTimestamp reimDate() {
	 return (NSTimestamp) storedValueForKey(REIM_DATE_KEY);
	}

	public void setReimDate(NSTimestamp value) {
	 takeStoredValueForKey(value, REIM_DATE_KEY);
	}

	public String reimLibelle() {
	 return (String) storedValueForKey(REIM_LIBELLE_KEY);
	}

	public void setReimLibelle(String value) {
	 takeStoredValueForKey(value, REIM_LIBELLE_KEY);
	}

	public Integer reimNumero() {
	 return (Integer) storedValueForKey(REIM_NUMERO_KEY);
	}

	public void setReimNumero(Integer value) {
	 takeStoredValueForKey(value, REIM_NUMERO_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget depenseBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_KEY);
	}

	public void setDepenseBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseBudget oldValue = depenseBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> reimputationActions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction>)storedValueForKey(REIMPUTATION_ACTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> reimputationActions(EOQualifier qualifier) {
	 return reimputationActions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> reimputationActions(EOQualifier qualifier, boolean fetch) {
	 return reimputationActions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> reimputationActions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationAction.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationActions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationAction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_ACTIONS_KEY);
	}
	
	public void removeFromReimputationActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ACTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationAction createReimputationActionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationAction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_ACTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationAction) eo;
	}
	
	public void deleteReimputationActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ACTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationActionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationAction> objects = reimputationActions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationActionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> reimputationAnalytiques() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique>)storedValueForKey(REIMPUTATION_ANALYTIQUES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> reimputationAnalytiques(EOQualifier qualifier) {
	 return reimputationAnalytiques(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> reimputationAnalytiques(EOQualifier qualifier, boolean fetch) {
	 return reimputationAnalytiques(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> reimputationAnalytiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationAnalytiques();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_ANALYTIQUES_KEY);
	}
	
	public void removeFromReimputationAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ANALYTIQUES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique createReimputationAnalytiquesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_ANALYTIQUES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique) eo;
	}
	
	public void deleteReimputationAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ANALYTIQUES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationAnalytiquesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique> objects = reimputationAnalytiques().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationAnalytiquesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> reimputationBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget>)storedValueForKey(REIMPUTATION_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> reimputationBudgets(EOQualifier qualifier) {
	 return reimputationBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> reimputationBudgets(EOQualifier qualifier, boolean fetch) {
	 return reimputationBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> reimputationBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_BUDGETS_KEY);
	}
	
	public void removeFromReimputationBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget createReimputationBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget) eo;
	}
	
	public void deleteReimputationBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget> objects = reimputationBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationBudgetsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> reimputationConventions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention>)storedValueForKey(REIMPUTATION_CONVENTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> reimputationConventions(EOQualifier qualifier) {
	 return reimputationConventions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> reimputationConventions(EOQualifier qualifier, boolean fetch) {
	 return reimputationConventions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> reimputationConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationConventions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_CONVENTIONS_KEY);
	}
	
	public void removeFromReimputationConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_CONVENTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention createReimputationConventionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_CONVENTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention) eo;
	}
	
	public void deleteReimputationConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_CONVENTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationConventionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention> objects = reimputationConventions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationConventionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> reimputationHorsMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche>)storedValueForKey(REIMPUTATION_HORS_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> reimputationHorsMarches(EOQualifier qualifier) {
	 return reimputationHorsMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> reimputationHorsMarches(EOQualifier qualifier, boolean fetch) {
	 return reimputationHorsMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> reimputationHorsMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationHorsMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_HORS_MARCHES_KEY);
	}
	
	public void removeFromReimputationHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_HORS_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche createReimputationHorsMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_HORS_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche) eo;
	}
	
	public void deleteReimputationHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_HORS_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationHorsMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> objects = reimputationHorsMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationHorsMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> reimputationMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche>)storedValueForKey(REIMPUTATION_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> reimputationMarches(EOQualifier qualifier) {
	 return reimputationMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> reimputationMarches(EOQualifier qualifier, boolean fetch) {
	 return reimputationMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> reimputationMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_MARCHES_KEY);
	}
	
	public void removeFromReimputationMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche createReimputationMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche) eo;
	}
	
	public void deleteReimputationMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> objects = reimputationMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> reimputationNewActions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction>)storedValueForKey(REIMPUTATION_NEW_ACTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> reimputationNewActions(EOQualifier qualifier) {
	 return reimputationNewActions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> reimputationNewActions(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewActions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> reimputationNewActions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewActions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ACTIONS_KEY);
	}
	
	public void removeFromReimputationNewActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ACTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction createReimputationNewActionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_ACTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction) eo;
	}
	
	public void deleteReimputationNewActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ACTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewActionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction> objects = reimputationNewActions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewActionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> reimputationNewAnalytiques() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique>)storedValueForKey(REIMPUTATION_NEW_ANALYTIQUES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> reimputationNewAnalytiques(EOQualifier qualifier) {
	 return reimputationNewAnalytiques(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> reimputationNewAnalytiques(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewAnalytiques(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> reimputationNewAnalytiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewAnalytiques();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ANALYTIQUES_KEY);
	}
	
	public void removeFromReimputationNewAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ANALYTIQUES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique createReimputationNewAnalytiquesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_ANALYTIQUES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique) eo;
	}
	
	public void deleteReimputationNewAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ANALYTIQUES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewAnalytiquesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique> objects = reimputationNewAnalytiques().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewAnalytiquesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> reimputationNewBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget>)storedValueForKey(REIMPUTATION_NEW_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> reimputationNewBudgets(EOQualifier qualifier) {
	 return reimputationNewBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> reimputationNewBudgets(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> reimputationNewBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_BUDGETS_KEY);
	}
	
	public void removeFromReimputationNewBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget createReimputationNewBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget) eo;
	}
	
	public void deleteReimputationNewBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> objects = reimputationNewBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewBudgetsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> reimputationNewConventions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention>)storedValueForKey(REIMPUTATION_NEW_CONVENTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> reimputationNewConventions(EOQualifier qualifier) {
	 return reimputationNewConventions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> reimputationNewConventions(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewConventions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> reimputationNewConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewConventions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_CONVENTIONS_KEY);
	}
	
	public void removeFromReimputationNewConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_CONVENTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention createReimputationNewConventionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_CONVENTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention) eo;
	}
	
	public void deleteReimputationNewConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_CONVENTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewConventionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> objects = reimputationNewConventions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewConventionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> reimputationNewHorsMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche>)storedValueForKey(REIMPUTATION_NEW_HORS_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> reimputationNewHorsMarches(EOQualifier qualifier) {
	 return reimputationNewHorsMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> reimputationNewHorsMarches(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewHorsMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> reimputationNewHorsMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewHorsMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_HORS_MARCHES_KEY);
	}
	
	public void removeFromReimputationNewHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_HORS_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche createReimputationNewHorsMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_HORS_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche) eo;
	}
	
	public void deleteReimputationNewHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_HORS_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewHorsMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche> objects = reimputationNewHorsMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewHorsMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> reimputationNewMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche>)storedValueForKey(REIMPUTATION_NEW_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> reimputationNewMarches(EOQualifier qualifier) {
	 return reimputationNewMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> reimputationNewMarches(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> reimputationNewMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_MARCHES_KEY);
	}
	
	public void removeFromReimputationNewMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche createReimputationNewMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche) eo;
	}
	
	public void deleteReimputationNewMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewMarche> objects = reimputationNewMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> reimputationNewPlancos() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco>)storedValueForKey(REIMPUTATION_NEW_PLANCOS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> reimputationNewPlancos(EOQualifier qualifier) {
	 return reimputationNewPlancos(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> reimputationNewPlancos(EOQualifier qualifier, boolean fetch) {
	 return reimputationNewPlancos(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> reimputationNewPlancos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationNewPlancos();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationNewPlancosRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_PLANCOS_KEY);
	}
	
	public void removeFromReimputationNewPlancosRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_PLANCOS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco createReimputationNewPlancosRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_PLANCOS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco) eo;
	}
	
	public void deleteReimputationNewPlancosRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_PLANCOS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationNewPlancosRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco> objects = reimputationNewPlancos().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationNewPlancosRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> reimputationPlancos() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco>)storedValueForKey(REIMPUTATION_PLANCOS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> reimputationPlancos(EOQualifier qualifier) {
	 return reimputationPlancos(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> reimputationPlancos(EOQualifier qualifier, boolean fetch) {
	 return reimputationPlancos(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> reimputationPlancos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reimputationPlancos();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReimputationPlancosRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_PLANCOS_KEY);
	}
	
	public void removeFromReimputationPlancosRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_PLANCOS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco createReimputationPlancosRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_PLANCOS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco) eo;
	}
	
	public void deleteReimputationPlancosRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_PLANCOS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReimputationPlancosRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> objects = reimputationPlancos().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReimputationPlancosRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOReimputation avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOReimputation createEOReimputation(EOEditingContext editingContext				, NSTimestamp reimDate
									, Integer reimNumero
					, org.cocktail.fwkcktldepense.server.metier.EODepenseBudget depenseBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EOReimputation eo = (EOReimputation) EOUtilities.createAndInsertInstance(editingContext, _EOReimputation.ENTITY_NAME);	 
							eo.setReimDate(reimDate);
											eo.setReimNumero(reimNumero);
						 eo.setDepenseBudgetRelationship(depenseBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputation creerInstance(EOEditingContext editingContext) {
		EOReimputation object = (EOReimputation)EOUtilities.createAndInsertInstance(editingContext, _EOReimputation.ENTITY_NAME);
  		return object;
		}

	

  public EOReimputation localInstanceIn(EOEditingContext editingContext) {
    EOReimputation localInstance = (EOReimputation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputation>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReimputation> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReimputation> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
