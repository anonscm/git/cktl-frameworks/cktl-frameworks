package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import com.webobjects.foundation.NSArray;

public interface _IDepenseControlePlanComptable {

	public static final String _PLAN_COMPTABLE_KEY = "planComptable";

	public void setDpcoTtcSaisie(BigDecimal restantTtcControlePlanComptable);

	public void setPourcentage(BigDecimal subtract);

	public EOPlanComptable planComptable();

	public BigDecimal pourcentage();

	public BigDecimal dpcoTtcSaisie();

	public BigDecimal dpcoHtSaisie();

	public BigDecimal dpcoMontantBudgetaire();

	public void setMontantTtc(BigDecimal uneDepenseCtrlImputationMontant);

	public boolean isGood();

	public boolean isInventaireObligatoire();

	public void setPlanComptableRelationship(EOPlanComptable uneImputationSelectionnee);

	public boolean isInventaireAffichable();

	public NSArray<EOInventaire> inventaires();

	public boolean isInventairesPossibles();

	public _IDepenseBudget depenseBudget();

	public NSArray<EOInventaire> getInventairesPossibles();

	public NSArray<EOInventaire> inventairesLiquides();

	public BigDecimal montantTtc();
}
