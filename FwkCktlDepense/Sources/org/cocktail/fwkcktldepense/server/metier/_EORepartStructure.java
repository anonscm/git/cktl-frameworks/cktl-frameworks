/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EORepartStructure.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EORepartStructure extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleRepartStructure";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_STRUCTURE";


//Attribute Keys
	public static final ERXKey<String> C_STRUCTURE_CLE = new ERXKey<String>("cStructureCle");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGANS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organs");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure> STRUCTURE_ULR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure>("structureUlr");

	// Attributes


	public static final String C_STRUCTURE_CLE_KEY = "cStructureCle";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

//Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_CLE_COLKEY = "C_STRUCTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String ORGANS_KEY = "organs";
	public static final String PERSONNE_KEY = "personne";
	public static final String STRUCTURE_ULR_KEY = "structureUlr";



	// Accessors methods
	public String cStructureCle() {
	 return (String) storedValueForKey(C_STRUCTURE_CLE_KEY);
	}

	public void setCStructureCle(String value) {
	 takeStoredValueForKey(value, C_STRUCTURE_CLE_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktldepense.server.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOStructure structureUlr() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOStructure)storedValueForKey(STRUCTURE_ULR_KEY);
	}

	public void setStructureUlrRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOStructure oldValue = structureUlr();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_ULR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organs() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)storedValueForKey(ORGANS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organs(EOQualifier qualifier) {
	 return organs(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> results;
			   results = organs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ORGANS_KEY);
	}
	
	public void removeFromOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGANS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOOrgan createOrgansRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ORGANS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan) eo;
	}
	
	public void deleteOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGANS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllOrgansRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOOrgan> objects = organs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteOrgansRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EORepartStructure avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EORepartStructure createEORepartStructure(EOEditingContext editingContext				, String cStructureCle
									, org.cocktail.fwkcktldepense.server.metier.EOPersonne personne		, org.cocktail.fwkcktldepense.server.metier.EOStructure structureUlr					) {
	 EORepartStructure eo = (EORepartStructure) EOUtilities.createAndInsertInstance(editingContext, _EORepartStructure.ENTITY_NAME);	 
							eo.setCStructureCle(cStructureCle);
										 eo.setPersonneRelationship(personne);
				 eo.setStructureUlrRelationship(structureUlr);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartStructure creerInstance(EOEditingContext editingContext) {
		EORepartStructure object = (EORepartStructure)EOUtilities.createAndInsertInstance(editingContext, _EORepartStructure.ENTITY_NAME);
  		return object;
		}

	

  public EORepartStructure localInstanceIn(EOEditingContext editingContext) {
    EORepartStructure localInstance = (EORepartStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartStructure fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartStructure fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORepartStructure> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartStructure)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORepartStructure> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartStructure)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartStructure fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartStructure eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartStructure ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartStructure fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
