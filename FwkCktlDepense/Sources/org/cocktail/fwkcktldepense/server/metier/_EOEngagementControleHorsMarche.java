/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEngagementControleHorsMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEngagementControleHorsMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControleHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_HORS_MARCHE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> EHOM_DATE_SAISIE = new ERXKey<NSTimestamp>("ehomDateSaisie");
	public static final ERXKey<java.math.BigDecimal> EHOM_HT_RESTE = new ERXKey<java.math.BigDecimal>("ehomHtReste");
	public static final ERXKey<java.math.BigDecimal> EHOM_HT_SAISIE = new ERXKey<java.math.BigDecimal>("ehomHtSaisie");
	public static final ERXKey<java.math.BigDecimal> EHOM_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("ehomMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> EHOM_MONTANT_BUDGETAIRE_RESTE = new ERXKey<java.math.BigDecimal>("ehomMontantBudgetaireReste");
	public static final ERXKey<java.math.BigDecimal> EHOM_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("ehomTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> EHOM_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("ehomTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> CODE_EXER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>("codeExer");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat> TYPE_ACHAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat>("typeAchat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ehomId";

	public static final String EHOM_DATE_SAISIE_KEY = "ehomDateSaisie";
	public static final String EHOM_HT_RESTE_KEY = "ehomHtReste";
	public static final String EHOM_HT_SAISIE_KEY = "ehomHtSaisie";
	public static final String EHOM_MONTANT_BUDGETAIRE_KEY = "ehomMontantBudgetaire";
	public static final String EHOM_MONTANT_BUDGETAIRE_RESTE_KEY = "ehomMontantBudgetaireReste";
	public static final String EHOM_TTC_SAISIE_KEY = "ehomTtcSaisie";
	public static final String EHOM_TVA_SAISIE_KEY = "ehomTvaSaisie";

//Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String EHOM_ID_KEY = "ehomId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String EHOM_DATE_SAISIE_COLKEY = "EHOM_DATE_SAISIE";
	public static final String EHOM_HT_RESTE_COLKEY = "EHOM_HT_RESTE";
	public static final String EHOM_HT_SAISIE_COLKEY = "EHOM_HT_SAISIE";
	public static final String EHOM_MONTANT_BUDGETAIRE_COLKEY = "EHOM_MONTANT_BUDGETAIRE";
	public static final String EHOM_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EHOM_MONTANT_BUDGETAIRE_RESTE";
	public static final String EHOM_TTC_SAISIE_COLKEY = "EHOM_TTC_SAISIE";
	public static final String EHOM_TVA_SAISIE_COLKEY = "EHOM_TVA_SAISIE";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String EHOM_ID_COLKEY = "EHOM_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
	public NSTimestamp ehomDateSaisie() {
	 return (NSTimestamp) storedValueForKey(EHOM_DATE_SAISIE_KEY);
	}

	public void setEhomDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, EHOM_DATE_SAISIE_KEY);
	}

	public java.math.BigDecimal ehomHtReste() {
	 return (java.math.BigDecimal) storedValueForKey(EHOM_HT_RESTE_KEY);
	}

	public void setEhomHtReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EHOM_HT_RESTE_KEY);
	}

	public java.math.BigDecimal ehomHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EHOM_HT_SAISIE_KEY);
	}

	public void setEhomHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EHOM_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal ehomMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(EHOM_MONTANT_BUDGETAIRE_KEY);
	}

	public void setEhomMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EHOM_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal ehomMontantBudgetaireReste() {
	 return (java.math.BigDecimal) storedValueForKey(EHOM_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public void setEhomMontantBudgetaireReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EHOM_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public java.math.BigDecimal ehomTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EHOM_TTC_SAISIE_KEY);
	}

	public void setEhomTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EHOM_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal ehomTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EHOM_TVA_SAISIE_KEY);
	}

	public void setEhomTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EHOM_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
	}

	public void setCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeExer oldValue = codeExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
	}

	public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeAchat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeAchat oldValue = typeAchat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOEngagementControleHorsMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEngagementControleHorsMarche createEOEngagementControleHorsMarche(EOEditingContext editingContext				, NSTimestamp ehomDateSaisie
							, java.math.BigDecimal ehomHtReste
							, java.math.BigDecimal ehomHtSaisie
							, java.math.BigDecimal ehomMontantBudgetaire
							, java.math.BigDecimal ehomMontantBudgetaireReste
							, java.math.BigDecimal ehomTtcSaisie
							, java.math.BigDecimal ehomTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer		, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat					) {
	 EOEngagementControleHorsMarche eo = (EOEngagementControleHorsMarche) EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControleHorsMarche.ENTITY_NAME);	 
							eo.setEhomDateSaisie(ehomDateSaisie);
									eo.setEhomHtReste(ehomHtReste);
									eo.setEhomHtSaisie(ehomHtSaisie);
									eo.setEhomMontantBudgetaire(ehomMontantBudgetaire);
									eo.setEhomMontantBudgetaireReste(ehomMontantBudgetaireReste);
									eo.setEhomTtcSaisie(ehomTtcSaisie);
									eo.setEhomTvaSaisie(ehomTvaSaisie);
						 eo.setCodeExerRelationship(codeExer);
				 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setTypeAchatRelationship(typeAchat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngagementControleHorsMarche creerInstance(EOEditingContext editingContext) {
		EOEngagementControleHorsMarche object = (EOEngagementControleHorsMarche)EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControleHorsMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOEngagementControleHorsMarche localInstanceIn(EOEditingContext editingContext) {
    EOEngagementControleHorsMarche localInstance = (EOEngagementControleHorsMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngagementControleHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngagementControleHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEngagementControleHorsMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEngagementControleHorsMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngagementControleHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControleHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControleHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControleHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
