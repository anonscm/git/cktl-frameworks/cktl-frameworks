/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeControleConvention.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeControleConvention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeControleConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_CTRL_CONVENTION";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> CCON_HT_SAISIE = new ERXKey<java.math.BigDecimal>("cconHtSaisie");
	public static final ERXKey<java.math.BigDecimal> CCON_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("cconMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> CCON_POURCENTAGE = new ERXKey<java.math.BigDecimal>("cconPourcentage");
	public static final ERXKey<java.math.BigDecimal> CCON_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("cconTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> CCON_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("cconTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> COMMANDE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>("commandeBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention> CONVENTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention>("convention");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cconId";

	public static final String CCON_HT_SAISIE_KEY = "cconHtSaisie";
	public static final String CCON_MONTANT_BUDGETAIRE_KEY = "cconMontantBudgetaire";
	public static final String CCON_POURCENTAGE_KEY = "cconPourcentage";
	public static final String CCON_TTC_SAISIE_KEY = "cconTtcSaisie";
	public static final String CCON_TVA_SAISIE_KEY = "cconTvaSaisie";

//Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String CCON_ID_KEY = "cconId";
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CCON_HT_SAISIE_COLKEY = "CCON_HT_SAISIE";
	public static final String CCON_MONTANT_BUDGETAIRE_COLKEY = "CCON_MONTANT_BUDGETAIRE";
	public static final String CCON_POURCENTAGE_COLKEY = "CCON_POURCENTAGE";
	public static final String CCON_TTC_SAISIE_COLKEY = "CCON_TTC_SAISIE";
	public static final String CCON_TVA_SAISIE_COLKEY = "CCON_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String CCON_ID_COLKEY = "CCON_ID";
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String COMMANDE_BUDGET_KEY = "commandeBudget";
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public java.math.BigDecimal cconHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CCON_HT_SAISIE_KEY);
	}

	public void setCconHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CCON_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal cconMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(CCON_MONTANT_BUDGETAIRE_KEY);
	}

	public void setCconMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CCON_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal cconPourcentage() {
	 return (java.math.BigDecimal) storedValueForKey(CCON_POURCENTAGE_KEY);
	}

	public void setCconPourcentage(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CCON_POURCENTAGE_KEY);
	}

	public java.math.BigDecimal cconTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CCON_TTC_SAISIE_KEY);
	}

	public void setCconTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CCON_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal cconTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CCON_TVA_SAISIE_KEY);
	}

	public void setCconTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CCON_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget commandeBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget)storedValueForKey(COMMANDE_BUDGET_KEY);
	}

	public void setCommandeBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget oldValue = commandeBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOConvention convention() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
	}

	public void setConventionRelationship(org.cocktail.fwkcktldepense.server.metier.EOConvention value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOConvention oldValue = convention();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOCommandeControleConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeControleConvention createEOCommandeControleConvention(EOEditingContext editingContext				, java.math.BigDecimal cconHtSaisie
							, java.math.BigDecimal cconMontantBudgetaire
							, java.math.BigDecimal cconPourcentage
							, java.math.BigDecimal cconTtcSaisie
							, java.math.BigDecimal cconTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget commandeBudget		, org.cocktail.fwkcktldepense.server.metier.EOConvention convention		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOCommandeControleConvention eo = (EOCommandeControleConvention) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeControleConvention.ENTITY_NAME);	 
							eo.setCconHtSaisie(cconHtSaisie);
									eo.setCconMontantBudgetaire(cconMontantBudgetaire);
									eo.setCconPourcentage(cconPourcentage);
									eo.setCconTtcSaisie(cconTtcSaisie);
									eo.setCconTvaSaisie(cconTvaSaisie);
						 eo.setCommandeBudgetRelationship(commandeBudget);
				 eo.setConventionRelationship(convention);
				 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeControleConvention creerInstance(EOEditingContext editingContext) {
		EOCommandeControleConvention object = (EOCommandeControleConvention)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeControleConvention.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeControleConvention localInstanceIn(EOEditingContext editingContext) {
    EOCommandeControleConvention localInstance = (EOCommandeControleConvention)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeControleConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeControleConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeControleConvention> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeControleConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeControleConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeControleConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeControleConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeControleConvention> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeControleConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeControleConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeControleConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeControleConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeControleConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeControleConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
