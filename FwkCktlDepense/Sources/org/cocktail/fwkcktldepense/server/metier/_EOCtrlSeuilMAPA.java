/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCtrlSeuilMAPA.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCtrlSeuilMAPA extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCtrlSeuilMAPA";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CTRL_SEUIL_MAPA";


//Attribute Keys
	public static final ERXKey<String> CM_CODE = new ERXKey<String>("cmCode");
	public static final ERXKey<String> CM_CODE_FAM = new ERXKey<String>("cmCodeFam");
	public static final ERXKey<String> CM_LIB = new ERXKey<String>("cmLib");
	public static final ERXKey<String> CM_LIB_FAM = new ERXKey<String>("cmLibFam");
	public static final ERXKey<java.math.BigDecimal> MONTANT_ENGAGE_HT = new ERXKey<java.math.BigDecimal>("montantEngageHt");
	public static final ERXKey<java.math.BigDecimal> SEUIL_MIN = new ERXKey<java.math.BigDecimal>("seuilMin");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> CODE_EXER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>("codeExer");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ceOrdre";

	public static final String CM_CODE_KEY = "cmCode";
	public static final String CM_CODE_FAM_KEY = "cmCodeFam";
	public static final String CM_LIB_KEY = "cmLib";
	public static final String CM_LIB_FAM_KEY = "cmLibFam";
	public static final String MONTANT_ENGAGE_HT_KEY = "montantEngageHt";
	public static final String SEUIL_MIN_KEY = "seuilMin";

//Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CM_CODE_COLKEY = "CM_CODE";
	public static final String CM_CODE_FAM_COLKEY = "CM_CODE_FAM";
	public static final String CM_LIB_COLKEY = "CM_LIB";
	public static final String CM_LIB_FAM_COLKEY = "CM_LIB_FAM";
	public static final String MONTANT_ENGAGE_HT_COLKEY = "MONTANT_HT";
	public static final String SEUIL_MIN_COLKEY = "SEUIL_MIN";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public String cmCode() {
	 return (String) storedValueForKey(CM_CODE_KEY);
	}

	public void setCmCode(String value) {
	 takeStoredValueForKey(value, CM_CODE_KEY);
	}

	public String cmCodeFam() {
	 return (String) storedValueForKey(CM_CODE_FAM_KEY);
	}

	public void setCmCodeFam(String value) {
	 takeStoredValueForKey(value, CM_CODE_FAM_KEY);
	}

	public String cmLib() {
	 return (String) storedValueForKey(CM_LIB_KEY);
	}

	public void setCmLib(String value) {
	 takeStoredValueForKey(value, CM_LIB_KEY);
	}

	public String cmLibFam() {
	 return (String) storedValueForKey(CM_LIB_FAM_KEY);
	}

	public void setCmLibFam(String value) {
	 takeStoredValueForKey(value, CM_LIB_FAM_KEY);
	}

	public java.math.BigDecimal montantEngageHt() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_ENGAGE_HT_KEY);
	}

	public void setMontantEngageHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_ENGAGE_HT_KEY);
	}

	public java.math.BigDecimal seuilMin() {
	 return (java.math.BigDecimal) storedValueForKey(SEUIL_MIN_KEY);
	}

	public void setSeuilMin(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SEUIL_MIN_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
	}

	public void setCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeExer oldValue = codeExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOCtrlSeuilMAPA avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCtrlSeuilMAPA createEOCtrlSeuilMAPA(EOEditingContext editingContext				, String cmCode
							, String cmCodeFam
							, String cmLib
							, String cmLibFam
							, java.math.BigDecimal montantEngageHt
							, java.math.BigDecimal seuilMin
					, org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOCtrlSeuilMAPA eo = (EOCtrlSeuilMAPA) EOUtilities.createAndInsertInstance(editingContext, _EOCtrlSeuilMAPA.ENTITY_NAME);	 
							eo.setCmCode(cmCode);
									eo.setCmCodeFam(cmCodeFam);
									eo.setCmLib(cmLib);
									eo.setCmLibFam(cmLibFam);
									eo.setMontantEngageHt(montantEngageHt);
									eo.setSeuilMin(seuilMin);
						 eo.setCodeExerRelationship(codeExer);
				 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCtrlSeuilMAPA creerInstance(EOEditingContext editingContext) {
		EOCtrlSeuilMAPA object = (EOCtrlSeuilMAPA)EOUtilities.createAndInsertInstance(editingContext, _EOCtrlSeuilMAPA.ENTITY_NAME);
  		return object;
		}

	

  public EOCtrlSeuilMAPA localInstanceIn(EOEditingContext editingContext) {
    EOCtrlSeuilMAPA localInstance = (EOCtrlSeuilMAPA)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCtrlSeuilMAPA fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCtrlSeuilMAPA fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCtrlSeuilMAPA> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCtrlSeuilMAPA eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCtrlSeuilMAPA)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCtrlSeuilMAPA fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCtrlSeuilMAPA fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCtrlSeuilMAPA> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCtrlSeuilMAPA eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCtrlSeuilMAPA)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCtrlSeuilMAPA fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCtrlSeuilMAPA eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCtrlSeuilMAPA ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCtrlSeuilMAPA fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA> objectsForRecherche(EOEditingContext ec,
														Integer ce3cmpBinding,
														String ceActifBinding,
														Integer ceAutresBinding,
														Integer ceMonopoleBinding,
														String ceRechBinding,
														String ceSupprBinding,
														String cmSupprBinding,
														org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExerBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														Integer niveauBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCtrlSeuilMAPA.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (ce3cmpBinding != null)
			bindings.takeValueForKey(ce3cmpBinding, "ce3cmp");
		  if (ceActifBinding != null)
			bindings.takeValueForKey(ceActifBinding, "ceActif");
		  if (ceAutresBinding != null)
			bindings.takeValueForKey(ceAutresBinding, "ceAutres");
		  if (ceMonopoleBinding != null)
			bindings.takeValueForKey(ceMonopoleBinding, "ceMonopole");
		  if (ceRechBinding != null)
			bindings.takeValueForKey(ceRechBinding, "ceRech");
		  if (ceSupprBinding != null)
			bindings.takeValueForKey(ceSupprBinding, "ceSuppr");
		  if (cmSupprBinding != null)
			bindings.takeValueForKey(cmSupprBinding, "cmSuppr");
		  if (codeExerBinding != null)
			bindings.takeValueForKey(codeExerBinding, "codeExer");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (niveauBinding != null)
			bindings.takeValueForKey(niveauBinding, "niveau");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
