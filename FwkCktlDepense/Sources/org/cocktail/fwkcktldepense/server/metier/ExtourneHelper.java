package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.apache.bcel.verifier.exc.InvalidMethodException;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class ExtourneHelper {

	
	public static NSArray<NSDictionary<String, Object>> repartitionPourcentageSurExtournePlanComptables(SourceCreditExtourneLiquidation source) {
		NSMutableArray<NSDictionary<String, Object>> array = new NSMutableArray<NSDictionary<String, Object>>();
		EODepenseBudget dbExtourne = source.getDbExtourne();
		if (dbExtourne.depTtcSaisie().floatValue() == 0)
			return array;
		if (dbExtourne.depenseControlePlanComptables() == null || dbExtourne.depenseControlePlanComptables().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < dbExtourne.depenseControlePlanComptables().count(); i++) {
			EODepenseControlePlanComptable ctrl = (EODepenseControlePlanComptable) dbExtourne.depenseControlePlanComptables().objectAtIndex(i);

			if (ctrl.dpcoTtcSaisie().floatValue() == 0)
				continue;

			boolean dernier = (i == dbExtourne.depenseControlePlanComptables().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.dpcoTtcSaisie().divide(dbExtourne.depTtcSaisie(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary<String, Object>(new NSArray<Object>(new Object[] {
					pourcentage, ctrl.planComptable()
			}),
					new NSArray<String>(new String[] {
							"pourcentage", "planComptable"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public static NSArray<NSDictionary<String, Object>> repartitionPourcentageSurExtourneConventions(SourceCreditExtourneLiquidation source) {
		NSMutableArray<NSDictionary<String, Object>> array = new NSMutableArray<NSDictionary<String, Object>>();
		EODepenseBudget dbExtourne = source.getDbExtourne();
		if (dbExtourne.depTtcSaisie().floatValue() == 0)
			return array;
		if (dbExtourne.depenseControleConventions() == null || dbExtourne.depenseControleConventions().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < dbExtourne.depenseControleConventions().count(); i++) {
			EODepenseControleConvention ctrl = (EODepenseControleConvention) dbExtourne.depenseControleConventions().objectAtIndex(i);

			if (ctrl.dconTtcSaisie().floatValue() == 0)
				continue;

			boolean dernier = (i == dbExtourne.depenseControleConventions().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.dconTtcSaisie().divide(dbExtourne.depTtcSaisie(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary<String, Object>(new NSArray<Object>(new Object[] {
					pourcentage, ctrl.convention()
			}),
					new NSArray<String>(new String[] {
							"pourcentage", "convention"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public static NSArray<NSDictionary<String, Object>> repartitionPourcentageSurExtourneAnalytiques(SourceCreditExtourneLiquidation source) {
		NSMutableArray<NSDictionary<String, Object>> array = new NSMutableArray<NSDictionary<String, Object>>();
		EODepenseBudget dbExtourne = source.getDbExtourne();
		if (dbExtourne.depTtcSaisie().floatValue() == 0)
			return array;
		if (dbExtourne.depenseControleAnalytiques() == null || dbExtourne.depenseControleAnalytiques().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < dbExtourne.depenseControlePlanComptables().count(); i++) {
			EODepenseControleAnalytique ctrl = (EODepenseControleAnalytique) dbExtourne.depenseControleAnalytiques().objectAtIndex(i);

			if (ctrl.danaTtcSaisie().floatValue() == 0)
				continue;

			boolean dernier = (i == dbExtourne.depenseControleAnalytiques().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.danaTtcSaisie().divide(dbExtourne.depTtcSaisie(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary<String, Object>(new NSArray<Object>(new Object[] {
					pourcentage, ctrl.codeAnalytique()
			}),
					new NSArray<String>(new String[] {
							"pourcentage", "codeAnalytique"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public static NSArray<NSDictionary<String, Object>> repartitionPourcentageSurExtourneMarches(SourceCreditExtourneLiquidation source) {
		NSMutableArray<NSDictionary<String, Object>> array = new NSMutableArray<NSDictionary<String, Object>>();
		EODepenseBudget dbExtourne = source.getDbExtourne();
		if (dbExtourne.depTtcSaisie().floatValue() == 0)
			return array;
		if (dbExtourne.depenseControleMarches() == null || dbExtourne.depenseControleMarches().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < dbExtourne.depenseControleMarches().count(); i++) {
			EODepenseControleMarche ctrl = (EODepenseControleMarche) dbExtourne.depenseControleMarches().objectAtIndex(i);

			if (ctrl.dmarTtcSaisie().floatValue() == 0)
				continue;

			boolean dernier = (i == dbExtourne.depenseControleMarches().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.dmarTtcSaisie().divide(dbExtourne.depTtcSaisie(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary<String, Object>(new NSArray<Object>(new Object[] {
					pourcentage, ctrl.attribution()
			}),
					new NSArray<String>(new String[] {
							"pourcentage", "attribution"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}
}
