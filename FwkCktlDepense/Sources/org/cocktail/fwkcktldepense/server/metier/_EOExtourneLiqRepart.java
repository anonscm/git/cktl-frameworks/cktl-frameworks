/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOExtourneLiqRepart.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOExtourneLiqRepart extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleExtourneLiqRepart";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.EXTOURNE_LIQ_REPART";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> TO_EXTOURNE_LIQ = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>("toExtourneLiq");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef> TO_EXTOURNE_LIQ_DEF = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef>("toExtourneLiqDef");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "elrId";


//Attributs non visibles
	public static final String ELD_ID_KEY = "eldId";
	public static final String EL_ID_KEY = "elId";
	public static final String ELR_ID_KEY = "elrId";

//Colonnes dans la base de donnees

	public static final String ELD_ID_COLKEY = "ELD_ID";
	public static final String EL_ID_COLKEY = "EL_ID";
	public static final String ELR_ID_COLKEY = "ELR_ID";


	// Relationships
	public static final String TO_EXTOURNE_LIQ_KEY = "toExtourneLiq";
	public static final String TO_EXTOURNE_LIQ_DEF_KEY = "toExtourneLiqDef";



	// Accessors methods
	public org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq toExtourneLiq() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq)storedValueForKey(TO_EXTOURNE_LIQ_KEY);
	}

	public void setToExtourneLiqRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq oldValue = toExtourneLiq();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXTOURNE_LIQ_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXTOURNE_LIQ_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef toExtourneLiqDef() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef)storedValueForKey(TO_EXTOURNE_LIQ_DEF_KEY);
	}

	public void setToExtourneLiqDefRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef oldValue = toExtourneLiqDef();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXTOURNE_LIQ_DEF_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXTOURNE_LIQ_DEF_KEY);
	 }
	}


	/**
	* Créer une instance de EOExtourneLiqRepart avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOExtourneLiqRepart createEOExtourneLiqRepart(EOEditingContext editingContext		, org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq toExtourneLiq		, org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqDef toExtourneLiqDef					) {
	 EOExtourneLiqRepart eo = (EOExtourneLiqRepart) EOUtilities.createAndInsertInstance(editingContext, _EOExtourneLiqRepart.ENTITY_NAME);	 
				 eo.setToExtourneLiqRelationship(toExtourneLiq);
				 eo.setToExtourneLiqDefRelationship(toExtourneLiqDef);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOExtourneLiqRepart creerInstance(EOEditingContext editingContext) {
		EOExtourneLiqRepart object = (EOExtourneLiqRepart)EOUtilities.createAndInsertInstance(editingContext, _EOExtourneLiqRepart.ENTITY_NAME);
  		return object;
		}

	

  public EOExtourneLiqRepart localInstanceIn(EOEditingContext editingContext) {
    EOExtourneLiqRepart localInstance = (EOExtourneLiqRepart)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOExtourneLiqRepart fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOExtourneLiqRepart fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOExtourneLiqRepart> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOExtourneLiqRepart eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOExtourneLiqRepart)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOExtourneLiqRepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOExtourneLiqRepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOExtourneLiqRepart> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOExtourneLiqRepart eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOExtourneLiqRepart)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOExtourneLiqRepart fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOExtourneLiqRepart eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOExtourneLiqRepart ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOExtourneLiqRepart fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
