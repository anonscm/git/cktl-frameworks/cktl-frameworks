/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPreDepenseControleHorsMarche extends _EOPreDepenseControleHorsMarche implements _IDepenseControleHorsMarche {
	private static Calculs serviceCalculs = Calculs.getInstance();
	

	private BigDecimal pourcentage;
	public static String PDHOM_POURCENTAGE = "pourcentage";

	public EOPreDepenseControleHorsMarche() {
		super();
		setPourcentage(new BigDecimal(0.0));
	}

	public BigDecimal montantTtc() {
		return dhomTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		if (preDepenseBudget() != null && preDepenseBudget().depTtcSaisie() != null && preDepenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(preDepenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (preDepenseBudget() != null) {
			NSArray depenseControleHorsMarches = preDepenseBudget().preDepenseControleHorsMarches();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPreDepenseControleHorsMarche.CODE_EXER_KEY + " != nil", null);
			depenseControleHorsMarches = EOQualifier.filteredArrayWithQualifier(depenseControleHorsMarches, qual);
			BigDecimal total = preDepenseBudget().computeSumForKey(depenseControleHorsMarches, EOPreDepenseControleHorsMarche.PDHOM_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (preDepenseBudget() != null)
			preDepenseBudget().corrigerMontantHorsMarches();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDhomHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDhomHtSaisie(aValue);
	}

	public void setDhomTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDhomTvaSaisie(aValue);
	}

	public void setDhomTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDhomTtcSaisie(aValue);
	}

	public void setDhomMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDhomMontantBudgetaire(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (dhomHtSaisie() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomHtSaisieManquant);
		if (dhomTvaSaisie() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomTvaSaisieManquant);
		if (dhomTtcSaisie() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomTtcSaisieManquant);
		if (dhomMontantBudgetaire() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.dhomMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.exerciceManquant);
		if (preDepenseBudget() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.depenseBudgetManquant);

		if (!dhomHtSaisie().abs().add(dhomTvaSaisie().abs()).equals(dhomTtcSaisie().abs()))
			setDhomTvaSaisie(dhomTtcSaisie().subtract(dhomHtSaisie()));
		if (!dhomMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDhomMontantBudgetaire(calculMontantBudgetaire());

		if (codeExer() != null && codeExer().codeMarche().cmNiveau().intValue() != EOCodeExer.niveauEngageable())
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.codeExerNiveauPasCoherent);
		if (codeExer() != null && !codeExer().exercice().equals(exercice()))
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.codeExerExercicePasCoherent);
		if (codeExer() != null && !preDepenseBudget().exercice().equals(exercice()))
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.depenseBudgetExercicePasCoherent);
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(dhomHtSaisie(), dhomTvaSaisie(), preDepenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (codeExer() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.codeExerManquant);
		if (typeAchat() == null)
			throw new DepenseControleHorsMarcheException(DepenseControleHorsMarcheException.typeAchatManquant);
	}
}