/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommande.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommande extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> COMM_DATE = new ERXKey<NSTimestamp>("commDate");
	public static final ERXKey<NSTimestamp> COMM_DATE_CREATION = new ERXKey<NSTimestamp>("commDateCreation");
	public static final ERXKey<String> COMM_LIBELLE = new ERXKey<String>("commLibelle");
	public static final ERXKey<Integer> COMM_NUMERO = new ERXKey<Integer>("commNumero");
	public static final ERXKey<String> COMM_REFERENCE = new ERXKey<String>("commReference");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOArticle> ARTICLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOArticle>("articles");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> COMMANDE_BASCULE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule>("commandeBascule");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> COMMANDE_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>("commandeBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> COMMANDE_DOCUMENTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>("commandeDocuments");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> COMMANDE_ENGAGEMENTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>("commandeEngagements");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> COMMANDE_IMPRESSIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression>("commandeImpressions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> COMMANDE_UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur>("commandeUtilisateur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODevise> DEVISE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODevise>("devise");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("fournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> TO_B2B_CXML_ORDER_REQUESTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest>("toB2bCxmlOrderRequests");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> TO_B2B_CXML_PUNCHOUTMSGS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg>("toB2bCxmlPunchoutmsgs");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtat");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_IMPRIMABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatImprimable");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "commId";

	public static final String COMM_DATE_KEY = "commDate";
	public static final String COMM_DATE_CREATION_KEY = "commDateCreation";
	public static final String COMM_LIBELLE_KEY = "commLibelle";
	public static final String COMM_NUMERO_KEY = "commNumero";
	public static final String COMM_REFERENCE_KEY = "commReference";

//Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DEV_ID_KEY = "devId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYET_ID_IMPRIMABLE_KEY = "tyetIdImprimable";
	public static final String ULT_ORDRE_KEY = "ultOrdre";

//Colonnes dans la base de donnees
	public static final String COMM_DATE_COLKEY = "COMM_DATE";
	public static final String COMM_DATE_CREATION_COLKEY = "COMM_DATE_CREATION";
	public static final String COMM_LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String COMM_NUMERO_COLKEY = "COMM_NUMERO";
	public static final String COMM_REFERENCE_COLKEY = "COMM_REFERENCE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DEV_ID_COLKEY = "DEV_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYET_ID_IMPRIMABLE_COLKEY = "TYET_ID_IMPRIMABLE";
	public static final String ULT_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ARTICLES_KEY = "articles";
	public static final String COMMANDE_BASCULE_KEY = "commandeBascule";
	public static final String COMMANDE_BUDGETS_KEY = "commandeBudgets";
	public static final String COMMANDE_DOCUMENTS_KEY = "commandeDocuments";
	public static final String COMMANDE_ENGAGEMENTS_KEY = "commandeEngagements";
	public static final String COMMANDE_IMPRESSIONS_KEY = "commandeImpressions";
	public static final String COMMANDE_UTILISATEUR_KEY = "commandeUtilisateur";
	public static final String DEVISE_KEY = "devise";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String TO_B2B_CXML_ORDER_REQUESTS_KEY = "toB2bCxmlOrderRequests";
	public static final String TO_B2B_CXML_PUNCHOUTMSGS_KEY = "toB2bCxmlPunchoutmsgs";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_IMPRIMABLE_KEY = "typeEtatImprimable";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public NSTimestamp commDate() {
	 return (NSTimestamp) storedValueForKey(COMM_DATE_KEY);
	}

	public void setCommDate(NSTimestamp value) {
	 takeStoredValueForKey(value, COMM_DATE_KEY);
	}

	public NSTimestamp commDateCreation() {
	 return (NSTimestamp) storedValueForKey(COMM_DATE_CREATION_KEY);
	}

	public void setCommDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, COMM_DATE_CREATION_KEY);
	}

	public String commLibelle() {
	 return (String) storedValueForKey(COMM_LIBELLE_KEY);
	}

	public void setCommLibelle(String value) {
	 takeStoredValueForKey(value, COMM_LIBELLE_KEY);
	}

	public Integer commNumero() {
	 return (Integer) storedValueForKey(COMM_NUMERO_KEY);
	}

	public void setCommNumero(Integer value) {
	 takeStoredValueForKey(value, COMM_NUMERO_KEY);
	}

	public String commReference() {
	 return (String) storedValueForKey(COMM_REFERENCE_KEY);
	}

	public void setCommReference(String value) {
	 takeStoredValueForKey(value, COMM_REFERENCE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EODevise devise() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODevise)storedValueForKey(DEVISE_KEY);
	}

	public void setDeviseRelationship(org.cocktail.fwkcktldepense.server.metier.EODevise value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODevise oldValue = devise();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEVISE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEVISE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = fournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
	}

	public void setTypeEtatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatImprimable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_IMPRIMABLE_KEY);
	}

	public void setTypeEtatImprimableRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatImprimable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_IMPRIMABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_IMPRIMABLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle> articles() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle>)storedValueForKey(ARTICLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle> articles(EOQualifier qualifier) {
	 return articles(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle> articles(EOQualifier qualifier, boolean fetch) {
	 return articles(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle> articles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOArticle.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOArticle.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = articles();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOArticle>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToArticlesRelationship(org.cocktail.fwkcktldepense.server.metier.EOArticle object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
	}
	
	public void removeFromArticlesRelationship(org.cocktail.fwkcktldepense.server.metier.EOArticle object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOArticle createArticlesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOArticle.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOArticle) eo;
	}
	
	public void deleteArticlesRelationship(org.cocktail.fwkcktldepense.server.metier.EOArticle object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllArticlesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOArticle> objects = articles().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteArticlesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> commandeBascule() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule>)storedValueForKey(COMMANDE_BASCULE_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> commandeBascule(EOQualifier qualifier) {
	 return commandeBascule(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> commandeBascule(EOQualifier qualifier, boolean fetch) {
	 return commandeBascule(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> commandeBascule(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule.COMMANDE_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeBascule();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeBasculeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_BASCULE_KEY);
	}
	
	public void removeFromCommandeBasculeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BASCULE_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule createCommandeBasculeRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_BASCULE_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule) eo;
	}
	
	public void deleteCommandeBasculeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BASCULE_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeBasculeRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> objects = commandeBascule().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeBasculeRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> commandeBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>)storedValueForKey(COMMANDE_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> commandeBudgets(EOQualifier qualifier) {
	 return commandeBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> commandeBudgets(EOQualifier qualifier, boolean fetch) {
	 return commandeBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> commandeBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_BUDGETS_KEY);
	}
	
	public void removeFromCommandeBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget createCommandeBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget) eo;
	}
	
	public void deleteCommandeBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> objects = commandeBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeBudgetsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> commandeDocuments() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)storedValueForKey(COMMANDE_DOCUMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> commandeDocuments(EOQualifier qualifier) {
	 return commandeDocuments(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> commandeDocuments(EOQualifier qualifier, boolean fetch) {
	 return commandeDocuments(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> commandeDocuments(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeDocuments();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_DOCUMENTS_KEY);
	}
	
	public void removeFromCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_DOCUMENTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument createCommandeDocumentsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_DOCUMENTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument) eo;
	}
	
	public void deleteCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_DOCUMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeDocumentsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> objects = commandeDocuments().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeDocumentsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>)storedValueForKey(COMMANDE_ENGAGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements(EOQualifier qualifier) {
	 return commandeEngagements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements(EOQualifier qualifier, boolean fetch) {
	 return commandeEngagements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> commandeEngagements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeEngagements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
	}
	
	public void removeFromCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement createCommandeEngagementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_ENGAGEMENTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement) eo;
	}
	
	public void deleteCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeEngagementsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement> objects = commandeEngagements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeEngagementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> commandeImpressions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression>)storedValueForKey(COMMANDE_IMPRESSIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> commandeImpressions(EOQualifier qualifier) {
	 return commandeImpressions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> commandeImpressions(EOQualifier qualifier, boolean fetch) {
	 return commandeImpressions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> commandeImpressions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeImpressions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeImpressionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_IMPRESSIONS_KEY);
	}
	
	public void removeFromCommandeImpressionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_IMPRESSIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression createCommandeImpressionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_IMPRESSIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression) eo;
	}
	
	public void deleteCommandeImpressionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_IMPRESSIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeImpressionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> objects = commandeImpressions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeImpressionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> commandeUtilisateur() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur>)storedValueForKey(COMMANDE_UTILISATEUR_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> commandeUtilisateur(EOQualifier qualifier) {
	 return commandeUtilisateur(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> commandeUtilisateur(EOQualifier qualifier, boolean fetch) {
	 return commandeUtilisateur(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> commandeUtilisateur(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = commandeUtilisateur();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCommandeUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_UTILISATEUR_KEY);
	}
	
	public void removeFromCommandeUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_UTILISATEUR_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur createCommandeUtilisateurRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_UTILISATEUR_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur) eo;
	}
	
	public void deleteCommandeUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_UTILISATEUR_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCommandeUtilisateurRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeUtilisateur> objects = commandeUtilisateur().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCommandeUtilisateurRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> toB2bCxmlOrderRequests() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest>)storedValueForKey(TO_B2B_CXML_ORDER_REQUESTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> toB2bCxmlOrderRequests(EOQualifier qualifier) {
	 return toB2bCxmlOrderRequests(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> toB2bCxmlOrderRequests(EOQualifier qualifier, boolean fetch) {
	 return toB2bCxmlOrderRequests(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> toB2bCxmlOrderRequests(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest.TO_COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toB2bCxmlOrderRequests();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToB2bCxmlOrderRequestsRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ORDER_REQUESTS_KEY);
	}
	
	public void removeFromToB2bCxmlOrderRequestsRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ORDER_REQUESTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest createToB2bCxmlOrderRequestsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_B2B_CXML_ORDER_REQUESTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest) eo;
	}
	
	public void deleteToB2bCxmlOrderRequestsRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ORDER_REQUESTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToB2bCxmlOrderRequestsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> objects = toB2bCxmlOrderRequests().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToB2bCxmlOrderRequestsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> toB2bCxmlPunchoutmsgs() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg>)storedValueForKey(TO_B2B_CXML_PUNCHOUTMSGS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> toB2bCxmlPunchoutmsgs(EOQualifier qualifier) {
	 return toB2bCxmlPunchoutmsgs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> toB2bCxmlPunchoutmsgs(EOQualifier qualifier, boolean fetch) {
	 return toB2bCxmlPunchoutmsgs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> toB2bCxmlPunchoutmsgs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg.TO_COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toB2bCxmlPunchoutmsgs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToB2bCxmlPunchoutmsgsRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
	}
	
	public void removeFromToB2bCxmlPunchoutmsgsRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg createToB2bCxmlPunchoutmsgsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg) eo;
	}
	
	public void deleteToB2bCxmlPunchoutmsgsRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToB2bCxmlPunchoutmsgsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg> objects = toB2bCxmlPunchoutmsgs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToB2bCxmlPunchoutmsgsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCommande avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommande createEOCommande(EOEditingContext editingContext				, NSTimestamp commDate
							, NSTimestamp commDateCreation
							, String commLibelle
									, org.cocktail.fwkcktldepense.server.metier.EODevise devise		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatImprimable		, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EOCommande eo = (EOCommande) EOUtilities.createAndInsertInstance(editingContext, _EOCommande.ENTITY_NAME);	 
							eo.setCommDate(commDate);
									eo.setCommDateCreation(commDateCreation);
									eo.setCommLibelle(commLibelle);
										 eo.setDeviseRelationship(devise);
				 eo.setExerciceRelationship(exercice);
				 eo.setFournisseurRelationship(fournisseur);
				 eo.setTypeEtatRelationship(typeEtat);
				 eo.setTypeEtatImprimableRelationship(typeEtatImprimable);
				 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommande creerInstance(EOEditingContext editingContext) {
		EOCommande object = (EOCommande)EOUtilities.createAndInsertInstance(editingContext, _EOCommande.ENTITY_NAME);
  		return object;
		}

	

  public EOCommande localInstanceIn(EOEditingContext editingContext) {
    EOCommande localInstance = (EOCommande)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommande> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommande> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> objectsForPreCommandesUtilisateurAvecDroits(EOEditingContext ec,
														String attLibelleBinding,
														String commLibelleBinding,
														Integer commNumeroBinding,
														String commReferenceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur createurBinding,
														NSTimestamp dateDebutBinding,
														NSTimestamp dateFinBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														String fouCodeBinding,
														String fouNomBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														String marLibelleBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("PreCommandesUtilisateurAvecDroits", EOCommande.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (attLibelleBinding != null)
			bindings.takeValueForKey(attLibelleBinding, "attLibelle");
		  if (commLibelleBinding != null)
			bindings.takeValueForKey(commLibelleBinding, "commLibelle");
		  if (commNumeroBinding != null)
			bindings.takeValueForKey(commNumeroBinding, "commNumero");
		  if (commReferenceBinding != null)
			bindings.takeValueForKey(commReferenceBinding, "commReference");
		  if (createurBinding != null)
			bindings.takeValueForKey(createurBinding, "createur");
		  if (dateDebutBinding != null)
			bindings.takeValueForKey(dateDebutBinding, "dateDebut");
		  if (dateFinBinding != null)
			bindings.takeValueForKey(dateFinBinding, "dateFin");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (marLibelleBinding != null)
			bindings.takeValueForKey(marLibelleBinding, "marLibelle");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> objectsForPreCommandesUtilisateurSansDroit(EOEditingContext ec,
														String attLibelleBinding,
														String commLibelleBinding,
														Integer commNumeroBinding,
														String commReferenceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur createurBinding,
														NSTimestamp dateDebutBinding,
														NSTimestamp dateFinBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														String fouCodeBinding,
														String fouNomBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														String marLibelleBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("PreCommandesUtilisateurSansDroit", EOCommande.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (attLibelleBinding != null)
			bindings.takeValueForKey(attLibelleBinding, "attLibelle");
		  if (commLibelleBinding != null)
			bindings.takeValueForKey(commLibelleBinding, "commLibelle");
		  if (commNumeroBinding != null)
			bindings.takeValueForKey(commNumeroBinding, "commNumero");
		  if (commReferenceBinding != null)
			bindings.takeValueForKey(commReferenceBinding, "commReference");
		  if (createurBinding != null)
			bindings.takeValueForKey(createurBinding, "createur");
		  if (dateDebutBinding != null)
			bindings.takeValueForKey(dateDebutBinding, "dateDebut");
		  if (dateFinBinding != null)
			bindings.takeValueForKey(dateFinBinding, "dateFin");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (marLibelleBinding != null)
			bindings.takeValueForKey(marLibelleBinding, "marLibelle");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> objectsForRecherche(EOEditingContext ec,
														String attLibelleBinding,
														String cmCodeBinding,
														String cmLibBinding,
														String commLibelleBinding,
														Integer commNumeroBinding,
														String commReferenceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur createurBinding,
														NSTimestamp dateDebutBinding,
														NSTimestamp dateFinBinding,
														String dppNumeroFactureBinding,
														Integer engNumeroBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														String fouCodeBinding,
														String fouNomBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														String marLibelleBinding,
														java.math.BigDecimal montantFactureHTMaxBinding,
														java.math.BigDecimal montantFactureHTMinBinding,
														java.math.BigDecimal montantFactureTTCMaxBinding,
														java.math.BigDecimal montantFactureTTCMinBinding,
														String organCrBinding,
														String organSsCrBinding,
														String organUbBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCommande.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (attLibelleBinding != null)
			bindings.takeValueForKey(attLibelleBinding, "attLibelle");
		  if (cmCodeBinding != null)
			bindings.takeValueForKey(cmCodeBinding, "cmCode");
		  if (cmLibBinding != null)
			bindings.takeValueForKey(cmLibBinding, "cmLib");
		  if (commLibelleBinding != null)
			bindings.takeValueForKey(commLibelleBinding, "commLibelle");
		  if (commNumeroBinding != null)
			bindings.takeValueForKey(commNumeroBinding, "commNumero");
		  if (commReferenceBinding != null)
			bindings.takeValueForKey(commReferenceBinding, "commReference");
		  if (createurBinding != null)
			bindings.takeValueForKey(createurBinding, "createur");
		  if (dateDebutBinding != null)
			bindings.takeValueForKey(dateDebutBinding, "dateDebut");
		  if (dateFinBinding != null)
			bindings.takeValueForKey(dateFinBinding, "dateFin");
		  if (dppNumeroFactureBinding != null)
			bindings.takeValueForKey(dppNumeroFactureBinding, "dppNumeroFacture");
		  if (engNumeroBinding != null)
			bindings.takeValueForKey(engNumeroBinding, "engNumero");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (marLibelleBinding != null)
			bindings.takeValueForKey(marLibelleBinding, "marLibelle");
		  if (montantFactureHTMaxBinding != null)
			bindings.takeValueForKey(montantFactureHTMaxBinding, "montantFactureHTMax");
		  if (montantFactureHTMinBinding != null)
			bindings.takeValueForKey(montantFactureHTMinBinding, "montantFactureHTMin");
		  if (montantFactureTTCMaxBinding != null)
			bindings.takeValueForKey(montantFactureTTCMaxBinding, "montantFactureTTCMax");
		  if (montantFactureTTCMinBinding != null)
			bindings.takeValueForKey(montantFactureTTCMinBinding, "montantFactureTTCMin");
		  if (organCrBinding != null)
			bindings.takeValueForKey(organCrBinding, "organCr");
		  if (organSsCrBinding != null)
			bindings.takeValueForKey(organSsCrBinding, "organSsCr");
		  if (organUbBinding != null)
			bindings.takeValueForKey(organUbBinding, "organUb");
		  if (typeEtatBinding != null)
			bindings.takeValueForKey(typeEtatBinding, "typeEtat");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommande> objectsForRechercheGlobale(EOEditingContext ec,
														String commLibelleBinding,
														Integer commNumeroBinding,
														String commReferenceBinding,
														String fouCodeBinding,
														String fouNomBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("RechercheGlobale", EOCommande.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (commLibelleBinding != null)
			bindings.takeValueForKey(commLibelleBinding, "commLibelle");
		  if (commNumeroBinding != null)
			bindings.takeValueForKey(commNumeroBinding, "commNumero");
		  if (commReferenceBinding != null)
			bindings.takeValueForKey(commReferenceBinding, "commReference");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
