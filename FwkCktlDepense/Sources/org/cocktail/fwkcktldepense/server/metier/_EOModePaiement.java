/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOModePaiement.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOModePaiement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleModePaiement";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_MODE_PAIEMENT";


//Attribute Keys
	public static final ERXKey<String> MOD_CODE = new ERXKey<String>("modCode");
	public static final ERXKey<String> MOD_DOM = new ERXKey<String>("modDom");
	public static final ERXKey<String> MOD_EMA_AUTO = new ERXKey<String>("modEmaAuto");
	public static final ERXKey<String> MOD_LIBELLE = new ERXKey<String>("modLibelle");
	public static final ERXKey<String> MOD_VALIDITE = new ERXKey<String>("modValidite");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE_VISA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptableVisa");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "modOrdre";

	public static final String MOD_CODE_KEY = "modCode";
	public static final String MOD_DOM_KEY = "modDom";
	public static final String MOD_EMA_AUTO_KEY = "modEmaAuto";
	public static final String MOD_LIBELLE_KEY = "modLibelle";
	public static final String MOD_VALIDITE_KEY = "modValidite";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String PCO_NUM_VISA_KEY = "pcoNumVisa";

//Colonnes dans la base de donnees
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String MOD_EMA_AUTO_COLKEY = "MOD_EMA_AUTO";
	public static final String MOD_LIBELLE_COLKEY = "MOD_LIBELLE";
	public static final String MOD_VALIDITE_COLKEY = "MOD_VALIDITE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String PCO_NUM_VISA_COLKEY = "PCO_NUM_VISA";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_VISA_KEY = "planComptableVisa";



	// Accessors methods
	public String modCode() {
	 return (String) storedValueForKey(MOD_CODE_KEY);
	}

	public void setModCode(String value) {
	 takeStoredValueForKey(value, MOD_CODE_KEY);
	}

	public String modDom() {
	 return (String) storedValueForKey(MOD_DOM_KEY);
	}

	public void setModDom(String value) {
	 takeStoredValueForKey(value, MOD_DOM_KEY);
	}

	public String modEmaAuto() {
	 return (String) storedValueForKey(MOD_EMA_AUTO_KEY);
	}

	public void setModEmaAuto(String value) {
	 takeStoredValueForKey(value, MOD_EMA_AUTO_KEY);
	}

	public String modLibelle() {
	 return (String) storedValueForKey(MOD_LIBELLE_KEY);
	}

	public void setModLibelle(String value) {
	 takeStoredValueForKey(value, MOD_LIBELLE_KEY);
	}

	public String modValidite() {
	 return (String) storedValueForKey(MOD_VALIDITE_KEY);
	}

	public void setModValidite(String value) {
	 takeStoredValueForKey(value, MOD_VALIDITE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptableVisa() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_VISA_KEY);
	}

	public void setPlanComptableVisaRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptableVisa();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_VISA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_VISA_KEY);
	 }
	}


	/**
	* Créer une instance de EOModePaiement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOModePaiement createEOModePaiement(EOEditingContext editingContext				, String modCode
							, String modDom
							, String modEmaAuto
							, String modLibelle
							, String modValidite
					, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice							) {
	 EOModePaiement eo = (EOModePaiement) EOUtilities.createAndInsertInstance(editingContext, _EOModePaiement.ENTITY_NAME);	 
							eo.setModCode(modCode);
									eo.setModDom(modDom);
									eo.setModEmaAuto(modEmaAuto);
									eo.setModLibelle(modLibelle);
									eo.setModValidite(modValidite);
						 eo.setExerciceRelationship(exercice);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOModePaiement creerInstance(EOEditingContext editingContext) {
		EOModePaiement object = (EOModePaiement)EOUtilities.createAndInsertInstance(editingContext, _EOModePaiement.ENTITY_NAME);
  		return object;
		}

	

  public EOModePaiement localInstanceIn(EOEditingContext editingContext) {
    EOModePaiement localInstance = (EOModePaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOModePaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOModePaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOModePaiement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOModePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOModePaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOModePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOModePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOModePaiement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOModePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOModePaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOModePaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOModePaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOModePaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOModePaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> objectsForRecherche(EOEditingContext ec,
														String modCodeBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOModePaiement.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (modCodeBinding != null)
			bindings.takeValueForKey(modCodeBinding, "modCode");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
