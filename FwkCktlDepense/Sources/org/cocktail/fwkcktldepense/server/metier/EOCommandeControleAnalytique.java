

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;


import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCommandeControleAnalytique extends _EOCommandeControleAnalytique
{
	private static Calculs serviceCalculs = Calculs.getInstance();
	
    public EOCommandeControleAnalytique() {
        super();
    }

    public BigDecimal montantTtc() {
    	return canaTtcSaisie();
    }
    
    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
    	
    	if (commandeBudget()!=null && commandeBudget().cbudTtcSaisie()!=null && commandeBudget().cbudTtcSaisie().floatValue()!=0.0)
    		setCanaPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(commandeBudget().cbudTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setCanaHtSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCanaHtSaisie(aValue);
    }

    public void setCanaTvaSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCanaTvaSaisie(aValue);
    }

    public void setCanaTtcSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCanaTtcSaisie(aValue);
    }

    public void setCanaMontantBudgetaire(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCanaMontantBudgetaire(aValue);
    }

    public void setCanaPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
        super.setCanaPourcentage(aValue);
        
    	if (commandeBudget()!=null) {
    		NSArray commandeControleAnalytiques = commandeBudget().commandeControleAnalytiques();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControleAnalytique.CODE_ANALYTIQUE_KEY+" != nil", null);
    		commandeControleAnalytiques = EOQualifier.filteredArrayWithQualifier(commandeControleAnalytiques, qual);
    		BigDecimal total=commandeBudget().computeSumForKey(commandeControleAnalytiques, EOCommandeControleAnalytique.CANA_POURCENTAGE_KEY);
    		if (total.floatValue()>100.0)
    	        super.setCanaPourcentage(new BigDecimal(100.0).subtract(total.subtract(aValue)));    			
    	}

        if (commandeBudget()!=null)
        	commandeBudget().corrigerMontantAnalytiques();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws CommandeControleAnalytiqueException {
        if (canaHtSaisie()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.canaHtSaisieManquant);
        if (canaTvaSaisie()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.canaTvaSaisieManquant);
        if (canaTtcSaisie()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.canaTtcSaisieManquant);
        if (canaMontantBudgetaire()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.canaMontantBudgetaireManquant);
        if (canaPourcentage()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.canaPourcentageManquant);
        if (exercice()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.exerciceManquant);
        if (commandeBudget()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.commandeBudgetManquant);
        
        if (canaTvaSaisie().floatValue()<0.0 || canaHtSaisie().floatValue()<0.0 || canaTtcSaisie().floatValue()<0.0 ||
        		canaMontantBudgetaire().floatValue()<0.0)
        	throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.montantsNegatifs);

        if (!commandeBudget().exercice().equals(exercice()))
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.commandeBudgetExercicePasCoherent);
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        if (codeAnalytique()==null)
            throw new CommandeControleAnalytiqueException(CommandeControleAnalytiqueException.codeAnalytiqueManquant);

        if (!canaHtSaisie().abs().add(canaTvaSaisie().abs()).equals(canaTtcSaisie().abs()))
        	setCanaTvaSaisie(canaTtcSaisie().subtract(canaHtSaisie()));
        if (commandeBudget().tauxProrata()!=null && !canaMontantBudgetaire().equals(calculMontantBudgetaire()))
        	setCanaMontantBudgetaire(calculMontantBudgetaire());
    }

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(canaHtSaisie(), canaTvaSaisie(), commandeBudget().tauxProrata().tapTaux());
	}
}
