/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;
import java.util.Date;

import org.cocktail.fwkcktldepense.server.FwkCktlDepenseHelper;
import org.cocktail.fwkcktldepense.server.exception.EngagementBudgetException;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.finder.FinderDevise;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderOrgan;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeAction;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeEtat;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.util.Calculs;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOEngagementBudget extends _EOEngagementBudget implements ISourceRepartitionCredit  {
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	public static final String ETAT_ENGAGE = "ENGAGE";
	public static final String ETAT_PARTIELLEMENT_SOLDE = "PART_SOLDE";
	public static final String ETAT_SOLDE = "SOLDE";

	private Number engIdProc;
	private int nbDecimales;

	private BigDecimal minBudgetaire = null;

	private NSArray arrayActions;
	private NSArray arrayAnalytiques;
	private NSArray arrayConventions;
	private NSArray arrayPlanComptables;
	private NSArray arrayCodeExers;

	private EOTypeAchat typeAchat;

	public EOCommande commande() {
		if (commandeEngagements() == null || commandeEngagements().count() == 0)
			return null;
		EOCommandeEngagement commandeEngagement = (EOCommandeEngagement) commandeEngagements().objectAtIndex(0);
		return commandeEngagement.commande();
	}

	public EOEngagementBudget() {
		super();
		engIdProc = null;
		nbDecimales = EODevise.defaultNbDecimales;
		majDecimales();

		arrayActions = null;
		arrayAnalytiques = null;
		arrayConventions = null;
		arrayPlanComptables = null;
		arrayCodeExers = null;
		typeAchat = null;
	}

	public String libelleCourt() {
		String libelleCourt = engLibelle();

		if (libelleCourt != null && libelleCourt.length() > 25)
			libelleCourt = libelleCourt.substring(0, 25);

		return libelleCourt;
	}

	public EOTypeEtat etat() {
		if (engMontantBudgetaireReste().floatValue() <= 0.0)
			return FinderTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.SOLDE);
		if (engMontantBudgetaireReste().floatValue() == engMontantBudgetaire().floatValue())
			return FinderTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.ENGAGE);

		return FinderTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.PART_SOLDE);
	}

	public boolean isSurUnMarche() {
		if (engagementControleMarches() == null)
			return false;
		return (engagementControleMarches().count() > 0);
	}

	public EOAttribution attribution() {
		if (engagementControleMarches() == null || engagementControleMarches().count() == 0)
			return null;
		return ((EOEngagementControleMarche) engagementControleMarches().objectAtIndex(0)).attribution();
	}

	public EOTypeAchat typAchat() {
		if (engagementControleHorsMarches() == null || engagementControleHorsMarches().count() == 0)
			return null;
		return ((EOEngagementControleHorsMarche) engagementControleHorsMarches().objectAtIndex(0)).typeAchat();
	}

	public boolean isConsultable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		// si l'utilisateur n'a pas les droits sur la ligne budgetaire alors non
		if (organs == null)
			organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

		// Si l'utilisateur n'a aucun droit sur les lignes budgetaires alors non
		if (organs.count() == 0)
			return false;

		if (!organs.containsObject(organ()))
			return false;

		// l'utilisateur a les droits necessaires alors oui
		return true;
	}

	public boolean isSupprimable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		if (typeApplication() == null || !typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
			return false;

		if (depenseBudgets() != null && depenseBudgets().count() > 0)
			return false;

		if (!FinderExercice.getExercicesEngageablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
			return false;

		return isConsultable(ed, utilisateur, organs);
	}

	/**
	 * @param ed
	 * @param utilisateur
	 * @param organs
	 * @return true notamment si engagement genere a partir de DEPENSE.
	 */
	public boolean isReimputable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		if (typeApplication() == null || !typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
			return false;

		return isConsultable(ed, utilisateur, organs);
	}

	public boolean isSoldable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {
		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		if (engMontantBudgetaireReste().floatValue() == 0.0 || engMontantBudgetaireReste().floatValue() == engMontantBudgetaire().floatValue())
			return false;

		if (!typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
			return false;

		// si l'utilisateur n'a pas le droit d'engager sur cet exercice alors non
		if (!FinderExercice.getExercicesEngageablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
			return false;

		return isConsultable(ed, utilisateur, organs);
	}

	public boolean isModifiable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {
		return isSupprimable(ed, utilisateur, organs);
	}

	////  DEBUT MODIF ENGAGEMENT SANS COMMANDE

	public EOTypeAchat typeAchat() {
		if (typeAchat != null)
			return typeAchat;

		if (engagementControleHorsMarches() == null || engagementControleHorsMarches().count() == 0)
			return null;
		return ((EOEngagementControleHorsMarche) engagementControleHorsMarches().objectAtIndex(0)).typeAchat();
	}

	public String type() {
		String type = null;

		if (typeAchat() != null) {
			type = typeAchat().typaLibelle();
		}
		else if (engagementControleMarches() != null && engagementControleMarches().count() > 0) {
			type = "MARCHE";
		}
		else {
			type = "???";
		}
		return type;
	}

	public void setTypeAchat(EOTypeAchat value) {
		typeAchat = value;
	}

	public BigDecimal minBudgetaire() {
		if (minBudgetaire == null) {
			if (engMontantBudgetaire() == null)
				minBudgetaire = new BigDecimal(0.0);
			else
				minBudgetaire = engMontantBudgetaire();
		}
		return minBudgetaire;
	}

	public BigDecimal restantHtControleAction() {
		return engHtSaisie().subtract(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControleAction() {
		return engTtcSaisie().subtract(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_TTC_SAISIE_KEY));
	}

	public BigDecimal restantHtControleAnalytique() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleAnalytique() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleConvention() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleConvention() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleHorsMarche() {
		return engHtSaisie().subtract(computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControleHorsMarche() {
		return engTtcSaisie().subtract(computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_TTC_SAISIE_KEY));
	}

	public BigDecimal restantHtControleMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControlePlanComptable() {
		return engHtSaisie().subtract(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControlePlanComptable() {
		return engTtcSaisie().subtract(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_TTC_SAISIE_KEY));
	}

	public NSArray getTypeActionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayActions == null && source() != null && utilisateur != null)
			arrayActions = FinderTypeAction.getTypeActions(ed, source(), utilisateur);
		if (arrayActions == null)
			arrayActions = new NSArray();

		if (arrayActions.count() == 0)
			return arrayActions;

		if (engagementControleActions() != null && engagementControleActions().count() > 0) {
			NSMutableArray resultats = new NSMutableArray(arrayActions);
			for (int i = 0; i < engagementControleActions().count(); i++) {
				EOTypeAction unTypeAction = ((EOEngagementControleAction) engagementControleActions().objectAtIndex(i)).typeAction();
				if (unTypeAction != null)
					resultats.removeObject(unTypeAction);
			}
			return resultats;
		}

		return arrayActions;
	}

	public NSArray getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques == null && source() != null && utilisateur != null)
			arrayAnalytiques = FinderCodeAnalytique.getCodeAnalytiques(ed, source().organ(), source().exercice());
		if (arrayAnalytiques == null)
			arrayAnalytiques = new NSArray();

		if (arrayAnalytiques.count() == 0)
			return arrayAnalytiques;

		if (engagementControleAnalytiques() != null && engagementControleAnalytiques().count() > 0) {
			NSMutableArray resultats = new NSMutableArray(arrayAnalytiques);
			for (int i = 0; i < engagementControleAnalytiques().count(); i++) {
				EOCodeAnalytique unCodeAnalytique = ((EOEngagementControleAnalytique) engagementControleAnalytiques().objectAtIndex(i)).codeAnalytique();
				if (unCodeAnalytique != null)
					resultats.removeObject(unCodeAnalytique);
			}

			return resultats;
		}

		return arrayAnalytiques;
	}

	public NSArray getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null && source() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(source().organ(), "organ");
			bindings.setObjectForKey(source().typeCredit(), "typeCredit");
			bindings.setObjectForKey(source().exercice(), "exercice");
			bindings.setObjectForKey(new Boolean(true), "montants");
			//bindings.setObjectForKey(EOConventionNonLimitative.MODE_GESTION_RESSOURCE_AFFECTEE, "convModeGestion");
			NSTimestamp dateClotureConventionMax = new NSTimestamp(new FwkCktlDepenseHelper().corrigeDateSelonExercice(new Date(), this.exercice().exeExercice()));
			bindings.setObjectForKey(dateClotureConventionMax, "conDateClotureMax");
			arrayConventions = FinderConvention.getConventions(ed, bindings);
		}
		if (arrayConventions == null)
			arrayConventions = new NSArray();

		if (arrayConventions.count() == 0)
			return arrayConventions;

		if (engagementControleConventions() != null && engagementControleConventions().count() > 0) {
			NSMutableArray resultats = new NSMutableArray(arrayConventions);
			for (int i = 0; i < engagementControleConventions().count(); i++) {
				EOConvention uneConvention = ((EOEngagementControleConvention) engagementControleConventions().objectAtIndex(i)).convention();
				if (uneConvention != null)
					resultats.removeObject(uneConvention);
			}

			return resultats;
		}

		return arrayConventions;
	}

	public NSArray getCodeExersPossibles(EOEditingContext ed, EOUtilisateur utilisateurs) {
		if (arrayCodeExers == null && typeAchat() != null && exercice() != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(typeAchat(), "typeAchat");
			bindings.setObjectForKey(exercice(), "exercice");

			arrayCodeExers = FinderCodeExer.getCodeExerValides(ed, bindings);
		}
		if (arrayCodeExers == null)
			arrayCodeExers = new NSArray();

		if (arrayCodeExers.count() == 0)
			return arrayCodeExers;

		if (engagementControleHorsMarches() != null && engagementControleHorsMarches().count() > 0) {
			NSMutableArray resultats = new NSMutableArray(arrayCodeExers);
			for (int i = 0; i < engagementControleHorsMarches().count(); i++) {
				EOCodeExer unCodeExer = ((EOEngagementControleHorsMarche) engagementControleHorsMarches().objectAtIndex(i)).codeExer();
				if (unCodeExer != null)
					resultats.removeObject(unCodeExer);
			}

			return resultats;
		}

		return arrayCodeExers;
	}

	public NSArray getPlanComptablesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayPlanComptables == null && source() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(source().typeCredit(), "typeCredit");
			bindings.setObjectForKey(utilisateur, "utilisateur");
			bindings.setObjectForKey(source().exercice(), "exercice");
			arrayPlanComptables = FinderPlanComptable.getPlanComptables(ed, bindings);
		}
		if (arrayPlanComptables == null)
			arrayPlanComptables = new NSArray();

		if (arrayPlanComptables.count() == 0)
			return arrayPlanComptables;

		if (engagementControlePlanComptables() != null && engagementControlePlanComptables().count() > 0) {
			NSMutableArray resultats = new NSMutableArray(arrayPlanComptables);
			for (int i = 0; i < engagementControlePlanComptables().count(); i++) {
				EOPlanComptable unPlanComptable = ((EOEngagementControlePlanComptable) engagementControlePlanComptables().objectAtIndex(i)).planComptable();
				if (unPlanComptable != null)
					resultats.removeObject(unPlanComptable);
			}

			return resultats;
		}

		return arrayPlanComptables;
	}

	public boolean isControleActionGood() {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControleAction.TYPE_ACTION_KEY + "=nil", null);
		NSArray array = EOQualifier.filteredArrayWithQualifier(engagementControleActions(), qual);
		if (array != null && array.count() > 0)
			return false;

		if (computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_POURCENTAGE).floatValue() != 100)
			return false;
		if (restantHtControleAction().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControleAction().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (engMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isControleAnalytiqueGood() {
		float sommePourcentage = computeSumForKey(engagementControleAnalytiques(), EOEngagementControleAnalytique.EANA_POURCENTAGE).floatValue();
		if (sommePourcentage < 0 || sommePourcentage > 100)
			return false;
		return true;
	}

	public boolean isControleConventionGood() {
		float sommePourcentage = computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_POURCENTAGE).floatValue();
		if (sommePourcentage < 0 || sommePourcentage > 100)
			return false;
		return true;
	}

	public boolean isControleHorsMarcheGood() {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControleHorsMarche.CODE_EXER_KEY + "=nil", null);
		NSArray array = EOQualifier.filteredArrayWithQualifier(engagementControleHorsMarches(), qual);
		if (array != null && array.count() > 0)
			return false;

		if (computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_POURCENTAGE).floatValue() != 100)
			return false;
		if (restantHtControleHorsMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControleHorsMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (engMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isControleMarcheGood() {
		if (restantHtControleMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControleMarche().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (engMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	public boolean isControlePlanComptableGood() {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControlePlanComptable.PLAN_COMPTABLE_KEY + "=nil", null);
		NSArray array = EOQualifier.filteredArrayWithQualifier(engagementControlePlanComptables(), qual);
		if (array != null && array.count() > 0)
			return false;

		if (computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_POURCENTAGE).floatValue() != 100)
			return false;
		if (restantHtControlePlanComptable().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (restantTtcControlePlanComptable().compareTo(new BigDecimal(0.0)) != 0)
			return false;
		if (engMontantBudgetaire().compareTo(minBudgetaire()) == -1)
			return false;
		return true;
	}

	protected void corrigerMontant() {
		// correction des controleurs
		corrigerMontantActions();
		corrigerMontantAnalytiques();
		corrigerMontantConventions();
		corrigerMontantHorsMarches();
		corrigerMontantMarches();
		corrigerMontantPlanComptables();
	}

	protected void corrigerMontantActions() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (engagementControleActions() == null || engagementControleActions().count() == 0)
			return;

		htReste = engHtSaisie();
		ttcReste = engTtcSaisie();
		budgetaireReste = engMontantBudgetaire();
		sommePourcentage = computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_POURCENTAGE);

		for (int i = 0; i < engagementControleActions().count(); i++) {
			EOEngagementControleAction engagement = (EOEngagementControleAction) engagementControleActions().objectAtIndex(i);

			boolean reste = (i == engagementControleActions().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, engagement.pourcentage(), engHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, engagement.pourcentage(), engTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, engagement.pourcentage(), engMontantBudgetaire(), reste);

			// on met a jour les montants
			if (engagement.eactHtSaisie() == null || engagement.eactHtSaisie().floatValue() != ht.floatValue())
				engagement.setEactHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (engagement.eactTvaSaisie() == null || engagement.eactTvaSaisie().floatValue() != tva.floatValue())
				engagement.setEactTvaSaisie(tva);
			if (engagement.eactTtcSaisie() == null || engagement.eactTtcSaisie().floatValue() != ttc.floatValue())
				engagement.setEactTtcSaisie(ttc);
			if (engagement.eactMontantBudgetaire() == null || engagement.eactMontantBudgetaire().floatValue() != budgetaire.floatValue())
				engagement.setEactMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantAnalytiques() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (engagementControleAnalytiques() == null || engagementControleAnalytiques().count() == 0)
			return;

		htReste = engHtSaisie();
		ttcReste = engTtcSaisie();
		budgetaireReste = engMontantBudgetaire();
		sommePourcentage = computeSumForKey(engagementControleAnalytiques(), EOEngagementControleAnalytique.EANA_POURCENTAGE);

		for (int i = 0; i < engagementControleAnalytiques().count(); i++) {
			EOEngagementControleAnalytique engagement = (EOEngagementControleAnalytique) engagementControleAnalytiques().objectAtIndex(i);

			boolean reste = (i == engagementControleAnalytiques().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, engagement.pourcentage(), engHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, engagement.pourcentage(), engTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, engagement.pourcentage(), engMontantBudgetaire(), reste);

			// on met a jour les montants
			if (engagement.eanaHtSaisie() == null || engagement.eanaHtSaisie().floatValue() != ht.floatValue())
				engagement.setEanaHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (engagement.eanaTvaSaisie() == null || engagement.eanaTvaSaisie().floatValue() != tva.floatValue())
				engagement.setEanaTvaSaisie(tva);
			if (engagement.eanaTtcSaisie() == null || engagement.eanaTtcSaisie().floatValue() != ttc.floatValue())
				engagement.setEanaTtcSaisie(ttc);
			if (engagement.eanaMontantBudgetaire() == null || engagement.eanaMontantBudgetaire().floatValue() != budgetaire.floatValue())
				engagement.setEanaMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantConventions() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (engagementControleConventions() == null || engagementControleConventions().count() == 0)
			return;

		htReste = engHtSaisie();
		ttcReste = engTtcSaisie();
		budgetaireReste = engMontantBudgetaire();
		sommePourcentage = computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_POURCENTAGE);

		for (int i = 0; i < engagementControleConventions().count(); i++) {
			EOEngagementControleConvention engagement = (EOEngagementControleConvention) engagementControleConventions().objectAtIndex(i);

			boolean reste = (i == engagementControleConventions().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, engagement.pourcentage(), engHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, engagement.pourcentage(), engTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, engagement.pourcentage(), engMontantBudgetaire(), reste);

			// on met a jour les montants
			if (engagement.econHtSaisie() == null || engagement.econHtSaisie().floatValue() != ht.floatValue())
				engagement.setEconHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (engagement.econTvaSaisie() == null || engagement.econTvaSaisie().floatValue() != tva.floatValue())
				engagement.setEconTvaSaisie(tva);
			if (engagement.econTtcSaisie() == null || engagement.econTtcSaisie().floatValue() != ttc.floatValue())
				engagement.setEconTtcSaisie(ttc);
			if (engagement.econMontantBudgetaire() == null || engagement.econMontantBudgetaire().floatValue() != budgetaire.floatValue())
				engagement.setEconMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantHorsMarches() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (engagementControleHorsMarches() == null || engagementControleHorsMarches().count() == 0)
			return;

		htReste = engHtSaisie();
		ttcReste = engTtcSaisie();
		budgetaireReste = engMontantBudgetaire();
		sommePourcentage = computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_POURCENTAGE);

		for (int i = 0; i < engagementControleHorsMarches().count(); i++) {
			EOEngagementControleHorsMarche engagement = (EOEngagementControleHorsMarche) engagementControleHorsMarches().objectAtIndex(i);

			boolean reste = (i == engagementControleHorsMarches().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, engagement.pourcentage(), engHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, engagement.pourcentage(), engTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, engagement.pourcentage(), engMontantBudgetaire(), reste);

			// on met a jour les montants
			if (engagement.ehomHtSaisie() == null || engagement.ehomHtSaisie().floatValue() != ht.floatValue())
				engagement.setEhomHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (engagement.ehomTvaSaisie() == null || engagement.ehomTvaSaisie().floatValue() != tva.floatValue())
				engagement.setEhomTvaSaisie(tva);
			if (engagement.ehomTtcSaisie() == null || engagement.ehomTtcSaisie().floatValue() != ttc.floatValue())
				engagement.setEhomTtcSaisie(ttc);
			if (engagement.ehomMontantBudgetaire() == null || engagement.ehomMontantBudgetaire().floatValue() != budgetaire.floatValue())
				engagement.setEhomMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	protected void corrigerMontantMarches() {
		if (engagementControleMarches() == null || engagementControleMarches().count() != 1)
			return;

		EOEngagementControleMarche engagementControleMarche = (EOEngagementControleMarche) engagementControleMarches().objectAtIndex(0);

		if (engHtSaisie().floatValue() != engagementControleMarche.emarHtSaisie().floatValue())
			engagementControleMarche.setEmarHtSaisie(engHtSaisie());
		if (engMontantBudgetaire().floatValue() != engagementControleMarche.emarMontantBudgetaire().floatValue())
			engagementControleMarche.setEmarMontantBudgetaire(engMontantBudgetaire());
		if (engTtcSaisie().floatValue() != engagementControleMarche.emarTtcSaisie().floatValue())
			engagementControleMarche.setEmarTtcSaisie(engTtcSaisie());
		if (engTvaSaisie().floatValue() != engagementControleMarche.emarTvaSaisie().floatValue())
			engagementControleMarche.setEmarTvaSaisie(engTvaSaisie());

	}

	protected void corrigerMontantPlanComptables() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (engagementControlePlanComptables() == null || engagementControlePlanComptables().count() == 0)
			return;

		htReste = engHtSaisie();
		ttcReste = engTtcSaisie();
		budgetaireReste = engMontantBudgetaire();
		sommePourcentage = computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_POURCENTAGE);

		for (int i = 0; i < engagementControlePlanComptables().count(); i++) {
			EOEngagementControlePlanComptable engagement = (EOEngagementControlePlanComptable) engagementControlePlanComptables().objectAtIndex(i);

			boolean reste = (i == engagementControlePlanComptables().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, engagement.pourcentage(), engHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, engagement.pourcentage(), engTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, engagement.pourcentage(), engMontantBudgetaire(), reste);

			// on met a jour les montants
			if (engagement.epcoHtSaisie() == null || engagement.epcoHtSaisie().floatValue() != ht.floatValue())
				engagement.setEpcoHtSaisie(ht);
			BigDecimal tva = ttc.subtract(ht);
			if (engagement.epcoTvaSaisie() == null || engagement.epcoTvaSaisie().floatValue() != tva.floatValue())
				engagement.setEpcoTvaSaisie(tva);
			if (engagement.epcoTtcSaisie() == null || engagement.epcoTtcSaisie().floatValue() != ttc.floatValue())
				engagement.setEpcoTtcSaisie(ttc);
			if (engagement.epcoMontantBudgetaire() == null || engagement.epcoMontantBudgetaire().floatValue() != budgetaire.floatValue())
				engagement.setEpcoMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(ht);
			ttcReste = ttcReste.subtract(ttc);
			budgetaireReste = budgetaireReste.subtract(budgetaire);
		}
	}

	private BigDecimal calculeMontant(BigDecimal sommePourcentage, BigDecimal montantReste, BigDecimal pourcentage, BigDecimal montant, boolean reste) {
		BigDecimal calcul;

		// si c'est le dernier et que le pourcentage est egal a 100 -> on met le reste
		if (sommePourcentage.floatValue() >= 100.0 && reste)
			return montantReste;

		// on calcule le montant par rapport au pourcentage
		int arrondi = decimalesPourArrondirMontant();

		calcul = pourcentage.multiply(montant).divide(new BigDecimal(100.0), arrondi, BigDecimal.ROUND_HALF_UP);

		// on verifie que le montant calcule ne depasse pas le reste
		if (calcul.compareTo(montantReste) == 1)
			calcul = montantReste;

		return calcul;
	}

	public boolean isComplet() {
		if (organ() == null)
			return false;
		if (tauxProrata() == null)
			return false;
		if (typeCredit() == null)
			return false;
		if (exercice() == null)
			return false;
		if (fournisseur() == null)
			return false;

		if (isControleActionGood() == false) {
			return false;
		}
		if (isControlePlanComptableGood() == false) {
			return false;
		}

		if (engHtSaisie().compareTo(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_HT_SAISIE_KEY)) != 0 ||
				engTvaSaisie().compareTo(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_TVA_SAISIE_KEY)) != 0 ||
				engTtcSaisie().compareTo(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_TTC_SAISIE_KEY)) != 0 ||
				engMontantBudgetaire().compareTo(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_MONTANT_BUDGETAIRE_KEY)) != 0)
			return false;

		if (engHtSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), EOEngagementControleAnalytique.EANA_HT_SAISIE_KEY)) == -1 ||
				engTvaSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), EOEngagementControleAnalytique.EANA_TVA_SAISIE_KEY)) == -1 ||
				engTtcSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), EOEngagementControleAnalytique.EANA_TTC_SAISIE_KEY)) == -1 ||
				engMontantBudgetaire().compareTo(computeSumForKey(engagementControleAnalytiques(), EOEngagementControleAnalytique.EANA_MONTANT_BUDGETAIRE_KEY)) == -1)
			return false;

		if (engHtSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_HT_SAISIE_KEY)) == -1 ||
				engTvaSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_TVA_SAISIE_KEY)) == -1 ||
				engTtcSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_TTC_SAISIE_KEY)) == -1 ||
				engMontantBudgetaire().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_MONTANT_BUDGETAIRE_KEY)) == -1)
			return false;

		if (engHtSaisie().compareTo(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_HT_SAISIE_KEY)) != 0 ||
				engTvaSaisie().compareTo(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_TVA_SAISIE_KEY)) != 0 ||
				engTtcSaisie().compareTo(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_TTC_SAISIE_KEY)) != 0 ||
				engMontantBudgetaire().compareTo(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_MONTANT_BUDGETAIRE_KEY)) != 0)
			return false;

		if (engagementControleHorsMarches() != null && engagementControleHorsMarches().count() > 0) {
			if (engHtSaisie().compareTo(computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_HT_SAISIE_KEY)) != 0 ||
					engTvaSaisie().compareTo(computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_TVA_SAISIE_KEY)) != 0 ||
					engTtcSaisie().compareTo(computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_TTC_SAISIE_KEY)) != 0 ||
					engMontantBudgetaire().compareTo(computeSumForKey(engagementControleHorsMarches(), EOEngagementControleHorsMarche.EHOM_MONTANT_BUDGETAIRE_KEY)) != 0)
				return false;

			if (engagementControleMarches() != null && engagementControleMarches().count() > 0)
				return false;
		}
		else {
			if (engagementControleMarches() == null || engagementControleMarches().count() == 0)
				return false;

			if (engHtSaisie().compareTo(computeSumForKey(engagementControleMarches(), EOEngagementControleMarche.EMAR_HT_SAISIE_KEY)) != 0 ||
					engTvaSaisie().compareTo(computeSumForKey(engagementControleMarches(), EOEngagementControleMarche.EMAR_TVA_SAISIE_KEY)) != 0 ||
					engTtcSaisie().compareTo(computeSumForKey(engagementControleMarches(), EOEngagementControleMarche.EMAR_TTC_SAISIE_KEY)) != 0 ||
					engMontantBudgetaire().compareTo(computeSumForKey(engagementControleMarches(), EOEngagementControleMarche.EMAR_MONTANT_BUDGETAIRE_KEY)) != 0)
				return false;
		}

		// verification des pourcentages
		if (new BigDecimal(100.0).compareTo(computeSumForKey(engagementControleActions(), EOEngagementControleAction.EACT_POURCENTAGE)) != 0)
			return false;
		if (new BigDecimal(100.0).compareTo(computeSumForKey(engagementControlePlanComptables(), EOEngagementControlePlanComptable.EPCO_POURCENTAGE)) != 0)
			return false;

		return true;
	}

	////  FIN MODIF ENGAGEMENT SANS COMMANDE

	public BigDecimal sourceDisponible() {
		return source().getDisponible();
		//		if (_ISourceCredit.ESourceCreditType.BUDGET.equals(source().type())) {
		//			NSArray<EOBudgetExecCredit> budgets = FinderBudget.getBudgetsPourSource(editingContext(), (EOSource) source());
		//
		//			if (budgets == null || budgets.count() == 0)
		//				return BigDecimal.ZERO;
		//
		//			return ((EOBudgetExecCredit) budgets.objectAtIndex(0)).bdxcDisponible();
		//		}
		//		else if (_ISourceCredit.ESourceCreditType.EXTOURNE.equals(source().type())) {
		//			return ()
		//		}
		//		//FIXME
		//		return BigDecimal.ZERO;

	}

	public BigDecimal montantHtReste() {
		if (engMontantBudgetaire().floatValue() == engMontantBudgetaireReste().floatValue())
			return engHtSaisie();

		majDecimales();
		return engHtSaisie().multiply(ratioInitReste()).setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal montantTtcReste() {
		if (engMontantBudgetaire().floatValue() == engMontantBudgetaireReste().floatValue())
			return engTtcSaisie();

		majDecimales();
		return engTtcSaisie().multiply(ratioInitReste()).setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}

	private BigDecimal ratioInitReste() {
		return engMontantBudgetaireReste().divide(engMontantBudgetaire(), 10, BigDecimal.ROUND_HALF_UP);
	}

	public void setEngIdProc(Number value) {
		engIdProc = value;
	}

	public Number engIdProc() {
		return engIdProc;
	}

	public int decimalesPourArrondirMontant() {
		return nbDecimales;
	}

	public _ISourceCredit source() {
		return new EOSource(organ(), typeCredit(), tauxProrata(), exercice());
	}

	public void setExerciceRelationship(EOExercice exercice) {
		super.setExerciceRelationship(exercice);
		majDecimales();
	}

	public void majDecimales() {
		if (exercice() == null) {
			nbDecimales = EODevise.defaultNbDecimales;
			return;
		}

		EODevise devise = FinderDevise.getDeviseEnCours(editingContext(), exercice());
		if (devise == null)
			nbDecimales = EODevise.defaultNbDecimales;
		else
			nbDecimales = devise.devNbDecimales().intValue();
	}

	public boolean isLiquidable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		if (typeApplication() == null || !typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
			return false;

		if (isSolde())
			return false;

		// si l'utilisateur n'a pas le droit d'engager sur cet exercice alors non
		if (!FinderExercice.getExercicesLiquidablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
			return false;

		// si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires presentes alors non
		if (organs == null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(utilisateur, "utilisateur");
			bindings.setObjectForKey(exercice(), "exercice");
			organs = FinderOrgan.getOrgans(ed, bindings);
		}

		// Si l'utilisateur n'a aucun droit sur les lignes budgetaires alors non
		if (organs == null || organs.count() == 0)
			return false;

		if (!organs.containsObject(organ()))
			return false;

		// l'utilisateur a les droits necessaires alors oui
		return true;
	}

	public NSArray repartitionPourcentageActions() {
		NSMutableArray array = new NSMutableArray();

		if (engMontantBudgetaireReste().floatValue() == 0)
			return array;
		if (engagementControleActions() == null || engagementControleActions().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < engagementControleActions().count(); i++) {
			EOEngagementControleAction ctrl = (EOEngagementControleAction) engagementControleActions().objectAtIndex(i);

			if (ctrl.eactMontantBudgetaireReste().floatValue() == 0)
				continue;

			boolean dernier = (i == engagementControleActions().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.eactMontantBudgetaireReste().divide(engMontantBudgetaireReste(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.typeAction()
			}),
					new NSArray(new Object[] {
							"pourcentage", "typeAction"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public NSArray repartitionPourcentageAnalytiques() {
		NSMutableArray array = new NSMutableArray();

		if (engMontantBudgetaireReste().floatValue() == 0)
			return array;
		if (engagementControleAnalytiques() == null || engagementControleAnalytiques().count() == 0)
			return array;

		BigDecimal pourcentageRestant = computeSumForKey(engagementControleAnalytiques(),
				EOEngagementControleAnalytique.EANA_MONTANT_BUDGETAIRE_RESTE_KEY).divide(engMontantBudgetaireReste(), 7,
				BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.0));
		if (pourcentageRestant.floatValue() == 0)
			return array;

		for (int i = 0; i < engagementControleAnalytiques().count(); i++) {
			EOEngagementControleAnalytique ctrl = (EOEngagementControleAnalytique) engagementControleAnalytiques().objectAtIndex(i);

			if (ctrl.eanaMontantBudgetaireReste().floatValue() == 0)
				continue;

			boolean dernier = (i == engagementControleAnalytiques().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.eanaMontantBudgetaireReste().divide(engMontantBudgetaireReste(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.codeAnalytique()
			}),
					new NSArray(new Object[] {
							"pourcentage", "codeAnalytique"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public NSArray repartitionPourcentageConventions() {
		NSMutableArray array = new NSMutableArray();

		if (engMontantBudgetaireReste().floatValue() == 0)
			return array;
		if (engagementControleConventions() == null || engagementControleConventions().count() == 0)
			return array;

		BigDecimal pourcentageRestant = computeSumForKey(engagementControleConventions(),
				EOEngagementControleConvention.ECON_MONTANT_BUDGETAIRE_RESTE_KEY).divide(engMontantBudgetaireReste(), 7,
				BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.0));
		if (pourcentageRestant.floatValue() == 0)
			return array;

		for (int i = 0; i < engagementControleConventions().count(); i++) {
			EOEngagementControleConvention ctrl = (EOEngagementControleConvention) engagementControleConventions().objectAtIndex(i);

			if (ctrl.econMontantBudgetaireReste().floatValue() == 0)
				continue;

			boolean dernier = (i == engagementControleConventions().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.econMontantBudgetaireReste().divide(engMontantBudgetaireReste(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.convention()
			}),
					new NSArray(new Object[] {
							"pourcentage", "convention"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public NSArray repartitionPourcentageHorsMarches() {
		NSMutableArray array = new NSMutableArray();

		if (engMontantBudgetaireReste().floatValue() == 0)
			return array;
		if (engagementControleHorsMarches() == null || engagementControleHorsMarches().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < engagementControleHorsMarches().count(); i++) {
			EOEngagementControleHorsMarche ctrl = (EOEngagementControleHorsMarche) engagementControleHorsMarches().objectAtIndex(i);

			if (ctrl.ehomMontantBudgetaireReste().floatValue() == 0)
				continue;

			boolean dernier = (i == engagementControleHorsMarches().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.ehomMontantBudgetaireReste().divide(engMontantBudgetaireReste(), 7, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.codeExer(), ctrl.typeAchat()
			}),
					new NSArray(new Object[] {
							"pourcentage", "codeExer", "typeAchat"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public NSArray repartitionPourcentageMarches() {
		NSMutableArray array = new NSMutableArray();

		if (engMontantBudgetaireReste().floatValue() == 0)
			return array;
		if (engagementControleMarches() == null || engagementControleMarches().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < engagementControleMarches().count(); i++) {
			EOEngagementControleMarche ctrl = (EOEngagementControleMarche) engagementControleMarches().objectAtIndex(i);

			if (ctrl.emarMontantBudgetaireReste().floatValue() == 0)
				continue;

			boolean dernier = (i == engagementControleMarches().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.emarMontantBudgetaireReste().divide(engMontantBudgetaireReste(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.attribution()
			}),
					new NSArray(new Object[] {
							"pourcentage", "attribution"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public NSArray repartitionPourcentagePlanComptables() {
		NSMutableArray array = new NSMutableArray();

		if (engMontantBudgetaireReste().floatValue() == 0)
			return array;
		if (engagementControlePlanComptables() == null || engagementControlePlanComptables().count() == 0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < engagementControlePlanComptables().count(); i++) {
			EOEngagementControlePlanComptable ctrl = (EOEngagementControlePlanComptable) engagementControlePlanComptables().objectAtIndex(i);

			if (ctrl.epcoMontantBudgetaireReste().floatValue() == 0)
				continue;

			boolean dernier = (i == engagementControlePlanComptables().count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = ctrl.epcoMontantBudgetaireReste().divide(engMontantBudgetaireReste(), 7, BigDecimal.ROUND_HALF_UP).
						multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.planComptable()
			}),
					new NSArray(new Object[] {
							"pourcentage", "planComptable"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	public void setSource(_ISourceCredit source) {
		setOrganRelationship(source.organ());
		setTypeCreditRelationship(source.typeCredit());
		setTauxProrataRelationship(source.tauxProrata());
		setExerciceRelationship(source.exercice());
	}

	public void setEngHtSaisie(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0.0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(decimalesPourArrondirMontant(), BigDecimal.ROUND_HALF_UP);
		super.setEngHtSaisie(aValue);

		if (engTtcSaisie() == null || engTtcSaisie().floatValue() < aValue.floatValue()) {
			super.setEngTtcSaisie(aValue);
			super.setEngTvaSaisie(new BigDecimal(0.0));
		}
		else {
			super.setEngTvaSaisie(engTtcSaisie().subtract(engHtSaisie()));
		}

		if (tauxProrata() != null)
			setEngMontantBudgetaire(calculMontantBudgetaire());

	}

	public void setEngHtSaisieRecalcul(BigDecimal aValue) {
		setEngHtSaisie(aValue);
		corrigerMontant();
	}

	public void setEngTvaSaisie(BigDecimal aValue) {
		aValue = aValue.setScale(decimalesPourArrondirMontant(), BigDecimal.ROUND_HALF_UP);
		super.setEngTvaSaisie(aValue);
	}

	public void setEngTtcSaisie(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0.0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(decimalesPourArrondirMontant(), BigDecimal.ROUND_HALF_UP);
		super.setEngTtcSaisie(aValue);

		if (engHtSaisie() == null || engHtSaisie().floatValue() > aValue.floatValue()) {
			super.setEngHtSaisie(aValue);
			super.setEngTvaSaisie(new BigDecimal(0.0));
		}
		else {
			super.setEngTvaSaisie(engTtcSaisie().subtract(engHtSaisie()));
		}

		if (tauxProrata() != null)
			setEngMontantBudgetaire(calculMontantBudgetaire());
	}

	public void setEngTtcSaisieRecalcul(BigDecimal aValue) {
		setEngTtcSaisie(aValue);
		corrigerMontant();
	}

	public void setEngMontantBudgetaire(BigDecimal aValue) {
		// appel a cette methode pour initialiser la valeur de depart
		minBudgetaire();

		aValue = aValue.setScale(decimalesPourArrondirMontant(), BigDecimal.ROUND_HALF_UP);
		super.setEngMontantBudgetaire(aValue);
	}

	public void setEngMontantBudgetaireReste(BigDecimal aValue) {
		if (aValue == null) {
			aValue = BigDecimal.ZERO;
		}
		aValue = aValue.setScale(decimalesPourArrondirMontant(), BigDecimal.ROUND_HALF_UP);
		super.setEngMontantBudgetaireReste(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (engHtSaisie() == null)
			throw new EngagementBudgetException(EngagementBudgetException.engHtSaisieManquant);
		if (engTvaSaisie() == null)
			throw new EngagementBudgetException(EngagementBudgetException.engTvaSaisieManquant);
		if (engTtcSaisie() == null)
			throw new EngagementBudgetException(EngagementBudgetException.engTtcSaisieManquant);
		if (engMontantBudgetaire() == null)
			throw new EngagementBudgetException(EngagementBudgetException.engMontantBudgetaireManquant);
		if (engMontantBudgetaireReste() == null)
			throw new EngagementBudgetException(EngagementBudgetException.engMontantBudgetaireResteManquant);
		if (engDateSaisie() == null)
			setEngDateSaisie(new NSTimestamp());
		if (engLibelle() == null)
			throw new EngagementBudgetException(EngagementBudgetException.engLibelleManquant);

		if (!engHtSaisie().abs().add(engTvaSaisie().abs()).equals(engTtcSaisie().abs()))
			setEngTvaSaisie(engTtcSaisie().subtract(engHtSaisie()));

		if (engTtcSaisie().floatValue() < 0.0)
			setEngMontantBudgetaire(new BigDecimal(0.0));
		else if (!engMontantBudgetaire().equals(calculMontantBudgetaire()))
			setEngMontantBudgetaire(calculMontantBudgetaire());

		if (engTvaSaisie().floatValue() < 0.0 || engHtSaisie().floatValue() < 0.0 || engTtcSaisie().floatValue() < 0.0 ||
				engMontantBudgetaire().floatValue() < 0.0 || engMontantBudgetaireReste().floatValue() < 0.0)
			throw new EngagementBudgetException(EngagementBudgetException.montantsNegatifs);

		if (utilisateur() == null)
			throw new EngagementBudgetException(EngagementBudgetException.utilisateurManquant);
		if (typeCredit() == null)
			throw new EngagementBudgetException(EngagementBudgetException.typeCreditManquant);
		if (typeApplication() == null)
			throw new EngagementBudgetException(EngagementBudgetException.typeApplicationManquant);
		if (tauxProrata() == null)
			throw new EngagementBudgetException(EngagementBudgetException.tauxProrataManquant);
		if (organ() == null)
			throw new EngagementBudgetException(EngagementBudgetException.organManquant);
		if (fournisseur() == null)
			throw new EngagementBudgetException(EngagementBudgetException.fournisseurManquant);
		if (exercice() == null)
			throw new EngagementBudgetException(EngagementBudgetException.exerciceManquant);
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(engHtSaisie(), engTvaSaisie(), tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		if (organ() != null && organ().isCodeAnalytiqueObligatoire()) {
			if (engagementControleAnalytiques() == null || engagementControleAnalytiques().count() == 0)
				throw new EngagementBudgetException(EngagementBudgetException.codeAnalytiqueManquant);

			if (engHtSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), "eanaHtSaisie")) != 0 ||
					engTvaSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), "eanaTvaSaisie")) != 0 ||
					engTtcSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), "eanaTtcSaisie")) != 0)
				throw new EngagementBudgetException(EngagementBudgetException.montantsOrganAnalytiqueIncoherent);
		}
		else {
			if (engHtSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), "eanaHtSaisie")) == -1 ||
					engTvaSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), "eanaTvaSaisie")) == -1 ||
					engTtcSaisie().compareTo(computeSumForKey(engagementControleAnalytiques(), "eanaTtcSaisie")) == -1)
				throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControleAnalytiqueIncoherent);
		}

		// on verifie que les montants de l'engagement sont les memes que ceux des engagementControle
		if (engHtSaisie().compareTo(computeSumForKey(engagementControleActions(), "eactHtSaisie")) != 0 ||
				engTvaSaisie().compareTo(computeSumForKey(engagementControleActions(), "eactTvaSaisie")) != 0 ||
				engTtcSaisie().compareTo(computeSumForKey(engagementControleActions(), "eactTtcSaisie")) != 0)
			throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControleActionIncoherent);

		if (engHtSaisie().compareTo(computeSumForKey(engagementControlePlanComptables(), "epcoHtSaisie")) != 0 ||
				engTvaSaisie().compareTo(computeSumForKey(engagementControlePlanComptables(), "epcoTvaSaisie")) != 0 ||
				engTtcSaisie().compareTo(computeSumForKey(engagementControlePlanComptables(), "epcoTtcSaisie")) != 0)
			throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControlePlanComptableIncoherent);

		if (engagementControleHorsMarches() != null && engagementControleHorsMarches().count() > 0) {

			if (engHtSaisie().compareTo(computeSumForKey(engagementControleHorsMarches(), "ehomHtSaisie")) != 0 ||
					engTvaSaisie().compareTo(computeSumForKey(engagementControleHorsMarches(), "ehomTvaSaisie")) != 0 ||
					engTtcSaisie().compareTo(computeSumForKey(engagementControleHorsMarches(), "ehomTtcSaisie")) != 0)
				throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControleHorsMarcheIncoherent);

			if (engagementControleMarches() != null && engagementControleMarches().count() > 0)
				throw new EngagementBudgetException(EngagementBudgetException.controleMarcheHorsMarcheIncoherent);
		}
		else {
			if (engagementControleMarches() == null || engagementControleMarches().count() == 0)
				throw new EngagementBudgetException(EngagementBudgetException.controleMarcheHorsMarcheManquant);

			if (engHtSaisie().compareTo(computeSumForKey(engagementControleMarches(), "emarHtSaisie")) != 0 ||
					engTvaSaisie().compareTo(computeSumForKey(engagementControleMarches(), "emarTvaSaisie")) != 0 ||
					engTtcSaisie().compareTo(computeSumForKey(engagementControleMarches(), "emarTtcSaisie")) != 0)
				throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControleMarcheIncoherent);
		}

		validateBeforeTransactionSavePourConvention();

	}

	protected void validateBeforeTransactionSavePourConvention() throws EngagementBudgetException {
		if (organ() == null) {
			return;
		}

		if (organ().isConventionObligatoire()) {
			validateBeforeTransactionSaveQuandConventionObligatoire();
		} else {
			validateBeforeTransactionSaveQuandConventionNonObligatoire();
		}
	}

	protected void validateBeforeTransactionSaveQuandConventionObligatoire() throws EngagementBudgetException {
		if (organ() == null || !organ().isConventionObligatoire()) {
			return;
		}

		if (conventionsNonRenseignees()) {
			throw new EngagementBudgetException(EngagementBudgetException.CONVENTION_MANQUANTE);
		}

		if (montantsEngagementDifferentsDesMontantsConventions()) {
			throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControleConventionIncoherent);
		}
	}

	protected void validateBeforeTransactionSaveQuandConventionNonObligatoire() throws EngagementBudgetException {
		if (conventionsNonRenseignees()) {
			return;
		}

		if (montantsEngagementInferieursAuxMontantsConventions()) {
			throw new EngagementBudgetException(EngagementBudgetException.montantsEngagementControleConventionIncoherent);
		}
	}

	protected boolean conventionsNonRenseignees() {
		return engagementControleConventions() == null || engagementControleConventions().count() == 0;
	}

	protected boolean montantsEngagementDifferentsDesMontantsConventions() {
		return engHtSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_HT_SAISIE_KEY)) != 0
				|| engTvaSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_TVA_SAISIE_KEY)) != 0
				|| engTtcSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_TTC_SAISIE_KEY)) != 0;
	}

	protected boolean montantsEngagementInferieursAuxMontantsConventions() {
		return engHtSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_HT_SAISIE_KEY)) == -1
				|| engTvaSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_TVA_SAISIE_KEY)) == -1
				|| engTtcSaisie().compareTo(computeSumForKey(engagementControleConventions(), EOEngagementControleConvention.ECON_TTC_SAISIE_KEY)) == -1;
	}

	// calcule le montant budgetaire de la facture avec le prorata de l'engagement (c'est fait expres !)
	public BigDecimal sommeBudgetaireDepense() {
		BigDecimal total = new BigDecimal(0.0);

		for (int i = 0; i < depenseBudgets().count(); i++)
			total = total.add(
					serviceCalculs.calculMontantBudgetaire(((EODepenseBudget) depenseBudgets().objectAtIndex(i)).depHtSaisie(),
					((EODepenseBudget) depenseBudgets().objectAtIndex(i)).depTvaSaisie(), tauxProrata().tapTaux()));

		return total;
	}

	protected BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	public boolean isPartiellementSolde() {
		if (engMontantBudgetaire().floatValue() > engMontantBudgetaireReste().floatValue() &&
				engMontantBudgetaireReste().floatValue() > 0.0)
			return true;
		return false;
	}

	public boolean isSolde() {
		if (engMontantBudgetaireReste().floatValue() == 0.0)
			return true;
		return false;
	}

	public boolean isEngage() {
		if (engMontantBudgetaireReste().floatValue() == engMontantBudgetaire().floatValue() &&
				engMontantBudgetaireReste().floatValue() > 0.0)
			return true;
		return false;
	}

	/**
	 * @return true si pas de liquidation finale.
	 */
	public boolean isPreLiquidation() {
		return (depenseBudgets() == null || depenseBudgets().count() == 0);
	}

	/**
	 * @return Les depensesBudgets ou preDepenseBudgets selon qu'on est en preLiquidation ou pas. des types _IDepenseBudget.
	 */
	public NSArray currentDepenseBudgets() {
		return NSArrayCtrl.unionOfNSArrays(new NSArray[] {
				preDepenseBudgets(), depenseBudgets()
		});
	}

	public EODepenseBudget initialiserDepenseBudget(EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur, boolean prerempli)
			throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (depensePapier.fournisseur() == null)
			depensePapier.setFournisseurRelationship(fournisseur());
		if (!depensePapier.fournisseur().equals(fournisseur()))
			throw new FactoryException("le fournisseur de la depensePapier doit etre le meme que celui de l'engagement");

		FactoryDepenseBudget factoryDepenseBudget = new FactoryDepenseBudget();
		NSMutableArray depenseBudgets = new NSMutableArray();

		EOEngagementBudget engagementBudget = this;

		if (engagementBudget.isSolde()) {
			return null;
		}

		EODepenseBudget depenseBudget;

		if (!prerempli) {
			depenseBudget = factoryDepenseBudget.creer(ed, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), depensePapier,
					engagementBudget, engagementBudget.tauxProrata(), engagementBudget.exercice(), utilisateur, source().type(), source());
		}
		else {
			BigDecimal engHt = engagementBudget.montantHtReste();
			BigDecimal engTtc = engagementBudget.montantTtcReste();
			depenseBudget = factoryDepenseBudget.creer(ed, engHt, engTtc.subtract(engHt), engTtc,
					engagementBudget.engMontantBudgetaireReste(), depensePapier,
					engagementBudget, engagementBudget.tauxProrata(), engagementBudget.exercice(), utilisateur, source().type(), source());
		}

		if (!prerempli && depenseBudgets != null && depenseBudgets.count() == 1) {
			((EODepenseBudget) depenseBudgets.objectAtIndex(0)).setDepHtSaisie(depensePapier.dppHtInitial());
			((EODepenseBudget) depenseBudgets.objectAtIndex(0)).setDepTtcSaisie(depensePapier.dppTtcInitial());
		}

		return depenseBudget;
	}

	/**
	 * @return true s'il s'agit d'un engagement sur une poche d'extourne.
	 */
	public Boolean isSurExtournePoche() {
		return (!isNewObject() && engMontantBudgetaire().compareTo(BigDecimal.ZERO) == 0 && engTtcSaisie().doubleValue() > 0 && engagementControleActions().count() == 0);
	}

	/**
	 * @return true s'il s'agit d'un engagement généré par une liquidation d'extourne
	 */
	public Boolean isExtourne() {
		return engTtcSaisie() != null && engTtcSaisie().doubleValue() < 0;
	}

}
