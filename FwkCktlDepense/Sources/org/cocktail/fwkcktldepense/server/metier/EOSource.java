/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.finder.FinderBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOSource extends Object implements _ISourceCredit {
	private EOOrgan organ;
	private EOTypeCredit typeCredit;
	private EOTauxProrata tauxProrata;
	private EOExercice exercice;
	private ESourceCreditType type;
	private NSArray<EOConvention> arrayConventions;
	private NSArray<EOCodeAnalytique> arrayAnalytiques;

	public EOSource(EOOrgan lOrgan, EOTypeCredit leTypeCredit, EOTauxProrata leTauxProrata, EOExercice lExercice) {
		super();

		organ = lOrgan;
		typeCredit = leTypeCredit;
		tauxProrata = leTauxProrata;
		exercice = lExercice;
		type = ESourceCreditType.BUDGET;
	}

	public String libelle() {
		String libelle = "";
		if (organ == null || typeCredit == null)
			return libelle;

		libelle = organ.sourceLibelle() + " - " + typeCredit.tcdCode();

		if (tauxProrata != null)
			libelle = libelle + " - " + tauxProrata.tapTaux();

		return libelle;
	}

	public String libelleCourt() {
		String libelleCourt = libelle();
		if (libelleCourt != null && libelleCourt.length() > 50)
			libelleCourt = libelleCourt.substring(0, 50);
		return libelleCourt;
	}

	public String codeOrganEtTypeCredit() {
	    String code = "";
        if (organ == null || typeCredit == null)
            return code;
        code = organ.sourceLibelle() + " - " + typeCredit.tcdCode();
        return code;	    
	}
	
	public void setOrgan(EOOrgan lOrgan) {
		organ = lOrgan;
	}

	public EOOrgan organ() {
		return organ;
	}

	public void setTypeCredit(EOTypeCredit leTypeCredit) {
		typeCredit = leTypeCredit;
	}

	public EOTypeCredit typeCredit() {
		return typeCredit;
	}

	public NSArray<EOTauxProrata> tauxProrataDisponibles() {
		if (organ() == null || exercice() == null) {
			return NSArray.emptyArray();
		}
		return organ().tauxProratasPourExerciceTries(exercice());
	}

	public void setTauxProrata(EOTauxProrata leTauxProrata) {
		tauxProrata = leTauxProrata;
	}

	public EOTauxProrata tauxProrata() {
		return tauxProrata;
	}

	public void setExercice(EOExercice lExercice) {
		exercice = lExercice;
	}

	public EOExercice exercice() {
		return exercice;
	}

	public Boolean isComplet() {
		return exercice() != null && organ() != null && typeCredit() != null && tauxProrata() != null;
	}

	public ESourceCreditType type() {
		return ESourceCreditType.BUDGET;
	}

	public BigDecimal getDisponible() {
		if (organ() != null && typeCredit() != null) {
			NSArray<EOBudgetExecCredit> budgets = FinderBudget.getBudgetsPourSource(organ().editingContext(), (EOSource) this);

			if (budgets == null || budgets.count() == 0)
				return BigDecimal.ZERO;

			return ((EOBudgetExecCredit) budgets.objectAtIndex(0)).bdxcDisponible();
		}
		return BigDecimal.ZERO;
	}

	public NSArray<EOConvention> getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null && organ() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(organ(), "organ");
			bindings.setObjectForKey(typeCredit(), "typeCredit");
			bindings.setObjectForKey(exercice(), "exercice");
			bindings.setObjectForKey(Boolean.TRUE, "montants");
			bindings.setObjectForKey(Boolean.TRUE, "montants");
			NSTimestamp dateFinPaiementMax = MyDateCtrl.getDateJour();
			bindings.setObjectForKey(dateFinPaiementMax, "conDateFinPaiementMax");
			//bindings.setObjectForKey(EOConventionNonLimitative.MODE_GESTION_RESSOURCE_AFFECTEE, "convModeGestion");

			arrayConventions = FinderConvention.getConventions(ed, bindings);
		}
		if (arrayConventions == null)
			arrayConventions = NSArray.emptyArray();
		return arrayConventions;
	}

	public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques == null && utilisateur != null)
			arrayAnalytiques = FinderCodeAnalytique.getCodeAnalytiques(ed, organ(), exercice());
		if (arrayAnalytiques == null)
			arrayAnalytiques = NSArray.emptyArray();
		if (arrayAnalytiques.count() == 0)
			return arrayAnalytiques;
		return arrayAnalytiques;
	}

}
