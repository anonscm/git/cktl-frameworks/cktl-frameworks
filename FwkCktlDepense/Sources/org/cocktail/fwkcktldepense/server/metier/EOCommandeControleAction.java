

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;


import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleActionException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCommandeControleAction extends _EOCommandeControleAction
{
	private static Calculs serviceCalculs = Calculs.getInstance();
	
    public EOCommandeControleAction() {
        super();
    }

    public BigDecimal montantTtc() {
    	return cactTtcSaisie();
    }
    
    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
    	
    	if (commandeBudget()!=null && commandeBudget().cbudTtcSaisie()!=null && commandeBudget().cbudTtcSaisie().floatValue()!=0.0)
    		setCactPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(commandeBudget().cbudTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setCactHtSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCactHtSaisie(aValue);
    }

    public void setCactTvaSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCactTvaSaisie(aValue);
    }

    public void setCactTtcSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCactTtcSaisie(aValue);
    }

    public void setCactMontantBudgetaire(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCactMontantBudgetaire(aValue);
    }

    public void setCactPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
        super.setCactPourcentage(aValue);
        
    	if (commandeBudget()!=null) {
    		NSArray commandeControleActions = commandeBudget().commandeControleActions();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControleAction.TYPE_ACTION_KEY+" != nil", null);
    		commandeControleActions = EOQualifier.filteredArrayWithQualifier(commandeControleActions, qual);
    		BigDecimal total=commandeBudget().computeSumForKey(commandeControleActions, EOCommandeControleAction.CACT_POURCENTAGE_KEY);
    		if (total.floatValue()>100.0)
    	        super.setCactPourcentage(new BigDecimal(100.0).subtract(total.subtract(aValue)));    			
    	}

        if (commandeBudget()!=null)
        	commandeBudget().corrigerMontantActions();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws CommandeControleActionException {
        if (cactHtSaisie()==null)
            throw new CommandeControleActionException(CommandeControleActionException.cactHtSaisieManquant);
        if (cactTvaSaisie()==null)
            throw new CommandeControleActionException(CommandeControleActionException.cactTvaSaisieManquant);
        if (cactTtcSaisie()==null)
            throw new CommandeControleActionException(CommandeControleActionException.cactTtcSaisieManquant);
        if (cactMontantBudgetaire()==null)
            throw new CommandeControleActionException(CommandeControleActionException.cactMontantBudgetaireManquant);
        if (cactPourcentage()==null)
            throw new CommandeControleActionException(CommandeControleActionException.cactPourcentageManquant);
        if (exercice()==null)
            throw new CommandeControleActionException(CommandeControleActionException.exerciceManquant);
        if (commandeBudget()==null)
            throw new CommandeControleActionException(CommandeControleActionException.commandeBudgetManquant);
        
        if (!cactHtSaisie().abs().add(cactTvaSaisie().abs()).equals(cactTtcSaisie().abs()))
        	setCactTvaSaisie(cactTtcSaisie().subtract(cactHtSaisie()));
        if (commandeBudget().tauxProrata()!=null && !cactMontantBudgetaire().equals(calculMontantBudgetaire()))
        	setCactMontantBudgetaire(
        			calculMontantBudgetaire());

        if (cactTvaSaisie().floatValue()<0.0 || cactHtSaisie().floatValue()<0.0 || cactTtcSaisie().floatValue()<0.0 ||
        		cactMontantBudgetaire().floatValue()<0.0)
        	throw new CommandeControleActionException(CommandeControleActionException.montantsNegatifs);

        if (!commandeBudget().exercice().equals(exercice()))
            throw new CommandeControleActionException(CommandeControleActionException.commandeBudgetExercicePasCoherent);
    }

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(cactHtSaisie(), cactTvaSaisie(), commandeBudget().tauxProrata().tapTaux());
	}
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        if (typeAction()==null)
            throw new CommandeControleActionException(CommandeControleActionException.typeActionManquant);
    }
}
