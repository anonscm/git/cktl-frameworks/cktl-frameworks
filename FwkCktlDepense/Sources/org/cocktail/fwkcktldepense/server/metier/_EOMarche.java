/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_MARCHE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> MAR_DEBUT = new ERXKey<NSTimestamp>("marDebut");
	public static final ERXKey<NSTimestamp> MAR_FIN = new ERXKey<NSTimestamp>("marFin");
	public static final ERXKey<String> MAR_INDEX = new ERXKey<String>("marIndex");
	public static final ERXKey<String> MAR_LIBELLE = new ERXKey<String>("marLibelle");
	public static final ERXKey<String> MAR_SUPPR = new ERXKey<String>("marSuppr");
	public static final ERXKey<String> MAR_VALIDE = new ERXKey<String>("marValide");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "marOrdre";

	public static final String MAR_DEBUT_KEY = "marDebut";
	public static final String MAR_FIN_KEY = "marFin";
	public static final String MAR_INDEX_KEY = "marIndex";
	public static final String MAR_LIBELLE_KEY = "marLibelle";
	public static final String MAR_SUPPR_KEY = "marSuppr";
	public static final String MAR_VALIDE_KEY = "marValide";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MAR_ORDRE_KEY = "marOrdre";

//Colonnes dans la base de donnees
	public static final String MAR_DEBUT_COLKEY = "MAR_DEBUT";
	public static final String MAR_FIN_COLKEY = "MAR_FIN";
	public static final String MAR_INDEX_COLKEY = "MAR_INDEX";
	public static final String MAR_LIBELLE_COLKEY = "MAR_LIBELLE";
	public static final String MAR_SUPPR_COLKEY = "MAR_SUPPR";
	public static final String MAR_VALIDE_COLKEY = "MAR_VALIDE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MAR_ORDRE_COLKEY = "MAR_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public NSTimestamp marDebut() {
	 return (NSTimestamp) storedValueForKey(MAR_DEBUT_KEY);
	}

	public void setMarDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, MAR_DEBUT_KEY);
	}

	public NSTimestamp marFin() {
	 return (NSTimestamp) storedValueForKey(MAR_FIN_KEY);
	}

	public void setMarFin(NSTimestamp value) {
	 takeStoredValueForKey(value, MAR_FIN_KEY);
	}

	public String marIndex() {
	 return (String) storedValueForKey(MAR_INDEX_KEY);
	}

	public void setMarIndex(String value) {
	 takeStoredValueForKey(value, MAR_INDEX_KEY);
	}

	public String marLibelle() {
	 return (String) storedValueForKey(MAR_LIBELLE_KEY);
	}

	public void setMarLibelle(String value) {
	 takeStoredValueForKey(value, MAR_LIBELLE_KEY);
	}

	public String marSuppr() {
	 return (String) storedValueForKey(MAR_SUPPR_KEY);
	}

	public void setMarSuppr(String value) {
	 takeStoredValueForKey(value, MAR_SUPPR_KEY);
	}

	public String marValide() {
	 return (String) storedValueForKey(MAR_VALIDE_KEY);
	}

	public void setMarValide(String value) {
	 takeStoredValueForKey(value, MAR_VALIDE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOMarche createEOMarche(EOEditingContext editingContext								, String marIndex
							, String marLibelle
									, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOMarche eo = (EOMarche) EOUtilities.createAndInsertInstance(editingContext, _EOMarche.ENTITY_NAME);	 
											eo.setMarIndex(marIndex);
									eo.setMarLibelle(marLibelle);
										 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMarche creerInstance(EOEditingContext editingContext) {
		EOMarche object = (EOMarche)EOUtilities.createAndInsertInstance(editingContext, _EOMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOMarche localInstanceIn(EOEditingContext editingContext) {
    EOMarche localInstance = (EOMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMarche> objectsForRecherche(EOEditingContext ec,
														NSTimestamp dateBinding,
														String marSupprBinding,
														String marValideBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOMarche.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (dateBinding != null)
			bindings.takeValueForKey(dateBinding, "date");
		  if (marSupprBinding != null)
			bindings.takeValueForKey(marSupprBinding, "marSuppr");
		  if (marValideBinding != null)
			bindings.takeValueForKey(marValideBinding, "marValide");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
