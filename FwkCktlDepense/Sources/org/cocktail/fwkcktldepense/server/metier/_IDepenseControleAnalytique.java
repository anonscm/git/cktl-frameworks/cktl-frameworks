package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

public interface _IDepenseControleAnalytique {

	public final static String _CODE_ANALYTIQUE_KEY = "codeAnalytique";

	public EOCodeAnalytique codeAnalytique();

	public void setCodeAnalytiqueRelationship(EOCodeAnalytique unCodeAnalytiqueSelectionne);

	public void setPourcentage(BigDecimal uneDepenseCtrlAnalytiquePourcentage);

	public BigDecimal pourcentage();

	public BigDecimal danaHtSaisie();

	public BigDecimal danaTtcSaisie();

	public BigDecimal danaMontantBudgetaire();

	public void setMontantTtc(BigDecimal uneDepenseCtrlAnalytiqueMontant);

	public void setDanaTtcSaisie(BigDecimal restantTtcControleAnalytique);

	public BigDecimal montantTtc();
}
