/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPreDepenseBudget.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPreDepenseBudget extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_BUDGET";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> DEP_HT_SAISIE = new ERXKey<java.math.BigDecimal>("depHtSaisie");
	public static final ERXKey<java.math.BigDecimal> DEP_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("depMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> DEP_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("depTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DEP_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("depTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> DEPENSE_PAPIER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepensePapier>("depensePapier");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> PRE_DEPENSE_CONTROLE_ACTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction>("preDepenseControleActions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> PRE_DEPENSE_CONTROLE_ANALYTIQUES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique>("preDepenseControleAnalytiques");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> PRE_DEPENSE_CONTROLE_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention>("preDepenseControleConventions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> PRE_DEPENSE_CONTROLE_HORS_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche>("preDepenseControleHorsMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> PRE_DEPENSE_CONTROLE_MARCHES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche>("preDepenseControleMarches");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable>("preDepenseControlePlanComptables");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata> TAUX_PRORATA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata>("tauxProrata");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdepId";

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";

//Attributs non visibles
	public static final String DPP_ID_KEY = "dppId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PDEP_ID_KEY = "pdepId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_HT_SAISIE_COLKEY = "PDEP_HT_SAISIE";
	public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "PDEP_MONTANT_BUDGETAIRE";
	public static final String DEP_TTC_SAISIE_COLKEY = "PDEP_TTC_SAISIE";
	public static final String DEP_TVA_SAISIE_COLKEY = "PDEP_TVA_SAISIE";

	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PDEP_ID_COLKEY = "PDEP_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_PAPIER_KEY = "depensePapier";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PRE_DEPENSE_CONTROLE_ACTIONS_KEY = "preDepenseControleActions";
	public static final String PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY = "preDepenseControleAnalytiques";
	public static final String PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY = "preDepenseControleConventions";
	public static final String PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY = "preDepenseControleHorsMarches";
	public static final String PRE_DEPENSE_CONTROLE_MARCHES_KEY = "preDepenseControleMarches";
	public static final String PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY = "preDepenseControlePlanComptables";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public java.math.BigDecimal depHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_HT_SAISIE_KEY);
	}

	public void setDepHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal depMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
	}

	public void setDepMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal depTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_TTC_SAISIE_KEY);
	}

	public void setDepTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal depTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DEP_TVA_SAISIE_KEY);
	}

	public void setDepTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DEP_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepensePapier depensePapier() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepensePapier)storedValueForKey(DEPENSE_PAPIER_KEY);
	}

	public void setDepensePapierRelationship(org.cocktail.fwkcktldepense.server.metier.EODepensePapier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepensePapier oldValue = depensePapier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_PAPIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_PAPIER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
	}

	public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.server.metier.EOTauxProrata value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTauxProrata oldValue = tauxProrata();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> preDepenseControleActions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction>)storedValueForKey(PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> preDepenseControleActions(EOQualifier qualifier) {
	 return preDepenseControleActions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> preDepenseControleActions(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControleActions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> preDepenseControleActions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControleActions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
	}
	
	public void removeFromPreDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction createPreDepenseControleActionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction) eo;
	}
	
	public void deletePreDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControleActionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction> objects = preDepenseControleActions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControleActionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> preDepenseControleAnalytiques() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique>)storedValueForKey(PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> preDepenseControleAnalytiques(EOQualifier qualifier) {
	 return preDepenseControleAnalytiques(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> preDepenseControleAnalytiques(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControleAnalytiques(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> preDepenseControleAnalytiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControleAnalytiques();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public void removeFromPreDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique createPreDepenseControleAnalytiquesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique) eo;
	}
	
	public void deletePreDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControleAnalytiquesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique> objects = preDepenseControleAnalytiques().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControleAnalytiquesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> preDepenseControleConventions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention>)storedValueForKey(PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> preDepenseControleConventions(EOQualifier qualifier) {
	 return preDepenseControleConventions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> preDepenseControleConventions(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControleConventions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> preDepenseControleConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControleConventions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
	}
	
	public void removeFromPreDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention createPreDepenseControleConventionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention) eo;
	}
	
	public void deletePreDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControleConventionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> objects = preDepenseControleConventions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControleConventionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> preDepenseControleHorsMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche>)storedValueForKey(PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> preDepenseControleHorsMarches(EOQualifier qualifier) {
	 return preDepenseControleHorsMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> preDepenseControleHorsMarches(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControleHorsMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> preDepenseControleHorsMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControleHorsMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public void removeFromPreDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche createPreDepenseControleHorsMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche) eo;
	}
	
	public void deletePreDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControleHorsMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> objects = preDepenseControleHorsMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControleHorsMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> preDepenseControleMarches() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche>)storedValueForKey(PRE_DEPENSE_CONTROLE_MARCHES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> preDepenseControleMarches(EOQualifier qualifier) {
	 return preDepenseControleMarches(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> preDepenseControleMarches(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControleMarches(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> preDepenseControleMarches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControleMarches();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
	}
	
	public void removeFromPreDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche createPreDepenseControleMarchesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche) eo;
	}
	
	public void deletePreDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControleMarchesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche> objects = preDepenseControleMarches().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControleMarchesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> preDepenseControlePlanComptables() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable>)storedValueForKey(PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> preDepenseControlePlanComptables(EOQualifier qualifier) {
	 return preDepenseControlePlanComptables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> preDepenseControlePlanComptables(EOQualifier qualifier, boolean fetch) {
	 return preDepenseControlePlanComptables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> preDepenseControlePlanComptables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseControlePlanComptables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public void removeFromPreDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable createPreDepenseControlePlanComptablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable) eo;
	}
	
	public void deletePreDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseControlePlanComptablesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable> objects = preDepenseControlePlanComptables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseControlePlanComptablesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPreDepenseBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPreDepenseBudget createEOPreDepenseBudget(EOEditingContext editingContext				, java.math.BigDecimal depHtSaisie
							, java.math.BigDecimal depMontantBudgetaire
							, java.math.BigDecimal depTtcSaisie
							, java.math.BigDecimal depTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EODepensePapier depensePapier		, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata		, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EOPreDepenseBudget eo = (EOPreDepenseBudget) EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseBudget.ENTITY_NAME);	 
							eo.setDepHtSaisie(depHtSaisie);
									eo.setDepMontantBudgetaire(depMontantBudgetaire);
									eo.setDepTtcSaisie(depTtcSaisie);
									eo.setDepTvaSaisie(depTvaSaisie);
						 eo.setDepensePapierRelationship(depensePapier);
				 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setTauxProrataRelationship(tauxProrata);
				 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreDepenseBudget creerInstance(EOEditingContext editingContext) {
		EOPreDepenseBudget object = (EOPreDepenseBudget)EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseBudget.ENTITY_NAME);
  		return object;
		}

	

  public EOPreDepenseBudget localInstanceIn(EOEditingContext editingContext) {
    EOPreDepenseBudget localInstance = (EOPreDepenseBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreDepenseBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreDepenseBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreDepenseBudget> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreDepenseBudget> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreDepenseBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
