/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOExtourneLiq.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOExtourneLiq extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleExtourneLiq";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.EXTOURNE_LIQ";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> FWK_CARAMBOLE_DEPENSE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("fwkCaramboleDepenseBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> TO_DEPENSE_BUDGET_N = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("toDepenseBudgetN");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> TO_DEPENSE_BUDGET_N1 = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("toDepenseBudgetN1");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> TO_EXTOURNE_LIQ_REPARTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart>("toExtourneLiqReparts");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "elId";


//Attributs non visibles
	public static final String DEP_ID_N_KEY = "depIdN";
	public static final String DEP_ID_N1_KEY = "depIdN1";
	public static final String EL_ID_KEY = "elId";

//Colonnes dans la base de donnees

	public static final String DEP_ID_N_COLKEY = "DEP_ID_N";
	public static final String DEP_ID_N1_COLKEY = "DEP_ID_N1";
	public static final String EL_ID_COLKEY = "EL_ID";


	// Relationships
	public static final String FWK_CARAMBOLE_DEPENSE_BUDGET_KEY = "fwkCaramboleDepenseBudget";
	public static final String TO_DEPENSE_BUDGET_N_KEY = "toDepenseBudgetN";
	public static final String TO_DEPENSE_BUDGET_N1_KEY = "toDepenseBudgetN1";
	public static final String TO_EXTOURNE_LIQ_REPARTS_KEY = "toExtourneLiqReparts";



	// Accessors methods
	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget fwkCaramboleDepenseBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget)storedValueForKey(FWK_CARAMBOLE_DEPENSE_BUDGET_KEY);
	}

	public void setFwkCaramboleDepenseBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseBudget oldValue = fwkCaramboleDepenseBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FWK_CARAMBOLE_DEPENSE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FWK_CARAMBOLE_DEPENSE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget toDepenseBudgetN() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget)storedValueForKey(TO_DEPENSE_BUDGET_N_KEY);
	}

	public void setToDepenseBudgetNRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseBudget oldValue = toDepenseBudgetN();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPENSE_BUDGET_N_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPENSE_BUDGET_N_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget toDepenseBudgetN1() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget)storedValueForKey(TO_DEPENSE_BUDGET_N1_KEY);
	}

	public void setToDepenseBudgetN1Relationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseBudget oldValue = toDepenseBudgetN1();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPENSE_BUDGET_N1_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPENSE_BUDGET_N1_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> toExtourneLiqReparts() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart>)storedValueForKey(TO_EXTOURNE_LIQ_REPARTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> toExtourneLiqReparts(EOQualifier qualifier) {
	 return toExtourneLiqReparts(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> toExtourneLiqReparts(EOQualifier qualifier, boolean fetch) {
	 return toExtourneLiqReparts(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> toExtourneLiqReparts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart.TO_EXTOURNE_LIQ_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toExtourneLiqReparts();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToExtourneLiqRepartsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_REPARTS_KEY);
	}
	
	public void removeFromToExtourneLiqRepartsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_REPARTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart createToExtourneLiqRepartsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_REPARTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart) eo;
	}
	
	public void deleteToExtourneLiqRepartsRelationship(org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_REPARTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToExtourneLiqRepartsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiqRepart> objects = toExtourneLiqReparts().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToExtourneLiqRepartsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOExtourneLiq avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOExtourneLiq createEOExtourneLiq(EOEditingContext editingContext		, org.cocktail.fwkcktldepense.server.metier.EODepenseBudget fwkCaramboleDepenseBudget				, org.cocktail.fwkcktldepense.server.metier.EODepenseBudget toDepenseBudgetN1					) {
	 EOExtourneLiq eo = (EOExtourneLiq) EOUtilities.createAndInsertInstance(editingContext, _EOExtourneLiq.ENTITY_NAME);	 
				 eo.setFwkCaramboleDepenseBudgetRelationship(fwkCaramboleDepenseBudget);
						 eo.setToDepenseBudgetN1Relationship(toDepenseBudgetN1);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOExtourneLiq creerInstance(EOEditingContext editingContext) {
		EOExtourneLiq object = (EOExtourneLiq)EOUtilities.createAndInsertInstance(editingContext, _EOExtourneLiq.ENTITY_NAME);
  		return object;
		}

	

  public EOExtourneLiq localInstanceIn(EOEditingContext editingContext) {
    EOExtourneLiq localInstance = (EOExtourneLiq)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOExtourneLiq fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOExtourneLiq fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOExtourneLiq> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOExtourneLiq eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOExtourneLiq)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOExtourneLiq fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOExtourneLiq fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOExtourneLiq> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOExtourneLiq eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOExtourneLiq)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOExtourneLiq fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOExtourneLiq eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOExtourneLiq ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOExtourneLiq fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
