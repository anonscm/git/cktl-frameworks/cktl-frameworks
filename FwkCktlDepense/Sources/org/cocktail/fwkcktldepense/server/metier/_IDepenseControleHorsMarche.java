package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

public interface _IDepenseControleHorsMarche {

	public static final String _CODE_EXER_KEY = "codeExer";

	public EOCodeExer codeExer();

	public void setCodeExerRelationship(EOCodeExer unCodeNomenclatureSelectionne);

	public void setPourcentage(BigDecimal uneDepenseCtrlHorsMarchePourcentage);

	public BigDecimal pourcentage();

	public BigDecimal dhomTtcSaisie();

	public BigDecimal dhomHtSaisie();

	public BigDecimal dhomMontantBudgetaire();

	public void setMontantTtc(BigDecimal uneDepenseCtrlHorsMarcheMontant);

	public BigDecimal montantTtc();

	public void setDhomTtcSaisie(BigDecimal restantTtcControleHorsMarche);

	public void setTypeAchatRelationship(EOTypeAchat typeAchat);

}
