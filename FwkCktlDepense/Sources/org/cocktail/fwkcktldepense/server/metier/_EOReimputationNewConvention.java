/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOReimputationNewConvention.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOReimputationNewConvention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationNewConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_REIMPUTATION_NEW_CONVENTION";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> RECO_HT_SAISIE = new ERXKey<java.math.BigDecimal>("recoHtSaisie");
	public static final ERXKey<java.math.BigDecimal> RECO_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("recoMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> RECO_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("recoTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> RECO_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("recoTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention> CONVENTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention>("convention");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation> REIMPUTATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation>("reimputation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recoId";

	public static final String RECO_HT_SAISIE_KEY = "recoHtSaisie";
	public static final String RECO_MONTANT_BUDGETAIRE_KEY = "recoMontantBudgetaire";
	public static final String RECO_TTC_SAISIE_KEY = "recoTtcSaisie";
	public static final String RECO_TVA_SAISIE_KEY = "recoTvaSaisie";

//Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String RECO_ID_KEY = "recoId";
	public static final String REIM_ID_KEY = "reimId";

//Colonnes dans la base de donnees
	public static final String RECO_HT_SAISIE_COLKEY = "RECO_HT_SAISIE";
	public static final String RECO_MONTANT_BUDGETAIRE_COLKEY = "RECO_MONTANT_BUDGETAIRE";
	public static final String RECO_TTC_SAISIE_COLKEY = "RECO_TTC_SAISIE";
	public static final String RECO_TVA_SAISIE_COLKEY = "RECO_TVA_SAISIE";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String RECO_ID_COLKEY = "RECO_ID";
	public static final String REIM_ID_COLKEY = "REIM_ID";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String REIMPUTATION_KEY = "reimputation";



	// Accessors methods
	public java.math.BigDecimal recoHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(RECO_HT_SAISIE_KEY);
	}

	public void setRecoHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RECO_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal recoMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(RECO_MONTANT_BUDGETAIRE_KEY);
	}

	public void setRecoMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RECO_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal recoTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(RECO_TTC_SAISIE_KEY);
	}

	public void setRecoTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RECO_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal recoTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(RECO_TVA_SAISIE_KEY);
	}

	public void setRecoTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, RECO_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOConvention convention() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
	}

	public void setConventionRelationship(org.cocktail.fwkcktldepense.server.metier.EOConvention value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOConvention oldValue = convention();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
	}

	public void setReimputationRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOReimputation oldValue = reimputation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
	 }
	}


	/**
	* Créer une instance de EOReimputationNewConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOReimputationNewConvention createEOReimputationNewConvention(EOEditingContext editingContext				, java.math.BigDecimal recoHtSaisie
							, java.math.BigDecimal recoMontantBudgetaire
							, java.math.BigDecimal recoTtcSaisie
							, java.math.BigDecimal recoTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOConvention convention		, org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation					) {
	 EOReimputationNewConvention eo = (EOReimputationNewConvention) EOUtilities.createAndInsertInstance(editingContext, _EOReimputationNewConvention.ENTITY_NAME);	 
							eo.setRecoHtSaisie(recoHtSaisie);
									eo.setRecoMontantBudgetaire(recoMontantBudgetaire);
									eo.setRecoTtcSaisie(recoTtcSaisie);
									eo.setRecoTvaSaisie(recoTvaSaisie);
						 eo.setConventionRelationship(convention);
				 eo.setReimputationRelationship(reimputation);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputationNewConvention creerInstance(EOEditingContext editingContext) {
		EOReimputationNewConvention object = (EOReimputationNewConvention)EOUtilities.createAndInsertInstance(editingContext, _EOReimputationNewConvention.ENTITY_NAME);
  		return object;
		}

	

  public EOReimputationNewConvention localInstanceIn(EOEditingContext editingContext) {
    EOReimputationNewConvention localInstance = (EOReimputationNewConvention)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputationNewConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputationNewConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReimputationNewConvention> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationNewConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationNewConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationNewConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationNewConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReimputationNewConvention> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationNewConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationNewConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputationNewConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationNewConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationNewConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationNewConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
