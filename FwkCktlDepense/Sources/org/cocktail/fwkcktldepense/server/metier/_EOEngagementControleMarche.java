/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEngagementControleMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEngagementControleMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControleMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_MARCHE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> EMAR_DATE_SAISIE = new ERXKey<NSTimestamp>("emarDateSaisie");
	public static final ERXKey<java.math.BigDecimal> EMAR_HT_RESTE = new ERXKey<java.math.BigDecimal>("emarHtReste");
	public static final ERXKey<java.math.BigDecimal> EMAR_HT_SAISIE = new ERXKey<java.math.BigDecimal>("emarHtSaisie");
	public static final ERXKey<java.math.BigDecimal> EMAR_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("emarMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> EMAR_MONTANT_BUDGETAIRE_RESTE = new ERXKey<java.math.BigDecimal>("emarMontantBudgetaireReste");
	public static final ERXKey<java.math.BigDecimal> EMAR_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("emarTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> EMAR_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("emarTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttribution> ATTRIBUTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttribution>("attribution");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "emarId";

	public static final String EMAR_DATE_SAISIE_KEY = "emarDateSaisie";
	public static final String EMAR_HT_RESTE_KEY = "emarHtReste";
	public static final String EMAR_HT_SAISIE_KEY = "emarHtSaisie";
	public static final String EMAR_MONTANT_BUDGETAIRE_KEY = "emarMontantBudgetaire";
	public static final String EMAR_MONTANT_BUDGETAIRE_RESTE_KEY = "emarMontantBudgetaireReste";
	public static final String EMAR_TTC_SAISIE_KEY = "emarTtcSaisie";
	public static final String EMAR_TVA_SAISIE_KEY = "emarTvaSaisie";

//Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String EMAR_ID_KEY = "emarId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String EMAR_DATE_SAISIE_COLKEY = "EMAR_DATE_SAISIE";
	public static final String EMAR_HT_RESTE_COLKEY = "EMAR_HT_RESTE";
	public static final String EMAR_HT_SAISIE_COLKEY = "EMAR_HT_SAISIE";
	public static final String EMAR_MONTANT_BUDGETAIRE_COLKEY = "EMAR_MONTANT_BUDGETAIRE";
	public static final String EMAR_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EMAR_MONTANT_BUDGETAIRE_RESTE";
	public static final String EMAR_TTC_SAISIE_COLKEY = "EMAR_TTC_SAISIE";
	public static final String EMAR_TVA_SAISIE_COLKEY = "EMAR_TVA_SAISIE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String EMAR_ID_COLKEY = "EMAR_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public NSTimestamp emarDateSaisie() {
	 return (NSTimestamp) storedValueForKey(EMAR_DATE_SAISIE_KEY);
	}

	public void setEmarDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, EMAR_DATE_SAISIE_KEY);
	}

	public java.math.BigDecimal emarHtReste() {
	 return (java.math.BigDecimal) storedValueForKey(EMAR_HT_RESTE_KEY);
	}

	public void setEmarHtReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMAR_HT_RESTE_KEY);
	}

	public java.math.BigDecimal emarHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EMAR_HT_SAISIE_KEY);
	}

	public void setEmarHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMAR_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal emarMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(EMAR_MONTANT_BUDGETAIRE_KEY);
	}

	public void setEmarMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMAR_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal emarMontantBudgetaireReste() {
	 return (java.math.BigDecimal) storedValueForKey(EMAR_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public void setEmarMontantBudgetaireReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMAR_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public java.math.BigDecimal emarTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EMAR_TTC_SAISIE_KEY);
	}

	public void setEmarTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMAR_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal emarTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EMAR_TVA_SAISIE_KEY);
	}

	public void setEmarTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EMAR_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAttribution attribution() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
	}

	public void setAttributionRelationship(org.cocktail.fwkcktldepense.server.metier.EOAttribution value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAttribution oldValue = attribution();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOEngagementControleMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEngagementControleMarche createEOEngagementControleMarche(EOEditingContext editingContext				, NSTimestamp emarDateSaisie
							, java.math.BigDecimal emarHtReste
							, java.math.BigDecimal emarHtSaisie
							, java.math.BigDecimal emarMontantBudgetaire
							, java.math.BigDecimal emarMontantBudgetaireReste
							, java.math.BigDecimal emarTtcSaisie
							, java.math.BigDecimal emarTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOAttribution attribution		, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOEngagementControleMarche eo = (EOEngagementControleMarche) EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControleMarche.ENTITY_NAME);	 
							eo.setEmarDateSaisie(emarDateSaisie);
									eo.setEmarHtReste(emarHtReste);
									eo.setEmarHtSaisie(emarHtSaisie);
									eo.setEmarMontantBudgetaire(emarMontantBudgetaire);
									eo.setEmarMontantBudgetaireReste(emarMontantBudgetaireReste);
									eo.setEmarTtcSaisie(emarTtcSaisie);
									eo.setEmarTvaSaisie(emarTvaSaisie);
						 eo.setAttributionRelationship(attribution);
				 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngagementControleMarche creerInstance(EOEditingContext editingContext) {
		EOEngagementControleMarche object = (EOEngagementControleMarche)EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControleMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOEngagementControleMarche localInstanceIn(EOEditingContext editingContext) {
    EOEngagementControleMarche localInstance = (EOEngagementControleMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngagementControleMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngagementControleMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEngagementControleMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControleMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControleMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControleMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControleMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEngagementControleMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControleMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControleMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngagementControleMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControleMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControleMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControleMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
