/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOReimputationPlanco.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOReimputationPlanco extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationPlanco";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_PLANCO";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> REPC_HT_SAISIE = new ERXKey<java.math.BigDecimal>("repcHtSaisie");
	public static final ERXKey<java.math.BigDecimal> REPC_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("repcMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> REPC_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("repcTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> REPC_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("repcTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation> REIMPUTATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation>("reimputation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "repcId";

	public static final String REPC_HT_SAISIE_KEY = "repcHtSaisie";
	public static final String REPC_MONTANT_BUDGETAIRE_KEY = "repcMontantBudgetaire";
	public static final String REPC_TTC_SAISIE_KEY = "repcTtcSaisie";
	public static final String REPC_TVA_SAISIE_KEY = "repcTvaSaisie";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String REIM_ID_KEY = "reimId";
	public static final String REPC_ID_KEY = "repcId";

//Colonnes dans la base de donnees
	public static final String REPC_HT_SAISIE_COLKEY = "REPC_HT_SAISIE";
	public static final String REPC_MONTANT_BUDGETAIRE_COLKEY = "REPC_MONTANT_BUDGETAIRE";
	public static final String REPC_TTC_SAISIE_COLKEY = "REPC_TTC_SAISIE";
	public static final String REPC_TVA_SAISIE_COLKEY = "REPC_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String REPC_ID_COLKEY = "REPC_ID";


	// Relationships
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String REIMPUTATION_KEY = "reimputation";



	// Accessors methods
	public java.math.BigDecimal repcHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REPC_HT_SAISIE_KEY);
	}

	public void setRepcHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REPC_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal repcMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(REPC_MONTANT_BUDGETAIRE_KEY);
	}

	public void setRepcMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REPC_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal repcTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REPC_TTC_SAISIE_KEY);
	}

	public void setRepcTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REPC_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal repcTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REPC_TVA_SAISIE_KEY);
	}

	public void setRepcTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REPC_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
	}

	public void setReimputationRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOReimputation oldValue = reimputation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
	 }
	}


	/**
	* Créer une instance de EOReimputationPlanco avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOReimputationPlanco createEOReimputationPlanco(EOEditingContext editingContext				, java.math.BigDecimal repcHtSaisie
							, java.math.BigDecimal repcMontantBudgetaire
							, java.math.BigDecimal repcTtcSaisie
							, java.math.BigDecimal repcTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable		, org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation					) {
	 EOReimputationPlanco eo = (EOReimputationPlanco) EOUtilities.createAndInsertInstance(editingContext, _EOReimputationPlanco.ENTITY_NAME);	 
							eo.setRepcHtSaisie(repcHtSaisie);
									eo.setRepcMontantBudgetaire(repcMontantBudgetaire);
									eo.setRepcTtcSaisie(repcTtcSaisie);
									eo.setRepcTvaSaisie(repcTvaSaisie);
						 eo.setPlanComptableRelationship(planComptable);
				 eo.setReimputationRelationship(reimputation);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputationPlanco creerInstance(EOEditingContext editingContext) {
		EOReimputationPlanco object = (EOReimputationPlanco)EOUtilities.createAndInsertInstance(editingContext, _EOReimputationPlanco.ENTITY_NAME);
  		return object;
		}

	

  public EOReimputationPlanco localInstanceIn(EOEditingContext editingContext) {
    EOReimputationPlanco localInstance = (EOReimputationPlanco)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputationPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputationPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReimputationPlanco> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReimputationPlanco> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputationPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
