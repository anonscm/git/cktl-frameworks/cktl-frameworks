/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleActionException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPreDepenseControleAction extends _EOPreDepenseControleAction implements _IDepenseControleAction {
	private static Calculs serviceCalculs = Calculs.getInstance();
	

	private BigDecimal pourcentage;
	public static String PDACT_POURCENTAGE = "pourcentage";

	public EOPreDepenseControleAction() {
		super();
		setPourcentage(new BigDecimal(0.0));
	}

	public BigDecimal montantTtc() {
		return dactTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		if (preDepenseBudget() != null && preDepenseBudget().depTtcSaisie() != null && preDepenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(preDepenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (preDepenseBudget() != null) {
			NSArray depenseControleActions = preDepenseBudget().preDepenseControleActions();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPreDepenseControleAction.TYPE_ACTION_KEY + " != nil", null);
			depenseControleActions = EOQualifier.filteredArrayWithQualifier(depenseControleActions, qual);
			BigDecimal total = preDepenseBudget().computeSumForKey(depenseControleActions, EOPreDepenseControleAction.PDACT_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (preDepenseBudget() != null)
			preDepenseBudget().corrigerMontantActions();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDactHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDactHtSaisie(aValue);
	}

	public void setDactTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDactTvaSaisie(aValue);
	}

	public void setDactTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDactTtcSaisie(aValue);
	}

	public void setDactMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDactMontantBudgetaire(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws DepenseControleActionException {
		if (dactHtSaisie() == null)
			throw new DepenseControleActionException(DepenseControleActionException.dactHtSaisieManquant);
		if (dactTvaSaisie() == null)
			throw new DepenseControleActionException(DepenseControleActionException.dactTvaSaisieManquant);
		if (dactTtcSaisie() == null)
			throw new DepenseControleActionException(DepenseControleActionException.dactTtcSaisieManquant);
		if (dactMontantBudgetaire() == null)
			throw new DepenseControleActionException(DepenseControleActionException.dactMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControleActionException(DepenseControleActionException.exerciceManquant);
		if (preDepenseBudget() == null)
			throw new DepenseControleActionException(DepenseControleActionException.depenseBudgetManquant);

		if (!dactHtSaisie().abs().add(dactTvaSaisie().abs()).equals(dactTtcSaisie().abs()))
			setDactTvaSaisie(dactTtcSaisie().subtract(dactHtSaisie()));
		if (!dactMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDactMontantBudgetaire(calculMontantBudgetaire());
		if (!preDepenseBudget().exercice().equals(exercice()))
			throw new DepenseControleActionException(DepenseControleActionException.depenseBudgetExercicePasCoherent);
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(dactHtSaisie(), dactTvaSaisie(), preDepenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (typeAction() == null)
			throw new DepenseControleActionException(DepenseControleActionException.typeActionManquant);
	}

}