/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOSaCommande.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOSaCommande extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleSaCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_SA_COMMANDE";


//Attribute Keys
	public static final ERXKey<String> SA_EMAILS = new ERXKey<String>("saEmails");
	public static final ERXKey<String> SA_LIBELLE = new ERXKey<String>("saLibelle");
	public static final ERXKey<String> VLCO_ETAT = new ERXKey<String>("vlcoEtat");
	public static final ERXKey<String> VLCO_VALIDE = new ERXKey<String>("vlcoValide");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commande");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vcloId";

	public static final String SA_EMAILS_KEY = "saEmails";
	public static final String SA_LIBELLE_KEY = "saLibelle";
	public static final String VLCO_ETAT_KEY = "vlcoEtat";
	public static final String VLCO_VALIDE_KEY = "vlcoValide";

//Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String VCLO_ID_KEY = "vcloId";

//Colonnes dans la base de donnees
	public static final String SA_EMAILS_COLKEY = "SA_EMAILS";
	public static final String SA_LIBELLE_COLKEY = "SA_LIBELLE";
	public static final String VLCO_ETAT_COLKEY = "VLCO_ETAT";
	public static final String VLCO_VALIDE_COLKEY = "VLCO_VALIDE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String VCLO_ID_COLKEY = "VCLO_ID";


	// Relationships
	public static final String COMMANDE_KEY = "commande";



	// Accessors methods
	public String saEmails() {
	 return (String) storedValueForKey(SA_EMAILS_KEY);
	}

	public void setSaEmails(String value) {
	 takeStoredValueForKey(value, SA_EMAILS_KEY);
	}

	public String saLibelle() {
	 return (String) storedValueForKey(SA_LIBELLE_KEY);
	}

	public void setSaLibelle(String value) {
	 takeStoredValueForKey(value, SA_LIBELLE_KEY);
	}

	public String vlcoEtat() {
	 return (String) storedValueForKey(VLCO_ETAT_KEY);
	}

	public void setVlcoEtat(String value) {
	 takeStoredValueForKey(value, VLCO_ETAT_KEY);
	}

	public String vlcoValide() {
	 return (String) storedValueForKey(VLCO_VALIDE_KEY);
	}

	public void setVlcoValide(String value) {
	 takeStoredValueForKey(value, VLCO_VALIDE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande commande() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
	}

	public void setCommandeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commande();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
	 }
	}


	/**
	* Créer une instance de EOSaCommande avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOSaCommande createEOSaCommande(EOEditingContext editingContext						, String saLibelle
							, String vlcoEtat
							, String vlcoValide
					, org.cocktail.fwkcktldepense.server.metier.EOCommande commande					) {
	 EOSaCommande eo = (EOSaCommande) EOUtilities.createAndInsertInstance(editingContext, _EOSaCommande.ENTITY_NAME);	 
									eo.setSaLibelle(saLibelle);
									eo.setVlcoEtat(vlcoEtat);
									eo.setVlcoValide(vlcoValide);
						 eo.setCommandeRelationship(commande);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSaCommande creerInstance(EOEditingContext editingContext) {
		EOSaCommande object = (EOSaCommande)EOUtilities.createAndInsertInstance(editingContext, _EOSaCommande.ENTITY_NAME);
  		return object;
		}

	

  public EOSaCommande localInstanceIn(EOEditingContext editingContext) {
    EOSaCommande localInstance = (EOSaCommande)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOSaCommande>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSaCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSaCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSaCommande> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSaCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSaCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSaCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSaCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSaCommande> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSaCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSaCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSaCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSaCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSaCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSaCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
