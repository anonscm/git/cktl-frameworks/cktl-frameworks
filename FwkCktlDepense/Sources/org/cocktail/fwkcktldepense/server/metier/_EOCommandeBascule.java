/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeBascule.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeBascule extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeBascule";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_BASCULE";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE_DESTINATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commandeDestination");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE_ORIGINE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commandeOrigine");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "combId";


//Attributs non visibles
	public static final String COMB_ID_KEY = "combId";
	public static final String COMM_ID_DESTINATION_KEY = "commIdDestination";
	public static final String COMM_ID_ORIGINE_KEY = "commIdOrigine";

//Colonnes dans la base de donnees

	public static final String COMB_ID_COLKEY = "COMB_ID";
	public static final String COMM_ID_DESTINATION_COLKEY = "COMM_ID_DESTINATION";
	public static final String COMM_ID_ORIGINE_COLKEY = "COMM_ID_ORIGINE";


	// Relationships
	public static final String COMMANDE_DESTINATION_KEY = "commandeDestination";
	public static final String COMMANDE_ORIGINE_KEY = "commandeOrigine";



	// Accessors methods
	public org.cocktail.fwkcktldepense.server.metier.EOCommande commandeDestination() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_DESTINATION_KEY);
	}

	public void setCommandeDestinationRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commandeDestination();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_DESTINATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_DESTINATION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande commandeOrigine() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_ORIGINE_KEY);
	}

	public void setCommandeOrigineRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commandeOrigine();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_ORIGINE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_ORIGINE_KEY);
	 }
	}


	/**
	* Créer une instance de EOCommandeBascule avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeBascule createEOCommandeBascule(EOEditingContext editingContext		, org.cocktail.fwkcktldepense.server.metier.EOCommande commandeDestination		, org.cocktail.fwkcktldepense.server.metier.EOCommande commandeOrigine					) {
	 EOCommandeBascule eo = (EOCommandeBascule) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeBascule.ENTITY_NAME);	 
				 eo.setCommandeDestinationRelationship(commandeDestination);
				 eo.setCommandeOrigineRelationship(commandeOrigine);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeBascule creerInstance(EOEditingContext editingContext) {
		EOCommandeBascule object = (EOCommandeBascule)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeBascule.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeBascule localInstanceIn(EOEditingContext editingContext) {
    EOCommandeBascule localInstance = (EOCommandeBascule)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeBascule fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeBascule fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeBascule> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeBascule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeBascule)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeBascule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeBascule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeBascule> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeBascule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeBascule)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeBascule fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeBascule eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeBascule ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeBascule fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeBascule> objectsForRecherche(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCommandeBascule.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
