/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCatalogueArticle.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCatalogueArticle extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCatalogueArticle";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CATALOGUE_ARTICLE";


//Attribute Keys
	public static final ERXKey<String> CAAR_LIBELLE = new ERXKey<String>("caarLibelle");
	public static final ERXKey<java.math.BigDecimal> CAAR_PRIX_HT = new ERXKey<java.math.BigDecimal>("caarPrixHt");
	public static final ERXKey<java.math.BigDecimal> CAAR_PRIX_TTC = new ERXKey<java.math.BigDecimal>("caarPrixTtc");
	public static final ERXKey<String> CAAR_REFERENCE = new ERXKey<String>("caarReference");
	public static final ERXKey<NSTimestamp> CAT_DATE_DEBUT = new ERXKey<NSTimestamp>("catDateDebut");
	public static final ERXKey<NSTimestamp> CAT_DATE_FIN = new ERXKey<NSTimestamp>("catDateFin");
	public static final ERXKey<String> CAT_LIBELLE = new ERXKey<String>("catLibelle");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttribution> ATTRIBUTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttribution>("attribution");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> CATALOGUE_ARTICLE_PERE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle>("catalogueArticlePere");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> CODE_MARCHE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche>("codeMarche");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("fournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTva> TVA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTva>("tva");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeApplication> TYPE_APPLICATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeApplication>("typeApplication");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeArticle> TYPE_ARTICLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeArticle>("typeArticle");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "caarId";

	public static final String CAAR_LIBELLE_KEY = "caarLibelle";
	public static final String CAAR_PRIX_HT_KEY = "caarPrixHt";
	public static final String CAAR_PRIX_TTC_KEY = "caarPrixTtc";
	public static final String CAAR_REFERENCE_KEY = "caarReference";
	public static final String CAT_DATE_DEBUT_KEY = "catDateDebut";
	public static final String CAT_DATE_FIN_KEY = "catDateFin";
	public static final String CAT_LIBELLE_KEY = "catLibelle";

//Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String CAAR_ID_KEY = "caarId";
	public static final String CAAR_ID_PERE_KEY = "caarIdPere";
	public static final String CAT_ID_KEY = "catId";
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String TYAR_ID_KEY = "tyarId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CAAR_LIBELLE_COLKEY = "CAAR_LIBELLE";
	public static final String CAAR_PRIX_HT_COLKEY = "CAAR_PRIX_HT";
	public static final String CAAR_PRIX_TTC_COLKEY = "CAAR_PRIX_TTC";
	public static final String CAAR_REFERENCE_COLKEY = "CAAR_REFERENCE";
	public static final String CAT_DATE_DEBUT_COLKEY = "CAT_DATE_DEBUT";
	public static final String CAT_DATE_FIN_COLKEY = "CAT_DATE_FIN";
	public static final String CAT_LIBELLE_COLKEY = "CAT_LIBELLE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String CAAR_ID_COLKEY = "CAAR_ID";
	public static final String CAAR_ID_PERE_COLKEY = "CAAR_ID_PERE";
	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String TYAR_ID_COLKEY = "TYAR_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String CATALOGUE_ARTICLE_PERE_KEY = "catalogueArticlePere";
	public static final String CODE_MARCHE_KEY = "codeMarche";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String TVA_KEY = "tva";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_ARTICLE_KEY = "typeArticle";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
	public String caarLibelle() {
	 return (String) storedValueForKey(CAAR_LIBELLE_KEY);
	}

	public void setCaarLibelle(String value) {
	 takeStoredValueForKey(value, CAAR_LIBELLE_KEY);
	}

	public java.math.BigDecimal caarPrixHt() {
	 return (java.math.BigDecimal) storedValueForKey(CAAR_PRIX_HT_KEY);
	}

	public void setCaarPrixHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CAAR_PRIX_HT_KEY);
	}

	public java.math.BigDecimal caarPrixTtc() {
	 return (java.math.BigDecimal) storedValueForKey(CAAR_PRIX_TTC_KEY);
	}

	public void setCaarPrixTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CAAR_PRIX_TTC_KEY);
	}

	public String caarReference() {
	 return (String) storedValueForKey(CAAR_REFERENCE_KEY);
	}

	public void setCaarReference(String value) {
	 takeStoredValueForKey(value, CAAR_REFERENCE_KEY);
	}

	public NSTimestamp catDateDebut() {
	 return (NSTimestamp) storedValueForKey(CAT_DATE_DEBUT_KEY);
	}

	public void setCatDateDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, CAT_DATE_DEBUT_KEY);
	}

	public NSTimestamp catDateFin() {
	 return (NSTimestamp) storedValueForKey(CAT_DATE_FIN_KEY);
	}

	public void setCatDateFin(NSTimestamp value) {
	 takeStoredValueForKey(value, CAT_DATE_FIN_KEY);
	}

	public String catLibelle() {
	 return (String) storedValueForKey(CAT_LIBELLE_KEY);
	}

	public void setCatLibelle(String value) {
	 takeStoredValueForKey(value, CAT_LIBELLE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAttribution attribution() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
	}

	public void setAttributionRelationship(org.cocktail.fwkcktldepense.server.metier.EOAttribution value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAttribution oldValue = attribution();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle catalogueArticlePere() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle)storedValueForKey(CATALOGUE_ARTICLE_PERE_KEY);
	}

	public void setCatalogueArticlePereRelationship(org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle oldValue = catalogueArticlePere();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_ARTICLE_PERE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_ARTICLE_PERE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeMarche codeMarche() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
	}

	public void setCodeMarcheRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarche value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeMarche oldValue = codeMarche();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = fournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTva tva() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTva)storedValueForKey(TVA_KEY);
	}

	public void setTvaRelationship(org.cocktail.fwkcktldepense.server.metier.EOTva value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTva oldValue = tva();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplication() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
	}

	public void setTypeApplicationRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeApplication value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeApplication oldValue = typeApplication();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeArticle typeArticle() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
	}

	public void setTypeArticleRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeArticle value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeArticle oldValue = typeArticle();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ARTICLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
	}

	public void setTypeEtatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOCatalogueArticle avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCatalogueArticle createEOCatalogueArticle(EOEditingContext editingContext				, String caarLibelle
							, java.math.BigDecimal caarPrixHt
							, java.math.BigDecimal caarPrixTtc
									, NSTimestamp catDateDebut
									, String catLibelle
									, org.cocktail.fwkcktldepense.server.metier.EOCodeMarche codeMarche		, org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur		, org.cocktail.fwkcktldepense.server.metier.EOTva tva		, org.cocktail.fwkcktldepense.server.metier.EOTypeApplication typeApplication		, org.cocktail.fwkcktldepense.server.metier.EOTypeArticle typeArticle		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat					) {
	 EOCatalogueArticle eo = (EOCatalogueArticle) EOUtilities.createAndInsertInstance(editingContext, _EOCatalogueArticle.ENTITY_NAME);	 
							eo.setCaarLibelle(caarLibelle);
									eo.setCaarPrixHt(caarPrixHt);
									eo.setCaarPrixTtc(caarPrixTtc);
											eo.setCatDateDebut(catDateDebut);
											eo.setCatLibelle(catLibelle);
										 eo.setCodeMarcheRelationship(codeMarche);
				 eo.setFournisseurRelationship(fournisseur);
				 eo.setTvaRelationship(tva);
				 eo.setTypeApplicationRelationship(typeApplication);
				 eo.setTypeArticleRelationship(typeArticle);
				 eo.setTypeEtatRelationship(typeEtat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCatalogueArticle creerInstance(EOEditingContext editingContext) {
		EOCatalogueArticle object = (EOCatalogueArticle)EOUtilities.createAndInsertInstance(editingContext, _EOCatalogueArticle.ENTITY_NAME);
  		return object;
		}

	

  public EOCatalogueArticle localInstanceIn(EOEditingContext editingContext) {
    EOCatalogueArticle localInstance = (EOCatalogueArticle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCatalogueArticle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCatalogueArticle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCatalogueArticle> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCatalogueArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCatalogueArticle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCatalogueArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCatalogueArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCatalogueArticle> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCatalogueArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCatalogueArticle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCatalogueArticle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCatalogueArticle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCatalogueArticle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCatalogueArticle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle> objectsForRecherche(EOEditingContext ec,
														String attributionLibelleBinding,
														String attSupprBinding,
														String attValideBinding,
														String catLibelleBinding,
														Integer cleBinding,
														String cnCodeBinding,
														String cnLibelleBinding,
														org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExerBinding,
														NSTimestamp dateBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														String fouCodeBinding,
														String fouNomBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														String libelleBinding,
														String lotSupprBinding,
														String lotValideBinding,
														NSTimestamp marchedateBinding,
														String marchefouCodeBinding,
														String marchefouNomBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur marchefournisseurBinding,
														String marSupprBinding,
														String marValideBinding,
														java.math.BigDecimal prixHtBinding,
														java.math.BigDecimal prixHtMaxBinding,
														java.math.BigDecimal prixHtMinBinding,
														java.math.BigDecimal prixTtcBinding,
														java.math.BigDecimal prixTtcMaxBinding,
														java.math.BigDecimal prixTtcMinBinding,
														String referenceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCatalogueArticle.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (attributionLibelleBinding != null)
			bindings.takeValueForKey(attributionLibelleBinding, "attributionLibelle");
		  if (attSupprBinding != null)
			bindings.takeValueForKey(attSupprBinding, "attSuppr");
		  if (attValideBinding != null)
			bindings.takeValueForKey(attValideBinding, "attValide");
		  if (catLibelleBinding != null)
			bindings.takeValueForKey(catLibelleBinding, "catLibelle");
		  if (cleBinding != null)
			bindings.takeValueForKey(cleBinding, "cle");
		  if (cnCodeBinding != null)
			bindings.takeValueForKey(cnCodeBinding, "cnCode");
		  if (cnLibelleBinding != null)
			bindings.takeValueForKey(cnLibelleBinding, "cnLibelle");
		  if (codeExerBinding != null)
			bindings.takeValueForKey(codeExerBinding, "codeExer");
		  if (dateBinding != null)
			bindings.takeValueForKey(dateBinding, "date");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (libelleBinding != null)
			bindings.takeValueForKey(libelleBinding, "libelle");
		  if (lotSupprBinding != null)
			bindings.takeValueForKey(lotSupprBinding, "lotSuppr");
		  if (lotValideBinding != null)
			bindings.takeValueForKey(lotValideBinding, "lotValide");
		  if (marchedateBinding != null)
			bindings.takeValueForKey(marchedateBinding, "marchedate");
		  if (marchefouCodeBinding != null)
			bindings.takeValueForKey(marchefouCodeBinding, "marchefouCode");
		  if (marchefouNomBinding != null)
			bindings.takeValueForKey(marchefouNomBinding, "marchefouNom");
		  if (marchefournisseurBinding != null)
			bindings.takeValueForKey(marchefournisseurBinding, "marchefournisseur");
		  if (marSupprBinding != null)
			bindings.takeValueForKey(marSupprBinding, "marSuppr");
		  if (marValideBinding != null)
			bindings.takeValueForKey(marValideBinding, "marValide");
		  if (prixHtBinding != null)
			bindings.takeValueForKey(prixHtBinding, "prixHt");
		  if (prixHtMaxBinding != null)
			bindings.takeValueForKey(prixHtMaxBinding, "prixHtMax");
		  if (prixHtMinBinding != null)
			bindings.takeValueForKey(prixHtMinBinding, "prixHtMin");
		  if (prixTtcBinding != null)
			bindings.takeValueForKey(prixTtcBinding, "prixTtc");
		  if (prixTtcMaxBinding != null)
			bindings.takeValueForKey(prixTtcMaxBinding, "prixTtcMax");
		  if (prixTtcMinBinding != null)
			bindings.takeValueForKey(prixTtcMinBinding, "prixTtcMin");
		  if (referenceBinding != null)
			bindings.takeValueForKey(referenceBinding, "reference");
		  if (typeEtatBinding != null)
			bindings.takeValueForKey(typeEtatBinding, "typeEtat");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
