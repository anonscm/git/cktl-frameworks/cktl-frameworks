package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

public interface _IDepenseControleMarche {

	public void setPourcentage(BigDecimal uneDepenseCtrlMarchePourcentage);

	public BigDecimal pourcentage();

	public BigDecimal dmarTtcSaisie();

	public BigDecimal dmarHtSaisie();

	public BigDecimal dmarMontantBudgetaire();

	public void setMontantTtc(BigDecimal uneDepenseCtrlMarcheMontant);

	public BigDecimal montantTtc();

	public void setDmarTtcSaisie(BigDecimal restantTtcControleMarche);

	public EOAttribution attribution();

	public EOExercice exercice();

}
