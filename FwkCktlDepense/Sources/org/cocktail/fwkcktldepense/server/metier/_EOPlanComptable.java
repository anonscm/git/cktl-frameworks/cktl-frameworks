/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPlanComptable.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPlanComptable extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_PLAN_COMPTABLE";


//Attribute Keys
	public static final ERXKey<String> PCO_EMARGEMENT = new ERXKey<String>("pcoEmargement");
	public static final ERXKey<String> PCO_LIBELLE = new ERXKey<String>("pcoLibelle");
	public static final ERXKey<String> PCO_NATURE = new ERXKey<String>("pcoNature");
	public static final ERXKey<String> PCO_NUM = new ERXKey<String>("pcoNum");
	public static final ERXKey<String> PCO_VALIDITE = new ERXKey<String>("pcoValidite");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptableNonImprimable> PLAN_COMPTABLE_NON_IMPRIMABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptableNonImprimable>("planComptableNonImprimable");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> TO_CODE_MARCHE_PLAN_COMPTABLES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable>("toCodeMarchePlanComptables");

	// Attributes


	public static final String PCO_EMARGEMENT_KEY = "pcoEmargement";
	public static final String PCO_LIBELLE_KEY = "pcoLibelle";
	public static final String PCO_NATURE_KEY = "pcoNature";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_VALIDITE_KEY = "pcoValidite";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String PCO_EMARGEMENT_COLKEY = "PCO_EMARGEMENT";
	public static final String PCO_LIBELLE_COLKEY = "PCO_LIBELLE";
	public static final String PCO_NATURE_COLKEY = "PCO_NATURE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_VALIDITE_COLKEY = "PCO_VALIDITE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_NON_IMPRIMABLE_KEY = "planComptableNonImprimable";
	public static final String TO_CODE_MARCHE_PLAN_COMPTABLES_KEY = "toCodeMarchePlanComptables";



	// Accessors methods
	public String pcoEmargement() {
	 return (String) storedValueForKey(PCO_EMARGEMENT_KEY);
	}

	public void setPcoEmargement(String value) {
	 takeStoredValueForKey(value, PCO_EMARGEMENT_KEY);
	}

	public String pcoLibelle() {
	 return (String) storedValueForKey(PCO_LIBELLE_KEY);
	}

	public void setPcoLibelle(String value) {
	 takeStoredValueForKey(value, PCO_LIBELLE_KEY);
	}

	public String pcoNature() {
	 return (String) storedValueForKey(PCO_NATURE_KEY);
	}

	public void setPcoNature(String value) {
	 takeStoredValueForKey(value, PCO_NATURE_KEY);
	}

	public String pcoNum() {
	 return (String) storedValueForKey(PCO_NUM_KEY);
	}

	public void setPcoNum(String value) {
	 takeStoredValueForKey(value, PCO_NUM_KEY);
	}

	public String pcoValidite() {
	 return (String) storedValueForKey(PCO_VALIDITE_KEY);
	}

	public void setPcoValidite(String value) {
	 takeStoredValueForKey(value, PCO_VALIDITE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptableNonImprimable planComptableNonImprimable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptableNonImprimable)storedValueForKey(PLAN_COMPTABLE_NON_IMPRIMABLE_KEY);
	}

	public void setPlanComptableNonImprimableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptableNonImprimable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptableNonImprimable oldValue = planComptableNonImprimable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_NON_IMPRIMABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_NON_IMPRIMABLE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> toCodeMarchePlanComptables() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable>)storedValueForKey(TO_CODE_MARCHE_PLAN_COMPTABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> toCodeMarchePlanComptables(EOQualifier qualifier) {
	 return toCodeMarchePlanComptables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> toCodeMarchePlanComptables(EOQualifier qualifier, boolean fetch) {
	 return toCodeMarchePlanComptables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> toCodeMarchePlanComptables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = toCodeMarchePlanComptables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToToCodeMarchePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TO_CODE_MARCHE_PLAN_COMPTABLES_KEY);
	}
	
	public void removeFromToCodeMarchePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CODE_MARCHE_PLAN_COMPTABLES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable createToCodeMarchePlanComptablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TO_CODE_MARCHE_PLAN_COMPTABLES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable) eo;
	}
	
	public void deleteToCodeMarchePlanComptablesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CODE_MARCHE_PLAN_COMPTABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllToCodeMarchePlanComptablesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCodeMarchePlanComptable> objects = toCodeMarchePlanComptables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteToCodeMarchePlanComptablesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPlanComptable avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPlanComptable createEOPlanComptable(EOEditingContext editingContext				, String pcoEmargement
							, String pcoLibelle
							, String pcoNature
							, String pcoNum
							, String pcoValidite
												) {
	 EOPlanComptable eo = (EOPlanComptable) EOUtilities.createAndInsertInstance(editingContext, _EOPlanComptable.ENTITY_NAME);	 
							eo.setPcoEmargement(pcoEmargement);
									eo.setPcoLibelle(pcoLibelle);
									eo.setPcoNature(pcoNature);
									eo.setPcoNum(pcoNum);
									eo.setPcoValidite(pcoValidite);
									 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanComptable creerInstance(EOEditingContext editingContext) {
		EOPlanComptable object = (EOPlanComptable)EOUtilities.createAndInsertInstance(editingContext, _EOPlanComptable.ENTITY_NAME);
  		return object;
		}

	

  public EOPlanComptable localInstanceIn(EOEditingContext editingContext) {
    EOPlanComptable localInstance = (EOPlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPlanComptable> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPlanComptable> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> objectsForRecherche(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														String pcoNumBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOPlanComptable.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (pcoNumBinding != null)
			bindings.takeValueForKey(pcoNumBinding, "pcoNum");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
