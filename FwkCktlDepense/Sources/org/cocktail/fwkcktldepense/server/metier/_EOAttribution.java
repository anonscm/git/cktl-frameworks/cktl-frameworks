/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOAttribution.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOAttribution extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleAttribution";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_ATTRIBUTION";


//Attribute Keys
	public static final ERXKey<NSTimestamp> ATT_DEBUT = new ERXKey<NSTimestamp>("attDebut");
	public static final ERXKey<NSTimestamp> ATT_FIN = new ERXKey<NSTimestamp>("attFin");
	public static final ERXKey<java.math.BigDecimal> ATT_HT = new ERXKey<java.math.BigDecimal>("attHt");
	public static final ERXKey<String> ATT_SUPPR = new ERXKey<String>("attSuppr");
	public static final ERXKey<String> ATT_VALIDE = new ERXKey<String>("attValide");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> ATTRIBUTION_EXECUTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution>("attributionExecutions");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("fournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOLot> LOT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOLot>("lot");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> SOUS_TRAITANTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant>("sousTraitants");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> TO_RIB_FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur>("toRibFournisseur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "attOrdre";

	public static final String ATT_DEBUT_KEY = "attDebut";
	public static final String ATT_FIN_KEY = "attFin";
	public static final String ATT_HT_KEY = "attHt";
	public static final String ATT_SUPPR_KEY = "attSuppr";
	public static final String ATT_VALIDE_KEY = "attValide";

//Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TIT_ORDRE_KEY = "titOrdre";

//Colonnes dans la base de donnees
	public static final String ATT_DEBUT_COLKEY = "ATT_DEBUT";
	public static final String ATT_FIN_COLKEY = "ATT_FIN";
	public static final String ATT_HT_COLKEY = "ATT_HT";
	public static final String ATT_SUPPR_COLKEY = "ATT_SUPPR";
	public static final String ATT_VALIDE_COLKEY = "ATT_VALIDE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";


	// Relationships
	public static final String ATTRIBUTION_EXECUTIONS_KEY = "attributionExecutions";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String LOT_KEY = "lot";
	public static final String SOUS_TRAITANTS_KEY = "sousTraitants";
	public static final String TO_RIB_FOURNISSEUR_KEY = "toRibFournisseur";



	// Accessors methods
	public NSTimestamp attDebut() {
	 return (NSTimestamp) storedValueForKey(ATT_DEBUT_KEY);
	}

	public void setAttDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, ATT_DEBUT_KEY);
	}

	public NSTimestamp attFin() {
	 return (NSTimestamp) storedValueForKey(ATT_FIN_KEY);
	}

	public void setAttFin(NSTimestamp value) {
	 takeStoredValueForKey(value, ATT_FIN_KEY);
	}

	public java.math.BigDecimal attHt() {
	 return (java.math.BigDecimal) storedValueForKey(ATT_HT_KEY);
	}

	public void setAttHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, ATT_HT_KEY);
	}

	public String attSuppr() {
	 return (String) storedValueForKey(ATT_SUPPR_KEY);
	}

	public void setAttSuppr(String value) {
	 takeStoredValueForKey(value, ATT_SUPPR_KEY);
	}

	public String attValide() {
	 return (String) storedValueForKey(ATT_VALIDE_KEY);
	}

	public void setAttValide(String value) {
	 takeStoredValueForKey(value, ATT_VALIDE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = fournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOLot lot() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOLot)storedValueForKey(LOT_KEY);
	}

	public void setLotRelationship(org.cocktail.fwkcktldepense.server.metier.EOLot value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOLot oldValue = lot();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EORibFournisseur toRibFournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EORibFournisseur)storedValueForKey(TO_RIB_FOURNISSEUR_KEY);
	}

	public void setToRibFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EORibFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EORibFournisseur oldValue = toRibFournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_FOURNISSEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> attributionExecutions() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution>)storedValueForKey(ATTRIBUTION_EXECUTIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> attributionExecutions(EOQualifier qualifier) {
	 return attributionExecutions(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> attributionExecutions(EOQualifier qualifier, boolean fetch) {
	 return attributionExecutions(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> attributionExecutions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution.ATTRIBUTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = attributionExecutions();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAttributionExecutionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ATTRIBUTION_EXECUTIONS_KEY);
	}
	
	public void removeFromAttributionExecutionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ATTRIBUTION_EXECUTIONS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution createAttributionExecutionsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ATTRIBUTION_EXECUTIONS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution) eo;
	}
	
	public void deleteAttributionExecutionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ATTRIBUTION_EXECUTIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAttributionExecutionsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOAttributionExecution> objects = attributionExecutions().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAttributionExecutionsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> sousTraitants() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant>)storedValueForKey(SOUS_TRAITANTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> sousTraitants(EOQualifier qualifier) {
	 return sousTraitants(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> sousTraitants(EOQualifier qualifier, boolean fetch) {
	 return sousTraitants(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> sousTraitants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOSousTraitant.ATTRIBUTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOSousTraitant.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = sousTraitants();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToSousTraitantsRelationship(org.cocktail.fwkcktldepense.server.metier.EOSousTraitant object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, SOUS_TRAITANTS_KEY);
	}
	
	public void removeFromSousTraitantsRelationship(org.cocktail.fwkcktldepense.server.metier.EOSousTraitant object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, SOUS_TRAITANTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOSousTraitant createSousTraitantsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOSousTraitant.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, SOUS_TRAITANTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOSousTraitant) eo;
	}
	
	public void deleteSousTraitantsRelationship(org.cocktail.fwkcktldepense.server.metier.EOSousTraitant object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, SOUS_TRAITANTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllSousTraitantsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOSousTraitant> objects = sousTraitants().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteSousTraitantsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOAttribution avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOAttribution createEOAttribution(EOEditingContext editingContext				, NSTimestamp attDebut
							, NSTimestamp attFin
							, java.math.BigDecimal attHt
							, String attSuppr
							, String attValide
					, org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur		, org.cocktail.fwkcktldepense.server.metier.EOLot lot							) {
	 EOAttribution eo = (EOAttribution) EOUtilities.createAndInsertInstance(editingContext, _EOAttribution.ENTITY_NAME);	 
							eo.setAttDebut(attDebut);
									eo.setAttFin(attFin);
									eo.setAttHt(attHt);
									eo.setAttSuppr(attSuppr);
									eo.setAttValide(attValide);
						 eo.setFournisseurRelationship(fournisseur);
				 eo.setLotRelationship(lot);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAttribution creerInstance(EOEditingContext editingContext) {
		EOAttribution object = (EOAttribution)EOUtilities.createAndInsertInstance(editingContext, _EOAttribution.ENTITY_NAME);
  		return object;
		}

	

  public EOAttribution localInstanceIn(EOEditingContext editingContext) {
    EOAttribution localInstance = (EOAttribution)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAttribution fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAttribution fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOAttribution> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOAttribution> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAttribution fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAttribution eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAttribution ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAttribution fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAttribution> objectsForRecherche(EOEditingContext ec,
														String attSupprBinding,
														String attValideBinding,
														org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExerBinding,
														NSTimestamp dateBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														org.cocktail.fwkcktldepense.server.metier.EOLot lotBinding,
														String lotSupprBinding,
														String lotValideBinding,
														org.cocktail.fwkcktldepense.server.metier.EOMarche marcheBinding,
														String marSupprBinding,
														String marValideBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOAttribution.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (attSupprBinding != null)
			bindings.takeValueForKey(attSupprBinding, "attSuppr");
		  if (attValideBinding != null)
			bindings.takeValueForKey(attValideBinding, "attValide");
		  if (codeExerBinding != null)
			bindings.takeValueForKey(codeExerBinding, "codeExer");
		  if (dateBinding != null)
			bindings.takeValueForKey(dateBinding, "date");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (lotBinding != null)
			bindings.takeValueForKey(lotBinding, "lot");
		  if (lotSupprBinding != null)
			bindings.takeValueForKey(lotSupprBinding, "lotSuppr");
		  if (lotValideBinding != null)
			bindings.takeValueForKey(lotValideBinding, "lotValide");
		  if (marcheBinding != null)
			bindings.takeValueForKey(marcheBinding, "marche");
		  if (marSupprBinding != null)
			bindings.takeValueForKey(marSupprBinding, "marSuppr");
		  if (marValideBinding != null)
			bindings.takeValueForKey(marValideBinding, "marValide");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
