

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleConventionException;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;



public class EOEngagementControleConvention extends _EOEngagementControleConvention
{
	private BigDecimal pourcentage;
	public static String ECON_POURCENTAGE="pourcentage";

    public EOEngagementControleConvention() {
        super();
        setPourcentage(new BigDecimal(0.0));
    }

    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

    	if (engagementBudget()!=null && engagementBudget().engTtcSaisie()!=null && engagementBudget().engTtcSaisie().floatValue()!=0.0)
    		setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(engagementBudget().engTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
    	pourcentage=aValue;

    	if (engagementBudget()!=null) {
    		NSArray engageControleConventions = engagementBudget().engagementControleConventions();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControleConvention.CONVENTION_KEY+" != nil", null);
    		engageControleConventions = EOQualifier.filteredArrayWithQualifier(engageControleConventions, qual);
    		BigDecimal total=engagementBudget().computeSumForKey(engageControleConventions, EOEngagementControleConvention.ECON_POURCENTAGE);
    		if (total.floatValue()>100.0)
    	        pourcentage=new BigDecimal(100.0).subtract(total.subtract(aValue));    			
    	}

        if (engagementBudget()!=null)
        	engagementBudget().corrigerMontantActions();
    }

    public BigDecimal pourcentage() {
    	return pourcentage;
    }

    public void setEconHtSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEconHtSaisie(aValue);
    }
    
    public void setEconTvaSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEconTvaSaisie(aValue);
    }

    public void setEconTtcSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEconTtcSaisie(aValue);
    }

    public void setEconMontantBudgetaire(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEconMontantBudgetaire(aValue);
    }

    public void setEconMontantBudgetaireReste(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEconMontantBudgetaireReste(aValue);
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        if (econHtSaisie()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.econHtSaisieManquant);
        if (econTvaSaisie()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.econTvaSaisieManquant);
        if (econTtcSaisie()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.econTtcSaisieManquant);
        if (econMontantBudgetaire()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.econMontantBudgetaireManquant);
        if (econMontantBudgetaireReste()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.econMontantBudgetaireResteManquant);
        if (econDateSaisie()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.econDateSaisieManquant);
        if (convention()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.conventionManquant);
        if (exercice()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.exerciceManquant);
        if (engagementBudget()==null)
            throw new EngagementControleConventionException(EngagementControleConventionException.engagementBudgetManquant);
        
        if (!econHtSaisie().abs().add(econTvaSaisie().abs()).equals(econTtcSaisie().abs()))
        	setEconTvaSaisie(econTtcSaisie().subtract(econHtSaisie()));
//        if (!econHtSaisie().abs().add(econTvaSaisie().abs()).equals(econTtcSaisie().abs()))
//        	throw new EngagementControleConventionException(EngagementControleConventionException.econTtcSaisiePasCoherent);
//        if (!econMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(econHtSaisie(), econTvaSaisie())))
//        	setEconMontantBudgetaire(engagementBudget().tauxProrata().montantBudgetaire(econHtSaisie(), econTvaSaisie()));        
//        if (!econMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(econHtSaisie(), econTvaSaisie())))
//        	throw new EngagementControleConventionException(EngagementControleConventionException.econMontantBudgetairePasCoherent);

        if (econTvaSaisie().floatValue()<0.0 || econHtSaisie().floatValue()<0.0 || econTtcSaisie().floatValue()<0.0 ||
        		econMontantBudgetaire().floatValue()<0.0 || econMontantBudgetaireReste().floatValue()<0.0)
        	throw new EngagementControleConventionException(EngagementControleConventionException.montantsNegatifs);

        if (!engagementBudget().exercice().equals(exercice()))
            throw new EngagementControleConventionException(EngagementControleConventionException.engageBudgetExercicePasCoherent);
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    }
}
