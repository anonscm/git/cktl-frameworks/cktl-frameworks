/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOLot.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOLot extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleLot";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_LOT";


//Attribute Keys
	public static final ERXKey<String> LOT_CATALOGUE = new ERXKey<String>("lotCatalogue");
	public static final ERXKey<NSTimestamp> LOT_DEBUT = new ERXKey<NSTimestamp>("lotDebut");
	public static final ERXKey<NSTimestamp> LOT_FIN = new ERXKey<NSTimestamp>("lotFin");
	public static final ERXKey<java.math.BigDecimal> LOT_HT = new ERXKey<java.math.BigDecimal>("lotHt");
	public static final ERXKey<String> LOT_INDEX = new ERXKey<String>("lotIndex");
	public static final ERXKey<String> LOT_LIBELLE = new ERXKey<String>("lotLibelle");
	public static final ERXKey<String> LOT_SUPPR = new ERXKey<String>("lotSuppr");
	public static final ERXKey<String> LOT_VALIDE = new ERXKey<String>("lotValide");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> LOT_NOMENCLATURES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature>("lotNomenclatures");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> LOT_ORGANS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan>("lotOrgans");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOMarche> MARCHE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOMarche>("marche");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "lotOrdre";

	public static final String LOT_CATALOGUE_KEY = "lotCatalogue";
	public static final String LOT_DEBUT_KEY = "lotDebut";
	public static final String LOT_FIN_KEY = "lotFin";
	public static final String LOT_HT_KEY = "lotHt";
	public static final String LOT_INDEX_KEY = "lotIndex";
	public static final String LOT_LIBELLE_KEY = "lotLibelle";
	public static final String LOT_SUPPR_KEY = "lotSuppr";
	public static final String LOT_VALIDE_KEY = "lotValide";

//Attributs non visibles
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String MAR_ORDRE_KEY = "marOrdre";

//Colonnes dans la base de donnees
	public static final String LOT_CATALOGUE_COLKEY = "LOT_CATALOGUE";
	public static final String LOT_DEBUT_COLKEY = "LOT_DEBUT";
	public static final String LOT_FIN_COLKEY = "LOT_FIN";
	public static final String LOT_HT_COLKEY = "LOT_HT";
	public static final String LOT_INDEX_COLKEY = "LOT_INDEX";
	public static final String LOT_LIBELLE_COLKEY = "LOT_LIBELLE";
	public static final String LOT_SUPPR_COLKEY = "LOT_SUPPR";
	public static final String LOT_VALIDE_COLKEY = "LOT_VALIDE";

	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String MAR_ORDRE_COLKEY = "MAR_ORDRE";


	// Relationships
	public static final String LOT_NOMENCLATURES_KEY = "lotNomenclatures";
	public static final String LOT_ORGANS_KEY = "lotOrgans";
	public static final String MARCHE_KEY = "marche";



	// Accessors methods
	public String lotCatalogue() {
	 return (String) storedValueForKey(LOT_CATALOGUE_KEY);
	}

	public void setLotCatalogue(String value) {
	 takeStoredValueForKey(value, LOT_CATALOGUE_KEY);
	}

	public NSTimestamp lotDebut() {
	 return (NSTimestamp) storedValueForKey(LOT_DEBUT_KEY);
	}

	public void setLotDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, LOT_DEBUT_KEY);
	}

	public NSTimestamp lotFin() {
	 return (NSTimestamp) storedValueForKey(LOT_FIN_KEY);
	}

	public void setLotFin(NSTimestamp value) {
	 takeStoredValueForKey(value, LOT_FIN_KEY);
	}

	public java.math.BigDecimal lotHt() {
	 return (java.math.BigDecimal) storedValueForKey(LOT_HT_KEY);
	}

	public void setLotHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, LOT_HT_KEY);
	}

	public String lotIndex() {
	 return (String) storedValueForKey(LOT_INDEX_KEY);
	}

	public void setLotIndex(String value) {
	 takeStoredValueForKey(value, LOT_INDEX_KEY);
	}

	public String lotLibelle() {
	 return (String) storedValueForKey(LOT_LIBELLE_KEY);
	}

	public void setLotLibelle(String value) {
	 takeStoredValueForKey(value, LOT_LIBELLE_KEY);
	}

	public String lotSuppr() {
	 return (String) storedValueForKey(LOT_SUPPR_KEY);
	}

	public void setLotSuppr(String value) {
	 takeStoredValueForKey(value, LOT_SUPPR_KEY);
	}

	public String lotValide() {
	 return (String) storedValueForKey(LOT_VALIDE_KEY);
	}

	public void setLotValide(String value) {
	 takeStoredValueForKey(value, LOT_VALIDE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOMarche marche() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOMarche)storedValueForKey(MARCHE_KEY);
	}

	public void setMarcheRelationship(org.cocktail.fwkcktldepense.server.metier.EOMarche value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOMarche oldValue = marche();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MARCHE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, MARCHE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> lotNomenclatures() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature>)storedValueForKey(LOT_NOMENCLATURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> lotNomenclatures(EOQualifier qualifier) {
	 return lotNomenclatures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> lotNomenclatures(EOQualifier qualifier, boolean fetch) {
	 return lotNomenclatures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> lotNomenclatures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature.LOT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = lotNomenclatures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLotNomenclaturesRelationship(org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LOT_NOMENCLATURES_KEY);
	}
	
	public void removeFromLotNomenclaturesRelationship(org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LOT_NOMENCLATURES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature createLotNomenclaturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LOT_NOMENCLATURES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature) eo;
	}
	
	public void deleteLotNomenclaturesRelationship(org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LOT_NOMENCLATURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLotNomenclaturesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature> objects = lotNomenclatures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLotNomenclaturesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> lotOrgans() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan>)storedValueForKey(LOT_ORGANS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> lotOrgans(EOQualifier qualifier) {
	 return lotOrgans(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> lotOrgans(EOQualifier qualifier, boolean fetch) {
	 return lotOrgans(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> lotOrgans(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOLotOrgan.LOT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOLotOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = lotOrgans();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLotOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOLotOrgan object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LOT_ORGANS_KEY);
	}
	
	public void removeFromLotOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOLotOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LOT_ORGANS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOLotOrgan createLotOrgansRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOLotOrgan.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LOT_ORGANS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOLotOrgan) eo;
	}
	
	public void deleteLotOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOLotOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LOT_ORGANS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLotOrgansRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOLotOrgan> objects = lotOrgans().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLotOrgansRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOLot avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOLot createEOLot(EOEditingContext editingContext				, String lotCatalogue
													, String lotIndex
							, String lotLibelle
									, org.cocktail.fwkcktldepense.server.metier.EOMarche marche					) {
	 EOLot eo = (EOLot) EOUtilities.createAndInsertInstance(editingContext, _EOLot.ENTITY_NAME);	 
							eo.setLotCatalogue(lotCatalogue);
															eo.setLotIndex(lotIndex);
									eo.setLotLibelle(lotLibelle);
										 eo.setMarcheRelationship(marche);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLot creerInstance(EOEditingContext editingContext) {
		EOLot object = (EOLot)EOUtilities.createAndInsertInstance(editingContext, _EOLot.ENTITY_NAME);
  		return object;
		}

	

  public EOLot localInstanceIn(EOEditingContext editingContext) {
    EOLot localInstance = (EOLot)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLot fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLot fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOLot> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLot eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLot)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLot fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLot fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOLot> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLot eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLot)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLot fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLot eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLot ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLot fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOLot> objectsForRecherche(EOEditingContext ec,
														NSTimestamp dateBinding,
														String lotSupprBinding,
														String lotValideBinding,
														org.cocktail.fwkcktldepense.server.metier.EOMarche marcheBinding,
														String marSupprBinding,
														String marValideBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOLot.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (dateBinding != null)
			bindings.takeValueForKey(dateBinding, "date");
		  if (lotSupprBinding != null)
			bindings.takeValueForKey(lotSupprBinding, "lotSuppr");
		  if (lotValideBinding != null)
			bindings.takeValueForKey(lotValideBinding, "lotValide");
		  if (marcheBinding != null)
			bindings.takeValueForKey(marcheBinding, "marche");
		  if (marSupprBinding != null)
			bindings.takeValueForKey(marSupprBinding, "marSuppr");
		  if (marValideBinding != null)
			bindings.takeValueForKey(marValideBinding, "marValide");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
