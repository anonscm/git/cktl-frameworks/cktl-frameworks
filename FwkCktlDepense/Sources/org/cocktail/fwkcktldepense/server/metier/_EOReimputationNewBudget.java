/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOReimputationNewBudget.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOReimputationNewBudget extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationNewBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_REIMPUTATION_NEW_BUDGET";


//Attribute Keys
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organ");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation> REIMPUTATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation>("reimputation");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata> TAUX_PRORATA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTauxProrata>("tauxProrata");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("typeCredit");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rebuId";


//Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String REBU_ID_KEY = "rebuId";
	public static final String REIM_ID_KEY = "reimId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String REBU_ID_COLKEY = "REBU_ID";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String ORGAN_KEY = "organ";
	public static final String REIMPUTATION_KEY = "reimputation";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organ() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
	}

	public void setOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organ();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
	}

	public void setReimputationRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOReimputation oldValue = reimputation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
	}

	public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.server.metier.EOTauxProrata value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTauxProrata oldValue = tauxProrata();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
	}

	public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = typeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
	 }
	}


	/**
	* Créer une instance de EOReimputationNewBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOReimputationNewBudget createEOReimputationNewBudget(EOEditingContext editingContext		, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOOrgan organ		, org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation		, org.cocktail.fwkcktldepense.server.metier.EOTauxProrata tauxProrata		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit					) {
	 EOReimputationNewBudget eo = (EOReimputationNewBudget) EOUtilities.createAndInsertInstance(editingContext, _EOReimputationNewBudget.ENTITY_NAME);	 
				 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setOrganRelationship(organ);
				 eo.setReimputationRelationship(reimputation);
				 eo.setTauxProrataRelationship(tauxProrata);
				 eo.setTypeCreditRelationship(typeCredit);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputationNewBudget creerInstance(EOEditingContext editingContext) {
		EOReimputationNewBudget object = (EOReimputationNewBudget)EOUtilities.createAndInsertInstance(editingContext, _EOReimputationNewBudget.ENTITY_NAME);
  		return object;
		}

	

  public EOReimputationNewBudget localInstanceIn(EOEditingContext editingContext) {
    EOReimputationNewBudget localInstance = (EOReimputationNewBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputationNewBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputationNewBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReimputationNewBudget> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationNewBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationNewBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationNewBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationNewBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReimputationNewBudget> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationNewBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationNewBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputationNewBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationNewBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationNewBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationNewBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
