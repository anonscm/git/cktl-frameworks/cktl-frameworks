
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.Finder;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOAttribution extends _EOAttribution {
	public static final String ETAT_VALIDE = "O";
	public static final String ETAT_NON_VALIDE = "N";
	public static final String ETAT_SUPPRIME = "O";
	public static final String ETAT_NON_SUPPRIME = "N";
	public static final String LIBELLE_KEY = "libelle";

	public EOAttribution() {
		super();
	}

	public boolean isValideForDate(NSTimestamp date) {
		if (!attValide().equals(EOAttribution.ETAT_VALIDE) || !attSuppr().equals(EOAttribution.ETAT_NON_SUPPRIME))
			return false;
		if (!lot().lotValide().equals(EOLot.ETAT_VALIDE) || !lot().lotSuppr().equals(EOLot.ETAT_NON_SUPPRIME))
			return false;
		if (!lot().marche().marValide().equals(EOMarche.ETAT_VALIDE) || !lot().marche().marSuppr().equals(EOMarche.ETAT_NON_SUPPRIME))
			return false;

		if (date == null)
			date = new NSTimestamp();

		if (ZDateUtil.isAfter(attDebut(), date) || ZDateUtil.isBefore(attFin(), date))
			return false;
		if (ZDateUtil.isAfter(lot().lotDebut(), date) || ZDateUtil.isBefore(lot().lotFin(), date))
			return false;
		if (ZDateUtil.isAfter(lot().marche().marDebut(), date) || ZDateUtil.isBefore(lot().marche().marFin(), date))
			return false;

		return true;
	}

	public String libelle() {
		String chaine = "(" + lot().marche().exercice().exeExercice() + "/" + lot().marche().marIndex() + "/" + lot().lotIndex() + ")-" + lot().lotLibelle();

		if (fournisseur() == null)
			return chaine;
		return fournisseur().fouCode() + "-" + chaine;
	}

	public BigDecimal disponible(EOEditingContext ed) {
		BigDecimal dispo = new BigDecimal("" + super.attHt());
		NSArray attributionExecutions = Finder.fetchArray(ed, EOAttributionExecution.ENTITY_NAME, "attribution=%@", new NSArray(this), null, false);

		for (int i = 0; i < attributionExecutions.count(); i++) {
			EOAttributionExecution attributionExecution = (EOAttributionExecution) attributionExecutions.objectAtIndex(i);

			if (attributionExecution.aeeExecution().compareTo(new BigDecimal(0.0)) != 0)
				dispo = dispo.add(attributionExecution.aeeExecution());
			else {
				dispo = dispo.add(attributionExecution.aeeEngHt());
				dispo = dispo.add(attributionExecution.aeeLiqHt());
			}
		}

		return dispo;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		throw new FactoryException(ENTITY_NAME + " : " + FactoryException.messageErreur);
	}

	public boolean isFournisseurValide(EOFournisseur fournisseur) {
		if (isTitulaire(fournisseur))
			return true;
		if (isSousTraitant(fournisseur))
			return true;
		return false;
	}

	public boolean isTitulaire(EOFournisseur fournisseur) {
		if (fournisseur().equals(fournisseur))
			return true;
		return false;
	}

	public boolean isSousTraitant(EOFournisseur fournisseur) {
		for (int i = 0; i < sousTraitants().count(); i++) {
			if (((EOSousTraitant) sousTraitants().objectAtIndex(i)).fournisseur().equals(fournisseur))
				return true;
		}
		return false;
	}
}
