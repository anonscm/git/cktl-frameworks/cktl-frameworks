package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

public interface _IDepenseControleAction {
	public static final String _TYPE_ACTION_KEY = "typeAction";

	public void setDactTtcSaisie(BigDecimal restantTtcControleAction);

	public void setPourcentage(BigDecimal subtract);

	public EOTypeAction typeAction();

	public void setTypeActionRelationship(EOTypeAction unTypeActionSelectionne);

	public BigDecimal pourcentage();

	public BigDecimal dactHtSaisie();

	public BigDecimal dactTtcSaisie();

	public BigDecimal dactMontantBudgetaire();

	public void setMontantTtc(BigDecimal uneDepenseCtrlActionMontant);

	public BigDecimal montantTtc();

}
