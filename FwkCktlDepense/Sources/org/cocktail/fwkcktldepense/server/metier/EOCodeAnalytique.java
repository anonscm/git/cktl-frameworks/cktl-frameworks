/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSValidation;

public class EOCodeAnalytique extends _EOCodeAnalytique {
	public static String TYET_LIBELLE_PUBLIC = "OUI";
	public static String TYET_LIBELLE_PRIVE = "NON";
	public static String TYET_LIBELLE_UTILISABLE = "OUI";
	public static String TYET_LIBELLE_PAS_UTILISABLE = "NON";

	public static String TYET_LIBELLE_VALIDE = "VALIDE";
	public static String TYET_LIBELLE_ANNULE = "ANNULE";

	public static final String VISIBILITE_LIBELLE_PUBLIC = "Codes publics";
	public static final String VISIBILITE_LIBELLE_PRIVE = "Codes privés";

	public static final EOSortOrdering SORT_CAN_CODE_ASC = EOSortOrdering.sortOrderingWithKey(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
	public static final EOSortOrdering SORT_TYPE_ETAT_PUBLIC_ASC = EOSortOrdering.sortOrderingWithKey(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
	public static final EOSortOrdering SORT_TYPE_ETAT_PUBLIC_DESC = EOSortOrdering.sortOrderingWithKey(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY, EOSortOrdering.CompareCaseInsensitiveDescending);

	public EOCodeAnalytique() {
		super();
	}

	public boolean isPublic() {
		if (typeEtatPublic() == null)
			return false;
		if (typeEtatPublic().tyetLibelle().equals(EOCodeAnalytique.TYET_LIBELLE_PUBLIC))
			return true;
		return false;
	}

	public boolean isUtilisable() {
		if (typeEtatUtilisable() == null)
			return false;
		if (typeEtatUtilisable().tyetLibelle().equals(EOCodeAnalytique.TYET_LIBELLE_UTILISABLE))
			return true;
		return false;
	}

	public boolean isValide() {
		if (typeEtat() == null)
			return false;
		if (typeEtat().tyetLibelle().equals(EOCodeAnalytique.TYET_LIBELLE_VALIDE))
			return true;
		return false;
	}

	public EOCodeAnalytiqueSuivi codeAnalytiqueSuivi() {
		try {
			BigDecimal montant = super.codeAnalytiqueSuivi().montantBudgetaire();
			if (montant == null || montant.equals(NSKeyValueCoding.NullValue))
				return null;
			return super.codeAnalytiqueSuivi();
		} catch (Exception e) {
			return null;
		}
	}

	public String libelle() {
		String libelle = canCode();
		if (canMontant() != null) {
			if (codeAnalytiqueSuivi() == null)
				libelle = libelle + " (0.00/" + canMontant() + ")";
			else
				libelle = libelle + " (" + codeAnalytiqueSuivi().montantBudgetaire() + "/" + canMontant() + ")";
		}
		libelle = libelle + " - " + canLibelle();
		return libelle;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String visibiliteLibelle() {
		return (isPublic() ? VISIBILITE_LIBELLE_PUBLIC : VISIBILITE_LIBELLE_PRIVE);
	}
}
