/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.finder.FinderDevise;
import org.cocktail.fwkcktldepense.server.finder.FinderEcriture;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderInfosReversement;
import org.cocktail.fwkcktldepense.server.finder.FinderOrgan;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeAction;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.interfaces.IFactoryControle;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;
import org.cocktail.fwkcktldepense.server.repartitions.IRepartition;
import org.cocktail.fwkcktldepense.server.service.DepenseService;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public class EODepenseBudget extends _EODepenseBudget implements _IDepenseBudget, IDepenseBudget, ISourceRepartitionCredit {
    private static Calculs serviceCalculs = Calculs.getInstance();
    private static DepenseService serviceDepense = new DepenseService();

    public final static Logger logger = Logger.getLogger(EODepenseBudget.class);
    public static final String RESTE_DISPONIBLE_BUDGETAIRE_POUR_EXTOURNE_KEY = "resteDisponibleBudgetairePourExtourne";

    private NSArray<EOTypeAction> arrayActions;
    private NSArray<EOCodeAnalytique> arrayAnalytiques;
    private NSArray<EOConvention> arrayConventions;
    private NSArray<EOCodeExer> arrayCodeExers;
    private NSArray<EOPlanComptable> arrayPlanComptables;
    private NSArray arrayEmargements;
    private boolean solde;
    private BigDecimal maxReversement;
    private _ISourceCredit source;

    /** Le taux de prorata enregistré (avant reimputation) */
    private EOTauxProrata tauxProrataOld;
    private boolean sourceHasChanged = false;
    private int nbDecimales = -1;

    private ESourceCreditType sourceTypeCredit;

    public EODepenseBudget() {
        super();
        arrayActions = null;
        arrayAnalytiques = null;
        arrayConventions = null;
        arrayCodeExers = null;
        arrayPlanComptables = null;
        arrayEmargements = null;
        solde = false;
        maxReversement = new BigDecimal(0.0);
    }

    private void setMaxReversement(BigDecimal value) {
        if (value == null)
            value = new BigDecimal(0.0);
        maxReversement = value;
    }

    public BigDecimal maxReversement() {
        if (maxReversement == null)
            setMaxReversement(new BigDecimal(0.0));
        return maxReversement;
    }

    public void evaluerMaxReversement() {
        if (depenseBudgetReversement() == null) {
            setMaxReversement(new BigDecimal(0.0));
            return;
        }

        if (depenseBudgetReversement().reversements() == null || depenseBudgetReversement().reversements().count() == 0) {
            setMaxReversement(depenseBudgetReversement().depTtcSaisie());
            return;
        }

        BigDecimal value = depenseBudgetReversement().depTtcSaisie();
        for (int i = 0; i < depenseBudgetReversement().reversements().count(); i++)
            value = value.add(((EODepenseBudget) depenseBudgetReversement().reversements().objectAtIndex(i)).depTtcSaisie());
        setMaxReversement(value);
    }

    public boolean isReversement() {
        if (depenseBudgetReversement() == null)
            return false;
        return true;
    }

    /**
     * @param value
     *            True si la depense budget doit solder son engagement
     *            correspondant lors de l'enregistrement.
     */
    public void setSolde(boolean value) {
        solde = value;
    }

    public boolean solde() {
        return solde;
    }

    public EOTauxProrata tauxProrata() {
        return super.tauxProrata();
    }

    public void setTauxProrata(EOTauxProrata taux) {
        if (taux == null)
            return;

        takeStoredValueForKey(taux, EODepenseBudget.TAUX_PRORATA_KEY);
        if (taux != null && depHtSaisie() != null && depTvaSaisie() != null) {
            super.setDepMontantBudgetaire(serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), taux.tapTaux()));
            corrigerMontant();
        }
    }

    public boolean isEmargementObligatoire() {
        return serviceDepense.isEmargementObligatoire(depensePapier());
    }

    public BigDecimal restantHtControleAction() {
        return depHtSaisie().subtract(computeSumForKey(depenseControleActions(), EODepenseControleAction.DACT_HT_SAISIE_KEY));
    }

    public BigDecimal restantTtcControleAction() {
        return depTtcSaisie().subtract(computeSumForKey(depenseControleActions(), EODepenseControleAction.DACT_TTC_SAISIE_KEY));
    }

    public BigDecimal restantHtControleAnalytique() {
        return new BigDecimal(0.0);
    }

    public BigDecimal restantTtcControleAnalytique() {
        return new BigDecimal(0.0);
    }

    public BigDecimal restantHtControleConvention() {
        return new BigDecimal(0.0);
    }

    public BigDecimal restantTtcControleConvention() {
        return new BigDecimal(0.0);
    }

    public BigDecimal restantHtControleHorsMarche() {
        return depHtSaisie().subtract(computeSumForKey(depenseControleHorsMarches(), EODepenseControleHorsMarche.DHOM_HT_SAISIE_KEY));
    }

    public BigDecimal restantTtcControleHorsMarche() {
        return depTtcSaisie().subtract(computeSumForKey(depenseControleHorsMarches(), EODepenseControleHorsMarche.DHOM_TTC_SAISIE_KEY));
    }

    public BigDecimal restantHtControleMarche() {
        return new BigDecimal(0.0);
    }

    public BigDecimal restantTtcControleMarche() {
        return new BigDecimal(0.0);
    }

    public BigDecimal restantHtControlePlanComptable() {
        return depHtSaisie().subtract(computeSumForKey(depenseControlePlanComptables(), EODepenseControlePlanComptable.DPCO_HT_SAISIE_KEY));
    }

    public BigDecimal restantTtcControlePlanComptable() {
        return depTtcSaisie().subtract(computeSumForKey(depenseControlePlanComptables(), EODepenseControlePlanComptable.DPCO_TTC_SAISIE_KEY));
    }

    public NSArray getEcrituresPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            if (arrayEmargements == null && depensePapier() != null && depensePapier().modePaiement() != null) {
                NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
                bindings.setObjectForKey(depensePapier().modePaiement(), "modePaiement");
                arrayEmargements = FinderEcriture.getEcritureDetails(ed, bindings);

                // TODO : restreindre par rapport au gescode de la source (900
                // ou UB de la source) dixit Fred
                // parametres pour ca ?
            }
            if (arrayEmargements == null)
                arrayEmargements = new NSArray();

            if (arrayEmargements.count() == 0)
                return arrayEmargements;

            // Vu qu'on associe une ecriture a tt l'engagement, si une ecriture
            // presente plus d'ecriture possible
            if (depenseControlePlanComptables() != null && depenseControlePlanComptables().count() > 0) {
                for (int i = 0; i < depenseControlePlanComptables().count(); i++) {
                    EOEcritureDetail uneEcriture = ((EODepenseControlePlanComptable) depenseControlePlanComptables().objectAtIndex(i))
                            .ecritureDetail();
                    if (uneEcriture != null)
                        return NSArray.emptyArray();
                }
            }
        } else {
            arrayEmargements = new NSArray();
        }

        return arrayEmargements;
    }

    public void resetTypeActionsPossibles() {
        arrayActions = null;
    }

    public NSArray<EOTypeAction> getTypeActionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            if (arrayActions == null && getSource() != null && utilisateur != null)
                arrayActions = FinderTypeAction.getTypeActions(ed, getSource(), utilisateur);
            if (arrayActions == null)
                arrayActions = NSArray.emptyArray();

            if (arrayActions.count() == 0)
                return arrayActions;
        } else {
            arrayActions = NSArray.emptyArray();
        }

        return arrayActions;
    }

    public void resetCodeAnalytiquesPossibles() {
        arrayAnalytiques = null;
    }

    public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            arrayAnalytiques = getSource().getCodeAnalytiquesPossibles(ed, utilisateur);
        } else {
            arrayAnalytiques = NSArray.emptyArray();
        }

        return arrayAnalytiques;
    }

    public void resetConventionsPossibles() {
        arrayConventions = null;
    }

    public NSArray<EOConvention> getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            arrayConventions = getSource().getConventionsPossibles(ed, utilisateur);
        } else {
            arrayConventions = NSArray.emptyArray();
        }

        return arrayConventions;
    }

    public void resetCodeExersPossibles() {
        arrayCodeExers = null;
    }

    public NSArray<EOCodeExer> getCodeExersPossibles(EOEditingContext ed, EOUtilisateur utilisateur, EOCommande commande) {
        if (depenseBudgetReversement() == null) {
            if (arrayCodeExers == null && commande != null) {
                arrayCodeExers = FinderCodeExer.getCodeExerPourCommande(ed, commande);
            }
            if (arrayCodeExers == null)
                arrayCodeExers = NSArray.emptyArray();

            if (arrayCodeExers.count() == 0)
                return arrayCodeExers;
        } else {
            arrayCodeExers = NSArray.emptyArray();
        }

        return arrayCodeExers;
    }

    public void resetPlanComptablesPossibles() {
        arrayPlanComptables = null;
    }

    public NSArray<EOPlanComptable> getPlanComptablesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            if (arrayPlanComptables == null && getSource() != null && getSource().typeCredit() != null && utilisateur != null) {
                NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
                bindings.setObjectForKey(getSource().typeCredit(), "typeCredit");
                bindings.setObjectForKey(utilisateur, "utilisateur");
                bindings.setObjectForKey(getSource().exercice(), "exercice");
                arrayPlanComptables = FinderPlanComptable.getPlanComptables(ed, bindings);
            }
            if (arrayPlanComptables == null)
                arrayPlanComptables = NSArray.emptyArray();

            if (arrayPlanComptables.count() == 0)
                return arrayPlanComptables;
        } else {
            arrayPlanComptables = NSArray.emptyArray();
        }

        return arrayPlanComptables;
    }

    public NSArray<EOTypeAction> getTypeActionsPossiblesForReimputation(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            if (arrayActions == null && getSource() != null && utilisateur != null)
                arrayActions = FinderTypeAction.getTypeActions(ed, getSource(), utilisateur);
            if (arrayActions == null)
                arrayActions = NSArray.emptyArray();

            if (arrayActions.count() == 0)
                return arrayActions;
        } else {
            // dans le cas des ORV on propose comme codes ceux affectés à la
            // dépense initiale
            arrayActions = NSArray.emptyArray();
            NSArray tmp = depenseControleActions();
            arrayActions = (NSArray) tmp.valueForKey(EODepenseControleAction.TYPE_ACTION_KEY);
        }

        return arrayActions;
    }

    public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossiblesForReimputation(EOEditingContext ed, EOUtilisateur utilisateur) {

        if (depenseBudgetReversement() == null) {
            if (arrayAnalytiques == null && getSource() != null && utilisateur != null)
                arrayAnalytiques = FinderCodeAnalytique.getCodeAnalytiques(ed, getSource().organ(), getSource().exercice());
            if (arrayAnalytiques == null)
                arrayAnalytiques = new NSArray();

            if (arrayAnalytiques.count() == 0)
                return arrayAnalytiques;
        } else {
            // dans le cas des ORV on propose comme codes ceux affectés à la
            // dépense initiale
            arrayAnalytiques = new NSArray();
            NSArray tmp = depenseControleAnalytiques();
            arrayAnalytiques = (NSArray) tmp.valueForKey(EODepenseControleAnalytique.CODE_ANALYTIQUE_KEY);
        }

        return arrayAnalytiques;
    }

    public NSArray<EOConvention> getConventionsPossiblesForReimputation(EOEditingContext ed, EOUtilisateur utilisateur) {
        if (depenseBudgetReversement() == null) {
            if ((arrayConventions == null || sourceHasChanged) && getSource() != null && utilisateur != null) {

                NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
                bindings.setObjectForKey(getSource().organ(), "organ");
                bindings.setObjectForKey(getSource().typeCredit(), "typeCredit");
                bindings.setObjectForKey(getSource().exercice(), "exercice");
                bindings.setObjectForKey(new Boolean(true), "montants");
                bindings.setObjectForKey(EOConventionNonLimitative.MODE_GESTION_RESSOURCE_AFFECTEE, "convModeGestion");

                NSTimestamp dateFinPaiementMax = this.depensePapier().dppDateServiceFait();
                bindings.setObjectForKey(dateFinPaiementMax, "conDateFinPaiementMax");
                arrayConventions = FinderConvention.getConventions(ed, bindings);
            }
            if (arrayConventions == null)
                arrayConventions = new NSArray();

            if (arrayConventions.count() == 0)
                return arrayConventions;

        } else {
            // dans le cas des ORV on propose comme codes ceux affectés à la
            // dépense initiale
            arrayConventions = new NSArray();
            NSArray tmp = depenseControleConventions();
            arrayConventions = (NSArray) tmp.valueForKey(EODepenseControleConvention.CONVENTION_KEY);
        }

        return arrayConventions;
    }

    public NSArray<EOCodeExer> getCodeExersPossiblesForReimputation(EOEditingContext ed, EOUtilisateur utilisateur, EOCommande commande) {
        if (depenseBudgetReversement() == null) {
            if (arrayCodeExers == null && /* engagementBudget() != null && */commande != null) {
                arrayCodeExers = FinderCodeExer.getCodeExerPourCommande(ed, commande);
            }
            if (arrayCodeExers == null)
                arrayCodeExers = new NSArray();

            if (arrayCodeExers.count() == 0)
                return arrayCodeExers;
        } else {
            // dans le cas des ORV on propose comme codes ceux affectés à la
            // dépense initiale
            arrayCodeExers = new NSArray();
            NSArray tmp = depenseControleHorsMarches();
            arrayCodeExers = (NSArray<EOCodeExer>) tmp.valueForKey(EODepenseControleHorsMarche.CODE_EXER_KEY);
        }

        return arrayCodeExers;
    }

    public NSArray<EOPlanComptable> getPlanComptablesPossiblesForReimputation(EOEditingContext ed, EOUtilisateur utilisateur) {
        // TODO a valider
        if (depenseBudgetReversement() == null) {
            if (arrayPlanComptables == null && getSource() != null && utilisateur != null) {

                NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
                bindings.setObjectForKey(getSource().typeCredit(), "typeCredit");
                bindings.setObjectForKey(utilisateur, "utilisateur");
                bindings.setObjectForKey(getSource().exercice(), "exercice");

                arrayPlanComptables = FinderPlanComptable.getPlanComptables(ed, bindings);
            }
            if (arrayPlanComptables == null)
                arrayPlanComptables = new NSArray();

            if (arrayPlanComptables.count() == 0)
                return arrayPlanComptables;
        } else {
            arrayPlanComptables = new NSArray();
        }

        return arrayPlanComptables;
    }

    public boolean isControleEcritureGood() {
        return true;
    }

    public boolean isControleActionGood() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ACTION).isControleGood(this);
    }

    public boolean isControleAnalytiqueGood() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ANALYTIQUE).isControleGood(this);
    }

    public boolean isControleConventionGood() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_CONVENTION).isControleGood(this);
    }

    public boolean isControleHorsMarcheGood() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_HORS_MARCHE).isControleGood(this);
    }

    public boolean isControleMarcheGood() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_MARCHE).isControleGood(this);
    }

    public boolean isControlePlanComptableGood() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_PLAN_COMPTABLE).isControleGood(this);
    }

    /**
     * Répartitions automatiques du crédit de la dépense
     */
    public void creerRepartitionAutomatique() {
        if (isSurExtourne()) {
            creerRepartitionAutomatiqueExtourne();
        } else {
            if (isDepenseBudgetReversement()) {
                creerRepartitionAutomatiqueBudgetReversement();
            } else {
                creerRepartitionAutomatiqueBudgetDepense();
            }

        }
    }

    private void creerRepartitionAutomatiqueBudgetDepense() {

        if (isEngagementEtDepenseARepartir()) {
            creerRepartitionAutomatiqueEngagementBudgetaire(engagementBudget(), engagementBudget().exercice(), false);
        }
    }

    private void creerRepartitionAutomatiqueBudgetReversement() {

        if (isDepenseARepartir()) {
            definirExerciceParDefaut();
            creerRepartitionAutomatiqueEngagementBudgetaire(this, exercice(), true);
        }
    }

    private void creerRepartitionAutomatiqueEngagementBudgetaire(ISourceRepartitionCredit source, EOExercice exercice, boolean plafondReversement) {

        creerRepartitionAutomatique(source, exercice, serviceDepense.getListeRepartitionsEngagementBudgetaire(), plafondReversement);
    }

    /**
     * Seul le cas mandat d'extourne est traité
     */
    private void creerRepartitionAutomatiqueExtourne() {

        if (isMandatExtourne() && depenseBudgetReversement() == null) {
            if (isEngagementEtDepenseARepartir()) {

                creerRepartitionAutomatique(getSourceRepartitionExtourne(), engagementBudget().exercice(),
                        serviceDepense.getListeRepartitionsMandatExtourne(), false);

            }
        }
    }

    private void creerRepartitionAutomatique(ISourceRepartitionCredit source, EOExercice exercice, List<String> repartitions,
            boolean plafondReversement) {

        for (String repartition : repartitions) {
            IRepartition service = serviceDepense.getGenerateurRepartition(repartition);
            service.genererDepensesControle(editingContext(), this, source, repartition, exercice, plafondReversement);
        }
    }

    private boolean isMandatExtourne() {
        return getSource() instanceof SourceCreditExtourneLiquidation;
    }

    private boolean isDepenseBudgetReversement() {
        return !(depenseBudgetReversement() == null);
    }

    private boolean isDepenseARepartir() {
        return !(depHtSaisie() == null || depTtcSaisie() == null);
    }

    private boolean isEngagementEtDepenseARepartir() {
        return !(engagementBudget() == null || depHtSaisie() == null || depTtcSaisie() == null);
    }

    private ISourceRepartitionCredit getSourceRepartitionExtourne() {
        ISourceRepartitionCredit source;
        source = new SourceRepartitionCreditExtourne((SourceCreditExtourneLiquidation) getSource());
        return source;
    }

    private void definirExerciceParDefaut() {
        if (exercice() == null && engagementBudget() != null)
            setExerciceRelationship(engagementBudget().exercice());
        if (exercice() == null && depensePapier() != null)
            setExerciceRelationship(depensePapier().exercice());
    }

    public NSArray repartitionPourcentageActions() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ACTION).repartitionPourcentageAvecDernier(this, maxReversement());
    }

    public NSArray repartitionPourcentageAnalytiques() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ANALYTIQUE).repartitionPourcentage(this, maxReversement());
    }

    public NSArray repartitionPourcentageConventions() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_CONVENTION).repartitionPourcentage(this, maxReversement());
    }

    public NSArray repartitionPourcentagePlanComptables() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_PLAN_COMPTABLE).repartitionPourcentageAvecDernier(this,
                maxReversement());
    }

    public NSArray repartitionPourcentageMarches() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_MARCHE).repartitionPourcentageAvecDernier(this, maxReversement());
    }

    public NSArray repartitionPourcentageHorsMarches() {
        return serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_HORS_MARCHE).repartitionPourcentageAvecDernier(this,
                maxReversement());
    }

    public boolean isReversable() {
        if (depenseControlePlanComptables().count() == 0)
            return false;
        if (depenseBudgetReversement() != null)
            return false;

        for (int i = 0; i < depenseControlePlanComptables().count(); i++) {
            EODepenseControlePlanComptable depenseControlePlanComptable = (EODepenseControlePlanComptable) depenseControlePlanComptables()
                    .objectAtIndex(i);

            if (depenseControlePlanComptable.dpcoMontantBudgetaire().floatValue() < 0.0)
                return false;
            if (depenseControlePlanComptable.mandat() == null)
                return false;
            if (!depenseControlePlanComptable.mandat().isVise())
                return false;
        }

        return true;
    }

    public boolean isSupprimable() {
        if (depenseControlePlanComptables().count() == 0)
            return true;

        for (int i = 0; i < depenseControlePlanComptables().count(); i++) {
            EODepenseControlePlanComptable depenseControlePlanComptable = (EODepenseControlePlanComptable) depenseControlePlanComptables()
                    .objectAtIndex(i);

            if (depenseControlePlanComptable.mandat() != null)
                return false;
        }

        return true;
    }

    protected void corrigerMontant() {
        // correction des controleurs
        corrigerMontantActions();
        corrigerMontantAnalytiques();
        corrigerMontantConventions();
        corrigerMontantHorsMarches();
        corrigerMontantMarches();
        corrigerMontantPlanComptables();
    }

    public void corrigerMontantActions() {
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ACTION).corrigerMontantsComplexe(this);
    }

    public void corrigerMontantAnalytiques() {

        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ANALYTIQUE).corrigerMontantsComplexe(this);
    }

    public void corrigerMontantConventions() {
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_CONVENTION).corrigerMontantsComplexe(this);
    }

    public void corrigerMontantHorsMarches() {
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_HORS_MARCHE).corrigerMontantsComplexe(this);
    }

    public void corrigerMontantMarches() {
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_MARCHE).corrigerMontants(this);
    }

    public void corrigerMontantPlanComptables() {
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_PLAN_COMPTABLE).corrigerMontants(this);
    }

    private BigDecimal fractionHtTtcEngagement() {
        if (engagementBudget() == null || engagementBudget().engHtSaisie() == null || engagementBudget().engTtcSaisie() == null)
            return new BigDecimal(1.0);
        if (engagementBudget().engHtSaisie().floatValue() == 0 || engagementBudget().engTtcSaisie().floatValue() == 0)
            return new BigDecimal(1.0);

        return engagementBudget().engTtcSaisie().divide(engagementBudget().engHtSaisie(), 3, BigDecimal.ROUND_HALF_UP);
    }

    public int decimalesPourArrondirMontant() {
        if (nbDecimales >= 0)
            return nbDecimales;

        if (engagementBudget() != null) {
            nbDecimales = engagementBudget().decimalesPourArrondirMontant();
            return nbDecimales;
        }

        if (exercice() == null) {
            if (depensePapier() != null && depensePapier().exercice() != null)
                setExerciceRelationship(depensePapier().exercice());
        }

        if (exercice() == null) {
            nbDecimales = EODevise.defaultNbDecimales;
            return nbDecimales;
        }

        EODevise devise = FinderDevise.getDeviseEnCours(editingContext(), exercice());
        if (devise == null)
            nbDecimales = EODevise.defaultNbDecimales;
        else
            nbDecimales = devise.devNbDecimales().intValue();

        return nbDecimales;
    }

    public void setDepHtSaisie(BigDecimal aValue) {
        if (depenseBudgetReversement() == null) {

            aValue = normaliseValuePositive(aValue);
            super.setDepHtSaisie(aValue);

            if (engagementBudget() != null) {
                // on calcule le ttc en fonction du ht-ttc de l'engagement
                setDepTtcSaisie(depHtSaisie().multiply(fractionHtTtcEngagement()));
                return;
            }

            if (depTtcSaisie() == null || depTtcSaisie().floatValue() < depHtSaisie().floatValue()) {
                setDepTtcSaisie(depHtSaisie());
                return;
            }

            setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));
            if (tauxProrata() != null)
                setDepMontantBudgetaire(serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), tauxProrata().tapTaux()));

        } else {

            aValue = normaliseValueNegative(aValue);
            super.setDepHtSaisie(aValue);

        }
        corrigerMontant();
    }

    private BigDecimal normaliseValueNegative(BigDecimal aValue) {
        if (aValue == null || aValue.floatValue() > 0)
            aValue = new BigDecimal(0.0);
        int arrondi = decimalesPourArrondirMontant();
        aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        return aValue;
    }

    private BigDecimal normaliseValuePositive(BigDecimal aValue) {
        if (aValue == null || aValue.floatValue() < 0)
            aValue = new BigDecimal(0.0);
        int arrondi = decimalesPourArrondirMontant();
        aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        return aValue;
    }

    public void setDepTvaSaisie(BigDecimal aValue) {
        if (aValue == null || (depenseBudgetReversement() == null && aValue.floatValue() < 0)
                || (depenseBudgetReversement() != null && aValue.floatValue() > 0))
            aValue = new BigDecimal(0.0);
        int arrondi = decimalesPourArrondirMontant();
        aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setDepTvaSaisie(aValue);
    }

    public void setDepTtcSaisie(BigDecimal aValue) {
        if (depenseBudgetReversement() == null) {
            aValue = normaliseValuePositive(aValue);
            super.setDepTtcSaisie(aValue);

            if (depHtSaisie() == null || depHtSaisie().floatValue() > depTtcSaisie().floatValue())
                setDepHtSaisie(depTtcSaisie().divide(fractionHtTtcEngagement(), decimalesPourArrondirMontant(), BigDecimal.ROUND_UP));
        } else {
            aValue = normaliseValueNegative(aValue);
            super.setDepTtcSaisie(aValue);

            if (depHtSaisie() == null || depHtSaisie().floatValue() < depTtcSaisie().floatValue())
                super.setDepTtcSaisie(depHtSaisie());
        }

        setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));

        if (tauxProrata() != null)
            setDepMontantBudgetaire(serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), tauxProrata().tapTaux()));

        corrigerMontant();
    }

    public void setDepTtcSaisieSansCalcul(BigDecimal aValue) {
        aValue = standardiseMontantSaisi(aValue);
        super.setDepTtcSaisie(aValue);

        rafraichirMontantTvaEtMontantBudgetaire();
    }

    public void setDepHtSaisieSansCalcul(BigDecimal aValue) {
        aValue = standardiseMontantSaisi(aValue);
        super.setDepHtSaisie(aValue);

        rafraichirMontantTvaEtMontantBudgetaire();
    }

    private BigDecimal standardiseMontantSaisi(BigDecimal aValue) {
        if (aValue == null)
            aValue = Calculs.ZERO;
        int arrondi = decimalesPourArrondirMontant();
        aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        return aValue;
    }

    private void rafraichirMontantTvaEtMontantBudgetaire() {
        if (depHtSaisie() != null && depTtcSaisie() != null)
            super.setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));

        if (tauxProrata() != null && depHtSaisie() != null && depTvaSaisie() != null)
            super.setDepMontantBudgetaire(serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), tauxProrata().tapTaux()));

        corrigerMontant();
    }

    public void setDepMontantBudgetaire(BigDecimal aValue) {
        if (aValue == null || (depenseBudgetReversement() == null && aValue.floatValue() < 0)
                || (depenseBudgetReversement() != null && aValue.floatValue() > 0))
            aValue = new BigDecimal(0.0);
        int arrondi = decimalesPourArrondirMontant();
        aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setDepMontantBudgetaire(aValue);
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * 
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {

        if (depHtSaisie() == null)
            throw new DepenseBudgetException(DepenseBudgetException.depHtSaisieManquant);
        if (depTvaSaisie() == null)
            throw new DepenseBudgetException(DepenseBudgetException.depTvaSaisieManquant);
        if (depTtcSaisie() == null)
            throw new DepenseBudgetException(DepenseBudgetException.depTtcSaisieManquant);
        if (depMontantBudgetaire() == null)
            throw new DepenseBudgetException(DepenseBudgetException.depMontantBudgetaireManquant);

        if (depensePapier() == null)
            throw new DepenseBudgetException(DepenseBudgetException.depensePapierManquant);
        if (exercice() == null)
            throw new DepenseBudgetException(DepenseBudgetException.exerciceManquant);
        // if (engagementBudget() == null)
        // throw new
        // DepenseBudgetException(DepenseBudgetException.engagementBudgetManquant);
        if (tauxProrata() == null)
            throw new DepenseBudgetException(DepenseBudgetException.tauxProrataManquant);
        if (utilisateur() == null)
            throw new DepenseBudgetException(DepenseBudgetException.utilisateurManquant);

        if (!depHtSaisie().abs().add(depTvaSaisie().abs()).equals(depTtcSaisie().abs()))
            setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));
        // if
        // (!depHtSaisie().abs().add(depTvaSaisie().abs()).equals(depTtcSaisie().abs()))
        // throw new
        // DepenseBudgetException(DepenseBudgetException.depTtcSaisiePasCoherent);
        if (depenseBudgetReversement() == null && depTvaSaisie().floatValue() < 0.0)
            throw new DepenseBudgetException(DepenseBudgetException.depMontantNegatif);

        if (!depMontantBudgetaire().equals(calculMontantBudgetaire()))
            setDepMontantBudgetaire(calculMontantBudgetaire());

        if (depenseBudgetReversement() == null && depTtcSaisie().floatValue() < 0.0)
            throw new DepenseBudgetException(DepenseBudgetException.depenseBudgetReversementManquant);
        if (depenseBudgetReversement() != null && engagementBudget() != null
                && !engagementBudget().equals(depenseBudgetReversement().engagementBudget()))
            throw new DepenseBudgetException(DepenseBudgetException.engagementBudgetPasCoherent);
        if (depenseBudgetReversement() != null && !tauxProrata().equals(depenseBudgetReversement().tauxProrata()))
            throw new DepenseBudgetException(DepenseBudgetException.tauxProrataReversementPasCoherent);

        if (engagementBudget() != null && !engagementBudget().exercice().equals(exercice()))
            throw new DepenseBudgetException(DepenseBudgetException.engagementBudgetExercicePasCoherent);
        if (!depensePapier().exercice().equals(exercice()))
            throw new DepenseBudgetException(DepenseBudgetException.depensePapierExercicePasCoherent);

    }

    private BigDecimal calculMontantBudgetaire() {
        return serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), tauxProrata().tapTaux());
    }

    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        if (depenseControlePlanComptables() == null || depenseControlePlanComptables().count() != 1)
            throw new DepenseBudgetException(DepenseBudgetException.imputationsIncoherentes);

        if (engagementBudget() != null && !engagementBudget().fournisseur().equals(depensePapier().fournisseur()))
            throw new DepenseBudgetException(DepenseBudgetException.fournisseurIncoherent);

        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ANALYTIQUE).valider(this);
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_ACTION).valider(this);
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_CONVENTION).valider(this);
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_PLAN_COMPTABLE).valider(this);
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_MARCHE).valider(this);
        serviceDepense.getGenerateurRepartition(DepenseService.REPARTITION_HORS_MARCHE).valider(this);

    }

    @Deprecated
    protected BigDecimal computeSumForKey(NSArray eo, String key) {
        return serviceDepense.computeSumForKey(eo, key);
    }

    public Boolean isReimputable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {
        if (ed == null || utilisateur == null || exercice() == null || engagementBudget() == null)
            return Boolean.FALSE;

        if (!FinderExercice.getExercicesReimputablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
            return Boolean.FALSE;

        // si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires
        // presentes alors non
        if (organs == null)
            organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

        if (!engagementBudget().isConsultable(ed, utilisateur, organs))
            return Boolean.FALSE;

        // l'utilisateur a les droits necessaires alors oui
        return Boolean.TRUE;
    }

    public Boolean isReimputableForActions() {
        // TODO isReimputableForActions
        return Boolean.TRUE;
    }

    public Boolean isReimputableForAnalytiques() {
        // TODO isReimputableForAnalytiques
        return Boolean.TRUE;
    }

    public Boolean isReimputableForConventions() {
        // TODO isReimputableForConventions
        return Boolean.TRUE;
    }

    public Boolean isReimputableForHorsMarches() {
        // TODO isReimputableForHorsMarches
        return Boolean.TRUE;
    }

    public Boolean isReimputableForPlanComptable() {
        // possible si la dépense n'est pas mandatée
        // Seul un changement est possible, pas de répartition.
        //
        // problèmes éventuels liés à un inventaire ???
        return Boolean.valueOf(!isMandate().booleanValue());
    }

    public Boolean isReimputableForSource() {
        // TODO isReimputableForSource
        return Boolean.TRUE;
    }

    public Boolean isReimputableForTauxProrata() {
        // seulement dans le cas ou la dépense n'est pas encore mandatée, car
        // cela entraînerait des modifications d'écritures comptables
        return Boolean.valueOf(!isMandate().booleanValue());
    }

    public Boolean isReimputableForLigneBudgetaire() {
        // * dépense sans inventaire : si la dépense est mandatée, on choisit
        // une ligne budgétaire de la même UB, sinon toutes les lignes, dans la
        // limite que le disponible le permette.
        // * dépense avec inventaire : suivant un paramètre a mettre en place,
        // même limitation que la dépense sans inventaire ou restriction au
        // niveau du CR (puisque le code inventaire prend en compte le CR)
        // * le changement de ligne peut impliquer la modification des
        // conventions et des codes analytiques éventuellement associés à cette
        // dépense cf. changement convention
        // * le changement de ligne peut impliquer une création d'un nouvel
        // engagement, si l'engagement concerne d'autres liquidations (hors ORVs
        // liés a celle qu'on ré-impute) par exemple
        // * si la dépense ré-imputée possède un ou plusieurs ORVs on modifie
        // aussi l'ORV et inversement ..

        // TODO isReimputableForTauxProrata
        return Boolean.TRUE;
    }

    public Boolean isReimputableForTypeCredit() {
        // TODO isReimputableForTauxProrata

        // ré-imputation possible par un type de crédit de la même section que
        // le précédent, sous réserve que le disponible budgétaire soit
        // suffisant et que l'imputation comptable utilisée par cette dépense
        // soit affectée au type de crédit dans Maracuja.

        return Boolean.TRUE;
    }

    /**
     * @return true si le depenseBudget est associe a un mandat
     */
    public Boolean isMandate() {
        return Boolean.valueOf(depenseControlePlanComptables() != null && depenseControlePlanComptables().count() > 0
                && ((EODepenseControlePlanComptable) depenseControlePlanComptables().objectAtIndex(0)).mandat() != null);

    }

    /**
     * Renvoie la source de credit. Si le champ source est nul, on l'initialise
     * avec celui de l'engagement. Utile pour la reimputation.
     * 
     * @return la source de credit.
     */
    public _ISourceCredit getSource() {
        if (source == null) {
            if (isSurExtourne() && engagementBudget() != null && engagementBudget().organ() != null) {
                if (engagementBudget().organ().isUB()) {
                    source = EOExtourneCreditsUb.fetchByQualifier(
                            editingContext(),
                            ERXQ.and(ERXQ.equals(_IDepenseExtourneCredits.TO_ORGAN_KEY, engagementBudget().organ()),
                                    ERXQ.equals(_IDepenseExtourneCredits.TO_EXERCICE_KEY, engagementBudget().exercice()),
                                    ERXQ.equals(_IDepenseExtourneCredits.TO_TYPE_CREDIT_KEY, engagementBudget().typeCredit())));
                } else {
                    source = EOExtourneCreditsCr.fetchByQualifier(
                            editingContext(),
                            ERXQ.and(ERXQ.equals(_IDepenseExtourneCredits.TO_ORGAN_KEY, engagementBudget().organ()),
                                    ERXQ.equals(_IDepenseExtourneCredits.TO_EXERCICE_KEY, engagementBudget().exercice()),
                                    ERXQ.equals(_IDepenseExtourneCredits.TO_TYPE_CREDIT_KEY, engagementBudget().typeCredit())));
                }

            }
            if (source == null && engagementBudget() != null) {
                source = engagementBudget().source();
            }
        }
        return source;
    }

    /**
     * Permet de specifier une nouvelle source dans le cas d'une reimputation.
     * 
     * @param source
     */
    public void setSource(_ISourceCredit source) {
        this.source = source;
        this.sourceHasChanged = true;

    }

    public EOTauxProrata tauxProrataOld() {
        return tauxProrataOld;
    }

    public void setTauxProrataOld(EOTauxProrata oldTauxProrata) {
        this.tauxProrataOld = oldTauxProrata;
    }

    /**
     * Prepare l'objet pour qu'il puisse etre reimpute.
     * <ul>
     * <li>Memorisation du taux de prorata initial</li>
     * <li>Initialisation des inventaires a partir des inventaires liquides</li>
     * </ul>
     */
    public void prepareForReimputation() {
        setTauxProrataOld(tauxProrata());
        for (int i = 0; i < depenseControlePlanComptables().count(); i++) {
            EODepenseControlePlanComptable dpco = ((EODepenseControlePlanComptable) depenseControlePlanComptables().objectAtIndex(i));
            dpco.ajouterInventairesReimputation(dpco.inventairesLiquides());
        }
    }

    /**
     * @return True si l'objet a été modifié (en ignorant les modifications sur
     *         les clés de IGNORE_CHANGES_IN_KEYS)
     */
    public boolean objectHasChangedForReimputation() {
        if (isNewObject()) {
            return true;
        }
        if (sourceHasChanged) {
            return true;
        }
        NSArray<String> ignoreChangesInKeys = new NSArray<String>(new String[] { UTILISATEUR_KEY });
        NSArray committedKeys = changesFromCommittedSnapshot().allKeys();
        return !ERXArrayUtilities.arrayContainsArray(ignoreChangesInKeys, committedKeys);
    }

    /**
     * @param edc
     * @param qualifier
     * @return Les liquidations d'extourne en fonction des filtres passés
     */
    public static NSArray<EODepenseBudget> fetchLiquidationsExtourne(EOEditingContext edc, EOQualifier qualifier) {
        NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();

        quals.addObject(ERXQ.isNotNull(TO_EXTOURNE_LIQ_N1S_KEY + "." + EOExtourneLiq.DEP_ID_N1_KEY));
        if (qualifier != null) {
            quals.addObject(qualifier);
        }
        return fetchAll(edc, new EOAndQualifier(quals), new NSArray(new Object[] { ERXS.asc(DEP_ID_KEY) }));

    }

    public String pconumAsListe() {
        NSArray pcos = (NSArray) depenseControlePlanComptables().valueForKey(
                EODepenseControlePlanComptable._PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY);
        return pcos.componentsJoinedByString(",");
    }

    public void updateFromSource() {
        // on ne fait rien avec organ et typecredit
        setTauxProrata(getSource().tauxProrata());
    }

    public ESourceCreditType getSourceTypeCredit() {
        return sourceTypeCredit;
    }

    public void setSourceTypeCredit(ESourceCreditType sourceTypeCredit) {
        this.sourceTypeCredit = sourceTypeCredit;
    }

    public Boolean isExtourne() {
        return toExtourneLiqN1s() != null && toExtourneLiqN1s().count() > 0;
    }

    public Boolean isSurExtourne() {
        if (getSourceTypeCredit() == null) {
            initSourceTypeCredit();
        }
        return ESourceCreditType.EXTOURNE.equals(getSourceTypeCredit());
    }

    private void initSourceTypeCredit() {
        if (toExtourneLiqDefs().count() > 0) {
            setSourceTypeCredit(ESourceCreditType.EXTOURNE);
        } else {
            setSourceTypeCredit(ESourceCreditType.BUDGET);
        }
    }

    /**
     * @return Le montant Budgétaire disponible si cet objet est une liquidation
     *         d'extourne
     */
    public BigDecimal resteDisponibleBudgetairePourExtourne() {
        if (isExtourne()) {
            BigDecimal ht = engagementBudget().engHtSaisie();
            BigDecimal ttc = engagementBudget().engTtcSaisie();
            return serviceCalculs.calculMontantBudgetaire(ht, ttc.subtract(ht), tauxProrata().tapTaux()).abs();
        }
        return null;
    }

    /**
     * Verifier l'etat de cette depense budget. Si la depense n'est rattaché a
     * aucun plan comptable ou que sa valeur est égale à 0 alors on peut ignorer
     * cette depense lors des traitements métier.
     * 
     * @return true si cette depense ne doit pas être prise en compte lors des
     *         futurs traitements ; false sinon.
     */
    public boolean estIgnoree() {
        return !hasPlansComptables() || !hasMontantsDefinis();

    }

    /**
     * @return true si cette depense est rattachee a des plans comptables ;
     *         false sinon.
     */
    public boolean hasPlansComptables() {
        return this.depenseControlePlanComptables() != null && this.depenseControlePlanComptables().count() > 0;
    }

    /**
     * @return true si cette depense a un montant HT ou un montant TTC defini ;
     *         false sinon.
     */
    public boolean hasMontantsDefinis() {
        return (this.depHtSaisie() != null && this.depHtSaisie().floatValue() > 0.0f)
                || (this.depTtcSaisie() != null && this.depTtcSaisie().floatValue() > 0.0f);
    }

}
