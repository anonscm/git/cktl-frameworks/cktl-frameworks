/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOConventionNonLimitative.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOConventionNonLimitative extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleConventionNonLimitative";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CONVENTION_NON_LIMITATIVE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> CON_DATE_CLOTURE = new ERXKey<NSTimestamp>("conDateCloture");
	public static final ERXKey<NSTimestamp> CON_DATE_FIN_PAIEMENT = new ERXKey<NSTimestamp>("conDateFinPaiement");
	public static final ERXKey<java.math.BigDecimal> CONV_DISPO = new ERXKey<java.math.BigDecimal>("convDispo");
	public static final ERXKey<String> CONV_MODE_GESTION = new ERXKey<String>("convModeGestion");
	public static final ERXKey<java.math.BigDecimal> CONV_MONTANT = new ERXKey<java.math.BigDecimal>("convMontant");
	public static final ERXKey<String> CONV_REFERENCE = new ERXKey<String>("convReference");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention> CONVENTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention>("convention");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organ");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("typeCredit");

	// Attributes


	public static final String CON_DATE_CLOTURE_KEY = "conDateCloture";
	public static final String CON_DATE_FIN_PAIEMENT_KEY = "conDateFinPaiement";
	public static final String CONV_DISPO_KEY = "convDispo";
	public static final String CONV_MODE_GESTION_KEY = "convModeGestion";
	public static final String CONV_MONTANT_KEY = "convMontant";
	public static final String CONV_REFERENCE_KEY = "convReference";

//Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CON_DATE_CLOTURE_COLKEY = "CON_DATE_CLOTURE";
	public static final String CON_DATE_FIN_PAIEMENT_COLKEY = "CON_DATE_FIN_PAIEMENT";
	public static final String CONV_DISPO_COLKEY = "CONV_DISPO";
	public static final String CONV_MODE_GESTION_COLKEY = "CONV_MODE_GESTION";
	public static final String CONV_MONTANT_COLKEY = "CONV_MONTANT";
	public static final String CONV_REFERENCE_COLKEY = "CONV_REFERENCE";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
	public NSTimestamp conDateCloture() {
	 return (NSTimestamp) storedValueForKey(CON_DATE_CLOTURE_KEY);
	}

	public void setConDateCloture(NSTimestamp value) {
	 takeStoredValueForKey(value, CON_DATE_CLOTURE_KEY);
	}

	public NSTimestamp conDateFinPaiement() {
	 return (NSTimestamp) storedValueForKey(CON_DATE_FIN_PAIEMENT_KEY);
	}

	public void setConDateFinPaiement(NSTimestamp value) {
	 takeStoredValueForKey(value, CON_DATE_FIN_PAIEMENT_KEY);
	}

	public java.math.BigDecimal convDispo() {
	 return (java.math.BigDecimal) storedValueForKey(CONV_DISPO_KEY);
	}

	public void setConvDispo(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CONV_DISPO_KEY);
	}

	public String convModeGestion() {
	 return (String) storedValueForKey(CONV_MODE_GESTION_KEY);
	}

	public void setConvModeGestion(String value) {
	 takeStoredValueForKey(value, CONV_MODE_GESTION_KEY);
	}

	public java.math.BigDecimal convMontant() {
	 return (java.math.BigDecimal) storedValueForKey(CONV_MONTANT_KEY);
	}

	public void setConvMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CONV_MONTANT_KEY);
	}

	public String convReference() {
	 return (String) storedValueForKey(CONV_REFERENCE_KEY);
	}

	public void setConvReference(String value) {
	 takeStoredValueForKey(value, CONV_REFERENCE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOConvention convention() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
	}

	public void setConventionRelationship(org.cocktail.fwkcktldepense.server.metier.EOConvention value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOConvention oldValue = convention();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organ() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
	}

	public void setOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organ();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
	}

	public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = typeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
	 }
	}


	/**
	* Créer une instance de EOConventionNonLimitative avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOConventionNonLimitative createEOConventionNonLimitative(EOEditingContext editingContext								, java.math.BigDecimal convDispo
							, String convModeGestion
							, java.math.BigDecimal convMontant
							, String convReference
					, org.cocktail.fwkcktldepense.server.metier.EOConvention convention				, org.cocktail.fwkcktldepense.server.metier.EOOrgan organ		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCredit					) {
	 EOConventionNonLimitative eo = (EOConventionNonLimitative) EOUtilities.createAndInsertInstance(editingContext, _EOConventionNonLimitative.ENTITY_NAME);	 
											eo.setConvDispo(convDispo);
									eo.setConvModeGestion(convModeGestion);
									eo.setConvMontant(convMontant);
									eo.setConvReference(convReference);
						 eo.setConventionRelationship(convention);
						 eo.setOrganRelationship(organ);
				 eo.setTypeCreditRelationship(typeCredit);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOConventionNonLimitative creerInstance(EOEditingContext editingContext) {
		EOConventionNonLimitative object = (EOConventionNonLimitative)EOUtilities.createAndInsertInstance(editingContext, _EOConventionNonLimitative.ENTITY_NAME);
  		return object;
		}

	

  public EOConventionNonLimitative localInstanceIn(EOEditingContext editingContext) {
    EOConventionNonLimitative localInstance = (EOConventionNonLimitative)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOConventionNonLimitative fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOConventionNonLimitative fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOConventionNonLimitative> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConventionNonLimitative eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConventionNonLimitative)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOConventionNonLimitative fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOConventionNonLimitative fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOConventionNonLimitative> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConventionNonLimitative eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConventionNonLimitative)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOConventionNonLimitative fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConventionNonLimitative eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConventionNonLimitative ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOConventionNonLimitative fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> objectsForRecherche(EOEditingContext ec,
														NSTimestamp conDateClotureMaxBinding,
														NSTimestamp conDateFinPaiementMaxBinding,
														org.cocktail.fwkcktldepense.server.metier.EOConvention conventionBinding,
														String convModeGestionBinding,
														String convReferenceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan organBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCreditBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOConventionNonLimitative.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (conDateClotureMaxBinding != null)
			bindings.takeValueForKey(conDateClotureMaxBinding, "conDateClotureMax");
		  if (conDateFinPaiementMaxBinding != null)
			bindings.takeValueForKey(conDateFinPaiementMaxBinding, "conDateFinPaiementMax");
		  if (conventionBinding != null)
			bindings.takeValueForKey(conventionBinding, "convention");
		  if (convModeGestionBinding != null)
			bindings.takeValueForKey(convModeGestionBinding, "convModeGestion");
		  if (convReferenceBinding != null)
			bindings.takeValueForKey(convReferenceBinding, "convReference");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (organBinding != null)
			bindings.takeValueForKey(organBinding, "organ");
		  if (typeCreditBinding != null)
			bindings.takeValueForKey(typeCreditBinding, "typeCredit");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> objectsForRechercheRA(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOConvention conventionBinding,
														String convReferenceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan organBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeCredit typeCreditBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("RechercheRA", EOConventionNonLimitative.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (conventionBinding != null)
			bindings.takeValueForKey(conventionBinding, "convention");
		  if (convReferenceBinding != null)
			bindings.takeValueForKey(convReferenceBinding, "convReference");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (organBinding != null)
			bindings.takeValueForKey(organBinding, "organ");
		  if (typeCreditBinding != null)
			bindings.takeValueForKey(typeCreditBinding, "typeCredit");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
