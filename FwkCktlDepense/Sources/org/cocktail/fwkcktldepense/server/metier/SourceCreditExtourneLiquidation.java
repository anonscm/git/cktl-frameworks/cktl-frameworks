package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Représente une source de crédit construite à partir d'une liquidation d'extourne.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class SourceCreditExtourneLiquidation implements _ISourceCredit {
	
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	private EODepenseBudget dbExtourne;
	private EODepenseBudget dbPrec;
	private EOTauxProrata prorata;
	private NSArray<EOTauxProrata> prorataDispos;
	private NSArray<EOConvention> arrayConventions;
	private NSArray<EOCodeAnalytique> arrayAnalytiques;

	/**
	 * @param db Il doit s'agir d'une liquidation d'extourne.
	 */
	public SourceCreditExtourneLiquidation(EODepenseBudget db) {
		this.dbExtourne = db;

		if (db.toExtourneLiqN1s() == null || db.toExtourneLiqN1s().count() == 0) {
			throw new RuntimeException("La depenseBudget devrait être une liquidation d'extourne, or la relation toExtourneLiqN1s n'est pas renseignée.");
		}
		EODepenseBudget dbExPrec = db.toExtourneLiqN1s().objectAtIndex(0).toDepenseBudgetN();
		if (dbExPrec != null) {
			prorataDispos = new NSArray<EOTauxProrata>(db.toExtourneLiqN1s().objectAtIndex(0).toDepenseBudgetN().tauxProrata());
			setTauxProrata(db.toExtourneLiqN1s().objectAtIndex(0).toDepenseBudgetN().tauxProrata());
		}
		else {
			prorataDispos = organ().tauxProratasPourExerciceTries(exercice().exercicePrecedent());
		}
	}

	public EOOrgan organ() {
		return dbExtourne.engagementBudget().organ();
	}

	public EOTypeCredit typeCredit() {
		return dbExtourne.engagementBudget().typeCredit();
	}

	public EOExercice exercice() {
		return dbExtourne.engagementBudget().exercice();
	}

	public ESourceCreditType type() {
		return ESourceCreditType.EXTOURNE;
	}

	public String libelle() {
		String libelle = "(extourne) " + libelleOrgan();
		if (typeCredit() != null) {
			libelle += " - " + typeCredit().tcdCode();
		}

		if (tauxProrata() != null) {
			libelle = libelle + " - " + tauxProrata().tapTaux();
		}

		return libelle;
	}

	public String libelleCourt() {
		String libelleCourt = libelle();
		if (libelleCourt != null && libelleCourt.length() > 50) {
			libelleCourt = libelleCourt.substring(0, 50);
		}
		return libelleCourt;
	}

    public String codeOrganEtTypeCredit() {
        String code = "(extourne) " + libelleOrgan();
        if (typeCredit() != null) {
            code += " - " + typeCredit().tcdCode();
        }

        return code;
    }
	
	public String libelleOrgan() {
		String libelle = "";
		if (organ() == null) {
			return libelle;
		}

		libelle = organ().sourceLibelle();
		return libelle;
	}

	public EOTauxProrata tauxProrata() {
		return prorata;
	}

	public void setTauxProrata(EOTauxProrata tauxProrata) {
		prorata = tauxProrata;
	}

	public NSArray<EOTauxProrata> tauxProrataDisponibles() {
		return prorataDispos;
	}

	public Boolean isComplet() {
		return exercice() != null && organ() != null && typeCredit() != null && tauxProrata() != null;
	}

	/**
	 * @return Le reste à engager budgétaire recalculé en fonction du prorata.
	 */
	public BigDecimal getDisponible() {
		if (tauxProrata() == null) {
			return null;
		}
		BigDecimal montantTtcReste = getDbExtourne().engagementBudget().engTtcSaisie();
		BigDecimal montantHtReste = getDbExtourne().engagementBudget().engHtSaisie();
		BigDecimal montantTvaReste = montantTtcReste.subtract(montantHtReste);
		return serviceCalculs.calculMontantBudgetaire(montantHtReste, montantTvaReste, tauxProrata().tapTaux()).abs();
	}

	/**
	 * @return Celle de l'exercice précédent
	 */
	public NSArray<EOConvention> getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null) {
			arrayConventions = (NSArray<EOConvention>) (getDbExtourne().depenseControleConventions().count() > 0 ? getDbExtourne().depenseControleConventions().valueForKey(EODepenseControleConvention.CONVENTION_KEY) : NSArray.emptyArray());
		}
		return arrayConventions;
	}

	/**
	 * @return Ceux de l'exercice précédent
	 */
	public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques == null) {
			//FIXME  faut-il pas mieux proposer la liste des codes analytiques possibles de la liqudation d'extourne précédente ? On attend une demande d'évolution éventuelle
			arrayAnalytiques = (NSArray<EOCodeAnalytique>) (getDbExtourne().depenseControleAnalytiques().count() > 0 ? getDbExtourne().depenseControleAnalytiques().valueForKey(EODepenseControleAnalytique.CODE_ANALYTIQUE_KEY) : NSArray.emptyArray());
		}
		return arrayAnalytiques;
	}

	/**
	 * @return La liquidation initiale sur N-1 si elle existe.
	 */
	public EODepenseBudget getDbPrec() {
		return dbPrec;
	}

	/**
	 * @return la liquidation d'extourne (sur N+1)
	 */
	public EODepenseBudget getDbExtourne() {
		return dbExtourne;
	}

}
