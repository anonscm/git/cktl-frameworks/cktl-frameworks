/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOExtourneCreditsCr extends _EOExtourneCreditsCr implements _IDepenseExtourneCredits {

	private static final long serialVersionUID = 1L;

	private EOTauxProrata tauxProrata;
	private NSArray<EOConvention> arrayConventions;
	private NSArray<EOCodeAnalytique> arrayAnalytiques;

	public EOExtourneCreditsCr() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public EOOrgan organ() {
		return toOrgan();
	}

	/**
	 * C'est celui de l'exercice n (précédent)
	 */
	public EOTypeCredit typeCredit() {
		return toTypeCredit();
	}

	/**
	 * Exercice n+1
	 */
	public EOExercice exercice() {
		return toExercice();
	}

	public ESourceCreditType type() {
		return ESourceCreditType.EXTOURNE;
	}

	public String libelle() {
		String libelle = "(extourne) " + libelleOrgan();
		if (typeCredit() != null) {
			libelle += " - " + typeCredit().tcdCode();
		}

		if (tauxProrata() != null) {
			libelle = libelle + " - " + tauxProrata().tapTaux();
		}

		return libelle;
	}

	public String libelleCourt() {
		String libelleCourt = libelle();
		if (libelleCourt != null && libelleCourt.length() > 50) {
			libelleCourt = libelleCourt.substring(0, 50);
		}
		return libelleCourt;
	}

    public String codeOrganEtTypeCredit() {
        String code = "(extourne) " + libelleOrgan();
        if (typeCredit() != null) {
            code += " - " + typeCredit().tcdCode();
        }

        return code;
    }
	
	public String libelleOrgan() {
		String libelle = "";
		if (organ() == null) {
			return libelle;
		}

		libelle = organ().sourceLibelle();
		return libelle;
	}

	public EOTauxProrata tauxProrata() {
		return tauxProrata;
	}

	public void setTauxProrata(EOTauxProrata tauxProrata) {
		this.tauxProrata = tauxProrata;
	}

	public Boolean isComplet() {
		return exercice() != null && organ() != null && typeCredit() != null && tauxProrata() != null;
	}

	/**
	 * Ceux de l'exercice précédent
	 */
	public NSArray<EOTauxProrata> tauxProrataDisponibles() {
		if (organ() == null || exercice() == null || exercice().exercicePrecedent() == null) {
			return NSArray.emptyArray();
		}
		return organ().tauxProratasPourExerciceTries(exercice().exercicePrecedent());
	}

	public BigDecimal getDisponible() {
		return vecMontantBudDispoReel().negate();
	}

	/**
	 * Celles de l'exercice précédent
	 */
	public NSArray<EOConvention> getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null && organ() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(organ(), "organ");
			bindings.setObjectForKey(typeCredit(), "typeCredit");
			bindings.setObjectForKey(exercice().exercicePrecedent(), "exercice");
			bindings.setObjectForKey(Boolean.TRUE, "montants");
			//bindings.setObjectForKey(EOConventionNonLimitative.MODE_GESTION_RESSOURCE_AFFECTEE, "convModeGestion");
			arrayConventions = FinderConvention.getConventions(ed, bindings);
		}
		if (arrayConventions == null)
			arrayConventions = NSArray.emptyArray();
		return arrayConventions;
	}

	/**
	 * Ceux de l'exercice précédent
	 */
	public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques == null && utilisateur != null)
			arrayAnalytiques = FinderCodeAnalytique.getCodeAnalytiques(ed, organ(), exercice().exercicePrecedent());
		if (arrayAnalytiques == null)
			arrayAnalytiques = NSArray.emptyArray();
		if (arrayAnalytiques.count() == 0)
			return arrayAnalytiques;
		return arrayAnalytiques;
	}
}