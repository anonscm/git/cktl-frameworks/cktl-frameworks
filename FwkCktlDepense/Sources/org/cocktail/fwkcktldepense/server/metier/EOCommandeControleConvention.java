

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleConventionException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;



public class EOCommandeControleConvention extends _EOCommandeControleConvention
{
	private static Calculs serviceCalculs = Calculs.getInstance();

    public EOCommandeControleConvention() {
        super();
    }
  
    public BigDecimal montantTtc() {
    	return cconTtcSaisie();
    }    

    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
    	
    	if (commandeBudget()!=null && commandeBudget().cbudTtcSaisie()!=null && commandeBudget().cbudTtcSaisie().floatValue()!=0.0)
    		setCconPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(commandeBudget().cbudTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setCconHtSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCconHtSaisie(aValue);
    }

    public void setCconTvaSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCconTvaSaisie(aValue);
    }

    public void setCconTtcSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCconTtcSaisie(aValue);
    }

    public void setCconMontantBudgetaire(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCconMontantBudgetaire(aValue);
    }

    public void setCconPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
       super.setCconPourcentage(aValue);

       if (commandeBudget()!=null) {
		NSArray commandeControleConventions = commandeBudget().commandeControleConventions();
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControleConvention.CONVENTION_KEY+" != nil", null);
		commandeControleConventions = EOQualifier.filteredArrayWithQualifier(commandeControleConventions, qual);
		BigDecimal total=commandeBudget().computeSumForKey(commandeControleConventions, EOCommandeControleConvention.CCON_POURCENTAGE_KEY);
		if (total.floatValue()>100.0)
			super.setCconPourcentage(new BigDecimal(100.0).subtract(total.subtract(aValue)));    			
       }
       
       if (commandeBudget()!=null)
       	commandeBudget().corrigerMontantConventions();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws CommandeControleConventionException {
        if (cconHtSaisie()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.cconHtSaisieManquant);
        if (cconTvaSaisie()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.cconTvaSaisieManquant);
        if (cconTtcSaisie()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.cconTtcSaisieManquant);
        if (cconMontantBudgetaire()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.cconMontantBudgetaireManquant);
        if (cconPourcentage()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.cconPourcentageManquant);
        if (exercice()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.exerciceManquant);
        if (commandeBudget()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.commandeBudgetManquant);
        
        if (cconTvaSaisie().floatValue()<0.0 || cconHtSaisie().floatValue()<0.0 || cconTtcSaisie().floatValue()<0.0 ||
        		cconMontantBudgetaire().floatValue()<0.0)
        	throw new CommandeControleConventionException(CommandeControleConventionException.montantsNegatifs);

        if (!commandeBudget().exercice().equals(exercice()))
            throw new CommandeControleConventionException(CommandeControleConventionException.commandeBudgetExercicePasCoherent);
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        if (convention()==null)
            throw new CommandeControleConventionException(CommandeControleConventionException.conventionManquant);

        if (!cconHtSaisie().abs().add(cconTvaSaisie().abs()).equals(cconTtcSaisie().abs()))
        	setCconTvaSaisie(cconTtcSaisie().subtract(cconHtSaisie()));
        if (commandeBudget().tauxProrata()!=null &&
        		!cconMontantBudgetaire().equals(calculMontantBudgetaire()))
        	setCconMontantBudgetaire(calculMontantBudgetaire());
    }

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(cconHtSaisie(), cconTvaSaisie(), commandeBudget().tauxProrata().tapTaux());
	}
}
