/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOFournisseur.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOFournisseur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleFournisseur";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_FOURNISSEUR";


//Attribute Keys
	public static final ERXKey<String> ADR_ADRESSE1 = new ERXKey<String>("adrAdresse1");
	public static final ERXKey<String> ADR_ADRESSE2 = new ERXKey<String>("adrAdresse2");
	public static final ERXKey<String> ADR_BP = new ERXKey<String>("adrBp");
	public static final ERXKey<String> ADR_CIVILITE = new ERXKey<String>("adrCivilite");
	public static final ERXKey<String> ADR_CP = new ERXKey<String>("adrCp");
	public static final ERXKey<String> ADR_CP_ETRANGER = new ERXKey<String>("adrCpEtranger");
	public static final ERXKey<String> ADR_VILLE = new ERXKey<String>("adrVille");
	public static final ERXKey<String> FOU_CODE = new ERXKey<String>("fouCode");
	public static final ERXKey<String> FOU_ETRANGER = new ERXKey<String>("fouEtranger");
	public static final ERXKey<String> FOU_MARCHE = new ERXKey<String>("fouMarche");
	public static final ERXKey<String> FOU_NOM = new ERXKey<String>("fouNom");
	public static final ERXKey<Integer> FOU_ORDRE = new ERXKey<Integer>("fouOrdre");
	public static final ERXKey<String> FOU_TYPE = new ERXKey<String>("fouType");
	public static final ERXKey<String> FOU_VALIDE = new ERXKey<String>("fouValide");
	public static final ERXKey<String> PAYS = new ERXKey<String>("pays");
	public static final ERXKey<String> SIRET = new ERXKey<String>("siret");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse> ADRESSE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse>("adresse");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure> STRUCTURE_ULRS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure>("structureUlrs");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String ADR_CIVILITE_KEY = "adrCivilite";
	public static final String ADR_CP_KEY = "adrCp";
	public static final String ADR_CP_ETRANGER_KEY = "adrCpEtranger";
	public static final String ADR_VILLE_KEY = "adrVille";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_ETRANGER_KEY = "fouEtranger";
	public static final String FOU_MARCHE_KEY = "fouMarche";
	public static final String FOU_NOM_KEY = "fouNom";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String PAYS_KEY = "pays";
	public static final String SIRET_KEY = "siret";

//Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_BP_COLKEY = "ADR_BP";
	public static final String ADR_CIVILITE_COLKEY = "ADR_CIVILITE";
	public static final String ADR_CP_COLKEY = "ADR_CP";
	public static final String ADR_CP_ETRANGER_COLKEY = "ADR_CP_ETRANGER";
	public static final String ADR_VILLE_COLKEY = "ADR_VILLE";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_ETRANGER_COLKEY = "FOU_ETRANGER";
	public static final String FOU_MARCHE_COLKEY = "FOU_MARCHE";
	public static final String FOU_NOM_COLKEY = "FOU_NOM";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
	public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
	public static final String PAYS_COLKEY = "PAYS";
	public static final String SIRET_COLKEY = "SIRET";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String PERSONNE_KEY = "personne";
	public static final String STRUCTURE_ULRS_KEY = "structureUlrs";



	// Accessors methods
	public String adrAdresse1() {
	 return (String) storedValueForKey(ADR_ADRESSE1_KEY);
	}

	public void setAdrAdresse1(String value) {
	 takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
	}

	public String adrAdresse2() {
	 return (String) storedValueForKey(ADR_ADRESSE2_KEY);
	}

	public void setAdrAdresse2(String value) {
	 takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
	}

	public String adrBp() {
	 return (String) storedValueForKey(ADR_BP_KEY);
	}

	public void setAdrBp(String value) {
	 takeStoredValueForKey(value, ADR_BP_KEY);
	}

	public String adrCivilite() {
	 return (String) storedValueForKey(ADR_CIVILITE_KEY);
	}

	public void setAdrCivilite(String value) {
	 takeStoredValueForKey(value, ADR_CIVILITE_KEY);
	}

	public String adrCp() {
	 return (String) storedValueForKey(ADR_CP_KEY);
	}

	public void setAdrCp(String value) {
	 takeStoredValueForKey(value, ADR_CP_KEY);
	}

	public String adrCpEtranger() {
	 return (String) storedValueForKey(ADR_CP_ETRANGER_KEY);
	}

	public void setAdrCpEtranger(String value) {
	 takeStoredValueForKey(value, ADR_CP_ETRANGER_KEY);
	}

	public String adrVille() {
	 return (String) storedValueForKey(ADR_VILLE_KEY);
	}

	public void setAdrVille(String value) {
	 takeStoredValueForKey(value, ADR_VILLE_KEY);
	}

	public String fouCode() {
	 return (String) storedValueForKey(FOU_CODE_KEY);
	}

	public void setFouCode(String value) {
	 takeStoredValueForKey(value, FOU_CODE_KEY);
	}

	public String fouEtranger() {
	 return (String) storedValueForKey(FOU_ETRANGER_KEY);
	}

	public void setFouEtranger(String value) {
	 takeStoredValueForKey(value, FOU_ETRANGER_KEY);
	}

	public String fouMarche() {
	 return (String) storedValueForKey(FOU_MARCHE_KEY);
	}

	public void setFouMarche(String value) {
	 takeStoredValueForKey(value, FOU_MARCHE_KEY);
	}

	public String fouNom() {
	 return (String) storedValueForKey(FOU_NOM_KEY);
	}

	public void setFouNom(String value) {
	 takeStoredValueForKey(value, FOU_NOM_KEY);
	}

	public Integer fouOrdre() {
	 return (Integer) storedValueForKey(FOU_ORDRE_KEY);
	}

	public void setFouOrdre(Integer value) {
	 takeStoredValueForKey(value, FOU_ORDRE_KEY);
	}

	public String fouType() {
	 return (String) storedValueForKey(FOU_TYPE_KEY);
	}

	public void setFouType(String value) {
	 takeStoredValueForKey(value, FOU_TYPE_KEY);
	}

	public String fouValide() {
	 return (String) storedValueForKey(FOU_VALIDE_KEY);
	}

	public void setFouValide(String value) {
	 takeStoredValueForKey(value, FOU_VALIDE_KEY);
	}

	public String pays() {
	 return (String) storedValueForKey(PAYS_KEY);
	}

	public void setPays(String value) {
	 takeStoredValueForKey(value, PAYS_KEY);
	}

	public String siret() {
	 return (String) storedValueForKey(SIRET_KEY);
	}

	public void setSiret(String value) {
	 takeStoredValueForKey(value, SIRET_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAdresse adresse() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAdresse)storedValueForKey(ADRESSE_KEY);
	}

	public void setAdresseRelationship(org.cocktail.fwkcktldepense.server.metier.EOAdresse value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAdresse oldValue = adresse();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktldepense.server.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> structureUlrs() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure>)storedValueForKey(STRUCTURE_ULRS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> structureUlrs(EOQualifier qualifier) {
	 return structureUlrs(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> structureUlrs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> results;
			   results = structureUlrs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToStructureUlrsRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
	}
	
	public void removeFromStructureUlrsRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOStructure createStructureUlrsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOStructure.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, STRUCTURE_ULRS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOStructure) eo;
	}
	
	public void deleteStructureUlrsRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, STRUCTURE_ULRS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllStructureUlrsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOStructure> objects = structureUlrs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteStructureUlrsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOFournisseur avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOFournisseur createEOFournisseur(EOEditingContext editingContext										, String adrCivilite
													, String fouCode
							, String fouEtranger
							, String fouMarche
							, String fouNom
							, Integer fouOrdre
							, String fouType
							, String fouValide
									, org.cocktail.fwkcktldepense.server.metier.EOAdresse adresse		, org.cocktail.fwkcktldepense.server.metier.EOPersonne personne					) {
	 EOFournisseur eo = (EOFournisseur) EOUtilities.createAndInsertInstance(editingContext, _EOFournisseur.ENTITY_NAME);	 
													eo.setAdrCivilite(adrCivilite);
															eo.setFouCode(fouCode);
									eo.setFouEtranger(fouEtranger);
									eo.setFouMarche(fouMarche);
									eo.setFouNom(fouNom);
									eo.setFouOrdre(fouOrdre);
									eo.setFouType(fouType);
									eo.setFouValide(fouValide);
										 eo.setAdresseRelationship(adresse);
				 eo.setPersonneRelationship(personne);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournisseur creerInstance(EOEditingContext editingContext) {
		EOFournisseur object = (EOFournisseur)EOUtilities.createAndInsertInstance(editingContext, _EOFournisseur.ENTITY_NAME);
  		return object;
		}

	

  public EOFournisseur localInstanceIn(EOEditingContext editingContext) {
    EOFournisseur localInstance = (EOFournisseur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFournisseur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFournisseur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFournisseur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFournisseur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFournisseur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFournisseur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFournisseur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFournisseur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFournisseur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFournisseur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFournisseur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFournisseur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFournisseur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFournisseur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> objectsForRecherche(EOEditingContext ec,
														String adrCiviliteBinding,
														String cStructureCleBinding,
														String fouCodeBinding,
														String fouNomBinding,
														String fournisseurInstanceValidationBinding,
														String fournisseurValideBinding,
														org.cocktail.fwkcktldepense.server.metier.EOPersonne personneBinding,
														String siretBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOFournisseur.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (adrCiviliteBinding != null)
			bindings.takeValueForKey(adrCiviliteBinding, "adrCivilite");
		  if (cStructureCleBinding != null)
			bindings.takeValueForKey(cStructureCleBinding, "cStructureCle");
		  if (fouCodeBinding != null)
			bindings.takeValueForKey(fouCodeBinding, "fouCode");
		  if (fouNomBinding != null)
			bindings.takeValueForKey(fouNomBinding, "fouNom");
		  if (fournisseurInstanceValidationBinding != null)
			bindings.takeValueForKey(fournisseurInstanceValidationBinding, "fournisseurInstanceValidation");
		  if (fournisseurValideBinding != null)
			bindings.takeValueForKey(fournisseurValideBinding, "fournisseurValide");
		  if (personneBinding != null)
			bindings.takeValueForKey(personneBinding, "personne");
		  if (siretBinding != null)
			bindings.takeValueForKey(siretBinding, "siret");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
