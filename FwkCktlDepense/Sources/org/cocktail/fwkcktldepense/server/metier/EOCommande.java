/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.cocktail.fwkcktldepense.server.exception.CommandeException;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepensePapier;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderFournisseur;
import org.cocktail.fwkcktldepense.server.finder.FinderOrgan;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeEtat;
import org.cocktail.fwkcktldepense.server.util.CommandeArticleComparator;
import org.cocktail.fwkcktldepense.server.util.ObjetHorsMarche;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCommande extends _EOCommande {
	public static final String ETAT_PRECOMMANDE = "PRECOMMANDE";
	public static final String ETAT_PARTIELLEMENT_ENGAGEE = "PART_ENGAGEE";
	public static final String ETAT_ENGAGEE = "ENGAGEE";
	public static final String ETAT_PARTIELLEMENT_SOLDEE = "PART_SOLDEE";
	public static final String ETAT_SOLDEE = "SOLDEE";
	public static final String ETAT_ANNULEE = "ANNULEE";

	public static final String ETAT_IMPRIMABLE = "OUI";
	public static final String ETAT_NON_IMPRIMABLE = "NON";

	//private TableauHorsMarche tableauHorsMarche;
	//private TableauMarche tableauMarche;

	private NSMutableArray<NSDictionary<String, Object>> tableauHorsMarche, tableauMarche;

	private Number commIdProc;
	private EOFournisseur fournisseurInterneClient;
	private EOAttribution monAttribution;

	private boolean isEnregistrerPI = false;
	private String messageErreur = "";

	private Number prestId = null;

	private EOTypeApplication typeApplication;

	public EOCommande() {
		super();
		commIdProc = null;
		monAttribution = null;

		reconstruireTableaux();
	}

	public Number prestationId() {
		return prestId;
	}

	public void setMonAttribution(EOAttribution attribution) {
		monAttribution = attribution;
	}

	public void setPrestationId(Number prestationId) {
		prestId = prestationId;
	}

	public void setTypeApplication(EOTypeApplication typa) {
		typeApplication = typa;
	}

	public EOTypeApplication typeApplication() {
		return typeApplication;
	}

	public void setCommIdProc(Number value) {
		commIdProc = value;
	}

	public Number commIdProc() {
		return commIdProc;
	}

	public int decimalesPourArrondirMontant() {
		if (devise() == null)
			return EODevise.defaultNbDecimales;
		return devise().devNbDecimales().intValue();
	}

	public boolean isEnregistrerPi() {
		return isEnregistrerPI;
	}

	public boolean isMontantNonImprimable() {
		if (commandeEngagements() == null || commandeEngagements().count() == 0)
			return false;

		for (int i = 0; i < commandeEngagements().count(); i++) {
			EOEngagementBudget engage = ((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget();
			if (engage == null)
				return false;
			for (int j = 0; j < engage.engagementControlePlanComptables().count(); j++) {
				EOPlanComptable planco = ((EOEngagementControlePlanComptable) engage.engagementControlePlanComptables().objectAtIndex(j)).planComptable();
				if (!planco.isNonImprimable())
					return false;
			}
		}

		return true;
	}

	public boolean isBasculee() {
		if (commandeBascule() != null && commandeBascule().count() > 0)
			return true;
		return false;
	}

	public EOCommande commandeBasculee() {
		if (!isBasculee())
			return null;

		return ((EOCommandeBascule) commandeBascule().objectAtIndex(0)).commandeDestination();
	}

	public void setIsEnregistrerPI(boolean value) {
		isEnregistrerPI = value;
	}

	public boolean isPrestationInterne() {
		if (exercice() == null || fournisseur() == null)
			return false;
		if (!FinderParametre.getParametreAutorisePIFromCommande(editingContext(), exercice()))
			return false;

		if (commandeBudgets() != null && commandeBudgets().count() > 1)
			return false;
		if (fournisseur().isFournisseurInterne())
			return true;

		return false;
	}

	public boolean isPrestationInfosGood() {
		try {
			validatePrestationInfos();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void validatePrestationInfos() throws NSValidation.ValidationException {
		if (utilisateur() == null)
			throw new FactoryException("Pour une prestation interne il faut un utilisateur");
		if (fournisseurInterneClient() == null)
			throw new FactoryException("Pour une prestation interne il faut un fournisseur interne 'client'");

		if (commandeEngagements() == null || commandeEngagements().count() != 1)
			throw new FactoryException("Pour une prestation interne il faut un seul engagement");

		EOCommandeEngagement commandeEngagement = (EOCommandeEngagement) commandeEngagements().objectAtIndex(0);
		if (commandeEngagement.engagementBudget() != null && commandeEngagement.engagementBudget().engagementControlePlanComptables() != null &&
				commandeEngagement.engagementBudget().engagementControlePlanComptables().count() > 1)
			throw new FactoryException("Pour une prestation interne il faut une seule imputation comptable");
	}

	public void setFournisseurInterneClient(EOFournisseur fournisseur) {
		fournisseurInterneClient = fournisseur;
	}

	public EOFournisseur fournisseurInterneClient() {
		if (fournisseurInterneClient == null) {
			if (commandeBudgets() != null && commandeBudgets().count() == 1) {
				EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(0);

				EOPersonne personne = personnePourOrgan(commandeBudget.organ());
				if (personne != null) {
					NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
					bindings.setObjectForKey(personne, "personne");
					NSArray<EOFournisseur> lesFournisseurs = FinderFournisseur.getFournisseurs(editingContext(), bindings);

					if (lesFournisseurs != null && lesFournisseurs.count() > 0)
						fournisseurInterneClient = (EOFournisseur) lesFournisseurs.objectAtIndex(0);
				}
			}
		}
		return fournisseurInterneClient;
	}

	private EOPersonne personnePourOrgan(EOOrgan organ) {
		if (organ == null)
			return null;
		if (organ.structureUlr() != null && organ.structureUlr().personne() != null)
			return organ.structureUlr().personne();
		if (organ.orgNiveau().intValue() > 2)
			return personnePourOrgan(organ.organPere());
		return null;
	}

	public NSArray<EODepenseBudget> initialiserDepenseBudgets(EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur, boolean prerempli)
			throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (depensePapier.fournisseur() == null)
			depensePapier.setFournisseurRelationship(fournisseur());
		if (!depensePapier.fournisseur().equals(fournisseur()))
			throw new FactoryException("le fournisseur de la depensePapier doit etre le meme que celui de la commande");

		new FactoryDepensePapier().supprimerDepenseBudgets(depensePapier);

		NSMutableArray<EODepenseBudget> depenseBudgets = new NSMutableArray<EODepenseBudget>();

		for (int i = 0; i < commandeEngagements().count(); i++) {
			EOEngagementBudget engagementBudget = ((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget();
			EODepenseBudget depenseBudget = engagementBudget.initialiserDepenseBudget(ed, depensePapier, utilisateur, prerempli);
			if (depenseBudget != null) {
				depenseBudgets.addObject(depenseBudget);
			}
		}

		if (!prerempli && depenseBudgets != null && depenseBudgets.count() == 1) {
			((EODepenseBudget) depenseBudgets.objectAtIndex(0)).setDepHtSaisie(depensePapier.dppHtInitial());
			((EODepenseBudget) depenseBudgets.objectAtIndex(0)).setDepTtcSaisie(depensePapier.dppTtcInitial());
		}

		return depenseBudgets;
	}

	public NSArray initialiserPreDepenseBudgets(EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur, boolean prerempli)
			throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (depensePapier.fournisseur() == null)
			depensePapier.setFournisseurRelationship(fournisseur());
		if (!depensePapier.fournisseur().equals(fournisseur()))
			throw new FactoryException("le fournisseur de la depensePapier doit etre le meme que celui de la commande");

		new FactoryDepensePapier().supprimerPreDepenseBudgets(depensePapier);

		FactoryPreDepenseBudget factoryPreDepenseBudget = new FactoryPreDepenseBudget();

		NSMutableArray preDepenseBudgets = new NSMutableArray();

		for (int i = 0; i < commandeEngagements().count(); i++) {
			EOEngagementBudget engagementBudget = ((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget();

			if (engagementBudget.isSolde())
				continue;

			EOPreDepenseBudget preDepenseBudget;

			if (!prerempli) {
				preDepenseBudget = factoryPreDepenseBudget.creer(ed, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), depensePapier,
						engagementBudget, engagementBudget.tauxProrata(), engagementBudget.exercice(), utilisateur, engagementBudget.source().type(), engagementBudget.source());
			}
			else {
				BigDecimal engHt = engagementBudget.montantHtReste();
				BigDecimal engTtc = engagementBudget.montantTtcReste();
				preDepenseBudget = factoryPreDepenseBudget.creer(ed, engHt, engTtc.subtract(engHt), engTtc,
						engagementBudget.engMontantBudgetaireReste(), depensePapier,
						engagementBudget, engagementBudget.tauxProrata(), engagementBudget.exercice(), utilisateur, engagementBudget.source().type(), engagementBudget.source());
			}
			preDepenseBudgets.addObject(preDepenseBudget);
		}

		if (!prerempli && preDepenseBudgets != null && preDepenseBudgets.count() == 1) {
			((EOPreDepenseBudget) preDepenseBudgets.objectAtIndex(0)).setDepHtSaisie(depensePapier.dppHtInitial());
			((EOPreDepenseBudget) preDepenseBudgets.objectAtIndex(0)).setDepTtcSaisie(depensePapier.dppTtcInitial());
		}

		return preDepenseBudgets;
	}

	public EOTva tvaCommune(EOCodeExer codeExer) {
		EOTva tva = null;

		if (articles() == null || articles().count() == 0)
			return null;

		for (int i = 0; i < articles().count(); i++) {
			EOArticle article = (EOArticle) articles().objectAtIndex(i);

			if (tva == null) {
				if (codeExer == null) {
					if (article.tva() == null)
						return null;
					tva = article.tva();
				}
				else {
					if (article.codeExer() != null && FinderCodeExer.getCodeExerNiveau2(article.codeExer()).equals(codeExer)) {
						if (article.tva() == null)
							return null;
						tva = article.tva();
					}
				}

				continue;
			}

			if (codeExer == null) {
				if (article.tva() == null || !article.tva().equals(tva))
					return null;
			}
			else {
				if (article.codeExer() != null && FinderCodeExer.getCodeExerNiveau2(article.codeExer()).equals(codeExer)) {
					if (article.tva() == null)
						return null;
					if (!article.tva().equals(tva))
						return null;
				}
			}
		}
		return tva;
	}

	public boolean isRepartitionAutomatiquePossible() {
		if (tableauHorsMarche == null)
			tableauHorsMarche = new NSMutableArray<NSDictionary<String, Object>>();
		if (tableauMarche == null)
			tableauMarche = new NSMutableArray<NSDictionary<String, Object>>();
		if (tableauHorsMarche.count() == 0 && tableauMarche.count() == 0)
			return false;

		//if (tableauHorsMarche.count()>1)
		//	return false;
		if (tableauMarche.count() > 1)
			return false;

		if (commandeBudgets() == null || commandeBudgets().count() != 1)
			return false;

		return true;
	}

	public void setRepartitionAutomatiquePossible() {
		if (!isRepartitionAutomatiquePossible())
			return;

		EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(0);

		if (tableauHorsMarche != null && tableauHorsMarche.count() > 0) {

			for (int i = 0; i < tableauHorsMarche.count(); i++) {

				NSDictionary<String, Object> ligne = tableauHorsMarche.objectAtIndex(i);

				NSMutableArray lesQualifiers = new NSMutableArray();
				lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@",
						new NSArray((EOTypeAchat) ligne.objectForKey("typeAchat"))));
				lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@",
						new NSArray((EOCodeExer) ligne.objectForKey("codeExer"))));
				NSArray resultat = EOQualifier.filteredArrayWithQualifier((NSArray) commandeBudget.
						valueForKeyPath(EOCommandeBudget.COMMANDE_CONTROLE_HORS_MARCHES_KEY), new EOAndQualifier(lesQualifiers));

				if (resultat.count() == 1) {
					EOCommandeControleHorsMarche ctrl = (EOCommandeControleHorsMarche) resultat.objectAtIndex(0);

					ctrl.setChomHtSaisie((BigDecimal) ligne.objectForKey("ht"));
					ctrl.setChomTtcSaisie((BigDecimal) ligne.objectForKey("ttc"));
				}
			}
			return;
		}

		if (tableauMarche != null && tableauMarche.count() == 1) {

			if (commandeBudget.commandeControleMarches() == null || commandeBudget.commandeControleMarches().count() != 1)
				return;

			EOCommandeControleMarche ctrl = (EOCommandeControleMarche) commandeBudget.commandeControleMarches().objectAtIndex(0);

			ctrl.setCmarHtSaisie((BigDecimal) (tableauMarche.objectAtIndex(0)).objectForKey("ht"));
			ctrl.setCmarTtcSaisie((BigDecimal) (tableauMarche.objectAtIndex(0)).objectForKey("ttc"));
		}

	}

	public boolean isRepartitionMarcheHorsMarcheGood() {
		if (isSurUnMarche())
			return isControleMarcheGood();
		return isControleHorsMarcheGood();
	}

	public boolean isControleActionGood() {
		if (commandeBudgets() == null)
			return false;
		for (int i = 0; i < commandeBudgets().count(); i++)
			if (!((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).isControleActionGood())
				return false;

		if (totalHt().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_HT_SAISIE_KEY)) != 0)
			return false;
		if (totalTtc().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_TTC_SAISIE_KEY)) != 0)
			return false;

		return true;
	}

	public boolean isFournisseurModifiable() {
		return false;
	}

	public boolean isTauxProrataModifiableLiquidation(EOEditingContext ed) {
		if (ed == null || exercice() == null)
			return false;
		return !FinderParametre.getParametreDepenseMemeProrataEngagement(ed, exercice());
	}

	public boolean isControleAnalytiqueGood() {
		if (commandeBudgets() == null)
			return false;
		for (int i = 0; i < commandeBudgets().count(); i++)
			if (!((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).isControleAnalytiqueGood())
				return false;

		if (totalHt().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_HT_SAISIE_KEY)) != 0)
			return false;
		if (totalTtc().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_TTC_SAISIE_KEY)) != 0)
			return false;

		return true;
	}

	public boolean isControleConventionGood() {
		if (commandeBudgets() == null)
			return false;
		for (int i = 0; i < commandeBudgets().count(); i++)
			if (!((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).isControleConventionGood())
				return false;

		if (totalHt().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_HT_SAISIE_KEY)) != 0)
			return false;
		if (totalTtc().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_TTC_SAISIE_KEY)) != 0)
			return false;

		return true;
	}

	public boolean isControleHorsMarcheGood() {
		if (commandeBudgets() == null)
			return false;
		for (int i = 0; i < commandeBudgets().count(); i++)
			if (!((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).isControleHorsMarcheGood())
				return false;

		if (totalHt().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_HT_SAISIE_KEY)) != 0)
			return false;
		if (totalTtc().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_TTC_SAISIE_KEY)) != 0)
			return false;

		NSMutableArray arrayCtrlHorsMarche = new NSMutableArray();
		for (int i = 0; i < commandeBudgets().count(); i++)
			arrayCtrlHorsMarche.addObjectsFromArray((NSArray) ((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).
					valueForKeyPath(EOCommandeBudget.COMMANDE_CONTROLE_HORS_MARCHES_KEY));

		for (int i = 0; i < tableauHorsMarche.count(); i++) {
			NSDictionary ligne = (NSDictionary) tableauHorsMarche.objectAtIndex(i);

			NSMutableArray lesQualifiers = new NSMutableArray();
			lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@",
					new NSArray((EOTypeAchat) ligne.objectForKey("typeAchat"))));
			lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@",
					new NSArray((EOCodeExer) ligne.objectForKey("codeExer"))));

			NSArray array = EOQualifier.filteredArrayWithQualifier(arrayCtrlHorsMarche, new EOAndQualifier(lesQualifiers));

			if (((BigDecimal) ligne.objectForKey("ht")).compareTo(computeSumForKey(array, EOCommandeControleHorsMarche.CHOM_HT_SAISIE_KEY)) != 0)
				return false;
			if (((BigDecimal) ligne.objectForKey("ttc")).compareTo(computeSumForKey(array, EOCommandeControleHorsMarche.CHOM_TTC_SAISIE_KEY)) != 0)
				return false;
		}

		return true;
	}

	public boolean isControleMarcheGood() {
		if (commandeBudgets() == null)
			return false;
		for (int i = 0; i < commandeBudgets().count(); i++)
			if (!((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).isControleMarcheGood())
				return false;

		if (totalHt().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_HT_SAISIE_KEY)) != 0)
			return false;
		if (totalTtc().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_TTC_SAISIE_KEY)) != 0)
			return false;

		return true;
	}

	public boolean isControlePlanComptableGood() {
		if (commandeBudgets() == null)
			return false;
		for (int i = 0; i < commandeBudgets().count(); i++)
			if (!((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).isControlePlanComptableGood())
				return false;

		if (totalHt().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_HT_SAISIE_KEY)) != 0)
			return false;
		if (totalTtc().compareTo(computeSumForKey(commandeBudgets(), EOCommandeBudget.CBUD_TTC_SAISIE_KEY)) != 0)
			return false;

		return true;
	}

	public boolean isImprimable() {
		if (typeEtatImprimable() == null)
			return false;
		if (!typeEtatImprimable().tyetLibelle().equals(EOCommande.ETAT_IMPRIMABLE))
			return false;
		return true;
	}

	public boolean isEnvoyerB2bPossible() {
		if (!isCommandeB2b()) {
			return false;
		}
		if (typeEtatImprimable() == null)
			return false;
		if (!typeEtatImprimable().tyetLibelle().equals(EOCommande.ETAT_IMPRIMABLE))
			return false;

		return true;
	}

	public boolean isCommandeB2b() {
		return (toB2bCxmlPunchoutmsgs() != null && toB2bCxmlPunchoutmsgs().count() > 0);
	}

	public boolean isDepenseAuComptantPossible() {
		if (exercice() == null)
			return false;
		BigDecimal max = FinderParametre.getParametreDepenseComptantMax(editingContext(), exercice());
		if (max.floatValue() > totalTtc().floatValue())
			return true;
		return false;
	}

	public BigDecimal totalHt() {
		return computeSumForKey(articles(), EOArticle.ART_PRIX_TOTAL_HT_KEY);
	}

	public BigDecimal totalTtc() {
		return computeSumForKey(articles(), EOArticle.ART_PRIX_TOTAL_TTC_KEY);
	}

	public boolean isComplete() {
		BigDecimal ht = new BigDecimal(0.0), ttc = new BigDecimal(0.0);

		if (articles().count() == 0)
			return false;

		for (int i = 0; i < commandeBudgets().count(); i++) {
			EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(i);
			if (!commandeBudget.isComplet())
				return false;
			ht = ht.add(commandeBudget.cbudHtSaisie());
			ttc = ttc.add(commandeBudget.cbudTtcSaisie());
		}

		if (ht.compareTo(computeSumForKey(articles(), EOArticle.ART_PRIX_TOTAL_HT_KEY)) != 0 ||
				ttc.compareTo(computeSumForKey(articles(), EOArticle.ART_PRIX_TOTAL_TTC_KEY)) != 0)
			return false;

		return true;
	}

	public boolean isEnregistrable() {
		messageErreur = "";

		if (articles() == null || articles().count() == 0) {
			messageErreur = "La commande ne possede aucun article.";
			return false;
		} else {
			for (int i = 0; i < articles().count(); i++) {
				EOCodeExer codeExer = (EOCodeExer) articles().objectAtIndex(i).codeExer();
				if (codeExer == null) {
					messageErreur = "Certains articles ne possèdent pas de code de nomenclature.";
					return false;
				}
			}
		}

		if (commLibelle() == null || commLibelle().length() < 1) {
			messageErreur = "Le libelle de la commande doit-etre renseigne.";
			return false;
		}

		if (commandeBudgets() != null) {
			if (isControleMaxDepasse()) {
				messageErreur = "Le seuil maximum d'un des codes de nomenclature est depasse.";
				return false;
			}
			for (int i = 0; i < commandeBudgets().count(); i++) {
				EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(i);
				if (!commandeBudget.isComplet()) {
					messageErreur = "La repartition est incomplete.";
					return false;
				}
			}

			// Test seulement a la creation de la commande
			if (EOUtilities.primaryKeyForObject(editingContext(), this) == null) {

				// Test sur les montants des conventions
				for (int i = 0; i < commandeBudgets().count(); i++) {
					EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(i);
					if (commandeBudget.commandeControleConventions() == null || commandeBudget.commandeControleConventions().count() < 1)
						continue;
					for (int j = 0; j < commandeBudget.commandeControleConventions().count(); j++) {
						EOCommandeControleConvention ctrl = (EOCommandeControleConvention) commandeBudget.commandeControleConventions().objectAtIndex(j);

						EOConvention convention = ctrl.convention();
						if (convention == null || convention.disponible() == null)
							continue;
						if (convention.disponible().floatValue() < ctrl.cconHtSaisie().floatValue()) {
							messageErreur = "Le disponible d'une convention selectionnee sera depasse.";
							return !convention.isLimitative();
						}
					}
				}

				// Test sur les montants des codes analytiques
				for (int i = 0; i < commandeBudgets().count(); i++) {
					EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(i);
					if (commandeBudget.commandeControleAnalytiques() == null || commandeBudget.commandeControleAnalytiques().count() < 1)
						continue;
					for (int j = 0; j < commandeBudget.commandeControleAnalytiques().count(); j++) {
						EOCommandeControleAnalytique ctrl = (EOCommandeControleAnalytique) commandeBudget.commandeControleAnalytiques().objectAtIndex(j);

						EOCodeAnalytique codeAnalytique = ctrl.codeAnalytique();
						if (codeAnalytique == null || codeAnalytique.canMontant() == null)
							continue;
						BigDecimal montantSuivi = new BigDecimal(0.0);
						if (codeAnalytique.codeAnalytiqueSuivi() != null)
							montantSuivi = codeAnalytique.codeAnalytiqueSuivi().montantBudgetaire();
						if (codeAnalytique.canMontant().floatValue() < ctrl.canaMontantBudgetaire().floatValue() + montantSuivi.floatValue()) {
							messageErreur = "Le disponible d'un code analytique selectionne sera depasse.";
							return !codeAnalytique.typeEtatDepassement().tyetLibelle().equals(EOTypeEtat.CODE_ANALYTIQUE_BLOCAGE);
						}
					}
				}

			}
		}

		return true;
	}

	public String messageErreur() {
		return messageErreur;
	}

	public boolean isDuplicable(EOExercice exerciceDestination) {
		if (!exerciceDestination.estEngageableOuvert() && !exerciceDestination.estEngageableRestreint())
			return false;
		if (attribution() != null && !attribution().isValideForDate(new NSTimestamp()))
			return false;

		return true;
	}

	public boolean isConsultable(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (ed == null || utilisateur == null || exercice() == null)
			return false;
		return isConsultable(ed, utilisateur, FinderOrgan.getOrgans(ed, utilisateur, exercice()));
	}

	public boolean isConsultable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		// si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires presentes alors non
		if (organs == null)
			organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

		// si il y a ni engagement ni prevision de financement, c'est une precommande
		if (commandeEngagements().count() == 0 && commandeBudgets().count() == 0) {
			//si l'utilisateur a des droits alors oui
			if (organs.count() > 0)
				return true;
			// si l'utilisateur la cree alors oui
			if (utilisateur().equals(utilisateur))
				return true;

			return false;
		}

		// Si l'utilisateur n'a aucun droit sur les lignes budgetaires alors non
		if (organs.count() == 0)
			return false;

		for (int i = 0; i < commandeEngagements().count(); i++) {
			if (!organs.containsObject(((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget().organ()))
				return false;
		}

		for (int i = 0; i < commandeBudgets().count(); i++) {
			if (!organs.containsObject(((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).organ()))
				return false;
		}

		// l'utilisateur a les droits necessaires alors oui
		return true;
	}

	public boolean isSupprimable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		setTypeEtatRelationship(typeEtatSuivantEngagements());

		// si il y a au moins une liquidation alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_SOLDEE) ||
				typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_SOLDEE))
			return false;

		// si la commande est deja annulee alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_ANNULEE))
			return false;

		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE) ||
				typeEtat().tyetLibelle().equals(EOCommande.ETAT_ENGAGEE)) {

			// si l'utilisateur n'a pas le droit d'engager sur cet exercice alors non
			if (!FinderExercice.getExercicesEngageablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
				return false;
		}

		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_PRECOMMANDE)) {
			// si l'exercice n'est pas ouvert pour cet utilisateur alors non
			if (!FinderExercice.getExercicesOuvertsPourUtilisateur(ed, utilisateur).containsObject(exercice()))
				return false;
		}

		// si au moins un engagement n'est pas type depense alors pas supprimable
		if (commandeEngagements() != null) {
			for (int i = 0; i < commandeEngagements().count(); i++) {
				if (!((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).
						engagementBudget().typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
					return false;
			}
		}

		// est-ce qu'elle est utilisee dans une prestation interne
		if (!isToutEngagementDepense())
			return false;

		// si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires presentes alors non
		if (organs == null)
			organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

		return isConsultable(ed, utilisateur, organs);
	}

	public boolean isSoldable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		setTypeEtatRelationship(typeEtatSuivantEngagements());

		// si la commande est soldee alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_SOLDEE))
			return false;

		// si la commande est deja annulee alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_ANNULEE))
			return false;

		// si la commande est une precommande alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_PRECOMMANDE))
			return false;

		// si la commande n'a pas de liquidation alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_ENGAGEE))
			return false;

		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE) ||
				typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_SOLDEE)) {

			// si l'utilisateur n'a pas le droit d'engager sur cet exercice alors non
			if (!FinderExercice.getExercicesEngageablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
				return false;
		}

		// si au moins un engagement n'est pas type depense alors pas soldable
		if (commandeEngagements() != null) {
			for (int i = 0; i < commandeEngagements().count(); i++) {
				if (!((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).
						engagementBudget().typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
					return false;
			}
		}

		// est-ce qu'elle est utilisee dans une prestation interne
		if (!isToutEngagementDepense())
			return false;

		// si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires presentes alors non
		if (organs == null)
			organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

		return isConsultable(ed, utilisateur, organs);
	}

	public boolean isModifiable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {

		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		setTypeEtatRelationship(typeEtatSuivantEngagements());

		// si la commande est soldee alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_SOLDEE))
			return false;

		// si la commande est deja annulee alors non
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_ANNULEE))
			return false;

		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE) ||
				typeEtat().tyetLibelle().equals(EOCommande.ETAT_ENGAGEE) ||
				typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_SOLDEE)) {

			// si l'utilisateur n'a pas le droit d'engager sur cet exercice alors non
			if (!FinderExercice.getExercicesEngageablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
				return false;
		}

		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_PRECOMMANDE)) {
			// si l'exercice n'est pas ouvert pour cet utilisateur alors non
			if (!FinderExercice.getExercicesOuvertsPourUtilisateur(ed, utilisateur).containsObject(exercice()))
				return false;
		}

		// si au moins un engagement n'est pas type depense alors pas modifiable
		if (commandeEngagements() != null) {
			for (int i = 0; i < commandeEngagements().count(); i++) {
				if (!((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).
						engagementBudget().typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
					return false;
			}
		}

		// est-ce qu'elle est utilisee dans une prestation interne
		if (!isToutEngagementDepense())
			return false;

		// si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires presentes alors non
		if (organs == null)
			organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

		return isConsultable(ed, utilisateur, organs);
	}

	public boolean isLiquidable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {
		if (ed == null || utilisateur == null || exercice() == null)
			return false;

		setTypeEtatRelationship(typeEtatSuivantEngagements());

		if (!typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE) &&
				!typeEtat().tyetLibelle().equals(EOCommande.ETAT_ENGAGEE) &&
				!typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_SOLDEE))
			return false;

		if (!FinderExercice.getExercicesLiquidablesPourUtilisateur(ed, utilisateur).containsObject(exercice()))
			return false;

		// si l'utilisateur n'a pas les droits sur ttes les lignes budgetaires presentes alors non
		if (organs == null)
			organs = FinderOrgan.getOrgans(ed, utilisateur, exercice());

		if (!isConsultable(ed, utilisateur, organs))
			return false;

		// est-ce qu'elle est utilisee dans une prestation interne
		if (!isToutEngagementDepense())
			return false;

		if (!isImprimable())
			return false;

		// l'utilisateur a les droits necessaires alors oui
		return true;
	}

	private boolean isToutEngagementDepense() {
		if (commandeEngagements() != null) {
			for (int i = 0; i < commandeEngagements().count(); i++) {
				EOCommandeEngagement commandeEngagement = (EOCommandeEngagement) commandeEngagements().objectAtIndex(i);
				if (!commandeEngagement.engagementBudget().typeApplication().tyapStrid().
						equals(EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK))
					return false;
			}
		}

		return true;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws CommandeException {
		if (commLibelle() == null || commLibelle().length() == 0)
			throw new CommandeException(CommandeException.commLibelleManquant);
		if (commDate() == null)
			setCommDate(new NSTimestamp());
		if (commDateCreation() == null)
			setCommDateCreation(new NSTimestamp());

		if (devise() == null)
			throw new CommandeException(CommandeException.deviseManquant);
		if (exercice() == null)
			throw new CommandeException(CommandeException.exerciceManquant);
		if (fournisseur() == null)
			throw new CommandeException(CommandeException.fournisseurManquant);
		if (utilisateur() == null)
			throw new CommandeException(CommandeException.utilisateurManquant);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		//if (commReference()==null || commReference().length()==0)
		//	throw new CommandeException(CommandeException.commReferenceManquant);

		if (typeEtat() == null)
			setTypeEtatRelationship(typeEtatSuivantEngagements());
		if (typeEtatImprimable() == null)
			setTypeEtatImprimableRelationship(FinderTypeEtat.getTypeEtat(editingContext(), EOCommande.ETAT_NON_IMPRIMABLE));

		if (typeEtat() == null)
			throw new CommandeException(CommandeException.typeEtatManquant);
		if (!typeEtats().containsObject(typeEtat().tyetLibelle()))
			throw new CommandeException(CommandeException.typeEtatIncompatible);

		if (typeEtatImprimable() == null)
			throw new CommandeException(CommandeException.typeEtatImprimableManquant);
		// tester qu'il y a bien des articles associes a cette commande
		if (articles() == null || articles().count() <= 0)
			throw new CommandeException(CommandeException.articlesManquant);

		// verifier coherence type etat avec les engagements
		if (typeEtat().tyetLibelle().equals(EOCommande.ETAT_ANNULEE)) {
			if (commandeEngagements().count() > 0)
				throw new CommandeException(CommandeException.commandeAnnuleeAvecEngagements);
		}
		else {
			if (!typeEtat().equals(typeEtatSuivantEngagements())) {
				//Rod 25/11/09 : modif pour coller aux templates velocity
				//				setTypeEtat(typeEtatSuivantEngagements());
				setTypeEtatRelationship(typeEtatSuivantEngagements());
			}

			if (!typeEtat().equals(typeEtatSuivantEngagements()))
				throw new CommandeException(CommandeException.typeEtatIncoherent);
		}

		if (totalHt() == null || totalTtc() == null)
			throw new CommandeException(CommandeException.articlesManquant);
		if (totalHt().floatValue() > totalTtc().floatValue())
			throw new CommandeException(CommandeException.htTtcIncoherent);

		if (!isArticlesCoherent())
			throw new CommandeException(CommandeException.articlesIncoherent);
	}

	public boolean isArticlesCoherent() {
		if (articles() == null || articles().count() == 0)
			return true;

		EOTypeAchat typeAchat = null;
		EOAttribution attribution = null;

		for (int i = 0; i < articles().count(); i++) {
			EOArticle article = (EOArticle) articles().objectAtIndex(i);

			if (i == 0) {
				typeAchat = article.typeAchat();
				attribution = article.attribution();
				continue;
			}

			if (attribution == null && article.attribution() != null)
				return false;
			if (attribution != null && article.attribution() == null)
				return false;
			if (attribution != null && article.attribution() != null && !attribution.equals(article.attribution()))
				return false;

			if (typeAchat == null && article.typeAchat() != null)
				return false;
			if (typeAchat != null && article.typeAchat() == null)
				return false;
			if (typeAchat != null && article.typeAchat() != null && !typeAchat.equals(article.typeAchat()))
				return false;
		}

		return true;
	}

	public EOTypeEtat typeEtatSuivantEngagements() {
		if (typeEtat() != null && typeEtat().tyetLibelle().equals(EOCommande.ETAT_ANNULEE))
			return typeEtat();

		// si pas d'engagement c'est une pre-commande (on oublie le test annulee dans cette methode)
		if (commandeEngagements().count() == 0)
			return FinderTypeEtat.getTypeEtat(editingContext(), EOCommande.ETAT_PRECOMMANDE);

		// on teste l'etat par rapport a tous ses engagements
		BigDecimal ht = new BigDecimal(0.0), ttc = new BigDecimal(0.0);
		BigDecimal reste = new BigDecimal(0.0), budgetaire = new BigDecimal(0.0);

		for (int i = 0; i < commandeEngagements().count(); i++) {
			ht = ht.add(((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget().engHtSaisie());
			ttc = ttc.add(((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget().engTtcSaisie());
			reste = reste.add(((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget().engMontantBudgetaireReste());
			budgetaire = budgetaire.add(((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget().engMontantBudgetaire());
		}

		// on verifie que la commande n'est pas partiellement engagee
		if (ht.compareTo(computeSumForKey(articles(), "artPrixTotalHt")) == -1 ||
				ttc.compareTo(computeSumForKey(articles(), "artPrixTotalTtc")) == -1)
			return FinderTypeEtat.getTypeEtat(editingContext(), EOCommande.ETAT_PARTIELLEMENT_ENGAGEE);

		// si totalement engagee, on regarde le reste
		if (reste.floatValue() == 0.0)
			return FinderTypeEtat.getTypeEtat(editingContext(), EOCommande.ETAT_SOLDEE);
		if (reste.floatValue() != budgetaire.floatValue())
			return FinderTypeEtat.getTypeEtat(editingContext(), EOCommande.ETAT_PARTIELLEMENT_SOLDEE);

		return FinderTypeEtat.getTypeEtat(editingContext(), EOCommande.ETAT_ENGAGEE);
	}

	private BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	public static final NSArray typeEtats() {
		return new NSArray(new String[] {
				ETAT_PRECOMMANDE, ETAT_PARTIELLEMENT_ENGAGEE, ETAT_ENGAGEE, ETAT_PARTIELLEMENT_SOLDEE,
				ETAT_SOLDEE, ETAT_ANNULEE
		});
	}

	public EOCommandeBudget containsSource(EOBudgetExecCredit source) {
		return containsSource(source.organ(), source.typeCredit());
	}

	public EOCommandeBudget containsSource(EOSource source) {
		return containsSource(source.organ(), source.typeCredit());
	}

	public EOCommandeBudget containsSource(EOOrgan organ, EOTypeCredit typeCredit) {
		NSArray commandeBudgets = commandeBudgets();

		if (commandeBudgets != null && commandeBudgets.count() > 0) {
			NSMutableArray args = new NSMutableArray();
			args.addObject(organ);
			args.addObject(typeCredit);
			EOQualifier qualSources = EOQualifier.qualifierWithQualifierFormat("organ=%@ and typeCredit=%@", args);
			NSArray array = EOQualifier.filteredArrayWithQualifier(commandeBudgets, qualSources);

			if (array != null && array.count() > 0)
				return (EOCommandeBudget) array.objectAtIndex(0);
		}

		return null;
	}

	public boolean isHorsMarche() {
		boolean isHorsMarche = false;
		NSArray lesArticles = articles();
		if (lesArticles != null && lesArticles.count() > 0) {
			EOArticle unArticle = (EOArticle) lesArticles.lastObject();
			if (unArticle.typeAchat() != null) {
				isHorsMarche = true;
			}
		}

		return isHorsMarche;
	}

	public boolean isSurUnMarche() {
		boolean isSurUnMarche = false;
		NSArray lesArticles = articles();
		if (lesArticles != null && lesArticles.count() > 0) {
			EOArticle unArticle = (EOArticle) lesArticles.lastObject();
			if (unArticle.typeAchat() == null && unArticle.attribution() != null) {
				isSurUnMarche = true;
			}
		}

		return isSurUnMarche;
	}

	public boolean isSurUnMarcheCatalogue() {
		if (!isSurUnMarche())
			return false;
		if (articles() == null || articles().count() == 0)
			return false;

		EOAttribution attribution = ((EOArticle) articles().objectAtIndex(0)).attribution();
		if (attribution == null)
			return false;

		if (attribution.lot().isCatalogue())
			return true;
		return false;
	}

	public String rechercheGlobale() {
		String rechercheGlobale = "";

		rechercheGlobale += commNumero() + " ";
		rechercheGlobale += commReference() + " ";
		rechercheGlobale += fournisseur().fouCode() + " ";
		rechercheGlobale += fournisseur().fouNom() + " ";
		rechercheGlobale += commLibelle() + " ";

		for (int i = 0; i < commandeEngagements().count(); i++) {
			EOEngagementBudget engage = ((EOCommandeEngagement) commandeEngagements().objectAtIndex(i)).engagementBudget();
			rechercheGlobale += engage.engNumero() + " ";

			//			for (int j = 0; j < engage.depenseBudgets().count(); j++) {
			//				EODepenseBudget depense = (EODepenseBudget) engage.depenseBudgets().objectAtIndex(j);
			//				rechercheGlobale += depense.depensePapier().dppNumeroFacture() + " ";
			//			}
			for (int j = 0; j < engage.currentDepenseBudgets().count(); j++) {
				_IDepenseBudget depense = (_IDepenseBudget) engage.currentDepenseBudgets().objectAtIndex(j);
				rechercheGlobale += depense.depensePapier().dppNumeroFacture() + " ";
			}
		}

		return rechercheGlobale;
	}

	public NSArray repartitionCodeExer() {
		NSMutableArray tableau = new NSMutableArray();

		if (articles() == null)
			return tableau;

		for (int i = 0; i < articles().count(); i++) {
			EOArticle article = (EOArticle) articles().objectAtIndex(i);

			EOCodeExer codeExerNiveau2 = FinderCodeExer.getCodeExerNiveau2(article.codeExer());

			if (codeExerNiveau2 == null)
				continue;

			NSArray resultat = EOQualifier.filteredArrayWithQualifier(tableau, EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(codeExerNiveau2)));
			NSMutableDictionary ligne;

			if (resultat.count() == 0) {
				ligne = new NSMutableDictionary();
				ligne.setObjectForKey(codeExerNiveau2, "codeExer");
				ligne.setObjectForKey(new BigDecimal(0.0), "ht");
				ligne.setObjectForKey(new BigDecimal(0.0), "ttc");
				tableau.addObject(ligne);
			}
			else
				ligne = (NSMutableDictionary) resultat.objectAtIndex(0);

			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ht")).add(article.artPrixTotalHt()), "ht");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttc")).add(article.artPrixTotalTtc()), "ttc");
		}

		return tableau;
	}

	public void reconstruireTableaux() {
		reconstruireTableauHorsMarche();
		reconstruireTableauMarche();

		setRepartitionAutomatiquePossible();
	}

	public void reconstruireTableauHorsMarche() {
		tableauHorsMarche = new NSMutableArray();

		if (articles() == null)
			return;

		for (int i = 0; i < articles().count(); i++) {
			EOArticle article = (EOArticle) articles().objectAtIndex(i);

			EOCodeExer codeExerNiveau2 = FinderCodeExer.getCodeExerNiveau2(article.codeExer());

			if (codeExerNiveau2 == null || article.typeAchat() == null)
				continue;

			NSMutableArray arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(codeExerNiveau2)));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@", new NSArray(article.typeAchat())));
			NSArray resultat = EOQualifier.filteredArrayWithQualifier(tableauHorsMarche, new EOAndQualifier(arrayQualifier));

			NSMutableDictionary<String, Object> ligne;

			if (resultat.count() == 0) {
				ligne = new NSMutableDictionary<String, Object>();
				ligne.setObjectForKey(codeExerNiveau2, "codeExer");
				ligne.setObjectForKey(article.typeAchat(), "typeAchat");
				ligne.setObjectForKey(new BigDecimal(0.0), "ht");
				ligne.setObjectForKey(new BigDecimal(0.0), "ttc");
				ligne.setObjectForKey(new BigDecimal(0.0), "htRestant");
				ligne.setObjectForKey(new BigDecimal(0.0), "ttcRestant");
				tableauHorsMarche.addObject(ligne);
			}
			else
				ligne = (NSMutableDictionary) resultat.objectAtIndex(0);

			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ht")).add(article.artPrixTotalHt()), "ht");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("htRestant")).add(article.artPrixTotalHt()), "htRestant");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttc")).add(article.artPrixTotalTtc()), "ttc");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttcRestant")).add(article.artPrixTotalTtc()), "ttcRestant");
		}

		majCommandeBudgetsHorsMarche();
	}

	public void reconstruireTableauMarche() {
		tableauMarche = new NSMutableArray();

		if (articles() == null)
			return;

		for (int i = 0; i < articles().count(); i++) {
			EOArticle article = (EOArticle) articles().objectAtIndex(i);

			if (article.attribution() == null)
				continue;

			NSArray resultat = EOQualifier.filteredArrayWithQualifier(tableauMarche,
					EOQualifier.qualifierWithQualifierFormat("attribution=%@", new NSArray(article.attribution())));

			NSMutableDictionary ligne;

			if (resultat.count() == 0) {
				ligne = new NSMutableDictionary();
				ligne.setObjectForKey(article.attribution(), "attribution");
				ligne.setObjectForKey(new BigDecimal(0.0), "ht");
				ligne.setObjectForKey(new BigDecimal(0.0), "ttc");
				ligne.setObjectForKey(new BigDecimal(0.0), "htRestant");
				ligne.setObjectForKey(new BigDecimal(0.0), "ttcRestant");
				tableauMarche.addObject(ligne);
			}
			else
				ligne = (NSMutableDictionary) resultat.objectAtIndex(0);

			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ht")).add(article.artPrixTotalHt()), "ht");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttc")).add(article.artPrixTotalTtc()), "ttc");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("htRestant")).add(article.artPrixTotalHt()), "htRestant");
			ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttcRestant")).add(article.artPrixTotalTtc()), "ttcRestant");
		}

		majCommandeBudgetsMarche();
	}

	public NSMutableArray<NSDictionary<String, Object>> tableauHorsMarche() {
		reconstruireTableauHorsMarche();
		return tableauHorsMarche;
	}

	public NSMutableArray<? extends NSKeyValueCoding> tableauMarche() {
		reconstruireTableauMarche();
		return tableauMarche;
	}

	private void majCommandeBudgetsHorsMarche() {
		FactoryCommandeControleHorsMarche factoryCommandeControleHorsMarche = new FactoryCommandeControleHorsMarche();

		// on supprime les commandeCtrl qui n'ont plus lieu d'exister
		NSArray arrayCtrl = distinctCommandeBudgetsCodeExerTypeAchat();

		NSMutableArray arrayCtrlHorsMarche = new NSMutableArray();
		for (int i = 0; i < commandeBudgets().count(); i++)
			arrayCtrlHorsMarche.addObjectsFromArray((NSArray) ((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).
					valueForKeyPath(EOCommandeBudget.COMMANDE_CONTROLE_HORS_MARCHES_KEY));

		for (int i = 0; i < arrayCtrl.count(); i++) {
			ObjetHorsMarche horsMarche = (ObjetHorsMarche) arrayCtrl.objectAtIndex(i);

			NSMutableArray lesQualifiers = new NSMutableArray();
			lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@", new NSArray(horsMarche.typeAchat())));
			lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(horsMarche.codeExer())));
			NSArray resultat = EOQualifier.filteredArrayWithQualifier(tableauHorsMarche, new EOAndQualifier(lesQualifiers));

			if (resultat.count() == 0) {
				NSArray array = EOQualifier.filteredArrayWithQualifier(arrayCtrlHorsMarche, new EOAndQualifier(lesQualifiers));

				for (int j = 0; j < array.count(); j++) {
					EOCommandeControleHorsMarche commandeControleHorsMarche = (EOCommandeControleHorsMarche) array.objectAtIndex(j);
					factoryCommandeControleHorsMarche.supprimer(commandeControleHorsMarche.editingContext(), commandeControleHorsMarche);
				}
			}
		}

		if (commandeBudgets().count() == 0)
			return;

		for (int i = 0; i < tableauHorsMarche.count(); i++) {
			NSMutableDictionary ligne = (NSMutableDictionary) tableauHorsMarche.objectAtIndex(i);

			for (int j = 0; j < commandeBudgets().count(); j++) {
				EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(j);

				NSMutableArray lesQualifiers = new NSMutableArray();
				lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@",
						new NSArray((EOTypeAchat) ligne.objectForKey("typeAchat"))));
				lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@",
						new NSArray((EOCodeExer) ligne.objectForKey("codeExer"))));
				NSArray resultat = EOQualifier.filteredArrayWithQualifier((NSArray) commandeBudget.
						valueForKeyPath(EOCommandeBudget.COMMANDE_CONTROLE_HORS_MARCHES_KEY), new EOAndQualifier(lesQualifiers));

				if (resultat.count() == 0)
					factoryCommandeControleHorsMarche.creer(commandeBudget.editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
							new BigDecimal(0.0), new BigDecimal(0.0), (EOCodeExer) ligne.objectForKey("codeExer"),
							(EOTypeAchat) ligne.objectForKey("typeAchat"), commandeBudget.exercice(), commandeBudget);
				else {
					for (int k = 0; k < resultat.count(); k++) {
						EOCommandeControleHorsMarche ctrl = (EOCommandeControleHorsMarche) resultat.objectAtIndex(k);
						ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("htRestant")).subtract(ctrl.chomHtSaisie()), "htRestant");
						ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttcRestant")).subtract(ctrl.chomTtcSaisie()), "ttcRestant");
					}
				}
			}
		}
	}

	private void majCommandeBudgetsMarche() {
		FactoryCommandeControleMarche factoryCommandeControleMarche = new FactoryCommandeControleMarche();

		// on supprime les commandeCtrl qui n'ont plus lieu d'exister
		NSArray arrayCtrl = distinctCommandeBudgetsAttribution();

		NSMutableArray arrayCtrlMarche = new NSMutableArray();
		for (int i = 0; i < commandeBudgets().count(); i++)
			arrayCtrlMarche.addObjectsFromArray((NSArray) ((EOCommandeBudget) commandeBudgets().objectAtIndex(i)).
					valueForKeyPath(EOCommandeBudget.COMMANDE_CONTROLE_MARCHES_KEY));

		for (int i = 0; i < arrayCtrl.count(); i++) {
			EOAttribution attribution = (EOAttribution) arrayCtrl.objectAtIndex(i);

			NSArray resultat = EOQualifier.filteredArrayWithQualifier(tableauMarche,
					EOQualifier.qualifierWithQualifierFormat("attribution=%@", new NSArray(attribution)));

			if (resultat.count() == 0) {
				NSArray array = EOQualifier.filteredArrayWithQualifier(arrayCtrlMarche,
						EOQualifier.qualifierWithQualifierFormat("attribution=%@", new NSArray(attribution)));

				for (int j = 0; j < array.count(); j++) {
					EOCommandeControleMarche commandeControleMarche = (EOCommandeControleMarche) array.objectAtIndex(j);
					factoryCommandeControleMarche.supprimer(commandeControleMarche.editingContext(), commandeControleMarche);
				}
			}
		}

		if (commandeBudgets().count() == 0)
			return;

		for (int i = 0; i < tableauMarche.count(); i++) {
			NSMutableDictionary ligne = (NSMutableDictionary) tableauMarche.objectAtIndex(i);

			for (int j = 0; j < commandeBudgets().count(); j++) {
				EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets().objectAtIndex(j);

				NSArray resultat = EOQualifier.filteredArrayWithQualifier((NSArray) commandeBudget.
						valueForKeyPath(EOCommandeBudget.COMMANDE_CONTROLE_MARCHES_KEY),
						EOQualifier.qualifierWithQualifierFormat("attribution=%@",
								new NSArray((EOAttribution) ligne.objectForKey("attribution"))));

				if (resultat.count() == 0)
					factoryCommandeControleMarche.creer(commandeBudget.editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
							new BigDecimal(0.0), new BigDecimal(0.0), (EOAttribution) ligne.objectForKey("attribution"),
							commandeBudget.exercice(), commandeBudget);
				else {
					for (int k = 0; k < resultat.count(); k++) {
						EOCommandeControleMarche ctrl = (EOCommandeControleMarche) resultat.objectAtIndex(k);
						ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("htRestant")).subtract(ctrl.cmarHtSaisie()), "htRestant");
						ligne.setObjectForKey(((BigDecimal) ligne.objectForKey("ttcRestant")).subtract(ctrl.cmarTtcSaisie()), "ttcRestant");
					}
				}
			}
		}
	}

	/*
	 * public void creerCommandeCtrlAPartirTableaux(EOCommandeBudget commandeBudget) { if (tableauHorsMarche!=null && tableauHorsMarche.count()>0) {
	 * FactoryCommandeControleHorsMarche factoryCommandeControleHorsMarche=new FactoryCommandeControleHorsMarche(); for (int i=0;
	 * i<tableauHorsMarche.count(); i++) { NSDictionary ligne=(NSDictionary)tableauHorsMarche.objectAtIndex(i);
	 * factoryCommandeControleHorsMarche.creer(commandeBudget.editingContext(), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), new
	 * BigDecimal(0.0), (EOCodeExer)ligne.objectForKey("codeExer"), (EOTypeAchat)ligne.objectForKey("typeAchat"), commandeBudget.exercice(),
	 * commandeBudget); } }
	 *
	 * if (tableauMarche!=null && tableauMarche.count()>0) { FactoryCommandeControleMarche factoryCommandeControleMarche=new
	 * FactoryCommandeControleMarche(); for (int i=0; i<tableauMarche.count(); i++) { NSDictionary ligne=(NSDictionary)tableauMarche.objectAtIndex(i);
	 * factoryCommandeControleMarche.creer(commandeBudget.editingContext(), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), new
	 * BigDecimal(0.0), (EOAttribution)ligne.objectForKey("attribution"), commandeBudget.exercice(), commandeBudget); } } }
	 */
	private NSArray distinctCommandeBudgetsCodeExerTypeAchat() {
		NSMutableArray array = new NSMutableArray();
		NSArray cmdeBudgets = commandeBudgets();
		if (cmdeBudgets.count() == 0)
			return array;

		NSArray arrayCtrl = new NSArray();
		EOCommandeBudget uneCommandeBudget = null;
		for (int i = 0; i < cmdeBudgets.count(); i++) {
			uneCommandeBudget = (EOCommandeBudget) cmdeBudgets.objectAtIndex(i);
			arrayCtrl = arrayCtrl.arrayByAddingObjectsFromArray(uneCommandeBudget.commandeControleHorsMarches());
		}

		if (arrayCtrl.count() == 0)
			return array;

		for (int i = 0; i < arrayCtrl.count(); i++) {
			EOCommandeControleHorsMarche cde = (EOCommandeControleHorsMarche) arrayCtrl.objectAtIndex(i);

			NSMutableArray lesQualifiers = new NSMutableArray();
			// TODO Exception si typeAchat() null
			// EGE Comment ce cas peut-il se produire ?
			lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.TYPE_ACHAT_KEY + "=%@",
					new NSArray(cde.typeAchat())));
			lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.CODE_EXER_KEY + "=%@",
					new NSArray(cde.codeExer())));

			NSArray resultat = EOQualifier.filteredArrayWithQualifier(array, new EOAndQualifier(lesQualifiers));
			if (resultat.count() == 0)
				array.addObject(new ObjetHorsMarche(cde.codeExer(), cde.typeAchat()));
		}

		return array;
	}

	private NSArray distinctCommandeBudgetsAttribution() {
		NSMutableArray array = new NSMutableArray();
		NSArray cmdeBudgets = commandeBudgets();
		if (cmdeBudgets.count() == 0)
			return array;

		NSArray arrayCtrl = new NSArray();
		EOCommandeBudget uneCommandeBudget = null;
		for (int i = 0; i < cmdeBudgets.count(); i++) {
			uneCommandeBudget = (EOCommandeBudget) cmdeBudgets.objectAtIndex(i);
			arrayCtrl = arrayCtrl.arrayByAddingObjectsFromArray(uneCommandeBudget.commandeControleMarches());
		}

		if (arrayCtrl.count() == 0)
			return array;

		EOCommandeControleMarche cde = (EOCommandeControleMarche) arrayCtrl.objectAtIndex(0);
		array.addObject(cde.attribution());

		return array;
	}

	public EOAttribution attribution() {
		EOAttribution attribution = null;
		NSArray articles = articles();
		if (articles != null && articles.count() > 0) {
			EOArticle firstArticle = (EOArticle) articles.objectAtIndex(0);
			attribution = firstArticle.attribution();
		}

		if (attribution == null)
			return monAttribution;

		return attribution;
	}

	public EOTypeAchat typeAchat() {
		EOTypeAchat typeAchat = null;
		NSArray articles = articles();
		if (articles != null && articles.count() > 0) {
			EOArticle firstArticle = (EOArticle) articles.objectAtIndex(0);
			typeAchat = firstArticle.typeAchat();
		}
		return typeAchat;
	}

	public String libelleCourt() {
		String libelleCourt = commLibelle();

		if (libelleCourt != null && libelleCourt.length() > 25)
			libelleCourt = libelleCourt.substring(0, 25);

		return libelleCourt;
	}

	public String referenceCourt() {
		String libelleCourt = commReference();

		if (libelleCourt != null && libelleCourt.length() > 25)
			libelleCourt = libelleCourt.substring(0, 10);

		return libelleCourt;
	}

	public String type() {
		String type = null;

		if (typeAchat() != null) {
			type = typeAchat().typaLibelle();
		}
		else if (attribution() != null) {
			type = "MARCHE";
		}
		else {
			type = "???";
		}
		return type;
	}

	/**
	 * Recherche les codes de nomenclature d'une commande.<BR>
	 *
	 * @return un NSArray contenant les EOCodeExer
	 */

	public NSArray codesExer() {
		NSMutableArray codesExer = new NSMutableArray();
		NSArray articles = articles();

		if (articles != null && articles.count() > 0) {
			Enumeration enumArticles = articles.objectEnumerator();
			while (enumArticles.hasMoreElements()) {
				EOArticle unArticle = (EOArticle) enumArticles.nextElement();
				EOCodeExer unCode = unArticle.codeExer();
				if (unCode != null && codesExer.containsObject(unCode) == false) {
					codesExer.addObject(unCode);
				}
			}
		}
		return codesExer;
	}

	public boolean isMAPA() {
		boolean isMAPA = false;
		if (isHorsMarche() == true && typeAchat() != null && EOTypeAchat.MARCHE_NON_FORMALISE.equals(typeAchat().typaLibelle())) {
			NSArray articles = articles();
			if (articles != null && articles.count() > 0) {
				NSArray lesSeuilsMAPA = FinderCtrlSeuilMAPA.getCtrlSeuilMAPAPourCommande(this.editingContext(), this);
				NSArray lesCodesExer = codesExer();
				Enumeration enumCodesExer = lesCodesExer.objectEnumerator();
				NSMutableArray qualifiers = new NSMutableArray();
				while (enumCodesExer.hasMoreElements()) {
					EOCodeExer unCodeExer = (EOCodeExer) enumCodesExer.nextElement();
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(unCodeExer));
					qualifiers.addObject(qual);
				}
				EOOrQualifier qualSeuils = new EOOrQualifier(qualifiers);
				lesSeuilsMAPA = EOQualifier.filteredArrayWithQualifier(lesSeuilsMAPA, qualSeuils);
				Enumeration enumLesSeuilsMAPA = lesSeuilsMAPA.objectEnumerator();
				while (enumLesSeuilsMAPA.hasMoreElements()) {
					EOCtrlSeuilMAPA unSeuil = (EOCtrlSeuilMAPA) enumLesSeuilsMAPA.nextElement();
					BigDecimal unSeuilMin = unSeuil.seuilMin();
					BigDecimal unSeuilMontantEngHt = unSeuil.montantEngageHt();

					EOQualifier qualCodeExer = EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(unSeuil.codeExer()));
					NSArray arrayTmp = EOQualifier.filteredArrayWithQualifier(articles(), qualCodeExer);
					BigDecimal totalArticlesHt = (BigDecimal) arrayTmp.valueForKeyPath("@sum.artPrixTotalHt");
					BigDecimal totalCmdeEngHt = new BigDecimal(0.0);
					NSArray arrayTmp1 = commandeEngagements();
					if (arrayTmp1 != null && arrayTmp1.count() > 0) {
						for (int i = 0; i < arrayTmp1.count(); i++) {
							EOEngagementBudget engBud = ((EOCommandeEngagement) arrayTmp1.objectAtIndex(i)).engagementBudget();
							NSArray engCtrlHom = engBud.engagementControleHorsMarches();
							engCtrlHom = EOQualifier.filteredArrayWithQualifier(engCtrlHom, qualCodeExer);
							totalCmdeEngHt = totalCmdeEngHt.add((BigDecimal) engCtrlHom.valueForKeyPath("@sum.ehomHtSaisie"));
						}
					}
					if (totalArticlesHt.floatValue() + unSeuilMontantEngHt.floatValue() - totalCmdeEngHt.floatValue() > unSeuilMin.floatValue()) {
						isMAPA = true;
						break;
					}
				}
			}
		}
		return isMAPA;
	}

	public NSArray<EOCtrlSeuilMAPA> seuilsMAPADepasses() {
		NSMutableArray seuilsDepasses = new NSMutableArray();
		if (isHorsMarche() == true) {
			NSArray articles = articles();
			if (articles != null && articles.count() > 0) {
				NSArray lesSeuilsMAPA = FinderCtrlSeuilMAPA.getCtrlSeuilMAPAPourCommande(this.editingContext(), this);
				NSArray lesCodesExer = codesExer();
				Enumeration enumCodesExer = lesCodesExer.objectEnumerator();
				NSMutableArray qualifiers = new NSMutableArray();
				while (enumCodesExer.hasMoreElements()) {
					EOCodeExer unCodeExer = (EOCodeExer) enumCodesExer.nextElement();
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(unCodeExer));
					qualifiers.addObject(qual);
				}
				EOOrQualifier qualSeuils = new EOOrQualifier(qualifiers);
				lesSeuilsMAPA = EOQualifier.filteredArrayWithQualifier(lesSeuilsMAPA, qualSeuils);
				Enumeration enumLesSeuilsMAPA = lesSeuilsMAPA.objectEnumerator();
				while (enumLesSeuilsMAPA.hasMoreElements()) {
					EOCtrlSeuilMAPA unSeuil = (EOCtrlSeuilMAPA) enumLesSeuilsMAPA.nextElement();
					BigDecimal unSeuilMin = unSeuil.seuilMin();
					BigDecimal unSeuilMontantEngHt = unSeuil.montantEngageHt();

					EOQualifier qualCodeExer = EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(unSeuil.codeExer()));
					NSArray arrayTmp = EOQualifier.filteredArrayWithQualifier(articles(), qualCodeExer);
					BigDecimal totalArticlesHt = (BigDecimal) arrayTmp.valueForKeyPath("@sum.artPrixTotalHt");
					BigDecimal totalCmdeEngHt = new BigDecimal(0.0);
					NSArray arrayTmp1 = commandeEngagements();
					if (arrayTmp1 != null && arrayTmp1.count() > 0) {
						for (int i = 0; i < arrayTmp1.count(); i++) {
							EOEngagementBudget engBud = ((EOCommandeEngagement) arrayTmp1.objectAtIndex(i)).engagementBudget();
							NSArray engCtrlHom = engBud.engagementControleHorsMarches();
							engCtrlHom = EOQualifier.filteredArrayWithQualifier(engCtrlHom, qualCodeExer);
							totalCmdeEngHt = totalCmdeEngHt.add((BigDecimal) engCtrlHom.valueForKeyPath("@sum.ehomHtSaisie"));
						}
					}
					if (totalArticlesHt.floatValue() + unSeuilMontantEngHt.floatValue() - totalCmdeEngHt.floatValue() > unSeuilMin.floatValue()) {
						seuilsDepasses.addObject(unSeuil);
					}
				}
			}
		}
		return seuilsDepasses;
	}

	public boolean isControleMaxDepasse() {
		if (isHorsMarche() == false || typeAchat() == null || !EOTypeAchat.MARCHE_NON_FORMALISE.equals(typeAchat().typaLibelle()))
			return false;
		if (articles() == null || articles().count() == 0)
			return false;

		NSArray lesCodesExer = codesExer();
		for (int i = 0; i < lesCodesExer.count(); i++) {
			EOCodeExer codeExer = (EOCodeExer) lesCodesExer.objectAtIndex(i);

			if (codeExer.ceControle().floatValue() <= 0.0)
				continue;

			NSMutableDictionary mesBindings = new NSMutableDictionary();
			mesBindings.setObjectForKey(exercice(), "exercice");
			mesBindings.setObjectForKey(codeExer, "codeExer");
			NSArray lesSeuilsMAPA = FinderCtrlSeuilMAPA.getCtrlSeuilMAPAValides(this.editingContext(), mesBindings);

			if (lesSeuilsMAPA.count() != 1)
				return false;

			EOQualifier qualCodeExer = EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(codeExer));
			BigDecimal totalArticlesHt = (BigDecimal) EOQualifier.filteredArrayWithQualifier(articles(), qualCodeExer).valueForKeyPath("@sum.artPrixTotalHt");

			BigDecimal totalCmdeEngHt = new BigDecimal(0.0);
			if (commandeEngagements() != null && commandeEngagements().count() > 0) {
				for (int j = 0; j < commandeEngagements().count(); j++) {
					EOEngagementBudget engBud = ((EOCommandeEngagement) commandeEngagements().objectAtIndex(j)).engagementBudget();
					NSArray engCtrlHom = engBud.engagementControleHorsMarches();
					engCtrlHom = EOQualifier.filteredArrayWithQualifier(engCtrlHom, qualCodeExer);
					totalCmdeEngHt = totalCmdeEngHt.add((BigDecimal) engCtrlHom.valueForKeyPath("@sum.ehomHtSaisie"));
				}
			}

			if (codeExer.ceControle().floatValue() < ((EOCtrlSeuilMAPA) lesSeuilsMAPA.objectAtIndex(0)).montantEngageHt().floatValue() +
					totalArticlesHt.floatValue() - totalCmdeEngHt.floatValue())
				return true;
		}

		return false;
	}

	public boolean isPrecommande() {
		boolean isPrecommande = false;
		EOTypeEtat type = typeEtat();
		if (type == null || type.tyetLibelle().equals(ETAT_PRECOMMANDE)) {
			NSArray budgets = commandeBudgets();
			if (budgets == null || budgets.count() == 0) {
				isPrecommande = true;
			}
		}
		return isPrecommande;
	}

	public boolean isInformable() {
		boolean isInformable = false;
		EOTypeEtat type = typeEtat();
		if (isPrecommande() ||
				type.tyetLibelle().equals(ETAT_PARTIELLEMENT_ENGAGEE) ||
				type.tyetLibelle().equals(ETAT_ENGAGEE)) {
			isInformable = true;
		}
		return isInformable;
	}

	public void rafraichir(EOEditingContext edc) {
		NSMutableArray gids = new NSMutableArray();
		EOGlobalID gid = edc.globalIDForObject(this);
		gids.addObject(gid);
		EOTypeEtat typeEtat = typeEtat();
		gids.addObject(edc.globalIDForObject(typeEtat));
		EOTypeEtat typeEtatImprimable = typeEtatImprimable();
		gids.addObject(edc.globalIDForObject(typeEtatImprimable));
		EOExercice exercice = exercice();
		gids.addObject(edc.globalIDForObject(exercice));
		EOFournisseur fournisseur = fournisseur();
		gids.addObject(edc.globalIDForObject(fournisseur));

		NSArray articles = this.articles();
		for (int i = 0; i < articles.count(); i++) {
			EOArticle unArticle = (EOArticle) articles.objectAtIndex(i);
			gids.addObject(edc.globalIDForObject(unArticle));
		}
		NSArray commandeBudgets = this.commandeBudgets();
		for (int i = 0; i < commandeBudgets.count(); i++) {
			EOCommandeBudget uneCommandeBudget = (EOCommandeBudget) commandeBudgets.objectAtIndex(i);
			gids.addObject(edc.globalIDForObject(uneCommandeBudget));
			NSArray commandeControleActions = uneCommandeBudget.commandeControleActions();
			for (int j = 0; j < commandeControleActions.count(); j++) {
				EOCommandeControleAction uneCommandeControleAction = (EOCommandeControleAction) commandeControleActions.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneCommandeControleAction));
			}
			NSArray commandeControleAnalytiques = uneCommandeBudget.commandeControleAnalytiques();
			for (int j = 0; j < commandeControleAnalytiques.count(); j++) {
				EOCommandeControleAnalytique uneCommandeControleAnalytique = (EOCommandeControleAnalytique) commandeControleAnalytiques.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneCommandeControleAnalytique));
			}
			NSArray commandeControleConventions = uneCommandeBudget.commandeControleConventions();
			for (int j = 0; j < commandeControleConventions.count(); j++) {
				EOCommandeControleConvention uneCommandeControleConvention = (EOCommandeControleConvention) commandeControleConventions.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneCommandeControleConvention));
			}
			NSArray commandeControleHorsMarches = uneCommandeBudget.commandeControleHorsMarches();
			for (int j = 0; j < commandeControleHorsMarches.count(); j++) {
				EOCommandeControleHorsMarche uneCommandeControleHorsMarche = (EOCommandeControleHorsMarche) commandeControleHorsMarches.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneCommandeControleHorsMarche));
			}
			NSArray commandeControleMarches = uneCommandeBudget.commandeControleMarches();
			for (int j = 0; j < commandeControleMarches.count(); j++) {
				EOCommandeControleMarche uneCommandeControleMarche = (EOCommandeControleMarche) commandeControleMarches.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneCommandeControleMarche));
			}
			NSArray commandeControlePlanComptables = uneCommandeBudget.commandeControlePlanComptables();
			for (int j = 0; j < commandeControlePlanComptables.count(); j++) {
				EOCommandeControlePlanComptable uneCommandeControlePlanComptable = (EOCommandeControlePlanComptable) commandeControlePlanComptables.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneCommandeControlePlanComptable));
			}
		}
		NSArray commandeEngagements = this.commandeEngagements();
		for (int i = 0; i < commandeEngagements.count(); i++) {
			EOCommandeEngagement uneCommandeEngagement = (EOCommandeEngagement) commandeEngagements.objectAtIndex(i);
			gids.addObject(edc.globalIDForObject(uneCommandeEngagement));
			EOEngagementBudget unEngagementBudget = uneCommandeEngagement.engagementBudget();
			gids.addObject(edc.globalIDForObject(unEngagementBudget));
			NSArray depenseBudgets = unEngagementBudget.depenseBudgets();
			for (int k = 0; k < depenseBudgets.count(); k++) {
				EODepenseBudget uneDepenseBudget = (EODepenseBudget) depenseBudgets.objectAtIndex(k);
				gids.addObject(edc.globalIDForObject(uneDepenseBudget));
				NSArray depenseControleActions = uneDepenseBudget.depenseControleActions();
				for (int j = 0; j < depenseControleActions.count(); j++) {
					EODepenseControleAction uneDepenseControleAction = (EODepenseControleAction) depenseControleActions.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleAction));
				}
				NSArray depenseControleAnalytiques = uneDepenseBudget.depenseControleAnalytiques();
				for (int j = 0; j < depenseControleAnalytiques.count(); j++) {
					EODepenseControleAnalytique uneDepenseControleAnalytique = (EODepenseControleAnalytique) depenseControleAnalytiques.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleAnalytique));
				}
				NSArray depenseControleConventions = uneDepenseBudget.depenseControleConventions();
				for (int j = 0; j < depenseControleConventions.count(); j++) {
					EODepenseControleConvention uneDepenseControleConvention = (EODepenseControleConvention) depenseControleConventions.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleConvention));
				}
				NSArray depenseControleHorsMarches = uneDepenseBudget.depenseControleHorsMarches();
				for (int j = 0; j < depenseControleHorsMarches.count(); j++) {
					EODepenseControleHorsMarche uneDepenseControleHorsMarche = (EODepenseControleHorsMarche) depenseControleHorsMarches.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleHorsMarche));
				}
				NSArray depenseControleMarches = uneDepenseBudget.depenseControleMarches();
				for (int j = 0; j < depenseControleMarches.count(); j++) {
					EODepenseControleMarche uneDepenseControleMarche = (EODepenseControleMarche) depenseControleMarches.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleMarche));
				}
				NSArray depenseControlePlanComptables = uneDepenseBudget.depenseControlePlanComptables();
				for (int j = 0; j < depenseControlePlanComptables.count(); j++) {
					EODepenseControlePlanComptable uneDepenseControlePlanComptable = (EODepenseControlePlanComptable) depenseControlePlanComptables.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControlePlanComptable));
				}
			}
			NSArray preDepenseBudgets = unEngagementBudget.preDepenseBudgets();
			for (int k = 0; k < preDepenseBudgets.count(); k++) {
				EOPreDepenseBudget uneDepenseBudget = (EOPreDepenseBudget) preDepenseBudgets.objectAtIndex(k);
				gids.addObject(edc.globalIDForObject(uneDepenseBudget));
				NSArray preDepenseControleActions = uneDepenseBudget.preDepenseControleActions();
				for (int j = 0; j < preDepenseControleActions.count(); j++) {
					EOPreDepenseControleAction uneDepenseControleAction = (EOPreDepenseControleAction) preDepenseControleActions.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleAction));
				}
				NSArray preDepenseControleAnalytiques = uneDepenseBudget.preDepenseControleAnalytiques();
				for (int j = 0; j < preDepenseControleAnalytiques.count(); j++) {
					EOPreDepenseControleAnalytique uneDepenseControleAnalytique = (EOPreDepenseControleAnalytique) preDepenseControleAnalytiques.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleAnalytique));
				}
				NSArray preDepenseControleConventions = uneDepenseBudget.preDepenseControleConventions();
				for (int j = 0; j < preDepenseControleConventions.count(); j++) {
					EOPreDepenseControleConvention uneDepenseControleConvention = (EOPreDepenseControleConvention) preDepenseControleConventions.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleConvention));
				}
				NSArray preDepenseControleHorsMarches = uneDepenseBudget.preDepenseControleHorsMarches();
				for (int j = 0; j < preDepenseControleHorsMarches.count(); j++) {
					EOPreDepenseControleHorsMarche uneDepenseControleHorsMarche = (EOPreDepenseControleHorsMarche) preDepenseControleHorsMarches.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleHorsMarche));
				}
				NSArray preDepenseControleMarches = uneDepenseBudget.preDepenseControleMarches();
				for (int j = 0; j < preDepenseControleMarches.count(); j++) {
					EOPreDepenseControleMarche uneDepenseControleMarche = (EOPreDepenseControleMarche) preDepenseControleMarches.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControleMarche));
				}
				NSArray preDepenseControlePlanComptables = uneDepenseBudget.preDepenseControlePlanComptables();
				for (int j = 0; j < preDepenseControlePlanComptables.count(); j++) {
					EOPreDepenseControlePlanComptable uneDepenseControlePlanComptable = (EOPreDepenseControlePlanComptable) preDepenseControlePlanComptables.objectAtIndex(j);
					gids.addObject(edc.globalIDForObject(uneDepenseControlePlanComptable));
				}
			}
			NSArray engagementControleActions = unEngagementBudget.engagementControleActions();
			for (int j = 0; j < engagementControleActions.count(); j++) {
				EOEngagementControleAction uneEngagementControleAction = (EOEngagementControleAction) engagementControleActions.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneEngagementControleAction));
			}
			NSArray engagementControleAnalytiques = unEngagementBudget.engagementControleAnalytiques();
			for (int j = 0; j < engagementControleAnalytiques.count(); j++) {
				EOEngagementControleAnalytique uneEngagementControleAnalytique = (EOEngagementControleAnalytique) engagementControleAnalytiques.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneEngagementControleAnalytique));
			}
			NSArray engagementControleConventions = unEngagementBudget.engagementControleConventions();
			for (int j = 0; j < engagementControleConventions.count(); j++) {
				EOEngagementControleConvention uneEngagementControleConvention = (EOEngagementControleConvention) engagementControleConventions.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneEngagementControleConvention));
			}
			NSArray engagementControleHorsMarches = unEngagementBudget.engagementControleHorsMarches();
			for (int j = 0; j < engagementControleHorsMarches.count(); j++) {
				EOEngagementControleHorsMarche uneEngagementControleHorsMarche = (EOEngagementControleHorsMarche) engagementControleHorsMarches.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneEngagementControleHorsMarche));
			}
			NSArray engagementControleMarches = unEngagementBudget.engagementControleMarches();
			for (int j = 0; j < engagementControleMarches.count(); j++) {
				EOEngagementControleMarche uneEngagementControleMarche = (EOEngagementControleMarche) engagementControleMarches.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneEngagementControleMarche));
			}
			NSArray engagementControlePlanComptables = unEngagementBudget.engagementControlePlanComptables();
			for (int j = 0; j < engagementControlePlanComptables.count(); j++) {
				EOEngagementControlePlanComptable uneEngagementControlePlanComptable = (EOEngagementControlePlanComptable) engagementControlePlanComptables.objectAtIndex(j);
				gids.addObject(edc.globalIDForObject(uneEngagementControlePlanComptable));
			}
		}
		edc.invalidateObjectsWithGlobalIDs(gids);
	}

    public List<EOArticle> articlesTriLibelle() {
    	List<EOArticle> articlesLocal = new ArrayList<EOArticle>();
    	if (articles() == null) {
    		return articlesLocal;
    	}
    	articlesLocal.addAll(articles());
    	Collections.sort(articlesLocal, new CommandeArticleComparator());
    	return articlesLocal;
    }

}
