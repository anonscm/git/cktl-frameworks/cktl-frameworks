/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.FinderGrhumParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderRib;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOFournisseur extends _EOFournisseur {
	public static final String LIBELLE_KEY = "libelle";
	public static final String ADRESSE_CONCAT_KEY = "adresseConcat";

	public static final String ETAT_VALIDE = "O";
	public static final String ETAT_INSTANCE_VALIDATION = "N";
	public static final String ETAT_ANNULE = "A";
	public static final String ETAT_ETRANGER = "O";

	public EOFournisseur() {
		super();
	}

	public String codePostal() {
		if (fouEtranger() != null && fouEtranger().equals(EOFournisseur.ETAT_ETRANGER) && adrCpEtranger() != null)
			return adrCpEtranger();
		return adrCp();
	}

	public boolean isFournisseurInterne() {
		if (personne() == null || personne().repartStructures() == null)
			return false;

		String cStructure = FinderGrhumParametre.getParametreStructureFournisseursInternes(editingContext());
		if (cStructure == null)
			return false;

		for (int i = 0; i < personne().repartStructures().count(); i++) {
			EORepartStructure repartStructure = (EORepartStructure) personne().repartStructures().objectAtIndex(i);
			if (repartStructure.cStructureCle().equals(cStructure))
				return true;
		}

		return false;
	}

	public boolean isPersonneMorale() {
		if (adrCivilite().equals("STR"))
			return true;
		return false;
	}

	public boolean isPersonnePhysique() {
		return !isPersonneMorale();
	}

	public String libelle() {
		return fouCode() + " - " + fouNom();
	}

	public String libelleCourt() {
		String libelleCourt = libelle();
		if (libelleCourt != null && libelleCourt.length() > 25) {
			libelleCourt = libelleCourt.substring(0, 25);
		}
		return libelleCourt;
	}

	public String rechercheGlobale() {
		return fouCode() + " " + fouNom();
	}

	public NSArray ribsValides() {
		NSArray ribsValides = new NSArray();
		ribsValides = FinderRib.getRibs(editingContext(), new NSMutableDictionary(this, "fournisseur"));
		return ribsValides;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		throw new FactoryException(ENTITY_NAME + " : " + FactoryException.messageErreur);
	}

	public boolean isValide() {
		if (fouValide().equals("O"))
			return true;
		return false;
	}

	public String adresseConcat() {
		return (adrAdresse1() != null ? adrAdresse1() + " " : "").concat(adrAdresse2() != null ? adrAdresse2() : "");
	}

}
