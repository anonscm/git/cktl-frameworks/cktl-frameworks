/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControlePlanComptableInventaire;
import org.cocktail.fwkcktldepense.server.finder.FinderInventaire;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptableInventoriable;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPreDepenseControlePlanComptable extends _EOPreDepenseControlePlanComptable implements _IDepenseControlePlanComptable {
	private static Calculs serviceCalculs = Calculs.getInstance();
	

	private static final long serialVersionUID = 1L;
	private BigDecimal pourcentage;
	public static String PDPCO_POURCENTAGE = "pourcentage";

	public EOPreDepenseControlePlanComptable() {
		super();
		setPourcentage(new BigDecimal(0.0));
	}

	public boolean isGood() {
		if (!isInventaireObligatoire())
			return true;
		if (dpcoMontantBudgetaire().floatValue() != computeSumForKey(preDepenseControlePlanComptableInventaires(),
				EOPreDepenseControlePlanComptableInventaire.PDPIN_MONTANT_BUDGETAIRE_KEY).floatValue())
			return false;
		return true;
	}

	private BigDecimal computeSumForKey(NSArray<? extends EOEnterpriseObject> eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	public boolean isInventaireObligatoire() {
		if (!isInventaireAffichable())
			return false;
		if (!FinderParametre.getParametreInventaireObligatoire(editingContext(), preDepenseBudget().exercice()))
			return false;
		return true;
	}

	public boolean isInventaireAffichable() {
		if (planComptable() == null || preDepenseBudget() == null || preDepenseBudget().exercice() == null)
			return false;
		if (FinderPlanComptableInventoriable.getPlanComptableInventoriables(editingContext(), planComptable(), preDepenseBudget().exercice()).count() > 0)
			return true;
		return false;
	}

	public boolean isInventairesPossibles() {
		if (getInventairesPossibles().count() == 0)
			return false;
		return true;
	}

	public NSArray<EOInventaire> inventairesLiquides() {
		return inventaires();
	}

	public NSArray<EOInventaire> getInventairesPossibles() {

		if (preDepenseBudget() == null || preDepenseBudget().depensePapier() == null || preDepenseBudget().depensePapier().commande() == null ||
				preDepenseBudget().engagementBudget() == null)
			return NSArray.emptyArray();

		NSMutableArray<EOInventaire> lesInventaires = new NSMutableArray<EOInventaire>();
		lesInventaires.addObjectsFromArray(FinderInventaire.getInventaires(editingContext(), preDepenseBudget().depensePapier().commande(),
				preDepenseBudget().engagementBudget()));

		EODepensePapier depensePapier = preDepenseBudget().depensePapier();
		for (int i = 0; i < depensePapier.preDepenseBudgets().count(); i++) {
			EOPreDepenseBudget preDepenseBudget = (EOPreDepenseBudget) depensePapier.preDepenseBudgets().objectAtIndex(i);

			for (int j = 0; j < preDepenseBudget.depenseControlePlanComptables().count(); j++) {
				EOPreDepenseControlePlanComptable depenseControlePlanComptable =
						(EOPreDepenseControlePlanComptable) preDepenseBudget.depenseControlePlanComptables().objectAtIndex(j);

				//if (depenseControlePlanComptable==this)
				//	continue;

				for (int k = 0; k < depenseControlePlanComptable.inventaires().count(); k++)
					lesInventaires.removeIdenticalObject((EOInventaire) depenseControlePlanComptable.inventaires().objectAtIndex(k));
			}
		}

		return lesInventaires;
	}

	/**
	 * Les inventaires en cours de creation.
	 * 
	 * @see EOPreDepenseControlePlanComptable#inventairesLiquides()
	 * @return
	 */
	public NSArray<EOInventaire> inventaires() {
		NSMutableArray<EOInventaire> array = new NSMutableArray<EOInventaire>();
		if (preDepenseControlePlanComptableInventaires() == null || preDepenseControlePlanComptableInventaires().count() == 0)
			return array;
		for (int i = 0; i < preDepenseControlePlanComptableInventaires().count(); i++) {
			EOPreDepenseControlePlanComptableInventaire objet = (EOPreDepenseControlePlanComptableInventaire)
					preDepenseControlePlanComptableInventaires().objectAtIndex(i);
			if (objet != null && objet.inventaire() != null)
				array.addObject(objet.inventaire());
		}
		return array;
	}

	public void ajouterInventaires(NSArray<EOInventaire> inventaires) {
		if (inventaires == null || inventaires.count() == 0)
			return;
		for (int i = 0; i < inventaires.count(); i++)
			ajouterInventaire((EOInventaire) inventaires.objectAtIndex(i));
	}

	public void ajouterInventaire(EOInventaire inventaire) {
		if (inventaires().containsObject(inventaire) == false)
			new FactoryPreDepenseControlePlanComptableInventaire().creer(editingContext(), inventaire.lidMontant(), this, inventaire);
		//		new FactoryPreDepenseControlePlanComptableInventaire().creer(editingContext(), this.dpcoMontantBudgetaire(), this, inventaire);
	}

	public void supprimerInventaire(EOInventaire inventaire) {
		if (preDepenseControlePlanComptableInventaires() == null || preDepenseControlePlanComptableInventaires().count() == 0)
			return;
		for (int i = preDepenseControlePlanComptableInventaires().count() - 1; i >= 0; i--) {
			EOPreDepenseControlePlanComptableInventaire dep = (EOPreDepenseControlePlanComptableInventaire) preDepenseControlePlanComptableInventaires().objectAtIndex(i);
			if (dep != null && dep.inventaire() != null && dep.inventaire().equals(inventaire))
				new FactoryPreDepenseControlePlanComptableInventaire().supprimer(editingContext(), dep);
		}
	}

	public BigDecimal montantTtc() {
		return dpcoTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		if (preDepenseBudget() != null && preDepenseBudget().depTtcSaisie() != null && preDepenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(preDepenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (preDepenseBudget() != null) {
			NSArray<EOPreDepenseControlePlanComptable> depenseControlePlanComptables = preDepenseBudget().preDepenseControlePlanComptables();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPreDepenseControlePlanComptable.PLAN_COMPTABLE_KEY + " != nil", null);
			depenseControlePlanComptables = EOQualifier.filteredArrayWithQualifier(depenseControlePlanComptables, qual);
			BigDecimal total = preDepenseBudget().computeSumForKey(depenseControlePlanComptables, EOPreDepenseControlePlanComptable.PDPCO_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (preDepenseBudget() != null)
			preDepenseBudget().corrigerMontantPlanComptables();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDpcoHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoHtSaisie(aValue);
	}

	public void setDpcoTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoTvaSaisie(aValue);
	}

	public void setDpcoTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoTtcSaisie(aValue);
	}

	public void setDpcoMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (preDepenseBudget() != null && preDepenseBudget().engagementBudget() != null)
			arrondi = preDepenseBudget().engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoMontantBudgetaire(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (dpcoHtSaisie() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoHtSaisieManquant);
		if (dpcoTvaSaisie() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoTvaSaisieManquant);
		if (dpcoTtcSaisie() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoTtcSaisieManquant);
		if (dpcoMontantBudgetaire() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.exerciceManquant);
		if (preDepenseBudget() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.depenseBudgetManquant);

		if (!dpcoHtSaisie().abs().add(dpcoTvaSaisie().abs()).equals(dpcoTtcSaisie().abs()))
			setDpcoTvaSaisie(dpcoTtcSaisie().subtract(dpcoHtSaisie()));
		if (!dpcoMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDpcoMontantBudgetaire(calculMontantBudgetaire());

		if (!preDepenseBudget().exercice().equals(exercice()))
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.depenseBudgetExercicePasCoherent);

		// TODO : rajouter le test suivant le mode de paiement ... l'existence d'une ecritureDetail .. 
		// et eventuellement que le montant de cette ecriture soit >= au montant du ttc d'ici ... 
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(dpcoHtSaisie(), dpcoTvaSaisie(), preDepenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (planComptable() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.planComptableManquant);
		if (isInventaireObligatoire()) {
			if (computeSumForKey(preDepenseControlePlanComptableInventaires(), EOPreDepenseControlePlanComptableInventaire.PDPIN_MONTANT_BUDGETAIRE_KEY).floatValue() != dpcoMontantBudgetaire().floatValue())
				throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.montantInventairesIncoherent);
		}
	}

	public _IDepenseBudget depenseBudget() {
		return preDepenseBudget();
	}

}