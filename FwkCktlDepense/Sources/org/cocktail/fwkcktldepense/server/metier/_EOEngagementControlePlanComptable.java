/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEngagementControlePlanComptable.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEngagementControlePlanComptable extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControlePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_PLANCO";


//Attribute Keys
	public static final ERXKey<NSTimestamp> EPCO_DATE_SAISIE = new ERXKey<NSTimestamp>("epcoDateSaisie");
	public static final ERXKey<java.math.BigDecimal> EPCO_HT_SAISIE = new ERXKey<java.math.BigDecimal>("epcoHtSaisie");
	public static final ERXKey<java.math.BigDecimal> EPCO_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("epcoMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> EPCO_MONTANT_BUDGETAIRE_RESTE = new ERXKey<java.math.BigDecimal>("epcoMontantBudgetaireReste");
	public static final ERXKey<java.math.BigDecimal> EPCO_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("epcoTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> EPCO_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("epcoTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "epcoId";

	public static final String EPCO_DATE_SAISIE_KEY = "epcoDateSaisie";
	public static final String EPCO_HT_SAISIE_KEY = "epcoHtSaisie";
	public static final String EPCO_MONTANT_BUDGETAIRE_KEY = "epcoMontantBudgetaire";
	public static final String EPCO_MONTANT_BUDGETAIRE_RESTE_KEY = "epcoMontantBudgetaireReste";
	public static final String EPCO_TTC_SAISIE_KEY = "epcoTtcSaisie";
	public static final String EPCO_TVA_SAISIE_KEY = "epcoTvaSaisie";

//Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String EPCO_ID_KEY = "epcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String EPCO_DATE_SAISIE_COLKEY = "EPCO_DATE_SAISIE";
	public static final String EPCO_HT_SAISIE_COLKEY = "EPCO_HT_SAISIE";
	public static final String EPCO_MONTANT_BUDGETAIRE_COLKEY = "EPCO_MONTANT_BUDGETAIRE";
	public static final String EPCO_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EPCO_MONTANT_BUDGETAIRE_RESTE";
	public static final String EPCO_TTC_SAISIE_COLKEY = "EPCO_TTC_SAISIE";
	public static final String EPCO_TVA_SAISIE_COLKEY = "EPCO_TVA_SAISIE";

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EPCO_ID_COLKEY = "EPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
	public NSTimestamp epcoDateSaisie() {
	 return (NSTimestamp) storedValueForKey(EPCO_DATE_SAISIE_KEY);
	}

	public void setEpcoDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, EPCO_DATE_SAISIE_KEY);
	}

	public java.math.BigDecimal epcoHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EPCO_HT_SAISIE_KEY);
	}

	public void setEpcoHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EPCO_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal epcoMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(EPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public void setEpcoMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal epcoMontantBudgetaireReste() {
	 return (java.math.BigDecimal) storedValueForKey(EPCO_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public void setEpcoMontantBudgetaireReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EPCO_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public java.math.BigDecimal epcoTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EPCO_TTC_SAISIE_KEY);
	}

	public void setEpcoTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EPCO_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal epcoTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EPCO_TVA_SAISIE_KEY);
	}

	public void setEpcoTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EPCO_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}


	/**
	* Créer une instance de EOEngagementControlePlanComptable avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEngagementControlePlanComptable createEOEngagementControlePlanComptable(EOEditingContext editingContext				, NSTimestamp epcoDateSaisie
							, java.math.BigDecimal epcoHtSaisie
							, java.math.BigDecimal epcoMontantBudgetaire
							, java.math.BigDecimal epcoMontantBudgetaireReste
							, java.math.BigDecimal epcoTtcSaisie
							, java.math.BigDecimal epcoTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable					) {
	 EOEngagementControlePlanComptable eo = (EOEngagementControlePlanComptable) EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControlePlanComptable.ENTITY_NAME);	 
							eo.setEpcoDateSaisie(epcoDateSaisie);
									eo.setEpcoHtSaisie(epcoHtSaisie);
									eo.setEpcoMontantBudgetaire(epcoMontantBudgetaire);
									eo.setEpcoMontantBudgetaireReste(epcoMontantBudgetaireReste);
									eo.setEpcoTtcSaisie(epcoTtcSaisie);
									eo.setEpcoTvaSaisie(epcoTvaSaisie);
						 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setPlanComptableRelationship(planComptable);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngagementControlePlanComptable creerInstance(EOEditingContext editingContext) {
		EOEngagementControlePlanComptable object = (EOEngagementControlePlanComptable)EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControlePlanComptable.ENTITY_NAME);
  		return object;
		}

	

  public EOEngagementControlePlanComptable localInstanceIn(EOEditingContext editingContext) {
    EOEngagementControlePlanComptable localInstance = (EOEngagementControlePlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngagementControlePlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngagementControlePlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEngagementControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEngagementControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngagementControlePlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControlePlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControlePlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControlePlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
