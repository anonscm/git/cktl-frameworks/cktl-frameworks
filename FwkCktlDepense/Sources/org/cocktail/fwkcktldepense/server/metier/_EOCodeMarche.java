/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCodeMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCodeMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCodeMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CODE_MARCHE";


//Attribute Keys
	public static final ERXKey<String> CM_CODE = new ERXKey<String>("cmCode");
	public static final ERXKey<String> CM_LIB = new ERXKey<String>("cmLib");
	public static final ERXKey<Integer> CM_NIVEAU = new ERXKey<Integer>("cmNiveau");
	public static final ERXKey<String> CM_SUPPR = new ERXKey<String>("cmSuppr");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> CODE_EXER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>("codeExer");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> CODE_MARCHE_PERE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche>("codeMarchePere");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cmOrdre";

	public static final String CM_CODE_KEY = "cmCode";
	public static final String CM_LIB_KEY = "cmLib";
	public static final String CM_NIVEAU_KEY = "cmNiveau";
	public static final String CM_SUPPR_KEY = "cmSuppr";

//Attributs non visibles
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String CM_PERE_KEY = "cmPere";

//Colonnes dans la base de donnees
	public static final String CM_CODE_COLKEY = "CM_CODE";
	public static final String CM_LIB_COLKEY = "CM_LIB";
	public static final String CM_NIVEAU_COLKEY = "CM_NIVEAU";
	public static final String CM_SUPPR_COLKEY = "CM_SUPPR";

	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String CM_PERE_COLKEY = "CM_PERE";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String CODE_MARCHE_PERE_KEY = "codeMarchePere";



	// Accessors methods
	public String cmCode() {
	 return (String) storedValueForKey(CM_CODE_KEY);
	}

	public void setCmCode(String value) {
	 takeStoredValueForKey(value, CM_CODE_KEY);
	}

	public String cmLib() {
	 return (String) storedValueForKey(CM_LIB_KEY);
	}

	public void setCmLib(String value) {
	 takeStoredValueForKey(value, CM_LIB_KEY);
	}

	public Integer cmNiveau() {
	 return (Integer) storedValueForKey(CM_NIVEAU_KEY);
	}

	public void setCmNiveau(Integer value) {
	 takeStoredValueForKey(value, CM_NIVEAU_KEY);
	}

	public String cmSuppr() {
	 return (String) storedValueForKey(CM_SUPPR_KEY);
	}

	public void setCmSuppr(String value) {
	 takeStoredValueForKey(value, CM_SUPPR_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeMarche codeMarchePere() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeMarche)storedValueForKey(CODE_MARCHE_PERE_KEY);
	}

	public void setCodeMarchePereRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarche value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeMarche oldValue = codeMarchePere();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_PERE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_PERE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> codeExer() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>)storedValueForKey(CODE_EXER_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> codeExer(EOQualifier qualifier) {
	 return codeExer(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> codeExer(EOQualifier qualifier, boolean fetch) {
	 return codeExer(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> codeExer(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCodeExer.CODE_MARCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCodeExer.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = codeExer();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, CODE_EXER_KEY);
	}
	
	public void removeFromCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_EXER_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCodeExer createCodeExerRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCodeExer.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, CODE_EXER_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeExer) eo;
	}
	
	public void deleteCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_EXER_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCodeExerRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> objects = codeExer().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCodeExerRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCodeMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCodeMarche createEOCodeMarche(EOEditingContext editingContext				, String cmCode
							, String cmLib
														) {
	 EOCodeMarche eo = (EOCodeMarche) EOUtilities.createAndInsertInstance(editingContext, _EOCodeMarche.ENTITY_NAME);	 
							eo.setCmCode(cmCode);
									eo.setCmLib(cmLib);
											 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeMarche creerInstance(EOEditingContext editingContext) {
		EOCodeMarche object = (EOCodeMarche)EOUtilities.createAndInsertInstance(editingContext, _EOCodeMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOCodeMarche localInstanceIn(EOEditingContext editingContext) {
    EOCodeMarche localInstance = (EOCodeMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCodeMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCodeMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
