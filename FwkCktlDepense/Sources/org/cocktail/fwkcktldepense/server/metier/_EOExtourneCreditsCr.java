/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOExtourneCreditsCr.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOExtourneCreditsCr extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleExtourneCreditsCr";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_EXTOURNE_CREDITS_CR";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_CONSOMME = new ERXKey<java.math.BigDecimal>("vecMontantBudConsomme");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_DISPONIBLE = new ERXKey<java.math.BigDecimal>("vecMontantBudDisponible");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_DISPO_REEL = new ERXKey<java.math.BigDecimal>("vecMontantBudDispoReel");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_BUD_INITIAL = new ERXKey<java.math.BigDecimal>("vecMontantBudInitial");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_HT = new ERXKey<java.math.BigDecimal>("vecMontantHt");
	public static final ERXKey<java.math.BigDecimal> VEC_MONTANT_TTC = new ERXKey<java.math.BigDecimal>("vecMontantTtc");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> TO_EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("toExercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> TO_ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("toOrgan");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit> TO_TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeCredit>("toTypeCredit");

	// Attributes


	public static final String VEC_MONTANT_BUD_CONSOMME_KEY = "vecMontantBudConsomme";
	public static final String VEC_MONTANT_BUD_DISPONIBLE_KEY = "vecMontantBudDisponible";
	public static final String VEC_MONTANT_BUD_DISPO_REEL_KEY = "vecMontantBudDispoReel";
	public static final String VEC_MONTANT_BUD_INITIAL_KEY = "vecMontantBudInitial";
	public static final String VEC_MONTANT_HT_KEY = "vecMontantHt";
	public static final String VEC_MONTANT_TTC_KEY = "vecMontantTtc";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String VEC_MONTANT_BUD_CONSOMME_COLKEY = "vec_montant_bud_consomme";
	public static final String VEC_MONTANT_BUD_DISPONIBLE_COLKEY = "vec_montant_bud_disponible";
	public static final String VEC_MONTANT_BUD_DISPO_REEL_COLKEY = "vec_montant_bud_dispo_reel";
	public static final String VEC_MONTANT_BUD_INITIAL_COLKEY = "vec_montant_bud_initial";
	public static final String VEC_MONTANT_HT_COLKEY = "vec_montant_ht";
	public static final String VEC_MONTANT_TTC_COLKEY = "vec_montant_ttc";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID_CR";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";



	// Accessors methods
	public java.math.BigDecimal vecMontantBudConsomme() {
	 return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_CONSOMME_KEY);
	}

	public void setVecMontantBudConsomme(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, VEC_MONTANT_BUD_CONSOMME_KEY);
	}

	public java.math.BigDecimal vecMontantBudDisponible() {
	 return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_DISPONIBLE_KEY);
	}

	public void setVecMontantBudDisponible(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, VEC_MONTANT_BUD_DISPONIBLE_KEY);
	}

	public java.math.BigDecimal vecMontantBudDispoReel() {
	 return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_DISPO_REEL_KEY);
	}

	public void setVecMontantBudDispoReel(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, VEC_MONTANT_BUD_DISPO_REEL_KEY);
	}

	public java.math.BigDecimal vecMontantBudInitial() {
	 return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_INITIAL_KEY);
	}

	public void setVecMontantBudInitial(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, VEC_MONTANT_BUD_INITIAL_KEY);
	}

	public java.math.BigDecimal vecMontantHt() {
	 return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_HT_KEY);
	}

	public void setVecMontantHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, VEC_MONTANT_HT_KEY);
	}

	public java.math.BigDecimal vecMontantTtc() {
	 return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_TTC_KEY);
	}

	public void setVecMontantTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, VEC_MONTANT_TTC_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice toExercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
	}

	public void setToExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = toExercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan toOrgan() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
	}

	public void setToOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = toOrgan();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeCredit toTypeCredit() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
	}

	public void setToTypeCreditRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeCredit oldValue = toTypeCredit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
	 }
	}


	/**
	* Créer une instance de EOExtourneCreditsCr avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOExtourneCreditsCr createEOExtourneCreditsCr(EOEditingContext editingContext				, java.math.BigDecimal vecMontantBudConsomme
							, java.math.BigDecimal vecMontantBudDisponible
							, java.math.BigDecimal vecMontantBudDispoReel
							, java.math.BigDecimal vecMontantBudInitial
							, java.math.BigDecimal vecMontantHt
							, java.math.BigDecimal vecMontantTtc
					, org.cocktail.fwkcktldepense.server.metier.EOExercice toExercice		, org.cocktail.fwkcktldepense.server.metier.EOOrgan toOrgan		, org.cocktail.fwkcktldepense.server.metier.EOTypeCredit toTypeCredit					) {
	 EOExtourneCreditsCr eo = (EOExtourneCreditsCr) EOUtilities.createAndInsertInstance(editingContext, _EOExtourneCreditsCr.ENTITY_NAME);	 
							eo.setVecMontantBudConsomme(vecMontantBudConsomme);
									eo.setVecMontantBudDisponible(vecMontantBudDisponible);
									eo.setVecMontantBudDispoReel(vecMontantBudDispoReel);
									eo.setVecMontantBudInitial(vecMontantBudInitial);
									eo.setVecMontantHt(vecMontantHt);
									eo.setVecMontantTtc(vecMontantTtc);
						 eo.setToExerciceRelationship(toExercice);
				 eo.setToOrganRelationship(toOrgan);
				 eo.setToTypeCreditRelationship(toTypeCredit);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOExtourneCreditsCr creerInstance(EOEditingContext editingContext) {
		EOExtourneCreditsCr object = (EOExtourneCreditsCr)EOUtilities.createAndInsertInstance(editingContext, _EOExtourneCreditsCr.ENTITY_NAME);
  		return object;
		}

	

  public EOExtourneCreditsCr localInstanceIn(EOEditingContext editingContext) {
    EOExtourneCreditsCr localInstance = (EOExtourneCreditsCr)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOExtourneCreditsCr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOExtourneCreditsCr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOExtourneCreditsCr> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOExtourneCreditsCr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOExtourneCreditsCr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOExtourneCreditsCr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOExtourneCreditsCr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOExtourneCreditsCr> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOExtourneCreditsCr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOExtourneCreditsCr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOExtourneCreditsCr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOExtourneCreditsCr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOExtourneCreditsCr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOExtourneCreditsCr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
