/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOConvention.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOConvention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CONVENTION";


//Attribute Keys
	public static final ERXKey<Integer> CONV_ORDRE = new ERXKey<Integer>("convOrdre");
	public static final ERXKey<String> CONV_REFERENCE = new ERXKey<String>("convReference");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> CONVENTION_NON_LIMITATIVES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative>("conventionNonLimitatives");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "convOrdre";

	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String CONV_REFERENCE_KEY = "convReference";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String CONV_REFERENCE_COLKEY = "CONV_REFERENCE";



	// Relationships
	public static final String CONVENTION_NON_LIMITATIVES_KEY = "conventionNonLimitatives";



	// Accessors methods
	public Integer convOrdre() {
	 return (Integer) storedValueForKey(CONV_ORDRE_KEY);
	}

	public void setConvOrdre(Integer value) {
	 takeStoredValueForKey(value, CONV_ORDRE_KEY);
	}

	public String convReference() {
	 return (String) storedValueForKey(CONV_REFERENCE_KEY);
	}

	public void setConvReference(String value) {
	 takeStoredValueForKey(value, CONV_REFERENCE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> conventionNonLimitatives() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative>)storedValueForKey(CONVENTION_NON_LIMITATIVES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> conventionNonLimitatives(EOQualifier qualifier) {
	 return conventionNonLimitatives(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> conventionNonLimitatives(EOQualifier qualifier, boolean fetch) {
	 return conventionNonLimitatives(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> conventionNonLimitatives(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative.CONVENTION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = conventionNonLimitatives();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToConventionNonLimitativesRelationship(org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
	}
	
	public void removeFromConventionNonLimitativesRelationship(org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative createConventionNonLimitativesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTION_NON_LIMITATIVES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative) eo;
	}
	
	public void deleteConventionNonLimitativesRelationship(org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllConventionNonLimitativesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative> objects = conventionNonLimitatives().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteConventionNonLimitativesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOConvention createEOConvention(EOEditingContext editingContext				, Integer convOrdre
							, String convReference
								) {
	 EOConvention eo = (EOConvention) EOUtilities.createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME);	 
							eo.setConvOrdre(convOrdre);
									eo.setConvReference(convReference);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOConvention creerInstance(EOEditingContext editingContext) {
		EOConvention object = (EOConvention)EOUtilities.createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME);
  		return object;
		}

	

  public EOConvention localInstanceIn(EOEditingContext editingContext) {
    EOConvention localInstance = (EOConvention)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOConvention>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOConvention> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOConvention> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
