/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier.b2b;

import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLParameters;
import org.cocktail.fwkcktldepense.server.Version;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOFournisB2bCxmlParam extends _EOFournisB2bCxmlParam {

	public EOFournisB2bCxmlParam() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public CXMLParameters toCxmlParameters() {
		CXMLParameters cxmlParameters = new CXMLParameters();
		cxmlParameters.setDeploymentMode(this.fbcpDeploymentMode());
		cxmlParameters.setSenderIdentity(this.fbcpIdentity());
		cxmlParameters.setSenderSharedSecret(this.fbcpSharedSecret());
		cxmlParameters.setSenderUserAgent(Version.APPLICATIONFINALNAME);
		cxmlParameters.setSupplierSetupURL(this.fbcpSupplierSetupUrl());
		cxmlParameters.setName(this.fbcpName());
		cxmlParameters.setToIdentity(this.fbcpSupplierCredId());
		return cxmlParameters;
	}

	public boolean isPunchOutEnabled() {
		return (this.fbcpIdentity() != null && this.fbcpSharedSecret() != null && this.fbcpSupplierSetupUrl() != null);
	}

	/**
	 * @param edc
	 * @param fournis
	 * @return Les parametres b2b actif pour un fournisseur.
	 */
	public static NSArray<EOFournisB2bCxmlParam> fetchAllValidesForFournisseur(EOEditingContext edc, EOFournisseur fournis) {
		if (fournis == null) {
			return NSArray.emptyArray();
		}
		NSMutableArray quals = new NSMutableArray();
		quals.add(new EOKeyValueQualifier(TO_FOURNISSEUR_KEY, EOQualifier.QualifierOperatorEqual, fournis));
		quals.add(new EOKeyValueQualifier(TO_TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.VALIDE));
		return fetchAll(edc, new EOAndQualifier(quals), null, true);
	}

	public String libelle() {
		return fbcpName() + (fbcpDeploymentMode() == null ? "" : " (" + fbcpDeploymentMode() + ")");
	}

}