/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOB2bCxmlOrderRequest.java instead.
package org.cocktail.fwkcktldepense.server.metier.b2b;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOB2bCxmlOrderRequest extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleB2bCxmlOrderRequest";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.B2B_CXML_ORDERREQUEST";


//Attribute Keys
	public static final ERXKey<Integer> COMM_ID = new ERXKey<Integer>("commId");
	public static final ERXKey<String> CXML = new ERXKey<String>("cxml");
	public static final ERXKey<String> CXML_REPONSE = new ERXKey<String>("cxmlReponse");
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> TO_COMMANDE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("toCommande");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> TO_FOURNIS_B2B_CXML_PARAM = new ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam>("toFournisB2bCxmlParam");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bcorId";

	public static final String COMM_ID_KEY = "commId";
	public static final String CXML_KEY = "cxml";
	public static final String CXML_REPONSE_KEY = "cxmlReponse";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";

//Attributs non visibles
	public static final String BCOR_ID_KEY = "bcorId";
	public static final String FBCP_ID_KEY = "fbcpId";

//Colonnes dans la base de donnees
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String CXML_COLKEY = "cxml";
	public static final String CXML_REPONSE_COLKEY = "cxml_Reponse";
	public static final String DATE_CREATION_COLKEY = "date_Creation";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_creation";

	public static final String BCOR_ID_COLKEY = "BCOR_ID";
	public static final String FBCP_ID_COLKEY = "FBCP_ID";


	// Relationships
	public static final String TO_COMMANDE_KEY = "toCommande";
	public static final String TO_FOURNIS_B2B_CXML_PARAM_KEY = "toFournisB2bCxmlParam";



	// Accessors methods
	public Integer commId() {
	 return (Integer) storedValueForKey(COMM_ID_KEY);
	}

	public void setCommId(Integer value) {
	 takeStoredValueForKey(value, COMM_ID_KEY);
	}

	public String cxml() {
	 return (String) storedValueForKey(CXML_KEY);
	}

	public void setCxml(String value) {
	 takeStoredValueForKey(value, CXML_KEY);
	}

	public String cxmlReponse() {
	 return (String) storedValueForKey(CXML_REPONSE_KEY);
	}

	public void setCxmlReponse(String value) {
	 takeStoredValueForKey(value, CXML_REPONSE_KEY);
	}

	public NSTimestamp dateCreation() {
	 return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_CREATION_KEY);
	}

	public Integer persIdCreation() {
	 return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande toCommande() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(TO_COMMANDE_KEY);
	}

	public void setToCommandeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = toCommande();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMMANDE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMMANDE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam toFournisB2bCxmlParam() {
	 return (org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam)storedValueForKey(TO_FOURNIS_B2B_CXML_PARAM_KEY);
	}

	public void setToFournisB2bCxmlParamRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam oldValue = toFournisB2bCxmlParam();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_B2B_CXML_PARAM_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_B2B_CXML_PARAM_KEY);
	 }
	}


	/**
	* Créer une instance de EOB2bCxmlOrderRequest avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOB2bCxmlOrderRequest createEOB2bCxmlOrderRequest(EOEditingContext editingContext						, String cxml
									, NSTimestamp dateCreation
									, org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam toFournisB2bCxmlParam					) {
	 EOB2bCxmlOrderRequest eo = (EOB2bCxmlOrderRequest) EOUtilities.createAndInsertInstance(editingContext, _EOB2bCxmlOrderRequest.ENTITY_NAME);	 
									eo.setCxml(cxml);
											eo.setDateCreation(dateCreation);
										 eo.setToFournisB2bCxmlParamRelationship(toFournisB2bCxmlParam);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOB2bCxmlOrderRequest creerInstance(EOEditingContext editingContext) {
		EOB2bCxmlOrderRequest object = (EOB2bCxmlOrderRequest)EOUtilities.createAndInsertInstance(editingContext, _EOB2bCxmlOrderRequest.ENTITY_NAME);
  		return object;
		}

	

  public EOB2bCxmlOrderRequest localInstanceIn(EOEditingContext editingContext) {
    EOB2bCxmlOrderRequest localInstance = (EOB2bCxmlOrderRequest)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOB2bCxmlOrderRequest fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOB2bCxmlOrderRequest fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOB2bCxmlOrderRequest> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOB2bCxmlOrderRequest eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOB2bCxmlOrderRequest)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOB2bCxmlOrderRequest fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOB2bCxmlOrderRequest fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOB2bCxmlOrderRequest> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOB2bCxmlOrderRequest eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOB2bCxmlOrderRequest)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOB2bCxmlOrderRequest fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOB2bCxmlOrderRequest eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOB2bCxmlOrderRequest ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOB2bCxmlOrderRequest fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
