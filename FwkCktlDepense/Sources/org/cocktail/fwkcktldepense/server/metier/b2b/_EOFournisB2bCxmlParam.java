/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOFournisB2bCxmlParam.java instead.
package org.cocktail.fwkcktldepense.server.metier.b2b;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOFournisB2bCxmlParam extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleB2bFournisB2bCxmlParam";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FOURNIS_B2B_CXML_PARAM";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
	public static final ERXKey<String> FBCP_COMMENTAIRES = new ERXKey<String>("fbcpCommentaires");
	public static final ERXKey<String> FBCP_DEPLOYMENT_MODE = new ERXKey<String>("fbcpDeploymentMode");
	public static final ERXKey<String> FBCP_IDENTITY = new ERXKey<String>("fbcpIdentity");
	public static final ERXKey<String> FBCP_NAME = new ERXKey<String>("fbcpName");
	public static final ERXKey<String> FBCP_SHARED_SECRET = new ERXKey<String>("fbcpSharedSecret");
	public static final ERXKey<String> FBCP_SUPPLIER_CRED_DOMAIN = new ERXKey<String>("fbcpSupplierCredDomain");
	public static final ERXKey<String> FBCP_SUPPLIER_CRED_ID = new ERXKey<String>("fbcpSupplierCredId");
	public static final ERXKey<String> FBCP_SUPPLIER_SETUP_URL = new ERXKey<String>("fbcpSupplierSetupUrl");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> TO_FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("toFournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTva> TO_TVA = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTva>("toTva");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TO_TYPE_ETAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("toTypeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fbcpId";

	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String FBCP_COMMENTAIRES_KEY = "fbcpCommentaires";
	public static final String FBCP_DEPLOYMENT_MODE_KEY = "fbcpDeploymentMode";
	public static final String FBCP_IDENTITY_KEY = "fbcpIdentity";
	public static final String FBCP_NAME_KEY = "fbcpName";
	public static final String FBCP_SHARED_SECRET_KEY = "fbcpSharedSecret";
	public static final String FBCP_SUPPLIER_CRED_DOMAIN_KEY = "fbcpSupplierCredDomain";
	public static final String FBCP_SUPPLIER_CRED_ID_KEY = "fbcpSupplierCredId";
	public static final String FBCP_SUPPLIER_SETUP_URL_KEY = "fbcpSupplierSetupUrl";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

//Attributs non visibles
	public static final String FBCP_DUREE_VALID_PANIER_KEY = "fbcpDureeValidPanier";
	public static final String FBCP_ID_KEY = "fbcpId";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String DATE_MODIFICATION_COLKEY = "date_modification";
	public static final String FBCP_COMMENTAIRES_COLKEY = "FBCP_COMMENTAIRES";
	public static final String FBCP_DEPLOYMENT_MODE_COLKEY = "FBCP_DEPLOYMENT_MODE";
	public static final String FBCP_IDENTITY_COLKEY = "FBCP_IDENTITY";
	public static final String FBCP_NAME_COLKEY = "FBCP_NAME";
	public static final String FBCP_SHARED_SECRET_COLKEY = "FBCP_SHARED_SECRET";
	public static final String FBCP_SUPPLIER_CRED_DOMAIN_COLKEY = "FBCP_SUPPLIER_CRED_DOMAIN";
	public static final String FBCP_SUPPLIER_CRED_ID_COLKEY = "FBCP_SUPPLIER_CRED_ID";
	public static final String FBCP_SUPPLIER_SETUP_URL_COLKEY = "FBCP_SUPPLIER_SETUP_URL";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_modification";

	public static final String FBCP_DUREE_VALID_PANIER_COLKEY = "FBCP_DUREE_VALID_PANIER";
	public static final String FBCP_ID_COLKEY = "FBCP_ID";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String TO_FOURNISSEUR_KEY = "toFournisseur";
	public static final String TO_TVA_KEY = "toTva";
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
	public NSTimestamp dateModification() {
	 return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
	}

	public void setDateModification(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
	}

	public String fbcpCommentaires() {
	 return (String) storedValueForKey(FBCP_COMMENTAIRES_KEY);
	}

	public void setFbcpCommentaires(String value) {
	 takeStoredValueForKey(value, FBCP_COMMENTAIRES_KEY);
	}

	public String fbcpDeploymentMode() {
	 return (String) storedValueForKey(FBCP_DEPLOYMENT_MODE_KEY);
	}

	public void setFbcpDeploymentMode(String value) {
	 takeStoredValueForKey(value, FBCP_DEPLOYMENT_MODE_KEY);
	}

	public String fbcpIdentity() {
	 return (String) storedValueForKey(FBCP_IDENTITY_KEY);
	}

	public void setFbcpIdentity(String value) {
	 takeStoredValueForKey(value, FBCP_IDENTITY_KEY);
	}

	public String fbcpName() {
	 return (String) storedValueForKey(FBCP_NAME_KEY);
	}

	public void setFbcpName(String value) {
	 takeStoredValueForKey(value, FBCP_NAME_KEY);
	}

	public String fbcpSharedSecret() {
	 return (String) storedValueForKey(FBCP_SHARED_SECRET_KEY);
	}

	public void setFbcpSharedSecret(String value) {
	 takeStoredValueForKey(value, FBCP_SHARED_SECRET_KEY);
	}

	public String fbcpSupplierCredDomain() {
	 return (String) storedValueForKey(FBCP_SUPPLIER_CRED_DOMAIN_KEY);
	}

	public void setFbcpSupplierCredDomain(String value) {
	 takeStoredValueForKey(value, FBCP_SUPPLIER_CRED_DOMAIN_KEY);
	}

	public String fbcpSupplierCredId() {
	 return (String) storedValueForKey(FBCP_SUPPLIER_CRED_ID_KEY);
	}

	public void setFbcpSupplierCredId(String value) {
	 takeStoredValueForKey(value, FBCP_SUPPLIER_CRED_ID_KEY);
	}

	public String fbcpSupplierSetupUrl() {
	 return (String) storedValueForKey(FBCP_SUPPLIER_SETUP_URL_KEY);
	}

	public void setFbcpSupplierSetupUrl(String value) {
	 takeStoredValueForKey(value, FBCP_SUPPLIER_SETUP_URL_KEY);
	}

	public Integer persIdModification() {
	 return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur toFournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(TO_FOURNISSEUR_KEY);
	}

	public void setToFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = toFournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTva toTva() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTva)storedValueForKey(TO_TVA_KEY);
	}

	public void setToTvaRelationship(org.cocktail.fwkcktldepense.server.metier.EOTva value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTva oldValue = toTva();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TVA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TVA_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat toTypeEtat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
	}

	public void setToTypeEtatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = toTypeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOFournisB2bCxmlParam avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOFournisB2bCxmlParam createEOFournisB2bCxmlParam(EOEditingContext editingContext				, NSTimestamp dateModification
									, String fbcpDeploymentMode
							, String fbcpIdentity
							, String fbcpName
							, String fbcpSharedSecret
							, String fbcpSupplierCredDomain
							, String fbcpSupplierCredId
							, String fbcpSupplierSetupUrl
							, Integer persIdModification
					, org.cocktail.fwkcktldepense.server.metier.EOFournisseur toFournisseur		, org.cocktail.fwkcktldepense.server.metier.EOTva toTva		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat toTypeEtat					) {
	 EOFournisB2bCxmlParam eo = (EOFournisB2bCxmlParam) EOUtilities.createAndInsertInstance(editingContext, _EOFournisB2bCxmlParam.ENTITY_NAME);	 
							eo.setDateModification(dateModification);
											eo.setFbcpDeploymentMode(fbcpDeploymentMode);
									eo.setFbcpIdentity(fbcpIdentity);
									eo.setFbcpName(fbcpName);
									eo.setFbcpSharedSecret(fbcpSharedSecret);
									eo.setFbcpSupplierCredDomain(fbcpSupplierCredDomain);
									eo.setFbcpSupplierCredId(fbcpSupplierCredId);
									eo.setFbcpSupplierSetupUrl(fbcpSupplierSetupUrl);
									eo.setPersIdModification(persIdModification);
						 eo.setToFournisseurRelationship(toFournisseur);
				 eo.setToTvaRelationship(toTva);
				 eo.setToTypeEtatRelationship(toTypeEtat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournisB2bCxmlParam creerInstance(EOEditingContext editingContext) {
		EOFournisB2bCxmlParam object = (EOFournisB2bCxmlParam)EOUtilities.createAndInsertInstance(editingContext, _EOFournisB2bCxmlParam.ENTITY_NAME);
  		return object;
		}

	

  public EOFournisB2bCxmlParam localInstanceIn(EOEditingContext editingContext) {
    EOFournisB2bCxmlParam localInstance = (EOFournisB2bCxmlParam)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFournisB2bCxmlParam fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFournisB2bCxmlParam fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFournisB2bCxmlParam> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFournisB2bCxmlParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFournisB2bCxmlParam)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFournisB2bCxmlParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFournisB2bCxmlParam fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFournisB2bCxmlParam> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFournisB2bCxmlParam eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFournisB2bCxmlParam)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFournisB2bCxmlParam fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFournisB2bCxmlParam eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFournisB2bCxmlParam ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFournisB2bCxmlParam fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
