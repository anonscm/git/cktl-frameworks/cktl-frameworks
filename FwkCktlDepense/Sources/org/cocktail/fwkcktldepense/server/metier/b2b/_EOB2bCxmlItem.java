/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOB2bCxmlItem.java instead.
package org.cocktail.fwkcktldepense.server.metier.b2b;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOB2bCxmlItem extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleB2bCxmlItem";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.B2B_CXML_ITEM";


//Attribute Keys
	public static final ERXKey<String> CLASSIFICATION = new ERXKey<String>("classification");
	public static final ERXKey<String> CLASSIFICATION_DOM = new ERXKey<String>("classificationDom");
	public static final ERXKey<String> COMMENTS = new ERXKey<String>("comments");
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<String> DESCRIPTION = new ERXKey<String>("description");
	public static final ERXKey<String> MANUFACTURER_NAME = new ERXKey<String>("manufacturerName");
	public static final ERXKey<String> MANUFACTURER_PART_ID = new ERXKey<String>("manufacturerPartID");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<String> SUPPLIER_PART_AUXILIARY_ID = new ERXKey<String>("supplierPartAuxiliaryId");
	public static final ERXKey<String> SUPPLIER_PART_ID = new ERXKey<String>("supplierPartId");
	public static final ERXKey<String> UNIT_OF_MEASURE = new ERXKey<String>("unitOfMeasure");
	public static final ERXKey<java.math.BigDecimal> UNIT_PRICE = new ERXKey<java.math.BigDecimal>("unitPrice");
	public static final ERXKey<String> URL = new ERXKey<String>("url");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOArticle> TO_ARTICLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOArticle>("toArticle");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam> TO_FOURNIS_B2B_CXML_PARAM = new ERXKey<org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam>("toFournisB2bCxmlParam");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bciId";

	public static final String CLASSIFICATION_KEY = "classification";
	public static final String CLASSIFICATION_DOM_KEY = "classificationDom";
	public static final String COMMENTS_KEY = "comments";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DESCRIPTION_KEY = "description";
	public static final String MANUFACTURER_NAME_KEY = "manufacturerName";
	public static final String MANUFACTURER_PART_ID_KEY = "manufacturerPartID";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String SUPPLIER_PART_AUXILIARY_ID_KEY = "supplierPartAuxiliaryId";
	public static final String SUPPLIER_PART_ID_KEY = "supplierPartId";
	public static final String UNIT_OF_MEASURE_KEY = "unitOfMeasure";
	public static final String UNIT_PRICE_KEY = "unitPrice";
	public static final String URL_KEY = "url";

//Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String BCI_ID_KEY = "bciId";
	public static final String FBCP_ID_KEY = "fbcpId";

//Colonnes dans la base de donnees
	public static final String CLASSIFICATION_COLKEY = "classification";
	public static final String CLASSIFICATION_DOM_COLKEY = "classification_Dom";
	public static final String COMMENTS_COLKEY = "comments";
	public static final String DATE_CREATION_COLKEY = "date_creation";
	public static final String DESCRIPTION_COLKEY = "description";
	public static final String MANUFACTURER_NAME_COLKEY = "manufacturer_Name";
	public static final String MANUFACTURER_PART_ID_COLKEY = "Manufacturer_Part_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_creation";
	public static final String SUPPLIER_PART_AUXILIARY_ID_COLKEY = "SUPPLIER_PART_AUXILIARY_ID";
	public static final String SUPPLIER_PART_ID_COLKEY = "SUPPLIER_PART_ID";
	public static final String UNIT_OF_MEASURE_COLKEY = "Unit_Of_Measure";
	public static final String UNIT_PRICE_COLKEY = "UNIT_PRICE";
	public static final String URL_COLKEY = "url";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String BCI_ID_COLKEY = "BCI_ID";
	public static final String FBCP_ID_COLKEY = "FBCP_ID";


	// Relationships
	public static final String TO_ARTICLE_KEY = "toArticle";
	public static final String TO_FOURNIS_B2B_CXML_PARAM_KEY = "toFournisB2bCxmlParam";



	// Accessors methods
	public String classification() {
	 return (String) storedValueForKey(CLASSIFICATION_KEY);
	}

	public void setClassification(String value) {
	 takeStoredValueForKey(value, CLASSIFICATION_KEY);
	}

	public String classificationDom() {
	 return (String) storedValueForKey(CLASSIFICATION_DOM_KEY);
	}

	public void setClassificationDom(String value) {
	 takeStoredValueForKey(value, CLASSIFICATION_DOM_KEY);
	}

	public String comments() {
	 return (String) storedValueForKey(COMMENTS_KEY);
	}

	public void setComments(String value) {
	 takeStoredValueForKey(value, COMMENTS_KEY);
	}

	public NSTimestamp dateCreation() {
	 return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_CREATION_KEY);
	}

	public String description() {
	 return (String) storedValueForKey(DESCRIPTION_KEY);
	}

	public void setDescription(String value) {
	 takeStoredValueForKey(value, DESCRIPTION_KEY);
	}

	public String manufacturerName() {
	 return (String) storedValueForKey(MANUFACTURER_NAME_KEY);
	}

	public void setManufacturerName(String value) {
	 takeStoredValueForKey(value, MANUFACTURER_NAME_KEY);
	}

	public String manufacturerPartID() {
	 return (String) storedValueForKey(MANUFACTURER_PART_ID_KEY);
	}

	public void setManufacturerPartID(String value) {
	 takeStoredValueForKey(value, MANUFACTURER_PART_ID_KEY);
	}

	public Integer persIdCreation() {
	 return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}

	public String supplierPartAuxiliaryId() {
	 return (String) storedValueForKey(SUPPLIER_PART_AUXILIARY_ID_KEY);
	}

	public void setSupplierPartAuxiliaryId(String value) {
	 takeStoredValueForKey(value, SUPPLIER_PART_AUXILIARY_ID_KEY);
	}

	public String supplierPartId() {
	 return (String) storedValueForKey(SUPPLIER_PART_ID_KEY);
	}

	public void setSupplierPartId(String value) {
	 takeStoredValueForKey(value, SUPPLIER_PART_ID_KEY);
	}

	public String unitOfMeasure() {
	 return (String) storedValueForKey(UNIT_OF_MEASURE_KEY);
	}

	public void setUnitOfMeasure(String value) {
	 takeStoredValueForKey(value, UNIT_OF_MEASURE_KEY);
	}

	public java.math.BigDecimal unitPrice() {
	 return (java.math.BigDecimal) storedValueForKey(UNIT_PRICE_KEY);
	}

	public void setUnitPrice(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, UNIT_PRICE_KEY);
	}

	public String url() {
	 return (String) storedValueForKey(URL_KEY);
	}

	public void setUrl(String value) {
	 takeStoredValueForKey(value, URL_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOArticle toArticle() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOArticle)storedValueForKey(TO_ARTICLE_KEY);
	}

	public void setToArticleRelationship(org.cocktail.fwkcktldepense.server.metier.EOArticle value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOArticle oldValue = toArticle();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ARTICLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_ARTICLE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam toFournisB2bCxmlParam() {
	 return (org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam)storedValueForKey(TO_FOURNIS_B2B_CXML_PARAM_KEY);
	}

	public void setToFournisB2bCxmlParamRelationship(org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam oldValue = toFournisB2bCxmlParam();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FOURNIS_B2B_CXML_PARAM_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TO_FOURNIS_B2B_CXML_PARAM_KEY);
	 }
	}


	/**
	* Créer une instance de EOB2bCxmlItem avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOB2bCxmlItem createEOB2bCxmlItem(EOEditingContext editingContext				, String classification
							, String classificationDom
									, NSTimestamp dateCreation
							, String description
							, String manufacturerName
							, String manufacturerPartID
											, String supplierPartId
							, String unitOfMeasure
							, java.math.BigDecimal unitPrice
							, org.cocktail.fwkcktldepense.server.metier.EOArticle toArticle		, org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam toFournisB2bCxmlParam					) {
	 EOB2bCxmlItem eo = (EOB2bCxmlItem) EOUtilities.createAndInsertInstance(editingContext, _EOB2bCxmlItem.ENTITY_NAME);	 
							eo.setClassification(classification);
									eo.setClassificationDom(classificationDom);
											eo.setDateCreation(dateCreation);
									eo.setDescription(description);
									eo.setManufacturerName(manufacturerName);
									eo.setManufacturerPartID(manufacturerPartID);
													eo.setSupplierPartId(supplierPartId);
									eo.setUnitOfMeasure(unitOfMeasure);
									eo.setUnitPrice(unitPrice);
								 eo.setToArticleRelationship(toArticle);
				 eo.setToFournisB2bCxmlParamRelationship(toFournisB2bCxmlParam);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOB2bCxmlItem creerInstance(EOEditingContext editingContext) {
		EOB2bCxmlItem object = (EOB2bCxmlItem)EOUtilities.createAndInsertInstance(editingContext, _EOB2bCxmlItem.ENTITY_NAME);
  		return object;
		}

	

  public EOB2bCxmlItem localInstanceIn(EOEditingContext editingContext) {
    EOB2bCxmlItem localInstance = (EOB2bCxmlItem)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOB2bCxmlItem fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOB2bCxmlItem fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOB2bCxmlItem> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOB2bCxmlItem eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOB2bCxmlItem)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOB2bCxmlItem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOB2bCxmlItem fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOB2bCxmlItem> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOB2bCxmlItem eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOB2bCxmlItem)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOB2bCxmlItem fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOB2bCxmlItem eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOB2bCxmlItem ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOB2bCxmlItem fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
