/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.finder.FinderInfosReversement;
import org.cocktail.fwkcktldepense.server.finder.FinderInventaire;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptableInventoriable;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EODepenseControlePlanComptable extends _EODepenseControlePlanComptable implements _IDepenseControlePlanComptable, IDepenseControle {
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	private static final long serialVersionUID = 1L;
	private BigDecimal pourcentage;
	public static String DPCO_POURCENTAGE = "pourcentage";
	private NSMutableArray<EOInventaire> inventaires;
	private BigDecimal maxReversement;

	public EODepenseControlePlanComptable() {
		super();
		setPourcentage(new BigDecimal(0.0));
		maxReversement = new BigDecimal(0.0);
	}
	public void setMontantHT(BigDecimal montant) {
		setDpcoHtSaisie(montant);
	}
	public void setMontantTTC(BigDecimal montant) {
		setDpcoTtcSaisie(montant);
	}
	public void setMontantTVA(BigDecimal montant) {
		setDpcoTvaSaisie(montant);
	}
	public void setMontantBudgetaire(BigDecimal montant) {
		setDpcoMontantBudgetaire(montant);
	}
	public BigDecimal getMontantHT() {
		return dpcoHtSaisie();
	}
	public BigDecimal getMontantTTC() {
		return dpcoTtcSaisie();
	}
	public BigDecimal getMontantTVA() {
		return dpcoTvaSaisie();
	}
	public BigDecimal getMontantBudgetaire() {
		return dpcoMontantBudgetaire();
	}
	public BigDecimal getPourcentage() {
		return pourcentage();
	}

	public BigDecimal getSommeRepartition() {
		return FinderInfosReversement.getSumPlanComptable(editingContext(), this);
	}

	public String getLabelCode() {
		return "planComptable";
	}
	
	public Object getCode() {
		return this.planComptable();
	}

	public void setMaxReversement(BigDecimal value) {
		if (value == null)
			value = new BigDecimal(0.0);
		maxReversement = value;
	}

	public BigDecimal maxReversement() {
		if (maxReversement == null)
			setMaxReversement(new BigDecimal(0.0));
		return maxReversement;
	}

	public boolean isGood() {
		if (!isInventaireObligatoire())
			return true;
		if (inventaires == null)
			inventaires = new NSMutableArray<EOInventaire>();
		if (dpcoMontantBudgetaire().floatValue() != computeSumForKey(inventaires, EOInventaire.LID_MONTANT_KEY).floatValue())
			return false;
		return true;
	}

	private BigDecimal computeSumForKey(NSArray<? extends EOEnterpriseObject> eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	public boolean isInventaireObligatoire() {
		if (!isInventaireAffichable())
			return false;
		if (!FinderParametre.getParametreInventaireObligatoire(editingContext(), depenseBudget().exercice()))
			return false;
		return true;
	}

	public boolean isInventaireAffichable() {
		if (planComptable() == null || depenseBudget() == null || depenseBudget().exercice() == null)
			return false;
		if (FinderPlanComptableInventoriable.getPlanComptableInventoriables(editingContext(), planComptable(), depenseBudget().exercice()).count() > 0)
			return true;
		return false;
	}

	public boolean isInventairesPossibles() {
		if (getInventairesPossibles().count() == 0)
			return false;
		return true;
	}

	public NSArray<EOInventaire> inventairesLiquides() {
		return FinderInventaire.getInventaires(editingContext(), this);
	}

	public NSArray<EOInventaire> getInventairesPossibles() {

		// Cas pour reversements
		if (depenseBudget() != null && depenseBudget().depenseBudgetReversement() != null) {
			if (depenseBudget().depenseBudgetReversement().depenseControlePlanComptables() == null ||
					depenseBudget().depenseBudgetReversement().depenseControlePlanComptables().count() == 0)
				return NSArray.emptyArray();
			EODepenseControlePlanComptable depenseControlePlanComptable = (EODepenseControlePlanComptable)
					depenseBudget().depenseBudgetReversement().depenseControlePlanComptables().objectAtIndex(0);
			return FinderInventaire.getInventaires(editingContext(), depenseControlePlanComptable);
		}

		// Cas liquidation classique
		if (depenseBudget() == null || depenseBudget().depensePapier() == null || depenseBudget().depensePapier().commande() == null ||
				depenseBudget().engagementBudget() == null)
			return NSArray.emptyArray();

		NSMutableArray<EOInventaire> lesInventaires = new NSMutableArray<EOInventaire>();
		lesInventaires.addObjectsFromArray(FinderInventaire.getInventaires(editingContext(), depenseBudget().depensePapier().commande(),
				depenseBudget().engagementBudget()));

		EODepensePapier depensePapier = depenseBudget().depensePapier();
		for (int i = 0; i < depensePapier.depenseBudgets().count(); i++) {
			EODepenseBudget depenseBudget = (EODepenseBudget) depensePapier.depenseBudgets().objectAtIndex(i);

			for (int j = 0; j < depenseBudget.depenseControlePlanComptables().count(); j++) {
				EODepenseControlePlanComptable depenseControlePlanComptable =
						(EODepenseControlePlanComptable) depenseBudget.depenseControlePlanComptables().objectAtIndex(j);

				//if (depenseControlePlanComptable==this)
				//	continue;

				for (int k = 0; k < depenseControlePlanComptable.inventaires().count(); k++)
					lesInventaires.removeIdenticalObject((EOInventaire) depenseControlePlanComptable.
							inventaires().objectAtIndex(k));
			}
		}

		return lesInventaires;
	}

	/**
	 * Les inventaires en cours de creation.
	 * 
	 * @see EODepenseControlePlanComptable#inventairesLiquides()
	 * @return
	 */
	public NSArray<EOInventaire> inventaires() {
		if (inventaires == null)
			inventaires = new NSMutableArray<EOInventaire>();
		return inventaires;
	}

	public void ajouterInventaires(NSArray<EOInventaire> inventaires) {
		if (inventaires == null || inventaires.count() == 0)
			return;
		for (int i = 0; i < inventaires.count(); i++)
			ajouterInventaire((EOInventaire) inventaires.objectAtIndex(i));
	}

	public void ajouterInventairesReversement(NSArray<EOInventaire> inventaires) {
		if (inventaires == null || inventaires.count() == 0)
			return;
		for (int i = 0; i < inventaires.count(); i++) {
			EOInventaire inventaire = (EOInventaire) inventaires.objectAtIndex(i);
			inventaire.preparePourReversement();
			ajouterInventaire(inventaire);
		}
	}

	public void ajouterInventairesReimputation(NSArray<EOInventaire> inventaires) {
		if (inventaires == null || inventaires.count() == 0)
			return;
		for (int i = 0; i < inventaires.count(); i++) {
			EOInventaire inventaire = (EOInventaire) inventaires.objectAtIndex(i);
			//	inventaire.preparePourReversement();
			ajouterInventaire(inventaire);
			if (inventaire.lidMontant().floatValue() < 0) {
				inventaire.setMaxReversement(inventaire.lidMontant());
			}
		}
	}

	public void ajouterInventaire(EOInventaire inventaire) {
		if (inventaires == null)
			inventaires = new NSMutableArray<EOInventaire>();

		if (inventaires.containsObject(inventaire) == false)
			inventaires.addObject(inventaire);
	}

	public void supprimerInventaire(EOInventaire inventaire) {
		if (inventaires == null)
			inventaires = new NSMutableArray<EOInventaire>();

		inventaires.removeIdenticalObject(inventaire);
	}

	public BigDecimal montantTtc() {
		return dpcoTtcSaisie();
	}

	public void setMontantTtc(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && !depenseBudget().isReversement() && aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		if (depenseBudget() != null && depenseBudget().isReversement() && aValue.floatValue() > 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		if (depenseBudget() != null && depenseBudget().depTtcSaisie() != null && depenseBudget().depTtcSaisie().floatValue() != 0.0)
			setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
					divide(depenseBudget().depTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
	}

	public void setPourcentage(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		aValue = aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
		if (aValue.floatValue() > 100.0)
			aValue = new BigDecimal(100.0);
		pourcentage = aValue;

		if (depenseBudget() != null) {
			NSArray<EODepenseControlePlanComptable> depenseControlePlanComptables = depenseBudget().depenseControlePlanComptables();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EODepenseControlePlanComptable.PLAN_COMPTABLE_KEY + " != nil", null);
			depenseControlePlanComptables = EOQualifier.filteredArrayWithQualifier(depenseControlePlanComptables, qual);
			BigDecimal total = depenseBudget().computeSumForKey(depenseControlePlanComptables, EODepenseControlePlanComptable.DPCO_POURCENTAGE);
			if (total.floatValue() > 100.0)
				pourcentage = new BigDecimal(100.0).subtract(total.subtract(aValue));
		}

		if (depenseBudget() != null)
			depenseBudget().corrigerMontantPlanComptables();
	}

	public BigDecimal pourcentage() {
		return pourcentage;
	}

	public void setDpcoHtSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoHtSaisie(aValue);
	}

	public void setDpcoTvaSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoTvaSaisie(aValue);
	}

	public void setDpcoTtcSaisie(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoTtcSaisie(aValue);
	}

	public void setDpcoMontantBudgetaire(BigDecimal aValue) {
		int arrondi = EODevise.defaultNbDecimales;
		if (depenseBudget() != null)
			arrondi = depenseBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDpcoMontantBudgetaire(aValue);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (dpcoHtSaisie() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoHtSaisieManquant);
		if (dpcoTvaSaisie() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoTvaSaisieManquant);
		if (dpcoTtcSaisie() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoTtcSaisieManquant);
		if (dpcoMontantBudgetaire() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.dpcoMontantBudgetaireManquant);

		if (exercice() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.exerciceManquant);
		if (depenseBudget() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.depenseBudgetManquant);

		if (!dpcoHtSaisie().abs().add(dpcoTvaSaisie().abs()).equals(dpcoTtcSaisie().abs()))
			setDpcoTvaSaisie(dpcoTtcSaisie().subtract(dpcoHtSaisie()));
		if (!dpcoMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDpcoMontantBudgetaire(calculMontantBudgetaire());

		if (!depenseBudget().exercice().equals(exercice()))
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.depenseBudgetExercicePasCoherent);

		// TODO : rajouter le test suivant le mode de paiement ... l'existence d'une ecritureDetail .. 
		// et eventuellement que le montant de cette ecriture soit >= au montant du ttc d'ici ... 
	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(dpcoHtSaisie(), dpcoTvaSaisie(), depenseBudget().tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (planComptable() == null)
			throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.planComptableManquant);
		if (isInventaireObligatoire()) {
			if (computeSumForKey(inventaires(), EOInventaire.LID_MONTANT_KEY).floatValue() != dpcoMontantBudgetaire().floatValue())
				throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.montantInventairesIncoherent);

			// Cas pour reversements
			if (depenseBudget() != null && depenseBudget().depenseBudgetReversement() != null) {
				for (int i = 0; i < inventaires().count(); i++) {
					EOInventaire inventaire = (EOInventaire) inventaires.objectAtIndex(i);
					if (inventaire.lidMontant().floatValue() > 0.0)
						throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.montantInventaireIncoherent);
					if (inventaire.lidMontant().abs().floatValue() > inventaire.maxReversement().abs().floatValue())
						throw new DepenseControlePlanComptableException(DepenseControlePlanComptableException.montantInventaireIncoherent);
				}
			}
		}
	}
	public boolean isNull() {
		return false;
	}
}
