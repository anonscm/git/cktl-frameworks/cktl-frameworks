/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.util.Calculs;
import org.cocktail.fwkcktldepense.server.util.MontantTransaction;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOTauxProrata extends _EOTauxProrata {

	private static Calculs serviceCalcul = Calculs.getInstance();

	public static enum ETypeMontant {
		HT, TTC
	};

	public EOTauxProrata() {
		super();
	}

	/**
	 * @param ht
	 * @param tva le montant de la tva (pas le taux)
	 * @return
	 */
	@Deprecated
	public BigDecimal montantBudgetaire(BigDecimal ht, BigDecimal tva) {
		return serviceCalcul.calculMontantBudgetaire(ht, tva, tapTaux());
	}

	public NSDictionary<ETypeMontant, BigDecimal> htEtTtcAPartirMontantBudgetaireDispo(BigDecimal htOrigine, BigDecimal ttcOrigine, BigDecimal dispo) {
		NSMutableDictionary<ETypeMontant, BigDecimal> dico = new NSMutableDictionary<ETypeMontant, BigDecimal>();
		MontantTransaction transaction = serviceCalcul.calculMontantsProratiseAPartirDisponible(htOrigine, ttcOrigine, dispo, tapTaux());

		dico.setObjectForKey(transaction.getMontantHT().setScale(2, RoundingMode.HALF_UP), ETypeMontant.HT);
		dico.setObjectForKey(transaction.getMontantTTC().setScale(2, RoundingMode.HALF_UP), ETypeMontant.TTC);
		return dico;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		throw new FactoryException(ENTITY_NAME + " : " + FactoryException.messageErreur);
	}
}
