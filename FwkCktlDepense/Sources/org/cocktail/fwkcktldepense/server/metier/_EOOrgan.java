/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOOrgan.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOOrgan extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleOrgan";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_ORGAN";


//Attribute Keys
	public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
	public static final ERXKey<Integer> ORG_CANAL_OBLIGATOIRE = new ERXKey<Integer>("orgCanalObligatoire");
	public static final ERXKey<Integer> ORG_CONVENTION_OBLIGATOIRE = new ERXKey<Integer>("orgConventionObligatoire");
	public static final ERXKey<String> ORG_CR = new ERXKey<String>("orgCr");
	public static final ERXKey<NSTimestamp> ORG_DATE_CLOTURE = new ERXKey<NSTimestamp>("orgDateCloture");
	public static final ERXKey<NSTimestamp> ORG_DATE_OUVERTURE = new ERXKey<NSTimestamp>("orgDateOuverture");
	public static final ERXKey<String> ORG_ETAB = new ERXKey<String>("orgEtab");
	public static final ERXKey<String> ORG_LIBELLE = new ERXKey<String>("orgLibelle");
	public static final ERXKey<Integer> ORG_LUCRATIVITE = new ERXKey<Integer>("orgLucrativite");
	public static final ERXKey<Integer> ORG_NIVEAU = new ERXKey<Integer>("orgNiveau");
	public static final ERXKey<String> ORG_SOUSCR = new ERXKey<String>("orgSouscr");
	public static final ERXKey<String> ORG_UB = new ERXKey<String>("orgUb");
	public static final ERXKey<String> ORG_UNIV = new ERXKey<String>("orgUniv");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> ORGAN_EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice>("organExercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN_FILS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organFils");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> ORGAN_PERE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan>("organPere");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> ORGAN_PRORATAS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata>("organProratas");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> ORGAN_SIGNATAIRES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire>("organSignataires");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure> STRUCTURE_ULR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure>("structureUlr");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_CANAL_OBLIGATOIRE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatCanalObligatoire");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_CONVENTION_OBLIGATOIRE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatConventionObligatoire");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_OP_AUTORISEES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatOpAutorisees");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan> TYPE_ORGAN = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan>("typeOrgan");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> UTILISATEUR_ORGANS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan>("utilisateurOrgans");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "orgId";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String ORG_CANAL_OBLIGATOIRE_KEY = "orgCanalObligatoire";
	public static final String ORG_CONVENTION_OBLIGATOIRE_KEY = "orgConventionObligatoire";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_DATE_CLOTURE_KEY = "orgDateCloture";
	public static final String ORG_DATE_OUVERTURE_KEY = "orgDateOuverture";
	public static final String ORG_ETAB_KEY = "orgEtab";
	public static final String ORG_LIBELLE_KEY = "orgLibelle";
	public static final String ORG_LUCRATIVITE_KEY = "orgLucrativite";
	public static final String ORG_NIVEAU_KEY = "orgNiveau";
	public static final String ORG_SOUSCR_KEY = "orgSouscr";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String ORG_UNIV_KEY = "orgUniv";

//Attributs non visibles
	public static final String LOG_ORDRE_KEY = "logOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORG_OP_AUTORISEES_KEY = "orgOpAutorisees";
	public static final String ORG_PERE_KEY = "orgPere";
	public static final String TYOR_ID_KEY = "tyorId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String ORG_CANAL_OBLIGATOIRE_COLKEY = "ORG_CANAL_OBLIGATOIRE";
	public static final String ORG_CONVENTION_OBLIGATOIRE_COLKEY = "ORG_CONVENTION_OBLIGATOIRE";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_DATE_CLOTURE_COLKEY = "ORG_DATE_CLOTURE";
	public static final String ORG_DATE_OUVERTURE_COLKEY = "ORG_DATE_OUVERTURE";
	public static final String ORG_ETAB_COLKEY = "ORG_ETAB";
	public static final String ORG_LIBELLE_COLKEY = "ORG_LIB";
	public static final String ORG_LUCRATIVITE_COLKEY = "ORG_LUCRATIVITE";
	public static final String ORG_NIVEAU_COLKEY = "ORG_NIV";
	public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String ORG_UNIV_COLKEY = "ORG_UNIV";

	public static final String LOG_ORDRE_COLKEY = "LOG_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORG_OP_AUTORISEES_COLKEY = "ORG_OP_AUTORISEES";
	public static final String ORG_PERE_COLKEY = "ORG_PERE";
	public static final String TYOR_ID_COLKEY = "TYOR_ID";


	// Relationships
	public static final String ORGAN_EXERCICE_KEY = "organExercice";
	public static final String ORGAN_FILS_KEY = "organFils";
	public static final String ORGAN_PERE_KEY = "organPere";
	public static final String ORGAN_PRORATAS_KEY = "organProratas";
	public static final String ORGAN_SIGNATAIRES_KEY = "organSignataires";
	public static final String STRUCTURE_ULR_KEY = "structureUlr";
	public static final String TYPE_ETAT_CANAL_OBLIGATOIRE_KEY = "typeEtatCanalObligatoire";
	public static final String TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY = "typeEtatConventionObligatoire";
	public static final String TYPE_ETAT_OP_AUTORISEES_KEY = "typeEtatOpAutorisees";
	public static final String TYPE_ORGAN_KEY = "typeOrgan";
	public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";



	// Accessors methods
	public String cStructure() {
	 return (String) storedValueForKey(C_STRUCTURE_KEY);
	}

	public void setCStructure(String value) {
	 takeStoredValueForKey(value, C_STRUCTURE_KEY);
	}

	public Integer orgCanalObligatoire() {
	 return (Integer) storedValueForKey(ORG_CANAL_OBLIGATOIRE_KEY);
	}

	public void setOrgCanalObligatoire(Integer value) {
	 takeStoredValueForKey(value, ORG_CANAL_OBLIGATOIRE_KEY);
	}

	public Integer orgConventionObligatoire() {
	 return (Integer) storedValueForKey(ORG_CONVENTION_OBLIGATOIRE_KEY);
	}

	public void setOrgConventionObligatoire(Integer value) {
	 takeStoredValueForKey(value, ORG_CONVENTION_OBLIGATOIRE_KEY);
	}

	public String orgCr() {
	 return (String) storedValueForKey(ORG_CR_KEY);
	}

	public void setOrgCr(String value) {
	 takeStoredValueForKey(value, ORG_CR_KEY);
	}

	public NSTimestamp orgDateCloture() {
	 return (NSTimestamp) storedValueForKey(ORG_DATE_CLOTURE_KEY);
	}

	public void setOrgDateCloture(NSTimestamp value) {
	 takeStoredValueForKey(value, ORG_DATE_CLOTURE_KEY);
	}

	public NSTimestamp orgDateOuverture() {
	 return (NSTimestamp) storedValueForKey(ORG_DATE_OUVERTURE_KEY);
	}

	public void setOrgDateOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, ORG_DATE_OUVERTURE_KEY);
	}

	public String orgEtab() {
	 return (String) storedValueForKey(ORG_ETAB_KEY);
	}

	public void setOrgEtab(String value) {
	 takeStoredValueForKey(value, ORG_ETAB_KEY);
	}

	public String orgLibelle() {
	 return (String) storedValueForKey(ORG_LIBELLE_KEY);
	}

	public void setOrgLibelle(String value) {
	 takeStoredValueForKey(value, ORG_LIBELLE_KEY);
	}

	public Integer orgLucrativite() {
	 return (Integer) storedValueForKey(ORG_LUCRATIVITE_KEY);
	}

	public void setOrgLucrativite(Integer value) {
	 takeStoredValueForKey(value, ORG_LUCRATIVITE_KEY);
	}

	public Integer orgNiveau() {
	 return (Integer) storedValueForKey(ORG_NIVEAU_KEY);
	}

	public void setOrgNiveau(Integer value) {
	 takeStoredValueForKey(value, ORG_NIVEAU_KEY);
	}

	public String orgSouscr() {
	 return (String) storedValueForKey(ORG_SOUSCR_KEY);
	}

	public void setOrgSouscr(String value) {
	 takeStoredValueForKey(value, ORG_SOUSCR_KEY);
	}

	public String orgUb() {
	 return (String) storedValueForKey(ORG_UB_KEY);
	}

	public void setOrgUb(String value) {
	 takeStoredValueForKey(value, ORG_UB_KEY);
	}

	public String orgUniv() {
	 return (String) storedValueForKey(ORG_UNIV_KEY);
	}

	public void setOrgUniv(String value) {
	 takeStoredValueForKey(value, ORG_UNIV_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOOrgan organPere() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan)storedValueForKey(ORGAN_PERE_KEY);
	}

	public void setOrganPereRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOOrgan oldValue = organPere();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_PERE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_PERE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOStructure structureUlr() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOStructure)storedValueForKey(STRUCTURE_ULR_KEY);
	}

	public void setStructureUlrRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOStructure oldValue = structureUlr();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_ULR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatCanalObligatoire() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_CANAL_OBLIGATOIRE_KEY);
	}

	public void setTypeEtatCanalObligatoireRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatCanalObligatoire();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_CANAL_OBLIGATOIRE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_CANAL_OBLIGATOIRE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatConventionObligatoire() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY);
	}

	public void setTypeEtatConventionObligatoireRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatConventionObligatoire();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatOpAutorisees() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_OP_AUTORISEES_KEY);
	}

	public void setTypeEtatOpAutoriseesRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatOpAutorisees();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_OP_AUTORISEES_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_OP_AUTORISEES_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan typeOrgan() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan)storedValueForKey(TYPE_ORGAN_KEY);
	}

	public void setTypeOrganRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan oldValue = typeOrgan();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ORGAN_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORGAN_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> organExercice() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice>)storedValueForKey(ORGAN_EXERCICE_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> organExercice(EOQualifier qualifier) {
	 return organExercice(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> organExercice(EOQualifier qualifier, boolean fetch) {
	 return organExercice(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> organExercice(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOOrganExercice.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOOrganExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = organExercice();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToOrganExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganExercice object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
	}
	
	public void removeFromOrganExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOOrganExercice createOrganExerciceRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOOrganExercice.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_EXERCICE_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrganExercice) eo;
	}
	
	public void deleteOrganExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllOrganExerciceRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOOrganExercice> objects = organExercice().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteOrganExerciceRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organFils() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)storedValueForKey(ORGAN_FILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organFils(EOQualifier qualifier) {
	 return organFils(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organFils(EOQualifier qualifier, boolean fetch) {
	 return organFils(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organFils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORGAN_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = organFils();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToOrganFilsRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
	}
	
	public void removeFromOrganFilsRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOOrgan createOrganFilsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_FILS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrgan) eo;
	}
	
	public void deleteOrganFilsRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllOrganFilsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOOrgan> objects = organFils().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteOrganFilsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> organProratas() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata>)storedValueForKey(ORGAN_PRORATAS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> organProratas(EOQualifier qualifier) {
	 return organProratas(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> organProratas(EOQualifier qualifier, boolean fetch) {
	 return organProratas(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> organProratas(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOOrganProrata.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOOrganProrata.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = organProratas();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToOrganProratasRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganProrata object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
	}
	
	public void removeFromOrganProratasRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganProrata object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOOrganProrata createOrganProratasRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOOrganProrata.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_PRORATAS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrganProrata) eo;
	}
	
	public void deleteOrganProratasRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganProrata object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllOrganProratasRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOOrganProrata> objects = organProratas().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteOrganProratasRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> organSignataires() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire>)storedValueForKey(ORGAN_SIGNATAIRES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> organSignataires(EOQualifier qualifier) {
	 return organSignataires(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> organSignataires(EOQualifier qualifier, boolean fetch) {
	 return organSignataires(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> organSignataires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = organSignataires();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToOrganSignatairesRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
	}
	
	public void removeFromOrganSignatairesRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire createOrganSignatairesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_SIGNATAIRES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire) eo;
	}
	
	public void deleteOrganSignatairesRelationship(org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllOrganSignatairesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire> objects = organSignataires().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteOrganSignatairesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> utilisateurOrgans() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan>)storedValueForKey(UTILISATEUR_ORGANS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> utilisateurOrgans(EOQualifier qualifier) {
	 return utilisateurOrgans(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> utilisateurOrgans(EOQualifier qualifier, boolean fetch) {
	 return utilisateurOrgans(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> utilisateurOrgans(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = utilisateurOrgans();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToUtilisateurOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
	}
	
	public void removeFromUtilisateurOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan createUtilisateurOrgansRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_ORGANS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan) eo;
	}
	
	public void deleteUtilisateurOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllUtilisateurOrgansRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan> objects = utilisateurOrgans().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteUtilisateurOrgansRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOOrgan avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOOrgan createEOOrgan(EOEditingContext editingContext														, NSTimestamp orgDateOuverture
									, String orgLibelle
							, Integer orgLucrativite
							, Integer orgNiveau
											, String orgUniv
															, org.cocktail.fwkcktldepense.server.metier.EOTypeOrgan typeOrgan					) {
	 EOOrgan eo = (EOOrgan) EOUtilities.createAndInsertInstance(editingContext, _EOOrgan.ENTITY_NAME);	 
																	eo.setOrgDateOuverture(orgDateOuverture);
											eo.setOrgLibelle(orgLibelle);
									eo.setOrgLucrativite(orgLucrativite);
									eo.setOrgNiveau(orgNiveau);
													eo.setOrgUniv(orgUniv);
																 eo.setTypeOrganRelationship(typeOrgan);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrgan creerInstance(EOEditingContext editingContext) {
		EOOrgan object = (EOOrgan)EOUtilities.createAndInsertInstance(editingContext, _EOOrgan.ENTITY_NAME);
  		return object;
		}

	

  public EOOrgan localInstanceIn(EOEditingContext editingContext) {
    EOOrgan localInstance = (EOOrgan)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOOrgan>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOrgan fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrgan fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOOrgan> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrgan)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOOrgan> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrgan)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrgan fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrgan eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrgan ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrgan fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
