/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCodeExer.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCodeExer extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCodeExer";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CODE_EXER";


//Attribute Keys
	public static final ERXKey<Integer> CE3CMP = new ERXKey<Integer>("ce3cmp");
	public static final ERXKey<String> CE_ACTIF = new ERXKey<String>("ceActif");
	public static final ERXKey<Integer> CE_AUTRES = new ERXKey<Integer>("ceAutres");
	public static final ERXKey<java.math.BigDecimal> CE_CONTROLE = new ERXKey<java.math.BigDecimal>("ceControle");
	public static final ERXKey<Integer> CE_MONOPOLE = new ERXKey<Integer>("ceMonopole");
	public static final ERXKey<String> CE_RECH = new ERXKey<String>("ceRech");
	public static final ERXKey<String> CE_SUPPR = new ERXKey<String>("ceSuppr");
	public static final ERXKey<String> CE_TYPE = new ERXKey<String>("ceType");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche> CODE_MARCHE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarche>("codeMarche");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> CODE_MARCHE_FOURS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour>("codeMarcheFours");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ceOrdre";

	public static final String CE3CMP_KEY = "ce3cmp";
	public static final String CE_ACTIF_KEY = "ceActif";
	public static final String CE_AUTRES_KEY = "ceAutres";
	public static final String CE_CONTROLE_KEY = "ceControle";
	public static final String CE_MONOPOLE_KEY = "ceMonopole";
	public static final String CE_RECH_KEY = "ceRech";
	public static final String CE_SUPPR_KEY = "ceSuppr";
	public static final String CE_TYPE_KEY = "ceType";

//Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CE3CMP_COLKEY = "CE_3CMP";
	public static final String CE_ACTIF_COLKEY = "CE_ACTIF";
	public static final String CE_AUTRES_COLKEY = "CE_AUTRES";
	public static final String CE_CONTROLE_COLKEY = "CE_CONTROLE";
	public static final String CE_MONOPOLE_COLKEY = "CE_MONOPOLE";
	public static final String CE_RECH_COLKEY = "CE_RECH";
	public static final String CE_SUPPR_COLKEY = "CE_SUPPR";
	public static final String CE_TYPE_COLKEY = "CE_TYPE";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_MARCHE_KEY = "codeMarche";
	public static final String CODE_MARCHE_FOURS_KEY = "codeMarcheFours";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public Integer ce3cmp() {
	 return (Integer) storedValueForKey(CE3CMP_KEY);
	}

	public void setCe3cmp(Integer value) {
	 takeStoredValueForKey(value, CE3CMP_KEY);
	}

	public String ceActif() {
	 return (String) storedValueForKey(CE_ACTIF_KEY);
	}

	public void setCeActif(String value) {
	 takeStoredValueForKey(value, CE_ACTIF_KEY);
	}

	public Integer ceAutres() {
	 return (Integer) storedValueForKey(CE_AUTRES_KEY);
	}

	public void setCeAutres(Integer value) {
	 takeStoredValueForKey(value, CE_AUTRES_KEY);
	}

	public java.math.BigDecimal ceControle() {
	 return (java.math.BigDecimal) storedValueForKey(CE_CONTROLE_KEY);
	}

	public void setCeControle(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CE_CONTROLE_KEY);
	}

	public Integer ceMonopole() {
	 return (Integer) storedValueForKey(CE_MONOPOLE_KEY);
	}

	public void setCeMonopole(Integer value) {
	 takeStoredValueForKey(value, CE_MONOPOLE_KEY);
	}

	public String ceRech() {
	 return (String) storedValueForKey(CE_RECH_KEY);
	}

	public void setCeRech(String value) {
	 takeStoredValueForKey(value, CE_RECH_KEY);
	}

	public String ceSuppr() {
	 return (String) storedValueForKey(CE_SUPPR_KEY);
	}

	public void setCeSuppr(String value) {
	 takeStoredValueForKey(value, CE_SUPPR_KEY);
	}

	public String ceType() {
	 return (String) storedValueForKey(CE_TYPE_KEY);
	}

	public void setCeType(String value) {
	 takeStoredValueForKey(value, CE_TYPE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeMarche codeMarche() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
	}

	public void setCodeMarcheRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarche value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeMarche oldValue = codeMarche();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> codeMarcheFours() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour>)storedValueForKey(CODE_MARCHE_FOURS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> codeMarcheFours(EOQualifier qualifier) {
	 return codeMarcheFours(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> codeMarcheFours(EOQualifier qualifier, boolean fetch) {
	 return codeMarcheFours(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> codeMarcheFours(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour.CODE_EXER_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = codeMarcheFours();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCodeMarcheFoursRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, CODE_MARCHE_FOURS_KEY);
	}
	
	public void removeFromCodeMarcheFoursRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_MARCHE_FOURS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour createCodeMarcheFoursRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, CODE_MARCHE_FOURS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour) eo;
	}
	
	public void deleteCodeMarcheFoursRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_MARCHE_FOURS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCodeMarcheFoursRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour> objects = codeMarcheFours().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCodeMarcheFoursRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCodeExer avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCodeExer createEOCodeExer(EOEditingContext editingContext						, String ceActif
																	, org.cocktail.fwkcktldepense.server.metier.EOCodeMarche codeMarche		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOCodeExer eo = (EOCodeExer) EOUtilities.createAndInsertInstance(editingContext, _EOCodeExer.ENTITY_NAME);	 
									eo.setCeActif(ceActif);
																		 eo.setCodeMarcheRelationship(codeMarche);
				 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeExer creerInstance(EOEditingContext editingContext) {
		EOCodeExer object = (EOCodeExer)EOUtilities.createAndInsertInstance(editingContext, _EOCodeExer.ENTITY_NAME);
  		return object;
		}

	

  public EOCodeExer localInstanceIn(EOEditingContext editingContext) {
    EOCodeExer localInstance = (EOCodeExer)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeExer fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeExer fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCodeExer> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeExer)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCodeExer> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeExer)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeExer fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeExer eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeExer ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeExer fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> objectsForRecherche(EOEditingContext ec,
														Integer ce3cmpBinding,
														String ceActifBinding,
														Integer ceAutresBinding,
														Integer ceMonopoleBinding,
														String ceRechBinding,
														String ceSupprBinding,
														String cmSupprBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														Integer niveauBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCodeExer.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (ce3cmpBinding != null)
			bindings.takeValueForKey(ce3cmpBinding, "ce3cmp");
		  if (ceActifBinding != null)
			bindings.takeValueForKey(ceActifBinding, "ceActif");
		  if (ceAutresBinding != null)
			bindings.takeValueForKey(ceAutresBinding, "ceAutres");
		  if (ceMonopoleBinding != null)
			bindings.takeValueForKey(ceMonopoleBinding, "ceMonopole");
		  if (ceRechBinding != null)
			bindings.takeValueForKey(ceRechBinding, "ceRech");
		  if (ceSupprBinding != null)
			bindings.takeValueForKey(ceSupprBinding, "ceSuppr");
		  if (cmSupprBinding != null)
			bindings.takeValueForKey(cmSupprBinding, "cmSuppr");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (niveauBinding != null)
			bindings.takeValueForKey(niveauBinding, "niveau");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
