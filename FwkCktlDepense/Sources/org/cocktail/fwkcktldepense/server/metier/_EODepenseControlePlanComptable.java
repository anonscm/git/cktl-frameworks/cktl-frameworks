/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EODepenseControlePlanComptable.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EODepenseControlePlanComptable extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleDepenseControlePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_CTRL_PLANCO";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> DPCO_HT_SAISIE = new ERXKey<java.math.BigDecimal>("dpcoHtSaisie");
	public static final ERXKey<java.math.BigDecimal> DPCO_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("dpcoMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> DPCO_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("dpcoTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DPCO_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("dpcoTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> DEPENSE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("depenseBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail> ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail>("ecritureDetail");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOMandat> MANDAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOMandat>("mandat");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dpcoId";

	public static final String DPCO_HT_SAISIE_KEY = "dpcoHtSaisie";
	public static final String DPCO_MONTANT_BUDGETAIRE_KEY = "dpcoMontantBudgetaire";
	public static final String DPCO_TTC_SAISIE_KEY = "dpcoTtcSaisie";
	public static final String DPCO_TVA_SAISIE_KEY = "dpcoTvaSaisie";

//Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MAN_ID_KEY = "manId";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String DPCO_HT_SAISIE_COLKEY = "DPCO_HT_SAISIE";
	public static final String DPCO_MONTANT_BUDGETAIRE_COLKEY = "DPCO_MONTANT_BUDGETAIRE";
	public static final String DPCO_TTC_SAISIE_COLKEY = "DPCO_TTC_SAISIE";
	public static final String DPCO_TVA_SAISIE_COLKEY = "DPCO_TVA_SAISIE";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MAN_ID_COLKEY = "MAN_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String DEPENSE_BUDGET_KEY = "depenseBudget";
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String MANDAT_KEY = "mandat";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
	public java.math.BigDecimal dpcoHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_HT_SAISIE_KEY);
	}

	public void setDpcoHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal dpcoMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public void setDpcoMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal dpcoTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_TTC_SAISIE_KEY);
	}

	public void setDpcoTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal dpcoTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPCO_TVA_SAISIE_KEY);
	}

	public void setDpcoTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPCO_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget depenseBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_KEY);
	}

	public void setDepenseBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepenseBudget oldValue = depenseBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail ecritureDetail() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
	}

	public void setEcritureDetailRelationship(org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail oldValue = ecritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOMandat mandat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOMandat)storedValueForKey(MANDAT_KEY);
	}

	public void setMandatRelationship(org.cocktail.fwkcktldepense.server.metier.EOMandat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOMandat oldValue = mandat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}


	/**
	* Créer une instance de EODepenseControlePlanComptable avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EODepenseControlePlanComptable createEODepenseControlePlanComptable(EOEditingContext editingContext				, java.math.BigDecimal dpcoHtSaisie
							, java.math.BigDecimal dpcoMontantBudgetaire
							, java.math.BigDecimal dpcoTtcSaisie
							, java.math.BigDecimal dpcoTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EODepenseBudget depenseBudget				, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice				, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable					) {
	 EODepenseControlePlanComptable eo = (EODepenseControlePlanComptable) EOUtilities.createAndInsertInstance(editingContext, _EODepenseControlePlanComptable.ENTITY_NAME);	 
							eo.setDpcoHtSaisie(dpcoHtSaisie);
									eo.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
									eo.setDpcoTtcSaisie(dpcoTtcSaisie);
									eo.setDpcoTvaSaisie(dpcoTvaSaisie);
						 eo.setDepenseBudgetRelationship(depenseBudget);
						 eo.setExerciceRelationship(exercice);
						 eo.setPlanComptableRelationship(planComptable);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepenseControlePlanComptable creerInstance(EOEditingContext editingContext) {
		EODepenseControlePlanComptable object = (EODepenseControlePlanComptable)EOUtilities.createAndInsertInstance(editingContext, _EODepenseControlePlanComptable.ENTITY_NAME);
  		return object;
		}

	

  public EODepenseControlePlanComptable localInstanceIn(EOEditingContext editingContext) {
    EODepenseControlePlanComptable localInstance = (EODepenseControlePlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepenseControlePlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepenseControlePlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EODepenseControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepenseControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepenseControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepenseControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepenseControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EODepenseControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepenseControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepenseControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepenseControlePlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepenseControlePlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepenseControlePlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepenseControlePlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
