/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeDocument.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeDocument extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeDocument";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_DOCUMENT";


//Attribute Keys
	public static final ERXKey<NSTimestamp> COMD_DATE = new ERXKey<NSTimestamp>("comdDate");
	public static final ERXKey<String> COMD_URL = new ERXKey<String>("comdUrl");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commande");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> COMMANDE_IMPRESSION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression>("commandeImpression");
	public static final ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier> COURRIER = new ERXKey<org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier>("courrier");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeDocument> TYPE_DOCUMENT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeDocument>("typeDocument");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "comdId";

	public static final String COMD_DATE_KEY = "comdDate";
	public static final String COMD_URL_KEY = "comdUrl";

//Attributs non visibles
	public static final String CIMP_ID_KEY = "cimpId";
	public static final String COMD_ID_KEY = "comdId";
	public static final String COMM_ID_KEY = "commId";
	public static final String COU_NUMERO_KEY = "couNumero";
	public static final String TCOM_ID_KEY = "tcomId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String COMD_DATE_COLKEY = "COMD_DATE";
	public static final String COMD_URL_COLKEY = "COMD_URL";

	public static final String CIMP_ID_COLKEY = "CIMP_ID";
	public static final String COMD_ID_COLKEY = "COMD_ID";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String COU_NUMERO_COLKEY = "COU_NUMERO";
	public static final String TCOM_ID_COLKEY = "TCOM_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String COMMANDE_IMPRESSION_KEY = "commandeImpression";
	public static final String COURRIER_KEY = "courrier";
	public static final String TYPE_DOCUMENT_KEY = "typeDocument";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public NSTimestamp comdDate() {
	 return (NSTimestamp) storedValueForKey(COMD_DATE_KEY);
	}

	public void setComdDate(NSTimestamp value) {
	 takeStoredValueForKey(value, COMD_DATE_KEY);
	}

	public String comdUrl() {
	 return (String) storedValueForKey(COMD_URL_KEY);
	}

	public void setComdUrl(String value) {
	 takeStoredValueForKey(value, COMD_URL_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande commande() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
	}

	public void setCommandeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commande();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression commandeImpression() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression)storedValueForKey(COMMANDE_IMPRESSION_KEY);
	}

	public void setCommandeImpressionRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression oldValue = commandeImpression();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_IMPRESSION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_IMPRESSION_KEY);
	 }
	}

	public org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier courrier() {
	 return (org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier)storedValueForKey(COURRIER_KEY);
	}

	public void setCourrierRelationship(org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier value) {
	 if (value == null) {
	 	org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier oldValue = courrier();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COURRIER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COURRIER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeDocument typeDocument() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeDocument)storedValueForKey(TYPE_DOCUMENT_KEY);
	}

	public void setTypeDocumentRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeDocument value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeDocument oldValue = typeDocument();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_DOCUMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_DOCUMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}


	/**
	* Créer une instance de EOCommandeDocument avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeDocument createEOCommandeDocument(EOEditingContext editingContext				, NSTimestamp comdDate
							, org.cocktail.fwkcktldepense.server.metier.EOCommande commande						, org.cocktail.fwkcktldepense.server.metier.EOTypeDocument typeDocument		, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EOCommandeDocument eo = (EOCommandeDocument) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeDocument.ENTITY_NAME);	 
							eo.setComdDate(comdDate);
								 eo.setCommandeRelationship(commande);
								 eo.setTypeDocumentRelationship(typeDocument);
				 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeDocument creerInstance(EOEditingContext editingContext) {
		EOCommandeDocument object = (EOCommandeDocument)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeDocument.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeDocument localInstanceIn(EOEditingContext editingContext) {
    EOCommandeDocument localInstance = (EOCommandeDocument)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeDocument fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeDocument fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeDocument> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeDocument eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeDocument)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeDocument fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeDocument fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeDocument> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeDocument eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeDocument)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeDocument fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeDocument eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeDocument ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeDocument fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
