/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOAdresse.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOAdresse extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleAdresse";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ADRESSE";


//Attribute Keys
	public static final ERXKey<String> ADR_ADRESSE1 = new ERXKey<String>("adrAdresse1");
	public static final ERXKey<String> ADR_ADRESSE2 = new ERXKey<String>("adrAdresse2");
	public static final ERXKey<String> ADR_BP = new ERXKey<String>("adrBp");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
	public static final ERXKey<String> CP_ETRANGER = new ERXKey<String>("cpEtranger");
	public static final ERXKey<String> C_VOIE = new ERXKey<String>("cVoie");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPays> PAYS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPays>("pays");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "adrOrdre";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String C_PAYS_KEY = "cPays";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String VILLE_KEY = "ville";

//Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_BP_COLKEY = "ADR_BP";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String C_VOIE_COLKEY = "C_VOIE";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";


	// Relationships
	public static final String PAYS_KEY = "pays";



	// Accessors methods
	public String adrAdresse1() {
	 return (String) storedValueForKey(ADR_ADRESSE1_KEY);
	}

	public void setAdrAdresse1(String value) {
	 takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
	}

	public String adrAdresse2() {
	 return (String) storedValueForKey(ADR_ADRESSE2_KEY);
	}

	public void setAdrAdresse2(String value) {
	 takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
	}

	public String adrBp() {
	 return (String) storedValueForKey(ADR_BP_KEY);
	}

	public void setAdrBp(String value) {
	 takeStoredValueForKey(value, ADR_BP_KEY);
	}

	public String codePostal() {
	 return (String) storedValueForKey(CODE_POSTAL_KEY);
	}

	public void setCodePostal(String value) {
	 takeStoredValueForKey(value, CODE_POSTAL_KEY);
	}

	public String cPays() {
	 return (String) storedValueForKey(C_PAYS_KEY);
	}

	public void setCPays(String value) {
	 takeStoredValueForKey(value, C_PAYS_KEY);
	}

	public String cpEtranger() {
	 return (String) storedValueForKey(CP_ETRANGER_KEY);
	}

	public void setCpEtranger(String value) {
	 takeStoredValueForKey(value, CP_ETRANGER_KEY);
	}

	public String cVoie() {
	 return (String) storedValueForKey(C_VOIE_KEY);
	}

	public void setCVoie(String value) {
	 takeStoredValueForKey(value, C_VOIE_KEY);
	}

	public String ville() {
	 return (String) storedValueForKey(VILLE_KEY);
	}

	public void setVille(String value) {
	 takeStoredValueForKey(value, VILLE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPays pays() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPays)storedValueForKey(PAYS_KEY);
	}

	public void setPaysRelationship(org.cocktail.fwkcktldepense.server.metier.EOPays value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPays oldValue = pays();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAYS_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PAYS_KEY);
	 }
	}


	/**
	* Créer une instance de EOAdresse avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOAdresse createEOAdresse(EOEditingContext editingContext												, String cPays
											, org.cocktail.fwkcktldepense.server.metier.EOPays pays					) {
	 EOAdresse eo = (EOAdresse) EOUtilities.createAndInsertInstance(editingContext, _EOAdresse.ENTITY_NAME);	 
															eo.setCPays(cPays);
												 eo.setPaysRelationship(pays);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAdresse creerInstance(EOEditingContext editingContext) {
		EOAdresse object = (EOAdresse)EOUtilities.createAndInsertInstance(editingContext, _EOAdresse.ENTITY_NAME);
  		return object;
		}

	

  public EOAdresse localInstanceIn(EOEditingContext editingContext) {
    EOAdresse localInstance = (EOAdresse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOAdresse> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOAdresse> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
