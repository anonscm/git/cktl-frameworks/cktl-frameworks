/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.NSValidation;

public class EOFonction extends _EOFonction {

	public static final String FON_SPEC_GESTION_OUI = "O";
	public static final String FON_SPEC_GESTION_NON = "N";

	public static final String FON_SPEC_EXERCICE_OUI = "O";
	public static final String FON_SPEC_EXERCICE_NON = "N";

	public static final String FONCTION_ENGAGE = "DEENG";
	public static final String FONCTION_ENGAGE_PERIODE_INVENTAIRE = "DEENGINV";
	public static final String FONCTION_LIQUIDE = "DELIQ";
	public static final String FONCTION_LIQUIDE_PERIODE_INVENTAIRE = "DELIQINV";
	public static final String FONCTION_AUTRE_IMPUTATION = "DEAUTIMP";
	public static final String FONCTION_AUTRE_ACTION = "DEAUTACT";
	public static final String FONCTION_TRAITEMENT_EN_MASSE = "DEMASSE";
	public static final String FONCTION_VOIR_TTES_PRECOMMANDES = "DETTPCDE";

	public static final String FONCTION_CREATION_PRESTATION = "PRGPR";

	public static final String FONCTION_REIMPUTER = "REIMP";
	public static final String FONCTION_REIMPUTER_PERIODE_INVENTAIRE = "REIMPINV";

	public static final String FONCTION_B2B_CAT = "B2BCAT";
	public static final String FONCTION_B2B_COM = "B2BCOM";
	public static final String FONCTION_SFBDLIQ = "SFBDLIQ";
	public static final String FONCTION_DELIQEXT = "DELIQEXT";

	public static final String FONCTION_LIQUIDE_SANS_ENGAGEMENT = "LIQSENG";
	public static final String FONCTION_LIQUIDE_MANDAT_EXTOURNE = "LIQLIEXT";
	public static final String FONCTION_LIQUIDE_ENVELOPPE_EXTOURNE = "LIQENEXT";

	public EOFonction() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
