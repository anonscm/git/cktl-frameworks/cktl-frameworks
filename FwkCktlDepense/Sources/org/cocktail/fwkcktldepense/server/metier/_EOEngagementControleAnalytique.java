/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEngagementControleAnalytique.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOEngagementControleAnalytique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControleAnalytique";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_ANALYTIQUE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> EANA_DATE_SAISIE = new ERXKey<NSTimestamp>("eanaDateSaisie");
	public static final ERXKey<java.math.BigDecimal> EANA_HT_SAISIE = new ERXKey<java.math.BigDecimal>("eanaHtSaisie");
	public static final ERXKey<java.math.BigDecimal> EANA_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("eanaMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> EANA_MONTANT_BUDGETAIRE_RESTE = new ERXKey<java.math.BigDecimal>("eanaMontantBudgetaireReste");
	public static final ERXKey<java.math.BigDecimal> EANA_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("eanaTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> EANA_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("eanaTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> CODE_ANALYTIQUE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique>("codeAnalytique");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "eanaId";

	public static final String EANA_DATE_SAISIE_KEY = "eanaDateSaisie";
	public static final String EANA_HT_SAISIE_KEY = "eanaHtSaisie";
	public static final String EANA_MONTANT_BUDGETAIRE_KEY = "eanaMontantBudgetaire";
	public static final String EANA_MONTANT_BUDGETAIRE_RESTE_KEY = "eanaMontantBudgetaireReste";
	public static final String EANA_TTC_SAISIE_KEY = "eanaTtcSaisie";
	public static final String EANA_TVA_SAISIE_KEY = "eanaTvaSaisie";

//Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String EANA_ID_KEY = "eanaId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String EANA_DATE_SAISIE_COLKEY = "EANA_DATE_SAISIE";
	public static final String EANA_HT_SAISIE_COLKEY = "EANA_HT_SAISIE";
	public static final String EANA_MONTANT_BUDGETAIRE_COLKEY = "EANA_MONTANT_BUDGETAIRE";
	public static final String EANA_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EANA_MONTANT_BUDGETAIRE_RESTE";
	public static final String EANA_TTC_SAISIE_COLKEY = "EANA_TTC_SAISIE";
	public static final String EANA_TVA_SAISIE_COLKEY = "EANA_TVA_SAISIE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String EANA_ID_COLKEY = "EANA_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public NSTimestamp eanaDateSaisie() {
	 return (NSTimestamp) storedValueForKey(EANA_DATE_SAISIE_KEY);
	}

	public void setEanaDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, EANA_DATE_SAISIE_KEY);
	}

	public java.math.BigDecimal eanaHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EANA_HT_SAISIE_KEY);
	}

	public void setEanaHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EANA_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal eanaMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(EANA_MONTANT_BUDGETAIRE_KEY);
	}

	public void setEanaMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EANA_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal eanaMontantBudgetaireReste() {
	 return (java.math.BigDecimal) storedValueForKey(EANA_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public void setEanaMontantBudgetaireReste(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EANA_MONTANT_BUDGETAIRE_RESTE_KEY);
	}

	public java.math.BigDecimal eanaTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EANA_TTC_SAISIE_KEY);
	}

	public void setEanaTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EANA_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal eanaTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(EANA_TVA_SAISIE_KEY);
	}

	public void setEanaTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, EANA_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique codeAnalytique() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
	}

	public void setCodeAnalytiqueRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique oldValue = codeAnalytique();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
	}

	public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOEngagementControleAnalytique avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEngagementControleAnalytique createEOEngagementControleAnalytique(EOEditingContext editingContext				, NSTimestamp eanaDateSaisie
							, java.math.BigDecimal eanaHtSaisie
							, java.math.BigDecimal eanaMontantBudgetaire
							, java.math.BigDecimal eanaMontantBudgetaireReste
							, java.math.BigDecimal eanaTtcSaisie
							, java.math.BigDecimal eanaTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique codeAnalytique		, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOEngagementControleAnalytique eo = (EOEngagementControleAnalytique) EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControleAnalytique.ENTITY_NAME);	 
							eo.setEanaDateSaisie(eanaDateSaisie);
									eo.setEanaHtSaisie(eanaHtSaisie);
									eo.setEanaMontantBudgetaire(eanaMontantBudgetaire);
									eo.setEanaMontantBudgetaireReste(eanaMontantBudgetaireReste);
									eo.setEanaTtcSaisie(eanaTtcSaisie);
									eo.setEanaTvaSaisie(eanaTvaSaisie);
						 eo.setCodeAnalytiqueRelationship(codeAnalytique);
				 eo.setEngagementBudgetRelationship(engagementBudget);
				 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngagementControleAnalytique creerInstance(EOEditingContext editingContext) {
		EOEngagementControleAnalytique object = (EOEngagementControleAnalytique)EOUtilities.createAndInsertInstance(editingContext, _EOEngagementControleAnalytique.ENTITY_NAME);
  		return object;
		}

	

  public EOEngagementControleAnalytique localInstanceIn(EOEditingContext editingContext) {
    EOEngagementControleAnalytique localInstance = (EOEngagementControleAnalytique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngagementControleAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngagementControleAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEngagementControleAnalytique> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControleAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControleAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControleAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControleAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEngagementControleAnalytique> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControleAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControleAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngagementControleAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControleAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControleAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControleAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
