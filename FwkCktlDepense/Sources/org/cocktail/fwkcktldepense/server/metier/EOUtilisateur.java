/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderOrgan;
import org.cocktail.fwkcktldepense.server.finder.FinderUtilisateurFonction;
import org.cocktail.fwkcktldepense.server.finder.FinderUtilisateurFonctionExercice;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOUtilisateur extends _EOUtilisateur {
	public static String ETAT_VALIDE = "VALIDE";
	public static String ETAT_SUPPRIME = "SUPPRIME";

	public EOUtilisateur() {
		super();
	}

	public String nomPrenom() {
		String nomPrenom = "";
		EOIndividu individu = individu();
		if (individu != null && individu.nomUsuel() != null) {
			nomPrenom = individu.nomUsuel();
		}
		if (individu != null && individu.prenom() != null) {
			nomPrenom += " " + individu().prenom();
		}


		return nomPrenom;
	}

	public Integer persId() {
		return new Integer(super.persIdCle().toString());
		/*
		 * // on cherche la cle de la personne NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(editingContext(), personne()); if
		 * (dicoForPrimaryKeys==null) return null; return (Integer)dicoForPrimaryKeys.objectForKey(EOPersonne.PERS_ID_KEY);
		 */}

	public boolean isValide() {
		if (typeEtat() == null) {
			return false;
		}
		if (typeEtat().tyetLibelle() == null || !typeEtat().tyetLibelle().equals(ETAT_VALIDE)) {
			return false;
		}

		return hasUtilisateurDroitAJour();
	}

	/**
	 * @return true si utilisateur a les droits a jour ; false sinon.
	 */
	public boolean hasUtilisateurDroitAJour() {
		return hasUtilisateurDroitAJour(this);
	}

	public static boolean hasUtilisateurDroitAJour(EOUtilisateur utilisateur) {
		System.out.println(getNSTimeStamp());
		if (utilisateur.utlOuverture() == null || getNSTimeStamp().before(utilisateur.utlOuverture())) {
			return false;
		}
		if (utilisateur.utlFermeture() != null && utilisateur.utlFermeture().before(getNSTimeStamp())) {
			return false;
		}
		return true;
	}

	protected static NSTimestamp getNSTimeStamp() {
		return ZDateUtil.currentDateNSTimeStamp();
	}

	public boolean isDroitCreerPrestation(EOExercice exercice) {
		return FinderExercice.isExercicePrestationPourUtilisateur(editingContext(), exercice, this);
	}

	public boolean isUtilisateurEnMasse(EOExercice exercice, EOUtilisateurFonctionExercice utilisateurFonctionExercice) {
		if (utilisateurFonctionExercice != null)
			return true;

		if (exercice == null) {
			// On prend l'exercice correspondant a la date du jour
			exercice = FinderExercice.getExercicePourDate(editingContext(), new NSTimestamp());
		}

		if (exercice == null)
			return false;

		NSMutableDictionary mesBindings = new NSMutableDictionary();
		mesBindings.setObjectForKey(exercice, "exercice");
		mesBindings.setObjectForKey(this, "utilisateur");
		mesBindings.setObjectForKey(EOFonction.FONCTION_TRAITEMENT_EN_MASSE, "fonIdInterne");
		NSArray larray = FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(editingContext(), mesBindings);

		if (larray == null || larray.count() == 0)
			return false;

		return true;
	}

	public boolean isDroitSolderEnMasse(EOExercice exerciceSelectionCommande, NSArray<NSDictionary<String, Object>> commandesRawRow, boolean isUtilisateurEnMasse) {
		if (!isUtilisateurEnMasse || commandesRawRow == null || commandesRawRow.count() == 0 || (!exerciceSelectionCommande.estEngageableOuvert() &&
				!exerciceSelectionCommande.estEngageableRestreint()))
			return false;

		for (int i = 0; i < commandesRawRow.count(); i++) {
			NSDictionary<String, Object> commande = (NSDictionary<String, Object>) commandesRawRow.objectAtIndex(i);
			String etat = (String) commande.objectForKey("TYPEETAT");
			if (!etat.equals(EOCommande.ETAT_ENGAGEE) && !etat.equals(EOCommande.ETAT_PARTIELLEMENT_SOLDEE) && !etat.equals(EOCommande.ETAT_SOLDEE))
				return false;
		}

		return true;
	}

	public boolean isDroitDupliquerEnMasse(EOExercice exerciceSelectionCommande, EOExercice exerciceDestination, NSArray<NSDictionary<String, Object>> commandesRawRow, boolean isUtilisateurEnMasse) {
		if (!isUtilisateurEnMasse || commandesRawRow == null || commandesRawRow.count() == 0 || (!exerciceDestination.estEngageableOuvert() && !exerciceDestination.estEngageableRestreint()))
			return false;

		for (int i = 0; i < commandesRawRow.count(); i++) {
			NSDictionary<String, Object> commande = (NSDictionary<String, Object>) commandesRawRow.objectAtIndex(i);
			String etat = (String) commande.objectForKey("TYPEETAT");
			if (!etat.equals(EOCommande.ETAT_ENGAGEE))
				return false;
		}

		return true;
	}

	public boolean isDroitBasculerEnMasse(EOExercice exerciceSelectionCommande, EOExercice exerciceDestination, NSArray<NSDictionary<String, Object>> commandesRawRow, boolean isUtilisateurEnMasse) {
		if (!isUtilisateurEnMasse || commandesRawRow == null || commandesRawRow.count() == 0 || (!exerciceSelectionCommande.estEngageableOuvert() &&
				!exerciceSelectionCommande.estEngageableRestreint()) || (!exerciceDestination.estEngageableOuvert() && !exerciceDestination.estEngageableRestreint()))
			return false;
		if (exerciceSelectionCommande.equals(exerciceDestination))
			return false;

		for (int i = 0; i < commandesRawRow.count(); i++) {
			NSDictionary<String, Object> commande = (NSDictionary<String, Object>) commandesRawRow.objectAtIndex(i);
			String etat = (String) commande.objectForKey("TYPEETAT");
			if (!etat.equals(EOCommande.ETAT_ENGAGEE))
				return false;
		}

		return true;
	}

	/**
	 * @param exercice
	 * @return true si l'utilisateur a le droit d'utiliser des modes de paiements "extourne" lors de la liquidation pour l'exercice en cours.
	 */
	public boolean isDroitLiquiderAvecModePaiementExtourne(EOExercice exercice) {
		if (exercice == null) {
			return false;
		}
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(ERXQ.equals(EOUtilisateurFonctionExercice.EXERCICE_KEY, exercice));
		quals.addObject(ERXQ.equals(EOUtilisateurFonctionExercice.UTILISATEUR_FONCTION_KEY + "." + EOUtilisateurFonction.UTILISATEUR_KEY, this));
		quals.addObject(ERXQ.equals(EOUtilisateurFonctionExercice.UTILISATEUR_FONCTION_KEY + "." + EOUtilisateurFonction.FONCTION_KEY + "." + EOFonction.FON_ID_INTERNE_KEY, EOFonction.FONCTION_DELIQEXT));
		NSArray<EOUtilisateurFonctionExercice> res = EOUtilisateurFonctionExercice.fetchAll(editingContext(), ERXQ.and(quals));
		return Boolean.valueOf(res.count() > 0);

	}

	public boolean isDroitLiquider(EOExercice exercice) {
		if (exercice == null)
			return false;

		NSArray exercices = FinderExercice.getExercicesLiquidablesPourUtilisateur(editingContext(), this);
		if (exercices == null)
			return false;

		for (int i = 0; i < exercices.count(); i++) {
			if (exercice.exeExercice().equals(((EOExercice) exercices.objectAtIndex(i)).exeExercice()))
				return true;
		}
		return false;
	}

	public boolean isDroitLiquiderSansEngagement(EOExercice exercice) {
		return FinderUtilisateurFonction.getUtilisateurFonction(
				editingContext(), this, exercice, EOFonction.FONCTION_LIQUIDE_SANS_ENGAGEMENT) != null;
	}

	public boolean isDroitLiquiderSurMandatExtourne(EOExercice exercice) {
		return FinderUtilisateurFonction.getUtilisateurFonction(
				editingContext(), this, exercice, EOFonction.FONCTION_LIQUIDE_MANDAT_EXTOURNE) != null;
	}

	public boolean isDroitLiquiderSurEnveloppeExtourne(EOExercice exercice) {
		return FinderUtilisateurFonction.getUtilisateurFonction(
				editingContext(), this, exercice, EOFonction.FONCTION_LIQUIDE_ENVELOPPE_EXTOURNE) != null;
	}

	public boolean isDroitCreerDossiersLiquidation(EOExercice exerciceCourant) {
		return Boolean.valueOf(FinderUtilisateurFonction.getUtilisateurFonction(editingContext(), this, EOFonction.FONCTION_SFBDLIQ) != null);
	}

	public boolean isDroitEnvoyerCommandeB2b() {
		return Boolean.valueOf(FinderUtilisateurFonction.getUtilisateurFonction(editingContext(), this, EOFonction.FONCTION_B2B_CAT) != null);
	}

	/**
	 * Permet de savoir si un utilisateur possede des droits sur une ou plusieures lignes budgetaire.<BR>
	 * <BR>
	 *
	 * @param exercice exercice pour lequel on s'interroge sur les droits de l'utilisateur par defaut, l'exercice courant
	 * @return true si utilisateur avec des droits false sinon
	 */
	public boolean isUtilisateurAvecDroits(EOExercice exercice) {
		boolean isUtilisateurAvecDroits = false;
		if (exercice == null) {
			// On prend l'exercice correspondant a la date du jour
			exercice = FinderExercice.getExercicePourDate(editingContext(), new NSTimestamp());
		}
		if (exercice != null) {
			NSArray array = FinderOrgan.getOrgans(editingContext(), this, exercice);
			if (array != null && array.count() > 0) {
				isUtilisateurAvecDroits = true;
			}
			else {
				isUtilisateurAvecDroits = false;
			}
		}
		return isUtilisateurAvecDroits;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		throw new FactoryException(ENTITY_NAME + " : " + FactoryException.messageErreur);
	}

	public Boolean isDroitEngager(EOCommande commande) {
		Boolean res = Boolean.FALSE;
		if (commande != null && commande.exercice() != null) {
			EOEditingContext edc = commande.editingContext();
			EOExercice exercice = commande.exercice();
			NSArray<EOExercice> exercicesEngageables = FinderExercice.getExercicesEngageablesPourUtilisateur(edc, this);
			NSArray<EOOrgan> organs = FinderOrgan.getOrgans(edc, this, exercice);
			if (exercicesEngageables != null && exercicesEngageables.containsObject(exercice) &&
					organs != null && organs.count() > 0) {
				res = Boolean.TRUE;
			}
		}
		return res;
	}

}
