/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPreDepenseControleConvention.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPreDepenseControleConvention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseControleConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> DCON_HT_SAISIE = new ERXKey<java.math.BigDecimal>("dconHtSaisie");
	public static final ERXKey<java.math.BigDecimal> DCON_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("dconMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> DCON_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("dconTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DCON_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("dconTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention> CONVENTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOConvention>("convention");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> PRE_DEPENSE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>("preDepenseBudget");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdconId";

	public static final String DCON_HT_SAISIE_KEY = "dconHtSaisie";
	public static final String DCON_MONTANT_BUDGETAIRE_KEY = "dconMontantBudgetaire";
	public static final String DCON_TTC_SAISIE_KEY = "dconTtcSaisie";
	public static final String DCON_TVA_SAISIE_KEY = "dconTvaSaisie";

//Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PDCON_ID_KEY = "pdconId";
	public static final String PDEP_ID_KEY = "pdepId";

//Colonnes dans la base de donnees
	public static final String DCON_HT_SAISIE_COLKEY = "PDCON_HT_SAISIE";
	public static final String DCON_MONTANT_BUDGETAIRE_COLKEY = "PDCON_MONTANT_BUDGETAIRE";
	public static final String DCON_TTC_SAISIE_COLKEY = "PDCON_TTC_SAISIE";
	public static final String DCON_TVA_SAISIE_COLKEY = "PDCON_TVA_SAISIE";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PDCON_ID_COLKEY = "PDCON_ID";
	public static final String PDEP_ID_COLKEY = "PDEP_ID";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PRE_DEPENSE_BUDGET_KEY = "preDepenseBudget";



	// Accessors methods
	public java.math.BigDecimal dconHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DCON_HT_SAISIE_KEY);
	}

	public void setDconHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DCON_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal dconMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(DCON_MONTANT_BUDGETAIRE_KEY);
	}

	public void setDconMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DCON_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal dconTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DCON_TTC_SAISIE_KEY);
	}

	public void setDconTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DCON_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal dconTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DCON_TVA_SAISIE_KEY);
	}

	public void setDconTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DCON_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOConvention convention() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
	}

	public void setConventionRelationship(org.cocktail.fwkcktldepense.server.metier.EOConvention value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOConvention oldValue = convention();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget preDepenseBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget)storedValueForKey(PRE_DEPENSE_BUDGET_KEY);
	}

	public void setPreDepenseBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget oldValue = preDepenseBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRE_DEPENSE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PRE_DEPENSE_BUDGET_KEY);
	 }
	}


	/**
	* Créer une instance de EOPreDepenseControleConvention avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPreDepenseControleConvention createEOPreDepenseControleConvention(EOEditingContext editingContext				, java.math.BigDecimal dconHtSaisie
							, java.math.BigDecimal dconMontantBudgetaire
							, java.math.BigDecimal dconTtcSaisie
							, java.math.BigDecimal dconTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOConvention convention		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget preDepenseBudget					) {
	 EOPreDepenseControleConvention eo = (EOPreDepenseControleConvention) EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseControleConvention.ENTITY_NAME);	 
							eo.setDconHtSaisie(dconHtSaisie);
									eo.setDconMontantBudgetaire(dconMontantBudgetaire);
									eo.setDconTtcSaisie(dconTtcSaisie);
									eo.setDconTvaSaisie(dconTvaSaisie);
						 eo.setConventionRelationship(convention);
				 eo.setExerciceRelationship(exercice);
				 eo.setPreDepenseBudgetRelationship(preDepenseBudget);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreDepenseControleConvention creerInstance(EOEditingContext editingContext) {
		EOPreDepenseControleConvention object = (EOPreDepenseControleConvention)EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseControleConvention.ENTITY_NAME);
  		return object;
		}

	

  public EOPreDepenseControleConvention localInstanceIn(EOEditingContext editingContext) {
    EOPreDepenseControleConvention localInstance = (EOPreDepenseControleConvention)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreDepenseControleConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreDepenseControleConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreDepenseControleConvention> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseControleConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseControleConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseControleConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseControleConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreDepenseControleConvention> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseControleConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseControleConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreDepenseControleConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseControleConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseControleConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseControleConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
