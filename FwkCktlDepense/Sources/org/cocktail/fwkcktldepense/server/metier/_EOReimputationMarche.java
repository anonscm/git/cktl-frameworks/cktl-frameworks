/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOReimputationMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOReimputationMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_MARCHE";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> REMA_HT_SAISIE = new ERXKey<java.math.BigDecimal>("remaHtSaisie");
	public static final ERXKey<java.math.BigDecimal> REMA_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("remaMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> REMA_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("remaTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> REMA_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("remaTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttribution> ATTRIBUTION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAttribution>("attribution");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation> REIMPUTATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation>("reimputation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "remaId";

	public static final String REMA_HT_SAISIE_KEY = "remaHtSaisie";
	public static final String REMA_MONTANT_BUDGETAIRE_KEY = "remaMontantBudgetaire";
	public static final String REMA_TTC_SAISIE_KEY = "remaTtcSaisie";
	public static final String REMA_TVA_SAISIE_KEY = "remaTvaSaisie";

//Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String REIM_ID_KEY = "reimId";
	public static final String REMA_ID_KEY = "remaId";

//Colonnes dans la base de donnees
	public static final String REMA_HT_SAISIE_COLKEY = "REMA_HT_SAISIE";
	public static final String REMA_MONTANT_BUDGETAIRE_COLKEY = "REMA_MONTANT_BUDGETAIRE";
	public static final String REMA_TTC_SAISIE_COLKEY = "REMA_TTC_SAISIE";
	public static final String REMA_TVA_SAISIE_COLKEY = "REMA_TVA_SAISIE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String REMA_ID_COLKEY = "REMA_ID";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String REIMPUTATION_KEY = "reimputation";



	// Accessors methods
	public java.math.BigDecimal remaHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REMA_HT_SAISIE_KEY);
	}

	public void setRemaHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REMA_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal remaMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(REMA_MONTANT_BUDGETAIRE_KEY);
	}

	public void setRemaMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REMA_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal remaTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REMA_TTC_SAISIE_KEY);
	}

	public void setRemaTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REMA_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal remaTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REMA_TVA_SAISIE_KEY);
	}

	public void setRemaTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REMA_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAttribution attribution() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
	}

	public void setAttributionRelationship(org.cocktail.fwkcktldepense.server.metier.EOAttribution value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAttribution oldValue = attribution();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
	}

	public void setReimputationRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOReimputation oldValue = reimputation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
	 }
	}


	/**
	* Créer une instance de EOReimputationMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOReimputationMarche createEOReimputationMarche(EOEditingContext editingContext				, java.math.BigDecimal remaHtSaisie
							, java.math.BigDecimal remaMontantBudgetaire
							, java.math.BigDecimal remaTtcSaisie
							, java.math.BigDecimal remaTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOAttribution attribution		, org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation					) {
	 EOReimputationMarche eo = (EOReimputationMarche) EOUtilities.createAndInsertInstance(editingContext, _EOReimputationMarche.ENTITY_NAME);	 
							eo.setRemaHtSaisie(remaHtSaisie);
									eo.setRemaMontantBudgetaire(remaMontantBudgetaire);
									eo.setRemaTtcSaisie(remaTtcSaisie);
									eo.setRemaTvaSaisie(remaTvaSaisie);
						 eo.setAttributionRelationship(attribution);
				 eo.setReimputationRelationship(reimputation);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputationMarche creerInstance(EOEditingContext editingContext) {
		EOReimputationMarche object = (EOReimputationMarche)EOUtilities.createAndInsertInstance(editingContext, _EOReimputationMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOReimputationMarche localInstanceIn(EOEditingContext editingContext) {
    EOReimputationMarche localInstance = (EOReimputationMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputationMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputationMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReimputationMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReimputationMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputationMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
