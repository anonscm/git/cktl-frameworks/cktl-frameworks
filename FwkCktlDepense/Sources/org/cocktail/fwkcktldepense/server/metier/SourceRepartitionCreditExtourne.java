package org.cocktail.fwkcktldepense.server.metier;

import org.apache.bcel.verifier.exc.InvalidMethodException;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;

import com.webobjects.foundation.NSArray;

public class SourceRepartitionCreditExtourne implements ISourceRepartitionCredit {
	SourceCreditExtourneLiquidation source;
	public SourceRepartitionCreditExtourne(SourceCreditExtourneLiquidation source) {
		this.source = source;
	}
	
	public NSArray repartitionPourcentagePlanComptables() {
		return ExtourneHelper.repartitionPourcentageSurExtournePlanComptables(source);
	}
	
	public NSArray repartitionPourcentageMarches() {
		return ExtourneHelper.repartitionPourcentageSurExtourneMarches(source);
	}
	
	public NSArray repartitionPourcentageHorsMarches() {
		throw new InvalidMethodException("Pas de répartition hors-marché pour un crédit d'extourne");
	}
	
	public NSArray repartitionPourcentageConventions() {
		return ExtourneHelper.repartitionPourcentageSurExtourneConventions(source);
	}
	
	public NSArray repartitionPourcentageAnalytiques() {
		return ExtourneHelper.repartitionPourcentageSurExtourneAnalytiques(source);
	}
	
	public NSArray repartitionPourcentageActions() {
		throw new InvalidMethodException("Pas de répartition sur action LOLF pour un crédit d'extourne");
	}
}
