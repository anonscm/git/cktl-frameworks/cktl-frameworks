package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Interface pour les différentes sources de crédits (budgétaires ou extourne)
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public interface _ISourceCredit {

	public static enum ESourceCreditType {
		EXTOURNE, BUDGET
	};

	public EOOrgan organ();

	public EOTypeCredit typeCredit();

	public EOExercice exercice();

	public ESourceCreditType type();

	public String libelle();

	public String libelleCourt();

	public String codeOrganEtTypeCredit();
	
	public EOTauxProrata tauxProrata();

	public void setTauxProrata(EOTauxProrata tauxProrata);

	public NSArray<EOTauxProrata> tauxProrataDisponibles();

	public Boolean isComplet();

	public BigDecimal getDisponible();

	public NSArray<EOConvention> getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur);

	public NSArray<EOCodeAnalytique> getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur);
}
