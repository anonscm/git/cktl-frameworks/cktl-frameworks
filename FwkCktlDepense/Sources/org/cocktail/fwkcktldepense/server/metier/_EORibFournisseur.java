/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EORibFournisseur.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EORibFournisseur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleRibFournisseur";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_RIB_FOURNISSEUR";


//Attribute Keys
	public static final ERXKey<String> BIC = new ERXKey<String>("bic");
	public static final ERXKey<String> C_BANQUE = new ERXKey<String>("cBanque");
	public static final ERXKey<String> C_GUICHET = new ERXKey<String>("cGuichet");
	public static final ERXKey<String> CLE_RIB = new ERXKey<String>("cleRib");
	public static final ERXKey<String> IBAN = new ERXKey<String>("iban");
	public static final ERXKey<String> MOD_CODE = new ERXKey<String>("modCode");
	public static final ERXKey<String> NO_COMPTE = new ERXKey<String>("noCompte");
	public static final ERXKey<String> RIB_TITCO = new ERXKey<String>("ribTitco");
	public static final ERXKey<String> RIB_VALIDE = new ERXKey<String>("ribValide");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("fournisseur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ribOrdre";

	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String CLE_RIB_KEY = "cleRib";
	public static final String IBAN_KEY = "iban";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String NO_COMPTE_KEY = "noCompte";
	public static final String RIB_TITCO_KEY = "ribTitco";
	public static final String RIB_VALIDE_KEY = "ribValide";

//Attributs non visibles
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";

//Colonnes dans la base de donnees
	public static final String BIC_COLKEY = "BIC";
	public static final String C_BANQUE_COLKEY = "C_BANQUE";
	public static final String C_GUICHET_COLKEY = "C_GUICHET";
	public static final String CLE_RIB_COLKEY = "CLE_RIB";
	public static final String IBAN_COLKEY = "IBAN";
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String NO_COMPTE_COLKEY = "NO_COMPTE";
	public static final String RIB_TITCO_COLKEY = "RIB_TITCO";
	public static final String RIB_VALIDE_COLKEY = "RIB_VALIDE";

	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";


	// Relationships
	public static final String FOURNISSEUR_KEY = "fournisseur";



	// Accessors methods
	public String bic() {
	 return (String) storedValueForKey(BIC_KEY);
	}

	public void setBic(String value) {
	 takeStoredValueForKey(value, BIC_KEY);
	}

	public String cBanque() {
	 return (String) storedValueForKey(C_BANQUE_KEY);
	}

	public void setCBanque(String value) {
	 takeStoredValueForKey(value, C_BANQUE_KEY);
	}

	public String cGuichet() {
	 return (String) storedValueForKey(C_GUICHET_KEY);
	}

	public void setCGuichet(String value) {
	 takeStoredValueForKey(value, C_GUICHET_KEY);
	}

	public String cleRib() {
	 return (String) storedValueForKey(CLE_RIB_KEY);
	}

	public void setCleRib(String value) {
	 takeStoredValueForKey(value, CLE_RIB_KEY);
	}

	public String iban() {
	 return (String) storedValueForKey(IBAN_KEY);
	}

	public void setIban(String value) {
	 takeStoredValueForKey(value, IBAN_KEY);
	}

	public String modCode() {
	 return (String) storedValueForKey(MOD_CODE_KEY);
	}

	public void setModCode(String value) {
	 takeStoredValueForKey(value, MOD_CODE_KEY);
	}

	public String noCompte() {
	 return (String) storedValueForKey(NO_COMPTE_KEY);
	}

	public void setNoCompte(String value) {
	 takeStoredValueForKey(value, NO_COMPTE_KEY);
	}

	public String ribTitco() {
	 return (String) storedValueForKey(RIB_TITCO_KEY);
	}

	public void setRibTitco(String value) {
	 takeStoredValueForKey(value, RIB_TITCO_KEY);
	}

	public String ribValide() {
	 return (String) storedValueForKey(RIB_VALIDE_KEY);
	}

	public void setRibValide(String value) {
	 takeStoredValueForKey(value, RIB_VALIDE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = fournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
	 }
	}


	/**
	* Créer une instance de EORibFournisseur avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EORibFournisseur createEORibFournisseur(EOEditingContext editingContext						, String cBanque
							, String cGuichet
											, String modCode
							, String noCompte
							, String ribTitco
							, String ribValide
					, org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur					) {
	 EORibFournisseur eo = (EORibFournisseur) EOUtilities.createAndInsertInstance(editingContext, _EORibFournisseur.ENTITY_NAME);	 
									eo.setCBanque(cBanque);
									eo.setCGuichet(cGuichet);
													eo.setModCode(modCode);
									eo.setNoCompte(noCompte);
									eo.setRibTitco(ribTitco);
									eo.setRibValide(ribValide);
						 eo.setFournisseurRelationship(fournisseur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORibFournisseur creerInstance(EOEditingContext editingContext) {
		EORibFournisseur object = (EORibFournisseur)EOUtilities.createAndInsertInstance(editingContext, _EORibFournisseur.ENTITY_NAME);
  		return object;
		}

	

  public EORibFournisseur localInstanceIn(EOEditingContext editingContext) {
    EORibFournisseur localInstance = (EORibFournisseur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORibFournisseur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORibFournisseur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORibFournisseur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORibFournisseur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORibFournisseur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORibFournisseur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORibFournisseur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORibFournisseur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORibFournisseur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORibFournisseur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORibFournisseur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORibFournisseur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORibFournisseur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORibFournisseur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> objectsForRecherche(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseurBinding,
														String modCodeBinding,
														String ribValideBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EORibFournisseur.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (fournisseurBinding != null)
			bindings.takeValueForKey(fournisseurBinding, "fournisseur");
		  if (modCodeBinding != null)
			bindings.takeValueForKey(modCodeBinding, "modCode");
		  if (ribValideBinding != null)
			bindings.takeValueForKey(ribValideBinding, "ribValide");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
