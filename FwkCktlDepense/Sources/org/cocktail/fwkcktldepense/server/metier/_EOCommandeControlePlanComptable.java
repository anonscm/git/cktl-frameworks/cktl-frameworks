/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeControlePlanComptable.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeControlePlanComptable extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeControlePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_CTRL_PLANCO";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> CPCO_HT_SAISIE = new ERXKey<java.math.BigDecimal>("cpcoHtSaisie");
	public static final ERXKey<java.math.BigDecimal> CPCO_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("cpcoMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> CPCO_POURCENTAGE = new ERXKey<java.math.BigDecimal>("cpcoPourcentage");
	public static final ERXKey<java.math.BigDecimal> CPCO_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("cpcoTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> CPCO_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("cpcoTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> COMMANDE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>("commandeBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable> PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPlanComptable>("planComptable");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cpcoId";

	public static final String CPCO_HT_SAISIE_KEY = "cpcoHtSaisie";
	public static final String CPCO_MONTANT_BUDGETAIRE_KEY = "cpcoMontantBudgetaire";
	public static final String CPCO_POURCENTAGE_KEY = "cpcoPourcentage";
	public static final String CPCO_TTC_SAISIE_KEY = "cpcoTtcSaisie";
	public static final String CPCO_TVA_SAISIE_KEY = "cpcoTvaSaisie";

//Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String CPCO_ID_KEY = "cpcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String CPCO_HT_SAISIE_COLKEY = "CPCO_HT_SAISIE";
	public static final String CPCO_MONTANT_BUDGETAIRE_COLKEY = "CPCO_MONTANT_BUDGETAIRE";
	public static final String CPCO_POURCENTAGE_COLKEY = "CPCO_POURCENTAGE";
	public static final String CPCO_TTC_SAISIE_COLKEY = "CPCO_TTC_SAISIE";
	public static final String CPCO_TVA_SAISIE_COLKEY = "CPCO_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String CPCO_ID_COLKEY = "CPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String COMMANDE_BUDGET_KEY = "commandeBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
	public java.math.BigDecimal cpcoHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CPCO_HT_SAISIE_KEY);
	}

	public void setCpcoHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CPCO_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal cpcoMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(CPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public void setCpcoMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CPCO_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal cpcoPourcentage() {
	 return (java.math.BigDecimal) storedValueForKey(CPCO_POURCENTAGE_KEY);
	}

	public void setCpcoPourcentage(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CPCO_POURCENTAGE_KEY);
	}

	public java.math.BigDecimal cpcoTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CPCO_TTC_SAISIE_KEY);
	}

	public void setCpcoTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CPCO_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal cpcoTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CPCO_TVA_SAISIE_KEY);
	}

	public void setCpcoTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CPCO_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget commandeBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget)storedValueForKey(COMMANDE_BUDGET_KEY);
	}

	public void setCommandeBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget oldValue = commandeBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
	}

	public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.server.metier.EOPlanComptable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPlanComptable oldValue = planComptable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
	 }
	}


	/**
	* Créer une instance de EOCommandeControlePlanComptable avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeControlePlanComptable createEOCommandeControlePlanComptable(EOEditingContext editingContext				, java.math.BigDecimal cpcoHtSaisie
							, java.math.BigDecimal cpcoMontantBudgetaire
							, java.math.BigDecimal cpcoPourcentage
							, java.math.BigDecimal cpcoTtcSaisie
							, java.math.BigDecimal cpcoTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget commandeBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOPlanComptable planComptable					) {
	 EOCommandeControlePlanComptable eo = (EOCommandeControlePlanComptable) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeControlePlanComptable.ENTITY_NAME);	 
							eo.setCpcoHtSaisie(cpcoHtSaisie);
									eo.setCpcoMontantBudgetaire(cpcoMontantBudgetaire);
									eo.setCpcoPourcentage(cpcoPourcentage);
									eo.setCpcoTtcSaisie(cpcoTtcSaisie);
									eo.setCpcoTvaSaisie(cpcoTvaSaisie);
						 eo.setCommandeBudgetRelationship(commandeBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setPlanComptableRelationship(planComptable);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeControlePlanComptable creerInstance(EOEditingContext editingContext) {
		EOCommandeControlePlanComptable object = (EOCommandeControlePlanComptable)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeControlePlanComptable.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeControlePlanComptable localInstanceIn(EOEditingContext editingContext) {
    EOCommandeControlePlanComptable localInstance = (EOCommandeControlePlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeControlePlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeControlePlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeControlePlanComptable> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeControlePlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeControlePlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeControlePlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeControlePlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
