/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.finder.FinderEcriture;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeAction;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;
import org.cocktail.fwkcktldepense.server.service.DepenseService;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOPreDepenseBudget extends _EOPreDepenseBudget implements _IDepenseBudget {
	private static Calculs serviceCalculs = Calculs.getInstance();
	private static DepenseService serviceDepense = new DepenseService();

	private static final long serialVersionUID = 1L;
	private NSArray arrayActions, arrayAnalytiques, arrayConventions;
	private NSArray arrayCodeExers, arrayPlanComptables, arrayEmargements;
	private _ISourceCredit source;
	private ESourceCreditType sourceTypeCredit = ESourceCreditType.BUDGET;

	public EOPreDepenseBudget() {
		super();
		arrayActions = null;
		arrayAnalytiques = null;
		arrayConventions = null;
		arrayCodeExers = null;
		arrayPlanComptables = null;
		arrayEmargements = null;
	}

	public boolean isEmargementObligatoire() {
		return serviceDepense.isEmargementObligatoire(depensePapier());
	}

	public BigDecimal restantHtControleAction() {
		return depHtSaisie().subtract(computeSumForKey(preDepenseControleActions(), EOPreDepenseControleAction.DACT_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControleAction() {
		return depTtcSaisie().subtract(computeSumForKey(preDepenseControleActions(), EOPreDepenseControleAction.DACT_TTC_SAISIE_KEY));
	}

	public BigDecimal restantHtControleAnalytique() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleAnalytique() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleConvention() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleConvention() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControleHorsMarche() {
		return depHtSaisie().subtract(
				computeSumForKey(preDepenseControleHorsMarches(), EOPreDepenseControleHorsMarche.DHOM_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControleHorsMarche() {
		return depTtcSaisie().subtract(
				computeSumForKey(preDepenseControleHorsMarches(), EOPreDepenseControleHorsMarche.DHOM_TTC_SAISIE_KEY));
	}

	public BigDecimal restantHtControleMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantTtcControleMarche() {
		return new BigDecimal(0.0);
	}

	public BigDecimal restantHtControlePlanComptable() {
		return depHtSaisie().subtract(
				computeSumForKey(preDepenseControlePlanComptables(), EOPreDepenseControlePlanComptable.DPCO_HT_SAISIE_KEY));
	}

	public BigDecimal restantTtcControlePlanComptable() {
		return depTtcSaisie().subtract(
				computeSumForKey(preDepenseControlePlanComptables(), EOPreDepenseControlePlanComptable.DPCO_TTC_SAISIE_KEY));
	}

	public NSArray getEcrituresPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayEmargements == null && depensePapier() != null && depensePapier().modePaiement() != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(depensePapier().modePaiement(), "modePaiement");
			arrayEmargements = FinderEcriture.getEcritureDetails(ed, bindings);

			// TODO : restreindre par rapport au gescode de la source (900 ou UB de la source) dixit Fred
			// parametres pour ca ?
		}
		if (arrayEmargements == null)
			arrayEmargements = new NSArray();

		if (arrayEmargements.count() == 0)
			return arrayEmargements;

		// Vu qu'on associe une ecriture a tt l'engagement, si une ecriture presente plus d'ecriture possible
		if (preDepenseControlePlanComptables() != null && preDepenseControlePlanComptables().count() > 0) {
			for (int i = 0; i < preDepenseControlePlanComptables().count(); i++) {
				EOEcritureDetail uneEcriture = ((EOPreDepenseControlePlanComptable) preDepenseControlePlanComptables().
						objectAtIndex(i)).ecritureDetail();
				if (uneEcriture != null)
					return NSArray.emptyArray();
			}
		}

		return arrayEmargements;
	}

	public NSArray getTypeActionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayActions == null && getSource() != null && utilisateur != null)
			arrayActions = FinderTypeAction.getTypeActions(ed, getSource(), utilisateur);
		if (arrayActions == null)
			arrayActions = new NSArray();

		if (arrayActions.count() == 0)
			return arrayActions;

		return arrayActions;
	}

	public NSArray getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques == null && getSource() != null && utilisateur != null)
			arrayAnalytiques = FinderCodeAnalytique.getCodeAnalytiques(ed, getSource().organ(),
					getSource().exercice());
		if (arrayAnalytiques == null)
			arrayAnalytiques = new NSArray();

		if (arrayAnalytiques.count() == 0)
			return arrayAnalytiques;

		return arrayAnalytiques;
	}

	public NSArray getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null && getSource() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(getSource().organ(), "organ");
			bindings.setObjectForKey(getSource().typeCredit(), "typeCredit");
			bindings.setObjectForKey(getSource().exercice(), "exercice");
			bindings.setObjectForKey(new Boolean(true), "montants");
			NSTimestamp dateFinPaiementMax = this.depensePapier().dppDateFacture();
			if (dateFinPaiementMax != null) {
				bindings.setObjectForKey(dateFinPaiementMax, "conDateFinPaiementMax");
			}
			arrayConventions = FinderConvention.getConventions(ed, bindings);
		}
		if (arrayConventions == null)
			arrayConventions = new NSArray();

		if (arrayConventions.count() == 0)
			return arrayConventions;

		return arrayConventions;
	}

	public NSArray getCodeExersPossibles(EOEditingContext ed, EOUtilisateur utilisateur, EOCommande commande) {
		if (arrayCodeExers == null && engagementBudget() != null && commande != null) {
			arrayCodeExers = FinderCodeExer.getCodeExerPourCommande(ed, commande);
		}
		if (arrayCodeExers == null)
			arrayCodeExers = new NSArray();

		if (arrayCodeExers.count() == 0)
			return arrayCodeExers;

		return arrayCodeExers;
	}

	public NSArray getPlanComptablesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayPlanComptables == null && getSource() != null && utilisateur != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(getSource().typeCredit(), "typeCredit");
			bindings.setObjectForKey(utilisateur, "utilisateur");
			bindings.setObjectForKey(getSource().exercice(), "exercice");
			arrayPlanComptables = FinderPlanComptable.getPlanComptables(ed, bindings);
		}
		if (arrayPlanComptables == null)
			arrayPlanComptables = new NSArray();

		if (arrayPlanComptables.count() == 0)
			return arrayPlanComptables;

		return arrayPlanComptables;
	}

	public boolean isControleEcritureGood() {
		return true;
	}

	public boolean isControleActionGood() {
		return isGenericControleGood(
				EOPreDepenseControleAction.TYPE_ACTION_KEY, 
				preDepenseControleActions(), 
				EOPreDepenseControleAction.PDACT_POURCENTAGE,
				restantHtControleAction(),
				restantTtcControleAction());
	}

	public boolean isControleAnalytiqueGood() {
		float sommePourcentage = computeSumForKey(preDepenseControleAnalytiques(), EOPreDepenseControleAnalytique.PDANA_POURCENTAGE).floatValue();
		if (sommePourcentage < 0 || sommePourcentage > 100)
			return false;
		return true;
	}

	public boolean isControleConventionGood() {
		float sommePourcentage = computeSumForKey(preDepenseControleConventions(), EOPreDepenseControleConvention.PDCON_POURCENTAGE).floatValue();
		if (sommePourcentage < 0 || sommePourcentage > 100)
			return false;
		return true;
	}

	public boolean isControleHorsMarcheGood() {
		
		return isGenericControleGood(
				EOPreDepenseControleHorsMarche.CODE_EXER_KEY, 
				preDepenseControleHorsMarches(), 
				EOPreDepenseControleHorsMarche.PDHOM_POURCENTAGE,
				restantHtControleHorsMarche(),
				restantTtcControleHorsMarche());
		
	}
	
	public boolean isControleMarcheGood() {
		
		return isGenericControleGood(
				EOPreDepenseControleMarche.ATTRIBUTION_KEY, 
				preDepenseControleMarches(), 
				EOPreDepenseControleMarche.PDMAR_POURCENTAGE,
				restantHtControleMarche(),
				restantTtcControleMarche());
		
	}

	public boolean isControlePlanComptableGood() {
		
		if(!isGenericControleGood(
				EOPreDepenseControlePlanComptable.PLAN_COMPTABLE_KEY, 
				preDepenseControlePlanComptables(), 
				EOPreDepenseControlePlanComptable.PDPCO_POURCENTAGE,
				restantHtControlePlanComptable(),
				restantTtcControlePlanComptable())) {
			return false;
		}
		

		for (int i = 0; i < preDepenseControlePlanComptables().count(); i++)
			if (!((EOPreDepenseControlePlanComptable) preDepenseControlePlanComptables().objectAtIndex(i)).isGood())
				return false;

		return true;
	}

	public void creerRepartitionAutomatique() {
		if (isSurExtourne()) {
			//on ne traite que le cas extourne sur mandat d'extourne
			if (getSource() instanceof SourceCreditExtourneLiquidation) {

				//if (depenseBudgetReversement() == null) {
				if (engagementBudget() == null || depHtSaisie() == null || depTtcSaisie() == null)
					return;

				NSArray array;
				array = ExtourneHelper.repartitionPourcentageSurExtourneMarches((SourceCreditExtourneLiquidation) getSource());
				for (int j = 0; j < array.count(); j++) {
					NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
					new FactoryPreDepenseControleMarche().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
							new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
							(EOAttribution) dico.objectForKey("attribution"), engagementBudget().exercice(), this);
				}

				array = ExtourneHelper.repartitionPourcentageSurExtourneAnalytiques((SourceCreditExtourneLiquidation) getSource());
				for (int j = 0; j < array.count(); j++) {
					NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
					new FactoryPreDepenseControleAnalytique().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
							new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
							(EOCodeAnalytique) dico.objectForKey("codeAnalytique"), engagementBudget().exercice(), this);
				}

				array = ExtourneHelper.repartitionPourcentageSurExtourneConventions((SourceCreditExtourneLiquidation) getSource());
				for (int j = 0; j < array.count(); j++) {
					NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
					new FactoryPreDepenseControleConvention().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
							new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
							(EOConvention) dico.objectForKey("convention"), engagementBudget().exercice(), this);
				}

				array = ExtourneHelper.repartitionPourcentageSurExtournePlanComptables((SourceCreditExtourneLiquidation) getSource());
				for (int j = 0; j < array.count(); j++) {
					NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
					new FactoryPreDepenseControlePlanComptable().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
							new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
							(EOPlanComptable) dico.objectForKey("planComptable"), engagementBudget().exercice(), this);
				}
				//}
			}

		}
		else {

			if (engagementBudget() == null || depHtSaisie() == null || depTtcSaisie() == null)
				return;

			NSArray array;

			array = engagementBudget().repartitionPourcentageActions();
			for (int j = 0; j < array.count(); j++) {
				NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
				new FactoryPreDepenseControleAction().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
						new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
						(EOTypeAction) dico.objectForKey("typeAction"), engagementBudget().exercice(), this);
			}

			array = engagementBudget().repartitionPourcentageAnalytiques();
			for (int j = 0; j < array.count(); j++) {
				NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
				new FactoryPreDepenseControleAnalytique().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
						new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
						(EOCodeAnalytique) dico.objectForKey("codeAnalytique"), engagementBudget().exercice(), this);
			}

			array = engagementBudget().repartitionPourcentageConventions();
			for (int j = 0; j < array.count(); j++) {
				NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
				new FactoryPreDepenseControleConvention().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
						new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
						(EOConvention) dico.objectForKey("convention"), engagementBudget().exercice(), this);
			}

			array = engagementBudget().repartitionPourcentageHorsMarches();
			for (int j = 0; j < array.count(); j++) {
				NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
				new FactoryPreDepenseControleHorsMarche().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
						new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
						(EOCodeExer) dico.objectForKey("codeExer"),
						(EOTypeAchat) dico.objectForKey("typeAchat"), engagementBudget().exercice(), this);
			}

			array = engagementBudget().repartitionPourcentageMarches();
			for (int j = 0; j < array.count(); j++) {
				NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
				new FactoryPreDepenseControleMarche().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
						new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
						(EOAttribution) dico.objectForKey("attribution"), engagementBudget().exercice(), this);
			}

			array = engagementBudget().repartitionPourcentagePlanComptables();
			for (int j = 0; j < array.count(); j++) {
				NSDictionary dico = (NSDictionary) array.objectAtIndex(j);
				new FactoryPreDepenseControlePlanComptable().creer(editingContext(), new BigDecimal(0.0), new BigDecimal(0.0),
						new BigDecimal(0.0), new BigDecimal(0.0), (BigDecimal) dico.objectForKey("pourcentage"),
						(EOPlanComptable) dico.objectForKey("planComptable"), engagementBudget().exercice(), this);
			}
		}
	}

	protected void corrigerMontant() {
		// correction des controleurs
		corrigerMontantActions();
		corrigerMontantAnalytiques();
		corrigerMontantConventions();
		corrigerMontantHorsMarches();
		corrigerMontantMarches();
		corrigerMontantPlanComptables();
	}

	public void corrigerMontantActions() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (preDepenseControleActions() == null || preDepenseControleActions().count() == 0)
			return;

		htReste = depHtSaisie();
		ttcReste = depTtcSaisie();
		budgetaireReste = depMontantBudgetaire();
		sommePourcentage = computeSumForKey(preDepenseControleActions(), EOPreDepenseControleAction.PDACT_POURCENTAGE);

		for (int i = 0; i < preDepenseControleActions().count(); i++) {
			EOPreDepenseControleAction depense = (EOPreDepenseControleAction) preDepenseControleActions().objectAtIndex(i);

			boolean reste = (i == preDepenseControleActions().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, depense.pourcentage(), depHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, depense.pourcentage(), depTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, depense.pourcentage(), depMontantBudgetaire(), reste);

			// on met a jour les montants
			depense.setDactHtSaisie(ht);
			depense.setDactTvaSaisie(ttc.subtract(ht));
			depense.setDactTtcSaisie(ttc);
			depense.setDactMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(depense.dactHtSaisie());
			ttcReste = ttcReste.subtract(depense.dactTtcSaisie());
			budgetaireReste = budgetaireReste.subtract(depense.dactMontantBudgetaire());
		}
	}

	public void corrigerMontantAnalytiques() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (preDepenseControleAnalytiques() == null || preDepenseControleAnalytiques().count() == 0)
			return;

		htReste = depHtSaisie();
		ttcReste = depTtcSaisie();
		budgetaireReste = depMontantBudgetaire();
		sommePourcentage = computeSumForKey(preDepenseControleAnalytiques(), EOPreDepenseControleAnalytique.PDANA_POURCENTAGE);

		for (int i = 0; i < preDepenseControleAnalytiques().count(); i++) {
			EOPreDepenseControleAnalytique depense = (EOPreDepenseControleAnalytique) preDepenseControleAnalytiques().objectAtIndex(i);

			boolean reste = (i == preDepenseControleAnalytiques().count() - 1);
			ttc = calculeMontant(sommePourcentage, ttcReste, depense.pourcentage(), depTtcSaisie(), reste);
			ht = calculeMontant(sommePourcentage, htReste, depense.pourcentage(), depHtSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, depense.pourcentage(), depMontantBudgetaire(), reste);

			// on met a jour les montants
			depense.setDanaHtSaisie(ht);
			depense.setDanaTvaSaisie(ttc.subtract(ht));
			depense.setDanaTtcSaisie(ttc);
			depense.setDanaMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(depense.danaHtSaisie());
			ttcReste = ttcReste.subtract(depense.danaTtcSaisie());
			budgetaireReste = budgetaireReste.subtract(depense.danaMontantBudgetaire());
		}
	}

	public void corrigerMontantConventions() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (preDepenseControleConventions() == null || preDepenseControleConventions().count() == 0)
			return;

		htReste = depHtSaisie();
		ttcReste = depTtcSaisie();
		budgetaireReste = depMontantBudgetaire();
		sommePourcentage = computeSumForKey(preDepenseControleConventions(), EOPreDepenseControleConvention.PDCON_POURCENTAGE);

		for (int i = 0; i < preDepenseControleConventions().count(); i++) {
			EOPreDepenseControleConvention depense = (EOPreDepenseControleConvention) preDepenseControleConventions().objectAtIndex(i);

			boolean reste = (i == preDepenseControleConventions().count() - 1);
			ttc = calculeMontant(sommePourcentage, ttcReste, depense.pourcentage(), depTtcSaisie(), reste);
			ht = calculeMontant(sommePourcentage, htReste, depense.pourcentage(), depHtSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, depense.pourcentage(), depMontantBudgetaire(), reste);

			// on met a jour les montants
			depense.setDconHtSaisie(ht);
			depense.setDconTvaSaisie(ttc.subtract(ht));
			depense.setDconTtcSaisie(ttc);
			depense.setDconMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(depense.dconHtSaisie());
			ttcReste = ttcReste.subtract(depense.dconTtcSaisie());
			budgetaireReste = budgetaireReste.subtract(depense.dconMontantBudgetaire());
		}
	}

	public void corrigerMontantHorsMarches() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (preDepenseControleHorsMarches() == null || preDepenseControleHorsMarches().count() == 0)
			return;

		htReste = depHtSaisie();
		ttcReste = depTtcSaisie();
		budgetaireReste = depMontantBudgetaire();
		sommePourcentage = computeSumForKey(preDepenseControleHorsMarches(), EOPreDepenseControleHorsMarche.PDHOM_POURCENTAGE);

		for (int i = 0; i < preDepenseControleHorsMarches().count(); i++) {
			EOPreDepenseControleHorsMarche depense = (EOPreDepenseControleHorsMarche) preDepenseControleHorsMarches().objectAtIndex(i);

			boolean reste = (i == preDepenseControleHorsMarches().count() - 1);
			ttc = calculeMontant(sommePourcentage, ttcReste, depense.pourcentage(), depTtcSaisie(), reste);
			ht = calculeMontant(sommePourcentage, htReste, depense.pourcentage(), depHtSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, depense.pourcentage(), depMontantBudgetaire(), reste);

			// on met a jour les montants
			depense.setDhomHtSaisie(ht);
			depense.setDhomTvaSaisie(ttc.subtract(ht));
			depense.setDhomTtcSaisie(ttc);
			depense.setDhomMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(depense.dhomHtSaisie());
			ttcReste = ttcReste.subtract(depense.dhomTtcSaisie());
			budgetaireReste = budgetaireReste.subtract(depense.dhomMontantBudgetaire());
		}
	}

	public void corrigerMontantMarches() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (preDepenseControleMarches() == null || preDepenseControleMarches().count() == 0)
			return;

		htReste = depHtSaisie();
		ttcReste = depTtcSaisie();
		budgetaireReste = depMontantBudgetaire();
		sommePourcentage = computeSumForKey(preDepenseControleMarches(), EOPreDepenseControleMarche.PDMAR_POURCENTAGE);

		for (int i = 0; i < preDepenseControleMarches().count(); i++) {
			EOPreDepenseControleMarche depense = (EOPreDepenseControleMarche) preDepenseControleMarches().objectAtIndex(i);

			boolean reste = (i == preDepenseControleMarches().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, depense.pourcentage(), depHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, depense.pourcentage(), depTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, depense.pourcentage(), depMontantBudgetaire(), reste);

			// on met a jour les montants
			depense.setDmarHtSaisie(ht);
			depense.setDmarTvaSaisie(ttc.subtract(ht));
			depense.setDmarTtcSaisie(ttc);
			depense.setDmarMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(depense.dmarHtSaisie());
			ttcReste = ttcReste.subtract(depense.dmarTtcSaisie());
			budgetaireReste = budgetaireReste.subtract(depense.dmarMontantBudgetaire());
		}
	}

	public void corrigerMontantPlanComptables() {
		BigDecimal htReste, ttcReste, budgetaireReste, sommePourcentage;
		BigDecimal ht, ttc, budgetaire;

		if (preDepenseControlePlanComptables() == null || preDepenseControlePlanComptables().count() == 0)
			return;

		htReste = depHtSaisie();
		ttcReste = depTtcSaisie();
		budgetaireReste = depMontantBudgetaire();
		sommePourcentage = computeSumForKey(preDepenseControlePlanComptables(), EOPreDepenseControlePlanComptable.PDPCO_POURCENTAGE);

		for (int i = 0; i < preDepenseControlePlanComptables().count(); i++) {
			EOPreDepenseControlePlanComptable depense = (EOPreDepenseControlePlanComptable) preDepenseControlePlanComptables().objectAtIndex(i);

			boolean reste = (i == preDepenseControlePlanComptables().count() - 1);
			ht = calculeMontant(sommePourcentage, htReste, depense.pourcentage(), depHtSaisie(), reste);
			ttc = calculeMontant(sommePourcentage, ttcReste, depense.pourcentage(), depTtcSaisie(), reste);
			budgetaire = calculeMontant(sommePourcentage, budgetaireReste, depense.pourcentage(), depMontantBudgetaire(), reste);

			// on met a jour les montants
			depense.setDpcoHtSaisie(ht);
			depense.setDpcoTvaSaisie(ttc.subtract(ht));
			depense.setDpcoTtcSaisie(ttc);
			depense.setDpcoMontantBudgetaire(budgetaire);

			// on met a jour les restes
			htReste = htReste.subtract(depense.dpcoHtSaisie());
			ttcReste = ttcReste.subtract(depense.dpcoTtcSaisie());
			budgetaireReste = budgetaireReste.subtract(depense.dpcoMontantBudgetaire());
		}
	}

	private BigDecimal calculeMontant(BigDecimal sommePourcentage, BigDecimal montantReste, BigDecimal pourcentage, BigDecimal montant, boolean reste) {
		BigDecimal calcul;

		// si c'est le dernier et que le pourcentage est egal a 100 -> on met le reste 
		if (sommePourcentage.floatValue() >= 100.0 && reste)
			return montantReste;

		// on calcule le montant par rapport au pourcentage
		calcul = pourcentage.multiply(montant).divide(new BigDecimal(100.0), 2, BigDecimal.ROUND_HALF_UP);

		// on verifie que le montant calcule ne depasse pas le reste
		if (calcul.abs().compareTo(montantReste.abs()) == 1)
			calcul = montantReste;

		return calcul;
	}

	private BigDecimal fractionHtTtcEngagement() {
		if (engagementBudget() == null || engagementBudget().engHtSaisie() == null || engagementBudget().engTtcSaisie() == null)
			return new BigDecimal(1.0);
		if (engagementBudget().engHtSaisie().floatValue() == 0 || engagementBudget().engTtcSaisie().floatValue() == 0)
			return new BigDecimal(1.0);

		return engagementBudget().engTtcSaisie().divide(engagementBudget().engHtSaisie(), 3, BigDecimal.ROUND_HALF_UP);
	}

	public void setDepHtSaisie(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (engagementBudget() != null)
			arrondi = engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDepHtSaisie(aValue);

		if (engagementBudget() != null) {
			// on calcule le ttc en fonction du ht-ttc de l'engagement
			setDepTtcSaisie(depHtSaisie().multiply(fractionHtTtcEngagement()));
			return;
		}

		if (depTtcSaisie() == null || depTtcSaisie().floatValue() < depHtSaisie().floatValue()) {
			setDepTtcSaisie(depHtSaisie());
			return;
		}

		setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));
		if (tauxProrata() != null)
			setDepMontantBudgetaire(calculMontantBudgetaire());

		corrigerMontant();
	}

	public void setDepTvaSaisie(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (engagementBudget() != null)
			arrondi = engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDepTvaSaisie(aValue);
	}

	public void setDepTtcSaisie(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (engagementBudget() != null)
			arrondi = engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDepTtcSaisie(aValue);

		if (depHtSaisie() == null || depHtSaisie().floatValue() > depTtcSaisie().floatValue())
			setDepHtSaisie(depTtcSaisie().divide(fractionHtTtcEngagement(), arrondi, BigDecimal.ROUND_UP));

		setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));

		if (tauxProrata() != null)
			setDepMontantBudgetaire(calculMontantBudgetaire());

		corrigerMontant();
	}

	public void setDepTtcSaisieSansCalcul(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (engagementBudget() != null)
			arrondi = engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		super.setDepTtcSaisie(aValue);
		if (depHtSaisie() != null)
			super.setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));

		if (tauxProrata() != null && depHtSaisie() != null && depTvaSaisie() != null)
			super.setDepMontantBudgetaire(calculMontantBudgetaire());

		corrigerMontant();
	}

	public void setDepHtSaisieSansCalcul(BigDecimal aValue) {
		if (aValue == null)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (engagementBudget() != null)
			arrondi = engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		super.setDepHtSaisie(aValue);
		if (depTtcSaisie() != null)
			super.setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));

		if (tauxProrata() != null && depHtSaisie() != null && depTvaSaisie() != null)
			super.setDepMontantBudgetaire(calculMontantBudgetaire());

		corrigerMontant();
	}

	public void setDepMontantBudgetaire(BigDecimal aValue) {
		if (aValue == null || aValue.floatValue() < 0)
			aValue = new BigDecimal(0.0);
		int arrondi = EODevise.defaultNbDecimales;
		if (engagementBudget() != null)
			arrondi = engagementBudget().decimalesPourArrondirMontant();
		aValue = aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setDepMontantBudgetaire(aValue);
	}

	//	public EOSource source() {
	//		return new EOSource(engagementBudget().organ(), engagementBudget().typeCredit(), tauxProrata(), exercice());
	//	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (depHtSaisie() == null)
			throw new DepenseBudgetException(DepenseBudgetException.depHtSaisieManquant);
		if (depTvaSaisie() == null)
			throw new DepenseBudgetException(DepenseBudgetException.depTvaSaisieManquant);
		if (depTtcSaisie() == null)
			throw new DepenseBudgetException(DepenseBudgetException.depTtcSaisieManquant);
		if (depMontantBudgetaire() == null)
			throw new DepenseBudgetException(DepenseBudgetException.depMontantBudgetaireManquant);

		if (depensePapier() == null)
			throw new DepenseBudgetException(DepenseBudgetException.depensePapierManquant);
		if (exercice() == null)
			throw new DepenseBudgetException(DepenseBudgetException.exerciceManquant);
		if (engagementBudget() == null)
			throw new DepenseBudgetException(DepenseBudgetException.engagementBudgetManquant);
		if (tauxProrata() == null)
			throw new DepenseBudgetException(DepenseBudgetException.tauxProrataManquant);
		if (utilisateur() == null)
			throw new DepenseBudgetException(DepenseBudgetException.utilisateurManquant);

		if (!depHtSaisie().abs().add(depTvaSaisie().abs()).equals(depTtcSaisie().abs()))
			setDepTvaSaisie(depTtcSaisie().subtract(depHtSaisie()));
		if (!depMontantBudgetaire().equals(calculMontantBudgetaire()))
			setDepMontantBudgetaire(calculMontantBudgetaire());

		if (depTtcSaisie().floatValue() < 0.0)
			throw new DepenseBudgetException(DepenseBudgetException.depenseBudgetReversementManquant);

		if (!engagementBudget().exercice().equals(exercice()))
			throw new DepenseBudgetException(DepenseBudgetException.engagementBudgetExercicePasCoherent);
		if (!depensePapier().exercice().equals(exercice()))
			throw new DepenseBudgetException(DepenseBudgetException.depensePapierExercicePasCoherent);

	}

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), tauxProrata().tapTaux());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (!engagementBudget().fournisseur().equals(depensePapier().fournisseur()))
			throw new DepenseBudgetException(DepenseBudgetException.fournisseurIncoherent);
	}

	protected BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	public NSArray depenseControleActions() {
		return preDepenseControleActions();
	}

	public NSArray depenseControleAnalytiques() {
		return preDepenseControleAnalytiques();
	}

	public NSArray depenseControleConventions() {
		return preDepenseControleConventions();
	}

	public NSArray depenseControleHorsMarches() {
		return preDepenseControleHorsMarches();
	}

	public NSArray depenseControlePlanComptables() {
		return preDepenseControlePlanComptables();
	}

	public EOTauxProrata tauxProrata() {
		return super.tauxProrata();
	}

	public void setTauxProrata(EOTauxProrata taux) {
		if (taux == null)
			return;
		takeStoredValueForKey(taux, EOPreDepenseBudget.TAUX_PRORATA_KEY);
		if (taux != null && depHtSaisie() != null && depTvaSaisie() != null) {
			super.setDepMontantBudgetaire(
					serviceCalculs.calculMontantBudgetaire(depHtSaisie(), depTvaSaisie(), taux.tapTaux()));
			corrigerMontant();
		}
	}

	public boolean isSupprimable() {
		return true;
	}

	public boolean isReversable() {
		return false;
	}

	public Boolean isReimputable(EOEditingContext ed, EOUtilisateur utilisateur, NSArray organs) {
		return Boolean.FALSE;
	}

	public NSArray depenseControleMarches() {
		return preDepenseControleMarches();
	}

	public void resetCodeAnalytiquesPossibles() {
		arrayAnalytiques = null;

	}

	public void resetCodeExersPossibles() {
		arrayCodeExers = null;

	}

	public void resetConventionsPossibles() {
		arrayConventions = null;

	}

	public void resetPlanComptablesPossibles() {
		arrayPlanComptables = null;

	}

	public void resetTypeActionsPossibles() {
		arrayActions = null;

	}

	public void setSource(_ISourceCredit source) {
		this.source = source;

	}

	public void updateFromSource() {
		//on ne fait rien avec organ et typecredit
		setTauxProrata(getSource().tauxProrata());
	}

	/**
	 * Renvoie la source de credit. Si le champ source est nul, on l'initialise avec celui de l'engagement. Utile pour la reimputation.
	 * 
	 * @return la source de credit.
	 */
	public _ISourceCredit getSource() {

		if (source == null) {
			if (isSurExtourne() && engagementBudget() != null && engagementBudget().organ() != null) {
				if (engagementBudget().organ().isUB()) {
					source = EOExtourneCreditsUb.fetchByQualifier(editingContext(), ERXQ.and(
							ERXQ.equals(_IDepenseExtourneCredits.TO_ORGAN_KEY, engagementBudget().organ()),
							ERXQ.equals(_IDepenseExtourneCredits.TO_EXERCICE_KEY, engagementBudget().exercice()),
							ERXQ.equals(_IDepenseExtourneCredits.TO_TYPE_CREDIT_KEY, engagementBudget().typeCredit())
							));
				}
				else {
					source = EOExtourneCreditsCr.fetchByQualifier(editingContext(), ERXQ.and(
							ERXQ.equals(_IDepenseExtourneCredits.TO_ORGAN_KEY, engagementBudget().organ()),
							ERXQ.equals(_IDepenseExtourneCredits.TO_EXERCICE_KEY, engagementBudget().exercice()),
							ERXQ.equals(_IDepenseExtourneCredits.TO_TYPE_CREDIT_KEY, engagementBudget().typeCredit())
							));
				}

			}
			if (source == null && engagementBudget() != null) {
				source = engagementBudget().source();
			}
		}
		return source;
	}

	public ESourceCreditType getSourceTypeCredit() {
		return sourceTypeCredit;
	}

	public void setSourceTypeCredit(ESourceCreditType sourceTypeCredit) {
		this.sourceTypeCredit = sourceTypeCredit;
	}

	public Boolean isExtourne() {
		return Boolean.FALSE;
	}

	public Boolean isSurExtourne() {
		if (getSourceTypeCredit() == null) {
			initSourceTypeCredit();
		}
		return ESourceCreditType.EXTOURNE.equals(getSourceTypeCredit());
	}

	private void initSourceTypeCredit() {
		setSourceTypeCredit(ESourceCreditType.BUDGET);

	}

	protected boolean isGenericControleGood(String qualifierKey, NSArray depenses, String computeKey, BigDecimal ht, BigDecimal ttc) {
		
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifierKey + "=nil", null);
		NSArray array = EOQualifier.filteredArrayWithQualifier(depenses, qual);
		
		if (array != null && array.count() > 0) return false;

		if (computeSumForKey(depenses, computeKey) != Calculs.CENT) return false;
		if (ht.compareTo(Calculs.ZERO) != 0) return false;
		if (ttc.compareTo(Calculs.ZERO) != 0) return false;

	    return true;
	}
	

}