/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOUtilisateur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleUtilisateur";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_UTILISATEUR";


//Attribute Keys
	public static final ERXKey<Integer> PERS_ID_CLE = new ERXKey<Integer>("persIdCle");
	public static final ERXKey<NSTimestamp> UTL_FERMETURE = new ERXKey<NSTimestamp>("utlFermeture");
	public static final ERXKey<NSTimestamp> UTL_OUVERTURE = new ERXKey<NSTimestamp>("utlOuverture");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> FWK_CARAMBOLE_COMMANDE_DOCUMENTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>("fwkCaramboleCommandeDocuments");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOIndividu>("individu");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> REPART_STRUCTURES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>("repartStructures");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String PERS_ID_CLE_KEY = "persIdCle";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

//Attributs non visibles
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PERS_ID_KEY = "persId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PERS_ID_CLE_COLKEY = "PERS_ID";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY = "fwkCaramboleCommandeDocuments";
	public static final String INDIVIDU_KEY = "individu";
	public static final String PERSONNE_KEY = "personne";
	public static final String REPART_STRUCTURES_KEY = "repartStructures";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
	public Integer persIdCle() {
	 return (Integer) storedValueForKey(PERS_ID_CLE_KEY);
	}

	public void setPersIdCle(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CLE_KEY);
	}

	public NSTimestamp utlFermeture() {
	 return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
	}

	public void setUtlFermeture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_FERMETURE_KEY);
	}

	public NSTimestamp utlOuverture() {
	 return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
	}

	public void setUtlOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOIndividu individu() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOIndividu)storedValueForKey(INDIVIDU_KEY);
	}

	public void setIndividuRelationship(org.cocktail.fwkcktldepense.server.metier.EOIndividu value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOIndividu oldValue = individu();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktldepense.server.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
	}

	public void setTypeEtatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)storedValueForKey(FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments(EOQualifier qualifier) {
	 return fwkCaramboleCommandeDocuments(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments(EOQualifier qualifier, boolean fetch) {
	 return fwkCaramboleCommandeDocuments(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = fwkCaramboleCommandeDocuments();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToFwkCaramboleCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	}
	
	public void removeFromFwkCaramboleCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument createFwkCaramboleCommandeDocumentsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument) eo;
	}
	
	public void deleteFwkCaramboleCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllFwkCaramboleCommandeDocumentsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> objects = fwkCaramboleCommandeDocuments().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteFwkCaramboleCommandeDocumentsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructures() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)storedValueForKey(REPART_STRUCTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructures(EOQualifier qualifier) {
	 return repartStructures(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> results;
			   results = repartStructures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToRepartStructuresRelationship(org.cocktail.fwkcktldepense.server.metier.EORepartStructure object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
	}
	
	public void removeFromRepartStructuresRelationship(org.cocktail.fwkcktldepense.server.metier.EORepartStructure object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EORepartStructure createRepartStructuresRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EORepartStructure.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REPART_STRUCTURES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EORepartStructure) eo;
	}
	
	public void deleteRepartStructuresRelationship(org.cocktail.fwkcktldepense.server.metier.EORepartStructure object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRepartStructuresRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> objects = repartStructures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRepartStructuresRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOUtilisateur avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOUtilisateur createEOUtilisateur(EOEditingContext editingContext				, Integer persIdCle
									, NSTimestamp utlOuverture
					, org.cocktail.fwkcktldepense.server.metier.EOIndividu individu		, org.cocktail.fwkcktldepense.server.metier.EOPersonne personne		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat					) {
	 EOUtilisateur eo = (EOUtilisateur) EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);	 
							eo.setPersIdCle(persIdCle);
											eo.setUtlOuverture(utlOuverture);
						 eo.setIndividuRelationship(individu);
				 eo.setPersonneRelationship(personne);
				 eo.setTypeEtatRelationship(typeEtat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOUtilisateur creerInstance(EOEditingContext editingContext) {
		EOUtilisateur object = (EOUtilisateur)EOUtilities.createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);
  		return object;
		}

	

  public EOUtilisateur localInstanceIn(EOEditingContext editingContext) {
    EOUtilisateur localInstance = (EOUtilisateur)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOUtilisateur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOUtilisateur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> objectsForRecherche(EOEditingContext ec,
														NSTimestamp dateBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOUtilisateur.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (dateBinding != null)
			bindings.takeValueForKey(dateBinding, "date");
		  if (typeEtatBinding != null)
			bindings.takeValueForKey(typeEtatBinding, "typeEtat");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
