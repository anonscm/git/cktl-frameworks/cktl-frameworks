/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.ArticleException;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryArticle;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem;

import com.webobjects.foundation.NSValidation;

public class EOArticle extends _EOArticle {

	// TODO : verif les coherences entre ht et ttc pour pas avoir ttc<ht
	private Number artIdProc;
	boolean isCatalogable;
	boolean isCalculTotalTtcParHt = true;

	public EOArticle() {
		super();
		artIdProc = null;
		isCatalogable = false;
	}

	public void setArtIdProc(Number value) {
		artIdProc = value;
	}

	public Number artIdProc() {
		return artIdProc;
	}

	public void setIsCatalogable(boolean bool) {
		isCatalogable = bool;
	}

	public boolean isCatalogable() {
		return isCatalogable;
	}

	public void setTypeAchatRelationship(EOTypeAchat value) {
		super.setTypeAchatRelationship(value);
		new FactoryArticle().majCommande(commande(), codeExer(), typeAchat());
	}

	public void setCodeExerRelationship(EOCodeExer value) {
		super.setCodeExerRelationship(value);
		new FactoryArticle().majCommande(commande(), codeExer(), typeAchat());
	}

	protected BigDecimal calculePuTtc() {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();

		if (artPrixHt() == null || tva() == null)
			return new BigDecimal(0.0);

		return artPrixHt().add(tva().montantTva(artPrixHt(), arrondi));
	}

	public void setArtPrixHt(BigDecimal artPrixHt) {
		isCalculTotalTtcParHt = true;
		setLocalArtPrixHt(artPrixHt);
	}

	public void setLocalArtPrixHt(BigDecimal artPrixHt) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		if (artPrixHt == null)
			artPrixHt = new BigDecimal(0.0);
		artPrixHt = artPrixHt.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setArtPrixHt(artPrixHt);
		BigDecimal qte = artQuantite();
		if (qte != null /* && qte.floatValue()>0 */) {
			setArtPrixTotalHt(qte.multiply(artPrixHt));
			EOTva tva = tva();
			if (tva != null && isCalculTotalTtcParHt) {
				setLocalArtPrixTtc(artPrixHt.add(tva().montantTva(artPrixHt(), arrondi)));
			}
			BigDecimal artPrixTotalHt = artPrixTotalHt();
			if (artPrixTotalHt != null && tva != null) {
				setArtPrixTotalTtc(artPrixTotalHt.add(tva.montantTva(artPrixTotalHt, arrondi)));
			}
		}
	}

	public void setTva(EOTva value) {

		takeStoredValueForKey(value, EOArticle.TVA_KEY);
		if (value != null) {
			BigDecimal artPrixHt = artPrixHt();
			BigDecimal artPrixTtc = artPrixTtc();

			int arrondi = EODevise.defaultNbDecimales;
			if (commande() != null)
				arrondi = commande().decimalesPourArrondirMontant();

			if (artPrixHt != null && artPrixHt.abs().floatValue() > 0) {
				isCalculTotalTtcParHt = true;
				setLocalArtPrixTtc(artPrixHt.add(value.montantTva(artPrixHt, arrondi)));
			}
			else if (artPrixTtc != null && artPrixTtc.abs().floatValue() > 0) {
				isCalculTotalTtcParHt = false;
				setLocalArtPrixHt(value.montantHtAvecTtc(artPrixTtc, arrondi));
			}
		}
	}

	public void setArtPrixTtc(BigDecimal value) {
		isCalculTotalTtcParHt = false;
		setLocalArtPrixTtc(value);
	}

	protected void setLocalArtPrixTtc(BigDecimal value) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		if (value == null)
			value = new BigDecimal(0.0);
		value = value.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

		BigDecimal artPrixHt = artPrixHt();
		EOTva tva = tva();
		BigDecimal qte = artQuantite();

		// TODO : eventuellement limite a une fourchette la correction du ttc par rapport a la tva et au ht 
		// par exemple 1 euro ? ou pourcentage du prix  ?
		if (artPrixHt != null && artPrixHt.abs().floatValue() > value.abs().floatValue()) {
			value = artPrixHt;
		}
		super.setArtPrixTtc(value);

		if (artPrixHt == null || artPrixHt.abs().floatValue() == 0) {
			isCalculTotalTtcParHt = false;
			//  on recalcule le prix ht
			if (tva != null) {
				setLocalArtPrixHt(tva.montantHtAvecTtc(value, arrondi));
			}
		}

		setArtQuantite(qte);
	}

	public void setArtQuantite(BigDecimal artQuantite) {
		if (artQuantite != null)
			artQuantite = artQuantite.setScale(2, BigDecimal.ROUND_HALF_UP);

		super.setArtQuantite(artQuantite);

		if (artQuantite != null) {
			BigDecimal artPrixHt = artPrixHt();
			if (artPrixHt != null) {
				setArtPrixTotalHt(artPrixHt.multiply(artQuantite));
			}

			// on teste si le ttc est modifie
			if (artPrixTtc() == null || (artPrixTtc().equals(calculePuTtc()) && isCalculTotalTtcParHt)) {
				// si non alors 
				int arrondi = EODevise.defaultNbDecimales;
				if (commande() != null)
					arrondi = commande().decimalesPourArrondirMontant();
				if (artPrixTotalHt() != null && tva() != null)
					setArtPrixTotalTtc(artPrixTotalHt().add(tva().montantTva(artPrixTotalHt(), arrondi)));
			}
			else {
				// si oui ...
				if (artPrixTtc() != null && artQuantite != null)
					setArtPrixTotalTtc(artPrixTtc().multiply(artQuantite));
			}
		}
	}

	public void setArtPrixTotalHt(BigDecimal artPrixTotalHt) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		artPrixTotalHt = artPrixTotalHt.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setArtPrixTotalHt(artPrixTotalHt);
		// miseAJourTtc();
		new FactoryArticle().majCommande(commande(), codeExer(), typeAchat());
	}

	public void setArtPrixTotalTtc(BigDecimal artPrixTotalTtc) {
		int arrondi = EODevise.defaultNbDecimales;
		if (commande() != null)
			arrondi = commande().decimalesPourArrondirMontant();
		artPrixTotalTtc = artPrixTotalTtc.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setArtPrixTotalTtc(artPrixTotalTtc);
		new FactoryArticle().majCommande(commande(), codeExer(), typeAchat());
	}

	/*
	 * EGE 09/06/2007: Version originale private void miseAJourTtc() { if (tva()==null || artPrixHt()==null) return;
	 * setArtPrixTtc(artPrixHt().add(tva().montantTva(artPrixHt()))); }
	 * 
	 * private void miseAJourHt() { if (tva()==null || artPrixTtc()==null) return; setArtPrixHt(tva().montantHtAvecTtc(artPrixTtc())); }
	 * 
	 * public void setArtPrixHt(BigDecimal artPrixHt) { int arrondi=EODevise.defaultNbDecimales; if (commande()!=null)
	 * arrondi=commande().decimalesPourArrondirMontant(); if (artPrixHt==null) artPrixHt=new BigDecimal(0.0); artPrixHt=artPrixHt.setScale(arrondi,
	 * BigDecimal.ROUND_HALF_UP); super.setArtPrixHt(artPrixHt); if (artQuantite()!=null) { setArtPrixTotalHt(artQuantite().multiply(artPrixHt()));
	 * miseAJourTtc(); } }
	 * 
	 * public void setTva(EOTva value) { super.setTva(value);
	 * 
	 * miseAJourTtc(); }
	 * 
	 * public void setArtPrixTtc(BigDecimal value) { int arrondi=EODevise.defaultNbDecimales; if (commande()!=null)
	 * arrondi=commande().decimalesPourArrondirMontant(); if (value==null) value=new BigDecimal(0.0); value=value.setScale(arrondi,
	 * BigDecimal.ROUND_HALF_UP); // TODO : eventuellement limite a une fourchette la correction du ttc par rapport a la tva et au ht // par exemple 1
	 * euro ? ou pourcentage du prix ?
	 * 
	 * BigDecimal ancienTtc=artPrixTtc(); if (ancienTtc==null) ancienTtc=new BigDecimal(0.0);
	 * 
	 * if (value.floatValue()!=0 && (artPrixHt()==null || artPrixHt().floatValue()==0)) { super.setArtPrixTtc(value); miseAJourHt(); } else { if
	 * (artPrixHt()!=null && ((value.floatValue()<artPrixHt().floatValue() && artPrixHt().floatValue()>=0) ||
	 * (value.floatValue()>artPrixHt().floatValue() && artPrixHt().floatValue()<0))) value=artPrixHt(); super.setArtPrixTtc(value); if
	 * (artQuantite()!=null) { if (artPrixTotalHt()!=null && artPrixTotalHt().floatValue()!=0.0 && tva()!=null &&
	 * artPrixHt().add(tva().montantTva(artPrixHt())).floatValue()!=ancienTtc.floatValue())
	 * setArtPrixTotalTtc(artPrixTotalHt().add(tva().montantTva(artPrixTotalHt()))); else setArtPrixTotalTtc(artQuantite().multiply(artPrixTtc())); }
	 * } }
	 * 
	 * public void setArtQuantite(BigDecimal artQuantite) { artQuantite=artQuantite.setScale(2, BigDecimal.ROUND_HALF_UP);
	 * super.setArtQuantite(artQuantite); if (artPrixHt()!=null) // setArtPrixTotalTtc(artQuantite().multiply(artPrixTtc()));
	 * setArtPrixTotalHt(artQuantite().multiply(artPrixHt())); //if (artPrixTtc()!=null) // setArtPrixTotalTtc(artQuantite().multiply(artPrixTtc()));
	 * }
	 * 
	 * public void setArtPrixTotalHt(BigDecimal artPrixTotalHt) { int arrondi=EODevise.defaultNbDecimales; if (commande()!=null)
	 * arrondi=commande().decimalesPourArrondirMontant(); artPrixTotalHt=artPrixTotalHt.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
	 * super.setArtPrixTotalHt(artPrixTotalHt); miseAJourTtc(); new FactoryArticle().majCommande(commande(), codeExer(), typeAchat()); }
	 * 
	 * public void setArtPrixTotalTtc(BigDecimal artPrixTotalTtc) { int arrondi=EODevise.defaultNbDecimales; if (commande()!=null)
	 * arrondi=commande().decimalesPourArrondirMontant(); artPrixTotalTtc=artPrixTotalTtc.setScale(arrondi, BigDecimal.ROUND_HALF_UP); // TODO :
	 * eventuellement limite a une fourchette la correction du ttc par rapport a la tva et au ht // par exemple 1 euro ? ou pourcentage du prix ? if
	 * (artPrixTotalHt()!=null && ((artPrixTotalHt().floatValue()>0.0 && artPrixTotalTtc.floatValue()<artPrixTotalHt().floatValue()) ||
	 * (artPrixTotalHt().floatValue()<0.0 && artPrixTotalTtc.floatValue()>artPrixTotalHt().floatValue()))) artPrixTotalTtc=artPrixTotalHt();
	 * super.setArtPrixTotalTtc(artPrixTotalTtc); new FactoryArticle().majCommande(commande(), codeExer(), typeAchat()); }
	 */public String artLibelleCourt() {
		String artLibelleCourt = artLibelle();
		if (artLibelleCourt != null && artLibelleCourt.length() > 25)
			artLibelleCourt = artLibelleCourt.substring(0, 25);
		return artLibelleCourt;
	}

	public String artReferenceCourt() {
		String res = artReference();
		if (res != null && res.length() > 25)
			res = res.substring(0, 25);
		return res;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws ArticleException
	 */
	public void validateObjectMetier() throws ArticleException {
		if (codeExer() == null)
			throw new ArticleException(ArticleException.codeExerManquant);
		if (artQuantite() == null)
			throw new ArticleException(ArticleException.artQuantiteManquant);
		if (artQuantite().floatValue() <= 0.0)
			throw new ArticleException(ArticleException.artQuantiteNegative);
		if (artPrixHt() == null)
			throw new ArticleException(ArticleException.artPrixHtManquant);
		if (artPrixTtc() == null)
			throw new ArticleException(ArticleException.artPrixTtcManquant);

		if (tva() == null)
			throw new ArticleException(ArticleException.tvaManquant);
		if (artLibelle() == null || artLibelle().length() == 0)
			throw new ArticleException(ArticleException.artLibelleManquant);
		checkLongueursMax();
		if (commande() == null)
			throw new ArticleException(ArticleException.commandeManquant);

		if (artPrixTotalHt() == null)
			setArtPrixTotalHt(artQuantite().multiply(artPrixHt()));
		if (artPrixTotalTtc() == null)
			setArtPrixTotalTtc(artQuantite().multiply(artPrixTtc()));

		// on verifie que le abs(ttc) > abs(ht) mais on ne fait pas la comparaison ttc = ht * tauxtva 
		// car on ne sait pas si le ttc n'a pas ete corrige par l'utilisateur
		//if (!artPrixHt().add(tva().montantTva(artPrixHt())).equals(artPrixTtc()))
		if ((artPrixHt().floatValue() < 0.0 && artPrixTtc().floatValue() > 0.0) ||
				(artPrixHt().floatValue() > 0.0 && artPrixTtc().floatValue() < 0.0))
			throw new ArticleException(ArticleException.artPrixTtcPasCoherent);
		if (artPrixHt().abs().compareTo(artPrixTtc().abs()) == 1)
			throw new ArticleException(ArticleException.artPrixTtcPasCoherent);
		if (artPrixTotalHt().abs().compareTo(artPrixTotalTtc().abs()) == 1)
			throw new ArticleException(ArticleException.artPrixTtcPasCoherent);

		//if (!artQuantite().multiply(artPrixHt()).setScale(2, BigDecimal.ROUND_HALF_UP).equals(artPrixTotalHt()))
		//    throw new ArticleException(ArticleException.artPrixTotalHtPasCoherent);
		//if (!artQuantite().multiply(artPrixTtc()).setScale(2, BigDecimal.ROUND_HALF_UP).equals(artPrixTotalTtc()))
		//    throw new ArticleException(ArticleException.artPrixTotalTtcPasCoherent);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (attribution() == null && typeAchat() == null)
			throw new ArticleException(ArticleException.attributionOuTypeAchat);
		if (attribution() != null && typeAchat() != null)
			throw new ArticleException(ArticleException.attributionOuTypeAchat);
	}

	public EOB2bCxmlItem toB2bCxmlItem() {
		if (toB2bCxmlItems() != null && toB2bCxmlItems().count() > 0) {
			return (EOB2bCxmlItem) toB2bCxmlItems().objectAtIndex(0);
		}
		return null;
	}

	public void setToB2bCxmlItem(EOB2bCxmlItem cxmlItem) {
		deleteAllToB2bCxmlItemsRelationships();
		addToToB2bCxmlItemsRelationship(cxmlItem);
	}

	/**
	 * Verifie la taille des champs textes qui peuvent être saisis.
	 * 
	 * @throws ArticleException
	 */
	public void checkLongueursMax() throws ArticleException {
		try {
			checkLongueurMax(EOArticle.ART_LIBELLE_KEY);
		} catch (Exception e) {
			throw new ArticleException("Description : " + e.getMessage());
		}
		try {
			checkLongueurMax(EOArticle.ART_REFERENCE_KEY);
		} catch (Exception e) {
			throw new ArticleException("Référence : " + e.getMessage());
		}

	}

	public void checkLongueurMax(String attributeName) throws FactoryException {
		if (valueForKey(attributeName) != null && ((String) valueForKey(attributeName)).length() > maxLengthForAttribute(attributeName)) {
			throw new FactoryException(FactoryException.tailleLimiteDepassee + "(" + ((String) valueForKey(attributeName)).length() + " saisis / " + maxLengthForAttribute(attributeName) + " caractères autorisés)");
		}

	}
}
