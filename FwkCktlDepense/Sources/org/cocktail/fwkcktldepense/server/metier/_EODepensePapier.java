/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EODepensePapier.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EODepensePapier extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleDepensePapier";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_PAPIER";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DPP_DATE_FACTURE = new ERXKey<NSTimestamp>("dppDateFacture");
	public static final ERXKey<NSTimestamp> DPP_DATE_RECEPTION = new ERXKey<NSTimestamp>("dppDateReception");
	public static final ERXKey<NSTimestamp> DPP_DATE_SAISIE = new ERXKey<NSTimestamp>("dppDateSaisie");
	public static final ERXKey<NSTimestamp> DPP_DATE_SERVICE_FAIT = new ERXKey<NSTimestamp>("dppDateServiceFait");
	public static final ERXKey<java.math.BigDecimal> DPP_HT_INITIAL = new ERXKey<java.math.BigDecimal>("dppHtInitial");
	public static final ERXKey<java.math.BigDecimal> DPP_HT_SAISIE = new ERXKey<java.math.BigDecimal>("dppHtSaisie");
	public static final ERXKey<Integer> DPP_IM_DGP = new ERXKey<Integer>("dppImDgp");
	public static final ERXKey<java.math.BigDecimal> DPP_IM_TAUX = new ERXKey<java.math.BigDecimal>("dppImTaux");
	public static final ERXKey<Integer> DPP_NB_PIECE = new ERXKey<Integer>("dppNbPiece");
	public static final ERXKey<String> DPP_NUMERO_FACTURE = new ERXKey<String>("dppNumeroFacture");
	public static final ERXKey<NSTimestamp> DPP_SF_DATE = new ERXKey<NSTimestamp>("dppSfDate");
	public static final ERXKey<java.math.BigDecimal> DPP_TTC_INITIAL = new ERXKey<java.math.BigDecimal>("dppTtcInitial");
	public static final ERXKey<java.math.BigDecimal> DPP_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("dppTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DPP_TVA_INITIAL = new ERXKey<java.math.BigDecimal>("dppTvaInitial");
	public static final ERXKey<java.math.BigDecimal> DPP_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("dppTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> DEPENSE_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>("depenseBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> DEPENSE_PAPIER_REVERSEMENT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepensePapier>("depensePapierReversement");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail> ECRITURE_DETAIL = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail>("ecritureDetail");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur> FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOFournisseur>("fournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux> IM_TYPE_TAUX = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux>("imTypeTaux");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOModePaiement> MODE_PAIEMENT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOModePaiement>("modePaiement");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> PRE_DEPENSE_BUDGETS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>("preDepenseBudgets");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur> RIB_FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EORibFournisseur>("ribFournisseur");
	public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> SF_PERSONNE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("sfPersonne");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dppId";

	public static final String DPP_DATE_FACTURE_KEY = "dppDateFacture";
	public static final String DPP_DATE_RECEPTION_KEY = "dppDateReception";
	public static final String DPP_DATE_SAISIE_KEY = "dppDateSaisie";
	public static final String DPP_DATE_SERVICE_FAIT_KEY = "dppDateServiceFait";
	public static final String DPP_HT_INITIAL_KEY = "dppHtInitial";
	public static final String DPP_HT_SAISIE_KEY = "dppHtSaisie";
	public static final String DPP_IM_DGP_KEY = "dppImDgp";
	public static final String DPP_IM_TAUX_KEY = "dppImTaux";
	public static final String DPP_NB_PIECE_KEY = "dppNbPiece";
	public static final String DPP_NUMERO_FACTURE_KEY = "dppNumeroFacture";
	public static final String DPP_SF_DATE_KEY = "dppSfDate";
	public static final String DPP_TTC_INITIAL_KEY = "dppTtcInitial";
	public static final String DPP_TTC_SAISIE_KEY = "dppTtcSaisie";
	public static final String DPP_TVA_INITIAL_KEY = "dppTvaInitial";
	public static final String DPP_TVA_SAISIE_KEY = "dppTvaSaisie";

//Attributs non visibles
	public static final String DPP_ID_KEY = "dppId";
	public static final String DPP_ID_REVERSEMENT_KEY = "dppIdReversement";
	public static final String DPP_SF_PERS_ID_KEY = "dppSfPersId";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String IMTT_ID_KEY = "imttId";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DPP_DATE_FACTURE_COLKEY = "DPP_DATE_FACTURE";
	public static final String DPP_DATE_RECEPTION_COLKEY = "DPP_DATE_RECEPTION";
	public static final String DPP_DATE_SAISIE_COLKEY = "DPP_DATE_SAISIE";
	public static final String DPP_DATE_SERVICE_FAIT_COLKEY = "DPP_DATE_SERVICE_FAIT";
	public static final String DPP_HT_INITIAL_COLKEY = "DPP_HT_INITIAL";
	public static final String DPP_HT_SAISIE_COLKEY = "DPP_HT_SAISIE";
	public static final String DPP_IM_DGP_COLKEY = "DPP_IM_DGP";
	public static final String DPP_IM_TAUX_COLKEY = "DPP_IM_TAUX";
	public static final String DPP_NB_PIECE_COLKEY = "DPP_NB_PIECE";
	public static final String DPP_NUMERO_FACTURE_COLKEY = "DPP_NUMERO_FACTURE";
	public static final String DPP_SF_DATE_COLKEY = "DPP_SF_DATE";
	public static final String DPP_TTC_INITIAL_COLKEY = "DPP_TTC_INITIAL";
	public static final String DPP_TTC_SAISIE_COLKEY = "DPP_TTC_SAISIE";
	public static final String DPP_TVA_INITIAL_COLKEY = "DPP_TVA_INITIAL";
	public static final String DPP_TVA_SAISIE_COLKEY = "DPP_TVA_SAISIE";

	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String DPP_ID_REVERSEMENT_COLKEY = "DPP_ID_REVERSEMENT";
	public static final String DPP_SF_PERS_ID_COLKEY = "DPP_SF_PERS_ID";
	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String IMTT_ID_COLKEY = "IMTT_ID";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_BUDGETS_KEY = "depenseBudgets";
	public static final String DEPENSE_PAPIER_REVERSEMENT_KEY = "depensePapierReversement";
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String IM_TYPE_TAUX_KEY = "imTypeTaux";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String PRE_DEPENSE_BUDGETS_KEY = "preDepenseBudgets";
	public static final String RIB_FOURNISSEUR_KEY = "ribFournisseur";
	public static final String SF_PERSONNE_KEY = "sfPersonne";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public NSTimestamp dppDateFacture() {
	 return (NSTimestamp) storedValueForKey(DPP_DATE_FACTURE_KEY);
	}

	public void setDppDateFacture(NSTimestamp value) {
	 takeStoredValueForKey(value, DPP_DATE_FACTURE_KEY);
	}

	public NSTimestamp dppDateReception() {
	 return (NSTimestamp) storedValueForKey(DPP_DATE_RECEPTION_KEY);
	}

	public void setDppDateReception(NSTimestamp value) {
	 takeStoredValueForKey(value, DPP_DATE_RECEPTION_KEY);
	}

	public NSTimestamp dppDateSaisie() {
	 return (NSTimestamp) storedValueForKey(DPP_DATE_SAISIE_KEY);
	}

	public void setDppDateSaisie(NSTimestamp value) {
	 takeStoredValueForKey(value, DPP_DATE_SAISIE_KEY);
	}

	public NSTimestamp dppDateServiceFait() {
	 return (NSTimestamp) storedValueForKey(DPP_DATE_SERVICE_FAIT_KEY);
	}

	public void setDppDateServiceFait(NSTimestamp value) {
	 takeStoredValueForKey(value, DPP_DATE_SERVICE_FAIT_KEY);
	}

	public java.math.BigDecimal dppHtInitial() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_HT_INITIAL_KEY);
	}

	public void setDppHtInitial(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_HT_INITIAL_KEY);
	}

	public java.math.BigDecimal dppHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_HT_SAISIE_KEY);
	}

	public void setDppHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_HT_SAISIE_KEY);
	}

	public Integer dppImDgp() {
	 return (Integer) storedValueForKey(DPP_IM_DGP_KEY);
	}

	public void setDppImDgp(Integer value) {
	 takeStoredValueForKey(value, DPP_IM_DGP_KEY);
	}

	public java.math.BigDecimal dppImTaux() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_IM_TAUX_KEY);
	}

	public void setDppImTaux(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_IM_TAUX_KEY);
	}

	public Integer dppNbPiece() {
	 return (Integer) storedValueForKey(DPP_NB_PIECE_KEY);
	}

	public void setDppNbPiece(Integer value) {
	 takeStoredValueForKey(value, DPP_NB_PIECE_KEY);
	}

	public String dppNumeroFacture() {
	 return (String) storedValueForKey(DPP_NUMERO_FACTURE_KEY);
	}

	public void setDppNumeroFacture(String value) {
	 takeStoredValueForKey(value, DPP_NUMERO_FACTURE_KEY);
	}

	public NSTimestamp dppSfDate() {
	 return (NSTimestamp) storedValueForKey(DPP_SF_DATE_KEY);
	}

	public void setDppSfDate(NSTimestamp value) {
	 takeStoredValueForKey(value, DPP_SF_DATE_KEY);
	}

	public java.math.BigDecimal dppTtcInitial() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_TTC_INITIAL_KEY);
	}

	public void setDppTtcInitial(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_TTC_INITIAL_KEY);
	}

	public java.math.BigDecimal dppTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_TTC_SAISIE_KEY);
	}

	public void setDppTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal dppTvaInitial() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_TVA_INITIAL_KEY);
	}

	public void setDppTvaInitial(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_TVA_INITIAL_KEY);
	}

	public java.math.BigDecimal dppTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DPP_TVA_SAISIE_KEY);
	}

	public void setDppTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DPP_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EODepensePapier depensePapierReversement() {
	 return (org.cocktail.fwkcktldepense.server.metier.EODepensePapier)storedValueForKey(DEPENSE_PAPIER_REVERSEMENT_KEY);
	}

	public void setDepensePapierReversementRelationship(org.cocktail.fwkcktldepense.server.metier.EODepensePapier value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EODepensePapier oldValue = depensePapierReversement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_PAPIER_REVERSEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_PAPIER_REVERSEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail ecritureDetail() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
	}

	public void setEcritureDetailRelationship(org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail oldValue = ecritureDetail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
	}

	public void setFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOFournisseur oldValue = fournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux imTypeTaux() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux)storedValueForKey(IM_TYPE_TAUX_KEY);
	}

	public void setImTypeTauxRelationship(org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux oldValue = imTypeTaux();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IM_TYPE_TAUX_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, IM_TYPE_TAUX_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOModePaiement modePaiement() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
	}

	public void setModePaiementRelationship(org.cocktail.fwkcktldepense.server.metier.EOModePaiement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOModePaiement oldValue = modePaiement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EORibFournisseur ribFournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EORibFournisseur)storedValueForKey(RIB_FOURNISSEUR_KEY);
	}

	public void setRibFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EORibFournisseur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EORibFournisseur oldValue = ribFournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, RIB_FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne sfPersonne() {
	 return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(SF_PERSONNE_KEY);
	}

	public void setSfPersonneRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = sfPersonne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SF_PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, SF_PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)storedValueForKey(DEPENSE_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets(EOQualifier qualifier) {
	 return depenseBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets(EOQualifier qualifier, boolean fetch) {
	 return depenseBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> depenseBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.DEPENSE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = depenseBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
	}
	
	public void removeFromDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EODepenseBudget createDepenseBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EODepenseBudget) eo;
	}
	
	public void deleteDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDepenseBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseBudget> objects = depenseBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDepenseBudgetsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)storedValueForKey(PRE_DEPENSE_BUDGETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets(EOQualifier qualifier) {
	 return preDepenseBudgets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets(EOQualifier qualifier, boolean fetch) {
	 return preDepenseBudgets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> preDepenseBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget.DEPENSE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = preDepenseBudgets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
	}
	
	public void removeFromPreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget createPreDepenseBudgetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_BUDGETS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget) eo;
	}
	
	public void deletePreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPreDepenseBudgetsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> objects = preDepenseBudgets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePreDepenseBudgetsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EODepensePapier avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EODepensePapier createEODepensePapier(EOEditingContext editingContext				, NSTimestamp dppDateFacture
							, NSTimestamp dppDateReception
							, NSTimestamp dppDateSaisie
									, java.math.BigDecimal dppHtInitial
							, java.math.BigDecimal dppHtSaisie
											, Integer dppNbPiece
							, String dppNumeroFacture
									, java.math.BigDecimal dppTtcInitial
							, java.math.BigDecimal dppTtcSaisie
							, java.math.BigDecimal dppTvaInitial
							, java.math.BigDecimal dppTvaSaisie
									, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOFournisseur fournisseur										, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EODepensePapier eo = (EODepensePapier) EOUtilities.createAndInsertInstance(editingContext, _EODepensePapier.ENTITY_NAME);	 
							eo.setDppDateFacture(dppDateFacture);
									eo.setDppDateReception(dppDateReception);
									eo.setDppDateSaisie(dppDateSaisie);
											eo.setDppHtInitial(dppHtInitial);
									eo.setDppHtSaisie(dppHtSaisie);
													eo.setDppNbPiece(dppNbPiece);
									eo.setDppNumeroFacture(dppNumeroFacture);
											eo.setDppTtcInitial(dppTtcInitial);
									eo.setDppTtcSaisie(dppTtcSaisie);
									eo.setDppTvaInitial(dppTvaInitial);
									eo.setDppTvaSaisie(dppTvaSaisie);
										 eo.setExerciceRelationship(exercice);
				 eo.setFournisseurRelationship(fournisseur);
												 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepensePapier creerInstance(EOEditingContext editingContext) {
		EODepensePapier object = (EODepensePapier)EOUtilities.createAndInsertInstance(editingContext, _EODepensePapier.ENTITY_NAME);
  		return object;
		}

	

  public EODepensePapier localInstanceIn(EOEditingContext editingContext) {
    EODepensePapier localInstance = (EODepensePapier)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepensePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepensePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EODepensePapier> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepensePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepensePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepensePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepensePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EODepensePapier> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepensePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepensePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepensePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepensePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepensePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepensePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EODepensePapier> objectsForRecherche(EOEditingContext ec,
														Integer cleBinding,
														org.cocktail.fwkcktldepense.server.metier.EOCommande commandeBinding,
														String dppNumeroFactureBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateurBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EODepensePapier.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (cleBinding != null)
			bindings.takeValueForKey(cleBinding, "cle");
		  if (commandeBinding != null)
			bindings.takeValueForKey(commandeBinding, "commande");
		  if (dppNumeroFactureBinding != null)
			bindings.takeValueForKey(dppNumeroFactureBinding, "dppNumeroFacture");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (utilisateurBinding != null)
			bindings.takeValueForKey(utilisateurBinding, "utilisateur");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
