/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOMandat.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOMandat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleMandat";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_MANDAT";


//Attribute Keys
	public static final ERXKey<NSTimestamp> BOR_DATE_VISA = new ERXKey<NSTimestamp>("borDateVisa");
	public static final ERXKey<Integer> BOR_NUM = new ERXKey<Integer>("borNum");
	public static final ERXKey<String> GES_CODE = new ERXKey<String>("gesCode");
	public static final ERXKey<NSTimestamp> MAN_DATE_PAIEMENT = new ERXKey<NSTimestamp>("manDatePaiement");
	public static final ERXKey<NSTimestamp> MAN_DATE_VISA_PRINC = new ERXKey<NSTimestamp>("manDateVisaPrinc");
	public static final ERXKey<String> MAN_ETAT = new ERXKey<String>("manEtat");
	public static final ERXKey<Integer> MAN_NUMERO = new ERXKey<Integer>("manNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "manId";

	public static final String BOR_DATE_VISA_KEY = "borDateVisa";
	public static final String BOR_NUM_KEY = "borNum";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_DATE_PAIEMENT_KEY = "manDatePaiement";
	public static final String MAN_DATE_VISA_PRINC_KEY = "manDateVisaPrinc";
	public static final String MAN_ETAT_KEY = "manEtat";
	public static final String MAN_NUMERO_KEY = "manNumero";

//Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MAN_ID_KEY = "manId";

//Colonnes dans la base de donnees
	public static final String BOR_DATE_VISA_COLKEY = "BOR_DATE_VISA";
	public static final String BOR_NUM_COLKEY = "BOR_NUM";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MAN_DATE_PAIEMENT_COLKEY = "MAN_DATE_PAIEMENT";
	public static final String MAN_DATE_VISA_PRINC_COLKEY = "MAN_DATE_VISA_PRINC";
	public static final String MAN_ETAT_COLKEY = "MAN_ETAT";
	public static final String MAN_NUMERO_COLKEY = "MAN_NUMERO";

	public static final String BOR_ID_COLKEY = "BOR_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MAN_ID_COLKEY = "MAN_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
	public NSTimestamp borDateVisa() {
	 return (NSTimestamp) storedValueForKey(BOR_DATE_VISA_KEY);
	}

	public void setBorDateVisa(NSTimestamp value) {
	 takeStoredValueForKey(value, BOR_DATE_VISA_KEY);
	}

	public Integer borNum() {
	 return (Integer) storedValueForKey(BOR_NUM_KEY);
	}

	public void setBorNum(Integer value) {
	 takeStoredValueForKey(value, BOR_NUM_KEY);
	}

	public String gesCode() {
	 return (String) storedValueForKey(GES_CODE_KEY);
	}

	public void setGesCode(String value) {
	 takeStoredValueForKey(value, GES_CODE_KEY);
	}

	public NSTimestamp manDatePaiement() {
	 return (NSTimestamp) storedValueForKey(MAN_DATE_PAIEMENT_KEY);
	}

	public void setManDatePaiement(NSTimestamp value) {
	 takeStoredValueForKey(value, MAN_DATE_PAIEMENT_KEY);
	}

	public NSTimestamp manDateVisaPrinc() {
	 return (NSTimestamp) storedValueForKey(MAN_DATE_VISA_PRINC_KEY);
	}

	public void setManDateVisaPrinc(NSTimestamp value) {
	 takeStoredValueForKey(value, MAN_DATE_VISA_PRINC_KEY);
	}

	public String manEtat() {
	 return (String) storedValueForKey(MAN_ETAT_KEY);
	}

	public void setManEtat(String value) {
	 takeStoredValueForKey(value, MAN_ETAT_KEY);
	}

	public Integer manNumero() {
	 return (Integer) storedValueForKey(MAN_NUMERO_KEY);
	}

	public void setManNumero(Integer value) {
	 takeStoredValueForKey(value, MAN_NUMERO_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}


	/**
	* Créer une instance de EOMandat avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOMandat createEOMandat(EOEditingContext editingContext								, String gesCode
											, String manEtat
							, Integer manNumero
					, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice					) {
	 EOMandat eo = (EOMandat) EOUtilities.createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);	 
											eo.setGesCode(gesCode);
													eo.setManEtat(manEtat);
									eo.setManNumero(manNumero);
						 eo.setExerciceRelationship(exercice);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandat creerInstance(EOEditingContext editingContext) {
		EOMandat object = (EOMandat)EOUtilities.createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);
  		return object;
		}

	

  public EOMandat localInstanceIn(EOEditingContext editingContext) {
    EOMandat localInstance = (EOMandat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOMandat>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMandat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMandat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOMandat> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOMandat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMandat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMandat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMandat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMandat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
