

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleMarcheException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;



public class EOCommandeControleMarche extends _EOCommandeControleMarche
{
	private static Calculs serviceCalculs = Calculs.getInstance();
	
    public EOCommandeControleMarche() {
        super();
    }

    public void setCmarHtSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCmarHtSaisie(aValue);

		if (commandeBudget()!=null) {

			if (commandeBudget().commande()!=null) {
				EOTva tva=commandeBudget().commande().tvaCommune(null);
				if (tva!=null) {
					setCmarTtcSaisie(cmarHtSaisie().add(tva.montantTva(cmarHtSaisie(), arrondi)));
					return;
				}
			}

			if (cmarTtcSaisie()==null || cmarTtcSaisie().floatValue()<cmarHtSaisie().floatValue()) {
				setCmarTtcSaisie(cmarHtSaisie());
				return;
			}
			
			setCmarTvaSaisie(cmarTtcSaisie().subtract(cmarHtSaisie()));
			if (commandeBudget().tauxProrata()!=null)
				setCmarMontantBudgetaire(calculMontantBudgetaire());
			commandeBudget().mettreAJourMontants();
		}
    }

    public void setCmarTvaSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCmarTvaSaisie(aValue);
    }

    public void setCmarTtcSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCmarTtcSaisie(aValue);
        
        if (cmarHtSaisie()==null || cmarHtSaisie().floatValue()>cmarTtcSaisie().floatValue()) {
			if (commandeBudget()!=null && commandeBudget().commande()!=null) {
				EOTva tva=commandeBudget().commande().tvaCommune(null);
				if (tva!=null) {
					setCmarHtSaisie(tva.montantHtAvecTtc(cmarTtcSaisie(), arrondi));
					return;
				}
			}
			
			setCmarHtSaisie(cmarTtcSaisie());
			return;
        }
        
        setCmarTvaSaisie(cmarTtcSaisie().subtract(cmarHtSaisie()));
        
        if (commandeBudget()!=null) {
        	if (commandeBudget().tauxProrata()!=null)
        		setCmarMontantBudgetaire(calculMontantBudgetaire());
			commandeBudget().mettreAJourMontants();
        }
    }

    public void setCmarMontantBudgetaire(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
		aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCmarMontantBudgetaire(aValue);
    }

    public void setCmarPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
        super.setCmarPourcentage(aValue);
        
    	if (commandeBudget()!=null) {
       		NSArray commandeControleMarches = commandeBudget().commandeControleMarches();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControleMarche.ATTRIBUTION_KEY+" != nil", null);
    		commandeControleMarches = EOQualifier.filteredArrayWithQualifier(commandeControleMarches, qual);
    		BigDecimal total=commandeBudget().computeSumForKey(commandeControleMarches, EOCommandeControleMarche.CMAR_POURCENTAGE_KEY);
    		if (total.floatValue()>100.0)
    	        super.setCmarPourcentage(new BigDecimal(100.0).subtract(total.subtract(aValue)));    			
    	}
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws CommandeControleMarcheException {
        if (cmarHtSaisie()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.cmarHtSaisieManquant);
        if (cmarTvaSaisie()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.cmarTvaSaisieManquant);
        if (cmarTtcSaisie()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.cmarTtcSaisieManquant);
        if (cmarMontantBudgetaire()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.cmarMontantBudgetaireManquant);
        if (exercice()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.exerciceManquant);
        if (commandeBudget()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.commandeBudgetManquant);
        
        if (!cmarHtSaisie().abs().add(cmarTvaSaisie().abs()).equals(cmarTtcSaisie().abs()))
        	setCmarTvaSaisie(cmarTtcSaisie().subtract(cmarHtSaisie()));
        if (commandeBudget().tauxProrata()!=null && !cmarMontantBudgetaire().equals(calculMontantBudgetaire()))
        	setCmarMontantBudgetaire(calculMontantBudgetaire());

        if (cmarTvaSaisie().floatValue()<0.0 || cmarHtSaisie().floatValue()<0.0 || cmarTtcSaisie().floatValue()<0.0 ||
        		cmarMontantBudgetaire().floatValue()<0.0)
        	throw new CommandeControleMarcheException(CommandeControleMarcheException.montantsNegatifs);

        if (!commandeBudget().exercice().equals(exercice()))
            throw new CommandeControleMarcheException(CommandeControleMarcheException.commandeBudgetExercicePasCoherent);
    }

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(cmarHtSaisie(), cmarTvaSaisie(), commandeBudget().tauxProrata().tapTaux());
	}
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        if (attribution()==null)
            throw new CommandeControleMarcheException(CommandeControleMarcheException.attributionManquant);
    }
}
