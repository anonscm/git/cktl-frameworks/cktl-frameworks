/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeControleHorsMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeControleHorsMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeControleHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_CTRL_HORS_MARCHE";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> CHOM_HT_SAISIE = new ERXKey<java.math.BigDecimal>("chomHtSaisie");
	public static final ERXKey<java.math.BigDecimal> CHOM_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("chomMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> CHOM_POURCENTAGE = new ERXKey<java.math.BigDecimal>("chomPourcentage");
	public static final ERXKey<java.math.BigDecimal> CHOM_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("chomTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> CHOM_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("chomTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> CODE_EXER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>("codeExer");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget> COMMANDE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget>("commandeBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat> TYPE_ACHAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat>("typeAchat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "chomId";

	public static final String CHOM_HT_SAISIE_KEY = "chomHtSaisie";
	public static final String CHOM_MONTANT_BUDGETAIRE_KEY = "chomMontantBudgetaire";
	public static final String CHOM_POURCENTAGE_KEY = "chomPourcentage";
	public static final String CHOM_TTC_SAISIE_KEY = "chomTtcSaisie";
	public static final String CHOM_TVA_SAISIE_KEY = "chomTvaSaisie";

//Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String CHOM_ID_KEY = "chomId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String CHOM_HT_SAISIE_COLKEY = "CHOM_HT_SAISIE";
	public static final String CHOM_MONTANT_BUDGETAIRE_COLKEY = "CHOM_MONTANT_BUDGETAIRE";
	public static final String CHOM_POURCENTAGE_COLKEY = "CHOM_POURCENTAGE";
	public static final String CHOM_TTC_SAISIE_COLKEY = "CHOM_TTC_SAISIE";
	public static final String CHOM_TVA_SAISIE_COLKEY = "CHOM_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String CHOM_ID_COLKEY = "CHOM_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String COMMANDE_BUDGET_KEY = "commandeBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
	public java.math.BigDecimal chomHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CHOM_HT_SAISIE_KEY);
	}

	public void setChomHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CHOM_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal chomMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(CHOM_MONTANT_BUDGETAIRE_KEY);
	}

	public void setChomMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CHOM_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal chomPourcentage() {
	 return (java.math.BigDecimal) storedValueForKey(CHOM_POURCENTAGE_KEY);
	}

	public void setChomPourcentage(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CHOM_POURCENTAGE_KEY);
	}

	public java.math.BigDecimal chomTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CHOM_TTC_SAISIE_KEY);
	}

	public void setChomTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CHOM_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal chomTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(CHOM_TVA_SAISIE_KEY);
	}

	public void setChomTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CHOM_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
	}

	public void setCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeExer oldValue = codeExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget commandeBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget)storedValueForKey(COMMANDE_BUDGET_KEY);
	}

	public void setCommandeBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget oldValue = commandeBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
	}

	public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeAchat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeAchat oldValue = typeAchat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOCommandeControleHorsMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeControleHorsMarche createEOCommandeControleHorsMarche(EOEditingContext editingContext				, java.math.BigDecimal chomHtSaisie
							, java.math.BigDecimal chomMontantBudgetaire
									, java.math.BigDecimal chomTtcSaisie
							, java.math.BigDecimal chomTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer		, org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget commandeBudget		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat					) {
	 EOCommandeControleHorsMarche eo = (EOCommandeControleHorsMarche) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeControleHorsMarche.ENTITY_NAME);	 
							eo.setChomHtSaisie(chomHtSaisie);
									eo.setChomMontantBudgetaire(chomMontantBudgetaire);
											eo.setChomTtcSaisie(chomTtcSaisie);
									eo.setChomTvaSaisie(chomTvaSaisie);
						 eo.setCodeExerRelationship(codeExer);
				 eo.setCommandeBudgetRelationship(commandeBudget);
				 eo.setExerciceRelationship(exercice);
				 eo.setTypeAchatRelationship(typeAchat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeControleHorsMarche creerInstance(EOEditingContext editingContext) {
		EOCommandeControleHorsMarche object = (EOCommandeControleHorsMarche)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeControleHorsMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeControleHorsMarche localInstanceIn(EOEditingContext editingContext) {
    EOCommandeControleHorsMarche localInstance = (EOCommandeControleHorsMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeControleHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeControleHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeControleHorsMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeControleHorsMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeControleHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeControleHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeControleHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeControleHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
