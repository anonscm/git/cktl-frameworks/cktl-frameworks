

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;



public class EOCommandeControleHorsMarche extends _EOCommandeControleHorsMarche
{
	private static Calculs serviceCalculs = Calculs.getInstance();
	
	public EOCommandeControleHorsMarche() {
		super();
	}

	public void setChomHtSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
		aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setChomHtSaisie(aValue);

		if (commandeBudget()!=null) {
			if (commandeBudget().commande()!=null && codeExer()!=null) {
				EOTva tva=commandeBudget().commande().tvaCommune(codeExer());
				if (tva!=null) {
					setChomTtcSaisie(chomHtSaisie().add(tva.montantTva(chomHtSaisie(), arrondi)));
					return;
				}
			}

			if (chomTtcSaisie()==null || chomTtcSaisie().floatValue()<chomHtSaisie().floatValue()) {
				setChomTtcSaisie(chomHtSaisie());
				return;
			}
			
			setChomTvaSaisie(chomTtcSaisie().subtract(chomHtSaisie()));
			if (commandeBudget().tauxProrata()!=null)
				setChomMontantBudgetaire(calculMontantBudgetaire());
			commandeBudget().mettreAJourMontants();
		}
	}

	public void setChomTvaSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
		aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setChomTvaSaisie(aValue);
	}

	public void setChomTtcSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
		aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
		super.setChomTtcSaisie(aValue);

        if (chomHtSaisie()==null || chomHtSaisie().floatValue()>chomTtcSaisie().floatValue()) {
			if (commandeBudget()!=null && commandeBudget().commande()!=null && codeExer()!=null) {
				EOTva tva=commandeBudget().commande().tvaCommune(codeExer());
				if (tva!=null) {
					setChomHtSaisie(tva.montantHtAvecTtc(chomTtcSaisie(), arrondi));
					return;
				}
			}
			
			setChomHtSaisie(chomTtcSaisie());
			return;
        }
        
        setChomTvaSaisie(chomTtcSaisie().subtract(chomHtSaisie()));
        
        if (commandeBudget()!=null) {
        	if (commandeBudget().tauxProrata()!=null) {
        		setChomMontantBudgetaire(calculMontantBudgetaire());
        	}
			commandeBudget().mettreAJourMontants();
        }
	}

	public void setChomMontantBudgetaire(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	System.out.println("setChom : "+aValue);
		aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
    	System.out.println("    arrondi : "+aValue);
		super.setChomMontantBudgetaire(aValue);
	}

	public void setChomPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
		aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
		super.setChomPourcentage(aValue);

    	if (commandeBudget()!=null) {
       		NSArray commandeControleHorsMarches = commandeBudget().commandeControleHorsMarches();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.CODE_EXER_KEY+" != nil", null);
    		commandeControleHorsMarches = EOQualifier.filteredArrayWithQualifier(commandeControleHorsMarches, qual);
    		BigDecimal total=commandeBudget().computeSumForKey(commandeControleHorsMarches, EOCommandeControleHorsMarche.CHOM_POURCENTAGE_KEY);
    		if (total.floatValue()>100.0)
    	        super.setChomPourcentage(new BigDecimal(100.0).subtract(total.subtract(aValue)));    			
    	}
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */    
	 public void validateForSave() throws NSValidation.ValidationException {
		 validateObjectMetier();
		 validateBeforeTransactionSave();
		 super.validateForSave();
	 }

	 /**
	  * Peut etre appele e partir des factories.
	  * @throws NSValidation.ValidationException
	  */
	 public void validateObjectMetier() throws CommandeControleHorsMarcheException {
		 if (chomHtSaisie()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.chomHtSaisieManquant);
		 if (chomTvaSaisie()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.chomTvaSaisieManquant);
		 if (chomTtcSaisie()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.chomTtcSaisieManquant);
		 if (chomMontantBudgetaire()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.chomMontantBudgetaireManquant);
		 if (exercice()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.exerciceManquant);
		 if (commandeBudget()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.commandeBudgetManquant);

		 if (!chomHtSaisie().abs().add(chomTvaSaisie().abs()).equals(chomTtcSaisie().abs()))
			 setChomTvaSaisie(chomTtcSaisie().subtract(chomHtSaisie()));
		 if (commandeBudget().tauxProrata()!=null && !chomMontantBudgetaire().equals(calculMontantBudgetaire()))
	        setChomMontantBudgetaire(calculMontantBudgetaire());
	
		 if (chomTvaSaisie().floatValue()<0.0 || chomHtSaisie().floatValue()<0.0 || chomTtcSaisie().floatValue()<0.0 ||
				 chomMontantBudgetaire().floatValue()<0.0)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.montantsNegatifs);

		 if (!commandeBudget().exercice().equals(exercice()))
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.commandeBudgetExercicePasCoherent);
	 }

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(chomHtSaisie(), chomTvaSaisie(), commandeBudget().tauxProrata().tapTaux());
	}

	 /**
	  * A appeler par les validateforsave, forinsert, forupdate.
	  *
	  */
	 private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		 if (codeExer()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.codeExerManquant);
		 if (typeAchat()==null)
			 throw new CommandeControleHorsMarcheException(CommandeControleHorsMarcheException.typeAchatManquant);
	 }
}
