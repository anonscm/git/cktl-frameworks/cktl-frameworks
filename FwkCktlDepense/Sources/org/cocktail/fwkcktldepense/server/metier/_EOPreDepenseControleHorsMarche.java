/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPreDepenseControleHorsMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOPreDepenseControleHorsMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseControleHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> DHOM_HT_SAISIE = new ERXKey<java.math.BigDecimal>("dhomHtSaisie");
	public static final ERXKey<java.math.BigDecimal> DHOM_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("dhomMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> DHOM_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("dhomTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> DHOM_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("dhomTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> CODE_EXER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>("codeExer");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOExercice>("exercice");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget> PRE_DEPENSE_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget>("preDepenseBudget");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat> TYPE_ACHAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat>("typeAchat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdhomId";

	public static final String DHOM_HT_SAISIE_KEY = "dhomHtSaisie";
	public static final String DHOM_MONTANT_BUDGETAIRE_KEY = "dhomMontantBudgetaire";
	public static final String DHOM_TTC_SAISIE_KEY = "dhomTtcSaisie";
	public static final String DHOM_TVA_SAISIE_KEY = "dhomTvaSaisie";

//Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PDEP_ID_KEY = "pdepId";
	public static final String PDHOM_ID_KEY = "pdhomId";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String DHOM_HT_SAISIE_COLKEY = "PDHOM_HT_SAISIE";
	public static final String DHOM_MONTANT_BUDGETAIRE_COLKEY = "PDHOM_MONTANT_BUDGETAIRE";
	public static final String DHOM_TTC_SAISIE_COLKEY = "PDHOM_TTC_SAISIE";
	public static final String DHOM_TVA_SAISIE_COLKEY = "PDHOM_TVA_SAISIE";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PDEP_ID_COLKEY = "PDEP_ID";
	public static final String PDHOM_ID_COLKEY = "PDHOM_ID";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PRE_DEPENSE_BUDGET_KEY = "preDepenseBudget";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
	public java.math.BigDecimal dhomHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DHOM_HT_SAISIE_KEY);
	}

	public void setDhomHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DHOM_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal dhomMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(DHOM_MONTANT_BUDGETAIRE_KEY);
	}

	public void setDhomMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DHOM_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal dhomTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DHOM_TTC_SAISIE_KEY);
	}

	public void setDhomTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DHOM_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal dhomTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(DHOM_TVA_SAISIE_KEY);
	}

	public void setDhomTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, DHOM_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
	}

	public void setCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeExer oldValue = codeExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOExercice exercice() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
	}

	public void setExerciceRelationship(org.cocktail.fwkcktldepense.server.metier.EOExercice value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOExercice oldValue = exercice();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget preDepenseBudget() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget)storedValueForKey(PRE_DEPENSE_BUDGET_KEY);
	}

	public void setPreDepenseBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget oldValue = preDepenseBudget();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRE_DEPENSE_BUDGET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PRE_DEPENSE_BUDGET_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
	}

	public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeAchat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeAchat oldValue = typeAchat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOPreDepenseControleHorsMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPreDepenseControleHorsMarche createEOPreDepenseControleHorsMarche(EOEditingContext editingContext				, java.math.BigDecimal dhomHtSaisie
							, java.math.BigDecimal dhomMontantBudgetaire
							, java.math.BigDecimal dhomTtcSaisie
							, java.math.BigDecimal dhomTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer		, org.cocktail.fwkcktldepense.server.metier.EOExercice exercice		, org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget preDepenseBudget		, org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat					) {
	 EOPreDepenseControleHorsMarche eo = (EOPreDepenseControleHorsMarche) EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseControleHorsMarche.ENTITY_NAME);	 
							eo.setDhomHtSaisie(dhomHtSaisie);
									eo.setDhomMontantBudgetaire(dhomMontantBudgetaire);
									eo.setDhomTtcSaisie(dhomTtcSaisie);
									eo.setDhomTvaSaisie(dhomTvaSaisie);
						 eo.setCodeExerRelationship(codeExer);
				 eo.setExerciceRelationship(exercice);
				 eo.setPreDepenseBudgetRelationship(preDepenseBudget);
				 eo.setTypeAchatRelationship(typeAchat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreDepenseControleHorsMarche creerInstance(EOEditingContext editingContext) {
		EOPreDepenseControleHorsMarche object = (EOPreDepenseControleHorsMarche)EOUtilities.createAndInsertInstance(editingContext, _EOPreDepenseControleHorsMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOPreDepenseControleHorsMarche localInstanceIn(EOEditingContext editingContext) {
    EOPreDepenseControleHorsMarche localInstance = (EOPreDepenseControleHorsMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreDepenseControleHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreDepenseControleHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreDepenseControleHorsMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreDepenseControleHorsMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreDepenseControleHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseControleHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseControleHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseControleHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
