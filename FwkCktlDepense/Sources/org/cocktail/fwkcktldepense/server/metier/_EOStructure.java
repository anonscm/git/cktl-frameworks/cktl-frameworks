/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOStructure.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOStructure extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleStructureUlr";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_STRUCTURE";


//Attribute Keys
	public static final ERXKey<String> C_TYPE_STRUCTURE = new ERXKey<String>("cTypeStructure");
	public static final ERXKey<String> LC_STRUCTURE = new ERXKey<String>("lcStructure");
	public static final ERXKey<String> LL_STRUCTURE = new ERXKey<String>("llStructure");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> REPART_STRUCTURE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>("repartStructure");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";

//Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String PERSONNE_KEY = "personne";
	public static final String REPART_STRUCTURE_KEY = "repartStructure";



	// Accessors methods
	public String cTypeStructure() {
	 return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
	}

	public void setCTypeStructure(String value) {
	 takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
	}

	public String lcStructure() {
	 return (String) storedValueForKey(LC_STRUCTURE_KEY);
	}

	public void setLcStructure(String value) {
	 takeStoredValueForKey(value, LC_STRUCTURE_KEY);
	}

	public String llStructure() {
	 return (String) storedValueForKey(LL_STRUCTURE_KEY);
	}

	public void setLlStructure(String value) {
	 takeStoredValueForKey(value, LL_STRUCTURE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktldepense.server.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructure() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)storedValueForKey(REPART_STRUCTURE_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructure(EOQualifier qualifier) {
	 return repartStructure(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructure(EOQualifier qualifier, boolean fetch) {
	 return repartStructure(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> repartStructure(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EORepartStructure.STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = repartStructure();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EORepartStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRepartStructureRelationship(org.cocktail.fwkcktldepense.server.metier.EORepartStructure object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURE_KEY);
	}
	
	public void removeFromRepartStructureRelationship(org.cocktail.fwkcktldepense.server.metier.EORepartStructure object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURE_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EORepartStructure createRepartStructureRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EORepartStructure.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REPART_STRUCTURE_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EORepartStructure) eo;
	}
	
	public void deleteRepartStructureRelationship(org.cocktail.fwkcktldepense.server.metier.EORepartStructure object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURE_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRepartStructureRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EORepartStructure> objects = repartStructure().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRepartStructureRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOStructure avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOStructure createEOStructure(EOEditingContext editingContext				, String cTypeStructure
									, org.cocktail.fwkcktldepense.server.metier.EOPersonne personne					) {
	 EOStructure eo = (EOStructure) EOUtilities.createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);	 
							eo.setCTypeStructure(cTypeStructure);
										 eo.setPersonneRelationship(personne);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext) {
		EOStructure object = (EOStructure)EOUtilities.createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);
  		return object;
		}

	

  public EOStructure localInstanceIn(EOEditingContext editingContext) {
    EOStructure localInstance = (EOStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOStructure fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructure fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOStructure> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOStructure> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOStructure fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructure eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructure ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructure fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> objectsForRecherche(EOEditingContext ec,
														String cTypeStructureBinding,
														String llStructureBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOStructure.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (cTypeStructureBinding != null)
			bindings.takeValueForKey(cTypeStructureBinding, "cTypeStructure");
		  if (llStructureBinding != null)
			bindings.takeValueForKey(llStructureBinding, "llStructure");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
