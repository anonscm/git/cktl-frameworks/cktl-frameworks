/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCommandeImpression.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCommandeImpression extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeImpression";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_IMPRESSION";


//Attribute Keys
	public static final ERXKey<NSTimestamp> CIMP_DATE = new ERXKey<NSTimestamp>("cimpDate");
	public static final ERXKey<NSTimestamp> CIMP_DATE_LIVRAISON = new ERXKey<NSTimestamp>("cimpDateLivraison");
	public static final ERXKey<String> CIMP_INFOS_IMPRESSION = new ERXKey<String>("cimpInfosImpression");
	public static final ERXKey<String> CIMP_LIVRAISON_FAX = new ERXKey<String>("cimpLivraisonFax");
	public static final ERXKey<String> CIMP_LIVRAISON_TELEPHONE = new ERXKey<String>("cimpLivraisonTelephone");
	public static final ERXKey<String> CIMP_SERVICE_FAX = new ERXKey<String>("cimpServiceFax");
	public static final ERXKey<String> CIMP_SERVICE_TELEPHONE = new ERXKey<String>("cimpServiceTelephone");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse> ADRESSE_FOURNISSEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse>("adresseFournisseur");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse> ADRESSE_LIVRAISON = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse>("adresseLivraison");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse> ADRESSE_SERVICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOAdresse>("adresseService");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande> COMMANDE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommande>("commande");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> FWK_CARAMBOLE_COMMANDE_DOCUMENTS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>("fwkCaramboleCommandeDocuments");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure> STRUCTURE_LIVRAISON = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure>("structureLivraison");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure> STRUCTURE_SERVICE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOStructure>("structureService");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur> UTILISATEUR = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOUtilisateur>("utilisateur");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cimpId";

	public static final String CIMP_DATE_KEY = "cimpDate";
	public static final String CIMP_DATE_LIVRAISON_KEY = "cimpDateLivraison";
	public static final String CIMP_INFOS_IMPRESSION_KEY = "cimpInfosImpression";
	public static final String CIMP_LIVRAISON_FAX_KEY = "cimpLivraisonFax";
	public static final String CIMP_LIVRAISON_TELEPHONE_KEY = "cimpLivraisonTelephone";
	public static final String CIMP_SERVICE_FAX_KEY = "cimpServiceFax";
	public static final String CIMP_SERVICE_TELEPHONE_KEY = "cimpServiceTelephone";

//Attributs non visibles
	public static final String CIMP_FOURNISSEUR_ADRORDRE_KEY = "cimpFournisseurAdrordre";
	public static final String CIMP_ID_KEY = "cimpId";
	public static final String CIMP_LIVRAISON_ADRORDRE_KEY = "cimpLivraisonAdrordre";
	public static final String CIMP_LIVRAISON_CSTRUCTURE_KEY = "cimpLivraisonCstructure";
	public static final String CIMP_SERVICE_ADRORDRE_KEY = "cimpServiceAdrordre";
	public static final String CIMP_SERVICE_CSTRUCTURE_KEY = "cimpServiceCstructure";
	public static final String COMM_ID_KEY = "commId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String CIMP_DATE_COLKEY = "CIMP_DATE";
	public static final String CIMP_DATE_LIVRAISON_COLKEY = "CIMP_DATE_LIVRAISON";
	public static final String CIMP_INFOS_IMPRESSION_COLKEY = "CIMP_INFOS_IMPRESSION";
	public static final String CIMP_LIVRAISON_FAX_COLKEY = "CIMP_LIVRAISON_FAX";
	public static final String CIMP_LIVRAISON_TELEPHONE_COLKEY = "CIMP_LIVRAISON_TELEPHONE";
	public static final String CIMP_SERVICE_FAX_COLKEY = "CIMP_SERVICE_FAX";
	public static final String CIMP_SERVICE_TELEPHONE_COLKEY = "CIMP_SERVICE_TELEPHONE";

	public static final String CIMP_FOURNISSEUR_ADRORDRE_COLKEY = "CIMP_FOURNISSEUR_ADRORDRE";
	public static final String CIMP_ID_COLKEY = "CIMP_ID";
	public static final String CIMP_LIVRAISON_ADRORDRE_COLKEY = "CIMP_LIVRAISON_ADRORDRE";
	public static final String CIMP_LIVRAISON_CSTRUCTURE_COLKEY = "CIMP_LIVRAISON_CSTRUCTURE";
	public static final String CIMP_SERVICE_ADRORDRE_COLKEY = "CIMP_SERVICE_ADRORDRE";
	public static final String CIMP_SERVICE_CSTRUCTURE_COLKEY = "CIMP_SERVICE_CSTRUCTURE";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ADRESSE_FOURNISSEUR_KEY = "adresseFournisseur";
	public static final String ADRESSE_LIVRAISON_KEY = "adresseLivraison";
	public static final String ADRESSE_SERVICE_KEY = "adresseService";
	public static final String COMMANDE_KEY = "commande";
	public static final String FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY = "fwkCaramboleCommandeDocuments";
	public static final String STRUCTURE_LIVRAISON_KEY = "structureLivraison";
	public static final String STRUCTURE_SERVICE_KEY = "structureService";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
	public NSTimestamp cimpDate() {
	 return (NSTimestamp) storedValueForKey(CIMP_DATE_KEY);
	}

	public void setCimpDate(NSTimestamp value) {
	 takeStoredValueForKey(value, CIMP_DATE_KEY);
	}

	public NSTimestamp cimpDateLivraison() {
	 return (NSTimestamp) storedValueForKey(CIMP_DATE_LIVRAISON_KEY);
	}

	public void setCimpDateLivraison(NSTimestamp value) {
	 takeStoredValueForKey(value, CIMP_DATE_LIVRAISON_KEY);
	}

	public String cimpInfosImpression() {
	 return (String) storedValueForKey(CIMP_INFOS_IMPRESSION_KEY);
	}

	public void setCimpInfosImpression(String value) {
	 takeStoredValueForKey(value, CIMP_INFOS_IMPRESSION_KEY);
	}

	public String cimpLivraisonFax() {
	 return (String) storedValueForKey(CIMP_LIVRAISON_FAX_KEY);
	}

	public void setCimpLivraisonFax(String value) {
	 takeStoredValueForKey(value, CIMP_LIVRAISON_FAX_KEY);
	}

	public String cimpLivraisonTelephone() {
	 return (String) storedValueForKey(CIMP_LIVRAISON_TELEPHONE_KEY);
	}

	public void setCimpLivraisonTelephone(String value) {
	 takeStoredValueForKey(value, CIMP_LIVRAISON_TELEPHONE_KEY);
	}

	public String cimpServiceFax() {
	 return (String) storedValueForKey(CIMP_SERVICE_FAX_KEY);
	}

	public void setCimpServiceFax(String value) {
	 takeStoredValueForKey(value, CIMP_SERVICE_FAX_KEY);
	}

	public String cimpServiceTelephone() {
	 return (String) storedValueForKey(CIMP_SERVICE_TELEPHONE_KEY);
	}

	public void setCimpServiceTelephone(String value) {
	 takeStoredValueForKey(value, CIMP_SERVICE_TELEPHONE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAdresse adresseFournisseur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAdresse)storedValueForKey(ADRESSE_FOURNISSEUR_KEY);
	}

	public void setAdresseFournisseurRelationship(org.cocktail.fwkcktldepense.server.metier.EOAdresse value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAdresse oldValue = adresseFournisseur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_FOURNISSEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_FOURNISSEUR_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAdresse adresseLivraison() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAdresse)storedValueForKey(ADRESSE_LIVRAISON_KEY);
	}

	public void setAdresseLivraisonRelationship(org.cocktail.fwkcktldepense.server.metier.EOAdresse value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAdresse oldValue = adresseLivraison();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_LIVRAISON_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_LIVRAISON_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOAdresse adresseService() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOAdresse)storedValueForKey(ADRESSE_SERVICE_KEY);
	}

	public void setAdresseServiceRelationship(org.cocktail.fwkcktldepense.server.metier.EOAdresse value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOAdresse oldValue = adresseService();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_SERVICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_SERVICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCommande commande() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
	}

	public void setCommandeRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommande value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCommande oldValue = commande();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOStructure structureLivraison() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOStructure)storedValueForKey(STRUCTURE_LIVRAISON_KEY);
	}

	public void setStructureLivraisonRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOStructure oldValue = structureLivraison();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_LIVRAISON_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_LIVRAISON_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOStructure structureService() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOStructure)storedValueForKey(STRUCTURE_SERVICE_KEY);
	}

	public void setStructureServiceRelationship(org.cocktail.fwkcktldepense.server.metier.EOStructure value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOStructure oldValue = structureService();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_SERVICE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_SERVICE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
	}

	public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.server.metier.EOUtilisateur value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOUtilisateur oldValue = utilisateur();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)storedValueForKey(FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments(EOQualifier qualifier) {
	 return fwkCaramboleCommandeDocuments(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments(EOQualifier qualifier, boolean fetch) {
	 return fwkCaramboleCommandeDocuments(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> fwkCaramboleCommandeDocuments(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.COMMANDE_IMPRESSION_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = fwkCaramboleCommandeDocuments();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToFwkCaramboleCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	}
	
	public void removeFromFwkCaramboleCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument createFwkCaramboleCommandeDocumentsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument) eo;
	}
	
	public void deleteFwkCaramboleCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_CARAMBOLE_COMMANDE_DOCUMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllFwkCaramboleCommandeDocumentsRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument> objects = fwkCaramboleCommandeDocuments().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteFwkCaramboleCommandeDocumentsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCommandeImpression avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCommandeImpression createEOCommandeImpression(EOEditingContext editingContext				, NSTimestamp cimpDate
																	, org.cocktail.fwkcktldepense.server.metier.EOAdresse adresseFournisseur						, org.cocktail.fwkcktldepense.server.metier.EOCommande commande						, org.cocktail.fwkcktldepense.server.metier.EOUtilisateur utilisateur					) {
	 EOCommandeImpression eo = (EOCommandeImpression) EOUtilities.createAndInsertInstance(editingContext, _EOCommandeImpression.ENTITY_NAME);	 
							eo.setCimpDate(cimpDate);
																		 eo.setAdresseFournisseurRelationship(adresseFournisseur);
								 eo.setCommandeRelationship(commande);
								 eo.setUtilisateurRelationship(utilisateur);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommandeImpression creerInstance(EOEditingContext editingContext) {
		EOCommandeImpression object = (EOCommandeImpression)EOUtilities.createAndInsertInstance(editingContext, _EOCommandeImpression.ENTITY_NAME);
  		return object;
		}

	

  public EOCommandeImpression localInstanceIn(EOEditingContext editingContext) {
    EOCommandeImpression localInstance = (EOCommandeImpression)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommandeImpression fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommandeImpression fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommandeImpression> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeImpression eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeImpression)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeImpression fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeImpression fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommandeImpression> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeImpression eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeImpression)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommandeImpression fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeImpression eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeImpression ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeImpression fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
