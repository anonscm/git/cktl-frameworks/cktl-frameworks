

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleActionException;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;



public class EOEngagementControleAction extends _EOEngagementControleAction
{
	private BigDecimal pourcentage;
	public static String EACT_POURCENTAGE="pourcentage";

    public EOEngagementControleAction() {
        super();
        setPourcentage(new BigDecimal(0.0));
    }

    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);

    	if (engagementBudget()!=null && engagementBudget().engTtcSaisie()!=null && engagementBudget().engTtcSaisie().floatValue()!=0.0)
    		setPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(engagementBudget().engTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }

    public void setPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);
    	pourcentage=aValue;

    	if (engagementBudget()!=null) {
    		NSArray engageControleActions = engagementBudget().engagementControleActions();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOEngagementControleAction.TYPE_ACTION_KEY+" != nil", null);
    		engageControleActions = EOQualifier.filteredArrayWithQualifier(engageControleActions, qual);
    		BigDecimal total=engagementBudget().computeSumForKey(engageControleActions, EOEngagementControleAction.EACT_POURCENTAGE);
    		if (total.floatValue()>100.0)
    	        pourcentage=new BigDecimal(100.0).subtract(total.subtract(aValue));    			
    	}

        if (engagementBudget()!=null)
        	engagementBudget().corrigerMontantActions();
    }

    public BigDecimal pourcentage() {
    	return pourcentage;
    }

    public void setEactHtSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEactHtSaisie(aValue);
    }
    
    public void setEactTvaSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEactTvaSaisie(aValue);
    }

    public void setEactTtcSaisie(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEactTtcSaisie(aValue);
    }

    public void setEactMontantBudgetaire(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEactMontantBudgetaire(aValue);
    }

    public void setEactMontantBudgetaireReste(BigDecimal aValue) {
    	int arrondi=EODevise.defaultNbDecimales;
    	if (engagementBudget()!=null)
    		arrondi=engagementBudget().decimalesPourArrondirMontant();
        aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setEactMontantBudgetaireReste(aValue);
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        if (eactHtSaisie()==null)
            throw new EngagementControleActionException(EngagementControleActionException.eactHtSaisieManquant);
        if (eactTvaSaisie()==null)
            throw new EngagementControleActionException(EngagementControleActionException.eactTvaSaisieManquant);
        if (eactTtcSaisie()==null)
            throw new EngagementControleActionException(EngagementControleActionException.eactTtcSaisieManquant);
        if (eactMontantBudgetaire()==null)
            throw new EngagementControleActionException(EngagementControleActionException.eactMontantBudgetaireManquant);
        if (eactMontantBudgetaireReste()==null)
            throw new EngagementControleActionException(EngagementControleActionException.eactMontantBudgetaireResteManquant);
        if (eactDateSaisie()==null)
            throw new EngagementControleActionException(EngagementControleActionException.eactDateSaisieManquant);
        if (typeAction()==null)
            throw new EngagementControleActionException(EngagementControleActionException.typeActionManquant);
        if (exercice()==null)
            throw new EngagementControleActionException(EngagementControleActionException.exerciceManquant);
        if (engagementBudget()==null)
            throw new EngagementControleActionException(EngagementControleActionException.engagementBudgetManquant);
        
        if (!eactHtSaisie().abs().add(eactTvaSaisie().abs()).equals(eactTtcSaisie().abs()))
        	setEactTvaSaisie(eactTtcSaisie().subtract(eactHtSaisie()));
//        if (!eactHtSaisie().abs().add(eactTvaSaisie().abs()).equals(eactTtcSaisie().abs()))
//        	throw new EngagementControleActionException(EngagementControleActionException.eactTtcSaisiePasCoherent);
//        if (!eactMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(eactHtSaisie(), eactTvaSaisie())))
//        	setEactMontantBudgetaire(engagementBudget().tauxProrata().montantBudgetaire(eactHtSaisie(), eactTvaSaisie()));        
//        if (!eactMontantBudgetaire().equals(engagementBudget().tauxProrata().montantBudgetaire(eactHtSaisie(), eactTvaSaisie())))
//        	throw new EngagementControleActionException(EngagementControleActionException.eactMontantBudgetairePasCoherent);

        if (eactTvaSaisie().floatValue()<0.0 || eactHtSaisie().floatValue()<0.0 || eactTtcSaisie().floatValue()<0.0 ||
        		eactMontantBudgetaire().floatValue()<0.0 || eactMontantBudgetaireReste().floatValue()<0.0)
        	throw new EngagementControleActionException(EngagementControleActionException.montantsNegatifs);

        if (!engagementBudget().exercice().equals(exercice()))
            throw new EngagementControleActionException(EngagementControleActionException.engageBudgetExercicePasCoherent);
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    }
}
