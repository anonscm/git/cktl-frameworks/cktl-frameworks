/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOImDgp.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOImDgp extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleImDgp";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.IM_DGP";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
	public static final ERXKey<NSTimestamp> IMDG_DEBUT = new ERXKey<NSTimestamp>("imdgDebut");
	public static final ERXKey<Integer> IMDG_DGP = new ERXKey<Integer>("imdgDgp");
	public static final ERXKey<NSTimestamp> IMDG_FIN = new ERXKey<NSTimestamp>("imdgFin");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "imdgId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String IMDG_DEBUT_KEY = "imdgDebut";
	public static final String IMDG_DGP_KEY = "imdgDgp";
	public static final String IMDG_FIN_KEY = "imdgFin";

//Attributs non visibles
	public static final String IMDG_ID_KEY = "imdgId";
	public static final String ULT_ORDRE_KEY = "ultOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String IMDG_DEBUT_COLKEY = "IMDG_DEBUT";
	public static final String IMDG_DGP_COLKEY = "IMDG_DGP";
	public static final String IMDG_FIN_COLKEY = "IMDG_FIN";

	public static final String IMDG_ID_COLKEY = "IMDG_ID";
	public static final String ULT_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships



	// Accessors methods
	public NSTimestamp dateCreation() {
	 return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_CREATION_KEY);
	}

	public NSTimestamp dateModification() {
	 return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
	}

	public void setDateModification(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
	}

	public NSTimestamp imdgDebut() {
	 return (NSTimestamp) storedValueForKey(IMDG_DEBUT_KEY);
	}

	public void setImdgDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, IMDG_DEBUT_KEY);
	}

	public Integer imdgDgp() {
	 return (Integer) storedValueForKey(IMDG_DGP_KEY);
	}

	public void setImdgDgp(Integer value) {
	 takeStoredValueForKey(value, IMDG_DGP_KEY);
	}

	public NSTimestamp imdgFin() {
	 return (NSTimestamp) storedValueForKey(IMDG_FIN_KEY);
	}

	public void setImdgFin(NSTimestamp value) {
	 takeStoredValueForKey(value, IMDG_FIN_KEY);
	}


	/**
	* Créer une instance de EOImDgp avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOImDgp createEOImDgp(EOEditingContext editingContext				, NSTimestamp dateCreation
							, NSTimestamp dateModification
							, NSTimestamp imdgDebut
							, Integer imdgDgp
										) {
	 EOImDgp eo = (EOImDgp) EOUtilities.createAndInsertInstance(editingContext, _EOImDgp.ENTITY_NAME);	 
							eo.setDateCreation(dateCreation);
									eo.setDateModification(dateModification);
									eo.setImdgDebut(imdgDebut);
									eo.setImdgDgp(imdgDgp);
							 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImDgp creerInstance(EOEditingContext editingContext) {
		EOImDgp object = (EOImDgp)EOUtilities.createAndInsertInstance(editingContext, _EOImDgp.ENTITY_NAME);
  		return object;
		}

	

  public EOImDgp localInstanceIn(EOEditingContext editingContext) {
    EOImDgp localInstance = (EOImDgp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOImDgp>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOImDgp fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOImDgp fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOImDgp> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOImDgp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOImDgp)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOImDgp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOImDgp fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOImDgp> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOImDgp eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOImDgp)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOImDgp fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOImDgp eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOImDgp ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOImDgp fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
