/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOReimputationHorsMarche.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOReimputationHorsMarche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_HORS_MARCHE";


//Attribute Keys
	public static final ERXKey<java.math.BigDecimal> REHM_HT_SAISIE = new ERXKey<java.math.BigDecimal>("rehmHtSaisie");
	public static final ERXKey<java.math.BigDecimal> REHM_MONTANT_BUDGETAIRE = new ERXKey<java.math.BigDecimal>("rehmMontantBudgetaire");
	public static final ERXKey<java.math.BigDecimal> REHM_TTC_SAISIE = new ERXKey<java.math.BigDecimal>("rehmTtcSaisie");
	public static final ERXKey<java.math.BigDecimal> REHM_TVA_SAISIE = new ERXKey<java.math.BigDecimal>("rehmTvaSaisie");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer> CODE_EXER = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeExer>("codeExer");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation> REIMPUTATION = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOReimputation>("reimputation");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat> TYPE_ACHAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeAchat>("typeAchat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rehmId";

	public static final String REHM_HT_SAISIE_KEY = "rehmHtSaisie";
	public static final String REHM_MONTANT_BUDGETAIRE_KEY = "rehmMontantBudgetaire";
	public static final String REHM_TTC_SAISIE_KEY = "rehmTtcSaisie";
	public static final String REHM_TVA_SAISIE_KEY = "rehmTvaSaisie";

//Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String REHM_ID_KEY = "rehmId";
	public static final String REIM_ID_KEY = "reimId";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String REHM_HT_SAISIE_COLKEY = "REHM_HT_SAISIE";
	public static final String REHM_MONTANT_BUDGETAIRE_COLKEY = "REHM_MONTANT_BUDGETAIRE";
	public static final String REHM_TTC_SAISIE_COLKEY = "REHM_TTC_SAISIE";
	public static final String REHM_TVA_SAISIE_COLKEY = "REHM_TVA_SAISIE";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String REHM_ID_COLKEY = "REHM_ID";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String REIMPUTATION_KEY = "reimputation";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
	public java.math.BigDecimal rehmHtSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REHM_HT_SAISIE_KEY);
	}

	public void setRehmHtSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REHM_HT_SAISIE_KEY);
	}

	public java.math.BigDecimal rehmMontantBudgetaire() {
	 return (java.math.BigDecimal) storedValueForKey(REHM_MONTANT_BUDGETAIRE_KEY);
	}

	public void setRehmMontantBudgetaire(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REHM_MONTANT_BUDGETAIRE_KEY);
	}

	public java.math.BigDecimal rehmTtcSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REHM_TTC_SAISIE_KEY);
	}

	public void setRehmTtcSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REHM_TTC_SAISIE_KEY);
	}

	public java.math.BigDecimal rehmTvaSaisie() {
	 return (java.math.BigDecimal) storedValueForKey(REHM_TVA_SAISIE_KEY);
	}

	public void setRehmTvaSaisie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, REHM_TVA_SAISIE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
	}

	public void setCodeExerRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeExer value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeExer oldValue = codeExer();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
	}

	public void setReimputationRelationship(org.cocktail.fwkcktldepense.server.metier.EOReimputation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOReimputation oldValue = reimputation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
	}

	public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeAchat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeAchat oldValue = typeAchat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
	 }
	}


	/**
	* Créer une instance de EOReimputationHorsMarche avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOReimputationHorsMarche createEOReimputationHorsMarche(EOEditingContext editingContext				, java.math.BigDecimal rehmHtSaisie
							, java.math.BigDecimal rehmMontantBudgetaire
							, java.math.BigDecimal rehmTtcSaisie
							, java.math.BigDecimal rehmTvaSaisie
					, org.cocktail.fwkcktldepense.server.metier.EOCodeExer codeExer		, org.cocktail.fwkcktldepense.server.metier.EOReimputation reimputation		, org.cocktail.fwkcktldepense.server.metier.EOTypeAchat typeAchat					) {
	 EOReimputationHorsMarche eo = (EOReimputationHorsMarche) EOUtilities.createAndInsertInstance(editingContext, _EOReimputationHorsMarche.ENTITY_NAME);	 
							eo.setRehmHtSaisie(rehmHtSaisie);
									eo.setRehmMontantBudgetaire(rehmMontantBudgetaire);
									eo.setRehmTtcSaisie(rehmTtcSaisie);
									eo.setRehmTvaSaisie(rehmTvaSaisie);
						 eo.setCodeExerRelationship(codeExer);
				 eo.setReimputationRelationship(reimputation);
				 eo.setTypeAchatRelationship(typeAchat);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputationHorsMarche creerInstance(EOEditingContext editingContext) {
		EOReimputationHorsMarche object = (EOReimputationHorsMarche)EOUtilities.createAndInsertInstance(editingContext, _EOReimputationHorsMarche.ENTITY_NAME);
  		return object;
		}

	

  public EOReimputationHorsMarche localInstanceIn(EOEditingContext editingContext) {
    EOReimputationHorsMarche localInstance = (EOReimputationHorsMarche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputationHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputationHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReimputationHorsMarche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReimputationHorsMarche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputationHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
