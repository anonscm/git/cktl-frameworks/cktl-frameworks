/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCodeAnalytique.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOCodeAnalytique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCodeAnalytique";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CODE_ANALYTIQUE";


//Attribute Keys
	public static final ERXKey<String> CAN_CODE = new ERXKey<String>("canCode");
	public static final ERXKey<NSTimestamp> CAN_FERMETURE = new ERXKey<NSTimestamp>("canFermeture");
	public static final ERXKey<String> CAN_LIBELLE = new ERXKey<String>("canLibelle");
	public static final ERXKey<java.math.BigDecimal> CAN_MONTANT = new ERXKey<java.math.BigDecimal>("canMontant");
	public static final ERXKey<NSTimestamp> CAN_OUVERTURE = new ERXKey<NSTimestamp>("canOuverture");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> CODE_ANALYTIQUE_EXERCICES = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice>("codeAnalytiqueExercices");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> CODE_ANALYTIQUE_ORGANS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan>("codeAnalytiqueOrgans");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> CODE_ANALYTIQUE_PERE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique>("codeAnalytiquePere");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueSuivi> CODE_ANALYTIQUE_SUIVI = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueSuivi>("codeAnalytiqueSuivi");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtat");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_DEPASSEMENT = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatDepassement");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_PUBLIC = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatPublic");
	public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat> TYPE_ETAT_UTILISABLE = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOTypeEtat>("typeEtatUtilisable");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "canId";

	public static final String CAN_CODE_KEY = "canCode";
	public static final String CAN_FERMETURE_KEY = "canFermeture";
	public static final String CAN_LIBELLE_KEY = "canLibelle";
	public static final String CAN_MONTANT_KEY = "canMontant";
	public static final String CAN_OUVERTURE_KEY = "canOuverture";

//Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CAN_ID_PERE_KEY = "canIdPere";
	public static final String CAN_MONTANT_DEPASSEMENT_KEY = "canMontantDepassement";
	public static final String CAN_PUBLIC_KEY = "canPublic";
	public static final String CAN_UTILISABLE_KEY = "canUtilisable";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CAN_CODE_COLKEY = "CAN_CODE";
	public static final String CAN_FERMETURE_COLKEY = "CAN_FERMETURE";
	public static final String CAN_LIBELLE_COLKEY = "CAN_LIBELLE";
	public static final String CAN_MONTANT_COLKEY = "CAN_MONTANT";
	public static final String CAN_OUVERTURE_COLKEY = "CAN_OUVERTURE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CAN_ID_PERE_COLKEY = "CAN_ID_PERE";
	public static final String CAN_MONTANT_DEPASSEMENT_COLKEY = "CAN_MONTANT_DEPASSEMENT";
	public static final String CAN_PUBLIC_COLKEY = "CAN_PUBLIC";
	public static final String CAN_UTILISABLE_COLKEY = "CAN_UTILISABLE";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String CODE_ANALYTIQUE_EXERCICES_KEY = "codeAnalytiqueExercices";
	public static final String CODE_ANALYTIQUE_ORGANS_KEY = "codeAnalytiqueOrgans";
	public static final String CODE_ANALYTIQUE_PERE_KEY = "codeAnalytiquePere";
	public static final String CODE_ANALYTIQUE_SUIVI_KEY = "codeAnalytiqueSuivi";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_DEPASSEMENT_KEY = "typeEtatDepassement";
	public static final String TYPE_ETAT_PUBLIC_KEY = "typeEtatPublic";
	public static final String TYPE_ETAT_UTILISABLE_KEY = "typeEtatUtilisable";



	// Accessors methods
	public String canCode() {
	 return (String) storedValueForKey(CAN_CODE_KEY);
	}

	public void setCanCode(String value) {
	 takeStoredValueForKey(value, CAN_CODE_KEY);
	}

	public NSTimestamp canFermeture() {
	 return (NSTimestamp) storedValueForKey(CAN_FERMETURE_KEY);
	}

	public void setCanFermeture(NSTimestamp value) {
	 takeStoredValueForKey(value, CAN_FERMETURE_KEY);
	}

	public String canLibelle() {
	 return (String) storedValueForKey(CAN_LIBELLE_KEY);
	}

	public void setCanLibelle(String value) {
	 takeStoredValueForKey(value, CAN_LIBELLE_KEY);
	}

	public java.math.BigDecimal canMontant() {
	 return (java.math.BigDecimal) storedValueForKey(CAN_MONTANT_KEY);
	}

	public void setCanMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, CAN_MONTANT_KEY);
	}

	public NSTimestamp canOuverture() {
	 return (NSTimestamp) storedValueForKey(CAN_OUVERTURE_KEY);
	}

	public void setCanOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, CAN_OUVERTURE_KEY);
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique codeAnalytiquePere() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_PERE_KEY);
	}

	public void setCodeAnalytiquePereRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique oldValue = codeAnalytiquePere();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_PERE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_PERE_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueSuivi codeAnalytiqueSuivi() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueSuivi)storedValueForKey(CODE_ANALYTIQUE_SUIVI_KEY);
	}

	public void setCodeAnalytiqueSuiviRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueSuivi value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueSuivi oldValue = codeAnalytiqueSuivi();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_SUIVI_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_SUIVI_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
	}

	public void setTypeEtatRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtat();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatDepassement() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_DEPASSEMENT_KEY);
	}

	public void setTypeEtatDepassementRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatDepassement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_DEPASSEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_DEPASSEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatPublic() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_PUBLIC_KEY);
	}

	public void setTypeEtatPublicRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatPublic();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_PUBLIC_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_PUBLIC_KEY);
	 }
	}

	public org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatUtilisable() {
	 return (org.cocktail.fwkcktldepense.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_UTILISABLE_KEY);
	}

	public void setTypeEtatUtilisableRelationship(org.cocktail.fwkcktldepense.server.metier.EOTypeEtat value) {
	 if (value == null) {
	 	org.cocktail.fwkcktldepense.server.metier.EOTypeEtat oldValue = typeEtatUtilisable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_UTILISABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_UTILISABLE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> codeAnalytiqueExercices() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice>)storedValueForKey(CODE_ANALYTIQUE_EXERCICES_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> codeAnalytiqueExercices(EOQualifier qualifier) {
	 return codeAnalytiqueExercices(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> codeAnalytiqueExercices(EOQualifier qualifier, boolean fetch) {
	 return codeAnalytiqueExercices(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> codeAnalytiqueExercices(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice.CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = codeAnalytiqueExercices();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCodeAnalytiqueExercicesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_EXERCICES_KEY);
	}
	
	public void removeFromCodeAnalytiqueExercicesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_EXERCICES_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice createCodeAnalytiqueExercicesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_EXERCICES_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice) eo;
	}
	
	public void deleteCodeAnalytiqueExercicesRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_EXERCICES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCodeAnalytiqueExercicesRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueExercice> objects = codeAnalytiqueExercices().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCodeAnalytiqueExercicesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> codeAnalytiqueOrgans() {
	 return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan>)storedValueForKey(CODE_ANALYTIQUE_ORGANS_KEY);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> codeAnalytiqueOrgans(EOQualifier qualifier) {
	 return codeAnalytiqueOrgans(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> codeAnalytiqueOrgans(EOQualifier qualifier, boolean fetch) {
	 return codeAnalytiqueOrgans(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> codeAnalytiqueOrgans(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan.CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = codeAnalytiqueOrgans();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
	}
	
	public void removeFromCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
	}
	
	public org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan createCodeAnalytiqueOrgansRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_ORGANS_KEY);
	 return (org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan) eo;
	}
	
	public void deleteCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllCodeAnalytiqueOrgansRelationships() {
	 Enumeration<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytiqueOrgan> objects = codeAnalytiqueOrgans().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteCodeAnalytiqueOrgansRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCodeAnalytique avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCodeAnalytique createEOCodeAnalytique(EOEditingContext editingContext				, String canCode
									, String canLibelle
									, NSTimestamp canOuverture
									, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtat		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatDepassement		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatPublic		, org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatUtilisable					) {
	 EOCodeAnalytique eo = (EOCodeAnalytique) EOUtilities.createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);	 
							eo.setCanCode(canCode);
											eo.setCanLibelle(canLibelle);
											eo.setCanOuverture(canOuverture);
										 eo.setTypeEtatRelationship(typeEtat);
				 eo.setTypeEtatDepassementRelationship(typeEtatDepassement);
				 eo.setTypeEtatPublicRelationship(typeEtatPublic);
				 eo.setTypeEtatUtilisableRelationship(typeEtatUtilisable);
			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnalytique creerInstance(EOEditingContext editingContext) {
		EOCodeAnalytique object = (EOCodeAnalytique)EOUtilities.createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);
  		return object;
		}

	

  public EOCodeAnalytique localInstanceIn(EOEditingContext editingContext) {
    EOCodeAnalytique localInstance = (EOCodeAnalytique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCodeAnalytique> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCodeAnalytique> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique> objectsForRecherche(EOEditingContext ec,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan crBinding,
														org.cocktail.fwkcktldepense.server.metier.EOExercice exerciceBinding,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan organBinding,
														org.cocktail.fwkcktldepense.server.metier.EOOrgan sscrBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatPublicBinding,
														org.cocktail.fwkcktldepense.server.metier.EOTypeEtat typeEtatUtilisableBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCodeAnalytique.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (crBinding != null)
			bindings.takeValueForKey(crBinding, "cr");
		  if (exerciceBinding != null)
			bindings.takeValueForKey(exerciceBinding, "exercice");
		  if (organBinding != null)
			bindings.takeValueForKey(organBinding, "organ");
		  if (sscrBinding != null)
			bindings.takeValueForKey(sscrBinding, "sscr");
		  if (typeEtatBinding != null)
			bindings.takeValueForKey(typeEtatBinding, "typeEtat");
		  if (typeEtatPublicBinding != null)
			bindings.takeValueForKey(typeEtatPublicBinding, "typeEtatPublic");
		  if (typeEtatUtilisableBinding != null)
			bindings.takeValueForKey(typeEtatUtilisableBinding, "typeEtatUtilisable");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
