

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
package org.cocktail.fwkcktldepense.server.metier;


import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCommandeControlePlanComptable extends _EOCommandeControlePlanComptable
{
	private static Calculs serviceCalculs = Calculs.getInstance();
	
    public EOCommandeControlePlanComptable() {
        super();
    }

    public BigDecimal montantTtc() {
    	return cpcoTtcSaisie();
    }
    
    public void setMontantTtc(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
    	
    	if (commandeBudget()!=null && commandeBudget().cbudTtcSaisie()!=null && commandeBudget().cbudTtcSaisie().floatValue()!=0.0)
    		setCpcoPourcentage(aValue.multiply(new BigDecimal(100.0)).setScale(5).
        			divide(commandeBudget().cbudTtcSaisie().setScale(5), BigDecimal.ROUND_HALF_UP));
    }
    
    public void setCpcoHtSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCpcoHtSaisie(aValue);
    }

    public void setCpcoTvaSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCpcoTvaSaisie(aValue);
    }

    public void setCpcoTtcSaisie(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCpcoTtcSaisie(aValue);
    }

    public void setCpcoMontantBudgetaire(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	int arrondi=EODevise.defaultNbDecimales;
    	if (commandeBudget()!=null && commandeBudget().commande()!=null)
    		arrondi=commandeBudget().commande().decimalesPourArrondirMontant();
    	aValue=aValue.setScale(arrondi, BigDecimal.ROUND_HALF_UP);
        super.setCpcoMontantBudgetaire(aValue);
    }

    public void setCpcoPourcentage(BigDecimal aValue) {
		if (aValue==null || aValue.floatValue()<0)
    		aValue=new BigDecimal(0.0);
    	aValue=aValue.setScale(5, BigDecimal.ROUND_HALF_UP);
    	if (aValue.floatValue()>100.0)
    		aValue=new BigDecimal(100.0);

        super.setCpcoPourcentage(aValue);

    	if (commandeBudget()!=null) {
       		NSArray commandeControlePlanComptables = commandeBudget().commandeControlePlanComptables();
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommandeControlePlanComptable.PLAN_COMPTABLE_KEY+" != nil", null);
    		commandeControlePlanComptables = EOQualifier.filteredArrayWithQualifier(commandeControlePlanComptables, qual);
    		BigDecimal total=commandeBudget().computeSumForKey(commandeControlePlanComptables, EOCommandeControlePlanComptable.CPCO_POURCENTAGE_KEY);
    		if (total.floatValue()>100.0)
    	        super.setCpcoPourcentage(new BigDecimal(100.0).subtract(total.subtract(aValue)));    			
    	}
    	
        if (commandeBudget()!=null)
        	commandeBudget().corrigerMontantPlanComptables();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele e partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws CommandeControlePlanComptableException {
        if (cpcoHtSaisie()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.cpcoHtSaisieManquant);
        if (cpcoTvaSaisie()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.cpcoTvaSaisieManquant);
        if (cpcoTtcSaisie()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.cpcoTtcSaisieManquant);
        if (cpcoMontantBudgetaire()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.cpcoMontantBudgetaireManquant);
        if (cpcoPourcentage()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.cpcoPourcentageManquant);
        if (exercice()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.exerciceManquant);
        if (commandeBudget()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.commandeBudgetManquant);
        
        if (!cpcoHtSaisie().abs().add(cpcoTvaSaisie().abs()).equals(cpcoTtcSaisie().abs()))
        	setCpcoTvaSaisie(cpcoTtcSaisie().subtract(cpcoHtSaisie()));
        if (commandeBudget().tauxProrata()!=null && !cpcoMontantBudgetaire().equals(calculMontantBudgetaire()))
        	setCpcoMontantBudgetaire(calculMontantBudgetaire());

        if (cpcoTvaSaisie().floatValue()<0.0 || cpcoHtSaisie().floatValue()<0.0 || cpcoTtcSaisie().floatValue()<0.0 ||
        		cpcoMontantBudgetaire().floatValue()<0.0)
        	throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.montantsNegatifs);

        if (!commandeBudget().exercice().equals(exercice()))
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.commandeBudgetExercicePasCoherent);
    }

	private BigDecimal calculMontantBudgetaire() {
		return serviceCalculs.calculMontantBudgetaire(cpcoHtSaisie(), cpcoTvaSaisie(), commandeBudget().tauxProrata().tapTaux());
	}
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        if (planComptable()==null)
            throw new CommandeControlePlanComptableException(CommandeControlePlanComptableException.planComptableManquant);
    }
}
