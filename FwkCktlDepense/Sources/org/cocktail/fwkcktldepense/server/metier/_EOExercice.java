/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOExercice.java instead.
package org.cocktail.fwkcktldepense.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class _EOExercice extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleExercice";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_EXERCICE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> EXE_CLOTURE = new ERXKey<NSTimestamp>("exeCloture");
	public static final ERXKey<Integer> EXE_EXERCICE = new ERXKey<Integer>("exeExercice");
	public static final ERXKey<NSTimestamp> EXE_INVENTAIRE = new ERXKey<NSTimestamp>("exeInventaire");
	public static final ERXKey<NSTimestamp> EXE_OUVERTURE = new ERXKey<NSTimestamp>("exeOuverture");
	public static final ERXKey<String> EXE_STAT = new ERXKey<String>("exeStat");
	public static final ERXKey<String> EXE_STAT_ENG = new ERXKey<String>("exeStatEng");
	public static final ERXKey<String> EXE_STAT_LIQ = new ERXKey<String>("exeStatLiq");
	public static final ERXKey<String> EXE_TYPE = new ERXKey<String>("exeType");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "exeOrdre";

	public static final String EXE_CLOTURE_KEY = "exeCloture";
	public static final String EXE_EXERCICE_KEY = "exeExercice";
	public static final String EXE_INVENTAIRE_KEY = "exeInventaire";
	public static final String EXE_OUVERTURE_KEY = "exeOuverture";
	public static final String EXE_STAT_KEY = "exeStat";
	public static final String EXE_STAT_ENG_KEY = "exeStatEng";
	public static final String EXE_STAT_LIQ_KEY = "exeStatLiq";
	public static final String EXE_TYPE_KEY = "exeType";

//Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String EXE_CLOTURE_COLKEY = "EXE_CLOTURE";
	public static final String EXE_EXERCICE_COLKEY = "EXE_EXERCICE";
	public static final String EXE_INVENTAIRE_COLKEY = "EXE_INVENTAIRE";
	public static final String EXE_OUVERTURE_COLKEY = "EXE_OUVERTURE";
	public static final String EXE_STAT_COLKEY = "EXE_STAT";
	public static final String EXE_STAT_ENG_COLKEY = "EXE_STAT_ENG";
	public static final String EXE_STAT_LIQ_COLKEY = "EXE_STAT_LIQ";
	public static final String EXE_TYPE_COLKEY = "EXE_TYPE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships



	// Accessors methods
	public NSTimestamp exeCloture() {
	 return (NSTimestamp) storedValueForKey(EXE_CLOTURE_KEY);
	}

	public void setExeCloture(NSTimestamp value) {
	 takeStoredValueForKey(value, EXE_CLOTURE_KEY);
	}

	public Integer exeExercice() {
	 return (Integer) storedValueForKey(EXE_EXERCICE_KEY);
	}

	public void setExeExercice(Integer value) {
	 takeStoredValueForKey(value, EXE_EXERCICE_KEY);
	}

	public NSTimestamp exeInventaire() {
	 return (NSTimestamp) storedValueForKey(EXE_INVENTAIRE_KEY);
	}

	public void setExeInventaire(NSTimestamp value) {
	 takeStoredValueForKey(value, EXE_INVENTAIRE_KEY);
	}

	public NSTimestamp exeOuverture() {
	 return (NSTimestamp) storedValueForKey(EXE_OUVERTURE_KEY);
	}

	public void setExeOuverture(NSTimestamp value) {
	 takeStoredValueForKey(value, EXE_OUVERTURE_KEY);
	}

	public String exeStat() {
	 return (String) storedValueForKey(EXE_STAT_KEY);
	}

	public void setExeStat(String value) {
	 takeStoredValueForKey(value, EXE_STAT_KEY);
	}

	public String exeStatEng() {
	 return (String) storedValueForKey(EXE_STAT_ENG_KEY);
	}

	public void setExeStatEng(String value) {
	 takeStoredValueForKey(value, EXE_STAT_ENG_KEY);
	}

	public String exeStatLiq() {
	 return (String) storedValueForKey(EXE_STAT_LIQ_KEY);
	}

	public void setExeStatLiq(String value) {
	 takeStoredValueForKey(value, EXE_STAT_LIQ_KEY);
	}

	public String exeType() {
	 return (String) storedValueForKey(EXE_TYPE_KEY);
	}

	public void setExeType(String value) {
	 takeStoredValueForKey(value, EXE_TYPE_KEY);
	}


	/**
	* Créer une instance de EOExercice avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOExercice createEOExercice(EOEditingContext editingContext						, Integer exeExercice
											, String exeStat
							, String exeStatEng
							, String exeStatLiq
							, String exeType
								) {
	 EOExercice eo = (EOExercice) EOUtilities.createAndInsertInstance(editingContext, _EOExercice.ENTITY_NAME);	 
									eo.setExeExercice(exeExercice);
													eo.setExeStat(exeStat);
									eo.setExeStatEng(exeStatEng);
									eo.setExeStatLiq(exeStatLiq);
									eo.setExeType(exeType);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOExercice creerInstance(EOEditingContext editingContext) {
		EOExercice object = (EOExercice)EOUtilities.createAndInsertInstance(editingContext, _EOExercice.ENTITY_NAME);
  		return object;
		}

	

  public EOExercice localInstanceIn(EOEditingContext editingContext) {
    EOExercice localInstance = (EOExercice)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
	    @SuppressWarnings("unchecked")
	    NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> eoObjects = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOExercice fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOExercice fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOExercice> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOExercice)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOExercice> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOExercice)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOExercice fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOExercice eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOExercice ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOExercice fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOExercice> objectsForRecherche(EOEditingContext ec,
														NSTimestamp dateBinding) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOExercice.ENTITY_NAME);
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		  if (dateBinding != null)
			bindings.takeValueForKey(dateBinding, "date");
				spec = spec.fetchSpecificationWithQualifierBindings(bindings);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
