/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.metier;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktldepense.server.exception.DepensePapierException;
import org.cocktail.fwkcktldepense.server.finder.FinderDepensePapier;
import org.cocktail.fwkcktldepense.server.finder.FinderImDgp;
import org.cocktail.fwkcktldepense.server.finder.FinderImTaux;
import org.cocktail.fwkcktldepense.server.finder.FinderImTypeTaux;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeEtat;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;
import org.cocktail.fwkcktldepense.server.util.Calculs;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EODepensePapier extends _EODepensePapier {

	private abstract class Controle {
		abstract public boolean isOk(_IDepenseBudget element);
	}
	private class ControleAction extends Controle {
		@Override
		public boolean isOk(_IDepenseBudget element) {
			return element.isControleActionGood();
		};
	}
	private class ControleMarche extends Controle {
		@Override
		public boolean isOk(_IDepenseBudget element) {
			return element.isControleMarcheGood();
		};
	}
	private class ControleHorsMarche extends Controle {
		@Override
		public boolean isOk(_IDepenseBudget element) {
			return element.isControleHorsMarcheGood();
		};
	}
	private class ControleConvention extends Controle {
		@Override
		public boolean isOk(_IDepenseBudget element) {
			return element.isControleConventionGood();
		};
	}
	private class ControlePlanComptable extends Controle {
		@Override
		public boolean isOk(_IDepenseBudget element) {
			return element.isControlePlanComptableGood();
		};
	}
	private class ControleAnalytique extends Controle {
		@Override
		public boolean isOk(_IDepenseBudget element) {
			return element.isControleAnalytiqueGood();
		};
	}

	private final Controle controleMarche = new ControleMarche(); 
	private final Controle controleHorsMarche = new ControleHorsMarche(); 
	private final Controle controleAction = new ControleAction(); 
	private final Controle controleConvention = new ControleConvention(); 
	private final Controle controlePlanComptable = new ControlePlanComptable(); 
	private final Controle controleAnalytique = new ControleAnalytique(); 
	
	public static enum ELiquidationType {
		SUR_COMMANDE, SANS_ENGAGEMENT, SUR_EXTOURNE, SUR_EXTOURNE_ENVELOPPE
	};

	public static final String ETAT_PARTIELLEMENT_SOLDEE = "PART_SOLDEE";
	public static final String ETAT_SOLDEE = "SOLDEE";

	private EOCommande commande;
	private Number dppIdProc;
	private NSArray<? extends _IDepenseBudget> arrayDepenseBudgets;
	private ELiquidationType liquidationType;

	public EODepensePapier() {
		super();
		dppIdProc = null;
		commande = null;
		arrayDepenseBudgets = null;
	}

	public EOTypeEtat etatEnregistrement() {
		if (commande == null)
			return null;

		BigDecimal montantDeps = montantBudgetaire();

		BigDecimal montantEngs = new BigDecimal(0.0);
		for (int i = 0; i < commande.commandeEngagements().count(); i++) {
			EOEngagementBudget engagementBudget = ((EOCommandeEngagement) commande.commandeEngagements().objectAtIndex(i)).engagementBudget();
			montantEngs = montantEngs.add(engagementBudget.engMontantBudgetaireReste());
		}

		if (montantDeps.floatValue() >= montantEngs.floatValue())
			return FinderTypeEtat.getTypeEtat(editingContext(), ETAT_SOLDEE);

		return FinderTypeEtat.getTypeEtat(editingContext(), ETAT_PARTIELLEMENT_SOLDEE);
	}

	public void definirInfosImParDefaut() {
		definirImTypeTauxParDefaut();
		definirImTauxParDefaut();
		definirImDgpParDefaut();
	}

	public void definirImTypeTauxParDefaut() {
		if (exercice() == null || editingContext() == null)
			return;
		setImTypeTauxRelationship(FinderImTypeTaux.getTypeTauxParDefaut(editingContext(), exercice()));
	}

	public void definirImTauxParDefaut() {
		if (dppDateServiceFait() == null || imTypeTaux() == null || editingContext() == null)
			return;
		EOImTaux taux = FinderImTaux.getTaux(editingContext(), imTypeTaux(), dppDateServiceFait());
		if (taux == null)
			setDppImTaux(null);
		else
			setDppImTaux(taux.imtaTaux());
	}

	public void definirImDgpParDefaut() {
		if (dppDateServiceFait() == null || editingContext() == null)
			return;
		EOImDgp dgp = FinderImDgp.getTaux(editingContext(), dppDateServiceFait());
		if (dgp == null)
			setDppImDgp(null);
		else
			setDppImDgp(dgp.imdgDgp());
	}

	public boolean isMemeNumeroFacture() {
		if (commande == null || dppNumeroFacture() == null)
			return false;

		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(commande, "commande");
		dico.setObjectForKey(dppNumeroFacture(), "dppNumeroFacture");

		if (FinderDepensePapier.getDepensePapierPourNumeroFacture(editingContext(), dico).count() == 0)
			return false;
		return true;
	}

	public BigDecimal montantBudgetaire() {
		//	return computeSumForKey(depenseBudgets(), EODepenseBudget.DEP_MONTANT_BUDGETAIRE_KEY);
		return computeSumForKey(currentDepenseBudgets(), _IDepenseBudget.DEP_MONTANT_BUDGETAIRE_KEY);
	}

	public boolean isReversement() {
		if (depensePapierReversement() == null)
			return false;
		return true;
	}

	public boolean isComplete() {
		if (isReversement() == false) {
			if (dppNumeroFacture() == null || dppNumeroFacture().equals("") == true)
				return false;
			if (dppDateFacture() == null || dppDateReception() == null || dppDateServiceFait() == null)
				return false;
			if (dppNbPiece() == null)
				return false;
			if (dppHtInitial() == null || dppTtcInitial() == null)
				return false;
			if (dppTtcInitial().floatValue() < dppHtInitial().floatValue())
				return false;
			if (dppHtInitial().floatValue() < 0.0)
				return false;
			if (dppTtcInitial().floatValue() <= 0.0)
				return false;

			if (exercice() == null || fournisseur() == null || modePaiement() == null)
				return false;

			if (modePaiement().isRibObligatoire() && ribFournisseur() == null)
				return false;
		}
		else {
			if (dppNumeroFacture() == null || dppNumeroFacture().equals("") == true)
				return false;
			if (dppNbPiece() == null)
				return false;
			if (dppHtInitial() == null || dppTtcInitial() == null)
				return false;
			if (dppTtcInitial().abs().floatValue() < dppHtInitial().abs().floatValue())
				return false;
			if (dppHtInitial().floatValue() > 0.0)
				return false;
			if (dppTtcInitial().floatValue() >= 0.0)
				return false;

			if (exercice() == null || fournisseur() == null)
				return false;
		}

		return true;
	}

	public boolean isSupprimable() {
		if (pasDeDepenseBudgetaireCourante()) return true;

		for (int i = 0; i < currentDepenseBudgets().count(); i++) {
			_IDepenseBudget depenseBudget = (_IDepenseBudget) currentDepenseBudgets().objectAtIndex(i);
			if (!depenseBudget.isSupprimable())
				return false;
		}
		return true;
	}

	// pb de nouvelle caledonie ... pourtant bizarre, j'ai pas trouvé d'appel avec cette signature de methode
	public EOCommande commandeAssociee() {
		return commandeassociee();
	}

	public EOCommande commandeassociee() {
		EOCommande commande = null;
		NSArray<? extends _IDepenseBudget> depBuds = currentDepenseBudgets();

		if (depBuds != null && depBuds.count() > 0) {
			_IDepenseBudget db = (_IDepenseBudget) depBuds.lastObject();
			NSArray<EOCommandeEngagement> cmdeEngs = db.engagementBudget().commandeEngagements();
			if (cmdeEngs != null && cmdeEngs.count() > 0) {
				EOCommandeEngagement cmdeEng = (EOCommandeEngagement) cmdeEngs.lastObject();
				commande = cmdeEng.commande();
			}
		}

		return commande;
	}

	public EOCommande commande() {
		return commande;
	}

	public void setCommande(EOCommande cde, EOUtilisateur utilisateur) {
		commande = cde;
		if (commande != null) {
			if (commande.exercice() != null) {
				setExerciceRelationship(commande.exercice());
			}
			if (commande.fournisseur() != null)
				setFournisseurRelationship(commande.fournisseur());

			boolean prerempli = FinderParametre.getParametreLiquidationPreremplie(editingContext(), exercice());
			//arrayDepenseBudgets = commande.initialiserDepenseBudgets(editingContext(), this, utilisateur, prerempli);

			BigDecimal montantEngsHt = new BigDecimal(0.0);
			BigDecimal montantEngsTtc = new BigDecimal(0.0);

			if (prerempli && commande.commandeEngagements() != null) {
				setDppNbPiece(new Integer(1));

				for (int i = 0; i < commande.commandeEngagements().count(); i++) {
					EOEngagementBudget engagementBudget = ((EOCommandeEngagement) commande.commandeEngagements().objectAtIndex(i)).engagementBudget();
					montantEngsHt = montantEngsHt.add(engagementBudget.montantHtReste());
					montantEngsTtc = montantEngsTtc.add(engagementBudget.montantTtcReste());
				}
			}
			setDppHtInitial(montantEngsHt);
			setDppTtcInitial(montantEngsTtc);
		}
		else
			arrayDepenseBudgets = null;
	}

	public void setCommande(EOCommande cde) {
		commande = cde;
	}

	public void setDppIdProc(Number value) {
		dppIdProc = value;
	}

	public Number dppIdProc() {
		return dppIdProc;
	}

	public void setDppHtSaisie(BigDecimal aValue) {
		aValue = aValue.setScale(2, BigDecimal.ROUND_HALF_UP);
		super.setDppHtSaisie(aValue);

		if (dppTtcSaisie() != null && dppHtSaisie() != null) {
			if (dppTtcSaisie().abs().floatValue() > dppHtSaisie().abs().floatValue())
				setDppTvaSaisie(dppTtcSaisie().subtract(dppHtSaisie()));
		}
	}

	public void setDppTvaSaisie(BigDecimal aValue) {
		aValue = aValue.setScale(2, BigDecimal.ROUND_HALF_UP);
		super.setDppTvaSaisie(aValue);
	}

	public void setDppTtcSaisie(BigDecimal aValue) {
		aValue = aValue.setScale(2, BigDecimal.ROUND_HALF_UP);
		super.setDppTtcSaisie(aValue);

		if (dppTtcSaisie() != null && dppHtSaisie() != null) {
			if (dppTtcSaisie().abs().floatValue() > dppHtSaisie().abs().floatValue())
				setDppTvaSaisie(dppTtcSaisie().subtract(dppHtSaisie()));
		}
	}

	public void setDppNumeroFacture(String aValue) {
		if (aValue != null)
			aValue = aValue.trim();
		super.setDppNumeroFacture(aValue);
	}

	public void setDppHtInitial(BigDecimal aValue) {
		aValue = aValue.setScale(2, BigDecimal.ROUND_HALF_UP);
		super.setDppHtInitial(aValue);

		if (commande != null /* && (dppTtcInitial()==null || dppTtcInitial().floatValue()==0) */) {
			if (dppHtInitial().floatValue() == commande.totalHt().floatValue() || commande.totalHt().floatValue() == 0.0)
				setDppTtcInitial(commande.totalTtc());
			else {
				if (commande.totalTtc().abs().floatValue() >= commande.totalHt().abs().floatValue())
					setDppTtcInitial(dppHtInitial().multiply(commande.totalTtc().divide(commande.totalHt(), 5, BigDecimal.ROUND_HALF_UP)));
				else
					setDppTtcInitial(aValue);
			}
		}

		if (dppTtcInitial() == null || dppTtcInitial().abs().floatValue() < dppHtInitial().abs().floatValue())
			setDppTtcInitial(dppHtInitial());

		if (dppTtcInitial() != null && dppHtInitial() != null) {
			if (dppTtcInitial().abs().floatValue() >= dppHtInitial().abs().floatValue())
				setDppTvaInitial(dppTtcInitial().subtract(dppHtInitial()));
			if (currentDepenseBudgets() != null && currentDepenseBudgets().count() == 1) {
				_IDepenseBudget depenseBudget = (_IDepenseBudget) currentDepenseBudgets().objectAtIndex(0);
				depenseBudget.setDepHtSaisie(dppHtInitial());
				depenseBudget.setDepTtcSaisieSansCalcul(dppTtcInitial());
			}
		}
	}

	public void setDppTvaInitial(BigDecimal aValue) {
		aValue = aValue.setScale(2, BigDecimal.ROUND_HALF_UP);
		super.setDppTvaInitial(aValue);
	}

	public void setDppTtcInitial(BigDecimal aValue) {
		aValue = aValue.setScale(2, BigDecimal.ROUND_HALF_UP);

		if (aValue.abs().floatValue() < dppHtInitial().abs().floatValue())
			aValue = new BigDecimal(dppHtInitial().floatValue());
		super.setDppTtcInitial(aValue);

		if (dppTtcInitial() != null && dppHtInitial() != null) {
			if (dppTtcInitial().abs().floatValue() > dppHtInitial().abs().floatValue())
				setDppTvaInitial(dppTtcInitial().subtract(dppHtInitial()));
			if (currentDepenseBudgets() != null && currentDepenseBudgets().count() == 1) {
				_IDepenseBudget depenseBudget = (_IDepenseBudget) currentDepenseBudgets().objectAtIndex(0);
				depenseBudget.setDepTtcSaisie(dppTtcInitial());
				depenseBudget.setDepHtSaisieSansCalcul(dppHtInitial());
			}
		}
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele e partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (dppHtInitial() == null)
			throw new DepensePapierException(DepensePapierException.dppHtInitialManquant);
		if (dppTtcInitial() == null)
			throw new DepensePapierException(DepensePapierException.dppTtcInitialManquant);
		if (dppTvaInitial() == null)
			setDppTvaInitial(dppTtcInitial().subtract(dppHtInitial()));

		if (dppHtSaisie() == null)
			setDppHtSaisie(new BigDecimal(0.0));
		if (dppTvaSaisie() == null)
			setDppTvaSaisie(new BigDecimal(0.0));
		if (dppTtcSaisie() == null)
			setDppTtcSaisie(new BigDecimal(0.0));

		if (exercice() == null)
			throw new DepensePapierException(DepensePapierException.exerciceManquant);
		if (utilisateur() == null)
			throw new DepensePapierException(DepensePapierException.utilisateurManquant);
		if (fournisseur() == null)
			throw new DepensePapierException(DepensePapierException.fournisseurManquant);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (!isReversement() && modePaiement() != null && modePaiement().isRibObligatoire() && ribFournisseur() == null)
			throw new DepensePapierException(DepensePapierException.ribManquant);
		if (dppNumeroFacture() == null || dppNumeroFacture().length() == 0)
			throw new DepensePapierException(DepensePapierException.dppNumeroFactureManquant);
		if (dppDateFacture() == null)
			throw new DepensePapierException(DepensePapierException.dppDateFactureManquant);
		if (dppDateReception() == null)
			throw new DepensePapierException(DepensePapierException.dppDateReceptionManquant);
		if (dppDateSaisie() == null)
			setDppDateSaisie(new NSTimestamp());
		if (dppNbPiece() == null)
			setDppNbPiece(new Integer(1));

		if (!dppHtInitial().abs().add(dppTvaInitial().abs()).equals(dppTtcInitial().abs()))
			setDppTvaInitial(dppTtcInitial().subtract(dppHtInitial()));
		if (!dppHtInitial().abs().add(dppTvaInitial().abs()).equals(dppTtcInitial().abs()))
			throw new DepensePapierException(DepensePapierException.dppTtcInitialPasCoherent);
		if (!dppHtSaisie().abs().add(dppTvaSaisie().abs()).equals(dppTtcSaisie().abs()))
			setDppTvaSaisie(dppTtcSaisie().subtract(dppHtSaisie()));
		if (!isReversement() && dppTtcInitial().floatValue() < 0.0)
			throw new DepensePapierException(DepensePapierException.depensePapierReversementManquant);
	}

	public boolean isControleHorsMarcheGood() {
		return controleCoherenceParticuliere(controleHorsMarche);
	}

	public boolean isControleMarcheGood() {
		return controleCoherenceParticuliere(controleMarche);
	}

	public boolean isControleActionGood() {
		return controleCoherenceParticuliere(controleAction);
	}

	public boolean isControlePlanComptableGood() {
		// TODO : emargement auto
		if (modePaiement() == null) return false;
		return controleCoherenceParticuliere(controlePlanComptable);
	}

	public boolean isControleConventionGood() {
		return controleCoherenceParticuliere(controleConvention);
	}

	public boolean isControleAnalytiqueGood() {
		return controleCoherenceParticuliere(controleAnalytique);
	}

	/**
	 * @return true si les infos de la facture papier sont remplies (sauf date SF). Permet d'enregistrer une pre liquidation meme sans repartition.
	 */
	public boolean isSFEnregistrable() {
		return isCompletePourSF() && isPreRepartitionGood();
	}

	/**
	 * @return true si tout est ok (sauf la date SF). Permet d'afficher la repartition.
	 */
	public boolean isCompletePourSF() {
		if (dppNumeroFacture() == null || dppNumeroFacture().equals("") == true)
			return false;
		if (dppDateFacture() == null || dppDateReception() == null)
			return false;
		if (dppNbPiece() == null)
			return false;
		if (dppHtInitial() == null || dppTtcInitial() == null)
			return false;
		if (dppTtcInitial().floatValue() < dppHtInitial().floatValue())
			return false;
		if (dppHtInitial().floatValue() < 0.0)
			return false;
		if (dppTtcInitial().floatValue() <= 0.0)
			return false;

		if (exercice() == null || fournisseur() == null || modePaiement() == null)
			return false;

		if (modePaiement().isRibObligatoire() && ribFournisseur() == null)
			return false;
		return true;
	}

	public boolean isRepartitionComplete() {
		return (isControleHorsMarcheGood() || isControleMarcheGood()) &&
				isControleActionGood() &&
				isControlePlanComptableGood() &&
				isControleConventionGood() &&
				isControleAnalytiqueGood();
	}

	/**
	 * @return true si les eventuelles repartitions de preliquidation sont correctes (100% si saisies)
	 */
	public boolean isPreRepartitionGood() {
		//FIXME SF à implementer
		//verifier que le total des predepensebudget est égal au montant saisi
		boolean res = (dppHtInitial().compareTo(getMontantHtTotalPreDepenseBudgets()) == 0 && dppTtcInitial().compareTo(getMontantTtcTotalPreDepenseBudgets()) == 0);
		return res;
	}

	/**
	 * @return Le montant HT calculés à partir des montants saisis pour les predepensebudget
	 */
	public BigDecimal getMontantHtTotalPreDepenseBudgets() {
		BigDecimal total = BigDecimal.ZERO;
		NSArray pdeps = preDepenseBudgets();
		for (Iterator iterator = pdeps.iterator(); iterator.hasNext();) {
			EOPreDepenseBudget depense = (EOPreDepenseBudget) iterator.next();
			total = total.add(depense.depHtSaisie());
		}
		return total;
	}

	/**
	 * @return Le montant TTC calculés à partir des montants saisis pour les predepensebudget
	 */
	public BigDecimal getMontantTtcTotalPreDepenseBudgets() {
		BigDecimal total =  BigDecimal.ZERO;
		NSArray pdeps = preDepenseBudgets();
		for (Iterator iterator = pdeps.iterator(); iterator.hasNext();) {
			EOPreDepenseBudget depense = (EOPreDepenseBudget) iterator.next();
			total = total.add(depense.depTtcSaisie());
		}
		return total;
	}

	/**
	 * @return true si le bouton SF peut etre affiché.
	 */
	public boolean isSFReady() {
		return isCompletePourSF() && isRepartitionComplete();
	}

	public NSArray<? extends _IDepenseBudget> getArrayDepenseBudgets() {
		return arrayDepenseBudgets;
	}

	public void setArrayDepenseBudgets(NSArray<? extends _IDepenseBudget> arrayDepenseBudgets) {
		this.arrayDepenseBudgets = arrayDepenseBudgets;
	}

	/**
	 * @return true si pas de liquidation finale.
	 */
	public boolean isPreLiquidation() {
		return (depenseBudgets() == null || depenseBudgets().isEmpty());
	}

	/**
	 * @return Les depensesBudgets ou preDepenseBudgets selon qu'on est en preLiquidation ou pas. des types _IDepenseBudget.
	 */
	public NSArray<? extends _IDepenseBudget> currentDepenseBudgets() {
		if (isPreLiquidation()) {
			return preDepenseBudgets();
		}
		return depenseBudgets();
	}

	/**
	 * @return Les depensesBudgets ou preDepenseBudgets réalisées sur des budgets hors extourne, selon qu'on est en preLiquidation ou pas. des types
	 *         _IDepenseBudget.
	 */
	public NSArray<? extends _IDepenseBudget> currentDepenseBudgetsHorsExtourne() {
		NSArray<? extends _IDepenseBudget> dbs = (isPreLiquidation() ? preDepenseBudgets() : depenseBudgets());
		NSMutableArray<_IDepenseBudget> res = new NSMutableArray<_IDepenseBudget>();
		for (_IDepenseBudget db : dbs) {
			if (!ESourceCreditType.EXTOURNE.equals(db.getSourceTypeCredit())) {
				res.addObject(db);
			}
		}
		return res;
	}

	public ELiquidationType getLiquidationType() {
		return liquidationType;
	}

	public void setLiquidationType(ELiquidationType liquidationType) {
		this.liquidationType = liquidationType;
	}

	/**
	 * @return true si la depensePapier a des depenseControleMarches reliés.
	 */
	public Boolean isSurMarche() {
		return (depenseBudgets().count() > 0 && depenseBudgets().objectAtIndex(0).depenseControleMarches().count() > 0);
	}

	public EOTypeCredit typesCreditsSurSectionUnique() {
		if (isPreLiquidation()) return null;

		List<EOTypeCredit> typesCreditsLocal = typesCreditSansLesEngagementsNonRenseignes();
		EOTypeCredit typeCredit = null;
		if (typesCreditsLocal.size() > 0) {
			typeCredit = typesCreditsLocal.get(0);
			if(!isTousLesAutresTypesCreditsOnUneSectionLocaleIdentique(
					typesCreditsLocal, typeCredit)) {
				typeCredit = null;
			}
		}
		return typeCredit;
	}

	
	public boolean isAExtourner() {
		return modePaiement().isAExtourner();
	}

	
	protected boolean pasDeDepenseBudgetaireCourante() {
		return currentDepenseBudgets() == null || currentDepenseBudgets().count() == 0;
	}

	protected boolean toutesLesDepensesBudgetairesCourantesRespecteLaCondition(
			Controle condition) {
		for (int i = 0; i < currentDepenseBudgets().count(); i++) {
			_IDepenseBudget element = (_IDepenseBudget) currentDepenseBudgets().objectAtIndex(i);
			if (!condition.isOk(element)) return false;
		}
		return true;
	}
	
	protected boolean montantTTCDifferentSommeElementsBudgetairesTTC() {
		BigDecimal total = computeSumForKey(currentDepenseBudgets(), _IDepenseBudget.DEP_TTC_SAISIE_KEY);
		return dppTtcInitial().compareTo(total) != 0;
	}

	protected boolean montantHTDifferentSommeElementsBudgetairesHT() {
		BigDecimal total = computeSumForKey(currentDepenseBudgets(), _IDepenseBudget.DEP_HT_SAISIE_KEY);
		return dppHtInitial().compareTo(total) != 0;
	}

	protected boolean controleCoherenceParticuliere(Controle controle) {

		if (pasDeDepenseBudgetaireCourante()) return false;
		if(!toutesLesDepensesBudgetairesCourantesRespecteLaCondition(controle)) return false;
		if (montantHTDifferentSommeElementsBudgetairesHT()) return false;
		if (montantTTCDifferentSommeElementsBudgetairesTTC())return false;

		return true;
	}

	private BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo == null || eo.count() == 0)
			return Calculs.ZERO;
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}

	private boolean isTousLesAutresTypesCreditsOnUneSectionLocaleIdentique(
			List<EOTypeCredit> typesCreditsLocal, EOTypeCredit typeCredit) {
		for (int i = 1; i < typesCreditsLocal.size(); i++) {
			EOTypeCredit typeCredit2 = typesCreditsLocal.get(i);
			if (!typeCredit2.tcdSect().equals(typeCredit.tcdSect())) {
				return false;
			}
		}
		return true;
	}

	private List<EOTypeCredit> typesCreditSansLesEngagementsNonRenseignes() {
		List<EOTypeCredit> typesCreditsLocal = new ArrayList<EOTypeCredit>();
		for (EODepenseBudget depenseBudget : this.depenseBudgets()) {
			EOEngagementBudget engagementBudget = depenseBudget.engagementBudget();
			if (engagementBudget != null && engagementBudget.typeCredit() != null) {
				typesCreditsLocal.add(engagementBudget.typeCredit());
			}
		}
		return typesCreditsLocal;
	}



}
