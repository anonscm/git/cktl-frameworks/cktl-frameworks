/**
 * $License$
 */
package org.cocktail.fwkcktldepense.server.filtre;

import org.apache.commons.lang.StringUtils;

/**
 * POJO permettant de decrire un filtre.
 * @author flagouey
 * $LastChangedBy: flagouey $
 * $Date: 2012-08-27 17:17:41 +0200 (lun., 27 août 2012) $
 *
 */
public class CktlDepenseFiltreBean {

	private String key;
	private String label;
	private String defaultValue;
	private String value;

	public CktlDepenseFiltreBean(String key, String label) {
		this(key, label, null);
	}

	public CktlDepenseFiltreBean(String key, String label, String defaultValue) {
		this.key = key;
		this.label = label;
		this.defaultValue = defaultValue;
		this.value = defaultValue;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isEmpty() {
		return StringUtils.isEmpty(getValue());
	}

	public void clear() {
		this.value = this.defaultValue;
	}

	@Override
	public String toString() {
		return "CktlDepenseFiltreBean [key=" + key + ", label=" + label
				+ ", defaultValue=" + defaultValue + ", value=" + value + "]";
	}
}
