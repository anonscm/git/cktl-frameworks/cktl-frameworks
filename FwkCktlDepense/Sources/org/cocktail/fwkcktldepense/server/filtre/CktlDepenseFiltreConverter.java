/*
 * $License$
 */
package org.cocktail.fwkcktldepense.server.filtre;

import java.util.List;

/**
 * Convertit une liste de filtres en un résultat compréhensible par l'appelant.
 * @param <T> le type de résultat attendu par l'appelant.
 *
 * @author flagouey
 * $Date: 2012-06-29 11:45:09 +0200 (ven., 29 juin 2012) $
 * $LastChangedBy: flagouey $
 */
public interface CktlDepenseFiltreConverter<T> {

	/**
	 * Convertit une collection de filtre.
	 * @param filtres filtres à convertir.
	 * @return resultat de la conversion.
	 */
	public T convert(List<CktlDepenseFiltreBean> filtres);

}
