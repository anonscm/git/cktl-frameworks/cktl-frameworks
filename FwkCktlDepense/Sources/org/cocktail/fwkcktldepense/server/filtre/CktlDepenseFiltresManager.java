/*
 * $License$
 */
package org.cocktail.fwkcktldepense.server.filtre;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Gere une collection de filtres.
 *
 * @author flagouey $Date: 2012-07-17 09:06:42 +0200 (mar., 17 juil. 2012) $ $LastChangedBy: flagouey $
 */
public class CktlDepenseFiltresManager {

	/* Filtres. */
	private List<CktlDepenseFiltreBean> filtres;

	/**
	 * Constructeur par défaut.
	 */
	public CktlDepenseFiltresManager() {
		this.filtres = new ArrayList<CktlDepenseFiltreBean>();
	}

	/**
	 * Ajouter une filtre.
	 *
	 * @param filtre filtre à ajouter.
	 * @return true si l'ajout a eu lieu ; false sinon.
	 */
	public boolean add(CktlDepenseFiltreBean filtre) {
		return getFiltres().add(filtre);
	}

	public boolean addAll(Collection<CktlDepenseFiltreBean> filtres) {
		return this.filtres.addAll(filtres);
	}

	/**
	 * @return les filtres.
	 */
	public List<CktlDepenseFiltreBean> getFiltres() {
		//TODO on peut imaginer retourner une nouvelle liste pour bloquer les modifs sans passer par le manager
		return filtres;
	}

	/**
	 * @param filtres nouveaux filtres (remplace la collection existante).
	 */
	public void setFiltres(List<CktlDepenseFiltreBean> filtres) {
		this.filtres = filtres;
	}

	/**
	 * Vide les filtres présents.
	 */
	public void clear() {
		this.filtres.clear();
	}

	/**
	 * Convertit les filtres.
	 *
	 * @param converter le convertisseur.
	 * @return le résultat de la conversion.
	 */
	public <T> T convert(CktlDepenseFiltreConverter<T> converter) {
		return converter.convert(filtres);
	}

}
