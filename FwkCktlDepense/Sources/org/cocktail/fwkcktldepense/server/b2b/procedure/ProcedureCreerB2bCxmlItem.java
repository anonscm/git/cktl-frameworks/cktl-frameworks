/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.b2b.procedure;

import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldepense.server.procedure.Procedure;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerB2bCxmlItem extends Procedure {

	private static final String PROCEDURE_NAME = "insB2bCxmlItem";

	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOB2bCxmlItem cxmlItem) throws NSValidation.ValidationException {
		cxmlItem.validateForSave();

		return dataBus.executeProcedure(ProcedureCreerB2bCxmlItem.PROCEDURE_NAME,
				ProcedureCreerB2bCxmlItem.construireDictionnaire(cxmlItem));
	}

	private static NSDictionary construireDictionnaire(EOB2bCxmlItem cxmlItem) {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		Integer fbcpId = (Integer) EOUtilities.primaryKeyForObject(cxmlItem.editingContext(), cxmlItem.toFournisB2bCxmlParam()).valueForKey(EOFournisB2bCxmlParam.FBCP_ID_KEY);
		dico.takeValueForKey(fbcpId, "020_a_fbcpid");
		dico.takeValueForKey(cxmlItem.toArticle().artIdProc(), "030_a_artid");
		dico.takeValueForKey(cxmlItem.classification(), "040_a_classification");
		dico.takeValueForKey(cxmlItem.classificationDom(), "050_a_classificationdom");
		dico.takeValueForKey(cxmlItem.dateCreation(), "060_a_datecreation");
		dico.takeValueForKey(cxmlItem.description(), "070_a_description");
		dico.takeValueForKey(cxmlItem.manufacturerName(), "080_a_manufacturername");
		dico.takeValueForKey(cxmlItem.manufacturerPartID(), "090_a_manufacturerpartid");
		dico.takeValueForKey(cxmlItem.persIdCreation(), "100_a_persidcreation");
		dico.takeValueForKey(cxmlItem.supplierPartId(), "110_a_supplierpartid");
		dico.takeValueForKey(cxmlItem.supplierPartAuxiliaryId(), "120_a_supplierpartauxiliaryid");
		dico.takeValueForKey(cxmlItem.unitOfMeasure(), "130_a_unitofmeasure");
		dico.takeValueForKey(cxmlItem.unitPrice(), "140_a_unitprice");
		dico.takeValueForKey(cxmlItem.url(), "150_a_url");
		dico.takeValueForKey(cxmlItem.comments(), "160_a_comments");

		return dico;
	}
}