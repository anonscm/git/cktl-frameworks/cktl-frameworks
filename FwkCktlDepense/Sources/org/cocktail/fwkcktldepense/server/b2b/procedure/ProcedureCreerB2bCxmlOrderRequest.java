/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.b2b.procedure;

import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldepense.server.procedure.Procedure;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerB2bCxmlOrderRequest extends Procedure {

	private static final String PROCEDURE_NAME = "insB2bCxmlOrderRequest";

	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOB2bCxmlOrderRequest msg) throws NSValidation.ValidationException {
		msg.validateForSave();

		return dataBus.executeProcedure(ProcedureCreerB2bCxmlOrderRequest.PROCEDURE_NAME,
				ProcedureCreerB2bCxmlOrderRequest.construireDictionnaire(msg));
	}

	private static NSDictionary construireDictionnaire(EOB2bCxmlOrderRequest msg) {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		EOFournisB2bCxmlParam param = msg.toFournisB2bCxmlParam();

		Integer fbcpId = (Integer) EOUtilities.primaryKeyForObject(param.editingContext(), param).valueForKey(EOFournisB2bCxmlParam.FBCP_ID_KEY);

		dico.takeValueForKey(fbcpId, "020_a_fbcpid");
		dico.takeValueForKey(msg.commId(), "030_a_commid");
		dico.takeValueForKey(msg.dateCreation(), "040_a_datecreation");
		dico.takeValueForKey(msg.persIdCreation(), "050_a_persidcreation");
		dico.takeValueForKey(msg.cxml(), "060_a_cxml");
		dico.takeValueForKey(msg.cxmlReponse(), "070_a_cxmlreponse");
		return dico;
	}
}