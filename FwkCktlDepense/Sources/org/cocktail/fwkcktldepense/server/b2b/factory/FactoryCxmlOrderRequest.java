/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.b2b.factory;

import java.util.Date;

import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLMessageFactory;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOCxml;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Entite qui memorise le cxml envoye lors d''un OrderRequest.
 *
 * @author rprin
 */
public class FactoryCxmlOrderRequest {
	public static final String EXCEPTION_PREFIX = "[B2B] ";

	/**
	 * Cree l'entite qui memorise le cxml envoye lors d''un OrderRequest.
	 *
	 * @param ed
	 * @param xmlContent
	 * @return
	 * @throws Exception
	 */
	public EOB2bCxmlOrderRequest creerOrderRequest(EOEditingContext ed, Integer persId, EOCommande commande, EOFournisB2bCxmlParam param, String xmlContent) throws Exception {
		if (xmlContent == null) {
			throw new Exception(EXCEPTION_PREFIX + "Contenu XML vide. Impossible de creer une instance de EOB2bCxmlOrderRequest \n");
		}
		//analyser le cxml pour recuperer le param
		EOCxml orderRequest = CXMLMessageFactory.load(xmlContent);
		if (!orderRequest.isOrderRequest()) {
			throw new Exception(EXCEPTION_PREFIX + "Le message n'est pas un OrderRequest \n" + xmlContent);
		}

		if (param == null) {
			throw new Exception(EXCEPTION_PREFIX + "Profil (parametres) B2b CXML nul");
		}

		EOB2bCxmlOrderRequest res = EOB2bCxmlOrderRequest.creerInstance(ed);
		res.setPersIdCreation(persId);
		res.setDateCreation(new NSTimestamp(new Date()));
		res.setCxml(xmlContent);
		res.setToFournisB2bCxmlParamRelationship(param.localInstanceIn(ed));
		res.setToCommandeRelationship(commande.localInstanceIn(ed));
		res.setCommId(Integer.valueOf(((Number) EOUtilities.primaryKeyForObject(commande.editingContext(), commande).valueForKey(EOCommande.COMM_ID_KEY)).intValue()));
		return res;
	}
}
