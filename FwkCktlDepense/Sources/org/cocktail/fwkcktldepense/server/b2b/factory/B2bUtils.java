/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.b2b.factory;

import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemOut;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class B2bUtils {

	/**
	 * Convertit un EOArticle en ItemOut pour envoi en orderRequest.
	 * 
	 * @return
	 */
	public static EOItemOut articleToItemOut(EOEditingContext ec, EOArticle article, Integer lineNumber) {

		if (article.toB2bCxmlItem() != null) {
			EOItemOut itemOut = EOItemOut.createEmptyItemOut(ec);
			itemOut.itemID().setSupplierPartAuxiliaryID(article.toB2bCxmlItem().supplierPartAuxiliaryId());
			itemOut.itemID().setSupplierPartID(article.toB2bCxmlItem().supplierPartId());
			//itemOut.comments().setVal(null);
			itemOut.setLineNumber(lineNumber);
			itemOut.setQuantity(Integer.valueOf(article.artQuantite().intValue()));
			//itemOut.setRequestedDeliveryDate(null);
			//itemOut.setRequisitionID(null);
			//itemOut.setShippingRelationship(null);
			//itemOut.setShipToRelationship(null);
			itemOut.setSupplierIDRelationship(null);
			itemOut.tax().money().setCurrency(article.commande().devise().devCode());
			itemOut.tax().money().setVal(article.artPrixTotalTtc().subtract(article.artPrixTotalHt()));
			itemOut.itemDetail().classification().setVal(article.toB2bCxmlItem().classification());
			itemOut.itemDetail().classification().setDomain(article.toB2bCxmlItem().classificationDom());
			itemOut.itemDetail().description().setVal(article.toB2bCxmlItem().description());
			itemOut.itemDetail().setManufacturerName(article.toB2bCxmlItem().manufacturerName());
			itemOut.itemDetail().setManufacturerPartID(article.toB2bCxmlItem().manufacturerPartID());
			itemOut.itemDetail().setUnitOfMeasure(article.toB2bCxmlItem().unitOfMeasure());
			itemOut.itemDetail().unitPrice().money().setCurrency(article.commande().devise().devCode());
			itemOut.itemDetail().unitPrice().money().setVal(article.artPrixHt());
			//itemOut.itemDetail().url().setName(null);
			itemOut.itemDetail().url().setVal(article.toB2bCxmlItem().url());
			itemOut.comments().setVal(article.toB2bCxmlItem().comments());
			return itemOut;
		}
		return null;
	}

	public static NSArray<EOItemOut> articlesToItemOuts(EOEditingContext ec, NSArray<EOArticle> articles) {
		NSMutableArray<EOItemOut> res = new NSMutableArray<EOItemOut>();
		for (int i = 0; i < articles.count(); i++) {
			EOArticle article = (EOArticle) articles.objectAtIndex(i);
			res.addObject(articleToItemOut(ec, article, new Integer(i + 1)));
		}
		return res.immutableClone();
	}

}
