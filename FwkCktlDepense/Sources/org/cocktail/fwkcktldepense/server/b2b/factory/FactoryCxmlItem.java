/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.b2b.factory;

import java.util.Date;

import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn;
import org.cocktail.fwkcktldepense.server.exception.ArticleException;
import org.cocktail.fwkcktldepense.server.factory.Factory;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlItem;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryCxmlItem extends Factory {
	public static final String EXCEPTION_PREFIX = "[B2B] ";

	public EOB2bCxmlItem creerEOB2bCxmlItem(EOEditingContext ed, Integer persId, EOArticle article, EOFournisB2bCxmlParam fournisCxmlParam, EOItemIn b2bCxmlItemIn) throws ArticleException {
		try {
			//remplir les elements propres au b2b
			EOB2bCxmlItem eob2bCxmlItem = EOB2bCxmlItem.creerInstance(ed);

			eob2bCxmlItem.setToFournisB2bCxmlParamRelationship(fournisCxmlParam);
			eob2bCxmlItem.setClassification(b2bCxmlItemIn.itemDetail().classification().getVal());
			eob2bCxmlItem.setClassificationDom(b2bCxmlItemIn.itemDetail().classification().domain());
			eob2bCxmlItem.setDateCreation(new NSTimestamp(new Date()));
			eob2bCxmlItem.setDescription(b2bCxmlItemIn.itemDetail().description().getVal());
			eob2bCxmlItem.setManufacturerName(b2bCxmlItemIn.itemDetail().manufacturerName());
			eob2bCxmlItem.setManufacturerPartID(b2bCxmlItemIn.itemDetail().manufacturerPartID());
			eob2bCxmlItem.setSupplierPartId(b2bCxmlItemIn.itemID().supplierPartID());
			eob2bCxmlItem.setSupplierPartAuxiliaryId(b2bCxmlItemIn.itemID().supplierPartAuxiliaryID());
			eob2bCxmlItem.setPersIdCreation(persId);
			eob2bCxmlItem.setUnitOfMeasure(b2bCxmlItemIn.itemDetail().unitOfMeasure());
			eob2bCxmlItem.setUnitPrice(b2bCxmlItemIn.itemDetail().unitPrice().money().getVal());
			eob2bCxmlItem.setComments(null);
			if (b2bCxmlItemIn.itemDetail().url() != null) {
				eob2bCxmlItem.setUrl(b2bCxmlItemIn.itemDetail().url().getVal());
			}
			eob2bCxmlItem.setToArticleRelationship(article);
			article.addToToB2bCxmlItemsRelationship(eob2bCxmlItem);

			return eob2bCxmlItem;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ArticleException(EXCEPTION_PREFIX + e.getMessage());
		}

	}

	/**
	 * Duplique l'item pour un nouvel article.
	 *
	 * @param ed
	 * @param persId
	 * @param article
	 * @param oldEOB2bCxmlItem
	 * @return
	 * @throws ArticleException
	 */
	public EOB2bCxmlItem dupliquerEOB2bCxmlItem(EOEditingContext ed, Integer persId, EOArticle article, EOB2bCxmlItem oldEOB2bCxmlItem) throws ArticleException {
		try {
			EOB2bCxmlItem eob2bCxmlItem = EOB2bCxmlItem.creerInstance(ed);

			eob2bCxmlItem.setToFournisB2bCxmlParamRelationship(oldEOB2bCxmlItem.toFournisB2bCxmlParam());
			eob2bCxmlItem.setClassification(oldEOB2bCxmlItem.classification());
			eob2bCxmlItem.setClassificationDom(oldEOB2bCxmlItem.classificationDom());
			eob2bCxmlItem.setDateCreation(new NSTimestamp(new Date()));
			eob2bCxmlItem.setDescription(oldEOB2bCxmlItem.description());
			eob2bCxmlItem.setManufacturerName(oldEOB2bCxmlItem.manufacturerName());
			eob2bCxmlItem.setManufacturerPartID(oldEOB2bCxmlItem.manufacturerPartID());
			eob2bCxmlItem.setSupplierPartId(oldEOB2bCxmlItem.supplierPartId());
			eob2bCxmlItem.setSupplierPartAuxiliaryId(oldEOB2bCxmlItem.supplierPartAuxiliaryId());
			eob2bCxmlItem.setPersIdCreation(persId);
			eob2bCxmlItem.setUnitOfMeasure(oldEOB2bCxmlItem.unitOfMeasure());
			eob2bCxmlItem.setUnitPrice(oldEOB2bCxmlItem.unitPrice());
			eob2bCxmlItem.setUrl(oldEOB2bCxmlItem.url());
			eob2bCxmlItem.setComments(oldEOB2bCxmlItem.comments());
			eob2bCxmlItem.setToArticleRelationship(article);
			article.addToToB2bCxmlItemsRelationship(eob2bCxmlItem);

			return eob2bCxmlItem;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ArticleException(EXCEPTION_PREFIX + e.getMessage());
		}

	}
}
