/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.b2b.process;

import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLMessageFactory;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLParameters;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLTransaction;
import org.cocktail.fwkcktlb2b.cxml.server.engine.ProxyProperties;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOCxml;
import org.cocktail.fwkcktldepense.server.b2b.factory.FactoryCxmlOrderRequest;
import org.cocktail.fwkcktldepense.server.b2b.procedure.ProcedureCreerB2bCxmlOrderRequest;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldepense.server.process.Process;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOEditingContext;

public class ProcessGestionCommandeB2b extends Process {

	public static String b2bSendOrderRequest(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOCxml cxmlOrderRequest, CXMLParameters parameters, ProxyProperties proxyProperties, Integer persId, EOFournisB2bCxmlParam param) throws FactoryException {
		EOB2bCxmlOrderRequest or = null;
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			//Enregistrer le message a envoyer
			String cxml = CXMLMessageFactory.toXml(cxmlOrderRequest);
			FactoryCxmlOrderRequest fa = new FactoryCxmlOrderRequest();
			or = fa.creerOrderRequest(ed, persId, commande, param, cxml);

			databus.beginTransaction();

			//Envoyer le message
			CXMLTransaction transaction = new CXMLTransaction();
			String res = transaction.creerTransactionOrderRequest(cxmlOrderRequest, parameters, proxyProperties);
			or.setCxmlReponse(res);

			String orderId = transaction.parseOrderRequestResponse(res);
			ProcedureCreerB2bCxmlOrderRequest.enregistrer(databus, or);

			//On commit si c'est ok
			databus.commitTransaction();

			return orderId;

		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}
}
