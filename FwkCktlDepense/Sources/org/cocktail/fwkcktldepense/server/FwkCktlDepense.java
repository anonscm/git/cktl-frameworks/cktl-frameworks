package org.cocktail.fwkcktldepense.server;

import er.extensions.ERXFrameworkPrincipal;

public class FwkCktlDepense extends ERXFrameworkPrincipal {
	public static FwkCktlDepenseParamManager paramManager = new FwkCktlDepenseParamManager();
	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlDepense.class);
	}

	@Override
	public void didFinishInitialization() {
		super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();

	}

	@Override
	public void finishInitialization() {
		// TODO Auto-generated method stub

	}
}
