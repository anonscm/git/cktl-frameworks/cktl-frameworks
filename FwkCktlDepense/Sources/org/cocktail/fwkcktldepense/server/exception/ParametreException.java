/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;

import org.cocktail.fwkcktldepense.server.metier.EOParametre;

public class ParametreException extends FactoryException {
	public static final String articleCmNiveauManquant = "il faut definir le parametre "+EOParametre.ARTICLE_CM_NIVEAU;
	public static final String ctrlOrganDestManquant = "il faut definir le parametre "+EOParametre.CTRL_ORGAN_DEST;
	public static final String ctrlLimiteBudgestManquant = "il faut definir le parametre "+EOParametre.CTRL_LIMITE_BUDGEST;
	public static final String depenseIdemTauxProrataManquant = "il faut definir le parametre "+EOParametre.DEPENSE_IDEM_TAP_ID;
	public static final String liquidationPreremplieManquant = "il faut definir le parametre "+EOParametre.LIQUIDATION_PREREMPLIE;
	public static final String inventaireObligatoireManquant = "il faut definir le parametre "+EOParametre.INVENTAIRE_OBLIGATOIRE;

	public static final String articleCmNiveauIncoherent = "le parametre "+EOParametre.ARTICLE_CM_NIVEAU+" n''a pas la bonne valeur";

	public ParametreException() {
		super();
	}

	public ParametreException(String s) {
		super(s);
	}

}
