/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;



public class DepensePapierException extends FactoryException {
	public static final String dppHtInitialManquant = "il faut saisir le montant HT de la facture";
	public static final String dppTvaInitialManquant = "il faut saisir le montant de la TVA de la facture";
	public static final String dppTtcInitialManquant = "il faut saisir le montant TTC de la facture";
	public static final String dppHtSaisieManquant = "il faut saisir le montant liquide HT de la facture";
	public static final String dppTvaSaisieManquant = "il faut saisir le montant liquide de la TVA de la facture";
	public static final String dppTtcSaisieManquant = "il faut saisir le montant liquide TTC de la facture";
	public static final String dppNumeroFactureManquant = "il faut saisir le numero de la facture";
	public static final String dppDateServiceFaitManquant = "il faut saisir la date de service fait de la facture";
	public static final String dppDateSaisieManquant = "il faut saisir la date de saisie de la facture";
	public static final String dppDateReceptionManquant = "il faut saisir la date de reception fait de la facture";
	public static final String dppDateFactureManquant = "il faut saisir la date de la facture";
	public static final String exerciceManquant = "il faut associer un exercice";
	public static final String utilisateurManquant = "il faut associer l'utilisateur creant la facture";
	public static final String modePaiementManquant = "il faut associer un mode de paiement";
	public static final String ribManquant = "pour ce mode de paiement, il faut associer un rib";
	public static final String fournisseurManquant = "il faut associer un fournisseur";
	public static final String depensePapierReversementManquant = "c'est un ordre de reversement il faut associer la depense initiale";

	public static final String dppTtcInitialPasCoherent = "le montant TTC n'est pas coherent avec le HT et la TVA";
	public static final String dppTtcSaisiePasCoherent = "le montant liquide TTC n'est pas coherent avec le HT et la TVA";
	public static final String montantsDepenseBudgetIncoherent = "les montants des actions sont incoherents par rapport a ceux de l'engagement";

	public DepensePapierException() {
		super();
	}

	public DepensePapierException(String s) {
		super(s);
	}

}
