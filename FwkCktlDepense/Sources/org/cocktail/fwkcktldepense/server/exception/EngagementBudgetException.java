/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;

public class EngagementBudgetException extends FactoryException {
	public static final String engHtSaisieManquant = "il faut saisir le montant HT de l'engagement";
	public static final String engTvaSaisieManquant = "il faut saisir le montant de la TVA de l'engagement";
	public static final String engTtcSaisieManquant = "il faut saisir le montant TTC de l'engagement";
	public static final String engMontantBudgetaireManquant = "il faut saisir le montant budgetaire de l'engagement";
	public static final String engMontantBudgetaireResteManquant = "il faut saisir le montant budgetaire restant de l'engagement";
	public static final String engDateSaisieManquant = "il faut saisir la date de saisie de l'engagement";
	public static final String engLibelleManquant = "il faut saisir le libelle de l'engagement";
	public static final String utilisateurManquant = "il faut associer l'utilisateur qui cree l'engagement";
	public static final String typeCreditManquant = "il faut associer un type de credit";
	public static final String typeApplicationManquant = "il faut associer un type d'application";
	public static final String tauxProrataManquant = "il faut associer un taux de prorata";
	public static final String organManquant = "il faut associer une ligne budgetaire";
	public static final String fournisseurManquant = "il faut associer un fournisseur";
	public static final String exerciceManquant = "il faut associer un exercice";

	public static final String codeAnalytiqueManquant = "pour cette ligne budgetaire il faut associer au moins un code analytique";
	public static final String montantsOrganAnalytiqueIncoherent="pour cette ligne budgetaire il faut repartir entierement le montant parmi les codes analytiques choisis";

	public static final String CONVENTION_MANQUANTE = "pour cette ligne budgetaire il faut associer au moins une convention";

	public static final String engTtcSaisiePasCoherent = "le montant TTC n'est pas coherent avec le HT et la TVA";
	public static final String engMontantBudgetairePasCoherent = "le montant budgetaire n'est pas coherent avec le taux de prorata, le HT et la TVA";
	public static final String engMontantBudgetaireRestePasCoherent = "le montant budgetaire restant n'est pas coherent avec le taux de prorata, le HT et la TVA";
	public static final String montantsNegatifs = "les montants de l'engagement doivent etre prositifs";

	public static final String montantsEngagementControleActionIncoherent = "les montants des actions sont incoherents par rapport a ceux de l'engagement";
	public static final String montantsEngagementControleAnalytiqueIncoherent = "les montants des codes analytiques sont incoherents par rapport a ceux de l'engagement";
	public static final String montantsEngagementControleConventionIncoherent = "les montants des conventions sont incoherents par rapport a ceux de l'engagement";
	public static final String montantsEngagementControleHorsMarcheIncoherent = "les montants des codes de nomenclature sont incoherents par rapport a ceux de l'engagement";
	public static final String montantsEngagementControleMarcheIncoherent = "les montants des attributions sont incoherents par rapport a ceux de l'engagement";
	public static final String montantsEngagementControlePlanComptableIncoherent = "les montants des imputations sont incoherents par rapport a ceux de l'engagement";
	public static final String controleMarcheHorsMarcheManquant = "l'engagement doit etre 'marche' ou 'hors marche'";
	public static final String controleMarcheHorsMarcheIncoherent = "l'engagement ne peut etre 'marche' et 'hors marche' a la fois";

	public static final String montantsDepenseBudgetIncoherent = "les montants des depenses sont incoherents par rapport a ceux de l'engagement";
	public static final String montantsBudgetairesDepenseBudgetIncoherent = "les montants budgetaires des depenses sont incoherents par rapport a ceux de l'engagement";

	public static final String supprimerEngageControle = "on ne peut pas supprimer cet engagement car il y a des liquidations";

	public EngagementBudgetException() {
		super();
	}

	public EngagementBudgetException(String s) {
		super(s);
	}
}
