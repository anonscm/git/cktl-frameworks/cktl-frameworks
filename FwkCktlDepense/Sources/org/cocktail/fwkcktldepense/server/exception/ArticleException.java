/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;

public class ArticleException extends FactoryException {
	public static final String artPrixHtManquant = "il faut saisir le prix unitaire HT de l'article";
	public static final String artPrixTtcManquant = "il faut saisir le prix unitaire TTC de l'article";
	public static final String artQuantiteManquant = "il faut saisir la quantite de l'article";
	public static final String artQuantiteNegative = "il faut que la quantite de l'article soit positive";
	public static final String artLibelleManquant = "il faut saisir le libelle de l'article";
	public static final String tvaManquant = "il faut saisir la tva de l'article";
	public static final String codeExerManquant = "il faut saisir le code de nomenclature de l'article";
	public static final String commandeManquant = "il faut associer l'article a une commande";
	public static final String fournisseurManquant = "il faut associer l'article a un fournisseur";
	public static final String typeEtatManquant = "il faut associer l'article a un type etat";

	public static final String artPrixTtcPasCoherent = "le prix unitaire TTC n'est pas coherent avec le prix unitaire HT et le taux de tva";
	public static final String artPrixTotalHtPasCoherent = "le total HT n'est pas coherent par rapport au HT unitaire et a la tva";
	public static final String artPrixTotalTtcPasCoherent = "le total HT n'est pas coherent par rapport au HT unitaire et a la tva";
	public static final String codeExerPasBonNiveau = "le code de nomenclature doit etre de niveau 2 ou 3";
	public static final String attributionIncoherente = "l'attribution n'est pas coherente";
	public static final String attributionInvalide = "l'attribution n'est pas valide pour la date de creation de la commande";
	public static final String attributionOuTypeAchat = "il faut une attribution ou un type achat";

	public ArticleException() {
		super();

	}

	public ArticleException(String s) {
		super(s);

	}

}
