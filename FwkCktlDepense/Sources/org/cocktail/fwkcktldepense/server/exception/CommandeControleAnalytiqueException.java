/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;

public class CommandeControleAnalytiqueException extends FactoryException {
	public static final String canaHtSaisieManquant = "il faut saisir le montant HT de l'action";
	public static final String canaTvaSaisieManquant = "il faut saisir le montant de la TVA de l'action";
	public static final String canaTtcSaisieManquant = "il faut saisir le montant TTC de l'action";
	public static final String canaMontantBudgetaireManquant = "il faut saisir le montant budgetaire de l'action";
	public static final String canaPourcentageManquant = "il faut saisir le pourcentage du code analytique";
	public static final String codeAnalytiqueManquant = "il faut associer un code analytique";
	public static final String exerciceManquant = "il faut associer un exercice";
	public static final String commandeBudgetManquant = "il faut associer un commandeBudget";
	public static final String montantsNegatifs = "les montants de la repartition doivent etre positifs";
	
	public static final String canaTtcSaisiePasCoherent = "le montant TTC n'est pas coherent avec le HT et la TVA";
	public static final String canaMontantBudgetairePasCoherent = "le montant budgetaire n'est pas coherent avec le taux de prorata, le HT et la TVA";
	public static final String commandeBudgetExercicePasCoherent = "il faut que la commandeAnalytique soit sur le meme exercice que la commandeBudget";

	public CommandeControleAnalytiqueException() {
		super();
	}

	public CommandeControleAnalytiqueException(String s) {
		super(s);
	}

}
