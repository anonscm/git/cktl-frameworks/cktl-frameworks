/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;


public class DepenseBudgetException extends FactoryException {
	public static final String depHtSaisieManquant = "il faut saisir le montant HT de la liquidation";
	public static final String depTvaSaisieManquant = "il faut saisir le montant de la TVA de la liquidation";
	public static final String depTtcSaisieManquant = "il faut saisir le montant TTC de la liquidation";
	public static final String depMontantBudgetaireManquant = "il faut saisir le montant budgetaire de la liquidation";
	public static final String tauxProrataManquant = "il faut associer un taux de prorata";
	public static final String exerciceManquant = "il faut associer un exercice";
	public static final String engagementBudgetManquant = "il faut associer l'engagement";
	public static final String depensePapierManquant = "il faut associer une facture papier";
	public static final String utilisateurManquant = "il faut associer un utilisateur";
	public static final String depenseBudgetReversementManquant = "c'est un ordre de reversement il faut associer la depense initiale";

	public static final String imputationsIncoherentes = "pour une ligne de depense il faut une seule imputation comptable";
	public static final String fournisseurIncoherent = "le fournisseur de la depensePapier doit etre le meme que celui de l'engagement";

	public static final String codeAnalytiqueManquant = "pour cette ligne budgetaire il faut associer au moins un code analytique";
	public static final String montantsOrganAnalytiqueIncoherent="pour cette ligne budgetaire il faut repartir entierement le montant parmi les codes analytiques choisis";

	public static final String CONVENTION_MANQUANTE = "pour cette ligne budgetaire il faut associer au moins une convention";

	public static final String depMontantNegatif = "les montants doivent etre positifs";
	public static final String engagementBudgetPasCoherent = "l'engagement doit etre le meme que la depense initiale";
	public static final String depTtcSaisiePasCoherent = "le montant TTC n'est pas coherent avec le HT et la TVA";
	public static final String depMontantBudgetairePasCoherent = "le montant budgetaire n'est pas coherent avec le taux de prorata, le HT et la TVA";
	public static final String engagementBudgetExercicePasCoherent = "il faut que la depense soit sur le meme exercice que l'engagement";
	public static final String depensePapierExercicePasCoherent = "il faut que la depense soit sur le meme exercice que la depense papier";
	public static final String tauxProrataPasCoherent = "il faut que le taux de prorata de la depense soit le meme que l'engagement initial";
	public static final String tauxProrataReversementPasCoherent = "l'ordre de reversement doit avoir le meme prorata que la facture initiale";

	public static final String montantsDepenseControleEmargementIncoherent = "les montants des ecritures sont incoherents par rapport a ceux de la depense";
	public static final String montantsDepenseControleActionIncoherent = "les montants des actions sont incoherents par rapport a ceux de la depense";
	public static final String montantsDepenseControleAnalytiqueIncoherent = "les montants des codes analytiques sont incoherents par rapport a ceux de la depense";
	public static final String montantsDepenseControleConventionIncoherent = "les montants des conventions sont incoherents par rapport a ceux de la depense";
	public static final String montantsDepenseControleHorsMarcheIncoherent = "les montants des codes de nomenclature sont incoherents par rapport a ceux de la depense";
	public static final String montantsDepenseControleMarcheIncoherent = "les montants des attributions sont incoherents par rapport a ceux de la depense";
	public static final String montantsDepenseControlePlanComptableIncoherent = "les montants des imputations sont incoherents par rapport a ceux de la depense";
	public static final String depenseControlePlanComptableUnique = "Il faut qu'une imputation comptable par depense budget";
	public static final String controleMarcheHorsMarcheManquant = "l'engagement doit etre 'marche' ou 'hors marche'";
	public static final String controleMarcheHorsMarcheIncoherent = "l'engagement ne peut etre 'marche' et 'hors marche' a la fois";

	public DepenseBudgetException() {
		super();
	}

	public DepenseBudgetException(String s) {
		super(s);
	}
}
