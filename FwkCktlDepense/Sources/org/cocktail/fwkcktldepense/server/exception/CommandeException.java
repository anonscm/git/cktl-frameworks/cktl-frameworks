/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;


public class CommandeException extends FactoryException {
	public static final String commLibelleManquant = "il faut saisir le libelle de la commande";
	public static final String commReferenceManquant = "il faut saisir la reference de la commande";
	public static final String deviseManquant = "il faut saisir la devise de la commande";
	public static final String exerciceManquant = "il faut saisir l'exercice de la commande";
	public static final String fournisseurManquant = "il faut saisie le fournisseur de la commande";
	public static final String utilisateurManquant = "il faut saisir l'utilisateur qui cree la commande";
	public static final String commNumeroManquant = "il faut que la commande ait un numero";
	public static final String typeEtatManquant = "il faut que la commande ait un etat";
	public static final String typeEtatImprimableManquant = "il faut que la commande ait un etat d'impression";
	public static final String articlesManquant = "il faut que la commande ait des articles";

	public static final String articlesIncoherent = "les articles ne sont pas coherents (typeAchat ou attribution)";
	public static final String htTtcIncoherent = "le TTC de la commande doit etre superieur ou egal au HT";
	
	public static final String typeEtatIncompatible = "cet etat n est pas permis pour une commande";
	public static final String typeEtatIncoherent= "l''etat de la commande est incompatible avec les engagements";

	public static final String typeEtatPreCommandeIncoherent = "l''etat de la commande ''preCommande'' n'est pas correct";
	public static final String typeEtatAnnuleeIncoherent = "l''etat de la commande ''annulee'' n'est pas correct";
	public static final String typeEtatEngageeIncoherent = "l''etat de la commande ''engagee'' n'est pas correct";
	public static final String typeEtatPartiellementEngageeIncoherent = "l''etat de la commande ''partiellement engagee'' n'est pas correct";
	public static final String typeEtatPartiellementSoldeeIncoherent = "l''etat de la commande ''partiellement soldee'' n'est pas correct";
	public static final String typeEtatSoldeeIncoherent = "l''etat de la commande 'soldee' n'est pas correct";
	public static final String commandeAnnuleeAvecEngagements= "une commande annulee ne doit pas avoir d'engagement";
	
	public CommandeException() {
		super();
	}

	public CommandeException(String s) {
		super(s);
	}

}
