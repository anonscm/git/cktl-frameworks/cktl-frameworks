/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;


public class CommandeBudgetException extends FactoryException {
	public static final String cbudHtSaisieManquant = "il faut saisir le montant HT";
	public static final String cbudTvaSaisieManquant = "il faut saisir le montant de la TVA";
	public static final String cbudTtcSaisieManquant = "il faut saisir le montant TTC";
	public static final String cbudMontantBudgetaireManquant = "il faut saisir le montant budgetaire";
	public static final String tauxProrataManquant = "il faut associer un taux de prorata";
	public static final String exerciceManquant = "il faut associer un exercice";
	public static final String organManquant = "il faut associer la ligne budgetaire";
	public static final String typeCreditManquant = "il faut associer le type de credit";
	public static final String commandeManquant = "il faut associer une commande";
	public static final String ligneNonUtilisee = "une repartition sur une source de credit n'est pas utilisee";
	
	public static final String cbudMontantNegatif = "les montants doivent etre positifs";
	public static final String cbudTtcSaisiePasCoherent = "le montant TTC n'est pas coherent avec le HT et la TVA";
	public static final String cbudMontantBudgetairePasCoherent = "le montant budgetaire n'est pas coherent avec le taux de prorata, le HT et la TVA";
	public static final String commandeExercicePasCoherent = "il faut que la commandeBudget soit sur le meme exercice que la commande";

	public static final String montantsCommandeControleActionIncoherent = "les montants des actions sont incoherents par rapport a ceux de la commande";
	public static final String montantsCommandeControleAnalytiqueIncoherent = "les montants des codes analytiques sont incoherents par rapport a ceux de la commande";
	public static final String montantsCommandeControleConventionIncoherent = "les montants des conventions sont incoherents par rapport a ceux de la commande";
	public static final String montantsCommandeControleHorsMarcheIncoherent = "les montants des codes de nomenclature sont incoherents par rapport a ceux de la commande";
	public static final String montantsCommandeControleMarcheIncoherent = "les montants des attributions sont incoherents par rapport a ceux de la commande";
	public static final String montantsCommandeControlePlanComptableIncoherent = "les montants des imputations sont incoherents par rapport a ceux de la commande";
	public static final String pourcentageCommandeControleActionIncoherent = "le total des pourcentages des actions ne sont pas a 100%";
	public static final String pourcentageCommandeControleAnalytiqueIncoherent = "le total des pourcentages des codes analytiques ne sont pas a 100%";
	public static final String pourcentageCommandeControleHorsMarcheIncoherent = "le total des pourcentages des codes de nomenclature ne sont pas a 100%";
	public static final String pourcentageCommandeControleMarcheIncoherent = "le total des pourcentages des attributions ne sont pas a 100%";
	public static final String pourcentageCommandeControlePlanComptableIncoherent = "le total des pourcentages des imputations ne sont pas a 100%";
	public static final String controleMarcheHorsMarcheManquant = "la commande doit etre 'marche' ou 'hors marche'";
	public static final String controleMarcheHorsMarcheIncoherent = "la commande ne peut etre 'marche' et 'hors marche' a la fois";

	public CommandeBudgetException() {
		super();
	}

	public CommandeBudgetException(String s) {
		super(s);
	}
}
