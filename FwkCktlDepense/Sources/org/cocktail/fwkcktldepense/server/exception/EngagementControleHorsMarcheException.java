/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.exception;

public class EngagementControleHorsMarcheException extends FactoryException {
	public static final String ehomHtSaisieManquant = "il faut saisir le montant HT de l'engagement";
	public static final String ehomHtResteManquant = "il faut saisir le montant HT restant de l'engagement";
	public static final String ehomTvaSaisieManquant = "il faut saisir le montant de la TVA de l'engagement";
	public static final String ehomTtcSaisieManquant = "il faut saisir le montant TTC de l'engagement";
	public static final String ehomMontantBudgetaireManquant = "il faut saisir le montant budgetaire de l'engagement";
	public static final String ehomMontantBudgetaireResteManquant = "il faut saisir le montant budgetaire restant de l'engagement";
	public static final String ehomDateSaisieManquant = "il faut saisir la date de saisie de l'engagement";
	public static final String codeExerManquant = "il faut associer une code de nomenclature";
	public static final String typeAchatManquant = "il faut associer un type achat";
	public static final String exerciceManquant = "il faut associer un exercice";
	public static final String engagementBudgetManquant = "il faut associer l'engagement budget correspondant";
	
	public static final String ehomTtcSaisiePasCoherent = "le montant TTC n'est pas coherent avec le HT et la TVA";
	public static final String ehomMontantBudgetairePasCoherent = "le montant budgetaire n'est pas coherent avec le taux de prorata, le HT et la TVA";
	public static final String montantsNegatifs = "les montants de l'engagement doivent etre prositifs";

	public static final String engageBudgetExercicePasCoherent = "il faut que l'exercice soit sur le meme que celui de l'engagement";
	public static final String codeExerNiveauPasCoherent = "il faut un code de nomenclature de niveau 2";
	public static final String codeExerExercicePasCoherent = "il faut un code de nomenclature de meme exercice que l'engagement";
	

	public static final String supprimerEngageControle = "on ne peut pas supprimer cet engagement car il y a des liquidations";

	public EngagementControleHorsMarcheException() {
		super();
	}

	public EngagementControleHorsMarcheException(String s) {
		super(s);
	}
}
