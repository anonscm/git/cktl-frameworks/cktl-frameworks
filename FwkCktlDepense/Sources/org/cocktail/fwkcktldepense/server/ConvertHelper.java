/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server;

import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EORibFournisseur;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Classe pour faciliter les conversions d'objets entre modeles et framework. Normalement devrait à terme disparaitre pour supprimer les doublons
 * entre modeles.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class ConvertHelper {
	public static EORib convertDepenseRibToPersonneRib(EOEditingContext editingContext, EORibFournisseur rib) {
		return EORib.fetchByKeyValue(editingContext, EORib.RIB_ORDRE_KEY, rib.rawPrimaryKey());
	}

	public static EOFournis convertDepenseFournisToPersonneFournis(EOEditingContext editingContext, EOFournisseur fournisseur) {
		return EOFournis.fetchByKeyValue(editingContext, EOFournis.FOU_ORDRE_KEY, fournisseur.rawPrimaryKey());
	}

	public static org.cocktail.fwkcktldepense.server.metier.EOStructure convertPersonneStructureToDepenseStructure(EOEditingContext editingContext, EOStructure structure) {
		return org.cocktail.fwkcktldepense.server.metier.EOStructure.fetchByKeyValue(editingContext, org.cocktail.fwkcktldepense.server.metier.EOStructure.C_STRUCTURE_KEY, structure.rawPrimaryKey());
	}

	public static NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> convertPersonneStructuresToDepenseStructures(EOEditingContext editingContext, NSArray<EOStructure> structures) {
		NSMutableArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> res = new NSMutableArray<org.cocktail.fwkcktldepense.server.metier.EOStructure>();
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		for (int i = 0; i < structures.count(); i++) {
			quals.add(ERXQ.equals(org.cocktail.fwkcktldepense.server.metier.EOStructure.C_STRUCTURE_KEY, structures.objectAtIndex(i).rawPrimaryKey()));
		}
		if (structures.count() > 0) {
			return org.cocktail.fwkcktldepense.server.metier.EOStructure.fetchAll(editingContext, ERXQ.or(quals), new NSArray(new Object[] {
					org.cocktail.fwkcktldepense.server.metier.EOStructure.SORT_LL_STRUCTURE_ASC
			}));
		}
		return NSArray.emptyArray();

	}
}
