package org.cocktail.fwkcktldepense.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FwkCktlDepenseHelper {

	/**
	 * Si exercice de la date est superieur à exercice, renvoie 31/12/exercice, sinon renvoie date.
	 * 
	 * @param date
	 * @param exercice
	 * @return Si exercice de la date est superieur à exercice, renvoie 31/12/exercice, sibnon renvoie date.
	 */
	public final Date corrigeDateSelonExercice(Date date, Integer exercice) {
		if (date == null || exercice == null) {
			return null;
		}

		Integer exerciceFromDate = Integer.valueOf(new SimpleDateFormat("yyyy").format(date));
		if (exerciceFromDate.intValue() > exercice.intValue()) {
			try {
				return new SimpleDateFormat("dd/MM/yyyy").parse("31/12/" + exercice.intValue());
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}
		return date;

	}
}
