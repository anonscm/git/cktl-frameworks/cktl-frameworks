package org.cocktail.fwkcktldepense.server;

import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlpersonne.common.metier.manager.CktlGrhumParamManager;

import er.extensions.eof.ERXEC;

/**
 * a completer
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class FwkCktlDepenseParamManager extends CktlGrhumParamManager {

	public static final String CARAMBOLE_TYPE_FOURNISSEUR_DEFAUT = "org.cocktail.carambole.fournisseur.typeDefaut";

	public FwkCktlDepenseParamManager() {
		super(ERXEC.newEditingContext());
		addParam(CARAMBOLE_TYPE_FOURNISSEUR_DEFAUT, "Type de fournisseur à préselectionner lors de la création des fournisseurs (F: fournisseur, C: Client, T: Tiers)", EOFournis.FOU_TYPE_FOURNISSEUR, EOGrhumParametresType.texteLibre);

		//		getParamList().add(PARAM_RH_ADMIN);
		//		getParamComments().put(PARAM_RH_ADMIN, "C_Structure du groupe RH autorisé à accéder");
		//		getParamDefault().put(PARAM_RH_ADMIN, "0");
		//		getParamTypes().put(PARAM_RH_ADMIN, EOGrhumParametresType.cStructure);
	}

}
