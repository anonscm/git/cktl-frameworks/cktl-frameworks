/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleActionException;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEngagementControleAction extends Factory {

	public FactoryEngagementControleAction() {
		super();
	}

	public FactoryEngagementControleAction(boolean withLog) {
		super(withLog);
	}

	public EOEngagementControleAction creer(EOEditingContext ed, BigDecimal eactHtSaisie, BigDecimal eactTvaSaisie,
			BigDecimal eactTtcSaisie, BigDecimal eactMontantBudgetaire, EOTypeAction typeAction, EOExercice exercice,
			EOEngagementBudget engagementBudget)
			throws EngagementControleActionException {

		//		EOEngagementControleAction newEngage = creer(ed);
		//
		//		setEactHtSaisie(newEngage, eactHtSaisie);
		//		setEactTvaSaisie(newEngage, eactTvaSaisie);
		//		setEactTtcSaisie(newEngage, eactTtcSaisie);
		//		setEactMontantBudgetaire(newEngage, eactMontantBudgetaire);
		//		setEactMontantBudgetaireReste(newEngage, eactMontantBudgetaire);
		//		setEactDateSaisie(newEngage, new NSTimestamp());
		//
		//		affecterTypeAction(newEngage, typeAction);
		//		affecterExercice(newEngage, exercice);
		//		affecterEngagementBudget(newEngage, engagementBudget);
		//
		//		try {
		//			newEngage.validateObjectMetier();
		//			return newEngage;
		//		} catch (EngagementControleActionException e) {
		//			e.printStackTrace();
		//			System.out.println(newEngage);
		//		}
		//
		//		return null;
		return creer(ed, eactHtSaisie, eactTvaSaisie, eactTtcSaisie, eactMontantBudgetaire, null, typeAction, exercice, engagementBudget);
	}

	public EOEngagementControleAction creer(EOEditingContext ed, BigDecimal eactHtSaisie, BigDecimal eactTvaSaisie,
			BigDecimal eactTtcSaisie, BigDecimal eactMontantBudgetaire, BigDecimal eactPourcentage, EOTypeAction typeAction, EOExercice exercice,
			EOEngagementBudget engagementBudget)
			throws EngagementControleActionException {

		EOEngagementControleAction newEngage = creer(ed);

		setEactHtSaisie(newEngage, eactHtSaisie);
		setEactTvaSaisie(newEngage, eactTvaSaisie);
		setEactTtcSaisie(newEngage, eactTtcSaisie);
		setEactMontantBudgetaire(newEngage, eactMontantBudgetaire);
		setEactMontantBudgetaireReste(newEngage, eactMontantBudgetaire);
		setEactDateSaisie(newEngage, new NSTimestamp());

		affecterTypeAction(newEngage, typeAction);
		affecterExercice(newEngage, exercice);
		affecterEngagementBudget(newEngage, engagementBudget);
		if (eactPourcentage != null) {
			setEactPourcentage(newEngage, eactPourcentage);
		}

		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementControleActionException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}

		return null;
	}

	public EOEngagementControleAction creer(EOEditingContext ed) throws EngagementControleActionException {
		EOEngagementControleAction newEngage = (EOEngagementControleAction) Factory.instanceForEntity(ed, EOEngagementControleAction.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}

	public void setEactHtSaisie(EOEngagementControleAction engage, BigDecimal eactHtSaisie) {
		if (engage == null)
			return;
		engage.setEactHtSaisie(eactHtSaisie);
	}

	public void setEactTvaSaisie(EOEngagementControleAction engage, BigDecimal eactTvaSaisie) {
		if (engage == null)
			return;
		engage.setEactTvaSaisie(eactTvaSaisie);
	}

	public void setEactTtcSaisie(EOEngagementControleAction engage, BigDecimal eactTtcSaisie) {
		if (engage == null)
			return;
		engage.setEactTtcSaisie(eactTtcSaisie);
	}

	public void setEactMontantBudgetaire(EOEngagementControleAction engage, BigDecimal eactMontantBudgetaire) {
		if (engage == null)
			return;
		engage.setEactMontantBudgetaire(eactMontantBudgetaire);
	}

	public void setEactMontantBudgetaireReste(EOEngagementControleAction engage, BigDecimal eactMontantBudgetaireReste) {
		if (engage == null)
			return;
		engage.setEactMontantBudgetaireReste(eactMontantBudgetaireReste);
	}

	public void setEactDateSaisie(EOEngagementControleAction engage, NSTimestamp eactDateSaisie) {
		if (engage == null)
			return;
		engage.setEactDateSaisie(eactDateSaisie);
	}

	public void affecterTypeAction(EOEngagementControleAction engage, EOTypeAction typeAction) {
		engage.setTypeActionRelationship(typeAction);
	}

	public void affecterExercice(EOEngagementControleAction engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}

	public void affecterEngagementBudget(EOEngagementControleAction engage, EOEngagementBudget engagementBudget) {
		engage.setEngagementBudgetRelationship(engagementBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementControleAction engage) throws EngagementControleActionException {
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
		}

		engage.setTypeActionRelationship(null);
		engage.setExerciceRelationship(null);
		engage.setEngagementBudgetRelationship(null);

		ed.deleteObject(engage);
	}

	public void verifier(EOEngagementControleAction engage) {
		engage.validateObjectMetier();
	}

	public void setEactPourcentage(EOEngagementControleAction engage, BigDecimal eactPourcentage) {
		if (engage == null)
			return;
		engage.setPourcentage(eactPourcentage);
	}
}
