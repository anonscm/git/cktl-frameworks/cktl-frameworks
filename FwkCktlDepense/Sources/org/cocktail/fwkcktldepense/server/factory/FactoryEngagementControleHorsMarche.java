/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEngagementControleHorsMarche extends Factory {

	public FactoryEngagementControleHorsMarche() {
		super();
	}

	public FactoryEngagementControleHorsMarche(boolean withLog) {
		super(withLog);
	}

	public EOEngagementControleHorsMarche creer(EOEditingContext ed, BigDecimal ehomHtSaisie, BigDecimal ehomTvaSaisie, 
			BigDecimal ehomTtcSaisie, BigDecimal ehomMontantBudgetaire, EOCodeExer codeExer, EOTypeAchat typeAchat, 
			EOExercice exercice, EOEngagementBudget engagementBudget)
	throws EngagementControleHorsMarcheException {
		
		EOEngagementControleHorsMarche newEngage = creer(ed);
		
		setEhomHtSaisie(newEngage, ehomHtSaisie);
		setEhomTvaSaisie(newEngage, ehomTvaSaisie);
		setEhomTtcSaisie(newEngage, ehomTtcSaisie);
		setEhomMontantBudgetaire(newEngage, ehomMontantBudgetaire);
		setEhomMontantBudgetaireReste(newEngage, ehomMontantBudgetaire);
		setEhomMontantHTReste(newEngage, ehomHtSaisie);
		setEhomDateSaisie(newEngage, new NSTimestamp());
		
		affecterCodeExer(newEngage, codeExer);
		affecterTypeAchat(newEngage, typeAchat);
		affecterExercice(newEngage, exercice);
		affecterEngagementBudget(newEngage, engagementBudget);
		
		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementControleHorsMarcheException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}
		
		return null;
	}
	
	public EOEngagementControleHorsMarche creer(EOEditingContext ed) throws EngagementControleHorsMarcheException {
		EOEngagementControleHorsMarche newEngage=(EOEngagementControleHorsMarche)Factory.instanceForEntity(ed,EOEngagementControleHorsMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}
	
	
	public void setEhomHtSaisie(EOEngagementControleHorsMarche engage, BigDecimal ehomHtSaisie) {
		if (engage==null)
			return;
		engage.setEhomHtSaisie(ehomHtSaisie);
	}
	
	public void setEhomTvaSaisie(EOEngagementControleHorsMarche engage, BigDecimal ehomTvaSaisie) {
		if (engage==null)
			return;
		engage.setEhomTvaSaisie(ehomTvaSaisie);
	}
	
	public void setEhomTtcSaisie(EOEngagementControleHorsMarche engage, BigDecimal ehomTtcSaisie) {
		if (engage==null)
			return;
		engage.setEhomTtcSaisie(ehomTtcSaisie);
	}
	
	public void setEhomMontantBudgetaire(EOEngagementControleHorsMarche engage, BigDecimal ehomMontantBudgetaire) {
		if (engage==null)
			return;
		engage.setEhomMontantBudgetaire(ehomMontantBudgetaire);
	}
	
	public void setEhomMontantBudgetaireReste(EOEngagementControleHorsMarche engage, BigDecimal ehomMontantBudgetaireReste) {
		if (engage==null)
			return;
		engage.setEhomMontantBudgetaireReste(ehomMontantBudgetaireReste);
	}

	public void setEhomMontantHTReste(EOEngagementControleHorsMarche engage, BigDecimal ehomHtReste) {
		if (engage==null)
			return;
		engage.setEhomHtReste(ehomHtReste);
	}

	public void setEhomDateSaisie(EOEngagementControleHorsMarche engage, NSTimestamp ehomDateSaisie) {
		if (engage==null)
			return;
		engage.setEhomDateSaisie(ehomDateSaisie);
	}

	public void affecterCodeExer(EOEngagementControleHorsMarche engage, EOCodeExer codeExer) {
		engage.setCodeExerRelationship(codeExer);
	}
	
	public void affecterTypeAchat(EOEngagementControleHorsMarche engage, EOTypeAchat typeAchat) {
		engage.setTypeAchatRelationship(typeAchat);
	}
	
	public void affecterExercice(EOEngagementControleHorsMarche engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}
	
	public void affecterEngagementBudget(EOEngagementControleHorsMarche engage, EOEngagementBudget engagementBudget) {
		engage.setEngagementBudgetRelationship(engagementBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementControleHorsMarche engage) throws EngagementControleHorsMarcheException{
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
		}
		
		engage.setCodeExerRelationship(null);
		engage.setTypeAchatRelationship(null);
		engage.setExerciceRelationship(null);
		engage.setEngagementBudgetRelationship(null);
		
		ed.deleteObject(engage);
	}
	
	public void verifier(EOEngagementControleHorsMarche engage) {
		engage.validateObjectMetier();
	}
}
