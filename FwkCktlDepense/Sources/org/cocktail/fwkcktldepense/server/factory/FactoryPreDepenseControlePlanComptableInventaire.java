/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPreDepenseControlePlanComptableInventaire extends Factory {

	public FactoryPreDepenseControlePlanComptableInventaire() {
		super();
	}

	public FactoryPreDepenseControlePlanComptableInventaire(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseControlePlanComptableInventaire creer(EOEditingContext ed, BigDecimal pdpinMontantBudgetaire,
			EOPreDepenseControlePlanComptable preDepense, EOInventaire inventaire)
	throws DepenseControlePlanComptableException {
		
		EOPreDepenseControlePlanComptableInventaire newDepense = creer(ed);
		
		setPdpinMontantBudgetaire(newDepense, pdpinMontantBudgetaire);

		affecterPreDepenseControlePlanComptable(newDepense, preDepense);
		affecterInventaire(newDepense, inventaire);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControlePlanComptableException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}
	
	public EOPreDepenseControlePlanComptableInventaire creer(EOEditingContext ed) throws DepenseControlePlanComptableException {
		EOPreDepenseControlePlanComptableInventaire newDepense=(EOPreDepenseControlePlanComptableInventaire)Factory.
		 	instanceForEntity(ed,EOPreDepenseControlePlanComptableInventaire.ENTITY_NAME);
		ed.insertObject(newDepense);
		return newDepense;
	}
	
	public void setPdpinMontantBudgetaire(EOPreDepenseControlePlanComptableInventaire depense, BigDecimal pdpinMontantBudgetaire) {
		if (depense==null)
			return;
		depense.setPdpinMontantBudgetaire(pdpinMontantBudgetaire);
	}
	
	public void affecterInventaire(EOPreDepenseControlePlanComptableInventaire depense, EOInventaire inventaire) {
		depense.setInventaireRelationship(inventaire);
	}
	public void affecterPreDepenseControlePlanComptable(EOPreDepenseControlePlanComptableInventaire depense, EOPreDepenseControlePlanComptable preDepense) {
		depense.setPreDepenseControlePlanComptableRelationship(preDepense);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseControlePlanComptableInventaire depense) throws DepenseControlePlanComptableException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/
		
		depense.setInventaireRelationship(null);
		depense.setPreDepenseControlePlanComptableRelationship(null);
		
		ed.deleteObject(depense);
	}
	
	public void verifier(EOPreDepenseControlePlanComptableInventaire depense) {
		depense.validateObjectMetier();
	}
}
