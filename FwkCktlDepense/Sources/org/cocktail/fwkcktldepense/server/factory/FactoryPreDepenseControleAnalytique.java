/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author tsaivre
 */
public class FactoryPreDepenseControleAnalytique extends Factory implements _IFactoryDepenseControleAnalytique {

	public FactoryPreDepenseControleAnalytique() {
		super();
	}

	public FactoryPreDepenseControleAnalytique(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseControleAnalytique copie(EOEditingContext ed, EOPreDepenseControleAnalytique depense, EOPreDepenseBudget budget) {

		EOPreDepenseControleAnalytique newDepense = creer(ed);

		setPdanaHtSaisie(newDepense, depense.danaHtSaisie());
		setPdanaTvaSaisie(newDepense, depense.danaTvaSaisie());
		setPdanaTtcSaisie(newDepense, depense.danaTtcSaisie());
		setPdanaMontantBudgetaire(newDepense, depense.danaMontantBudgetaire());

		affecterCodeAnalytique(newDepense, depense.codeAnalytique());
		affecterExercice(newDepense, depense.exercice());
		affecterPreDepenseBudget(newDepense, budget);

		setPdanaPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EOPreDepenseControleAnalytique creer(EOEditingContext ed, BigDecimal danaHtSaisie, BigDecimal danaTvaSaisie,
			BigDecimal danaTtcSaisie, BigDecimal danaMontantBudgetaire, BigDecimal danaPourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			EOPreDepenseBudget depenseBudget)
			throws DepenseControleAnalytiqueException {

		EOPreDepenseControleAnalytique newDepense = creer(ed);

		setPdanaHtSaisie(newDepense, danaHtSaisie);
		setPdanaTvaSaisie(newDepense, danaTvaSaisie);
		setPdanaTtcSaisie(newDepense, danaTtcSaisie);
		setPdanaMontantBudgetaire(newDepense, danaMontantBudgetaire);

		affecterCodeAnalytique(newDepense, codeAnalytique);
		affecterExercice(newDepense, exercice);
		affecterPreDepenseBudget(newDepense, depenseBudget);

		setPdanaPourcentage(newDepense, danaPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleAnalytiqueException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseControleAnalytique creer(EOEditingContext ed) throws DepenseControleAnalytiqueException {
		EOPreDepenseControleAnalytique newDepense = (EOPreDepenseControleAnalytique) Factory.
				instanceForEntity(ed, EOPreDepenseControleAnalytique.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setPdanaPourcentage(EOPreDepenseControleAnalytique depense, BigDecimal danaPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(danaPourcentage);
	}

	public void setPdanaHtSaisie(EOPreDepenseControleAnalytique depense, BigDecimal danaHtSaisie) {
		if (depense == null)
			return;
		depense.setDanaHtSaisie(danaHtSaisie);
	}

	public void setPdanaTvaSaisie(EOPreDepenseControleAnalytique depense, BigDecimal danaTvaSaisie) {
		if (depense == null)
			return;
		depense.setDanaTvaSaisie(danaTvaSaisie);
	}

	public void setPdanaTtcSaisie(EOPreDepenseControleAnalytique depense, BigDecimal danaTtcSaisie) {
		if (depense == null)
			return;
		depense.setDanaTtcSaisie(danaTtcSaisie);
	}

	public void setPdanaMontantBudgetaire(EOPreDepenseControleAnalytique depense, BigDecimal danaMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDanaMontantBudgetaire(danaMontantBudgetaire);
	}

	public void affecterCodeAnalytique(EOPreDepenseControleAnalytique depense, EOCodeAnalytique codeAnalytique) {
		depense.setCodeAnalytiqueRelationship(codeAnalytique);
	}

	public void affecterExercice(EOPreDepenseControleAnalytique depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterPreDepenseBudget(EOPreDepenseControleAnalytique depense, EOPreDepenseBudget depenseBudget) {
		depense.setPreDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseControleAnalytique depense) throws DepenseControleAnalytiqueException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setCodeAnalytiqueRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setPreDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EOPreDepenseControleAnalytique depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleAnalytique _copie(EOEditingContext ed, _IDepenseControleAnalytique depense, _IDepenseBudget budget) {
		return copie(ed, (EOPreDepenseControleAnalytique) depense, (EOPreDepenseBudget) budget);
	}

	public _IDepenseControleAnalytique _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleAnalytiqueException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, codeAnalytique, exercice, (EOPreDepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControleAnalytique depense, BigDecimal danaPourcentage) {
		setPdanaPourcentage((EOPreDepenseControleAnalytique) depense, danaPourcentage);

	}

	public void _setHtSaisie(_IDepenseControleAnalytique depense, BigDecimal htSaisie) {
		setPdanaHtSaisie((EOPreDepenseControleAnalytique) depense, htSaisie);

	}

	public void _setTvaSaisie(_IDepenseControleAnalytique depense, BigDecimal tvaSaisie) {
		setPdanaTvaSaisie((EOPreDepenseControleAnalytique) depense, tvaSaisie);
	}

	public void _setTtcSaisie(_IDepenseControleAnalytique depense, BigDecimal ttcSaisie) {
		setPdanaTtcSaisie((EOPreDepenseControleAnalytique) depense, ttcSaisie);

	}

	public void _setMontantBudgetaire(_IDepenseControleAnalytique depense, BigDecimal montantBudgetaire) {
		setPdanaMontantBudgetaire((EOPreDepenseControleAnalytique) depense, montantBudgetaire);

	}

	public void _affecterCodeAnalytique(_IDepenseControleAnalytique depense, EOCodeAnalytique codeAnalytique) {
		affecterCodeAnalytique((EOPreDepenseControleAnalytique) depense, codeAnalytique);

	}

	public void _affecterExercice(_IDepenseControleAnalytique depense, EOExercice exercice) {
		affecterExercice((EOPreDepenseControleAnalytique) depense, exercice);

	}

	public void _affecterDepenseBudget(_IDepenseControleAnalytique depense, _IDepenseBudget depenseBudget) {
		affecterPreDepenseBudget((EOPreDepenseControleAnalytique) depense, (EOPreDepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControleAnalytique depense) throws DepenseControleAnalytiqueException {
		supprimer(ed, (EOPreDepenseControleAnalytique) depense);

	}

	public void _verifier(_IDepenseControleAnalytique depense) {
		verifier((EOPreDepenseControleAnalytique) depense);

	}
}
