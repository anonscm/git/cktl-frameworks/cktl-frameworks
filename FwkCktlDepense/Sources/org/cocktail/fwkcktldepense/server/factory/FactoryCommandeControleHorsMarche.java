/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleActionException;
import org.cocktail.fwkcktldepense.server.exception.CommandeControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryCommandeControleHorsMarche extends Factory {

	public FactoryCommandeControleHorsMarche() {
		super();
	}

	public FactoryCommandeControleHorsMarche(boolean withLog) {
		super(withLog);
	}

	protected NSArray dupliquer(EOEditingContext ed, EOCommandeBudget origine, EOCommandeBudget commandeBudget) throws CommandeControleActionException {
		NSMutableArray array = new NSMutableArray();
		NSArray ctrls = commandeBudget.commandeControleHorsMarches();
		boolean creation = true;

		for (int i = 0; i < origine.commandeControleHorsMarches().count(); i++) {
			EOCommandeControleHorsMarche controle = (EOCommandeControleHorsMarche) origine.commandeControleHorsMarches().objectAtIndex(i);
			creation = true;

			if (ctrls != null && ctrls.count() > 0) {
				NSMutableArray arrayQuals = new NSMutableArray();
				arrayQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.CODE_EXER_KEY + "=%@",
						new NSArray(controle.codeExer())));
				arrayQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleHorsMarche.TYPE_ACHAT_KEY + "=%@",
						new NSArray(controle.typeAchat())));

				NSArray result = EOQualifier.filteredArrayWithQualifier(ctrls, new EOAndQualifier(arrayQuals));
				if (result.count() > 0) {
					creation = false;
				}
			}

			if (creation)
				array.addObject(creer(ed, controle.chomHtSaisie(), controle.chomTvaSaisie(), controle.chomTtcSaisie(),
						controle.chomMontantBudgetaire(), FinderCodeExer.getCodeExerEquivalentPourExercice(ed, controle.codeExer(), commandeBudget.exercice()),
						controle.typeAchat(), commandeBudget.exercice(), commandeBudget));
		}

		return array;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, NSArray controles) {
		for (int i = 0; i < controles.count(); i++) {
			EOCommandeControleHorsMarche controle = (EOCommandeControleHorsMarche) controles.objectAtIndex(i);
			if (!verifier(exercice, utilisateur, organ, typeCredit, controle))
				return false;
		}
		return true;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOCommandeControleHorsMarche controle) {
		EOCodeExer codeExer = controle.codeExer();
		if (!exercice.exeExercice().equals(controle.exercice().exeExercice()))
			codeExer = FinderCodeExer.getCodeExerEquivalentPourExercice(controle.editingContext(), controle.codeExer(), exercice);
		return verifier(exercice, utilisateur, organ, typeCredit, codeExer, controle.typeAchat());
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOCodeExer codeExer, EOTypeAchat typeAchat) {
		if (exercice == null || utilisateur == null || organ == null || typeCredit == null || codeExer == null || typeAchat == null)
			return false;
		return true;
	}

	public EOCommandeControleHorsMarche creer(EOEditingContext ed, BigDecimal chomHtSaisie, BigDecimal chomTvaSaisie,
			BigDecimal chomTtcSaisie, BigDecimal chomMontantBudgetaire,
			EOCodeExer codeExer, EOTypeAchat typeAchat, EOExercice exercice, EOCommandeBudget commandeBudget)
			throws CommandeControleHorsMarcheException {

		EOCommandeControleHorsMarche newCommande = creer(ed);

		setChomHtSaisie(newCommande, chomHtSaisie);
		setChomTvaSaisie(newCommande, chomTvaSaisie);
		setChomTtcSaisie(newCommande, chomTtcSaisie);
		setChomMontantBudgetaire(newCommande, chomMontantBudgetaire);

		affecterCodeExer(newCommande, codeExer);
		affecterTypeAchat(newCommande, typeAchat);
		affecterExercice(newCommande, exercice);
		affecterCommandeBudget(newCommande, commandeBudget);

		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeControleHorsMarcheException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}

		return null;
	}

	public EOCommandeControleHorsMarche creer(EOEditingContext ed) throws CommandeControleHorsMarcheException {
		EOCommandeControleHorsMarche newCommande = (EOCommandeControleHorsMarche) Factory.instanceForEntity(ed, EOCommandeControleHorsMarche.ENTITY_NAME);
		ed.insertObject(newCommande);
		return newCommande;
	}

	public void setChomHtSaisie(EOCommandeControleHorsMarche commande, BigDecimal chomHtSaisie) {
		if (commande == null)
			return;
		commande.setChomHtSaisie(chomHtSaisie);
	}

	public void setChomTvaSaisie(EOCommandeControleHorsMarche commande, BigDecimal chomTvaSaisie) {
		if (commande == null)
			return;
		commande.setChomTvaSaisie(chomTvaSaisie);
	}

	public void setChomTtcSaisie(EOCommandeControleHorsMarche commande, BigDecimal chomTtcSaisie) {
		if (commande == null)
			return;
		commande.setChomTtcSaisie(chomTtcSaisie);
	}

	public void setChomMontantBudgetaire(EOCommandeControleHorsMarche commande, BigDecimal chomMontantBudgetaire) {
		if (commande == null)
			return;
		commande.setChomMontantBudgetaire(chomMontantBudgetaire);
	}

	public void affecterCodeExer(EOCommandeControleHorsMarche commande, EOCodeExer codeExer) {
		commande.setCodeExerRelationship(codeExer);
	}

	public void affecterTypeAchat(EOCommandeControleHorsMarche commande, EOTypeAchat typeAchat) {
		commande.setTypeAchatRelationship(typeAchat);
	}

	public void affecterExercice(EOCommandeControleHorsMarche commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
	}

	public void affecterCommandeBudget(EOCommandeControleHorsMarche commande, EOCommandeBudget commandeBudget) {
		commande.setCommandeBudgetRelationship(commandeBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeControleHorsMarche commande) throws CommandeControleHorsMarcheException {
		EOCommandeBudget commandeBudget = commande.commandeBudget();

		commande.setCodeExerRelationship(null);
		//Rod 25/11/09 : modif pour coller aux templates velocity
		//		commande.setTypeAchat(null);
		commande.setTypeAchatRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setCommandeBudgetRelationship(null);

		ed.deleteObject(commande);
		if (commandeBudget != null)
			commandeBudget.mettreAJourMontants();
	}

	public void verifier(EOCommandeControleHorsMarche commande) {
		commande.validateObjectMetier();
	}
}
