/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryDepenseControleHorsMarche extends Factory implements _IFactoryDepenseControleHorsMarche {

	public FactoryDepenseControleHorsMarche() {
		super();
	}

	public FactoryDepenseControleHorsMarche(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleHorsMarche copie(EOEditingContext ed, EODepenseControleHorsMarche depense, EODepenseBudget budget) {

		EODepenseControleHorsMarche newDepense = _creer(ed);

		setDhomHtSaisie(newDepense, depense.dhomHtSaisie());
		setDhomTvaSaisie(newDepense, depense.dhomTvaSaisie());
		setDhomTtcSaisie(newDepense, depense.dhomTtcSaisie());
		setDhomMontantBudgetaire(newDepense, depense.dhomMontantBudgetaire());

		_affecterTypeAchat(newDepense, depense.typeAchat());
		_affecterCodeExer(newDepense, depense.codeExer());
		affecterExercice(newDepense, depense.exercice());
		affecterDepenseBudget(newDepense, budget);

		setDhomPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EODepenseControleHorsMarche creer(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie,
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat,
			EOExercice exercice, EODepenseBudget depenseBudget)
			throws DepenseControleHorsMarcheException {

		EODepenseControleHorsMarche newDepense = _creer(ed);

		setDhomHtSaisie(newDepense, dhomHtSaisie);
		setDhomTvaSaisie(newDepense, dhomTvaSaisie);
		setDhomTtcSaisie(newDepense, dhomTtcSaisie);
		setDhomMontantBudgetaire(newDepense, dhomMontantBudgetaire);

		_affecterCodeExer(newDepense, codeExer);
		affecterExercice(newDepense, exercice);
		_affecterTypeAchat(newDepense, typeAchat);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDhomPourcentage(newDepense, dhomPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleHorsMarcheException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EODepenseControleHorsMarche _creer(EOEditingContext ed) throws DepenseControleHorsMarcheException {
		EODepenseControleHorsMarche newDepense = (EODepenseControleHorsMarche) Factory.
				instanceForEntity(ed, EODepenseControleHorsMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setDhomPourcentage(EODepenseControleHorsMarche depense, BigDecimal dhomPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dhomPourcentage);
	}

	public void setDhomHtSaisie(EODepenseControleHorsMarche depense, BigDecimal dhomHtSaisie) {
		if (depense == null)
			return;
		depense.setDhomHtSaisie(dhomHtSaisie);
	}

	public void setDhomTvaSaisie(EODepenseControleHorsMarche depense, BigDecimal dhomTvaSaisie) {
		if (depense == null)
			return;
		depense.setDhomTvaSaisie(dhomTvaSaisie);
	}

	public void setDhomTtcSaisie(EODepenseControleHorsMarche depense, BigDecimal dhomTtcSaisie) {
		if (depense == null)
			return;
		depense.setDhomTtcSaisie(dhomTtcSaisie);
	}

	public void setDhomMontantBudgetaire(EODepenseControleHorsMarche depense, BigDecimal dhomMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDhomMontantBudgetaire(dhomMontantBudgetaire);
	}

	public void affecterExercice(EODepenseControleHorsMarche depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterDepenseBudget(EODepenseControleHorsMarche depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}

	public void affecterCodeExer(EODepenseControleHorsMarche depense, EOCodeExer codeExer) {
		depense.setCodeExerRelationship(codeExer);
	}

	public void affecterTypeAchat(EODepenseControleHorsMarche depense, EOTypeAchat typeAchat) {
		depense.setTypeAchatRelationship(typeAchat);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setCodeExerRelationship(null);
		depense.setTypeAchatRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EODepenseControleHorsMarche depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleHorsMarche _copie(EOEditingContext ed, _IDepenseControleHorsMarche depense, _IDepenseBudget budget) {
		return copie(ed, (EODepenseControleHorsMarche) depense, (EODepenseBudget) budget);
	}

	public _IDepenseControleHorsMarche _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleHorsMarcheException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, codeExer, typeAchat, exercice, (EODepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControleHorsMarche depense, BigDecimal dactPourcentage) {
		setDhomPourcentage((EODepenseControleHorsMarche) depense, dactPourcentage);
	}

	public void setHtSaisie(_IDepenseControleHorsMarche depense, BigDecimal htSaisie) {
		setDhomHtSaisie((EODepenseControleHorsMarche) depense, htSaisie);

	}

	public void setTvaSaisie(_IDepenseControleHorsMarche depense, BigDecimal tvaSaisie) {
		setDhomTvaSaisie((EODepenseControleHorsMarche) depense, tvaSaisie);

	}

	public void setTtcSaisie(_IDepenseControleHorsMarche depense, BigDecimal ttcSaisie) {
		setDhomTtcSaisie((EODepenseControleHorsMarche) depense, ttcSaisie);

	}

	public void setMontantBudgetaire(_IDepenseControleHorsMarche depense, BigDecimal montantBudgetaire) {
		setDhomMontantBudgetaire((EODepenseControleHorsMarche) depense, montantBudgetaire);

	}

	public void _affecterExercice(_IDepenseControleHorsMarche depense, EOExercice exercice) {
		affecterExercice((EODepenseControleHorsMarche) depense, exercice);
	}

	public void _affecterDepenseBudget(_IDepenseControleHorsMarche depense, _IDepenseBudget depenseBudget) {
		affecterDepenseBudget((EODepenseControleHorsMarche) depense, (EODepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException {
		supprimer(ed, (EODepenseControleHorsMarche) depense);

	}

	public void _verifier(_IDepenseControleHorsMarche depense) {
		verifier((EODepenseControleHorsMarche) depense);

	}

	public void _affecterCodeExer(_IDepenseControleHorsMarche depense, EOCodeExer codeExer) {
		affecterCodeExer((EODepenseControleHorsMarche) depense, codeExer);

	}

	public void _affecterTypeAchat(_IDepenseControleHorsMarche depense, EOTypeAchat typeAchat) {
		affecterTypeAchat((EODepenseControleHorsMarche) depense, typeAchat);

	}
}
