/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepensePapierException;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.server.metier.EORibFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryReversementPapier extends Factory {

	public FactoryReversementPapier() {
		super();
	}

	public FactoryReversementPapier(boolean withLog) {
		super(withLog);
	}

	public EODepensePapier creer(EOEditingContext ed, BigDecimal dppHtInitial, BigDecimal dppTvaInitial, 
			BigDecimal dppTtcInitial, String dppNumeroFacture, EOExercice exercice, EOUtilisateur utilisateur,
			EODepenseBudget depenseBudget)
	throws DepensePapierException {
		
		EODepensePapier newDepense = creer(ed);
		
		setDppHtSaisie(newDepense, new BigDecimal(0.0));
		setDppTvaSaisie(newDepense, new BigDecimal(0.0));
		setDppTtcSaisie(newDepense, new BigDecimal(0.0));
		setDppHtInitial(newDepense, dppHtInitial);
		setDppTvaInitial(newDepense, dppTvaInitial);
		setDppTtcInitial(newDepense, dppTtcInitial);
		setDppNumeroFacture(newDepense, dppNumeroFacture);
		setDppDateServiceFait(newDepense, new NSTimestamp());
		setDppDateSaisie(newDepense, new NSTimestamp());
		setDppDateReception(newDepense, new NSTimestamp());
		setDppDateFacture(newDepense, new NSTimestamp());
		
		newDepense.setDppNbPiece(new Integer(1));

		affecterExercice(newDepense, exercice);
		affecterUtilisateur(newDepense, utilisateur);
		affecterFournisseur(newDepense, depenseBudget.engagementBudget().fournisseur());
		affecterDepensePapierReversement(newDepense, depenseBudget.depensePapier());
		affecterModePaiement(newDepense, depenseBudget.depensePapier().modePaiement());
		
		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepensePapierException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}
	
	public EODepensePapier creer(EOEditingContext ed) throws DepensePapierException {
		EODepensePapier newDepense=(EODepensePapier)Factory.instanceForEntity(ed,EODepensePapier.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}
	
	// TODO new Manu : normalement ne s'en sert plus
	public EODepensePapier creer(EOEditingContext ed,EODepenseBudget depenseBudget, EOUtilisateur utilisateur) throws DepensePapierException {
		EODepensePapier newDepense = creer(ed);
		
		setDppHtSaisie(newDepense, new BigDecimal(0.0));
		setDppTvaSaisie(newDepense, new BigDecimal(0.0));
		setDppTtcSaisie(newDepense, new BigDecimal(0.0));
		setDppDateServiceFait(newDepense, new NSTimestamp());
		setDppDateSaisie(newDepense, new NSTimestamp());
		setDppDateReception(newDepense, new NSTimestamp());
		setDppDateFacture(newDepense, new NSTimestamp());
		newDepense.setDppNbPiece(new Integer(1));
		affecterUtilisateur(newDepense, utilisateur);

		setDppHtInitial(newDepense, depenseBudget.depHtSaisie());
		setDppTtcInitial(newDepense, depenseBudget.depTtcSaisie());
		affecterExercice(newDepense, depenseBudget.exercice());
		affecterUtilisateur(newDepense, utilisateur);
		affecterFournisseur(newDepense, depenseBudget.engagementBudget().fournisseur());
		affecterDepensePapierReversement(newDepense, depenseBudget.depensePapier());

		affecterDepensePapierReversement(newDepense, depenseBudget.depensePapier());
		affecterModePaiement(newDepense, depenseBudget.depensePapier().modePaiement());
		
		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepensePapierException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		return newDepense;
	}
	
	
	public void setDppHtSaisie(EODepensePapier depense, BigDecimal dppHtSaisie) {
		if (depense==null)
			return;
		depense.setDppHtSaisie(dppHtSaisie);
	}
	
	public void setDppTvaSaisie(EODepensePapier depense, BigDecimal dppTvaSaisie) {
		if (depense==null)
			return;
		depense.setDppTvaSaisie(dppTvaSaisie);
	}
	
	public void setDppTtcSaisie(EODepensePapier depense, BigDecimal dppTtcSaisie) {
		if (depense==null)
			return;
		depense.setDppTtcSaisie(dppTtcSaisie);
	}

	public void setDppHtInitial(EODepensePapier depense, BigDecimal dppHtInitial) {
		if (depense==null)
			return;
		depense.setDppHtInitial(dppHtInitial);
	}
	
	public void setDppTvaInitial(EODepensePapier depense, BigDecimal dppTvaInitial) {
		if (depense==null)
			return;
		depense.setDppTvaInitial(dppTvaInitial);
	}
	
	public void setDppTtcInitial(EODepensePapier depense, BigDecimal dppTtcInitial) {
		if (depense==null)
			return;
		depense.setDppTtcInitial(dppTtcInitial);
	}

	public void setDppNumeroFacture(EODepensePapier depense, String dppNumeroFacture) {
		if (depense==null)
			return;
		depense.setDppNumeroFacture(dppNumeroFacture);
	}
	
	public void setDppDateServiceFait(EODepensePapier depense, NSTimestamp dppDateServiceFait) {
		if (depense==null)
			return;
		depense.setDppDateServiceFait(dppDateServiceFait);
	}

	public void setDppDateSaisie(EODepensePapier depense, NSTimestamp dppDateSaisie) {
		if (depense==null)
			return;
		depense.setDppDateSaisie(dppDateSaisie);
	}

	public void setDppDateReception(EODepensePapier depense, NSTimestamp dppDateReception) {
		if (depense==null)
			return;
		depense.setDppDateReception(dppDateReception);
	}

	public void setDppDateFacture(EODepensePapier depense, NSTimestamp dppDateFacture) {
		if (depense==null)
			return;
		depense.setDppDateFacture(dppDateFacture);
	}

	public void affecterUtilisateur(EODepensePapier depense, EOUtilisateur utilisateur) {
		depense.setUtilisateurRelationship(utilisateur);
	}
	
	public void affecterCommande(EODepensePapier depense, EOCommande commande) {
		depense.setCommande(commande, depense.utilisateur());
	}

	public void affecterDepensePapierReversement(EODepensePapier depense, EODepensePapier depensePapier) {
		depense.setDepensePapierReversementRelationship(depensePapier);
	}
	
	public void affecterExercice(EODepensePapier depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterModePaiement(EODepensePapier depense, EOModePaiement modePaiement) {
		depense.setModePaiementRelationship(modePaiement);
	}
	
	public void affecterRib(EODepensePapier depense, EORibFournisseur rib) {
		depense.setRibFournisseurRelationship(rib);
	}

	public void affecterFournisseur(EODepensePapier depense, EOFournisseur fournisseur) {
		depense.setFournisseurRelationship(fournisseur);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepensePapier depense) throws DepensePapierException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/
		
		supprimerDepenseBudgets(depense);
		
		depense.setUtilisateurRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setModePaiementRelationship(null);
		depense.setFournisseurRelationship(null);
		depense.setRibFournisseurRelationship(null);
		depense.setDepensePapierReversementRelationship(null);
		
		ed.deleteObject(depense);
	}
	
	public void supprimerDepenseBudgets(EODepensePapier depense) {
    	NSArray larray=depense.depenseBudgets();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseBudget factory=new FactoryDepenseBudget();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseBudget)larray.objectAtIndex(i));
    	}
	}
	
	public void verifier(EODepensePapier depense) {
		depense.validateObjectMetier();
	}
}
