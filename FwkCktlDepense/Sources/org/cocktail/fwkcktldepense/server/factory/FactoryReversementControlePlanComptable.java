/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryReversementControlePlanComptable extends Factory {

	public FactoryReversementControlePlanComptable() {
		super();
	}

	public FactoryReversementControlePlanComptable(boolean withLog) {
		super(withLog);
	}

	public EODepenseControlePlanComptable creer(EOEditingContext ed, BigDecimal dpcoHtSaisie, BigDecimal dpcoTvaSaisie, 
			BigDecimal dpcoTtcSaisie, BigDecimal dpcoMontantBudgetaire, BigDecimal dpcoPourcentage, EOPlanComptable planComptable, EOExercice exercice,
			EODepenseBudget depenseBudget)
	throws DepenseControlePlanComptableException {
		
		EODepenseControlePlanComptable newDepense = creer(ed);
		
		setDpcoHtSaisie(newDepense, dpcoHtSaisie);
		setDpcoTvaSaisie(newDepense, dpcoTvaSaisie);
		setDpcoTtcSaisie(newDepense, dpcoTtcSaisie);
		setDpcoMontantBudgetaire(newDepense, dpcoMontantBudgetaire);

		affecterPlanComptable(newDepense, planComptable);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDpcoPourcentage(newDepense, dpcoPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControlePlanComptableException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}
	
	public EODepenseControlePlanComptable creer(EOEditingContext ed) throws DepenseControlePlanComptableException {
		EODepenseControlePlanComptable newDepense=(EODepenseControlePlanComptable)Factory.
		 	instanceForEntity(ed,EODepenseControlePlanComptable.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}
	
	public void setDpcoPourcentage(EODepenseControlePlanComptable depense, BigDecimal dpcoPourcentage) {
		if (depense==null)
			return;
		depense.setPourcentage(dpcoPourcentage);
	}
	
	public void setDpcoHtSaisie(EODepenseControlePlanComptable depense, BigDecimal dpcoHtSaisie) {
		if (depense==null)
			return;
		depense.setDpcoHtSaisie(dpcoHtSaisie);
	}
	
	public void setDpcoTvaSaisie(EODepenseControlePlanComptable depense, BigDecimal dpcoTvaSaisie) {
		if (depense==null)
			return;
		depense.setDpcoTvaSaisie(dpcoTvaSaisie);
	}
	
	public void setDpcoTtcSaisie(EODepenseControlePlanComptable depense, BigDecimal dpcoTtcSaisie) {
		if (depense==null)
			return;
		depense.setDpcoTtcSaisie(dpcoTtcSaisie);
	}
	
	public void setDpcoMontantBudgetaire(EODepenseControlePlanComptable depense, BigDecimal dpcoMontantBudgetaire) {
		if (depense==null)
			return;
		depense.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
	}
	
	public void affecterPlanComptable(EODepenseControlePlanComptable depense, EOPlanComptable planComptable) {
		depense.setPlanComptableRelationship(planComptable);
	}
	
	public void affecterExercice(EODepenseControlePlanComptable depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}
	
	public void affecterDepenseBudget(EODepenseControlePlanComptable depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}

	public void ajouterInventaires(EODepenseControlePlanComptable depense, NSArray inventaires) {
		if (inventaires==null || inventaires.count()==0)
			return;
		for (int i=0; i<inventaires.count(); i++)
			ajouterInventaire(depense, (EOInventaire)inventaires.objectAtIndex(i));
	}

	public void ajouterInventaire(EODepenseControlePlanComptable depense, EOInventaire inventaire) {
		depense.ajouterInventaire(inventaire);
	}
	
	public void supprimerInventaire(EODepenseControlePlanComptable depense, EOInventaire inventaire) {
		depense.supprimerInventaire(inventaire);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControlePlanComptable depense) throws DepenseControlePlanComptableException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/
		
		depense.setPlanComptableRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);
		
		ed.deleteObject(depense);
	}
	
	public void verifier(EODepenseControlePlanComptable depense) {
		depense.validateObjectMetier();
	}
}
