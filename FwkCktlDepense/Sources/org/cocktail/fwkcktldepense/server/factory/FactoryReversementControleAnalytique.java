/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author tsaivre
 *
 */
public class FactoryReversementControleAnalytique extends Factory {

	public FactoryReversementControleAnalytique() {
		super();
	}

	public FactoryReversementControleAnalytique(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleAnalytique creer(EOEditingContext ed, BigDecimal danaHtSaisie, BigDecimal danaTvaSaisie, 
			BigDecimal danaTtcSaisie, BigDecimal danaMontantBudgetaire, BigDecimal danaPourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			EODepenseBudget depenseBudget)
	throws DepenseControleAnalytiqueException {
		
		EODepenseControleAnalytique newDepense = creer(ed);
		
		setDanaHtSaisie(newDepense, danaHtSaisie);
		setDanaTvaSaisie(newDepense, danaTvaSaisie);
		setDanaTtcSaisie(newDepense, danaTtcSaisie);
		setDanaMontantBudgetaire(newDepense, danaMontantBudgetaire);

		affecterCodeAnalytique(newDepense, codeAnalytique);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDanaPourcentage(newDepense, danaPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleAnalytiqueException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}
	
	public EODepenseControleAnalytique creer(EOEditingContext ed) throws DepenseControleAnalytiqueException {
		EODepenseControleAnalytique newDepense=(EODepenseControleAnalytique)Factory.
		 	instanceForEntity(ed,EODepenseControleAnalytique.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}
	
	public void setDanaPourcentage(EODepenseControleAnalytique depense, BigDecimal danaPourcentage) {
		if (depense==null)
			return;
		depense.setPourcentage(danaPourcentage);
	}

	public void setDanaHtSaisie(EODepenseControleAnalytique depense, BigDecimal danaHtSaisie) {
		if (depense==null)
			return;
		depense.setDanaHtSaisie(danaHtSaisie);
	}
	
	public void setDanaTvaSaisie(EODepenseControleAnalytique depense, BigDecimal danaTvaSaisie) {
		if (depense==null)
			return;
		depense.setDanaTvaSaisie(danaTvaSaisie);
	}
	
	public void setDanaTtcSaisie(EODepenseControleAnalytique depense, BigDecimal danaTtcSaisie) {
		if (depense==null)
			return;
		depense.setDanaTtcSaisie(danaTtcSaisie);
	}
	
	public void setDanaMontantBudgetaire(EODepenseControleAnalytique depense, BigDecimal danaMontantBudgetaire) {
		if (depense==null)
			return;
		depense.setDanaMontantBudgetaire(danaMontantBudgetaire);
	}
	
	public void affecterCodeAnalytique(EODepenseControleAnalytique depense, EOCodeAnalytique codeAnalytique) {
		depense.setCodeAnalytiqueRelationship(codeAnalytique);
	}
	
	public void affecterExercice(EODepenseControleAnalytique depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}
	
	public void affecterDepenseBudget(EODepenseControleAnalytique depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleAnalytique depense) throws DepenseControleAnalytiqueException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/

		depense.setCodeAnalytiqueRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);
		
		ed.deleteObject(depense);
	}
	
	public void verifier(EODepenseControleAnalytique depense) {
		depense.validateObjectMetier();
	}
}
