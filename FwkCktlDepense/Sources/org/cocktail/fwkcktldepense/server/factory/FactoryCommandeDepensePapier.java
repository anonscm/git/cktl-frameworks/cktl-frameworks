/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeDepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCommandeDepensePapier extends Factory {

	public FactoryCommandeDepensePapier() {
		super();
	}

	public FactoryCommandeDepensePapier(boolean withLog) {
		super(withLog);
	}

	public EOCommandeDepensePapier creer(EOEditingContext ed, EOCommande commande, EODepensePapier depensePapier) {
		EOCommandeDepensePapier newObject = creer(ed);
		affecterCommande(newObject, commande);
		affecterDepensePapier(newObject, depensePapier);

		return newObject;
	}

	public EOCommandeDepensePapier creer(EOEditingContext ed) {
		EOCommandeDepensePapier newObject=(EOCommandeDepensePapier)Factory.instanceForEntity(ed,EOCommandeDepensePapier.ENTITY_NAME);
		ed.insertObject(newObject);
		return newObject;
	}

	public void affecterCommande(EOCommandeDepensePapier cde, EOCommande commande) {
		cde.setCommandeRelationship(commande);
	}

	public void affecterDepensePapier(EOCommandeDepensePapier cde, EODepensePapier depensePapier) {
		cde.setDepensePapierRelationship(depensePapier);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeDepensePapier cde) {
		cde.setCommandeRelationship(null);
		cde.setDepensePapierRelationship(null);

		ed.deleteObject(cde);
	}
}
