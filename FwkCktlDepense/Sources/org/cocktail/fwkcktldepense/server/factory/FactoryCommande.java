/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLBuyerCookie;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLMessageFactory;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOCxml;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn;
import org.cocktail.fwkcktldepense.server.b2b.factory.FactoryCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.b2b.procedure.B2bCxmlCommandeException;
import org.cocktail.fwkcktldepense.server.exception.CommandeException;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderDevise;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderFournisseur;
import org.cocktail.fwkcktldepense.server.finder.FinderTva;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeAchat;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeApplication;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeEtat;
import org.cocktail.fwkcktldepense.server.finder.FinderUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogue;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODevise;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle;
import org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryCommande extends Factory {

	public FactoryCommande() {
		super();
	}

	public FactoryCommande(boolean withLog) {
		super(withLog);
	}

	public EOCommande ajouterSource(EOEditingContext ed, EOCommande commande, EOSource source) {
		return ajouterSource(ed, commande, source.exercice(), source.organ(), source.typeCredit(), source.tauxProrata());
	}

	public EOCommande ajouterSourceEtConvention(EOEditingContext ed, EOCommande commande, EOSource source, EOConvention convention) {
		return ajouterSourceEtConvention(ed, commande, source.exercice(), source.organ(), source.typeCredit(), source.tauxProrata(), convention);
	}

	/**
	 * Ajoute une source de financement a la commande si celle ci n'est pas deja existante <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande a laquelle on associe la source
	 * @param source source de financement a relier
	 */
	public EOCommande ajouterSource(EOEditingContext ed, EOCommande commande, EOBudgetExecCredit source, EOTauxProrata taux) {
		return ajouterSource(ed, commande, source.exercice(), source.organ(), source.typeCredit(), taux);
	}

	public EOCommande ajouterSourceEtConvention(EOEditingContext ed, EOCommande commande, EOBudgetExecCredit source, EOTauxProrata taux, EOConvention convention) {
		return ajouterSourceEtConvention(ed, commande, source.exercice(), source.organ(), source.typeCredit(), taux, convention);
	}

	public EOCommande ajouterSource(EOEditingContext ed, EOCommande commande, EOExercice exercice, EOOrgan organ,
			EOTypeCredit typeCredit, EOTauxProrata taux) {
		return ajouterSourceEtConvention(ed, commande, exercice, organ, typeCredit, taux, null);
	}

	public EOCommande ajouterSourceEtConvention(EOEditingContext ed, EOCommande commande, EOExercice exercice, EOOrgan organ,
			EOTypeCredit typeCredit, EOTauxProrata taux, EOConvention convention) {
		if (commande == null)
			throw new FactoryException("il faut une commande");
		if (organ == null)
			throw new FactoryException("il faut une ligne budgetaire");
		if (typeCredit == null)
			throw new FactoryException("il faut un type de credit");
		if (taux == null)
			throw new FactoryException("il faut un taux de prorata");
		if (commande.exercice() == null)
			throw new FactoryException("il faut associer un exercice a la commande");

		// verification de la coherence de l'exercice entre la commande et la source
		if (!commande.exercice().equals(exercice))
			throw new FactoryException("l'exercice de la commande et celui de la source ne sont pas les memes");

		// verification que la source n'est pas deja presente
		// Recherche pour savoir si source pas deja presente sur la commande
		EOCommandeBudget commandeBudget = commande.containsSource(organ, typeCredit);
		if (commandeBudget == null) {
			FactoryCommandeBudget factoryCommandeBudget = new FactoryCommandeBudget();
			// creation de la commandeBudget
			commandeBudget = factoryCommandeBudget.creer(ed, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0),
					new BigDecimal(0.0), organ, taux, typeCredit, commande.exercice(), commande);
			if (convention != null)
				(new FactoryCommandeControleConvention()).creer(ed, commandeBudget.cbudHtSaisie(), commandeBudget.cbudTvaSaisie(), commandeBudget.cbudTtcSaisie(),
						commandeBudget.cbudMontantBudgetaire(), new BigDecimal(100.0), convention, exercice, commandeBudget);
			//commande.creerCommandeCtrlAPartirTableaux(commandeBudget);
		}

		return commande;
	}

	/**
	 * Supprime une source de financement de la commande <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande pour laquelle on supprime la source
	 * @param source source de financement a supprimer
	 */
	public void supprimerSource(EOEditingContext ed, EOCommande commande, EOCommandeBudget source) {
		FactoryCommandeBudget factoryCommandeBudget = new FactoryCommandeBudget();

		if (commande == null)
			throw new FactoryException("il faut une commande");
		if (source == null)
			throw new FactoryException("il faut une source de financement");

		// suppression de la commandeBudget
		factoryCommandeBudget.supprimer(ed, source);
		// TODO EGE: A tester avec delete cascade sur la relation commandeBudgets
		// commande.deleteCommandeBudgetsRelationship(source);
	}

	/**
	 * Ajoute un ensemble d'articles a la commande <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande a laquelle seront ajoutes les articles
	 * @param fournisseur fournisseur de la commande (sstraitant eventuel) si null fournisseur principal des articles
	 * @param articles articles a ajouter
	 * @param attribution attribution des articles a ajouter a la commande
	 * @param typeAchat type achat des articles a ajouter a la commande
	 */
	public void ajouterArticles(EOEditingContext ed, EOCommande commande, EOFournisseur fournisseur, NSArray articles, EOAttribution attribution, EOTypeAchat typeAchat) {
		if (commande == null)
			throw new FactoryException("il faut une commande");
		if (articles == null || articles.count() == 0)
			throw new FactoryException("il faut au moins un article");
		if ((commande.articles() == null || commande.articles().count() == 0) &&
				(attribution == null) && (typeAchat == null)) {
			throw new FactoryException("il faut soit une attribution soit un type achat");
		}
		EOCatalogue unArticleAAjouter = (EOCatalogue) articles.objectAtIndex(0);
		if (commande.fournisseur() == null) {
			if (fournisseur != null) {
				affecterFournisseur(commande, fournisseur);
			}
			else {
				affecterFournisseur(commande, unArticleAAjouter.fournisseur());
			}
		}
		FactoryArticle factoryArticle = new FactoryArticle();
		Enumeration enumArticles = articles.objectEnumerator();
		while (enumArticles.hasMoreElements()) {
			unArticleAAjouter = (EOCatalogue) enumArticles.nextElement();
			factoryArticle.dupliquer(ed, unArticleAAjouter, commande, attribution, typeAchat);
		}
	}

	/**
	 * Ajoute un article a la commande <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande a laquelle seront ajoutes les articles
	 * @param article article a ajouter
	 * @param attribution attribution de l'article a ajouter a la commande
	 * @param typeAchat type achat de l'article a ajouter a la commande
	 */
	public void ajouterArticle(EOEditingContext ed, EOCommande commande, EOCatalogue article, EOAttribution attribution, EOTypeAchat typeAchat) {
		ajouterArticles(ed, commande, null, new NSArray(article), attribution, typeAchat);
	}

	/**
	 * Ajoute un article a la commande <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande a laquelle sera ajoute l'article
	 * @param article article a ajouter
	 * @param attribution attribution de l'article a ajouter a la commande
	 * @param typeAchat type achat de l'article a ajouter a la commande
	 */
	public void ajouterArticle(EOEditingContext ed, EOCommande commande, EOFournisseur fournisseur, EOArticle article, EOAttribution attribution, EOTypeAchat typeAchat) {
		if (commande == null)
			throw new FactoryException("il faut une commande");
		if (article == null)
			throw new FactoryException("il faut un article");
		if ((commande.articles() == null || commande.articles().count() == 0) &&
				(attribution == null) && (typeAchat == null)) {
			throw new FactoryException("il faut soit une attribution soit un type achat");
		}
		if (commande.fournisseur() == null && fournisseur != null) {
			affecterFournisseur(commande, fournisseur);
		}

		FactoryArticle factoryArticle = new FactoryArticle();
		factoryArticle.affecterCommande(article, commande);
		if (commande.typeAchat() == null && typeAchat != null) {
			factoryArticle.affecterTypeAchat(article, typeAchat);
		}
		if (commande.attribution() == null && attribution != null) {
			factoryArticle.affecterAttribution(article, attribution);
		}

		//verifier si ca pose pas de pb en cas de modif ultérieure de l'article si exception
		article.validateObjectMetier();
		factoryArticle.majCommande(commande, article.codeExer(), article.typeAchat());
	}

	/**
	 * Supprime un article de la commande <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param source article a supprimer
	 */
	public void supprimerArticle(EOEditingContext ed, EOArticle article) {
		FactoryArticle factoryArticle = new FactoryArticle();

		if (article == null)
			throw new FactoryException("il faut un article");

		factoryArticle.supprimer(ed, article);
	}

	/**
	 * Supprime tous les articles d'une commande
	 *
	 * @param ed
	 * @param commande
	 */
	public void supprimerTousLesArticles(EOEditingContext ed, EOCommande commande) {
		NSArray articles = commande.articles();
		FactoryArticle factoryArticle = new FactoryArticle();
		for (int i = articles.count() - 1; i >= 0; i--) {
			factoryArticle.supprimer(ed, (EOArticle) articles.objectAtIndex(i));
		}

	}

	/**
	 * Permet de dupliquer une commande existante sur un exercice donne sans les informations budgetaires <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande d'origine que l'on va dupliquer
	 * @param exercice exercice cible sur lequel on va creer la nouvelle commande
	 * @param utilisateur utilisateur effectuant cette operation
	 * @return une EOCommande
	 * @throws CommandeException
	 */
	public EOCommande dupliquer(EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur) throws CommandeException {
		return dupliquer(ed, commande, exercice, utilisateur, true);
	}

	/**
	 * Permet de dupliquer une commande existante sur un exercice donne <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande d'origine que l'on va dupliquer
	 * @param exercice exercice cible sur lequel on va creer la nouvelle commande
	 * @param utilisateur utilisateur effectuant cette operation
	 * @param withInfosBudgetaires boolean indiquant si on duplique ou pas les informations budgetaires
	 * @return une EOCommande
	 * @throws CommandeException
	 */

	public EOCommande dupliquer(EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur, boolean withInfosBudgetaires) throws CommandeException {
		EOCommande newCommande = creer(ed);

		// infos de la commande
		setCommLibelle(newCommande, commande.commLibelle());
		affecterDevise(newCommande, commande.devise());
		affecterExercice(newCommande, exercice);
		affecterFournisseur(newCommande, commande.fournisseur());
		affecterTypeEtat(newCommande, FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_PRECOMMANDE));
		affecterTypeEtatImprimable(newCommande, FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_NON_IMPRIMABLE));
		affecterUtilisateur(newCommande, utilisateur);

		// infos budgetaires (si demande)
		if (withInfosBudgetaires) {
			FactoryCommandeBudget factoryCommandeBudget = new FactoryCommandeBudget();
			for (int i = 0; i < commande.commandeBudgets().count(); i++)
				factoryCommandeBudget.dupliquer(ed, (EOCommandeBudget) commande.commandeBudgets().objectAtIndex(i), newCommande);
		}

		//si infos b2b on les duplique aussi
		if (commande.toB2bCxmlPunchoutmsgs() != null && commande.toB2bCxmlPunchoutmsgs().count() > 0) {
			EOB2bCxmlPunchoutmsg msg = (EOB2bCxmlPunchoutmsg) commande.toB2bCxmlPunchoutmsgs().objectAtIndex(0);
			FactoryCxmlPunchoutmsg f = new FactoryCxmlPunchoutmsg();
			f.dupliquerEOB2bCxmlPunchoutmsg(ed, commande.utilisateur().persId(), commande, msg);
		}

		// articles
		FactoryArticle factoryArticle = new FactoryArticle();
		for (int i = 0; i < commande.articles().count(); i++)
			factoryArticle.dupliquer(ed, (EOArticle) commande.articles().objectAtIndex(i), newCommande);

		try {
			return newCommande;
		} catch (CommandeException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}

		return null;
	}

	public EOCommande creer(EOEditingContext ed, String commLibelle, String commReference,
			EODevise devise, EOExercice exercice, EOFournisseur fournisseur, EOTypeEtat typeEtat,
			EOTypeEtat typeEtatImprimable, EOUtilisateur utilisateur)
			throws CommandeException {

		EOCommande newCommande = creer(ed);

		setCommLibelle(newCommande, commLibelle);
		setCommReference(newCommande, commReference);

		if (devise == null)
			devise = FinderDevise.getDeviseEnCours(ed, exercice);

		affecterDevise(newCommande, devise);
		affecterExercice(newCommande, exercice);
		affecterFournisseur(newCommande, fournisseur);
		if (typeEtat != null)
			affecterTypeEtat(newCommande, typeEtat);
		else
			affecterTypeEtat(newCommande, FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_PRECOMMANDE));

		if (typeEtatImprimable != null)
			affecterTypeEtatImprimable(newCommande, typeEtatImprimable);
		else
			affecterTypeEtatImprimable(newCommande, FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_NON_IMPRIMABLE));

		affecterUtilisateur(newCommande, utilisateur);

		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}

		return null;
	}

	public EOCommande creer(EOEditingContext ed) throws CommandeException {
		EOCommande newCommande = (EOCommande) Factory.instanceForEntity(ed, EOCommande.ENTITY_NAME);
		ed.insertObject(newCommande);
		return newCommande;
	}

	public void setCommLibelle(EOCommande commande, String commLibelle) {
		if (commande == null)
			return;
		commande.setCommLibelle(commLibelle);
	}

	public void setCommReference(EOCommande commande, String commReference) {
		if (commande == null)
			return;
		commande.setCommReference(commReference);
	}

	public void affecterDevise(EOCommande commande, EODevise devise) {
		commande.setDeviseRelationship(devise);
	}

	public void affecterExercice(EOCommande commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
		// On recalcule la devise systematiquement
		// pour prendre un compte un eventuel chgt de devise
		// d'un exercice a l'autre
		affecterDevise(commande, FinderDevise.getDeviseEnCours(commande.editingContext(), exercice));
	}

	public void affecterFournisseur(EOCommande commande, EOFournisseur fournisseur) {
		commande.setFournisseurRelationship(fournisseur);
	}

	public void affecterTypeEtat(EOCommande commande, EOTypeEtat typeEtat) {
		commande.setTypeEtatRelationship(typeEtat);
	}

	public void affecterTypeEtatImprimable(EOCommande commande, EOTypeEtat typeEtatImprimable) {
		commande.setTypeEtatImprimableRelationship(typeEtatImprimable);
	}

	public void affecterUtilisateur(EOCommande commande, EOUtilisateur utilisateur) {
		commande.setUtilisateurRelationship(utilisateur);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommande commande) throws CommandeException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		commande.setDeviseRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setFournisseurRelationship(null);
		commande.setTypeEtatRelationship(null);
		commande.setTypeEtatImprimableRelationship(null);
		commande.setUtilisateurRelationship(null);

		NSArray larray = commande.articles();
		if (larray.count() > 0) {
			FactoryArticle factory = new FactoryArticle();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(ed, (EOArticle) larray.objectAtIndex(i));
		}

		larray = commande.commandeBudgets();
		if (larray.count() > 0) {
			FactoryCommandeBudget factory = new FactoryCommandeBudget();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(ed, (EOCommandeBudget) larray.objectAtIndex(i));
		}

		larray = commande.commandeEngagements();
		if (larray.count() > 0) {
			FactoryEngagementBudget factory = new FactoryEngagementBudget();
			FactoryCommandeEngagement factoryCdeEngagement = new FactoryCommandeEngagement();
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOEngagementBudget engage = ((EOCommandeEngagement) larray.objectAtIndex(i)).engagementBudget();
				factoryCdeEngagement.supprimer(ed, (EOCommandeEngagement) larray.objectAtIndex(i));
				factory.supprimer(ed, engage);
			}
		}

		ed.deleteObject(commande);
	}

	public void verifier(EOCommande commande) {
		commande.validateObjectMetier();
	}

	public EOCommande creer(EOEditingContext edc, EOXMLCommande laCommandeACreer, EOUtilisateur utilisateur) throws CommandeException {
		EOCommande commande = null;

		EOExercice exercice = (EOExercice) EOUtilities.objectWithPrimaryKeyValue(edc, EOExercice.ENTITY_NAME, laCommandeACreer.exercice());
		EOFournisseur fournisseur = FinderFournisseur.getFournisseur(edc, laCommandeACreer.fournisseur());
		EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(edc, laCommandeACreer.typeApplication().toUpperCase());
		String libelle = laCommandeACreer.libelle();
		String reference = laCommandeACreer.reference();

		// Traitement des articles
		NSArray articles = laCommandeACreer.articles();
		EOAttribution attribution = null;
		EOTypeAchat typeAchat = null;
		if (laCommandeACreer.attribution() != null) {
			attribution = (EOAttribution) EOUtilities.objectWithPrimaryKeyValue(edc, EOAttribution.ENTITY_NAME, laCommandeACreer.attribution());
		}
		else {
			NSArray lesCodesExer = (NSArray) articles.valueForKeyPath("codeExer");
			NSArray typeAchatCommuns = FinderTypeAchat.getTypeAchatsCommunsPourCodeExers(edc, exercice, lesCodesExer);
			if (typeAchatCommuns != null && typeAchatCommuns.count() == 1) {
				typeAchat = (EOTypeAchat) typeAchatCommuns.lastObject();
			}
			else {
				throw new FactoryException("Impossible de determiner un type achat inequivoque");
			}
		}
		if (exercice != null &&
				fournisseur != null &&
				typeApplication != null &&
				libelle != null &&
				(attribution != null || typeAchat != null)) {
			commande = creer(edc, libelle, reference, null, exercice, fournisseur, null, null, utilisateur);
			commande.setTypeApplication(typeApplication);

			// Creation des articles
			FactoryArticle fa = new FactoryArticle();
			Enumeration enumArticles = articles.objectEnumerator();
			while (enumArticles.hasMoreElements()) {
				EOArticle newArticle = null;
				EOXMLArticle unArticle = (EOXMLArticle) enumArticles.nextElement();
				EOTva tva = FinderTva.getTvaForTaux(edc, unArticle.tva());
				EOCodeExer codeExer = FinderCodeExer.getCodeExer(edc, unArticle.codeExer(), exercice);
				if (tva != null && codeExer != null) {
					newArticle = fa.creer(edc, unArticle);
					if (newArticle != null) {
						fa.affecterAttribution(newArticle, attribution);
						fa.affecterTypeAchat(newArticle, typeAchat);
						fa.affecterCodeExer(newArticle, codeExer);
						fa.affecterTva(newArticle, tva);
						ajouterArticle(edc, commande, fournisseur, newArticle, attribution, typeAchat);
					}
				}
				else {
					throw new FactoryException("Au moins un article ne possede pas de code de nomenclature ou d'un taux tva");
				}
			}
		}
		else {
			throw new FactoryException("L'exercice, le fournisseur, le type application et l'attribution ou le type achat sont des attributs obligatoires.");
		}

		return commande;
	}

	public EOCommande creer(EOEditingContext ed, EOB2bCxmlPunchoutmsg msg, EOCommande oldCommande) throws CommandeException {
		try {
			EOCxml punchOutOrderMessage = CXMLMessageFactory.load(msg.cxml());

			if (!punchOutOrderMessage.isPunchOutOrderMessage()) {
				throw new B2bCxmlCommandeException("Le message cXML n'est pas un punchOutOrderMessage\n" + punchOutOrderMessage);
			}

			String buyerCookieString = punchOutOrderMessage.message().punchOutOrderMessage().buyerCookie();
			System.out.println("buyerCookieString = " + buyerCookieString);
			CXMLBuyerCookie buyerCookie = CXMLBuyerCookie.parse(buyerCookieString);

			Integer persId = buyerCookie.getPersId();
			Integer exeOrdre = buyerCookie.getExeOrdre();
			Integer ceOrdre = buyerCookie.getCeOrdre();
			Long commId = buyerCookie.getCommId();

			if (persId == null) {
				throw new B2bCxmlCommandeException("persId nul");
			}
			if (exeOrdre == null) {
				throw new B2bCxmlCommandeException("exeOrdre nul");
			}
			if (ceOrdre == null) {
				throw new B2bCxmlCommandeException("ceOrdre nul");
			}

			EOUtilisateur utilisateur = FinderUtilisateur.getUtilisateurByPersId(ed, persId);
			if (utilisateur == null) {
				throw new B2bCxmlCommandeException("Aucun utilisateur trouve pour persId=" + persId);
			}

			EOCodeExer codeExer = EOCodeExer.fetchByKeyValue(ed, EOCodeExer.CE_ORDRE_KEY, ceOrdre);
			if (codeExer == null) {
				throw new B2bCxmlCommandeException("Aucun codeExer trouve pour ceOrdre=" + ceOrdre);
			}
			EOTypeAchat typeAchat = null;
			EOAttribution attribution = null;
			if (buyerCookie.getTypaId() != null) {
				typeAchat = EOTypeAchat.fetchByKeyValue(ed, EOTypeAchat.TYPA_ID_KEY, buyerCookie.getTypaId());
			}
			if (buyerCookie.getAttOrdre() != null) {
				attribution = EOAttribution.fetchByKeyValue(ed, EOAttribution.ATT_ORDRE_KEY, buyerCookie.getAttOrdre());
			}
			EOTypeEtat typeEtatImprimable = FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_NON_IMPRIMABLE);
			EOTypeEtat typeEtat = FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_PRECOMMANDE);
			EOFournisseur fournisseur = msg.toFournisB2bCxmlParam().toFournisseur();
			EOExercice exercice = FinderExercice.getExercice(ed, exeOrdre);
			if (exercice == null) {
				throw new B2bCxmlCommandeException("Aucun exercice trouve pour exeOrdre=" + exeOrdre);
			}

			EODevise devise = FinderDevise.getDeviseEnCours(ed, exercice);
			String deviseCode = devise.devCode();

			EOCommande commande = null;
			//			System.out.println("oldCommande=" + oldCommande);
			//			System.out.println("oldCommande.editingContext()=" + (oldCommande != null ? oldCommande.editingContext() : ""));
			//			System.out.println("ed=" + ed);
			if (oldCommande != null && !ed.equals(oldCommande.editingContext())) {
				oldCommande = oldCommande.localInstanceIn(ed);
				if (ed.globalIDForObject(oldCommande) == null) {
					System.err.println("globalid de oldCommande est null");
				}
			}

			//			System.out.println("commId=" + commId);
			//			System.out.println("oldCommande=" + oldCommande);
			//			System.out.println("oldCommande.hascode=" + (oldCommande != null ? oldCommande.hashCode() : ""));

			String commReference = "[B2B] Indiquez la référence de la commande";
			String commLibelle = "[B2B] - " + punchOutOrderMessage.header().sender().userAgent() + " / " + punchOutOrderMessage.payloadID();

			//s'il s'agit d'une ancienne commande, on la recupere, sinon on en crée une nouvelle
			if (commId != null && oldCommande != null && commId.equals(Long.valueOf(oldCommande.hashCode()))) {
				System.out.println("Commande recuperee");
				commande = oldCommande;
				if (ZStringUtil.isEmpty(commande.commLibelle())) {
					commande.setCommLibelle(commLibelle);
				}
				if (ZStringUtil.isEmpty(commande.commReference())) {
					commande.setCommReference(commReference);
				}
				supprimerTousLesArticles(ed, commande);
			}

			else {
				System.out.println("Nouvelle commande");
				commande = creer(ed, commLibelle, commReference, devise, exercice, fournisseur, typeEtat, typeEtatImprimable, utilisateur);
			}

			FactoryArticle fa = new FactoryArticle();

			NSArray articles = punchOutOrderMessage.message().punchOutOrderMessage().itemIn();
			if (articles.count() == 0) {
				throw new B2bCxmlCommandeException("La commande transmise ne contient pas d'articles");
			}

			for (int i = 0; i < articles.count(); i++) {
				EOItemIn unItemIn = (EOItemIn) articles.objectAtIndex(i);
				//verifier qu'on a la bonne devise
				if (!(unItemIn.itemDetail().unitPrice().money().currency() != null && unItemIn.itemDetail().unitPrice().money().currency().equals(deviseCode))) {
					throw new B2bCxmlCommandeException("Devise non compatible (" + unItemIn.itemDetail().unitPrice().money().currency() + ")");
				}

				EOArticle newArticle = fa.creer(ed, unItemIn, utilisateur, msg.toFournisB2bCxmlParam());
				newArticle.setTva(msg.toFournisB2bCxmlParam().toTva());
				fa.affecterAttribution(newArticle, attribution);
				fa.affecterTypeAchat(newArticle, typeAchat);
				fa.affecterCodeExer(newArticle, codeExer);
				ajouterArticle(ed, commande, fournisseur, newArticle, attribution, typeAchat);
			}

			// Verifier que le total de la commande generee est identique au total de la commande transmise par le fournisseur
			BigDecimal totalB2b = punchOutOrderMessage.message().punchOutOrderMessage().punchOutOrderMessageHeader().total().money().getVal();
			if (commande.totalHt().compareTo(totalB2b) != 0) {
				throw new B2bCxmlCommandeException("Total transmis different du total calcule (" + totalB2b + " / " + commande.totalHt() + ")");
			}

			//relier la commande au punchoutMsg
			commande.addToToB2bCxmlPunchoutmsgsRelationship(msg);
			msg.setToCommandeRelationship(commande);

			return commande;
		} catch (CommandeException e1) {
			System.err.println("erreur lors du traitement du msg :" + msg);
			e1.printStackTrace();
			throw e1;

		} catch (Exception e) {
			System.err.println("erreur lors du traitement du msg :" + msg);
			e.printStackTrace();
			throw new B2bCxmlCommandeException(e.getMessage());
		}

	}
}
