/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleActionException;
import org.cocktail.fwkcktldepense.server.exception.CommandeControleConventionException;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class FactoryCommandeControleConvention extends Factory {
	
	public FactoryCommandeControleConvention() {
		super();
	}
	
	public FactoryCommandeControleConvention(boolean withLog) {
		super(withLog);
	}
	
	protected NSArray dupliquer(EOEditingContext ed, EOCommandeBudget origine, EOCommandeBudget commandeBudget) throws CommandeControleActionException {
		NSMutableArray array=new NSMutableArray();
		
		for (int i=0; i<origine.commandeControleConventions().count(); i++) {
			EOCommandeControleConvention controle=(EOCommandeControleConvention)origine.commandeControleConventions().objectAtIndex(i);
			array.addObject(creer(ed, controle.cconHtSaisie(), controle.cconTvaSaisie(), controle.cconTtcSaisie(), 
					controle.cconMontantBudgetaire(), controle.cconPourcentage(), controle.convention(), commandeBudget.exercice(), commandeBudget));
		}
		
		return array;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, NSArray controles) {
		for (int i=0; i<controles.count(); i++) {
			EOCommandeControleConvention controle=(EOCommandeControleConvention)controles.objectAtIndex(i);
			if (!verifier(exercice, utilisateur, organ, typeCredit, controle))
				return false;
		}
		return true;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOCommandeControleConvention controle) {
		return verifier(exercice, utilisateur, organ, typeCredit, controle.convention());
	}
	
	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOConvention convention) {
		if (exercice==null || utilisateur==null || organ==null || typeCredit==null || convention==null)
			return false;
		NSMutableDictionary bindings=new NSMutableDictionary();
		bindings.setObjectForKey(organ, "organ");
		bindings.setObjectForKey(typeCredit, "typeCredit");
		bindings.setObjectForKey(exercice, "exercice");
		bindings.setObjectForKey(utilisateur, "utilisateur");
		bindings.setObjectForKey(convention, "convention");
		if (FinderConvention.getConventions(convention.editingContext(), bindings).count()<=0)
			return false;
		return true;
	}

	public EOCommandeControleConvention creer(EOEditingContext ed, BigDecimal cconHtSaisie, BigDecimal cconTvaSaisie, 
			BigDecimal cconTtcSaisie, BigDecimal cconMontantBudgetaire, BigDecimal cconPourcentage, 
			EOConvention convention, EOExercice exercice, EOCommandeBudget commandeBudget)
	throws CommandeControleConventionException {
		
		EOCommandeControleConvention newCommande = creer(ed);
		
		setCconHtSaisie(newCommande, cconHtSaisie);
		setCconTvaSaisie(newCommande, cconTvaSaisie);
		setCconTtcSaisie(newCommande, cconTtcSaisie);
		setCconMontantBudgetaire(newCommande, cconMontantBudgetaire);
		setCconPourcentage(newCommande, cconPourcentage);
		
		affecterCodeConvention(newCommande, convention);
		affecterExercice(newCommande, exercice);
		affecterCommandeBudget(newCommande, commandeBudget);
		
		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeControleConventionException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}
		
		return null;
	}
	
	public EOCommandeControleConvention creer(EOEditingContext ed) throws CommandeControleConventionException {
		EOCommandeControleConvention newCommande=(EOCommandeControleConvention)Factory.instanceForEntity(ed,EOCommandeControleConvention.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newCommande);
		return newCommande;
	}
	
	
	public void setCconHtSaisie(EOCommandeControleConvention commande, BigDecimal cconHtSaisie) {
		if (commande==null)
			return;
		commande.setCconHtSaisie(cconHtSaisie);
	}
	
	public void setCconTvaSaisie(EOCommandeControleConvention commande, BigDecimal cconTvaSaisie) {
		if (commande==null)
			return;
		commande.setCconTvaSaisie(cconTvaSaisie);
	}
	
	public void setCconTtcSaisie(EOCommandeControleConvention commande, BigDecimal cconTtcSaisie) {
		if (commande==null)
			return;
		commande.setCconTtcSaisie(cconTtcSaisie);
	}
	
	public void setCconMontantBudgetaire(EOCommandeControleConvention commande, BigDecimal cconMontantBudgetaire) {
		if (commande==null)
			return;
		commande.setCconMontantBudgetaire(cconMontantBudgetaire);
	}
	
	public void setCconPourcentage(EOCommandeControleConvention commande, BigDecimal cconPourcentage) {
		if (commande==null)
			return;
		commande.setCconPourcentage(cconPourcentage);
	}

	public void affecterCodeConvention(EOCommandeControleConvention commande, EOConvention convention) {
		commande.setConventionRelationship(convention);
	}
	
	public void affecterExercice(EOCommandeControleConvention commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
	}
	
	public void affecterCommandeBudget(EOCommandeControleConvention commande, EOCommandeBudget commandeBudget) {
		commande.setCommandeBudgetRelationship(commandeBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeControleConvention commande) throws CommandeControleConventionException {
		commande.setConventionRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setCommandeBudgetRelationship(null);

		ed.deleteObject(commande);
	}
	
	public void verifier(EOCommandeControleConvention commande) {
		commande.validateObjectMetier();
	}
}
