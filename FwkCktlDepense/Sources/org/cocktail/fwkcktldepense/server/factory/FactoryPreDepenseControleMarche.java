/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPreDepenseControleMarche extends Factory {

	public FactoryPreDepenseControleMarche() {
		super();
	}

	public FactoryPreDepenseControleMarche(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseControleMarche copie(EOEditingContext ed, EOPreDepenseControleMarche depense, EOPreDepenseBudget budget) {

		EOPreDepenseControleMarche newDepense = creer(ed);

		setPdmarHtSaisie(newDepense, depense.dmarHtSaisie());
		setPdmarTvaSaisie(newDepense, depense.dmarTvaSaisie());
		setPdmarTtcSaisie(newDepense, depense.dmarTtcSaisie());
		setPdmarMontantBudgetaire(newDepense, depense.dmarMontantBudgetaire());

		affecterAttribution(newDepense, depense.attribution());
		affecterExercice(newDepense, depense.exercice());
		affecterPreDepenseBudget(newDepense, budget);

		setPdmarPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EOPreDepenseControleMarche creer(EOEditingContext ed, BigDecimal dmarHtSaisie, BigDecimal dmarTvaSaisie,
			BigDecimal dmarTtcSaisie, BigDecimal dmarMontantBudgetaire, BigDecimal dmarPourcentage, EOAttribution attribution, EOExercice exercice,
			EOPreDepenseBudget depenseBudget)
			throws DepenseControleMarcheException {

		EOPreDepenseControleMarche newDepense = creer(ed);

		setPdmarHtSaisie(newDepense, dmarHtSaisie);
		setPdmarTvaSaisie(newDepense, dmarTvaSaisie);
		setPdmarTtcSaisie(newDepense, dmarTtcSaisie);
		setPdmarMontantBudgetaire(newDepense, dmarMontantBudgetaire);

		affecterAttribution(newDepense, attribution);
		affecterExercice(newDepense, exercice);
		affecterPreDepenseBudget(newDepense, depenseBudget);

		setPdmarPourcentage(newDepense, dmarPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleMarcheException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseControleMarche creer(EOEditingContext ed) throws DepenseControleMarcheException {
		EOPreDepenseControleMarche newDepense = (EOPreDepenseControleMarche) Factory.
				instanceForEntity(ed, EOPreDepenseControleMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setPdmarPourcentage(EOPreDepenseControleMarche depense, BigDecimal dmarPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dmarPourcentage);
	}

	public void setPdmarHtSaisie(EOPreDepenseControleMarche depense, BigDecimal dmarHtSaisie) {
		if (depense == null)
			return;
		depense.setDmarHtSaisie(dmarHtSaisie);
	}

	public void setPdmarTvaSaisie(EOPreDepenseControleMarche depense, BigDecimal dmarTvaSaisie) {
		if (depense == null)
			return;
		depense.setDmarTvaSaisie(dmarTvaSaisie);
	}

	public void setPdmarTtcSaisie(EOPreDepenseControleMarche depense, BigDecimal dmarTtcSaisie) {
		if (depense == null)
			return;
		depense.setDmarTtcSaisie(dmarTtcSaisie);
	}

	public void setPdmarMontantBudgetaire(EOPreDepenseControleMarche depense, BigDecimal dmarMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDmarMontantBudgetaire(dmarMontantBudgetaire);
	}

	public void affecterAttribution(EOPreDepenseControleMarche depense, EOAttribution attribution) {
		depense.setAttributionRelationship(attribution);
	}

	public void affecterExercice(EOPreDepenseControleMarche depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterPreDepenseBudget(EOPreDepenseControleMarche depense, EOPreDepenseBudget depenseBudget) {
		depense.setPreDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseControleMarche depense) throws DepenseControleMarcheException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setAttributionRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setPreDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EOPreDepenseControleMarche depense) {
		depense.validateObjectMetier();
	}
}
