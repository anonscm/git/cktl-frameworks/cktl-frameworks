package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseControleAnalytique {

	public _IDepenseControleAnalytique _copie(EOEditingContext ed, _IDepenseControleAnalytique depense, _IDepenseBudget budget);

	public _IDepenseControleAnalytique _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie,
			BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			_IDepenseBudget depenseBudget)
			throws DepenseControleAnalytiqueException;

	public void _setPourcentage(_IDepenseControleAnalytique depense, BigDecimal dactPourcentage);

	public void _setHtSaisie(_IDepenseControleAnalytique depense, BigDecimal htSaisie);

	public void _setTvaSaisie(_IDepenseControleAnalytique depense, BigDecimal tvaSaisie);

	public void _setTtcSaisie(_IDepenseControleAnalytique depense, BigDecimal ttcSaisie);

	public void _setMontantBudgetaire(_IDepenseControleAnalytique depense, BigDecimal montantBudgetaire);

	public void _affecterCodeAnalytique(_IDepenseControleAnalytique depense, EOCodeAnalytique codeAnalytique);

	public void _affecterExercice(_IDepenseControleAnalytique depense, EOExercice exercice);

	public void _affecterDepenseBudget(_IDepenseControleAnalytique depense, _IDepenseBudget depenseBudget);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void _supprimer(EOEditingContext ed, _IDepenseControleAnalytique depense) throws DepenseControleAnalytiqueException;

	public void _verifier(_IDepenseControleAnalytique depense);
}
