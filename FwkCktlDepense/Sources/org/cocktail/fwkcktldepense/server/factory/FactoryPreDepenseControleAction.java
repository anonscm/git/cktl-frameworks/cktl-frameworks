/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleActionException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPreDepenseControleAction extends Factory implements _IFactoryDepenseControleAction {

	public FactoryPreDepenseControleAction() {
		super();
	}

	public FactoryPreDepenseControleAction(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseControleAction copie(EOEditingContext ed, EOPreDepenseControleAction depense, EOPreDepenseBudget budget) {

		EOPreDepenseControleAction newDepense = creer(ed);

		setPdactHtSaisie(newDepense, depense.dactHtSaisie());
		setPdactTvaSaisie(newDepense, depense.dactTvaSaisie());
		setPdactTtcSaisie(newDepense, depense.dactTtcSaisie());
		setPdactMontantBudgetaire(newDepense, depense.dactMontantBudgetaire());

		affecterTypeAction(newDepense, depense.typeAction());
		affecterExercice(newDepense, depense.exercice());
		affecterPreDepenseBudget(newDepense, budget);

		setPdactPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EOPreDepenseControleAction creer(EOEditingContext ed, BigDecimal dactHtSaisie, BigDecimal dactTvaSaisie,
			BigDecimal dactTtcSaisie, BigDecimal dactMontantBudgetaire, BigDecimal dactPourcentage, EOTypeAction typeAction, EOExercice exercice,
			EOPreDepenseBudget depenseBudget)
			throws DepenseControleActionException {

		EOPreDepenseControleAction newDepense = creer(ed);

		setPdactHtSaisie(newDepense, dactHtSaisie);
		setPdactTvaSaisie(newDepense, dactTvaSaisie);
		setPdactTtcSaisie(newDepense, dactTtcSaisie);
		setPdactMontantBudgetaire(newDepense, dactMontantBudgetaire);

		affecterTypeAction(newDepense, typeAction);
		affecterExercice(newDepense, exercice);
		affecterPreDepenseBudget(newDepense, depenseBudget);

		setPdactPourcentage(newDepense, dactPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleActionException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseControleAction creer(EOEditingContext ed) throws DepenseControleActionException {
		EOPreDepenseControleAction newDepense = (EOPreDepenseControleAction) Factory.instanceForEntity(ed, EOPreDepenseControleAction.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setPdactPourcentage(EOPreDepenseControleAction depense, BigDecimal dactPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dactPourcentage);
	}

	public void setPdactHtSaisie(EOPreDepenseControleAction depense, BigDecimal dactHtSaisie) {
		if (depense == null)
			return;
		depense.setDactHtSaisie(dactHtSaisie);
	}

	public void setPdactTvaSaisie(EOPreDepenseControleAction depense, BigDecimal dactTvaSaisie) {
		if (depense == null)
			return;
		depense.setDactTvaSaisie(dactTvaSaisie);
	}

	public void setPdactTtcSaisie(EOPreDepenseControleAction depense, BigDecimal dactTtcSaisie) {
		if (depense == null)
			return;
		depense.setDactTtcSaisie(dactTtcSaisie);
	}

	public void setPdactMontantBudgetaire(EOPreDepenseControleAction depense, BigDecimal dactMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDactMontantBudgetaire(dactMontantBudgetaire);
	}

	public void affecterTypeAction(EOPreDepenseControleAction depense, EOTypeAction typeAction) {
		depense.setTypeActionRelationship(typeAction);
	}

	public void affecterExercice(EOPreDepenseControleAction depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterPreDepenseBudget(EOPreDepenseControleAction depense, EOPreDepenseBudget depenseBudget) {
		depense.setPreDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseControleAction depense) throws DepenseControleActionException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setTypeActionRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setPreDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EOPreDepenseControleAction depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleAction copie(EOEditingContext ed, _IDepenseControleAction depense, _IDepenseBudget budget) {
		return copie(ed, (EOPreDepenseControleAction) depense, (EOPreDepenseBudget) budget);
	}

	public _IDepenseControleAction creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOTypeAction typeAction, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleActionException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, typeAction, exercice, (EOPreDepenseBudget) depenseBudget);
	}

	public void setIactHtSaisie(_IDepenseControleAction depense, BigDecimal dactHtSaisie) {
		setPdactHtSaisie((EOPreDepenseControleAction) depense, dactHtSaisie);

	}

	public void setIactTvaSaisie(_IDepenseControleAction depense, BigDecimal dactTvaSaisie) {
		setPdactTvaSaisie((EOPreDepenseControleAction) depense, dactTvaSaisie);

	}

	public void setIactTtcSaisie(_IDepenseControleAction depense, BigDecimal dactTtcSaisie) {
		setPdactTtcSaisie((EOPreDepenseControleAction) depense, dactTtcSaisie);

	}

	public void setIactMontantBudgetaire(_IDepenseControleAction depense, BigDecimal dactMontantBudgetaire) {
		setPdactMontantBudgetaire((EOPreDepenseControleAction) depense, dactMontantBudgetaire);

	}

	public void affecterTypeAction(_IDepenseControleAction depense, EOTypeAction typeAction) {
		affecterTypeAction((EOPreDepenseControleAction) depense, typeAction);

	}

	public void affecterExercice(_IDepenseControleAction depense, EOExercice exercice) {
		affecterExercice((EOPreDepenseControleAction) depense, exercice);

	}

	public void affecterDepenseBudget(_IDepenseControleAction depense, _IDepenseBudget depenseBudget) {
		affecterDepenseBudget((EOPreDepenseControleAction) depense, (EOPreDepenseBudget) depenseBudget);

	}

	public void supprimer(EOEditingContext ed, _IDepenseControleAction depense) throws DepenseControleActionException {
		supprimer(ed, (EOPreDepenseControleAction) depense);

	}

	public void verifier(_IDepenseControleAction depense) {
		verifier((EOPreDepenseControleAction) depense);

	}

	public void setIactPourcentage(_IDepenseControleAction depense, BigDecimal dactPourcentage) {
		setPdactPourcentage((EOPreDepenseControleAction) depense, dactPourcentage);

	}
}
