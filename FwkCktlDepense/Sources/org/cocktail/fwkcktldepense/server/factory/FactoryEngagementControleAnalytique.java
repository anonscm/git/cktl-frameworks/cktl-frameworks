/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEngagementControleAnalytique extends Factory {

	public FactoryEngagementControleAnalytique() {
		super();
	}

	public FactoryEngagementControleAnalytique(boolean withLog) {
		super(withLog);
	}

	public EOEngagementControleAnalytique creer(EOEditingContext ed, BigDecimal eanaHtSaisie, BigDecimal eanaTvaSaisie, 
			BigDecimal eanaTtcSaisie, BigDecimal eanaMontantBudgetaire, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			EOEngagementBudget engagementBudget)
	throws EngagementControleAnalytiqueException {
		
		EOEngagementControleAnalytique newEngage = creer(ed);
		
		setEanaHtSaisie(newEngage, eanaHtSaisie);
		setEanaTvaSaisie(newEngage, eanaTvaSaisie);
		setEanaTtcSaisie(newEngage, eanaTtcSaisie);
		setEanaMontantBudgetaire(newEngage, eanaMontantBudgetaire);
		setEanaMontantBudgetaireReste(newEngage, eanaMontantBudgetaire);
		setEanaDateSaisie(newEngage, new NSTimestamp());
		
		affecterCodeAnalytique(newEngage, codeAnalytique);
		affecterExercice(newEngage, exercice);
		affecterEngagementBudget(newEngage, engagementBudget);
		
		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementControleAnalytiqueException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}
		
		return null;
	}
	
	public EOEngagementControleAnalytique creer(EOEditingContext ed) throws EngagementControleAnalytiqueException {
		EOEngagementControleAnalytique newEngage=(EOEngagementControleAnalytique)Factory.instanceForEntity(ed,EOEngagementControleAnalytique.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}
	
	
	public void setEanaHtSaisie(EOEngagementControleAnalytique engage, BigDecimal eanaHtSaisie) {
		if (engage==null)
			return;
		engage.setEanaHtSaisie(eanaHtSaisie);
	}
	
	public void setEanaTvaSaisie(EOEngagementControleAnalytique engage, BigDecimal eanaTvaSaisie) {
		if (engage==null)
			return;
		engage.setEanaTvaSaisie(eanaTvaSaisie);
	}
	
	public void setEanaTtcSaisie(EOEngagementControleAnalytique engage, BigDecimal eanaTtcSaisie) {
		if (engage==null)
			return;
		engage.setEanaTtcSaisie(eanaTtcSaisie);
	}
	
	public void setEanaMontantBudgetaire(EOEngagementControleAnalytique engage, BigDecimal eanaMontantBudgetaire) {
		if (engage==null)
			return;
		engage.setEanaMontantBudgetaire(eanaMontantBudgetaire);
	}
	
	public void setEanaMontantBudgetaireReste(EOEngagementControleAnalytique engage, BigDecimal eanaMontantBudgetaireReste) {
		if (engage==null)
			return;
		engage.setEanaMontantBudgetaireReste(eanaMontantBudgetaireReste);
	}

	public void setEanaDateSaisie(EOEngagementControleAnalytique engage, NSTimestamp eanaDateSaisie) {
		if (engage==null)
			return;
		engage.setEanaDateSaisie(eanaDateSaisie);
	}

	public void affecterCodeAnalytique(EOEngagementControleAnalytique engage, EOCodeAnalytique codeAnalytique) {
		engage.setCodeAnalytiqueRelationship(codeAnalytique);
	}
	
	public void affecterExercice(EOEngagementControleAnalytique engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}
	
	public void affecterEngagementBudget(EOEngagementControleAnalytique engage, EOEngagementBudget engagementBudget) {
		engage.setEngagementBudgetRelationship(engagementBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementControleAnalytique engage) throws EngagementControleAnalytiqueException{
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
		}
		
		engage.setCodeAnalytiqueRelationship(null);
		engage.setExerciceRelationship(null);
		engage.setEngagementBudgetRelationship(null);
		
		ed.deleteObject(engage);
	}
	
	public void verifier(EOEngagementControleAnalytique engage) {
		engage.validateObjectMetier();
	}
}
