/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryPreDepenseControlePlanComptable extends Factory implements _IFactoryDepenseControlePlanComptable {

	public FactoryPreDepenseControlePlanComptable() {
		super();
	}

	public FactoryPreDepenseControlePlanComptable(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseControlePlanComptable copie(EOEditingContext ed, EOPreDepenseControlePlanComptable depense, EOPreDepenseBudget budget) {

		EOPreDepenseControlePlanComptable newDepense = creer(ed);

		setPdpcoHtSaisie(newDepense, depense.dpcoHtSaisie());
		setPdpcoTvaSaisie(newDepense, depense.dpcoTvaSaisie());
		setPdpcoTtcSaisie(newDepense, depense.dpcoTtcSaisie());
		setPdpcoMontantBudgetaire(newDepense, depense.dpcoMontantBudgetaire());

		affecterPlanComptable(newDepense, depense.planComptable());
		affecterExercice(newDepense, depense.exercice());
		affecterPreDepenseBudget(newDepense, budget);
		newDepense.setEcritureDetailRelationship(depense.ecritureDetail());

		setPdpcoPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EOPreDepenseControlePlanComptable creer(EOEditingContext ed, BigDecimal dpcoHtSaisie, BigDecimal dpcoTvaSaisie,
			BigDecimal dpcoTtcSaisie, BigDecimal dpcoMontantBudgetaire, BigDecimal dpcoPourcentage, EOPlanComptable planComptable, EOExercice exercice,
			EOPreDepenseBudget depenseBudget)
			throws DepenseControlePlanComptableException {

		EOPreDepenseControlePlanComptable newDepense = creer(ed);

		setPdpcoHtSaisie(newDepense, dpcoHtSaisie);
		setPdpcoTvaSaisie(newDepense, dpcoTvaSaisie);
		setPdpcoTtcSaisie(newDepense, dpcoTtcSaisie);
		setPdpcoMontantBudgetaire(newDepense, dpcoMontantBudgetaire);

		affecterPlanComptable(newDepense, planComptable);
		affecterExercice(newDepense, exercice);
		affecterPreDepenseBudget(newDepense, depenseBudget);

		setPdpcoPourcentage(newDepense, dpcoPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControlePlanComptableException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseControlePlanComptable creer(EOEditingContext ed) throws DepenseControlePlanComptableException {
		EOPreDepenseControlePlanComptable newDepense = (EOPreDepenseControlePlanComptable) Factory.
				instanceForEntity(ed, EOPreDepenseControlePlanComptable.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setPdpcoPourcentage(EOPreDepenseControlePlanComptable depense, BigDecimal dpcoPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dpcoPourcentage);
	}

	public void setPdpcoHtSaisie(EOPreDepenseControlePlanComptable depense, BigDecimal dpcoHtSaisie) {
		if (depense == null)
			return;
		depense.setDpcoHtSaisie(dpcoHtSaisie);
	}

	public void setPdpcoTvaSaisie(EOPreDepenseControlePlanComptable depense, BigDecimal dpcoTvaSaisie) {
		if (depense == null)
			return;
		depense.setDpcoTvaSaisie(dpcoTvaSaisie);
	}

	public void setPdpcoTtcSaisie(EOPreDepenseControlePlanComptable depense, BigDecimal dpcoTtcSaisie) {
		if (depense == null)
			return;
		depense.setDpcoTtcSaisie(dpcoTtcSaisie);
	}

	public void setPdpcoMontantBudgetaire(EOPreDepenseControlePlanComptable depense, BigDecimal dpcoMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
	}

	public void affecterPlanComptable(EOPreDepenseControlePlanComptable depense, EOPlanComptable planComptable) {
		depense.setPlanComptableRelationship(planComptable);
	}

	public void affecterExercice(EOPreDepenseControlePlanComptable depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterPreDepenseBudget(EOPreDepenseControlePlanComptable depense, EOPreDepenseBudget depenseBudget) {
		depense.setPreDepenseBudgetRelationship(depenseBudget);
	}

	public void ajouterInventaires(EOPreDepenseControlePlanComptable depense, NSArray inventaires) {
		if (inventaires == null || inventaires.count() == 0)
			return;
		for (int i = 0; i < inventaires.count(); i++)
			ajouterInventaire(depense, (EOInventaire) inventaires.objectAtIndex(i));
	}

	public void ajouterInventaire(EOPreDepenseControlePlanComptable depense, EOInventaire inventaire) {
		depense.ajouterInventaire(inventaire);
	}

	public void supprimerInventaire(EOPreDepenseControlePlanComptable depense, EOInventaire inventaire) {
		depense.supprimerInventaire(inventaire);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseControlePlanComptable depense) throws DepenseControlePlanComptableException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setPlanComptableRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setPreDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EOPreDepenseControlePlanComptable depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControlePlanComptable _copie(EOEditingContext ed, _IDepenseControlePlanComptable depense, _IDepenseBudget budget) {
		return copie(ed, (EOPreDepenseControlePlanComptable) depense, (EOPreDepenseBudget) budget);
	}

	public _IDepenseControlePlanComptable _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOPlanComptable planComptable, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControlePlanComptableException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, planComptable, exercice, (EOPreDepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControlePlanComptable depense, BigDecimal dactPourcentage) {
		setPdpcoPourcentage((EOPreDepenseControlePlanComptable) depense, dactPourcentage);
	}

	public void _setHtSaisie(_IDepenseControlePlanComptable depense, BigDecimal htSaisie) {
		setPdpcoHtSaisie((EOPreDepenseControlePlanComptable) depense, htSaisie);

	}

	public void _setTvaSaisie(_IDepenseControlePlanComptable depense, BigDecimal tvaSaisie) {
		setPdpcoTvaSaisie((EOPreDepenseControlePlanComptable) depense, tvaSaisie);

	}

	public void _setTtcSaisie(_IDepenseControlePlanComptable depense, BigDecimal ttcSaisie) {
		setPdpcoTtcSaisie((EOPreDepenseControlePlanComptable) depense, ttcSaisie);

	}

	public void _setMontantBudgetaire(_IDepenseControlePlanComptable depense, BigDecimal montantBudgetaire) {
		setPdpcoMontantBudgetaire((EOPreDepenseControlePlanComptable) depense, montantBudgetaire);

	}

	public void _affecterPlanComptable(_IDepenseControlePlanComptable depense, EOPlanComptable planComptable) {
		affecterPlanComptable((EOPreDepenseControlePlanComptable) depense, planComptable);

	}

	public void _affecterExercice(_IDepenseControlePlanComptable depense, EOExercice exercice) {
		affecterExercice((EOPreDepenseControlePlanComptable) depense, exercice);

	}

	public void _affecterDepenseBudget(_IDepenseControlePlanComptable depense, _IDepenseBudget depenseBudget) {
		affecterPreDepenseBudget((EOPreDepenseControlePlanComptable) depense, (EOPreDepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControlePlanComptable depense) throws DepenseControlePlanComptableException {
		supprimer(ed, (EOPreDepenseControlePlanComptable) depense);

	}

	public void _verifier(_IDepenseControlePlanComptable depense) {
		verifier((EOPreDepenseControlePlanComptable) depense);

	}

	public void _supprimerInventaire(_IDepenseControlePlanComptable dcpc, EOInventaire inventaire) {
		supprimerInventaire((EOPreDepenseControlePlanComptable) dcpc, inventaire);

	}

}
