package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleActionException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseControleAction {

	public _IDepenseControleAction copie(EOEditingContext ed, _IDepenseControleAction depense, _IDepenseBudget budget);

	public _IDepenseControleAction creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie,
			BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOTypeAction typeAction, EOExercice exercice,
			_IDepenseBudget depenseBudget)
			throws DepenseControleActionException;

	public _IDepenseControleAction creer(EOEditingContext ed) throws DepenseControleActionException;

	public void setIactPourcentage(_IDepenseControleAction depense, BigDecimal dactPourcentage);

	public void setIactHtSaisie(_IDepenseControleAction depense, BigDecimal dactHtSaisie);

	public void setIactTvaSaisie(_IDepenseControleAction depense, BigDecimal dactTvaSaisie);

	public void setIactTtcSaisie(_IDepenseControleAction depense, BigDecimal dactTtcSaisie);

	public void setIactMontantBudgetaire(_IDepenseControleAction depense, BigDecimal dactMontantBudgetaire);

	public void affecterTypeAction(_IDepenseControleAction depense, EOTypeAction typeAction);

	public void affecterExercice(_IDepenseControleAction depense, EOExercice exercice);

	public void affecterDepenseBudget(_IDepenseControleAction depense, _IDepenseBudget depenseBudget);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, _IDepenseControleAction depense) throws DepenseControleActionException;

	public void verifier(_IDepenseControleAction depense);

}
