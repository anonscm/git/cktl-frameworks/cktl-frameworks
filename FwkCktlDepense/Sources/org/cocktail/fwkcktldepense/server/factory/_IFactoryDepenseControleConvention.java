package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleConventionException;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseControleConvention {

	public _IDepenseControleConvention _copie(EOEditingContext ed, _IDepenseControleConvention depense, _IDepenseBudget budget);

	public _IDepenseControleConvention _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie,
			BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOConvention convention, EOExercice exercice,
			_IDepenseBudget depenseBudget)
			throws DepenseControleConventionException;

	public void _setPourcentage(_IDepenseControleConvention depense, BigDecimal dactPourcentage);

	public void _setHtSaisie(_IDepenseControleConvention depense, BigDecimal htSaisie);

	public void _setTvaSaisie(_IDepenseControleConvention depense, BigDecimal tvaSaisie);

	public void _setTtcSaisie(_IDepenseControleConvention depense, BigDecimal ttcSaisie);

	public void _setMontantBudgetaire(_IDepenseControleConvention depense, BigDecimal montantBudgetaire);

	public void _affecterConvention(_IDepenseControleConvention depense, EOConvention convention);

	public void _affecterExercice(_IDepenseControleConvention depense, EOExercice exercice);

	public void _affecterDepenseBudget(_IDepenseControleConvention depense, _IDepenseBudget depenseBudget);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void _supprimer(EOEditingContext ed, _IDepenseControleConvention depense) throws DepenseControleConventionException;

	public void _verifier(_IDepenseControleConvention depense);

}
