/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import org.cocktail.fwkcktldepense.server.exception.CommandeEngagementException;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCommandeEngagement extends Factory {

	public FactoryCommandeEngagement() {
		super();
	}

	public FactoryCommandeEngagement(boolean withLog) {
		super(withLog);
	}

    public EOCommandeEngagement creer(EOEditingContext ed, EOCommande commande, EOEngagementBudget engagement) throws CommandeEngagementException {

    	EOCommandeEngagement newCommandeEngagement = creer(ed);

        affecterCommande(newCommandeEngagement, commande);
        affecterEngagement(newCommandeEngagement, engagement);

        try {
        	newCommandeEngagement.validateObjectMetier();
            return newCommandeEngagement;
        } catch (CommandeEngagementException e) {
            e.printStackTrace();
            System.out.println(newCommandeEngagement);
        }

        return null;
    }

    public EOCommandeEngagement creer(EOEditingContext ed) throws CommandeEngagementException {
    	EOCommandeEngagement newCommandeEngagement=(EOCommandeEngagement)Factory.instanceForEntity(ed,EOCommandeEngagement.ENTITY_NAME);
        ed.insertObject(newCommandeEngagement);
        return newCommandeEngagement;
    }
    
    public void affecterCommande(EOCommandeEngagement commandeEngagement, EOCommande commande) {
    	commandeEngagement.setCommandeRelationship(commande);
    }

    public void affecterEngagement(EOCommandeEngagement commandeEngagement, EOEngagementBudget engagementBudget) {
    	commandeEngagement.setEngagementBudgetRelationship(engagementBudget);
    }

    /**
     * Permet de supprimer tant que ce n est pas dans la base.
     */
    public void supprimer(EOEditingContext ed, EOCommandeEngagement commandeEngagement) throws CommandeEngagementException {
        //Si l'article est enregistree
        /*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
            // l ecriture est elle valide ?
            if (EcritureDetail.ecriture().ecrEtat().equals(EOEcriture.ecritureValide))
                throw new EcritureDetailException(EcritureDetailException.supprimerDetailEcritureValide);
        }*/

       	commandeEngagement.setCommandeRelationship(null);
    	commandeEngagement.setEngagementBudgetRelationship(null);

        ed.deleteObject(commandeEngagement);
    }

    public void verifier(EOCommandeEngagement commandeEngagement) {
    	commandeEngagement.validateObjectMetier();
    }

}
