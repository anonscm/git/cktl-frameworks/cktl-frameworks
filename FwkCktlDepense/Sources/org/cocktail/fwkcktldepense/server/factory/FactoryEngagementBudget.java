/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementBudgetException;
import org.cocktail.fwkcktldepense.server.exception.EngagementControleMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryEngagementBudget extends Factory {

	public FactoryEngagementBudget() {
		super();
	}

	public FactoryEngagementBudget(boolean withLog) {
		super(withLog);
	}

	public EOEngagementBudget creer(EOEditingContext ed, String engLibelle, BigDecimal engHtSaisie, BigDecimal engTvaSaisie, 
			BigDecimal engTtcSaisie, BigDecimal engMontantBudgetaire, EOFournisseur fournisseur,
			EOSource source, EOTypeApplication typeApplication, EOUtilisateur utilisateur)
	throws EngagementBudgetException {
		return creer(ed, engLibelle, engHtSaisie, engTvaSaisie, engTtcSaisie, engMontantBudgetaire, fournisseur, source.organ(), 
				source.tauxProrata(), typeApplication, source.typeCredit(), source.exercice(), utilisateur);
	}
	
	public EOEngagementBudget creer(EOEditingContext ed, String engLibelle, BigDecimal engHtSaisie, BigDecimal engTvaSaisie, 
			BigDecimal engTtcSaisie, BigDecimal engMontantBudgetaire, EOFournisseur fournisseur,
			EOOrgan organ, EOTauxProrata tauxProrata, EOTypeApplication typeApplication, EOTypeCredit typeCredit,
			EOExercice exercice, EOUtilisateur utilisateur)
	throws EngagementBudgetException {
		
		EOEngagementBudget newEngage = creer(ed);

		affecterExercice(newEngage, exercice);

		setEngLibelle(newEngage, engLibelle);
		setEngHtSaisie(newEngage, engHtSaisie);
		setEngTvaSaisie(newEngage, engTvaSaisie);
		setEngTtcSaisie(newEngage, engTtcSaisie);
		setEngMontantBudgetaire(newEngage, engMontantBudgetaire);
		setEngMontantBudgetaireReste(newEngage, engMontantBudgetaire);
		
		affecterFournisseur(newEngage, fournisseur);
		affecterOrgan(newEngage, organ);
		affecterTauxProrata(newEngage, tauxProrata);
		affecterTypeApplication(newEngage, typeApplication);
		affecterTypeCredit(newEngage, typeCredit);
		affecterExercice(newEngage, exercice);
		affecterUtilisateur(newEngage, utilisateur);
		
		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementBudgetException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}
		
		return null;
	}
	
	public EOEngagementBudget creer(EOEditingContext ed) throws EngagementBudgetException {
		EOEngagementBudget newEngage=(EOEngagementBudget)Factory.instanceForEntity(ed,EOEngagementBudget.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}
	
	
    public void setSource(EOEngagementBudget engage, EOSource source) {
    	affecterOrgan(engage, source.organ());
    	affecterTypeCredit(engage, source.typeCredit());
    	affecterTauxProrata(engage, source.tauxProrata());
    	affecterExercice(engage, source.exercice());
    }

	public void setEngLibelle(EOEngagementBudget engage, String engLibelle) {
		if (engage==null)
			return;
		engage.setEngLibelle(engLibelle);
	}

	public void setEngHtSaisie(EOEngagementBudget engage, BigDecimal engHtSaisie) {
		if (engage==null)
			return;
		engage.setEngHtSaisie(engHtSaisie);
	}
	
	public void setEngTvaSaisie(EOEngagementBudget engage, BigDecimal engTvaSaisie) {
		if (engage==null)
			return;
		engage.setEngTvaSaisie(engTvaSaisie);
	}
	
	public void setEngTtcSaisie(EOEngagementBudget engage, BigDecimal engTtcSaisie) {
		if (engage==null)
			return;
		engage.setEngTtcSaisie(engTtcSaisie);
	}
	
	public void setEngMontantBudgetaire(EOEngagementBudget engage, BigDecimal engMontantBudgetaire) {
		if (engage==null)
			return;
		engage.setEngMontantBudgetaire(engMontantBudgetaire);
	}
	
	public void setEngMontantBudgetaireReste(EOEngagementBudget engage, BigDecimal engMontantBudgetaireReste) {
		if (engage==null)
			return;
		engage.setEngMontantBudgetaireReste(engMontantBudgetaireReste);
	}
	
	public void affecterFournisseur(EOEngagementBudget engage, EOFournisseur fournisseur) {
		engage.setFournisseurRelationship(fournisseur);
	}
	
	public void affecterOrgan(EOEngagementBudget engage, EOOrgan organ) {
		engage.setOrganRelationship(organ);
	}
	
	public void affecterTauxProrata(EOEngagementBudget engage, EOTauxProrata tauxProrata) {
		engage.setTauxProrataRelationship(tauxProrata);
	}

	public void affecterTypeApplication(EOEngagementBudget engage, EOTypeApplication typeApplication) {
		engage.setTypeApplicationRelationship(typeApplication);
	}

	public void affecterTypeCredit(EOEngagementBudget engage, EOTypeCredit typeCredit) {
		engage.setTypeCreditRelationship(typeCredit);
	}

	public void affecterUtilisateur(EOEngagementBudget engage, EOUtilisateur utilisateur) {
		engage.setUtilisateurRelationship(utilisateur);
	}
	
	public void affecterExercice(EOEngagementBudget engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}
	
	public void ajouterAttribution(EOEngagementBudget engage, EOAttribution attribution) {
		if (attribution==null)
			return;
		
		if (engage.engagementControleMarches()==null || engage.engagementControleMarches().count()==0 ||
				EOQualifier.filteredArrayWithQualifier(engage.engagementControleMarches(), 
						EOQualifier.qualifierWithQualifierFormat(EOEngagementControleMarche.ATTRIBUTION_KEY+"=%@", new NSArray(attribution))).count()==0)
			new FactoryEngagementControleMarche().creer(engage.editingContext(), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0),
					attribution, engage.exercice(), engage);
	}

	public void ajouterCodeExerTypeAchat(EOEngagementBudget engage, EOCodeExer codeExer, EOTypeAchat typeAchat) {
		if (codeExer==null || typeAchat==null)
			return;
		
		NSMutableArray array=new NSMutableArray();
		array.addObject(codeExer);
		array.addObject(typeAchat);
		
		if (engage.engagementControleHorsMarches()==null || engage.engagementControleHorsMarches().count()==0 ||
				EOQualifier.filteredArrayWithQualifier(engage.engagementControleHorsMarches(), 
						EOQualifier.qualifierWithQualifierFormat(EOEngagementControleHorsMarche.CODE_EXER_KEY+"=%@ and "+
								EOEngagementControleHorsMarche.TYPE_ACHAT_KEY+"=%@", array)).count()==0)
			new FactoryEngagementControleHorsMarche().creer(engage.editingContext(), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0),
					codeExer, typeAchat, engage.exercice(), engage);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementBudget engage) throws EngagementBudgetException {
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
			// y a t-il des liquidations ?
			if (engage.depenseBudgets().count()>0) {
				throw new EngagementControleMarcheException(EngagementControleMarcheException.supprimerEngageControle);
			}
		}
		
		engage.setFournisseurRelationship(null);
		engage.setOrganRelationship(null);
		engage.setTauxProrataRelationship(null);
		engage.setTypeApplicationRelationship(null);
		engage.setTypeCreditRelationship(null);
		engage.setUtilisateurRelationship(null);
		engage.setExerciceRelationship(null);
		
		supprimerEngagementContoleActions(engage);
    	supprimerEngagementContoleAnalytiques(engage);
    	supprimerEngagementContoleConventions(engage);
    	supprimerEngagementContoleHorsMarches(engage);
    	supprimerEngagementContoleMarches(engage);
    	supprimerEngagementContolePlanComptables(engage);

		ed.deleteObject(engage);
	}
	
	public void supprimerEngagementContoleActions(EOEngagementBudget engage) {
    	NSArray larray=engage.engagementControleActions();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryEngagementControleAction factory=new FactoryEngagementControleAction();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(engage.editingContext(), (EOEngagementControleAction)larray.objectAtIndex(i));
    	}
	}

	public void supprimerEngagementContoleAnalytiques(EOEngagementBudget engage) {
    	NSArray larray=engage.engagementControleAnalytiques();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryEngagementControleAnalytique factory=new FactoryEngagementControleAnalytique();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(engage.editingContext(), (EOEngagementControleAnalytique)larray.objectAtIndex(i));
    	}
	}

	public void supprimerEngagementContoleConventions(EOEngagementBudget engage) {
    	NSArray larray=engage.engagementControleConventions();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryEngagementControleConvention factory=new FactoryEngagementControleConvention();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(engage.editingContext(), (EOEngagementControleConvention)larray.objectAtIndex(i));
    	}
	}

	public void supprimerEngagementContoleHorsMarches(EOEngagementBudget engage) {
    	NSArray larray=engage.engagementControleHorsMarches();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryEngagementControleHorsMarche factory=new FactoryEngagementControleHorsMarche();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(engage.editingContext(), (EOEngagementControleHorsMarche)larray.objectAtIndex(i));
    	}
	}

	public void supprimerEngagementContoleMarches(EOEngagementBudget engage) {
    	NSArray larray=engage.engagementControleMarches();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryEngagementControleMarche factory=new FactoryEngagementControleMarche();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(engage.editingContext(), (EOEngagementControleMarche)larray.objectAtIndex(i));
    	}
	}
	
	public void supprimerEngagementContolePlanComptables(EOEngagementBudget engage) {
    	NSArray larray=engage.engagementControlePlanComptables();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryEngagementControlePlanComptable factory=new FactoryEngagementControlePlanComptable();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(engage.editingContext(), (EOEngagementControlePlanComptable)larray.objectAtIndex(i));
    	}
	}

	public void verifier(EOEngagementBudget engage) {
		engage.validateObjectMetier();
	}
}
