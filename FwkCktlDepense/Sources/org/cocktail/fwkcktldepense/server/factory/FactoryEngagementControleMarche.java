/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEngagementControleMarche extends Factory {

	public FactoryEngagementControleMarche() {
		super();
	}

	public FactoryEngagementControleMarche(boolean withLog) {
		super(withLog);
	}

	public EOEngagementControleMarche creer(EOEditingContext ed, BigDecimal emarHtSaisie, BigDecimal emarTvaSaisie, 
			BigDecimal emarTtcSaisie, BigDecimal emarMontantBudgetaire, EOAttribution attribution, 
			EOExercice exercice, EOEngagementBudget engagementBudget)
	throws EngagementControleMarcheException {
		
		EOEngagementControleMarche newEngage = creer(ed);
		
		setEmarHtSaisie(newEngage, emarHtSaisie);
		setEmarHtReste(newEngage, emarHtSaisie);
		setEmarTvaSaisie(newEngage, emarTvaSaisie);
		setEmarTtcSaisie(newEngage, emarTtcSaisie);
		setEmarMontantBudgetaire(newEngage, emarMontantBudgetaire);
		setEmarMontantBudgetaireReste(newEngage, emarMontantBudgetaire);
		setEmarDateSaisie(newEngage, new NSTimestamp());
		
		affecterAttribution(newEngage, attribution);
		affecterExercice(newEngage, exercice);
		affecterEngagementBudget(newEngage, engagementBudget);
		
		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementControleMarcheException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}
		
		return null;
	}
	
	public EOEngagementControleMarche creer(EOEditingContext ed) throws EngagementControleMarcheException {
		EOEngagementControleMarche newEngage=(EOEngagementControleMarche)Factory.instanceForEntity(ed,EOEngagementControleMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}
	
	
	public void setEmarHtSaisie(EOEngagementControleMarche engage, BigDecimal emarHtSaisie) {
		if (engage==null)
			return;
		engage.setEmarHtSaisie(emarHtSaisie);
	}

	public void setEmarHtReste(EOEngagementControleMarche engage, BigDecimal emarHtReste) {
		if (engage==null)
			return;
		engage.setEmarHtReste(emarHtReste);
	}
	
	public void setEmarTvaSaisie(EOEngagementControleMarche engage, BigDecimal emarTvaSaisie) {
		if (engage==null)
			return;
		engage.setEmarTvaSaisie(emarTvaSaisie);
	}
	
	public void setEmarTtcSaisie(EOEngagementControleMarche engage, BigDecimal emarTtcSaisie) {
		if (engage==null)
			return;
		engage.setEmarTtcSaisie(emarTtcSaisie);
	}
	
	public void setEmarMontantBudgetaire(EOEngagementControleMarche engage, BigDecimal emarMontantBudgetaire) {
		if (engage==null)
			return;
		engage.setEmarMontantBudgetaire(emarMontantBudgetaire);
	}
	
	public void setEmarMontantBudgetaireReste(EOEngagementControleMarche engage, BigDecimal emarMontantBudgetaireReste) {
		if (engage==null)
			return;
		engage.setEmarMontantBudgetaireReste(emarMontantBudgetaireReste);
	}

	public void setEmarDateSaisie(EOEngagementControleMarche engage, NSTimestamp emarDateSaisie) {
		if (engage==null)
			return;
		engage.setEmarDateSaisie(emarDateSaisie);
	}

	public void affecterAttribution(EOEngagementControleMarche engage, EOAttribution attribution) {
		engage.setAttributionRelationship(attribution);
	}
	
	public void affecterExercice(EOEngagementControleMarche engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}
	
	public void affecterEngagementBudget(EOEngagementControleMarche engage, EOEngagementBudget engagementBudget) {
		engage.setEngagementBudgetRelationship(engagementBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementControleMarche engage) throws EngagementControleMarcheException{
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
		}
		
		engage.setAttributionRelationship(null);
		engage.setExerciceRelationship(null);
		engage.setEngagementBudgetRelationship(null);
		
		ed.deleteObject(engage);
	}
	
	public void verifier(EOEngagementControleMarche engage) {
		engage.validateObjectMetier();
	}
}
