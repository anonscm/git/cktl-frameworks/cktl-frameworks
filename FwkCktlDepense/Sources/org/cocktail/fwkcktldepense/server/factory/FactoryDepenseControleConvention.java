/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.exception.DepenseControleConventionException;
import org.cocktail.fwkcktldepense.server.interfaces.IFactoryControle;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author tsaivre
 */
public class FactoryDepenseControleConvention extends Factory implements _IFactoryDepenseControleConvention, IFactoryControle {

	public FactoryDepenseControleConvention() {
		super();
	}

	public FactoryDepenseControleConvention(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleConvention copie(EOEditingContext ed, EODepenseControleConvention depense, EODepenseBudget budget) {

		EODepenseControleConvention newDepense = creer(ed);

		setDconHtSaisie(newDepense, depense.dconHtSaisie());
		setDconTvaSaisie(newDepense, depense.dconTvaSaisie());
		setDconTtcSaisie(newDepense, depense.dconTtcSaisie());
		setDconMontantBudgetaire(newDepense, depense.dconMontantBudgetaire());

		affecterConvention(newDepense, depense.convention());
		affecterExercice(newDepense, depense.exercice());
		affecterDepenseBudget(newDepense, budget);

		setDconPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EODepenseControleConvention creer(EOEditingContext ed, BigDecimal pourcentage, Object code, EOExercice exercice,
			EODepenseBudget depenseBudget) throws DepenseControleAnalytiqueException {
		return creer(ed, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, pourcentage, (EOConvention) code, exercice, depenseBudget);
	}

	public EODepenseControleConvention creer(EOEditingContext ed, BigDecimal dconHtSaisie, BigDecimal dconTvaSaisie,
			BigDecimal dconTtcSaisie, BigDecimal dconMontantBudgetaire, BigDecimal dconPourcentage, EOConvention convention, EOExercice exercice,
			EODepenseBudget depenseBudget)
			throws DepenseControleConventionException {

		EODepenseControleConvention newDepense = creer(ed);

		setDconHtSaisie(newDepense, dconHtSaisie);
		setDconTvaSaisie(newDepense, dconTvaSaisie);
		setDconTtcSaisie(newDepense, dconTtcSaisie);
		setDconMontantBudgetaire(newDepense, dconMontantBudgetaire);

		affecterConvention(newDepense, convention);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDconPourcentage(newDepense, dconPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleConventionException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EODepenseControleConvention creer(EOEditingContext ed) throws DepenseControleConventionException {
		EODepenseControleConvention newDepense = (EODepenseControleConvention) Factory.
				instanceForEntity(ed, EODepenseControleConvention.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setDconPourcentage(EODepenseControleConvention depense, BigDecimal dconPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dconPourcentage);
	}

	public void setDconHtSaisie(EODepenseControleConvention depense, BigDecimal dconHtSaisie) {
		if (depense == null)
			return;
		depense.setDconHtSaisie(dconHtSaisie);
	}

	public void setDconTvaSaisie(EODepenseControleConvention depense, BigDecimal dconTvaSaisie) {
		if (depense == null)
			return;
		depense.setDconTvaSaisie(dconTvaSaisie);
	}

	public void setDconTtcSaisie(EODepenseControleConvention depense, BigDecimal dconTtcSaisie) {
		if (depense == null)
			return;
		depense.setDconTtcSaisie(dconTtcSaisie);
	}

	public void setDconMontantBudgetaire(EODepenseControleConvention depense, BigDecimal dconMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDconMontantBudgetaire(dconMontantBudgetaire);
	}

	public void affecterConvention(EODepenseControleConvention depense, EOConvention convention) {
		depense.setConventionRelationship(convention);
	}

	public void affecterExercice(EODepenseControleConvention depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterDepenseBudget(EODepenseControleConvention depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleConvention depense) throws DepenseControleConventionException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setConventionRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EODepenseControleConvention depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleConvention _copie(EOEditingContext ed, _IDepenseControleConvention depense, _IDepenseBudget budget) {
		return copie(ed, (EODepenseControleConvention) depense, (EODepenseBudget) budget);
	}

	public _IDepenseControleConvention _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOConvention convention, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleConventionException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, convention, exercice, (EODepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControleConvention depense, BigDecimal dactPourcentage) {
		setDconPourcentage((EODepenseControleConvention) depense, dactPourcentage);

	}

	public void _setHtSaisie(_IDepenseControleConvention depense, BigDecimal htSaisie) {
		setDconHtSaisie((EODepenseControleConvention) depense, htSaisie);

	}

	public void _setTvaSaisie(_IDepenseControleConvention depense, BigDecimal tvaSaisie) {
		setDconTvaSaisie((EODepenseControleConvention) depense, tvaSaisie);

	}

	public void _setTtcSaisie(_IDepenseControleConvention depense, BigDecimal ttcSaisie) {
		setDconTtcSaisie((EODepenseControleConvention) depense, ttcSaisie);

	}

	public void _setMontantBudgetaire(_IDepenseControleConvention depense, BigDecimal montantBudgetaire) {
		setDconMontantBudgetaire((EODepenseControleConvention) depense, montantBudgetaire);

	}

	public void _affecterConvention(_IDepenseControleConvention depense, EOConvention convention) {
		affecterConvention((EODepenseControleConvention) depense, convention);

	}

	public void _affecterExercice(_IDepenseControleConvention depense, EOExercice exercice) {
		affecterExercice((EODepenseControleConvention) depense, exercice);

	}

	public void _affecterDepenseBudget(_IDepenseControleConvention depense, _IDepenseBudget depenseBudget) {
		affecterDepenseBudget((EODepenseControleConvention) depense, (EODepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControleConvention depense) throws DepenseControleConventionException {
		supprimer(ed, (EODepenseControleConvention) depense);

	}

	public void _verifier(_IDepenseControleConvention depense) {
		verifier((EODepenseControleConvention) depense);

	}
}
