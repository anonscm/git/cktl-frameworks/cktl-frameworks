/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldepense.server.exception.CommandeImpressionException;
import org.cocktail.fwkcktldepense.server.impression.XMLCommande;
import org.cocktail.fwkcktldepense.server.metier.EOAdresse;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression;
import org.cocktail.fwkcktldepense.server.metier.EOStructure;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryCommandeImpression extends Factory {
	public static Logger LOGGER = Logger.getLogger(FactoryCommandeImpression.class.getName());
	private static CktlPrinter printer;
	private static String MAQUETTE = XMLCommande.MAQUETTE;
	private static int TIMEOUT = 500;

	public FactoryCommandeImpression() {
		super();
	}

	public FactoryCommandeImpression(boolean withLog) {
		super(withLog);
	}

	public FactoryCommandeImpression(Hashtable config, String maquette, int jobAliveTimeOut) {
		super();
		// On cree et on initialise le pilote d'impression
		try {
			printer = CktlPrinter.newDefaultInstance(config);
			if (maquette != null)
				MAQUETTE = maquette;
			if (jobAliveTimeOut > 0)
				TIMEOUT = jobAliveTimeOut;
		} catch (ClassNotFoundException e) {
			// Le pilote n'a pas ete trouve
			e.printStackTrace();
			throw new CommandeImpressionException(CommandeImpressionException.cimpPiloteIntrouvable);
		}
	}

	public int imprimer(EOCommandeImpression commandeImpression) throws CommandeImpressionException {
		return imprimer(commandeImpression, false, false);
	}

	public int imprimer(EOCommandeImpression commandeImpression, boolean isNePasAfficherMontants, boolean debug) throws CommandeImpressionException {
		int jobId = -1;
		StringWriter myStringWriter = new StringWriter();
		CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
		try {
			XMLCommande.commandeInXml(myCktlXMLWriter, commandeImpression, isNePasAfficherMontants);
			myCktlXMLWriter.close();
			FileOutputStream fs;

			try {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("[Carambole_Bon_De_Commande_" + commandeImpression.commande().commNumero() + "] XML transmis à SIX=" + myStringWriter.toString());
				}

				if (debug) {
					CktlLog.log("Enregistrement du fichier 'Carambole_Bon_De_Commande.xml' dans " + System.getProperty("java.io.tmpdir"));
					fs = new FileOutputStream(System.getProperty("java.io.tmpdir") + "/Carambole_Bon_De_Commande_" + commandeImpression.commande().commNumero() + ".xml");
					fs.write(myStringWriter.toString().getBytes());
					fs.close();
				}
				byte[] xmlbytes = myStringWriter.toString().getBytes();
				ByteArrayInputStream xmlstream = new ByteArrayInputStream(xmlbytes);

				if (xmlbytes.length == 0) {
					throw new CommandeImpressionException(CommandeImpressionException.erreurTailleFluxDonneesXMLNulle);
				}

				jobId = genererPDF(xmlstream, xmlbytes.length, MAQUETTE, TIMEOUT);

				CktlLog.trace("Flux PDF genere avec jobid=" + jobId);

			} catch (Exception e) {
				e.printStackTrace();
				throw new CommandeImpressionException(CommandeImpressionException.erreurGenerationPDF + " : " + e.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommandeImpressionException(CommandeImpressionException.erreurGenerationPDF + " : " + e.getMessage());
		}

		return jobId;
	}

	/**
	 * @param xmlstream
	 * @param streamSize
	 * @return le numero du job
	 * @throws Exception
	 */
	private int genererPDF(InputStream xmlstream, int streamSize, String maquetteID, int jobAliveTimeOut) throws Exception {
		CktlLog.trace("Generation du pdf");
		CktlLog.trace("Taille du flux xml", "" + streamSize);

		// C'est ce qu'on va imprimer
		//		String dataFile = "/tmp/donnees.xml";
		//		String resultFile = "/tmp/.pdf";
		//String maquetteID = "JEFY_BONCOMMANDE";

		if (!printer.checkTemplate(maquetteID)) {
			throw new Exception("Le modele " + maquetteID + " n est pas present sur le serveur d impression");
		}

		// On fait appel a l'impression. On restera bloque jusqu'a la
		// fin de l'impression.
		//printer.printFileImmediate(maquetteID, arg1, arg2)

		//On conserve le resultat de l'impression pendant 5 mn
		printer.setJobAliveTimeout(jobAliveTimeOut);

		int jobId = printer.printFileDiffered(maquetteID, xmlstream, streamSize);
		// On continue s'il n'y a pas d'erreurs.
		if (printer.hasSuccess()) {
			// OK. On attend la fin de creation du document...
			// On pose les verrous pour pouvoir executer "wait"
			synchronized (this) {
				do {
					// On attend 1 seconde avant de redemander
					try {
						wait(1000);
					} catch (Exception e) {
					}
					// On verifie l'etat d'impression et on s'arrete si c'est fini
					printer.getPrintStatus(jobId);
				} while (printer.isPrintInProgress());
				// Les deux dernieres lignes sont equivalentes a la condition
				// while (printer.getPrintStatus(jobId) == LRPrintConst.IN_PROGRESS);
			}
		}
		else {
			throw new Exception("La generation du flux PDF sur le serveur d'impression (jobid=" + jobId + " ) a provoque des erreurs. Les erreurs doivent etre disponibles dans le log du serveur d'impression.");
		}

		//pdfStream = printer.printFileImmediate(maquetteID,xmlstream, streamSize );

		return jobId;
	}

	/**
	 * Renvoie le taille du flux pdf e partir d'un jobid.
	 * 
	 * @param jobId
	 * @return
	 * @throws Exception
	 */
	public long getPdfStreamSize(int jobId) throws Exception {
		long taille = 0;
		try {
			taille = printer.getContentSize();
		} catch (Exception e) {
			throw new Exception("La longueur du flux PDF genere est nulle.");
		}
		return taille;
	}

	/**
	 * Renvoie le flux pdf e partir d'un jobid.
	 * 
	 * @param jobId
	 * @return
	 * @throws Exception
	 */
	public InputStream getPdfStream(int jobId) throws Exception {
		if (getPdfStreamSize(jobId) == 0) {
			throw new Exception("La longueur du flux PDF genere est nulle.");
		}

		InputStream loc = printer.getPrintResult(jobId);
		return loc;
	}

	public EOCommandeImpression creer(EOEditingContext ed, String cimpInfosImpression, NSTimestamp cimpDate,
			NSTimestamp cimpDateLivraison, String cimpLivraisonFax, String cimpLivraisonTelephone,
			String cimpServiceFax, String cimpServiceTelephone, EOAdresse adresseFournisseur, EOAdresse adresseLivraison,
			EOAdresse adresseService, EOStructure structureLivraison, EOStructure structureService,
			EOUtilisateur utilisateur, EOCommande commande) throws CommandeImpressionException {

		EOCommandeImpression newCommandeImpression = creer(ed);

		setCimpInfosImpression(newCommandeImpression, cimpInfosImpression);
		setCimpDate(newCommandeImpression, cimpDate);
		setCimpDateLivraison(newCommandeImpression, cimpDateLivraison);
		setCimpLivraisonFax(newCommandeImpression, cimpLivraisonFax);
		setCimpLivraisonTelephone(newCommandeImpression, cimpLivraisonTelephone);
		setCimpServiceFax(newCommandeImpression, cimpServiceFax);
		setCimpServiceTelephone(newCommandeImpression, cimpServiceTelephone);

		affecterAdresseFournisseur(newCommandeImpression, adresseFournisseur);
		affecterAdresseLivraison(newCommandeImpression, adresseLivraison);
		affecterAdresseService(newCommandeImpression, adresseService);
		affecterStructureLivraison(newCommandeImpression, structureLivraison);
		affecterStructureService(newCommandeImpression, structureService);
		affecterUtilisateur(newCommandeImpression, utilisateur);
		affecterCommande(newCommandeImpression, commande);

		try {
			newCommandeImpression.validateObjectMetier();
			return newCommandeImpression;
		} catch (CommandeImpressionException e) {
			e.printStackTrace();
			System.out.println(newCommandeImpression);
			throw e;
		}

		// return null;
	}

	public EOCommandeImpression creer(EOEditingContext ed) throws CommandeImpressionException {
		EOCommandeImpression newCommandeImpression = (EOCommandeImpression) Factory.instanceForEntity(ed, EOCommandeImpression.ENTITY_NAME);
		ed.insertObject(newCommandeImpression);
		return newCommandeImpression;
	}

	public void setCimpInfosImpression(EOCommandeImpression commandeImpression, String cimpInfosImpression) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpInfosImpression(cimpInfosImpression);
	}

	public void setCimpDate(EOCommandeImpression commandeImpression, NSTimestamp cimpDate) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpDate(cimpDate);
	}

	public void setCimpDateLivraison(EOCommandeImpression commandeImpression, NSTimestamp date) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpDateLivraison(date);
	}

	public void setCimpLivraisonFax(EOCommandeImpression commandeImpression, String fax) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpLivraisonFax(fax);
	}

	public void setCimpLivraisonTelephone(EOCommandeImpression commandeImpression, String tel) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpLivraisonTelephone(tel);
	}

	public void setCimpServiceFax(EOCommandeImpression commandeImpression, String fax) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpServiceFax(fax);
	}

	public void setCimpServiceTelephone(EOCommandeImpression commandeImpression, String tel) {
		if (commandeImpression == null)
			return;
		commandeImpression.setCimpServiceTelephone(tel);
	}

	public void affecterAdresseFournisseur(EOCommandeImpression commandeImpression, EOAdresse adresse) {
		commandeImpression.setAdresseFournisseurRelationship(adresse);
	}

	public void affecterAdresseLivraison(EOCommandeImpression commandeImpression, EOAdresse adresse) {
		commandeImpression.setAdresseLivraisonRelationship(adresse);
	}

	public void affecterAdresseService(EOCommandeImpression commandeImpression, EOAdresse adresse) {
		commandeImpression.setAdresseServiceRelationship(adresse);
	}

	public void affecterStructureLivraison(EOCommandeImpression commandeImpression, EOStructure structure) {
		commandeImpression.setStructureLivraisonRelationship(structure);
	}

	public void affecterStructureService(EOCommandeImpression commandeImpression, EOStructure structure) {
		commandeImpression.setStructureServiceRelationship(structure);
	}

	public void affecterUtilisateur(EOCommandeImpression commandeImpression, EOUtilisateur utilisateur) {
		commandeImpression.setUtilisateurRelationship(utilisateur);
	}

	public void affecterCommande(EOCommandeImpression commandeImpression, EOCommande commande) {
		commandeImpression.setCommandeRelationship(commande);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeImpression commandeImpression) throws CommandeImpressionException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		commandeImpression.setUtilisateurRelationship(null);
		commandeImpression.setCommandeRelationship(null);
		commandeImpression.setAdresseFournisseurRelationship(null);
		commandeImpression.setAdresseLivraisonRelationship(null);
		commandeImpression.setAdresseServiceRelationship(null);
		commandeImpression.setStructureLivraisonRelationship(null);
		commandeImpression.setStructureServiceRelationship(null);

		ed.deleteObject(commandeImpression);
	}

	public void verifier(EOCommandeImpression commandeImpression) {
		commandeImpression.validateObjectMetier();
	}
}
