/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn;
import org.cocktail.fwkcktldepense.server.b2b.factory.FactoryCxmlItem;
import org.cocktail.fwkcktldepense.server.exception.ArticleException;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderTva;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogue;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldepense.server.metier.xml.EOXMLArticle;
import org.cocktail.fwkcktlwebapp.server.util.CktlHtmlParser;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;

public class FactoryArticle extends Factory {

	public FactoryArticle() {
		super();
	}

	public FactoryArticle(boolean withLog) {
		super(withLog);
	}

	public EOArticle dupliquer(EOEditingContext ed, EOArticle article, EOCommande commande) throws ArticleException {
		if (article == null || article.codeExer() == null || commande.exercice() == null)
			return null;

		EOCodeExer newCodeExer = article.codeExer();
		if (article.codeExer().exercice() != null && !article.codeExer().exercice().exeExercice().equals(commande.exercice()))
			newCodeExer = FinderCodeExer.getCodeExerEquivalentPourExercice(ed, article.codeExer(), commande.exercice());

		if (article.attribution() != null && commande != null && !article.attribution().isValideForDate(commande.commDateCreation()))
			throw new ArticleException(ArticleException.attributionInvalide);

		EOArticle res = creer(ed, article.artReference(), article.artLibelle(), article.artQuantite(),
				article.artPrixHt(), article.artPrixTtc(), article.artPrixTotalHt(), article.artPrixTotalTtc(), commande,
				article.tva(), newCodeExer, article.typeAchat(), article.attribution(), article.articleCatalogue(), null /* article.article() */,
				false);

		//si infos b2b on les duplique aussi
		if (article.toB2bCxmlItem() != null) {
			FactoryCxmlItem f = new FactoryCxmlItem();
			f.dupliquerEOB2bCxmlItem(ed, commande.utilisateur().persId(), article, article.toB2bCxmlItem());
		}

		return res;
	}

	public EOArticle dupliquer(EOEditingContext ed, EOCatalogue article, EOCommande commande, EOAttribution attribution, EOTypeAchat typeAchat)
			throws ArticleException {

		if (article == null || article.codeExer() == null || commande.exercice() == null)
			return null;

		EOCodeExer newCodeExer = article.codeExer();
		if (article.codeExer().exercice() != null && !article.codeExer().exercice().exeExercice().equals(commande.exercice()))
			newCodeExer = FinderCodeExer.getCodeExerEquivalentPourExercice(ed, article.codeExer(), commande.exercice());

		if (newCodeExer == null)
			throw new ArticleException(ArticleException.codeExerManquant);

		if (typeAchat == null) {
			if (attribution == null && article.attribution() != null)
				attribution = article.attribution();

			if (attribution != null && (article.attribution() != null && !attribution.equals(article.attribution())))
				throw new ArticleException(ArticleException.attributionIncoherente);
			if (attribution != null && commande != null && !attribution.isValideForDate(commande.commDateCreation()))
				throw new ArticleException(ArticleException.attributionInvalide);
		}
		if (attribution == null && typeAchat == null)
			throw new ArticleException(ArticleException.attributionOuTypeAchat);
		if (attribution != null && typeAchat != null)
			throw new ArticleException(ArticleException.attributionOuTypeAchat);

		EOArticle res = creer(ed, article.reference(), article.libelle(), new BigDecimal(1.0),
				article.prixHt(), article.prixTtc(), article.prixHt(), article.prixTtc(), commande,
				article.tva(), newCodeExer, typeAchat, attribution, article.larticleCatalogue(), null,
				false);

		return res;
	}

	public EOArticle creer(EOEditingContext ed, String artReference, String artLibelle, BigDecimal artQuantite,
			BigDecimal artPrixHt, BigDecimal artPrixTtc, BigDecimal artPrixTotalHt, BigDecimal artPrixTotalTtc, EOCommande commande,
			EOTva tva, EOCodeExer codeExer, EOTypeAchat typeAchat, EOAttribution attribution, EOCatalogueArticle articleCatalogue, EOArticle article,
			boolean ajoutCatalogue)
			throws ArticleException {

		EOArticle newArticle = creer(ed);

		setArtReference(newArticle, artReference);
		setArtLibelle(newArticle, artLibelle);
		setArtQuantite(newArticle, artQuantite);
		setArtPrixHt(newArticle, artPrixHt);
		setArtPrixTotalHt(newArticle, artPrixTotalHt);

		affecterTva(newArticle, tva);
		setArtPrixTtc(newArticle, artPrixTtc);
		setArtPrixTotalTtc(newArticle, artPrixTotalTtc);
		affecterCommande(newArticle, commande);
		affecterTypeAchat(newArticle, typeAchat);
		affecterCodeExer(newArticle, codeExer);
		affecterAttribution(newArticle, attribution);
		affecterCatalogueArticle(newArticle, articleCatalogue);
		affecterArticlePere(newArticle, article);

		newArticle.setIsCatalogable(ajoutCatalogue);

		majCommande(commande, codeExer, typeAchat);

		try {
			//newArticle.validateObjectMetier();
			return newArticle;
		} catch (ArticleException e) {
			affecterCommande(newArticle, null);
			majCommande(commande, codeExer, typeAchat);
			e.printStackTrace();
			System.out.println(newArticle);
			throw e;
		}

	}

	public EOArticle creer(EOEditingContext edc, EOXMLArticle unArticle) throws ArticleException {
		EOArticle newArticle = creer(edc);
		newArticle.setArtReference(unArticle.reference());
		newArticle.setArtLibelle(unArticle.libelle());
		newArticle.setArtQuantite(unArticle.quantite());
		newArticle.setArtPrixHt(unArticle.prixHt());
		newArticle.setArtPrixTtc(unArticle.prixTtc());
		newArticle.setArtPrixTotalHt(unArticle.prixTotalHt());
		newArticle.setArtPrixTotalTtc(unArticle.prixTotalTtc());

		return newArticle;
	}

	/**
	 * Cree un article prevenant du b2b cxml.
	 *
	 * @param edc
	 * @param b2bCxmlItemIn
	 * @param utilisateur
	 * @param fournisCxmlParam
	 * @return
	 * @throws ArticleException
	 */
	public EOArticle creer(EOEditingContext edc, EOItemIn b2bCxmlItemIn, EOUtilisateur utilisateur, EOFournisB2bCxmlParam fournisCxmlParam) throws ArticleException {
		EOArticle newArticle = creer(edc);
		//Affecter une tva par defaut
		EOTva tva = FinderTva.getTvaForTaux(edc, new BigDecimal(0.00));
		BigDecimal ttc = b2bCxmlItemIn.itemDetail().unitPrice().money().getVal();
		BigDecimal ht = ttc;
		//au cas ou la tva est transmise, mais a priori ca n'arrive pas (on utilise la tva definie dans les params)
		if (b2bCxmlItemIn.tax() != null) {
			BigDecimal tax = b2bCxmlItemIn.tax().money().getVal();
			if (tax != null) {
				ht = ttc.add(tax.negate());
				BigDecimal taux = tax.multiply(new BigDecimal(100.00)).divide(ht, 2, BigDecimal.ROUND_HALF_UP);
				if (taux.doubleValue() > 0.00) {
					EOTva tvaNew = FinderTva.getTvaForTaux(edc, taux);
					if (tvaNew != null) {
						tva = tvaNew;
					}
				}
			}
		}
		newArticle.setTva(tva);
		newArticle.setArtReference(b2bCxmlItemIn.itemID().supplierPartID());
		//		newArticle.setArtLibelle(b2bCxmlItemIn.itemDetail().description().getVal());
		newArticle.setArtLibelle(CktlHtmlParser.html2text(CktlHtmlParser.html2text(b2bCxmlItemIn.itemDetail().description().getVal())));
		newArticle.setArtQuantite(BigDecimal.valueOf(b2bCxmlItemIn.quantity().longValue()));
		newArticle.setArtPrixHt(ht);
		newArticle.setArtPrixTtc(ttc);

		//newArticle.setArtPrixHt(b2bCxmlItemIn.itemDetail().unitPrice().money().getVal());

		//le clacul du ttc est normalement effectue lors du setArtPrixHt
		//newArticle.setArtPrixTtc(unArticle.prixTtc());
		//newArticle.setArtPrixTotalHt(unArticle.prixTotalHt());
		//newArticle.setArtPrixTotalTtc(unArticle.prixTotalTtc());

		FactoryCxmlItem f = new FactoryCxmlItem();
		f.creerEOB2bCxmlItem(edc, utilisateur.persId(), newArticle, fournisCxmlParam, b2bCxmlItemIn);

		return newArticle;
	}

	public EOArticle creer(EOEditingContext ed) throws ArticleException {
		EOArticle newArticle = (EOArticle) Factory.instanceForEntity(ed, EOArticle.ENTITY_NAME);
		ed.insertObject(newArticle);
		return newArticle;
	}

	public void modifierArticle(EOArticle article) {
		majCommande(article.commande(), article.codeExer(), article.typeAchat());
	}

	public void setArtReference(EOArticle article, String artReference) {
		if (article == null)
			return;
		article.setArtReference(artReference);
	}

	public void setArtLibelle(EOArticle article, String artLibelle) {
		if (article == null)
			return;
		article.setArtLibelle(artLibelle);
	}

	public void setArtQuantite(EOArticle article, BigDecimal artQuantite) {
		if (article == null)
			return;
		article.setArtQuantite(artQuantite);
	}

	public void setArtPrixHt(EOArticle article, BigDecimal artPrixHt) {
		if (article == null)
			return;
		article.setArtPrixHt(artPrixHt);
	}

	public void setArtPrixTtc(EOArticle article, BigDecimal artPrixTtc) {
		if (article == null)
			return;
		article.setArtPrixTtc(artPrixTtc);
	}

	private void setArtPrixTotalHt(EOArticle article, BigDecimal artPrixTotalHt) {
		if (article == null)
			return;
		if (article.artPrixHt() == null)
			throw new ArticleException(ArticleException.artPrixHtManquant);
		if (article.artQuantite() == null)
			throw new ArticleException(ArticleException.artQuantiteManquant);
		article.setArtPrixTotalHt(artPrixTotalHt);
	}

	private void setArtPrixTotalTtc(EOArticle article, BigDecimal artPrixTotalTtc) {
		if (article == null)
			return;
		if (article.artPrixTtc() == null)
			throw new ArticleException(ArticleException.artPrixTtcManquant);
		if (article.artQuantite() == null)
			throw new ArticleException(ArticleException.artQuantiteManquant);
		article.setArtPrixTotalTtc(artPrixTotalTtc);
	}

	public void affecterCommande(EOArticle article, EOCommande commande) {
		article.setCommandeRelationship(commande);
	}

	public void affecterTva(EOArticle article, EOTva tva) {
		article.setTvaRelationship(tva);
	}

	public void affecterTypeAchat(EOArticle article, EOTypeAchat typeAchat) {
		article.setTypeAchatRelationship(typeAchat);
	}

	public void affecterCodeExer(EOArticle article, EOCodeExer codeExer) {
		if (codeExer != null) {
			Integer parametre = FinderParametre.getParametreCmNiveau(article.editingContext(), codeExer.exercice());

			if (parametre == null)
				return;
			if (!codeExer.codeMarche().cmNiveau().equals(parametre))
				throw new ArticleException(ArticleException.codeExerPasBonNiveau);
		}

		article.setCodeExerRelationship(codeExer);
	}

	public void affecterAttribution(EOArticle article, EOAttribution attribution) {
		article.setAttributionRelationship(attribution);
	}

	public void affecterCatalogueArticle(EOArticle article, EOCatalogueArticle articleCatalogue) {
		article.setArticleCatalogueRelationship(articleCatalogue);
	}

	public void affecterArticlePere(EOArticle article, EOArticle articlePere) {
		article.setArticleRelationship(articlePere);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOArticle article) throws ArticleException {

		EOCommande commande = article.commande();
		EOCodeExer codeExer = article.codeExer();
		EOTypeAchat typeAchat = article.typeAchat();

		if (EOUtilities.primaryKeyForObject(ed, commande) != null)
			commande.setMonAttribution(article.attribution());

		article.setCommandeRelationship(null);
		article.setTvaRelationship(null);
		article.setCodeExerRelationship(null);
		article.setAttributionRelationship(null);
		article.setArticleCatalogueRelationship(null);
		article.setArticleRelationship(null);

		// TODO: pour Thierry: a etudier pour les precommandes
		if ((commande.articles() == null || commande.articles().count() == 0) &&
				(EOUtilities.primaryKeyForObject(ed, commande) == null ||
				commande.typeEtat().tyetLibelle().equals(EOCommande.ETAT_PRECOMMANDE))) {
			// Cas d'une commande en cours de creation ou d'une precommande
			commande.setFournisseurRelationship(null);
		}
		majCommande(commande, codeExer, typeAchat);

		ed.deleteObject(article);
	}

	public void verifier(EOArticle article) {
		article.validateObjectMetier();
	}

	public void majCommande(EOCommande commande, EOCodeExer codeExer, EOTypeAchat typeAchat) {
		if (commande == null)
			return;

		// TODO : peut etre faire une methode qui n'appelle qu'avec codeexer et typeachat
		commande.reconstruireTableaux();
	}

}
