/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryCommandeControlePlanComptable extends Factory {
	
	public FactoryCommandeControlePlanComptable() {
		super();
	}
	
	protected NSArray dupliquer(EOEditingContext ed, EOCommandeBudget origine, EOCommandeBudget commandeBudget) throws CommandeControlePlanComptableException {
		NSMutableArray array=new NSMutableArray();
		
		for (int i=0; i<origine.commandeControlePlanComptables().count(); i++) {
			EOCommandeControlePlanComptable controle=(EOCommandeControlePlanComptable)origine.commandeControlePlanComptables().objectAtIndex(i);
			
			
			EOPlanComptable newPlanComptable=controle.planComptable();
			if (!commandeBudget.exercice().exeExercice().equals(origine.exercice().exeExercice())) {
				newPlanComptable=FinderPlanComptable.getPlanComptable(ed, commandeBudget.exercice(), newPlanComptable.pcoNum());
			}

			array.addObject(creer(ed, controle.cpcoHtSaisie(), controle.cpcoTvaSaisie(), controle.cpcoTtcSaisie(), 
					controle.cpcoMontantBudgetaire(), controle.cpcoPourcentage(), newPlanComptable, commandeBudget.exercice(), commandeBudget));
		}
		
		return array;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, NSArray controles) {
		for (int i=0; i<controles.count(); i++) {
			EOCommandeControlePlanComptable controle=(EOCommandeControlePlanComptable)controles.objectAtIndex(i);
			if (!verifier(exercice, utilisateur, organ, typeCredit, controle))
				return false;
		}
		return true;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOCommandeControlePlanComptable controle) {
		EOPlanComptable newPlanComptable=controle.planComptable();
		if (!exercice.exeExercice().equals(controle.exercice().exeExercice())) {
			newPlanComptable=FinderPlanComptable.getPlanComptable(controle.editingContext(), exercice, newPlanComptable.pcoNum());
		}
		return verifier(exercice, utilisateur, organ, typeCredit, newPlanComptable);
	}
	
	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOPlanComptable planComptable) {
		if (exercice==null || utilisateur==null || organ==null || typeCredit==null || planComptable==null)
			return false;
		if (!planComptable.pcoValidite().equals(EOPlanComptable.PCO_VALIDE))
			return false;
		
		return true;
	}

	public FactoryCommandeControlePlanComptable(boolean withLog) {
		super(withLog);
	}
	
	public EOCommandeControlePlanComptable creer(EOEditingContext ed, BigDecimal cpcoHtSaisie, BigDecimal cpcoTvaSaisie, 
			BigDecimal cpcoTtcSaisie, BigDecimal cpcoMontantBudgetaire, BigDecimal cpcoPourcentage, 
			EOPlanComptable planComptable, EOExercice exercice, EOCommandeBudget commandeBudget)
	throws CommandeControlePlanComptableException {
		
		EOCommandeControlePlanComptable newCommande = creer(ed);
		
		setCpcoHtSaisie(newCommande, cpcoHtSaisie);
		setCpcoTvaSaisie(newCommande, cpcoTvaSaisie);
		setCpcoTtcSaisie(newCommande, cpcoTtcSaisie);
		setCpcoMontantBudgetaire(newCommande, cpcoMontantBudgetaire);
		setCpcoPourcentage(newCommande, cpcoPourcentage);
		
		affecterPlanComptable(newCommande, planComptable);
		affecterExercice(newCommande, exercice);
		affecterCommandeBudget(newCommande, commandeBudget);
		
		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeControlePlanComptableException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}
		
		return null;
	}
	
	public EOCommandeControlePlanComptable creer(EOEditingContext ed) throws CommandeControlePlanComptableException {
		EOCommandeControlePlanComptable newCommande=(EOCommandeControlePlanComptable)Factory.instanceForEntity(ed,EOCommandeControlePlanComptable.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newCommande);
		return newCommande;
	}
	
	
	public void setCpcoHtSaisie(EOCommandeControlePlanComptable commande, BigDecimal cpcoHtSaisie) {
		if (commande==null)
			return;
		commande.setCpcoHtSaisie(cpcoHtSaisie);
	}
	
	public void setCpcoTvaSaisie(EOCommandeControlePlanComptable commande, BigDecimal cpcoTvaSaisie) {
		if (commande==null)
			return;
		commande.setCpcoTvaSaisie(cpcoTvaSaisie);
	}
	
	public void setCpcoTtcSaisie(EOCommandeControlePlanComptable commande, BigDecimal cpcoTtcSaisie) {
		if (commande==null)
			return;
		commande.setCpcoTtcSaisie(cpcoTtcSaisie);
	}
	
	public void setCpcoMontantBudgetaire(EOCommandeControlePlanComptable commande, BigDecimal cpcoMontantBudgetaire) {
		if (commande==null)
			return;
		commande.setCpcoMontantBudgetaire(cpcoMontantBudgetaire);
	}
	
	public void setCpcoPourcentage(EOCommandeControlePlanComptable commande, BigDecimal cpcoPourcentage) {
		if (commande==null)
			return;
		commande.setCpcoPourcentage(cpcoPourcentage);
	}

	public void affecterPlanComptable(EOCommandeControlePlanComptable commande, EOPlanComptable planComptable) {
		commande.setPlanComptableRelationship(planComptable);
	}
	
	public void affecterExercice(EOCommandeControlePlanComptable commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
	}
	
	public void affecterCommandeBudget(EOCommandeControlePlanComptable commande, EOCommandeBudget commandeBudget) {
		commande.setCommandeBudgetRelationship(commandeBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeControlePlanComptable commande) throws CommandeControlePlanComptableException {
		commande.setPlanComptableRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setCommandeBudgetRelationship(null);

		ed.deleteObject(commande);
	}
	
	public void verifier(EOCommandeControlePlanComptable commande) {
		commande.validateObjectMetier();
	}
}
