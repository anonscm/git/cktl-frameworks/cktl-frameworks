package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.exception.DepenseControleMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleMarche;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseControleMarche {

	public _IDepenseControleMarche _copie(EOEditingContext ed, _IDepenseControleMarche depense, _IDepenseBudget budget);

	public _IDepenseControleMarche _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOAttribution attribution, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleMarcheException;

	public void _setPourcentage(_IDepenseControleMarche depense, BigDecimal pourcentage);

	public void setHtSaisie(_IDepenseControleMarche depense, BigDecimal htSaisie);

	public void setTvaSaisie(_IDepenseControleMarche depense, BigDecimal tvaSaisie);

	public void setTtcSaisie(_IDepenseControleMarche depense, BigDecimal ttcSaisie);

	public void setMontantBudgetaire(_IDepenseControleMarche depense, BigDecimal montantBudgetaire);

	public void _affecterAttribution(_IDepenseControleMarche depense, EOAttribution attibution);

	public void _affecterExercice(_IDepenseControleMarche depense, EOExercice exercice);

	public void _affecterDepenseBudget(_IDepenseControleMarche depense, _IDepenseBudget depenseBudget);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void _supprimer(EOEditingContext ed, _IDepenseControleMarche depense) throws DepenseControleHorsMarcheException;

	public void _verifier(_IDepenseControleMarche depense);
}
