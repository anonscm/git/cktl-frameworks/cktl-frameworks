package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseControlePlanComptable {

	public _IDepenseControlePlanComptable _copie(EOEditingContext ed, _IDepenseControlePlanComptable depense, _IDepenseBudget budget);

	public _IDepenseControlePlanComptable _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie,
			BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOPlanComptable planComptable, EOExercice exercice,
			_IDepenseBudget depenseBudget)
			throws DepenseControlePlanComptableException;

	public void _setPourcentage(_IDepenseControlePlanComptable depense, BigDecimal dactPourcentage);

	public void _setHtSaisie(_IDepenseControlePlanComptable depense, BigDecimal htSaisie);

	public void _setTvaSaisie(_IDepenseControlePlanComptable depense, BigDecimal tvaSaisie);

	public void _setTtcSaisie(_IDepenseControlePlanComptable depense, BigDecimal ttcSaisie);

	public void _setMontantBudgetaire(_IDepenseControlePlanComptable depense, BigDecimal montantBudgetaire);

	public void _affecterPlanComptable(_IDepenseControlePlanComptable depense, EOPlanComptable planComptable);

	public void _affecterExercice(_IDepenseControlePlanComptable depense, EOExercice exercice);

	public void _affecterDepenseBudget(_IDepenseControlePlanComptable depense, _IDepenseBudget depenseBudget);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void _supprimer(EOEditingContext ed, _IDepenseControlePlanComptable depense) throws DepenseControlePlanComptableException;

	public void _verifier(_IDepenseControlePlanComptable depense);

	public void _supprimerInventaire(_IDepenseControlePlanComptable dcpc, EOInventaire inventaire);

}
