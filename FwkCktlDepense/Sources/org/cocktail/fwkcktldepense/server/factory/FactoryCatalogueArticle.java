/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.ArticleException;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOTypeArticle;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCatalogueArticle extends Factory {

	public FactoryCatalogueArticle() {
		super();
	}

	public FactoryCatalogueArticle(boolean withLog) {
		super(withLog);
	}

    public EOCatalogueArticle creer(EOEditingContext ed, String artReference, String artLibelle, 
    		BigDecimal artPrixHt, BigDecimal artPrixTtc, EOTva tva, EOCodeExer codeExer, 
    		EOTypeArticle typeArticle, EOFournisseur fournisseur, EOCatalogueArticle articleCatalogue)
            throws ArticleException {

    	EOCatalogueArticle newArticle = creer(ed);

        setCaarReference(newArticle, artReference);
        setCaarLibelle(newArticle, artLibelle);
        setCaarPrixHt(newArticle, artPrixHt);
        setCaarPrixTtc(newArticle, artPrixTtc);

        affecterTva(newArticle, tva);
        affecterTypeArticle(newArticle, typeArticle);
        affecterFournisseur(newArticle, fournisseur);
        affecterCodeMarche(newArticle, codeExer);
        affecterCatalogueArticle(newArticle, articleCatalogue);

        try {
            newArticle.validateObjectMetier();
            return newArticle;
        } catch (ArticleException e) {
            e.printStackTrace();
            System.out.println(newArticle);
        }

        return null;
    }

    public EOCatalogueArticle creer(EOEditingContext ed) throws ArticleException {
    	EOCatalogueArticle newArticle=(EOCatalogueArticle)Factory.instanceForEntity(ed,EOCatalogueArticle.ENTITY_NAME);
        ed.insertObject(newArticle);
        return newArticle;
    }
    
    
    public void setCaarReference(EOCatalogueArticle article, String artReference) {
    	if (article==null)
    		return;
    	article.setCaarReference(artReference);
    }
    
    public void setCaarLibelle(EOCatalogueArticle article, String artLibelle) {
    	if (article==null)
    		return;
    	article.setCaarLibelle(artLibelle);
    }
    
    public void setCaarPrixHt(EOCatalogueArticle article, BigDecimal artPrixHt) {
    	if (article==null)
    		return;
    	article.setCaarPrixHt(artPrixHt);
    }
    
    public void setCaarPrixTtc(EOCatalogueArticle article, BigDecimal artPrixTtc) {
    	if (article==null)
    		return;
    	article.setCaarPrixTtc(artPrixTtc);
    }
    
    public void affecterTva(EOCatalogueArticle article, EOTva tva) {
        article.setTvaRelationship(tva);
    }

    public void affecterTypeArticle(EOCatalogueArticle article, EOTypeArticle typeArticle) {
        article.setTypeArticleRelationship(typeArticle);
    }

    public void affecterFournisseur(EOCatalogueArticle article, EOFournisseur fournisseur) {
        article.setFournisseurRelationship(fournisseur);
    }

    public void affecterCodeMarche(EOCatalogueArticle article, EOCodeExer codeExer) {
    	if (codeExer!=null) {
            Integer parametre=FinderParametre.getParametreCmNiveau(article.editingContext(), codeExer.exercice());
            
            if (parametre==null)
            	return;
            if (!codeExer.codeMarche().cmNiveau().equals(parametre))
                throw new ArticleException(ArticleException.codeExerPasBonNiveau);  		
    	}

    	article.setCodeMarcheRelationship(codeExer.codeMarche());
    }

    public void affecterCatalogueArticle(EOCatalogueArticle article, EOCatalogueArticle articleCatalogue) {
    	article.setCatalogueArticlePereRelationship(articleCatalogue);
    }

    /**
     * Permet de supprimer tant que ce n est pas dans la base.
     */
    public void supprimer(EOEditingContext ed, EOCatalogueArticle article) throws ArticleException {
        //Si l'article est enregistree
        /*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
            // l ecriture est elle valide ?
            if (EcritureDetail.ecriture().ecrEtat().equals(EOEcriture.ecritureValide))
                throw new EcritureDetailException(EcritureDetailException.supprimerDetailEcritureValide);
        }*/

    	article.setTvaRelationship(null);
    	article.setCodeMarcheRelationship(null);
    	article.setCatalogueArticlePereRelationship(null);

        ed.deleteObject(article);
    }

    public void verifier(EOCatalogueArticle article) {
    	article.validateObjectMetier();
    }
}
