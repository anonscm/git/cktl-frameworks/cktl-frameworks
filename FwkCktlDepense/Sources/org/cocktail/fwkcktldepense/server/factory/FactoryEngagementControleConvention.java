/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControleConventionException;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEngagementControleConvention extends Factory {

	public FactoryEngagementControleConvention() {
		super();
	}

	public FactoryEngagementControleConvention(boolean withLog) {
		super(withLog);
	}

	public EOEngagementControleConvention creer(EOEditingContext ed, BigDecimal econHtSaisie, BigDecimal econTvaSaisie, 
			BigDecimal econTtcSaisie, BigDecimal econMontantBudgetaire, EOConvention convention, EOExercice exercice,
			EOEngagementBudget engagementBudget)
	throws EngagementControleConventionException {
		
		EOEngagementControleConvention newEngage = creer(ed);
		
		setEconHtSaisie(newEngage, econHtSaisie);
		setEconTvaSaisie(newEngage, econTvaSaisie);
		setEconTtcSaisie(newEngage, econTtcSaisie);
		setEconMontantBudgetaire(newEngage, econMontantBudgetaire);
		setEconMontantBudgetaireReste(newEngage, econMontantBudgetaire);
		setEconDateSaisie(newEngage, new NSTimestamp());
		
		affecterConvention(newEngage, convention);
		affecterExercice(newEngage, exercice);
		affecterEngagementBudget(newEngage, engagementBudget);
		
		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementControleConventionException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}
		
		return null;
	}
	
	public EOEngagementControleConvention creer(EOEditingContext ed) throws EngagementControleConventionException {
		EOEngagementControleConvention newEngage=(EOEngagementControleConvention)Factory.instanceForEntity(ed,EOEngagementControleConvention.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}
	
	
	public void setEconHtSaisie(EOEngagementControleConvention engage, BigDecimal econHtSaisie) {
		if (engage==null)
			return;
		engage.setEconHtSaisie(econHtSaisie);
	}
	
	public void setEconTvaSaisie(EOEngagementControleConvention engage, BigDecimal econTvaSaisie) {
		if (engage==null)
			return;
		engage.setEconTvaSaisie(econTvaSaisie);
	}
	
	public void setEconTtcSaisie(EOEngagementControleConvention engage, BigDecimal econTtcSaisie) {
		if (engage==null)
			return;
		engage.setEconTtcSaisie(econTtcSaisie);
	}
	
	public void setEconMontantBudgetaire(EOEngagementControleConvention engage, BigDecimal econMontantBudgetaire) {
		if (engage==null)
			return;
		engage.setEconMontantBudgetaire(econMontantBudgetaire);
	}
	
	public void setEconMontantBudgetaireReste(EOEngagementControleConvention engage, BigDecimal econMontantBudgetaireReste) {
		if (engage==null)
			return;
		engage.setEconMontantBudgetaireReste(econMontantBudgetaireReste);
	}

	public void setEconDateSaisie(EOEngagementControleConvention engage, NSTimestamp econDateSaisie) {
		if (engage==null)
			return;
		engage.setEconDateSaisie(econDateSaisie);
	}

	public void affecterConvention(EOEngagementControleConvention engage, EOConvention convention) {
		engage.setConventionRelationship(convention);
	}
	
	public void affecterExercice(EOEngagementControleConvention engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}
	
	public void affecterEngagementBudget(EOEngagementControleConvention engage, EOEngagementBudget engagementBudget) {
		engage.setEngagementBudgetRelationship(engagementBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementControleConvention engage) throws EngagementControleConventionException{
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
		}
		
		engage.setConventionRelationship(null);
		engage.setExerciceRelationship(null);
		engage.setEngagementBudgetRelationship(null);
		
		ed.deleteObject(engage);
	}
	
	public void verifier(EOEngagementControleConvention engage) {
		engage.validateObjectMetier();
	}
}
