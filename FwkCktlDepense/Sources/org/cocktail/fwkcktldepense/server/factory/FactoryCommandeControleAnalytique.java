/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleActionException;
import org.cocktail.fwkcktldepense.server.exception.CommandeControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryCommandeControleAnalytique extends Factory {
	
	public FactoryCommandeControleAnalytique() {
		super();
	}
	
	public FactoryCommandeControleAnalytique(boolean withLog) {
		super(withLog);
	}
	
	protected NSArray dupliquer(EOEditingContext ed, EOCommandeBudget origine, EOCommandeBudget commandeBudget) throws CommandeControleActionException {
		NSMutableArray array=new NSMutableArray();
		
		for (int i=0; i<origine.commandeControleAnalytiques().count(); i++) {
			EOCommandeControleAnalytique controle=(EOCommandeControleAnalytique)origine.commandeControleAnalytiques().objectAtIndex(i);
			array.addObject(creer(ed, controle.canaHtSaisie(), controle.canaTvaSaisie(), controle.canaTtcSaisie(), 
					controle.canaMontantBudgetaire(), controle.canaPourcentage(), controle.codeAnalytique(), commandeBudget.exercice(), commandeBudget));
		}
		
		return array;
	}

	protected boolean verifier(EOExercice exercice, EOOrgan organ, NSArray controles) {
		for (int i=0; i<controles.count(); i++) {
			EOCommandeControleAnalytique controle=(EOCommandeControleAnalytique)controles.objectAtIndex(i);
			if (!verifier(exercice, organ, controle))
				return false;
		}
		return true;
	}

	protected boolean verifier(EOExercice exercice, EOOrgan organ, EOCommandeControleAnalytique controle) {
		return verifier(exercice, organ, controle.codeAnalytique());
	}
	
	protected boolean verifier(EOExercice exercice, EOOrgan organ, EOCodeAnalytique codeAnalytique) {
		if (exercice==null || organ==null || codeAnalytique==null)
			return false;
		if (!FinderCodeAnalytique.getCodeAnalytiques(codeAnalytique.editingContext(), organ, exercice).containsObject(codeAnalytique))
			return false;
		return true;
	}

	public EOCommandeControleAnalytique creer(EOEditingContext ed, BigDecimal canaHtSaisie, BigDecimal canaTvaSaisie, 
			BigDecimal canaTtcSaisie, BigDecimal canaMontantBudgetaire, BigDecimal canaPourcentage, 
			EOCodeAnalytique codeAnalytique, EOExercice exercice, EOCommandeBudget commandeBudget)
	throws CommandeControleAnalytiqueException {
		
		EOCommandeControleAnalytique newCommande = creer(ed);
		
		setCanaHtSaisie(newCommande, canaHtSaisie);
		setCanaTvaSaisie(newCommande, canaTvaSaisie);
		setCanaTtcSaisie(newCommande, canaTtcSaisie);
		setCanaMontantBudgetaire(newCommande, canaMontantBudgetaire);
		setCanaPourcentage(newCommande, canaPourcentage);
		
		affecterCodeAnalytique(newCommande, codeAnalytique);
		affecterExercice(newCommande, exercice);
		affecterCommandeBudget(newCommande, commandeBudget);
		
		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeControleAnalytiqueException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}
		
		return null;
	}
	
	public EOCommandeControleAnalytique creer(EOEditingContext ed) throws CommandeControleAnalytiqueException {
		EOCommandeControleAnalytique newCommande=(EOCommandeControleAnalytique)Factory.instanceForEntity(ed,EOCommandeControleAnalytique.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newCommande);
		return newCommande;
	}
	
	
	public void setCanaHtSaisie(EOCommandeControleAnalytique commande, BigDecimal canaHtSaisie) {
		if (commande==null)
			return;
		commande.setCanaHtSaisie(canaHtSaisie);
	}
	
	public void setCanaTvaSaisie(EOCommandeControleAnalytique commande, BigDecimal canaTvaSaisie) {
		if (commande==null)
			return;
		commande.setCanaTvaSaisie(canaTvaSaisie);
	}
	
	public void setCanaTtcSaisie(EOCommandeControleAnalytique commande, BigDecimal canaTtcSaisie) {
		if (commande==null)
			return;
		commande.setCanaTtcSaisie(canaTtcSaisie);
	}
	
	public void setCanaMontantBudgetaire(EOCommandeControleAnalytique commande, BigDecimal canaMontantBudgetaire) {
		if (commande==null)
			return;
		commande.setCanaMontantBudgetaire(canaMontantBudgetaire);
	}
	
	public void setCanaPourcentage(EOCommandeControleAnalytique commande, BigDecimal canaPourcentage) {
		if (commande==null)
			return;
		commande.setCanaPourcentage(canaPourcentage);
	}

	public void affecterCodeAnalytique(EOCommandeControleAnalytique commande, EOCodeAnalytique codeAnalytique) {
		commande.setCodeAnalytiqueRelationship(codeAnalytique);
	}
	
	public void affecterExercice(EOCommandeControleAnalytique commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
	}
	
	public void affecterCommandeBudget(EOCommandeControleAnalytique commande, EOCommandeBudget commandeBudget) {
		commande.setCommandeBudgetRelationship(commandeBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeControleAnalytique commande) throws CommandeControleAnalytiqueException {
		commande.setCodeAnalytiqueRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setCommandeBudgetRelationship(null);

		ed.deleteObject(commande);
	}
	
	public void verifier(EOCommandeControleAnalytique commande) {
		commande.validateObjectMetier();
	}
}
