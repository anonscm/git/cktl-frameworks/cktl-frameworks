/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryReversementControleMarche extends Factory {

	public FactoryReversementControleMarche() {
		super();
	}

	public FactoryReversementControleMarche(boolean withLog) {
		super(withLog);
	}
	
	public EODepenseControleMarche creer(EOEditingContext ed, BigDecimal dmarHtSaisie, BigDecimal dmarTvaSaisie, 
			BigDecimal dmarTtcSaisie, BigDecimal dmarMontantBudgetaire, BigDecimal dmarPourcentage, EOAttribution attribution, EOExercice exercice,
			EODepenseBudget depenseBudget)
	throws DepenseControleMarcheException {
		
		EODepenseControleMarche newDepense = creer(ed);
		
		setDmarHtSaisie(newDepense, dmarHtSaisie);
		setDmarTvaSaisie(newDepense, dmarTvaSaisie);
		setDmarTtcSaisie(newDepense, dmarTtcSaisie);
		setDmarMontantBudgetaire(newDepense, dmarMontantBudgetaire);

		affecterAttribution(newDepense, attribution);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDmarPourcentage(newDepense, dmarPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleMarcheException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}
	
	public EODepenseControleMarche creer(EOEditingContext ed) throws DepenseControleMarcheException {
		EODepenseControleMarche newDepense=(EODepenseControleMarche)Factory.
		 	instanceForEntity(ed,EODepenseControleMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}
	
	public void setDmarPourcentage(EODepenseControleMarche depense, BigDecimal dmarPourcentage) {
		if (depense==null)
			return;
		depense.setPourcentage(dmarPourcentage);
	}

	public void setDmarHtSaisie(EODepenseControleMarche depense, BigDecimal dmarHtSaisie) {
		if (depense==null)
			return;
		depense.setDmarHtSaisie(dmarHtSaisie);
	}
	
	public void setDmarTvaSaisie(EODepenseControleMarche depense, BigDecimal dmarTvaSaisie) {
		if (depense==null)
			return;
		depense.setDmarTvaSaisie(dmarTvaSaisie);
	}
	
	public void setDmarTtcSaisie(EODepenseControleMarche depense, BigDecimal dmarTtcSaisie) {
		if (depense==null)
			return;
		depense.setDmarTtcSaisie(dmarTtcSaisie);
	}
	
	public void setDmarMontantBudgetaire(EODepenseControleMarche depense, BigDecimal dmarMontantBudgetaire) {
		if (depense==null)
			return;
		depense.setDmarMontantBudgetaire(dmarMontantBudgetaire);
	}
	
	public void affecterAttribution(EODepenseControleMarche depense, EOAttribution attribution) {
		depense.setAttributionRelationship(attribution);
	}
	
	public void affecterExercice(EODepenseControleMarche depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}
	
	public void affecterDepenseBudget(EODepenseControleMarche depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleMarche depense) throws DepenseControleMarcheException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/
		
		depense.setAttributionRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);
		
		ed.deleteObject(depense);
	}
	
	public void verifier(EODepenseControleMarche depense) {
		depense.validateObjectMetier();
	}
}
