/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleActionException;
import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.interfaces.IFactoryControle;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryDepenseControleAction extends Factory implements _IFactoryDepenseControleAction, IFactoryControle {

	public FactoryDepenseControleAction() {
		super();
	}

	public FactoryDepenseControleAction(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleAction copie(EOEditingContext ed, EODepenseControleAction depense, _IDepenseBudget budget) {

		EODepenseControleAction newDepense = creer(ed);

		setDactHtSaisie(newDepense, depense.dactHtSaisie());
		setDactTvaSaisie(newDepense, depense.dactTvaSaisie());
		setDactTtcSaisie(newDepense, depense.dactTtcSaisie());
		setDactMontantBudgetaire(newDepense, depense.dactMontantBudgetaire());

		affecterTypeAction(newDepense, depense.typeAction());
		affecterExercice(newDepense, depense.exercice());
		affecterDepenseBudget(newDepense, (EODepenseBudget) budget);

		setDactPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EODepenseControleAction creer(EOEditingContext ed, BigDecimal pourcentage, Object code, EOExercice exercice,
			EODepenseBudget depenseBudget) throws DepenseControleAnalytiqueException {
		return creer(ed, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, pourcentage, (EOTypeAction) code, exercice, depenseBudget);
	}

	public EODepenseControleAction creer(EOEditingContext ed, BigDecimal dactHtSaisie, BigDecimal dactTvaSaisie,
			BigDecimal dactTtcSaisie, BigDecimal dactMontantBudgetaire, BigDecimal dactPourcentage, EOTypeAction typeAction, EOExercice exercice,
			EODepenseBudget depenseBudget)
			throws DepenseControleActionException {

		EODepenseControleAction newDepense = creer(ed);

		setDactHtSaisie(newDepense, dactHtSaisie);
		setDactTvaSaisie(newDepense, dactTvaSaisie);
		setDactTtcSaisie(newDepense, dactTtcSaisie);
		setDactMontantBudgetaire(newDepense, dactMontantBudgetaire);

		affecterTypeAction(newDepense, typeAction);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDactPourcentage(newDepense, dactPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleActionException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EODepenseControleAction creer(EOEditingContext ed) throws DepenseControleActionException {
		EODepenseControleAction newDepense = (EODepenseControleAction) Factory.instanceForEntity(ed, EODepenseControleAction.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setDactPourcentage(EODepenseControleAction depense, BigDecimal dactPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dactPourcentage);
	}

	public void setDactHtSaisie(EODepenseControleAction depense, BigDecimal dactHtSaisie) {
		if (depense == null)
			return;
		depense.setDactHtSaisie(dactHtSaisie);
	}

	public void setDactTvaSaisie(EODepenseControleAction depense, BigDecimal dactTvaSaisie) {
		if (depense == null)
			return;
		depense.setDactTvaSaisie(dactTvaSaisie);
	}

	public void setDactTtcSaisie(EODepenseControleAction depense, BigDecimal dactTtcSaisie) {
		if (depense == null)
			return;
		depense.setDactTtcSaisie(dactTtcSaisie);
	}

	public void setDactMontantBudgetaire(EODepenseControleAction depense, BigDecimal dactMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDactMontantBudgetaire(dactMontantBudgetaire);
	}

	public void affecterTypeAction(EODepenseControleAction depense, EOTypeAction typeAction) {
		depense.setTypeActionRelationship(typeAction);
	}

	public void affecterExercice(EODepenseControleAction depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterDepenseBudget(EODepenseControleAction depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleAction depense) throws DepenseControleActionException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setTypeActionRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EODepenseControleAction depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleAction copie(EOEditingContext ed, _IDepenseControleAction depense, _IDepenseBudget budget) {
		return copie(ed, (EODepenseControleAction) depense, (EODepenseBudget) budget);
	}

	public _IDepenseControleAction creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOTypeAction typeAction, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleActionException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, typeAction, exercice, (EODepenseBudget) depenseBudget);
	}

	public void setIactHtSaisie(_IDepenseControleAction depense, BigDecimal dactHtSaisie) {
		setDactHtSaisie((EODepenseControleAction) depense, dactHtSaisie);

	}

	public void setIactTvaSaisie(_IDepenseControleAction depense, BigDecimal dactTvaSaisie) {
		setDactTvaSaisie((EODepenseControleAction) depense, dactTvaSaisie);

	}

	public void setIactTtcSaisie(_IDepenseControleAction depense, BigDecimal dactTtcSaisie) {
		setDactTtcSaisie((EODepenseControleAction) depense, dactTtcSaisie);

	}

	public void setIactMontantBudgetaire(_IDepenseControleAction depense, BigDecimal dactMontantBudgetaire) {
		setDactMontantBudgetaire((EODepenseControleAction) depense, dactMontantBudgetaire);

	}

	public void affecterTypeAction(_IDepenseControleAction depense, EOTypeAction typeAction) {
		affecterTypeAction((EODepenseControleAction) depense, typeAction);

	}

	public void affecterExercice(_IDepenseControleAction depense, EOExercice exercice) {
		affecterExercice((EODepenseControleAction) depense, exercice);

	}

	public void affecterDepenseBudget(_IDepenseControleAction depense, _IDepenseBudget depenseBudget) {
		affecterDepenseBudget((EODepenseControleAction) depense, (EODepenseBudget) depenseBudget);

	}

	public void supprimer(EOEditingContext ed, _IDepenseControleAction depense) throws DepenseControleActionException {
		supprimer(ed, (EODepenseControleAction) depense);

	}

	public void verifier(_IDepenseControleAction depense) {
		verifier((EODepenseControleAction) depense);

	}

	public void setIactPourcentage(_IDepenseControleAction depense, BigDecimal dactPourcentage) {
		setDactPourcentage((EODepenseControleAction) depense, dactPourcentage);

	}

}
