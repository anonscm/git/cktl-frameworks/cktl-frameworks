/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryReversementControleHorsMarche extends Factory {

	public FactoryReversementControleHorsMarche() {
		super();
	}

	public FactoryReversementControleHorsMarche(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleHorsMarche creer(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie, 
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat, 
			EOExercice exercice, EODepenseBudget depenseBudget)
	throws DepenseControleHorsMarcheException {
		
		EODepenseControleHorsMarche newDepense = creer(ed);
		
		setDhomHtSaisie(newDepense, dhomHtSaisie);
		setDhomTvaSaisie(newDepense, dhomTvaSaisie);
		setDhomTtcSaisie(newDepense, dhomTtcSaisie);
		setDhomMontantBudgetaire(newDepense, dhomMontantBudgetaire);

		affecterCodeExer(newDepense, codeExer);
		affecterExercice(newDepense, exercice);
		affecterTypeAchat(newDepense, typeAchat);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDhomPourcentage(newDepense, dhomPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleHorsMarcheException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}
	
	public EODepenseControleHorsMarche creer(EOEditingContext ed) throws DepenseControleHorsMarcheException {
		EODepenseControleHorsMarche newDepense=(EODepenseControleHorsMarche)Factory.
		 	instanceForEntity(ed,EODepenseControleHorsMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}
	
	public void setDhomPourcentage(EODepenseControleHorsMarche depense, BigDecimal dhomPourcentage) {
		if (depense==null)
			return;
		depense.setPourcentage(dhomPourcentage);
	}

	public void setDhomHtSaisie(EODepenseControleHorsMarche depense, BigDecimal dhomHtSaisie) {
		if (depense==null)
			return;
		depense.setDhomHtSaisie(dhomHtSaisie);
	}
	
	public void setDhomTvaSaisie(EODepenseControleHorsMarche depense, BigDecimal dhomTvaSaisie) {
		if (depense==null)
			return;
		depense.setDhomTvaSaisie(dhomTvaSaisie);
	}
	
	public void setDhomTtcSaisie(EODepenseControleHorsMarche depense, BigDecimal dhomTtcSaisie) {
		if (depense==null)
			return;
		depense.setDhomTtcSaisie(dhomTtcSaisie);
	}
	
	public void setDhomMontantBudgetaire(EODepenseControleHorsMarche depense, BigDecimal dhomMontantBudgetaire) {
		if (depense==null)
			return;
		depense.setDhomMontantBudgetaire(dhomMontantBudgetaire);
	}
	
	public void affecterCodeExer(EODepenseControleHorsMarche depense, EOCodeExer codeExer) {
		depense.setCodeExerRelationship(codeExer);
	}
	
	public void affecterTypeAchat(EODepenseControleHorsMarche depense, EOTypeAchat typeAchat) {
		depense.setTypeAchatRelationship(typeAchat);
	}

	public void affecterExercice(EODepenseControleHorsMarche depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}
	
	public void affecterDepenseBudget(EODepenseControleHorsMarche depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/
		
		depense.setCodeExerRelationship(null);
		depense.setTypeAchatRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);
		
		ed.deleteObject(depense);
	}
	
	public void verifier(EODepenseControleHorsMarche depense) {
		depense.validateObjectMetier();
	}
}
