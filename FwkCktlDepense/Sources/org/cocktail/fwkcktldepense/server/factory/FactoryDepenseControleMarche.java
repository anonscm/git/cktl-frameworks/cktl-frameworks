/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.exception.DepenseControleMarcheException;
import org.cocktail.fwkcktldepense.server.interfaces.IFactoryControle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleMarche;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryDepenseControleMarche extends Factory implements _IFactoryDepenseControleMarche, IFactoryControle {

	public FactoryDepenseControleMarche() {
		super();
	}

	public FactoryDepenseControleMarche(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleMarche copie(EOEditingContext ed, EODepenseControleMarche depense, EODepenseBudget budget) {

		EODepenseControleMarche newDepense = creer(ed);

		setDmarHtSaisie(newDepense, depense.dmarHtSaisie());
		setDmarTvaSaisie(newDepense, depense.dmarTvaSaisie());
		setDmarTtcSaisie(newDepense, depense.dmarTtcSaisie());
		setDmarMontantBudgetaire(newDepense, depense.dmarMontantBudgetaire());

		affecterAttribution(newDepense, depense.attribution());
		affecterExercice(newDepense, depense.exercice());
		affecterDepenseBudget(newDepense, budget);

		setDmarPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EODepenseControleMarche creer(EOEditingContext ed, BigDecimal pourcentage, Object code, EOExercice exercice,
			EODepenseBudget depenseBudget) throws DepenseControleAnalytiqueException {
		return creer(ed, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, pourcentage, (EOAttribution) code, exercice, depenseBudget);
	}

	public EODepenseControleMarche creer(EOEditingContext ed, BigDecimal dmarHtSaisie, BigDecimal dmarTvaSaisie,
			BigDecimal dmarTtcSaisie, BigDecimal dmarMontantBudgetaire, BigDecimal dmarPourcentage, EOAttribution attribution, EOExercice exercice,
			EODepenseBudget depenseBudget)
			throws DepenseControleMarcheException {

		EODepenseControleMarche newDepense = creer(ed);

		setDmarHtSaisie(newDepense, dmarHtSaisie);
		setDmarTvaSaisie(newDepense, dmarTvaSaisie);
		setDmarTtcSaisie(newDepense, dmarTtcSaisie);
		setDmarMontantBudgetaire(newDepense, dmarMontantBudgetaire);

		affecterAttribution(newDepense, attribution);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDmarPourcentage(newDepense, dmarPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleMarcheException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EODepenseControleMarche creer(EOEditingContext ed) throws DepenseControleMarcheException {
		EODepenseControleMarche newDepense = (EODepenseControleMarche) Factory.
				instanceForEntity(ed, EODepenseControleMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setDmarPourcentage(EODepenseControleMarche depense, BigDecimal dmarPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dmarPourcentage);
	}

	public void setDmarHtSaisie(EODepenseControleMarche depense, BigDecimal dmarHtSaisie) {
		if (depense == null)
			return;
		depense.setDmarHtSaisie(dmarHtSaisie);
	}

	public void setDmarTvaSaisie(EODepenseControleMarche depense, BigDecimal dmarTvaSaisie) {
		if (depense == null)
			return;
		depense.setDmarTvaSaisie(dmarTvaSaisie);
	}

	public void setDmarTtcSaisie(EODepenseControleMarche depense, BigDecimal dmarTtcSaisie) {
		if (depense == null)
			return;
		depense.setDmarTtcSaisie(dmarTtcSaisie);
	}

	public void setDmarMontantBudgetaire(EODepenseControleMarche depense, BigDecimal dmarMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDmarMontantBudgetaire(dmarMontantBudgetaire);
	}

	public void affecterAttribution(EODepenseControleMarche depense, EOAttribution attribution) {
		depense.setAttributionRelationship(attribution);
	}

	public void affecterExercice(EODepenseControleMarche depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterDepenseBudget(EODepenseControleMarche depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleMarche depense) throws DepenseControleMarcheException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setAttributionRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EODepenseControleMarche depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleMarche _copie(EOEditingContext ed, _IDepenseControleMarche depense, _IDepenseBudget budget) {
		return copie(ed, (EODepenseControleMarche) depense, (EODepenseBudget) budget);
	}

	public _IDepenseControleMarche _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOAttribution attribution, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleMarcheException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, attribution, exercice, (EODepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControleMarche depense, BigDecimal dactPourcentage) {
		setDmarPourcentage((EODepenseControleMarche) depense, dactPourcentage);
		//setDhomPourcentage((EODepenseControleMarche) depense, dactPourcentage);
	}

	public void setHtSaisie(_IDepenseControleMarche depense, BigDecimal htSaisie) {
		setDmarHtSaisie((EODepenseControleMarche) depense, htSaisie);

	}

	public void setTvaSaisie(_IDepenseControleMarche depense, BigDecimal tvaSaisie) {
		setDmarTvaSaisie((EODepenseControleMarche) depense, tvaSaisie);

	}

	public void setTtcSaisie(_IDepenseControleMarche depense, BigDecimal ttcSaisie) {
		setDmarTtcSaisie((EODepenseControleMarche) depense, ttcSaisie);

	}

	public void setMontantBudgetaire(_IDepenseControleMarche depense, BigDecimal montantBudgetaire) {
		setDmarMontantBudgetaire((EODepenseControleMarche) depense, montantBudgetaire);

	}

	public void _affecterExercice(_IDepenseControleMarche depense, EOExercice exercice) {
		affecterExercice((EODepenseControleMarche) depense, exercice);
	}

	public void _affecterDepenseBudget(_IDepenseControleMarche depense, _IDepenseBudget depenseBudget) {
		affecterDepenseBudget((EODepenseControleMarche) depense, (EODepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControleMarche depense) throws DepenseControleMarcheException {
		supprimer(ed, (EODepenseControleMarche) depense);

	}

	public void _verifier(_IDepenseControleMarche depense) {
		verifier((EODepenseControleMarche) depense);

	}

	public void _affecterAttribution(_IDepenseControleMarche depense, EOAttribution attibution) {
		affecterAttribution((EODepenseControleMarche) depense, attibution);

	}

}
