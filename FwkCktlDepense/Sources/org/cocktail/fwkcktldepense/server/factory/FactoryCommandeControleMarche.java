/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleActionException;
import org.cocktail.fwkcktldepense.server.exception.CommandeControleMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryCommandeControleMarche extends Factory {
	
	public FactoryCommandeControleMarche() {
		super();
	}
	
	public FactoryCommandeControleMarche(boolean withLog) {
		super(withLog);
	}
	
	protected NSArray dupliquer(EOEditingContext ed, EOCommandeBudget origine, EOCommandeBudget commandeBudget) throws CommandeControleActionException {
		NSMutableArray array=new NSMutableArray();
		NSArray ctrls=commandeBudget.commandeControleMarches();
		boolean creation=true;

		for (int i=0; i<origine.commandeControleMarches().count(); i++) {
			EOCommandeControleMarche controle=(EOCommandeControleMarche)origine.commandeControleMarches().objectAtIndex(i);
			creation=true;

			if (ctrls!=null && ctrls.count()>0) {
				NSMutableArray arrayQuals=new NSMutableArray();
				arrayQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCommandeControleMarche.ATTRIBUTION_KEY+"=%@", 
						new NSArray(controle.attribution())));

				NSArray result=EOQualifier.filteredArrayWithQualifier(ctrls, new EOAndQualifier(arrayQuals));
				if (result.count()>0) {
					creation=false;
				}
			}
			
			if (creation)
				array.addObject(creer(ed, controle.cmarHtSaisie(), controle.cmarTvaSaisie(), controle.cmarTtcSaisie(), 
					controle.cmarMontantBudgetaire(), controle.attribution(), commandeBudget.exercice(), commandeBudget));
		}
		
		return array;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, NSArray controles) {
		for (int i=0; i<controles.count(); i++) {
			EOCommandeControleMarche controle=(EOCommandeControleMarche)controles.objectAtIndex(i);
			if (!verifier(exercice, utilisateur, organ, typeCredit, controle))
				return false;
		}
		return true;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOCommandeControleMarche controle) {
		return verifier(exercice, utilisateur, organ, typeCredit, controle.attribution());
	}
	
	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOAttribution attribution) {
		if (exercice==null || utilisateur==null || organ==null || typeCredit==null || attribution==null)
			return false;
		if (!attribution.isValideForDate(null))
			return false;
		return true;
	}

	public EOCommandeControleMarche creer(EOEditingContext ed, BigDecimal cmarHtSaisie, BigDecimal cmarTvaSaisie, 
			BigDecimal cmarTtcSaisie, BigDecimal cmarMontantBudgetaire, 
			EOAttribution attribution, EOExercice exercice, EOCommandeBudget commandeBudget)
	throws CommandeControleMarcheException {
		
		EOCommandeControleMarche newCommande = creer(ed);
		
		setCmarHtSaisie(newCommande, cmarHtSaisie);
		setCmarTvaSaisie(newCommande, cmarTvaSaisie);
		setCmarTtcSaisie(newCommande, cmarTtcSaisie);
		setCmarMontantBudgetaire(newCommande, cmarMontantBudgetaire);
		
		affecterAttribution(newCommande, attribution);
		affecterExercice(newCommande, exercice);
		affecterCommandeBudget(newCommande, commandeBudget);
		
		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeControleMarcheException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}
		
		return null;
	}
	
	public EOCommandeControleMarche creer(EOEditingContext ed) throws CommandeControleMarcheException {
		EOCommandeControleMarche newCommande=(EOCommandeControleMarche)Factory.instanceForEntity(ed,EOCommandeControleMarche.ENTITY_NAME);
		ed.insertObject(newCommande);
		return newCommande;
	}
	
	
	public void setCmarHtSaisie(EOCommandeControleMarche commande, BigDecimal cmarHtSaisie) {
		if (commande==null)
			return;
		commande.setCmarHtSaisie(cmarHtSaisie);
	}
	
	public void setCmarTvaSaisie(EOCommandeControleMarche commande, BigDecimal cmarTvaSaisie) {
		if (commande==null)
			return;
		commande.setCmarTvaSaisie(cmarTvaSaisie);
	}
	
	public void setCmarTtcSaisie(EOCommandeControleMarche commande, BigDecimal cmarTtcSaisie) {
		if (commande==null)
			return;
		commande.setCmarTtcSaisie(cmarTtcSaisie);
	}

	public void setCmarMontantBudgetaire(EOCommandeControleMarche commande, BigDecimal cmarMontantBudgetaire) {
		if (commande==null)
			return;
		commande.setCmarMontantBudgetaire(cmarMontantBudgetaire);
	}
	
	public void affecterAttribution(EOCommandeControleMarche commande, EOAttribution attribution) {
		commande.setAttributionRelationship(attribution);
	}
	
	public void affecterExercice(EOCommandeControleMarche commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
	}
	
	public void affecterCommandeBudget(EOCommandeControleMarche commande, EOCommandeBudget commandeBudget) {
		commande.setCommandeBudgetRelationship(commandeBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeControleMarche commande) throws CommandeControleMarcheException {
		EOCommandeBudget commandeBudget=commande.commandeBudget();
		
		commande.setAttributionRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setCommandeBudgetRelationship(null);

		ed.deleteObject(commande);
		if (commandeBudget!=null)
			commandeBudget.mettreAJourMontants();
	}
	
	public void verifier(EOCommandeControleMarche commande) {
		commande.validateObjectMetier();
	}
}
