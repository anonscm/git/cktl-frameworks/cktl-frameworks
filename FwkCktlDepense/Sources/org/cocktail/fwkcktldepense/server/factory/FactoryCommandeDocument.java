/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import org.cocktail.fwkcktldepense.server.exception.CommandeDocumentException;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression;
import org.cocktail.fwkcktldepense.server.metier.EOTypeDocument;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.geide.fwkcktlgedibus.metier.serveur.Courrier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryCommandeDocument extends Factory {

	public FactoryCommandeDocument() {
		super();
	}

	public FactoryCommandeDocument(boolean withLog) {
		super(withLog);
	}

	public EOCommandeDocument creer(EOEditingContext ed, EOCommande commande, EOTypeDocument typeDocument, EOUtilisateur utilisateur, NSTimestamp date,
			EOCommandeImpression commandeImpression, Courrier courrier, String url) throws CommandeDocumentException {

		EOCommandeDocument newCommandeDocument = creer(ed);

		setComdDate(newCommandeDocument, date);
		setComdUrl(newCommandeDocument, url);

		affecterCommande(newCommandeDocument, commande);
		affecterTypeDocument(newCommandeDocument, typeDocument);
		affecterCommandeImpression(newCommandeDocument, commandeImpression);
		affecterCourrier(newCommandeDocument, courrier);
		affecterUtilisateur(newCommandeDocument, utilisateur);

		try {
			newCommandeDocument.validateObjectMetier();
			return newCommandeDocument;
		} catch (CommandeDocumentException e) {
			e.printStackTrace();
			System.out.println(newCommandeDocument);
			throw e;
		}

		// return null;
	}

	public EOCommandeDocument creer(EOEditingContext ed) throws CommandeDocumentException {
		EOCommandeDocument newCommandeDocument = (EOCommandeDocument) Factory.instanceForEntity(ed, EOCommandeDocument.ENTITY_NAME);
		ed.insertObject(newCommandeDocument);
		return newCommandeDocument;
	}

	public void setComdUrl(EOCommandeDocument commandeDocument, String url) {
		if (commandeDocument == null)
			return;
		commandeDocument.setComdUrl(url);
	}

	public void setComdDate(EOCommandeDocument commandeDocument, NSTimestamp date) {
		if (commandeDocument == null)
			return;
		commandeDocument.setComdDate(date);
	}

	public void affecterTypeDocument(EOCommandeDocument commandeDocument, EOTypeDocument typeDocument) {
		commandeDocument.setTypeDocumentRelationship(typeDocument);
	}

	public void affecterCommandeImpression(EOCommandeDocument commandeDocument, EOCommandeImpression commandeImpression) {
		commandeDocument.setCommandeImpressionRelationship(commandeImpression);
	}

	public void affecterCourrier(EOCommandeDocument commandeDocument, Courrier courrier) {
		commandeDocument.setCourrierRelationship(courrier);
	}

	public void affecterUtilisateur(EOCommandeDocument commandeDocument, EOUtilisateur utilisateur) {
		commandeDocument.setUtilisateurRelationship(utilisateur);
	}

	public void affecterCommande(EOCommandeDocument commandeDocument, EOCommande commande) {
		commandeDocument.setCommandeRelationship(commande);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeDocument commandeDocument) throws CommandeDocumentException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		commandeDocument.setUtilisateurRelationship(null);
		commandeDocument.setCommandeRelationship(null);
		commandeDocument.setCommandeImpressionRelationship(null);
		commandeDocument.setCourrierRelationship(null);
		commandeDocument.setTypeDocumentRelationship(null);

		ed.deleteObject(commandeDocument);
	}

	public void verifier(EOCommandeDocument commandeDocument) {
		commandeDocument.validateObjectMetier();
	}
}
