/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.EngagementControlePlanComptableException;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryEngagementControlePlanComptable extends Factory {

	public FactoryEngagementControlePlanComptable() {
		super();
	}

	public FactoryEngagementControlePlanComptable(boolean withLog) {
		super(withLog);
	}

	public EOEngagementControlePlanComptable creer(EOEditingContext ed, BigDecimal epcoHtSaisie, BigDecimal epcoTvaSaisie, 
			BigDecimal epcoTtcSaisie, BigDecimal epcoMontantBudgetaire, EOPlanComptable planComptable, 
			EOExercice exercice, EOEngagementBudget engagementBudget)
	throws EngagementControlePlanComptableException {
		
		EOEngagementControlePlanComptable newEngage = creer(ed);
		
		setEpcoHtSaisie(newEngage, epcoHtSaisie);
		setEpcoTvaSaisie(newEngage, epcoTvaSaisie);
		setEpcoTtcSaisie(newEngage, epcoTtcSaisie);
		setEpcoMontantBudgetaire(newEngage, epcoMontantBudgetaire);
		setEpcoMontantBudgetaireReste(newEngage, epcoMontantBudgetaire);
		setEpcoDateSaisie(newEngage, new NSTimestamp());
		
		affecterPlanComptable(newEngage, planComptable);
		affecterExercice(newEngage, exercice);
		affecterEngagementBudget(newEngage, engagementBudget);
		
		try {
			newEngage.validateObjectMetier();
			return newEngage;
		} catch (EngagementControlePlanComptableException e) {
			e.printStackTrace();
			System.out.println(newEngage);
		}
		
		return null;
	}
	
	public EOEngagementControlePlanComptable creer(EOEditingContext ed) throws EngagementControlePlanComptableException {
		EOEngagementControlePlanComptable newEngage=(EOEngagementControlePlanComptable)Factory.instanceForEntity(ed,EOEngagementControlePlanComptable.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newEngage);
		return newEngage;
	}
	
	
	public void setEpcoHtSaisie(EOEngagementControlePlanComptable engage, BigDecimal epcoHtSaisie) {
		if (engage==null)
			return;
		engage.setEpcoHtSaisie(epcoHtSaisie);
	}
	
	public void setEpcoTvaSaisie(EOEngagementControlePlanComptable engage, BigDecimal epcoTvaSaisie) {
		if (engage==null)
			return;
		engage.setEpcoTvaSaisie(epcoTvaSaisie);
	}
	
	public void setEpcoTtcSaisie(EOEngagementControlePlanComptable engage, BigDecimal epcoTtcSaisie) {
		if (engage==null)
			return;
		engage.setEpcoTtcSaisie(epcoTtcSaisie);
	}
	
	public void setEpcoMontantBudgetaire(EOEngagementControlePlanComptable engage, BigDecimal epcoMontantBudgetaire) {
		if (engage==null)
			return;
		engage.setEpcoMontantBudgetaire(epcoMontantBudgetaire);
	}
	
	public void setEpcoMontantBudgetaireReste(EOEngagementControlePlanComptable engage, BigDecimal epcoMontantBudgetaireReste) {
		if (engage==null)
			return;
		engage.setEpcoMontantBudgetaireReste(epcoMontantBudgetaireReste);
	}

	public void setEpcoDateSaisie(EOEngagementControlePlanComptable engage, NSTimestamp epcoDateSaisie) {
		if (engage==null)
			return;
		engage.setEpcoDateSaisie(epcoDateSaisie);
	}

	public void affecterPlanComptable(EOEngagementControlePlanComptable engage, EOPlanComptable planComptable) {
		engage.setPlanComptableRelationship(planComptable);
	}
	
	public void affecterExercice(EOEngagementControlePlanComptable engage, EOExercice exercice) {
		engage.setExerciceRelationship(exercice);
	}
	
	public void affecterEngagementBudget(EOEngagementControlePlanComptable engage, EOEngagementBudget engagementBudget) {
		engage.setEngagementBudgetRelationship(engagementBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOEngagementControlePlanComptable engage) throws EngagementControlePlanComptableException{
		// Si l'engage est enregistree
		if (ed.insertedObjects().indexOfObject(engage) == NSArray.NotFound) {
		}
		
		engage.setPlanComptableRelationship(null);
		engage.setExerciceRelationship(null);
		engage.setEngagementBudgetRelationship(null);
		
		ed.deleteObject(engage);
	}
	
	public void verifier(EOEngagementControlePlanComptable engage) {
		engage.validateObjectMetier();
	}
}
