package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseControleHorsMarche {

	public _IDepenseControleHorsMarche _copie(EOEditingContext ed, _IDepenseControleHorsMarche depense, _IDepenseBudget budget);

	public _IDepenseControleHorsMarche _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleHorsMarcheException;

	public void _setPourcentage(_IDepenseControleHorsMarche depense, BigDecimal pourcentage);

	public void setHtSaisie(_IDepenseControleHorsMarche depense, BigDecimal htSaisie);

	public void setTvaSaisie(_IDepenseControleHorsMarche depense, BigDecimal tvaSaisie);

	public void setTtcSaisie(_IDepenseControleHorsMarche depense, BigDecimal ttcSaisie);

	public void setMontantBudgetaire(_IDepenseControleHorsMarche depense, BigDecimal montantBudgetaire);

	public void _affecterCodeExer(_IDepenseControleHorsMarche depense, EOCodeExer codeExer);

	public void _affecterTypeAchat(_IDepenseControleHorsMarche depense, EOTypeAchat typeAchat);

	public void _affecterExercice(_IDepenseControleHorsMarche depense, EOExercice exercice);

	public void _affecterDepenseBudget(_IDepenseControleHorsMarche depense, _IDepenseBudget depenseBudget);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void _supprimer(EOEditingContext ed, _IDepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException;

	public void _verifier(_IDepenseControleHorsMarche depense);

}
