package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;

import com.webobjects.eocontrol.EOEditingContext;

public interface _IFactoryDepenseBudget {

	public _IDepenseBudget creer(EOEditingContext ed, BigDecimal depHtSaisie, BigDecimal depTvaSaisie,
			BigDecimal depTtcSaisie, BigDecimal depMontantBudgetaire, EODepensePapier depensePapier,
			EOEngagementBudget engagementBudget, EOTauxProrata tauxProrata, EOExercice exercice, EOUtilisateur utilisateur, ESourceCreditType sourceCreditType, _ISourceCredit source)
			throws DepenseBudgetException;

	public _IDepenseBudget copie(EOEditingContext ed, _IDepenseBudget depenseBudget);

	public _IDepenseBudget creerSansRepartAuto(EOEditingContext ed, BigDecimal depHtSaisie, BigDecimal depTvaSaisie,
			BigDecimal depTtcSaisie, BigDecimal depMontantBudgetaire, EODepensePapier depensePapier,
			EOEngagementBudget engagementBudget, EOTauxProrata tauxProrata, EOExercice exercice, EOUtilisateur utilisateur)
			throws DepenseBudgetException;

	public _IDepenseBudget creer(EOEditingContext ed) throws DepenseBudgetException;

	public void setDepHtSaisie(_IDepenseBudget depense, BigDecimal depHtSaisie);

	public void setDepTvaSaisie(_IDepenseBudget depense, BigDecimal depTvaSaisie);

	public void setDepTtcSaisie(_IDepenseBudget depense, BigDecimal depTtcSaisie);

	public void setDepMontantBudgetaire(_IDepenseBudget depense, BigDecimal depMontantBudgetaire);

	public void affecterDepensePapier(_IDepenseBudget depense, EODepensePapier depensePapier);

	public void affecterEngagementBudget(_IDepenseBudget depense, EOEngagementBudget engagementBudget);

	public void affecterTauxProrata(_IDepenseBudget depense, EOTauxProrata tauxProrata);

	public void affecterUtilisateur(_IDepenseBudget depense, EOUtilisateur utilisateur);

	public void affecterExercice(_IDepenseBudget depense, EOExercice exercice);

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, _IDepenseBudget depense) throws DepenseBudgetException;

	public void supprimerDepenseContoleActions(_IDepenseBudget depense);

	public void supprimerDepenseContoleAnalytiques(_IDepenseBudget depense);

	public void supprimerDepenseContoleConventions(_IDepenseBudget depense);

	public void supprimerDepenseContoleHorsMarches(_IDepenseBudget depense);

	public void supprimerDepenseContoleMarches(_IDepenseBudget depense);

	public void supprimerDepenseContolePlanComptables(_IDepenseBudget depense);

	public void verifier(_IDepenseBudget depense);
}
