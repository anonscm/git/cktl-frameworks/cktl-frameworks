/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryPreDepenseBudget extends Factory implements _IFactoryDepenseBudget {

	public FactoryPreDepenseBudget() {
		super();
	}

	public FactoryPreDepenseBudget(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseBudget creer(EOEditingContext ed, BigDecimal depHtSaisie, BigDecimal depTvaSaisie,
			BigDecimal depTtcSaisie, BigDecimal depMontantBudgetaire, EODepensePapier depensePapier,
			EOEngagementBudget engagementBudget, EOTauxProrata tauxProrata, EOExercice exercice, EOUtilisateur utilisateur, ESourceCreditType sourceCreditType, _ISourceCredit source)
			throws DepenseBudgetException {

		EOPreDepenseBudget newDepense = creer(ed);
		newDepense.setSource(source);
		newDepense.setSourceTypeCredit(sourceCreditType);
		setPdepHtSaisie(newDepense, depHtSaisie);
		setPdepTvaSaisie(newDepense, depTvaSaisie);
		setPdepTtcSaisie(newDepense, depTtcSaisie);
		setPdepMontantBudgetaire(newDepense, depMontantBudgetaire);

		affecterDepensePapier(newDepense, depensePapier);
		affecterEngagementBudget(newDepense, engagementBudget);
		affecterTauxProrata(newDepense, tauxProrata);
		affecterExercice(newDepense, exercice);
		affecterUtilisateur(newDepense, utilisateur);

		newDepense.creerRepartitionAutomatique();

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseBudgetException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseBudget copie(EOEditingContext ed, EOPreDepenseBudget depenseBudget) {

		EOPreDepenseBudget newDepense = creer(ed);

		setPdepHtSaisie(newDepense, depenseBudget.depHtSaisie());
		setPdepTvaSaisie(newDepense, depenseBudget.depTvaSaisie());
		setPdepTtcSaisie(newDepense, depenseBudget.depTtcSaisie());
		setPdepMontantBudgetaire(newDepense, depenseBudget.depMontantBudgetaire());

		affecterDepensePapier(newDepense, depenseBudget.depensePapier());
		affecterEngagementBudget(newDepense, depenseBudget.engagementBudget());
		affecterTauxProrata(newDepense, depenseBudget.tauxProrata());
		affecterExercice(newDepense, depenseBudget.exercice());
		affecterUtilisateur(newDepense, depenseBudget.utilisateur());

		return newDepense;
	}

	public EOPreDepenseBudget creerSansRepartAuto(EOEditingContext ed, BigDecimal depHtSaisie, BigDecimal depTvaSaisie,
			BigDecimal depTtcSaisie, BigDecimal depMontantBudgetaire, EODepensePapier depensePapier,
			EOEngagementBudget engagementBudget, EOTauxProrata tauxProrata, EOExercice exercice, EOUtilisateur utilisateur)
			throws DepenseBudgetException {

		EOPreDepenseBudget newDepense = creer(ed);

		setPdepHtSaisie(newDepense, depHtSaisie);
		setPdepTvaSaisie(newDepense, depTvaSaisie);
		setPdepTtcSaisie(newDepense, depTtcSaisie);
		setPdepMontantBudgetaire(newDepense, depMontantBudgetaire);

		affecterDepensePapier(newDepense, depensePapier);
		affecterEngagementBudget(newDepense, engagementBudget);
		affecterTauxProrata(newDepense, tauxProrata);
		affecterExercice(newDepense, exercice);
		affecterUtilisateur(newDepense, utilisateur);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseBudgetException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseBudget creer(EOEditingContext ed) throws DepenseBudgetException {
		EOPreDepenseBudget newDepense = (EOPreDepenseBudget) Factory.instanceForEntity(ed, EOPreDepenseBudget.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));

		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setPdepHtSaisie(EOPreDepenseBudget depense, BigDecimal depHtSaisie) {
		if (depense == null)
			return;
		depense.setDepHtSaisie(depHtSaisie);
	}

	public void setPdepTvaSaisie(EOPreDepenseBudget depense, BigDecimal depTvaSaisie) {
		if (depense == null)
			return;
		depense.setDepTvaSaisie(depTvaSaisie);
	}

	public void setPdepTtcSaisie(EOPreDepenseBudget depense, BigDecimal depTtcSaisie) {
		if (depense == null)
			return;
		depense.setDepTtcSaisie(depTtcSaisie);
	}

	public void setPdepMontantBudgetaire(EOPreDepenseBudget depense, BigDecimal depMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDepMontantBudgetaire(depMontantBudgetaire);
	}

	public void affecterDepensePapier(EOPreDepenseBudget depense, EODepensePapier depensePapier) {
		depense.setDepensePapierRelationship(depensePapier);
	}

	public void affecterEngagementBudget(EOPreDepenseBudget depense, EOEngagementBudget engagementBudget) {
		depense.setEngagementBudgetRelationship(engagementBudget);
		if (engagementBudget != null)
			engagementBudget.majDecimales();
	}

	public void affecterTauxProrata(EOPreDepenseBudget depense, EOTauxProrata tauxProrata) {
		depense.setTauxProrataRelationship(tauxProrata);
	}

	public void affecterUtilisateur(EOPreDepenseBudget depense, EOUtilisateur utilisateur) {
		depense.setUtilisateurRelationship(utilisateur);
	}

	public void affecterExercice(EOPreDepenseBudget depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseBudget depense) throws DepenseBudgetException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setDepensePapierRelationship(null);
		depense.setEngagementBudgetRelationship(null);
		depense.setTauxProrataRelationship(null);
		depense.setUtilisateurRelationship(null);
		depense.setExerciceRelationship(null);

		supprimerDepenseContoleActions(depense);
		supprimerDepenseContoleAnalytiques(depense);
		supprimerDepenseContoleConventions(depense);
		supprimerDepenseContoleHorsMarches(depense);
		supprimerDepenseContoleMarches(depense);
		supprimerDepenseContolePlanComptables(depense);

		ed.deleteObject(depense);
	}

	public void supprimerDepenseContoleActions(EOPreDepenseBudget depense) {
		NSArray larray = depense.preDepenseControleActions();

		if (larray != null && larray.count() > 0) {
			FactoryPreDepenseControleAction factory = new FactoryPreDepenseControleAction();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(depense.editingContext(), (EOPreDepenseControleAction) larray.objectAtIndex(i));
		}
	}

	public void supprimerDepenseContoleAnalytiques(EOPreDepenseBudget depense) {
		NSArray larray = depense.preDepenseControleAnalytiques();

		if (larray != null && larray.count() > 0) {
			FactoryPreDepenseControleAnalytique factory = new FactoryPreDepenseControleAnalytique();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(depense.editingContext(), (EOPreDepenseControleAnalytique) larray.objectAtIndex(i));
		}
	}

	public void supprimerDepenseContoleConventions(EOPreDepenseBudget depense) {
		NSArray larray = depense.preDepenseControleConventions();

		if (larray != null && larray.count() > 0) {
			FactoryPreDepenseControleConvention factory = new FactoryPreDepenseControleConvention();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(depense.editingContext(), (EOPreDepenseControleConvention) larray.objectAtIndex(i));
		}
	}

	public void supprimerDepenseContoleHorsMarches(EOPreDepenseBudget depense) {
		NSArray larray = depense.preDepenseControleHorsMarches();

		if (larray != null && larray.count() > 0) {
			FactoryPreDepenseControleHorsMarche factory = new FactoryPreDepenseControleHorsMarche();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(depense.editingContext(), (EOPreDepenseControleHorsMarche) larray.objectAtIndex(i));
		}
	}

	public void supprimerDepenseContoleMarches(EOPreDepenseBudget depense) {
		NSArray larray = depense.preDepenseControleMarches();

		if (larray != null && larray.count() > 0) {
			FactoryPreDepenseControleMarche factory = new FactoryPreDepenseControleMarche();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(depense.editingContext(), (EOPreDepenseControleMarche) larray.objectAtIndex(i));
		}
	}

	public void supprimerDepenseContolePlanComptables(EOPreDepenseBudget depense) {
		NSArray larray = depense.preDepenseControlePlanComptables();

		if (larray != null && larray.count() > 0) {
			FactoryPreDepenseControlePlanComptable factory = new FactoryPreDepenseControlePlanComptable();
			for (int i = larray.count() - 1; i >= 0; i--)
				factory.supprimer(depense.editingContext(), (EOPreDepenseControlePlanComptable) larray.objectAtIndex(i));
		}
	}

	public void verifier(EOPreDepenseBudget depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseBudget copie(EOEditingContext ed, _IDepenseBudget depenseBudget) {
		return copie(ed, (EOPreDepenseBudget) depenseBudget);
	}

	public void setDepHtSaisie(_IDepenseBudget depense, BigDecimal depHtSaisie) {
		setDepHtSaisie((EOPreDepenseBudget) depense, depHtSaisie);

	}

	public void setDepTvaSaisie(_IDepenseBudget depense, BigDecimal depTvaSaisie) {
		setDepTvaSaisie((EOPreDepenseBudget) depense, depTvaSaisie);

	}

	public void setDepTtcSaisie(_IDepenseBudget depense, BigDecimal depTtcSaisie) {
		setDepTtcSaisie((EOPreDepenseBudget) depense, depTtcSaisie);

	}

	public void setDepMontantBudgetaire(_IDepenseBudget depense, BigDecimal depMontantBudgetaire) {
		setDepMontantBudgetaire((EOPreDepenseBudget) depense, depMontantBudgetaire);

	}

	public void affecterDepensePapier(_IDepenseBudget depense, EODepensePapier depensePapier) {
		affecterDepensePapier((EOPreDepenseBudget) depense, depensePapier);

	}

	public void affecterEngagementBudget(_IDepenseBudget depense, EOEngagementBudget engagementBudget) {
		affecterEngagementBudget((EOPreDepenseBudget) depense, engagementBudget);

	}

	public void affecterTauxProrata(_IDepenseBudget depense, EOTauxProrata tauxProrata) {
		affecterTauxProrata((EOPreDepenseBudget) depense, tauxProrata);

	}

	public void affecterUtilisateur(_IDepenseBudget depense, EOUtilisateur utilisateur) {
		affecterUtilisateur((EOPreDepenseBudget) depense, utilisateur);

	}

	public void affecterExercice(_IDepenseBudget depense, EOExercice exercice) {
		affecterExercice((EOPreDepenseBudget) depense, exercice);

	}

	public void supprimer(EOEditingContext ed, _IDepenseBudget depense) throws DepenseBudgetException {
		supprimer(ed, (EOPreDepenseBudget) depense);

	}

	public void supprimerDepenseContoleActions(_IDepenseBudget depense) {
		supprimerDepenseContoleActions((EOPreDepenseBudget) depense);

	}

	public void supprimerDepenseContoleAnalytiques(_IDepenseBudget depense) {
		supprimerDepenseContoleAnalytiques((EOPreDepenseBudget) depense);

	}

	public void supprimerDepenseContoleConventions(_IDepenseBudget depense) {
		supprimerDepenseContoleConventions((EOPreDepenseBudget) depense);

	}

	public void supprimerDepenseContoleHorsMarches(_IDepenseBudget depense) {
		supprimerDepenseContoleHorsMarches((EOPreDepenseBudget) depense);

	}

	public void supprimerDepenseContoleMarches(_IDepenseBudget depense) {
		supprimerDepenseContoleMarches((EOPreDepenseBudget) depense);

	}

	public void supprimerDepenseContolePlanComptables(_IDepenseBudget depense) {
		supprimerDepenseContolePlanComptables((EOPreDepenseBudget) depense);

	}

	public void verifier(_IDepenseBudget depense) {
		verifier((EOPreDepenseBudget) depense);

	}
}
