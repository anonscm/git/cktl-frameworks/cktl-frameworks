/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeControleActionException;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FactoryCommandeControleAction extends Factory {
	
	public FactoryCommandeControleAction() {
		super();
	}
	
	public FactoryCommandeControleAction(boolean withLog) {
		super(withLog);
	}
	
	protected NSArray dupliquer(EOEditingContext ed, EOCommandeBudget origine, EOCommandeBudget commandeBudget) throws CommandeControleActionException {
		NSMutableArray array=new NSMutableArray();
		
		for (int i=0; i<origine.commandeControleActions().count(); i++) {
			EOCommandeControleAction controle=(EOCommandeControleAction)origine.commandeControleActions().objectAtIndex(i);
			
			EOTypeAction newAction=controle.typeAction();
			if (!commandeBudget.exercice().exeExercice().equals(origine.exercice().exeExercice())) {
				newAction=FinderTypeAction.getLesTypeActions(ed, commandeBudget.exercice(), newAction.tyacCode());
			}

			array.addObject(creer(ed, controle.cactHtSaisie(), controle.cactTvaSaisie(), controle.cactTtcSaisie(), 
					controle.cactMontantBudgetaire(), controle.cactPourcentage(), newAction, commandeBudget.exercice(), commandeBudget));
		}
		
		return array;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, NSArray controles) {
		for (int i=0; i<controles.count(); i++) {
			EOCommandeControleAction controle=(EOCommandeControleAction)controles.objectAtIndex(i);
			if (!verifier(exercice, utilisateur, organ, typeCredit, controle))
				return false;
		}
		return true;
	}

	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOCommandeControleAction controle) {
		EOTypeAction newAction=controle.typeAction();
		if (!exercice.exeExercice().equals(controle.exercice().exeExercice())) {
			newAction=FinderTypeAction.getLesTypeActions(controle.editingContext(), exercice, newAction.tyacCode());
		}

		return verifier(exercice, utilisateur, organ, typeCredit, newAction);
	}
	
	protected boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOOrgan organ, EOTypeCredit typeCredit, EOTypeAction typeAction) {
		if (exercice==null || utilisateur==null || organ==null || typeCredit==null || typeAction==null)
			return false;
		if (!FinderTypeAction.getTypeActions(typeAction.editingContext(), exercice, organ, typeCredit, utilisateur).containsObject(typeAction))
			return false;
		return true;
	}

	public EOCommandeControleAction creer(EOEditingContext ed, BigDecimal cactHtSaisie, BigDecimal cactTvaSaisie, 
			BigDecimal cactTtcSaisie, BigDecimal cactMontantBudgetaire, BigDecimal cactPourcentage, 
			EOTypeAction typeAction, EOExercice exercice, EOCommandeBudget commandeBudget)
	throws CommandeControleActionException {
		
		EOCommandeControleAction newCommande = creer(ed);
		
		setCactHtSaisie(newCommande, cactHtSaisie);
		setCactTvaSaisie(newCommande, cactTvaSaisie);
		setCactTtcSaisie(newCommande, cactTtcSaisie);
		setCactMontantBudgetaire(newCommande, cactMontantBudgetaire);
		setCactPourcentage(newCommande, cactPourcentage);
		
		affecterTypeAction(newCommande, typeAction);
		affecterExercice(newCommande, exercice);
		affecterCommandeBudget(newCommande, commandeBudget);
		
		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeControleActionException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}
		
		return null;
	}
	
	public EOCommandeControleAction creer(EOEditingContext ed) throws CommandeControleActionException {
		EOCommandeControleAction newCommande=(EOCommandeControleAction)Factory.instanceForEntity(ed,EOCommandeControleAction.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newCommande);
		return newCommande;
	}
	
	
	public void setCactHtSaisie(EOCommandeControleAction commande, BigDecimal cactHtSaisie) {
		if (commande==null)
			return;
		commande.setCactHtSaisie(cactHtSaisie);
	}
	
	public void setCactTvaSaisie(EOCommandeControleAction commande, BigDecimal cactTvaSaisie) {
		if (commande==null)
			return;
		commande.setCactTvaSaisie(cactTvaSaisie);
	}
	
	public void setCactTtcSaisie(EOCommandeControleAction commande, BigDecimal cactTtcSaisie) {
		if (commande==null)
			return;
		commande.setCactTtcSaisie(cactTtcSaisie);
	}
	
	public void setCactMontantBudgetaire(EOCommandeControleAction commande, BigDecimal cactMontantBudgetaire) {
		if (commande==null)
			return;
		commande.setCactMontantBudgetaire(cactMontantBudgetaire);
	}
	
	public void setCactPourcentage(EOCommandeControleAction commande, BigDecimal cactPourcentage) {
		if (commande==null)
			return;
		commande.setCactPourcentage(cactPourcentage);
	}
	
	public void affecterTypeAction(EOCommandeControleAction commande, EOTypeAction typeAction) {
		commande.setTypeActionRelationship(typeAction);
	}
	
	public void affecterExercice(EOCommandeControleAction commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
	}
	
	public void affecterCommandeBudget(EOCommandeControleAction commande, EOCommandeBudget commandeBudget) {
		commande.setCommandeBudgetRelationship(commandeBudget);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeControleAction commande) throws CommandeControleActionException {
		commande.setTypeActionRelationship(null);
		commande.setExerciceRelationship(null);
		commande.setCommandeBudgetRelationship(null);
		
		ed.deleteObject(commande);
	}
	
	public void verifier(EOCommandeControleAction commande) {
		commande.validateObjectMetier();
	}
}
