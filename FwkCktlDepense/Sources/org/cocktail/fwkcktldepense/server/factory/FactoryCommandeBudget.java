/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.CommandeBudgetException;
import org.cocktail.fwkcktldepense.server.finder.FinderBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOOrganProrata;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class FactoryCommandeBudget extends Factory {

	public FactoryCommandeBudget() {
		super();
	}

	public FactoryCommandeBudget(boolean withLog) {
		super(withLog);
	}

	public EOCommandeBudget dupliquer(EOEditingContext ed, EOCommandeBudget commandeBudget, EOCommande commande) throws CommandeBudgetException {

		// verifs si ce duplicable
		if (commande.utilisateur()==null || commande.exercice()==null)
			return null;
		
		EOTypeCredit newTypeCredit=commandeBudget.typeCredit();
		if (!commande.exercice().exeExercice().equals(commandeBudget.exercice().exeExercice())) {
			NSMutableDictionary dico=new NSMutableDictionary();
			dico.setObjectForKey(commande.exercice(), "exercice");
			dico.setObjectForKey(commandeBudget.typeCredit().tcdCode(), "tcdCode");
			newTypeCredit=FinderTypeCredit.getTypeCredit(ed, dico);
		}
		
		if (!verifier(commande.exercice(), commande.utilisateur(), commandeBudget.tauxProrata(), commandeBudget.organ(), newTypeCredit))
			return null;
		
		if (!new FactoryCommandeControleAction().verifier(commande.exercice(), commande.utilisateur(), commandeBudget.organ(), 
				newTypeCredit, commandeBudget.commandeControleActions()))
			return null;
		if (!new FactoryCommandeControleAnalytique().verifier(commande.exercice(), commandeBudget.organ(), commandeBudget.commandeControleAnalytiques()))
			return null;
		if (!new FactoryCommandeControleConvention().verifier(commande.exercice(), commande.utilisateur(), commandeBudget.organ(), 
				newTypeCredit, commandeBudget.commandeControleConventions()))
			return null;
		if (!new FactoryCommandeControleHorsMarche().verifier(commande.exercice(), commande.utilisateur(), commandeBudget.organ(), 
				newTypeCredit, commandeBudget.commandeControleHorsMarches()))
			return null;
		if (!new FactoryCommandeControleMarche().verifier(commande.exercice(), commande.utilisateur(), commandeBudget.organ(), 
				newTypeCredit, commandeBudget.commandeControleMarches()))
			return null;
		if (!new FactoryCommandeControlePlanComptable().verifier(commande.exercice(), commande.utilisateur(), commandeBudget.organ(), 
				newTypeCredit, commandeBudget.commandeControlePlanComptables()))
			return null;

		// creation commandeBudget
		EOCommandeBudget newCommandeBudget=creer(ed, commandeBudget.cbudHtSaisie(), commandeBudget.cbudTvaSaisie(), 
				commandeBudget.cbudTtcSaisie(), commandeBudget.cbudMontantBudgetaire(), commandeBudget.organ(), commandeBudget.tauxProrata(), 
				newTypeCredit, commande.exercice(), commande);
		
		// creation repartition
		new FactoryCommandeControleAction().dupliquer(ed, commandeBudget, newCommandeBudget);
		new FactoryCommandeControleAnalytique().dupliquer(ed, commandeBudget, newCommandeBudget);
		new FactoryCommandeControleConvention().dupliquer(ed, commandeBudget, newCommandeBudget);
		new FactoryCommandeControleHorsMarche().dupliquer(ed, commandeBudget, newCommandeBudget);
		new FactoryCommandeControleMarche().dupliquer(ed, commandeBudget, newCommandeBudget);
		new FactoryCommandeControlePlanComptable().dupliquer(ed, commandeBudget, newCommandeBudget);
		
		return newCommandeBudget;
	}

	public boolean verifier(EOExercice exercice, EOUtilisateur utilisateur, EOTauxProrata tauxProrata, EOOrgan organ, EOTypeCredit typeCredit) {

		if (!((NSArray)organ.organProratas().valueForKeyPath(EOOrganProrata.TAUX_PRORATA_KEY)).containsObject(tauxProrata))
			return false;
		
		NSMutableDictionary bindings=new NSMutableDictionary();
		bindings.setObjectForKey(organ, "organ");
		bindings.setObjectForKey(exercice, "exercice");
		bindings.setObjectForKey(utilisateur, "utilisateur");
		bindings.setObjectForKey(typeCredit, "typeCredit");
		
		if (FinderBudget.getBudgets(utilisateur.editingContext(), bindings).count()<=0)
			return false;
		
		return true;
	}
	
	/**
	 * 
	 * @param ed
	 * @param cbudHtSaisie
	 * @param cbudTvaSaisie
	 * @param cbudTtcSaisie
	 * @param cbudMontantBudgetaire
	 * @param source
	 * @param exercice
	 * @param commande
	 * @return
	 * @throws CommandeBudgetException
	 */
	public EOCommandeBudget creer(EOEditingContext ed, BigDecimal cbudHtSaisie, BigDecimal cbudTvaSaisie, 
			BigDecimal cbudTtcSaisie, BigDecimal cbudMontantBudgetaire, EOSource source, EOCommande commande)
	throws CommandeBudgetException {
		
		return creer(ed, cbudHtSaisie, cbudTvaSaisie, cbudTtcSaisie, cbudMontantBudgetaire, source.organ(), source.tauxProrata(), 
				source.typeCredit(), source.exercice(), commande);
	}

	/**
	 * 
	 * @param ed
	 * @param cbudHtSaisie
	 * @param cbudTvaSaisie
	 * @param cbudTtcSaisie
	 * @param cbudMontantBudgetaire
	 * @param organ
	 * @param tauxProrata
	 * @param typeCredit
	 * @param exercice
	 * @param commande
	 * @return
	 * @throws CommandeBudgetException
	 */
	public EOCommandeBudget creer(EOEditingContext ed, BigDecimal cbudHtSaisie, BigDecimal cbudTvaSaisie, 
			BigDecimal cbudTtcSaisie, BigDecimal cbudMontantBudgetaire, 
			EOOrgan organ, EOTauxProrata tauxProrata, EOTypeCredit typeCredit,
			EOExercice exercice, EOCommande commande)
	throws CommandeBudgetException {
		
		EOCommandeBudget newCommande = creer(ed);
		
		setCbudHtSaisie(newCommande, cbudHtSaisie);
		setCbudTvaSaisie(newCommande, cbudTvaSaisie);
		setCbudTtcSaisie(newCommande, cbudTtcSaisie);
		setCbudMontantBudgetaire(newCommande, cbudMontantBudgetaire);
		
		affecterOrgan(newCommande, organ);
		affecterTauxProrata(newCommande, tauxProrata);
		affecterTypeCredit(newCommande, typeCredit);
		affecterExercice(newCommande, exercice);
		affecterCommande(newCommande, commande);
		
		try {
			newCommande.validateObjectMetier();
			return newCommande;
		} catch (CommandeBudgetException e) {
			e.printStackTrace();
			System.out.println(newCommande);
		}
		
		return null;
	}
	
	public EOCommandeBudget creer(EOEditingContext ed) throws CommandeBudgetException {
		EOCommandeBudget newCommande=(EOCommandeBudget)Factory.instanceForEntity(ed,EOCommandeBudget.ENTITY_NAME);
		ed.insertObject(newCommande);
		return newCommande;
	}
	
    public void setSource(EOCommandeBudget commande, EOSource source) {
    	affecterOrgan(commande, source.organ());
    	affecterTypeCredit(commande, source.typeCredit());
    	affecterTauxProrata(commande, source.tauxProrata());
    	affecterExercice(commande, source.exercice());
    }

	public void setCbudHtSaisie(EOCommandeBudget commande, BigDecimal cbudHtSaisie) {
		if (commande==null)
			return;
		commande.setCbudHtSaisie(cbudHtSaisie);
	}
	
	public void setCbudTvaSaisie(EOCommandeBudget commande, BigDecimal cbudTvaSaisie) {
		if (commande==null)
			return;
		commande.setCbudTvaSaisie(cbudTvaSaisie);
	}
	
	public void setCbudTtcSaisie(EOCommandeBudget commande, BigDecimal cbudTtcSaisie) {
		if (commande==null)
			return;
		commande.setCbudTtcSaisie(cbudTtcSaisie);
	}
	
	public void setCbudMontantBudgetaire(EOCommandeBudget commande, BigDecimal cbudMontantBudgetaire) {
		if (commande==null)
			return;
		commande.setCbudMontantBudgetaire(cbudMontantBudgetaire);
	}
	
	public void affecterOrgan(EOCommandeBudget commande, EOOrgan organ) {
		commande.setOrganRelationship(organ);
		affecterConventionRA(commande);
	}
	
	public void affecterTauxProrata(EOCommandeBudget commande, EOTauxProrata tauxProrata) {
		commande.setTauxProrataRelationship(tauxProrata);
	}

	public void affecterTypeCredit(EOCommandeBudget commande, EOTypeCredit typeCredit) {
		commande.setTypeCreditRelationship(typeCredit);
		affecterConventionRA(commande);
	}

	public void affecterCommande(EOCommandeBudget commande, EOCommande cde) {
		commande.setCommandeRelationship(cde);
	}
	
	public void affecterExercice(EOCommandeBudget commande, EOExercice exercice) {
		commande.setExerciceRelationship(exercice);
		affecterConventionRA(commande);
	}
		
	private void affecterConventionRA(EOCommandeBudget commande) {
		if (commande==null)
			return;
		if (commande.organ()==null || commande.typeCredit()==null || commande.exercice()==null) {
			supprimerCommandeContoleConventions(commande);
			return;
		}
		if (commande.commandeControleConventions()!=null && commande.commandeControleConventions().count()>0)
			return;
		
		NSMutableDictionary bindings=new NSMutableDictionary();
		bindings.setObjectForKey(commande.organ(), "organ");
		bindings.setObjectForKey(commande.typeCredit(), "typeCredit");
		bindings.setObjectForKey(commande.exercice(), "exercice");
		
		NSArray conventionsRA=FinderConvention.getConventionsRessourcesAffectees(commande.editingContext(), bindings);
		
		if (conventionsRA!=null && conventionsRA.count()==1)
			(new FactoryCommandeControleConvention()).creer(commande.editingContext(), commande.cbudHtSaisie(), commande.cbudTvaSaisie(), 
					commande.cbudTtcSaisie(), commande.cbudMontantBudgetaire(), new BigDecimal(100.0), (EOConvention)conventionsRA.objectAtIndex(0),
					commande.exercice(), commande);
	}
	
	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOCommandeBudget commande) throws CommandeBudgetException {
		
		commande.setCommandeRelationship(null);
		
		commande.setOrganRelationship(null);
		commande.setTauxProrataRelationship(null);
		commande.setTypeCreditRelationship(null);
		commande.setExerciceRelationship(null);
		
		supprimerCommandeContoleActions(commande);
    	supprimerCommandeContoleAnalytiques(commande);
    	supprimerCommandeContoleConventions(commande);
    	supprimerCommandeContoleHorsMarches(commande);
    	supprimerCommandeContoleMarches(commande);
    	supprimerCommandeContolePlanComptables(commande);
    	
		ed.deleteObject(commande);
	}
	
	public void supprimerCommandeContoleActions(EOCommandeBudget commande) {
    	NSArray larray=commande.commandeControleActions();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryCommandeControleAction factory=new FactoryCommandeControleAction();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(commande.editingContext(), (EOCommandeControleAction)larray.objectAtIndex(i));
    	}
	}

	public void supprimerCommandeContoleAnalytiques(EOCommandeBudget commande) {
    	NSArray larray=commande.commandeControleAnalytiques();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryCommandeControleAnalytique factory=new FactoryCommandeControleAnalytique();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(commande.editingContext(), (EOCommandeControleAnalytique)larray.objectAtIndex(i));
    	}
	}

	public void supprimerCommandeContoleConventions(EOCommandeBudget commande) {
    	NSArray larray=commande.commandeControleConventions();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryCommandeControleConvention factory=new FactoryCommandeControleConvention();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(commande.editingContext(), (EOCommandeControleConvention)larray.objectAtIndex(i));
    	}
	}

	public void supprimerCommandeContoleHorsMarches(EOCommandeBudget commande) {
    	NSArray larray=commande.commandeControleHorsMarches();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryCommandeControleHorsMarche factory=new FactoryCommandeControleHorsMarche();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(commande.editingContext(), (EOCommandeControleHorsMarche)larray.objectAtIndex(i));
    	}
	}

	public void supprimerCommandeContoleMarches(EOCommandeBudget commande) {
    	NSArray larray=commande.commandeControleMarches();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryCommandeControleMarche factory=new FactoryCommandeControleMarche();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(commande.editingContext(), (EOCommandeControleMarche)larray.objectAtIndex(i));
    	}
	}
	
	public void supprimerCommandeContolePlanComptables(EOCommandeBudget commande) {
    	NSArray larray=commande.commandeControlePlanComptables();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryCommandeControlePlanComptable factory=new FactoryCommandeControlePlanComptable();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(commande.editingContext(), (EOCommandeControlePlanComptable)larray.objectAtIndex(i));
    	}
	}

	public void verifier(EOCommandeBudget commande) {
		commande.validateObjectMetier();
	}
}
