/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPreDepenseControleHorsMarche extends Factory implements _IFactoryDepenseControleHorsMarche {

	public FactoryPreDepenseControleHorsMarche() {
		super();
	}

	public FactoryPreDepenseControleHorsMarche(boolean withLog) {
		super(withLog);
	}

	public EOPreDepenseControleHorsMarche copie(EOEditingContext ed, EOPreDepenseControleHorsMarche depense, EOPreDepenseBudget budget) {

		EOPreDepenseControleHorsMarche newDepense = creer(ed);

		setPdhomHtSaisie(newDepense, depense.dhomHtSaisie());
		setPdhomTvaSaisie(newDepense, depense.dhomTvaSaisie());
		setPdhomTtcSaisie(newDepense, depense.dhomTtcSaisie());
		setPdhomMontantBudgetaire(newDepense, depense.dhomMontantBudgetaire());

		affecterTypeAchat(newDepense, depense.typeAchat());
		affecterCodeExer(newDepense, depense.codeExer());
		affecterExercice(newDepense, depense.exercice());
		affecterPreDepenseBudget(newDepense, budget);

		setPdhomPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}

	public EOPreDepenseControleHorsMarche creer(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie,
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat,
			EOExercice exercice, EOPreDepenseBudget depenseBudget)
			throws DepenseControleHorsMarcheException {

		EOPreDepenseControleHorsMarche newDepense = creer(ed);

		setPdhomHtSaisie(newDepense, dhomHtSaisie);
		setPdhomTvaSaisie(newDepense, dhomTvaSaisie);
		setPdhomTtcSaisie(newDepense, dhomTtcSaisie);
		setPdhomMontantBudgetaire(newDepense, dhomMontantBudgetaire);

		affecterCodeExer(newDepense, codeExer);
		affecterExercice(newDepense, exercice);
		affecterTypeAchat(newDepense, typeAchat);
		affecterPreDepenseBudget(newDepense, depenseBudget);

		setPdhomPourcentage(newDepense, dhomPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleHorsMarcheException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EOPreDepenseControleHorsMarche creer(EOEditingContext ed) throws DepenseControleHorsMarcheException {
		EOPreDepenseControleHorsMarche newDepense = (EOPreDepenseControleHorsMarche) Factory.
				instanceForEntity(ed, EOPreDepenseControleHorsMarche.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setPdhomPourcentage(EOPreDepenseControleHorsMarche depense, BigDecimal dhomPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(dhomPourcentage);
	}

	public void setPdhomHtSaisie(EOPreDepenseControleHorsMarche depense, BigDecimal dhomHtSaisie) {
		if (depense == null)
			return;
		depense.setDhomHtSaisie(dhomHtSaisie);
	}

	public void setPdhomTvaSaisie(EOPreDepenseControleHorsMarche depense, BigDecimal dhomTvaSaisie) {
		if (depense == null)
			return;
		depense.setDhomTvaSaisie(dhomTvaSaisie);
	}

	public void setPdhomTtcSaisie(EOPreDepenseControleHorsMarche depense, BigDecimal dhomTtcSaisie) {
		if (depense == null)
			return;
		depense.setDhomTtcSaisie(dhomTtcSaisie);
	}

	public void setPdhomMontantBudgetaire(EOPreDepenseControleHorsMarche depense, BigDecimal dhomMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDhomMontantBudgetaire(dhomMontantBudgetaire);
	}

	public void affecterCodeExer(EOPreDepenseControleHorsMarche depense, EOCodeExer codeExer) {
		depense.setCodeExerRelationship(codeExer);
	}

	public void affecterTypeAchat(EOPreDepenseControleHorsMarche depense, EOTypeAchat typeAchat) {
		depense.setTypeAchatRelationship(typeAchat);
	}

	public void affecterExercice(EOPreDepenseControleHorsMarche depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterPreDepenseBudget(EOPreDepenseControleHorsMarche depense, EOPreDepenseBudget depenseBudget) {
		depense.setPreDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EOPreDepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setCodeExerRelationship(null);
		depense.setTypeAchatRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setPreDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EOPreDepenseControleHorsMarche depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleHorsMarche _copie(EOEditingContext ed, _IDepenseControleHorsMarche depense, _IDepenseBudget budget) {
		return copie(ed, (EOPreDepenseControleHorsMarche) depense, (EOPreDepenseBudget) budget);
	}

	public _IDepenseControleHorsMarche _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleHorsMarcheException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, codeExer, typeAchat, exercice, (EOPreDepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControleHorsMarche depense, BigDecimal dactPourcentage) {
		setPdhomPourcentage((EOPreDepenseControleHorsMarche) depense, dactPourcentage);
	}

	public void setHtSaisie(_IDepenseControleHorsMarche depense, BigDecimal htSaisie) {
		setPdhomHtSaisie((EOPreDepenseControleHorsMarche) depense, htSaisie);

	}

	public void setTvaSaisie(_IDepenseControleHorsMarche depense, BigDecimal tvaSaisie) {
		setPdhomTvaSaisie((EOPreDepenseControleHorsMarche) depense, tvaSaisie);

	}

	public void setTtcSaisie(_IDepenseControleHorsMarche depense, BigDecimal ttcSaisie) {
		setPdhomTtcSaisie((EOPreDepenseControleHorsMarche) depense, ttcSaisie);

	}

	public void setMontantBudgetaire(_IDepenseControleHorsMarche depense, BigDecimal montantBudgetaire) {
		setPdhomMontantBudgetaire((EOPreDepenseControleHorsMarche) depense, montantBudgetaire);

	}

	public void _affecterExercice(_IDepenseControleHorsMarche depense, EOExercice exercice) {
		affecterExercice((EOPreDepenseControleHorsMarche) depense, exercice);
	}

	public void _affecterDepenseBudget(_IDepenseControleHorsMarche depense, _IDepenseBudget depenseBudget) {
		affecterPreDepenseBudget((EOPreDepenseControleHorsMarche) depense, (EOPreDepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException {
		supprimer(ed, (EOPreDepenseControleHorsMarche) depense);

	}

	public void _verifier(_IDepenseControleHorsMarche depense) {
		verifier((EOPreDepenseControleHorsMarche) depense);

	}

	public void _affecterCodeExer(_IDepenseControleHorsMarche depense, EOCodeExer codeExer) {
		affecterCodeExer((EOPreDepenseControleHorsMarche) depense, codeExer);

	}

	public void _affecterTypeAchat(_IDepenseControleHorsMarche depense, EOTypeAchat typeAchat) {
		affecterTypeAchat((EOPreDepenseControleHorsMarche) depense, typeAchat);

	}

}
