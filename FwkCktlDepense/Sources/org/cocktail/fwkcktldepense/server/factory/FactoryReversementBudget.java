/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryReversementBudget extends Factory {

	public FactoryReversementBudget() {
		super();
	}

	public FactoryReversementBudget(boolean withLog) {
		super(withLog);
	}

	public EODepenseBudget creer(EOEditingContext ed, BigDecimal depHtSaisie, BigDecimal depTvaSaisie, 
			BigDecimal depTtcSaisie, BigDecimal depMontantBudgetaire, EODepensePapier depensePapier,
			EODepenseBudget depenseBudget,  EOExercice exercice, EOUtilisateur utilisateur)
	throws DepenseBudgetException {
		
		EODepenseBudget newDepense = creer(ed);
		
		newDepense.setSolde(false);
		
		setDepHtSaisie(newDepense, depHtSaisie);
		setDepTvaSaisie(newDepense, depTvaSaisie);
		setDepTtcSaisie(newDepense, depTtcSaisie);
		setDepMontantBudgetaire(newDepense, depMontantBudgetaire);
		
		affecterDepensePapier(newDepense, depensePapier);
		affecterEngagementBudget(newDepense, depenseBudget.engagementBudget());
		affecterTauxProrata(newDepense, depenseBudget.tauxProrata());
		affecterExercice(newDepense, exercice);
		affecterUtilisateur(newDepense, utilisateur);
		affecterDepenseBudgetReversement(newDepense, depenseBudget);

		newDepense.creerRepartitionAutomatique();

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseBudgetException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EODepenseBudget creerSansRepartAuto(EOEditingContext ed, BigDecimal depHtSaisie, BigDecimal depTvaSaisie, 
			BigDecimal depTtcSaisie, BigDecimal depMontantBudgetaire, EODepensePapier depensePapier,
			EODepenseBudget depenseBudget, EOExercice exercice, EOUtilisateur utilisateur)
	throws DepenseBudgetException {
		
		EODepenseBudget newDepense = creer(ed);
		
		newDepense.setSolde(false);

		setDepHtSaisie(newDepense, depHtSaisie);
		setDepTvaSaisie(newDepense, depTvaSaisie);
		setDepTtcSaisie(newDepense, depTtcSaisie);
		setDepMontantBudgetaire(newDepense, depMontantBudgetaire);
		
		affecterDepensePapier(newDepense, depensePapier);
		affecterEngagementBudget(newDepense, depenseBudget.engagementBudget());
		affecterTauxProrata(newDepense, depenseBudget.tauxProrata());
		affecterExercice(newDepense, exercice);
		affecterUtilisateur(newDepense, utilisateur);
		affecterDepenseBudgetReversement(newDepense, depenseBudget);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseBudgetException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}
		
		return null;
	}

	public EODepenseBudget creer(EOEditingContext ed) throws DepenseBudgetException {
		EODepenseBudget newDepense=(EODepenseBudget)Factory.instanceForEntity(ed,EODepenseBudget.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		
		newDepense.setSolde(false);

		ed.insertObject(newDepense);
		return newDepense;
	}
	
	
	public void setDepHtSaisie(EODepenseBudget depense, BigDecimal depHtSaisie) {
		if (depense==null)
			return;
		depense.setDepHtSaisie(depHtSaisie);
	}
	
	public void setDepTvaSaisie(EODepenseBudget depense, BigDecimal depTvaSaisie) {
		if (depense==null)
			return;
		depense.setDepTvaSaisie(depTvaSaisie);
	}
	
	public void setDepTtcSaisie(EODepenseBudget depense, BigDecimal depTtcSaisie) {
		if (depense==null)
			return;
		depense.setDepTtcSaisie(depTtcSaisie);
	}
	
	public void setDepMontantBudgetaire(EODepenseBudget depense, BigDecimal depMontantBudgetaire) {
		if (depense==null)
			return;
		depense.setDepMontantBudgetaire(depMontantBudgetaire);
	}
	
	public void affecterDepensePapier(EODepenseBudget depense, EODepensePapier depensePapier) {
		depense.setDepensePapierRelationship(depensePapier);
	}
	
	public void affecterEngagementBudget(EODepenseBudget depense, EOEngagementBudget engagementBudget) {
		depense.setEngagementBudgetRelationship(engagementBudget);
		if (engagementBudget!=null)
			engagementBudget.majDecimales();
	}
	
	public void affecterTauxProrata(EODepenseBudget depense, EOTauxProrata tauxProrata) {
		depense.setTauxProrataRelationship(tauxProrata);
	}
	
	public void affecterUtilisateur(EODepenseBudget depense, EOUtilisateur utilisateur) {
		depense.setUtilisateurRelationship(utilisateur);
	}
	
	public void affecterExercice(EODepenseBudget depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterDepenseBudgetReversement(EODepenseBudget depense, EODepenseBudget depenseReversement) {
		depense.setDepenseBudgetReversementRelationship(depenseReversement);
		depense.evaluerMaxReversement();
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseBudget depense) throws DepenseBudgetException {
		//Si l'article est enregistree
		/*if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) {
		 // l ecriture est elle valide ?
		  if (EcritureDetail.ecriture().ecrEtat().equals(
		  EOEcriture.ecritureValide))
		  throw new EcritureDetailException(
		  EcritureDetailException.supprimerDetailEcritureValide);
		  }*/
		
		depense.setDepensePapierRelationship(null);
		depense.setEngagementBudgetRelationship(null);
		depense.setTauxProrataRelationship(null);
		depense.setUtilisateurRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetReversementRelationship(null);

		supprimerDepenseContoleActions(depense);
		supprimerDepenseContoleAnalytiques(depense);
		supprimerDepenseContoleConventions(depense);
		supprimerDepenseContoleHorsMarches(depense);
		supprimerDepenseContoleMarches(depense);
		supprimerDepenseContolePlanComptables(depense);

		ed.deleteObject(depense);
	}
	
	public void supprimerDepenseContoleActions(EODepenseBudget depense) {
    	NSArray larray=depense.depenseControleActions();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseControleAction factory=new FactoryDepenseControleAction();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseControleAction)larray.objectAtIndex(i));
    	}
	}

	public void supprimerDepenseContoleAnalytiques(EODepenseBudget depense) {
    	NSArray larray=depense.depenseControleAnalytiques();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseControleAnalytique factory=new FactoryDepenseControleAnalytique();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseControleAnalytique)larray.objectAtIndex(i));
    	}
	}

	public void supprimerDepenseContoleConventions(EODepenseBudget depense) {
    	NSArray larray=depense.depenseControleConventions();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseControleConvention factory=new FactoryDepenseControleConvention();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseControleConvention)larray.objectAtIndex(i));
    	}
	}

	public void supprimerDepenseContoleHorsMarches(EODepenseBudget depense) {
    	NSArray larray=depense.depenseControleHorsMarches();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseControleHorsMarche factory=new FactoryDepenseControleHorsMarche();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseControleHorsMarche)larray.objectAtIndex(i));
    	}
	}

	public void supprimerDepenseContoleMarches(EODepenseBudget depense) {
    	NSArray larray=depense.depenseControleMarches();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseControleMarche factory=new FactoryDepenseControleMarche();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseControleMarche)larray.objectAtIndex(i));
    	}
	}

	public void supprimerDepenseContolePlanComptables(EODepenseBudget depense) {
    	NSArray larray=depense.depenseControlePlanComptables();
    	
    	if (larray!=null && larray.count()>0) {
    		FactoryDepenseControlePlanComptable factory=new FactoryDepenseControlePlanComptable();
    		for (int i=larray.count()-1; i>=0; i--)
        		factory.supprimer(depense.editingContext(), (EODepenseControlePlanComptable)larray.objectAtIndex(i));
    	}
	}

	public void verifier(EODepenseBudget depense) {
		depense.validateObjectMetier();
	}
}
