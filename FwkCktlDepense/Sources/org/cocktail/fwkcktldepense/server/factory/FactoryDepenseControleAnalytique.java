/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleAnalytiqueException;
import org.cocktail.fwkcktldepense.server.interfaces.IFactoryControle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author tsaivre
 */
public class FactoryDepenseControleAnalytique extends Factory implements _IFactoryDepenseControleAnalytique, IFactoryControle {

	public FactoryDepenseControleAnalytique() {
		super();
	}

	public FactoryDepenseControleAnalytique(boolean withLog) {
		super(withLog);
	}

	public EODepenseControleAnalytique copie(EOEditingContext ed, EODepenseControleAnalytique depense, EODepenseBudget budget) {

		EODepenseControleAnalytique newDepense = creer(ed);

		setDanaHtSaisie(newDepense, depense.danaHtSaisie());
		setDanaTvaSaisie(newDepense, depense.danaTvaSaisie());
		setDanaTtcSaisie(newDepense, depense.danaTtcSaisie());
		setDanaMontantBudgetaire(newDepense, depense.danaMontantBudgetaire());

		affecterCodeAnalytique(newDepense, depense.codeAnalytique());
		affecterExercice(newDepense, depense.exercice());
		affecterDepenseBudget(newDepense, budget);

		setDanaPourcentage(newDepense, depense.pourcentage());

		return newDepense;
	}
	
	public EODepenseControleAnalytique creer(EOEditingContext ed, BigDecimal pourcentage, Object code, EOExercice exercice,
			EODepenseBudget depenseBudget) throws DepenseControleAnalytiqueException {
		return creer(ed, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, pourcentage, (EOCodeAnalytique) code, exercice, depenseBudget);
	}

	public EODepenseControleAnalytique creer(EOEditingContext ed, BigDecimal danaHtSaisie, BigDecimal danaTvaSaisie,
			BigDecimal danaTtcSaisie, BigDecimal danaMontantBudgetaire, BigDecimal danaPourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			EODepenseBudget depenseBudget)
			throws DepenseControleAnalytiqueException {

		EODepenseControleAnalytique newDepense = creer(ed);

		setDanaHtSaisie(newDepense, danaHtSaisie);
		setDanaTvaSaisie(newDepense, danaTvaSaisie);
		setDanaTtcSaisie(newDepense, danaTtcSaisie);
		setDanaMontantBudgetaire(newDepense, danaMontantBudgetaire);

		affecterCodeAnalytique(newDepense, codeAnalytique);
		affecterExercice(newDepense, exercice);
		affecterDepenseBudget(newDepense, depenseBudget);

		setDanaPourcentage(newDepense, danaPourcentage);

		try {
			newDepense.validateObjectMetier();
			return newDepense;
		} catch (DepenseControleAnalytiqueException e) {
			e.printStackTrace();
			System.out.println(newDepense);
		}

		return null;
	}

	public EODepenseControleAnalytique creer(EOEditingContext ed) throws DepenseControleAnalytiqueException {
		EODepenseControleAnalytique newDepense = (EODepenseControleAnalytique) Factory.
				instanceForEntity(ed, EODepenseControleAnalytique.ENTITY_NAME);
		//   return new EOArrete(EOClassDescription.classDescriptionForEntityName("Arrete"));
		ed.insertObject(newDepense);
		return newDepense;
	}

	public void setDanaPourcentage(EODepenseControleAnalytique depense, BigDecimal danaPourcentage) {
		if (depense == null)
			return;
		depense.setPourcentage(danaPourcentage);
	}

	public void setDanaHtSaisie(EODepenseControleAnalytique depense, BigDecimal danaHtSaisie) {
		if (depense == null)
			return;
		depense.setDanaHtSaisie(danaHtSaisie);
	}

	public void setDanaTvaSaisie(EODepenseControleAnalytique depense, BigDecimal danaTvaSaisie) {
		if (depense == null)
			return;
		depense.setDanaTvaSaisie(danaTvaSaisie);
	}

	public void setDanaTtcSaisie(EODepenseControleAnalytique depense, BigDecimal danaTtcSaisie) {
		if (depense == null)
			return;
		depense.setDanaTtcSaisie(danaTtcSaisie);
	}

	public void setDanaMontantBudgetaire(EODepenseControleAnalytique depense, BigDecimal danaMontantBudgetaire) {
		if (depense == null)
			return;
		depense.setDanaMontantBudgetaire(danaMontantBudgetaire);
	}

	public void affecterCodeAnalytique(EODepenseControleAnalytique depense, EOCodeAnalytique codeAnalytique) {
		depense.setCodeAnalytiqueRelationship(codeAnalytique);
	}

	public void affecterExercice(EODepenseControleAnalytique depense, EOExercice exercice) {
		depense.setExerciceRelationship(exercice);
	}

	public void affecterDepenseBudget(EODepenseControleAnalytique depense, EODepenseBudget depenseBudget) {
		depense.setDepenseBudgetRelationship(depenseBudget);
	}

	/**
	 * Permet de supprimer tant que ce n est pas dans la base.
	 */
	public void supprimer(EOEditingContext ed, EODepenseControleAnalytique depense) throws DepenseControleAnalytiqueException {
		//Si l'article est enregistree
		/*
		 * if (ed.insertedObjects().indexOfObject(article) == NSArray.NotFound) { // l ecriture est elle valide ? if
		 * (EcritureDetail.ecriture().ecrEtat().equals( EOEcriture.ecritureValide)) throw new EcritureDetailException(
		 * EcritureDetailException.supprimerDetailEcritureValide); }
		 */

		depense.setCodeAnalytiqueRelationship(null);
		depense.setExerciceRelationship(null);
		depense.setDepenseBudgetRelationship(null);

		ed.deleteObject(depense);
	}

	public void verifier(EODepenseControleAnalytique depense) {
		depense.validateObjectMetier();
	}

	public _IDepenseControleAnalytique _copie(EOEditingContext ed, _IDepenseControleAnalytique depense, _IDepenseBudget budget) {
		return copie(ed, (EODepenseControleAnalytique) depense, (EODepenseBudget) budget);
	}

	public _IDepenseControleAnalytique _creer(EOEditingContext ed, BigDecimal htSaisie, BigDecimal tvaSaisie, BigDecimal ttcSaisie, BigDecimal montantBudgetaire, BigDecimal pourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice, _IDepenseBudget depenseBudget)
			throws DepenseControleAnalytiqueException {
		return creer(ed, htSaisie, tvaSaisie, ttcSaisie, montantBudgetaire, pourcentage, codeAnalytique, exercice, (EODepenseBudget) depenseBudget);
	}

	public void _setPourcentage(_IDepenseControleAnalytique depense, BigDecimal danaPourcentage) {
		setDanaPourcentage((EODepenseControleAnalytique) depense, danaPourcentage);

	}

	public void _setHtSaisie(_IDepenseControleAnalytique depense, BigDecimal htSaisie) {
		setDanaHtSaisie((EODepenseControleAnalytique) depense, htSaisie);

	}

	public void _setTvaSaisie(_IDepenseControleAnalytique depense, BigDecimal tvaSaisie) {
		setDanaTvaSaisie((EODepenseControleAnalytique) depense, tvaSaisie);
	}

	public void _setTtcSaisie(_IDepenseControleAnalytique depense, BigDecimal ttcSaisie) {
		setDanaTtcSaisie((EODepenseControleAnalytique) depense, ttcSaisie);

	}

	public void _setMontantBudgetaire(_IDepenseControleAnalytique depense, BigDecimal montantBudgetaire) {
		setDanaMontantBudgetaire((EODepenseControleAnalytique) depense, montantBudgetaire);

	}

	public void _affecterCodeAnalytique(_IDepenseControleAnalytique depense, EOCodeAnalytique codeAnalytique) {
		affecterCodeAnalytique((EODepenseControleAnalytique) depense, codeAnalytique);

	}

	public void _affecterExercice(_IDepenseControleAnalytique depense, EOExercice exercice) {
		affecterExercice((EODepenseControleAnalytique) depense, exercice);

	}

	public void _affecterDepenseBudget(_IDepenseControleAnalytique depense, _IDepenseBudget depenseBudget) {
		affecterDepenseBudget((EODepenseControleAnalytique) depense, (EODepenseBudget) depenseBudget);

	}

	public void _supprimer(EOEditingContext ed, _IDepenseControleAnalytique depense) throws DepenseControleAnalytiqueException {
		supprimer(ed, (EODepenseControleAnalytique) depense);

	}

	public void _verifier(_IDepenseControleAnalytique depense) {
		verifier((EODepenseControleAnalytique) depense);

	}
}
