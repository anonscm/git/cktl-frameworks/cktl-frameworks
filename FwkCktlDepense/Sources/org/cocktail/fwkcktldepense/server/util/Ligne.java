/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;

import com.webobjects.eocontrol.EOCustomObject;

public abstract class Ligne extends EOCustomObject {
	private static Calculs serviceCalcul = Calculs.getInstance();

	private BigDecimal montantHt, montantTtc, pourcentage, montantResteEngage;//, montantResteARepartir;
	private EOTauxProrata prorata;
	private Tableau tableau;
	
	protected Ligne(Tableau pere) {
		super();
		montantHt=new BigDecimal(0.0);
		montantTtc=new BigDecimal(0.0);
		pourcentage=new BigDecimal(0.0);
		montantResteEngage=new BigDecimal(0.0);
		tableau=pere;
	}
	
	public boolean isGood() {
		if (pourcentage.floatValue()<0.0 || pourcentage.floatValue()>100.0)
			return false;
		if (montantHt.floatValue()<0.0)
			return false;
		if (montantTtc.floatValue()<0.0)
			return false;
		if (montantHt.floatValue()>montantTtc.floatValue())
			return false;
		return true;
	}

	public void setMontantResteEngage(BigDecimal value) {
		if (value==null)
			value=new BigDecimal(0.0);
		montantResteEngage=value.setScale(2, BigDecimal.ROUND_HALF_UP);	
	}
	
	public void addMontantResteEngage(BigDecimal value) {
		setMontantResteEngage(montantResteEngage().add(value));
	}

	public BigDecimal montantResteEngage() {
		if (montantHt==null)
			setMontantResteEngage(new BigDecimal(0.0));
		return montantResteEngage;		
	}

	public void setMontantHt(BigDecimal value) {
		if (value==null)
			value=new BigDecimal(0.0);
		montantHt=value.setScale(2, BigDecimal.ROUND_HALF_UP);	
		mettreAJourTableau();
	}
	
	public void addMontantHt(BigDecimal value) {
		setMontantHt(montantHt().add(value));
	}

	public BigDecimal montantHt() {
		if (montantHt==null)
			setMontantHt(new BigDecimal(0.0));
		return montantHt;		
	}

	public void setMontantTtc(BigDecimal value) {
		if (value==null)
			value=new BigDecimal(0.0);
		montantTtc=value.setScale(2, BigDecimal.ROUND_HALF_UP);	
		mettreAJourTableau();
	}
	
	public void addMontantTtc(BigDecimal value) {
		setMontantTtc(montantTtc().add(value));
	}

	public BigDecimal montantTtc() {
		if (montantTtc==null)
			setMontantTtc(new BigDecimal(0.0));
		return montantTtc;		
	}
	
	public BigDecimal montantBudgetaire() {
		if (prorata==null)
			return new BigDecimal(0.0);
		if (montantTtc().floatValue()<montantHt().floatValue())
			return new BigDecimal(0.0);
			
		//return prorata.montantBudgetaire(montantHt(), montantTtc().subtract(montantHt()));		
		return serviceCalcul.calculMontantBudgetaire(montantHt(), montantTtc().subtract(montantHt()), prorata.tapTaux());		
	}

	public void setTauxProrata(EOTauxProrata taux) {
		prorata=taux;
	}
	
	public void setPourcentage(BigDecimal pourcent, BigDecimal totalHt, BigDecimal totalTtc) {
		if (pourcent==null)
			pourcent=new BigDecimal(0.0);
		if (totalHt==null)
			totalHt=new BigDecimal(0.0);
		if (totalTtc==null)
			totalTtc=new BigDecimal(0.0);
		
		pourcentage=pourcent.setScale(0, BigDecimal.ROUND_HALF_UP);	
		setMontantHt(totalHt.multiply(pourcentage.divide(new BigDecimal(100.0),2,BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP));	
		setMontantTtc(totalTtc.multiply(pourcentage.divide(new BigDecimal(100.0),2,BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	public BigDecimal pourcentage() {
		if (pourcentage==null)
			setPourcentage(new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0));
		return pourcentage;
	}
	
	private void mettreAJourTableau() {
		if (tableau==null)
			return;
		tableau.mettreAJourPere();
	}
}
