/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;

import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

public abstract class Tableau extends NSMutableArray implements NSKeyValueCoding, EOKeyValueCoding {
	private static Calculs serviceCalcul = Calculs.getInstance();

	private BigDecimal minBudgetaire, totalHtFixe, totalTtcFixe;
	private boolean boolUtilePourcentage;
	private EOSource source;
	private Object   objetPere;

	public Tableau(EOSource laSource, boolean utilePourcentage, Object pere) {
		super();
		minBudgetaire=new BigDecimal(0.0);
		source=laSource;
		boolUtilePourcentage=utilePourcentage;
		totalHtFixe=null;
		totalTtcFixe=null;
		objetPere=pere;
	}

	@Deprecated
	final protected void mettreAJourPere() {
		if (objetPere()==null)
			return;
	}
	
	protected Object objetPere() {
		return objetPere;
	}
	
	public boolean isTableauMontant() {
		return !boolUtilePourcentage;
	}

	public boolean isTableauPourcentage() {
		return boolUtilePourcentage;
	}
	
	protected void addLigne(Ligne ligne) {
		if (ligne==null)
			return ;
		addObject(ligne);
		if (source!=null && source.tauxProrata()!=null)
			ligne.setTauxProrata(source.tauxProrata());
	}
	
	protected void removeLigne(Ligne ligne) {
		if (ligne==null)
			return ;
		removeObject(ligne);
	}
	
	public void setMinBudgetaire(BigDecimal value) {
		if (value==null)
			value=new BigDecimal(0.0);
		minBudgetaire=value.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	public void addMinBudgetaire(BigDecimal value) {
		setMinBudgetaire(minBudgetaire().add(value));
	}
	
	public BigDecimal minBudgetaire() {
		if (minBudgetaire==null)
			setMinBudgetaire(new BigDecimal(0.0));
		return minBudgetaire;
	}

	public BigDecimal totalLignesHt() {
		if (count()==0)
			return new BigDecimal(0.0);
		return computeSumForKey("montantHt");
	}
	
	public BigDecimal totalLignesTtc() {
		if (count()==0)
			return new BigDecimal(0.0);
		return computeSumForKey("montantTtc");
	}

	public BigDecimal totalPourcentage() {
		if (count()==0)
			return new BigDecimal(0.0);
		return computeSumForKey("pourcentage");
	}

	public BigDecimal totalHtFixe() {
		return totalHtFixe;
	}

	public BigDecimal totalTtcFixe() {
		return totalTtcFixe;
	}

	public void setTotalHtFixe(BigDecimal value) {
		if (value==null)
			value=new BigDecimal(0.0);
		totalHtFixe=value.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	public void setTotalTtcFixe(BigDecimal value) {
		if (value==null)
			value=new BigDecimal(0.0);
		totalHtFixe=value.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	private BigDecimal computeSumForKey(String key) {
		return (BigDecimal)valueForKeyPath("@sum."+key);
	}

	public BigDecimal montantBudgetaire() {
		if (source==null || source.tauxProrata()==null)
			return new BigDecimal(0.0);
		return serviceCalcul.calculMontantBudgetaire(totalLignesHt(), totalLignesTtc().subtract(totalLignesHt()), source.tauxProrata().tapTaux());		
	}

	public void setSource(EOSource laSource) {
		source=laSource;
		EOTauxProrata prorata=null;
		if (source!=null && source.tauxProrata()!=null)
			prorata=source.tauxProrata();

		for (int i=0; i<count(); i++)
			((Ligne)objectAtIndex(i)).setTauxProrata(prorata);
	}

	public EOSource source() {
		return source;
	}
	
	public boolean isGood() {
		if (source==null || source.tauxProrata()==null)
			return false;
		if (montantBudgetaire().compareTo(minBudgetaire)==-1)
			return false;

		for (int i=0; i<count(); i++) {
			if (!((Ligne)objectAtIndex(i)).isGood())
				return false;
		}
		
		if (boolUtilePourcentage) {
			if (totalPourcentage().floatValue()<0.0 || totalPourcentage().floatValue()>100.0)
				return false;
		}
		
		return true;
	}
	
	public void takeStoredValueForKey(Object value, String key) {
	}
	
	public Object storedValueForKey(String key) {
		return valueForKey(key);
	}
	
	public void takeValueForKey(Object value, String key) {
	}
	
	public Object valueForKey(String key) {
		
		if (key.equals("minBudgetaire"))
			return minBudgetaire();
		if (key.equals("totalHtFixe"))
			return totalHtFixe();
		if (key.equals("totalTtcFixe"))
			return totalTtcFixe();
		if (key.equals("totalLignesHt"))
			return totalLignesHt();
		if (key.equals("totalLignesTtc"))
			return totalLignesTtc();
		if (key.equals("montantBudgetaire"))
			return montantBudgetaire();
		if (key.equals("source"))
			return source();
		return null;
	}

	public Object handleQueryWithUnboundKey(String arg0) {
		return null;
	}

	public void handleTakeValueForUnboundKey(Object arg0, String arg1) {
	}

	public void unableToSetNullForKey(String arg0) {
	}

}
