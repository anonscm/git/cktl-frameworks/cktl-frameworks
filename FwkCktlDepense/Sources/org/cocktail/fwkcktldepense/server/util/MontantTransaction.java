package org.cocktail.fwkcktldepense.server.util;

import java.math.BigDecimal;
import java.math.BigInteger;

public class MontantTransaction {
	private BigDecimal ht = BigDecimal.ZERO;
	private BigDecimal ttc = BigDecimal.ZERO;
	private BigDecimal montantBudgetaire = BigDecimal.ZERO;

	public MontantTransaction(){};
	
	public MontantTransaction(BigDecimal montantHT, BigDecimal montantTTC, BigDecimal montantBudgetaire) {
		this.ttc = montantTTC;
		this.ht = montantHT;
		this.montantBudgetaire = montantBudgetaire;
	};

	public MontantTransaction(BigDecimal montantHT, BigDecimal montantTTC) {
		this.ttc = montantTTC;
		this.ht = montantHT;
	};
	
	/**
	 * Indique si un MontantTransaction est egal à un autre (égalité stricte sur les composants)
	 * @param other
	 * @return
	 */
	public boolean equals(MontantTransaction other) {
		return this.getMontantHT().compareTo(other.getMontantHT()) == 0 
				&& this.getMontantTTC().compareTo(other.getMontantTTC()) == 0
				&& this.getMontantBudgetaire().compareTo(other.getMontantBudgetaire()) == 0;
	}
	
	/**
	 * Plafonne un Montant de Transaction par un autre. Les composants sont plafonné un à un.
	 * @param plafond
	 * @return
	 */
	public MontantTransaction plafonner(MontantTransaction plafond) {
		return new MontantTransaction(
				  plafonner(getMontantHT(), plafond.getMontantHT()),
				  plafonner(getMontantTTC(), plafond.getMontantTTC()),
				  plafonner(getMontantBudgetaire(), plafond.getMontantBudgetaire())
				);
	}
	
	/**
	 * Ajoute un Montant de Transaction à un autre. Les composants sont aditionnés un à un.
	 * @param plafond
	 * @return
	 */
	public MontantTransaction add(MontantTransaction other) {
		return new MontantTransaction(
				  getMontantHT().add(other.getMontantHT()),
				  getMontantTTC().add(other.getMontantTTC()),
				  getMontantBudgetaire().add(other.getMontantBudgetaire())
				);
	}

	/**
	 * Soustraire un Montant de Transaction à un autre. Les composants sont soustrait un à un.
	 * @param plafond
	 * @return
	 */
	public MontantTransaction subtract(MontantTransaction other) {
		return new MontantTransaction(
				  getMontantHT().subtract(other.getMontantHT()),
				  getMontantTTC().subtract(other.getMontantTTC()),
				  getMontantBudgetaire().subtract(other.getMontantBudgetaire())
				);
	}

	/**
	 * Multiplie les composants d'un MontantTransaction par un BigDecimal
	 * @param multiplicateur
	 * @return
	 */
	public MontantTransaction multiply(BigDecimal multiplicateur) {
		return new MontantTransaction(
				getMontantHT().multiply(multiplicateur),
				getMontantTTC().multiply(multiplicateur),
				getMontantBudgetaire().multiply(multiplicateur));
	}
	
	/**
	 * Divise les composants d'un MontantTransaction par un BigDecimal
	 * @param diviseur
	 * @return
	 */
	public MontantTransaction divide(BigDecimal diviseur) {
		return new MontantTransaction(
				getMontantHT().divide(diviseur),
				getMontantTTC().divide(diviseur),
				getMontantBudgetaire().divide(diviseur));
	}
	
	/**
	 * Divise les composants en limitant le nombre de décimales.
	 * La méthode d'arrondi est par défaut BigDecimal.ROUND_HALF_UP.
	 * @param diviseur
	 * @param decimales Nombre de décimales conservés.
	 * @return
	 */
	public MontantTransaction divide(BigDecimal diviseur, int decimales) {
		return divide(diviseur, decimales, BigDecimal.ROUND_HALF_UP);
	}

	/**
     * Divise les composants en limitant le nombre de décimales et la méthode d'arrondi.
	 * @param diviseur
	 * @param decimales Nombre de décimales conservés.
	 * @param method Méthode d'arrondi (Voir BigInteger)
	 * @return
	 */
	public MontantTransaction divide(BigDecimal diviseur, int decimales, int method ) {
		return new MontantTransaction(
				getMontantHT().divide(diviseur, decimales, method),
				getMontantTTC().divide(diviseur, decimales, method),
				getMontantBudgetaire().divide(diviseur, decimales, method));
	}
	
	public BigDecimal getMontantHT() {
		return ht;
	}

	public BigDecimal getMontantTTC() {
		return ttc;
	}

	public BigDecimal getMontantBudgetaire() {
		return montantBudgetaire;
	}

	private BigDecimal plafonner(BigDecimal a, BigDecimal b) {
		return a.compareTo(b) == 1 ? b : a;
	}
	
}

