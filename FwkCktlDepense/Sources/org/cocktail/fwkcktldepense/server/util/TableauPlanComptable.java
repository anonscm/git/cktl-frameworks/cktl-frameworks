/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import org.cocktail.fwkcktldepense.server.finder.FinderPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;


public class TableauPlanComptable extends Tableau {

	private NSArray arrayPlanComptables;

	public TableauPlanComptable(EOSource laSource, boolean utilePourcentage, Object pere) {
		super(null, utilePourcentage, pere);
		if (laSource!=null)
			setSource(laSource);
		arrayPlanComptables=null;
	}
	
	public void addLigne(Ligne ligne) {
		if (!(ligne instanceof LignePlanComptable))
			return;
		super.addLigne(ligne);
	}

	public boolean isGood() {
		super.isGood();
		
		if (isTableauPourcentage() && totalPourcentage().floatValue()!=100.0)
			return false;

		if (isTableauMontant()) {
			if (totalHtFixe()!=null && totalHtFixe().floatValue()!=totalLignesHt().floatValue())
				return false;
			if (totalTtcFixe()!=null && totalTtcFixe().floatValue()!=totalLignesTtc().floatValue())
				return false;
		}

		return true;
	}
	
    public NSArray getPlanComptablesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayPlanComptables==null && source()!=null && utilisateur!=null) {
			NSMutableDictionary bindings=new NSMutableDictionary();
			bindings.setObjectForKey(source().typeCredit(), "typeCredit");
			bindings.setObjectForKey(utilisateur, "utilisateur");
			bindings.setObjectForKey(source().exercice(), "exercice");
			arrayPlanComptables=FinderPlanComptable.getPlanComptables(ed, bindings);
		}
		if (arrayPlanComptables==null)
			arrayPlanComptables=new NSArray();

		if (arrayPlanComptables.count()==0)
			return arrayPlanComptables;

		if (count()>0) {
			NSMutableArray resultats=new NSMutableArray(arrayPlanComptables);
			for (int i=0; i<count(); i++)
				resultats.removeObject(((LignePlanComptable)objectAtIndex(i)).planComptable());
			return resultats;
		}
		
    	return arrayPlanComptables;
    }
}
