/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class ZDateUtil {

    public static final String[] MOIS_ANNEE = new String[] { "JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET", "AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE" };

    /**
     * renvoie la date en cours formatee dd/mm/yyyy
     */
    public static String dateFrench() {
        NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%Y");
        String ladate = formatter.format(new NSTimestamp());
        return ladate;
    }

    /**
     * 
     * Ajoute un nombre d'heures (positif ou negatif) a un NSTimeStamp et en
     * renvoie un nouveau dans le TimeZone par defaut.
     * 
     * @param hours
     * @param ts
     * @return
     */
    public static NSTimestamp addHoursToNSTimestamp(int hours, NSTimestamp ts) {
        GregorianCalendar myCalendar = new GregorianCalendar();
        myCalendar.setTime(ts);
        // NSLog.out.appendln( "TimeZone du calendar: " +
        // myCalendar.getTimeZone() );
        myCalendar.add(Calendar.HOUR, hours);
        Date laDate = myCalendar.getTime();
        return new NSTimestamp(laDate);
    }

    /**
     * Ajoute des jours, heures, minutes, secondes a une date.
     * 
     * @param aDate
     *            La date de depart
     * @param aDay
     *            Nombre de jours a rajouter
     * @param hours
     *            Nombre d'heures a rajouter
     * @param minutes
     *            Nombre de minutes a rajouter
     * @param seconds
     *            Nombre de secondes a rajouter
     * @return La nouvelle date
     */
    public static final Date addDHMS(Date aDate, int aDay, int hours, int minutes, int seconds) {
        GregorianCalendar myCalendar = new GregorianCalendar();
        myCalendar.setTime(aDate);
        myCalendar.add(Calendar.DAY_OF_MONTH, aDay);
        myCalendar.add(Calendar.HOUR, hours);
        myCalendar.add(Calendar.MINUTE, minutes);
        myCalendar.add(Calendar.SECOND, seconds);
        return myCalendar.getTime();
    }

    /**
     * @param aDate
     * @return La date sous forme dd/mm/yyyy 23:59:59
     */
    public static final Date lastSecondOfDay(Date aDate) {
        return addDHMS(getDateOnly(aDate), 0, 23, 59, 59);
    }

    /**
     * Nettoie une date de ses heures, minutes, secondes en gardant seulement le
     * jou, mois, annee.
     * 
     * @param aDate
     * @return
     */
    public static final Date getDateOnly(Date aDate) {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(aDate);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc.getTime();
    }

    /**
     * Le separateur par defaut dans une expression de type <i>date</i>. La
     * presente implementation utilise le separateur "/".
     */
    public static final String DATE_SEPARATOR = "/";

    /**
     * Le separateur par defaut dans une expression de type <i>heures</i>. La
     * presente implementation utilise le separateur ":".
     */
    public static final String TIME_SEPARATOR = ":";

    /**
     * Le format par defaut pour une expression de <i>date</i>. La presente
     * implementation utilise le format "%d/%m/%Y" (jour/mois/annee).
     */
    public static final String DEFAULT_FORMAT = "%d/%m/%Y";

    /**
     * Complete une expression de la date. Le separateur et le format de la date
     * comlete sont ceux par defaut : jour/mois/annee.
     * 
     * <p>
     * Les regles de la completion (x est la valeur donnee, <i>y</i> cette de
     * la date completee) :
     * <ul>
     * <li>tous les separateurs sont ellimines. Par exemple, xx/xx est
     * transforme en <i>xxxx</i> ;</li>
     * <li><i>vide</i> - <i>yy/yy/yyyy</i> (y - jour, moi, annee en cours);</li>
     * <li><i>x</i> - <i>0x/yy/yyyy</i> (y - moi, annee en cours);</li>
     * <li><i>xx</i> - <i>xx/yy/yyyy</i> (y - moi, annee en cours);</li>
     * <li><i>xxx</i> - <i>xx/0x/yyyy</i> (y - annee en cours);</li>
     * <li><i>xxxx</i> - <i>xx/xx/yyyy</i> (y - annee en cours);</li>
     * <li><i>xxxxx</i> - <i>xx/xx/yyyx</i> (y - annee en cours);</li>
     * <li><i>xxxxxx</i> - <i>xx/xx/yyxx</i> (y - annee en cours);</li>
     * <li><i>xxxxxxx</i> - <i>xx/xx/yxxx</i> (y - annee en cours);</li>
     * <li><i>xxxxxxxx</i> (ou une valeur plus grande) - <i>xx/xx/xxxx</i> (y -
     * annee en cours).</li>
     * </ul>
     * </p>
     * 
     * <p>
     * La chaine vide est retournee si la valeur de la date est incorrecte.
     * </p>
     */
    public static String dateCompletion(String uneDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        String maChaine, retour;

        String annee = String.valueOf(calendar.get(Calendar.YEAR));
        // On met le mois sur 2 caracteres dans tous les cas
        String mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        if (mois.length() <= 1)
            mois = "0" + mois;
        String jour = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        if (jour.length() <= 1)
            jour = "0" + jour;

        // dd/mm/yyyy -> ddmmyyyy
        maChaine = "";
        if (uneDate != null) {
            StringTokenizer st = new StringTokenizer(uneDate, DATE_SEPARATOR);
            while (st.hasMoreTokens())
                maChaine += st.nextToken();
        }

        switch (maChaine.length()) {
        case 0:
            break;
        case 1:
            jour = "0" + maChaine;
            break;
        case 2:
            jour = maChaine;
            break;
        case 3:
            jour = maChaine.substring(0, 2);
            mois = "0" + maChaine.substring(2);
            break;
        case 4:
            jour = maChaine.substring(0, 2);
            mois = maChaine.substring(2);
            break;
        case 5:
            jour = maChaine.substring(0, 2);
            mois = maChaine.substring(2).substring(0, 2);
            annee = annee.substring(0, 3) + maChaine.substring(4);
            break;

        case 6:
            jour = maChaine.substring(0, 2);
            mois = maChaine.substring(2).substring(0, 2);
            annee = annee.substring(0, 2) + maChaine.substring(4);
            break;

        case 7:
            jour = maChaine.substring(0, 2);
            mois = maChaine.substring(2).substring(0, 2);
            annee = annee.substring(0, 1) + maChaine.substring(4);
            break;

        default:
            jour = maChaine.substring(0, 2);
            mois = maChaine.substring(2).substring(0, 2);
            annee = maChaine.substring(4).substring(0, 4);
        }
        retour = jour + DATE_SEPARATOR + mois + DATE_SEPARATOR + annee;

        if (!isValid(retour))
            retour = "";
        return retour;
    }

    /**
     * Teste la validite de la date vis a vis du format par defaut.
     * 
     * <p>
     * Dans certains cas, les methodes de convertion des dates de WebObjects
     * permet de traiter les dates incorrectes comme etant correctes. Par
     * exemple, les jours "de trop" d'un mois sont reportes dans le mois
     * suivant. Cette methode permet de traiter la validite d'une date en
     * evitant ce type de formation.
     * </p>
     * 
     * @see #DEFAULT_FORMAT
     */
    public static boolean isValid(String dateString) {
        return dateToString(stringToDate(dateString)).equals(dateString);
    }

    // /**
    // * Teste si deux dates representent le meme moment.
    // */
    // private static boolean isSameDay(NSTimestamp date1, NSTimestamp date2) {
    // return dateToString(date1).equals(dateToString(date2));
    // }

    /**
     * Teste si la date <i>date1</i> precede ou est egale a la date <i>date2</i>.
     */
    public static boolean isBeforeEq(NSTimestamp date1, NSTimestamp date2) {
        // WO4.5.x != WO5.x
        return (date1.getTime() <= date2.getTime());
    }

    /**
     * Teste si la date <i>date1</i> precede strictement la date <i>date2</i>.
     */
    public static boolean isBefore(NSTimestamp date1, NSTimestamp date2) {
        // WO4.5.x != WO5.x
        return (date1.getTime() < date2.getTime());
    }

    /**
     * Teste si la date <i>date1</i> succede ou est egale a la date la <i>date2</i>.
     */
    public static boolean isAfterEq(NSTimestamp date1, NSTimestamp date2) {
        return isBeforeEq(date2, date1);
    }

    /**
     * Teste si la date <i>date1</i> succede strictement la date <i>date2</i>.
     */
    public static boolean isAfter(NSTimestamp date1, NSTimestamp date2) {
        return isBefore(date2, date1);
    }

    /**
     * Converti un objet date en un objet <code>String</code> suivant le
     * format <code>dateFormat</code>. Le format doit correspondre au format
     * accepte par <code>NSTimestampFormatter</code>.
     * 
     * @see #dateToString(NSTimestamp)
     */
    public static String dateToString(NSTimestamp date, String dateFormat) {
        // WO4.5.x != WO5.x
        String dateString;
        NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
        try {
            dateString = formatter.format(date);
        } catch (Exception ex) {
            dateString = "";
        }
        return dateString;
    }

    /**
     * Converti un objet date en un objet <code>String</code>. La conversion
     * utilise le format par defaut.
     * 
     * @see #dateToString(NSTimestamp, String)
     * @see #DEFAULT_FORMAT
     */
    public static String dateToString(NSTimestamp gregorianDate) {
        return dateToString(gregorianDate, DEFAULT_FORMAT);
    }

    /**
     * Converti une chaine de caracteres <code>sDate</code> en un objet date.
     * Le format de la date <code>sDate</code> doit correspondre au format
     * <code>dateFormat</code>. Les format possibles sont definis dans la
     * classe <code>NSTimestampFormatter</code>.
     * 
     * <p>
     * La date est convertie en utilisant le fuseau horaire
     * <code>NSTimeZone.defaultTimeZone()</code>.
     * </p>
     * 
     * @return La date ou null si la valeur <code>sDate</code> indique une
     *         date invalide.
     * 
     * @see #stringToDate(String)
     */
    public static NSTimestamp stringToDate(String sDate, String dateFormat) {
        // WO4.5.x != WO5.x
        NSTimestamp date = null;
        NSTimestampFormatter formatter;
        if ((dateFormat == null) || (dateFormat.trim().length() == 0))
            return null;
        try {
            formatter = new NSTimestampFormatter(dateFormat);
            date = (NSTimestamp) formatter.parseObject(sDate);
            if (!sDate.equals(dateToString(date, dateFormat)))
                return null;
        } catch (Exception ex) {
        }
        return date;
    }

    /**
     * Converti une chaine de caracteres <code>sDate</code> en un objet date.
     * Le format de la date <code>sDate</code> doit correspondre au format par
     * defaut.
     * 
     * <p>
     * La date est convertie en utilisant le fuseau horaire
     * <code>NSTimeZone.defaultTimeZone()</code>.
     * </p>
     * 
     * @return La date ou null si la valeur <code>sDate</code> indique une
     *         date invalide.
     * 
     * @see #stringToDate(String, String)
     * @see #DEFAULT_FORMAT
     */
    public static NSTimestamp stringToDate(String dateString) {
        return stringToDate(dateString, DEFAULT_FORMAT);
    }

    /**
     * Convertie le numero du jour de la semaine de la representation anglaise
     * vers celle francaise.
     */
    public static int getDayOfWeek(int dayOfWeek) {
        return (dayOfWeek == 0) ? (dayOfWeek + 6) : (dayOfWeek - 1);
    }

    /**
     * Retourne le numero de jour de la semaine correspondant a la date
     * indiquee. Lundi est le premier jour de la semaine.
     */
    public static int getDayOfWeek(NSTimestamp date) {
        // WO4.5.x != WO5.x
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.setFirstDayOfWeek(GregorianCalendar.MONDAY);
        return calendar.get(GregorianCalendar.DAY_OF_WEEK);
    }

    /**
     * @deprecated Utiliser la methode <code>currentDateTimeString</code>.
     * 
     * @see #currentDateTimeString()
     */
    public static String getCurrentDateTime() {
        return currentDateTimeString();
    }

    /**
     * Retourne la representation du moment en cour sous forme d'une chaine de
     * caracteres. Le format de la chaine est "jj/mm/AAAA HH:MM:SS".
     * 
     * @see #currentDateString()
     */
    public static String currentDateTimeString() {
        java.util.Calendar cal = new GregorianCalendar();
        StringBuffer sb = new StringBuffer(currentDateString());
        sb.append(" ");
        sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.HOUR_OF_DAY), 2)).append(TIME_SEPARATOR);
        sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.MINUTE), 2)).append(TIME_SEPARATOR);
        sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.SECOND), 2));
        return sb.toString();
    }

    /**
     * Retourne la representation du moment en cour sous forme d'une chaine de
     * caracteres. Le format de la chaine est "jj/mm/AAAA".
     * 
     * @see #currentDateTimeString()
     */
    public static NSTimestamp currentDateNSTimeStamp() {
    	return new NSTimestamp();	
    }
    
    public static String currentDateString() {
        java.util.Calendar cal = new GregorianCalendar();
        StringBuffer sb = new StringBuffer("");
        sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.DAY_OF_MONTH), 2)).append(DATE_SEPARATOR);
        sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.MONTH) + 1, 2)).append(DATE_SEPARATOR);
        if (cal.get(GregorianCalendar.YEAR) > 100)
            sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.YEAR) % 100, 2));
        else
            sb.append(ZStringUtil.get0Int(cal.get(GregorianCalendar.YEAR), 2));
        return sb.toString();
    }

    /**
     * Convertie et retourne la date dans le feseau horaire en cours.
     * 
     * @deprecated Cette methode traite incorrectement les dates creees dans les
     *             differents fuseaux horaires. Utiliser les methode
     *             <code>dateToString</code> et <code>stringToDate</code>
     *             pour la conversion des dates.
     * 
     * @see NSTimeZone#defaultTimeZone()
     */
    public static NSTimestamp toLocalDate(NSTimestamp date) {
        // NSTimestamp t;
        // if (date == null) return null;
        // t = new NSTimestamp(date.getTime(),
        // NSTimeZone.timeZoneWithName("GMT", true));
        // return new NSTimestamp(t.getTime(), NSTimeZone.defaultTimeZone());
        return date;
    }

    /**
     * Retourne la date/time representant le moment en cours dans le fuseau
     * horaire par defaut.
     * 
     * @see NSTimeZone#defaultTimeZone()
     */
    public static NSTimestamp now() {
        // return toLocalDate(new NSTimestamp());
        return new NSTimestamp();
    }

    /**
     * @return Un gregorianCalendar initialise a la date du jour (sans les
     *         heures/minutes/etc). Utilisez getToday().getTime() pour recuperer
     *         la date du jour nettoyee des secondes sous forme de Date.
     */
    public static GregorianCalendar getToday() {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc;
    }

    /**
     * Retourne un tableau de tous les jours feries en France de l'annee donnee.
     */
    public static NSArray holidaysFR(int year) {
        int jour, mois, nCycleLunaire, nBissextile, nLettDimanche, nC1, nC2, nC3;
        NSTimestamp leJourTemp;
        NSMutableArray result = new NSMutableArray();

        result.addObject(stringToDate("01/01/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("01/05/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("08/05/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("14/07/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("15/08/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("01/11/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("11/11/" + year, DEFAULT_FORMAT));
        result.addObject(stringToDate("25/12/" + year, DEFAULT_FORMAT));

        if ((year == 1954) || (year == 2049)) {
            jour = 18;
            mois = 4;
        } else {
            if ((year == 1981) || (year == 2076)) {
                jour = 19;
                mois = 4;
            } else {
                nCycleLunaire = year % 19;
                nBissextile = year % 4;
                nLettDimanche = year % 7;
                nC1 = ((nCycleLunaire * 19) + 24) % 30;
                nC2 = (nBissextile * 2 + nLettDimanche * 4 + nC1 * 6 + 5) % 7;
                nC3 = nC1 + nC2;

                if (nC3 <= 9) {
                    jour = 22 + nC3;
                    mois = 3;
                } else {
                    jour = nC3 - 9;
                    mois = 4;
                }
            }
        }

        leJourTemp = new NSTimestamp(year, mois, jour, 0, 0, 0, NSTimeZone.defaultTimeZone());
        // Lundi de Paques
        leJourTemp = leJourTemp.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
        result.addObject(leJourTemp);
        // Jeudi de l'ascension : 38 jours apres le lundi de paques
        leJourTemp = leJourTemp.timestampByAddingGregorianUnits(0, 0, 38, 0, 0, 0);
        result.addObject(leJourTemp);
        // Lundi de Pentecote : 11 jours apres l'ascension
        leJourTemp = leJourTemp.timestampByAddingGregorianUnits(0, 0, 11, 0, 0, 0);
        result.addObject(leJourTemp);
        return result;
    }

    /**
     * Test si le jour donnee est un jour ferie en France.
     */
    public static boolean isHolidayFR(NSTimestamp aDate) {
        GregorianCalendar aCalendar = new GregorianCalendar();
        aCalendar.setTime(aDate);
        return (holidaysFR(aCalendar.get(Calendar.YEAR)).containsObject(aDate));
    }

    /**
     * Retourne le numero de la semaine correspondant a la date donnee.
     */
    public static int weekNumber(NSTimestamp aDate) {
        GregorianCalendar aCalendar = new GregorianCalendar();
        aCalendar.setTime(aDate);
        return aCalendar.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * @param exercice
     * @return Une liste d'objets Date (les 1ers jours de chaque mois de
     *         l'annee)
     */
    public static final ArrayList getFirstDaysOfMonth(final int exercice) {
        ArrayList list = new ArrayList(12);
        for (int i = 0; i < 12; i++) {
            list.add(new GregorianCalendar(exercice, i, 1).getTime());
        }
        return list;
    }

    public static final Date getFirstDayOfYear(final Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 0);
        return cal.getTime();
    }

    public static final Date getFirstDayOfMonth(final Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }

    public static final Date getLastDayOfMonth(final Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        Date tmp = getFirstDayOfMonth(cal.getTime());
        cal = new GregorianCalendar();
        cal.setTime(tmp);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }

    public static final Date getLastDayOfYear(final int exercice) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.DAY_OF_MONTH, 31);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.YEAR, exercice);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);   
        
        return cal.getTime();
    }
    public static final Date getFirstDayOfYear(final int exercice) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.YEAR, exercice);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static final Date getMinOf2Dates(final Date date1, final Date date2) {
        if (date1.getTime() < date2.getTime()) {
            return date1;
        }
        return date2;
    }
    
    public static final Date getMaxOf2Dates(final Date date1, final Date date2) {
        if (date1.getTime() > date2.getTime()) {
            return date1;
        }
        return date2;
    }

    public static final Date addMonths(final Date date, int months) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }

    public static final int getYear(final Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static final int getMonth(final Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static final int getDayOfMonth(final Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * @param debut
     * @param fin
     * @return La duree en miliseconges entre deux dates.
     */
    public static final long calcMillisecondsBetween(final Date debut, final Date fin) {
        GregorianCalendar caldebut = new GregorianCalendar();
        caldebut.setTime(debut);

        GregorianCalendar calfin = new GregorianCalendar();
        calfin.setTime(fin);

        return calfin.getTimeInMillis() - caldebut.getTimeInMillis();
    }

    public static final Date calcDateBetween(final Date debut, final Date fin) {
        return new Date(calcMillisecondsBetween(debut, fin));
    }

    public static final String formatDuree(long millisec) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(new Date(millisec));

        int h = cal.get(Calendar.HOUR_OF_DAY);
        int m = cal.get(Calendar.MINUTE);
        int s = cal.get(Calendar.SECOND);
        int ms = cal.get(Calendar.MILLISECOND);

        String res = (h != 0 ? h + " Heures " : "") + (m != 0 ? m + " minutes " : "") + (s != 0 ? s + " secondes " : "") + (ms != 0 ? ms + " ms " : "");

        return res;
    }

}
