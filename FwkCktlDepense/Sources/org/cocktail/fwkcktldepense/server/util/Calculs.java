package org.cocktail.fwkcktldepense.server.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculs {
	
	public static final int NB_DECIMALES_CALCUL_TAUX_TVA = 10;
	public static final int NB_DECIMALES_CALCUL_COEFFICIENT = 3;
	
	public static final BigDecimal ZERO = BigDecimal.ZERO;
	public static final BigDecimal UN = BigDecimal.ONE;
	public static final BigDecimal CENT = new BigDecimal(100);

	private static final Calculs instance = new Calculs(); 
	
	public static Calculs getInstance() {
		return instance;
	}
	
	/**
	 * Calcul un taux de TVA à partir du HT et du TTC.
	 * 
	 * rod 25.10.2012 : on passe en round_half_up sinon pb sur les liquidations definitives en cas de depassement. La part sur extourne avait une différence d'1ct si le montant de tva était de 5,545 par exemple.
     * fla 13.02.2014 : on passe les calculs sur 10 décimales. L'arrondi sur 2 decimales est deplace en derniere etape.
	 * 
	 * @param ttc
	 * @param ht
	 * @return
	 */
	public BigDecimal calculTauxTva(BigDecimal ttc, BigDecimal ht) {
		if(ht.equals(ZERO)) return ZERO;
		return ttc.divide(ht, NB_DECIMALES_CALCUL_TAUX_TVA, BigDecimal.ROUND_HALF_UP).subtract(BigDecimal.valueOf(1d));
	}

	/**
	 * Calcul le montant de la TVA à partir du taux et du montant HT.
	 * @param taux Taux de TVA (en %)
	 * @param ht
	 * @param nbDecimales
	 * @return
	 */
	public BigDecimal calculMontantTVAaPartirHT(BigDecimal taux, BigDecimal ht, int nbDecimales) {
		return ht.multiply(transformePourcentageEnCoefficient(taux,NB_DECIMALES_CALCUL_COEFFICIENT))
				.setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * Calcul le montant de la TVA a partir du TTC
	 * @param taux (en %)
	 * @param ttc
	 * @param nbDecimales
	 * @return
	 */
	public BigDecimal calculMontantTVAaPartirTTC(BigDecimal taux, BigDecimal ttc, int nbDecimales) {
		BigDecimal ht = calculHTaPartirTTC(taux, ttc, nbDecimales);
		return ttc.subtract(ht).setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * Calcul un montant HT a partic du TTC et d'un taux.
	 * @param taux (en %)
	 * @param ttc
	 * @param nbDecimales
	 * @return
	 */
	public BigDecimal calculHTaPartirTTC(BigDecimal taux, BigDecimal ttc, int nbDecimales) {
		BigDecimal tx = UN.add(transformePourcentageEnCoefficient(taux, NB_DECIMALES_CALCUL_COEFFICIENT));
		return ttc.divide(tx, nbDecimales, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * Calcul un montant TTC a partir d'un HT et d'un taux.
	 * @param taux (en %)
	 * @param ht
	 * @param nbDecimales
	 * @return
	 */
	public BigDecimal calculTTCaPartirHT(BigDecimal taux, BigDecimal ht, int nbDecimales) {
		ht = ht.setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
		BigDecimal montantTtc = ht.add(calculMontantTVAaPartirHT(taux, ht, nbDecimales));
		return montantTtc;
	}

	/**
	 * Recalcul si nécessaire les montants HT et TTC d'un transaction en fonction du dicponible budgétaire.
	 *  
	 * @param ttcOrigine
	 * @param htOrigine
	 * @param montantBudgetaireDisponible
	 * @param tauxProratisation % de la TVA n'impactant pas le budget
	 * @return
	 */
	public MontantTransaction calculMontantsProratiseAPartirDisponible(
			BigDecimal htOrigine, 
			BigDecimal ttcOrigine, 
			BigDecimal montantBudgetaireDisponible, 
			BigDecimal tauxProratisation) {
		BigDecimal montantBudgetaire = calculMontantBudgetaire(htOrigine, ttcOrigine.subtract(htOrigine), tauxProratisation);

		if (montantBudgetaireDisponibleInsuffisant(montantBudgetaireDisponible,
				montantBudgetaire)) {
			return new MontantTransaction(htOrigine, ttcOrigine);
		}

		BigDecimal tauxTva = calculTauxTva(ttcOrigine, htOrigine);
		BigDecimal coeffProratisationTauxTva = 
				UN.subtract(tauxProratisation.divide(CENT));		
		BigDecimal coeffPourCalculHTAPartirDeMontantBudgetaire = 
				UN.add(tauxTva.multiply(coeffProratisationTauxTva));
		
		// Todo Pourquoi scale 3
		BigDecimal ht = montantBudgetaireDisponible.divide(
				coeffPourCalculHTAPartirDeMontantBudgetaire, 3, RoundingMode.HALF_UP);
		
		//TODO Pourquoi pas de scale
		BigDecimal ttc = ht.multiply(tauxTva.add(new BigDecimal(1d)));
		
		return new MontantTransaction(ht, ttc);
	}

	//TODO curieux le passage en float sur des BigDecimal
	private boolean montantBudgetaireDisponibleInsuffisant(
			BigDecimal montantBudgétaireDisponible, BigDecimal montant) {
		return montant.floatValue() <= montantBudgétaireDisponible.floatValue();
	}
	
	/**
	 * Calcul un montant budgétaire à partir d'un montant HT, d'un montant de TVA et d'un taux de proratisation.
	 * Le taux de proratisation est le % du montant de TVA sans impact budgétaire.
	 * 
	 * @param ht
	 * @param montantTVA le montant de la tva
	 * @return
	 */
	//TODO incohérence sur les scales de retour GLV 2014
	// Pourquoi 4 décimales et pas trois comme TVA
	public BigDecimal calculMontantBudgetaire(BigDecimal ht, BigDecimal montantTVA, BigDecimal tauxProratisation) {
		if (tauxProratisation.equals(CENT))
			return ht;
		if (tauxProratisation.equals(ZERO))
			return ht.add(montantTVA);

		return ht.add(montantTVA.multiply(UN.subtract(transformePourcentageEnCoefficient(tauxProratisation, 4)).
				setScale(4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	private BigDecimal transformePourcentageEnCoefficient(BigDecimal taux, int nbDecimales) {
		return taux.divide(CENT, nbDecimales, BigDecimal.ROUND_HALF_UP);
	}


}
