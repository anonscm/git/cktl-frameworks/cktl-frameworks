/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import org.cocktail.fwkcktldepense.server.finder.FinderConvention;
import org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

@Deprecated
public class TableauConvention extends Tableau {

	private NSArray arrayConventions;

	public TableauConvention(EOSource laSource, boolean utilePourcentage, Object pere) {
		super(null, utilePourcentage, pere);
		if (laSource != null)
			setSource(laSource);
		arrayConventions = null;
	}

	public void addLigne(Ligne ligne) {
		if (!(ligne instanceof LigneConvention))
			return;
		super.addLigne(ligne);
	}

	public boolean isGood() {
		super.isGood();

		if (isTableauMontant()) {
			if (totalHtFixe() != null && totalHtFixe().floatValue() < totalLignesHt().floatValue())
				return false;
			if (totalTtcFixe() != null && totalTtcFixe().floatValue() < totalLignesTtc().floatValue())
				return false;
		}

		return true;
	}

	public NSArray getConventionsPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayConventions == null && source() != null && utilisateur != null) {
			NSMutableDictionary bindings = new NSMutableDictionary();
			bindings.setObjectForKey(source().organ(), "organ");
			bindings.setObjectForKey(source().typeCredit(), "typeCredit");
			bindings.setObjectForKey(source().exercice(), "exercice");
			bindings.setObjectForKey(new Boolean(true), "montants");
			bindings.setObjectForKey(EOConventionNonLimitative.MODE_GESTION_RESSOURCE_AFFECTEE, "convModeGestion");
			arrayConventions = FinderConvention.getConventions(ed, bindings);
		}
		if (arrayConventions == null)
			arrayConventions = new NSArray();

		if (arrayConventions.count() == 0)
			return arrayConventions;

		if (count() > 0) {
			NSMutableArray resultats = new NSMutableArray(arrayConventions);
			for (int i = 0; i < count(); i++)
				resultats.removeObject(((LigneConvention) objectAtIndex(i)).convention());
			return resultats;
		}

		return arrayConventions;
	}
}
