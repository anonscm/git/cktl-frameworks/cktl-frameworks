/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import org.cocktail.fwkcktldepense.server.finder.FinderCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public class TableauAnalytique extends Tableau {

	private NSArray arrayAnalytiques;

	public TableauAnalytique(EOSource laSource, boolean utilePourcentage, Object pere) {
		super(null, utilePourcentage, pere);
		if (laSource!=null)
			setSource(laSource);
		arrayAnalytiques=null;
	}

	public void addLigne(Ligne ligne) {
		if (!(ligne instanceof LigneAnalytique))
			return;
		super.addLigne(ligne);
	}

	public boolean isGood() {
		super.isGood();
		
		if (isTableauMontant()) {
			if (totalHtFixe()!=null && totalHtFixe().floatValue()<totalLignesHt().floatValue())
				return false;
			if (totalTtcFixe()!=null && totalTtcFixe().floatValue()<totalLignesTtc().floatValue())
				return false;
		}
		
		return true;
	}
	
    public NSArray getCodeAnalytiquesPossibles(EOEditingContext ed, EOUtilisateur utilisateur) {
		if (arrayAnalytiques==null && source()!=null && utilisateur!=null)
			arrayAnalytiques=FinderCodeAnalytique.getCodeAnalytiques(ed, source().organ(), source().exercice());
		if (arrayAnalytiques==null)
			arrayAnalytiques=new NSArray();

		if (arrayAnalytiques.count()==0)
			return arrayAnalytiques;

		if (count()>0) {
			NSMutableArray resultats=new NSMutableArray(arrayAnalytiques);
			for (int i=0; i<count(); i++)
				resultats.removeObject(((LigneAnalytique)objectAtIndex(i)).codeAnalytique());
			return resultats;
		}
		
    	return arrayAnalytiques;
    }
}
