/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.util;

import java.io.Serializable;
import java.util.Comparator;

import org.cocktail.fwkcktldepense.server.metier.EOArticle;

public class CommandeArticleComparator implements Comparator<EOArticle>, Serializable {

	/** Serial version ID. */
	private static final long serialVersionUID = 1L;

	public int compare(EOArticle articleT1, EOArticle articleT2) {
		// identity equality.
		if (articleT1 == articleT2) {
			return 0;
		}

		if (articleT1 == null && articleT2 != null) {
			return 1;
		}
		if (articleT1 != null && articleT2 == null) {
			return -1;
		}

		String articleLibelleT1 = articleT1.artLibelle();
		String articleLibelleT2 = articleT2.artLibelle();

		if (articleLibelleT1 == null && articleLibelleT2 == null) {
			return 0;
		}
		if (articleLibelleT1 == null && articleLibelleT2 != null) {
			return 1;
		}
		if (articleLibelleT1 != null && articleLibelleT2 == null) {
			return -1;
		}

		return articleLibelleT1.compareTo(articleLibelleT2);
	}

}
