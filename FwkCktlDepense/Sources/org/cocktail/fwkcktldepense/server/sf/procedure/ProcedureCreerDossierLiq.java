/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.sf.procedure;

import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.procedure.Procedure;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerDossierLiq extends Procedure {

	private static final String PROCEDURE_NAME = "creerDossierLiq";

	public static boolean enregistrer(_CktlBasicDataBus dataBus, NSArray dpcos, EOUtilisateur utilisateur, EOExercice exercice, String gesCode) throws NSValidation.ValidationException {
		if (dpcos == null || dpcos.count() == 0) {
			throw new NSValidation.ValidationException("Aucune liquidation");
		}
		if (exercice == null) {
			throw new NSValidation.ValidationException("Exercice obligatoire");
		}
		if (utilisateur == null) {
			throw new NSValidation.ValidationException("Utilisateur obligatoire");
		}
		if (gesCode == null) {
			throw new NSValidation.ValidationException("Code gestion obligatoire");
		}

		return dataBus.executeProcedure(ProcedureCreerDossierLiq.PROCEDURE_NAME,
				ProcedureCreerDossierLiq.construireDictionnaire(dpcos, utilisateur, exercice, gesCode));
	}

	private static NSDictionary construireDictionnaire(NSArray dpcos, EOUtilisateur utilisateur, EOExercice exercice, String gesCode) {
		NSMutableDictionary dico = new NSMutableDictionary();

		Integer utlOrdre = (Integer) EOUtilities.primaryKeyForObject(utilisateur.editingContext(), utilisateur).valueForKey(EOUtilisateur.UTL_ORDRE_KEY);
		Integer exeOrdre = exercice.exeExercice();

		//Integer borId = (Integer) EOUtilities.primaryKeyForObject(cxmlItem.editingContext(), cxmlItem.toFournisB2bCxmlParam()).valueForKey(EOFournisB2bCxmlParam.FBCP_ID_KEY);
		dico.takeValueForKey(construireChaineDpcoIds(dpcos), "a020_les_depid");
		dico.takeValueForKey(utlOrdre, "a030_utl_ordre");
		dico.takeValueForKey(exeOrdre, "a040_exe_ordre");
		dico.takeValueForKey(gesCode, "a060_ges_code");
		return dico;
	}

	protected static String construireChaineDpcoIds(NSArray dpcos) {
		String chaine = "";
		for (int i = 0; i < dpcos.count(); i++) {
			EODepenseControlePlanComptable dpco = (EODepenseControlePlanComptable) dpcos.objectAtIndex(i);
			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(dpco.editingContext(), dpco);
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EODepenseControlePlanComptable.DPCO_ID_KEY) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}
}