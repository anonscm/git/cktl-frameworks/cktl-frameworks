package org.cocktail.fwkcktldepense.server.interfaces;

import java.math.BigDecimal;


public class NullDepenseControle implements IDepenseControle {

	public boolean isNull() {
		return true;
	}
	
	public void setMontantHT(BigDecimal montant) {
		throw new UnsupportedOperationException();
	}

	public void setMontantTTC(BigDecimal montant) {
		throw new UnsupportedOperationException();
	}

	public void setMontantTVA(BigDecimal montant) {
		throw new UnsupportedOperationException();
	}

	public void setMontantBudgetaire(BigDecimal montant) {
		throw new UnsupportedOperationException();
	}

	public BigDecimal getMontantHT() {
		return BigDecimal.ZERO;
	}

	public BigDecimal getMontantTTC() {
		return BigDecimal.ZERO;
	}

	public BigDecimal getMontantTVA() {
		return BigDecimal.ZERO;
	}

	public BigDecimal getMontantBudgetaire() {
		return BigDecimal.ZERO;
	}

	public BigDecimal getPourcentage() {
		return BigDecimal.ZERO;
	}

	public String getLabelCode() {
		throw new UnsupportedOperationException();
	}

	public Object getCode() {
		throw new UnsupportedOperationException();
	}

	public BigDecimal getSommeRepartition() {
		return BigDecimal.ZERO;
	}

	public void setMaxReversement(BigDecimal montantPlafond) {
		throw new UnsupportedOperationException();
	}

}
