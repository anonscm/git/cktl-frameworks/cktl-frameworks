package org.cocktail.fwkcktldepense.server.interfaces;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;

public interface IFactoryControle {

	public IDepenseControle creer(EOEditingContext ed, BigDecimal pourcentage, Object code, EOExercice exercice,
			EODepenseBudget depenseBudget) throws FactoryException;

}
