package org.cocktail.fwkcktldepense.server.interfaces;

import java.math.BigDecimal;

import com.webobjects.foundation.NSArray;

public interface IDepenseBudget {
	
	BigDecimal depHtSaisie();

	BigDecimal depTtcSaisie();

	BigDecimal depMontantBudgetaire();

	Boolean isSurExtourne();

	boolean isReversement();

	IDepenseBudget depenseBudgetReversement();
	
	NSArray depenseControleAnalytiques();

	BigDecimal restantHtControleAnalytique();

	BigDecimal restantTtcControleAnalytique();

	
	NSArray depenseControleActions();

	BigDecimal restantHtControleAction();

	BigDecimal restantTtcControleAction();

	
	NSArray depenseControleConventions();

	BigDecimal restantHtControleConvention();

	BigDecimal restantTtcControleConvention();

	
	NSArray depenseControleHorsMarches();

	BigDecimal restantHtControleHorsMarche();

	BigDecimal restantTtcControleHorsMarche();

	
	NSArray depenseControleMarches();

	BigDecimal restantHtControleMarche();

	BigDecimal restantTtcControleMarche();

	
	NSArray depenseControlePlanComptables();

	BigDecimal restantHtControlePlanComptable();

	BigDecimal restantTtcControlePlanComptable();

}
