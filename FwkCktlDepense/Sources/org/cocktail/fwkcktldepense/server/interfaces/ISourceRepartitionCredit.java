package org.cocktail.fwkcktldepense.server.interfaces;

import com.webobjects.foundation.NSArray;

public interface ISourceRepartitionCredit {
	
	public NSArray repartitionPourcentageActions();
	
	public NSArray repartitionPourcentageAnalytiques();
	
	public NSArray repartitionPourcentageConventions();
	
	public NSArray repartitionPourcentageHorsMarches();
	
	public NSArray repartitionPourcentageMarches();
	
	public NSArray repartitionPourcentagePlanComptables();
}
