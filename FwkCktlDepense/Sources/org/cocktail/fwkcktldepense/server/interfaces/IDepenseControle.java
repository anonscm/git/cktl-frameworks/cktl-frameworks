package org.cocktail.fwkcktldepense.server.interfaces;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEditingContext;

public interface IDepenseControle {
	public static  IDepenseControle NULL_DEPENSE_CONTROLE = new NullDepenseControle();

	public boolean isNull();
	
	public void setMontantHT(BigDecimal montant);

	public void setMontantTTC(BigDecimal montant);
	
	public void setMontantTVA(BigDecimal montant);
	
	public void setMontantBudgetaire(BigDecimal montant);
	
	public BigDecimal getMontantHT();
	
	public BigDecimal getMontantTTC();
	
	public BigDecimal getMontantTVA();
	
	public BigDecimal getMontantBudgetaire();
	
	public BigDecimal getPourcentage();
	
	public String getLabelCode();
	
	public Object getCode();
	
	public BigDecimal getSommeRepartition();

	/**
	 * Défini un plafon de reversement
	 * @param montantPlafond Montant du plafond de reversement
	 */
	public void setMaxReversement(BigDecimal montantPlafond);
	
}
