/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerEngagement extends Procedure {

	private static final String PROCEDURE_NAME = "insEngage";

	/**
	 * Appele la procedure de creation de l'engagement budget <BR>
	 * 
	 * @param dataBus _CktlBasicDataBus servant a gerer les transactions
	 * @param engagement EOEngagementBudget qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEngagementBudget engagement) throws NSValidation.ValidationException {

		// on verifie la validite des objets a enregistrer 
		engagement.validateForSave();

		for (int i = 0; i < engagement.engagementControleActions().count(); i++)
			((EOEngagementControleAction) engagement.engagementControleActions().objectAtIndex(i)).validateForSave();
		for (int i = 0; i < engagement.engagementControleAnalytiques().count(); i++)
			((EOEngagementControleAnalytique) engagement.engagementControleAnalytiques().objectAtIndex(i)).validateForSave();
		for (int i = 0; i < engagement.engagementControleConventions().count(); i++)
			((EOEngagementControleConvention) engagement.engagementControleConventions().objectAtIndex(i)).validateForSave();
		for (int i = 0; i < engagement.engagementControleHorsMarches().count(); i++)
			((EOEngagementControleHorsMarche) engagement.engagementControleHorsMarches().objectAtIndex(i)).validateForSave();
		for (int i = 0; i < engagement.engagementControleMarches().count(); i++)
			((EOEngagementControleMarche) engagement.engagementControleMarches().objectAtIndex(i)).validateForSave();
		for (int i = 0; i < engagement.engagementControlePlanComptables().count(); i++)
			((EOEngagementControlePlanComptable) engagement.engagementControlePlanComptables().objectAtIndex(i)).validateForSave();

		// lancement de la procedure		
		return dataBus.executeProcedure(ProcedureCreerEngagement.PROCEDURE_NAME,
				ProcedureCreerEngagement.construireDictionnaire(engagement));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'engagement budget <BR>
	 * 
	 * @param engagement EOEngagementBudget pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOEngagementBudget engagement) {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_eng_id");

		// on cherche la cle de l'exercice
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.exercice().editingContext(), engagement.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020_a_exe_ordre");

		// on met le numero a null, il sera generee dans la procedure
		dico.takeValueForKey(null, "025_a_eng_numero");

		// on cherche la cle du fournisseur
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.fournisseur().editingContext(), engagement.fournisseur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisseur.FOU_ORDRE_KEY), "030_a_fou_ordre");

		// on cherche la cle de la ligne budgetaire
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.organ().editingContext(), engagement.organ());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOOrgan.ORG_ID_KEY), "040_a_org_id");

		// on cherche la cle du type de credit
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.typeCredit().editingContext(), engagement.typeCredit());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeCredit.TCD_ORDRE_KEY), "050_a_tcd_ordre");

		// on cherche la cle du taux de prorata
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.tauxProrata().editingContext(), engagement.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "060_a_tap_id");

		// on renseigne le libelle
		dico.takeValueForKey(engagement.engLibelle(), "065_a_eng_libelle");

		// on renseigne les montants
		dico.takeValueForKey(engagement.engHtSaisie(), "070_a_eng_ht_saisie");
		dico.takeValueForKey(engagement.engTtcSaisie(), "080_a_eng_ttc_saisie");

		// on cherche la cle du type d'application
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.typeApplication().editingContext(), engagement.typeApplication());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeApplication.TYAP_ID_KEY), "100_a_tyap_id");

		// on cherche la cle de l'utilisateur
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engagement.utilisateur().editingContext(), engagement.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "110_a_utl_ordre");

		// on construit la chaine pour les engagementCtrlAction
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineAction(engagement), "130_a_chaine_action");

		// on construit la chaine pour les engagementCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineAnalytique(engagement), "140_a_chaine_analytique");

		// on construit la chaine pour les engagementCtrlConvention
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineConvention(engagement), "145_a_chaine_convention");

		// on construit la chaine pour les engagementCtrlHorsMarche
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineHorsMarche(engagement), "150_a_chaine_hors_marche");

		// on construit la chaine pour les engagementCtrlMarche
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineMarche(engagement), "160_a_chaine_marche");

		// on construit la chaine pour les engagementCtrlPlanco
		dico.takeValueForKey(ProcedureCreerEngagement.construireChainePlanComptable(engagement), "170_a_chaine_planco");

		return dico;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOEngagementControleAction de l'engagement<BR>
	 * Format de la chaine : tyac_id$eact_ht_saisie$eact_ttc_saisie$...$ <BR>
	 * 
	 * @param engagement EOEngagementBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAction(EOEngagementBudget engagement) {
		String chaine = "";

		for (int i = 0; i < engagement.engagementControleActions().count(); i++) {
			EOEngagementControleAction engageCtrlAction = (EOEngagementControleAction) engagement.engagementControleActions().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlAction.typeAction().editingContext(),
					engageCtrlAction.typeAction());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOTypeAction.TYAC_ID_KEY) + "$";

			chaine = chaine + engageCtrlAction.eactHtSaisie() + "$";
			chaine = chaine + engageCtrlAction.eactTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOEngagementControleAnalytique de l'engagement<BR>
	 * Format de la chaine : can_id$eana_ht_saisie$eana_ttc_saisie$...$ <BR>
	 * 
	 * @param engagement EOEngagementBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAnalytique(EOEngagementBudget engagement) {
		String chaine = "";

		for (int i = 0; i < engagement.engagementControleAnalytiques().count(); i++) {
			EOEngagementControleAnalytique engageCtrlAnalytique = (EOEngagementControleAnalytique) engagement.engagementControleAnalytiques().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlAnalytique.codeAnalytique().editingContext(),
					engageCtrlAnalytique.codeAnalytique());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOCodeAnalytique.CAN_ID_KEY) + "$";

			chaine = chaine + engageCtrlAnalytique.eanaHtSaisie() + "$";
			chaine = chaine + engageCtrlAnalytique.eanaTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOEngagementControleConvention de l'engagement<BR>
	 * Format de la chaine : conv_ordre$econ_ht_saisie$econ_ttc_saisie$...$ <BR>
	 * 
	 * @param engagement EOEngagementBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineConvention(EOEngagementBudget engagement) {
		String chaine = "";

		for (int i = 0; i < engagement.engagementControleConventions().count(); i++) {
			EOEngagementControleConvention engageCtrlConvention = (EOEngagementControleConvention) engagement.engagementControleConventions().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlConvention.convention().editingContext(),
					engageCtrlConvention.convention());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOConvention.CONV_ORDRE_KEY) + "$";

			chaine = chaine + engageCtrlConvention.econHtSaisie() + "$";
			chaine = chaine + engageCtrlConvention.econTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOEngagementControleHorsMarche de l'engagement<BR>
	 * Format de la chaine : typa_id$ce_ordre$ehom_ht_saisie$ehom_ttc_saisie$...$ <BR>
	 * 
	 * @param engagement EOEngagementBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineHorsMarche(EOEngagementBudget engagement) {
		String chaine = "";

		for (int i = 0; i < engagement.engagementControleHorsMarches().count(); i++) {
			EOEngagementControleHorsMarche engageCtrlHorsMarche = (EOEngagementControleHorsMarche) engagement.engagementControleHorsMarches().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlHorsMarche.typeAchat().editingContext(),
					engageCtrlHorsMarche.typeAchat());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOTypeAchat.TYPA_ID_KEY) + "$";

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlHorsMarche.codeExer().editingContext(),
					engageCtrlHorsMarche.codeExer());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOCodeExer.CE_ORDRE_KEY) + "$";

			chaine = chaine + engageCtrlHorsMarche.ehomHtSaisie() + "$";
			chaine = chaine + engageCtrlHorsMarche.ehomTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOEngagementControleMarche de l'engagement<BR>
	 * Format de la chaine : att_ordre$emar_ht_saisie$emar_ttc_saisie$...$ <BR>
	 * 
	 * @param engagement EOEngagementBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineMarche(EOEngagementBudget engagement) {
		String chaine = "";

		for (int i = 0; i < engagement.engagementControleMarches().count(); i++) {
			EOEngagementControleMarche engageCtrlMarche = (EOEngagementControleMarche) engagement.engagementControleMarches().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlMarche.attribution().editingContext(),
					engageCtrlMarche.attribution());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOAttribution.ATT_ORDRE_KEY) + "$";

			chaine = chaine + engageCtrlMarche.emarHtSaisie() + "$";
			chaine = chaine + engageCtrlMarche.emarTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOEngagementControlePlanComptable de l'engagement<BR>
	 * Format de la chaine : pco_num$epco_ht_saisie$epco_ttc_saisie$...$ <BR>
	 * 
	 * @param engagement EOEngagementBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChainePlanComptable(EOEngagementBudget engagement) {
		String chaine = "";

		for (int i = 0; i < engagement.engagementControlePlanComptables().count(); i++) {
			EOEngagementControlePlanComptable engageCtrlPlanComptable = (EOEngagementControlePlanComptable) engagement.engagementControlePlanComptables().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(engageCtrlPlanComptable.planComptable().editingContext(),
					engageCtrlPlanComptable.planComptable());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPlanComptable.PCO_NUM_KEY) + "$";

			chaine = chaine + engageCtrlPlanComptable.epcoHtSaisie() + "$";
			chaine = chaine + engageCtrlPlanComptable.epcoTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}
}