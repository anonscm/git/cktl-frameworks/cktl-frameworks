/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureBasculerCommande extends Procedure {

	private static final String PROCEDURE_NAME = "basculerCommande";

	/**
	 * Appele la procedure de trace de basculement de la commande
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param commandeOrigine
	 *        EOCommande qui sera bascule
	 * @param commandeDestination
	 *        nouvelle EOCommande
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOCommande commandeOrigine, EOCommande commandeDestination) throws NSValidation.ValidationException{
		return dataBus.executeProcedure(ProcedureBasculerCommande.PROCEDURE_NAME, 
				ProcedureBasculerCommande.construireDictionnaire(commandeOrigine, commandeDestination));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir des commandes
	 * <BR>
	 * @param commandeOrigine
	 *        EOCommande d'origine pour lequel construire le dictionnaire
	 * @param commandeDestination
	 *        EOCommande de destination pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOCommande commandeOrigine, EOCommande commandeDestination) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on cherche la cle de la commande d'origine
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeOrigine.editingContext(), commandeOrigine);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCommande.COMM_ID_KEY), "010_a_comm_id_origine");

		// on cherche la cle de la commande de destination
		if (commandeDestination.commIdProc()==null) {
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeDestination.editingContext(), commandeDestination);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCommande.COMM_ID_KEY), "020_a_comm_id_destination");
		}
		else
			dico.takeValueForKey(commandeDestination.commIdProc(), "020_a_comm_id_destination");

		return dico;
	}
}