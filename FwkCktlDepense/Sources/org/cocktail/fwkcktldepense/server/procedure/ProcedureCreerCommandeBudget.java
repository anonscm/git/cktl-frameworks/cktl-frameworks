/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerCommandeBudget extends Procedure {

	private static final String PROCEDURE_NAME = "insCommandeBudget";

	/**
	 * Appele la procedure de creation de la commande budget
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param commandeBudget
	 *        EOCommandeBudget qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOCommandeBudget commandeBudget) 
	    throws NSValidation.ValidationException{
		
		// on verifie la validite des objets a enregistrer 
		
    	NSArray larray=commandeBudget.commandeControleActions();
    	
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleAction ctrl=(EOCommandeControleAction)larray.objectAtIndex(i);
    			if (ctrl.typeAction()==null)
    				new FactoryCommandeControleAction().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}

    	larray=commandeBudget.commandeControleAnalytiques();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleAnalytique ctrl=(EOCommandeControleAnalytique)larray.objectAtIndex(i);
    			if (ctrl.codeAnalytique()==null)
    				new FactoryCommandeControleAnalytique().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}
    	
    	larray=commandeBudget.commandeControleConventions();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
			EOCommandeControleConvention ctrl=(EOCommandeControleConvention)larray.objectAtIndex(i);
			if (ctrl.convention()==null)
				new FactoryCommandeControleConvention().supprimer(ctrl.editingContext(), ctrl);
			else
				ctrl.validateForSave();
		}
    	}
    	
    	larray=commandeBudget.commandeControleHorsMarches();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleHorsMarche ctrl=(EOCommandeControleHorsMarche)larray.objectAtIndex(i);
    			ctrl.validateForSave();
    		}
    	}
    	
    	larray=commandeBudget.commandeControleMarches();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleMarche ctrl=(EOCommandeControleMarche)larray.objectAtIndex(i);
    			ctrl.validateForSave();
    		}
    	}
    	
    	larray=commandeBudget.commandeControlePlanComptables();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControlePlanComptable ctrl=(EOCommandeControlePlanComptable)larray.objectAtIndex(i);
    			if (ctrl.planComptable()==null)
    				new FactoryCommandeControlePlanComptable().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}
			
		commandeBudget.validateForSave();

		// lancement de la procedure
	    return dataBus.executeProcedure(ProcedureCreerCommandeBudget.PROCEDURE_NAME, 
	    		ProcedureCreerCommandeBudget.construireDictionnaire(commandeBudget));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la commande budget
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOCommandeBudget commandeBudget) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_cbud_id");
		
		// on cherche la cle de l'engagement
		if (commandeBudget.commande().commIdProc()==null) {
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.commande().editingContext(), commandeBudget.commande());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCommande.COMM_ID_KEY), "020_a_comm_id");
		}
		else
			dico.takeValueForKey(commandeBudget.commande().commIdProc(), "020_a_comm_id");
			
		// on cherche la cle de l'exercice
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.exercice().editingContext(), commandeBudget.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "030_a_exe_ordre");
	
		// on cherche la cle de la ligne budgetaire correspondante
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.organ().editingContext(), commandeBudget.organ());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOOrgan.ORG_ID_KEY), "040_a_org_id");

		// on cherche la cle du type de credit
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.typeCredit().editingContext(), commandeBudget.typeCredit());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeCredit.TCD_ORDRE_KEY), "050_a_tcd_ordre");

		// on cherche la cle du taux de prorata
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.tauxProrata().editingContext(), commandeBudget.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "060_a_tap_id");
		
		// on renseigne les montants
		dico.takeValueForKey(commandeBudget.cbudHtSaisie(), "070_a_cbud_ht_saisie");
		dico.takeValueForKey(commandeBudget.cbudTtcSaisie(), "080_a_cbud_ttc_saisie");

		// on construit la chaine pour les commandeCtrlAction
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineAction(commandeBudget), "090_a_chaine_action");
		
		// on construit la chaine pour les commandeCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineAnalytique(commandeBudget), "100_a_chaine_analytique");

		// on construit la chaine pour les commandeCtrlConvention
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineConvention(commandeBudget), "110_a_chaine_convention");

		// on construit la chaine pour les commandeCtrlHorsMarche
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineHorsMarche(commandeBudget), "114_a_chaine_hors_marche");

		// on construit la chaine pour les commandeCtrlMarche
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineMarche(commandeBudget), "117_a_chaine_marche");

		// on construit la chaine pour les articleCtrlPlanco
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChainePlanComptable(commandeBudget), "120_a_chaine_planco");

		return dico;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOCommandeControleAction de la commande budget<BR>
	 * Format de la chaine :  tyac_id$cact_ht_saisie$cact_ttc_saisie$cact_pourcentage$...$ 
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAction(EOCommandeBudget commandeBudget) {
		String chaine="";
		
		for (int i=0; i<commandeBudget.commandeControleActions().count(); i++) {
			EOCommandeControleAction commandeCtrlAction=(EOCommandeControleAction)commandeBudget.commandeControleActions().objectAtIndex(i);
			
			if (commandeCtrlAction.cactHtSaisie().floatValue()==0.0 &&
					commandeCtrlAction.cactTvaSaisie().floatValue()==0.0 &&
					commandeCtrlAction.cactTtcSaisie().floatValue()==0.0 &&
					commandeCtrlAction.cactMontantBudgetaire().floatValue()==0.0)
				continue;
				
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlAction.typeAction().editingContext(),
					commandeCtrlAction.typeAction());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOTypeAction.TYAC_ID_KEY)+"$";
			
			chaine=chaine+commandeCtrlAction.cactHtSaisie()+"$";
			chaine=chaine+commandeCtrlAction.cactTtcSaisie()+"$";
			chaine=chaine+commandeCtrlAction.cactPourcentage()+"$";
		}
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOCommandeControleAnalytique de la commande budget<BR>
	 * Format de la chaine :  can_id$cana_ht_saisie$cana_ttc_saisie$cana_pourcentage$...$ 
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAnalytique(EOCommandeBudget commandeBudget) {
		String chaine="";

		for (int i=0; i<commandeBudget.commandeControleAnalytiques().count(); i++) {
			EOCommandeControleAnalytique commandeCtrlAnalytique=(EOCommandeControleAnalytique)commandeBudget.commandeControleAnalytiques().objectAtIndex(i);
			
			if (commandeCtrlAnalytique.canaHtSaisie().floatValue()==0.0 &&
					commandeCtrlAnalytique.canaTvaSaisie().floatValue()==0.0 &&
					commandeCtrlAnalytique.canaTtcSaisie().floatValue()==0.0 &&
					commandeCtrlAnalytique.canaMontantBudgetaire().floatValue()==0.0)
				continue;

			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlAnalytique.codeAnalytique().editingContext(),
					commandeCtrlAnalytique.codeAnalytique());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOCodeAnalytique.CAN_ID_KEY)+"$";
			
			chaine=chaine+commandeCtrlAnalytique.canaHtSaisie()+"$";
			chaine=chaine+commandeCtrlAnalytique.canaTtcSaisie()+"$";
			chaine=chaine+commandeCtrlAnalytique.canaPourcentage()+"$";
		}
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOCommandeControleConvention de la commande budget<BR>
	 * Format de la chaine :  conv_ordre$ccon_ht_saisie$ccon_ttc_saisie$ccon_pourcentage$...$ 
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineConvention(EOCommandeBudget commandeBudget) {
		String chaine="";

		for (int i=0; i<commandeBudget.commandeControleConventions().count(); i++) {
			EOCommandeControleConvention commandeCtrlConvention=(EOCommandeControleConvention)commandeBudget.commandeControleConventions().objectAtIndex(i);
			
			if (commandeCtrlConvention.cconHtSaisie().floatValue()==0.0 &&
					commandeCtrlConvention.cconTvaSaisie().floatValue()==0.0 &&
					commandeCtrlConvention.cconTtcSaisie().floatValue()==0.0 &&
					commandeCtrlConvention.cconMontantBudgetaire().floatValue()==0.0)
				continue;

			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlConvention.convention().editingContext(),
					commandeCtrlConvention.convention());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOConvention.CONV_ORDRE_KEY)+"$";
			
			chaine=chaine+commandeCtrlConvention.cconHtSaisie()+"$";
			chaine=chaine+commandeCtrlConvention.cconTtcSaisie()+"$";
			chaine=chaine+commandeCtrlConvention.cconPourcentage()+"$";
		}
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOCommandeControleHorsMarche de la commande budget<BR>
	 * Format de la chaine :  typa_id$ce_ordre$chom_ht_saisie$chom_ttc_saisie$...$ 
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineHorsMarche(EOCommandeBudget commandeBudget) {
		String chaine="";

		for (int i=0; i<commandeBudget.commandeControleHorsMarches().count(); i++) {
			EOCommandeControleHorsMarche commandeCtrlHorsMarche=(EOCommandeControleHorsMarche)commandeBudget.commandeControleHorsMarches().objectAtIndex(i);
			
			if (commandeCtrlHorsMarche.chomHtSaisie().floatValue()==0.0 &&
					commandeCtrlHorsMarche.chomTvaSaisie().floatValue()==0.0 &&
					commandeCtrlHorsMarche.chomTtcSaisie().floatValue()==0.0 &&
					commandeCtrlHorsMarche.chomMontantBudgetaire().floatValue()==0.0)
				continue;

			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlHorsMarche.typeAchat().editingContext(),
					commandeCtrlHorsMarche.typeAchat());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOTypeAchat.TYPA_ID_KEY)+"$";

			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlHorsMarche.codeExer().editingContext(),
					commandeCtrlHorsMarche.codeExer());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOCodeExer.CE_ORDRE_KEY)+"$";

			chaine=chaine+commandeCtrlHorsMarche.chomHtSaisie()+"$";
			chaine=chaine+commandeCtrlHorsMarche.chomTtcSaisie()+"$";
		}
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOCommandeControleMarche de la commande budget<BR>
	 * Format de la chaine :  att_ordre$cmar_ht_saisie$cmar_ttc_saisie$...$ 
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineMarche(EOCommandeBudget commandeBudget) {
		String chaine="";

		for (int i=0; i<commandeBudget.commandeControleMarches().count(); i++) {
			EOCommandeControleMarche commandeCtrlMarche=(EOCommandeControleMarche)commandeBudget.commandeControleMarches().objectAtIndex(i);
			
			if (commandeCtrlMarche.cmarHtSaisie().floatValue()==0.0 &&
					commandeCtrlMarche.cmarTvaSaisie().floatValue()==0.0 &&
					commandeCtrlMarche.cmarTtcSaisie().floatValue()==0.0 &&
					commandeCtrlMarche.cmarMontantBudgetaire().floatValue()==0.0)
				continue;

			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlMarche.attribution().editingContext(),
					commandeCtrlMarche.attribution());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOAttribution.ATT_ORDRE_KEY)+"$";
			
			chaine=chaine+commandeCtrlMarche.cmarHtSaisie()+"$";
			chaine=chaine+commandeCtrlMarche.cmarTtcSaisie()+"$";
		}
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOCommandeControlePlanComptable de la commande budget<BR>
	 * Format de la chaine :  pco_num$cpco_ht_saisie$cpco_ttc_saisie$cpco_pourcentage$...$ 
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChainePlanComptable(EOCommandeBudget commandeBudget) {
		String chaine="";

		for (int i=0; i<commandeBudget.commandeControlePlanComptables().count(); i++) {
			EOCommandeControlePlanComptable commandeCtrlPlanComptable=(EOCommandeControlePlanComptable)commandeBudget.commandeControlePlanComptables().objectAtIndex(i);
			
			if (commandeCtrlPlanComptable.cpcoHtSaisie().floatValue()==0.0 &&
					commandeCtrlPlanComptable.cpcoTvaSaisie().floatValue()==0.0 &&
					commandeCtrlPlanComptable.cpcoTtcSaisie().floatValue()==0.0 &&
					commandeCtrlPlanComptable.cpcoMontantBudgetaire().floatValue()==0.0)
				continue;

			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeCtrlPlanComptable.planComptable().editingContext(),
					commandeCtrlPlanComptable.planComptable());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOPlanComptable.PCO_NUM_KEY)+"$";
			
			chaine=chaine+commandeCtrlPlanComptable.cpcoHtSaisie()+"$";
			chaine=chaine+commandeCtrlPlanComptable.cpcoTtcSaisie()+"$";
			chaine=chaine+commandeCtrlPlanComptable.cpcoPourcentage()+"$";
		}
		chaine=chaine+"$";

		return chaine;
	}
}