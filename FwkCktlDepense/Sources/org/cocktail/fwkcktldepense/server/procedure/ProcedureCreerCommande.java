/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODevise;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerCommande extends Procedure {

	private static final String PROCEDURE_NAME = "insCommande";

	/**
	 * Appele la procedure de creation de la commande
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param commande
	 *        EOCommande qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOCommande commande) throws NSValidation.ValidationException{
		commande.validateForSave();
		
		return dataBus.executeProcedure(ProcedureCreerCommande.PROCEDURE_NAME, 
				ProcedureCreerCommande.construireDictionnaire(commande));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la commande
	 * <BR>
	 * @param commande
	 *        EOCommande pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOCommande commande) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_comm_id");

		// on cherche la cle de l'exercice
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commande.exercice().editingContext(), commande.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020_a_exe_ordre");

		// on met le numero de la commande a null, il sera genere dans la procedure
		dico.takeValueForKey(null, "030_a_comm_numero");

		// on cherche la cle du type de l'etat 
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commande.typeEtat().editingContext(), commande.typeEtat());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeEtat.TYET_ID_KEY), "040_a_tyet_id");

		// on cherche la cle du fournisseur
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commande.fournisseur().editingContext(), commande.fournisseur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisseur.FOU_ORDRE_KEY), "050_a_fou_ordre");

		// on renseigne la reference de la commande
		if (commande.commReference()!=null)
			dico.takeValueForKey(commande.commReference(), "060_a_comm_reference");

		// on renseigne le libelle de la commande
		dico.takeValueForKey(commande.commLibelle(), "070_a_comm_libelle");

		// on cherche la cle du type de l'etat imprimable
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commande.typeEtatImprimable().editingContext(), commande.typeEtatImprimable());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeEtat.TYET_ID_KEY), "080_a_tyet_id_imprimable");

		// on cherche la cle de la devise
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commande.devise().editingContext(), commande.devise());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EODevise.DEV_ID_KEY), "100_a_dev_id");

		// on cherche la cle de l'utilisateur
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commande.utilisateur().editingContext(), commande.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "110_a_utl_ordre");

		return dico;
	}
}