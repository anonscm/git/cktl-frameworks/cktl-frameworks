/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerPreDepense extends Procedure {

	private static final String PROCEDURE_NAME = "insPreDepense";

	/**
	 * Appele la procedure de creation de la pre depense budget <BR>
	 * 
	 * @param dataBus _CktlBasicDataBus servant a gerer les transactions
	 * @param engagement EOPreDepenseBudget qui sera enregistre
	 * @return boolean indiquant si oui ou non la procedure s'est bien deroulee
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOPreDepenseBudget preDepenseBudget) throws NSValidation.ValidationException {

		// on verifie la validite des objets a enregistrer 

		NSArray larray = preDepenseBudget.preDepenseControleActions();

		if (larray != null && larray.count() > 0) {
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOPreDepenseControleAction ctrl = (EOPreDepenseControleAction) larray.objectAtIndex(i);
				if (ctrl.typeAction() == null)
					new FactoryPreDepenseControleAction().supprimer(ctrl.editingContext(), ctrl);
				else
					ctrl.validateForSave();
			}
		}

		larray = preDepenseBudget.preDepenseControleAnalytiques();
		if (larray != null && larray.count() > 0) {
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOPreDepenseControleAnalytique ctrl = (EOPreDepenseControleAnalytique) larray.objectAtIndex(i);
				if (ctrl.codeAnalytique() == null)
					new FactoryPreDepenseControleAnalytique().supprimer(ctrl.editingContext(), ctrl);
				else
					ctrl.validateForSave();
			}
		}

		larray = preDepenseBudget.preDepenseControleConventions();
		if (larray != null && larray.count() > 0) {
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOPreDepenseControleConvention ctrl = (EOPreDepenseControleConvention) larray.objectAtIndex(i);
				if (ctrl.convention() == null)
					new FactoryPreDepenseControleConvention().supprimer(ctrl.editingContext(), ctrl);
				else
					ctrl.validateForSave();
			}
		}

		larray = preDepenseBudget.preDepenseControleHorsMarches();
		if (larray != null && larray.count() > 0) {
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOPreDepenseControleHorsMarche ctrl = (EOPreDepenseControleHorsMarche) larray.objectAtIndex(i);
				ctrl.validateForSave();
			}
		}

		larray = preDepenseBudget.preDepenseControleMarches();
		if (larray != null && larray.count() > 0) {
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOPreDepenseControleMarche ctrl = (EOPreDepenseControleMarche) larray.objectAtIndex(i);
				ctrl.validateForSave();
			}
		}

		larray = preDepenseBudget.preDepenseControlePlanComptables();
		if (larray != null && larray.count() > 0) {
			for (int i = larray.count() - 1; i >= 0; i--) {
				EOPreDepenseControlePlanComptable ctrl = (EOPreDepenseControlePlanComptable) larray.objectAtIndex(i);
				if (ctrl.planComptable() == null)
					new FactoryPreDepenseControlePlanComptable().supprimer(ctrl.editingContext(), ctrl);
				else
					ctrl.validateForSave();
			}
		}

		preDepenseBudget.validateForSave();

		// lancement de la procedure
		return dataBus.executeProcedure(ProcedureCreerPreDepense.PROCEDURE_NAME,
				ProcedureCreerPreDepense.construireDictionnaire(preDepenseBudget));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la depense budget <BR>
	 * 
	 * @param engagement EOPreDepenseBudget pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOPreDepenseBudget preDepenseBudget) {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_dep_id");

		// on cherche la cle de l'exercice
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(preDepenseBudget.exercice().editingContext(), preDepenseBudget.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020_a_exe_ordre");

		// on cherche la cle de la depense papier correspondante
		if (preDepenseBudget.depensePapier().dppIdProc() == null) {
			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(preDepenseBudget.depensePapier().editingContext(), preDepenseBudget.depensePapier());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EODepensePapier.DPP_ID_KEY), "030_a_dpp_id");
		}
		else
			dico.takeValueForKey(preDepenseBudget.depensePapier().dppIdProc(), "030_a_dpp_id");

		// on cherche la cle de l'engagement budget correspondant
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(preDepenseBudget.engagementBudget().editingContext(), preDepenseBudget.engagementBudget());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOEngagementBudget.ENG_ID_KEY), "040_a_eng_id");

		// on renseigne les montants
		dico.takeValueForKey(preDepenseBudget.depHtSaisie(), "050_a_pdep_ht_saisie");
		dico.takeValueForKey(preDepenseBudget.depTtcSaisie(), "060_a_pdep_ttc_saisie");

		// on cherche la cle du taux de prorata
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(preDepenseBudget.tauxProrata().editingContext(), preDepenseBudget.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "070_a_tap_id");

		// on cherche la cle de l'utilisateur
		dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(preDepenseBudget.utilisateur().editingContext(), preDepenseBudget.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "080_a_utl_ordre");

		// on construit la chaine pour les depenseCtrlAction
		dico.takeValueForKey(ProcedureCreerPreDepense.construireChaineAction(preDepenseBudget), "110_a_chaine_action");

		// on construit la chaine pour les depenseCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerPreDepense.construireChaineAnalytique(preDepenseBudget), "120_a_chaine_analytique");

		// on construit la chaine pour les depenseCtrlConvention
		dico.takeValueForKey(ProcedureCreerPreDepense.construireChaineConvention(preDepenseBudget), "125_a_chaine_convention");

		// on construit la chaine pour les depenseCtrlHorsMarche
		dico.takeValueForKey(ProcedureCreerPreDepense.construireChaineHorsMarche(preDepenseBudget), "140_a_chaine_hors_marche");

		// on construit la chaine pour les depenseCtrlMarche
		dico.takeValueForKey(ProcedureCreerPreDepense.construireChaineMarche(preDepenseBudget), "150_a_chaine_marche");

		// on construit la chaine pour les depenseCtrlPlanco
		dico.takeValueForKey(ProcedureCreerPreDepense.construireChainePlanComptable(preDepenseBudget), "160_a_chaine_planco");

		return dico;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOPreDepenseControleAction de la depense<BR>
	 * Format de la chaine : tyac_id$dact_ht_saisie$dact_ttc_saisie$...$ <BR>
	 * 
	 * @param depenseBudget EOPreDepenseBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAction(EOPreDepenseBudget depenseBudget) {
		String chaine = "";

		for (int i = 0; i < depenseBudget.preDepenseControleActions().count(); i++) {
			EOPreDepenseControleAction depenseCtrlAction = (EOPreDepenseControleAction) depenseBudget.preDepenseControleActions().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlAction.typeAction().editingContext(),
					depenseCtrlAction.typeAction());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOTypeAction.TYAC_ID_KEY) + "$";

			chaine = chaine + depenseCtrlAction.dactHtSaisie() + "$";
			chaine = chaine + depenseCtrlAction.dactTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOPreDepenseControleAnalytique de la depense<BR>
	 * Format de la chaine : can_id$dana_ht_saisie$dana_ttc_saisie$...$ <BR>
	 * 
	 * @param depenseBudget EOPreDepenseBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAnalytique(EOPreDepenseBudget depenseBudget) {
		String chaine = "";

		for (int i = 0; i < depenseBudget.preDepenseControleAnalytiques().count(); i++) {
			EOPreDepenseControleAnalytique depenseCtrlAnalytique = (EOPreDepenseControleAnalytique) depenseBudget.preDepenseControleAnalytiques().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlAnalytique.codeAnalytique().editingContext(),
					depenseCtrlAnalytique.codeAnalytique());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOCodeAnalytique.CAN_ID_KEY) + "$";

			chaine = chaine + depenseCtrlAnalytique.danaHtSaisie() + "$";
			chaine = chaine + depenseCtrlAnalytique.danaTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOPreDepenseControleConvention de la depense<BR>
	 * Format de la chaine : conv_ordre$dcon_ht_saisie$dcon_ttc_saisie$...$ <BR>
	 * 
	 * @param depenseBudget EOPreDepenseBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineConvention(EOPreDepenseBudget depenseBudget) {
		String chaine = "";

		for (int i = 0; i < depenseBudget.preDepenseControleConventions().count(); i++) {
			EOPreDepenseControleConvention depenseCtrlConvention = (EOPreDepenseControleConvention) depenseBudget.preDepenseControleConventions().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlConvention.convention().editingContext(),
					depenseCtrlConvention.convention());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOConvention.CONV_ORDRE_KEY) + "$";

			chaine = chaine + depenseCtrlConvention.dconHtSaisie() + "$";
			chaine = chaine + depenseCtrlConvention.dconTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOPreDepenseControleHorsMarche de la depense<BR>
	 * Format de la chaine : typa_id$ce_ordre$dhom_ht_saisie$dhom_ttc_saisie$...$ <BR>
	 * 
	 * @param depenseBudget EOPreDepenseBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineHorsMarche(EOPreDepenseBudget depenseBudget) {
		String chaine = "";

		for (int i = 0; i < depenseBudget.preDepenseControleHorsMarches().count(); i++) {
			EOPreDepenseControleHorsMarche depenseCtrlHorsMarche = (EOPreDepenseControleHorsMarche) depenseBudget.preDepenseControleHorsMarches().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlHorsMarche.typeAchat().editingContext(),
					depenseCtrlHorsMarche.typeAchat());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOTypeAchat.TYPA_ID_KEY) + "$";

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlHorsMarche.codeExer().editingContext(),
					depenseCtrlHorsMarche.codeExer());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOCodeExer.CE_ORDRE_KEY) + "$";

			chaine = chaine + depenseCtrlHorsMarche.dhomHtSaisie() + "$";
			chaine = chaine + depenseCtrlHorsMarche.dhomTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOPreDepenseControleMarche de la depense<BR>
	 * Format de la chaine : att_ordre$dmar_ht_saisie$dmar_ttc_saisie$...$ <BR>
	 * 
	 * @param depenseBudget EOPreDepenseBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineMarche(EOPreDepenseBudget depenseBudget) {
		String chaine = "";

		for (int i = 0; i < depenseBudget.preDepenseControleMarches().count(); i++) {
			EOPreDepenseControleMarche depenseCtrlMarche = (EOPreDepenseControleMarche) depenseBudget.preDepenseControleMarches().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlMarche.attribution().editingContext(),
					depenseCtrlMarche.attribution());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOAttribution.ATT_ORDRE_KEY) + "$";

			chaine = chaine + depenseCtrlMarche.dmarHtSaisie() + "$";
			chaine = chaine + depenseCtrlMarche.dmarTtcSaisie() + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EOPreDepenseControlePlanComptable de la depense<BR>
	 * Format de la chaine : pco_num$dpco_ht_saisie$dpco_ttc_saisie$ecd_ordre$...$ <BR>
	 * 
	 * @param depenseBudget EOPreDepenseBudget a partir duquel on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChainePlanComptable(EOPreDepenseBudget depenseBudget) {
		String chaine = "";

		for (int i = 0; i < depenseBudget.preDepenseControlePlanComptables().count(); i++) {
			EOPreDepenseControlePlanComptable depenseCtrlPlanComptable = (EOPreDepenseControlePlanComptable) depenseBudget.preDepenseControlePlanComptables().objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlPlanComptable.planComptable().editingContext(),
					depenseCtrlPlanComptable.planComptable());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPlanComptable.PCO_NUM_KEY) + "$";

			chaine = chaine + depenseCtrlPlanComptable.dpcoHtSaisie() + "$";
			chaine = chaine + depenseCtrlPlanComptable.dpcoTtcSaisie() + "$";

			if (depenseCtrlPlanComptable.ecritureDetail() != null) {
				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(depenseCtrlPlanComptable.ecritureDetail().editingContext(),
						depenseCtrlPlanComptable.ecritureDetail());
				chaine = chaine + dicoForPrimaryKeys.objectForKey(EOEcritureDetail.ECD_ORDRE_KEY);
			}
			chaine = chaine + "$";

			for (int j = 0; j < depenseCtrlPlanComptable.preDepenseControlePlanComptableInventaires().count(); j++) {
				EOPreDepenseControlePlanComptableInventaire pdcInventaire = (EOPreDepenseControlePlanComptableInventaire) depenseCtrlPlanComptable.preDepenseControlePlanComptableInventaires().objectAtIndex(j);

				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(pdcInventaire.editingContext(), pdcInventaire.inventaire());
				chaine = chaine + dicoForPrimaryKeys.objectForKey(EOInventaire.INVC_ID_KEY) + "|";
				chaine = chaine + pdcInventaire.pdpinMontantBudgetaire() + "|";
				if (j == depenseCtrlPlanComptable.preDepenseControlePlanComptableInventaires().count() - 1)
					chaine = chaine + "|";
			}
			chaine = chaine + "$";
		}

		chaine = chaine + "$";

		return chaine;
	}
}