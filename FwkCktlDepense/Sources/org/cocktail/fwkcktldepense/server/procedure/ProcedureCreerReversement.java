/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerReversement extends Procedure {

	private static final String PROCEDURE_NAME = "insReverse";

	/**
	 * Appele la procedure de creation de la depense budget
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param engagement
	 *        EODepenseBudget qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EODepenseBudget depenseBudget) throws NSValidation.ValidationException{

		
		// on verifie la validite des objets a enregistrer 
		
    	NSArray larray=depenseBudget.depenseControleActions();
    	
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EODepenseControleAction ctrl=(EODepenseControleAction)larray.objectAtIndex(i);
    			if (ctrl.typeAction()==null)
    				new FactoryDepenseControleAction().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}

    	larray=depenseBudget.depenseControleAnalytiques();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EODepenseControleAnalytique ctrl=(EODepenseControleAnalytique)larray.objectAtIndex(i);
    			if (ctrl.codeAnalytique()==null)
    				new FactoryDepenseControleAnalytique().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}
    	
    	larray=depenseBudget.depenseControleConventions();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
			EODepenseControleConvention ctrl=(EODepenseControleConvention)larray.objectAtIndex(i);
			if (ctrl.convention()==null)
				new FactoryDepenseControleConvention().supprimer(ctrl.editingContext(), ctrl);
			else
				ctrl.validateForSave();
		}
    	}
    	
    	larray=depenseBudget.depenseControleHorsMarches();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EODepenseControleHorsMarche ctrl=(EODepenseControleHorsMarche)larray.objectAtIndex(i);
    			ctrl.validateForSave();
    		}
    	}
    	
    	larray=depenseBudget.depenseControleMarches();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EODepenseControleMarche ctrl=(EODepenseControleMarche)larray.objectAtIndex(i);
    			ctrl.validateForSave();
    		}
    	}
    	
    	larray=depenseBudget.depenseControlePlanComptables();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EODepenseControlePlanComptable ctrl=(EODepenseControlePlanComptable)larray.objectAtIndex(i);
    			if (ctrl.planComptable()==null)
    				new FactoryDepenseControlePlanComptable().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}
			
		depenseBudget.validateForSave();
		
		// lancement de la procedure
	    return dataBus.executeProcedure(ProcedureCreerReversement.PROCEDURE_NAME, 
	    		ProcedureCreerReversement.construireDictionnaire(depenseBudget));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la depense budget
	 * <BR>
	 * @param engagement
	 *        EODepenseBudget pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EODepenseBudget depenseBudget) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_dep_id");
		
		// on cherche la cle de l'exercice
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseBudget.exercice().editingContext(), depenseBudget.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020_a_exe_ordre");

		// on cherche la cle de la depense papier correspondante
		if (depenseBudget.depensePapier().dppIdProc()==null) {
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseBudget.depensePapier().editingContext(), depenseBudget.depensePapier());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EODepensePapier.DPP_ID_KEY), "030_a_dpp_id");
		}
		else
			dico.takeValueForKey(depenseBudget.depensePapier().dppIdProc(), "030_a_dpp_id");

		// on cherche la cle de l'engagement budget correspondant
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseBudget.engagementBudget().editingContext(), depenseBudget.engagementBudget());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOEngagementBudget.ENG_ID_KEY), "040_a_eng_id");
		
		// on renseigne les montants
		dico.takeValueForKey(depenseBudget.depHtSaisie(), "050_a_dep_ht_saisie");
		dico.takeValueForKey(depenseBudget.depTtcSaisie(), "060_a_dep_ttc_saisie");

		// on cherche la cle du taux de prorata
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseBudget.tauxProrata().editingContext(), depenseBudget.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "070_a_tap_id");

		// on cherche la cle de l'utilisateur
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseBudget.utilisateur().editingContext(), depenseBudget.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "080_a_utl_ordre");

		// on cherche la cle de la depense d'origine dans le cas d'ordre de reversement
		if (depenseBudget.depenseBudgetReversement()!=null){
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseBudget.depenseBudgetReversement().editingContext(),
					depenseBudget.depenseBudgetReversement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EODepenseBudget.DEP_ID_KEY), "100_a_dep_id_reversement");
		}
		else dico.takeValueForKey(null, "100_a_dep_id_reversement");
		
		// on construit la chaine pour les depenseCtrlAction
		dico.takeValueForKey(ProcedureCreerReversement.construireChaineAction(depenseBudget), "110_a_chaine_action");
		
		// on construit la chaine pour les depenseCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerReversement.construireChaineAnalytique(depenseBudget), "120_a_chaine_analytique");

		// on construit la chaine pour les depenseCtrlConvention
		dico.takeValueForKey(ProcedureCreerReversement.construireChaineConvention(depenseBudget), "125_a_chaine_convention");
		
		// on construit la chaine pour les depenseCtrlHorsMarche
		dico.takeValueForKey(ProcedureCreerReversement.construireChaineHorsMarche(depenseBudget), "130_a_chaine_hors_marche");

		// on construit la chaine pour les depenseCtrlMarche
		dico.takeValueForKey(ProcedureCreerReversement.construireChaineMarche(depenseBudget), "140_a_chaine_marche");
		
		// on construit la chaine pour les depenseCtrlPlanco
		dico.takeValueForKey(ProcedureCreerReversement.construireChainePlanComptable(depenseBudget), "150_a_chaine_planco");
		
		return dico;
	}

	/**
	 * Construit la chaine de caracteres relative aux EODepenseControleAction de la depense<BR>
	 * Format de la chaine :  tyac_id$dact_ht_saisie$dact_ttc_saisie$...$ 
	 * <BR>
	 * @param depenseBudget
	 *        EODepenseBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAction(EODepenseBudget depenseBudget) {
		String chaine="";
		
		for (int i=0; i<depenseBudget.depenseControleActions().count(); i++) {
			EODepenseControleAction depenseCtrlAction=(EODepenseControleAction)depenseBudget.depenseControleActions().objectAtIndex(i);
			
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlAction.typeAction().editingContext(),
					depenseCtrlAction.typeAction());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOTypeAction.TYAC_ID_KEY)+"$";
			
			chaine=chaine+depenseCtrlAction.dactHtSaisie()+"$";
			chaine=chaine+depenseCtrlAction.dactTtcSaisie()+"$";
		}
		
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EODepenseControleAnalytique de la depense<BR>
	 * Format de la chaine :  can_id$dana_ht_saisie$dana_ttc_saisie$...$ 
	 * <BR>
	 * @param depenseBudget
	 *        EODepenseBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineAnalytique(EODepenseBudget depenseBudget) {
		String chaine="";
		
		for (int i=0; i<depenseBudget.depenseControleAnalytiques().count(); i++) {
			EODepenseControleAnalytique depenseCtrlAnalytique=(EODepenseControleAnalytique)depenseBudget.depenseControleAnalytiques().objectAtIndex(i);
			
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlAnalytique.codeAnalytique().editingContext(),
					depenseCtrlAnalytique.codeAnalytique());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOCodeAnalytique.CAN_ID_KEY)+"$";
			
			chaine=chaine+depenseCtrlAnalytique.danaHtSaisie()+"$";
			chaine=chaine+depenseCtrlAnalytique.danaTtcSaisie()+"$";
		}
		
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EODepenseControleConvention de la depense<BR>
	 * Format de la chaine :  conv_ordre$dcon_ht_saisie$dcon_ttc_saisie$...$ 
	 * <BR>
	 * @param depenseBudget
	 *        EODepenseBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineConvention(EODepenseBudget depenseBudget) {
		String chaine="";
		
		for (int i=0; i<depenseBudget.depenseControleConventions().count(); i++) {
			EODepenseControleConvention depenseCtrlConvention=(EODepenseControleConvention)depenseBudget.depenseControleConventions().objectAtIndex(i);
			
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlConvention.convention().editingContext(),
					depenseCtrlConvention.convention());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOConvention.CONV_ORDRE_KEY)+"$";
			
			chaine=chaine+depenseCtrlConvention.dconHtSaisie()+"$";
			chaine=chaine+depenseCtrlConvention.dconTtcSaisie()+"$";
		}
		
		chaine=chaine+"$";

		return chaine;
	}
	
	/**
	 * Construit la chaine de caracteres relative aux EODepenseControleHorsMarche de la depense<BR>
	 * Format de la chaine :  typa_id$ce_ordre$dhom_ht_saisie$dhom_ttc_saisie$...$ 
	 * <BR>
	 * @param depenseBudget
	 *        EODepenseBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineHorsMarche(EODepenseBudget depenseBudget) {
		String chaine="";
		
		for (int i=0; i<depenseBudget.depenseControleHorsMarches().count(); i++) {
			EODepenseControleHorsMarche depenseCtrlHorsMarche=(EODepenseControleHorsMarche)depenseBudget.depenseControleHorsMarches().objectAtIndex(i);
			
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlHorsMarche.typeAchat().editingContext(),
					depenseCtrlHorsMarche.typeAchat());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOTypeAchat.TYPA_ID_KEY)+"$";

			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlHorsMarche.codeExer().editingContext(),
					depenseCtrlHorsMarche.codeExer());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOCodeExer.CE_ORDRE_KEY)+"$";

			chaine=chaine+depenseCtrlHorsMarche.dhomHtSaisie()+"$";
			chaine=chaine+depenseCtrlHorsMarche.dhomTtcSaisie()+"$";
		}
		
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EODepenseControleMarche de la depense<BR>
	 * Format de la chaine :  att_ordre$dmar_ht_saisie$dmar_ttc_saisie$...$ 
	 * <BR>
	 * @param depenseBudget
	 *        EODepenseBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChaineMarche(EODepenseBudget depenseBudget) {
		String chaine="";
		
		for (int i=0; i<depenseBudget.depenseControleMarches().count(); i++) {
			EODepenseControleMarche depenseCtrlMarche=(EODepenseControleMarche)depenseBudget.depenseControleMarches().objectAtIndex(i);
			
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlMarche.attribution().editingContext(),
					depenseCtrlMarche.attribution());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOAttribution.ATT_ORDRE_KEY)+"$";
			
			chaine=chaine+depenseCtrlMarche.dmarHtSaisie()+"$";
			chaine=chaine+depenseCtrlMarche.dmarTtcSaisie()+"$";
		}
		
		chaine=chaine+"$";

		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EODepenseControlePlanComptable de la depense<BR>
	 * Format de la chaine :  pco_num$dpco_ht_saisie$dpco_ttc_saisie$ecd_ordre$chaine_inventaire$...$ 
	 * <BR>
	 * @param depenseBudget
	 *        EODepenseBudget a partir duquel on travaille
	 * @return
	 *        un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String construireChainePlanComptable(EODepenseBudget depenseBudget) {
		String chaine="";
		
		for (int i=0; i<depenseBudget.depenseControlePlanComptables().count(); i++) {
			EODepenseControlePlanComptable depenseCtrlPlanComptable=(EODepenseControlePlanComptable)depenseBudget.depenseControlePlanComptables().objectAtIndex(i);
			
			NSDictionary dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlPlanComptable.planComptable().editingContext(),
					depenseCtrlPlanComptable.planComptable());
			chaine=chaine+dicoForPrimaryKeys.objectForKey(EOPlanComptable.PCO_NUM_KEY)+"$";
			
			chaine=chaine+depenseCtrlPlanComptable.dpcoHtSaisie()+"$";
			chaine=chaine+depenseCtrlPlanComptable.dpcoTtcSaisie()+"$";
			
			if (depenseCtrlPlanComptable.ecritureDetail()!=null) {
				dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depenseCtrlPlanComptable.ecritureDetail().editingContext(),
						depenseCtrlPlanComptable.ecritureDetail());
				chaine=chaine+dicoForPrimaryKeys.objectForKey(EOEcritureDetail.ECD_ORDRE_KEY);
			}
			chaine=chaine+"$";
			
			for (int j=0; j<depenseCtrlPlanComptable.inventaires().count(); j++) {
				EOInventaire inventaire=(EOInventaire)depenseCtrlPlanComptable.inventaires().objectAtIndex(j);
				
				if (inventaire.lidMontant().floatValue()==0.0)
					continue;
				
				dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(inventaire.editingContext(), inventaire);
				chaine=chaine+dicoForPrimaryKeys.objectForKey(EOInventaire.INVC_ID_KEY)+"|";
				chaine=chaine+inventaire.lidMontant()+"|";
				if (j==depenseCtrlPlanComptable.inventaires().count()-1)
					chaine=chaine+"|";
			}
			chaine=chaine+"$";
		}
		
		chaine=chaine+"$";

		return chaine;
	}
}