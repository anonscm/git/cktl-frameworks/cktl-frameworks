/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux;
import org.cocktail.fwkcktldepense.server.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.server.metier.EORibFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerDepensePapierAvecInteretsMoratoires extends Procedure {

	private static final String PROCEDURE_NAME = "insDepensePapierAvecIM";

	/**
	 * Appele la procedure de creation de la depense papier (facture envoyee par le fournisseur)
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param depensePapier
	 *        EODepensePapier qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EODepensePapier depensePapier) throws NSValidation.ValidationException {
		depensePapier.validateForSave();
		
	    return dataBus.executeProcedure(ProcedureCreerDepensePapierAvecInteretsMoratoires.PROCEDURE_NAME, 
	    		ProcedureCreerDepensePapierAvecInteretsMoratoires.construireDictionnaire(depensePapier));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la depense papier
	 * <BR>
	 * @param depensePapier
	 *        EODepensePapier pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EODepensePapier depensePapier) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_dpp_id");
		
		// on cherche la cle de l'exercice
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.exercice().editingContext(), depensePapier.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020_a_exe_ordre");

		// on renseigne le numero de la facture
		dico.takeValueForKey(depensePapier.dppNumeroFacture(), "030_a_dpp_numero_facture");

		// on renseigne les montants
		dico.takeValueForKey(depensePapier.dppHtInitial(), "040_a_dpp_ht_initial");
		dico.takeValueForKey(depensePapier.dppTtcInitial(), "050_a_dpp_ttc_initial");

		// on cherche la cle du fournisseur
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.fournisseur().editingContext(), depensePapier.fournisseur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisseur.FOU_ORDRE_KEY), "060_a_fou_ordre");
		
		// on cherche la cle du rib si il y en a 
		if (depensePapier.ribFournisseur()!=null){
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.ribFournisseur().editingContext(), depensePapier.ribFournisseur());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORibFournisseur.RIB_ORDRE_KEY), "070_a_rib_ordre");
		}
		else dico.takeValueForKey(null, "070_a_rib_ordre");

		// on cherche la cle du mode de paiement si il y en a
		if (depensePapier.modePaiement()!=null){
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.modePaiement().editingContext(), depensePapier.modePaiement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOModePaiement.MOD_ORDRE_KEY), "080_a_mod_ordre");
		}
		else dico.takeValueForKey(null, "080_a_mod_ordre");

		// on renseigne les dates
		dico.takeValueForKey(depensePapier.dppDateFacture(), "090_a_dpp_date_facture");
		dico.takeValueForKey(depensePapier.dppDateSaisie(), "100_a_dpp_date_saisie");
		dico.takeValueForKey(depensePapier.dppDateReception(), "110_a_dpp_date_reception");
		dico.takeValueForKey(depensePapier.dppDateServiceFait(), "120_a_dpp_date_service_fait");
	
		// on renseigne le nombre de pieces
		dico.takeValueForKey(depensePapier.dppNbPiece(), "130_a_dpp_nb_piece");
	
		// on cherche la cle de l'utilisateur
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.utilisateur().editingContext(), depensePapier.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "140_a_utl_ordre");

		// on cherche la cle de la depense d'origine dans le cas d'ordre de reversement
		if (depensePapier.depensePapierReversement()!=null){
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.depensePapierReversement().editingContext(),
					depensePapier.depensePapierReversement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EODepensePapier.DPP_ID_KEY), "150_a_dpp_id_reversement");
		}
		else dico.takeValueForKey(null, "150_a_dpp_id_reversement");
		
		dico.takeValueForKey(depensePapier.dppImTaux(), "160_a_dpp_im_taux");
		dico.takeValueForKey(depensePapier.dppImDgp(), "170_a_dpp_im_dgp");
		if (depensePapier.imTypeTaux()!=null){
			dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.imTypeTaux().editingContext(),
					depensePapier.imTypeTaux());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOImTypeTaux.IMTT_ID_KEY), "180_imtt_id");
		}
		else dico.takeValueForKey(null, "180_imtt_id");
		
		return dico;
	}
}