/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeMarche;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureCreerArticleCatalogue extends Procedure {

	private static final String PROCEDURE_NAME = "insArticleCatalogue";

	/**
	 * Appele la procedure de creation de l'article catalogue
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param article
	 *        EOArticle qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOArticle article) throws NSValidation.ValidationException{
		article.validateForSave();
		
	    return dataBus.executeProcedure(ProcedureCreerArticleCatalogue.PROCEDURE_NAME, 
	    		ProcedureCreerArticleCatalogue.construireDictionnaire(article));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'article
	 * <BR>
	 * @param article
	 *        EOArticle pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOArticle article) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010_a_caar_id");
		
		// on cherche la cle du fournisseur
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(article.commande().fournisseur().editingContext(),
				article.commande().fournisseur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisseur.FOU_ORDRE_KEY), "020_a_fou_ordre");

		// on renseigne le libelle de l'article
		dico.takeValueForKey(article.artLibelle(), "030_a_art_libelle");

		// on renseigne les montants
		dico.takeValueForKey(article.artPrixHt(), "040_a_art_prix_ht");
		dico.takeValueForKey(article.artPrixTtc(), "050_a_art_prix_ttc");
		
		// on renseigne la reference de l'article
		if (article.artReference()==null)
			article.setArtReference(" ");
		dico.takeValueForKey(article.artReference(), "060_a_art_reference");

		// on cherche la cle du code de nomenclature
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(article.codeExer().codeMarche().editingContext(),
        		article.codeExer().codeMarche());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCodeMarche.CM_ORDRE_KEY), "070_a_cm_ordre");

		// on cherche la cle du taux de tva
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(article.tva().editingContext(), article.tva());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTva.TVA_ID_KEY), "080_a_tva_id");

		return dico;
	}
}