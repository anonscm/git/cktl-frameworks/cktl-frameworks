/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureSupprimerPreDepenseBudgets extends Procedure {

	private static final String PROCEDURE_NAME = "delPreDepenseBudgets";

	/**
	 * Appele la procedure d'annulation des pdepenseBudget associees a la depensePapier
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param commande
	 *        EODepensePapier qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EODepensePapier depensePapier) throws NSValidation.ValidationException{
		if (depensePapier.editingContext().globalIDForObject(depensePapier).isTemporary())
			return true;
		return dataBus.executeProcedure(ProcedureSupprimerPreDepenseBudgets.PROCEDURE_NAME, 
				ProcedureSupprimerPreDepenseBudgets.construireDictionnaire(depensePapier));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la depenseBudget
	 * <BR>
	 * @param commande
	 *        EOCommande pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EODepensePapier depensePapier) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on cherche la cle de la commande
		
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.editingContext(), depensePapier);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EODepensePapier.DPP_ID_KEY), "010_a_dpp_id");

		// on cherche la cle de l'utilisateur
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(depensePapier.utilisateur().editingContext(), depensePapier.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020_a_utl_ordre");

		return dico;
	}
}