/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureModifierEngagement extends Procedure {

	private static final String PROCEDURE_NAME = "updEngage";

	/**
	 * Appele la procedure de modification de l'engagement
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param commande
	 *        EOCommande qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOEngagementBudget engageBudget) throws NSValidation.ValidationException{

		// on verifie la validite des objets a enregistrer 
		engageBudget.validateForSave();

		for (int i=0; i<engageBudget.engagementControleActions().count(); i++)
			((EOEngagementControleAction)engageBudget.engagementControleActions().objectAtIndex(i)).validateForSave();
		for (int i=0; i<engageBudget.engagementControleAnalytiques().count(); i++)
			((EOEngagementControleAnalytique)engageBudget.engagementControleAnalytiques().objectAtIndex(i)).validateForSave();
		for (int i=0; i<engageBudget.engagementControleConventions().count(); i++)
			((EOEngagementControleConvention)engageBudget.engagementControleConventions().objectAtIndex(i)).validateForSave();
		for (int i=0; i<engageBudget.engagementControleHorsMarches().count(); i++)
			((EOEngagementControleHorsMarche)engageBudget.engagementControleHorsMarches().objectAtIndex(i)).validateForSave();
		for (int i=0; i<engageBudget.engagementControleMarches().count(); i++)
			((EOEngagementControleMarche)engageBudget.engagementControleMarches().objectAtIndex(i)).validateForSave();
		for (int i=0; i<engageBudget.engagementControlePlanComptables().count(); i++)
			((EOEngagementControlePlanComptable)engageBudget.engagementControlePlanComptables().objectAtIndex(i)).validateForSave();
			
		// lancement de la procedure				
		return dataBus.executeProcedure(ProcedureModifierEngagement.PROCEDURE_NAME, 
				ProcedureModifierEngagement.construireDictionnaire(engageBudget));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'engagement
	 * <BR>
	 * @param commande
	 *        EOCommande pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOEngagementBudget engagement) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on cherche la cle de l'engagement
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(engagement.editingContext(), engagement);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOEngagementBudget.ENG_ID_KEY), "010_a_eng_id");

		// on renseigne les montants
		dico.takeValueForKey(engagement.engHtSaisie(), "020_a_eng_ht_saisie");
		dico.takeValueForKey(engagement.engTtcSaisie(), "030_a_eng_ttc_saisie");

		// on cherche la cle de l'utilisateur
        dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(engagement.utilisateur().editingContext(), engagement.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "040_a_utl_ordre");

		// on construit la chaine pour les engagementCtrlAction
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineAction(engagement), "050_a_chaine_action");
		
		// on construit la chaine pour les engagementCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineAnalytique(engagement), "060_a_chaine_analytique");

		// on construit la chaine pour les engagementCtrlConvention
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineConvention(engagement), "065_a_chaine_convention");

		// on construit la chaine pour les engagementCtrlHorsMarche
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineHorsMarche(engagement), "070_a_chaine_hors_marche");

		// on construit la chaine pour les engagementCtrlMarche
		dico.takeValueForKey(ProcedureCreerEngagement.construireChaineMarche(engagement), "080_a_chaine_marche");
		
		// on construit la chaine pour les engagementCtrlPlanco
		dico.takeValueForKey(ProcedureCreerEngagement.construireChainePlanComptable(engagement), "090_a_chaine_planco");

		return dico;
	}
}