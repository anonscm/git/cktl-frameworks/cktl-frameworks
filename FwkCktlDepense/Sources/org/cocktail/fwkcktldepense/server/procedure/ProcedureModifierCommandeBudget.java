/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.procedure;

import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureModifierCommandeBudget extends Procedure {

	private static final String PROCEDURE_NAME = "updCommandeBudget";

	/**
	 * Appele la procedure de modification de la commande budget
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param commandeBudget
	 *        EOCommandeBudget qui sera enregistre
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOCommandeBudget commandeBudget) 
	    throws NSValidation.ValidationException{

		// on verifie la validite des objets a enregistrer 
		
    	NSArray larray=commandeBudget.commandeControleActions();
    	
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleAction ctrl=(EOCommandeControleAction)larray.objectAtIndex(i);
    			if (ctrl.typeAction()==null)
    				new FactoryCommandeControleAction().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}

    	larray=commandeBudget.commandeControleAnalytiques();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleAnalytique ctrl=(EOCommandeControleAnalytique)larray.objectAtIndex(i);
    			if (ctrl.codeAnalytique()==null)
    				new FactoryCommandeControleAnalytique().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}
    	
    	larray=commandeBudget.commandeControleConventions();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
			EOCommandeControleConvention ctrl=(EOCommandeControleConvention)larray.objectAtIndex(i);
			if (ctrl.convention()==null)
				new FactoryCommandeControleConvention().supprimer(ctrl.editingContext(), ctrl);
			else
				ctrl.validateForSave();
		}
    	}
    	
    	larray=commandeBudget.commandeControleHorsMarches();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleHorsMarche ctrl=(EOCommandeControleHorsMarche)larray.objectAtIndex(i);
    			ctrl.validateForSave();
    		}
    	}
    	
    	larray=commandeBudget.commandeControleMarches();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControleMarche ctrl=(EOCommandeControleMarche)larray.objectAtIndex(i);
    			ctrl.validateForSave();
    		}
    	}
    	
    	larray=commandeBudget.commandeControlePlanComptables();
    	if (larray!=null && larray.count()>0) {
    		for (int i=larray.count()-1; i>=0; i--) {
    			EOCommandeControlePlanComptable ctrl=(EOCommandeControlePlanComptable)larray.objectAtIndex(i);
    			if (ctrl.planComptable()==null)
    				new FactoryCommandeControlePlanComptable().supprimer(ctrl.editingContext(), ctrl);
    			else
    				ctrl.validateForSave();
    		}
    	}
    	
		commandeBudget.validateForSave();

		// lancement de la procedure		
	    return dataBus.executeProcedure(ProcedureModifierCommandeBudget.PROCEDURE_NAME, 
	    		ProcedureModifierCommandeBudget.construireDictionnaire(commandeBudget));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de la commande budget
	 * <BR>
	 * @param commandeBudget
	 *        EOCommandeBudget pour lequel construire le dictionnaire
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOCommandeBudget commandeBudget) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// on cherche la cle
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.editingContext(), commandeBudget);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCommandeBudget.CBUD_ID_KEY), "010_a_cbud_id");

		// on renseigne les montants
		dico.takeValueForKey(commandeBudget.cbudHtSaisie(), "070_a_cbud_ht_saisie");
		dico.takeValueForKey(commandeBudget.cbudTtcSaisie(), "080_a_cbud_ttc_saisie");

		// on renseigne le taux de prorata
		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(commandeBudget.tauxProrata().editingContext(), commandeBudget.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "085_a_tap_id");

		// on construit la chaine pour les articleCtrlAction
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineAction(commandeBudget), "090_a_chaine_action");
		
		// on construit la chaine pour les articleCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineAnalytique(commandeBudget), "100_a_chaine_analytique");

		// on construit la chaine pour les articleCtrlAnalytique
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineConvention(commandeBudget), "110_a_chaine_convention");

		// on construit la chaine pour les commandeCtrlHorsMarche
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineHorsMarche(commandeBudget), "114_a_chaine_hors_marche");

		// on construit la chaine pour les commandeCtrlMarche
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChaineMarche(commandeBudget), "117_a_chaine_marche");

		// on construit la chaine pour les articleCtrlPlanco
		dico.takeValueForKey(ProcedureCreerCommandeBudget.construireChainePlanComptable(commandeBudget), "120_a_chaine_planco");

		return dico;
	}
}