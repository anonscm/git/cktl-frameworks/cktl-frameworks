/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.service;

import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOParametre;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;

public class ConfigurationExtourneService {

	public static final String EXTOURNE_ENVELOPPE_NIVEAU_SELECTION_KEY = "org.cocktail.gfc.depense.extourne.enveloppe.niveau_selection";

	public static final String EXTOURNE_ENVELOPPE_AUTORISEE_TCD_PREFIX_KEY = "org.cocktail.gfc.depense.extourne.enveloppe.autorisee_sur_tcd_";
	public static final String EXTOURNE_ENVELOPPE_AUTORISEE_TCD1_KEY = "org.cocktail.gfc.depense.extourne.enveloppe.autorisee_sur_tcd_1";
	public static final String EXTOURNE_ENVELOPPE_AUTORISEE_TCD2_KEY = "org.cocktail.gfc.depense.extourne.enveloppe.autorisee_sur_tcd_2";
	public static final String EXTOURNE_ENVELOPPE_AUTORISEE_TCD3_KEY = "org.cocktail.gfc.depense.extourne.enveloppe.autorisee_sur_tcd_3";

	public static final String EXTOURNE_FLECHEE_AUTORISEE_TCD_PREFIX_KEY = "org.cocktail.gfc.depense.extourne.flechee.autorisee_sur_tcd_";
	public static final String EXTOURNE_FLECHEE_AUTORISEE_TCD1_KEY = "org.cocktail.gfc.depense.extourne.flechee.autorisee_sur_tcd_1";
	public static final String EXTOURNE_FLECHEE_AUTORISEE_TCD2_KEY = "org.cocktail.gfc.depense.extourne.flechee.autorisee_sur_tcd_2";
	public static final String EXTOURNE_FLECHEE_AUTORISEE_TCD3_KEY = "org.cocktail.gfc.depense.extourne.flechee.autorisee_sur_tcd_3";

	private static ConfigurationExtourneService INSTANCE = new ConfigurationExtourneService();

	public static final ConfigurationExtourneService instance() {
		return INSTANCE;
	}

	public static void setConfigurationExtourneService(ConfigurationExtourneService service) {
		INSTANCE = service;
	}

	/**
	 * @param exercice
	 * @return UB ou CR
	 */
	public Integer extourneEnveloppeNiveauSelection(EOEditingContext edc, EOExercice exercice) {
		// si l'exercice est null, on renvoit UB par defaut.
		if (exercice == null) {
			return EOOrgan.ORG_NIV_2;
		}

		//TODO ameliorer pour eviter de systematiquement recharger cette infos. La stocker par exercice dans la session.
		EOParametre param = FinderParametre.getParametre(edc, EXTOURNE_ENVELOPPE_NIVEAU_SELECTION_KEY, exercice);
		Integer niveauOrgan = null;
		String niveauSelectionAsString = param.parValue();
		if (niveauSelectionAsString != null) {
			niveauSelectionAsString = niveauSelectionAsString.trim();
		}
		if (niveauSelectionAsString != null && EOOrgan.LIB_NIV_MAP.containsKey(niveauSelectionAsString)) {
			niveauOrgan = EOOrgan.LIB_NIV_MAP.get(niveauSelectionAsString);
		}
		return niveauOrgan;
	}

	//TODO ameliorer pour eviter de systematiquement recharger cette infos. La stocker par exercice dans la session.
	public boolean extourneEnveloppeTypeCreditAutorise(EOEditingContext edc, EOExercice exercice, EOTypeCredit.ESection section) {
		if (section == null || exercice == null) {
			return false;
		}

		EOParametre paramTypeCredit = FinderParametre.getParametre(
				edc, EXTOURNE_ENVELOPPE_AUTORISEE_TCD_PREFIX_KEY + section.section(), exercice);
		return extourneTypeCreditAutorise(paramTypeCredit);
	}

	public boolean extourneFlecheeTypeCreditAutorise(EOEditingContext edc, EOExercice exercice, EOTypeCredit.ESection section) {
		if (section == null || exercice == null) {
			return false;
		}

		EOParametre paramTypeCredit = FinderParametre.getParametre(
				edc, EXTOURNE_FLECHEE_AUTORISEE_TCD_PREFIX_KEY + section.section(), exercice);
		return extourneTypeCreditAutorise(paramTypeCredit);
	}

	private boolean extourneTypeCreditAutorise(EOParametre paramTypeCredit) {
		boolean typeCreditAutorise = false;
		String valeurTypeCredit = paramTypeCredit.parValue();
		if (valeurTypeCredit != null) {
			valeurTypeCredit = valeurTypeCredit.trim();
			if ("O".equalsIgnoreCase(valeurTypeCredit) || "true".equalsIgnoreCase(valeurTypeCredit)) {
				typeCreditAutorise = true;
			}
		}
		return typeCreditAutorise;
	}

}
