package org.cocktail.fwkcktldepense.server.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.interfaces.IFactoryControle;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.repartitions.AbstractRepartition;
import org.cocktail.fwkcktldepense.server.repartitions.IRepartition;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionAction;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionCodeAnalytique;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionConvention;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionHorsMarche;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionMarche;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionPlanComptable;

import com.webobjects.foundation.NSArray;

public class DepenseService {
	public static final String REPARTITION_ANALYTIQUE = "codeAnalytique";
	public static final String REPARTITION_CONVENTION = "convention";
	public static final String REPARTITION_MARCHE = "attribution";
	public static final String REPARTITION_PLAN_COMPTABLE = "planComptable";
	public static final String REPARTITION_ACTION = "typeAction";
	public static final String REPARTITION_HORS_MARCHE = "typeAchat";
	
	private final static Map<String, IFactoryControle> FACTORIES = new HashMap<String, IFactoryControle>();
	private final static Map<String, IRepartition> GENERATEURS_REPARTITIONS = new HashMap<String, IRepartition>();
	private final static List<String> LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE = new ArrayList<String>();
	private final static List<String> LISTE_REPARTITION_MANDAT_EXTOURNE = new ArrayList<String>();

	static {
		FACTORIES.put(REPARTITION_ANALYTIQUE, new FactoryDepenseControleAnalytique());
		FACTORIES.put(REPARTITION_CONVENTION, new FactoryDepenseControleConvention());
		FACTORIES.put(REPARTITION_MARCHE, new FactoryDepenseControleMarche());
		FACTORIES.put(REPARTITION_PLAN_COMPTABLE, new FactoryDepenseControlePlanComptable());
		FACTORIES.put(REPARTITION_ACTION, new FactoryDepenseControleAction());

		GENERATEURS_REPARTITIONS.put(REPARTITION_ANALYTIQUE, new RepartitionCodeAnalytique());
		GENERATEURS_REPARTITIONS.put(REPARTITION_CONVENTION, new RepartitionConvention());
		GENERATEURS_REPARTITIONS.put(REPARTITION_MARCHE, new RepartitionMarche());
		GENERATEURS_REPARTITIONS.put(REPARTITION_PLAN_COMPTABLE, new RepartitionPlanComptable());
		GENERATEURS_REPARTITIONS.put(REPARTITION_ACTION, new RepartitionAction());
		GENERATEURS_REPARTITIONS.put(REPARTITION_HORS_MARCHE, new RepartitionHorsMarche());

		LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE.add("typeAction");
		LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE.add("typeAchat");
		LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE.add("codeAnalytique");
		LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE.add("convention");
		LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE.add("attribution");
		LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE.add("planComptable");
		
		LISTE_REPARTITION_MANDAT_EXTOURNE.add("codeAnalytique");
		LISTE_REPARTITION_MANDAT_EXTOURNE.add("convention");
		LISTE_REPARTITION_MANDAT_EXTOURNE.add("attribution");
		LISTE_REPARTITION_MANDAT_EXTOURNE.add("planComptable");


	}
	
	/**
	 * Retourne la liste des répartitions dans le cadre d'un engagement budgétaire (reversement ou dépense).
	 * @return
	 */
	public List<String> getListeRepartitionsEngagementBudgetaire() {
		return LISTE_REPARTITION_ENGAGEMENT_BUDGETAIRE;
	}

	/**
	 * Retourne la liste des répartitions dans le cadre d'un mandat d'extourne.
	 * @return
	 */
	public List<String> getListeRepartitionsMandatExtourne() {
		return LISTE_REPARTITION_MANDAT_EXTOURNE;
	}

	/** 
	 * Retourne une factory d'élément de répartition en fonction du type de répartition demandée.
	 * @param typeRepartition
	 * @return Une factory
	 */
	public IFactoryControle getFactoryDepenseControle(String typeRepartition) throws FactoryException {
		if (!FACTORIES.containsKey(typeRepartition)) throw new FactoryException(
				"Cette répartion n'a pas de factory :" + typeRepartition);
		return FACTORIES.get(typeRepartition);
	}

	/** 
	 * Retourne un générateur de répartition  d'une dépense en fonction du type de répartition demandée.
	 * @param typeRepartition
	 * @return Un générateur
	 */
	public IRepartition getGenerateurRepartition(String typeRepartition) {
		if (!GENERATEURS_REPARTITIONS.containsKey(typeRepartition)) throw new FactoryException(
				"Cette répartition est inconnue :" + typeRepartition);
		return GENERATEURS_REPARTITIONS.get(typeRepartition);
	}

	/**
	 * Indique si la dépense doit être émargé en fonction du mode de paiement.
	 * 
	 * @param depensePapier
	 * @return
	 */
	public boolean isEmargementObligatoire(EODepensePapier depensePapier) {
		if (depensePapier == null || depensePapier.modePaiement() == null)
			return false;
		return depensePapier.modePaiement().isEmargementObligatoire();
	}

	/**
	 * Somme un champ d'une liste d'EntityObject
	 * @param entites
	 * @param key
	 * @return
	 */
	public BigDecimal computeSumForKey(NSArray entites, String key) {
		if (entites == null || entites.count() == 0)
			return BigDecimal.ZERO;
		return (BigDecimal) entites.valueForKeyPath("@sum." + key);
	}


}
