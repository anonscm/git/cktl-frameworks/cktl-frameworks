package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;

import com.webobjects.foundation.NSArray;

public class RepartitionCodeAnalytique extends AbstractRepartition {

	@Override
	public boolean isControleGood(IDepenseBudget depenseBudget) {
		return isRepartionValide(depenseBudget);
	}

	@Override
	public void valider(EODepenseBudget depenseBudget) throws DepenseBudgetException {
		//Dans le cas d'une reimputation l'organ a pu changer, il ne faut pas se baser sur l'organ de l'engagement
		if (depenseBudget.engagementBudget() != null && depenseBudget.getSource().organ() != null && depenseBudget.getSource().organ().isCodeAnalytiqueObligatoire()) {
			if (depenseBudget.depenseControleAnalytiques() == null || depenseBudget.depenseControleAnalytiques().count() == 0)
				throw new DepenseBudgetException(depenseBudget.getSource().organ().sourceLibelle() + " : " + DepenseBudgetException.codeAnalytiqueManquant);

			if (depenseBudget.depHtSaisie().abs().compareTo(serviceDepense.computeSumForKey(depenseBudget.depenseControleAnalytiques(),
					EODepenseControleAnalytique.DANA_HT_SAISIE_KEY).abs()) != 0 ||
					depenseBudget.depTvaSaisie().abs().compareTo(serviceDepense.computeSumForKey(depenseBudget.depenseControleAnalytiques(),
							EODepenseControleAnalytique.DANA_TVA_SAISIE_KEY).abs()) != 0 ||
					depenseBudget.depTtcSaisie().abs().compareTo(serviceDepense.computeSumForKey(depenseBudget.depenseControleAnalytiques(),
							EODepenseControleAnalytique.DANA_TTC_SAISIE_KEY).abs()) != 0)
				throw new DepenseBudgetException(DepenseBudgetException.montantsOrganAnalytiqueIncoherent);
		}
		else {
			if (depenseBudget.depHtSaisie().abs().compareTo(serviceDepense.computeSumForKey(depenseBudget.depenseControleAnalytiques(),
					EODepenseControleAnalytique.DANA_HT_SAISIE_KEY).abs()) == -1 ||
					depenseBudget.depTvaSaisie().abs().compareTo(serviceDepense.computeSumForKey(depenseBudget.depenseControleAnalytiques(),
							EODepenseControleAnalytique.DANA_TVA_SAISIE_KEY).abs()) == -1 ||
					depenseBudget.depTtcSaisie().abs().compareTo(serviceDepense.computeSumForKey(depenseBudget.depenseControleAnalytiques(),
							EODepenseControleAnalytique.DANA_TTC_SAISIE_KEY).abs()) == -1)
				throw new DepenseBudgetException(DepenseBudgetException.montantsDepenseControleAnalytiqueIncoherent);
		}
	}
	
	@Override
	protected NSArray getRepartition(ISourceRepartitionCredit source) {
		return source.repartitionPourcentageAnalytiques();
	};

	@Override
	protected String getQualifierCondition() {
		throw new RuntimeException("Il n'y à pas de qualifier pour la verification de la réparatition analytique");
	}

	@Override
	protected String getQualifierReversement() {
		return EODepenseControleAnalytique.CODE_ANALYTIQUE_KEY + "=%@";
	}
	
	@Override
	protected String getPourcentageKey() {
		return EODepenseControleAnalytique.DANA_POURCENTAGE;
	}

	@Override
	protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
		return depenseBudget.depenseControleAnalytiques();
	}
	
	@Override
	protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
		return depenseBudget.restantHtControleAnalytique();
	}

	@Override
	protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
		return depenseBudget.restantTtcControleAnalytique();
	}

}
