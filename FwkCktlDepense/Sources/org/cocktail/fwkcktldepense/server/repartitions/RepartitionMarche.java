package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;

import com.webobjects.foundation.NSArray;

public class RepartitionMarche extends AbstractRepartition {
	
	
	@Override
	protected NSArray getRepartition(ISourceRepartitionCredit source) {
		return source.repartitionPourcentageMarches();
	};

	@Override
	protected String getQualifierCondition() {
		return EODepenseControleMarche.ATTRIBUTION_KEY + "=nil";
	}

	@Override
	protected String getQualifierReversement() {
		throw new RuntimeException("Il n'y à pas de qualifier pour les reversements de la répartition marché");
	}

	@Override
	protected String getPourcentageKey() {
		return EODepenseControleMarche.DMAR_POURCENTAGE;
	}
	
	@Override
	protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
		return depenseBudget.depenseControleMarches();
	}
	
	@Override
	protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
		return depenseBudget.restantHtControleMarche();
	}

	@Override
	protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
		return depenseBudget.restantTtcControleMarche();
	}

	@Override
	public void valider(EODepenseBudget depenseBudget)
			throws DepenseBudgetException {
		if (getRepartitionDepense(depenseBudget)!= null && getRepartitionDepense(depenseBudget).count() > 0) {

			if (depenseBudget.depHtSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleMarche.DMAR_HT_SAISIE_KEY)) != 0 ||
					depenseBudget.depTvaSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleMarche.DMAR_TVA_SAISIE_KEY)) != 0 ||
							depenseBudget.depTtcSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleMarche.DMAR_TTC_SAISIE_KEY)) != 0)
				throw new DepenseBudgetException(DepenseBudgetException.montantsDepenseControleMarcheIncoherent);
		}

	}
}
