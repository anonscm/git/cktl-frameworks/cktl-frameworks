package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class RepartitionHorsMarche extends AbstractRepartition {
	
	@Override
	public NSArray repartitionPourcentageAvecDernier(IDepenseBudget depenseBudget, BigDecimal maxReversement) {
		if (depenseBudget.depenseBudgetReversement() == null)
			return new NSMutableArray();

		NSArray repartition = getRepartitionDepenseReversement(depenseBudget);
		NSMutableArray array = new NSMutableArray();

		if (repartition == null || repartition.count() == 0)
			return array;

		BigDecimal resteAReverser = new BigDecimal(maxReversement.floatValue());
		if (resteAReverser.floatValue() == 0.0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < repartition.count(); i++) {
			IDepenseControle ctrl = (IDepenseControle) repartition.objectAtIndex(i);

			BigDecimal resteCtrl = ctrl.getMontantTTC().add(ctrl.getSommeRepartition());

			if (resteCtrl.floatValue() == 0)
				continue;

			boolean dernier = (i == repartition.count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = resteCtrl.divide(resteAReverser, 7, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.getCode(), ((EODepenseControleHorsMarche) ctrl).typeAchat(), resteCtrl
			}),
					new NSArray(new Object[] {
							"pourcentage", ctrl.getLabelCode(), "typeAchat", "max"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	@Override
	public void genererDepensesControle(EOEditingContext context, EODepenseBudget depenseBudget, 
			ISourceRepartitionCredit source, String key, EOExercice exercice, boolean plafondReversement) {
		
		NSArray depenses = getRepartition(source);
		for (int j = 0; j < depenses.count(); j++) {
			NSDictionary depense = (NSDictionary) depenses.objectAtIndex(j);
			
			IDepenseControle depenseControle = getNouveauDepenseControle(
					context, depenseBudget, exercice, key, depense);
			
			if(plafondReversement) {
				ajoutePlafondReversement(depense, depenseControle);
			}
		}
	}

	protected EODepenseControleHorsMarche getNouveauDepenseControle(
			EOEditingContext context, EODepenseBudget depenseBudget,
			EOExercice exercice, String key, NSDictionary elementsDeDepense) {
		
		BigDecimal pourcentage = getPourcentage(elementsDeDepense);
		EOCodeExer codeExercice = getCodeExercice(elementsDeDepense);
		EOTypeAchat typeAchat = getTypeAchat(key, elementsDeDepense);
		
		return getFactoryDepenseControle().creer(
				context, new BigDecimal(0.0), new BigDecimal(0.0),
				new BigDecimal(0.0), new BigDecimal(0.0), pourcentage,
				codeExercice, typeAchat, exercice, depenseBudget);
	}

	protected EOTypeAchat getTypeAchat(String key,
			NSDictionary elementsDeDepense) {
		return (EOTypeAchat) elementsDeDepense.objectForKey(key);
	}

	protected EOCodeExer getCodeExercice(NSDictionary elementsDeDepense) {
		return (EOCodeExer) elementsDeDepense.objectForKey("codeExer");
	}

	protected BigDecimal getPourcentage(NSDictionary elementsDeDepense) {
		return (BigDecimal) elementsDeDepense.objectForKey("pourcentage");
	}

	protected FactoryDepenseControleHorsMarche getFactoryDepenseControle() {
		return new FactoryDepenseControleHorsMarche();
	}

	@Override
	public boolean isControleGood(IDepenseBudget depenseBudget) {

		if (depenseBudget.isSurExtourne()) {
			return true;
		}
		
		return super.isControleGood(depenseBudget);
	}
	
	@Override
	protected NSArray getRepartition(ISourceRepartitionCredit source) {
		return source.repartitionPourcentageHorsMarches();
	};

	@Override
	protected String getQualifierCondition() {
		return EODepenseControleHorsMarche.CODE_EXER_KEY + "=nil";
	}

	@Override
	protected String getQualifierReversement() {
		return EODepenseControleHorsMarche.CODE_EXER_KEY + "=%@";
	}
	
	@Override
	protected String getPourcentageKey() {
		return EODepenseControleHorsMarche.DHOM_POURCENTAGE;
	}
	
	@Override
	protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
		return depenseBudget.depenseControleHorsMarches();
	}
	
	@Override
	protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
		return depenseBudget.restantHtControleHorsMarche();
	}

	@Override
	protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
		return depenseBudget.restantTtcControleHorsMarche();
	}

	@Override
	public void valider(EODepenseBudget depenseBudget)
			throws DepenseBudgetException {
		if (getRepartitionDepense(depenseBudget) != null && getRepartitionDepense(depenseBudget).count() > 0) {

			if (depenseBudget.depHtSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget),
					EODepenseControleHorsMarche.DHOM_HT_SAISIE_KEY)) != 0 ||
							depenseBudget.depTvaSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget),
							EODepenseControleHorsMarche.DHOM_TVA_SAISIE_KEY)) != 0 ||
					depenseBudget.depTtcSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget),
							EODepenseControleHorsMarche.DHOM_TTC_SAISIE_KEY)) != 0)
				throw new DepenseBudgetException(DepenseBudgetException.montantsDepenseControleHorsMarcheIncoherent);

			if (depenseBudget.depenseControleMarches() != null && depenseBudget.depenseControleMarches().count() > 0)
				throw new DepenseBudgetException(DepenseBudgetException.controleMarcheHorsMarcheIncoherent);
		}
	}

}
