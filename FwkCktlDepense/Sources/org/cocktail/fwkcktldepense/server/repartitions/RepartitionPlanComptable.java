package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class RepartitionPlanComptable extends AbstractRepartition {

	@Override
	public void valider(EODepenseBudget depenseBudget)
			throws DepenseBudgetException {
		if (getRepartitionDepense(depenseBudget) != null && getRepartitionDepense(depenseBudget).count() != 1)
			throw new DepenseBudgetException(DepenseBudgetException.depenseControlePlanComptableUnique);

		
	}

	@Override
	protected void ajoutePlafondReversement(NSDictionary dico,
			IDepenseControle ctrl) {
		ctrl.setMaxReversement((BigDecimal) dico.objectForKey("max"));
		((EODepenseControlePlanComptable) ctrl).ajouterInventairesReversement(
				((EODepenseControlePlanComptable) ctrl).getInventairesPossibles());
	}

	@Override
	protected boolean postControle(IDepenseBudget depenseBudget) {
		NSArray<EODepenseControlePlanComptable> repartitionDepense = getRepartitionDepense(depenseBudget);
		for (int i = 0; i < repartitionDepense.count(); i++)
			if (!((EODepenseControlePlanComptable) repartitionDepense.objectAtIndex(i)).isGood())
				return false;
		return true;
	}

	@Override
	protected NSArray getRepartition(ISourceRepartitionCredit source) {
		return source.repartitionPourcentagePlanComptables();
	};


	@Override
	protected String getQualifierCondition() {
		return EODepenseControlePlanComptable.PLAN_COMPTABLE_KEY + "=nil";
	}

	@Override
	protected String getQualifierReversement() {
		throw new RuntimeException("Il n'y à pas de qualifier pour les reversements de la répartition par plan comptable");
	}

	@Override
	protected String getPourcentageKey() {
		return EODepenseControlePlanComptable.DPCO_POURCENTAGE;
	}

	@Override
	protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
		return depenseBudget.depenseControlePlanComptables();
	}
	
	@Override
	protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
		return depenseBudget.restantHtControlePlanComptable();
	}

	@Override
	protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
		return depenseBudget.restantTtcControlePlanComptable();
	}

}
