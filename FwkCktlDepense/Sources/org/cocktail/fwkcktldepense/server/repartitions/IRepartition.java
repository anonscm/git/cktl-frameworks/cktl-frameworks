package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public interface IRepartition {

	public NSArray repartitionPourcentage(IDepenseBudget depenseBudget, BigDecimal maxReversement);
	public NSArray repartitionPourcentageAvecDernier(IDepenseBudget depenseBudget, BigDecimal maxReversement);
	
	public void corrigerMontants(IDepenseBudget depenseBudget);
	public void corrigerMontantsComplexe(IDepenseBudget depenseBudget);
	
	public  void genererDepensesControle(EOEditingContext context, EODepenseBudget depenseBudget, ISourceRepartitionCredit source, String key,
			EOExercice exercice, boolean plafondReversement);
	
	public boolean isControleGood(IDepenseBudget depenseBudget);
	
	public void valider(EODepenseBudget depenseBudget) throws DepenseBudgetException;


}
