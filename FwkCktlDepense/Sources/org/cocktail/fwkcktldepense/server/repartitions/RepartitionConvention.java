package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;

import com.webobjects.foundation.NSArray;

public class RepartitionConvention extends AbstractRepartition {

    @Override
    public boolean isControleGood(IDepenseBudget depenseBudget) {
        return isRepartionValide(depenseBudget);
    }

    @Override
    public void valider(EODepenseBudget depenseBudget) throws DepenseBudgetException {
        validateBeforeTransactionSavePourConvention(depenseBudget);
    }

    protected void validateBeforeTransactionSavePourConvention(EODepenseBudget depenseBudget) throws DepenseBudgetException {

        if (depenseBudget.getSource() == null || depenseBudget.getSource().organ() == null) {
            return;
        }

        if (depenseBudget.getSource().organ().isConventionObligatoire()) {
            validateBeforeTransactionSaveQuandConventionObligatoire(depenseBudget);
        } else {
            validateBeforeTransactionSaveQuandConventionNonObligatoire(depenseBudget);
        }
    }

    protected void validateBeforeTransactionSaveQuandConventionObligatoire(EODepenseBudget depenseBudget) throws DepenseBudgetException {
        if (depenseBudget.getSource().organ() == null || !depenseBudget.getSource().organ().isConventionObligatoire()) {
            return;
        }

        if (conventionsNonRenseignees(depenseBudget)) {
            throw new DepenseBudgetException(DepenseBudgetException.CONVENTION_MANQUANTE);
        }

        if (montantsDepenseDifferentsDesMontantsConventions(depenseBudget)) {
            throw new DepenseBudgetException(DepenseBudgetException.montantsDepenseControleConventionIncoherent);
        }
    }

    protected void validateBeforeTransactionSaveQuandConventionNonObligatoire(EODepenseBudget depenseBudget) throws DepenseBudgetException {
        if (conventionsNonRenseignees(depenseBudget)) {
            return;
        }

        if (montantsDepenseInferieursAuxMontantsConventions(depenseBudget)) {
            throw new DepenseBudgetException(DepenseBudgetException.montantsDepenseControleConventionIncoherent);
        }
    }

    protected boolean conventionsNonRenseignees(EODepenseBudget depenseBudget) {
        return getRepartitionDepense(depenseBudget) == null || getRepartitionDepense(depenseBudget).count() == 0;
    }

    protected boolean montantsDepenseDifferentsDesMontantsConventions(EODepenseBudget depenseBudget) {
        return depenseBudget
                .depHtSaisie()
                .abs()
                .compareTo(
                        serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleConvention.DCON_HT_SAISIE_KEY).abs()) != 0
                || depenseBudget
                        .depTvaSaisie()
                        .abs()
                        .compareTo(
                                serviceDepense
                                        .computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleConvention.DCON_TVA_SAISIE_KEY)
                                        .abs()) != 0
                || depenseBudget
                        .depTtcSaisie()
                        .abs()
                        .compareTo(
                                serviceDepense
                                        .computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleConvention.DCON_TTC_SAISIE_KEY)
                                        .abs()) != 0;
    }

    protected boolean montantsDepenseInferieursAuxMontantsConventions(EODepenseBudget depenseBudget) {
        return depenseBudget
                .depHtSaisie()
                .abs()
                .compareTo(
                        serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleConvention.DCON_HT_SAISIE_KEY).abs()) == -1
                || depenseBudget
                        .depTvaSaisie()
                        .abs()
                        .compareTo(
                                serviceDepense
                                        .computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleConvention.DCON_TVA_SAISIE_KEY)
                                        .abs()) == -1
                || depenseBudget
                        .depTtcSaisie()
                        .abs()
                        .compareTo(
                                serviceDepense
                                        .computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleConvention.DCON_TTC_SAISIE_KEY)
                                        .abs()) == -1;
    }

    @Override
    protected NSArray getRepartition(ISourceRepartitionCredit source) {
        return source.repartitionPourcentageConventions();
    };

    @Override
    protected String getQualifierCondition() {
        throw new RuntimeException("Il n'y à pas de qualifier pour la verification de la réparatition par convention");
    }

    @Override
    protected String getQualifierReversement() {
        return EODepenseControleConvention.CONVENTION_KEY + "=%@";
    }

    @Override
    protected String getPourcentageKey() {
        return EODepenseControleConvention.DCON_POURCENTAGE;
    }

    @Override
    protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
        return depenseBudget.depenseControleConventions();
    }

    @Override
    protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
        return depenseBudget.restantHtControleConvention();
    }

    @Override
    protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
        return depenseBudget.restantTtcControleConvention();
    }

}
