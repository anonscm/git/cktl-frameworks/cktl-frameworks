package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class RepartitionAction extends AbstractRepartition {

	@Override
	public boolean isControleGood(IDepenseBudget depenseBudget) {
		if (depenseBudget.isSurExtourne()) {
			return true;
		}

		return super.isControleGood(depenseBudget);
	}

	@Override
	public void valider(EODepenseBudget depenseBudget) throws DepenseBudgetException {
		if (depenseBudget.depenseControleActions().count() > 0) {
			if (depenseBudget.depHtSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleAction.DACT_HT_SAISIE_KEY)) != 0 ||
					depenseBudget.depTvaSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleAction.DACT_TVA_SAISIE_KEY)) != 0 ||
					depenseBudget.depTtcSaisie().compareTo(serviceDepense.computeSumForKey(getRepartitionDepense(depenseBudget), EODepenseControleAction.DACT_TTC_SAISIE_KEY)) != 0)
				throw new DepenseBudgetException(DepenseBudgetException.montantsDepenseControleActionIncoherent);
		}

	}
	
	@Override
	protected NSArray getRepartition(ISourceRepartitionCredit source) {
		return source.repartitionPourcentageActions();
	};

	@Override
	protected String getQualifierCondition() {
		return EODepenseControleAction.TYPE_ACTION_KEY + "=nil";
	}

	@Override
	protected String getQualifierReversement() {
		return EODepenseControleAction.TYPE_ACTION_KEY + "=%@";
	}
	
	@Override
	protected String getPourcentageKey() {
		return EODepenseControleAction.DACT_POURCENTAGE;
	}

	@Override
	protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
		return depenseBudget.depenseControleActions();
	}

	@Override
	protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
		return depenseBudget.restantHtControleAction();
	}

	@Override
	protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
		return depenseBudget.restantTtcControleAction();
	}

}
