package org.cocktail.fwkcktldepense.server.repartitions;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseControle;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.interfaces.NullDepenseControle;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.service.DepenseService;
import org.cocktail.fwkcktldepense.server.util.Calculs;
import org.cocktail.fwkcktldepense.server.util.MontantTransaction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public abstract class AbstractRepartition implements IRepartition {
	
	protected static DepenseService serviceDepense = new DepenseService();
	
	public NSArray repartitionPourcentage(IDepenseBudget depenseBudget, BigDecimal maxReversement) {
		if (depenseBudget.depenseBudgetReversement() == null)
			return new NSMutableArray();

		NSArray repartition = getRepartitionDepenseReversement(depenseBudget);
		NSMutableArray array = new NSMutableArray();

		if (repartition == null || repartition.count() == 0)
			return array;

		BigDecimal resteAReverser = new BigDecimal(maxReversement.floatValue());
		if (resteAReverser.floatValue() == 0.0)
			return array;

		for (int i = 0; i < repartition.count(); i++) {
			IDepenseControle ctrl = (IDepenseControle) repartition.objectAtIndex(i);

			BigDecimal resteCtrl = ctrl.getMontantTTC().add(ctrl.getSommeRepartition());

			if (resteCtrl.floatValue() == 0)
				continue;

			BigDecimal pourcentage = resteCtrl.divide(resteAReverser, 7, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.0));

			array.addObject(
					new NSDictionary(
							new NSArray(
									new Object[] {
											pourcentage, ctrl.getCode(), resteCtrl
									}
							),
							new NSArray(
									new Object[] {
											"pourcentage", ctrl.getLabelCode(), "max"
									}
							)
					)
			);
		}

		return array;
	}

	public NSArray repartitionPourcentageAvecDernier(IDepenseBudget depenseBudget, BigDecimal maxReversement) {
		if (depenseBudget.depenseBudgetReversement() == null)
			return new NSMutableArray();

		NSArray repartition = getRepartitionDepenseReversement(depenseBudget);
		NSMutableArray array = new NSMutableArray();

		if (repartition == null || repartition.count() == 0)
			return array;

		BigDecimal resteAReverser = new BigDecimal(maxReversement.floatValue());
		if (resteAReverser.floatValue() == 0.0)
			return array;

		BigDecimal pourcentageRestant = new BigDecimal(100.0);

		for (int i = 0; i < repartition.count(); i++) {
			IDepenseControle ctrl = (IDepenseControle) repartition.objectAtIndex(i);

			BigDecimal resteCtrl = ctrl.getMontantTTC().add(ctrl.getSommeRepartition());

			if (resteCtrl.floatValue() == 0)
				continue;

			boolean dernier = (i == repartition.count() - 1);
			BigDecimal pourcentage;

			if (dernier)
				pourcentage = pourcentageRestant;
			else
				pourcentage = resteCtrl.divide(resteAReverser, 7, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100.0));

			if (pourcentage.floatValue() > pourcentageRestant.floatValue())
				pourcentage = pourcentageRestant;

			array.addObject(new NSDictionary(new NSArray(new Object[] {
					pourcentage, ctrl.getCode(), resteCtrl
			}),
					new NSArray(new Object[] {
							"pourcentage", ctrl.getLabelCode(), "max"
					})));

			pourcentageRestant = pourcentageRestant.subtract(pourcentage);
		}

		return array;
	}

	
	public void corrigerMontants(IDepenseBudget depenseBudget) {

		if (!isRepartitionExiste(depenseBudget)) return;

		MontantTransaction operation = new MontantTransaction(
				depenseBudget.depHtSaisie(),
				depenseBudget.depTtcSaisie(),
				depenseBudget.depMontantBudgetaire());
		
		MontantTransaction reste = operation;
		
		NSArray<IDepenseControle> repartition = getRepartitionDepense(depenseBudget);
		
		boolean repartitionComplete = isRepartitionComplete(
				getSommePourcentageRepartition(repartition));
		int nbElementsRepartition = repartition.count() - 1;
		
		for (int i = 0; i < repartition.count(); i++) {
			IDepenseControle depense = repartition.objectAtIndex(i);
			
			boolean dernier = (i == nbElementsRepartition);
			reste = corrigerMontant(operation, reste, repartitionComplete && dernier,
					nbElementsRepartition, depense);
		}
	}

	public void corrigerMontantsComplexe(
			IDepenseBudget depenseBudget) {
		
		if (depenseBudget.isReversement()) {
			corrigerMontantsAvecReversement(depenseBudget);
		}
		else {
			corrigerMontants(depenseBudget);
		}

	}


	private void corrigerMontantsAvecReversement(IDepenseBudget depenseBudget) {
		NSArray repartitionsReversement = getRepartitionDepenseReversement(depenseBudget);
		
		BigDecimal sommePourcentage;
		BigDecimal ttc;

		if (!isRepartitionExiste(depenseBudget)) return;

		NSArray<IDepenseControle> repartition = getRepartitionDepense(depenseBudget);

		MontantTransaction operation = new MontantTransaction(
				depenseBudget.depHtSaisie(),
				depenseBudget.depTtcSaisie(),
				depenseBudget.depMontantBudgetaire());
		
		MontantTransaction reste = operation;

		sommePourcentage = serviceDepense.computeSumForKey(repartition, getPourcentageKey());

		for (int i = 0; i < repartition.count(); i++) {
			IDepenseControle depense = (IDepenseControle) repartition.objectAtIndex(i);

			MontantTransaction transaction;

			boolean dernier = (i == repartition.count() - 1);
			IDepenseControle elementRepartitionOrigine = IDepenseControle.NULL_DEPENSE_CONTROLE;
			if (depenseBudget.depenseBudgetReversement() != null && repartitionsReversement != null && depense.getCode() != null)
				elementRepartitionOrigine = getDepenseControleDeReversement(repartitionsReversement, depense);

			ttc = calculeMontant(sommePourcentage, reste.getMontantTTC(), depense.getPourcentage(), depenseBudget.depTtcSaisie(), dernier);
			if (!elementRepartitionOrigine.isNull() && ttc.abs().floatValue() == elementRepartitionOrigine.getMontantTTC().floatValue()) {
				transaction = new MontantTransaction(
									elementRepartitionOrigine.getMontantHT(),
									elementRepartitionOrigine.getMontantTTC(),
									elementRepartitionOrigine.getMontantBudgetaire()).multiply(new BigDecimal(-1));
			}
			else {
				transaction = 
						operation.multiply(depense.getPourcentage()).
						divide(Calculs.CENT, 2, BigDecimal.ROUND_HALF_UP).
						plafonner(reste);
			}
			mettreAJourMontant(depense, transaction);
			reste = reste.subtract(transaction);
		}
	}


	protected IDepenseControle getDepenseControleDeReversement(
			NSArray repartitionsReversement, IDepenseControle depense) {
		NSArray resultat =  EOQualifier.filteredArrayWithQualifier(repartitionsReversement,
				EOQualifier.qualifierWithQualifierFormat(getQualifierReversement(), new NSArray(depense.getCode())));
		
		if (resultat.count() > 0) {
			return (IDepenseControle) repartitionsReversement.objectAtIndex(0);					
		}
		else {
			return IDepenseControle.NULL_DEPENSE_CONTROLE;
		}
	}
	
	@Deprecated
	private BigDecimal calculeMontant(BigDecimal sommePourcentage, BigDecimal montantReste, BigDecimal pourcentage, BigDecimal montant, boolean reste) {
		BigDecimal calcul;

		// si c'est le dernier et que le pourcentage est egal a 100 -> on met le reste
		if (sommePourcentage.floatValue() >= 100.0 && reste)
			return montantReste;

		// on calcule le montant par rapport au pourcentage
		calcul = pourcentage.multiply(montant).divide(new BigDecimal(100.0), 2, BigDecimal.ROUND_HALF_UP);

		// on verifie que le montant calcule ne depasse pas le reste
		if (calcul.abs().compareTo(montantReste.abs()) == 1)
			calcul = montantReste;

		return calcul;
	}


	protected MontantTransaction corrigerMontant(MontantTransaction operation,
			MontantTransaction reste, boolean ditribuerReste,
			int nbElementsRepartition, IDepenseControle depense) {
		MontantTransaction transaction;
		if(ditribuerReste) {
			transaction = reste;
		}
		else {
			transaction = 
					operation.multiply(depense.getPourcentage()).
					divide(Calculs.CENT, 2, BigDecimal.ROUND_HALF_UP).
					plafonner(reste);
		}
		mettreAJourMontant(depense, transaction);

		return reste.subtract(transaction);
	}


	/**
	 * Contrôle de la validité d'une répartition de dépense
	 * Ce contrôle implémente un pattern commande.
	 * @param depenseBudget
	 * @return
	 */
	public boolean isControleGood(IDepenseBudget depenseBudget) {
		NSArray repartitionDepense = getRepartitionDepense(depenseBudget);
		
		if (isRepartitionInvalide(repartitionDepense)) return false;

		if (!isRepartitionComplete(getPourcentageKey(), repartitionDepense)) return false;
		if (getRestantHT(depenseBudget).compareTo(BigDecimal.ZERO) != 0) return false;
		if (getRestantTTC(depenseBudget).compareTo(BigDecimal.ZERO) != 0)return false;

		
		return postControle(depenseBudget);
	}

	private boolean isRepartitionInvalide(NSArray repartitionDepense) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(getQualifierCondition(), null);
		NSArray array = EOQualifier.filteredArrayWithQualifier(repartitionDepense, qual);
		return array != null && array.count() > 0;
	}
	
	protected boolean postControle(IDepenseBudget depenseBudget) {
		return true;
	}

	public  void genererDepensesControle(EOEditingContext context, EODepenseBudget depenseBudget, ISourceRepartitionCredit source, String key,
			EOExercice exercice, boolean plafondReversement) {
		NSArray depenses = getRepartition(source);
		for (int j = 0; j < depenses.count(); j++) {
			NSDictionary dico = (NSDictionary) depenses.objectAtIndex(j);
			IDepenseControle ctrl = serviceDepense.getFactoryDepenseControle(key).creer(
					context, (BigDecimal) dico.objectForKey("pourcentage"), dico.objectForKey(key), exercice, depenseBudget);
			if(plafondReversement) {
				ajoutePlafondReversement(dico, ctrl);
			}
		}
	}
	
	protected boolean isRepartitionComplete(BigDecimal sommePourcentage) {
		return sommePourcentage.compareTo(Calculs.CENT) >= 0;
	}


	protected void mettreAJourMontant(IDepenseControle depense,
			MontantTransaction transaction) {
		depense.setMontantHT(transaction.getMontantHT());
		depense.setMontantTVA(transaction.getMontantTTC().subtract(transaction.getMontantHT()));
		depense.setMontantTTC(transaction.getMontantTTC());
		depense.setMontantBudgetaire(transaction.getMontantBudgetaire());
	}
	
	
	protected void ajoutePlafondReversement(NSDictionary dico,
			IDepenseControle ctrl) {
		ctrl.setMaxReversement((BigDecimal) dico.objectForKey("max"));
	}

	protected boolean isRepartitionComplete(String key, NSArray repartitionDepense) {
		return serviceDepense.computeSumForKey(
				repartitionDepense, key).floatValue() == 100;
	}

	protected boolean isRepartionValide(IDepenseBudget depenseBudget) {
		float sommePourcentage = getSommePourcentageRepartition(getRepartitionDepense(depenseBudget)).
				floatValue();
		
		if (sommePourcentage < 0 || sommePourcentage > 100)return false;
		
		return true;
	}

	protected boolean isRepartitionExiste(IDepenseBudget depenseBudget) {
		NSArray repartition = getRepartitionDepense(depenseBudget);
		return repartition != null && repartition.count() > 0;
	}

	protected BigDecimal getSommePourcentageRepartition(
			NSArray<IDepenseControle> repartition) {
		return serviceDepense.computeSumForKey(repartition, getPourcentageKey());
	}

	protected NSArray getRepartitionDepenseReversement(IDepenseBudget depenseBudget) {
		if(depenseBudget.depenseBudgetReversement() == null) return null;
		return getRepartitionDepense(depenseBudget.depenseBudgetReversement());
	}

	abstract protected String getQualifierCondition();
	
	abstract protected NSArray getRepartition(ISourceRepartitionCredit source);
	
	/**
	 * Retourne le qualifier pour le filtrage des reversements
	 * @return
	 */
	abstract protected String getQualifierReversement();

	abstract protected String getPourcentageKey();
	
	abstract protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget);
	
	abstract protected BigDecimal getRestantHT(IDepenseBudget depenseBudget);

	abstract protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget);

	abstract public void valider(EODepenseBudget depenseBudget) throws DepenseBudgetException;
}
