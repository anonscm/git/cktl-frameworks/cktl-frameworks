/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeEtat;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeDocument;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureAnnulerCommande;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureBasculerCommande;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerCommandeDocument;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerCommandeImpression;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerPrestation;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSolderCommande;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSolderCommandeSansDroit;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcessCommande extends Process {

	/**
	 * Cree une commande a partir d'une autre sur un exercice donne<BR>
	 * La duplication ne cree que les objets EOCommande et EOArticle, pas de donnees budgetaires <BR>
	 * 
	 * @param ed editingContext dans lequel se fait l'action
	 * @param commande commande de depart que l'on duplique
	 * @param exercice exercice sur lequel dupliquer la commande
	 * @param utilisateur utilisateur qui effectue cette duplication
	 * @return la nouvelle EOCommande
	 * @throws FactoryException
	 */
	public static EOCommande dupliquer(EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur) throws FactoryException {

		return dupliquer(ed, commande, exercice, utilisateur, false, false);
	}

	public static EOCommande dupliquer(EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur, boolean withInfosBudgetaires, boolean bascule)
			throws FactoryException {
		if (!bascule) {
			if (commande.toB2bCxmlPunchoutmsgs() != null && commande.toB2bCxmlPunchoutmsgs().count() > 0) {
				throw new FactoryException("cette commande a été créée à partir d'un site marchand (B2B). Impossible de la dupliquer");
			}
		}

		exercice = (EOExercice) ed.faultForGlobalID(exercice.editingContext().globalIDForObject(exercice), ed);
		if (!FinderExercice.getExercicesOuvertsPourUtilisateur(ed, utilisateur, true).containsObject(exercice))
			throw new FactoryException("cet utilisateur n'a pas le droit de creer une commande sur cet exercice");

		//Verifier si l'utilisateur a le droit d'engager. Si non, pas de duplication d'infos budgétaires
		if (!utilisateur.isDroitEngager(commande)) {
			withInfosBudgetaires = false;
		}

		return new FactoryCommande().dupliquer(ed, commande, exercice, utilisateur, withInfosBudgetaires);
	}

	/**
	 * Duplique des commandes existantes en lot et enregistre les nouvelles. <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commandes NSArray contenant les EOCommande a dupliquer
	 * @param exercice EOExercice sur lequel cree la nouvelle commande
	 * @param utilisateur EOUtilisateur qui effectue cette action
	 * @return un NSArray de NSDictionary contenant : "origine" : la commande d'origine, "destination" : la nouvelle commande (null si erreur),
	 *         "erreur" : l'erreur si il y en a eu une
	 * @throws FactoryException
	 */
	public static NSArray<NSDictionary<String, ?>> dupliquer(_CktlBasicDataBus databus, EOEditingContext ed, NSArray<EOCommande> commandes, EOExercice exercice, EOUtilisateur utilisateur) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commandes, exercice, utilisateur, false, false, false);
	}

	public static NSArray<NSDictionary<String, ?>> dupliquer(_CktlBasicDataBus databus, EOEditingContext ed, NSArray<EOCommande> commandes, EOExercice exercice, EOUtilisateur utilisateur, boolean withInfosBudgetaires, boolean creationEngagements) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commandes, exercice, utilisateur, false, withInfosBudgetaires, creationEngagements);
	}

	/**
	 * Duplique une commande existante et enregistre la nouvelle <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande EOCommande a dupliquer
	 * @param exercice EOExercice sur lequel cree la nouvelle commande
	 * @param utilisateur EOUtilisateur qui effectue cette action
	 * @return la nouvelle EOCommande cree
	 * @throws FactoryException
	 */
	public static EOCommande dupliquer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commande, exercice, utilisateur, false, false, false);
	}

	/**
	 * Duplique des commandes existantes en lot et enregistre les nouvelles ... et solde les commandes d'origine <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commandes NSArray contenant les EOCommande a dupliquer et a solder
	 * @param exercice EOExercice sur lequel cree la nouvelle commande
	 * @param utilisateur EOUtilisateur qui effectue cette action
	 * @return un NSArray de NSDictionary contenant : "origine" : la commande d'origine, "destination" : la nouvelle commande (null si erreur),
	 *         "erreur" : l'erreur si il y en a eu une
	 * @throws FactoryException
	 */
	public static NSArray<NSDictionary<String, ?>> basculer(_CktlBasicDataBus databus, EOEditingContext ed, NSArray<EOCommande> commandes, EOExercice exercice, EOUtilisateur utilisateur) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commandes, exercice, utilisateur, true, false, false);
	}

	public static NSArray<NSDictionary<String, ?>> basculer(_CktlBasicDataBus databus, EOEditingContext ed, NSArray<EOCommande> commandes, EOExercice exercice, EOUtilisateur utilisateur, boolean withInfosBudgetaires, boolean creationEngagements) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commandes, exercice, utilisateur, true, withInfosBudgetaires, creationEngagements);
	}

	/**
	 * Duplique une commande existante et enregistre la nouvelle, et solde celle d'origine <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande EOCommande a dupliquer et a solder
	 * @param exercice EOExercice sur lequel cree la nouvelle commande
	 * @param utilisateur EOUtilisateur qui effectue cette action
	 * @return la nouvelle EOCommande cree
	 * @throws FactoryException
	 */
	public static EOCommande basculer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commande, exercice, utilisateur, true, false, false);
	}

	public static EOCommande basculer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur, boolean withInfosBudgetaires, boolean creationEngagements) throws FactoryException {
		return dupliquerEnregistrer(databus, ed, commande, exercice, utilisateur, true, withInfosBudgetaires, creationEngagements);
	}

	/**
	 * Duplique des commandes existantes en lot et enregistre les nouvelles <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commandes NSArray contenant les EOCommande a dupliquer
	 * @param exercice EOExercice sur lequel cree la nouvelle commande
	 * @param utilisateur EOUtilisateur qui effectue cette action
	 * @param solde boolean indiquant si on doit solder la commande d'origine
	 * @return un NSArray de NSDictionary contenant : "origine" : la commande d'origine, "destination" : la nouvelle commande (null si erreur),
	 *         "erreur" : l'erreur si il y en a eu une
	 * @throws FactoryException
	 */
	private static NSArray<NSDictionary<String, ?>> dupliquerEnregistrer(_CktlBasicDataBus databus, EOEditingContext ed, NSArray<EOCommande> commandes, EOExercice exercice, EOUtilisateur utilisateur, boolean solde, boolean withInfosBudgetaires, boolean creationEngagements)
			throws FactoryException {
		NSMutableArray<NSDictionary<String, ?>> array = new NSMutableArray<NSDictionary<String, ? extends Object>>();

		//array de dico : origine, destination, erreur

		if (commandes == null || commandes.count() == 0)
			return array;
		if (databus == null || !CktlDataBus.isDatabaseConnected())
			throw new FactoryException("probleme avec le dataBus");

		if (exercice == null)
			throw new FactoryException("il faut passer l'exercice de destination en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer l'utilisateur en parametre");

		for (int i = 0; i < commandes.count(); i++) {
			NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
			try {
				EOCommande commande = (EOCommande) commandes.objectAtIndex(i);
				dico.setObjectForKey(commande, "origine");

				EOCommande newCommande = dupliquerEnregistrer(databus, ed, commande, exercice, utilisateur, solde, withInfosBudgetaires, creationEngagements);
				if (newCommande == null)
					throw new FactoryException("Probleme a l'enregistrement de la commande");

				dico.setObjectForKey(newCommande, "destination");

			} catch (FactoryException e) {
				dico.setObjectForKey(e.getMessageFormatte(), "erreur");
			} catch (Exception e) {
				dico.setObjectForKey(e.getMessage(), "erreur");
			}
			array.addObject(dico);
		}

		return array;
	}

	/**
	 * Duplique une commande existante et enregistre la nouvelle <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande EOCommande a dupliquer
	 * @param exercice EOExercice sur lequel cree la nouvelle commande
	 * @param utilisateur EOUtilisateur qui effectue cette action
	 * @param solde boolean indiquant si on doit solder la commande d'origine
	 * @return la nouvelle EOCommande cree
	 * @throws FactoryException
	 */
	private static EOCommande dupliquerEnregistrer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOExercice exercice, EOUtilisateur utilisateur, boolean solde, boolean withInfosBudgetaires, boolean creationEngagements) throws FactoryException {
		if (commande == null)
			throw new FactoryException("il faut passer une commande en parametre");
		if (databus == null || !CktlDataBus.isDatabaseConnected())
			throw new FactoryException("probleme avec le dataBus");

		EOCommande newCommande = null;
		try {
			newCommande = dupliquer(ed, commande, exercice, utilisateur, withInfosBudgetaires, solde);

			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessCommande.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			ProcessGestionCommande.insererCommande(databus, ed, newCommande);

			if (creationEngagements) {
				if (!newCommande.isComplete())
					throw new FactoryException("Les informations budgetaires ne sont pas completes");

				engagerSansTransaction(databus, ed, newCommande, utilisateur);
			}

			if (solde && !commande.exercice().exeExercice().equals(exercice.exeExercice()) &&
					!ProcedureBasculerCommande.enregistrer(databus, commande, newCommande)) {
				throw new FactoryException((String) databus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			if (solde)
				solderSansDroitSansTransaction(databus, ed, commande);

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}

		return newCommande;
	}

	/**
	 * Enregistre une commande que ce soit en creation ou en modification <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande commande a enregistrer
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {
		boolean commandeEnModification = false;

		if (commande == null)
			throw new FactoryException("il faut passer une commande en parametre");
		if (databus == null || !CktlDataBus.isDatabaseConnected())
			throw new FactoryException("probleme avec le dataBus");

		// on regarde si la commande a une cle et donc si c'est une creation ou une modification
		if (EOUtilities.primaryKeyForObject(ed, commande) != null)
			commandeEnModification = true;

		//Rod 25/11/09 : modif pour coller aux templates velocity
		//		commande.setUtilisateur(utilisateur);
		commande.setUtilisateurRelationship(utilisateur);

		// creation d'une nouvelle commande
		if (!commandeEnModification) {
			try {
				if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
					System.out.println("Methode (" + utilisateur.nomPrenom()
							+ ") ProcessCommande.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
					_CktlBasicDataBus.adaptorContext().rollbackTransaction();
				}

				databus.beginTransaction();

				ProcessGestionCommande.insererCommande(databus, ed, commande);
				engagerSansTransaction(databus, ed, commande, utilisateur);
				if (commande.commandeDocuments() != null) {
					for (int i = 0; i < commande.commandeDocuments().count(); i++)
						documenterSansTransaction(databus, ed, (EOCommandeDocument) commande.commandeDocuments().objectAtIndex(i), utilisateur);
				}

				databus.commitTransaction();
			} catch (FactoryException e) {
				databus.rollbackTransaction();
				System.out.println("FactoryException : " + e);
				throw e;
			} catch (Exception e) {
				databus.rollbackTransaction();
				System.out.println("Exception : " + e);
				throw new FactoryException(e.getMessage(), e);
			}

			ProcessCommande.enregistrerPrestationInterne(databus, ed, commande);

			return;
		}

		// modification d'une commande precedemment enregistree

		if (commande.typeEtat().equals(FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_ANNULEE)))
			throw new FactoryException("on ne peut pas modifier une commande annulee");
		if (commande.typeEtat().equals(FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_SOLDEE)))
			throw new FactoryException("on ne peut pas modifier une commande soldee");

		// TODO : virer pour cause de pb ... mais voir si code bon ... ou seulement pb de invalidate a rajouter
		//if (!commande.isModifiable(ed, utilisateur, null))
		//	throw new FactoryException("cet utilisateur ne peut pas modifier cette commande");

		// on ouvre une transaction

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessCommande.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			// si la commande n'est pas une precommmande ... on engage pour effectuer les modifs eventuelles
			//if (!commande.typeEtat().equals(FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_PRECOMMANDE)))
			engagerSansTransaction(databus, ed, commande, utilisateur);
			ProcessGestionCommande.modifierCommande(databus, ed, commande);

			databus.commitTransaction();
		} catch (FactoryException e) {
			databus.rollbackTransaction();
			System.out.println("FactoryException : " + e);
			throw e;
		} catch (Exception e) {
			databus.rollbackTransaction();
			System.out.println("Exception : " + e);
			throw new FactoryException(e.getMessage());
		}

		ProcessCommande.enregistrerPrestationInterne(databus, ed, commande);
	}

	/**
	 * Engage une commande <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande commande a engager
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	protected static void engager(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {
		if (!commande.isModifiable(ed, utilisateur, null))
			throw new FactoryException("cet utilisateur ne peut pas modifier cette commande");

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessCommande.engager : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			engagerSansTransaction(databus, ed, commande, utilisateur);

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	protected static void enregistrerPrestationInterne(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande) throws FactoryException {
		if (commande == null)
			return;

		if (commande.prestationId() == null) {
			if (commande.typeApplication() != null && commande.typeApplication().tyapLibelle() != null
					&& commande.typeApplication().tyapLibelle().equals(EOTypeApplication.APPLICATION_PRESTATION)) {
				commande.setIsEnregistrerPI(true);
			}
			else {
				if (!commande.isPrestationInterne())
					return;
			}
			if (commande.fournisseurInterneClient() == null)
				return;

			if (commande.commandeEngagements() == null || commande.commandeEngagements().count() == 0)
				return;

			if (!commande.utilisateur().isDroitCreerPrestation(commande.exercice()))
				return;

			if (!commande.isEnregistrerPi())
				return;
		}

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + commande.utilisateur().nomPrenom()
						+ ") ProcessCommande.enregistrerPrestationInterne : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();
			ProcedureCreerPrestation.enregistrer(databus, commande);
			databus.commitTransaction();

		} catch (RuntimeException e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage() + "\nVotre commande a ete malgre tout enregistree, vous pouvez la reprendre via PIE.", false);
		}
	}

	/**
	 * Engage une commande sans transaction <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande commande a engager
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	private static void engagerSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {
		if (commande == null)
			throw new FactoryException("il faut passer une commande en parametre");
		if (commande.typeEtat().equals(FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_ANNULEE)))
			throw new FactoryException("on ne peut pas engager une commande annulee");
		if (commande.typeEtat().equals(FinderTypeEtat.getTypeEtat(ed, EOCommande.ETAT_SOLDEE)))
			throw new FactoryException("on ne peut pas engager une commande soldee");

		//Rod 25/11/09 : modif pour coller aux templates velocity
		//		commande.setUtilisateur(utilisateur);
		commande.setUtilisateurRelationship(utilisateur);

		NSArray lesEngagements = ProcessGestionCommande.genererEngagements(ed, commande);

		// mis en commentaire pour le cas d'une commande engagee qu'on passe en precommande .. effets de bord ?

		//		if (lesEngagements.count()==0) {
		//			if (commande.commandeEngagements().count()>0)
		//				throw new FactoryException("la commande a des engagements et la repartition budgetaire est vide");
		//			return;
		//		}

		// si la commande n'a pas encore d'engagement ... on cree ceux renvoyer par la methode de generation
		if (commande.commandeEngagements().count() == 0) {
			if (lesEngagements.count() == 0)
				return;

			ProcessEngagement.insererEngagementBudgets(databus, ed, commande, lesEngagements);

			// on change l'etat de la commande
			//Rod 25/11/09 : modif pour coller aux templates velocity
			//commande.setTypeEtat(commande.typeEtatSuivantEngagements());
			commande.setTypeEtatRelationship(commande.typeEtatSuivantEngagements());
			return;
		}

		// on regarde les engagements a supprimer, a modifier et a creer
		NSArray engagementsASupprimer = ProcessGestionCommande.engagementsASupprimer(commande, lesEngagements, utilisateur);
		NSArray engagementsAModifier = ProcessGestionCommande.engagementsAModifier(commande, lesEngagements, utilisateur);
		NSArray engagementsACreer = ProcessGestionCommande.engagementsACreer(commande, lesEngagements, utilisateur);

		// si aucune modif ... on arrete la
		if (engagementsACreer.count() == 0 && engagementsAModifier.count() == 0 && engagementsASupprimer.count() == 0)
			return;

		// on supprime, modifie et cree les engagements necessaires
		ProcessEngagement.supprimerEngagementBudgets(databus, ed, commande, engagementsASupprimer);
		ProcessEngagement.modifierEngagementBudgets(databus, ed, commande, engagementsAModifier);
		ProcessEngagement.insererEngagementBudgets(databus, ed, commande, engagementsACreer);

		// on change l'etat de la commande
		//Rod 25/11/09 : modif pour coller aux templates velocity
		//commande.setTypeEtat(commande.typeEtatSuivantEngagements());
		commande.setTypeEtatRelationship(commande.typeEtatSuivantEngagements());
	}

	/**
	 * Annule une commande et supprime les engagements associes <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande commande a annuler
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void annuler(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (commande == null)
			throw new FactoryException("il faut passer une commande en parametre");
		if (commande.isSupprimable(ed, utilisateur, null) == false)
			throw new FactoryException("l'utilisateur ne peut pas annuler cette commande");

		//Rod 25/11/09 : modif pour coller aux templates velocity
		//commande.setUtilisateur(utilisateur);
		commande.setUtilisateurRelationship(utilisateur);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessCommande.annuler : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			pbProcedure = ProcedureAnnulerCommande.enregistrer(databus, commande);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Enregsitre les infos d'une commande <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commandeImpression EOCommandeImpression
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void imprimer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommandeImpression commandeImpression, EOUtilisateur utilisateur) throws FactoryException {

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			imprimerSansTransaction(databus, ed, commandeImpression, utilisateur);

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	public static void imprimerSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EOCommandeImpression commandeImpression, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (commandeImpression == null)
			throw new FactoryException("il faut passer une commandeImpression en parametre");
		if (!commandeImpression.commande().isImprimable())
			throw new FactoryException("la commande n'est pas imprimable");

		commandeImpression.setUtilisateurRelationship(utilisateur);

		pbProcedure = ProcedureCreerCommandeImpression.enregistrer(databus, commandeImpression);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure)
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));

		commandeImpression.setCimpIdProc((Number) dico.valueForKey("010_a_cimp_id"));
	}

	/**
	 * Enregsitre les documents d'une commande <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commandeDocument EOCommandeDocument
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void documenter(_CktlBasicDataBus databus, EOEditingContext ed, EOCommandeDocument commandeDocument, EOUtilisateur utilisateur) throws FactoryException {
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			documenterSansTransaction(databus, ed, commandeDocument, utilisateur);

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	public static void documenterSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EOCommandeDocument commandeDocument, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (commandeDocument == null)
			throw new FactoryException("il faut passer une commandeDocument en parametre");

		//Rod 25/11/09 : modif pour coller aux templates velocity
		//commandeDocument.setUtilisateur(utilisateur);
		commandeDocument.setUtilisateurRelationship(utilisateur);

		pbProcedure = ProcedureCreerCommandeDocument.enregistrer(databus, commandeDocument);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure)
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
	}

	public static void documenterEtImprimer(_CktlBasicDataBus databus, EOEditingContext ed, EOCommandeDocument commandeDocument, EOUtilisateur utilisateur) throws FactoryException {
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			if (commandeDocument.commandeImpression() != null)
				imprimerSansTransaction(databus, ed, commandeDocument.commandeImpression(), utilisateur);
			documenterSansTransaction(databus, ed, commandeDocument, utilisateur);

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Solde des commandes en lot <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commandes NSArray contenant les EOCommande a solder
	 * @param utilisateur utilisateur effectuant cette action
	 * @return un NSArray de NSDictionary contenant : "commande" : la commande, "erreur" : l'erreur si il y en a eu une
	 * @throws FactoryException
	 */
	public static NSArray<NSDictionary<String, ?>> solder(_CktlBasicDataBus databus, EOEditingContext ed, NSArray<EOCommande> commandes, EOUtilisateur utilisateur) throws FactoryException {
		NSMutableArray array = new NSMutableArray();

		//array de dico : commande, erreur

		if (commandes == null || commandes.count() == 0)
			return array;
		if (databus == null || !CktlDataBus.isDatabaseConnected())
			throw new FactoryException("probleme avec le dataBus");

		if (utilisateur == null)
			throw new FactoryException("il faut passer l'utilisateur en parametre");

		for (int i = 0; i < commandes.count(); i++) {
			NSMutableDictionary dico = new NSMutableDictionary();
			try {
				EOCommande commande = (EOCommande) commandes.objectAtIndex(i);
				dico.setObjectForKey(commande, "origine");

				solderSansTransaction(databus, ed, commande, utilisateur);
			} catch (Exception e) {
				dico.setObjectForKey(e.getMessage(), "erreur");
			}
			array.addObject(dico);
		}

		return array;

	}

	/**
	 * Solde la commande et les engagements associes.<BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param commande commande a solder
	 * @param utilisateur utilisateur qui solde la commande
	 * @throws FactoryException
	 */
	public static void solder(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {
		if (commande == null)
			throw new FactoryException("il faut passer une commande en parametre");

		//Rod 25/11/09 : modif pour coller aux templates velocity
		//commande.setUtilisateur(utilisateur);
		commande.setUtilisateurRelationship(utilisateur);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessCommande.solder : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			solderSansTransaction(databus, ed, commande, utilisateur);

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	private static void solderSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		pbProcedure = ProcedureSolderCommande.enregistrer(databus, commande, utilisateur);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}
	}

	private static void solderSansDroitSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		pbProcedure = ProcedureSolderCommandeSansDroit.enregistrer(databus, commande);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}
	}

	/**
	 * Memorise les utilisateurs contactes par mail par l'utilisateur de l'application.<BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param utilisateur utilisateur connecte
	 * @param utilisateurs NSArray de EOUtilisateur contactes par mail
	 */
	public static void insererUtilisateursContactes(_CktlBasicDataBus databus, EOCommande commande, NSArray utilisateurs) {
		ProcessPreference.insererUtilisateursContactes(databus, commande, utilisateurs);
	}

}
