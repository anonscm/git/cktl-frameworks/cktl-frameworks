/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerPreferenceMail;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerPreferenceOrgan;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerPreferenceOrgan;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class ProcessPreference extends Process {

	/**
	 * Enregistre les personnes informees pour une commande<BR>
	 * @param databus
	 *        _CktlBasicDataBus permettant de gerer les transactions
	 * @param commande
	 *        EOCommande concernee
	 * @param utilisateurs
	 *        NSArray d'EOUtilisateur ayant ete informes
	 */
	public static void insererUtilisateursContactes(_CktlBasicDataBus databus, EOCommande commande, NSArray utilisateurs) {
		boolean pbProcedure=false;
		NSDictionary dico=null;

		if (commande==null)
			return;
		
		EOUtilisateur utilisateur=commande.utilisateur();
		
		if (utilisateur==null || utilisateurs==null || utilisateurs.count()==0)
			return;

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ("+utilisateur.nomPrenom()
						+") ProcessPreference.insererUtilisateursContactes : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			for (int i=0; i<utilisateurs.count(); i++) {
				if (!(utilisateurs.objectAtIndex(i) instanceof EOUtilisateur))
					continue;
				EOUtilisateur utilisateurMail=(EOUtilisateur)utilisateurs.objectAtIndex(i);

				pbProcedure=ProcedureCreerPreferenceMail.enregistrer(databus, utilisateur, commande, utilisateurMail);
				dico=new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String)dico.valueForKey(_CktlBasicDataBus.ERROR_KEY)); 
				}
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}
	
	/**
	 * Ajoute des lignes budgetaires preferees a l'utilisateur<BR>
	 * @param databus
	 *        _CktlBasicDataBus permettant de gerer les transactions
	 * @param utilisateur
	 *        EOUtilisateur ajoutant des lignes budgetaires preferees
	 * @param organs
	 *        NSArray d'EOOrgan a ajouter aux preferences
	 */
	public static void insererOrgansPreferes(_CktlBasicDataBus databus, EOUtilisateur utilisateur, NSArray organs) {
		boolean pbProcedure=false;
		NSDictionary dico=null;

		if (utilisateur==null || organs==null || organs.count()==0)
			return;
		
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ("+utilisateur.nomPrenom()
						+") ProcessPreference.insererOrgansPreferes : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			for (int i=0; i<organs.count(); i++) {
				if (!(organs.objectAtIndex(i) instanceof EOOrgan))
					continue;
				EOOrgan organ=(EOOrgan)organs.objectAtIndex(i);

				pbProcedure=ProcedureCreerPreferenceOrgan.enregistrer(databus, utilisateur, organ);
				dico=new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String)dico.valueForKey(_CktlBasicDataBus.ERROR_KEY)); 
				}
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Ajoute une ligne budgetaire preferee a l'utilisateur<BR>
	 * @param databus
	 *        _CktlBasicDataBus permettant de gerer les transactions
	 * @param utilisateur
	 *        EOUtilisateur ajoutant une ligne budgetaire preferee
	 * @param organ
	 *        EOOrgan a ajouter aux preferences
	 */
	public static void insererOrganPrefere(_CktlBasicDataBus databus, EOUtilisateur utilisateur, EOOrgan organ) {
		boolean pbProcedure=false;
		NSDictionary dico=null;

		if (utilisateur==null || organ==null)
			return;
		
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ("+utilisateur.nomPrenom()
						+") ProcessPreference.insererOrganPrefere : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			pbProcedure=ProcedureCreerPreferenceOrgan.enregistrer(databus, utilisateur, organ);
			dico=new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String)dico.valueForKey(_CktlBasicDataBus.ERROR_KEY)); 
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Supprime des lignes budgetaires preferees de l'utilisateur<BR>
	 * @param databus
	 *        _CktlBasicDataBus permettant de gerer les transactions
	 * @param utilisateur
	 *        EOUtilisateur supprimant ses lignes budgetaires
	 * @param organs
	 *        NSArray d'EOOrgan a supprimer
	 */
	public static void supprimerOrgansPreferes(_CktlBasicDataBus databus, EOUtilisateur utilisateur, NSArray organs) {
		boolean pbProcedure=false;
		NSDictionary dico=null;

		if (utilisateur==null || organs==null || organs.count()==0)
			return;
		
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ("+utilisateur.nomPrenom()
						+") ProcessPreference.supprimerOrgansPreferes : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			for (int i=0; i<organs.count(); i++) {
				if (!(organs.objectAtIndex(i) instanceof EOOrgan))
					continue;
				EOOrgan organ=(EOOrgan)organs.objectAtIndex(i);

				pbProcedure=ProcedureSupprimerPreferenceOrgan.enregistrer(databus, utilisateur, organ);
				dico=new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String)dico.valueForKey(_CktlBasicDataBus.ERROR_KEY)); 
				}
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Supprime une ligne budgetaire preferee de l'utilisateur<BR>
	 * @param databus
	 *        _CktlBasicDataBus permettant de gerer les transactions
	 * @param utilisateur
	 *        EOUtilisateur supprimant sa ligne budgetaire
	 * @param organ
	 *        EOOrgan a supprimer
	 */
	public static void supprimerOrganPrefere(_CktlBasicDataBus databus, EOUtilisateur utilisateur, EOOrgan organ) {
		boolean pbProcedure=false;
		NSDictionary dico=null;

		if (utilisateur==null || organ==null)
			return;
		
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode ("+utilisateur.nomPrenom()
						+") ProcessPreference.supprimerOrganPrefere : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			pbProcedure=ProcedureSupprimerPreferenceOrgan.enregistrer(databus, utilisateur, organ);
			dico=new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String)dico.valueForKey(_CktlBasicDataBus.ERROR_KEY)); 
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}
}
