/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import org.cocktail.fwkcktldepense.server.exception.PaiementExtourneException;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.service.ConfigurationExtourneService;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class Process {

	// TODO a sortir dans des classes "Controle" si plusieurs nouveaux controles s'ajoutent.
	public static void controlerExtournePossible(EODepensePapier depensePapier) throws PaiementExtourneException {
		EOModePaiement depensePapierModePaiement = depensePapier.modePaiement();
		if (depensePapierModePaiement == null || !EOModePaiement.MODE_DOM_EXTOURNE.equals(depensePapierModePaiement.modDom())) {
			return;
		}

		EOEditingContext edc = depensePapier.editingContext();
		EOExercice exerciceCourant = depensePapier.exercice();
		EOTypeCredit typeCreditUnique = depensePapier.typesCreditsSurSectionUnique();

		// traitement unique
		if (typeCreditUnique != null) {
			if (!extourneAutoriseePourTypeCredit(edc, exerciceCourant, typeCreditUnique)) {
				throw new PaiementExtourneException(typeCreditUnique);
			}
			return;
		}

		// traitement de chaque section
		for (EODepenseBudget depenseBudget : depensePapier.depenseBudgets()) {
			if (!depenseBudget.estIgnoree()) {
				EOTypeCredit typeCredit = depenseBudget.engagementBudget().typeCredit();
				if (!extourneAutoriseePourTypeCredit(edc, exerciceCourant, typeCredit)) {
					throw new PaiementExtourneException(typeCredit);
				}
			}
		}
	}

	private static boolean extourneAutoriseePourTypeCredit(EOEditingContext edc, EOExercice exerciceCourant, EOTypeCredit typeCredit) {
		boolean extourneAutoriseePourTypeCredit = true;
		if (typeCredit != null) {
			if (!sectionAutorisee(edc, exerciceCourant, typeCredit)) {
				extourneAutoriseePourTypeCredit = false;
			}
		}
		return extourneAutoriseePourTypeCredit;
	}

	private static boolean sectionAutorisee(EOEditingContext edc, EOExercice exerciceCourant, EOTypeCredit typeCredit) {
		ConfigurationExtourneService confExtourneService = ConfigurationExtourneService.instance();
		EOTypeCredit.ESection section = typeCredit.section();
		boolean sectionAutorisee = false;
		boolean extourneEnveloppeTypeCreditAutorise =
				confExtourneService.extourneEnveloppeTypeCreditAutorise(edc, exerciceCourant.exerciceSuivant(), section);
		boolean extourneFlecheeTypeCreditAutorise =
				confExtourneService.extourneFlecheeTypeCreditAutorise(edc, exerciceCourant.exerciceSuivant(), section);
		if (extourneEnveloppeTypeCreditAutorise || extourneFlecheeTypeCreditAutorise) {
			sectionAutorisee = true;
		}
		return sectionAutorisee;
	}

}
