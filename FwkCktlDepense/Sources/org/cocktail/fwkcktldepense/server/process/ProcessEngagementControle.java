/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ProcessEngagementControle extends Process {

	/**
	 * @param ancienEngage
	 * @param nouveauEngage
	 * @return
	 * @throws FactoryException
	 */
	protected static boolean isModifsActions(EOEngagementBudget ancienEngage, EOEngagementBudget nouveauEngage) throws FactoryException {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray anciens = (NSArray) ancienEngage.engagementControleActions();
		NSArray nouveaux = (NSArray) nouveauEngage.engagementControleActions();
		boolean isModifs = false;

		for (int i = anciens.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementControleAction engagement = (EOEngagementControleAction) anciens.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction=%@", new NSArray(engagement.typeAction())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(nouveaux, new EOAndQualifier(arrayQualifier));

			// cas de la suppression
			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(null);
				continue;
			}

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs fois la meme action sur l'engagement");

			EOEngagementControleAction nouveau = (EOEngagementControleAction) larray.objectAtIndex(0);

			// cas de la modification
			if (engagement.eactHtSaisie().compareTo(nouveau.eactHtSaisie()) != 0 || engagement.eactTvaSaisie().compareTo(nouveau.eactTvaSaisie()) != 0 ||
					engagement.eactTtcSaisie().compareTo(nouveau.eactTtcSaisie()) != 0 || engagement.eactMontantBudgetaire().compareTo(nouveau.eactMontantBudgetaire()) != 0) {

				isModifs = true;

				engagement.setEactHtSaisie(nouveau.eactHtSaisie());
				engagement.setEactTvaSaisie(nouveau.eactTvaSaisie());
				engagement.setEactTtcSaisie(nouveau.eactTtcSaisie());
				engagement.setEactMontantBudgetaire(nouveau.eactMontantBudgetaire());
				engagement.setEactMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEactDateSaisie(new NSTimestamp());
			}
		}

		// cas de la creation
		for (int i = nouveaux.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementControleAction engagement = (EOEngagementControleAction) nouveaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction=%@", new NSArray(engagement.typeAction())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(anciens, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(ancienEngage);
			}
		}

		if (isModifs && ancienEngage.engagementControleActions().count() > 0)
			((EOEngagementControleAction) ancienEngage.engagementControleActions().objectAtIndex(0)).
					setEactMontantBudgetaireReste(ancienEngage.engMontantBudgetaireReste());

		return isModifs;
	}

	/**
	 * @param ancienEngage
	 * @param nouveauEngage
	 * @return
	 * @throws FactoryException
	 */
	protected static boolean isModifsAnalytiques(EOEngagementBudget ancienEngage, EOEngagementBudget nouveauEngage) throws FactoryException {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray anciens = (NSArray) ancienEngage.engagementControleAnalytiques();
		NSArray nouveaux = (NSArray) nouveauEngage.engagementControleAnalytiques();
		boolean isModifs = false;

		for (int i = anciens.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementControleAnalytique engagement = (EOEngagementControleAnalytique) anciens.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("codeAnalytique=%@", new NSArray(engagement.codeAnalytique())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(nouveaux, new EOAndQualifier(arrayQualifier));

			// cas de la suppression
			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(null);
				continue;
			}

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs fois le meme code analytique sur l'engagement");

			EOEngagementControleAnalytique nouveau = (EOEngagementControleAnalytique) larray.objectAtIndex(0);

			// cas de la modification
			if (engagement.eanaHtSaisie().compareTo(nouveau.eanaHtSaisie()) != 0 || engagement.eanaTvaSaisie().compareTo(nouveau.eanaTvaSaisie()) != 0 ||
					engagement.eanaTtcSaisie().compareTo(nouveau.eanaTtcSaisie()) != 0 || engagement.eanaMontantBudgetaire().compareTo(nouveau.eanaMontantBudgetaire()) != 0) {

				isModifs = true;

				engagement.setEanaHtSaisie(nouveau.eanaHtSaisie());
				engagement.setEanaTvaSaisie(nouveau.eanaTvaSaisie());
				engagement.setEanaTtcSaisie(nouveau.eanaTtcSaisie());
				engagement.setEanaMontantBudgetaire(nouveau.eanaMontantBudgetaire());
				engagement.setEanaMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEanaDateSaisie(new NSTimestamp());
			}
		}

		// cas de la creation
		for (int i = nouveaux.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementControleAnalytique engagement = (EOEngagementControleAnalytique) nouveaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("codeAnalytique=%@", new NSArray(engagement.codeAnalytique())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(anciens, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(ancienEngage);
			}
		}

		return isModifs;
	}

	/**
	 * @param ancienEngage
	 * @param nouveauEngage
	 * @return
	 * @throws FactoryException
	 */
	protected static boolean isModifsConventions(EOEngagementBudget ancienEngage, EOEngagementBudget nouveauEngage) throws FactoryException {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray anciens = (NSArray) ancienEngage.engagementControleConventions();
		NSArray nouveaux = (NSArray) nouveauEngage.engagementControleConventions();
		boolean isModifs = false;

		for (int i = anciens.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementControleConvention engagement = (EOEngagementControleConvention) anciens.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("convention=%@", new NSArray(engagement.convention())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(nouveaux, new EOAndQualifier(arrayQualifier));

			// cas de la suppression
			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(null);
				continue;
			}

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs fois la meme convention sur l'engagement");

			EOEngagementControleConvention nouveau = (EOEngagementControleConvention) larray.objectAtIndex(0);

			// cas de la modification
			if (engagement.econHtSaisie().compareTo(nouveau.econHtSaisie()) != 0 || engagement.econTvaSaisie().compareTo(nouveau.econTvaSaisie()) != 0 ||
					engagement.econTtcSaisie().compareTo(nouveau.econTtcSaisie()) != 0 || engagement.econMontantBudgetaire().compareTo(nouveau.econMontantBudgetaire()) != 0) {

				isModifs = true;

				engagement.setEconHtSaisie(nouveau.econHtSaisie());
				engagement.setEconTvaSaisie(nouveau.econTvaSaisie());
				engagement.setEconTtcSaisie(nouveau.econTtcSaisie());
				engagement.setEconMontantBudgetaire(nouveau.econMontantBudgetaire());
				engagement.setEconMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEconDateSaisie(new NSTimestamp());
			}
		}

		// cas de la creation
		for (int i = nouveaux.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementControleConvention engagement = (EOEngagementControleConvention) nouveaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("convention=%@", new NSArray(engagement.convention())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(anciens, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(ancienEngage);
			}
		}

		return isModifs;
	}

	/**
	 * @param ancienEngage
	 * @param nouveauEngage
	 * @return
	 * @throws FactoryException
	 */
	protected static boolean isModifsHorsMarches(EOEngagementBudget ancienEngage, EOEngagementBudget nouveauEngage) throws FactoryException {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray anciens = (NSArray) ancienEngage.engagementControleHorsMarches();
		NSArray nouveaux = (NSArray) nouveauEngage.engagementControleHorsMarches();
		boolean isModifs = false;

		for (int i = anciens.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementControleHorsMarche engagement = (EOEngagementControleHorsMarche) anciens.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(engagement.codeExer())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@", new NSArray(engagement.typeAchat())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(nouveaux, new EOAndQualifier(arrayQualifier));

			// cas de la suppression
			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(null);
				continue;
			}

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs fois le meme code nomenclature sur l'engagement");

			EOEngagementControleHorsMarche nouveau = (EOEngagementControleHorsMarche) larray.objectAtIndex(0);

			// cas de la modification
			if (engagement.ehomHtSaisie().compareTo(nouveau.ehomHtSaisie()) != 0 || engagement.ehomTvaSaisie().compareTo(nouveau.ehomTvaSaisie()) != 0 ||
					engagement.ehomTtcSaisie().compareTo(nouveau.ehomTtcSaisie()) != 0 || engagement.ehomMontantBudgetaire().compareTo(nouveau.ehomMontantBudgetaire()) != 0) {

				isModifs = true;

				engagement.setEhomHtSaisie(nouveau.ehomHtSaisie());
				engagement.setEhomTvaSaisie(nouveau.ehomTvaSaisie());
				engagement.setEhomTtcSaisie(nouveau.ehomTtcSaisie());
				engagement.setEhomMontantBudgetaire(nouveau.ehomMontantBudgetaire());
				engagement.setEhomMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEhomMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEhomDateSaisie(new NSTimestamp());
			}
		}

		// cas de la creation
		for (int i = nouveaux.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementControleHorsMarche engagement = (EOEngagementControleHorsMarche) nouveaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("codeExer=%@", new NSArray(engagement.codeExer())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeAchat=%@", new NSArray(engagement.typeAchat())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(anciens, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(ancienEngage);
			}
		}

		if (isModifs && ancienEngage.engagementControleHorsMarches().count() > 0)
			((EOEngagementControleHorsMarche) ancienEngage.engagementControleHorsMarches().objectAtIndex(0)).
					setEhomMontantBudgetaireReste(ancienEngage.engMontantBudgetaireReste());

		return isModifs;
	}

	/**
	 * @param ancienEngage
	 * @param nouveauEngage
	 * @return
	 * @throws FactoryException
	 */
	protected static boolean isModifsMarches(EOEngagementBudget ancienEngage, EOEngagementBudget nouveauEngage) throws FactoryException {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray anciens = (NSArray) ancienEngage.engagementControleMarches();
		NSArray nouveaux = (NSArray) nouveauEngage.engagementControleMarches();
		boolean isModifs = false;

		for (int i = anciens.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementControleMarche engagement = (EOEngagementControleMarche) anciens.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("attribution=%@", new NSArray(engagement.attribution())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(nouveaux, new EOAndQualifier(arrayQualifier));

			// cas de la suppression
			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(null);
				continue;
			}

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs fois la meme attribution sur l'engagement");

			EOEngagementControleMarche nouveau = (EOEngagementControleMarche) larray.objectAtIndex(0);

			// cas de la modification
			if (engagement.emarHtSaisie().compareTo(nouveau.emarHtSaisie()) != 0 || engagement.emarTvaSaisie().compareTo(nouveau.emarTvaSaisie()) != 0 ||
					engagement.emarTtcSaisie().compareTo(nouveau.emarTtcSaisie()) != 0 || engagement.emarMontantBudgetaire().compareTo(nouveau.emarMontantBudgetaire()) != 0) {

				isModifs = true;

				engagement.setEmarHtSaisie(nouveau.emarHtSaisie());
				engagement.setEmarTvaSaisie(nouveau.emarTvaSaisie());
				engagement.setEmarTtcSaisie(nouveau.emarTtcSaisie());
				engagement.setEmarMontantBudgetaire(nouveau.emarMontantBudgetaire());
				engagement.setEmarMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEmarMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEmarDateSaisie(new NSTimestamp());
			}
		}

		// cas de la creation
		for (int i = nouveaux.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementControleMarche engagement = (EOEngagementControleMarche) nouveaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("attribution=%@", new NSArray(engagement.attribution())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(anciens, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(ancienEngage);
			}
		}

		if (isModifs && ancienEngage.engagementControleMarches().count() > 0)
			((EOEngagementControleMarche) ancienEngage.engagementControleMarches().objectAtIndex(0)).
					setEmarMontantBudgetaireReste(ancienEngage.engMontantBudgetaireReste());

		return isModifs;
	}

	/**
	 * @param ancienEngage
	 * @param nouveauEngage
	 * @return
	 * @throws FactoryException
	 */
	protected static boolean isModifsPlanComptables(EOEngagementBudget ancienEngage, EOEngagementBudget nouveauEngage) throws FactoryException {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray anciens = (NSArray) ancienEngage.engagementControlePlanComptables();
		NSArray nouveaux = (NSArray) nouveauEngage.engagementControlePlanComptables();
		boolean isModifs = false;

		for (int i = anciens.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementControlePlanComptable engagement = (EOEngagementControlePlanComptable) anciens.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable=%@", new NSArray(engagement.planComptable())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(nouveaux, new EOAndQualifier(arrayQualifier));

			// cas de la suppression
			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(null);
				continue;
			}

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs fois la meme imputation sur l'engagement");

			EOEngagementControlePlanComptable nouveau = (EOEngagementControlePlanComptable) larray.objectAtIndex(0);

			// cas de la modification
			if (engagement.epcoHtSaisie().compareTo(nouveau.epcoHtSaisie()) != 0 || engagement.epcoTvaSaisie().compareTo(nouveau.epcoTvaSaisie()) != 0 ||
					engagement.epcoTtcSaisie().compareTo(nouveau.epcoTtcSaisie()) != 0 || engagement.epcoMontantBudgetaire().compareTo(nouveau.epcoMontantBudgetaire()) != 0) {

				isModifs = true;

				engagement.setEpcoHtSaisie(nouveau.epcoHtSaisie());
				engagement.setEpcoTvaSaisie(nouveau.epcoTvaSaisie());
				engagement.setEpcoTtcSaisie(nouveau.epcoTtcSaisie());
				engagement.setEpcoMontantBudgetaire(nouveau.epcoMontantBudgetaire());
				engagement.setEpcoMontantBudgetaireReste(new BigDecimal(0.0));
				engagement.setEpcoDateSaisie(new NSTimestamp());
			}
		}

		// cas de la creation
		for (int i = nouveaux.count() - 1; i >= 0; i--) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementControlePlanComptable engagement = (EOEngagementControlePlanComptable) nouveaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable=%@", new NSArray(engagement.planComptable())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagement.exercice())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(anciens, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0) {
				isModifs = true;
				engagement.setEngagementBudgetRelationship(ancienEngage);
			}
		}

		if (isModifs && ancienEngage.engagementControlePlanComptables().count() > 0)
			((EOEngagementControlePlanComptable) ancienEngage.engagementControlePlanComptables().objectAtIndex(0)).
					setEpcoMontantBudgetaireReste(ancienEngage.engMontantBudgetaireReste());

		return isModifs;
	}
}
