/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.b2b.procedure.ProcedureCreerB2bCxmlItem;
import org.cocktail.fwkcktldepense.server.b2b.procedure.ProcedureCreerB2bCxmlPunchoutmessage;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryCatalogueArticle;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeBudget;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.finder.FinderDevise;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerArticle;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerArticleCatalogue;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerCommande;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerCommandeBudget;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureModifierArticle;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureModifierCommande;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureModifierCommandeBudget;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerArticle;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerCommandeBudget;
import org.cocktail.fwkcktldepense.server.util.LigneMarche;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class ProcessGestionCommande extends Process {

	/**
	 * Genere les engagements previsionnels a partir des commandesBudget de la commande <BR>
	 * 
	 * @param ed editingContext dans lequel on cree les objets
	 * @param commande commande pour laquelle on genere ces engagements
	 * @return un NSMutableArray de EOEngagementBudget
	 */
	protected static NSMutableArray genererEngagements(EOEditingContext ed, EOCommande commande) {
		NSMutableArray lesEngagements = new NSMutableArray();

		if (commande == null)
			return lesEngagements;

		for (int i = 0; i < commande.commandeBudgets().count(); i++) {
			EOCommandeBudget commandeBudget = (EOCommandeBudget) commande.commandeBudgets().objectAtIndex(i);

			if (!commandeBudget.isComplet())
				continue;

			if (commandeBudget.cbudMontantBudgetaire().compareTo(new BigDecimal(0.0)) == 0 &&
					commandeBudget.cbudHtSaisie().compareTo(new BigDecimal(0.0)) == 0 &&
					commandeBudget.cbudTvaSaisie().compareTo(new BigDecimal(0.0)) == 0 &&
					commandeBudget.cbudTtcSaisie().compareTo(new BigDecimal(0.0)) == 0)
				continue;

			lesEngagements.addObject(ProcessEngagement.creer(ed, commandeBudget));
		}
		return lesEngagements;
	}

	/**
	 * On recherche les engagements a supprimer compte tenu de la nouvelle repartition <BR>
	 * 
	 * @param commande commande pour laquelle on fait cette recherche
	 * @param engagementBudget NSArray de EOEngagementBudget crees a partir des commandeBudget
	 * @param utilisateur utilisateur qui supprime l'engagement
	 * @return un NSArray de EOEngagementBudget
	 */
	protected static NSArray engagementsASupprimer(EOCommande commande, NSArray engagementBudgets, EOUtilisateur utilisateur) throws FactoryException {
		NSMutableArray lesEngagements = new NSMutableArray(), arrayQualifier = new NSMutableArray();

		NSArray engagementsInitiaux = (NSArray) commande.commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY);

		if (engagementBudgets.count() == 0)
			return engagementsInitiaux;

		for (int i = 0; i < engagementsInitiaux.count(); i++) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementBudget engagementBudget = (EOEngagementBudget) engagementsInitiaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("organ=%@", new NSArray(engagementBudget.organ())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit=%@", new NSArray(engagementBudget.typeCredit())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("tauxProrata=%@", new NSArray(engagementBudget.tauxProrata())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagementBudget.exercice())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("fournisseur=%@", new NSArray(engagementBudget.fournisseur())));

			NSArray larray = EOQualifier.filteredArrayWithQualifier(engagementBudgets, new EOAndQualifier(arrayQualifier));

			if (larray.count() > 0)
				continue;

			// l'engagement ne sert plus ... on verifie que l'on peut le supprimer
			if (engagementBudget.engMontantBudgetaire().compareTo(engagementBudget.engMontantBudgetaireReste()) != 0 ||
					engagementBudget.depenseBudgets().count() > 0) {
				String message = "un engagement a des liquidations on ne peut pas le supprimer, ligne budgetaire : " +
						engagementBudget.organ().sourceLibelle() + ", type de credit : " + engagementBudget.typeCredit().tcdCode();

				throw new FactoryException(message);
			}
			//Rod 25/11/09 : modif pour coller aux templates velocity
			//engagementBudget.setUtilisateur(utilisateur);
			engagementBudget.setUtilisateurRelationship(utilisateur);

			// on l'ajoute a la liste
			lesEngagements.addObject(engagementBudget);
		}

		return lesEngagements;
	}

	/**
	 * On recherche les engagements a modifier compte tenu de la nouvelle repartition <BR>
	 * 
	 * @param commande Commande pour laquelle on fait cette recherche
	 * @param engagementBudget NSArray de EOEngagementBudget crees a partir des commandeBudget
	 * @param utilisateur utilisateur qui modifie l'engagement
	 * @return un NSArray de EOEngagementBudget
	 */
	protected static NSArray engagementsAModifier(EOCommande commande, NSArray engagementBudgets, EOUtilisateur utilisateur) throws FactoryException {
		NSMutableArray lesEngagements = new NSMutableArray(), arrayQualifier = new NSMutableArray();

		if (engagementBudgets.count() == 0)
			return lesEngagements;

		NSArray engagementsInitiaux = (NSArray) commande.commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY);

		for (int i = 0; i < engagementsInitiaux.count(); i++) {
			// on recherche si l'engagement est dans les nouveaux engagements
			EOEngagementBudget engagementBudget = (EOEngagementBudget) engagementsInitiaux.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("organ=%@", new NSArray(engagementBudget.organ())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit=%@", new NSArray(engagementBudget.typeCredit())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("tauxProrata=%@", new NSArray(engagementBudget.tauxProrata())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagementBudget.exercice())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("fournisseur=%@", new NSArray(engagementBudget.fournisseur())));

			NSArray larray = EOQualifier.filteredArrayWithQualifier(engagementBudgets, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0)
				continue;

			if (larray.count() > 1)
				throw new FactoryException("il y a plusieurs engagements avec les memes donnees budgetaires");

			// on regarde si il y a eu des modifications apportees aux montants et repartitions
			// si il y a eu des modifs la methode renvoie true sinon false

			// si pas de modification on fait un continue
			if (!new ProcessEngagement().modifierEngagement(engagementBudget, (EOEngagementBudget) larray.objectAtIndex(0)))
				continue;

			// on change l'utilisateur par celui qui a fait des modifs
			//Rod 25/11/09 : modif pour coller aux templates velocity
			//			engagementBudget.setUtilisateur(utilisateur);
			engagementBudget.setUtilisateurRelationship(utilisateur);

			// on l'ajoute a la liste
			lesEngagements.addObject(engagementBudget);
		}

		return lesEngagements;
	}

	/**
	 * On recherche les engagements a creer compte tenu de la nouvelle repartition <BR>
	 * 
	 * @param commande Commande pour laquelle on fait cette recherche
	 * @param engagementBudget NSArray de EOEngagementBudget crees a partir des commandeBudget
	 * @param utilisateur utilisateur qui cree l'engagement
	 * @return un NSArray de EOEngagementBudget
	 */
	protected static NSArray engagementsACreer(EOCommande commande, NSArray engagementBudgets, EOUtilisateur utilisateur) throws FactoryException {
		NSMutableArray lesEngagements = new NSMutableArray(), arrayQualifier = new NSMutableArray();

		NSArray engagementsInitiaux = (NSArray) commande.commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY);

		for (int i = 0; i < engagementBudgets.count(); i++) {
			// on recherche si l'engagement est dans les anciens engagements
			EOEngagementBudget engagementBudget = (EOEngagementBudget) engagementBudgets.objectAtIndex(i);

			arrayQualifier = new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("organ=%@", new NSArray(engagementBudget.organ())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit=%@", new NSArray(engagementBudget.typeCredit())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("tauxProrata=%@", new NSArray(engagementBudget.tauxProrata())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(engagementBudget.exercice())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("fournisseur=%@", new NSArray(engagementBudget.fournisseur())));
			NSArray larray = EOQualifier.filteredArrayWithQualifier(engagementsInitiaux, new EOAndQualifier(arrayQualifier));

			if (larray.count() == 0)
				lesEngagements.addObject(engagementBudget);
		}

		return lesEngagements;
	}

	/**
	 * @param databus
	 * @param ed
	 * @param commande
	 * @throws FactoryException
	 */
	protected static void insererCommande(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (commande.devise() == null)
			commande.setDeviseRelationship(FinderDevise.getDeviseEnCours(ed, commande.exercice()));

		// on enregistre dans la table commande
		pbProcedure = ProcedureCreerCommande.enregistrer(databus, commande);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}

		commande.setCommIdProc((Number) dico.valueForKey("010_a_comm_id"));
		//Rod 25/11/09 : modif pour coller aux templates velocity
		//		commande.setCommNumero((Number) dico.valueForKey("030_a_comm_numero"));
		commande.setCommNumero((Integer) dico.valueForKey("030_a_comm_numero"));

		// on enregistre eventuellement dans la table commandeBudget
		insererCommandeBudgets(databus, ed, commande.commandeBudgets());

		// on enregistre eventuellement dans les tables article et articleBudget
		NSArray lesArticlesSansPere = EOQualifier.filteredArrayWithQualifier(commande.articles(),
				EOQualifier.qualifierWithQualifierFormat("article=nil", null));
		insererArticles(databus, ed, lesArticlesSansPere);

		NSArray lesArticlesAvecPere = EOQualifier.filteredArrayWithQualifier(commande.articles(),
				EOQualifier.qualifierWithQualifierFormat("article!=nil", null));

		insererArticles(databus, ed, lesArticlesAvecPere);

		//b2b
		if (commande.toB2bCxmlPunchoutmsgs() != null && commande.toB2bCxmlPunchoutmsgs().count() > 0) {
			EOB2bCxmlPunchoutmsg msg = (EOB2bCxmlPunchoutmsg) commande.toB2bCxmlPunchoutmsgs().objectAtIndex(0);
			msg.setCommId(Integer.valueOf(commande.commIdProc().intValue()));
			pbProcedure = ProcedureCreerB2bCxmlPunchoutmessage.enregistrer(databus, msg);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
		}

	}

	/**
	 * @param databus
	 * @param ed
	 * @param commande
	 * @throws FactoryException
	 */
	protected static void modifierCommande(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (commande.devise() == null)
			commande.setDeviseRelationship(FinderDevise.getDeviseEnCours(ed, commande.exercice()));

		// on enregistre dans la table commande
		pbProcedure = ProcedureModifierCommande.enregistrer(databus, commande);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}

		// gestion des suppressions
		supprimerArticles(databus, ed, ed.deletedObjects());
		supprimerCommandeBudgets(databus, ed, ed.deletedObjects());

		// gestion des insertions
		insererCommandeBudgets(databus, ed, ed.insertedObjects());
		insererArticles(databus, ed, ed.insertedObjects());

		// gestion des modifications
		modifierArticles(databus, ed, ed.updatedObjects());
		modifierCommandeBudgets(databus, ed, ed.updatedObjects());
	}

	/**
	 * @param databus
	 * @param ed
	 * @param commandeBudgets
	 */
	private static void insererCommandeBudgets(_CktlBasicDataBus databus, EOEditingContext ed, NSArray commandeBudgets) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < commandeBudgets.count(); i++) {
			if (commandeBudgets.objectAtIndex(i) instanceof EOCommandeBudget == false)
				continue;

			EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets.objectAtIndex(i);

			miseEnFormeCommandeCtrlMarcheHorsMarche(commandeBudget);

			if (!commandeBudget.isComplet())
				continue;

			pbProcedure = ProcedureCreerCommandeBudget.enregistrer(databus, commandeBudget);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			commandeBudget.setCbudIdProc((Number) dico.valueForKey("010_a_cbud_id"));
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param commandeBudgets
	 * @throws FactoryException
	 */
	private static void modifierCommandeBudgets(_CktlBasicDataBus databus, EOEditingContext ed, NSArray commandeBudgets) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < commandeBudgets.count(); i++) {
			if (commandeBudgets.objectAtIndex(i) instanceof EOCommandeBudget == false)
				continue;

			//ROD 23/04/12 j'ai l'impression qu'on ne passe jamais dans ce code ?

			EOCommandeBudget commandeBudget = (EOCommandeBudget) commandeBudgets.objectAtIndex(i);

			miseEnFormeCommandeCtrlMarcheHorsMarche(commandeBudget);

			if (!commandeBudget.isComplet())
				continue;

			pbProcedure = ProcedureModifierCommandeBudget.enregistrer(databus, commandeBudget);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param deleted
	 * @throws FactoryException
	 */
	private static void supprimerCommandeBudgets(_CktlBasicDataBus databus, EOEditingContext ed, NSArray deleted) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < deleted.count(); i++) {
			if (deleted.objectAtIndex(i) instanceof EOCommandeBudget == false)
				continue;

			EOCommandeBudget commandeBudget = (EOCommandeBudget) deleted.objectAtIndex(i);

			pbProcedure = ProcedureSupprimerCommandeBudget.enregistrer(databus, commandeBudget);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param deleted
	 * @throws FactoryException
	 */
	private static void supprimerArticles(_CktlBasicDataBus databus, EOEditingContext ed, NSArray deleted) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < deleted.count(); i++) {
			if (deleted.objectAtIndex(i) instanceof EOArticle == false)
				continue;

			EOArticle article = (EOArticle) deleted.objectAtIndex(i);

			pbProcedure = ProcedureSupprimerArticle.enregistrer(databus, article);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param articles
	 */
	private static void insererArticles(_CktlBasicDataBus databus, EOEditingContext ed, NSArray articles) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < articles.count(); i++) {
			if (articles.objectAtIndex(i) instanceof EOArticle == false)
				continue;

			EOArticle article = (EOArticle) articles.objectAtIndex(i);

			if (article.commande() == null)
				continue;

			if (article.isCatalogable()) {
				pbProcedure = ProcedureCreerArticleCatalogue.enregistrer(databus, article);
				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}

				EOCatalogueArticle articleCatalogue = new FactoryCatalogueArticle().creer(ed, article.artReference(), article.artLibelle(),
						article.artPrixHt(), article.artPrixTtc(), article.tva(), article.codeExer(),
						null, article.commande().fournisseur(), null);

				articleCatalogue.setCaarIdProc((Number) dico.valueForKey("010_a_caar_id"));
				article.setArticleCatalogueRelationship(articleCatalogue);
			}

			pbProcedure = ProcedureCreerArticle.enregistrer(databus, article);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			article.setArtIdProc((Number) dico.valueForKey("010_a_art_id"));

			//b2b
			if (article.toB2bCxmlItem() != null) {
				pbProcedure = ProcedureCreerB2bCxmlItem.enregistrer(databus, article.toB2bCxmlItem());
				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}

		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param articles
	 * @throws FactoryException
	 */
	private static void modifierArticles(_CktlBasicDataBus databus, EOEditingContext ed, NSArray articles) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < articles.count(); i++) {
			if (articles.objectAtIndex(i) instanceof EOArticle == false)
				continue;

			EOArticle article = (EOArticle) articles.objectAtIndex(i);

			if (article.commande() == null)
				continue;

			pbProcedure = ProcedureModifierArticle.enregistrer(databus, article);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
		}
	}

	/**
	 * @param commandeBudget
	 */
	private static void miseEnFormeCommandeCtrlMarcheHorsMarche(EOCommandeBudget commandeBudget) {
		if (commandeBudget == null)
			return;
		if (commandeBudget.commande() == null)
			return;

		if (commandeBudget.commande().isSurUnMarche()) {
			// si plusieurs attribution... ce pas possible ... on supprime tout
			if (commandeBudget.commandeControleMarches() != null && commandeBudget.commandeControleMarches().count() > 1)
				new FactoryCommandeBudget().supprimerCommandeContoleMarches(commandeBudget);

			// si pas d'attribution ... on cree la repartition
			if (commandeBudget.commandeControleMarches() == null || commandeBudget.commandeControleMarches().count() == 0) {
				if (commandeBudget.commande().tableauMarche().count() == 0)
					return;

				EOAttribution attribution = ((LigneMarche) commandeBudget.commande().tableauMarche().objectAtIndex(0)).attribution();

				new FactoryCommandeControleMarche().creer(commandeBudget.editingContext(), commandeBudget.cbudHtSaisie(),
						commandeBudget.cbudTvaSaisie(), commandeBudget.cbudTtcSaisie(), commandeBudget.cbudMontantBudgetaire(),
						attribution, commandeBudget.exercice(), commandeBudget);
			}

			EOCommandeControleMarche controleMarche = (EOCommandeControleMarche) commandeBudget.commandeControleMarches().objectAtIndex(0);

			controleMarche.setCmarHtSaisie(commandeBudget.cbudHtSaisie());
			controleMarche.setCmarTvaSaisie(commandeBudget.cbudTvaSaisie());
			controleMarche.setCmarTtcSaisie(commandeBudget.cbudTtcSaisie());
			controleMarche.setCmarMontantBudgetaire(commandeBudget.cbudMontantBudgetaire());
		}
	}
}
