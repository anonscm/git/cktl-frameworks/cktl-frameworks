/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeDepensePapier;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeDepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerCommandeDepPapier;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerDepense;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerDepenseDirecte;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerDepenseExtourne;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerReversement;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerReversementPapier;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureReimputerDepense;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSolderEngageSansDroit;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerDepenseBudget;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerDepensePapier;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerReversement;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerReversementPapier;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class ProcessDepense extends Process {

	public static void enregistrerEtAssocier(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, EOCommande commande, EOUtilisateur utilisateur) throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (commande == null)
			throw new FactoryException("il faut passer une commande en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		// on regarde si la depensePapier a une cle
		if (EOUtilities.primaryKeyForObject(ed, depensePapier) != null)
			return;
		if (depensePapier.isReversement())
			return;

		if (depensePapier.depenseBudgets() != null && depensePapier.depenseBudgets().count() > 0)
			throw new FactoryException("cette methode n'est utilisee que pour la presaisie des factures");

		depensePapier.setUtilisateurRelationship(utilisateur);

		try {
			// on lance l'enregistrement
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();

			boolean pbProcedure = false;
			NSDictionary dico = null;

			// insertion de la depensePapier
			pbProcedure = ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier.enregistrer(databus, depensePapier);

			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			depensePapier.setDppIdProc((Number) dico.valueForKey("010_a_dpp_id"));

			// insertion de la depensePapier
			EOCommandeDepensePapier commandeDepensePapier = new FactoryCommandeDepensePapier().creer(ed, commande, depensePapier);
			pbProcedure = ProcedureCreerCommandeDepPapier.enregistrer(databus, commandeDepensePapier);

			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			depensePapier.setDppIdProc((Number) dico.valueForKey("010_a_dpp_id"));

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Enregistre une depense papier et ses depenses budget <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param depensePapier depensePapier a enregistrer
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur) throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		// on enleve ce test car la depensePapier a pu etre pre-enregistree avant la liquidation
		// on regarde si la depensePapier a une cle
		//if (EOUtilities.primaryKeyForObject(ed, depensePapier)!=null)
		//	return;

		if (depensePapier.depenseBudgets() == null || depensePapier.depenseBudgets().count() == 0)
			throw new FactoryException("la depensePapier n'a pas de depenseBudget");

		for (int i = 0; i < depensePapier.depenseBudgets().count(); i++) {
			if (EOUtilities.primaryKeyForObject(ed, (EODepenseBudget) depensePapier.depenseBudgets().objectAtIndex(i)) != null)
				return;
		}

		depensePapier.setUtilisateurRelationship(utilisateur);

		EODepensePapier maDepensePapier = miseEnFormeDepenseBudgets(depensePapier, utilisateur);

		if (maDepensePapier == null) {
			throw new FactoryException("Probleme d'enregistrement !!!!");
		}

		try {
			// on lance l'enregistrement
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();

			boolean pbProcedure = false;
			NSDictionary dico = null;

			// On enleve le test du globalId temporaire, car la depensePapier n'est qu'une copie et donc non enregistree en base
			if (maDepensePapier.dppIdProc() == null /* && maDepensePapier.editingContext().globalIDForObject(maDepensePapier).isTemporary() */) {
				// insertion de la depensePapier
				if (!maDepensePapier.isReversement())
					//					pbProcedure = ProcedureCreerDepensePapier.enregistrer(databus, maDepensePapier);
					pbProcedure = ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier.enregistrer(databus, maDepensePapier);
				else
					pbProcedure = ProcedureCreerReversementPapier.enregistrer(databus, maDepensePapier);

				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
				maDepensePapier.setDppIdProc((Number) dico.valueForKey("010_a_dpp_id"));
			}

			NSMutableArray engagementsASolder = new NSMutableArray();

			// insertion des depenseBudgets
			for (int i = 0; i < maDepensePapier.depenseBudgets().count(); i++) {
				EODepenseBudget depenseBudget = (EODepenseBudget) maDepensePapier.depenseBudgets().objectAtIndex(i);

				if (!maDepensePapier.isReversement())
					pbProcedure = ProcedureCreerDepense.enregistrer(databus, depenseBudget);
				else
					pbProcedure = ProcedureCreerReversement.enregistrer(databus, depenseBudget);

				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}

				if (!maDepensePapier.isReversement() && depenseBudget.solde() && depenseBudget.engagementBudget() != null &&
						!engagementsASolder.containsObject(depenseBudget.engagementBudget())) {
					depenseBudget.engagementBudget().setUtilisateurRelationship(utilisateur);
					engagementsASolder.addObject(depenseBudget.engagementBudget());
				}
			}

			if (!maDepensePapier.isReversement()) {
				for (int i = 0; i < engagementsASolder.count(); i++) {
					pbProcedure = ProcedureSolderEngageSansDroit.enregistrer(databus, (EOEngagementBudget) engagementsASolder.objectAtIndex(i));
					dico = new NSDictionary(databus.executedProcResult());
					if (!pbProcedure) {
						throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
					}
				}
			}

			databus.commitTransaction();
			depensePapier.setDppIdProc(maDepensePapier.dppIdProc());

		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Enregistre une depense papier et ses depenses budget <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param depensePapier depensePapier a enregistrer
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void enregistrerLiquidationDirecte(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier,
			EOOrgan organ, EOTypeCredit typeCredit, String engLibelle, EOTypeApplication typeApplication, EOUtilisateur utilisateur) throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (organ == null)
			throw new FactoryException("il faut passer un organ en parametre");
		if (typeCredit == null)
			throw new FactoryException("il faut passer un typeCredit en parametre");
		if (typeApplication == null)
			throw new FactoryException("il faut passer un typeApplication en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		// on enleve ce test car la depensePapier a pu etre pre-enregistree avant la liquidation
		// on regarde si la depensePapier a une cle
		//if (EOUtilities.primaryKeyForObject(ed, depensePapier)!=null)
		//	return;

		if (depensePapier.depenseBudgets() == null || depensePapier.depenseBudgets().count() == 0)
			throw new FactoryException("la depensePapier n'a pas de depenseBudget");

		for (int i = 0; i < depensePapier.depenseBudgets().count(); i++) {
			if (EOUtilities.primaryKeyForObject(ed, (EODepenseBudget) depensePapier.depenseBudgets().objectAtIndex(i)) != null)
				return;
		}

		depensePapier.setUtilisateurRelationship(utilisateur);

		EODepensePapier maDepensePapier = miseEnFormeDepenseBudgets(depensePapier, utilisateur);

		if (maDepensePapier == null) {
			throw new FactoryException("Probleme d'enregistrement !!!!");
		}

		try {
			// on lance l'enregistrement
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();

			boolean pbProcedure = false;
			NSDictionary dico = null;

			// On enleve le test du globalId temporaire, car la depensePapier n'est qu'une copie et donc non enregistree en base
			if (maDepensePapier.dppIdProc() == null /* && maDepensePapier.editingContext().globalIDForObject(maDepensePapier).isTemporary() */) {
				// insertion de la depensePapier
				if (!maDepensePapier.isReversement())
					//					pbProcedure = ProcedureCreerDepensePapier.enregistrer(databus, maDepensePapier);
					pbProcedure = ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier.enregistrer(databus, maDepensePapier);
				else
					pbProcedure = ProcedureCreerReversementPapier.enregistrer(databus, maDepensePapier);

				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
				maDepensePapier.setDppIdProc((Number) dico.valueForKey("010_a_dpp_id"));
			}

			// insertion des depenseBudgets
			for (int i = 0; i < maDepensePapier.depenseBudgets().count(); i++) {
				EODepenseBudget depenseBudget = (EODepenseBudget) maDepensePapier.depenseBudgets().objectAtIndex(i);

				if (!maDepensePapier.isReversement())
					pbProcedure = ProcedureCreerDepenseDirecte.enregistrer(databus, depenseBudget, depensePapier.fournisseur(), organ, typeCredit, engLibelle, typeApplication);
				else
					pbProcedure = ProcedureCreerReversement.enregistrer(databus, depenseBudget);

				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}

			databus.commitTransaction();
			depensePapier.setDppIdProc(maDepensePapier.dppIdProc());

		} catch (Exception e) {
			maDepensePapier.setDppIdProc(null);
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Enregistre une depense papier et ses depenses budget dans le cadre d'une depense sur crédits d'extourne.<BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param depensePapier depensePapier a enregistrer
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void enregistrerLiquidationSurExtourne(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier,
			EODepenseBudget depenseDefinitiveSurExtourne, EODepenseBudget depenseDefinitiveDepassement, EOUtilisateur utilisateur) throws FactoryException {
		NSMutableArray<EOEngagementBudget> engagementsASolder = new NSMutableArray<EOEngagementBudget>();
		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		// on enleve ce test car la depensePapier a pu etre pre-enregistree avant la liquidation
		// on regarde si la depensePapier a une cle
		//if (EOUtilities.primaryKeyForObject(ed, depensePapier)!=null)
		//	return;

		if (depenseDefinitiveSurExtourne == null)
			throw new FactoryException("il faut definir une depense definitive sur credit d'extourne");

		if (EOUtilities.primaryKeyForObject(ed, depenseDefinitiveSurExtourne) != null)
			return;
		if (depenseDefinitiveDepassement != null && EOUtilities.primaryKeyForObject(ed, depenseDefinitiveDepassement) != null)
			return;

		depensePapier.setUtilisateurRelationship(utilisateur);

		//EODepensePapier maDepensePapier = miseEnFormeDepenseBudgets(depensePapier, utilisateur);
		EODepensePapier maDepensePapier = depensePapier;

		if (maDepensePapier == null)
			throw new FactoryException("Probleme d'enregistrement !!!!");
		maDepensePapier.setUtilisateurRelationship(utilisateur);

		try {
			// on lance l'enregistrement
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();

			boolean pbProcedure = false;
			NSDictionary dico = null;

			// On enleve le test du globalId temporaire, car la depensePapier n'est qu'une copie et donc non enregistree en base
			if (maDepensePapier.dppIdProc() == null /* && maDepensePapier.editingContext().globalIDForObject(maDepensePapier).isTemporary() */) {
				// insertion de la depensePapier
				pbProcedure = ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier.enregistrer(databus, maDepensePapier);

				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure)
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				maDepensePapier.setDppIdProc((Number) dico.valueForKey("010_a_dpp_id"));
			}

			// Insertion des depenseBudgets
			//--> Ajout Rod
			if (depenseDefinitiveSurExtourne.solde() && depenseDefinitiveSurExtourne.engagementBudget() != null &&
					!engagementsASolder.containsObject(depenseDefinitiveSurExtourne.engagementBudget())) {
				depenseDefinitiveSurExtourne.engagementBudget().setUtilisateurRelationship(utilisateur);
				engagementsASolder.addObject(depenseDefinitiveSurExtourne.engagementBudget());
			}
			//<--

			// depense sur extourne
			pbProcedure = ProcedureCreerDepenseExtourne.enregistrer(databus, depenseDefinitiveSurExtourne, null, null, null, null, null);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			// depense de depassement
			if (depenseDefinitiveDepassement != null) {
				pbProcedure = ProcedureCreerDepenseDirecte.enregistrer(databus, depenseDefinitiveDepassement, null, null, null, null, null);
				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}

			//--> Ajout Rod
			for (int i = 0; i < engagementsASolder.count(); i++) {
				pbProcedure = ProcedureSolderEngageSansDroit.enregistrer(databus, (EOEngagementBudget) engagementsASolder.objectAtIndex(i));
				dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}
			//<--

			databus.commitTransaction();
			depensePapier.setDppIdProc(maDepensePapier.dppIdProc());

		} catch (Exception e) {
			maDepensePapier.setDppIdProc(null);
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage(), e);
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param depensePapier
	 * @param utilisateur
	 * @throws FactoryException
	 */
	public static void supprimer(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur) throws FactoryException {
		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.supprimer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			if (depensePapier.depenseBudgets() != null) {
				for (int i = 0; i < depensePapier.depenseBudgets().count(); i++) {
					EODepenseBudget depenseBudget = (EODepenseBudget) depensePapier.depenseBudgets().objectAtIndex(i);
					supprimerSansTransaction(databus, ed, depenseBudget, utilisateur);
				}
			}

			if (depensePapier.preDepenseBudgets() != null && depensePapier.preDepenseBudgets().count() > 0)
				ProcessPreDepense.supprimerSansTransaction(databus, ed, depensePapier, utilisateur);

			// EGE On a precedemment eliminer en base toutes les depenseBudgets
			// le test suivant n'est donc pas utile !
			// suppression eventuellement de la depensePapier si ce la derniere
			if ((depensePapier.depenseBudgets() == null || depensePapier.depenseBudgets().count() == 0) &&
					(depensePapier.preDepenseBudgets() == null || depensePapier.preDepenseBudgets().count() == 0)) {
				boolean pbProcedure;

				if (!depensePapier.isReversement())
					pbProcedure = ProcedureSupprimerDepensePapier.enregistrer(databus, depensePapier);
				else
					pbProcedure = ProcedureSupprimerReversementPapier.enregistrer(databus, depensePapier);

				NSDictionary dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param depenseBudget
	 * @param utilisateur
	 * @throws FactoryException
	 */
	public static void supprimer(_CktlBasicDataBus databus, EOEditingContext ed, EODepenseBudget depenseBudget, EOUtilisateur utilisateur) throws FactoryException {
		if (depenseBudget == null)
			throw new FactoryException("il faut passer une depenseBudget en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.supprimer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			EODepensePapier depensePapier = depenseBudget.depensePapier();

			supprimerSansTransaction(databus, ed, depenseBudget, utilisateur);
			depensePapier.removeFromDepenseBudgetsRelationship(depenseBudget);

			// suppression eventuellement de la depensePapier si ce la derniere
			if (depensePapier.depenseBudgets() == null || depensePapier.depenseBudgets().count() == 0) {
				boolean pbProcedure;

				if (!depensePapier.isReversement())
					pbProcedure = ProcedureSupprimerDepensePapier.enregistrer(databus, depensePapier);
				else
					pbProcedure = ProcedureSupprimerReversementPapier.enregistrer(databus, depensePapier);

				NSDictionary dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}

			databus.commitTransaction();

		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param depenseBudget
	 * @param utilisateur
	 * @throws FactoryException
	 */
	private static void supprimerSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EODepenseBudget depenseBudget, EOUtilisateur utilisateur) throws FactoryException {
		if (depenseBudget.depensePapier() == null)
			throw new FactoryException("la depenseBudget a supprimer n'est pas rattacher a une depensePapier");
		if (!depenseBudget.isSupprimable())
			throw new FactoryException("la depenseBudget n'est pas supprimable et est sans doute rattachee a un mandat");
		depenseBudget.setUtilisateurRelationship(utilisateur);

		depenseBudget.depensePapier().setUtilisateurRelationship(utilisateur);

		// suppression de la depenseBudget
		boolean pbProcedure;

		if (!depenseBudget.depensePapier().isReversement())
			pbProcedure = ProcedureSupprimerDepenseBudget.enregistrer(databus, depenseBudget);
		else
			pbProcedure = ProcedureSupprimerReversement.enregistrer(databus, depenseBudget);

		NSDictionary dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}
		depenseBudget.setDepensePapierRelationship(null);
	}

	/**
	 * Methode redecomposant les depenseBudget pour qu'ils n'aient qu'une seule imputation <BR>
	 * 
	 * @param depensePapier
	 * @param utilisateur
	 */
	private static EODepensePapier miseEnFormeDepenseBudgets(EODepensePapier depensePapier, EOUtilisateur utilisateur) {
		NSArray depenseBudgets = depensePapier.depenseBudgets();

		if (depenseBudgets == null || depenseBudgets.count() == 0)
			return null;

		EODepensePapier temp = new FactoryDepensePapier().copie(depensePapier.editingContext(), depensePapier);
		temp.setCommande(null);

		if (depensePapier.dppNbPiece() == null)
			temp.setDppNbPiece(new Integer(1));
		else
			temp.setDppNbPiece(depensePapier.dppNbPiece());

		for (int i = 0; i < depenseBudgets.count(); i++) {
			EODepenseBudget depenseBudget = (EODepenseBudget) depenseBudgets.objectAtIndex(i);
			depenseBudget.setUtilisateurRelationship(utilisateur);

			// si pas d'imputation on detache la depenseBudget de la depensePapier
			if (depenseBudget.depenseControlePlanComptables() == null || depenseBudget.depenseControlePlanComptables().count() == 0)
				continue;
			if (depenseBudget.depHtSaisie().floatValue() == 0.0 && depenseBudget.depTtcSaisie().floatValue() == 0.0)
				continue;

			for (int j = 0; j < depenseBudget.depenseControlePlanComptables().count(); j++) {
				EODepenseControlePlanComptable depenseControlePlanComptable =
						(EODepenseControlePlanComptable) depenseBudget.depenseControlePlanComptables().objectAtIndex(j);
				depenseBudgetPourDepenseControlePlanComptable(depenseControlePlanComptable, temp);
			}
		}

		return temp;
	}

	/**
	 * @param depenseControlePlanComptable
	 * @param depensePapier
	 */
	private static void depenseBudgetPourDepenseControlePlanComptable(
			EODepenseControlePlanComptable depenseControlePlanComptable, EODepensePapier depensePapier) {
		if (depenseControlePlanComptable == null || depenseControlePlanComptable.depenseBudget() == null)
			return;

		EODepenseBudget depenseBudget = depenseControlePlanComptable.depenseBudget();

		EODepenseBudget nouvelle = new FactoryDepenseBudget().copie(depenseControlePlanComptable.editingContext(), depenseBudget);
		nouvelle.setDepensePapierRelationship(depensePapier);

		// plan comptable
		EODepenseControlePlanComptable depensePlanco = new FactoryDepenseControlePlanComptable().copie(
				depenseControlePlanComptable.editingContext(), depenseControlePlanComptable, nouvelle);
		depensePlanco.ajouterInventaires(depenseControlePlanComptable.inventaires());
		depensePlanco.setPourcentage(new BigDecimal(100.0));

		// on met la depenseBudget a jour avec les montants de la depensePlanco, vu qu'un planco=un budget
		nouvelle.setDepHtSaisie(depenseControlePlanComptable.dpcoHtSaisie());
		nouvelle.setDepTtcSaisie(depenseControlePlanComptable.dpcoTtcSaisie());

		// action
		if (depenseBudget.depenseControleActions() != null && depenseBudget.depenseControleActions().count() > 0) {
			if (depenseBudget.depenseControleActions().count() == 1) {
				EODepenseControleAction depenseControleAction =
						(EODepenseControleAction) depenseBudget.depenseControleActions().objectAtIndex(0);
				new FactoryDepenseControleAction().copie(nouvelle.editingContext(), depenseControleAction, nouvelle);
			}
			else {
				NSArray arrayTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(depenseBudget.depenseControleActions(),
						new NSArray(EOSortOrdering.sortOrderingWithKey(EODepenseControleAction.DACT_TTC_SAISIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));

				for (int i = 0; i < arrayTrie.count(); i++) {
					EODepenseControleAction dep = (EODepenseControleAction) arrayTrie.objectAtIndex(i);
					new FactoryDepenseControleAction().copie(nouvelle.editingContext(), dep, nouvelle);
				}

				nouvelle.corrigerMontantActions();
			}
		}

		// analytique
		if (depenseBudget.depenseControleAnalytiques() != null && depenseBudget.depenseControleAnalytiques().count() > 0) {
			if (depenseBudget.depenseControleAnalytiques().count() == 1) {
				EODepenseControleAnalytique depenseControleAnalytique =
						(EODepenseControleAnalytique) depenseBudget.depenseControleAnalytiques().objectAtIndex(0);

				if (depenseControleAnalytique.codeAnalytique() != null)
					new FactoryDepenseControleAnalytique().copie(nouvelle.editingContext(), depenseControleAnalytique, nouvelle);
			}
			else {
				NSArray arrayTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(depenseBudget.depenseControleAnalytiques(),
						new NSArray(EOSortOrdering.sortOrderingWithKey(EODepenseControleAnalytique.DANA_TTC_SAISIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));

				for (int i = 0; i < arrayTrie.count(); i++) {
					EODepenseControleAnalytique dep = (EODepenseControleAnalytique) arrayTrie.objectAtIndex(i);
					if (dep.codeAnalytique() != null)
						new FactoryDepenseControleAnalytique().copie(nouvelle.editingContext(), dep, nouvelle);
				}

				nouvelle.corrigerMontantAnalytiques();
			}
		}

		// convention
		if (depenseBudget.depenseControleConventions() != null && depenseBudget.depenseControleConventions().count() > 0) {
			if (depenseBudget.depenseControleConventions().count() == 1) {
				EODepenseControleConvention depenseControleConvention =
						(EODepenseControleConvention) depenseBudget.depenseControleConventions().objectAtIndex(0);

				if (depenseControleConvention.convention() != null)
					new FactoryDepenseControleConvention().copie(nouvelle.editingContext(), depenseControleConvention, nouvelle);
			}
			else {
				NSArray arrayTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(depenseBudget.depenseControleConventions(),
						new NSArray(EOSortOrdering.sortOrderingWithKey(EODepenseControleConvention.DCON_TTC_SAISIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));

				for (int i = 0; i < arrayTrie.count(); i++) {
					EODepenseControleConvention dep = (EODepenseControleConvention) arrayTrie.objectAtIndex(i);
					if (dep.convention() != null)
						new FactoryDepenseControleConvention().copie(nouvelle.editingContext(), dep, nouvelle);
				}

				nouvelle.corrigerMontantConventions();
			}
		}

		// hors marche
		if (depenseBudget.depenseControleHorsMarches() != null && depenseBudget.depenseControleHorsMarches().count() > 0) {
			if (depenseBudget.depenseControleHorsMarches().count() == 1) {
				EODepenseControleHorsMarche depenseControleHorsMarche =
						(EODepenseControleHorsMarche) depenseBudget.depenseControleHorsMarches().objectAtIndex(0);
				new FactoryDepenseControleHorsMarche().copie(nouvelle.editingContext(), depenseControleHorsMarche, nouvelle);
			}
			else {
				NSArray arrayTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(depenseBudget.depenseControleHorsMarches(),
						new NSArray(EOSortOrdering.sortOrderingWithKey(EODepenseControleHorsMarche.DHOM_TTC_SAISIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));

				for (int i = 0; i < arrayTrie.count(); i++) {
					EODepenseControleHorsMarche dep = (EODepenseControleHorsMarche) arrayTrie.objectAtIndex(i);
					new FactoryDepenseControleHorsMarche().copie(nouvelle.editingContext(), dep, nouvelle);
				}

				nouvelle.corrigerMontantHorsMarches();
			}
		}

		// marche
		if (depenseBudget.depenseControleMarches() != null && depenseBudget.depenseControleMarches().count() > 0) {
			if (depenseBudget.depenseControleMarches().count() == 1) {
				EODepenseControleMarche depenseControleMarche =
						(EODepenseControleMarche) depenseBudget.depenseControleMarches().objectAtIndex(0);

				new FactoryDepenseControleMarche().copie(nouvelle.editingContext(), depenseControleMarche, nouvelle);
			}
			else {
				NSArray arrayTrie = EOSortOrdering.sortedArrayUsingKeyOrderArray(depenseBudget.depenseControleMarches(),
						new NSArray(EOSortOrdering.sortOrderingWithKey(EODepenseControleMarche.DMAR_TTC_SAISIE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));

				for (int i = 0; i < arrayTrie.count(); i++) {
					EODepenseControleMarche dep = (EODepenseControleMarche) arrayTrie.objectAtIndex(i);
					new FactoryDepenseControleMarche().copie(nouvelle.editingContext(), dep, nouvelle);
				}

				nouvelle.corrigerMontantMarches();
			}
		}
	}

	/**
	 * Enregistre la reimputation ordonnateur d'une liquidation. Effectue differents controles avant appel a la procedure.
	 * 
	 * @param databus
	 * @param ed
	 * @param depenseBudget
	 * @param utilisateur
	 * @throws FactoryException
	 */
	//	public static void enregistrerReimputation(_CktlBasicDataBus databus, EOEditingContext ed, EODepenseBudget depenseBudget, EOUtilisateur utilisateur,
	//			EOOrgan organ, EOTypeCredit typeCredit) throws FactoryException {
	//		enregistrerReimputation(databus, ed, depenseBudget, utilisateur, "", organ, typeCredit);
	//	}

	public static void enregistrerReimputation(_CktlBasicDataBus databus, EOEditingContext ed, EODepenseBudget depenseBudget, EOUtilisateur utilisateur,
			String libelle, EOOrgan organ, EOTypeCredit typeCredit) throws FactoryException {
		//verifier que la reimputation est possible
		//verifier que l'imputation comptable est coherente avec le type de credit
		//		for (int i = 0; i < depenseBudget.depenseControlePlanComptables().count(); i++) {
		//			EODepenseControlePlanComptable dpco = (EODepenseControlePlanComptable) depenseBudget.depenseControlePlanComptables().objectAtIndex(i);
		//			NSArray pcos = depenseBudget.getPlanComptablesPossiblesForReimputation(ed, utilisateur);
		//			if (pcos.indexOfIdenticalObject(dpco.planComptable()) == NSArray.NotFound) {
		//				throw new FactoryException("l'imputation comptable " + dpco.planComptable().pcoNum() + " n'est pas autorisee pour le type de credit " + depenseBudget.source().typeCredit().tcdCode());
		//			}
		//		}
		//
		//		//Verifier que les codes analytiques sont autorises pour la ligne budgetaire
		//		for (int i = 0; i < depenseBudget.depenseControleAnalytiques().count(); i++) {
		//			EODepenseControleAnalytique dpca = (EODepenseControleAnalytique) depenseBudget.depenseControleAnalytiques().objectAtIndex(i);
		//			NSArray cas = depenseBudget.getPlanComptablesPossiblesForReimputation(ed, utilisateur);
		//			if (cas.indexOfIdenticalObject(dpca.codeAnalytique()) == NSArray.NotFound) {
		//				throw new FactoryException("le code analytique " + dpca.codeAnalytique().canCode() + " n'est pas autorise pour la ligne budgetaire " + depenseBudget.source().organ().sourceLibelle());
		//			}
		//		}
		//
		//		//Verifier que les conventions sont autorisees pour la ligne budgetaire
		//		for (int i = 0; i < depenseBudget.depenseControleConventions().count(); i++) {
		//			EODepenseControleConvention dpcc = (EODepenseControleConvention) depenseBudget.depenseControleConventions().objectAtIndex(i);
		//			NSArray cons = depenseBudget.getConventionsPossiblesForReimputation(ed, utilisateur);
		//			if (cons.indexOfIdenticalObject(dpcc.convention()) == NSArray.NotFound) {
		//				throw new FactoryException("la convention " + dpcc.convention().convReference() + " n'est pas autorisee pour la ligne budgetaire " + depenseBudget.source().organ().sourceLibelle());
		//			}
		//		}
		//
		//		//si mandate, verifier que taux de prorata n'a pas été change (sinon pb ecritures comptables)
		//		if (depenseBudget.isMandate().booleanValue()) {
		//			if (!depenseBudget.tauxProrataOld().equals(depenseBudget.tauxProrata())) {
		//				throw new FactoryException("La facture a ete mandatee, vous ne pouvez pas changer le taux de prorata");
		//			}
		//		}

		depenseBudget.setUtilisateurRelationship(utilisateur);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessDepense.enregistrerReimputation : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			boolean pbProcedure = ProcedureReimputerDepense.enregistrer(databus, depenseBudget, libelle, organ, typeCredit);

			if (!pbProcedure) {
				NSDictionary dico = new NSDictionary(databus.executedProcResult());
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();

		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}
}
