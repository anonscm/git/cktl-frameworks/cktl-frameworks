/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerPreDepense;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureServiceFaitEtLiquide;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerPreDepenseBudgets;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

public class ProcessPreDepense extends Process {

	/**
	 * Enregistre une depense papier es ses depenses budget <BR>
	 * 
	 * @param databus CRIBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param depensePapier depensePapier a enregistrer
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur) throws FactoryException {

		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		// on enleve ce test car la depensePapier a pu etre pre-enregistree avant la liquidation
		// on regarde si la depensePapier a une cle
		//if (EOUtilities.primaryKeyForObject(ed, depensePapier)!=null)
		//	return;

		depensePapier.setUtilisateurRelationship(utilisateur);

		try {
			// on lance l'enregistrement
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessPreDepense.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();

			boolean pbProcedure = false;
			NSDictionary dico = null;

			//			pbProcedure = ProcedureCreerDepensePapier.enregistrer(databus, depensePapier);
			pbProcedure = ProcedureCreerDepensePapierAvecInteretsMoratoiresEtServiceFacturier.enregistrer(databus, depensePapier);

			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			if (depensePapier.dppIdProc() == null && depensePapier.editingContext().globalIDForObject(depensePapier).isTemporary())
				depensePapier.setDppIdProc((Number) dico.valueForKey("010_a_dpp_id"));

			ProcedureSupprimerPreDepenseBudgets.enregistrer(databus, depensePapier);

			// insertion des preDepenseBudgets
			if (depensePapier.preDepenseBudgets() != null) {
				for (int i = 0; i < depensePapier.preDepenseBudgets().count(); i++) {
					EOPreDepenseBudget preDepenseBudget = (EOPreDepenseBudget) depensePapier.preDepenseBudgets().objectAtIndex(i);

					pbProcedure = ProcedureCreerPreDepense.enregistrer(databus, preDepenseBudget);
					dico = new NSDictionary(databus.executedProcResult());
					if (!pbProcedure) {
						throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
					}
				}
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			depensePapier.setDppIdProc(null);
			throw new FactoryException(e.getMessage());
		}
	}

	public static void serviceFaitEtLiquide(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, NSTimestamp date, EOUtilisateur utilisateur) throws FactoryException {
		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");
		if (date == null)
			throw new FactoryException("il faut passer une date de service fait en parametre");

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") serviceFaitEtLiquide : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			depensePapier.setUtilisateurRelationship(utilisateur);
			depensePapier.setDppDateServiceFait(date);

			databus.beginTransaction();

			boolean pbProcedure = ProcedureServiceFaitEtLiquide.enregistrer(databus, depensePapier);

			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param depenseBudget
	 * @param utilisateur
	 * @throws FactoryException
	 */
	public static void supprimer(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur) throws FactoryException {
		if (depensePapier == null)
			throw new FactoryException("il faut passer une depensePapier en parametre");
		if (utilisateur == null)
			throw new FactoryException("il faut passer un utilisateur en parametre");

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessPreDepense.supprimer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			supprimerSansTransaction(databus, ed, depensePapier, utilisateur);

			databus.commitTransaction();

		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param preDepenseBudget
	 * @param utilisateur
	 * @throws FactoryException
	 */
	public static void supprimerSansTransaction(_CktlBasicDataBus databus, EOEditingContext ed, EODepensePapier depensePapier, EOUtilisateur utilisateur) throws FactoryException {
		depensePapier.setUtilisateurRelationship(utilisateur);

		// suppression de la depenseBudget
		boolean pbProcedure;

		pbProcedure = ProcedureSupprimerPreDepenseBudgets.enregistrer(databus, depensePapier);
		NSDictionary dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}
	}
}
