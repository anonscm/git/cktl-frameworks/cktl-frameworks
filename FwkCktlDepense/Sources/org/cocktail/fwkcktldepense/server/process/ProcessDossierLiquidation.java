package org.cocktail.fwkcktldepense.server.process;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.sf.procedure.ProcedureCreerDossierLiq;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class ProcessDossierLiquidation extends Process {

	public static Integer enregistrer(_CktlBasicDataBus databus, EOEditingContext ed, NSArray dpcos, EOUtilisateur utilisateur, EOExercice exercice, String gesCode) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		// on enregistre 
		pbProcedure = ProcedureCreerDossierLiq.enregistrer(databus, dpcos, utilisateur, exercice, gesCode);
		dico = new NSDictionary(databus.executedProcResult());
		if (!pbProcedure) {
			throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}

		Integer res = null;
		res = (Integer) dico.valueForKey("a010_a_bor_id");
		return res;
	}
}
