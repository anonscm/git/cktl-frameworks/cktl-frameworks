/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeEngagement;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementBudget;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerCommandeEngagement;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureCreerEngagement;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureModifierEngagement;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSolderEngage;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSolderEngageSansDroit;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerCommandeEngagement;
import org.cocktail.fwkcktldepense.server.procedure.ProcedureSupprimerEngagement;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

public class ProcessEngagement extends Process {

	/**
	 * Enregistre un engagement que ce soit en creation ou en modification <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param engagement engagement a enregistrer
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ed, EOEngagementBudget engagement, EOUtilisateur utilisateur) throws FactoryException {
		boolean enModification = false;

		if (engagement == null)
			throw new FactoryException("il faut passer un engagement en parametre");
		if (databus == null || !CktlDataBus.isDatabaseConnected())
			throw new FactoryException("probleme avec le dataBus");

		// on regarde si la commande a une cle et donc si c'est une creation ou une modification
		if (EOUtilities.primaryKeyForObject(ed, engagement) != null)
			enModification = true;

		//Rod 25/11/09 : modif pour coller aux templates velocity
		//engagement.setUtilisateur(utilisateur);
		engagement.setUtilisateurRelationship(utilisateur);

		// creation d'un nouvel engagement
		if (!enModification) {
			try {
				if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
					System.out.println("Methode (" + utilisateur.nomPrenom()
							+ ") ProcessEngage.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
					_CktlBasicDataBus.adaptorContext().rollbackTransaction();
				}

				databus.beginTransaction();

				boolean pbProcedure = ProcedureCreerEngagement.enregistrer(databus, engagement);
				NSDictionary dico = new NSDictionary(databus.executedProcResult());
				if (!pbProcedure) {
					throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}

				engagement.setEngIdProc((Number) dico.valueForKey("010_a_eng_id"));
				//Rod 25/11/09 : modif pour coller aux templates velocity
				//engagement.setEngNumero((Number) dico.valueForKey("025_a_eng_numero"));
				engagement.setEngNumero((Integer) dico.valueForKey("025_a_eng_numero"));

				databus.commitTransaction();
			} catch (Exception e) {
				databus.rollbackTransaction();
				throw new FactoryException(e.getMessage());
			}

			return;
		}

		// modification d'un engagement precedemment enregistre

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessEngage.enregistrer : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			boolean pbProcedure = ProcedureModifierEngagement.enregistrer(databus, engagement);
			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Supprime l'engagement <BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param engagement engagement a annuler
	 * @param utilisateur utilisateur qui effectue cette operation
	 * @throws FactoryException
	 */
	public static void annuler(_CktlBasicDataBus databus, EOEditingContext ed, EOEngagementBudget engagement, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (engagement == null)
			throw new FactoryException("il faut passer un engagement en parametre");
		if (engagement.isSupprimable(ed, utilisateur, null) == false)
			throw new FactoryException("l'utilisateur ne peut pas annuler cet engagement");

		engagement.setUtilisateurRelationship(utilisateur);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessEngage.annuler : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			pbProcedure = ProcedureSupprimerEngagement.enregistrer(databus, engagement);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Solde l'engagement.<BR>
	 * 
	 * @param databus _CktlBasicDataBus permettant de gerer les transactions
	 * @param ed editingContext dans lequel on enregistre
	 * @param engagement engagement a solder
	 * @param utilisateur utilisateur qui solde l'engagement
	 * @throws FactoryException
	 */
	public static void solder(_CktlBasicDataBus databus, EOEditingContext ed, EOEngagementBudget engagement, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (engagement == null)
			throw new FactoryException("il faut passer un engagement en parametre");

		engagement.setUtilisateurRelationship(utilisateur);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessEngagement.solder : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}

			databus.beginTransaction();

			pbProcedure = ProcedureSolderEngage.enregistrer(databus, engagement);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * Solde l'engagement sans verification des droits utilisateurs<br>
	 * 
	 * @param databus
	 * @param ed
	 * @param engagement
	 * @param utilisateur
	 * @throws FactoryException
	 */
	public static void solderSansDroit(_CktlBasicDataBus databus, EOEditingContext ed, EOEngagementBudget engagement, EOUtilisateur utilisateur) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		if (engagement == null)
			throw new FactoryException("il faut passer un engagement en parametre");

		engagement.setUtilisateurRelationship(utilisateur);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction()) {
				System.out.println("Methode (" + utilisateur.nomPrenom()
						+ ") ProcessEngagement.solder : hasOpenTransaction() --> rollbackTransaction()");
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();
			}
			databus.beginTransaction();
			pbProcedure = ProcedureSolderEngageSansDroit.enregistrer(databus, engagement);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			throw new FactoryException(e.getMessage());
		}
	}

	/**
	 * @param ed
	 * @param commandeBudget
	 * @return
	 */
	public static EOEngagementBudget creer(EOEditingContext ed, EOCommandeBudget commandeBudget) {
		int i = 0;
		EOEngagementBudget engagementBudget;

		// TODO : surement corriger les restants budgetaires et ht suivant les depenses ... pour passer la validation a l'enregistrement

		EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(ed, EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
		if (typeApplication == null)
			throw new FactoryException("Type application " + EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK +
					" (ProcessEngagement.creer(), edc=" + ed + ")");

		// Creation de l'engagementBudget
		engagementBudget = new FactoryEngagementBudget().creer(ed, commandeBudget.commande().commLibelle(), commandeBudget.cbudHtSaisie(),
				commandeBudget.cbudTvaSaisie(), commandeBudget.cbudTtcSaisie(), commandeBudget.cbudMontantBudgetaire(),
				commandeBudget.commande().fournisseur(),
				commandeBudget.organ(), commandeBudget.tauxProrata(),
				typeApplication,
				commandeBudget.typeCredit(), commandeBudget.exercice(), commandeBudget.commande().utilisateur());

		// Creation des engageControleAction
		for (i = 0; i < commandeBudget.commandeControleActions().count(); i++) {
			EOCommandeControleAction commandeControleAction =
					(EOCommandeControleAction) commandeBudget.commandeControleActions().objectAtIndex(i);

			if (commandeControleAction.typeAction() == null)
				continue;
			if (commandeControleAction.cactHtSaisie().floatValue() == 0 && commandeControleAction.cactTvaSaisie().floatValue() == 0
					&& commandeControleAction.cactTtcSaisie().floatValue() == 0)
				continue;

			new FactoryEngagementControleAction().creer(ed, commandeControleAction.cactHtSaisie(),
					commandeControleAction.cactTvaSaisie(), commandeControleAction.cactTtcSaisie(),
					commandeControleAction.cactMontantBudgetaire(), commandeControleAction.typeAction(),
					commandeControleAction.exercice(), engagementBudget);
		}

		// Creation des engageControleAnalytique
		for (i = 0; i < commandeBudget.commandeControleAnalytiques().count(); i++) {
			EOCommandeControleAnalytique commandeControleAnalytique =
					(EOCommandeControleAnalytique) commandeBudget.commandeControleAnalytiques().objectAtIndex(i);

			if (commandeControleAnalytique.codeAnalytique() == null)
				continue;
			if (commandeControleAnalytique.canaHtSaisie().floatValue() == 0 && commandeControleAnalytique.canaTvaSaisie().floatValue() == 0
					&& commandeControleAnalytique.canaTtcSaisie().floatValue() == 0)
				continue;

			new FactoryEngagementControleAnalytique().creer(ed, commandeControleAnalytique.canaHtSaisie(),
					commandeControleAnalytique.canaTvaSaisie(), commandeControleAnalytique.canaTtcSaisie(),
					commandeControleAnalytique.canaMontantBudgetaire(), commandeControleAnalytique.codeAnalytique(),
					commandeControleAnalytique.exercice(), engagementBudget);
		}

		// Creation des engageControleConvention
		for (i = 0; i < commandeBudget.commandeControleConventions().count(); i++) {
			EOCommandeControleConvention commandeControleConvention =
					(EOCommandeControleConvention) commandeBudget.commandeControleConventions().objectAtIndex(i);

			if (commandeControleConvention.convention() == null)
				continue;
			if (commandeControleConvention.cconHtSaisie().floatValue() == 0 && commandeControleConvention.cconTvaSaisie().floatValue() == 0
					&& commandeControleConvention.cconTtcSaisie().floatValue() == 0)
				continue;

			new FactoryEngagementControleConvention().creer(ed, commandeControleConvention.cconHtSaisie(),
					commandeControleConvention.cconTvaSaisie(), commandeControleConvention.cconTtcSaisie(),
					commandeControleConvention.cconMontantBudgetaire(), commandeControleConvention.convention(),
					commandeControleConvention.exercice(), engagementBudget);
		}

		// Creation des engageControlePlanComptables
		for (i = 0; i < commandeBudget.commandeControlePlanComptables().count(); i++) {
			EOCommandeControlePlanComptable commandeControlePlanComptable =
					(EOCommandeControlePlanComptable) commandeBudget.commandeControlePlanComptables().objectAtIndex(i);

			if (commandeControlePlanComptable.planComptable() == null)
				continue;
			if (commandeControlePlanComptable.cpcoHtSaisie().floatValue() == 0 && commandeControlePlanComptable.cpcoTvaSaisie().floatValue() == 0
					&& commandeControlePlanComptable.cpcoTtcSaisie().floatValue() == 0)
				continue;

			new FactoryEngagementControlePlanComptable().creer(ed, commandeControlePlanComptable.cpcoHtSaisie(),
					commandeControlePlanComptable.cpcoTvaSaisie(), commandeControlePlanComptable.cpcoTtcSaisie(),
					commandeControlePlanComptable.cpcoMontantBudgetaire(), commandeControlePlanComptable.planComptable(),
					commandeControlePlanComptable.exercice(), engagementBudget);
		}

		// Creation des engageControleHorsMarches
		for (i = 0; i < commandeBudget.commandeControleHorsMarches().count(); i++) {
			EOCommandeControleHorsMarche commandeControleHorsMarche =
					(EOCommandeControleHorsMarche) commandeBudget.commandeControleHorsMarches().objectAtIndex(i);

			if (commandeControleHorsMarche.codeExer() == null)
				continue;
			if (commandeControleHorsMarche.chomHtSaisie().floatValue() == 0 && commandeControleHorsMarche.chomTvaSaisie().floatValue() == 0
					&& commandeControleHorsMarche.chomTtcSaisie().floatValue() == 0)
				continue;

			new FactoryEngagementControleHorsMarche().creer(ed, commandeControleHorsMarche.chomHtSaisie(),
					commandeControleHorsMarche.chomTvaSaisie(), commandeControleHorsMarche.chomTtcSaisie(),
					commandeControleHorsMarche.chomMontantBudgetaire(), commandeControleHorsMarche.codeExer(),
					commandeControleHorsMarche.typeAchat(), commandeControleHorsMarche.exercice(), engagementBudget);
		}

		// Creation des engageControleMarches
		for (i = 0; i < commandeBudget.commandeControleMarches().count(); i++) {
			EOCommandeControleMarche commandeControleMarche =
					(EOCommandeControleMarche) commandeBudget.commandeControleMarches().objectAtIndex(i);

			if (commandeControleMarche.attribution() == null)
				continue;
			if (commandeControleMarche.cmarHtSaisie().floatValue() == 0 && commandeControleMarche.cmarTvaSaisie().floatValue() == 0
					&& commandeControleMarche.cmarTtcSaisie().floatValue() == 0)
				continue;

			new FactoryEngagementControleMarche().creer(ed, commandeControleMarche.cmarHtSaisie(),
					commandeControleMarche.cmarTvaSaisie(), commandeControleMarche.cmarTtcSaisie(),
					commandeControleMarche.cmarMontantBudgetaire(), commandeControleMarche.attribution(),
					commandeControleMarche.exercice(), engagementBudget);
		}

		return engagementBudget;
	}

	/**
	 * @param databus
	 * @param ed
	 * @param articleBudgets
	 */
	public static void insererEngagementBudgets(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, NSArray engagementBudgets) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < engagementBudgets.count(); i++) {
			EOEngagementBudget engagementBudget = (EOEngagementBudget) engagementBudgets.objectAtIndex(i);
			engagementBudget.setUtilisateurRelationship(commande.utilisateur());

			pbProcedure = ProcedureCreerEngagement.enregistrer(databus, engagementBudget);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			engagementBudget.setEngIdProc((Number) dico.valueForKey("010_a_eng_id"));
			//Rod 25/11/09 : modif pour coller aux templates velocity
			//engagementBudget.setEngNumero((Number) dico.valueForKey("025_a_eng_numero"));
			engagementBudget.setEngNumero((Integer) dico.valueForKey("025_a_eng_numero"));

			EOCommandeEngagement commandeEngagement = new FactoryCommandeEngagement().creer(ed, commande, engagementBudget);

			pbProcedure = ProcedureCreerCommandeEngagement.enregistrer(databus, commandeEngagement);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			commandeEngagement.setComeIdProc((Number) dico.valueForKey("010_a_come_id"));
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param commande
	 * @param engagementBudgets
	 * @throws FactoryException
	 */
	public static void supprimerEngagementBudgets(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, NSArray engagementBudgets) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < engagementBudgets.count(); i++) {
			EOEngagementBudget engagementBudget = (EOEngagementBudget) engagementBudgets.objectAtIndex(i);
			engagementBudget.setUtilisateurRelationship(commande.utilisateur());

			NSArray larray = EOQualifier.filteredArrayWithQualifier(commande.commandeEngagements(),
					EOQualifier.qualifierWithQualifierFormat("engagementBudget=%@", new NSArray(engagementBudget)));
			if (larray.count() == 0)
				continue;

			pbProcedure = ProcedureSupprimerCommandeEngagement.enregistrer(databus, (EOCommandeEngagement) larray.objectAtIndex(0));
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			pbProcedure = ProcedureSupprimerEngagement.enregistrer(databus, engagementBudget);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			new FactoryCommandeEngagement().supprimer(ed, (EOCommandeEngagement) larray.objectAtIndex(0));
		}
	}

	/**
	 * @param databus
	 * @param ed
	 * @param commande
	 * @param engagementBudgets
	 * @throws FactoryException
	 */
	public static void modifierEngagementBudgets(_CktlBasicDataBus databus, EOEditingContext ed, EOCommande commande, NSArray engagementBudgets) throws FactoryException {
		boolean pbProcedure = false;
		NSDictionary dico = null;

		for (int i = 0; i < engagementBudgets.count(); i++) {
			EOEngagementBudget engagementBudget = (EOEngagementBudget) engagementBudgets.objectAtIndex(i);
			engagementBudget.setUtilisateurRelationship(commande.utilisateur());

			pbProcedure = ProcedureModifierEngagement.enregistrer(databus, engagementBudget);
			dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new FactoryException((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
		}
	}

	/**
	 * @param ancien
	 * @param nouveau
	 * @return
	 * @throws FactoryException
	 */
	public boolean modifierEngagement(EOEngagementBudget ancien, EOEngagementBudget nouveau) throws FactoryException {
		boolean isModifs = false;
		BigDecimal montantDepense;

		if (!ancien.organ().equals(nouveau.organ()) || !ancien.typeCredit().equals(nouveau.typeCredit()))
			throw new FactoryException("on ne peut pas modifier la ligne budgetaire et le type de credit d'un engagement");
		if (!ancien.tauxProrata().equals(nouveau.tauxProrata()))
			throw new FactoryException("on ne peut pas modifier le taux de prorata d'un engagement");
		if (!ancien.exercice().equals(nouveau.exercice()))
			throw new FactoryException("on ne peut pas modifier l'exercice d'un engagement");
		if (!ancien.fournisseur().equals(nouveau.fournisseur()))
			throw new FactoryException("on ne peut pas modifier le fournisseur d'un engagement");

		if (ancien.engHtSaisie().compareTo(nouveau.engHtSaisie()) != 0 || ancien.engTvaSaisie().compareTo(nouveau.engTvaSaisie()) != 0 ||
				ancien.engTtcSaisie().compareTo(nouveau.engTtcSaisie()) != 0 || ancien.engMontantBudgetaire().compareTo(nouveau.engMontantBudgetaire()) != 0) {

			montantDepense = ancien.engMontantBudgetaire().subtract(ancien.engMontantBudgetaireReste());

			if (nouveau.engMontantBudgetaire().subtract(montantDepense).floatValue() < 0)
				throw new FactoryException("l'engagement existant est partiellement liquide et le montant budgetaire ne peut pas descendre en dessous de " +
						montantDepense);

			isModifs = true;

			ancien.setEngHtSaisie(nouveau.engHtSaisie());
			ancien.setEngTvaSaisie(nouveau.engTvaSaisie());
			ancien.setEngTtcSaisie(nouveau.engTtcSaisie());
			ancien.setEngMontantBudgetaire(nouveau.engMontantBudgetaire());
			ancien.setEngMontantBudgetaireReste(ancien.engMontantBudgetaire().subtract(montantDepense));
			ancien.setEngDateSaisie(new NSTimestamp());
		}

		if (ProcessEngagementControle.isModifsActions(ancien, nouveau))
			isModifs = true;
		if (ProcessEngagementControle.isModifsAnalytiques(ancien, nouveau))
			isModifs = true;
		if (ProcessEngagementControle.isModifsConventions(ancien, nouveau))
			isModifs = true;
		if (ProcessEngagementControle.isModifsHorsMarches(ancien, nouveau))
			isModifs = true;
		if (ProcessEngagementControle.isModifsMarches(ancien, nouveau))
			isModifs = true;
		if (ProcessEngagementControle.isModifsPlanComptables(ancien, nouveau))
			isModifs = true;

		return isModifs;
	}
}
