/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogue;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCodeMarcheFour;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOGrhumParametres;
import org.cocktail.fwkcktldepense.server.metier.EOPersonne;
import org.cocktail.fwkcktldepense.server.metier.EORepartStructure;
import org.cocktail.fwkcktldepense.server.metier.EOSousTraitant;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderFournisseur extends Finder {

	/**
	 * Recherche de fournisseurs par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : fouNom, fouCode, siret, personneMorale(Boolean), personne <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOFournisseur
	 */
	public static final NSArray<EOFournisseur> getFournisseurs(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray larray;
		NSMutableDictionary mesBindings = new NSMutableDictionary();

		if (bindings != null) {
			mesBindings.addEntriesFromDictionary(bindings);
		}
		mesBindings.setObjectForKey(EOFournisseur.ETAT_VALIDE, "fournisseurValide");
		mesBindings.setObjectForKey(EOFournisseur.ETAT_INSTANCE_VALIDATION, "fournisseurInstanceValidation");

		if (bindings.objectForKey("personneMorale") != null && bindings.objectForKey("personneMorale") instanceof Boolean)
			mesBindings.setObjectForKey("STR", "adrCivilite");

		larray = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOFournisseur.ENTITY_NAME, "Recherche", mesBindings);

		return larray;
	}

	/**
	 * Recherche du fournisseur correspondant au fouCode.<BR>
	 * Bindings pris en compte : fouNom, fouCode, siret, personneMorale(Boolean), personne <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param fouCode code du fournisseur a rechercher
	 * @return EOFournisseur ou null si pas de fournisseur pour le fouCode
	 */
	public static final EOFournisseur getFournisseur(EOEditingContext ed, String fouCode) {
		EOFournisseur fournisseur = null;

		if (fouCode == null || fouCode.equals("")) {
			throw new FactoryException("'fouCode' doit-etre renseigne");
		}

		NSArray larray;
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();

		bindings.setObjectForKey(fouCode, "fouCode");

		larray = getFournisseurs(ed, bindings);
		if (larray != null && larray.count() == 1) {
			fournisseur = (EOFournisseur) larray.lastObject();
		}

		return fournisseur;
	}

	public static final NSArray<EOFournisseur> getFournisseursForRechercheGlobale(EOEditingContext ed, String rechercheGlobale, Boolean personneMorale) {
		NSArray larray;
		EOFetchSpecification fs = new EOFetchSpecification();
		fs.setEntityName(EOFournisseur.ENTITY_NAME);

		NSArray tokens = NSArray.componentsSeparatedByString(rechercheGlobale, " ");
		Enumeration enumTokens = tokens.objectEnumerator();
		NSMutableArray qualifiers = new NSMutableArray();
		while (enumTokens.hasMoreElements()) {
			String unToken = (String) enumTokens.nextElement();
			if (unToken.trim().equals("") == false) {
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFournisseur.FOU_CODE_KEY + " caseInsensitiveLike %@ OR " + EOFournisseur.FOU_NOM_KEY + " caseInsensitiveLike %@ ", new NSArray(new String[] {
						"*" + unToken + "*", "*" + unToken + "*"
				}));
				qualifiers.addObject(qual);
			}
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOFournisseur.FOU_VALIDE_KEY + " = 'O' OR " + EOFournisseur.FOU_VALIDE_KEY + " = 'N'", null);
		EOQualifier qualifierbis = EOQualifier.qualifierWithQualifierFormat(EOFournisseur.FOU_TYPE_KEY + "='F' OR " + EOFournisseur.FOU_TYPE_KEY + "='T'", null);
		qualifiers.addObject(qualifier);
		qualifiers.addObject(qualifierbis);
		if (personneMorale != null) {
			EOQualifier qualPM = EOQualifier.qualifierWithQualifierFormat(EOFournisseur.ADR_CIVILITE_KEY + "='STR'", null);
			qualifiers.addObject(qualPM);
		}
		fs.setQualifier(new EOAndQualifier(qualifiers));
		EOSortOrdering ordFouNom = EOSortOrdering.sortOrderingWithKey(EOFournisseur.FOU_NOM_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
		EOSortOrdering ordFouCode = EOSortOrdering.sortOrderingWithKey(EOFournisseur.FOU_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
		NSArray orderings = new NSArray(new EOSortOrdering[] {
				ordFouNom, ordFouCode
		});
		fs.setSortOrderings(orderings);
		fs.setFetchLimit(50);
		larray = ed.objectsWithFetchSpecification(fs);

		return larray;
	}

	/**
	 * Recherche de fournisseurs obligatoires pour un EOCodeExer.<BR>
	 * Si le tableau retourne est vide c'est qu'il n'y a pas de blocage sur le fournisseur <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param codeExer EOCodeExer pour lequel faire la recherche
	 * @return un NSArray contenant des EOFournisseur
	 */
	public static final NSArray<EOFournisseur> getFournisseursForCodeExer(EOEditingContext ed, EOCodeExer codeExer) {
		if (codeExer == null)
			return NSArray.emptyArray();

		NSMutableDictionary mesBindings = new NSMutableDictionary();
		mesBindings.setObjectForKey(codeExer, EOCodeMarcheFour.CODE_EXER_KEY);
		NSArray larray = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOCodeMarcheFour.ENTITY_NAME, "Recherche", mesBindings);
		return (NSArray) larray.valueForKeyPath(EOCodeMarcheFour.FOURNISSEUR_KEY);
	}

	/**
	 * Recherche les fournisseurs possibles pour une attribution, le fournisseur principal et ses sous traitants <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param attribution EOAttribution pour laquelle faire la recherche
	 * @return un NSArray contenant des EOFournisseur
	 */
	public static final NSArray<EOFournisseur> getFournisseurs(EOEditingContext ed, EOAttribution attribution) {
		NSMutableArray array = new NSMutableArray();
		EOFournisseur fournisseurPrincipal = attribution.fournisseur();
		array.addObject(fournisseurPrincipal);

		NSArray sousTraitants = Finder.fetchArray(EOSousTraitant.ENTITY_NAME,
				EOQualifier.qualifierWithQualifierFormat("attribution=%@", new NSArray(attribution)), null, ed, false);

		if (sousTraitants != null && sousTraitants.count() > 0) {
			NSMutableArray fournisseursSsTraitant = new NSMutableArray((NSArray) sousTraitants.valueForKey(EOSousTraitant.FOURNISSEUR_KEY));

			if (fournisseursSsTraitant.containsObject(fournisseurPrincipal)) {
				fournisseursSsTraitant.removeObject(fournisseurPrincipal);
			}
			array.addObjectsFromArray(fournisseursSsTraitant);
		}
		return array;
	}

	/**
	 * Recherche les fournisseurs internes (Structures) auxquels l'utilisateur appartient<BR>
	 * Si utilisateur=null, la methode renvoie tous les fournisseurs internes<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param utilisateur utilisateur pour lequel on fait la recherche
	 * @return un NSArray contenant des EOFournisseur
	 */
	public static final NSArray<EOFournisseur> getFournisseursInternes(EOEditingContext ed, EOUtilisateur utilisateur) {

		String cStructure = FinderGrhumParametre.getParametreStructureFournisseursInternes(ed);
		if (cStructure == null)
			return NSArray.emptyArray();

		NSMutableDictionary mesBindings = new NSMutableDictionary();
		mesBindings.setObjectForKey(cStructure, EORepartStructure.C_STRUCTURE_CLE_KEY);
		if (utilisateur != null)
			mesBindings.setObjectForKey(utilisateur, EOPersonne.UTILISATEUR_KEY);

		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOFournisseur.ENTITY_NAME, "Recherche", mesBindings);
	}

	/**
	 * Recherche les fournisseurs d'une liste d'articles <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param articles NSArray liste des articles
	 * @return un NSArray contenant des EOFournisseur
	 */
	public static final NSArray<EOFournisseur> getFournisseurs(EOEditingContext ed, NSArray articles) {
		NSMutableArray fournisseurs = new NSMutableArray();
		Enumeration enumArticles = articles.objectEnumerator();
		while (enumArticles.hasMoreElements()) {
			EOCatalogue unArticle = (EOCatalogue) enumArticles.nextElement();
			EOFournisseur unFournisseur = unArticle.fournisseur();
			if (fournisseurs.containsObject(unFournisseur) == false) {
				fournisseurs.addObject(unFournisseur);
			}
		}
		return fournisseurs;
	}

	/**
	 * Recherche des fournisseurs via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : fouCode, fouNom, rechercheGlobale, personneMorale, fournisseurInterne, attribution, codeExer<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des NSDictionary { code = fou_code; nom = fou_nom; libelleFournisseur = code+nom; siret = siret; adresse1;
	 *         adresse2; codepostal; ville; bp; pays; telephones, faxs, ribs }
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<NSDictionary<String, Object>> getRawRowFournisseurs(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, false, bindings), null);
	}

	public static final int getRawRowCountFournisseurs(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nbreFournisseurs = -1;

		NSArray fournisseurs = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, true, bindings), null);

		if (fournisseurs != null && fournisseurs.count() == 1) {
			NSDictionary row = (NSDictionary) fournisseurs.lastObject();
			nbreFournisseurs = ((Number) row.valueForKey("NBREFOURNISSEURS")).intValue();
		}

		return nbreFournisseurs;
	}

	protected static String constructionChaine(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {

		String selectClause = "";
		String fromClause = "";
		String whereClause = "";
		String groupByClause = "";
		String orderClause = "";

		if (isForCount) {
			selectClause = "SELECT count(DISTINCT f.fou_ordre) nbreFournisseurs";

		}
		else {
			selectClause = "SELECT DISTINCT f.fou_code code, f.fou_nom nom, f.FOU_CODE||' '||f.FOU_NOM libelleFournisseur, f.siret siret,";
			selectClause += "f.adr_adresse1 adresse1, f.adr_adresse2 adresse2, decode(f.fou_etranger, 'O',f.adr_cp_etranger,f.adr_cp) codepostal,";
			selectClause += "f.adr_ville ville, f.adr_bp bp, f.pays pays";
			selectClause += ",jefy_depense.get_telephones(f.pers_id) telephones,jefy_depense.get_faxs(f.pers_id) faxs,jefy_depense.get_ribs(f.fou_ordre) ribs";

			orderClause = "ORDER BY f.fou_nom";
		}

		fromClause = "FROM jefy_depense.v_fournisseur f";
		whereClause = "WHERE f.fou_valide<>'" + EOFournisseur.ETAT_ANNULE + "' and f.fou_type in ('F','T')";

		if (bindings.objectForKey("personneMorale") != null && bindings.objectForKey("personneMorale") instanceof Boolean) {
			if (((Boolean) bindings.objectForKey("personneMorale")).booleanValue())
				whereClause += " AND f.adr_civilite='STR'";
			//else
			//	whereClause+=" AND f.adr_civilite<>'STR'";
		}

		if (bindings.objectForKey("fouCode") != null) {
			whereClause += " AND upper(f.fou_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("fouCode")) + "%')";
		}
		if (bindings.objectForKey("fouNom") != null) {
			whereClause += " AND upper(f.fou_nom) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("fouNom")) + "%')";
		}

		if (bindings.objectForKey("rechercheGlobale") != null) {
			NSArray tokens = NSArray.componentsSeparatedByString((String) bindings.objectForKey("rechercheGlobale"), " ");
			Enumeration enumTokens = tokens.objectEnumerator();
			while (enumTokens.hasMoreElements()) {
				String unToken = (String) enumTokens.nextElement();
				if (unToken.trim().equals("") == false) {
					whereClause += " AND (upper(f.fou_code) like upper('%" + ZStringUtil.sqlString(unToken) + "%') OR upper(f.fou_nom) like upper('%" +
							ZStringUtil.sqlString(unToken) + "%'))";
				}
			}
		}

		if (bindings.objectForKey("fournisseurInterne") != null && bindings.objectForKey("fournisseurInterne") instanceof Boolean) {
			if (((Boolean) bindings.objectForKey("fournisseurInterne")).booleanValue()) {
				whereClause += " AND f.pers_id in (select pers_id from grhum.repart_structure where c_structure in ";
				whereClause += "(select param_value from grhum.grhum_parametres where param_key='" + EOGrhumParametres.ANNUAIRE_FOU_VALIDE_INTERNE + "'))";
			}
		}

		if (bindings.objectForKey("codeExer") != null) {
			Number ceOrdre = (Number) EOUtilities.primaryKeyForObject(ed, (EOCodeExer) bindings.objectForKey("codeExer")).objectForKey(EOCodeExer.CE_ORDRE_KEY);
			fromClause += ", jefy_depense.v_code_marche_four cmf";
			whereClause += " AND cmf.fou_ordre=f.fou_ordre and cmf.ce_ordre=" + ceOrdre;
		}

		if (bindings.objectForKey("attribution") != null) {
			Number attOrdre = (Number) EOUtilities.primaryKeyForObject(ed, (EOAttribution) bindings.objectForKey("attribution")).objectForKey(EOAttribution.ATT_ORDRE_KEY);
			fromClause += ", jefy_depense.v_attribution att, jefy_depense.v_sous_traitant stt";
			whereClause += " AND (f.fou_ordre=att.fou_ordre or f.fou_ordre=stt.fou_ordre) and att.att_ordre=stt.att_ordre(+) and att.att_ordre=" + attOrdre;
		}

		return selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + " " + orderClause;
	}

}
