/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public final class FinderInfosReversement extends Finder {

	public static final BigDecimal getSumAction(EOEditingContext ed, EODepenseControleAction depense) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseControleAction.TYPE_ACTION_KEY + "=%@",
				new NSArray(depense.typeAction())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EODepenseControleAction.DEPENSE_BUDGET_KEY + "." + EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY + "=%@",
				new NSArray(depense.depenseBudget())));

		NSArray resultats = Finder.fetchArray(EODepenseControleAction.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		return computeSumForKey(resultats, EODepenseControleAction.DACT_TTC_SAISIE_KEY);
	}

	public static final BigDecimal getSumAnalytique(EOEditingContext ed, EODepenseControleAnalytique depense) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseControleAnalytique.CODE_ANALYTIQUE_KEY + "=%@",
				new NSArray(depense.codeAnalytique())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EODepenseControleAnalytique.DEPENSE_BUDGET_KEY + "." + EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY + "=%@",
				new NSArray(depense.depenseBudget())));

		NSArray resultats = Finder.fetchArray(EODepenseControleAnalytique.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		return computeSumForKey(resultats, EODepenseControleAnalytique.DANA_TTC_SAISIE_KEY);
	}

	public static final BigDecimal getSumConvention(EOEditingContext ed, EODepenseControleConvention depense) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseControleConvention.CONVENTION_KEY + "=%@",
				new NSArray(depense.convention())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EODepenseControleConvention.DEPENSE_BUDGET_KEY + "." + EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY + "=%@",
				new NSArray(depense.depenseBudget())));

		NSArray resultats = Finder.fetchArray(EODepenseControleConvention.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		return computeSumForKey(resultats, EODepenseControleConvention.DCON_TTC_SAISIE_KEY);
	}

	public static final BigDecimal getSumHorsMarche(EOEditingContext ed, EODepenseControleHorsMarche depense) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseControleHorsMarche.CODE_EXER_KEY + "=%@",
				new NSArray(depense.codeExer())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EODepenseControleHorsMarche.DEPENSE_BUDGET_KEY + "." + EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY + "=%@",
				new NSArray(depense.depenseBudget())));

		NSArray resultats = Finder.fetchArray(EODepenseControleHorsMarche.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		return computeSumForKey(resultats, EODepenseControleHorsMarche.DHOM_TTC_SAISIE_KEY);
	}

	public static final BigDecimal getSumMarche(EOEditingContext ed, EODepenseControleMarche depense) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseControleMarche.ATTRIBUTION_KEY + "=%@",
				new NSArray(depense.attribution())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EODepenseControleMarche.DEPENSE_BUDGET_KEY + "." + EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY + "=%@",
				new NSArray(depense.depenseBudget())));

		NSArray resultats = Finder.fetchArray(EODepenseControleMarche.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		return computeSumForKey(resultats, EODepenseControleMarche.DMAR_TTC_SAISIE_KEY);
	}

	public static final BigDecimal getSumPlanComptable(EOEditingContext ed, EODepenseControlePlanComptable depense) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EODepenseControlePlanComptable.PLAN_COMPTABLE_KEY + "=%@",
				new NSArray(depense.planComptable())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EODepenseControlePlanComptable.DEPENSE_BUDGET_KEY + "." + EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY + "=%@",
				new NSArray(depense.depenseBudget())));

		NSArray resultats = Finder.fetchArray(EODepenseControlePlanComptable.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		return computeSumForKey(resultats, EODepenseControlePlanComptable.DPCO_TTC_SAISIE_KEY);
	}

	private static BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}
}
