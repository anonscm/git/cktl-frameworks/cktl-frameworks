/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public final class FinderEngagementBudget extends Finder {

	/**
	 * Recherche d'engagement par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, fournisseur, engNumero, exercice,<BR>
	 * engLibelle, cmCode, cmLib, organUb, organCr, organSsCr, dateDebut, dateFin,<BR>
	 * attLibelle, marLibelle, exercice, fouNom, fouCode, dppNumeroFacture<BR>
	 * minHT, maxHT, minTTC, maxTTC, utilisateurRecherche, typeApplication<BR>
	 * <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOEngagement
	 */
	public static final NSArray getEngagementBudget(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray lesEngagements;

		if (bindings == null)
			throw new FactoryException("les bindings 'exercice' et 'utilisateur' sont obligatoires");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("utilisateur") == null)
			throw new FactoryException("le bindings 'utilisateur' est obligatoire");

		// Retraitement du dico de binding pour ajouter '*' a la fin des bindings 
		// associes a l''operateur caseInsensitiveLike
		NSMutableDictionary newBindings = new NSMutableDictionary(bindings);
		String fouCode = (String) bindings.objectForKey("fouCode");
		if (fouCode != null && fouCode.equals("") == false)
			newBindings.setObjectForKey(fouCode + "*", "fouCode");
		String fouNom = (String) bindings.objectForKey("fouNom");
		if (fouNom != null && fouNom.equals("") == false)
			newBindings.setObjectForKey(fouNom + "*", "fouNom");
		String engLibelle = (String) bindings.objectForKey("engLibelle");
		if (engLibelle != null && engLibelle.equals("") == false)
			newBindings.setObjectForKey(engLibelle + "*", "engLibelle");
		String attLibelle = (String) bindings.objectForKey("attLibelle");
		if (attLibelle != null && attLibelle.equals("") == false)
			newBindings.setObjectForKey(attLibelle + "*", "attLibelle");
		String marLibelle = (String) bindings.objectForKey("marLibelle");
		if (marLibelle != null && marLibelle.equals("") == false)
			newBindings.setObjectForKey(marLibelle + "*", "marLibelle");
		String dppNumeroFacture = (String) bindings.objectForKey("dppNumeroFacture");
		if (dppNumeroFacture != null && dppNumeroFacture.equals("") == false)
			newBindings.setObjectForKey(dppNumeroFacture + "*", "dppNumeroFacture");
		String organUb = (String) bindings.objectForKey("organUb");
		if (organUb != null && organUb.equals("") == false)
			newBindings.setObjectForKey(organUb + "*", "organUb");
		String organCr = (String) bindings.objectForKey("organCr");
		if (organCr != null && organCr.equals("") == false)
			newBindings.setObjectForKey(organCr + "*", "organCr");
		String organSsCr = (String) bindings.objectForKey("organSsCr");
		if (organSsCr != null && organSsCr.equals("") == false)
			newBindings.setObjectForKey(organSsCr + "*", "organSsCr");

		lesEngagements = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOEngagementBudget.ENTITY_NAME, "Recherche", newBindings);

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEngagements, sort());
	}

	public static final EOEngagementBudget getEngagementBudget(EOEditingContext ed, EOExercice exercice, Number numero) {
		EOEngagementBudget engagement = null;
		if (exercice == null || numero == null)
			throw new FactoryException("les arguments 'exercice' et 'numero' sont obligatoires");

		NSMutableDictionary bdgs = new NSMutableDictionary();
		bdgs.setObjectForKey(exercice, "exercice");
		bdgs.setObjectForKey(numero, "engNumero");
		NSArray engagements = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOEngagementBudget.ENTITY_NAME, "Recherche", bdgs);
		if (engagements != null)
			engagement = (EOEngagementBudget) engagements.lastObject();

		return engagement;
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOEngagementBudget.ENG_NUMERO_KEY, EOSortOrdering.CompareDescending));
		return array;
	}

	/**
	 * Recherche des engagements via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : typeApplication, utilisateur, fournisseur, exercice,<BR>
	 * engLibelle, attribution, codeExer, organ, dateDebut, dateFin,<BR>
	 * attLibelle, marLibelle, fouNom, fouCode, engNumero, dppNumeroFacture<BR>
	 * minHT, maxHT, minTTC, maxTTC, utilisateurRecherche, cmCode, cmLib<BR>
	 * minDepHT, maxDepHT, minDepTTC, maxDepTTC, pcoNum, pcoLib, canCode, canLib, convReference, tyacCode, tyacLib <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des NSDictionary { numero = eng_numero; dateCreation = eng_date_saisie; libelle = eng_libelle; prenom =
	 *         Utilisateur.PRENOM; nom = UTILISATEUR.NOM_USUEL; etat = TYET_LIBELLE; codeFournisseur = FOU_CODE; nomFournisseur = FOU_NOM; totalHT =
	 *         eng_ht_saisie; totalTTC = eng_ttc_saisie; typeApplication = tyap_libelle }
	 */
	public static final NSArray<NSDictionary<String, Object>> getRawRowEngagements(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, false, bindings), null);
	}

	public static final int getRawRowCountEngagementsBudgets(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nbreEngagements = -1;
		NSArray engagements = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, true, bindings), null);

		if (engagements != null && engagements.count() == 1) {
			NSDictionary row = (NSDictionary) engagements.lastObject();
			nbreEngagements = ((Number) row.valueForKey("NBREENGAGEMENTS")).intValue();
		}

		return nbreEngagements;
	}

	protected static String constructionChaine(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		String selectClause = "";
		String fromClause = "FROM jefy_depense.engage_budget eb, jefy_depense.v_organ vo, jefy_depense.v_utilisateur_organ vuo, jefy_depense.v_type_etat t, jefy_admin.utilisateur u, grhum.individu_ulr i, jefy_depense.v_fournisseur f";
		String whereClause = " WHERE decode(eb.eng_montant_budgetaire_reste, 0, 210, decode(eb.eng_montant_budgetaire_reste, eb.eng_montant_budgetaire, 208,209))=t.tyet_id";
		whereClause += " AND eb.org_id = vo.org_id(+)";
		whereClause += " AND vo.org_id = vuo.org_id(+)";
		whereClause += " AND eb.utl_ordre=u.utl_ordre";
		whereClause += " AND u.no_individu=i.no_individu";
		whereClause += " AND eb.fou_ordre=f.fou_ordre";
		whereClause += " AND (eb.utl_ordre=" + utlOrdre + " or vuo.utl_ordre=" + utlOrdre + ")";
		whereClause += " AND eb.exe_ordre=" + exeOrdre;
		String orderClause = "";

		if (isForCount) {
			selectClause = "SELECT count(DISTINCT eb.eng_NUMERO) nbreEngagements";

		}
		else {
			selectClause = "SELECT DISTINCT eb.eng_NUMERO numero, tyap.tyap_libelle typeApplication, eb.eng_date_saisie dateCreation, eb.eng_LIBELLE libelle, T.TYET_LIBELLE typeEtat, i.NOM_USUEL||' '||i.PRENOM nomPrenom, f.FOU_CODE||' '||f.FOU_NOM libelleFournisseur, eb.eng_ht_saisie totalHT, eb.eng_ttc_saisie totalTTC";
			fromClause += ", jefy_depense.v_type_application tyap";
			whereClause += " AND eb.tyap_id = tyap.tyap_id";
			orderClause = "ORDER BY eb.eng_numero DESC";
		}

		// modif selon les bindings
		if (bindings.objectForKey("typeApplication") != null) {
			whereClause += " AND eb.tyap_id=" + (Number) EOUtilities.primaryKeyForObject(ed,
					(EOTypeApplication) bindings.objectForKey("typeApplication")).objectForKey(EOTypeApplication.TYAP_ID_KEY);
		}

		if (bindings.objectForKey("pcoNum") != null || bindings.objectForKey("pcoLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_planco engplanco";
			whereClause += " AND eb.eng_id=engplanco.eng_id";

			if (bindings.objectForKey("pcoNum") != null)
				whereClause += " AND engplanco.pco_num like '" + ZStringUtil.sqlString((String) bindings.objectForKey("pcoNum")) + "%'";

			if (bindings.objectForKey("pcoLib") != null) {
				fromClause += ", maracuja.plan_comptable_exer plancompt";
				whereClause += " AND engplanco.pco_num=plancompt.pco_num and engplanco.exe_ordre=plancompt.exe_ordre";
				whereClause += " AND upper(plancompt.pco_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("pcoLib")) + "%')";
			}
		}
		if (bindings.objectForKey("canCode") != null || bindings.objectForKey("canLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_analytique engana, jefy_depense.v_code_analytique codeana";
			whereClause += " AND eb.eng_id=engana.eng_id AND engana.can_id=codeana.can_id";

			if (bindings.objectForKey("canCode") != null)
				whereClause += " AND upper(codeana.can_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("canCode")) + "%')";

			if (bindings.objectForKey("canLib") != null)
				whereClause += " AND upper(codeana.can_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("canLib")) + "%')";
		}
		if (bindings.objectForKey("convReference") != null) {
			fromClause += ", jefy_depense.engage_ctrl_convention engconv, jefy_depense.v_convention convention";
			whereClause += " AND eb.eng_id=engconv.eng_id AND engconv.conv_ordre=convention.conv_ordre";

			if (bindings.objectForKey("convReference") != null)
				whereClause += " AND upper(convention.conv_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("convReference")) + "%')";
		}
		if (bindings.objectForKey("tyacCode") != null || bindings.objectForKey("tyacLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_action engact, jefy_depense.v_type_action typeaction";
			whereClause += " AND eb.eng_id=engact.eng_id AND engact.tyac_id=typeaction.tyac_id";

			if (bindings.objectForKey("tyacCode") != null)
				whereClause += " AND upper(typeaction.tyac_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("tyacCode")) + "%')";

			if (bindings.objectForKey("tyacLib") != null)
				whereClause += " AND upper(typeaction.tyac_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("tyacLib")) + "%')";
		}

		if (bindings.objectForKey("typeEtat") != null) {
			EOTypeEtat typeEtat = (EOTypeEtat) bindings.objectForKey("typeEtat");
			whereClause += " AND t.tyet_id=" + (Number) EOUtilities.primaryKeyForObject(ed, typeEtat).objectForKey(EOTypeEtat.TYET_ID_KEY);
		}
		if (bindings.objectForKey("lesTypesEtat") != null && bindings.objectForKey("typeEtat") == null) {
			NSArray typesEtatLibelle = (NSArray) bindings.objectForKey("lesTypesEtat");
			if (typesEtatLibelle.count() > 0) {
				whereClause += " AND (";
				Enumeration enumTypesEtatLibelle = typesEtatLibelle.objectEnumerator();
				while (enumTypesEtatLibelle.hasMoreElements()) {
					if (whereClause.endsWith("(")) {
						whereClause += " t.tyet_libelle='";
					}
					else {
						whereClause += " OR t.tyet_libelle='";
					}
					String libelle = (String) enumTypesEtatLibelle.nextElement();
					whereClause += libelle + "'";
				}
				whereClause += ")";
			}
		}
		if (bindings.objectForKey("fournisseur") != null) {
			EOFournisseur fournisseur = (EOFournisseur) bindings.objectForKey("fournisseur");
			whereClause += " AND eb.fou_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, fournisseur).objectForKey(EOFournisseur.FOU_ORDRE_KEY);
		}

		if (bindings.objectForKey("codeExer") != null || bindings.objectForKey("cmCode") != null || bindings.objectForKey("cmLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_hors_marche enghom";
			whereClause += " AND eb.eng_id=enghom.eng_id";

			if (bindings.objectForKey("codeExer") != null) {
				EOCodeExer codeExer = (EOCodeExer) bindings.objectForKey("codeExer");
				whereClause += " AND a.ce_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, codeExer).objectForKey(EOCodeExer.CE_ORDRE_KEY);
			}
			if (bindings.objectForKey("cmCode") != null) {
				if (fromClause.indexOf("jefy_marches.code_exer mce") == -1) {
					fromClause += ", jefy_marches.code_exer mce";
					whereClause += " AND enghom.ce_ordre=mce.ce_ordre ";
				}
				if (fromClause.indexOf("jefy_marches.code_marche mcm") == -1) {
					fromClause += ", jefy_marches.code_marche mcm";
					whereClause += " AND mce.cm_ordre=mcm.cm_ordre ";
				}
				whereClause += " AND upper(mcm.cm_code) like upper('" + ZStringUtil.sqlString((String) bindings.objectForKey("cmCode")) + "%')";
			}

			if (bindings.objectForKey("cmLib") != null) {
				if (fromClause.indexOf("jefy_marches.code_exer mce") == -1) {
					fromClause += ", jefy_marches.code_exer mce";
					whereClause += " AND enghom.ce_ordre=mce.ce_ordre ";
				}
				if (fromClause.indexOf("jefy_marches.code_marche mcm") == -1) {
					fromClause += ", jefy_marches.code_marche mcm";
					whereClause += " AND mce.cm_ordre=mcm.cm_ordre ";
				}
				whereClause += " AND upper(mcm.cm_lib) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("cmLib")) + "%')";
			}
		}

		if (bindings.objectForKey("utilisateurRecherche") != null) {
			EOUtilisateur utilisateurRecherche = (EOUtilisateur) bindings.objectForKey("utilisateurRecherche");
			whereClause += " AND eb.utl_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, utilisateurRecherche).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);
		}

		if (bindings.objectForKey("fouCode") != null) {
			whereClause += " AND upper(f.fou_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("fouCode")) + "%')";
		}
		if (bindings.objectForKey("fouNom") != null) {
			whereClause += " AND upper(f.fou_nom) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("fouNom")) + "%')";
		}
		if (bindings.objectForKey("engNumero") != null) {
			whereClause += " AND eb.eng_numero=" + bindings.objectForKey("engNumero");
		}
		if (bindings.objectForKey("engLibelle") != null) {
			whereClause += " AND upper(eb.eng_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("engLibelle")) + "%')";
		}
		if (bindings.objectForKey("organ") != null) {
			EOOrgan organ = (EOOrgan) bindings.objectForKey("organ");
			whereClause += " AND eb.org_id=" + (Number) EOUtilities.primaryKeyForObject(ed, organ).objectForKey(EOOrgan.ORG_ID_KEY);
		}

		if (bindings.objectForKey("organUb") != null || bindings.objectForKey("organCr") != null || bindings.objectForKey("organSsCr") != null) {
			fromClause += ", jefy_admin.organ org";
			whereClause += " AND eb.org_id=org.org_id";

			if (bindings.objectForKey("organUb") != null)
				whereClause += " AND upper(org.org_ub) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organUb")) + "%')";
			if (bindings.objectForKey("organCr") != null)
				whereClause += " AND upper(org.org_cr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organCr")) + "%')";
			if (bindings.objectForKey("organSsCr") != null)
				whereClause += " AND upper(org.org_souscr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organSsCr")) + "%')";
		}

		if (bindings.objectForKey("dateDebut") != null)
			whereClause += " AND to_date(to_char(eb.eng_date_saisie,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateDebut")) + "','dd/mm/yyyy')";
		if (bindings.objectForKey("dateFin") != null)
			whereClause += " AND to_date(to_char(eb.eng_date_saisie,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateFin")) + "','dd/mm/yyyy')";

		if (bindings.objectForKey("attribution") != null || bindings.objectForKey("attLibelle") != null || bindings.objectForKey("marLibelle") != null) {
			fromClause += ", jefy_depense.engage_ctrl_marche engmar";
			whereClause += " AND eb.eng_id=engmar.eng_id";

			if (bindings.objectForKey("attribution") != null) {
				EOAttribution attribution = (EOAttribution) bindings.objectForKey("attribution");
				whereClause += " AND engmar.att_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, attribution).objectForKey(EOAttribution.ATT_ORDRE_KEY);
			}
			else {
				fromClause += ", jefy_marches.attribution att, jefy_marches.lot l, jefy_marches.marche m";
				whereClause += " and engmar.att_ordre=att.att_ordre and att.lot_ordre=l.lot_ordre and m.mar_ordre=l.mar_ordre";

				if (bindings.objectForKey("attLibelle") != null)
					whereClause += " AND upper('('||m.exe_ordre||'/'||m.mar_index||'/'||l.lot_index||')-'||l.lot_libelle) like upper('%" +
							ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("attLibelle")) + "%')";

				if (bindings.objectForKey("marLibelle") != null)
					whereClause += " AND upper(m.exe_ordre||'-'||m.mar_index||'/'||m.mar_libelle) like upper('%" +
							ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("marLibelle")) + "%')";
			}
		}

		if (bindings.objectForKey("dppNumeroFacture") != null || bindings.objectForKey("minDepHT") != null ||
				bindings.objectForKey("maxDepHT") != null || bindings.objectForKey("minDepTTC") != null || bindings.objectForKey("maxDepTTC") != null) {
			if (fromClause.indexOf("depense_budget db") == -1) {
				fromClause += ", jefy_depense.depense_budget db";
				whereClause += " AND eb.eng_id=db.eng_id ";
			}
			if (fromClause.indexOf("depense_papier dp") == -1) {
				fromClause += ", jefy_depense.depense_papier dp";
				whereClause += " AND db.dpp_id=dp.dpp_id ";
			}

			if (bindings.objectForKey("dppNumeroFacture") != null)
				whereClause += " AND upper(dp.dpp_numero_facture) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("dppNumeroFacture")) + "%')";
			if (bindings.objectForKey("minDepHT") != null)
				whereClause += " AND dp.dpp_ht_initial>=" + bindings.objectForKey("minDepHT");
			if (bindings.objectForKey("maxDepHT") != null)
				whereClause += " AND dp.dpp_ht_initial<=" + bindings.objectForKey("maxDepHT");
			if (bindings.objectForKey("minDepTTC") != null)
				whereClause += " AND dp.dpp_ttc_initial>=" + bindings.objectForKey("minDepTTC");
			if (bindings.objectForKey("maxDepTTC") != null)
				whereClause += " AND dp.dpp_ttc_initial<=" + bindings.objectForKey("maxDepTTC");
		}

		if (bindings.objectForKey("minHT") != null)
			whereClause += " AND eb.eng_ht_saisie>=" + bindings.objectForKey("minHT");

		if (bindings.objectForKey("maxHT") != null)
			whereClause += " AND eb.eng_ht_saisie<=" + bindings.objectForKey("maxHT");

		if (bindings.objectForKey("minTTC") != null)
			whereClause += " AND eb.eng_ttc_saisie>=" + bindings.objectForKey("minTTC");

		if (bindings.objectForKey("maxTTC") != null)
			whereClause += " AND eb.eng_ttc_saisie<=" + bindings.objectForKey("maxTTC");

		return selectClause + " " + fromClause + " " + whereClause + " " + orderClause;
	}

}
