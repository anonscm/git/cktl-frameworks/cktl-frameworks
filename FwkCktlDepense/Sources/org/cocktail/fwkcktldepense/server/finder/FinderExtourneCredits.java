package org.cocktail.fwkcktldepense.server.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsCr;
import org.cocktail.fwkcktldepense.server.metier.EOExtourneCreditsUb;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan;
import org.cocktail.fwkcktldepense.server.metier._IDepenseExtourneCredits;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public class FinderExtourneCredits extends Finder {

	private static final FinderExtourneCredits INSTANCE = new FinderExtourneCredits();

	public static FinderExtourneCredits instance() {
		return INSTANCE;
	}

	private FinderExtourneCredits() {
	}

	public List<_IDepenseExtourneCredits> findExtourneCreditsDisponiblesPourNiveauUb(
			EOEditingContext editingContext, EOUtilisateur utilisateur, EOExercice exercice, EOQualifier qualifiers) {
		return findExtourneCreditsDisponiblesPourNiveau(
				editingContext, utilisateur, exercice, EOExtourneCreditsUb.ENTITY_NAME, qualifiers, defaultSort());
	}

	public List<_IDepenseExtourneCredits> findExtourneCreditsDisponiblesPourNiveauCr(
			EOEditingContext editingContext, EOUtilisateur utilisateur, EOExercice exercice, EOQualifier qualifiers) {
		List<EOSortOrdering> sorts = defaultSort();
		sorts.add(sortByCR());
		return findExtourneCreditsDisponiblesPourNiveau(
				editingContext, utilisateur, exercice, EOExtourneCreditsCr.ENTITY_NAME, qualifiers, sorts);
	}

	private List<_IDepenseExtourneCredits> findExtourneCreditsDisponiblesPourNiveau(
			EOEditingContext editingContext, EOUtilisateur utilisateur, EOExercice exercice, String extourneEntityName, EOQualifier qualifiers,
			List<EOSortOrdering> sorts) {
		NSMutableArray<EOQualifier> criteriaArray = new NSMutableArray<EOQualifier>();
		EOQualifier exerciceQual = ERXQ.is(_IDepenseExtourneCredits.TO_EXERCICE_KEY, exercice);
		criteriaArray.add(exerciceQual);
		if (qualifiers != null) {
			criteriaArray.addObjects(qualifiers);
		}
		if (utilisateur != null) {
			criteriaArray.add(ERXQ.equals(_IDepenseExtourneCredits.TO_ORGAN.append(EOOrgan.UTILISATEUR_ORGANS).append(EOUtilisateurOrgan.UTILISATEUR).key(), utilisateur));
		}
		EOQualifier criteria = new EOAndQualifier(criteriaArray);
		return Finder.fetchArray(extourneEntityName, criteria, new NSArray<EOSortOrdering>(sorts), editingContext, true);
	}

	private List<EOSortOrdering> defaultSort() {
		List<EOSortOrdering> sorts = new ArrayList<EOSortOrdering>();
		sorts.add(sortByUB());
		return sorts;
	}

	private EOSortOrdering sortByUB() {
		EOSortOrdering sortNiveauUB = EOSortOrdering.sortOrderingWithKey(
				_IDepenseExtourneCredits.TO_ORGAN.append(EOOrgan.ORG_UB).key(),
				EOSortOrdering.CompareAscending);
		return sortNiveauUB;
	}

	private EOSortOrdering sortByCR() {
		EOSortOrdering sortNiveauUB = EOSortOrdering.sortOrderingWithKey(
				_IDepenseExtourneCredits.TO_ORGAN.append(EOOrgan.ORG_CR).key(),
				EOSortOrdering.CompareAscending);
		return sortNiveauUB;
	}
}
