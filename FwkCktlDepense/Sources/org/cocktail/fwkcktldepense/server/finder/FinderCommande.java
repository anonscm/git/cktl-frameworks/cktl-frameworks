/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFonction;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public final class FinderCommande extends Finder {

	/**
	 * Recherche de commandes par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : typeEtat, utilisateur, fournisseur, commNumero, exercice,<BR>
	 * commReference, commLibelle, attribution, codeExer, organ, dateDebut, dateFin,<BR>
	 * attLibelle, marLibelle, exercice, commNumero, fouNom, fouCode, engNumero, dppNumeroFacture<BR>
	 * minHT, maxHT, minTTC, maxTTC, utilisateurRecherche <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOCommande
	 */
	public static final NSArray<EOCommande> getCommandes1(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray lesCommandes;
		NSMutableArray lesCommandesConsultables = new NSMutableArray();
		EOOrQualifier qualTypeEtat = null;

		if (bindings == null)
			throw new FactoryException("les bindings 'exercice' et 'utilisateur' sont obligatoires");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("createur") == null)
			throw new FactoryException("le bindings 'createur' est obligatoire");
		if (bindings.objectForKey("attribution") != null && bindings.objectForKey("codeExer") != null)
			throw new FactoryException("il faut choisir entre le bindings 'attribution' et 'codeExer'");

		// Retraitement du dico de binding pour ajouter '*' a la fin des bindings 
		// associes a l''operateur caseInsensitiveLike
		NSMutableDictionary newBindings = new NSMutableDictionary(bindings);
		String fouCode = (String) bindings.objectForKey("fouCode");
		if (fouCode != null && fouCode.equals("") == false) {
			newBindings.setObjectForKey(fouCode + "*", "fouCode");
		}
		String fouNom = (String) bindings.objectForKey("fouNom");
		if (fouNom != null && fouNom.equals("") == false) {
			newBindings.setObjectForKey(fouNom + "*", "fouNom");
		}
		String fouReference = (String) bindings.objectForKey("fouReference");
		if (fouReference != null && fouReference.equals("") == false) {
			newBindings.setObjectForKey(fouReference + "*", "fouReference");
		}
		String commLibelle = (String) bindings.objectForKey("commLibelle");
		if (commLibelle != null && commLibelle.equals("") == false) {
			newBindings.setObjectForKey(commLibelle + "*", "commLibelle");
		}
		String attLibelle = (String) bindings.objectForKey("attLibelle");
		if (attLibelle != null && attLibelle.equals("") == false) {
			newBindings.setObjectForKey(attLibelle + "*", "attLibelle");
		}
		String marLibelle = (String) bindings.objectForKey("marLibelle");
		if (marLibelle != null && marLibelle.equals("") == false) {
			newBindings.setObjectForKey(marLibelle + "*", "marLibelle");
		}
		if (bindings.objectForKey("lesTypesEtat") != null) {
			if (bindings.objectForKey("lesTypesEtat") instanceof NSArray) {
				NSArray etats = (NSArray) bindings.objectForKey("lesTypesEtat");
				Enumeration enumTypeEtat = etats.objectEnumerator();
				NSMutableArray qualifiers = new NSMutableArray();
				while (enumTypeEtat.hasMoreElements()) {
					String unEtat = (String) enumTypeEtat.nextElement();
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeEtat.tyetLibelle=%@", new NSArray(unEtat));
					qualifiers.addObject(qual);
				}
				qualTypeEtat = new EOOrQualifier(qualifiers);
				newBindings.removeObjectForKey("typeEtat");
			}
		}
		lesCommandes = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOCommande.ENTITY_NAME, "Recherche", newBindings);

		if (qualTypeEtat != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, qualTypeEtat);
		}
		if (bindings.objectForKey("attribution") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"articles.attribution=%@",
					new NSArray(bindings.objectForKey("attribution"))));
		}

		if (bindings.objectForKey("codeExer") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"articles.codeExer=%@",
					new NSArray(bindings.objectForKey("codeExer"))));
		}

		if (bindings.objectForKey("organ") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"commandeEngagements.engagementBudget.organ=%@",
					new NSArray(bindings.objectForKey("organ"))));
		}

		if (bindings.objectForKey("minHT") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"totalHt>=%@", new NSArray(bindings.objectForKey("minHT"))));
		}

		if (bindings.objectForKey("maxHT") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"totalHt<=%@", new NSArray(bindings.objectForKey("maxHT"))));
		}

		if (bindings.objectForKey("minTTC") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"totalTtc>=%@", new NSArray(bindings.objectForKey("minTTC"))));
		}

		if (bindings.objectForKey("maxTTC") != null) {
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, EOQualifier.qualifierWithQualifierFormat(
					"totalTtc<=%@", new NSArray(bindings.objectForKey("maxTTC"))));
		}

		NSArray organs = new NSArray();
		if (lesCommandes.count() > 0)
			organs = FinderOrgan.getOrgans(ed, bindings);

		for (int i = 0; i < lesCommandes.count(); i++) {
			EOCommande commande = (EOCommande) lesCommandes.objectAtIndex(i);
			if (commande.isConsultable(ed, (EOUtilisateur) bindings.objectForKey("utilisateur"), organs))
				lesCommandesConsultables.addObject(commande);
		}

		return lesCommandesConsultables;
	}

	/**
	 * @param ed : editingContext dans lequel se fait le fetch
	 * @param exercice : exercice ou chercher la commande
	 * @param numero : numero de la commande a fetcher
	 * @return : EOCommande ou null
	 */
	public static EOCommande getCommande(EOEditingContext ed, EOExercice exercice, Number numero) {
		EOCommande commande = null;
		if (exercice == null || numero == null)
			throw new FactoryException("les arguments 'exercice' et 'numero' sont obligatoires");

		NSMutableDictionary bdgs = new NSMutableDictionary();
		bdgs.setObjectForKey(exercice, EOCommande.EXERCICE_KEY);
		bdgs.setObjectForKey(numero, EOCommande.COMM_NUMERO_KEY);
		NSArray commandes = EOUtilities.objectsMatchingValues(ed, EOCommande.ENTITY_NAME, bdgs);
		if (commandes != null) {
			commande = (EOCommande) commandes.lastObject();
		}

		//    	EOQualifier qual = EOQualifier.qualifierToMatchAllValues(bdgs);
		//    	NSArray commandes = Finder.fetchArray(EOCommande.ENTITY_NAME, qual, null, ed, true);
		//    	
		//    	if (commandes != null) {
		//    		commande = (EOCommande)commandes.lastObject();
		//    	}

		return commande;
	}

	/**
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings
	 * @return un NSArray contenant des EOCommande
	 */
	public static final NSArray<EOCommande> getPreCommandes(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray lesPreCommandes = null;

		if (bindings == null)
			throw new FactoryException("les bindings 'exercice' et 'createur' ou 'utilisateur' sont obligatoires");
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("les bindings 'createur' ou 'utilisateur' sont obligatoires");

		// Retraitement du dico de binding pour ajouter '*' a la fin des bindings 
		// associes a l''operateur caseInsensitiveLike
		NSMutableDictionary newBindings = new NSMutableDictionary(bindings);
		String fouCode = (String) bindings.objectForKey("fouCode");
		if (fouCode != null && fouCode.equals("") == false) {
			newBindings.setObjectForKey(fouCode + "*", "fouCode");
		}
		String fouNom = (String) bindings.objectForKey("fouNom");
		if (fouNom != null && fouNom.equals("") == false) {
			newBindings.setObjectForKey(fouNom + "*", "fouNom");
		}
		String commReference = (String) bindings.objectForKey("commReference");
		if (commReference != null && commReference.equals("") == false) {
			newBindings.setObjectForKey(commReference + "*", "commReference");
		}
		String commLibelle = (String) bindings.objectForKey("commLibelle");
		if (commLibelle != null && commLibelle.equals("") == false) {
			newBindings.setObjectForKey(commLibelle + "*", "commLibelle");
		}
		// On ne recupere que des precommandes
		newBindings.setObjectForKey(EOCommande.ETAT_PRECOMMANDE, "lesTypesEtat");

		EOFetchSpecification fs = null;
		if (utilisateur.isUtilisateurAvecDroits(exercice)) {
			fs = EOFetchSpecification.fetchSpecificationNamed("PreCommandesUtilisateurAvecDroits", EOCommande.ENTITY_NAME);
		}
		else {
			fs = EOFetchSpecification.fetchSpecificationNamed("PreCommandesUtilisateurSansDroit", EOCommande.ENTITY_NAME);
		}
		fs = fs.fetchSpecificationWithQualifierBindings(newBindings);
		lesPreCommandes = ed.objectsWithFetchSpecification(fs);
		if (lesPreCommandes != null && lesPreCommandes.count() > 0) {
			BigDecimal totalHTMin = (BigDecimal) bindings.objectForKey("montantHTMin");
			BigDecimal totalHTMax = (BigDecimal) bindings.objectForKey("montantHTMax");
			BigDecimal totalTTCMin = (BigDecimal) bindings.objectForKey("montantTTCMin");
			BigDecimal totalTTCMax = (BigDecimal) bindings.objectForKey("montantTTCMax");
			NSMutableArray qualifiers = new NSMutableArray();
			if (totalHTMin != null) {
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalHt>=%@", new NSArray(totalHTMin));
				qualifiers.addObject(qual);
			}
			if (totalHTMax != null) {
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalHt<=%@", new NSArray(totalHTMax));
				qualifiers.addObject(qual);
			}
			if (totalTTCMin != null) {
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalTtc>=%@", new NSArray(totalTTCMin));
				qualifiers.addObject(qual);
			}
			if (totalTTCMax != null) {
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalTtc<=%@", new NSArray(totalTTCMax));
				qualifiers.addObject(qual);
			}
			if (qualifiers.count() > 0) {
				lesPreCommandes = EOQualifier.filteredArrayWithQualifier(lesPreCommandes, new EOAndQualifier(qualifiers));
			}
		}
		lesPreCommandes = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesPreCommandes, sort());

		return lesPreCommandes;
	}

	/**
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings
	 * @return un NSArray contenant des EOCommande
	 */
	public static final NSArray<EOCommande> getCommandes(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray lesCommandes = null;
		EOOrQualifier qualTypeEtat = null;

		if (bindings == null)
			throw new FactoryException("les bindings 'exercice' et 'createur' ou 'utilisateur' sont obligatoires");
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("les bindings 'createur' ou 'utilisateur' sont obligatoires");
		if (bindings.objectForKey("attribution") != null && bindings.objectForKey("codeExer") != null)
			throw new FactoryException("il faut choisir entre le bindings 'attribution' et 'codeExer'");

		// Seuls les utilisateurs ayant des droits sur au moins un organ ont le droit 
		// de traiter des commandes
		if (utilisateur.isUtilisateurAvecDroits(exercice)) {
			// Retraitement du dico de binding pour ajouter '*' a la fin des bindings 
			// associes a l''operateur caseInsensitiveLike
			NSMutableDictionary newBindings = new NSMutableDictionary(bindings);
			String fouCode = (String) bindings.objectForKey("fouCode");
			if (fouCode != null && fouCode.equals("") == false) {
				newBindings.setObjectForKey(fouCode + "*", "fouCode");
			}
			String fouNom = (String) bindings.objectForKey("fouNom");
			if (fouNom != null && fouNom.equals("") == false) {
				newBindings.setObjectForKey("*" + fouNom + "*", "fouNom");
			}
			String commReference = (String) bindings.objectForKey("commReference");
			if (commReference != null && commReference.equals("") == false) {
				newBindings.setObjectForKey(commReference + "*", "commReference");
			}
			String numFacture = (String) bindings.objectForKey("dppNumeroFacture");
			if (numFacture != null && numFacture.equals("") == false) {
				newBindings.setObjectForKey("*" + numFacture + "*", "dppNumeroFacture");
			}
			String commLibelle = (String) bindings.objectForKey("commLibelle");
			if (commLibelle != null && commLibelle.equals("") == false) {
				newBindings.setObjectForKey("*" + commLibelle + "*", "commLibelle");
			}
			String attLibelle = (String) bindings.objectForKey("attLibelle");
			if (attLibelle != null && attLibelle.equals("") == false) {
				newBindings.setObjectForKey("*" + attLibelle + "*", "attLibelle");
			}
			String marLibelle = (String) bindings.objectForKey("marLibelle");
			if (marLibelle != null && marLibelle.equals("") == false) {
				newBindings.setObjectForKey("*" + marLibelle + "*", "marLibelle");
			}
			String cmCode = (String) bindings.objectForKey("cmCode");
			if (cmCode != null && cmCode.equals("") == false) {
				newBindings.setObjectForKey(cmCode + "*", "cmCode");
			}
			String cmLib = (String) bindings.objectForKey("cmLib");
			if (cmLib != null && cmLib.equals("") == false) {
				newBindings.setObjectForKey("*" + cmLib + "*", "cmLib");
			}
			String organCr = (String) bindings.objectForKey("organCr");
			if (organCr != null && organCr.equals("") == false) {
				newBindings.setObjectForKey(organCr + "*", "organCr");
			}
			String organSsCr = (String) bindings.objectForKey("organSsCr");
			if (organSsCr != null && organSsCr.equals("") == false) {
				newBindings.setObjectForKey(organSsCr + "*", "organSsCr");
			}
			if (bindings.objectForKey("lesTypesEtat") == null) {
				// Par defaut, on recupere toutes les commandes dont l'etat est autre que ETAT_PRECOMMANDE
				NSMutableArray typesEtatLibelle = new NSMutableArray();
				typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE);
				typesEtatLibelle.addObject(EOCommande.ETAT_ENGAGEE);
				typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_SOLDEE);
				typesEtatLibelle.addObject(EOCommande.ETAT_SOLDEE);
				typesEtatLibelle.addObject(EOCommande.ETAT_ANNULEE);
				newBindings.setObjectForKey(typesEtatLibelle, "lesTypesEtat");
			}
			if (newBindings.objectForKey("lesTypesEtat") instanceof NSArray) {
				NSArray etats = (NSArray) newBindings.objectForKey("lesTypesEtat");
				NSMutableArray orQualifiers = new NSMutableArray();
				Enumeration enumTypeEtat = etats.objectEnumerator();
				while (enumTypeEtat.hasMoreElements()) {
					String unEtat = (String) enumTypeEtat.nextElement();
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeEtat.tyetLibelle=%@", new NSArray(unEtat));
					orQualifiers.addObject(qual);
				}
				qualTypeEtat = new EOOrQualifier(orQualifiers);
			}

			EOFetchSpecification fs = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCommande.ENTITY_NAME);
			fs = fs.fetchSpecificationWithQualifierBindings(newBindings);
			if (qualTypeEtat != null) {
				EOQualifier qual = fs.qualifier();
				qual = new EOAndQualifier(new NSArray(new EOQualifier[] {
						qual, qualTypeEtat
				}));
				fs.setQualifier(qual);
			}
			lesCommandes = ed.objectsWithFetchSpecification(fs);
			if (lesCommandes != null && lesCommandes.count() > 0) {
				BigDecimal totalHTMin = (BigDecimal) bindings.objectForKey("montantHTMin");
				BigDecimal totalHTMax = (BigDecimal) bindings.objectForKey("montantHTMax");
				BigDecimal totalTTCMin = (BigDecimal) bindings.objectForKey("montantTTCMin");
				BigDecimal totalTTCMax = (BigDecimal) bindings.objectForKey("montantTTCMax");
				NSMutableArray qualifiers = new NSMutableArray();
				if (totalHTMin != null) {
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalHt>=%@", new NSArray(totalHTMin));
					qualifiers.addObject(qual);
				}
				if (totalHTMax != null) {
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalHt<=%@", new NSArray(totalHTMax));
					qualifiers.addObject(qual);
				}
				if (totalTTCMin != null) {
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalTtc>=%@", new NSArray(totalTTCMin));
					qualifiers.addObject(qual);
				}
				if (totalTTCMax != null) {
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("totalTtc<=%@", new NSArray(totalTTCMax));
					qualifiers.addObject(qual);
				}
				if (qualifiers.count() > 0) {
					lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, new EOAndQualifier(qualifiers));
				}
			}
			lesCommandes = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesCommandes, sort());
		}
		return lesCommandes;
	}

	/**
	 * Recherche des 100 dernieres commandes correspondants aux criteres.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @param rechercheGlobale chaine de caractere contenant les elements a chercher separes par un espace
	 * @return un NSArray contenant des EOCommande
	 */
	public static final NSArray<EOCommande> getPreCommandesForRechercheGlobale(EOEditingContext ed, NSDictionary<String, Object> bindings, String rechercheGlobale) {
		NSArray larray;
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("les bindings 'createur' ou 'utilisateur' sont obligatoires");
		EOFetchSpecification fs = null;
		if (utilisateur.isUtilisateurAvecDroits(exercice)) {
			fs = EOFetchSpecification.fetchSpecificationNamed("PreCommandesUtilisateurAvecDroits", EOCommande.ENTITY_NAME);
		}
		else {
			fs = EOFetchSpecification.fetchSpecificationNamed("PreCommandesUtilisateurSansDroit", EOCommande.ENTITY_NAME);
		}
		// EOFetchSpecification fs = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCommande.ENTITY_NAME);
		fs = fs.fetchSpecificationWithQualifierBindings(bindings);
		EOQualifier qualFs = fs.qualifier();

		NSArray tokens = NSArray.componentsSeparatedByString(rechercheGlobale, " ");
		Enumeration enumTokens = tokens.objectEnumerator();
		NSMutableArray qualifiers = new NSMutableArray();
		while (enumTokens.hasMoreElements()) {
			String unToken = (String) enumTokens.nextElement();
			if (unToken.trim().equals("") == false) {
				NSMutableDictionary bdgs = new NSMutableDictionary();
				try {
					Integer unNumero = Integer.valueOf(unToken);
					bdgs.setObjectForKey(unNumero, "commNumero");
				} catch (NumberFormatException e) {
					// TODO: handle exception
				}
				bdgs.setObjectForKey("*" + unToken + "*", "commReference");
				bdgs.setObjectForKey("*" + unToken + "*", "commLibelle");
				bdgs.setObjectForKey("*" + unToken + "*", "fouCode");
				bdgs.setObjectForKey("*" + unToken + "*", "fouNom");
				EOFetchSpecification fsRechercheGlobale = EOFetchSpecification.fetchSpecificationNamed("RechercheGlobale", EOCommande.ENTITY_NAME);
				fsRechercheGlobale = fsRechercheGlobale.fetchSpecificationWithQualifierBindings(bdgs);
				qualifiers.addObject(fsRechercheGlobale.qualifier());
			}
		}

		qualifiers.addObject(qualFs);
		fs.setQualifier(new EOAndQualifier(qualifiers));
		fs.setFetchLimit(100);
		larray = ed.objectsWithFetchSpecification(fs);

		return larray;
	}

	/**
	 * Recherche des 100 dernieres commandes correspondants aux criteres.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @param rechercheGlobale chaine de caractere contenant les elements a chercher separes par un espace
	 * @return un NSArray contenant des EOCommande
	 */
	public static final NSArray<EOCommande> getCommandesForRechercheGlobale(EOEditingContext ed, NSDictionary<String, Object> bindings, String rechercheGlobale) {
		NSArray larray;
		EOOrQualifier qualTypeEtat = null;
		if (bindings.objectForKey("lesTypesEtat") != null) {
			if (bindings.objectForKey("lesTypesEtat") instanceof NSArray) {
				NSArray etats = (NSArray) bindings.objectForKey("lesTypesEtat");
				Enumeration enumTypeEtat = etats.objectEnumerator();
				NSMutableArray qualifiers = new NSMutableArray();
				while (enumTypeEtat.hasMoreElements()) {
					String unEtat = (String) enumTypeEtat.nextElement();
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeEtat.tyetLibelle=%@", new NSArray(unEtat));
					qualifiers.addObject(qual);
				}
				qualTypeEtat = new EOOrQualifier(qualifiers);
				// newBindings.removeObjectForKey("typeEtat");
			}
		}
		EOFetchSpecification fs = EOFetchSpecification.fetchSpecificationNamed("Recherche", EOCommande.ENTITY_NAME);
		fs = fs.fetchSpecificationWithQualifierBindings(bindings);
		EOQualifier qualFs = fs.qualifier();

		NSArray tokens = NSArray.componentsSeparatedByString(rechercheGlobale, " ");
		Enumeration enumTokens = tokens.objectEnumerator();
		NSMutableArray qualifiers = new NSMutableArray();
		while (enumTokens.hasMoreElements()) {
			String unToken = (String) enumTokens.nextElement();
			if (unToken.trim().equals("") == false) {
				NSMutableDictionary bdgs = new NSMutableDictionary();
				try {
					Integer unNumero = Integer.valueOf(unToken);
					bdgs.setObjectForKey(unNumero, "commNumero");
				} catch (NumberFormatException e) {
					// TODO: handle exception
				}
				bdgs.setObjectForKey("*" + unToken + "*", "commReference");
				bdgs.setObjectForKey("*" + unToken + "*", "commLibelle");
				bdgs.setObjectForKey("*" + unToken + "*", "fouCode");
				bdgs.setObjectForKey("*" + unToken + "*", "fouNom");
				EOFetchSpecification fsRechercheGlobale = EOFetchSpecification.fetchSpecificationNamed("RechercheGlobale", EOCommande.ENTITY_NAME);
				fsRechercheGlobale = fsRechercheGlobale.fetchSpecificationWithQualifierBindings(bdgs);
				qualifiers.addObject(fsRechercheGlobale.qualifier());
			}
		}

		qualifiers.addObject(qualFs);
		fs.setQualifier(new EOAndQualifier(qualifiers));
		if (qualTypeEtat != null) {
			EOQualifier qual = fs.qualifier();
			qual = new EOAndQualifier(new NSArray(new EOQualifier[] {
					qual, qualTypeEtat
			}));
			fs.setQualifier(qual);
		}
		fs.setFetchLimit(100);
		larray = ed.objectsWithFetchSpecification(fs);

		return larray;
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOCommande.COMM_NUMERO_KEY, EOSortOrdering.CompareDescending));
		return array;
	}

	/**
	 * Recherche des commandes via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : typeEtat, utilisateur, fournisseur, commNumero, exercice,<BR>
	 * commReference, commLibelle, attribution, codeExer, organ, dateDebut, dateFin,<BR>
	 * attLibelle, marLibelle, exercice, commNumero, fouNom, fouCode, engNumero, dppNumeroFacture<BR>
	 * minHT, maxHT, minTTC, maxTTC, utilisateurRecherche, cmCode, cmLib<BR>
	 * minDepHT, maxDepHT, minDepTTC, maxDepTTC, pcoNum, pcoLib, canCode, canLib, convReference, tyacCode, tyacLib<BR>
	 * organUb, organCr, organSsCr, artLibelle, artReference <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des NSDictionary { numero = comm_Numero; dateCreation = comm_Date; reference = comm_reference; libelle =
	 *         comm_libelle; prenom = Utilisateur.PRENOM; nom = UTILISATEUR.NOM_USUEL; etat = TYET_LIBELLE; codeFournisseur = FOU_CODE; nomFournisseur
	 *         = FOU_NOM; }
	 */
	public static final NSArray<NSDictionary<String, Object>> getRawRowCommandes(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaineCommande(ed, false, bindings), null);
	}

	public static final int getRawRowCountCommandes(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nbreCommandes = -1;
		NSArray commandes = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaineCommande(ed, true, bindings), null);

		if (commandes != null && commandes.count() == 1) {
			NSDictionary row = (NSDictionary) commandes.lastObject();
			nbreCommandes = ((Number) row.valueForKey("NBRECOMMANDES")).intValue();
		}

		return nbreCommandes;
	}

	protected static String constructionChaineCommande(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {
		boolean isTypeEtatAnnule = false;

		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		if (bindings.objectForKey("lesTypesEtat") != null && bindings.objectForKey("typeEtat") == null) {
			NSArray typesEtatLibelle = (NSArray) bindings.objectForKey("lesTypesEtat");
			for (int i = 0; i < typesEtatLibelle.count(); i++) {
				String lib = (String) typesEtatLibelle.objectAtIndex(i);
				if (lib.equals(EOCommande.ETAT_ANNULEE))
					isTypeEtatAnnule = true;
			}
		}

		// droit ou pas de voir les precommandes
		NSMutableDictionary mesBindings = new NSMutableDictionary();
		mesBindings.setObjectForKey(exercice, "exercice");
		mesBindings.setObjectForKey(utilisateur, "utilisateur");
		mesBindings.setObjectForKey(EOFonction.FONCTION_VOIR_TTES_PRECOMMANDES, "fonIdInterne");
		NSArray larray = FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);

		String sqlString = "";
		String selectClause = "";
		String fromClause = "FROM jefy_depense.commande c, jefy_depense.commande_engagement ce, jefy_depense.engage_budget eb, jefy_admin.organ vo, jefy_admin.utilisateur_organ vuo, jefy_depense.commande_utilisateur cu, jefy_admin.type_etat t";
		if (isTypeEtatAnnule)
			fromClause += ", jefy_admin.utilisateur_organ vuob, jefy_depense.commande_budget cb";
		String whereClause = "WHERE c.exe_ordre=" + exeOrdre;
		whereClause += " and c.tyet_id=t.tyet_id ";
		whereClause += " AND c.comm_id=cu.comm_id(+) ";
		whereClause += " AND c.comm_id=ce.comm_id(+) ";
		whereClause += " AND ce.eng_id = eb.eng_id(+) ";
		whereClause += " AND eb.org_id = vo.org_id(+) ";
		whereClause += " AND vo.org_id = vuo.org_id(+) ";
		if (isTypeEtatAnnule)
			whereClause += " and c.comm_id=cb.comm_id(+) and cb.org_id=vuob.org_id(+)";
		whereClause += " AND (c.utl_ordre=" + utlOrdre + " or vuo.utl_ordre=" + utlOrdre + " or cu.utl_ordre=" + utlOrdre;
		if (larray != null && larray.count() > 0)
			whereClause += " or t.tyet_libelle='PRECOMMANDE'";
		if (isTypeEtatAnnule)
			whereClause += " or vuob.utl_ordre=" + utlOrdre;
		whereClause += ") ";
		String groupByClause = "";
		String orderClause = "";
		String havingClause = "";

		if (isForCount) {
			selectClause = "SELECT count(DISTINCT c.COMM_ID) nbreCommandes";
		}
		else {
			selectClause = "SELECT DISTINCT c.COMM_NUMERO numero, c.COMM_DATE dateCreation, c.COMM_REFERENCE reference, c.COMM_LIBELLE libelle, T.TYET_LIBELLE typeEtat, i.NOM_USUEL||' '||i.PRENOM nomPrenom, f.FOU_CODE||' '||f.FOU_NOM libelleFournisseur, bis.art_id, bis.ART_PRIX_TOTAL_HT totalHT, bis.ART_PRIX_TOTAL_TTC totalTTC";
			fromClause += ", jefy_admin.utilisateur u, grhum.individu_ulr i, jefy_depense.v_fournisseur f, jefy_depense.article bis";
			whereClause += " AND c.utl_ordre=u.utl_ordre ";
			whereClause += " AND u.no_individu=i.no_individu ";
			whereClause += " AND c.fou_ordre=f.fou_ordre and c.comm_id=bis.comm_id ";
			groupByClause = "GROUP BY numero, dateCreation, reference, libelle, typeEtat, nomPrenom, libelleFournisseur";
			orderClause = "ORDER BY dateCreation DESC";
		}

		if (bindings.objectForKey("organUb") != null)
			whereClause += " AND upper(vo.org_ub) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organUb")) + "%')";
		if (bindings.objectForKey("organCr") != null)
			whereClause += " AND upper(vo.org_cr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organCr")) + "%')";
		if (bindings.objectForKey("organSsCr") != null)
			whereClause += " AND upper(vo.org_souscr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organSsCr")) + "%')";

		if (bindings.objectForKey("pcoNum") != null || bindings.objectForKey("pcoLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_planco commplanco";
			whereClause += " AND eb.eng_id=commplanco.eng_id";

			if (bindings.objectForKey("pcoNum") != null)
				whereClause += " AND commplanco.pco_num like '" + ZStringUtil.sqlString((String) bindings.objectForKey("pcoNum")) + "%'";

			if (bindings.objectForKey("pcoLib") != null) {
				fromClause += ", maracuja.plan_comptable_exer plancompt";
				whereClause += " AND commplanco.pco_num=plancompt.pco_num and commplanco.exe_ordre=plancompt.exe_ordre";
				whereClause += " AND upper(plancompt.pco_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("pcoLib")) + "%')";
			}
		}
		if (bindings.objectForKey("canCode") != null || bindings.objectForKey("canLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_analytique commana, jefy_admin.code_analytique codeana";
			whereClause += " AND eb.eng_id=commana.eng_id AND commana.can_id=codeana.can_id";

			if (bindings.objectForKey("canCode") != null)
				whereClause += " AND upper(codeana.can_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("canCode")) + "%')";

			if (bindings.objectForKey("canLib") != null)
				whereClause += " AND upper(codeana.can_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("canLib")) + "%')";
		}
		if (bindings.objectForKey("convReference") != null) {
			fromClause += ", jefy_depense.engage_ctrl_convention commconv, jefy_depense.v_convention convention";
			whereClause += " AND eb.eng_id=commconv.eng_id AND commconv.conv_ordre=convention.conv_ordre";

			if (bindings.objectForKey("convReference") != null)
				whereClause += " AND upper(convention.conv_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("convReference")) + "%')";
		}
		if (bindings.objectForKey("tyacCode") != null || bindings.objectForKey("tyacLib") != null) {
			fromClause += ", jefy_depense.engage_ctrl_action commact, jefy_admin.lolf_nomenclature_depense typeaction";
			whereClause += " AND eb.eng_id=commact.eng_id AND commact.tyac_id=typeaction.lolf_id";

			if (bindings.objectForKey("tyacCode") != null)
				whereClause += " AND upper(typeaction.lolf_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("tyacCode")) + "%')";

			if (bindings.objectForKey("tyacLib") != null)
				whereClause += " AND upper(typeaction.lolf_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("tyacLib")) + "%')";
		}

		if (bindings.objectForKey("typeEtat") != null) {
			EOTypeEtat typeEtat = (EOTypeEtat) bindings.objectForKey("typeEtat");
			whereClause += " AND c.tyet_id=" + (Number) EOUtilities.primaryKeyForObject(ed, typeEtat).objectForKey(EOTypeEtat.TYET_ID_KEY);
		}
		if (bindings.objectForKey("lesTypesEtat") != null && bindings.objectForKey("typeEtat") == null) {
			NSArray typesEtatLibelle = (NSArray) bindings.objectForKey("lesTypesEtat");
			if (typesEtatLibelle.count() > 0) {
				if (fromClause.indexOf("jefy_admin.type_etat t") == -1) {
					fromClause += ", jefy_admin.type_etat t";
					whereClause += " AND c.tyet_id=t.tyet_id ";
				}

				whereClause += " AND (";
				Enumeration enumTypesEtatLibelle = typesEtatLibelle.objectEnumerator();
				while (enumTypesEtatLibelle.hasMoreElements()) {
					if (whereClause.endsWith("(")) {
						whereClause += " t.tyet_libelle='";
					}
					else {
						whereClause += " OR t.tyet_libelle='";
					}
					String libelle = (String) enumTypesEtatLibelle.nextElement();
					whereClause += libelle + "'";
				}
				whereClause += ")";
			}
		}
		if (bindings.objectForKey("fournisseur") != null) {
			EOFournisseur fournisseur = (EOFournisseur) bindings.objectForKey("fournisseur");
			whereClause += " AND c.fou_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, fournisseur).objectForKey(EOFournisseur.FOU_ORDRE_KEY);
		}
		if (bindings.objectForKey("codeExer") != null) {
			EOCodeExer codeExer = (EOCodeExer) bindings.objectForKey("codeExer");
			if (fromClause.indexOf("article a") == -1) {
				fromClause += ", jefy_depense.article a";
				whereClause += " AND c.comm_id=a.comm_id ";
			}
			whereClause += " AND a.ce_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, codeExer).objectForKey(EOCodeExer.CE_ORDRE_KEY);
		}
		if (bindings.objectForKey("artLibelle") != null) {
			if (fromClause.indexOf("article a") == -1) {
				fromClause += ", jefy_depense.article a";
				whereClause += " AND c.comm_id=a.comm_id ";
			}
			whereClause += " AND (upper(a.art_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("artLibelle")) + "%')";
			whereClause += " OR upper(a.art_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("artLibelle")) + "%'))";
		}
		if (bindings.objectForKey("artReference") != null) {
			if (fromClause.indexOf("article a") == -1) {
				fromClause += ", jefy_depense.article a";
				whereClause += " AND c.comm_id=a.comm_id ";
			}
			whereClause += " AND upper(a.art_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("artReference")) + "%')";
		}

		if (bindings.objectForKey("cmCode") != null) {
			if (fromClause.indexOf("article a") == -1) {
				fromClause += ", jefy_depense.article a";
				whereClause += " AND c.comm_id=a.comm_id ";
			}
			if (fromClause.indexOf("jefy_marches.code_exer mce") == -1) {
				fromClause += ", jefy_marches.code_exer mce";
				whereClause += " AND a.ce_ordre=mce.ce_ordre ";
			}
			if (fromClause.indexOf("jefy_marches.code_marche mcm") == -1) {
				fromClause += ", jefy_marches.code_marche mcm";
				whereClause += " AND mce.cm_ordre=mcm.cm_ordre ";
			}
			whereClause += " AND upper(mcm.cm_code) like upper('" + ZStringUtil.sqlString((String) bindings.objectForKey("cmCode")) + "%')";
		}
		if (bindings.objectForKey("cmLib") != null) {
			if (fromClause.indexOf("article a") == -1) {
				fromClause += ", jefy_depense.article a";
				whereClause += " AND c.comm_id=a.comm_id ";
			}
			if (fromClause.indexOf("jefy_marches.code_exer mce") == -1) {
				fromClause += ", jefy_marches.code_exer mce";
				whereClause += " AND a.ce_ordre=mce.ce_ordre ";
			}
			if (fromClause.indexOf("jefy_marches.code_marche mcm") == -1) {
				fromClause += ", jefy_marches.code_marche mcm";
				whereClause += " AND mce.cm_ordre=mcm.cm_ordre ";
			}
			whereClause += " AND upper(mcm.cm_lib) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("cmLib")) + "%')";
		}
		if (bindings.objectForKey("utilisateurRecherche") != null) {
			EOUtilisateur utilisateurRecherche = (EOUtilisateur) bindings.objectForKey("utilisateurRecherche");
			whereClause += " AND c.utl_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, utilisateurRecherche).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);
		}

		if (bindings.objectForKey("fouCode") != null) {
			if (fromClause.indexOf("v_fournisseur f") == -1) {
				fromClause += ", jefy_depense.v_fournisseur f";
				whereClause += " AND c.fou_ordre=f.fou_ordre ";
			}

			whereClause += " AND upper(f.fou_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("fouCode")) + "%')";
		}
		if (bindings.objectForKey("fouNom") != null) {
			if (fromClause.indexOf("v_fournisseur f") == -1) {
				fromClause += ", jefy_depense.v_fournisseur f";
				whereClause += " AND c.fou_ordre=f.fou_ordre ";
			}
			whereClause += " AND upper(f.fou_nom) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("fouNom")) + "%')";
		}
		if (bindings.objectForKey("commNumero") != null) {
			whereClause += " AND c.comm_numero=" + bindings.objectForKey("commNumero");
		}
		if (bindings.objectForKey("commReference") != null) {
			whereClause += " AND upper(c.comm_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("commReference")) + "%')";
		}
		if (bindings.objectForKey("commLibelle") != null) {
			whereClause += " AND upper(c.comm_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("commLibelle")) + "%')";
		}
		if (bindings.objectForKey("engNumero") != null) {
			whereClause += " AND eb.eng_numero=" + bindings.objectForKey("engNumero");
		}
		if (bindings.objectForKey("organ") != null) {
			EOOrgan organ = (EOOrgan) bindings.objectForKey("organ");
			whereClause += " AND eb.org_id=" + (Number) EOUtilities.primaryKeyForObject(ed, organ).objectForKey(EOOrgan.ORG_ID_KEY);
		}

		if (bindings.objectForKey("dateDebut") != null)
			whereClause += " AND to_date(to_char(c.comm_date,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateDebut")) + "','dd/mm/yyyy')";
		if (bindings.objectForKey("dateFin") != null)
			whereClause += " AND to_date(to_char(c.comm_date,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateFin")) + "','dd/mm/yyyy')";

		if (bindings.objectForKey("attribution") != null || bindings.objectForKey("attLibelle") != null ||
				bindings.objectForKey("marLibelle") != null) {
			if (fromClause.indexOf("article a") == -1) {
				fromClause += ", jefy_depense.article a";
				whereClause += " AND c.comm_id=a.comm_id ";
			}

			if (bindings.objectForKey("attribution") != null) {
				EOAttribution attribution = (EOAttribution) bindings.objectForKey("attribution");
				whereClause += " AND a.att_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, attribution).objectForKey(EOAttribution.ATT_ORDRE_KEY);
			}
			else {
				fromClause += ", jefy_marches.attribution att, jefy_marches.lot l, jefy_marches.marche m";
				whereClause += " and a.att_ordre=att.att_ordre and att.lot_ordre=l.lot_ordre and m.mar_ordre=l.mar_ordre";

				if (bindings.objectForKey("attLibelle") != null)
					whereClause += " AND upper('('||m.exe_ordre||'/'||m.mar_index||'/'||l.lot_index||')-'||l.lot_libelle) like upper('%" +
							ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("attLibelle")) + "%')";

				if (bindings.objectForKey("marLibelle") != null)
					whereClause += " AND upper(m.exe_ordre||'-'||m.mar_index||'/'||m.mar_libelle) like upper('%" +
							ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("marLibelle")) + "%')";
			}
		}

		if (bindings.objectForKey("dppNumeroFacture") != null || bindings.objectForKey("minDepHT") != null ||
				bindings.objectForKey("maxDepHT") != null || bindings.objectForKey("minDepTTC") != null || bindings.objectForKey("maxDepTTC") != null) {
			if (fromClause.indexOf("depense_budget db") == -1) {
				fromClause += ", jefy_depense.depense_budget db";
				whereClause += " AND eb.eng_id=db.eng_id ";
			}
			if (fromClause.indexOf("depense_papier dp") == -1) {
				fromClause += ", jefy_depense.depense_papier dp";
				whereClause += " AND db.dpp_id=dp.dpp_id ";
			}

			if (bindings.objectForKey("dppNumeroFacture") != null)
				whereClause += " AND upper(dp.dpp_numero_facture) like upper('%" + bindings.objectForKey("dppNumeroFacture") + "%')";
			if (bindings.objectForKey("minDepHT") != null)
				whereClause += " AND dp.dpp_ht_initial>=" + bindings.objectForKey("minDepHT");
			if (bindings.objectForKey("maxDepHT") != null)
				whereClause += " AND dp.dpp_ht_initial<=" + bindings.objectForKey("maxDepHT");
			if (bindings.objectForKey("minDepTTC") != null)
				whereClause += " AND dp.dpp_ttc_initial>=" + bindings.objectForKey("minDepTTC");
			if (bindings.objectForKey("maxDepTTC") != null)
				whereClause += " AND dp.dpp_ttc_initial<=" + bindings.objectForKey("maxDepTTC");
		}

		if (isForCount) {
			if (bindings.objectForKey("minHT") != null)
				whereClause += " and (select SUM(ART_PRIX_TOTAL_HT) from jefy_depense.article where comm_id=c.comm_id)>=" + bindings.objectForKey("minHT");
			if (bindings.objectForKey("maxHT") != null)
				whereClause += " and (select SUM(ART_PRIX_TOTAL_HT) from jefy_depense.article where comm_id=c.comm_id)<=" + bindings.objectForKey("maxHT");
			if (bindings.objectForKey("minTTC") != null)
				whereClause += " and (select SUM(ART_PRIX_TOTAL_TTC) from jefy_depense.article where comm_id=c.comm_id)>=" + bindings.objectForKey("minTTC");
			if (bindings.objectForKey("maxTTC") != null)
				whereClause += " and (select SUM(ART_PRIX_TOTAL_TTC) from jefy_depense.article where comm_id=c.comm_id)<=" + bindings.objectForKey("maxTTC");
		}
		else {
			if (bindings.objectForKey("minHT") != null) {
				if (fromClause.indexOf("article bis") == -1) {
					fromClause += ", jefy_depense.article bis";
					whereClause += " AND c.comm_id=bis.comm_id ";
				}

				if (havingClause.equals(""))
					havingClause = "having ";
				else
					havingClause += " and ";

				havingClause += "SUM(totalHT)>=" + bindings.objectForKey("minHT");
			}
			if (bindings.objectForKey("maxHT") != null) {
				if (fromClause.indexOf("article bis") == -1) {
					fromClause += ", jefy_depense.article bis";
					whereClause += " AND c.comm_id=bis.comm_id ";
				}

				if (havingClause.equals(""))
					havingClause = "having ";
				else
					havingClause += " and ";

				havingClause += "SUM(totalHT)<=" + bindings.objectForKey("maxHT");
			}
			if (bindings.objectForKey("minTTC") != null) {
				if (fromClause.indexOf("article bis") == -1) {
					fromClause += ", jefy_depense.article bis";
					whereClause += " AND c.comm_id=bis.comm_id ";
				}

				if (havingClause.equals(""))
					havingClause = "having ";
				else
					havingClause += " and ";

				havingClause += "SUM(totalTTC)>=" + bindings.objectForKey("minTTC");
			}
			if (bindings.objectForKey("maxTTC") != null) {
				if (fromClause.indexOf("article bis") == -1) {
					fromClause += ", jefy_depense.article bis";
					whereClause += " AND c.comm_id=bis.comm_id ";
				}

				if (havingClause.equals(""))
					havingClause = "having ";
				else
					havingClause += " and ";

				havingClause += "SUM(totalTTC)<=" + bindings.objectForKey("maxTTC");
			}
		}

		if (isForCount) {
			sqlString = selectClause + " " + fromClause + " " + whereClause;
		}
		else {
			sqlString = "select numero, dateCreation, reference, libelle, typeEtat, nomPrenom, libelleFournisseur, sum(totalHT) totalHT, sum(totalTTC) totalTTC from (";
			sqlString += selectClause + " " + fromClause + " " + whereClause + ") " + groupByClause + " " + havingClause + " " + orderClause;
		}

		return sqlString;
	}

	/**
	 * Recherche des PRE-commandes via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : typeEtat, utilisateur, fournisseur, commNumero, exercice,<BR>
	 * commReference, commLibelle, attribution, codeExer, organ, dateDebut, dateFin,<BR>
	 * attLibelle, marLibelle, exercice, commNumero, fouNom, fouCode, engNumero, dppNumeroFacture<BR>
	 * minHT, maxHT, minTTC, maxTTC, utilisateurRecherche <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des NSDictionary { numero = comm_Numero; dateCreation = comm_Date; reference = comm_reference; libelle =
	 *         comm_libelle; prenom = Utilisateur.PRENOM; nom = UTILISATEUR.NOM_USUEL; etat = TYET_LIBELLE; codeFournisseur = FOU_CODE; nomFournisseur
	 *         = FOU_NOM; }
	 */
	public static final NSArray<NSDictionary<String, Object>> getRawRowPreCommandes(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChainePreCommande(ed, false, bindings), null);
	}

	public static final int getRawRowCountPreCommandes(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nbrePreCommandes = -1;
		NSArray commandes = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChainePreCommande(ed, true, bindings), null);

		if (commandes != null && commandes.count() == 1) {
			NSDictionary row = (NSDictionary) commandes.lastObject();
			nbrePreCommandes = ((Number) row.valueForKey("NBREPRECOMMANDES")).intValue();
		}

		return nbrePreCommandes;
	}

	protected static String constructionChainePreCommande(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		// parties communes des differents select
		String selectClause = "";
		String fromClause = "";
		String whereClause = "";
		String groupByClause = "";
		String orderClause = "";

		if (isForCount)
			selectClause = "SELECT count(DISTINCT c.COMM_NUMERO) nbrePreCommandes";
		else
			selectClause = "SELECT DISTINCT c.COMM_NUMERO numero, c.COMM_DATE dateCreation, c.COMM_REFERENCE reference, c.COMM_LIBELLE libelle, T.TYET_LIBELLE typeEtat,i.NOM_USUEL||' '||i.PRENOM nomPrenom, f.FOU_CODE||' '||f.FOU_NOM libelleFournisseur, SUM(a.ART_PRIX_TOTAL_HT) totalHT, SUM(a.ART_PRIX_TOTAL_TTC) totalTTC";

		fromClause = "FROM jefy_depense.commande c, jefy_depense.v_type_etat t";
		whereClause = "WHERE c.tyet_id=t.tyet_id  AND t.tyet_libelle='PRECOMMANDE' AND c.exe_ordre=" + exeOrdre;

		// droit ou pas de voir les precommandes
		NSMutableDictionary mesBindings = new NSMutableDictionary();
		mesBindings.setObjectForKey(exercice, "exercice");
		mesBindings.setObjectForKey(utilisateur, "utilisateur");
		mesBindings.setObjectForKey(EOFonction.FONCTION_VOIR_TTES_PRECOMMANDES, "fonIdInterne");
		NSArray larray = FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);

		if (larray == null || larray.count() == 0) {
			//			fromClause += ", jefy_depense.commande_utilisateur cu";
			fromClause += ",  (select comm_id from jefy_depense.commande where utl_ordre = " + utlOrdre + " union select comm_id from jefy_depense.commande_utilisateur  where  utl_ordre = " + utlOrdre + ") cu ";
			//			whereClause += " AND c.comm_id=cu.comm_id(+) "; 
			whereClause += " AND c.comm_id = cu.comm_id ";
			//whereClause += " AND (c.utl_ordre="+utlOrdre+" or cu.utl_ordre="+utlOrdre+") ";
		}

		if (!isForCount) {
			fromClause += ", jefy_admin.utilisateur u, grhum.individu_ulr i, jefy_depense.v_fournisseur f, jefy_depense.article a";
			whereClause += " AND c.utl_ordre=u.utl_ordre ";
			whereClause += " AND u.no_individu=i.no_individu ";
			whereClause += " AND c.fou_ordre=f.fou_ordre and c.comm_id=a.comm_id ";
			groupByClause = "GROUP BY c.COMM_NUMERO, c.COMM_DATE, c.COMM_REFERENCE, c.COMM_LIBELLE, T.TYET_LIBELLE, i.PRENOM, i.NOM_USUEL, f.FOU_CODE, f.FOU_NOM";
			orderClause = "ORDER BY c.COMM_DATE DESC";
		}

		return selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + " " + orderClause;
	}

}
