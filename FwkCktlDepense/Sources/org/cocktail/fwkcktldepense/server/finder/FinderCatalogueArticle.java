/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCodeMarche;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOLot;
import org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature;
import org.cocktail.fwkcktldepense.server.metier.EOMarche;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public final class FinderCatalogueArticle extends Finder {

	/**
	 * Recherche des articles catalogue valides par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : libelle, fournisseur, codeExer, prixHt, exercice, prixTtc, reference, fouCode, fouNom, cnCode, cnLibelle, prixHtMin,
	 * prixHtMax, prixTtcMin, prixTtcMax, attribution, date, marcheLibelle, attributionLibelle <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOArticleCatalogue
	 */
	public static final NSArray<EOCatalogueArticle> getArticlesValides(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray array;

		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");

		NSDictionary mesBindings = retraiterBindings(ed, bindings);

		array = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOCatalogueArticle.ENTITY_NAME, "Recherche", mesBindings);

		if (mesBindings.objectForKey("attribution") != null) {
			NSArray restrictionArray = ((EOAttribution) mesBindings.objectForKey("attribution")).lot().lotNomenclatures();

			if (restrictionArray.count() > 0) {
				NSMutableArray qualifier = new NSMutableArray();
				for (int i = 0; i < restrictionArray.count(); i++) {
					EOCodeMarche codeMarche = ((EOLotNomenclature) restrictionArray.objectAtIndex(i)).codeExer().codeMarche();
					qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
							EOCatalogueArticle.CODE_MARCHE_KEY + "=%@ or " +
									EOCatalogueArticle.CODE_MARCHE_KEY + "." + EOCodeMarche.CODE_MARCHE_PERE_KEY + "=%@",
							new NSArray(new Object[] {
									codeMarche, codeMarche
							})));
				}

				array = EOQualifier.filteredArrayWithQualifier(array, new EOOrQualifier(qualifier));
			}
		}

		// TODO : rajout du binding typeAchat ... et voir si l'article correspond a ce type achat par rapport au codeexer
		Integer parametreNiveau = FinderParametre.getParametreCmNiveau(ed, (EOExercice) mesBindings.objectForKey("exercice"));
		for (int i = 0; i < array.count(); i++) {
			EOCatalogueArticle article = (EOCatalogueArticle) array.objectAtIndex(i);

			if (article.codeMarche().cmNiveau().intValue() == parametreNiveau.intValue() + 1)
				article.setCodeMarcheRelationship(article.codeMarche().codeMarchePere());

			article.setCodeExer(FinderCodeExer.getCodeExerPourCodeMarcheEtExercice(ed, article.codeMarche(),
					(EOExercice) mesBindings.objectForKey("exercice")));
		}

		return array;
	}

	private static final NSDictionary retraiterBindings(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		boolean isOnlyMarche = false;
		boolean isOnlyHorsMarche = false;

		if ((bindings.objectForKey("onMarche") != null && ((Boolean) bindings.objectForKey("onMarche")).booleanValue() == true) || bindings.objectForKey("attribution") != null)
			isOnlyMarche = true;
		if ((bindings.objectForKey("onMarche") != null && ((Boolean) bindings.objectForKey("onMarche")).booleanValue() == false) || bindings.objectForKey("typeAchat") != null)
			isOnlyHorsMarche = true;

		EOTypeEtat typeEtat = FinderTypeEtat.getTypeEtat(ed, EOCatalogueArticle.ETAT_VALIDE);
		mesBindings.setObjectForKey(typeEtat, "typeEtat");

		if (mesBindings.objectForKey("date") == null)
			mesBindings.setObjectForKey(new NSTimestamp(), "date");
		if (mesBindings.objectForKey("marchedate") == null)
			mesBindings.setObjectForKey(mesBindings.objectForKey("date"), "marchedate");
		if (mesBindings.objectForKey("marchefouNom") == null && mesBindings.objectForKey("fouNom") != null)
			mesBindings.setObjectForKey(mesBindings.objectForKey("fouNom"), "marchefouNom");

		//////////
		if (mesBindings.objectForKey("attribution") == null) {

			mesBindings.setObjectForKey(EOAttribution.ETAT_VALIDE, "attValide");
			mesBindings.setObjectForKey(EOAttribution.ETAT_NON_SUPPRIME, "attSuppr");

			mesBindings.setObjectForKey(EOLot.ETAT_VALIDE, "lotValide");
			mesBindings.setObjectForKey(EOLot.ETAT_NON_SUPPRIME, "lotSuppr");

			mesBindings.setObjectForKey(EOMarche.ETAT_VALIDE, "marValide");
			mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "marSuppr");
		}
		else {
			if (((EOAttribution) mesBindings.objectForKey("attribution")).lot().isCatalogue()) {
				if (mesBindings.objectForKey("fournisseur") != null)
					mesBindings.removeObjectForKey("fournisseur");
				if (mesBindings.objectForKey("fouCode") != null)
					mesBindings.removeObjectForKey("fouCode");
				if (mesBindings.objectForKey("fouNom") != null)
					mesBindings.removeObjectForKey("fouNom");
				if (mesBindings.objectForKey("marchefournisseur") != null)
					mesBindings.removeObjectForKey("marchefournisseur");
				if (mesBindings.objectForKey("marchefouCode") != null)
					mesBindings.removeObjectForKey("marchefouCode");
				if (mesBindings.objectForKey("marchefouNom") != null)
					mesBindings.removeObjectForKey("marchefouNom");
			}
			else {
				if (mesBindings.objectForKey("fournisseur") != null) {
					if (mesBindings.objectForKey("attribution") != null)
						mesBindings.removeObjectForKey("attribution");
					if (mesBindings.objectForKey("marcheLibelle") != null)
						mesBindings.removeObjectForKey("marcheLibelle");
					if (mesBindings.objectForKey("attributionLibelle") != null)
						mesBindings.removeObjectForKey("attributionLibelle");
				}
			}
		}
		////////

		if (isOnlyMarche) {
			if (mesBindings.objectForKey("date") != null)
				mesBindings.removeObjectForKey("date");
			if (mesBindings.objectForKey("fournisseur") != null)
				mesBindings.removeObjectForKey("fournisseur");
			if (mesBindings.objectForKey("fouCode") != null)
				mesBindings.removeObjectForKey("fouCode");
			if (mesBindings.objectForKey("fouNom") != null)
				mesBindings.removeObjectForKey("fouNom");
			isOnlyHorsMarche = false;
		}

		if (isOnlyHorsMarche) {
			if (mesBindings.objectForKey("marchedate") != null)
				mesBindings.removeObjectForKey("marchedate");
			if (mesBindings.objectForKey("marchefournisseur") != null)
				mesBindings.removeObjectForKey("marchefournisseur");
			if (mesBindings.objectForKey("marchefouCode") != null)
				mesBindings.removeObjectForKey("marchefouCode");
			if (mesBindings.objectForKey("marchefouNom") != null)
				mesBindings.removeObjectForKey("marchefouNom");

			if (mesBindings.objectForKey("attValide") != null)
				mesBindings.setObjectForKey(EOAttribution.ETAT_VALIDE, "attValide");
			if (mesBindings.objectForKey("attSuppr") != null)
				mesBindings.setObjectForKey(EOAttribution.ETAT_NON_SUPPRIME, "attSuppr");
			if (mesBindings.objectForKey("lotValide") != null)
				mesBindings.setObjectForKey(EOLot.ETAT_VALIDE, "lotValide");
			if (mesBindings.objectForKey("lotSuppr") != null)
				mesBindings.setObjectForKey(EOLot.ETAT_NON_SUPPRIME, "lotSuppr");
			if (mesBindings.objectForKey("marValide") != null)
				mesBindings.setObjectForKey(EOMarche.ETAT_VALIDE, "marValide");
			if (mesBindings.objectForKey("marSuppr") != null)
				mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "marSuppr");
			if (mesBindings.objectForKey("attribution") != null)
				mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "attribution");

			if (mesBindings.objectForKey("marcheLibelle") != null)
				mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "marcheLibelle");
			if (mesBindings.objectForKey("attributionLibelle") != null)
				mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "attributionLibelle");
			if (mesBindings.objectForKey("onMarche") != null)
				mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "onMarche");
		}

		return mesBindings;
	}

	/*
	 * Bindings pris en compte : libelle, prixHt, exercice, prixTtc, reference, fouCode, typeAchat, fouNom, cnCode, cnLibelle, prixHtMin, prixHtMax,
	 * prixTtcMin, prixTtcMax, rechercheGlobale
	 */
	public static final NSArray<NSDictionary<String, Object>> getRawRowArticlesValides(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray articles = null;

		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");

		NSDictionary mesBindings = retraiterBindings(ed, bindings);

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		NSTimestamp laDate = new NSTimestamp();
		if (mesBindings != null && mesBindings.objectForKey("date") != null)
			laDate = (NSTimestamp) mesBindings.objectForKey("date");

		String sqlString = "";
		String selectClause = "";
		String fromClause = "";
		String whereClause = "";
		String orderClause = "";

		selectClause = "SELECT a.caar_id cle, decode(a.att_ordre,null,null,'('||mar.exe_ordre||'/'||mar.mar_Index||'/'||lot.lot_Index||')-'||lot.lot_Libelle) attribution,";
		selectClause += "a.tyar_id type_article, a.caar_id_pere article_pere, f.fou_code fouCode, f.fou_nom fouNom, a.caar_reference reference,";
		selectClause += "decode(c.cm_niveau,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU')),c.cm_code,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU'))+1,cpere.cm_code,c.cm_code) cnCode,";
		selectClause += "decode(c.cm_niveau,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU')),c.cm_lib,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU'))+1,cpere.cm_lib,c.cm_lib) cnLibelle,";
		selectClause += "a.caar_libelle libelle, a.caar_prix_ht prixHt, t.tva_taux tauxTva, a.caar_prix_ttc prixTtc";
		fromClause = " FROM jefy_depense.v_catalogue_article a, jefy_depense.v_code_marche c, jefy_depense.v_fournisseur f, jefy_depense.v_tva t, jefy_depense.v_code_exer ce, jefy_depense.v_code_exer cepere, jefy_depense.v_code_marche cpere,";
		fromClause += " jefy_marches.attribution att, jefy_marches.lot lot, jefy_marches.marche mar";
		whereClause = " WHERE a.cm_ordre=c.cm_ordre and a.fou_ordre=f.fou_ordre and a.tva_id=t.tva_id and c.cm_ordre=ce.cm_ordre and a.tyet_id=1";
//		whereClause += " AND ce.ce_pere=cepere.ce_ordre and cepere.cm_ordre=cpere.cm_ordre AND ce.exe_ordre=" + exeOrdre;
		whereClause += " AND cepere.cm_ordre=cpere.cm_ordre AND cpere.cm_ordre = c.cm_pere AND ce.exe_ordre=" + exeOrdre + " AND cepere.exe_ordre=" + exeOrdre;
		
		whereClause += " AND to_date(to_char(a.cat_date_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + ZDateUtil.dateToString(laDate) + "','dd/mm/yyyy')";
		whereClause += " AND (a.cat_date_fin is null or to_date(to_char(a.cat_date_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + ZDateUtil.dateToString(laDate) + "','dd/mm/yyyy'))";
		whereClause += " and a.att_ordre=att.att_ordre(+) and att.lot_ordre=lot.lot_ordre(+) and lot.mar_ordre=mar.mar_ordre(+)";

		orderClause = " ORDER BY decode(a.caar_id_pere, null, a.caar_id, a.caar_id_pere), a.caar_id";

		if (mesBindings.objectForKey("rechercheGlobale") != null) {
			NSArray tokens = NSArray.componentsSeparatedByString((String) mesBindings.objectForKey("rechercheGlobale"), " ");
			Enumeration enumTokens = tokens.objectEnumerator();
			while (enumTokens.hasMoreElements()) {
				String unToken = (String) enumTokens.nextElement();
				if (unToken.trim().equals("") == false) {
					whereClause += " AND (upper(c.cm_code) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(c.cm_lib) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					if (mesBindings.objectForKey("fouCode") == null && mesBindings.objectForKey("fouNom") == null && mesBindings.objectForKey("fournisseur") == null) {
						whereClause += " OR upper(f.fou_code) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
						whereClause += " OR upper(f.fou_nom) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					}
					whereClause += " OR upper(a.caar_reference) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(a.caar_libelle) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper('('||mar.exe_ordre||'/'||mar.mar_Index||'/'||lot.lot_Index||')-'||lot.lot_Libelle) like upper('%" +
							ZStringUtil.sqlString(unToken) + "%')";
					whereClause += ")";
				}
			}
		}

		if (mesBindings.objectForKey("libelle") != null)
			whereClause += " AND upper(a.caar_libelle) like upper('%" + mesBindings.objectForKey("libelle") + "%')";
		if (mesBindings.objectForKey("reference") != null)
			whereClause += " AND upper(a.caar_reference) like upper('%" + mesBindings.objectForKey("reference") + "%')";

		if (mesBindings.objectForKey("prixHt") != null)
			whereClause += " AND a.caar_prix_ht=" + mesBindings.objectForKey("prixHt");
		if (mesBindings.objectForKey("prixHtMin") != null)
			whereClause += " AND a.caar_prix_ht>=" + mesBindings.objectForKey("prixHtMin");
		if (mesBindings.objectForKey("prixHt") != null)
			whereClause += " AND a.acaar_prix_ht<=" + mesBindings.objectForKey("prixHtMax");

		if (mesBindings.objectForKey("prixTtc") != null)
			whereClause += " AND a.caar_prix_ttc=" + mesBindings.objectForKey("prixTtc");
		if (mesBindings.objectForKey("prixTtc") != null)
			whereClause += " AND a.caar_prix_ttc>=" + mesBindings.objectForKey("prixTtcMin");
		if (mesBindings.objectForKey("prixTtc") != null)
			whereClause += " AND a.caar_prix_ttc<=" + mesBindings.objectForKey("prixTtcMax");

		if (mesBindings.objectForKey("attribution") == null) {
			NSTimestamp uneDate = (NSTimestamp) mesBindings.objectForKey("marchedate");
			if (uneDate != null) {
				uneDate = (NSTimestamp) mesBindings.objectForKey("marchedate");

				whereClause += " and ";
				if (mesBindings.objectForKey("date") != null)
					whereClause += " (a.att_ordre is null or (";

				whereClause += " att.att_valide='O' and att.att_suppr='N' and lot.lot_valide='O' and lot.lot_suppr='N'";
				whereClause += " and mar.mar_valide='O' and mar.mar_suppr='N'";

				whereClause += " and to_date(to_char(mar.mar_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy')";
				whereClause += " and (to_date(to_char(mar.mar_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy') or mar.mar_fin is null)";
				whereClause += " and to_date(to_char(lot.lot_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy')";
				whereClause += " and (to_date(to_char(lot.lot_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy') or lot.lot_fin is null)";
				whereClause += " and to_date(to_char(att.att_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy')";
				whereClause += " and (to_date(to_char(att.att_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy') or att.att_fin is null)";

				if (mesBindings.objectForKey("date") != null)
					whereClause += ")) ";
			}
		}
		else {
			if (((EOAttribution) mesBindings.objectForKey("attribution")).lot().isCatalogue())
				whereClause += " and att.att_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOAttribution) mesBindings.objectForKey("attribution")).objectForKey(EOAttribution.ATT_ORDRE_KEY);
			else {
				NSArray restrictionArray = ((EOAttribution) bindings.objectForKey("attribution")).lot().lotNomenclatures();

				if (restrictionArray.count() > 0) {
					whereClause += " and (";
					for (int i = 0; i < restrictionArray.count(); i++) {
						if (i > 0)
							whereClause += " or ";
						EOCodeMarche codeMarche = ((EOLotNomenclature) restrictionArray.objectAtIndex(i)).codeExer().codeMarche();
						whereClause += "c.cm_code='" + codeMarche.cmCode() + "' or cpere.cm_code='" + codeMarche.cmCode() + "'";
					}
					whereClause += ")";
				}
			}
		}

		if (mesBindings.objectForKey("fournisseur") != null || mesBindings.objectForKey("marchefournisseur") != null) {
			whereClause += " AND (";
			if (mesBindings.objectForKey("fournisseur") != null)
				whereClause += "f.fou_ordre=" +
						(Number) EOUtilities.primaryKeyForObject(ed, (EOFournisseur) mesBindings.objectForKey("fournisseur")).objectForKey(EOFournisseur.FOU_ORDRE_KEY);
			if (mesBindings.objectForKey("fournisseur") != null && mesBindings.objectForKey("marchefournisseur") != null)
				whereClause += " or ";
			if (mesBindings.objectForKey("marchefournisseur") != null)
				whereClause += " att.fou_ordre=" +
						(Number) EOUtilities.primaryKeyForObject(ed, (EOFournisseur) mesBindings.objectForKey("marchefournisseur")).objectForKey(EOFournisseur.FOU_ORDRE_KEY);
			whereClause += ") ";
		}
		else {
			if (mesBindings.objectForKey("fouCode") != null || mesBindings.objectForKey("fouNom") != null || mesBindings.objectForKey("marchefouCode") != null
					|| mesBindings.objectForKey("marchefouNom") != null) {

				if (mesBindings.objectForKey("marchefouCode") != null || mesBindings.objectForKey("marchefouNom") != null) {
					fromClause += ", jefy_depense.v_fournisseur fatt";
					whereClause += " and att.fou_ordre=fatt.fou_ordre(+) ";
				}

				whereClause += " AND (";

				if (mesBindings.objectForKey("fouCode") != null || mesBindings.objectForKey("fouNom") != null)
					whereClause += "(";
				if (mesBindings.objectForKey("fouCode") != null)
					whereClause += "upper(f.fou_code) like upper('%" + mesBindings.objectForKey("fouCode") + "%')";
				if (mesBindings.objectForKey("fouCode") != null && mesBindings.objectForKey("fouNom") != null)
					whereClause += " or ";
				if (mesBindings.objectForKey("fouNom") != null)
					whereClause += "upper(f.fou_nom) like upper('%" + mesBindings.objectForKey("fouNom") + "%')";

				if (mesBindings.objectForKey("fouCode") != null || mesBindings.objectForKey("fouNom") != null)
					whereClause += ")";

				if ((mesBindings.objectForKey("fouCode") != null || mesBindings.objectForKey("fouNom") != null) &&
						(mesBindings.objectForKey("marchefouCode") != null || mesBindings.objectForKey("marchefouNom") != null))
					whereClause += " or ";

				if (mesBindings.objectForKey("marchefouCode") != null || mesBindings.objectForKey("marchefouNom") != null)
					whereClause += "(";
				if (mesBindings.objectForKey("marchefouCode") != null)
					whereClause += "upper(fatt.fou_code) like upper('%" + mesBindings.objectForKey("marchefouCode") + "%') ";//or fatt.fou_code is null";
				if (mesBindings.objectForKey("marchefouCode") != null && mesBindings.objectForKey("marchefouNom") != null)
					whereClause += " or ";
				if (mesBindings.objectForKey("marchefouNom") != null)
					whereClause += "upper(fatt.fou_nom) like upper('%" + mesBindings.objectForKey("marchefouNom") + "%') ";//or fatt.fou_nom is null";

				if (mesBindings.objectForKey("marchefouCode") != null || mesBindings.objectForKey("marchefouNom") != null)
					whereClause += ")";

				whereClause += ") ";
			}
		}

		if (mesBindings.objectForKey("codeExer") != null)
			whereClause += " and ce.ce_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOCodeExer) mesBindings.objectForKey("codeExer")).objectForKey(EOCodeExer.CE_ORDRE_KEY);
		if (mesBindings.objectForKey("cnCode") != null)
			whereClause += " AND upper(c.cm_code) like upper('%" + mesBindings.objectForKey("cnCode") + "%')";
		if (mesBindings.objectForKey("cnLibelle") != null)
			whereClause += " AND upper(c.cm_lib) like upper('%" + mesBindings.objectForKey("cnLibelle") + "%')";

		if (mesBindings.objectForKey("typeAchat") != null) {
			EOTypeAchat typeAchat = (EOTypeAchat) mesBindings.objectForKey("typeAchat");

			if (typeAchat.typaLibelle().equals(EOTypeAchat.MONOPOLE))
				whereClause += " and ce.ce_monopole=1";
			if (typeAchat.typaLibelle().equals(EOTypeAchat.SANS_ACHAT))
				whereClause += " and ce.ce_autres=1";
			if (typeAchat.typaLibelle().equals(EOTypeAchat.TROIS_CMP))
				whereClause += " and ce.ce_3cmp=1";
			if (typeAchat.typaLibelle().equals(EOTypeAchat.MARCHE_NON_FORMALISE))
				whereClause += " and ce.ce_monopole=0 and ce.ce_autres=0 and ce.ce_3cmp=0";
		}

		if (mesBindings.objectForKey("marcheLibelle") != null)
			whereClause += " and upper(mar.mar_libelle) like upper('%" + mesBindings.objectForKey("marcheLibelle") + "%')";

		if (mesBindings.objectForKey("attributionLibelle") != null)
			whereClause += " and upper('('||mar.exe_ordre||'/'||mar.mar_Index||'/'||lot.lot_Index||')-'||lot.lot_Libelle) like upper('%" + mesBindings.objectForKey("attributionLibelle") + "%')";

		// lancement requete
		sqlString = selectClause + " " + fromClause + " " + whereClause + " " + orderClause;

		articles = EOUtilities.rawRowsForSQL(ed, "carambole", sqlString, null);

		return articles;
	}

	/**
	 * Recherche des articles catalogue valides pour un fournisseur <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param fournisseur EOFournisseur pour lequel effectuer la recherche
	 * @param exercice EOExercice sur lequel effectuer la recherche
	 * @return un NSArray contenant des EOArticleCatalogue
	 */
	public static final NSArray<EOCatalogueArticle> getArticlesValidesPourFournisseur(EOEditingContext ed, EOFournisseur fournisseur, EOExercice exercice) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(fournisseur, "fournisseur");
		bindings.setObjectForKey(exercice, "exercice");
		return getArticlesValides(ed, bindings);
	}

	/**
	 * Recherche des articles catalogue valides pour un code nomenclature <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param codeExer EOCodeExer pour lequel effectuer la recherche
	 * @return un NSArray contenant des EOArticleCatalogue
	 */
	public static final NSArray<EOCatalogueArticle> getArticlesValidesPourCodeExer(EOEditingContext ed, EOCodeExer codeExer) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(codeExer, "codeExer");
		return getArticlesValides(ed, bindings);
	}

	/**
	 * Recherche des articles catalogue valides par son libelle <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle String pour lequel faire la recherche avec un caseInsensitiveLike
	 * @param exercice EOExercice sur lequel effectuer la recherche
	 * @return un NSArray contenant des EOArticleCatalogue
	 */
	public static final NSArray<EOCatalogueArticle> getArticlesValidesPourLibelle(EOEditingContext ed, String libelle, EOExercice exercice) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		EOTypeEtat typeEtat = FinderTypeEtat.getTypeEtat(ed, EOCatalogueArticle.ETAT_VALIDE);
		NSArray array;

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.TYPE_ETAT_KEY + "=%@", new NSArray(typeEtat)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.CAAR_LIBELLE_KEY + " caseInsensitiveLike %@",
				new NSArray("*" + libelle.toUpperCase() + "*")));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.CODE_MARCHE_KEY + "." +
				EOCodeMarche.CODE_EXER_KEY + "." + EOCodeExer.EXERCICE_KEY + "=%@", new NSArray(exercice)));

		array = Finder.fetchArray(EOCatalogueArticle.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		for (int i = 0; i < array.count(); i++) {
			EOCatalogueArticle article = (EOCatalogueArticle) array.objectAtIndex(i);

			Integer parametreNiveau = FinderParametre.getParametreCmNiveau(ed, exercice);
			if (article.codeMarche().cmNiveau().intValue() == parametreNiveau.intValue() + 1)
				article.setCodeMarcheRelationship(article.codeMarche().codeMarchePere());

			article.setCodeExer(FinderCodeExer.getCodeExerPourCodeMarcheEtExercice(ed, article.codeMarche(), exercice));
		}

		return Finder.tableauTrie(array, sort());
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOCatalogueArticle.CAAR_LIBELLE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Recherche de catalogues de marche par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : attribution, libelle, fournisseur, codeExer, prixHt, exercice, prixTtc, reference, fouCode, fouNom, cnCode,
	 * cnLibelle, prixHtMin, prixHtMax, prixTtcMin, prixTtcMax, date, marcheLibelle, attributionLibelle <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOMarchesCatalogue
	 */
	public static final NSArray getArticlesPourAttribution(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray array, restrictionArray = new NSArray();
		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");

		array = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOCatalogueArticle.ENTITY_NAME, "Recherche", mesBindings);

		if (restrictionArray.count() > 0) {
			NSMutableArray qualifier = new NSMutableArray();
			for (int i = 0; i < restrictionArray.count(); i++) {
				EOCodeMarche codeMarche = ((EOLotNomenclature) restrictionArray.objectAtIndex(i)).codeExer().codeMarche();
				qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOCatalogueArticle.CODE_MARCHE_KEY + "=%@ or " +
								EOCatalogueArticle.CODE_MARCHE_KEY + "." + EOCodeMarche.CODE_MARCHE_PERE_KEY + "=%@",
						new NSArray(new Object[] {
								codeMarche, codeMarche
						})));
			}

			array = EOQualifier.filteredArrayWithQualifier(array, new EOOrQualifier(qualifier));
		}

		if (bindings.objectForKey("marcheLibelle") != null)
			array = EOQualifier.filteredArrayWithQualifier(array,
					EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.ATTRIBUTION_KEY + "." +
							EOAttribution.LOT_KEY + "." + EOLot.MARCHE_KEY + ".libelle caseInsensitiveLike %@",
							new NSArray(bindings.objectForKey("marcheLibelle"))));

		Integer parametreNiveau = FinderParametre.getParametreCmNiveau(ed, (EOExercice) mesBindings.objectForKey("exercice"));
		for (int i = 0; i < array.count(); i++) {
			EOCatalogueArticle article = (EOCatalogueArticle) array.objectAtIndex(i);

			if (article.codeMarche().cmNiveau().intValue() == parametreNiveau.intValue() + 1)
				article.setCodeMarcheRelationship(article.codeMarche().codeMarchePere());

			article.setCodeExer(FinderCodeExer.getCodeExerPourCodeMarcheEtExercice(ed, article.codeMarche(),
					(EOExercice) mesBindings.objectForKey("exercice")));
		}

		return array;
	}

	/*
	 * Bindings pris en compte : libelle, prixHt, exercice, prixTtc, reference, fouCode, codeExer, date, rechercheGlobale, fouNom, cnCode, cnLibelle,
	 * prixHtMin, prixHtMax, prixTtcMin, prixTtcMax, attribution, fournisseur, marcheLibelle, attributionLibelle
	 */
	public static final NSArray<NSDictionary<String, Object>> getRawRowArticlesPourAttribution(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray articles = null;

		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		EOExercice exercice = (EOExercice) mesBindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);

		String sqlString = "";
		String selectClause = "";
		String fromClause = "";
		String whereClause = "";
		String orderClause = "";

		selectClause = "select a.cat_ordre cle,";
		selectClause += "'('||mar.exe_ordre||'/'||mar.mar_Index||'/'||lot.lot_Index||')-'||lot.lot_Libelle attribution,";
		selectClause += "decode(c.cm_niveau,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU')),c.cm_code,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU'))+1,cpere.cm_code,c.cm_code) cnCode,";
		selectClause += "decode(c.cm_niveau,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU')),c.cm_lib,grhum.en_nombre(jefy_depense.Get_Parametre(ce.exe_ordre, 'ARTICLE_CM_NIVEAU'))+1,cpere.cm_lib,c.cm_lib) cnLibelle,";
		selectClause += "f.fou_code fouCode, f.fou_nom fouNom,a.cat_ref reference, a.cat_desc libelle, a.cat_prix_ht prixHt, t.tva_taux tauxTva, a.cat_prix_ttc prixTtc";
		fromClause = " from jefy_depense.v_marches_catalogue a, jefy_depense.v_code_marche c, jefy_depense.v_fournisseur f, jefy_depense.v_tva t, jefy_depense.v_code_exer ce, jefy_depense.v_code_exer cepere, jefy_depense.v_code_marche cpere,";
		fromClause += " jefy_marches.attribution att, jefy_marches.lot lot, jefy_marches.marche mar";
		whereClause = " WHERE a.cm_ordre=c.cm_ordre and a.att_ordre=att.att_ordre and att.fou_ordre=f.fou_ordre and a.tva_id=t.tva_id and c.cm_ordre=ce.cm_ordre";
		whereClause += " and ce.ce_pere=cepere.ce_ordre and cepere.cm_ordre=cpere.cm_ordre and att.lot_ordre=lot.lot_ordre and lot.mar_ordre=mar.mar_ordre";
		whereClause += " and a.tyet_id=1 and ce.exe_ordre=" + exeOrdre;
		orderClause = " ORDER BY a.cat_desc";

		if (bindings.objectForKey("rechercheGlobale") != null) {
			NSArray tokens = NSArray.componentsSeparatedByString((String) bindings.objectForKey("rechercheGlobale"), " ");
			Enumeration enumTokens = tokens.objectEnumerator();
			while (enumTokens.hasMoreElements()) {
				String unToken = (String) enumTokens.nextElement();
				if (unToken.trim().equals("") == false) {
					whereClause += " AND (upper(c.cm_code) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(c.cm_lib) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(f.fou_code) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(f.fou_nom) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(a.cat_ref) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper(a.cat_desc) like upper('%" + ZStringUtil.sqlString(unToken) + "%')";
					whereClause += " OR upper('('||mar.exe_ordre||'/'||mar.mar_Index||'/'||lot.lot_Index||')-'||lot.lot_Libelle) like upper('%" +
							ZStringUtil.sqlString(unToken) + "%')";
					whereClause += ")";
				}
			}
		}

		if (bindings.objectForKey("typeAchat") != null) {
			EOTypeAchat typeAchat = (EOTypeAchat) bindings.objectForKey("typeAchat");

			if (typeAchat.typaLibelle().equals(EOTypeAchat.MONOPOLE))
				whereClause += " and ce.ce_monopole=1";
			if (typeAchat.typaLibelle().equals(EOTypeAchat.SANS_ACHAT))
				whereClause += " and ce.ce_autres=1";
			if (typeAchat.typaLibelle().equals(EOTypeAchat.TROIS_CMP))
				whereClause += " and ce.ce_3cmp=1";
			if (typeAchat.typaLibelle().equals(EOTypeAchat.MARCHE_NON_FORMALISE))
				whereClause += " and ce.ce_monopole=0 and ce.ce_autres=0 and ce.ce_3cmp=0";
		}

		if (mesBindings.objectForKey("libelle") != null)
			whereClause += " AND upper(a.cat_desc) like upper('%" + mesBindings.objectForKey("libelle") + "%')";
		if (mesBindings.objectForKey("reference") != null)
			whereClause += " AND upper(a.cat_ref) like upper('%" + mesBindings.objectForKey("reference") + "%')";

		if (mesBindings.objectForKey("prixHt") != null)
			whereClause += " AND a.cat_prix_ht=" + mesBindings.objectForKey("prixHt");
		if (mesBindings.objectForKey("prixHtMin") != null)
			whereClause += " AND a.cat_prix_ht>=" + mesBindings.objectForKey("prixHtMin");
		if (mesBindings.objectForKey("prixHt") != null)
			whereClause += " AND a.cat_prix_ht<=" + mesBindings.objectForKey("prixHtMax");

		if (mesBindings.objectForKey("prixTtc") != null)
			whereClause += " AND a.cat_prix_ttc=" + mesBindings.objectForKey("prixTtc");
		if (mesBindings.objectForKey("prixTtc") != null)
			whereClause += " AND a.cat_prix_ttc>=" + mesBindings.objectForKey("prixTtcMin");
		if (mesBindings.objectForKey("prixTtc") != null)
			whereClause += " AND a.cat_prix_ttc<=" + mesBindings.objectForKey("prixTtcMax");

		if (mesBindings.objectForKey("cnCode") != null)
			whereClause += " AND upper(c.cm_code) like upper('%" + mesBindings.objectForKey("cnCode") + "%')";
		if (mesBindings.objectForKey("cnLibelle") != null)
			whereClause += " AND upper(c.cm_lib) like upper('%" + mesBindings.objectForKey("cnLibelle") + "%')";

		if (mesBindings.objectForKey("attribution") == null) {
			NSTimestamp uneDate = (NSTimestamp) mesBindings.objectForKey("date");
			if (uneDate == null) {
				mesBindings.setObjectForKey(new NSTimestamp(), "date");
				uneDate = (NSTimestamp) mesBindings.objectForKey("date");
			}
			whereClause += " and att.att_valide='O' and att.att_suppr='N' and lot.lot_valide='O' and lot.lot_suppr='N'";
			whereClause += " and mar.mar_valide='O' and mar.mar_suppr='N'";

			whereClause += " and to_date(to_char(mar.mar_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy')";
			whereClause += " and (to_date(to_char(mar.mar_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy') or mar.mar_fin is null)";
			whereClause += " and to_date(to_char(lot.lot_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy')";
			whereClause += " and (to_date(to_char(lot.lot_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy') or lot.lot_fin is null)";
			whereClause += " and to_date(to_char(att.att_debut,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy')";
			whereClause += " and (to_date(to_char(att.att_fin,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + DateCtrl.dateToString(uneDate, "%d/%m/%Y") + "','dd/mm/yyyy') or att.att_fin is null)";

		}
		else {
			if (((EOAttribution) mesBindings.objectForKey("attribution")).lot().isCatalogue()) {
				mesBindings.removeObjectForKey("fournisseur");
				mesBindings.removeObjectForKey("fouCode");
				mesBindings.removeObjectForKey("fouNom");
			}
			else {
				NSArray restrictionArray = ((EOAttribution) bindings.objectForKey("attribution")).lot().lotNomenclatures();

				if (restrictionArray.count() > 0) {
					whereClause += " and (";
					for (int i = 0; i < restrictionArray.count(); i++) {
						if (i > 0)
							whereClause += " or ";
						EOCodeMarche codeMarche = ((EOLotNomenclature) restrictionArray.objectAtIndex(i)).codeExer().codeMarche();
						whereClause += "c.cm_code='" + codeMarche.cmCode() + "' or cpere.cm_code='" + codeMarche.cmCode() + "'";
					}
					whereClause += ")";
				}

				if (mesBindings.objectForKey("fournisseur") != null)
					mesBindings.removeObjectForKey("attribution");
			}
		}

		if (mesBindings.objectForKey("attribution") != null)
			whereClause += " and att.att_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOAttribution) mesBindings.objectForKey("attribution")).objectForKey(EOAttribution.ATT_ORDRE_KEY);
		if (mesBindings.objectForKey("fournisseur") != null)
			whereClause += " and f.fou_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOFournisseur) mesBindings.objectForKey("fournisseur")).objectForKey(EOFournisseur.FOU_ORDRE_KEY);
		if (mesBindings.objectForKey("codeExer") != null)
			whereClause += " and ce.ce_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOCodeExer) mesBindings.objectForKey("codeExer")).objectForKey(EOCodeExer.CE_ORDRE_KEY);
		if (mesBindings.objectForKey("fouCode") != null)
			whereClause += " AND upper(f.fou_code) like upper('%" + mesBindings.objectForKey("fouCode") + "%')";
		if (mesBindings.objectForKey("fouNom") != null)
			whereClause += " AND upper(f.fou_nom) like upper('%" + mesBindings.objectForKey("fouNom") + "%')";

		if (mesBindings.objectForKey("marcheLibelle") != null)
			whereClause += " and upper(mar.mar_libelle) like upper('%" + mesBindings.objectForKey("marcheLibelle") + "%')";

		if (mesBindings.objectForKey("attributionLibelle") != null)
			whereClause += " and upper('('||mar.exe_ordre||'/'||mar.mar_Index||'/'||lot.lot_Index||')-'||lot.lot_Libelle) like upper('%" + mesBindings.objectForKey("attributionLibelle") + "%')";

		// lancement requete
		sqlString = selectClause + " " + fromClause + " " + whereClause + " " + orderClause;

		articles = EOUtilities.rawRowsForSQL(ed, "carambole", sqlString, null);

		return articles;
	}

	public static final NSArray<EOCatalogueArticle> getOptions(EOEditingContext ed, EOCatalogueArticle article) {
		return Finder.fetchArray(ed, EOCatalogueArticle.ENTITY_NAME, EOCatalogueArticle.CATALOGUE_ARTICLE_PERE_KEY + "=%@", new NSArray(article), null, false);
	}

}
