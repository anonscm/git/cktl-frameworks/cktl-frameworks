/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOOrganExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderOrgan extends Finder {

	public static final String ORGAN_DEPENSE = "DEPENSE";
	public static final String ORGAN_RECETTE = "RECETTE";
	public static final String ORGAN_BUDGET = "BUDGET";

	// tyet_id pris en compte :
	// 16 : TOUTES
	// 19 : AUCUNE
	// 20 : RECETTES
	// 21 : DEPENSES

	public static final String DECODE_DEPENSE = "decode(o.org_op_autorisees, 19,0,20,0,1)";
	public static final String DECODE_BUDGET = "1";
	public static final String DECODE_RECETTE = "decode(o.org_op_autorisees, 19,0,21,0,1)";

	/**
	 * @param ec
	 * @param structureUlr La structure de rattachement
	 * @return Un tableau contenant toutes les organ rattaches a la structure (ainsi que les organ enfants sans structure specifiee).
	 */
	public static NSArray<EOOrgan> fetchOrgansForStructure(EOEditingContext ec,
			String cStructure, EOExercice exercice) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOOrgan.C_STRUCTURE_KEY + "=%@ and " + EOOrgan.ORGAN_EXERCICE_KEY + "." + EOOrganExercice.EXERCICE_KEY + "=%@",
				new NSArray<Object>(new Object[] {
						cStructure, exercice
				}));
		// recuperer tous les organ directement rattaches a la structure
		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, qual, null);
		@SuppressWarnings("unchecked")
		NSArray<EOOrgan> res1 = ec.objectsWithFetchSpecification(fs);
		NSMutableArray<EOOrgan> res = new NSMutableArray<EOOrgan>();
		// recuperer tous les enfants
		for (int i = 0; i < res1.count(); i++) {
			res.addObjectsFromArray(getAllOrganFilsSansStructure((EOOrgan) res1.objectAtIndex(i)));
		}
		return res;
	}

	/**
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif) sans structure specifiee.
	 */
	public static NSArray<EOOrgan> getAllOrganFilsSansStructure(EOOrgan organ) {
		NSMutableArray<EOOrgan> res = new NSMutableArray<EOOrgan>();
		Enumeration<EOOrgan> enumeration = organ.organFils().objectEnumerator();
		while (enumeration.hasMoreElements()) {
			final EOOrgan object = (EOOrgan) enumeration.nextElement();
			if (object.cStructure() == null || NSKeyValueCoding.NullValue.equals(object.cStructure())) {
				res.addObject(object);
				res.addObjectsFromArray(getAllOrganFilsSansStructure(object));
			}
		}
		return res.immutableClone();
	}

	/*
	 * Recherche de lignes budgetaire par la fetchSpecification Recherche.<BR> Bindings pris en compte : utilisateur, exercice, convention
	 */
	public static final NSArray<EOOrgan> getOrgansPourConventionEtExercice(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("utilisateur") == null)
			throw new FactoryException("le bindings 'utilisateur' est obligatoire");
		if (bindings.objectForKey("convention") == null)
			throw new FactoryException("le bindings 'convention' est obligatoire");

		@SuppressWarnings("unchecked")
		NSArray<EOConventionNonLimitative> larray = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOConventionNonLimitative.ENTITY_NAME, "Recherche", bindings);
		@SuppressWarnings("unchecked")
		NSArray<EOOrgan> resultats = (NSArray<EOOrgan>) larray.valueForKeyPath(EOConventionNonLimitative.ORGAN_KEY);

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(resultats, sort());
	}

	/**
	 * Recherche de lignes budgetaire par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, exercice, sourceLibelle <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOOrgan
	 */

	@SuppressWarnings("unchecked")
	public static final NSArray<EOOrgan> getOrgans(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray<EOOrgan> lesOrgans = NSArray.emptyArray();
		NSArray<EOUtilisateurOrgan> larray = FinderOrgan.getUtilisateurOrgans(ed, bindings);

		if (larray != null && larray.count() > 0) {
			// TODO Optimisation base de donnees
			lesOrgans = (NSArray<EOOrgan>) larray.valueForKeyPath(EOUtilisateurOrgan.ORGAN_KEY);

			if (bindings != null && bindings.objectForKey("sourceLibelle") != null)
				lesOrgans = EOQualifier.filteredArrayWithQualifier(lesOrgans,
						new EOKeyValueQualifier("sourceLibelle", EOQualifier.QualifierOperatorCaseInsensitiveLike, bindings.objectForKey("sourceLibelle"))
						);
			//			lesOrgans = EOQualifier.filteredArrayWithQualifier(lesOrgans,
			//					EOQualifier.qualifierWithQualifierFormat("sourceLibelle caseInsensitiveLike %@",
			//							new NSArray(bindings.objectForKey("sourceLibelle"))));
		}
		return lesOrgans;
	}

	public static final NSArray<EOOrgan> getOrgans(EOEditingContext ed, EOUtilisateur utilisateur, EOExercice exercice) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(utilisateur, "utilisateur");
		bindings.setObjectForKey(exercice, "exercice");
		return FinderOrgan.getOrgans(ed, bindings);
	}

	/**
	 * Recherche de droits pour un utilisateur sur les CR ou sous CR par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOUtilisateurOrgan
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<EOUtilisateurOrgan> getUtilisateurOrgans(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("utilisateur") == null)
			throw new FactoryException("le bindings 'utilisateur' est obligatoire");

		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOUtilisateurOrgan.ENTITY_NAME, "Recherche", bindings);
	}

	private static NSArray<EOSortOrdering> sort() {
		NSMutableArray<EOSortOrdering> array = new NSMutableArray<EOSortOrdering>();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

	public static final NSArray<NSDictionary<String, Object>> getRawRowOrgan(EOEditingContext ed, NSDictionary<String, Object> bindings, int niveauMax) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("utilisateur") == null)
			throw new FactoryException("le bindings 'utilisateur' est obligatoire");

		if (bindings.objectForKey("exercice").getClass() != EOExercice.class)
			throw new FactoryException("le bindings 'exercice' doit etre de type EOExercice");
		if (bindings.objectForKey("utilisateur").getClass() != EOUtilisateur.class)
			throw new FactoryException("le bindings 'utilisateur' doit etre de type EOUtilisateur");

		String typeUtilisation = FinderOrgan.ORGAN_DEPENSE;
		if (bindings.objectForKey("typeUtilisation") != null)
			typeUtilisation = (String) bindings.objectForKey("typeUtilisation");

		return getRawRowOrgan(ed, (EOExercice) bindings.objectForKey("exercice"), (EOUtilisateur) bindings.objectForKey("utilisateur"), typeUtilisation, niveauMax);
	}

	@SuppressWarnings("unchecked")
	public static final NSArray<NSDictionary<String, Object>> getRawRowOrgan(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur, String typeUtilisation, int niveauMax) {
		if (exercice == null)
			throw new FactoryException("le parametre 'exercice' est obligatoire");
		if (utilisateur == null)
			throw new FactoryException("le parametre 'utilisateur' est obligatoire");

		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, exercice, utilisateur, typeUtilisation, niveauMax), null);
	}

	@SuppressWarnings("unchecked")
	public static final NSArray<NSDictionary<String, Object>> getRawRowOrgan(EOEditingContext ed, Integer exeOrdre, Integer utlOrdre, String typeUtilisation, int niveauMax) {
		if (exeOrdre == null)
			throw new FactoryException("le parametre 'exeOrdre' est obligatoire");
		if (utlOrdre == null)
			throw new FactoryException("le parametre 'utlOrdre' est obligatoire");

		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, exeOrdre, utlOrdre, typeUtilisation, niveauMax), null);
	}

	protected static String constructionChaine(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur, String typeUtilisation, int niveauMax) {
		Integer exeOrdre = new Integer(exercice.exeExercice().toString());
		Integer utlOrdre = new Integer(EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY).toString());
		return constructionChaine(ed, exeOrdre, utlOrdre, typeUtilisation, niveauMax);
	}

	protected static String constructionChaine(EOEditingContext ed, Integer exeOrdre, Integer utlOrdre, String typeUtilisation, int niveauMax) {
		String select = "";

		if (typeUtilisation == null || (!typeUtilisation.equals(FinderOrgan.ORGAN_BUDGET) && !typeUtilisation.equals(FinderOrgan.ORGAN_RECETTE) &&
				!typeUtilisation.equals(FinderOrgan.ORGAN_DEPENSE)))
			typeUtilisation = FinderOrgan.ORGAN_DEPENSE;

		String decodeTypeUtilisation = "";
		if (typeUtilisation.equals(FinderOrgan.ORGAN_DEPENSE))
			decodeTypeUtilisation = FinderOrgan.DECODE_DEPENSE;
		if (typeUtilisation.equals(FinderOrgan.ORGAN_BUDGET))
			decodeTypeUtilisation = FinderOrgan.DECODE_BUDGET;
		if (typeUtilisation.equals(FinderOrgan.ORGAN_RECETTE))
			decodeTypeUtilisation = FinderOrgan.DECODE_RECETTE;

		if (niveauMax < 0)
			throw new FactoryException("le niveau maximum doit etre >=0");

		// tous les sous cr
		if (niveauMax >= 4) {
			select = select + "select o.org_id, o.org_niv, o.org_pere, o.org_univ, o.org_etab, o.org_ub, o.org_cr, o.org_lib, o.org_lucrativite, ";
			select = select + "o.org_date_ouverture, o.org_date_cloture, o.c_structure, o.log_ordre, o.tyor_id, o.org_souscr, " + decodeTypeUtilisation + " droit ";
			select = select + "from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + " and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))";

			select = select + " union all ";
		}

		// tous les cr
		if (niveauMax >= 3) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ where org_id in ";
			select = select + "(select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))) ";
			select = select + "     union all ";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR, " + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// toutes les ub
		if (niveauMax >= 2) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ o where org_id in ";
			select = select + "   (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "    select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// tous les etab
		if (niveauMax >= 1) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ o where org_id in ";
			select = select + "   (select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))))";
			select = select + "    union all";
			select = select + "    select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "    select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=1 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// toutes les racines
		if (niveauMax >= 0) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ o where org_id in ";
			select = select
					+ "   (select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre="
					+ utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))))";
			select = select + "    union all";
			select = select + "    select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))))";
			select = select + "    union all";
			select = select + "    select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "    select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=1 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=0 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";
		}

		return select;
	}
}
