/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.metier.EOGrhumParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public final class FinderGrhumParametre extends Finder {

	//private static NSArray arrayParametres;
	//private static FinderParametre sharedInstance;

	/**
	 * Permet d'avoir une seule instance de la classe et de garder les parametres en memoire <BR>
	 * 
	 * @return l'instance de FinderParametre
	 */
	/*
	 * private static FinderParametre sharedInstance() { if (sharedInstance == null) { arrayParametres=null; sharedInstance = new FinderParametre(); }
	 * return sharedInstance; }
	 */

	/**
	 * Recherche d'un parametre par son code et son exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un EOGrhumParametres
	 */
	public static final EOGrhumParametres getParametre(EOEditingContext ed, String cle) {
		//return FinderParametre.sharedInstance().getUnParametre(ed, cle);
		return getUnParametre(ed, cle);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un NSArray de EOGrhumParametres
	 */
	public static final NSArray getParametres(EOEditingContext ed, String cle) {
		return getLesParametres(ed, cle);
	}

	/**
	 * Recherche d'un parametre par son code et son exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un EOGrhumParametres
	 */
	private static EOGrhumParametres getUnParametre(EOEditingContext ed, String cle) {
		NSArray parametres = getLesParametres(ed, cle);

		if (parametres == null || parametres.count() == 0)
			return null;

		return (EOGrhumParametres) parametres.objectAtIndex(0);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un NSArray de EOGrhumParametres
	 */
	private static NSArray getLesParametres(EOEditingContext ed, String cle) {
		NSArray parametres = Finder.fetchArray(EOGrhumParametres.ENTITY_NAME,
				EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + "=%@", new NSArray(cle)), null, ed, false);

		if (parametres == null || parametres.count() == 0)
			return null;

		return parametres;
	}

	/**
	 * Fetch tous les parametres pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	//	private static NSArray fetchParametres(EOEditingContext ed) {
	//		// if (arrayParametres==null)
	//			return Finder.fetchArray(ed,EOGrhumParametres.ENTITY_NAME,null,null,null,false);
	//	}

	/**
	 * Recherche la valeur du parametre autorisant la creation d'une PI a partir d'une commande suivant l'exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return cStructure (String)
	 */
	public static final String getParametreStructureFournisseursInternes(EOEditingContext ed) {
		EOGrhumParametres parametre = getUnParametre(ed, EOGrhumParametres.ANNUAIRE_FOU_VALIDE_INTERNE);

		if (parametre == null || parametre.paramValue() == null)
			return null;

		return parametre.paramValue();
	}
}
