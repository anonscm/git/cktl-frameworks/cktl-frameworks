/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAdresse;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktldepense.server.metier.EOStructure;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderAdresse extends Finder {

	/**
	 * Recherches des adresses d'un fournisseur <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param fournisseur fournisseur pour lequel on cherche les adresses
	 * @return un NSArray contenant des EOAdresse
	 */
	public static final NSArray<EOAdresse> getAdressesPourFournisseur(EOEditingContext ed, EOFournisseur fournisseur) {
		if (fournisseur == null)
			throw new FactoryException("le fournisseur est obligatoire");

		if (fournisseur.personne() == null)
			return NSArray.emptyArray();

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(fournisseur.personne(), "personne");

		NSMutableArray resultats = new NSMutableArray();
		resultats.addObjectsFromArray(getAdresses(ed, bindings));

		if (resultats.count() > 1 && fournisseur.adresse() != null) {
			resultats.removeObject(fournisseur.adresse());
			resultats.insertObjectAtIndex(fournisseur.adresse(), 0);
		}

		return resultats;
	}

	/**
	 * Recherches des adresses d'une structure <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param structure structure pour laquelle on cherche les adresses
	 * @return un NSArray contenant des EOAdresse
	 */
	public static final NSArray<EOAdresse> getAdressesPourStructure(EOEditingContext ed, EOStructure structure) {
		if (structure == null)
			throw new FactoryException("la structure est obligatoire");

		if (structure.personne() == null)
			return NSArray.emptyArray();

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(structure.personne(), "personne");

		return getAdresses(ed, bindings);
	}

	/**
	 * Recherche d'adresses par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : personne, tadrCode <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOAdresse
	 */
	private static final NSArray<EOAdresse> getAdresses(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null || bindings.objectForKey("personne") == null)
			throw new FactoryException("le binding 'personne' est obligatoire");

		NSArray resultats = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EORepartPersonneAdresse.ENTITY_NAME, "Recherche", bindings);
		if (resultats == null || resultats.count() == 0)
			return NSArray.emptyArray();
		NSArray adresses = (NSArray) resultats.valueForKeyPath(EORepartPersonneAdresse.ADRESSE_KEY);

		if (adresses == null || adresses.count() == 0)
			return NSArray.emptyArray();

		NSMutableArray adressesDistinctes = new NSMutableArray();

		for (int i = 0; i < adresses.count(); i++) {
			EOAdresse adresse = (EOAdresse) adresses.objectAtIndex(i);

			if (!adressesDistinctes.containsObject(adresse))
				adressesDistinctes.addObject(adresse);
		}

		return adressesDistinctes;
	}
}
