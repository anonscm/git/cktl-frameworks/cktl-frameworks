/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOConventionNonLimitative;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public final class FinderConvention extends Finder {

	public static final String BINDING_EXERCICE = "exercice";
	public static final String BINDING_ORGAN = "organ";
	public static final String BINDING_TYPE_CREDIT = "typeCredit";
	public static final String BINDING_CONV_MODE_GESTION_NOT_EQUAL = "convModeGestion";
	public static final String BINDING_CONV_REFERENCE_LIKE = "convReference";
	public static final String BINDING_CON_DATE_CLOTURE_MAX = "conDateClotureMax";
	public static final String BINDING_CON_DATE_FIN_PAIEMENT_MAX = "conDateFinPaiementMax";
	public static final String BINDING_UTILISATEUR_KEY = "utilisateur";
	public static final String BINDING_CONVENTION_KEY = "convention";

	/**
	 * Recherche les conventions simples disponibles pour une ligne budgetaire et un type de credit par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : organ, typeCredit, exercice, convModeGestion, convReference <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOConvention
	 */
	public static final NSArray<EOConvention> getConventions(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");

		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(ERXQ.equals(EOConventionNonLimitative.EXERCICE_KEY, bindings.objectForKey(BINDING_EXERCICE)));
		if (bindings.objectForKey(BINDING_ORGAN) != null) {
			quals.addObject(ERXQ.equals(EOConventionNonLimitative.ORGAN_KEY, bindings.objectForKey(BINDING_ORGAN)));
		}
		if (bindings.objectForKey(BINDING_TYPE_CREDIT) != null) {
			quals.addObject(ERXQ.equals(EOConventionNonLimitative.TYPE_CREDIT_KEY, bindings.objectForKey(BINDING_TYPE_CREDIT)));
		}
		if (bindings.objectForKey(BINDING_CONV_MODE_GESTION_NOT_EQUAL) != null) {
			quals.addObject(ERXQ.notEquals(EOConventionNonLimitative.CONV_MODE_GESTION_KEY, bindings.objectForKey(BINDING_CONV_MODE_GESTION_NOT_EQUAL)));
		}
		if (bindings.objectForKey(BINDING_CONV_REFERENCE_LIKE) != null) {
			quals.addObject(ERXQ.likeInsensitive(EOConventionNonLimitative.CONV_REFERENCE_KEY, bindings.objectForKey(BINDING_CONV_REFERENCE_LIKE)));
		}
		if (bindings.objectForKey(BINDING_UTILISATEUR_KEY) != null) {
			quals.addObject(ERXQ.equals(EOConventionNonLimitative.ORGAN_KEY + "." + EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurOrgan.UTILISATEUR_KEY, bindings.objectForKey(BINDING_UTILISATEUR_KEY)));
		}
		if (bindings.objectForKey(BINDING_CONVENTION_KEY) != null) {
			quals.addObject(ERXQ.equals(EOConventionNonLimitative.CONVENTION_KEY, bindings.objectForKey(BINDING_CONVENTION_KEY)));
		}
		if (bindings.objectForKey(BINDING_CON_DATE_CLOTURE_MAX) != null) {
			quals.addObject(ERXQ.or(ERXQ.isNull(EOConventionNonLimitative.CON_DATE_CLOTURE_KEY), ERXQ.greaterThanOrEqualTo(EOConventionNonLimitative.CON_DATE_CLOTURE_KEY, bindings.objectForKey(BINDING_CON_DATE_CLOTURE_MAX))));
		}
		if (bindings.objectForKey(BINDING_CON_DATE_FIN_PAIEMENT_MAX) != null) {
			quals.addObject(ERXQ.or(ERXQ.isNull(EOConventionNonLimitative.CON_DATE_FIN_PAIEMENT_KEY), ERXQ.greaterThanOrEqualTo(EOConventionNonLimitative.CON_DATE_FIN_PAIEMENT_KEY, bindings.objectForKey(BINDING_CON_DATE_FIN_PAIEMENT_MAX))));
		}

		//NSArray<EOConventionNonLimitative> larray = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOConventionNonLimitative.ENTITY_NAME, "Recherche", bindings);
		NSArray<EOConventionNonLimitative> larray = EOConventionNonLimitative.fetchAll(ed, ERXQ.and(quals));
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(miseEnFormeTableau(ed, larray, bindings), sort());
	}

	@SuppressWarnings("unchecked")
	public static final NSArray<EOConvention> getConventionsRessourcesAffectees(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");

		NSArray<EOConventionNonLimitative> larray = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOConventionNonLimitative.ENTITY_NAME, "RechercheRA", bindings);

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(miseEnFormeTableau(ed, larray, bindings), sort());
	}

	private static NSArray<EOConvention> miseEnFormeTableau(EOEditingContext ed, NSArray<EOConventionNonLimitative> larray, NSDictionary<String, Object> bindings) {
		NSMutableArray<EOConvention> conventions = new NSMutableArray<EOConvention>();
		for (int i = 0; i < larray.count(); i++) {
			EOConventionNonLimitative conventionNonLimitative = (EOConventionNonLimitative) larray.objectAtIndex(i);
			EOConvention convention = conventionNonLimitative.convention();

			if (bindings.objectForKey("montants") != null && bindings.objectForKey("montants") instanceof Boolean &&
					((Boolean) bindings.objectForKey("montants")).booleanValue()) {
				convention.setDisponible(conventionNonLimitative.convDispo());
				convention.setMontant(conventionNonLimitative.convMontant());
			}

			if (EOQualifier.filteredArrayWithQualifier(conventions, EOQualifier.qualifierWithQualifierFormat(EOConvention.CONV_ORDRE_KEY + "=%@",
					new NSArray<Object>(EOUtilities.primaryKeyForObject(ed, convention).objectForKey(EOConvention.CONV_ORDRE_KEY)))).count() == 0)
				conventions.addObject(convention);
		}

		return conventions;
	}

	private static NSArray<EOSortOrdering> sort() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOConvention.CONV_REFERENCE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
	}
}