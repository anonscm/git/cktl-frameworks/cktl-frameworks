/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOLot;
import org.cocktail.fwkcktldepense.server.metier.EOMarche;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderLot extends Finder {

	/**
	 * Recherche de lots par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : marche, date <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOLot
	 */
	public static final NSArray getLotsValidesPourMarche(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		if (bindings == null)
			throw new FactoryException("le bindings 'date' est obligatoire");
		if (bindings.objectForKey("date") == null)
			throw new FactoryException("le bindings 'date' est obligatoire");

		mesBindings.setObjectForKey(EOLot.ETAT_VALIDE, "lotValide");
		mesBindings.setObjectForKey(EOLot.ETAT_NON_SUPPRIME, "lotSuppr");

		mesBindings.setObjectForKey(EOMarche.ETAT_VALIDE, "marValide");
		mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "marSuppr");

		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOLot.ENTITY_NAME, "Recherche", mesBindings);
	}
}
