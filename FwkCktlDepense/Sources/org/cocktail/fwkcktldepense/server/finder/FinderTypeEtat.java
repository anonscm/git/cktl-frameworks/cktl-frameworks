/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public final class FinderTypeEtat extends Finder {

	/**
	 * Recherche d'un type etat par son libelle. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle libelle auquel correspond l'etat
	 * @return un EOTypeEtat
	 */
	public static final EOTypeEtat getTypeEtat(EOEditingContext ed, String libelle) {
		return getUnTypeEtat(ed, libelle);
	}

	/**
	 * Recherche d'un type etat par son libelle. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle libelle auquel correspond l'etat
	 * @return un EOTypeEtat
	 */
	private static EOTypeEtat getUnTypeEtat(EOEditingContext ed, String libelle) {
		NSArray typeEtats = null;
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(libelle));
		typeEtats = Finder.fetchArray(EOTypeEtat.ENTITY_NAME, qual, null, ed, true);
		/*
		 * NSArray arrayTypeEtats=fetchTypeEtats(ed);
		 * 
		 * typeEtats=new NSArray((NSArray)(EOQualifier.filteredArrayWithQualifier(arrayTypeEtats,
		 * EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY+"=%@", new NSArray(libelle)))));
		 */
		if (typeEtats == null || typeEtats.count() == 0)
			return null;

		return (EOTypeEtat) typeEtats.objectAtIndex(0);
	}

	/**
	 * Fetch tous les types d'etat pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray fetchTypeEtats(EOEditingContext ed) {
		// if (arrayTypeEtats==null)
		return Finder.fetchArray(ed, EOTypeEtat.ENTITY_NAME, null, null, null, false);
	}

	/**
	 * Recherche les types etat possibles pour une EOCommande. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @return un NSArray de EOTypeEtat
	 */
	public static NSArray getTypeEtatsDepense(EOEditingContext ed) {
		NSArray typeEtatsDepense = null;
		NSArray etatsDepense = new NSArray(new String[] {
				EOCommande.ETAT_ANNULEE, EOCommande.ETAT_ENGAGEE, EOCommande.ETAT_PARTIELLEMENT_ENGAGEE, EOCommande.ETAT_PARTIELLEMENT_SOLDEE, EOCommande.ETAT_PRECOMMANDE, EOCommande.ETAT_SOLDEE
		});
		NSArray qualifiers = new NSArray();

		NSArray arrayTypeEtats = fetchTypeEtats(ed);
		Enumeration enumEtatsDepense = etatsDepense.objectEnumerator();
		while (enumEtatsDepense.hasMoreElements()) {
			String unEtat = (String) enumEtatsDepense.nextElement();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(unEtat));
			qualifiers = qualifiers.arrayByAddingObject(qual);
		}
		EOOrQualifier qualTypeEtatsDepense = new EOOrQualifier(qualifiers);
		typeEtatsDepense = new NSArray((NSArray) (EOQualifier.filteredArrayWithQualifier(arrayTypeEtats, qualTypeEtatsDepense)));

		return typeEtatsDepense;
	}
}
