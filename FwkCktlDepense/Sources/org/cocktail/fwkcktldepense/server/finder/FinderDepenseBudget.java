package org.cocktail.fwkcktldepense.server.finder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOExtourneLiq;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class FinderDepenseBudget implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final FinderDepenseBudget INSTANCE = new FinderDepenseBudget();

	public static FinderDepenseBudget instance() {
		return INSTANCE;
	}

	private FinderDepenseBudget() {
	}

	public long countLiquidationsExtourneNonSoldees(EOEditingContext editingContext, EOExercice exercice, EOUtilisateur utilisateur) {
		// non liquidees
		EOQualifier clauseLiquidees = ERXQ.notEquals(
				EOExtourneLiq.TO_DEPENSE_BUDGET_N1
						.append(EODepenseBudget.ENGAGEMENT_BUDGET)
						.append(EOEngagementBudget.ENG_TTC_SAISIE).key(),
				BigDecimal.ZERO);

		// exercice
		EOQualifier clauseExercice = ERXQ.is(
				EOExtourneLiq.TO_DEPENSE_BUDGET_N1.append(EODepenseBudget.ENGAGEMENT_BUDGET).append(EOEngagementBudget.EXERCICE).key(),
				exercice);

		// utilisateur
		EOQualifier clauseUtilisateur = ERXQ.is(
				EOExtourneLiq.TO_DEPENSE_BUDGET_N1.append(EODepenseBudget.ENGAGEMENT_BUDGET).append(EOEngagementBudget.UTILISATEUR).key(),
				utilisateur);

		// fonctions utilisateurs ?

		EOQualifier criteria = ERXQ.and(clauseExercice, clauseLiquidees, clauseUtilisateur);
		Integer countResult = ERXEOControlUtilities.objectCountWithQualifier(editingContext, EOExtourneLiq.ENTITY_NAME, criteria);
		return countResult.longValue();
	}

	public List<EODepenseBudget> fetchLiquidationsExtourne(EOEditingContext editingContext, EOUtilisateur utilisateur,
			EOQualifier restrictions, List<EOSortOrdering> sorts) {

		// lie a l'extourne
		EOQualifier clauseTypeExtourne = ERXQ.isNotNull(EODepenseBudget.TO_EXTOURNE_LIQ_N1S.append(EOExtourneLiq.DEP_ID_N1_KEY).key());

		// restriction utilisateur
		EOQualifier clauseUtilisateur = ERXQ.is(EODepenseBudget.ENGAGEMENT_BUDGET.append(EOEngagementBudget.ORGAN).append(EOOrgan.UTILISATEUR_ORGANS).append(EOUtilisateurOrgan.UTILISATEUR).key(), utilisateur);

		EOQualifier criteria = ERXQ.and(clauseTypeExtourne, clauseUtilisateur, restrictions);

		// verifie le tri
		NSArray<EOSortOrdering> sortsAsArray = null;
		if (sorts != null && sorts.size() > 0) {
			sortsAsArray = new NSArray<EOSortOrdering>(sorts);
		}

		return EODepenseBudget.fetchAll(editingContext, criteria, sortsAsArray);
	}
}
