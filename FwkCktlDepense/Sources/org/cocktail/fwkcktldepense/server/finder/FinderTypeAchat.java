/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.metier.EOCatalogue;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCodeExer;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSSet;

public final class FinderTypeAchat extends Finder {

	/**
	 * Recherche d'un type achat par son libelle. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle libelle auquel correspond l'etat
	 * @return un EOTypeAchat
	 */
	public static final EOTypeAchat getTypeAchat(EOEditingContext ed, String libelle) {
		return getUnTypeAchat(ed, libelle);
	}

	/**
	 * Recherche des types achat correspondants a un codeExer <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param codeExer EOCodeExer pour lequel faire la recherche
	 * @return un NSArray de EOTypeAchat
	 */
	public static final NSArray<EOTypeAchat> getTypeAchats(EOEditingContext ed, EOCodeExer codeExer) {
		NSMutableArray<EOTypeAchat> array = new NSMutableArray<EOTypeAchat>();

		if (codeExer.ceMonopole().intValue() == 1)
			array.addObject(getUnTypeAchat(ed, EOTypeAchat.MONOPOLE));
		if (codeExer.ceAutres().intValue() == 1)
			array.addObject(getUnTypeAchat(ed, EOTypeAchat.SANS_ACHAT));
		if (codeExer.ce3cmp().intValue() == 1)
			array.addObject(getUnTypeAchat(ed, EOTypeAchat.TROIS_CMP));

		if (array.count() == 0)
			array.addObject(getUnTypeAchat(ed, EOTypeAchat.MARCHE_NON_FORMALISE));

		return array.immutableClone();
	}

	/**
	 * Recherche des types achat communs dans une liste d'articles <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param articles NSArray liste d'articles
	 * @return un NSArray de EOTypeAchat
	 */
	public static final NSArray<EOTypeAchat> getTypeAchatsCommuns(EOEditingContext ed, NSArray articles) {
		NSMutableArray lesTypesAchatsCommuns = new NSMutableArray();

		// TODO : si article HM et article Marche ...

		if (articles != null && articles.count() > 0) {
			NSMutableArray tousLesTypesAchats = new NSMutableArray();
			Enumeration enumArticles = articles.objectEnumerator();
			// Construction d'un tableau d'ensembles (NSArray(NSSet)) des types achat de chaque article
			while (enumArticles.hasMoreElements()) {
				EOCatalogue unArticle = (EOCatalogue) enumArticles.nextElement();
				EOCodeExer unCodeExer = unArticle.codeExer();
				NSArray lesTypesAchats = getTypeAchats(ed, unCodeExer);
				tousLesTypesAchats.addObject(new NSSet(lesTypesAchats));
			}
			// On calcule l'intersection entre tous les ensembles
			NSMutableSet typesAchatCommuns = new NSMutableSet((NSSet) tousLesTypesAchats.objectAtIndex(0));
			for (int i = 1; i < tousLesTypesAchats.count(); i++) {
				NSSet set1 = (NSSet) tousLesTypesAchats.objectAtIndex(1);
				typesAchatCommuns.intersectSet(set1);
			}
			lesTypesAchatsCommuns.addObjectsFromArray(typesAchatCommuns.allObjects());
		}

		return lesTypesAchatsCommuns;
	}

	public static final NSArray getTypeAchatsCommunsCodeExers(EOEditingContext ed, NSArray codeExers) {
		NSMutableArray lesTypesAchatsCommuns = new NSMutableArray();

		// TODO : si article HM et article Marche ...

		if (codeExers != null && codeExers.count() > 0) {
			NSMutableArray tousLesTypesAchats = new NSMutableArray();
			Enumeration enumArticles = codeExers.objectEnumerator();
			// Construction d'un tableau d'ensembles (NSArray(NSSet)) des types achat de chaque article
			while (enumArticles.hasMoreElements()) {
				EOCodeExer unCodeExer = (EOCodeExer) enumArticles.nextElement();
				NSArray lesTypesAchats = getTypeAchats(ed, unCodeExer);
				tousLesTypesAchats.addObject(new NSSet(lesTypesAchats));
			}
			// On calcule l'intersection entre tous les ensembles
			NSMutableSet typesAchatCommuns = new NSMutableSet((NSSet) tousLesTypesAchats.objectAtIndex(0));
			for (int i = 1; i < tousLesTypesAchats.count(); i++) {
				NSSet set1 = (NSSet) tousLesTypesAchats.objectAtIndex(1);
				typesAchatCommuns.intersectSet(set1);
			}
			lesTypesAchatsCommuns.addObjectsFromArray(typesAchatCommuns.allObjects());
		}

		return lesTypesAchatsCommuns;
	}

	/**
	 * Recherche des types achat communs dans une liste de codes exer <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param articles NSArray liste de codeExer
	 * @return un NSArray de EOTypeAchat
	 */
	public static final NSArray getTypeAchatsCommunsPourCodeExers(EOEditingContext ed, EOExercice exercice, NSArray codeExers) {
		NSMutableArray lesTypesAchatsCommuns = new NSMutableArray();

		if (codeExers != null && codeExers.count() > 0) {
			NSMutableArray tousLesTypesAchats = new NSMutableArray();
			Enumeration enumCodeExers = codeExers.objectEnumerator();
			// Construction d'un tableau d'ensembles (NSArray(NSSet)) des types achat de chaque codeExer
			while (enumCodeExers.hasMoreElements()) {
				EOXMLCodeExer unXMLCodeExer = (EOXMLCodeExer) enumCodeExers.nextElement();
				EOCodeExer unCodeExer = FinderCodeExer.getCodeExer(ed, unXMLCodeExer, exercice);
				NSArray lesTypesAchats = getTypeAchats(ed, unCodeExer);
				tousLesTypesAchats.addObject(new NSSet(lesTypesAchats));
			}
			// On calcule l'intersection entre tous les ensembles
			NSMutableSet typesAchatCommuns = new NSMutableSet((NSSet) tousLesTypesAchats.objectAtIndex(0));
			for (int i = 1; i < tousLesTypesAchats.count(); i++) {
				NSSet set1 = (NSSet) tousLesTypesAchats.objectAtIndex(1);
				typesAchatCommuns.intersectSet(set1);
			}
			lesTypesAchatsCommuns.addObjectsFromArray(typesAchatCommuns.allObjects());
		}

		return lesTypesAchatsCommuns;
	}

	/**
	 * Recherche d'un type achat par son libelle. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle libelle auquel correspond l'etat
	 * @return un EOTypeAchat
	 */
	private static EOTypeAchat getUnTypeAchat(EOEditingContext ed, String libelle) {
		NSArray typeAchats = null;

		NSArray arrayTypeAchats = fetchTypeAchats(ed);

		typeAchats = new NSArray((NSArray) (EOQualifier.filteredArrayWithQualifier(arrayTypeAchats,
				EOQualifier.qualifierWithQualifierFormat(EOTypeAchat.TYPA_LIBELLE_KEY + "=%@", new NSArray(libelle)))));

		if (typeAchats == null || typeAchats.count() == 0)
			return null;

		return (EOTypeAchat) typeAchats.objectAtIndex(0);
	}

	/**
	 * Fetch tous les types d'etat pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray fetchTypeAchats(EOEditingContext ed) {
		// if (arrayTypeAchats==null)
		return Finder.fetchArray(ed, EOTypeAchat.ENTITY_NAME, null, null, null, false);
	}
}
