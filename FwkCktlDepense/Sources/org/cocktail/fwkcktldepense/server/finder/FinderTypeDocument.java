/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.metier.EOTypeDocument;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public final class FinderTypeDocument extends Finder {

	/**
	 * Recherche d'un type document par son libelle. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle libelle auquel correspond le type document
	 * @return un EOTypeDocument
	 */
	public static final EOTypeDocument getTypeDocument(EOEditingContext ed, String libelle) {
		return getUnTypeDocument(ed, libelle);
	}

	/**
	 * Recherche d'un type document par son libelle. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param libelle libelle auquel correspond le type document
	 * @return un EOTypeDocument
	 */
	private static EOTypeDocument getUnTypeDocument(EOEditingContext ed, String libelle) {
		NSArray typeDocuments = null;
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeDocument.TCOM_LIBELLE_KEY + "=%@", new NSArray(libelle));
		typeDocuments = Finder.fetchArray(EOTypeDocument.ENTITY_NAME, qual, null, ed, true);
		/*
		 * NSArray arrayTypeEtats=fetchTypeEtats(ed);
		 * 
		 * typeEtats=new NSArray((NSArray)(EOQualifier.filteredArrayWithQualifier(arrayTypeEtats,
		 * EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY+"=%@", new NSArray(libelle)))));
		 */
		if (typeDocuments == null || typeDocuments.count() == 0)
			return null;

		return (EOTypeDocument) typeDocuments.objectAtIndex(0);
	}

	/**
	 * Fetch tous les types document pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray fetchTypeDocuments(EOEditingContext ed) {
		// if (arrayTypeEtats==null)
		return Finder.fetchArray(ed, EOTypeDocument.ENTITY_NAME, null, null, null, false);
	}

	/**
	 * Recherche les types document possibles pour une EOCommande. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @return un NSArray de EOTypeDocument
	 */
	public static NSArray getTypeDocumentsCommande(EOEditingContext ed) {
		NSArray typeDocumentsCommande = null;
		NSArray libelles = new NSArray(new String[] {
				EOTypeDocument.TYPE_BON_DE_COMMANDE, EOTypeDocument.TYPE_DEVIS, EOTypeDocument.TYPE_DIVERS, EOTypeDocument.TYPE_FACTURE
		});
		NSArray qualifiers = new NSArray();

		NSArray arrayTypeDocuments = fetchTypeDocuments(ed);
		Enumeration enumDocumentsCommande = libelles.objectEnumerator();
		while (enumDocumentsCommande.hasMoreElements()) {
			String unType = (String) enumDocumentsCommande.nextElement();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeDocument.TCOM_LIBELLE_KEY + "=%@", new NSArray(unType));
			qualifiers = qualifiers.arrayByAddingObject(qual);
		}
		typeDocumentsCommande = new NSArray((NSArray) (EOQualifier.filteredArrayWithQualifier(arrayTypeDocuments, new EOOrQualifier(qualifiers))));

		return typeDocumentsCommande;
	}
}
