/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderInventaire extends Finder {

	/**
	 * Recherche les inventaires lies a une commande pour une ligne budgetaire et un type de credit.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande pour laquelle on fait la recherche
	 * @param engage engagement pour lequel on fait la recherche
	 * @return un NSArray de EOInventaire
	 */
	public static final NSArray<EOInventaire> getInventaires(EOEditingContext ed, EOCommande commande, EOEngagementBudget engage) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();

		bindings.setObjectForKey(commande, "commande");
		bindings.setObjectForKey(engage.organ(), "organ");
		bindings.setObjectForKey(engage.typeCredit(), "typeCredit");

		@SuppressWarnings("unchecked")
		NSArray<EOInventaire> resultats = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOInventaire.ENTITY_NAME, "Recherche", bindings);
		if (resultats == null || resultats.count() == 0)
			return NSArray.emptyArray();
		return resultats;
	}

	/**
	 * Recherche les inventaires lies a une EODepenseCtrlPlanco, utile pour le reversement.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param depenseControlePlanComptable depenseControlePlanComptable pour laquelle on fait la recherche
	 * @return un NSArray de EOInventaire
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<EOInventaire> getInventaires(EOEditingContext ed, EODepenseControlePlanComptable depenseControlePlanComptable) {
		if (depenseControlePlanComptable == null)
			return NSArray.emptyArray();
		if (depenseControlePlanComptable.editingContext() == null || depenseControlePlanComptable.editingContext().globalIDForObject(depenseControlePlanComptable).isTemporary())
			return depenseControlePlanComptable.inventaires();

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOInventaire.DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY + "=%@",
				new NSArray<Object>(depenseControlePlanComptable));
		return Finder.fetchArray(EOInventaire.ENTITY_NAME, qual, null, ed, true);
	}
}
