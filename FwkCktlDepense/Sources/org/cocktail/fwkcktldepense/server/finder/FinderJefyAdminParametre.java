/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.metier.EODevise;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOJefyAdminParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public final class FinderJefyAdminParametre extends Finder {

	/**
	 * Recherche d'un parametre par son code et son exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un EOJefyAdminParametre
	 */
	public static final EOJefyAdminParametre getParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		return getUnParametre(ed, cle, exercice);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un NSArray de EOJefyAdminParametre
	 */
	public static final NSArray getParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		return getLesParametres(ed, cle, exercice);
	}

	/**
	 * Recherche d'un parametre par son code et son exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un EOJefyAdminParametre
	 */
	private static EOJefyAdminParametre getUnParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		NSArray parametres = getLesParametres(ed, cle, exercice);

		if (parametres == null || parametres.count() == 0)
			return null;

		return (EOJefyAdminParametre) parametres.objectAtIndex(0);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un NSArray de EOJefyAdminParametre
	 */
	private static NSArray getLesParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		NSArray parametres = null;

		NSArray arrayParametres = fetchParametres(ed);

		parametres = new NSArray((NSArray) (EOQualifier.filteredArrayWithQualifier(arrayParametres,
				EOQualifier.qualifierWithQualifierFormat(EOJefyAdminParametre.PAR_KEY_KEY + "=%@ and " +
						EOJefyAdminParametre.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
						cle, exercice
				})))));

		if (parametres == null || parametres.count() == 0)
			return null;

		return parametres;
	}

	/**
	 * Fetch tous les parametres pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray fetchParametres(EOEditingContext ed) {
		// if (arrayParametres==null)
		return Finder.fetchArray(ed, EOJefyAdminParametre.ENTITY_NAME, null, null, null, false);
	}

	/**
	 * Recherche la valeur du parametre de la devise en cours dans l'exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return le devCode (String)
	 */
	public static final String getDeviseCode(EOEditingContext ed, EOExercice exercice) {
		EOJefyAdminParametre parametre = getUnParametre(ed, EOJefyAdminParametre.DEVISE_DEV_CODE, exercice);

		if (parametre == null || parametre.parValue() == null)
			return EODevise.EURO_CODE;

		return parametre.parValue();
	}

	/**
	 * Recherche la valeur du parametre pour le montant concerné par la limite de la delegation de signature dans l'exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return String : HT, TTC, BUDGETAIRE
	 */
	public static final String getMethodeLimitativeMontantSignataire(EOEditingContext ed, EOExercice exercice) {
		EOJefyAdminParametre parametre = getUnParametre(ed, EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC, exercice);

		if (parametre == null || parametre.parValue() == null)
			return null;

		return parametre.parValue();
	}

	/**
	 * Recherche la valeur du parametre indiquant si le service facturier est disponible. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return String : OUI / NON (par defaut)
	 */
	public static final String getServiceFacturierDispo(EOEditingContext ed, EOExercice exercice) {
		EOJefyAdminParametre parametre = getUnParametre(ed, EOJefyAdminParametre.SERVICE_FACTURIER_DISPO, exercice);

		if (parametre == null || parametre.parValue() == null)
			return EOJefyAdminParametre.SERVICE_FACTURIER_DISPO_DEFAUT;

		return parametre.parValue();
	}

}
