/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux;
import org.cocktail.fwkcktldepense.server.metier.EOJefyAdminParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public final class FinderImTypeTaux extends Finder {

	public static final EOImTypeTaux getTypeTaux(EOEditingContext ed, Integer cle) {
		NSArray resultats = Finder.fetchArray(EOImTypeTaux.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(EOImTypeTaux.IMTT_ID_KEY + "=%@", new NSArray(cle)),
				null, ed, true);
		if (resultats == null || resultats.count() == 0)
			return null;
		return (EOImTypeTaux) resultats.objectAtIndex(0);
	}

	public static final EOImTypeTaux getTypeTauxParDefaut(EOEditingContext ed, EOExercice exercice) {
		EOJefyAdminParametre param = FinderJefyAdminParametre.getParametre(ed, EOJefyAdminParametre.IM_TYPE_TAUX_PAR_DEFAUT, exercice);
		if (param == null || param.parValue() == null)
			return null;
		try {
			return getTypeTaux(ed, new Integer(param.parValue()));
		} catch (Exception e) {
			return null;
		}
	}
}
