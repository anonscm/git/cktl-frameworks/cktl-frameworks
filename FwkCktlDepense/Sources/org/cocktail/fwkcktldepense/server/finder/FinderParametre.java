/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.exception.ParametreException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public final class FinderParametre extends Finder {
	private static Map<String, Map<Integer, Object>> paramCache = new HashMap<String, Map<Integer, Object>>();

	//private static NSArray arrayParametres;
	//private static FinderParametre sharedInstance;

	/**
	 * Permet d'avoir une seule instance de la classe et de garder les parametres en memoire <BR>
	 * 
	 * @return l'instance de FinderParametre
	 */
	/*
	 * private static FinderParametre sharedInstance() { if (sharedInstance == null) { arrayParametres=null; sharedInstance = new FinderParametre(); }
	 * return sharedInstance; }
	 */

	/**
	 * Recherche d'un parametre par son code et son exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un EOParametre
	 */
	public static final EOParametre getParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		//return FinderParametre.sharedInstance().getUnParametre(ed, cle, exercice);
		return getUnParametre(ed, cle, exercice);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un NSArray de EOParametre
	 */
	public static final NSArray getParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		return getLesParametres(ed, cle, exercice);
	}

	/**
	 * Recherche d'un parametre par son code et son exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un EOParametre
	 */
	private static EOParametre getUnParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		NSArray parametres = getLesParametres(ed, cle, exercice);

		if (parametres == null || parametres.count() == 0)
			return null;

		return (EOParametre) parametres.objectAtIndex(0);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle code du parametre
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return un NSArray de EOParametre
	 */
	private static NSArray getLesParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		NSArray parametres = Finder.fetchArray(EOParametre.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(EOParametre.PAR_KEY_KEY + "=%@ and " + EOParametre.EXERCICE_KEY + "=%@",
				new NSArray(new Object[] {
						cle, exercice
				})), null, ed, true);

		if (parametres == null || parametres.count() == 0)
			return null;

		return parametres;
	}

	/**
	 * Fetch tous les parametres pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray fetchParametres(EOEditingContext ed) {
		// if (arrayParametres==null)
		return Finder.fetchArray(ed, EOParametre.ENTITY_NAME, null, null, null, false);
	}

	/**
	 * Recherche la valeur du parametre du niveau a utiliser pour les codes de nomenclature suivant l'exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return le cmNiveau (Integer)
	 */
	public static final Integer getParametreCmNiveau(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.ARTICLE_CM_NIVEAU, exercice);

		if (parametre == null)
			throw new ParametreException(ParametreException.articleCmNiveauManquant);

		try {
			Integer niveau = new Integer(parametre.parValue());
			if (niveau.intValue() != 2 && niveau.intValue() != 3)
				throw new ParametreException(ParametreException.articleCmNiveauIncoherent);
			return niveau;
		} catch (NumberFormatException e) {
			throw new ParametreException(ParametreException.articleCmNiveauIncoherent);
		}
	}

	public static final BigDecimal getParametreDepenseComptantMax(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.DEPENSE_COMPTANT_MAX, exercice);

		if (parametre == null)
			return new BigDecimal(0.0);

		try {
			BigDecimal max = new BigDecimal(parametre.parValue());
			return max;
		} catch (NumberFormatException e) {
			return new BigDecimal(0.0);
		}
	}

	public static final boolean getParametreInventaireObligatoire(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.INVENTAIRE_OBLIGATOIRE, exercice);

		if (parametre == null)
			throw new ParametreException(ParametreException.inventaireObligatoireManquant);

		if (parametre.parValue() != null && parametre.parValue().equals("OUI"))
			return true;

		return false;
	}

	public static final boolean getParametreLiquidationPreremplie(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.LIQUIDATION_PREREMPLIE, exercice);

		if (parametre == null)
			throw new ParametreException(ParametreException.liquidationPreremplieManquant);

		if (parametre.parValue() != null && parametre.parValue().equals("OUI"))
			return true;

		return false;
	}

	public static final String getParametreCtrlAction(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.CTRL_ORGAN_DEST, exercice);

		if (parametre == null)
			throw new ParametreException(ParametreException.ctrlOrganDestManquant);

		return parametre.parValue();
	}

	public static final String getParametreMessageImpression(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.MESSAGE_IMPRESSION, exercice);

		if (parametre == null)
			return null;

		if (parametre.parValue() == null || parametre.parValue().equals(""))
			return null;

		return parametre.parValue();
	}

	/**
	 * Recherche la valeur du parametre autorisant la creation d'une PI a partir d'une commande suivant l'exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return boolean
	 */
	public static final boolean getParametreAutorisePIFromCommande(EOEditingContext ed, EOExercice exercice) {
		EOParametre parametre = getUnParametre(ed, EOParametre.AUTORISE_PI_FROM_COMMANDE, exercice);

		if (parametre == null)
			return false;

		if (parametre.parValue() == null || !parametre.parValue().equalsIgnoreCase("OUI"))
			return false;

		return true;
	}

	/**
	 * Recherche si le parametre qui si le prorata de la liquidation doit etre le meme que celui de l'engagement correspondant <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel on cherche ce parametre
	 * @return boolean
	 */
	public static final boolean getParametreDepenseMemeProrataEngagement(EOEditingContext ed, EOExercice exercice) {
		if (!paramCache.containsKey(EOParametre.DEPENSE_IDEM_TAP_ID)) {
			paramCache.put(EOParametre.DEPENSE_IDEM_TAP_ID, new HashMap<Integer, Object>());
		}
		Map<Integer, Object> map = paramCache.get(EOParametre.DEPENSE_IDEM_TAP_ID);
		if (!map.containsKey(exercice.exeExercice())) {
			EOParametre parametre = getUnParametre(ed, EOParametre.DEPENSE_IDEM_TAP_ID, exercice);
			if (parametre == null) {
				throw new ParametreException(ParametreException.depenseIdemTauxProrataManquant);
			}
			map.put(exercice.exeExercice(), parametre.parValue());
		}
		return !"NON".equals(map.get(exercice.exeExercice()));
	}
}
