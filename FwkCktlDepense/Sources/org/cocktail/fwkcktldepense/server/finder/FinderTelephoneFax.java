/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOPersonneTelephone;
import org.cocktail.fwkcktldepense.server.metier.EOStructure;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderTelephoneFax extends Finder {

	public static final NSArray<String> getTelephonesPourStructure(EOEditingContext ed, EOStructure structure) {
		if (structure == null)
			throw new FactoryException("la structure est obligatoire");

		NSMutableDictionary bindings = new NSMutableDictionary();
		bindings.setObjectForKey(structure.personne(), "personne");
		bindings.setObjectForKey("TEL", "typeNo");

		return getTelephonesFax(ed, bindings);
	}

	public static final NSArray<String> getFaxPourStructure(EOEditingContext ed, EOStructure structure) {
		if (structure == null)
			throw new FactoryException("la structure est obligatoire");

		NSMutableDictionary bindings = new NSMutableDictionary();
		bindings.setObjectForKey(structure.personne(), "personne");
		bindings.setObjectForKey("FAX", "typeNo");

		return getTelephonesFax(ed, bindings);
	}

	private static final NSArray<String> getTelephonesFax(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null || bindings.objectForKey("personne") == null || bindings.objectForKey("typeNo") == null)
			throw new FactoryException("les bindings 'personne' et 'typeNo' sont obligatoires");

		NSArray resultats = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOPersonneTelephone.ENTITY_NAME, "Recherche", bindings);
		if (resultats == null || resultats.count() == 0)
			return NSArray.emptyArray();
		NSArray telephones = (NSArray) resultats.valueForKeyPath(EOPersonneTelephone.NO_TELEPHONE_KEY);
		if (telephones == null || telephones.count() == 0)
			return NSArray.emptyArray();

		NSMutableArray telephonesDistinctes = new NSMutableArray();

		for (int i = 0; i < telephones.count(); i++) {
			String telephone = (String) telephones.objectAtIndex(i);
			if (!telephonesDistinctes.containsObject(telephone))
				telephonesDistinctes.addObject(telephone);
		}
		return telephonesDistinctes;
	}
}
