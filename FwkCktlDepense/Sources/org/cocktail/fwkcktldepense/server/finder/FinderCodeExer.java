/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCodeMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCodeExer;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderCodeExer extends Finder {

	/**
	 * Recherche des codeExer possibles pour une commande.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param commande commande pour laquelle on fait la recherche
	 * @return un NSArray contenant des EOCodeExer
	 */
	public static final NSArray<EOCodeExer> getCodeExerPourCommande(EOEditingContext ed, EOCommande commande) {
		if (commande == null || commande.exercice() == null) {
			return NSArray.emptyArray();
		}

		System.out.println("getCodeExerPourCommande");
		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article

		if (commande.typeAchat() != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(commande.exercice(), EOCommande.EXERCICE_KEY);
			bindings.setObjectForKey(commande.typeAchat(), EOCommandeControleHorsMarche.TYPE_ACHAT_KEY);

			if (commande.fournisseur() != null && (commande.typeAchat().typaLibelle().equals(EOTypeAchat.MONOPOLE) ||
					commande.typeAchat().typaLibelle().equals(EOTypeAchat.TROIS_CMP))) {
				bindings.setObjectForKey(commande.fournisseur(), EOCommande.FOURNISSEUR_KEY);
			}
			return getCodeExerValides(ed, bindings);
		}

		if (commande.attribution() != null) {
			return getCodeExers(ed, commande.attribution());
		}

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(commande.exercice(), EOCommande.EXERCICE_KEY);
		return getCodeExerValides(ed, bindings);
	}

	public static final NSArray<EOCodeExer> getCodeExerPourEngagementBudget(EOEditingContext ed, EOEngagementBudget eb) {
		if (eb == null || eb.exercice() == null) {
			return NSArray.emptyArray();
		}

		if (eb.typeAchat() != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(eb.exercice(), EOEngagementBudget.EXERCICE_KEY);
			bindings.setObjectForKey(eb.typeAchat(), EOEngagementControleHorsMarche.TYPE_ACHAT_KEY);

			if (eb.fournisseur() != null && (eb.typeAchat().typaLibelle().equals(EOTypeAchat.MONOPOLE) ||
					eb.typeAchat().typaLibelle().equals(EOTypeAchat.TROIS_CMP))) {
				bindings.setObjectForKey(eb.fournisseur(), EOEngagementBudget.FOURNISSEUR_KEY);
			}
			return getCodeExerValides(ed, bindings);
		}

		if (eb.attribution() != null) {
			return getCodeExers(ed, eb.attribution());
		}

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(eb.exercice(), EOEngagementBudget.EXERCICE_KEY);
		return getCodeExerValides(ed, bindings);
	}

	/**
	 * Recherche des codes nomenclature valides par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : exercice, ceRech, typeAchat, niveau, fournisseur <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOCodeExer
	 */
	public static final NSArray<EOCodeExer> getCodeExerValides(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		// TODO : regarder pour niveau 3 ... ya coucouille
		System.out.println("getCodeExerValides");

		if (bindings == null) {
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		}
		if (bindings.objectForKey(EOCodeExer.EXERCICE_KEY) == null) {
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		}

		mesBindings.setObjectForKey(EOCodeExer.ETAT_VALIDE, EOCodeExer.CE_SUPPR_KEY);
		mesBindings.setObjectForKey(EOCodeMarche.ETAT_VALIDE, EOCodeMarche.CM_SUPPR_KEY);
		mesBindings.setObjectForKey("O", EOCodeExer.CE_ACTIF_KEY);

		
		EOTypeAchat bindingForTypeAchat = (EOTypeAchat) bindings.objectForKey("typeAchat");
		if (bindingForTypeAchat != null) {
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.SANS_ACHAT)) {
				mesBindings.setObjectForKey(new Integer(1), EOCodeExer.CE_AUTRES_KEY);
			}
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.MONOPOLE)) {
				mesBindings.setObjectForKey(new Integer(1), EOCodeExer.CE_MONOPOLE_KEY);
			}
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.TROIS_CMP)) {
				mesBindings.setObjectForKey(new Integer(1), EOCodeExer.CE3CMP_KEY);
			}
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.MARCHE_NON_FORMALISE)) {
				mesBindings.setObjectForKey(new Integer(0), EOCodeExer.CE_AUTRES_KEY);
				mesBindings.setObjectForKey(new Integer(0), EOCodeExer.CE_MONOPOLE_KEY);
				mesBindings.setObjectForKey(new Integer(0), EOCodeExer.CE3CMP_KEY);
			}
		}

		if (bindings.objectForKey("niveau") == null) {
			//        	Integer parametreNiveau=FinderParametre.getParametreCmNiveau(ed, (EOExercice)bindings.objectForKey("exercice"));
			//        	mesBindings.setObjectForKey(parametreNiveau, "niveau");
			mesBindings.setObjectForKey(new Integer(EOCodeExer.niveauEngageable()), "niveau");
		}
		System.out.println(mesBindings);
		return Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOCodeExer.ENTITY_NAME, "Recherche", mesBindings), sort());
	}

	/**
	 * Recherche des codes nomenclature valides pour un exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice EOExercice pour lequel effectuer la recherche
	 * @return un NSArray contenant des EOCodeExer
	 */
	public static final NSArray<EOCodeExer> getCodeExerValidesPourExercice(EOEditingContext ed, EOExercice exercice) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray myResult = null;

		System.out.println("getCodeExerValidesPourExercice");

		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article ?
//		Integer parametreNiveau = FinderParametre.getParametreCmNiveau(ed, exercice);

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_SUPPR_KEY + "=%@", new NSArray(EOCodeExer.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCodeExer.CODE_MARCHE_KEY + "." + EOCodeMarche.CM_SUPPR_KEY + "=%@", new NSArray(EOCodeMarche.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY + "=%@", new NSArray(exercice)));
		//    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
		//    			EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_NIVEAU_KEY+"=%@", new NSArray(parametreNiveau)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCodeExer.CODE_MARCHE_KEY + "." + EOCodeMarche.CM_NIVEAU_KEY + "=%@", new NSArray(new Integer(EOCodeExer.niveauEngageable()))));

		arrayQualifier.addObject(new EOKeyValueQualifier(EOCodeExer.CE_ACTIF_KEY, EOQualifier.QualifierOperatorEqual, "O"));
		
		EOFetchSpecification spec = new EOFetchSpecification(EOCodeExer.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, true, true, null);
		spec.setPrefetchingRelationshipKeyPaths(new NSArray(EOCodeExer.CODE_MARCHE_KEY));
		myResult = new NSArray(ed.objectsWithFetchSpecification(spec));

		return Finder.tableauTrie(myResult, sort());
	}

	/**
	 * Recherche d'un code nomenclature valides correspondant au meme code passe en parametre pour un exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param codeExer EOCodeExer a partir duquel effectuer la recherche
	 * @param exercice EOExercice pour lequel effectuer la recherche
	 * @return un EOCodeExer
	 */
	public static final EOCodeExer getCodeExerEquivalentPourExercice(EOEditingContext ed, EOCodeExer codeExer, EOExercice exercice) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray arrayResultat;

		System.out.println("getCodeExerEquivalentPourExercice");

		if (codeExer.exercice() == exercice)
			return codeExer;

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_SUPPR_KEY + "=%@", new NSArray(EOCodeExer.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCodeExer.CODE_MARCHE_KEY + "." + EOCodeMarche.CM_SUPPR_KEY + "=%@", new NSArray(EOCodeMarche.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY + "=%@", new NSArray(exercice)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY + "=%@", new NSArray(codeExer.codeMarche())));

		arrayResultat = Finder.fetchArray(EOCodeExer.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);

		if (arrayResultat.count() == 1)
			return (EOCodeExer) arrayResultat.objectAtIndex(0);

		return null;
	}

	/**
	 * Recherche le codeExer correspondant a un codeMarche et a un exercice.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param codeMarche codeMarche pour lequel on fait la recherche
	 * @param exercice exercice pour lequel on fait la recherche
	 * @return un EOCodeExer
	 */
	public static final EOCodeExer getCodeExerPourCodeMarcheEtExercice(EOEditingContext ed, EOCodeMarche codeMarche, EOExercice exercice) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray arrayResultat;

		System.out.println("getCodeExerPourCodeMarcheEtExercice");

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY + "=%@", new NSArray(exercice)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CODE_MARCHE_KEY + "=%@", new NSArray(codeMarche)));

		arrayResultat = Finder.fetchArray(EOCodeExer.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);
		if (arrayResultat.count() == 1)
			return (EOCodeExer) arrayResultat.objectAtIndex(0);

		return null;
	}

	/**
	 * Recherche du codeExer de niveau 2 correspondant au codeExer passe en parametre.<BR>
	 * 
	 * @param codeExer codeExer pour lequel on fait la recherche
	 * @return un EOCodeExer
	 */
	public static EOCodeExer getCodeExerNiveau2(EOCodeExer codeExer) {
		if (codeExer == null || codeExer.codeMarche() == null || codeExer.codeMarche().cmNiveau() == null ||
				codeExer.codeMarche().cmNiveau().intValue() < EOCodeExer.niveauEngageable())
			return null;

		if (codeExer.codeMarche().cmNiveau().intValue() == EOCodeExer.niveauEngageable())
			return codeExer;

		if (codeExer.codeMarche().cmNiveau().intValue() == EOCodeExer.niveauEngageable() + 1)
			return codeExer.getPere();

		return null;
	}

	/**
	 * Recherche les CodeExer autorises pour cette attribution via son lot.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param attribution attribution pour laquelle on fait la recherche
	 * @return un NSArray contenant des EOCodeExer
	 */
	public static final NSArray<EOCodeExer> getCodeExers(EOEditingContext ed, EOAttribution attribution) {

		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article
		System.out.println("getCodeExers");

		NSMutableArray localSort = new NSMutableArray();
		localSort.addObject(EOSortOrdering.sortOrderingWithKey(EOLotNomenclature.CODE_EXER_KEY + "." + EOCodeExer.CODE_MARCHE_KEY + "." + EOCodeMarche.CM_CODE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));
		localSort.addObject(EOSortOrdering.sortOrderingWithKey(EOLotNomenclature.CODE_EXER_KEY + "." + EOCodeExer.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));

		NSArray array = Finder.fetchArray(EOLotNomenclature.ENTITY_NAME,
				EOQualifier.qualifierWithQualifierFormat(EOLotNomenclature.LOT_KEY + "=%@", new NSArray(attribution.lot())), localSort, ed, false);

		return (NSArray) array.valueForKeyPath(EOLotNomenclature.CODE_EXER_KEY);
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		System.out.println("sort");

		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOCodeExer.CODE_MARCHE_KEY + "." + EOCodeMarche.CM_CODE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOCodeExer.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

	public static EOCodeExer getCodeExer(EOEditingContext ed, EOXMLCodeExer unXMLCodeExer, EOExercice exercice) {
		EOCodeExer codeExer = null;

		System.out.println("getCodeExer");

		Number ceOrdre = unXMLCodeExer.ceOrdre();
		if (ceOrdre != null) {
			codeExer = (EOCodeExer) EOUtilities.objectWithPrimaryKeyValue(ed, EOCodeExer.ENTITY_NAME, ceOrdre);
		}
		else {
			Number cmOrdre = unXMLCodeExer.cmOrdre();
			if (cmOrdre != null) {
				EOCodeMarche cm = (EOCodeMarche) EOUtilities.objectWithPrimaryKeyValue(ed, EOCodeMarche.ENTITY_NAME, cmOrdre);
				codeExer = getCodeExerPourCodeMarcheEtExercice(ed, cm, exercice);
			}
		}
		return codeExer;
	}
}
