/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public final class FinderTva extends Finder {

	/**
	 * Recherche des taux de tva valides. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @return un NSArray de EOTva
	 */
	public static final NSArray<EOTva> getTvas(EOEditingContext ed) {
		return getLesTva(ed);
	}

	public static EOTva getTvaForTaux(EOEditingContext edc, BigDecimal taux) {
		EOTva tva = null;

		tva = (EOTva) EOUtilities.objectMatchingKeyAndValue(edc, EOTva.ENTITY_NAME, EOTva.TVA_TAUX_KEY, taux);

		return tva;
	}

	/**
	 * Recherche des taux de tva valides. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @return un NSArray de EOTva
	 */
	private static NSArray<EOTva> getLesTva(EOEditingContext ed) {
		NSArray arrayTva = fetchTvas(ed);

		return EOQualifier.filteredArrayWithQualifier(arrayTva, EOQualifier.qualifierWithQualifierFormat(
				EOTva.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(EOTva.ETAT_VALIDE)));
	}

	/**
	 * Fetch tout pour garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray<EOTva> fetchTvas(EOEditingContext ed) {
		// if (arrayTva==null)
		return Finder.fetchArray(ed, EOTva.ENTITY_NAME, null, null,
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOTva.TVA_TAUX_KEY, EOSortOrdering.CompareDescending)), false);
	}

}
