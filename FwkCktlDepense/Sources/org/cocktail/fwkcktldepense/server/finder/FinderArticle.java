/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public final class FinderArticle extends Finder {

	/**
	 * Recherche d'articles dans les differends catalogue par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : libelle, fournisseur, codeExer, attribution, typeAchat, prixHt, exercice, prixTtc, reference, date, onMarche,
	 * marcheLibelle, attributionLibelle <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOArticleCatalogue et des EOMarcheCatalogue
	 */
	public static final NSArray<EOCatalogueArticle> getArticles(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("attribution") != null && bindings.objectForKey("typeAchat") != null)
			throw new FactoryException("les bindings 'attribution' et 'typeAchat' sont exclusifs");

		return FinderCatalogueArticle.getArticlesValides(ed, bindings);
	}

	public static final NSArray<NSDictionary<String, Object>> getRawRowArticles(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("attribution") != null && bindings.objectForKey("typeAchat") != null)
			throw new FactoryException("les bindings 'attribution' et 'typeAchat' sont exclusifs");

		return FinderCatalogueArticle.getRawRowArticlesValides(ed, bindings);
	}

	/**
	 * Recherche d'un article correspondant a sa cle.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param cle cle de l'article a rechercher
	 * @return EOCatalogue ou null si pas d'article trouve
	 */
	public static final EOCatalogueArticle getArticle(EOEditingContext ed, Integer cle, EOExercice exercice) {
		if (cle == null)
			throw new FactoryException("'cle' doit-etre renseigne");

		EOCatalogueArticle article = null;

		NSArray larray;

		//       	NSMutableDictionary bindings=new NSMutableDictionary();
		//       	bindings.setObjectForKey(cle, "cle");     	
		//       	larray=EOUtilities.objectsWithFetchSpecificationAndBindings(ed,EOCatalogueArticle.ENTITY_NAME,"Recherche",bindings);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.CAAR_ID_KEY + "=%@", new NSArray(cle));

		// on trie sur la date car a ULR un catalogue appartenait a plusieurs attribution ?????
		NSMutableArray arraySort = new NSMutableArray();
		arraySort.addObject(EOSortOrdering.sortOrderingWithKey(EOCatalogueArticle.CAT_DATE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));

		larray = Finder.fetchArray(EOCatalogueArticle.ENTITY_NAME, qual, arraySort, ed, true);

		if (larray != null && larray.count() > 0)
			article = (EOCatalogueArticle) larray.lastObject();

		if (article != null) {
			Integer parametreNiveau = FinderParametre.getParametreCmNiveau(ed, exercice);

			if (article.codeMarche().cmNiveau().intValue() == parametreNiveau.intValue() + 1)
				article.setCodeMarcheRelationship(article.codeMarche().codeMarchePere());

			article.setCodeExer(FinderCodeExer.getCodeExerPourCodeMarcheEtExercice(ed, article.codeMarche(), exercice));
		}

		return article;
	}
}
