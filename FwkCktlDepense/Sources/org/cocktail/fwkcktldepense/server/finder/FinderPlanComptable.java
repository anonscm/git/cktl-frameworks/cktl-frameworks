/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFonction;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPlancoCredit;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderPlanComptable extends Finder {

	/**
	 * Recherche du plan comptable correspondant a un type de credit et a un utilisateur Bindings pris en compte : typeCredit, utilisateur, exercice <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOPlancoComptable
	 */
	public static final NSArray<EOPlanComptable> getPlanComptables(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("les bindings sont obligatoires");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("utilisateur") == null)
			throw new FactoryException("le bindings 'utilisateur' est obligatoire");

		// TODO : OPTIMISATION passe par planco avec fetch sur type credit par une relation sur plancoCredit
		// on recupere les comptes correspondant au type de credit
		NSArray<EOPlancoCredit> larray = FinderPlanComptable.getPlancoCreditPourTypeCredit(ed, bindings);
		NSArray<EOPlanComptable> planComptables = (NSArray<EOPlanComptable>) larray.valueForKeyPath(EOPlancoCredit.PLAN_COMPTABLE_KEY);

		// on regarde si l'utilisateur a les droits sur cet exercice de tous les utiliser
		EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(ed, EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
		if (typeApplication == null)
			throw new FactoryException("Type application " + EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK +
					" (FinderPlanComptable.getPlanComptables(), edc=" + ed + ")");

		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(bindings.objectForKey("utilisateur"), "utilisateur");
		EOFonction fonction = FinderFonction.getFonction(ed, EOFonction.FONCTION_AUTRE_IMPUTATION, typeApplication);
		if (fonction == null)
			throw new FactoryException("Fonction " + EOFonction.FONCTION_AUTRE_IMPUTATION +
					" (FinderPlanComptable.getPlanComptables(), edc=" + ed + ")");
		dico.setObjectForKey(fonction, "fonction");
		dico.setObjectForKey(bindings.objectForKey("exercice"), "exercice");
		if (FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, dico).count() > 0)
			return planComptables;

		// sinon on limite qu'aux comptes 2 et 6
		NSMutableArray arrayQualifier = new NSMutableArray();
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@",
				new NSArray("2*")));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@",
				new NSArray("6*")));

		return Finder.tableauTrie(EOQualifier.filteredArrayWithQualifier(planComptables,
				new EOOrQualifier(arrayQualifier)), sort());
	}

	public static EOPlanComptable getPlanComptable(EOEditingContext ed, EOExercice exercice, String pcoNum) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(pcoNum, "pcoNum");
		bindings.setObjectForKey(exercice, "exercice");

		NSArray array = Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOPlanComptable.ENTITY_NAME, "Recherche", bindings), sort());
		if (array == null || array.count() == 0)
			return null;
		return (EOPlanComptable) array.objectAtIndex(0);
	}

	/**
	 * Recherche des planco credit par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : typeCredit <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOPlancoCredit
	 */
	public static final NSArray<EOPlancoCredit> getPlancoCreditPourTypeCredit(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'typeCredit' est obligatoire");
		if (bindings.objectForKey("typeCredit") == null)
			throw new FactoryException("le bindings 'typeCredit' est obligatoire");

		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		mesBindings.setObjectForKey(EOPlanComptable.PCO_VALIDE, "pcoValide");
		mesBindings.setObjectForKey(EOPlancoCredit.PCC_VALIDE, "pccEtat");
		mesBindings.setObjectForKey(((EOTypeCredit) bindings.objectForKey("typeCredit")).exercice(), "exercice");

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOPlancoCredit.ENTITY_NAME, "Recherche", mesBindings), sort());
	}

	private static NSArray sort() {
		return new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
	}
}
