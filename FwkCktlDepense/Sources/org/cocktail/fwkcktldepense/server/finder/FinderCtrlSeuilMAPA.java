/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.finder;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCodeMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXQ;

public class FinderCtrlSeuilMAPA extends Finder {

	public static final String BINDING_TYPE_ACHAT = "typeAchat";
	public static final String BINDING_EXERCICE = "exercice";

	public static final void checkDepassementSeuil(EOEditingContext edc, EOExercice exercice, EOCtrlSeuilMAPA ctrlSeuilMapa, BigDecimal montantHt) throws Exception {

		if (montantHt.add(ctrlSeuilMapa.montantEngageHt()).compareTo(ctrlSeuilMapa.seuilMin()) > 0) {
			throw new Exception("ATTENTION! Le seuil de " + ctrlSeuilMapa.seuilMin().floatValue() + "&euro; est d&eacute;pass&eacute; sur ce code de nomenclature. Vous entrez dans un M&agrave;PA!");
		}
	}

	/**
	 * 18/03/2014 : normalement pas utilisé. Passage à deprecated dans un premier temps, prevoir de supprimer la methode a terme.
	 * 
	 * @param edc
	 * @param exercice
	 * @param typeAchat
	 * @param fournisseur
	 * @param codeExer
	 * @param attribution
	 * @param montantHt
	 * @throws Exception
	 * @deprecated
	 */
	public static final void checkDepassementSeuil(EOEditingContext edc, EOExercice exercice, EOTypeAchat typeAchat, EOFournisseur fournisseur, EOCodeExer codeExer, EOAttribution attribution, BigDecimal montantHt) throws Exception {
		NSArray<EOCtrlSeuilMAPA> lesSeuils = FinderCtrlSeuilMAPA.getCtrlSeuilMAPAPourTypeAchatEtFournisseur(edc, exercice, typeAchat, fournisseur, attribution);
		EOQualifier qualCodeExer = ERXQ.equals("codeExer", codeExer);
		lesSeuils = EOQualifier.filteredArrayWithQualifier(lesSeuils, qualCodeExer);
		BigDecimal montantHtDejaEngage = BigDecimal.ZERO;
		if (lesSeuils != null && lesSeuils.count() == 1) {
			EOCtrlSeuilMAPA unSeuil = (EOCtrlSeuilMAPA) lesSeuils.lastObject();
			montantHtDejaEngage = unSeuil.montantEngageHt();
			if (montantHt.add(montantHtDejaEngage).compareTo(unSeuil.seuilMin()) > 0) {
				throw new Exception("ATTENTION! Le seuil de " + unSeuil.seuilMin().floatValue() + "&euro; est d&eacute;pass&eacute; sur ce code de nomenclature. Vous entrez dans un M&agrave;PA!");
			}
		}
	}

	public static final NSArray<EOCtrlSeuilMAPA> getCtrlSeuilMAPAPourTypeAchatEtFournisseur(EOEditingContext ed, EOExercice exercice, EOTypeAchat typeAchat, EOFournisseur fournisseur, EOAttribution attribution) {
		if (exercice == null) {
			return NSArray.emptyArray();
		}
		if (attribution != null) {
			return getCtrlSeuilMAPAs(ed, attribution, exercice);
		}
		if (typeAchat != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(exercice, "exercice");
			bindings.setObjectForKey(typeAchat, "typeAchat");

			if (fournisseur != null && (typeAchat.typaLibelle().equals(EOTypeAchat.MONOPOLE) ||
					typeAchat.typaLibelle().equals(EOTypeAchat.TROIS_CMP)))
				bindings.setObjectForKey(fournisseur, "fournisseur");
			return getCtrlSeuilMAPAValides(ed, bindings);
		}

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(exercice, "exercice");
		return getCtrlSeuilMAPAValides(ed, bindings);
	}

	public static final NSArray<EOCtrlSeuilMAPA> getCtrlSeuilMAPAPourCommande(EOEditingContext ed, EOCommande commande) {
		if (commande == null || commande.exercice() == null)
			return NSArray.emptyArray();

		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article

		if (commande.typeAchat() != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(commande.exercice(), "exercice");
			bindings.setObjectForKey(commande.typeAchat(), "typeAchat");

			if (commande.fournisseur() != null && (commande.typeAchat().typaLibelle().equals(EOTypeAchat.MONOPOLE) ||
					commande.typeAchat().typaLibelle().equals(EOTypeAchat.TROIS_CMP)))
				bindings.setObjectForKey(commande.fournisseur(), "fournisseur");
			return getCtrlSeuilMAPAValides(ed, bindings);
		}

		if (commande.attribution() != null)
			return getCtrlSeuilMAPAs(ed, commande.attribution(), commande.exercice());

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(commande.exercice(), "exercice");
		return getCtrlSeuilMAPAValides(ed, bindings);
	}

	/**
	 * Recherche des codes nomenclature valides par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : exercice, ceRech, typeAchat, niveau <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOCtrlSeuilMAPA
	 */
	public static final NSArray<EOCtrlSeuilMAPA> getCtrlSeuilMAPAValides(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		// TODO : regarder pour niveau 3 ... ya coucouille

		if (bindings == null) {
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		}
		if (bindings.objectForKey(EOCtrlSeuilMAPA.EXERCICE_KEY) == null) {
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		}

		mesBindings.setObjectForKey(EOCodeExer.ETAT_VALIDE, EOCodeExer.CE_SUPPR_KEY);
		mesBindings.setObjectForKey(EOCodeMarche.ETAT_VALIDE, EOCodeMarche.CM_SUPPR_KEY);
		mesBindings.setObjectForKey("O", EOCodeExer.CE_ACTIF_KEY);

		EOTypeAchat bindingForTypeAchat = (EOTypeAchat) bindings.objectForKey("typeAchat");
		if (bindingForTypeAchat != null) {
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.SANS_ACHAT)) {
				mesBindings.setObjectForKey(new Integer(1), EOCodeExer.CE_AUTRES_KEY);
			}
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.MONOPOLE)) {
				mesBindings.setObjectForKey(new Integer(1), EOCodeExer.CE_MONOPOLE_KEY);
			}
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.TROIS_CMP)) {
				mesBindings.setObjectForKey(new Integer(1), EOCodeExer.CE3CMP_KEY);
			}
			if (bindingForTypeAchat.typaLibelle().equals(EOTypeAchat.MARCHE_NON_FORMALISE)) {
				mesBindings.setObjectForKey(new Integer(0), EOCodeExer.CE_AUTRES_KEY);
				mesBindings.setObjectForKey(new Integer(0), EOCodeExer.CE_MONOPOLE_KEY);
				mesBindings.setObjectForKey(new Integer(0), EOCodeExer.CE3CMP_KEY);
			}
		}

		if (bindings.objectForKey("niveau") == null) {
			Integer parametreNiveau = FinderParametre.getParametreCmNiveau(ed, (EOExercice) bindings.objectForKey("exercice"));
			mesBindings.setObjectForKey(parametreNiveau, "niveau");
		}

		@SuppressWarnings("unchecked")
		NSArray<EOCtrlSeuilMAPA> seuils = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOCtrlSeuilMAPA.ENTITY_NAME, "Recherche", mesBindings);

		// return Finder.tableauTrie(seuils, sort());
		return seuils;
	}

	/**
	 * Recherche les CodeExer autorises pour cette attribution via son lot
	 * 
	 * @param ed
	 * @param attribution
	 * @return
	 */
	public static final NSArray<EOCtrlSeuilMAPA> getCtrlSeuilMAPAs(EOEditingContext ed, EOAttribution attribution, EOExercice exercice) {

		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article
		NSArray<EOCtrlSeuilMAPA> ctrlSeuilMAPAs = NSArray.emptyArray();
		NSMutableArray localSort = new NSMutableArray();
		localSort.addObject(EOSortOrdering.sortOrderingWithKey(EOLotNomenclature.CODE_EXER_KEY + "." + EOCodeExer.CODE_MARCHE_KEY + "." + EOCodeMarche.CM_CODE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));
		localSort.addObject(EOSortOrdering.sortOrderingWithKey(EOLotNomenclature.CODE_EXER_KEY + "." + EOCodeExer.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending));

		NSArray array = Finder.fetchArray(EOLotNomenclature.ENTITY_NAME,
				EOQualifier.qualifierWithQualifierFormat(EOLotNomenclature.LOT_KEY + "=%@", new NSArray(attribution.lot())), localSort, ed, false);
		NSArray codeExers = (NSArray) array.valueForKeyPath(EOLotNomenclature.CODE_EXER_KEY);
		NSMutableArray qualifiers = new NSMutableArray();
		Enumeration enumCodeExers = codeExers.objectEnumerator();
		while (enumCodeExers.hasMoreElements()) {
			EOCodeExer unCodeExer = (EOCodeExer) enumCodeExers.nextElement();
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCtrlSeuilMAPA.CODE_EXER_KEY + "=%@", new NSArray(unCodeExer));
			qualifiers.addObject(qual);
		}
		EOOrQualifier qualCodeExer = new EOOrQualifier(qualifiers);
		EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(EOCtrlSeuilMAPA.EXERCICE_KEY + "=%@", new NSArray(exercice));
		EOAndQualifier qual = new EOAndQualifier(new NSArray(new EOQualifier[] {
				qualCodeExer, qualExercice
		}));
		EOFetchSpecification fs = new EOFetchSpecification(EOCtrlSeuilMAPA.ENTITY_NAME, qual, null);
		ctrlSeuilMAPAs = ed.objectsWithFetchSpecification(fs);
		return ctrlSeuilMAPAs;
	}

	//    private static NSArray sort() {
	//    	NSMutableArray array=new NSMutableArray();
	//    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOCtrlSeuilMAPA.CM_CODE_KEY,
	//    			EOSortOrdering.CompareCaseInsensitiveAscending));
	//    	return array;
	//    }

}
