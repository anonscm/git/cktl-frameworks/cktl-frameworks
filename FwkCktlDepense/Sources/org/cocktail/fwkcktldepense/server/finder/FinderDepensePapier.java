/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public final class FinderDepensePapier extends Finder {

	/**
	 * Recherche les depensePapier pour une commande<BR>
	 * Bindings pris en compte : commande, engagement<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray de EODepensePapier
	 */
	public static final NSArray<EODepensePapier> getDepensePapier(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSMutableArray engagements = new NSMutableArray();

		if (bindings == null)
			return NSArray.emptyArray();

		if (bindings.objectForKey("commande") != null) {
			EOCommande commande = (EOCommande) bindings.objectForKey("commande");

			if (commande == null || commande.commandeEngagements() == null || commande.commandeEngagements().count() == 0)
				return NSArray.emptyArray();

			engagements.addObjectsFromArray((NSArray) commande.commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY));
		}

		if (bindings.objectForKey("engagement") != null) {
			engagements.addObject((EOEngagementBudget) bindings.objectForKey("engagement"));
		}

		//    	if (engagements!=null && engagements.count()>0) {
		//    		NSMutableArray depenseBudgets=new NSMutableArray();
		//    		for (int i=0; i<engagements.count(); i++) {
		//    			EOEngagementBudget engage=(EOEngagementBudget)engagements.objectAtIndex(i);
		//
		//    			if (engage.depenseBudgets()==null || engage.depenseBudgets().count()==0)
		//    				continue;
		//    			depenseBudgets.addObjectsFromArray(engage.depenseBudgets());
		//    		}
		//
		//    		NSMutableArray depensePapiers=new NSMutableArray();
		//    		for (int i=0; i<depenseBudgets.count(); i++) {
		//    			EODepenseBudget depense=(EODepenseBudget)depenseBudgets.objectAtIndex(i);
		//    			if (!depensePapiers.containsObject(depense.depensePapier()))
		//    				depensePapiers.addObject(depense.depensePapier());
		//    		}
		//
		//    		return depensePapiers;
		//    	}
		if (engagements != null && engagements.count() > 0) {
			NSMutableArray depenseBudgets = new NSMutableArray();
			for (int i = 0; i < engagements.count(); i++) {
				EOEngagementBudget engage = (EOEngagementBudget) engagements.objectAtIndex(i);

				if (engage.currentDepenseBudgets() == null || engage.currentDepenseBudgets().count() == 0)
					continue;
				depenseBudgets.addObjectsFromArray(engage.currentDepenseBudgets());
			}

			NSMutableArray depensePapiers = new NSMutableArray();
			for (int i = 0; i < depenseBudgets.count(); i++) {
				_IDepenseBudget depense = (_IDepenseBudget) depenseBudgets.objectAtIndex(i);
				if (!depensePapiers.containsObject(depense.depensePapier()))
					depensePapiers.addObject(depense.depensePapier());
			}

			return depensePapiers;
		}

		return NSArray.emptyArray();
		//    	return Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
		//    			EOBudgetExecCredit.ENTITY_NAME, "Recherche", bindings), sort());
	}

	/**
	 * Recherche les depensePapier deja existante avec le meme numero de facture pour une commande par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : commande, dppNumeroFacture, exercice, utilisateur<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray de EODepensePapier
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<EODepensePapier> getDepensePapierPourNumeroFacture(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			return NSArray.emptyArray();
		if (bindings.objectForKey("commande") == null && (bindings.objectForKey("exercice") == null || bindings.objectForKey("utilisateur") == null) &&
				(bindings.objectForKey("cle") == null))
			return NSArray.emptyArray();

		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EODepensePapier.ENTITY_NAME, "Recherche", bindings);
	}

	/**
	 * Recherche des depensesPapier via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : dppNumeroFacture, utilisateur, exercice, organ, organUb, organCr, organSsCr, dateDebut, dateFin,<BR>
	 * minDepHT, maxDepHT, minDepTTC, maxDepTTC, exercice, commNumero, fouNom, fouCode, fournisseur, cmCode, cmLib, codeExer,<BR>
	 * engNumero, utilisateurRecherche, attribution, attLibelle, marLibelle, pcoNum, pcoLib, canCode, canLib, convReference, tyacCode, tyacLib<BR>
	 * manNumero, borNumero, dateVisa, dateVisaDebut, dateVisaFin, datePaiement, datePaiementDebut, datePaiementFin, inventaire supprime : minHT,
	 * maxHT, minTTC, maxTTC, artLibelle, artReference, typeEtat, commReference, commLibelle <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des NSDictionary { cle = DPP_ID numero = DPP_NUMERO_FACTURE; dateFacture = DPP_DATE_FACTURE; numeroCommande =
	 *         comm_numero (peut etre null); montantHt = DPP_HT_INITIAL; montantTtc = DPP_TTC_INITIAL; codeFournisseur = FOU_CODE; nomFournisseur =
	 *         FOU_NOM; }
	 */
	public static final NSArray<NSDictionary<String, Object>> getRawRowDepensePapierPourNumeroFacture(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, false, bindings), null);
	}

	public static final int getRawRowCountDepensePapierPourNumeroFacture(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nbreDepenses = -1;
		NSArray depenses = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, true, bindings), null);

		if (depenses != null && depenses.count() == 1) {
			NSDictionary row = (NSDictionary) depenses.lastObject();
			nbreDepenses = ((Number) row.valueForKey("NBREDEPENSES")).intValue();
		}

		return nbreDepenses;
	}

	protected static String constructionChaine(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		String selectClause = "";
		String fromClause = "FROM jefy_depense.DEPENSE_PAPIER dp, jefy_depense.v_fournisseur f, jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_depense.v_utilisateur_organ uo, jefy_depense.commande_engagement commeng, jefy_depense.commande comm";
		String whereClause = "WHERE dp.exe_ordre=" + exeOrdre;
		whereClause += " AND dp.fou_ordre=f.fou_ordre AND dp.dpp_id=db.dpp_id AND db.eng_id=eb.eng_id AND eb.org_id=uo.org_id";
		whereClause += " AND eb.eng_id=commeng.eng_id(+) AND commeng.comm_id=comm.comm_id(+)";
		whereClause += " AND uo.utl_ordre=" + utlOrdre;
		String orderClause = "";

		if (isForCount) {
			selectClause = "SELECT count(DISTINCT db.dep_id) NBREDEPENSES";
		}
		else {
			selectClause = "SELECT distinct dp.dpp_id cle, dp.DPP_NUMERO_FACTURE numero, dp.DPP_DATE_FACTURE dateFacture, f.fou_code codeFournisseur, f.fou_nom nomFournisseur, comm.comm_numero numeroCommande, DPP_HT_INITIAL montantHt, DPP_TTC_INITIAL montantTtc";
			orderClause = "ORDER BY upper(dp.DPP_NUMERO_FACTURE), dp.DPP_DATE_FACTURE";
		}

		if (bindings.objectForKey("utilisateurRecherche") != null)
			whereClause += " AND dp.utl_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOUtilisateur) bindings.objectForKey("utilisateurRecherche")).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		if (bindings.objectForKey("inventaire") != null) {
			if (fromClause.indexOf("depense_ctrl_planco commplanco") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_planco commplanco";
				whereClause += " AND db.dep_id=commplanco.dep_id";
			}
			whereClause += " AND commplanco.dpco_id in (select dpco_id from jefy_depense.v_inventaire where upper(clic_num_complet) like upper('%" +
					bindings.objectForKey("inventaire") + "%') and dpco_id is not null)";
		}

		if (bindings.objectForKey("organ") != null) {
			EOOrgan organ = (EOOrgan) bindings.objectForKey("organ");
			whereClause += " AND eb.org_id=" + (Number) EOUtilities.primaryKeyForObject(ed, organ).objectForKey(EOOrgan.ORG_ID_KEY);
		}

		if (bindings.objectForKey("organUb") != null || bindings.objectForKey("organCr") != null || bindings.objectForKey("organSsCr") != null) {
			if (fromClause.indexOf("jefy_admin.organ vo") == -1) {
				fromClause += ", jefy_admin.organ vo";
				whereClause += " AND eb.org_id=vo.org_id";
			}

			if (bindings.objectForKey("organUb") != null)
				whereClause += " AND upper(vo.org_ub) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organUb")) + "%')";
			if (bindings.objectForKey("organCr") != null)
				whereClause += " AND upper(vo.org_cr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organCr")) + "%')";
			if (bindings.objectForKey("organSsCr") != null)
				whereClause += " AND upper(vo.org_souscr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organSsCr")) + "%')";
		}

		if (bindings.objectForKey("manNumero") != null || bindings.objectForKey("borNumero") != null || bindings.objectForKey("dateVisa") != null ||
				bindings.objectForKey("datePaiment") != null) {
			if (fromClause.indexOf("depense_ctrl_planco commplanco") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_planco commplanco";
				whereClause += " AND db.dep_id=commplanco.dep_id";
			}

			fromClause += ", jefy_depense.v_mandat vman";
			whereClause += " AND commplanco.man_id=vman.man_id";

			if (bindings.objectForKey("manNumero") != null)
				whereClause += " AND vman.man_numero=" + bindings.objectForKey("manNumero");
			if (bindings.objectForKey("borNumero") != null)
				whereClause += " AND vman.bor_num=" + bindings.objectForKey("borNumero");
			if (bindings.objectForKey("dateVisa") != null)
				whereClause += " AND to_char(vman.BOR_DATE_VISA,'dd/mm/yyyy')=" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateVisa"));
			if (bindings.objectForKey("datePaiement") != null)
				whereClause += " AND to_char(vman.MAN_DATE_PAIEMENT,'dd/mm/yyyy')=" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("datePaiement"));
			if (bindings.objectForKey("dateVisaDebut") != null)
				whereClause += " AND to_date(to_char(vman.BOR_DATE_VISA,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateVisaDebut")) + "','dd/mm/yyyy')";
			if (bindings.objectForKey("dateVisaFin") != null)
				whereClause += " AND to_date(to_char(vman.MAN_DATE_PAIEMENT,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateVisaFin")) + "','dd/mm/yyyy')";
			if (bindings.objectForKey("datePaiementDebut") != null)
				whereClause += " AND to_date(to_char(vman.BOR_DATE_VISA,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("datePaiementDebut")) + "','dd/mm/yyyy')";
			if (bindings.objectForKey("datePaiementFin") != null)
				whereClause += " AND to_date(to_char(vman.MAN_DATE_PAIEMENT,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("datePaiementFin")) + "','dd/mm/yyyy')";
		}
		if (bindings.objectForKey("engNumero") != null)
			whereClause += " AND eb.eng_numero=" + bindings.objectForKey("engNumero");

		if (bindings.objectForKey("fouCode") != null)
			whereClause += " AND upper(f.fou_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("fouCode")) + "%')";
		if (bindings.objectForKey("fouNom") != null)
			whereClause += " AND upper(f.fou_nom) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("fouNom")) + "%')";
		if (bindings.objectForKey("fournisseur") != null)
			whereClause += " AND f.fou_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOFournisseur) bindings.objectForKey("fournisseur")).objectForKey(EOFournisseur.FOU_ORDRE_KEY);

		if (bindings.objectForKey("dateDebut") != null)
			whereClause += " AND to_date(to_char(dp.DPP_DATE_FACTURE,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateDebut")) + "','dd/mm/yyyy')";
		if (bindings.objectForKey("dateFin") != null)
			whereClause += " AND to_date(to_char(dp.DPP_DATE_FACTURE,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('" + ZDateUtil.dateToString((NSTimestamp) bindings.objectForKey("dateFin")) + "','dd/mm/yyyy')";

		if (bindings.objectForKey("dppNumeroFacture") != null)
			whereClause += " AND upper(dp.dpp_numero_facture) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("dppNumeroFacture")) + "%')";
		if (bindings.objectForKey("minDepHT") != null)
			whereClause += " AND dp.dpp_ht_initial>=" + bindings.objectForKey("minDepHT");
		if (bindings.objectForKey("maxDepHT") != null)
			whereClause += " AND dp.dpp_ht_initial<=" + bindings.objectForKey("maxDepHT");
		if (bindings.objectForKey("minDepTTC") != null)
			whereClause += " AND dp.dpp_ttc_initial>=" + bindings.objectForKey("minDepTTC");
		if (bindings.objectForKey("maxDepTTC") != null)
			whereClause += " AND dp.dpp_ttc_initial<=" + bindings.objectForKey("maxDepTTC");

		if (bindings.objectForKey("commNumero") != null)
			whereClause += " AND comm.comm_numero=" + bindings.objectForKey("commNumero");

		if (bindings.objectForKey("codeExer") != null) {
			if (fromClause.indexOf("depense_ctrl_hors_marche dhom") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_hors_marche dhom";
				whereClause += " AND db.dep_id=dhom.dep_id";
			}
			whereClause += " AND dhom.ce_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOCodeExer) bindings.objectForKey("codeExer")).objectForKey(EOCodeExer.CE_ORDRE_KEY);
		}

		if (bindings.objectForKey("cmCode") != null) {
			if (fromClause.indexOf("depense_ctrl_hors_marche dhom") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_hors_marche dhom";
				whereClause += " AND db.dep_id=dhom.dep_id";
			}
			if (fromClause.indexOf("jefy_marches.code_exer mce") == -1) {
				fromClause += ", jefy_marches.code_exer mce";
				whereClause += " AND dhom.ce_ordre=mce.ce_ordre ";
			}
			if (fromClause.indexOf("jefy_marches.code_marche mcm") == -1) {
				fromClause += ", jefy_marches.code_marche mcm";
				whereClause += " AND mce.cm_ordre=mcm.cm_ordre ";
			}
			whereClause += " AND upper(mcm.cm_code) like upper('" + ZStringUtil.sqlString((String) bindings.objectForKey("cmCode")) + "%')";
		}
		if (bindings.objectForKey("cmLib") != null) {
			if (fromClause.indexOf("depense_ctrl_hors_marche dhom") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_hors_marche dhom";
				whereClause += " AND db.dep_id=dhom.dep_id";
			}
			if (fromClause.indexOf("jefy_marches.code_exer mce") == -1) {
				fromClause += ", jefy_marches.code_exer mce";
				whereClause += " AND dhom.ce_ordre=mce.ce_ordre ";
			}
			if (fromClause.indexOf("jefy_marches.code_marche mcm") == -1) {
				fromClause += ", jefy_marches.code_marche mcm";
				whereClause += " AND mce.cm_ordre=mcm.cm_ordre ";
			}
			whereClause += " AND upper(mcm.cm_lib) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("cmLib")) + "%')";
		}

		if (bindings.objectForKey("attribution") != null || bindings.objectForKey("attLibelle") != null || bindings.objectForKey("marLibelle") != null) {
			if (fromClause.indexOf("depense_ctrl_marche dmar") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_marche dmar";
				whereClause += " AND db.dep_id=dmar.dep_id";
			}

			if (bindings.objectForKey("attribution") != null)
				whereClause += " AND dmar.att_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOAttribution) bindings.objectForKey("attribution")).objectForKey(EOAttribution.ATT_ORDRE_KEY);
			else {
				fromClause += ", jefy_marches.attribution att, jefy_marches.lot l, jefy_marches.marche m";
				whereClause += " and dmar.att_ordre=att.att_ordre and att.lot_ordre=l.lot_ordre and m.mar_ordre=l.mar_ordre";

				if (bindings.objectForKey("attLibelle") != null)
					whereClause += " AND upper('('||m.exe_ordre||'/'||m.mar_index||'/'||l.lot_index||')-'||l.lot_libelle) like upper('%" +
							ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("attLibelle")) + "%')";

				if (bindings.objectForKey("marLibelle") != null)
					whereClause += " AND upper(m.exe_ordre||'-'||m.mar_index||'/'||m.mar_libelle) like upper('%" +
							ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("marLibelle")) + "%')";
			}
		}

		if (bindings.objectForKey("pcoNum") != null || bindings.objectForKey("pcoLib") != null) {
			if (fromClause.indexOf("depense_ctrl_planco commplanco") == -1) {
				fromClause += ", jefy_depense.depense_ctrl_planco commplanco";
				whereClause += " AND db.dep_id=commplanco.dep_id";
			}
			if (bindings.objectForKey("pcoNum") != null)
				whereClause += " AND commplanco.pco_num like '" + ZStringUtil.sqlString((String) bindings.objectForKey("pcoNum")) + "%'";

			if (bindings.objectForKey("pcoLib") != null) {
				fromClause += ", maracuja.plan_comptable_exer plancompt";
				whereClause += " AND commplanco.pco_num=plancompt.pco_num and commplanco.exe_ordre=plancompt.exe_ordre";
				whereClause += " AND upper(plancompt.pco_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("pcoLib")) + "%')";
			}
		}
		if (bindings.objectForKey("canCode") != null || bindings.objectForKey("canLib") != null) {
			fromClause += ", jefy_depense.depense_ctrl_analytique commana, jefy_admin.code_analytique codeana";
			whereClause += " AND db.dep_id=commana.dep_id AND commana.can_id=codeana.can_id";

			if (bindings.objectForKey("canCode") != null)
				whereClause += " AND upper(codeana.can_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("canCode")) + "%')";

			if (bindings.objectForKey("canLib") != null)
				whereClause += " AND upper(codeana.can_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("canLib")) + "%')";
		}
		if (bindings.objectForKey("convReference") != null) {
			fromClause += ", jefy_depense.depense_ctrl_convention commconv, jefy_depense.v_convention convention";
			whereClause += " AND db.dep_id=commconv.dep_id AND commconv.conv_ordre=convention.conv_ordre";

			if (bindings.objectForKey("convReference") != null)
				whereClause += " AND upper(convention.conv_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("convReference")) + "%')";
		}
		if (bindings.objectForKey("tyacCode") != null || bindings.objectForKey("tyacLib") != null) {
			fromClause += ", jefy_depense.depense_ctrl_action commact, jefy_admin.lolf_nomenclature_depense typeaction";
			whereClause += " AND db.dep_id=commact.dep_id AND commact.tyac_id=typeaction.lolf_id";

			if (bindings.objectForKey("tyacCode") != null)
				whereClause += " AND upper(typeaction.lolf_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("tyacCode")) + "%')";

			if (bindings.objectForKey("tyacLib") != null)
				whereClause += " AND upper(typeaction.lolf_libelle) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("tyacLib")) + "%')";
		}

		return selectClause + " " + fromClause + " " + whereClause + " " + orderClause;
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	//    private static NSArray sort() {
	//    	NSMutableArray array=new NSMutableArray();
	//    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetExecCredit.ORGAN_KEY+"."+"sourceLibelle",
	//    			EOSortOrdering.CompareCaseInsensitiveAscending));
	//    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetExecCredit.TYPE_CREDIT_KEY+"."+
	//    			EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
	//    	return array;
	//    }

	public static final NSArray<NSDictionary<String, Object>> getRawRowPreLiquidation(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChainePreLiquidation(ed, false, bindings), null);
	}

	public static final int getRawRowCountPreLiquidation(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nb = -1;
		NSArray res = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChainePreLiquidation(ed, true, bindings), null);
		if (res != null && res.count() == 1) {
			NSDictionary row = (NSDictionary) res.lastObject();
			nb = ((Number) row.valueForKey("NBREPRELIQUIDATIONS")).intValue();
		}

		return nb;
	}

	protected static String constructionChainePreLiquidation(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		// parties communes des differents select
		String selectClause = "";
		String fromClause = "";
		String whereClause = "";
		String groupByClause = "";
		String orderClause = "";

		//FIXME optimiser la requete, trop lent...

		selectClause = "SELECT DISTINCT c.comm_numero numero, c.comm_date datecreation,c.comm_reference REFERENCE, c.comm_libelle libelle,t.tyet_libelle typeetat,i.nom_usuel || ' ' || i.prenom nomprenom,f.fou_code || ' ' || f.fou_nom libellefournisseur, SUM (a.art_prix_total_ht) totalht,SUM (a.art_prix_total_ttc) totalttc, i1.nom_usuel || ' ' || i1.prenom nomprenom2,dpp.dpp_id,dpp.DPP_DATE_FACTURE,dpp.DPP_NUMERO_FACTURE,dpp.DPP_HT_INITIAL,dpp.DPP_TTC_INITIAL, dpp.DPP_SF_DATE, trim(nvl(pers.pers_libelle,'') || ' ' || nvl(pers.pers_lc,'')) as NOMPRENOMSF";
		fromClause = "FROM  jefy_depense.pdepense_budget pdb,jefy_depense.depense_papier dpp,jefy_depense.engage_budget eb,jefy_depense.commande_engagement ce,jefy_depense.commande c,jefy_depense.v_type_etat t,jefy_admin.utilisateur u, grhum.individu_ulr i,jefy_admin.utilisateur u1, grhum.individu_ulr i1,jefy_depense.v_fournisseur f, jefy_depense.article a , grhum.personne pers, jefy_admin.utilisateur_organ uo   ";
		whereClause = " WHERE pdb.dpp_id=dpp.dpp_id   and pdb.eng_id= eb.eng_id  and eb.eng_id = ce.eng_id  " +
				"and ce.comm_id = c.comm_id  and  c.tyet_id = t.tyet_id    AND c.exe_ordre =" + exeOrdre;
		whereClause += " AND c.utl_ordre = u.utl_ordre AND u.no_individu = i.no_individu and pdb.utl_ordre = u1.utl_ordre " +
				"and u1.no_individu = i1.no_individu AND c.fou_ordre = f.fou_ordre AND c.comm_id = a.comm_id " +
				"and dpp.DPP_SF_PERS_ID = pers.pers_id(+) and uo.ORG_ID=eb.org_id and uo.utl_ordre="
				+ utlOrdre;
		groupByClause = "GROUP BY c.comm_numero,c.comm_date,c.comm_reference,c.comm_libelle,t.tyet_libelle,i.prenom,i.nom_usuel," +
				"f.fou_code,f.fou_nom,i1.nom_usuel,i1.prenom,dpp.dpp_id,dpp.DPP_DATE_FACTURE,dpp.DPP_NUMERO_FACTURE," +
				"dpp.DPP_HT_INITIAL,dpp.DPP_TTC_INITIAL,dpp.DPP_SF_DATE, pers.pers_libelle, pers.pers_lc";
		orderClause = "ORDER BY c.COMM_DATE DESC";

		String res = "";
		if (isForCount) {
			res = "select count(*) nbrePreLiquidations from (" + selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + ")";
		}
		else {
			res = selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + " " + orderClause;
		}

		return res;
	}

	public static final NSArray<NSDictionary<String, Object>> getRawRowLiquidationsPourDossier(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaineLiquidationsPourDossier(ed, false, false, false, bindings), null);
	}

	public static final NSArray<String> getRawRowLiquidationsLiquideursPourDossier(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return (NSArray<String>) EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaineLiquidationsPourDossier(ed, false, true, false, bindings), null).valueForKey("UTLNOMPRENOM");
	}
	//DT:00071366
	public static final NSArray<NSMutableDictionary> getRawRowLiquidationsFourniseursPourDossier(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return (NSArray<NSMutableDictionary>) EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaineLiquidationsPourDossier(ed, false, false, true, bindings), null);
	}


	public static final int getRawRowCountLiquidationsPourDossier(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nb = -1;
		NSArray res = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaineLiquidationsPourDossier(ed, true, false, false, bindings), null);
		if (res != null && res.count() == 1) {
			NSDictionary row = (NSDictionary) res.lastObject();
			nb = ((Number) row.valueForKey("NBRE")).intValue();
		}

		return nb;
	}

	protected static String constructionChaineLiquidationsPourDossier(EOEditingContext ed, boolean isForCount, boolean isForLiquideur, boolean isForFournisseur, NSDictionary<String, Object> bindings) {
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");

		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		//Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		// parties communes des differents select
		String selectClause = "";
		String fromClause = "";
		String whereClause = "";
		String groupByClause = "";
		String orderClause = "";

		selectClause = "select *";
		fromClause = "FROM  jefy_depense.v_depense_a_transferer ";
		whereClause = " where exe_exercice=" + exeOrdre;
		//19/12/2012 : on ne propose que les liquidations générées par l'application Carambole
		whereClause += " AND tyap_libelle='DEPENSE' ";

		if (!isForCount && !isForLiquideur) {
			if (bindings.objectForKey("organUb") != null) {
				whereClause += " AND upper(org_ub) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organUb")) + "%')";
			}
			if (bindings.objectForKey("organCr") != null) {
				whereClause += " AND upper(org_cr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organCr")) + "%')";
			}

			if (bindings.objectForKey("organSsCr") != null) {
				whereClause += " AND upper(org_souscr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organSsCr")) + "%')";
			}

			if (bindings.objectForKey("UTLNOMPRENOM") != null) {
				whereClause += " AND UTLNOMPRENOM = '" + ZStringUtil.sqlString((String) bindings.objectForKey("UTLNOMPRENOM")) + "'";
			}
			
			//DT:0007136
			if (bindings.objectForKey("ADR_NUMERO") != null) {
				whereClause += " AND ADR_NUMERO = " + bindings.objectForKey("ADR_NUMERO");
			}
			
			if (bindings.objectForKey("pcoNum") != null) {
				whereClause += " AND pco_num = '" + ZStringUtil.sqlString((String) bindings.objectForKey("pcoNum")) + "'";
			}

			orderClause = "ORDER BY DPP_DATE_FACTURE";
		}

		String res = "";
		if (isForCount) {
			res = "select count(*) NBRE from (" + selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + ")";
		}
		else if (isForLiquideur) {
			res = "select distinct UTLNOMPRENOM from (" + selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + ") order by UTLNOMPRENOM";
		}
		//DT:0007136
		else if (isForFournisseur) {
			res = "select distinct TRIM(ADR_NOM||' '||NVL(ADR_PRENOM,'')) as FOURNISSEUR, ADR_NUMERO from (" + selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + ") order by FOURNISSEUR";
		}
		else {
			res = selectClause + " " + fromClause + " " + whereClause + " and rownum <= 300 " + groupByClause + " " + orderClause;
		}

		return res;
	}
}