/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.metier.EOImTaux;
import org.cocktail.fwkcktldepense.server.metier.EOImTypeTaux;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public final class FinderImTaux extends Finder {

	public static final EOImTaux getTaux(EOEditingContext ed, EOImTypeTaux typeTaux, NSTimestamp date) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.TYPE_TAUX_KEY + "=%@", new NSArray(typeTaux)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_DEBUT_KEY + "<=%@", new NSArray(date)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(date)));

		NSArray resultats = Finder.fetchArray(EOImTaux.ENTITY_NAME, new EOAndQualifier(qualifiers), null, ed, true);
		if (resultats == null || resultats.count() == 0)
			return null;
		return (EOImTaux) resultats.objectAtIndex(0);
	}
}
