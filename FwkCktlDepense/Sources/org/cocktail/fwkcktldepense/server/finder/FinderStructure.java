/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.ConvertHelper;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOStructure;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderStructure extends Finder {

	/**
	 * @param ed
	 * @param bindings
	 * @return
	 * @deprecated
	 */
	public static final NSArray getStructures(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSMutableDictionary mesBindings = new NSMutableDictionary();

		mesBindings.addEntriesFromDictionary(bindings);
		mesBindings.setObjectForKey("A", "cTypeStructure");
		NSArray larray = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOStructure.ENTITY_NAME, "Recherche", mesBindings);

		return Finder.tableauTrie(larray, sort());
	}

	public static final NSArray<EOStructure> getComposantesEtServices(EOEditingContext ed, String nom) {
		NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> res = org.cocktail.fwkcktlpersonne.common.metier.EOStructure.rechercherServicesEtablissements(ed, nom);
		return ConvertHelper.convertPersonneStructuresToDepenseStructures(ed, res);
	}

	public static final NSArray<EOStructure> getStructuresPourCommande(EOEditingContext ed, EOCommande commande) {
		NSMutableArray larray = new NSMutableArray();

		if (commande == null || commande.commandeBudgets().count() == 0)
			return larray;

		NSArray tableau = commande.commandeBudgets();
		for (int i = 0; i < tableau.count(); i++) {
			EOStructure structure = ((EOCommandeBudget) tableau.objectAtIndex(i)).organ().structureUlr();
			if (structure != null && !larray.containsObject(structure))
				larray.addObject(structure);
			else {
				if (((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere() != null) {
					if (((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().structureUlr() != null &&
							!larray.containsObject(((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().structureUlr()))
						larray.addObject(((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().structureUlr());
					else if (((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().organPere() != null &&
							((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().organPere().structureUlr() != null &&
							!larray.containsObject(((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().organPere().structureUlr()))
						larray.addObject(((EOCommandeBudget) tableau.objectAtIndex(i)).organ().organPere().organPere().structureUlr());
				}
			}
		}

		return Finder.tableauTrie(larray, sort());
	}

	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}
}
