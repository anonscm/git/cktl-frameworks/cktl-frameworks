/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFonction;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOOrganAction;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderTypeAction extends Finder {

	/**
	 * Recherche les types actions pour un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel faire la recherche
	 * @return un NSArray de EOTypeAction
	 */
	public static final NSArray<EOTypeAction> getTypeActions(EOEditingContext ed, EOExercice exercice) {
		return getLesTypeActions(ed, exercice);
	}

	public static final NSArray<EOTypeAction> getTypeActions(EOEditingContext ed, _ISourceCredit source, EOUtilisateur utilisateur) {
		return getTypeActions(ed, source.exercice(), source.organ(), source.typeCredit(), utilisateur);
	}

	public static final NSArray<EOTypeAction> getTypeActions(EOEditingContext ed, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit,
			EOUtilisateur utilisateur) {
		if (exercice == null || organ == null || typeCredit == null)
			return NSArray.emptyArray();

		String parametre = FinderParametre.getParametreCtrlAction(ed, exercice);
		if (parametre == null)
			return NSArray.emptyArray();

		NSArray<EOTypeAction> lesActions = getTypeActions(ed, exercice);

		if (parametre.equals("NON"))
			return lesActions;

		EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(ed, EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
		if (typeApplication == null)
			throw new FactoryException("Type application " + EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK +
					" (FinderTypeAction.getTypeActions(), edc=" + ed + ") " + typeApplication);

		EOFonction fonction = FinderFonction.getFonction(ed, EOFonction.FONCTION_AUTRE_ACTION, typeApplication);
		if (fonction == null) {
			throw new FactoryException("fonction " + EOFonction.FONCTION_AUTRE_ACTION +
					" (FinderTypeAction.getTypeActions(" + ed + "," + exercice + "," + organ + "," + typeCredit + "," + utilisateur + ")");
		}
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(utilisateur, "utilisateur");
		dico.setObjectForKey(fonction, "fonction");
		dico.setObjectForKey(exercice, "exercice");
		if (FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, dico).count() > 0)
			return lesActions;

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(organ, "organ");
		bindings.setObjectForKey(exercice, "exercice");
		bindings.setObjectForKey(typeCredit, "typeCredit");

		NSArray lesActionsVotees = (NSArray) EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOOrganAction.ENTITY_NAME, "Recherche", bindings).valueForKeyPath(EOOrganAction.TYPE_ACTION_KEY);

		if (lesActionsVotees.count() == 0)
			return lesActions;
		return Finder.tableauTrie(lesActionsVotees, sort());
	}

	public static final NSArray<EOTypeAction> getTypeActionVote(EOEditingContext ed, EOSource source, EOTypeAction typeAction) {
		return getTypeActionVote(ed, source.exercice(), source.organ(), source.typeCredit(), typeAction);
	}

	public static final NSArray<EOTypeAction> getTypeActionVote(EOEditingContext ed, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit,
			EOTypeAction typeAction) {
		if (exercice == null || organ == null || typeCredit == null || typeAction == null)
			return NSArray.emptyArray();

		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(organ, "organ");
		bindings.setObjectForKey(exercice, "exercice");
		bindings.setObjectForKey(typeCredit, "typeCredit");
		bindings.setObjectForKey(typeAction, "typeAction");

		return Finder.tableauTrie((NSArray) EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOOrganAction.ENTITY_NAME, "Recherche", bindings).valueForKeyPath(EOOrganAction.TYPE_ACTION_KEY), sort());
	}

	/**
	 * Recherche les types actions pour un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice pour lequel faire la recherche
	 * @return un NSArray de EOTypeAction
	 */
	private static NSArray<EOTypeAction> getLesTypeActions(EOEditingContext ed, EOExercice exercice) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(EOTypeAction.ETAT_VALIDE, "tyetLibelle");
		bindings.setObjectForKey(exercice, "exercice");

		return Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOTypeAction.ENTITY_NAME, "Recherche", bindings), sort());
	}

	public static EOTypeAction getLesTypeActions(EOEditingContext ed, EOExercice exercice, String tyacCode) {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(EOTypeAction.ETAT_VALIDE, "tyetLibelle");
		bindings.setObjectForKey(tyacCode, "tyacCode");
		bindings.setObjectForKey(exercice, "exercice");

		NSArray array = Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOTypeAction.ENTITY_NAME, "Recherche", bindings), sort());
		if (array == null || array.count() == 0)
			return null;
		return (EOTypeAction) array.objectAtIndex(0);
	}

	/**
	 * Fetch tous les types action pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	/*
	 * private static NSArray fetchTypeActions(EOEditingContext ed) { // if (arrayTypeActions==null) return
	 * Finder.fetchArray(ed,EOTypeAction.ENTITY_NAME,null,null, sort(), false); }
	 */

	private static NSArray<EOSortOrdering> sort() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOTypeAction.TYAC_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
	}
}
