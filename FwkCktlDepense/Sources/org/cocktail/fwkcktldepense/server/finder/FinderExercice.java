/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFonction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurFonctionExercice;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public final class FinderExercice extends Finder {

	/**
	 * Selecteur d'exercice.
	 */
	private interface ISelectionExercice {
		/**
		 * Contrôle la validaité de l'exercice selon un critere propre a chaque implementation.
		 * 
		 * @param exercice exercice a controler.
		 * @return true si exercice repond au critere de validité ; false sinon.
		 */
		boolean check(EOExercice exercice);
	}

	private static class SelectionExerciceEngageable implements ISelectionExercice {
		public boolean check(EOExercice exercice) {
			return exercice.estEngageable();
		}
	};

	private static class SelectionExerciceEngageableRestreint implements ISelectionExercice {
		public boolean check(EOExercice exercice) {
			return exercice.estEngageableRestreint();
		}
	};

	private static class SelectionExerciceLiquidable implements ISelectionExercice {
		public boolean check(EOExercice exercice) {
			return exercice.estLiquidable();
		}
	};

	private static class SelectionExerciceLiquidableRestreint implements ISelectionExercice {
		public boolean check(EOExercice exercice) {
			return exercice.estLiquidableRestreint();
		}
	};

	/**
	 * Recherche d'un exercice correspondant a une date. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param date date de reference pour laquelle on veut l'exercice
	 * @return un EOExercice
	 */
	public static final EOExercice getExercicePourDate(EOEditingContext ed, NSTimestamp date) {
		return FinderExercice.getExercicePourUneDate(ed, date);
	}

	public static final NSArray<EOExercice> getExercices(EOEditingContext ed) {
		return FinderExercice.getLesExercices(ed);
	}

	private static final NSArray<EOExercice> getLesExercices(EOEditingContext ed) {
		return fetchExercices(ed);
	}

	/**
	 * Recherche d'un exercice correspondant a une date. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param date date de reference pour laquelle on veut l'exercice
	 * @return un EOExercice
	 */
	private static final EOExercice getExercicePourUneDate(EOEditingContext ed, NSTimestamp date) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		NSArray larray;

		NSArray<EOExercice> arrayExercices = fetchExercices(ed);

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_CLOTURE_KEY + "=nil or " +
				EOExercice.EXE_CLOTURE_KEY + ">=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_OUVERTURE_KEY + "<=%@", new NSArray(date)));
		larray = EOQualifier.filteredArrayWithQualifier(arrayExercices, new EOAndQualifier(arrayQualifier));

		if (larray.count() != 1) {
			for (int i = 0; i < larray.count(); i++)
				if (((EOExercice) larray.objectAtIndex(i)).estEngageableOuvert())
					return (EOExercice) larray.objectAtIndex(i);

			for (int i = 0; i < larray.count(); i++)
				if (((EOExercice) larray.objectAtIndex(i)).estEngageableRestreint())
					return (EOExercice) larray.objectAtIndex(i);

			return (EOExercice) larray.objectAtIndex(0);
		}

		return (EOExercice) larray.objectAtIndex(0);
	}

	/**
	 * Recherche des exercices ouverts a un utilisateur.<BR>
	 * 
	 * @param ed
	 * @param utilisateur
	 * @return
	 */
	public static NSArray<EOExercice> getExercicesOuvertsPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
		return getExercicesOuvertsPourUtilisateur(ed, utilisateur, false);
	}

	/**
	 * Recherche des exercices ouverts a un utilisateur.<BR>
	 * 
	 * @param ed
	 * @param utilisateur
	 * @return
	 */
	public static NSArray<EOExercice> getExercicesOuvertsPourUtilisateur(
			EOEditingContext ed, EOUtilisateur utilisateur, boolean sansRestrictionSurExerciceCourant) {
		NSMutableArray<EOExercice> listeExercices = new NSMutableArray<EOExercice>();

		if (!hasUtilisateurDroitAJour(utilisateur)) {
			return NSArray.emptyArray();
		}

		// on prend tout les exercices engageables
		listeExercices.addObjectsFromArray(getExercicesEngageablesPourUtilisateur(ed, utilisateur));

		// on recupere l'exercice en cours
		EOExercice exerciceCourant = getExercicePourUneDate(ed, getNSTimeStamp());

		// on regarde si cet exercice est pas present dans le array sinon on le rajoute
		if (exerciceCourant.estEngageableOuvert()) {
			if (!listeExercices.containsObject(exerciceCourant)) {
				listeExercices.addObject(exerciceCourant);
			}
		}

		if (sansRestrictionSurExerciceCourant && exerciceCourant.estOuvert()) {
			if (!listeExercices.containsObject(exerciceCourant)) {
				listeExercices.addObject(exerciceCourant);
			}
		}

		NSArray<EOExercice> exercicesLiquidables = getExercicesLiquidablesPourUtilisateur(ed, utilisateur);
		for (int i = 0; i < exercicesLiquidables.count(); i++) {
			EOExercice exer = exercicesLiquidables.objectAtIndex(i);
			if (!listeExercices.containsObject(exer)) {
				listeExercices.addObject(exer);
			}

		}

		return listeExercices;
	}

	/**
	 * Recherche des exercices pour lesquels l'utilisateur a le droit d'engager.<BR>
	 * 
	 * @param ed
	 * @param utilisateur
	 * @return
	 */
	public static final NSArray<EOExercice> getExercicesEngageablesPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
		NSMutableArray<EOExercice> lesExercices = new NSMutableArray<EOExercice>();

		if (!hasUtilisateurDroitAJour(utilisateur)) {
			return NSArray.emptyArray();
		}

		lesExercices.addAll(exercicesAutorises(ed, utilisateur, EOFonction.FONCTION_ENGAGE,
				new SelectionExerciceEngageable()));

		lesExercices.addAll(exercicesAutorises(ed, utilisateur, EOFonction.FONCTION_ENGAGE_PERIODE_INVENTAIRE,
				new SelectionExerciceEngageableRestreint()));
		return lesExercices;
	}

	public static final boolean isExercicePrestationPourUtilisateur(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur) {

		if (exercice == null || utilisateur == null)
			return false;

		NSMutableDictionary mesBindings = new NSMutableDictionary();

		mesBindings.setObjectForKey(utilisateur, "utilisateur");
		mesBindings.setObjectForKey(exercice, "exercice");
		mesBindings.setObjectForKey(EOFonction.FONCTION_CREATION_PRESTATION, "fonIdInterne");

		return (FinderUtilisateurFonction.getUtilisateurFontion(ed, mesBindings) != null);
	}

	/**
	 * Recherche des exercices pour lesquels l'utilisateur a le droit de liquider.<BR>
	 * 
	 * @param ed
	 * @param utilisateur
	 * @return
	 */
	public static final NSArray<EOExercice> getExercicesLiquidablesPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
		NSMutableArray<EOExercice> lesExercices = new NSMutableArray<EOExercice>();

		if (!hasUtilisateurDroitAJour(utilisateur)) {
			return NSArray.emptyArray();
		}

		lesExercices.addAll(exercicesAutorises(ed, utilisateur, EOFonction.FONCTION_LIQUIDE,
				new SelectionExerciceLiquidable()));
		lesExercices.addAll(exercicesAutorises(ed, utilisateur, EOFonction.FONCTION_LIQUIDE_PERIODE_INVENTAIRE,
				new SelectionExerciceLiquidableRestreint()));
		return lesExercices;
	}

	public static final NSArray<EOExercice> getExercicesReimputablesPourUtilisateur(EOEditingContext ed, EOUtilisateur utilisateur) {
		NSMutableArray<EOExercice> lesExercices = new NSMutableArray<EOExercice>();

		if (!hasUtilisateurDroitAJour(utilisateur)) {
			return NSArray.emptyArray();
		}

		// recuperer les exercices correspondants au droit de reimputer hors periode d'inventaire
		lesExercices.addAll(exercicesAutorises(ed, utilisateur, EOFonction.FONCTION_REIMPUTER,
				new SelectionExerciceLiquidable()));
		lesExercices.addAll(exercicesAutorises(ed, utilisateur, EOFonction.FONCTION_REIMPUTER_PERIODE_INVENTAIRE,
				new SelectionExerciceLiquidableRestreint()));

		return lesExercices;
	}

	/**
	 * Fetch l'exercice correspondant a l'exeOrdre. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exeOrdre cle de l'exercice
	 */
	public static EOExercice getExercice(EOEditingContext edc, Integer exeOrdre) {
		EOExercice exercice = null;
		@SuppressWarnings("unchecked")
		NSArray<EOExercice> exercices = EOUtilities.objectsMatchingKeyAndValue(edc, EOExercice.ENTITY_NAME, "exeOrdre", exeOrdre);
		if (exercices != null && exercices.count() == 1) {
			exercice = (EOExercice) exercices.lastObject();
		}

		return exercice;
	}

	public static boolean hasUtilisateurDroitAJour(EOUtilisateur utilisateur) {
		return EOUtilisateur.hasUtilisateurDroitAJour(utilisateur);
	}

	protected static NSTimestamp getNSTimeStamp() {
		return ZDateUtil.currentDateNSTimeStamp();
	}

	/**
	 * Fetch tous les exercices pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	@SuppressWarnings("unchecked")
	private static NSArray<EOExercice> fetchExercices(EOEditingContext ed) {
		return Finder.fetchArray(ed, EOExercice.ENTITY_NAME, null, null,
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending)), false);
	}

	private static List<EOExercice> exercicesAutorises(EOEditingContext ed,
			EOUtilisateur utilisateur, String laFonction, ISelectionExercice selecteur) {
		List<EOExercice> exercicesRetenus = new ArrayList<EOExercice>();
		NSArray<EOUtilisateurFonctionExercice> fonctionsExercice;
		NSMutableDictionary<String, Object> mesBindings;
		EOUtilisateurFonction utilisateurFonction;

		// recuperer les exercices correspondants au droit d'engager pendant la periode d'inventaire
		utilisateurFonction = FinderUtilisateurFonction.getUtilisateurFonction(ed, utilisateur, laFonction);
		if (utilisateurFonction != null) {
			mesBindings = new NSMutableDictionary<String, Object>();
			mesBindings.setObjectForKey(utilisateurFonction, "utilisateurFonction");
			fonctionsExercice = FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, mesBindings);

			// trier les exercices pour garder ceux qui sont engageables
			for (int i = 0; i < fonctionsExercice.count(); i++) {
				EOExercice exercice = ((EOUtilisateurFonctionExercice) fonctionsExercice.objectAtIndex(i)).exercice();
				if (exercice != null && selecteur.check(exercice)) {
					exercicesRetenus.add(exercice);
				}
			}
		}

		return exercicesRetenus;
	}

}
