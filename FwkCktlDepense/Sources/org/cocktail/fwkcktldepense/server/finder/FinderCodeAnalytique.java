/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderCodeAnalytique extends Finder {

	/**
	 * Recherche des codes analytiques autorises pour une ligne budgetaire et un exercice.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param organ ligne budgetaire pour laquelle on fait la recherche
	 * @param exercice exercice pour lequel on fait la recherche
	 * @return un NSArray contenant des EOCodeAnalytique
	 */
	public static final NSArray<EOCodeAnalytique> getCodeAnalytiques(EOEditingContext ed, EOOrgan organ, EOExercice exercice) {
		NSMutableDictionary mesBindings = new NSMutableDictionary();

		if (/* organ == null || */exercice == null)/*
													 * || organ.orgNiveau()==null || (organ.orgNiveau().intValue()!=3 &&
													 * organ.orgNiveau().intValue()!=4))
													 */
			return NSArray.emptyArray();

		mesBindings.setObjectForKey(FinderTypeEtat.getTypeEtat(ed, EOCodeAnalytique.TYET_LIBELLE_VALIDE), "typeEtat");
		mesBindings.setObjectForKey(exercice, "exercice");
		mesBindings.setObjectForKey(FinderTypeEtat.getTypeEtat(ed, EOCodeAnalytique.TYET_LIBELLE_UTILISABLE), "typeEtatUtilisable");
		mesBindings.setObjectForKey(FinderTypeEtat.getTypeEtat(ed, EOCodeAnalytique.TYET_LIBELLE_PUBLIC), "typeEtatPublic");

		EOOrgan organUb = null;
		EOOrgan organCr = null;

		if (organ != null) {
			if (organ.orgNiveau().intValue() == 2) {
				organUb = organ;
				organCr = null;
			}
			if (organ.orgNiveau().intValue() == 3) {
				organUb = organ.organPere();
				organCr = organ;
			}
			if (organ.orgNiveau().intValue() == 4) {
				organUb = organ.organPere().organPere();
				organCr = organ.organPere();
			}

			if (organUb == null)
				throw new FactoryException("La ligne budgetaire n'est pas du bon niveau (" + organ + ")");
			//			if (organCr == null)
			//				throw new FactoryException("La ligne budgetaire n'est pas du bon niveau (" + organ + ")");

			mesBindings.setObjectForKey(organUb, "organ");
			if (organCr != null) {
				mesBindings.setObjectForKey(organCr, "cr");
			}
			if (organ != null && organ.orgNiveau().intValue() == 4)
				mesBindings.setObjectForKey(organ, "sscr");
		}

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOCodeAnalytique.ENTITY_NAME, "Recherche", mesBindings), sort());
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		//    	return new NSArray(EOSortOrdering.sortOrderingWithKey(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return new NSArray(new Object[] {
				EOCodeAnalytique.SORT_TYPE_ETAT_PUBLIC_DESC, EOCodeAnalytique.SORT_CAN_CODE_ASC
		});
	}
}
