/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.List;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFonction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurFonctionExercice;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXQ;

public final class FinderUtilisateurFonction extends Finder {

	/**
	 * Recherche d'un utilisateurFonction pour un utilisateur et un libelle de fonction <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param utilisateur utilisateur pour lequel trouver la fonction associee
	 * @param libelleFonction String contenant le libelle de la fonction a rechercher
	 * @return un EOUtilisateurFonction
	 */
	public static final EOUtilisateurFonction getUtilisateurFonction(EOEditingContext ed, EOUtilisateur utilisateur, String libelleFonction) {
		NSMutableDictionary mesBindings = new NSMutableDictionary();

		EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(ed, EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
		if (typeApplication == null)
			throw new FactoryException("Type application " + EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK +
					" non trouve ! (utilisateur:" + utilisateur + ", ed:" + ed + "fonction:" + libelleFonction + ", edc=" + ed + ")");

		EOFonction fonction = FinderFonction.getFonction(ed, libelleFonction, typeApplication);
		if (fonction == null)
			return null;

		mesBindings.setObjectForKey(utilisateur, "utilisateur");
		mesBindings.setObjectForKey(fonction, "fonction");

		return FinderUtilisateurFonction.getUtilisateurFontion(ed, mesBindings);
	}

	/**
	 * Recherche d'un utilisateurFonction par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, fonction <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un EOUtilisateurFonction
	 */
	public static final EOUtilisateurFonction getUtilisateurFontion(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		NSArray larray = FinderUtilisateurFonction.getUtilisateurFonctions(ed, bindings);

		if (larray.count() == 1)
			return (EOUtilisateurFonction) larray.objectAtIndex(0);

		return null;
	}

	/**
	 * Recherche la fonction representant un droit pour un utilisateur sur une fonction et un exercice.
	 * @param editingContext editing context.
	 * @param utilisateur utilisateur.
	 * @param exercice exercice.
	 * @param libelleFonction fonction.
	 * @return la fonction representant un droit pour un utilisateur sur une fonction et un exercice.
	 */
	public static final EOUtilisateurFonction getUtilisateurFonction(
			EOEditingContext editingContext, EOUtilisateur utilisateur, EOExercice exercice, String libelleFonction) {
		EOUtilisateurFonction droitUtilisateur = null;
		EOQualifier exerciceRestriction = ERXQ.is(EOUtilisateurFonction.UTILISATEUR_FONCTION_EXERCICES.append(
				EOUtilisateurFonctionExercice.EXERCICE).key(), exercice);
		EOQualifier fonctionRestriction = ERXQ.is(EOUtilisateurFonction.FONCTION.append(
				EOFonction.FON_ID_INTERNE).key(), libelleFonction);
		EOQualifier utilisateurRestriction = ERXQ.is(EOUtilisateurFonction.UTILISATEUR_KEY, utilisateur);
		EOQualifier restrictions = ERXQ.and(exerciceRestriction, fonctionRestriction, utilisateurRestriction);

		List<EOUtilisateurFonction> droitsUtilisateur = EOUtilisateurFonction.fetchAll(editingContext, restrictions);
		if (droitsUtilisateur != null && droitsUtilisateur.size() > 0) {
			droitUtilisateur = droitsUtilisateur.get(0);
		}
		return droitUtilisateur;
	}

	/**
	 * Recherche des utilisateurFonction d'un utilisateur par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, fonction <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray de EOUtilisateurFonction
	 */
	public static final NSArray getUtilisateurFonctions(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOUtilisateurFonction.ENTITY_NAME, "Recherche", bindings);
	}
}
