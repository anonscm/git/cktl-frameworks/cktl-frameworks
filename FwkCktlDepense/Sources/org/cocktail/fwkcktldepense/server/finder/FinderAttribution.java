/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogue;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOLot;
import org.cocktail.fwkcktldepense.server.metier.EOMarche;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSSet;
import com.webobjects.foundation.NSTimestamp;

public final class FinderAttribution extends Finder {

	public static final String KEY_DATE = "date";
	public static final String KEY_FOURNISSEUR = "fournisseur";

	/**
	 * Recherche des attributions valides des marches par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : lot, marche, date, codeExer, fournisseur <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings map contenant les bindings
	 * @return une Liste contenant des EOAttribution
	 */
	public static final List<EOAttribution> getAttributionsValides(EOEditingContext ed, Map<String, Object> bindings) {
		if (bindings == null) {
			throw new FactoryException("le bindings 'date' est obligatoire");
		}
		if (!bindings.containsKey(KEY_DATE) || bindings.get(KEY_DATE) == null) {
			throw new FactoryException("le bindings 'date' est obligatoire");
		}

		NSMutableDictionary<String, Object> mesBindings = new NSMutableDictionary<String, Object>(bindings);

		mesBindings.setObjectForKey(EOAttribution.ETAT_VALIDE, "attValide");
		mesBindings.setObjectForKey(EOAttribution.ETAT_NON_SUPPRIME, "attSuppr");

		mesBindings.setObjectForKey(EOLot.ETAT_VALIDE, "lotValide");
		mesBindings.setObjectForKey(EOLot.ETAT_NON_SUPPRIME, "lotSuppr");

		mesBindings.setObjectForKey(EOMarche.ETAT_VALIDE, "marValide");
		mesBindings.setObjectForKey(EOMarche.ETAT_NON_SUPPRIME, "marSuppr");

		return FinderAttribution.getAttributions(ed, mesBindings);
	}

	/**
	 * Recherche des attributions valides des marches par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : lot, marche, date, codeExer, fournisseur <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOAttribution
	 */
	public static final NSArray<EOAttribution> getAttributionsValides(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		return new NSMutableArray<EOAttribution>(getAttributionsValides(ed, (Map<String, Object>) bindings));
	}

	/**
	 * Recherche des attributions des marches par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : lot, marche, date, attValide, attSuppr, lotValide, lotSuppr, fournisseur <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOAttribution
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<EOAttribution> getAttributions(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'date' est obligatoire");
		if (bindings.objectForKey("date") == null)
			throw new FactoryException("le bindings 'date' est obligatoire");

		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOAttribution.ENTITY_NAME, "Recherche", bindings);
	}

	/**
	 * Recherche les attributions d'un lot.<BR>
	 * <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param lot EOLot pour lequel effectuer la recherche
	 * @return un NSArray contenant des EOAttribution
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<EOAttribution> getAttributionsPourLot(EOEditingContext ed, EOLot lot) {
		return Finder.fetchArray(ed, EOAttribution.ENTITY_NAME, "lot=%@", new NSArray(lot), null, false);
	}

	/**
	 * Recherche les attributions valides d'un lot.<BR>
	 * <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param lot EOLot pour lequel effectuer la recherche
	 * @param date date a laquelle les attributions doivent etre valides
	 * @return un NSArray contenant des EOAttribution
	 */
	public static final NSArray<EOAttribution> getAttributionsValidesPourLot(EOEditingContext ed, EOLot lot, NSTimestamp date) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY + "=%@", new NSArray(lot)));

		// verification de la validite de l'attribution
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_DEBUT_KEY + "<=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_FIN_KEY + ">=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_VALIDE_KEY + "=%@", new NSArray(EOAttribution.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_SUPPR_KEY + "=%@", new NSArray(EOAttribution.ETAT_NON_SUPPRIME)));

		return Finder.fetchArray(EOAttribution.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);
	}

	/**
	 * Recherche les attributions d'un marche.<BR>
	 * <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param marche EOMarche pour lequel effectuer la recherche
	 * @return un NSArray contenant des EOAttribution
	 */
	public static final NSArray<EOAttribution> getAttributionsPourMarche(EOEditingContext ed, EOMarche marche) {
		return Finder.fetchArray(ed, EOAttribution.ENTITY_NAME, "lot.marche=%@", new NSArray(marche), null, false);
	}

	/**
	 * Recherche les attributions valides d'un marche.<BR>
	 * <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param marche EOMarche pour lequel effectuer la recherche
	 * @param date date a laquelle les attributions et les lots concernes doivent etre valides
	 * @return un NSArray contenant des EOAttribution
	 */
	public static final NSArray<EOAttribution> getAttributionsValidesPourMarche(EOEditingContext ed, EOMarche marche, NSTimestamp date) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("lot.marche=%@", new NSArray(marche)));

		// verification de la validite du lot
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("lot.lotDebut<=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("lot.lotFin>=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("lot.lotValide=%@", new NSArray(EOLot.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("lot.lotSuppr=%@", new NSArray(EOLot.ETAT_NON_SUPPRIME)));

		// verification de la validite de l'attribution
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("attDebut<=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("attFin>=%@", new NSArray(date)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("attValide=%@", new NSArray(EOAttribution.ETAT_VALIDE)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("attSuppr=%@", new NSArray(EOAttribution.ETAT_NON_SUPPRIME)));

		return Finder.fetchArray(EOAttribution.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, false);
	}

	/**
	 * Recherche les attributions d'une liste d'articles.<BR>
	 * <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param articles NSArray liste des articles
	 * @return un NSArray contenant des EOAttribution
	 */
	public static final NSArray<EOAttribution> getAttributions(EOEditingContext ed, NSArray articles) {
		NSMutableArray attributions = new NSMutableArray();
		Enumeration enumArticles = articles.objectEnumerator();
		while (enumArticles.hasMoreElements()) {
			EOCatalogue unArticle = (EOCatalogue) enumArticles.nextElement();
			EOAttribution uneAttribution = unArticle.attribution();
			if (uneAttribution != null) {
				if (attributions.containsObject(uneAttribution) == false) {
					attributions.addObject(uneAttribution);
				}
			}
			else {
				NSArray attributionsPourCodeExer = unArticle.autresAttributionsExistantesPourCodeExer(ed);
				Enumeration enumAttributionsPourCodeExer = attributionsPourCodeExer.objectEnumerator();
				while (enumAttributionsPourCodeExer.hasMoreElements()) {
					EOAttribution uneAutreAttribution = (EOAttribution) enumAttributionsPourCodeExer.nextElement();
					if (attributions.containsObject(uneAutreAttribution) == false) {
						attributions.addObject(uneAutreAttribution);
					}
				}
			}
		}

		return attributions;
	}

	/**
	 * Recherche les attributions communes d'une liste d'articles.<BR>
	 * <BR>
	 *
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param articles NSArray liste des articles
	 * @return un NSArray contenant des EOAttribution
	 */
	// TODO modif Manu A optimiser
	public static final NSArray<EOAttribution> getAttributionsCommunes(EOEditingContext ed, NSArray articles) {
		NSMutableArray attributionsCommunes = new NSMutableArray();
		if (articles != null && articles.count() > 0) {
			// Recherche sur les attributions des articles (marche catalogue)
			EOAttribution uneAttribution = ((EOCatalogue) articles.objectAtIndex(0)).attribution();
			if (uneAttribution != null) {
				NSArray attributionsCatalogue = (NSArray) articles.valueForKeyPath("attribution");
				for (int i = 1; i < attributionsCatalogue.count(); i++) {
					EOAttribution att = ((EOCatalogue) articles.objectAtIndex(i)).attribution();
					if (uneAttribution.equals(att) == false) {
						uneAttribution = null;
						break;
					}
				}
				if (uneAttribution != null) {
					// Tous les articles appartiennent au meme catalogue marche
					attributionsCommunes.addObject(uneAttribution);
					return attributionsCommunes;
				}
			}
			// Recherche sur les attributions associees aux codes exer des articles
			NSMutableArray attributionsCodeExer = new NSMutableArray();
			Enumeration enumArticles = articles.objectEnumerator();
			// Construction d'un tableau d'ensembles (NSArray(NSSet)) des attributions de chaque article
			while (enumArticles.hasMoreElements()) {
				EOCatalogue unArticle = (EOCatalogue) enumArticles.nextElement();
				NSArray atts = unArticle.autresAttributionsExistantesPourCodeExer(ed);
				if (atts == null || atts.count() == 0) {
					attributionsCodeExer = null;
					break;
				}
				else {
					attributionsCodeExer.addObject(new NSSet(atts));
				}
			}

			if (attributionsCodeExer != null && attributionsCodeExer.count() > 0) {
				// On calcule l'intersection entre tous les ensembles
				NSMutableSet attsCommunes = new NSMutableSet((NSSet) attributionsCodeExer.objectAtIndex(0));
				for (int i = 1; i < attributionsCodeExer.count(); i++) {
					NSSet set1 = (NSSet) attributionsCodeExer.objectAtIndex(1);
					attsCommunes.intersectSet(set1);
				}
				attributionsCommunes.addObjectsFromArray(attsCommunes.allObjects());
				// On elimine les attributions pour lesquelles
				// le fournisseur ne fait pas partie de ceux associes a l'attribution
				EOFournisseur leFournisseur = ((EOCatalogue) articles.lastObject()).fournisseur();
				NSMutableArray attributionsAExclure = new NSMutableArray();
				Enumeration enumAttributionsCommunes = attributionsCommunes.objectEnumerator();
				while (enumAttributionsCommunes.hasMoreElements()) {
					EOAttribution uneAtt = (EOAttribution) enumAttributionsCommunes.nextElement();
					NSArray attFournisseurs = FinderFournisseur.getFournisseurs(ed, uneAtt);
					if (attFournisseurs != null) {
						if (attFournisseurs.containsObject(leFournisseur) == false) {
							attributionsAExclure.addObject(uneAtt);
						}
					}
				}
				attributionsCommunes.removeObjectsInArray(attributionsAExclure);
			}
		}

		return attributionsCommunes;
	}
}
