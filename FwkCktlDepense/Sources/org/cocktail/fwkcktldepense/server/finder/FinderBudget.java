/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public final class FinderBudget extends Finder {

	/**
	 * Recherche de lignes budgetaire et de type de credit ouvert au budget par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, date <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOBudgetExecCredit
	 */
	public static final NSArray<EOBudgetExecCredit> getBudgetPourUtilisateur(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		EOExercice exercice = null;

		if (bindings == null)
			throw new FactoryException("le bindings 'date' est obligatoire");
		if (bindings.objectForKey("date") == null)
			throw new FactoryException("le bindings 'date' est obligatoire");

		// Verifier les lignes de budget correspondant a l'exercice la date
		exercice = FinderExercice.getExercicePourDate(ed, (NSTimestamp) bindings.objectForKey("date"));
		if (exercice == null)
			return NSArray.emptyArray();

		return Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOBudgetExecCredit.ENTITY_NAME, "Recherche", bindings), sort());
	}

	public static final NSArray<EOBudgetExecCredit> getBudgets(EOEditingContext ed, NSArray<NSDictionary<String, Object>> arrayBindings) {
		if (ed == null || arrayBindings == null || arrayBindings.count() <= 0)
			return NSArray.emptyArray();

		NSMutableArray qualifiers = new NSMutableArray();

		for (int i = 0; i < arrayBindings.count(); i++) {
			NSDictionary dico = (NSDictionary) arrayBindings.objectAtIndex(i);
			if (dico.objectForKey("CLE") != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.BDXC_ID_KEY + "=%@", new NSArray(dico.objectForKey("CLE"))));
		}

		if (qualifiers.count() == 0)
			return NSArray.emptyArray();

		return Finder.tableauTrie(Finder.fetchArray(EOBudgetExecCredit.ENTITY_NAME, new EOOrQualifier(qualifiers), null, ed, true), sort());
	}

	/**
	 * Recherche de lignes budgetaire et de type de credit ouvert au budget pour un utilisateur et un exercice.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings 'utilisateur' et 'exercice' : obligatoires
	 * @return un NSArray contenant des EOBudgetExecCredit
	 */
	public static final NSArray<EOBudgetExecCredit> getBudgets(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null || bindings.objectForKey("utilisateur") == null || bindings.objectForKey("exercice") == null)
			throw new FactoryException("les bindings 'utilisateur' et 'exercice' sont obligatoires");

		return Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
				EOBudgetExecCredit.ENTITY_NAME, "Recherche", bindings), sort());
	}

	/**
	 * Recherche des budgets via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : utilisateur, exercice, organUb, organCr, organSsCr, tcdCode, convReference, convention, creditsDispo, lignesPreferees <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des NSDictionary { cle = bdxc_id; ligne=UB/CR/ssCR; lib=libelle organ, typeCredit=tcd_code; disponible=dispo;
	 *         isProrata=0 ou 1 }
	 */
	@SuppressWarnings("unchecked")
	public static final NSArray<NSDictionary<String, Object>> getRawRowBudgets(EOEditingContext ed, NSDictionary<String, Object> bindings) {

		//FIXME Rod : il faudrait nettoyer la table des préférences utilisateurs lorsqu'un organ a été supprimé ?
		//delete from jefy_Admin.utilisateur_preference where pref_id in (select p.PREF_ID from jefy_depense.v_utilisateur_preference_organ p , jefy_admin.utilisateur u where p.utl_ordre=u.utl_ordre and p.UP_VALUE not in (select org_id from jefy_admin.organ));

		return EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, false, bindings), null);
	}

	public static final int getRawRowCountBudgets(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		int nbreBudgets = -1;
		NSArray budgets = EOUtilities.rawRowsForSQL(ed, "carambole", constructionChaine(ed, true, bindings), null);

		if (budgets != null && budgets.count() == 1) {
			NSDictionary row = (NSDictionary) budgets.lastObject();
			nbreBudgets = ((Number) row.valueForKey("NBREBUDGETS")).intValue();
		}

		return nbreBudgets;
	}

	protected static String constructionChaine(EOEditingContext ed, boolean isForCount, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOExercice exercice = (EOExercice) bindings.objectForKey("exercice");
		if (exercice == null)
			throw new FactoryException("le binding 'exercice' est obligatoire");
		EOUtilisateur utilisateur = (EOUtilisateur) bindings.objectForKey("utilisateur");
		if (utilisateur == null)
			throw new FactoryException("le binding 'utilisateur' est obligatoire");

		Number exeOrdre = (Number) EOUtilities.primaryKeyForObject(ed, exercice).objectForKey(EOExercice.EXE_ORDRE_KEY);
		Number utlOrdre = (Number) EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY);

		String selectClause = "";
		String fromClause = "FROM " + EOBudgetExecCredit.ENTITY_TABLE_NAME + " b, jefy_admin.organ o, jefy_admin.type_credit t, jefy_admin.utilisateur_organ uo";
		String whereClause = "WHERE b.exe_ordre=" + exeOrdre + " AND uo.utl_ordre=" + utlOrdre;
		whereClause += " AND b.org_id=o.org_id and b.tcd_ordre=t.tcd_ordre and o.org_id=uo.org_id and o.org_niv>=3";
		String orderClause = "";
		String groupByClause = "";

		if (isForCount) {
			selectClause = "select count(distinct b.bdxc_id) NBREBUDGETS";
			whereClause += " AND b.org_id=o.org_id and b.tcd_ordre=t.tcd_ordre and o.org_id=uo.org_id and o.org_niv>=3";

		}
		else {
			selectClause = "select distinct b.bdxc_id cle, o.org_ub organUb, o.org_cr organCr,";
			selectClause += " o.org_souscr organSsCr, o.org_ub||' / '||o.org_cr||decode(o.org_souscr, null,'',' / '||o.org_souscr) ligne, o.org_lib lib, t.tcd_code typeCredit,";
			selectClause += " jefy_depense.budget.engager_disponible(b.exe_ordre, b.org_id, b.tcd_ordre) disponible, decode(count(orp_id),0,0,1) isProrata,";
			selectClause += FinderOrgan.DECODE_DEPENSE + " droit";
			fromClause += ", jefy_admin.organ_prorata op";
			whereClause += " and b.org_id=op.org_id(+) and b.exe_ordre=op.exe_ordre(+)";
			orderClause = "ORDER BY o.org_ub||' / '||o.org_cr||decode(o.org_souscr, null,'',' / '||o.org_souscr), t.tcd_code";
			groupByClause = "GROUP BY b.bdxc_id , o.org_ub, o.org_cr, o.org_souscr, o.org_ub||' / '||o.org_cr||decode(o.org_souscr, null,'',' / '||o.org_souscr), o.org_lib,";
			groupByClause += "t.tcd_code, jefy_depense.budget.engager_disponible(b.exe_ordre, b.org_id, b.tcd_ordre), ";
			groupByClause += FinderOrgan.DECODE_DEPENSE;
		}

		if (bindings.objectForKey("organUb") != null)
			whereClause += " AND upper(o.org_ub) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organUb")) + "%')";
		if (bindings.objectForKey("organCr") != null)
			whereClause += " AND upper(o.org_cr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organCr")) + "%')";
		if (bindings.objectForKey("organSsCr") != null)
			whereClause += " AND upper(o.org_souscr) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("organSsCr")) + "%')";

		if (bindings.objectForKey("tcdCode") != null)
			whereClause += " AND upper(t.tcd_code) like upper('%" + ZStringUtil.sqlString((String) bindings.objectForKey("tcdCode")) + "%')";

		if (bindings.objectForKey(EOTypeCredit.TCD_SECT_KEY) != null) {
			whereClause += " AND t.tcd_sect = '" + ZStringUtil.sqlString((String) bindings.objectForKey(EOTypeCredit.TCD_SECT_KEY)) + "'";
		}

		if (bindings.objectForKey("convReference") != null || bindings.objectForKey("convention") != null) {
			fromClause += ", " + EOConventionNonLimitative.ENTITY_TABLE_NAME + " c";
			whereClause += " AND c.org_id=b.org_id and c.tcd_ordre=b.tcd_ordre and c.exe_ordre=b.exe_ordre";

			if (bindings.objectForKey("convReference") != null)
				whereClause += " AND upper(c.conv_reference) like upper('%" + ZStringUtil.sqlStringSansAccent((String) bindings.objectForKey("convReference")) + "%')";
			if (bindings.objectForKey("convention") != null)
				whereClause += " AND c.conv_ordre=" + (Number) EOUtilities.primaryKeyForObject(ed, (EOConvention) bindings.objectForKey("convention")).objectForKey(EOConvention.CONV_ORDRE_KEY);
		}

		if (bindings.objectForKey("creditsDispo") != null && bindings.objectForKey("creditsDispo") instanceof Boolean &&
				((Boolean) bindings.objectForKey("creditsDispo")).booleanValue())
			whereClause += " and jefy_depense.budget.engager_disponible(b.exe_ordre, b.org_id, b.tcd_ordre)>0";

		if (bindings.objectForKey("lignesPreferees") != null && bindings.objectForKey("lignesPreferees") instanceof Boolean &&
				((Boolean) bindings.objectForKey("lignesPreferees")).booleanValue()) {
			whereClause += " and b.org_id in (select jefy_depense.en_nombre(up_value) from jefy_depense.v_utilisateur_preference_organ where utl_ordre=" + utlOrdre + ")";
		}

		return selectClause + " " + fromClause + " " + whereClause + " " + groupByClause + " " + orderClause;
	}

	/**
	 * Recherche des types de credit ouverts au budget pour une ligne budgetaire et un exercice.<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param organ ligne budgetaire pour laquelle se fait la recherche
	 * @param exercice exercice pour lequel se fait la recherche
	 * @return un NSArray contenant des EOBudgetExecCredit
	 */
	public static final NSArray<EOBudgetExecCredit> getBudgetsPourOrgan(EOEditingContext ed, EOOrgan organ, EOExercice exercice) {
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.ORGAN_KEY + "=%@", new NSArray(organ)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.EXERCICE_KEY + "=%@", new NSArray(exercice)));

		return Finder.fetchArray(EOBudgetExecCredit.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, true);
	}

	/**
	 * Recherche du budget pour une source de credit (ligne budgetaire, type de credit, exercice).<BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param source source pour laquelle se fait la recherche
	 * @return un NSArray contenant des EOBudgetExecCredit
	 */
	public static final NSArray<EOBudgetExecCredit> getBudgetsPourSource(EOEditingContext ed, EOSource source) {
		if (source.organ() == null) {
			return NSArray.emptyArray();
		}
		NSMutableArray arrayQualifier = new NSMutableArray();

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.ORGAN_KEY + "=%@",
				new NSArray(source.organ())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.TYPE_CREDIT_KEY + "=%@",
				new NSArray(source.typeCredit())));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.EXERCICE_KEY + "=%@",
				new NSArray(source.exercice())));

		return Finder.fetchArray(EOBudgetExecCredit.ENTITY_NAME, new EOAndQualifier(arrayQualifier), null, ed, true);
	}

	/**
	 * Methode de tri.<BR>
	 * 
	 * @return un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetExecCredit.ORGAN_KEY + "." + "sourceLibelle",
				EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetExecCredit.TYPE_CREDIT_KEY + "." +
				EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

}