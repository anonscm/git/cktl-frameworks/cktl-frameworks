/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.finder;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public final class FinderModePaiement extends Finder {

	public static final NSArray<EOModePaiement> getModePaiements(EOEditingContext ed, NSDictionary<String, Object> bindings) {
		if (bindings == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("exercice") == null)
			throw new FactoryException("le bindings 'exercice' est obligatoire");

		NSArray resultats = EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOModePaiement.ENTITY_NAME, "Recherche", bindings);

		if (resultats.count() > 0)
			return Finder.tableauTrie(resultats, sort());

		return fetchModePaiements(ed);
	}

	/**
	 * Recherche d'un mode de paiement suivant son code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param code code du mode de paiement
	 * @param exercice exercice du mode de paiement
	 * @return un EOModePaiement
	 */
	public static final EOModePaiement getModePaiement(EOEditingContext ed, String code, EOExercice exercice) {
		return getUnModePaiement(ed, code, exercice);
	}

	/**
	 * Recherche les modes de paiement d'un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice du mode de paiement
	 * @return un EOModePaiement
	 * @deprecated
	 */
	public static final NSArray<EOModePaiement> getModePaiements(EOEditingContext ed, EOExercice exercice) {
		return getModePaiements(ed, exercice, null);
	}

	/**
	 * Recherche les modes de paiement d'un exercice en tenant compte des droits de l'utilisateur. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice du mode de paiement
	 * @return un EOModePaiement
	 */
	public static final NSArray<EOModePaiement> getModePaiements(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur) {
		return getLesModePaiements(ed, exercice, utilisateur);
	}

	/**
	 * Recherche les modes de paiement d'un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param exercice exercice du mode de paiement
	 * @param utilisateur qui realise la recherche
	 * @return un EOModePaiement
	 */
	private static NSArray<EOModePaiement> getLesModePaiements(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur) {
		NSArray<EOModePaiement> lesModePaiements = null;
		NSMutableArray<EOQualifier> qual = new NSMutableArray<EOQualifier>();

		qual.addObject(new EOKeyValueQualifier(EOModePaiement.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice));
		qual.addObject(new EOKeyValueQualifier(EOModePaiement.MOD_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual, EOModePaiement.MODE_PAIEMENT_VALIDE));
		if (utilisateur == null || !utilisateur.isDroitLiquiderAvecModePaiementExtourne(exercice)) {
			qual.addObject(new EOKeyValueQualifier(EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorNotEqual, EOModePaiement.MODE_DOM_EXTOURNE));
		}

		lesModePaiements = Finder.fetchArray(EOModePaiement.ENTITY_NAME, new EOAndQualifier(qual), sort(), ed, true);
		return lesModePaiements;
	}

	/**
	 * Recherche d'un mode de paiement suivant son code et un exercice. <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param code code du mode de paiement
	 * @param exercice exercice du mode de paiement
	 * @return un NSArray de EOModePaiement
	 */
	private static EOModePaiement getUnModePaiement(EOEditingContext ed, String code, EOExercice exercice) {
		NSArray lesModePaiements = null;

		NSArray arrayModePaiements = fetchModePaiements(ed);

		lesModePaiements = new NSArray((NSArray) (EOQualifier.filteredArrayWithQualifier(arrayModePaiements,
				EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_CODE_KEY + "=%@ and " + EOModePaiement.EXERCICE_KEY + "=%@",
						new NSArray(new Object[] {
								code, exercice
						})))));

		if (lesModePaiements == null || lesModePaiements.count() == 0)
			return null;

		return (EOModePaiement) lesModePaiements.objectAtIndex(0);
	}

	/**
	 * Fetch toutes les modes de paiement pour les garder en memoire et eviter de refetcher ensuite <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 */
	private static NSArray<EOModePaiement> fetchModePaiements(EOEditingContext ed) {
		// if (arrayModePaiements==null)
		return Finder.fetchArray(ed, EOModePaiement.ENTITY_NAME, EOModePaiement.MOD_VALIDITE_KEY + "=%@",
				new NSArray(EOModePaiement.MODE_PAIEMENT_VALIDE), sort(), false);
	}

	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOModePaiement.MOD_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}
}
