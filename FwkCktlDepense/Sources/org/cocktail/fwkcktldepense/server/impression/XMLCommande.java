/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.server.impression;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.StringEscapeUtils;
import org.cocktail.fwkcktldepense.server.ConvertHelper;
import org.cocktail.fwkcktldepense.server.finder.FinderCompte;
import org.cocktail.fwkcktldepense.server.finder.FinderJefyAdminParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderOrganSignataire;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression;
import org.cocktail.fwkcktldepense.server.metier.EOCompte;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOIndividu;
import org.cocktail.fwkcktldepense.server.metier.EOJefyAdminParametre;
import org.cocktail.fwkcktldepense.server.metier.EOOrganSignataire;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.NumberToLettres;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class XMLCommande {
	public static String MAQUETTE = "CARAMBOLE_BON_DE_COMMANDE";

	public static void commandeInXml(CktlXMLWriter writer, EOCommandeImpression commandeImpression, boolean isNePasAfficherMontants) throws Exception {
		try {
			writer.startElement("commande");

			if (commandeImpression != null && commandeImpression.commande() != null) {

				if (commandeImpression.isDepenseAuComptant())
					writer.writeElement("entete", escapeSpecialChars("DEPENSE AU COMPTANT"));
				else
					writer.writeElement("entete", escapeSpecialChars("BON DE COMMANDE"));
				writer.writeElement("nePasAfficherLeMontant", String.valueOf(isNePasAfficherMontants));
				writer.writeElement("numero", nombreInXml(commandeImpression.commande().commNumero()));
				writer.writeElement("exercice", nombreInXml(commandeImpression.commande().exercice().exeExercice()));
				writer.writeElement("reference", escapeSpecialChars(commandeImpression.commande().commReference()));
				writer.writeElement("libelle", escapeSpecialChars(commandeImpression.commande().commLibelle()));
				writer.writeElement("dateCreation", dateInXml(commandeImpression.commande().commDateCreation()));
				writer.writeElement("dateModification", dateInXml(commandeImpression.commande().commDate()));
				writer.writeElement("dateimpression", dateInXml(new NSTimestamp()));

				// on trie les articles par libelle dans le cadre du B2B.
				Collection<EOArticle> articles = commandeImpression.commande().articles();
				if (commandeImpression.commande().isCommandeB2b()) {
					articles = commandeImpression.commande().articlesTriLibelle();
				}

				repartitionCnInXml(writer, commandeImpression.commande());
				totauxInXml(writer, commandeImpression.commande());
				marcheLotInXml(writer, commandeImpression.commande());
				parametresInXml(writer, commandeImpression.commande());
				commandeImpressionInXml(writer, commandeImpression);
				fournisseurInXml(writer, commandeImpression);
				articlesInXml(writer, articles);
				engagementsInXml(writer, (NSArray) commandeImpression.commande().commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY));
				signatairesInXml(writer, (NSArray) commandeImpression.commande().commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY));
			}

			writer.endElement();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static void repartitionCnInXml(CktlXMLWriter writer, EOCommande commande) throws Exception {
		try {
			writer.startElement("cns");

			if (commande != null) {
				NSArray tableau = commande.repartitionCodeExer();

				if (tableau != null) {

					for (int i = 0; i < tableau.count(); i++) {
						NSDictionary dico = (NSDictionary) tableau.objectAtIndex(i);

						writer.startElement("cn");
						writer.writeElement("code", escapeSpecialChars(((EOCodeExer) dico.objectForKey("codeExer")).codeMarche().cmCode()));
						writer.writeElement("libelle", escapeSpecialChars(((EOCodeExer) dico.objectForKey("codeExer")).codeMarche().cmLib()));
						writer.writeElement("ht", nombreInXml((BigDecimal) dico.objectForKey("ht")));
						writer.writeElement("ttc", nombreInXml((BigDecimal) dico.objectForKey("ttc")));
						writer.endElement();
					}
				}
			}

			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void totauxInXml(CktlXMLWriter writer, EOCommande commande) throws Exception {
		try {
			writer.startElement("totaux");

			if (commande != null) {
				writer.writeElement("totalHt", nombreInXml(commande.totalHt()));
				writer.writeElement("totalTtc", nombreInXml(commande.totalTtc()));
				writer.writeElement("totalTva", nombreInXml(commande.totalTtc().subtract(commande.totalHt())));
				writer.writeElement("totalLettres", escapeSpecialChars(NumberToLettres.transforme(commande.totalTtc(),
						commande.devise().devLibelle().toLowerCase())));
			}

			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void marcheLotInXml(CktlXMLWriter writer, EOCommande commande) throws Exception {
		try {

			String lot = " ", marche = " ";

			if (commande != null) {
				EOAttribution attribution = commande.attribution();

				if (attribution != null) {
					if (attribution.lot() != null) {
						lot = attribution.lot().lotIndex();
						if (attribution.lot().marche() != null)
							marche = attribution.lot().marche().libelle();
					}
				}
			}

			writer.writeElement("lot", escapeSpecialChars(lot));
			writer.writeElement("marche", escapeSpecialChars(marche));
		} catch (Exception e) {
			throw e;
		}
	}

	private static void parametresInXml(CktlXMLWriter writer, EOCommande commande) throws Exception {
		try {
			EOJefyAdminParametre parametre;

			// Jefy_admin parametres
			parametre = FinderJefyAdminParametre.getParametre(commande.editingContext(), "VILLE", commande.exercice());
			if (parametre != null)
				writer.writeElement("lieuimpression", escapeSpecialChars(parametre.parValue()));

			writer.startElement("etablissement");

			parametre = FinderJefyAdminParametre.getParametre(commande.editingContext(), "UNIV", commande.exercice());
			if (parametre != null)
				writer.writeElement("nom", escapeSpecialChars(parametre.parValue()));

			parametre = FinderJefyAdminParametre.getParametre(commande.editingContext(), "NUMERO_SIRET", commande.exercice());
			if (parametre != null)
				writer.writeElement("numerosiret", escapeSpecialChars(parametre.parValue()));

			parametre = FinderJefyAdminParametre.getParametre(commande.editingContext(), "TVA_INTRACOM", commande.exercice());
			if (parametre != null)
				writer.writeElement("tvaintracom", escapeSpecialChars(parametre.parValue()));

			writer.endElement();

			// Jefy_depense parametres
			String messageImpression = FinderParametre.getParametreMessageImpression(commande.editingContext(), commande.exercice());
			writer.writeElement("messagefournisseur", escapeSpecialChars(messageImpression));

			// infos commandes
			writer.startElement("infossuivi");
			if (commande != null) {
				parametre = FinderJefyAdminParametre.getParametre(commande.editingContext(), "URL_FOURNISSEUR", commande.exercice());
				if (parametre != null) {
					writer.writeElement("urlsuivi", escapeSpecialChars(parametre.parValue()));

					EOCompte compte = FinderCompte.getCompteFournisseur(commande.editingContext(), commande.fournisseur());
					if (compte != null) {
						writer.writeElement("login", escapeSpecialChars(compte.cptLogin()));
						writer.writeElement("passwd", escapeSpecialChars(compte.cptPasswd()));
					}
				}
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void commandeImpressionInXml(CktlXMLWriter writer, EOCommandeImpression commandeImpression) throws Exception {
		try {

			if (commandeImpression != null) {
				writer.writeElement("observations", escapeSpecialChars(commandeImpression.cimpInfosImpression()));

				writer.startElement("service");
				if (commandeImpression.structureService() != null) {

					writer.writeElement("nom", escapeSpecialChars(commandeImpression.structureService().llStructure()));
					writer.writeElement("tel", escapeSpecialChars(commandeImpression.cimpServiceTelephone()));
					writer.writeElement("fax", escapeSpecialChars(commandeImpression.cimpServiceFax()));

					if (commandeImpression.adresseService() != null) {
						writer.startElement("adresse");
						writer.writeElement("adresse1", escapeSpecialChars(commandeImpression.adresseService().adrAdresse1()));
						writer.writeElement("adresse2", escapeSpecialChars(commandeImpression.adresseService().adrAdresse2()));
						writer.writeElement("boitePostale", escapeSpecialChars(commandeImpression.adresseService().adrBp()));
						writer.writeElement("codePostal", escapeSpecialChars(commandeImpression.adresseService().codePostal()));
						writer.writeElement("ville", escapeSpecialChars(commandeImpression.adresseService().ville()));
						if (commandeImpression.adresseService().pays() != null)
							writer.writeElement("pays", escapeSpecialChars(commandeImpression.adresseService().pays().lcPays()));
						writer.endElement();
					}
				}
				writer.endElement();

				writer.startElement("livraison");
				if (commandeImpression.structureLivraison() != null) {

					writer.writeElement("nom", escapeSpecialChars(commandeImpression.structureLivraison().llStructure()));
					writer.writeElement("tel", escapeSpecialChars(commandeImpression.cimpLivraisonTelephone()));
					writer.writeElement("fax", escapeSpecialChars(commandeImpression.cimpLivraisonFax()));

					if (commandeImpression.adresseLivraison() != null) {
						writer.startElement("adresse");
						writer.writeElement("adresse1", escapeSpecialChars(commandeImpression.adresseLivraison().adrAdresse1()));
						writer.writeElement("adresse2", escapeSpecialChars(commandeImpression.adresseLivraison().adrAdresse2()));
						writer.writeElement("boitePostale", escapeSpecialChars(commandeImpression.adresseLivraison().adrBp()));
						writer.writeElement("codePostal", escapeSpecialChars(commandeImpression.adresseLivraison().codePostal()));
						writer.writeElement("ville", escapeSpecialChars(commandeImpression.adresseLivraison().ville()));
						if (commandeImpression.adresseLivraison().pays() != null)
							writer.writeElement("pays", escapeSpecialChars(commandeImpression.adresseLivraison().pays().lcPays()));
						writer.endElement();
					}
				}
				writer.endElement();
			}
		} catch (Exception e) {
			throw e;
		}
	}

	private static void fournisseurInXml(CktlXMLWriter writer, EOCommandeImpression commandeImpression) throws Exception {
		try {
			writer.startElement("fournisseur");

			EOFournisseur fournisseur = commandeImpression.commande().fournisseur();

			if (fournisseur != null) {
				writer.writeElement("code", escapeSpecialChars(fournisseur.fouCode()));
				writer.writeElement("nom", escapeSpecialChars(fournisseur.fouNom()));

				writer.startElement("adresse");

				if (commandeImpression.adresseFournisseur() == null) {
					writer.writeElement("adresse1", escapeSpecialChars(fournisseur.adrAdresse1()));
					writer.writeElement("adresse2", escapeSpecialChars(fournisseur.adrAdresse2()));
					writer.writeElement("boitePostale", escapeSpecialChars(fournisseur.adrBp()));
					writer.writeElement("codePostal", escapeSpecialChars(fournisseur.codePostal()));
					writer.writeElement("ville", escapeSpecialChars(fournisseur.adrVille()));
					writer.writeElement("pays", escapeSpecialChars(fournisseur.pays()));
				}
				else {
					writer.writeElement("adresse1", escapeSpecialChars(commandeImpression.adresseFournisseur().adrAdresse1()));
					writer.writeElement("adresse2", escapeSpecialChars(commandeImpression.adresseFournisseur().adrAdresse2()));
					writer.writeElement("boitePostale", escapeSpecialChars(commandeImpression.adresseFournisseur().adrBp()));
					writer.writeElement("codePostal", escapeSpecialChars(commandeImpression.adresseFournisseur().codePostal(fournisseur)));
					writer.writeElement("ville", escapeSpecialChars(commandeImpression.adresseFournisseur().ville()));
					if (commandeImpression.adresseFournisseur().pays() != null) {
						writer.writeElement("pays", escapeSpecialChars(commandeImpression.adresseFournisseur().pays().lcPays()));
					}
				}
				writer.endElement();
				writer.startElement("faxes");
				//ajouter fax
				EOFournis fournis = ConvertHelper.convertDepenseFournisToPersonneFournis(fournisseur.editingContext(), fournisseur);
				NSArray<EOPersonneTelephone> faxes = fournis.toPersonne().toPersonneTelephones();
				faxes = EOQualifier.filteredArrayWithQualifier(faxes, new EOAndQualifier(new NSArray(new Object[] {
						EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
				})));
				faxes = EOSortOrdering.sortedArrayUsingKeyOrderArray(faxes, new NSArray(new Object[] {
						EOPersonneTelephone.SORT_TEL_PRINCIPAL_DESC
				}));
				for (int i = 0; i < faxes.count(); i++) {
					EOPersonneTelephone fax = faxes.objectAtIndex(i);
					writer.writeElement("fax", escapeSpecialChars(fax.getTelephoneFormateAvecIndicatif()));
				}
				writer.endElement();
				writer.startElement("telephones");
				//ajouter telephones
				NSArray<EOPersonneTelephone> tels = fournis.toPersonne().toPersonneTelephones();
				tels = EOQualifier.filteredArrayWithQualifier(tels, new EOAndQualifier(new NSArray(new Object[] {
						EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
				})));
				tels = EOSortOrdering.sortedArrayUsingKeyOrderArray(tels, new NSArray(new Object[] {
						EOPersonneTelephone.SORT_TEL_PRINCIPAL_DESC
				}));
				for (int i = 0; i < tels.count(); i++) {
					EOPersonneTelephone tel = tels.objectAtIndex(i);
					writer.writeElement("telephone", escapeSpecialChars(tel.getTelephoneFormateAvecIndicatif()));
				}
				writer.endElement();
			}

			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void articlesInXml(CktlXMLWriter writer, Collection<EOArticle> articles) throws Exception {
		try {
			writer.startElement("articles");

			if (articles == null)
				articles = new ArrayList<EOArticle>();

			for (EOArticle article : articles) {
				writer.startElement("article");
				writer.writeElement("reference", escapeSpecialChars(article.artReference()));
				writer.writeElement("libelle", escapeSpecialChars(article.artLibelle()));
				writer.writeElement("prixUnitaireHt", nombreInXml(article.artPrixHt()));
				writer.writeElement("prixUnitaireTtc", nombreInXml(article.artPrixTtc()));
				writer.writeElement("prixTotalHt", nombreInXml(article.artPrixTotalHt()));
				writer.writeElement("prixTotalTtc", nombreInXml(article.artPrixTotalTtc()));
				writer.writeElement("quantite", nombreInXml(article.artQuantite()));
				writer.writeElement("tva", nombreInXml(article.tva().tvaTaux()));
				writer.writeElement("codeNomenclature", escapeSpecialChars(article.codeExer().codeMarche().cmCode()));

				writer.endElement();
			}

			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	public static void signatairesInXml(CktlXMLWriter writer, NSArray engagements) throws Exception {
		try {
			NSMutableArray arrayIndividus = new NSMutableArray();
			NSMutableArray arrayTitres = new NSMutableArray();
			NSMutableArray arrayTypeSignatures = new NSMutableArray();

			if (engagements == null)
				engagements = new NSArray();

			String methode = EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_BUD;

			if (engagements.count() > 0) {
				EOEngagementBudget engage = (EOEngagementBudget) engagements.objectAtIndex(0);
				methode = FinderJefyAdminParametre.getMethodeLimitativeMontantSignataire(engage.editingContext(), engage.exercice());

				if (methode == null)
					methode = EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_BUD;
				else {
					if (!methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_BUD) &&
							!methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_TTC) &&
							!methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_HT))
						methode = EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_BUD;
				}
			}

			BigDecimal montantTotal = computeSumForKey(engagements, EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_KEY);
			if (methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_HT))
				montantTotal = computeSumForKey(engagements, EOEngagementBudget.ENG_HT_SAISIE_KEY);
			if (methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_TTC))
				montantTotal = computeSumForKey(engagements, EOEngagementBudget.ENG_TTC_SAISIE_KEY);

			for (int i = 0; i < engagements.count(); i++) {
				EOEngagementBudget engage = (EOEngagementBudget) engagements.objectAtIndex(i);

				/*
				 * BigDecimal montant=engage.engMontantBudgetaire(); if (methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_HT))
				 * montant=engage.engHtSaisie(); if (methode.equals(EOJefyAdminParametre.METHODE_LIMITE_MONTANT_SIGN_TC_VALUE_TTC))
				 * montant=engage.engTtcSaisie();
				 */

				NSArray array = FinderOrganSignataire.getOrganSignataires(engage.organ(), new NSTimestamp(), montantTotal, engage.typeCredit());
				for (int j = 0; j < array.count(); j++) {
					EOOrganSignataire organSignataire = (EOOrganSignataire) array.objectAtIndex(j);
					EOIndividu individu = organSignataire.individu();
					if (arrayIndividus.containsObject(individu) == false) {
						arrayIndividus.addObject(individu);
						if (organSignataire.orsiLibelleSignataire() == null) {
							arrayTitres.addObject(" ");
						}
						else {
							arrayTitres.addObject(organSignataire.orsiLibelleSignataire());
						}
						arrayTypeSignatures.addObject(organSignataire.typeSignature().tysiLibelle());
					}
				}
			}

			writer.startElement("signataires");

			for (int i = 0; i < arrayIndividus.count(); i++) {
				EOIndividu individu = (EOIndividu) arrayIndividus.objectAtIndex(i);
				String titre = (String) arrayTitres.objectAtIndex(i);
				String typeSignature = (String) arrayTypeSignatures.objectAtIndex(i);
				writer.startElement("signataire");
				writer.writeElement("nom", escapeSpecialChars(individu.nomUsuel()));
				writer.writeElement("prenom", escapeSpecialChars(individu.prenom()));
				writer.writeElement("typeSignataire", escapeSpecialChars(typeSignature));

				if (!titre.equals(" ")) {
					writer.writeElement("titre", escapeSpecialChars(titre));
				}

				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsInXml(CktlXMLWriter writer, NSArray engagements) throws Exception {
		try {
			writer.startElement("engagements");

			if (engagements == null)
				engagements = new NSArray();

			for (int i = 0; i < engagements.count(); i++) {
				EOEngagementBudget engage = (EOEngagementBudget) engagements.objectAtIndex(i);

				writer.startElement("engagement");
				writer.writeElement("numero", nombreInXml(engage.engNumero()));
				writer.writeElement("libelle", escapeSpecialChars(engage.engLibelle()));
				writer.writeElement("source", escapeSpecialChars(engage.source().libelle()));
				writer.writeElement("ligneBudgetaire", escapeSpecialChars(engage.organ().sourceLibelle()));
				writer.writeElement("libelleLigneBudgetaire", escapeSpecialChars(engage.organ().orgLibelle()));
				writer.writeElement("typeCreditCode", escapeSpecialChars(engage.typeCredit().tcdCode()));
				writer.writeElement("typeCreditAbrege", escapeSpecialChars(engage.typeCredit().tcdAbrege()));
				writer.writeElement("typeCreditLibelle", escapeSpecialChars(engage.typeCredit().tcdLibelle()));
				writer.writeElement("prorata", nombreInXml(engage.tauxProrata().tapTaux()));
				writer.writeElement("montantHt", nombreInXml(engage.engHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.engTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.engTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.engMontantBudgetaire()));

				engagementsActionsInXml(writer, (NSArray) engage.engagementControleActions());
				engagementsAnalytiquesInXml(writer, (NSArray) engage.engagementControleAnalytiques());
				engagementsConventionsInXml(writer, (NSArray) engage.engagementControleConventions());
				engagementsHorsMarchesInXml(writer, (NSArray) engage.engagementControleHorsMarches());
				engagementsMarchesInXml(writer, (NSArray) engage.engagementControleMarches());
				engagementsPlanComptablesInXml(writer, (NSArray) engage.engagementControlePlanComptables());

				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsActionsInXml(CktlXMLWriter writer, NSArray engagementsActions) throws Exception {
		try {
			writer.startElement("engagementsActions");

			if (engagementsActions == null)
				engagementsActions = new NSArray();

			for (int i = 0; i < engagementsActions.count(); i++) {
				EOEngagementControleAction engage = (EOEngagementControleAction) engagementsActions.objectAtIndex(i);

				writer.startElement("engagementAction");
				writer.writeElement("actionCode", escapeSpecialChars(engage.typeAction().tyacCode()));
				writer.writeElement("actionLibelle", escapeSpecialChars(engage.typeAction().tyacLibelle()));
				writer.writeElement("montantHt", nombreInXml(engage.eactHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.eactTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.eactTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.eactMontantBudgetaire()));
				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsAnalytiquesInXml(CktlXMLWriter writer, NSArray engagementsAnalytiques) throws Exception {
		if (engagementsAnalytiques == null || engagementsAnalytiques.count() == 0)
			return;

		try {
			writer.startElement("engagementsAnalytiques");

			if (engagementsAnalytiques == null)
				engagementsAnalytiques = new NSArray();

			for (int i = 0; i < engagementsAnalytiques.count(); i++) {
				EOEngagementControleAnalytique engage = (EOEngagementControleAnalytique) engagementsAnalytiques.objectAtIndex(i);

				writer.startElement("engagementAnalytique");
				writer.writeElement("analytiqueCode", escapeSpecialChars(engage.codeAnalytique().canCode()));
				writer.writeElement("analytiqueLibelle", escapeSpecialChars(engage.codeAnalytique().canLibelle()));
				writer.writeElement("montantHt", nombreInXml(engage.eanaHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.eanaTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.eanaTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.eanaMontantBudgetaire()));
				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsConventionsInXml(CktlXMLWriter writer, NSArray engagementsConventions) throws Exception {
		if (engagementsConventions == null || engagementsConventions.count() == 0)
			return;

		try {
			writer.startElement("engagementsConventions");

			if (engagementsConventions == null)
				engagementsConventions = new NSArray();

			for (int i = 0; i < engagementsConventions.count(); i++) {
				EOEngagementControleConvention engage = (EOEngagementControleConvention) engagementsConventions.objectAtIndex(i);

				writer.startElement("engagementConvention");
				writer.writeElement("convention", escapeSpecialChars(engage.convention().convReference()));
				writer.writeElement("montantHt", nombreInXml(engage.econHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.econTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.econTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.econMontantBudgetaire()));
				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsHorsMarchesInXml(CktlXMLWriter writer, NSArray engagementsHorsMarches) throws Exception {
		if (engagementsHorsMarches == null || engagementsHorsMarches.count() == 0)
			return;

		try {
			writer.startElement("engagementsHorsMarches");

			if (engagementsHorsMarches == null)
				engagementsHorsMarches = new NSArray();

			for (int i = 0; i < engagementsHorsMarches.count(); i++) {
				EOEngagementControleHorsMarche engage = (EOEngagementControleHorsMarche) engagementsHorsMarches.objectAtIndex(i);

				writer.startElement("engagementHorsMarche");
				writer.writeElement("nomenclatureCode", escapeSpecialChars(engage.codeExer().codeMarche().cmCode()));
				writer.writeElement("nomenclatureLibelle", escapeSpecialChars(engage.codeExer().codeMarche().cmLib()));
				writer.writeElement("montantHt", nombreInXml(engage.ehomHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.ehomTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.ehomTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.ehomMontantBudgetaire()));
				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsMarchesInXml(CktlXMLWriter writer, NSArray engagementsMarches) throws Exception {
		if (engagementsMarches == null || engagementsMarches.count() == 0)
			return;

		try {
			writer.startElement("engagementsMarches");

			if (engagementsMarches == null)
				engagementsMarches = new NSArray();

			for (int i = 0; i < engagementsMarches.count(); i++) {
				EOEngagementControleMarche engage = (EOEngagementControleMarche) engagementsMarches.objectAtIndex(i);

				writer.startElement("engagementMarche");
				writer.writeElement("lotIndex", escapeSpecialChars(engage.attribution().lot().lotIndex()));
				writer.writeElement("lotLibelle", StringCtrl.cut(escapeSpecialChars(engage.attribution().lot().lotLibelle()), 120));
				writer.writeElement("marcheIndex", escapeSpecialChars(engage.attribution().lot().marche().marIndex()));
				writer.writeElement("marcheLibelle", StringCtrl.cut(escapeSpecialChars(engage.attribution().lot().marche().marLibelle()), 120));
				writer.writeElement("marcheExercice", nombreInXml(engage.attribution().lot().marche().exercice().exeExercice()));
				writer.writeElement("montantHt", nombreInXml(engage.emarHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.emarTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.emarTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.emarMontantBudgetaire()));
				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			throw e;
		}
	}

	private static void engagementsPlanComptablesInXml(CktlXMLWriter writer, NSArray engagementsPlanCompables) throws Exception {
		try {
			writer.startElement("engagementsPlanComptables");

			if (engagementsPlanCompables == null)
				engagementsPlanCompables = new NSArray();

			for (int i = 0; i < engagementsPlanCompables.count(); i++) {
				EOEngagementControlePlanComptable engage = (EOEngagementControlePlanComptable) engagementsPlanCompables.objectAtIndex(i);

				writer.startElement("engagementPlanComptable");
				writer.writeElement("imputationCode", escapeSpecialChars(engage.planComptable().pcoNum()));
				writer.writeElement("imputationLibelle", escapeSpecialChars(engage.planComptable().pcoLibelle()));
				writer.writeElement("montantHt", nombreInXml(engage.epcoHtSaisie()));
				writer.writeElement("montantTva", nombreInXml(engage.epcoTvaSaisie()));
				writer.writeElement("montantTtc", nombreInXml(engage.epcoTtcSaisie()));
				writer.writeElement("montantBudgetaire", nombreInXml(engage.epcoMontantBudgetaire()));
				writer.endElement();
			}
			writer.endElement();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static String dateInXml(NSTimestamp date) throws IOException {
		if (date == null)
			return "";
		NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%Y");
		return formatter.format(date);
	}

	private static String nombreInXml(Number nombre) {
		if (nombre == null)
			return "";
		return nombre.toString();
	}

	public static String escapeSpecialChars(String val) {
		StringBuffer response = null;
		if (val == null || val.equals(""))
			return "";
		if ((val != null) && (!val.equals(""))) {
			response = new StringBuffer();
			int l = val.length();
			char c = ' ';
			for (int i = 0; i < l; i++) {
				c = val.charAt(i);
				if (c == '<')
					response = response.append(CktlXMLWriter.SpecChars.lt);
				else if (c == '>')
					response = response.append(CktlXMLWriter.SpecChars.gt);
				else if (c == '&')
					response = response.append(CktlXMLWriter.SpecChars.amp);
				else if (c == '"')
					response = response.append(CktlXMLWriter.SpecChars.quot);
				else if (c == '\'')
					response = response.append("&#x0027;");
				else if (c == '\t')
					response = response.append("&#x9;");
				else if (c == '\r')
					response = response.append(CktlXMLWriter.SpecChars.br);
				// use character references for anything above 0xff
				else if (c > 255)
					response = response.append("&#" + (int) c + ";");
				// trash control 0 chars: [0x00 - 0x20] except tab, carriage return and line feed
				else if (c == '\n')
					response = response.append(CktlXMLWriter.SpecChars.br);
				else if (c < 32)
					response = response.append(CktlXMLWriter.SpecChars.nbsp);
				// trash control 1 chars: [0x80 - 0x9f], del: 0x7f
				else if (c >= 127 && c < 160)
					response = response.append(CktlXMLWriter.SpecChars.nbsp);
				else
					response = response.append(c);
			}
		}

		if (response != null)
			return response.toString();
		else
			return "";
	}

	protected static BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0.0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + key);
	}
}
