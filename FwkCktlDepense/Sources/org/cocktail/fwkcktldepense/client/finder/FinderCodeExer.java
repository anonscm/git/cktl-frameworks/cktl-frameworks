/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.client.finder;

import org.cocktail.fwkcktldepense.client.metier.EOAttribution;
import org.cocktail.fwkcktldepense.client.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.client.metier.EOCodeMarche;
import org.cocktail.fwkcktldepense.client.metier.EOExercice;
import org.cocktail.fwkcktldepense.client.metier.EOLotNomenclature;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public final class FinderCodeExer extends Finder {

    /**
     * Recherche des codes nomenclature valides pour un exercice
     * <BR>
     * @param ed
	 *        editingContext dans lequel se fait le fetch
     * @param exercice
     *        EOExercice pour lequel effectuer la recherche
     * @return
     *        un NSArray contenant des EOCodeExer
     */
    public static final NSArray getCodeExerValidesPourExercice(EOEditingContext ed, EOExercice exercice) {
       	NSMutableArray arrayQualifier=new NSMutableArray();
    	NSArray myResult = null;
    	
		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article ?

    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.CE_SUPPR_KEY+"=%@", new NSArray(EOCodeExer.ETAT_VALIDE)));
    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
    			EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_SUPPR_KEY+"=%@", new NSArray(EOCodeMarche.ETAT_VALIDE)));
    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeExer.EXERCICE_KEY+"=%@", new NSArray(exercice)));
    	arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(
    			EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_NIVEAU_KEY+"=%@", new NSArray(new Integer(EOCodeExer.niveauEngageable()))));

        EOFetchSpecification spec = new EOFetchSpecification(EOCodeExer.ENTITY_NAME,new EOAndQualifier(arrayQualifier),null,true,true,null);
        spec.setPrefetchingRelationshipKeyPaths(new NSArray(EOCodeExer.CODE_MARCHE_KEY));
        myResult=new NSArray(ed.objectsWithFetchSpecification(spec));

    	return Finder.tableauTrie(myResult, sort());
    }
    
    /**
     * Recherche les CodeExer autorises pour cette attribution via son lot.<BR>
     * @param ed
	 *        editingContext dans lequel se fait le fetch
     * @param attribution
     *        attribution pour laquelle on fait la recherche
     * @return
     *        un NSArray contenant des EOCodeExer
     */
    public static final NSArray getCodeExers(EOEditingContext ed, EOAttribution attribution) {
    	
		// TODO : regarder pour niveau 3 ... ya coucouille ... pour article

        NSMutableArray localSort = new NSMutableArray();
        localSort.addObject(EOSortOrdering.sortOrderingWithKey(EOLotNomenclature.CODE_EXER_KEY+"."+EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY, 
          EOSortOrdering.CompareCaseInsensitiveAscending));
        localSort.addObject(EOSortOrdering.sortOrderingWithKey(EOLotNomenclature.CODE_EXER_KEY+"."+EOCodeExer.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, 
          EOSortOrdering.CompareCaseInsensitiveAscending));   

    	NSArray array=Finder.fetchArray(EOLotNomenclature.ENTITY_NAME, 
    			EOQualifier.qualifierWithQualifierFormat("lot=%@", new NSArray(attribution.lot())), localSort, ed, false);

    	return (NSArray)array.valueForKeyPath("codeExer");
    }
    
    /**
     * Methode de tri.<BR>
     * @return
     *        un NSArray contenant des EOSortOrdering
     */
    private static NSArray sort() {
    	NSMutableArray array=new NSMutableArray();
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOCodeExer.CODE_MARCHE_KEY+"."+EOCodeMarche.CM_CODE_KEY, 
    			EOSortOrdering.CompareCaseInsensitiveAscending));
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOCodeExer.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, 
    			EOSortOrdering.CompareCaseInsensitiveAscending));  	
    	return array;
    }
}
