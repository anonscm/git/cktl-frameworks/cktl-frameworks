/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.client.finder;

import org.cocktail.fwkcktldepense.client.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.client.metier.EOPlancoCredit;
import org.cocktail.fwkcktldepense.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPlanComptable {

	public static NSArray find(EOEditingContext ec, EOTypeCredit typeCredit, String recherche)	{
		NSMutableArray andQualifier=new NSMutableArray();
		
		if (typeCredit==null)
			return new NSArray();
		
		andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));
		andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PCC_ETAT_KEY+"=%@", new NSArray(EOPlancoCredit.PCC_VALIDE)));
		andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_VALIDITE_KEY+"=%@", 
				new NSArray(EOPlanComptable.PCO_VALIDE)));
		andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PLA_QUOI_KEY+"=%@", new NSArray(EOPlancoCredit.PLA_QUOI_DEPENSE)));
		
		if (recherche!=null) {
			NSMutableArray orQualifier=new NSMutableArray();
			orQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY+" caseInsensitiveLike %@", 
					new NSArray("*"+recherche+"*")));
			orQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_LIBELLE_KEY+" caseInsensitiveLike %@", 
					new NSArray("*"+recherche+"*")));
			andQualifier.addObject(new EOOrQualifier(orQualifier));
		}
		
    	NSArray array=Finder.fetchArray(EOPlancoCredit.ENTITY_NAME, new EOAndQualifier(andQualifier), 
    					new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlancoCredit.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, 
    							EOSortOrdering.CompareCaseInsensitiveAscending)), ec, false);

    	return (NSArray)array.valueForKeyPath(EOPlancoCredit.PLAN_COMPTABLE_KEY);
	}
}
