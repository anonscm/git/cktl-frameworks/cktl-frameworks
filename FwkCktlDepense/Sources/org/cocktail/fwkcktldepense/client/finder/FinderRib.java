/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.client.finder;

import org.cocktail.fwkcktldepense.client.exception.FactoryException;
import org.cocktail.fwkcktldepense.client.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.client.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.client.metier.EORibFournisseur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public final class FinderRib extends Finder {

	/**
	 * Recherche les ribs pour un fournisseur et un mode de paiement par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : fournisseur, modePaiement
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param bindings
	 *        dictionnaire contenant les bindings
	 * @return
	 *        un NSArray contenant des EORib
	 */
	public static final NSArray getRibs(EOEditingContext ed, EOModePaiement modePaiement, EOFournisseur fournisseur) {
		if (modePaiement==null)
			throw new FactoryException("le 'modePaiement' est obligatoire");      			
		if (fournisseur==null)
			throw new FactoryException("le 'fournisseur' est obligatoire");      			

		if (modePaiement!=null && !modePaiement.modDom().equals(EOModePaiement.MODE_VIREMENT))
			return new NSArray();

		NSMutableArray qual = new NSMutableArray();
		qual.addObject(EOQualifier.qualifierWithQualifierFormat(EORibFournisseur.FOURNISSEUR_KEY+"=%@", new NSArray(fournisseur)));
		qual.addObject(EOQualifier.qualifierWithQualifierFormat(EORibFournisseur.MOD_CODE_KEY+"=%@", new NSArray(modePaiement.modCode())));
		qual.addObject(EOQualifier.qualifierWithQualifierFormat(EORibFournisseur.RIB_VALIDE_KEY+"=%@", new NSArray(EORibFournisseur.ETAT_VALIDE)));
		return Finder.fetchArray(EORibFournisseur.ENTITY_NAME, new EOAndQualifier(qual), null, ed, true);
	}
}