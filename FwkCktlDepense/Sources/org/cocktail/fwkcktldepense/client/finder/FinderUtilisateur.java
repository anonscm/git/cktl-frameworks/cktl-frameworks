/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktldepense.client.finder;

import org.cocktail.fwkcktldepense.client.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderUtilisateur {

	public static EOUtilisateur findForPersId(EOEditingContext ec, Number persId)	{
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.PERS_ID_KEY+"=%@", new NSArray(persId)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY+"=%@", new NSArray(EOTypeEtat.VALIDE)));
			EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

			return (EOUtilisateur)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{e.printStackTrace();return null;}
	}
}
