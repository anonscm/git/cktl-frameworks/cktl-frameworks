/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.client.finder;

import org.cocktail.fwkcktldepense.client.exception.FactoryException;
import org.cocktail.fwkcktldepense.client.metier.EOAttribution;
import org.cocktail.fwkcktldepense.client.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.client.metier.EOLot;
import org.cocktail.fwkcktldepense.client.metier.EOMarche;
import org.cocktail.fwkcktldepense.client.metier.EOSousTraitant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public final class FinderAttribution extends Finder {

	/**
	 * Recherche des attributions valides des marches par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : lot, marche, date, codeExer, fournisseur
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param bindings
	 *        dictionnaire contenant les bindings
	 * @return
	 *        un NSArray contenant des EOAttribution
	 */
    public static final NSArray getAttributionsValides(EOEditingContext ed, NSTimestamp date, EOFournisseur fournisseur) {
    	if (date == null)
    		throw new FactoryException("la 'date' est obligatoire");      			
    	if (fournisseur == null)
    		throw new FactoryException("le 'fournisseur' est obligatoire");      			

    	NSMutableArray qual=new NSMutableArray(), orqual=new NSMutableArray();
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_VALIDE_KEY+"=%@", new NSArray(EOAttribution.ETAT_VALIDE)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_SUPPR_KEY+"=%@", new NSArray(EOAttribution.ETAT_NON_SUPPRIME)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_DEBUT_KEY+"<=%@", new NSArray(date)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.ATT_FIN_KEY+">=%@", new NSArray(date)));

    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.LOT_VALIDE_KEY+"=%@", new NSArray(EOLot.ETAT_VALIDE)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.LOT_SUPPR_KEY+"=%@", new NSArray(EOLot.ETAT_NON_SUPPRIME)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.LOT_DEBUT_KEY+"<=%@", new NSArray(date)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.LOT_FIN_KEY+">=%@", new NSArray(date)));

    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.MARCHE_KEY+"."+EOMarche.MAR_VALIDE_KEY+"=%@", new NSArray(EOMarche.ETAT_VALIDE)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.MARCHE_KEY+"."+EOMarche.MAR_SUPPR_KEY+"=%@", new NSArray(EOMarche.ETAT_NON_SUPPRIME)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.MARCHE_KEY+"."+EOMarche.MAR_DEBUT_KEY+"<=%@", new NSArray(date)));
    	qual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.LOT_KEY+"."+EOLot.MARCHE_KEY+"."+EOMarche.MAR_FIN_KEY+">=%@", new NSArray(date)));

    	orqual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.FOURNISSEUR_KEY+"=%@", new NSArray(fournisseur)));
    	orqual.addObject(EOQualifier.qualifierWithQualifierFormat(EOAttribution.SOUS_TRAITANTS_KEY+"."+EOSousTraitant.FOURNISSEUR_KEY+"=%@", new NSArray(fournisseur)));
    	qual.addObject(new EOOrQualifier(orqual));

    	return Finder.fetchArray(EOAttribution.ENTITY_NAME, new EOAndQualifier(qual), null, ed, true);
    }
}
