/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.client.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.client.exception.FactoryException;
import org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative;
import org.cocktail.fwkcktldepense.client.metier.EOExercice;
import org.cocktail.fwkcktldepense.client.metier.EOOrgan;
import org.cocktail.fwkcktldepense.client.metier.EOOrganExercice;
import org.cocktail.fwkcktldepense.client.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;


public final class FinderOrgan extends Finder {

	/**
	 * @param ec
	 * @param structureUlr
	 *          La structure de rattachement
	 * @return Un tableau contenant toutes les organ rattaches a la structure
	 *         (ainsi que les organ enfants sans structure specifiee).
	 */
	public static NSArray fetchOrgansForStructure(EOEditingContext ec,
			String cStructure, EOExercice exercice) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOOrgan.C_STRUCTURE_KEY + "=%@ and "+EOOrgan.ORGAN_EXERCICE_KEY+"."+EOOrganExercice.EXERCICE_KEY+"=%@", 
				new NSArray(new Object[] { cStructure, exercice }));
		// recuperer tous les organ directement rattaches a la structure
		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME,qual, null);
		NSArray res1 = ec.objectsWithFetchSpecification(fs);
		NSMutableArray res = new NSMutableArray();
		// recuperer tous les enfants
		for (int i = 0; i < res1.count(); i++) {
			res.addObjectsFromArray(getAllOrganFilsSansStructure((EOOrgan) res1.objectAtIndex(i)));
		}
		return res;
	}

	/**
	 * 
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif) sans structure
	 *         specifiee.
	 */
	public static NSArray getAllOrganFilsSansStructure(EOOrgan organ) {
		NSMutableArray res = new NSMutableArray();
		Enumeration enumeration = organ.organFils().objectEnumerator();
		while (enumeration.hasMoreElements()) {
			final EOOrgan object = (EOOrgan) enumeration.nextElement();
			if (object.cStructure() == null || NSKeyValueCoding.NullValue.equals(object.cStructure())) {
				res.addObject(object);
				res.addObjectsFromArray(getAllOrganFilsSansStructure(object));
			}
		}
		return res.immutableClone();
	}
	
	/* Recherche de lignes budgetaire par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, exercice, convention
	 */
    public static final NSArray getOrgansPourConventionEtExercice(EOEditingContext ed, NSDictionary bindings) {
    	if (bindings == null)
    		throw new FactoryException("le bindings 'exercice' est obligatoire");      			
    	if (bindings.objectForKey("exercice")==null)
    		throw new FactoryException("le bindings 'exercice' est obligatoire");      			
    	if (bindings.objectForKey("utilisateur")==null)
    		throw new FactoryException("le bindings 'utilisateur' est obligatoire");      			
    	if (bindings.objectForKey("convention")==null)
    		throw new FactoryException("le bindings 'convention' est obligatoire");      			
    	
    	NSArray	larray=EOUtilities.objectsWithFetchSpecificationAndBindings(ed,EOConventionNonLimitative.ENTITY_NAME,"Recherche",bindings);
    	NSArray resultats=(NSArray)larray.valueForKeyPath(EOConventionNonLimitative.ORGAN_KEY); 
    			
    	return EOSortOrdering.sortedArrayUsingKeyOrderArray(resultats, sort());
    }
    
	/**
	 * Recherche de lignes budgetaire par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, exercice, sourceLibelle
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param bindings
	 *        dictionnaire contenant les bindings
	 * @return
	 *        un NSArray contenant des EOOrgan
	 */
	
    public static final NSArray getOrgans(EOEditingContext ed, NSDictionary bindings) {
    	NSArray lesOrgans=new NSArray();
    	NSArray	larray=FinderOrgan.getUtilisateurOrgans(ed, bindings);
    	
    	if (larray != null && larray.count()>0) {
	    	// TODO Optimisation base de donnees
	    	lesOrgans=(NSArray)larray.valueForKeyPath(EOUtilisateurOrgan.ORGAN_KEY);
	    	
	    	if (bindings != null && bindings.objectForKey("sourceLibelle")!=null)
	    		lesOrgans=EOQualifier.filteredArrayWithQualifier(lesOrgans, 
	    				EOQualifier.qualifierWithQualifierFormat("sourceLibelle caseInsensitiveLike %@", 
	    						new NSArray(bindings.objectForKey("sourceLibelle"))));
//	    	if (lesOrgans != null && lesOrgans.count()>1) {
//	    		lesOrgans = Finder.tableauTrie(lesOrgans, sort());
//	    	}
    	}
    	return lesOrgans;    	
    }

    public static final NSArray getOrgans(EOEditingContext ed, EOUtilisateur utilisateur, EOExercice exercice) {
    	NSMutableDictionary bindings=new NSMutableDictionary();
    	bindings.setObjectForKey(utilisateur, "utilisateur");
    	bindings.setObjectForKey(exercice, "exercice");
    	return FinderOrgan.getOrgans(ed, bindings);
    }
    
    /**
	 * Recherche de droits pour un utilisateur sur les CR ou sous CR par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : utilisateur, exercice
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param bindings
	 *        dictionnaire contenant les bindings
	 * @return
	 *        un NSArray contenant des EOUtilisateurOrgan
     */
    public static final NSArray getUtilisateurOrgans(EOEditingContext ed, NSDictionary bindings) {
    	if (bindings == null)
    		throw new FactoryException("le bindings 'exercice' est obligatoire");      			
    	if (bindings.objectForKey("exercice")==null)
    		throw new FactoryException("le bindings 'exercice' est obligatoire");      			
    	if (bindings.objectForKey("utilisateur")==null)
    		throw new FactoryException("le bindings 'utilisateur' est obligatoire");      			

    	return EOUtilities.objectsWithFetchSpecificationAndBindings(ed,EOUtilisateurOrgan.ENTITY_NAME,"Recherche",bindings);
    }
    
    public static NSArray sort() {
    	NSMutableArray array=new NSMutableArray();
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
    	return array;
    }
}
