// _EOReimputationConvention.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReimputationConvention.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReimputationConvention extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_CONVENTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recoId";

	public static final String RECO_HT_SAISIE_KEY = "recoHtSaisie";
	public static final String RECO_MONTANT_BUDGETAIRE_KEY = "recoMontantBudgetaire";
	public static final String RECO_TTC_SAISIE_KEY = "recoTtcSaisie";
	public static final String RECO_TVA_SAISIE_KEY = "recoTvaSaisie";

// Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String RECO_ID_KEY = "recoId";
	public static final String REIM_ID_KEY = "reimId";

//Colonnes dans la base de donnees
	public static final String RECO_HT_SAISIE_COLKEY = "RECO_HT_SAISIE";
	public static final String RECO_MONTANT_BUDGETAIRE_COLKEY = "RECO_MONTANT_BUDGETAIRE";
	public static final String RECO_TTC_SAISIE_COLKEY = "RECO_TTC_SAISIE";
	public static final String RECO_TVA_SAISIE_COLKEY = "RECO_TVA_SAISIE";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String RECO_ID_COLKEY = "RECO_ID";
	public static final String REIM_ID_COLKEY = "REIM_ID";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String REIMPUTATION_KEY = "reimputation";



	// Accessors methods
  public java.math.BigDecimal recoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RECO_HT_SAISIE_KEY);
  }

  public void setRecoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RECO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal recoMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(RECO_MONTANT_BUDGETAIRE_KEY);
  }

  public void setRecoMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RECO_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal recoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RECO_TTC_SAISIE_KEY);
  }

  public void setRecoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RECO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal recoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RECO_TVA_SAISIE_KEY);
  }

  public void setRecoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RECO_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOConvention convention() {
    return (org.cocktail.fwkcktldepense.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.fwkcktldepense.client.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation() {
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
  }

  public void setReimputationRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOReimputation oldValue = reimputation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
    }
  }
  

  public static EOReimputationConvention createFwkCaramboleReimputationConvention(EOEditingContext editingContext, java.math.BigDecimal recoHtSaisie
, java.math.BigDecimal recoMontantBudgetaire
, java.math.BigDecimal recoTtcSaisie
, java.math.BigDecimal recoTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOConvention convention, org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation) {
    EOReimputationConvention eo = (EOReimputationConvention) createAndInsertInstance(editingContext, _EOReimputationConvention.ENTITY_NAME);    
		eo.setRecoHtSaisie(recoHtSaisie);
		eo.setRecoMontantBudgetaire(recoMontantBudgetaire);
		eo.setRecoTtcSaisie(recoTtcSaisie);
		eo.setRecoTvaSaisie(recoTvaSaisie);
    eo.setConventionRelationship(convention);
    eo.setReimputationRelationship(reimputation);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOReimputationConvention.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOReimputationConvention.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOReimputationConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReimputationConvention)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOReimputationConvention localInstanceIn(EOEditingContext editingContext, EOReimputationConvention eo) {
    EOReimputationConvention localInstance = (eo == null) ? null : (EOReimputationConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOReimputationConvention#localInstanceIn a la place.
   */
	public static EOReimputationConvention localInstanceOf(EOEditingContext editingContext, EOReimputationConvention eo) {
		return EOReimputationConvention.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOReimputationConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOReimputationConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOReimputationConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
