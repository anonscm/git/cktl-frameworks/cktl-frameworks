// _EOEngagementControleConvention.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngagementControleConvention.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngagementControleConvention extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControleConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_CONVENTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "econId";

	public static final String ECON_DATE_SAISIE_KEY = "econDateSaisie";
	public static final String ECON_HT_SAISIE_KEY = "econHtSaisie";
	public static final String ECON_MONTANT_BUDGETAIRE_KEY = "econMontantBudgetaire";
	public static final String ECON_MONTANT_BUDGETAIRE_RESTE_KEY = "econMontantBudgetaireReste";
	public static final String ECON_TTC_SAISIE_KEY = "econTtcSaisie";
	public static final String ECON_TVA_SAISIE_KEY = "econTvaSaisie";

// Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String ECON_ID_KEY = "econId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String ECON_DATE_SAISIE_COLKEY = "ECON_DATE_SAISIE";
	public static final String ECON_HT_SAISIE_COLKEY = "ECON_HT_SAISIE";
	public static final String ECON_MONTANT_BUDGETAIRE_COLKEY = "ECON_MONTANT_BUDGETAIRE";
	public static final String ECON_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ECON_MONTANT_BUDGETAIRE_RESTE";
	public static final String ECON_TTC_SAISIE_COLKEY = "ECON_TTC_SAISIE";
	public static final String ECON_TVA_SAISIE_COLKEY = "ECON_TVA_SAISIE";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String ECON_ID_COLKEY = "ECON_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public NSTimestamp econDateSaisie() {
    return (NSTimestamp) storedValueForKey(ECON_DATE_SAISIE_KEY);
  }

  public void setEconDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ECON_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal econHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ECON_HT_SAISIE_KEY);
  }

  public void setEconHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal econMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(ECON_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEconMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal econMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(ECON_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEconMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal econTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ECON_TTC_SAISIE_KEY);
  }

  public void setEconTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal econTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ECON_TVA_SAISIE_KEY);
  }

  public void setEconTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECON_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOConvention convention() {
    return (org.cocktail.fwkcktldepense.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.fwkcktldepense.client.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
  }

  public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget oldValue = engagementBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

  public static EOEngagementControleConvention createFwkCaramboleEngagementControleConvention(EOEditingContext editingContext, NSTimestamp econDateSaisie
, java.math.BigDecimal econHtSaisie
, java.math.BigDecimal econMontantBudgetaire
, java.math.BigDecimal econMontantBudgetaireReste
, java.math.BigDecimal econTtcSaisie
, java.math.BigDecimal econTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOConvention convention, org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice) {
    EOEngagementControleConvention eo = (EOEngagementControleConvention) createAndInsertInstance(editingContext, _EOEngagementControleConvention.ENTITY_NAME);    
		eo.setEconDateSaisie(econDateSaisie);
		eo.setEconHtSaisie(econHtSaisie);
		eo.setEconMontantBudgetaire(econMontantBudgetaire);
		eo.setEconMontantBudgetaireReste(econMontantBudgetaireReste);
		eo.setEconTtcSaisie(econTtcSaisie);
		eo.setEconTvaSaisie(econTvaSaisie);
    eo.setConventionRelationship(convention);
    eo.setEngagementBudgetRelationship(engagementBudget);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngagementControleConvention.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngagementControleConvention.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEngagementControleConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngagementControleConvention)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEngagementControleConvention localInstanceIn(EOEditingContext editingContext, EOEngagementControleConvention eo) {
    EOEngagementControleConvention localInstance = (eo == null) ? null : (EOEngagementControleConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngagementControleConvention#localInstanceIn a la place.
   */
	public static EOEngagementControleConvention localInstanceOf(EOEditingContext editingContext, EOEngagementControleConvention eo) {
		return EOEngagementControleConvention.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEngagementControleConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngagementControleConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControleConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControleConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControleConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControleConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControleConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControleConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngagementControleConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControleConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControleConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControleConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
