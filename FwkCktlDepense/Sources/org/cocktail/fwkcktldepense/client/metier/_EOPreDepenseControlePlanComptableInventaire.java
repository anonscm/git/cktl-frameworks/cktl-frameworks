// _EOPreDepenseControlePlanComptableInventaire.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreDepenseControlePlanComptableInventaire.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPreDepenseControlePlanComptableInventaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseControlePlanComptableInventaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdpinId";

	public static final String PDPIN_MONTANT_BUDGETAIRE_KEY = "pdpinMontantBudgetaire";

// Attributs non visibles
	public static final String INVC_ID_KEY = "invcId";
	public static final String PDPCO_ID_KEY = "pdpcoId";
	public static final String PDPIN_ID_KEY = "pdpinId";

//Colonnes dans la base de donnees
	public static final String PDPIN_MONTANT_BUDGETAIRE_COLKEY = "PDPIN_MONTANT_BUDGETAIRE";

	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String PDPCO_ID_COLKEY = "PDPCO_ID";
	public static final String PDPIN_ID_COLKEY = "PDPIN_ID";


	// Relationships
	public static final String INVENTAIRE_KEY = "inventaire";
	public static final String PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY = "preDepenseControlePlanComptable";



	// Accessors methods
  public java.math.BigDecimal pdpinMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(PDPIN_MONTANT_BUDGETAIRE_KEY);
  }

  public void setPdpinMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PDPIN_MONTANT_BUDGETAIRE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOInventaire inventaire() {
    return (org.cocktail.fwkcktldepense.client.metier.EOInventaire)storedValueForKey(INVENTAIRE_KEY);
  }

  public void setInventaireRelationship(org.cocktail.fwkcktldepense.client.metier.EOInventaire value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOInventaire oldValue = inventaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INVENTAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INVENTAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable preDepenseControlePlanComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable)storedValueForKey(PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
  }

  public void setPreDepenseControlePlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable oldValue = preDepenseControlePlanComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
    }
  }
  

  public static EOPreDepenseControlePlanComptableInventaire createFwkCarambolePreDepenseControlePlanComptableInventaire(EOEditingContext editingContext, java.math.BigDecimal pdpinMontantBudgetaire
, org.cocktail.fwkcktldepense.client.metier.EOInventaire inventaire, org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable preDepenseControlePlanComptable) {
    EOPreDepenseControlePlanComptableInventaire eo = (EOPreDepenseControlePlanComptableInventaire) createAndInsertInstance(editingContext, _EOPreDepenseControlePlanComptableInventaire.ENTITY_NAME);    
		eo.setPdpinMontantBudgetaire(pdpinMontantBudgetaire);
    eo.setInventaireRelationship(inventaire);
    eo.setPreDepenseControlePlanComptableRelationship(preDepenseControlePlanComptable);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPreDepenseControlePlanComptableInventaire.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPreDepenseControlePlanComptableInventaire.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPreDepenseControlePlanComptableInventaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreDepenseControlePlanComptableInventaire)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPreDepenseControlePlanComptableInventaire localInstanceIn(EOEditingContext editingContext, EOPreDepenseControlePlanComptableInventaire eo) {
    EOPreDepenseControlePlanComptableInventaire localInstance = (eo == null) ? null : (EOPreDepenseControlePlanComptableInventaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPreDepenseControlePlanComptableInventaire#localInstanceIn a la place.
   */
	public static EOPreDepenseControlePlanComptableInventaire localInstanceOf(EOEditingContext editingContext, EOPreDepenseControlePlanComptableInventaire eo) {
		return EOPreDepenseControlePlanComptableInventaire.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPreDepenseControlePlanComptableInventaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPreDepenseControlePlanComptableInventaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseControlePlanComptableInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseControlePlanComptableInventaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseControlePlanComptableInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseControlePlanComptableInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseControlePlanComptableInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseControlePlanComptableInventaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPreDepenseControlePlanComptableInventaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseControlePlanComptableInventaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseControlePlanComptableInventaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseControlePlanComptableInventaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
