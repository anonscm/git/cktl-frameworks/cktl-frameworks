// _EOStructure.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructure.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOStructure extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleStructureUlr";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_STRUCTURE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String PERSONNE_KEY = "personne";
	public static final String REPART_STRUCTURE_KEY = "repartStructure";



	// Accessors methods
  public String cTypeStructure() {
    return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
  }

  public void setCTypeStructure(String value) {
    takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPersonne personne() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.fwkcktldepense.client.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public NSArray repartStructure() {
    return (NSArray)storedValueForKey(REPART_STRUCTURE_KEY);
  }

  public NSArray repartStructure(EOQualifier qualifier) {
    return repartStructure(qualifier, null, false);
  }

  public NSArray repartStructure(EOQualifier qualifier, boolean fetch) {
    return repartStructure(qualifier, null, fetch);
  }

  public NSArray repartStructure(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EORepartStructure.STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartStructureRelationship(org.cocktail.fwkcktldepense.client.metier.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURE_KEY);
  }

  public void removeFromRepartStructureRelationship(org.cocktail.fwkcktldepense.client.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EORepartStructure createRepartStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleRepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_STRUCTURE_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EORepartStructure) eo;
  }

  public void deleteRepartStructureRelationship(org.cocktail.fwkcktldepense.client.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartStructureRelationships() {
    Enumeration objects = repartStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartStructureRelationship((org.cocktail.fwkcktldepense.client.metier.EORepartStructure)objects.nextElement());
    }
  }


  public static EOStructure createFwkCaramboleStructureUlr(EOEditingContext editingContext, String cTypeStructure
, org.cocktail.fwkcktldepense.client.metier.EOPersonne personne) {
    EOStructure eo = (EOStructure) createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);    
		eo.setCTypeStructure(cTypeStructure);
    eo.setPersonneRelationship(personne);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOStructure.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOStructure.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOStructure localInstanceIn(EOEditingContext editingContext) {
	  		return (EOStructure)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOStructure localInstanceIn(EOEditingContext editingContext, EOStructure eo) {
    EOStructure localInstance = (eo == null) ? null : (EOStructure)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOStructure#localInstanceIn a la place.
   */
	public static EOStructure localInstanceOf(EOEditingContext editingContext, EOStructure eo) {
		return EOStructure.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOStructure fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOStructure fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOStructure fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructure eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructure ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructure fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
