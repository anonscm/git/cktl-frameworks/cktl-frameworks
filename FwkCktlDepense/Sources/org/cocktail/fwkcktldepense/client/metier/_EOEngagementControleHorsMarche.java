// _EOEngagementControleHorsMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngagementControleHorsMarche.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngagementControleHorsMarche extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControleHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_HORS_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ehomId";

	public static final String EHOM_DATE_SAISIE_KEY = "ehomDateSaisie";
	public static final String EHOM_HT_RESTE_KEY = "ehomHtReste";
	public static final String EHOM_HT_SAISIE_KEY = "ehomHtSaisie";
	public static final String EHOM_MONTANT_BUDGETAIRE_KEY = "ehomMontantBudgetaire";
	public static final String EHOM_MONTANT_BUDGETAIRE_RESTE_KEY = "ehomMontantBudgetaireReste";
	public static final String EHOM_TTC_SAISIE_KEY = "ehomTtcSaisie";
	public static final String EHOM_TVA_SAISIE_KEY = "ehomTvaSaisie";

// Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String EHOM_ID_KEY = "ehomId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String EHOM_DATE_SAISIE_COLKEY = "EHOM_DATE_SAISIE";
	public static final String EHOM_HT_RESTE_COLKEY = "EHOM_HT_RESTE";
	public static final String EHOM_HT_SAISIE_COLKEY = "EHOM_HT_SAISIE";
	public static final String EHOM_MONTANT_BUDGETAIRE_COLKEY = "EHOM_MONTANT_BUDGETAIRE";
	public static final String EHOM_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EHOM_MONTANT_BUDGETAIRE_RESTE";
	public static final String EHOM_TTC_SAISIE_COLKEY = "EHOM_TTC_SAISIE";
	public static final String EHOM_TVA_SAISIE_COLKEY = "EHOM_TVA_SAISIE";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String EHOM_ID_COLKEY = "EHOM_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
  public NSTimestamp ehomDateSaisie() {
    return (NSTimestamp) storedValueForKey(EHOM_DATE_SAISIE_KEY);
  }

  public void setEhomDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, EHOM_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal ehomHtReste() {
    return (java.math.BigDecimal) storedValueForKey(EHOM_HT_RESTE_KEY);
  }

  public void setEhomHtReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EHOM_HT_RESTE_KEY);
  }

  public java.math.BigDecimal ehomHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EHOM_HT_SAISIE_KEY);
  }

  public void setEhomHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EHOM_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal ehomMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(EHOM_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEhomMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EHOM_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal ehomMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(EHOM_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEhomMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EHOM_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal ehomTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EHOM_TTC_SAISIE_KEY);
  }

  public void setEhomTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EHOM_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal ehomTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EHOM_TVA_SAISIE_KEY);
  }

  public void setEhomTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EHOM_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
  }

  public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget oldValue = engagementBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
  }

  public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeAchat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeAchat oldValue = typeAchat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
    }
  }
  

  public static EOEngagementControleHorsMarche createFwkCaramboleEngagementControleHorsMarche(EOEditingContext editingContext, NSTimestamp ehomDateSaisie
, java.math.BigDecimal ehomHtReste
, java.math.BigDecimal ehomHtSaisie
, java.math.BigDecimal ehomMontantBudgetaire
, java.math.BigDecimal ehomMontantBudgetaireReste
, java.math.BigDecimal ehomTtcSaisie
, java.math.BigDecimal ehomTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer, org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat) {
    EOEngagementControleHorsMarche eo = (EOEngagementControleHorsMarche) createAndInsertInstance(editingContext, _EOEngagementControleHorsMarche.ENTITY_NAME);    
		eo.setEhomDateSaisie(ehomDateSaisie);
		eo.setEhomHtReste(ehomHtReste);
		eo.setEhomHtSaisie(ehomHtSaisie);
		eo.setEhomMontantBudgetaire(ehomMontantBudgetaire);
		eo.setEhomMontantBudgetaireReste(ehomMontantBudgetaireReste);
		eo.setEhomTtcSaisie(ehomTtcSaisie);
		eo.setEhomTvaSaisie(ehomTvaSaisie);
    eo.setCodeExerRelationship(codeExer);
    eo.setEngagementBudgetRelationship(engagementBudget);
    eo.setExerciceRelationship(exercice);
    eo.setTypeAchatRelationship(typeAchat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngagementControleHorsMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngagementControleHorsMarche.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEngagementControleHorsMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngagementControleHorsMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEngagementControleHorsMarche localInstanceIn(EOEditingContext editingContext, EOEngagementControleHorsMarche eo) {
    EOEngagementControleHorsMarche localInstance = (eo == null) ? null : (EOEngagementControleHorsMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngagementControleHorsMarche#localInstanceIn a la place.
   */
	public static EOEngagementControleHorsMarche localInstanceOf(EOEditingContext editingContext, EOEngagementControleHorsMarche eo) {
		return EOEngagementControleHorsMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEngagementControleHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngagementControleHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngagementControleHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControleHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControleHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControleHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
