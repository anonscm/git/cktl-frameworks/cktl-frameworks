// _EOSaCommande.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSaCommande.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOSaCommande extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleSaCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_SA_COMMANDE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vcloId";

	public static final String SA_EMAILS_KEY = "saEmails";
	public static final String SA_LIBELLE_KEY = "saLibelle";
	public static final String VLCO_ETAT_KEY = "vlcoEtat";
	public static final String VLCO_VALIDE_KEY = "vlcoValide";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String VCLO_ID_KEY = "vcloId";

//Colonnes dans la base de donnees
	public static final String SA_EMAILS_COLKEY = "SA_EMAILS";
	public static final String SA_LIBELLE_COLKEY = "SA_LIBELLE";
	public static final String VLCO_ETAT_COLKEY = "VLCO_ETAT";
	public static final String VLCO_VALIDE_COLKEY = "VLCO_VALIDE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String VCLO_ID_COLKEY = "VCLO_ID";


	// Relationships
	public static final String COMMANDE_KEY = "commande";



	// Accessors methods
  public String saEmails() {
    return (String) storedValueForKey(SA_EMAILS_KEY);
  }

  public void setSaEmails(String value) {
    takeStoredValueForKey(value, SA_EMAILS_KEY);
  }

  public String saLibelle() {
    return (String) storedValueForKey(SA_LIBELLE_KEY);
  }

  public void setSaLibelle(String value) {
    takeStoredValueForKey(value, SA_LIBELLE_KEY);
  }

  public String vlcoEtat() {
    return (String) storedValueForKey(VLCO_ETAT_KEY);
  }

  public void setVlcoEtat(String value) {
    takeStoredValueForKey(value, VLCO_ETAT_KEY);
  }

  public String vlcoValide() {
    return (String) storedValueForKey(VLCO_VALIDE_KEY);
  }

  public void setVlcoValide(String value) {
    takeStoredValueForKey(value, VLCO_VALIDE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommande commande() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
  }

  public void setCommandeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommande oldValue = commande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
    }
  }
  

  public static EOSaCommande createFwkCaramboleSaCommande(EOEditingContext editingContext, String saLibelle
, String vlcoEtat
, String vlcoValide
, org.cocktail.fwkcktldepense.client.metier.EOCommande commande) {
    EOSaCommande eo = (EOSaCommande) createAndInsertInstance(editingContext, _EOSaCommande.ENTITY_NAME);    
		eo.setSaLibelle(saLibelle);
		eo.setVlcoEtat(vlcoEtat);
		eo.setVlcoValide(vlcoValide);
    eo.setCommandeRelationship(commande);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOSaCommande.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOSaCommande.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOSaCommande localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSaCommande)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOSaCommande localInstanceIn(EOEditingContext editingContext, EOSaCommande eo) {
    EOSaCommande localInstance = (eo == null) ? null : (EOSaCommande)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOSaCommande#localInstanceIn a la place.
   */
	public static EOSaCommande localInstanceOf(EOEditingContext editingContext, EOSaCommande eo) {
		return EOSaCommande.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOSaCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSaCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSaCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSaCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSaCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSaCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSaCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSaCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOSaCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSaCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSaCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSaCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
