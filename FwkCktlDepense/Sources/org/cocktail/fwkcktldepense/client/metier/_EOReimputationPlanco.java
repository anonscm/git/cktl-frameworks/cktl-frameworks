// _EOReimputationPlanco.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReimputationPlanco.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReimputationPlanco extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationPlanco";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_PLANCO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "repcId";

	public static final String REPC_HT_SAISIE_KEY = "repcHtSaisie";
	public static final String REPC_MONTANT_BUDGETAIRE_KEY = "repcMontantBudgetaire";
	public static final String REPC_TTC_SAISIE_KEY = "repcTtcSaisie";
	public static final String REPC_TVA_SAISIE_KEY = "repcTvaSaisie";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String REIM_ID_KEY = "reimId";
	public static final String REPC_ID_KEY = "repcId";

//Colonnes dans la base de donnees
	public static final String REPC_HT_SAISIE_COLKEY = "REPC_HT_SAISIE";
	public static final String REPC_MONTANT_BUDGETAIRE_COLKEY = "REPC_MONTANT_BUDGETAIRE";
	public static final String REPC_TTC_SAISIE_COLKEY = "REPC_TTC_SAISIE";
	public static final String REPC_TVA_SAISIE_COLKEY = "REPC_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String REPC_ID_COLKEY = "REPC_ID";


	// Relationships
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String REIMPUTATION_KEY = "reimputation";



	// Accessors methods
  public java.math.BigDecimal repcHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REPC_HT_SAISIE_KEY);
  }

  public void setRepcHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REPC_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal repcMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(REPC_MONTANT_BUDGETAIRE_KEY);
  }

  public void setRepcMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REPC_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal repcTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REPC_TTC_SAISIE_KEY);
  }

  public void setRepcTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REPC_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal repcTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REPC_TVA_SAISIE_KEY);
  }

  public void setRepcTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REPC_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation() {
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
  }

  public void setReimputationRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOReimputation oldValue = reimputation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
    }
  }
  

  public static EOReimputationPlanco createFwkCaramboleReimputationPlanco(EOEditingContext editingContext, java.math.BigDecimal repcHtSaisie
, java.math.BigDecimal repcMontantBudgetaire
, java.math.BigDecimal repcTtcSaisie
, java.math.BigDecimal repcTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable, org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation) {
    EOReimputationPlanco eo = (EOReimputationPlanco) createAndInsertInstance(editingContext, _EOReimputationPlanco.ENTITY_NAME);    
		eo.setRepcHtSaisie(repcHtSaisie);
		eo.setRepcMontantBudgetaire(repcMontantBudgetaire);
		eo.setRepcTtcSaisie(repcTtcSaisie);
		eo.setRepcTvaSaisie(repcTvaSaisie);
    eo.setPlanComptableRelationship(planComptable);
    eo.setReimputationRelationship(reimputation);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOReimputationPlanco.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOReimputationPlanco.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOReimputationPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReimputationPlanco)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOReimputationPlanco localInstanceIn(EOEditingContext editingContext, EOReimputationPlanco eo) {
    EOReimputationPlanco localInstance = (eo == null) ? null : (EOReimputationPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOReimputationPlanco#localInstanceIn a la place.
   */
	public static EOReimputationPlanco localInstanceOf(EOEditingContext editingContext, EOReimputationPlanco eo) {
		return EOReimputationPlanco.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOReimputationPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOReimputationPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOReimputationPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
