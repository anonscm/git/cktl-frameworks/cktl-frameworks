// _EOCommandeControlePlanComptable.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommandeControlePlanComptable.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCommandeControlePlanComptable extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeControlePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_CTRL_PLANCO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cpcoId";

	public static final String CPCO_HT_SAISIE_KEY = "cpcoHtSaisie";
	public static final String CPCO_MONTANT_BUDGETAIRE_KEY = "cpcoMontantBudgetaire";
	public static final String CPCO_POURCENTAGE_KEY = "cpcoPourcentage";
	public static final String CPCO_TTC_SAISIE_KEY = "cpcoTtcSaisie";
	public static final String CPCO_TVA_SAISIE_KEY = "cpcoTvaSaisie";

// Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String CPCO_ID_KEY = "cpcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String CPCO_HT_SAISIE_COLKEY = "CPCO_HT_SAISIE";
	public static final String CPCO_MONTANT_BUDGETAIRE_COLKEY = "CPCO_MONTANT_BUDGETAIRE";
	public static final String CPCO_POURCENTAGE_COLKEY = "CPCO_POURCENTAGE";
	public static final String CPCO_TTC_SAISIE_COLKEY = "CPCO_TTC_SAISIE";
	public static final String CPCO_TVA_SAISIE_COLKEY = "CPCO_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String CPCO_ID_COLKEY = "CPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String COMMANDE_BUDGET_KEY = "commandeBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
  public java.math.BigDecimal cpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CPCO_HT_SAISIE_KEY);
  }

  public void setCpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal cpcoMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(CPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public void setCpcoMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal cpcoPourcentage() {
    return (java.math.BigDecimal) storedValueForKey(CPCO_POURCENTAGE_KEY);
  }

  public void setCpcoPourcentage(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CPCO_POURCENTAGE_KEY);
  }

  public java.math.BigDecimal cpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CPCO_TTC_SAISIE_KEY);
  }

  public void setCpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal cpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CPCO_TVA_SAISIE_KEY);
  }

  public void setCpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CPCO_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget commandeBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget)storedValueForKey(COMMANDE_BUDGET_KEY);
  }

  public void setCommandeBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget oldValue = commandeBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

  public static EOCommandeControlePlanComptable createFwkCaramboleCommandeControlePlanComptable(EOEditingContext editingContext, java.math.BigDecimal cpcoHtSaisie
, java.math.BigDecimal cpcoMontantBudgetaire
, java.math.BigDecimal cpcoPourcentage
, java.math.BigDecimal cpcoTtcSaisie
, java.math.BigDecimal cpcoTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget commandeBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable) {
    EOCommandeControlePlanComptable eo = (EOCommandeControlePlanComptable) createAndInsertInstance(editingContext, _EOCommandeControlePlanComptable.ENTITY_NAME);    
		eo.setCpcoHtSaisie(cpcoHtSaisie);
		eo.setCpcoMontantBudgetaire(cpcoMontantBudgetaire);
		eo.setCpcoPourcentage(cpcoPourcentage);
		eo.setCpcoTtcSaisie(cpcoTtcSaisie);
		eo.setCpcoTvaSaisie(cpcoTvaSaisie);
    eo.setCommandeBudgetRelationship(commandeBudget);
    eo.setExerciceRelationship(exercice);
    eo.setPlanComptableRelationship(planComptable);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCommandeControlePlanComptable.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCommandeControlePlanComptable.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCommandeControlePlanComptable localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommandeControlePlanComptable)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCommandeControlePlanComptable localInstanceIn(EOEditingContext editingContext, EOCommandeControlePlanComptable eo) {
    EOCommandeControlePlanComptable localInstance = (eo == null) ? null : (EOCommandeControlePlanComptable)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCommandeControlePlanComptable#localInstanceIn a la place.
   */
	public static EOCommandeControlePlanComptable localInstanceOf(EOEditingContext editingContext, EOCommandeControlePlanComptable eo) {
		return EOCommandeControlePlanComptable.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCommandeControlePlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCommandeControlePlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCommandeControlePlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeControlePlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeControlePlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeControlePlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
