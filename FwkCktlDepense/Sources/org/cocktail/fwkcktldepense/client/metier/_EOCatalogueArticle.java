// _EOCatalogueArticle.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCatalogueArticle.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCatalogueArticle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCatalogueArticle";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CATALOGUE_ARTICLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "caarId";

	public static final String CAAR_LIBELLE_KEY = "caarLibelle";
	public static final String CAAR_PRIX_HT_KEY = "caarPrixHt";
	public static final String CAAR_PRIX_TTC_KEY = "caarPrixTtc";
	public static final String CAAR_REFERENCE_KEY = "caarReference";
	public static final String CAT_DATE_DEBUT_KEY = "catDateDebut";
	public static final String CAT_DATE_FIN_KEY = "catDateFin";
	public static final String CAT_LIBELLE_KEY = "catLibelle";

// Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String CAAR_ID_KEY = "caarId";
	public static final String CAAR_ID_PERE_KEY = "caarIdPere";
	public static final String CAT_ID_KEY = "catId";
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String TYAR_ID_KEY = "tyarId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CAAR_LIBELLE_COLKEY = "CAAR_LIBELLE";
	public static final String CAAR_PRIX_HT_COLKEY = "CAAR_PRIX_HT";
	public static final String CAAR_PRIX_TTC_COLKEY = "CAAR_PRIX_TTC";
	public static final String CAAR_REFERENCE_COLKEY = "CAAR_REFERENCE";
	public static final String CAT_DATE_DEBUT_COLKEY = "CAT_DATE_DEBUT";
	public static final String CAT_DATE_FIN_COLKEY = "CAT_DATE_FIN";
	public static final String CAT_LIBELLE_COLKEY = "CAT_LIBELLE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String CAAR_ID_COLKEY = "CAAR_ID";
	public static final String CAAR_ID_PERE_COLKEY = "CAAR_ID_PERE";
	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String TYAR_ID_COLKEY = "TYAR_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String CATALOGUE_ARTICLE_PERE_KEY = "catalogueArticlePere";
	public static final String CODE_MARCHE_KEY = "codeMarche";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String TVA_KEY = "tva";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_ARTICLE_KEY = "typeArticle";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public String caarLibelle() {
    return (String) storedValueForKey(CAAR_LIBELLE_KEY);
  }

  public void setCaarLibelle(String value) {
    takeStoredValueForKey(value, CAAR_LIBELLE_KEY);
  }

  public java.math.BigDecimal caarPrixHt() {
    return (java.math.BigDecimal) storedValueForKey(CAAR_PRIX_HT_KEY);
  }

  public void setCaarPrixHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAAR_PRIX_HT_KEY);
  }

  public java.math.BigDecimal caarPrixTtc() {
    return (java.math.BigDecimal) storedValueForKey(CAAR_PRIX_TTC_KEY);
  }

  public void setCaarPrixTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAAR_PRIX_TTC_KEY);
  }

  public String caarReference() {
    return (String) storedValueForKey(CAAR_REFERENCE_KEY);
  }

  public void setCaarReference(String value) {
    takeStoredValueForKey(value, CAAR_REFERENCE_KEY);
  }

  public NSTimestamp catDateDebut() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_DEBUT_KEY);
  }

  public void setCatDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_DEBUT_KEY);
  }

  public NSTimestamp catDateFin() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_FIN_KEY);
  }

  public void setCatDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_FIN_KEY);
  }

  public String catLibelle() {
    return (String) storedValueForKey(CAT_LIBELLE_KEY);
  }

  public void setCatLibelle(String value) {
    takeStoredValueForKey(value, CAT_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOAttribution attribution() {
    return (org.cocktail.fwkcktldepense.client.metier.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttributionRelationship(org.cocktail.fwkcktldepense.client.metier.EOAttribution value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOAttribution oldValue = attribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle catalogueArticlePere() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle)storedValueForKey(CATALOGUE_ARTICLE_PERE_KEY);
  }

  public void setCatalogueArticlePereRelationship(org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle oldValue = catalogueArticlePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_ARTICLE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_ARTICLE_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCodeMarche codeMarche() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
  }

  public void setCodeMarcheRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeMarche value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeMarche oldValue = codeMarche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.fwkcktldepense.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTva tva() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTva)storedValueForKey(TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.fwkcktldepense.client.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTva oldValue = tva();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeArticle typeArticle() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
  }

  public void setTypeArticleRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeArticle value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeArticle oldValue = typeArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  

  public static EOCatalogueArticle createFwkCaramboleCatalogueArticle(EOEditingContext editingContext, String caarLibelle
, java.math.BigDecimal caarPrixHt
, java.math.BigDecimal caarPrixTtc
, NSTimestamp catDateDebut
, String catLibelle
, org.cocktail.fwkcktldepense.client.metier.EOCodeMarche codeMarche, org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur, org.cocktail.fwkcktldepense.client.metier.EOTva tva, org.cocktail.fwkcktldepense.client.metier.EOTypeApplication typeApplication, org.cocktail.fwkcktldepense.client.metier.EOTypeArticle typeArticle, org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtat) {
    EOCatalogueArticle eo = (EOCatalogueArticle) createAndInsertInstance(editingContext, _EOCatalogueArticle.ENTITY_NAME);    
		eo.setCaarLibelle(caarLibelle);
		eo.setCaarPrixHt(caarPrixHt);
		eo.setCaarPrixTtc(caarPrixTtc);
		eo.setCatDateDebut(catDateDebut);
		eo.setCatLibelle(catLibelle);
    eo.setCodeMarcheRelationship(codeMarche);
    eo.setFournisseurRelationship(fournisseur);
    eo.setTvaRelationship(tva);
    eo.setTypeApplicationRelationship(typeApplication);
    eo.setTypeArticleRelationship(typeArticle);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCatalogueArticle.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCatalogueArticle.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCatalogueArticle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCatalogueArticle)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCatalogueArticle localInstanceIn(EOEditingContext editingContext, EOCatalogueArticle eo) {
    EOCatalogueArticle localInstance = (eo == null) ? null : (EOCatalogueArticle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCatalogueArticle#localInstanceIn a la place.
   */
	public static EOCatalogueArticle localInstanceOf(EOEditingContext editingContext, EOCatalogueArticle eo) {
		return EOCatalogueArticle.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCatalogueArticle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCatalogueArticle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCatalogueArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCatalogueArticle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCatalogueArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCatalogueArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCatalogueArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCatalogueArticle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCatalogueArticle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCatalogueArticle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCatalogueArticle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCatalogueArticle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
