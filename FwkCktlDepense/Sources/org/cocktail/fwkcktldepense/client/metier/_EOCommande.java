// _EOCommande.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommande.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCommande extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "commId";

	public static final String COMM_DATE_KEY = "commDate";
	public static final String COMM_DATE_CREATION_KEY = "commDateCreation";
	public static final String COMM_LIBELLE_KEY = "commLibelle";
	public static final String COMM_NUMERO_KEY = "commNumero";
	public static final String COMM_REFERENCE_KEY = "commReference";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DEV_ID_KEY = "devId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYET_ID_IMPRIMABLE_KEY = "tyetIdImprimable";
	public static final String ULT_ORDRE_KEY = "ultOrdre";

//Colonnes dans la base de donnees
	public static final String COMM_DATE_COLKEY = "COMM_DATE";
	public static final String COMM_DATE_CREATION_COLKEY = "COMM_DATE_CREATION";
	public static final String COMM_LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String COMM_NUMERO_COLKEY = "COMM_NUMERO";
	public static final String COMM_REFERENCE_COLKEY = "COMM_REFERENCE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DEV_ID_COLKEY = "DEV_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYET_ID_IMPRIMABLE_COLKEY = "TYET_ID_IMPRIMABLE";
	public static final String ULT_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ARTICLES_KEY = "articles";
	public static final String COMMANDE_BASCULE_KEY = "commandeBascule";
	public static final String COMMANDE_BUDGETS_KEY = "commandeBudgets";
	public static final String COMMANDE_DOCUMENTS_KEY = "commandeDocuments";
	public static final String COMMANDE_ENGAGEMENTS_KEY = "commandeEngagements";
	public static final String COMMANDE_IMPRESSIONS_KEY = "commandeImpressions";
	public static final String COMMANDE_UTILISATEUR_KEY = "commandeUtilisateur";
	public static final String DEVISE_KEY = "devise";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String TO_B2B_CXML_ORDER_REQUESTS_KEY = "toB2bCxmlOrderRequests";
	public static final String TO_B2B_CXML_PUNCHOUTMSGS_KEY = "toB2bCxmlPunchoutmsgs";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_IMPRIMABLE_KEY = "typeEtatImprimable";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp commDate() {
    return (NSTimestamp) storedValueForKey(COMM_DATE_KEY);
  }

  public void setCommDate(NSTimestamp value) {
    takeStoredValueForKey(value, COMM_DATE_KEY);
  }

  public NSTimestamp commDateCreation() {
    return (NSTimestamp) storedValueForKey(COMM_DATE_CREATION_KEY);
  }

  public void setCommDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, COMM_DATE_CREATION_KEY);
  }

  public String commLibelle() {
    return (String) storedValueForKey(COMM_LIBELLE_KEY);
  }

  public void setCommLibelle(String value) {
    takeStoredValueForKey(value, COMM_LIBELLE_KEY);
  }

  public Integer commNumero() {
    return (Integer) storedValueForKey(COMM_NUMERO_KEY);
  }

  public void setCommNumero(Integer value) {
    takeStoredValueForKey(value, COMM_NUMERO_KEY);
  }

  public String commReference() {
    return (String) storedValueForKey(COMM_REFERENCE_KEY);
  }

  public void setCommReference(String value) {
    takeStoredValueForKey(value, COMM_REFERENCE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODevise devise() {
    return (org.cocktail.fwkcktldepense.client.metier.EODevise)storedValueForKey(DEVISE_KEY);
  }

  public void setDeviseRelationship(org.cocktail.fwkcktldepense.client.metier.EODevise value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODevise oldValue = devise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEVISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEVISE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.fwkcktldepense.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtatImprimable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_IMPRIMABLE_KEY);
  }

  public void setTypeEtatImprimableRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeEtat oldValue = typeEtatImprimable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_IMPRIMABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_IMPRIMABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray articles() {
    return (NSArray)storedValueForKey(ARTICLES_KEY);
  }

  public NSArray articles(EOQualifier qualifier) {
    return articles(qualifier, null, false);
  }

  public NSArray articles(EOQualifier qualifier, boolean fetch) {
    return articles(qualifier, null, fetch);
  }

  public NSArray articles(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOArticle.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOArticle.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = articles();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToArticlesRelationship(org.cocktail.fwkcktldepense.client.metier.EOArticle object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
  }

  public void removeFromArticlesRelationship(org.cocktail.fwkcktldepense.client.metier.EOArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOArticle createArticlesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleArticle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOArticle) eo;
  }

  public void deleteArticlesRelationship(org.cocktail.fwkcktldepense.client.metier.EOArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllArticlesRelationships() {
    Enumeration objects = articles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteArticlesRelationship((org.cocktail.fwkcktldepense.client.metier.EOArticle)objects.nextElement());
    }
  }

  public NSArray commandeBascule() {
    return (NSArray)storedValueForKey(COMMANDE_BASCULE_KEY);
  }

  public NSArray commandeBascule(EOQualifier qualifier) {
    return commandeBascule(qualifier, null, false);
  }

  public NSArray commandeBascule(EOQualifier qualifier, boolean fetch) {
    return commandeBascule(qualifier, null, fetch);
  }

  public NSArray commandeBascule(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule.COMMANDE_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeBascule();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeBasculeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_BASCULE_KEY);
  }

  public void removeFromCommandeBasculeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BASCULE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule createCommandeBasculeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeBascule");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_BASCULE_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule) eo;
  }

  public void deleteCommandeBasculeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BASCULE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeBasculeRelationships() {
    Enumeration objects = commandeBascule().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeBasculeRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeBascule)objects.nextElement());
    }
  }

  public NSArray commandeBudgets() {
    return (NSArray)storedValueForKey(COMMANDE_BUDGETS_KEY);
  }

  public NSArray commandeBudgets(EOQualifier qualifier) {
    return commandeBudgets(qualifier, null, false);
  }

  public NSArray commandeBudgets(EOQualifier qualifier, boolean fetch) {
    return commandeBudgets(qualifier, null, fetch);
  }

  public NSArray commandeBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_BUDGETS_KEY);
  }

  public void removeFromCommandeBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BUDGETS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget createCommandeBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_BUDGETS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget) eo;
  }

  public void deleteCommandeBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeBudgetsRelationships() {
    Enumeration objects = commandeBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeBudgetsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget)objects.nextElement());
    }
  }

  public NSArray commandeDocuments() {
    return (NSArray)storedValueForKey(COMMANDE_DOCUMENTS_KEY);
  }

  public NSArray commandeDocuments(EOQualifier qualifier) {
    return commandeDocuments(qualifier, null, false);
  }

  public NSArray commandeDocuments(EOQualifier qualifier, boolean fetch) {
    return commandeDocuments(qualifier, null, fetch);
  }

  public NSArray commandeDocuments(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeDocuments();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_DOCUMENTS_KEY);
  }

  public void removeFromCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_DOCUMENTS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument createCommandeDocumentsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeDocument");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_DOCUMENTS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument) eo;
  }

  public void deleteCommandeDocumentsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_DOCUMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeDocumentsRelationships() {
    Enumeration objects = commandeDocuments().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeDocumentsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeDocument)objects.nextElement());
    }
  }

  public NSArray commandeEngagements() {
    return (NSArray)storedValueForKey(COMMANDE_ENGAGEMENTS_KEY);
  }

  public NSArray commandeEngagements(EOQualifier qualifier) {
    return commandeEngagements(qualifier, null, false);
  }

  public NSArray commandeEngagements(EOQualifier qualifier, boolean fetch) {
    return commandeEngagements(qualifier, null, fetch);
  }

  public NSArray commandeEngagements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeEngagements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
  }

  public void removeFromCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement createCommandeEngagementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeEngagement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_ENGAGEMENTS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement) eo;
  }

  public void deleteCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeEngagementsRelationships() {
    Enumeration objects = commandeEngagements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeEngagementsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement)objects.nextElement());
    }
  }

  public NSArray commandeImpressions() {
    return (NSArray)storedValueForKey(COMMANDE_IMPRESSIONS_KEY);
  }

  public NSArray commandeImpressions(EOQualifier qualifier) {
    return commandeImpressions(qualifier, null, false);
  }

  public NSArray commandeImpressions(EOQualifier qualifier, boolean fetch) {
    return commandeImpressions(qualifier, null, fetch);
  }

  public NSArray commandeImpressions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeImpressions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeImpressionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_IMPRESSIONS_KEY);
  }

  public void removeFromCommandeImpressionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_IMPRESSIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression createCommandeImpressionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeImpression");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_IMPRESSIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression) eo;
  }

  public void deleteCommandeImpressionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_IMPRESSIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeImpressionsRelationships() {
    Enumeration objects = commandeImpressions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeImpressionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeImpression)objects.nextElement());
    }
  }

  public NSArray commandeUtilisateur() {
    return (NSArray)storedValueForKey(COMMANDE_UTILISATEUR_KEY);
  }

  public NSArray commandeUtilisateur(EOQualifier qualifier) {
    return commandeUtilisateur(qualifier, null, false);
  }

  public NSArray commandeUtilisateur(EOQualifier qualifier, boolean fetch) {
    return commandeUtilisateur(qualifier, null, fetch);
  }

  public NSArray commandeUtilisateur(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur.COMMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeUtilisateur();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_UTILISATEUR_KEY);
  }

  public void removeFromCommandeUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_UTILISATEUR_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur createCommandeUtilisateurRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeUtilisateur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_UTILISATEUR_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur) eo;
  }

  public void deleteCommandeUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_UTILISATEUR_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeUtilisateurRelationships() {
    Enumeration objects = commandeUtilisateur().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeUtilisateurRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeUtilisateur)objects.nextElement());
    }
  }

  public NSArray toB2bCxmlOrderRequests() {
    return (NSArray)storedValueForKey(TO_B2B_CXML_ORDER_REQUESTS_KEY);
  }

  public NSArray toB2bCxmlOrderRequests(EOQualifier qualifier) {
    return toB2bCxmlOrderRequests(qualifier, null, false);
  }

  public NSArray toB2bCxmlOrderRequests(EOQualifier qualifier, boolean fetch) {
    return toB2bCxmlOrderRequests(qualifier, null, fetch);
  }

  public NSArray toB2bCxmlOrderRequests(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("toCommande", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("FwkCaramboleB2bCxmlOrderRequest", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = toB2bCxmlOrderRequests();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToB2bCxmlOrderRequestsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ORDER_REQUESTS_KEY);
  }

  public void removeFromToB2bCxmlOrderRequestsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ORDER_REQUESTS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createToB2bCxmlOrderRequestsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleB2bCxmlOrderRequest");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_B2B_CXML_ORDER_REQUESTS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteToB2bCxmlOrderRequestsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ORDER_REQUESTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToB2bCxmlOrderRequestsRelationships() {
    Enumeration objects = toB2bCxmlOrderRequests().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToB2bCxmlOrderRequestsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }

  public NSArray toB2bCxmlPunchoutmsgs() {
    return (NSArray)storedValueForKey(TO_B2B_CXML_PUNCHOUTMSGS_KEY);
  }

  public NSArray toB2bCxmlPunchoutmsgs(EOQualifier qualifier) {
    return toB2bCxmlPunchoutmsgs(qualifier, null, false);
  }

  public NSArray toB2bCxmlPunchoutmsgs(EOQualifier qualifier, boolean fetch) {
    return toB2bCxmlPunchoutmsgs(qualifier, null, fetch);
  }

  public NSArray toB2bCxmlPunchoutmsgs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("toCommande", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("FwkCaramboleB2bCxmlPunchoutmsg", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = toB2bCxmlPunchoutmsgs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToB2bCxmlPunchoutmsgsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
  }

  public void removeFromToB2bCxmlPunchoutmsgsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createToB2bCxmlPunchoutmsgsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleB2bCxmlPunchoutmsg");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteToB2bCxmlPunchoutmsgsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_PUNCHOUTMSGS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToB2bCxmlPunchoutmsgsRelationships() {
    Enumeration objects = toB2bCxmlPunchoutmsgs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToB2bCxmlPunchoutmsgsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }


  public static EOCommande createFwkCaramboleCommande(EOEditingContext editingContext, NSTimestamp commDate
, NSTimestamp commDateCreation
, String commLibelle
, org.cocktail.fwkcktldepense.client.metier.EODevise devise, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur, org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtat, org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtatImprimable, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur) {
    EOCommande eo = (EOCommande) createAndInsertInstance(editingContext, _EOCommande.ENTITY_NAME);    
		eo.setCommDate(commDate);
		eo.setCommDateCreation(commDateCreation);
		eo.setCommLibelle(commLibelle);
    eo.setDeviseRelationship(devise);
    eo.setExerciceRelationship(exercice);
    eo.setFournisseurRelationship(fournisseur);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeEtatImprimableRelationship(typeEtatImprimable);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCommande.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCommande.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCommande localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommande)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCommande localInstanceIn(EOEditingContext editingContext, EOCommande eo) {
    EOCommande localInstance = (eo == null) ? null : (EOCommande)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCommande#localInstanceIn a la place.
   */
	public static EOCommande localInstanceOf(EOEditingContext editingContext, EOCommande eo) {
		return EOCommande.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
