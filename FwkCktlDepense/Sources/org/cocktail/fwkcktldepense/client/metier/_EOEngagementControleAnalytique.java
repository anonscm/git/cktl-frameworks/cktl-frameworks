// _EOEngagementControleAnalytique.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngagementControleAnalytique.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngagementControleAnalytique extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementControleAnalytique";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_CTRL_ANALYTIQUE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "eanaId";

	public static final String EANA_DATE_SAISIE_KEY = "eanaDateSaisie";
	public static final String EANA_HT_SAISIE_KEY = "eanaHtSaisie";
	public static final String EANA_MONTANT_BUDGETAIRE_KEY = "eanaMontantBudgetaire";
	public static final String EANA_MONTANT_BUDGETAIRE_RESTE_KEY = "eanaMontantBudgetaireReste";
	public static final String EANA_TTC_SAISIE_KEY = "eanaTtcSaisie";
	public static final String EANA_TVA_SAISIE_KEY = "eanaTvaSaisie";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String EANA_ID_KEY = "eanaId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String EANA_DATE_SAISIE_COLKEY = "EANA_DATE_SAISIE";
	public static final String EANA_HT_SAISIE_COLKEY = "EANA_HT_SAISIE";
	public static final String EANA_MONTANT_BUDGETAIRE_COLKEY = "EANA_MONTANT_BUDGETAIRE";
	public static final String EANA_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EANA_MONTANT_BUDGETAIRE_RESTE";
	public static final String EANA_TTC_SAISIE_COLKEY = "EANA_TTC_SAISIE";
	public static final String EANA_TVA_SAISIE_COLKEY = "EANA_TVA_SAISIE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String EANA_ID_COLKEY = "EANA_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public NSTimestamp eanaDateSaisie() {
    return (NSTimestamp) storedValueForKey(EANA_DATE_SAISIE_KEY);
  }

  public void setEanaDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, EANA_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal eanaHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EANA_HT_SAISIE_KEY);
  }

  public void setEanaHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EANA_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal eanaMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(EANA_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEanaMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EANA_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal eanaMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(EANA_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEanaMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EANA_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal eanaTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EANA_TTC_SAISIE_KEY);
  }

  public void setEanaTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EANA_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal eanaTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EANA_TVA_SAISIE_KEY);
  }

  public void setEanaTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EANA_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
  }

  public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget oldValue = engagementBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

  public static EOEngagementControleAnalytique createFwkCaramboleEngagementControleAnalytique(EOEditingContext editingContext, NSTimestamp eanaDateSaisie
, java.math.BigDecimal eanaHtSaisie
, java.math.BigDecimal eanaMontantBudgetaire
, java.math.BigDecimal eanaMontantBudgetaireReste
, java.math.BigDecimal eanaTtcSaisie
, java.math.BigDecimal eanaTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOCodeAnalytique codeAnalytique, org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice) {
    EOEngagementControleAnalytique eo = (EOEngagementControleAnalytique) createAndInsertInstance(editingContext, _EOEngagementControleAnalytique.ENTITY_NAME);    
		eo.setEanaDateSaisie(eanaDateSaisie);
		eo.setEanaHtSaisie(eanaHtSaisie);
		eo.setEanaMontantBudgetaire(eanaMontantBudgetaire);
		eo.setEanaMontantBudgetaireReste(eanaMontantBudgetaireReste);
		eo.setEanaTtcSaisie(eanaTtcSaisie);
		eo.setEanaTvaSaisie(eanaTvaSaisie);
    eo.setCodeAnalytiqueRelationship(codeAnalytique);
    eo.setEngagementBudgetRelationship(engagementBudget);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngagementControleAnalytique.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngagementControleAnalytique.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEngagementControleAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngagementControleAnalytique)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEngagementControleAnalytique localInstanceIn(EOEditingContext editingContext, EOEngagementControleAnalytique eo) {
    EOEngagementControleAnalytique localInstance = (eo == null) ? null : (EOEngagementControleAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngagementControleAnalytique#localInstanceIn a la place.
   */
	public static EOEngagementControleAnalytique localInstanceOf(EOEditingContext editingContext, EOEngagementControleAnalytique eo) {
		return EOEngagementControleAnalytique.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEngagementControleAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngagementControleAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementControleAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementControleAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementControleAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementControleAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementControleAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementControleAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngagementControleAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementControleAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementControleAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementControleAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
