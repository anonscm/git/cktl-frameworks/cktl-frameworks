// _EOReimputationHorsMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReimputationHorsMarche.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReimputationHorsMarche extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_HORS_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rehmId";

	public static final String REHM_HT_SAISIE_KEY = "rehmHtSaisie";
	public static final String REHM_MONTANT_BUDGETAIRE_KEY = "rehmMontantBudgetaire";
	public static final String REHM_TTC_SAISIE_KEY = "rehmTtcSaisie";
	public static final String REHM_TVA_SAISIE_KEY = "rehmTvaSaisie";

// Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String REHM_ID_KEY = "rehmId";
	public static final String REIM_ID_KEY = "reimId";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String REHM_HT_SAISIE_COLKEY = "REHM_HT_SAISIE";
	public static final String REHM_MONTANT_BUDGETAIRE_COLKEY = "REHM_MONTANT_BUDGETAIRE";
	public static final String REHM_TTC_SAISIE_COLKEY = "REHM_TTC_SAISIE";
	public static final String REHM_TVA_SAISIE_COLKEY = "REHM_TVA_SAISIE";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String REHM_ID_COLKEY = "REHM_ID";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String REIMPUTATION_KEY = "reimputation";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
  public java.math.BigDecimal rehmHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REHM_HT_SAISIE_KEY);
  }

  public void setRehmHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REHM_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal rehmMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(REHM_MONTANT_BUDGETAIRE_KEY);
  }

  public void setRehmMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REHM_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal rehmTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REHM_TTC_SAISIE_KEY);
  }

  public void setRehmTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REHM_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal rehmTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REHM_TVA_SAISIE_KEY);
  }

  public void setRehmTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REHM_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation() {
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
  }

  public void setReimputationRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOReimputation oldValue = reimputation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
  }

  public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeAchat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeAchat oldValue = typeAchat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
    }
  }
  

  public static EOReimputationHorsMarche createFwkCaramboleReimputationHorsMarche(EOEditingContext editingContext, java.math.BigDecimal rehmHtSaisie
, java.math.BigDecimal rehmMontantBudgetaire
, java.math.BigDecimal rehmTtcSaisie
, java.math.BigDecimal rehmTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer, org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation, org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat) {
    EOReimputationHorsMarche eo = (EOReimputationHorsMarche) createAndInsertInstance(editingContext, _EOReimputationHorsMarche.ENTITY_NAME);    
		eo.setRehmHtSaisie(rehmHtSaisie);
		eo.setRehmMontantBudgetaire(rehmMontantBudgetaire);
		eo.setRehmTtcSaisie(rehmTtcSaisie);
		eo.setRehmTvaSaisie(rehmTvaSaisie);
    eo.setCodeExerRelationship(codeExer);
    eo.setReimputationRelationship(reimputation);
    eo.setTypeAchatRelationship(typeAchat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOReimputationHorsMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOReimputationHorsMarche.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOReimputationHorsMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReimputationHorsMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOReimputationHorsMarche localInstanceIn(EOEditingContext editingContext, EOReimputationHorsMarche eo) {
    EOReimputationHorsMarche localInstance = (eo == null) ? null : (EOReimputationHorsMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOReimputationHorsMarche#localInstanceIn a la place.
   */
	public static EOReimputationHorsMarche localInstanceOf(EOEditingContext editingContext, EOReimputationHorsMarche eo) {
		return EOReimputationHorsMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOReimputationHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOReimputationHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOReimputationHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
