// _EOEcritureDetailAEmarger.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcritureDetailAEmarger.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEcritureDetailAEmarger extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEcritureDetailAEmarger";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_ECRITURE_DETAIL_A_EMARGER";



	// Attributes


	public static final String ECD_CREDIT_KEY = "ecdCredit";
	public static final String ECD_DEBIT_KEY = "ecdDebit";
	public static final String ECD_LIBELLE_KEY = "ecdLibelle";
	public static final String ECD_MONTANT_KEY = "ecdMontant";
	public static final String ECD_RESTE_EMARGER_KEY = "ecdResteEmarger";
	public static final String ECD_SENS_KEY = "ecdSens";
	public static final String ECR_NUMERO_KEY = "ecrNumero";

// Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String ECD_CREDIT_COLKEY = "ECD_CREDIT";
	public static final String ECD_DEBIT_COLKEY = "ECD_DEBIT";
	public static final String ECD_LIBELLE_COLKEY = "ECD_LIBELLE";
	public static final String ECD_MONTANT_COLKEY = "ECD_MONTANT";
	public static final String ECD_RESTE_EMARGER_COLKEY = "ECD_RESTE_EMARGER";
	public static final String ECD_SENS_COLKEY = "ECD_SENS";
	public static final String ECR_NUMERO_COLKEY = "ECR_NUMERO";

	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String GESTION_KEY = "gestion";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
  public java.math.BigDecimal ecdCredit() {
    return (java.math.BigDecimal) storedValueForKey(ECD_CREDIT_KEY);
  }

  public void setEcdCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_CREDIT_KEY);
  }

  public java.math.BigDecimal ecdDebit() {
    return (java.math.BigDecimal) storedValueForKey(ECD_DEBIT_KEY);
  }

  public void setEcdDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_DEBIT_KEY);
  }

  public String ecdLibelle() {
    return (String) storedValueForKey(ECD_LIBELLE_KEY);
  }

  public void setEcdLibelle(String value) {
    takeStoredValueForKey(value, ECD_LIBELLE_KEY);
  }

  public java.math.BigDecimal ecdMontant() {
    return (java.math.BigDecimal) storedValueForKey(ECD_MONTANT_KEY);
  }

  public void setEcdMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_MONTANT_KEY);
  }

  public java.math.BigDecimal ecdResteEmarger() {
    return (java.math.BigDecimal) storedValueForKey(ECD_RESTE_EMARGER_KEY);
  }

  public void setEcdResteEmarger(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECD_RESTE_EMARGER_KEY);
  }

  public String ecdSens() {
    return (String) storedValueForKey(ECD_SENS_KEY);
  }

  public void setEcdSens(String value) {
    takeStoredValueForKey(value, ECD_SENS_KEY);
  }

  public Integer ecrNumero() {
    return (Integer) storedValueForKey(ECR_NUMERO_KEY);
  }

  public void setEcrNumero(Integer value) {
    takeStoredValueForKey(value, ECR_NUMERO_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail ecritureDetail() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
  }

  public void setEcritureDetailRelationship(org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail oldValue = ecritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOGestion gestion() {
    return (org.cocktail.fwkcktldepense.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.fwkcktldepense.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOModePaiement modePaiement() {
    return (org.cocktail.fwkcktldepense.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.fwkcktldepense.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

  public static EOEcritureDetailAEmarger createFwkCaramboleEcritureDetailAEmarger(EOEditingContext editingContext, java.math.BigDecimal ecdMontant
, java.math.BigDecimal ecdResteEmarger
, String ecdSens
, org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail ecritureDetail, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOGestion gestion, org.cocktail.fwkcktldepense.client.metier.EOModePaiement modePaiement, org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable) {
    EOEcritureDetailAEmarger eo = (EOEcritureDetailAEmarger) createAndInsertInstance(editingContext, _EOEcritureDetailAEmarger.ENTITY_NAME);    
		eo.setEcdMontant(ecdMontant);
		eo.setEcdResteEmarger(ecdResteEmarger);
		eo.setEcdSens(ecdSens);
    eo.setEcritureDetailRelationship(ecritureDetail);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    eo.setModePaiementRelationship(modePaiement);
    eo.setPlanComptableRelationship(planComptable);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEcritureDetailAEmarger.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEcritureDetailAEmarger.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEcritureDetailAEmarger localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcritureDetailAEmarger)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEcritureDetailAEmarger localInstanceIn(EOEditingContext editingContext, EOEcritureDetailAEmarger eo) {
    EOEcritureDetailAEmarger localInstance = (eo == null) ? null : (EOEcritureDetailAEmarger)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEcritureDetailAEmarger#localInstanceIn a la place.
   */
	public static EOEcritureDetailAEmarger localInstanceOf(EOEditingContext editingContext, EOEcritureDetailAEmarger eo) {
		return EOEcritureDetailAEmarger.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEcritureDetailAEmarger fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEcritureDetailAEmarger fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcritureDetailAEmarger eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcritureDetailAEmarger)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcritureDetailAEmarger fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcritureDetailAEmarger fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcritureDetailAEmarger eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcritureDetailAEmarger)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEcritureDetailAEmarger fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcritureDetailAEmarger eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcritureDetailAEmarger ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcritureDetailAEmarger fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
