// _EODepenseBudget.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepenseBudget.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepenseBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleDepenseBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DEP_ID_REVERSEMENT_KEY = "depIdReversement";
	public static final String DPP_ID_KEY = "dppId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TAP_ID_KEY = "tapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_HT_SAISIE_COLKEY = "DEP_HT_SAISIE";
	public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "DEP_MONTANT_BUDGETAIRE";
	public static final String DEP_TTC_SAISIE_COLKEY = "DEP_TTC_SAISIE";
	public static final String DEP_TVA_SAISIE_COLKEY = "DEP_TVA_SAISIE";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DEP_ID_REVERSEMENT_COLKEY = "DEP_ID_REVERSEMENT";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_BUDGET_REVERSEMENT_KEY = "depenseBudgetReversement";
	public static final String DEPENSE_CONTROLE_ACTIONS_KEY = "depenseControleActions";
	public static final String DEPENSE_CONTROLE_ANALYTIQUES_KEY = "depenseControleAnalytiques";
	public static final String DEPENSE_CONTROLE_CONVENTIONS_KEY = "depenseControleConventions";
	public static final String DEPENSE_CONTROLE_HORS_MARCHES_KEY = "depenseControleHorsMarches";
	public static final String DEPENSE_CONTROLE_MARCHES_KEY = "depenseControleMarches";
	public static final String DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY = "depenseControlePlanComptables";
	public static final String DEPENSE_PAPIER_KEY = "depensePapier";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String REIMPUTATIONS_KEY = "reimputations";
	public static final String REVERSEMENTS_KEY = "reversements";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TO_EXTOURNE_LIQ_DEFS_KEY = "toExtourneLiqDefs";
	public static final String TO_EXTOURNE_LIQ_N1S_KEY = "toExtourneLiqN1s";
	public static final String TO_EXTOURNE_LIQ_NS_KEY = "toExtourneLiqNs";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public java.math.BigDecimal depHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_HT_SAISIE_KEY);
  }

  public void setDepHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal depMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDepMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal depTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TTC_SAISIE_KEY);
  }

  public void setDepTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal depTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TVA_SAISIE_KEY);
  }

  public void setDepTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseBudget depenseBudgetReversement() {
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_REVERSEMENT_KEY);
  }

  public void setDepenseBudgetReversementRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODepenseBudget oldValue = depenseBudgetReversement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_REVERSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_REVERSEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EODepensePapier depensePapier() {
    return (org.cocktail.fwkcktldepense.client.metier.EODepensePapier)storedValueForKey(DEPENSE_PAPIER_KEY);
  }

  public void setDepensePapierRelationship(org.cocktail.fwkcktldepense.client.metier.EODepensePapier value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODepensePapier oldValue = depensePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
  }

  public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget oldValue = engagementBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.client.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray depenseControleActions() {
    return (NSArray)storedValueForKey(DEPENSE_CONTROLE_ACTIONS_KEY);
  }

  public NSArray depenseControleActions(EOQualifier qualifier) {
    return depenseControleActions(qualifier, null, false);
  }

  public NSArray depenseControleActions(EOQualifier qualifier, boolean fetch) {
    return depenseControleActions(qualifier, null, fetch);
  }

  public NSArray depenseControleActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseControleActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ACTIONS_KEY);
  }

  public void removeFromDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ACTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction createDepenseControleActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseControleAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_ACTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction) eo;
  }

  public void deleteDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControleActionsRelationships() {
    Enumeration objects = depenseControleActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControleActionsRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseControleAction)objects.nextElement());
    }
  }

  public NSArray depenseControleAnalytiques() {
    return (NSArray)storedValueForKey(DEPENSE_CONTROLE_ANALYTIQUES_KEY);
  }

  public NSArray depenseControleAnalytiques(EOQualifier qualifier) {
    return depenseControleAnalytiques(qualifier, null, false);
  }

  public NSArray depenseControleAnalytiques(EOQualifier qualifier, boolean fetch) {
    return depenseControleAnalytiques(qualifier, null, fetch);
  }

  public NSArray depenseControleAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseControleAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
  }

  public void removeFromDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique createDepenseControleAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseControleAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique) eo;
  }

  public void deleteDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControleAnalytiquesRelationships() {
    Enumeration objects = depenseControleAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControleAnalytiquesRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseControleAnalytique)objects.nextElement());
    }
  }

  public NSArray depenseControleConventions() {
    return (NSArray)storedValueForKey(DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public NSArray depenseControleConventions(EOQualifier qualifier) {
    return depenseControleConventions(qualifier, null, false);
  }

  public NSArray depenseControleConventions(EOQualifier qualifier, boolean fetch) {
    return depenseControleConventions(qualifier, null, fetch);
  }

  public NSArray depenseControleConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseControleConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public void removeFromDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention createDepenseControleConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseControleConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention) eo;
  }

  public void deleteDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControleConventionsRelationships() {
    Enumeration objects = depenseControleConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControleConventionsRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseControleConvention)objects.nextElement());
    }
  }

  public NSArray depenseControleHorsMarches() {
    return (NSArray)storedValueForKey(DEPENSE_CONTROLE_HORS_MARCHES_KEY);
  }

  public NSArray depenseControleHorsMarches(EOQualifier qualifier) {
    return depenseControleHorsMarches(qualifier, null, false);
  }

  public NSArray depenseControleHorsMarches(EOQualifier qualifier, boolean fetch) {
    return depenseControleHorsMarches(qualifier, null, fetch);
  }

  public NSArray depenseControleHorsMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseControleHorsMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
  }

  public void removeFromDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche createDepenseControleHorsMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseControleHorsMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche) eo;
  }

  public void deleteDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_HORS_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControleHorsMarchesRelationships() {
    Enumeration objects = depenseControleHorsMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControleHorsMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseControleHorsMarche)objects.nextElement());
    }
  }

  public NSArray depenseControleMarches() {
    return (NSArray)storedValueForKey(DEPENSE_CONTROLE_MARCHES_KEY);
  }

  public NSArray depenseControleMarches(EOQualifier qualifier) {
    return depenseControleMarches(qualifier, null, false);
  }

  public NSArray depenseControleMarches(EOQualifier qualifier, boolean fetch) {
    return depenseControleMarches(qualifier, null, fetch);
  }

  public NSArray depenseControleMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseControleMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_MARCHES_KEY);
  }

  public void removeFromDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche createDepenseControleMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseControleMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche) eo;
  }

  public void deleteDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControleMarchesRelationships() {
    Enumeration objects = depenseControleMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControleMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseControleMarche)objects.nextElement());
    }
  }

  public NSArray depenseControlePlanComptables() {
    return (NSArray)storedValueForKey(DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public NSArray depenseControlePlanComptables(EOQualifier qualifier) {
    return depenseControlePlanComptables(qualifier, null, false);
  }

  public NSArray depenseControlePlanComptables(EOQualifier qualifier, boolean fetch) {
    return depenseControlePlanComptables(qualifier, null, fetch);
  }

  public NSArray depenseControlePlanComptables(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseControlePlanComptables();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public void removeFromDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable createDepenseControlePlanComptablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseControlePlanComptable");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable) eo;
  }

  public void deleteDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControlePlanComptablesRelationships() {
    Enumeration objects = depenseControlePlanComptables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControlePlanComptablesRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable)objects.nextElement());
    }
  }

  public NSArray reimputations() {
    return (NSArray)storedValueForKey(REIMPUTATIONS_KEY);
  }

  public NSArray reimputations(EOQualifier qualifier) {
    return reimputations(qualifier, null, false);
  }

  public NSArray reimputations(EOQualifier qualifier, boolean fetch) {
    return reimputations(qualifier, null, fetch);
  }

  public NSArray reimputations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputation.DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
  }

  public void removeFromReimputationsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputation createReimputationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputation) eo;
  }

  public void deleteReimputationsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationsRelationships() {
    Enumeration objects = reimputations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputation)objects.nextElement());
    }
  }

  public NSArray reversements() {
    return (NSArray)storedValueForKey(REVERSEMENTS_KEY);
  }

  public NSArray reversements(EOQualifier qualifier) {
    return reversements(qualifier, null, false);
  }

  public NSArray reversements(EOQualifier qualifier, boolean fetch) {
    return reversements(qualifier, null, fetch);
  }

  public NSArray reversements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget.DEPENSE_BUDGET_REVERSEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reversements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReversementsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REVERSEMENTS_KEY);
  }

  public void removeFromReversementsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REVERSEMENTS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseBudget createReversementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REVERSEMENTS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseBudget) eo;
  }

  public void deleteReversementsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REVERSEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReversementsRelationships() {
    Enumeration objects = reversements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReversementsRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseBudget)objects.nextElement());
    }
  }

  public NSArray toExtourneLiqDefs() {
    return (NSArray)storedValueForKey(TO_EXTOURNE_LIQ_DEFS_KEY);
  }

  public NSArray toExtourneLiqDefs(EOQualifier qualifier) {
    return toExtourneLiqDefs(qualifier, null, false);
  }

  public NSArray toExtourneLiqDefs(EOQualifier qualifier, boolean fetch) {
    return toExtourneLiqDefs(qualifier, null, fetch);
  }

  public NSArray toExtourneLiqDefs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("toDepenseBudget", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("FwkCaramboleExtourneLiqDef", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = toExtourneLiqDefs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExtourneLiqDefsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_DEFS_KEY);
  }

  public void removeFromToExtourneLiqDefsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_DEFS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createToExtourneLiqDefsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleExtourneLiqDef");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_DEFS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteToExtourneLiqDefsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_DEFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExtourneLiqDefsRelationships() {
    Enumeration objects = toExtourneLiqDefs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExtourneLiqDefsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }

  public NSArray toExtourneLiqN1s() {
    return (NSArray)storedValueForKey(TO_EXTOURNE_LIQ_N1S_KEY);
  }

  public NSArray toExtourneLiqN1s(EOQualifier qualifier) {
    return toExtourneLiqN1s(qualifier, null, false);
  }

  public NSArray toExtourneLiqN1s(EOQualifier qualifier, boolean fetch) {
    return toExtourneLiqN1s(qualifier, null, fetch);
  }

  public NSArray toExtourneLiqN1s(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("fwkCaramboleDepenseBudget", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("FwkCaramboleExtourneLiq", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = toExtourneLiqN1s();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExtourneLiqN1sRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_N1S_KEY);
  }

  public void removeFromToExtourneLiqN1sRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_N1S_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createToExtourneLiqN1sRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleExtourneLiq");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_N1S_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteToExtourneLiqN1sRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_N1S_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExtourneLiqN1sRelationships() {
    Enumeration objects = toExtourneLiqN1s().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExtourneLiqN1sRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }

  public NSArray toExtourneLiqNs() {
    return (NSArray)storedValueForKey(TO_EXTOURNE_LIQ_NS_KEY);
  }

  public NSArray toExtourneLiqNs(EOQualifier qualifier) {
    return toExtourneLiqNs(qualifier, null, false);
  }

  public NSArray toExtourneLiqNs(EOQualifier qualifier, boolean fetch) {
    return toExtourneLiqNs(qualifier, null, fetch);
  }

  public NSArray toExtourneLiqNs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("toDepenseBudgetN", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("FwkCaramboleExtourneLiq", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = toExtourneLiqNs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExtourneLiqNsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_NS_KEY);
  }

  public void removeFromToExtourneLiqNsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_NS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createToExtourneLiqNsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleExtourneLiq");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_LIQ_NS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteToExtourneLiqNsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_LIQ_NS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExtourneLiqNsRelationships() {
    Enumeration objects = toExtourneLiqNs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExtourneLiqNsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }


  public static EODepenseBudget createFwkCaramboleDepenseBudget(EOEditingContext editingContext, java.math.BigDecimal depHtSaisie
, java.math.BigDecimal depMontantBudgetaire
, java.math.BigDecimal depTtcSaisie
, java.math.BigDecimal depTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EODepensePapier depensePapier, org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur) {
    EODepenseBudget eo = (EODepenseBudget) createAndInsertInstance(editingContext, _EODepenseBudget.ENTITY_NAME);    
		eo.setDepHtSaisie(depHtSaisie);
		eo.setDepMontantBudgetaire(depMontantBudgetaire);
		eo.setDepTtcSaisie(depTtcSaisie);
		eo.setDepTvaSaisie(depTvaSaisie);
    eo.setDepensePapierRelationship(depensePapier);
    eo.setEngagementBudgetRelationship(engagementBudget);
    eo.setExerciceRelationship(exercice);
    eo.setTauxProrataRelationship(tauxProrata);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EODepenseBudget.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EODepenseBudget.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EODepenseBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepenseBudget)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EODepenseBudget localInstanceIn(EOEditingContext editingContext, EODepenseBudget eo) {
    EODepenseBudget localInstance = (eo == null) ? null : (EODepenseBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EODepenseBudget#localInstanceIn a la place.
   */
	public static EODepenseBudget localInstanceOf(EOEditingContext editingContext, EODepenseBudget eo) {
		return EODepenseBudget.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EODepenseBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODepenseBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EODepenseBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepenseBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepenseBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepenseBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
