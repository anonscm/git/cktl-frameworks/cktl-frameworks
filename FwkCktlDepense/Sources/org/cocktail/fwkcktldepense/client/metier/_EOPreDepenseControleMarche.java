// _EOPreDepenseControleMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreDepenseControleMarche.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPreDepenseControleMarche extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseControleMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdmarId";

	public static final String DMAR_HT_SAISIE_KEY = "dmarHtSaisie";
	public static final String DMAR_MONTANT_BUDGETAIRE_KEY = "dmarMontantBudgetaire";
	public static final String DMAR_TTC_SAISIE_KEY = "dmarTtcSaisie";
	public static final String DMAR_TVA_SAISIE_KEY = "dmarTvaSaisie";

// Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PDEP_ID_KEY = "pdepId";
	public static final String PDMAR_ID_KEY = "pdmarId";

//Colonnes dans la base de donnees
	public static final String DMAR_HT_SAISIE_COLKEY = "PDMAR_HT_SAISIE";
	public static final String DMAR_MONTANT_BUDGETAIRE_COLKEY = "PDMAR_MONTANT_BUDGETAIRE";
	public static final String DMAR_TTC_SAISIE_COLKEY = "PDMAR_TTC_SAISIE";
	public static final String DMAR_TVA_SAISIE_COLKEY = "PDMAR_TVA_SAISIE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PDEP_ID_COLKEY = "PDEP_ID";
	public static final String PDMAR_ID_COLKEY = "PDMAR_ID";


	// Relationships
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PRE_DEPENSE_BUDGET_KEY = "preDepenseBudget";



	// Accessors methods
  public java.math.BigDecimal dmarHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DMAR_HT_SAISIE_KEY);
  }

  public void setDmarHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DMAR_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal dmarMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DMAR_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDmarMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DMAR_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal dmarTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DMAR_TTC_SAISIE_KEY);
  }

  public void setDmarTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DMAR_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dmarTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DMAR_TVA_SAISIE_KEY);
  }

  public void setDmarTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DMAR_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOAttribution attribution() {
    return (org.cocktail.fwkcktldepense.client.metier.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttributionRelationship(org.cocktail.fwkcktldepense.client.metier.EOAttribution value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOAttribution oldValue = attribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget preDepenseBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget)storedValueForKey(PRE_DEPENSE_BUDGET_KEY);
  }

  public void setPreDepenseBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget oldValue = preDepenseBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRE_DEPENSE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRE_DEPENSE_BUDGET_KEY);
    }
  }
  

  public static EOPreDepenseControleMarche createFwkCarambolePreDepenseControleMarche(EOEditingContext editingContext, java.math.BigDecimal dmarHtSaisie
, java.math.BigDecimal dmarMontantBudgetaire
, java.math.BigDecimal dmarTtcSaisie
, java.math.BigDecimal dmarTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOAttribution attribution, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget preDepenseBudget) {
    EOPreDepenseControleMarche eo = (EOPreDepenseControleMarche) createAndInsertInstance(editingContext, _EOPreDepenseControleMarche.ENTITY_NAME);    
		eo.setDmarHtSaisie(dmarHtSaisie);
		eo.setDmarMontantBudgetaire(dmarMontantBudgetaire);
		eo.setDmarTtcSaisie(dmarTtcSaisie);
		eo.setDmarTvaSaisie(dmarTvaSaisie);
    eo.setAttributionRelationship(attribution);
    eo.setExerciceRelationship(exercice);
    eo.setPreDepenseBudgetRelationship(preDepenseBudget);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPreDepenseControleMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPreDepenseControleMarche.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPreDepenseControleMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreDepenseControleMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPreDepenseControleMarche localInstanceIn(EOEditingContext editingContext, EOPreDepenseControleMarche eo) {
    EOPreDepenseControleMarche localInstance = (eo == null) ? null : (EOPreDepenseControleMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPreDepenseControleMarche#localInstanceIn a la place.
   */
	public static EOPreDepenseControleMarche localInstanceOf(EOEditingContext editingContext, EOPreDepenseControleMarche eo) {
		return EOPreDepenseControleMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPreDepenseControleMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPreDepenseControleMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseControleMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseControleMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseControleMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseControleMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseControleMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseControleMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPreDepenseControleMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseControleMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseControleMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseControleMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
