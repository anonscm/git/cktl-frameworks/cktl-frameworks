// _EOInventaire.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInventaire.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOInventaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleInventaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_INVENTAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "invcId";

	public static final String CLIC_NUM_COMPLET_KEY = "clicNumComplet";
	public static final String LID_MONTANT_KEY = "lidMontant";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String INVC_ID_KEY = "invcId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CLIC_NUM_COMPLET_COLKEY = "CLIC_NUM_COMPLET";
	public static final String LID_MONTANT_COLKEY = "LID_MONTANT";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY = "depenseControlePlanComptable";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public String clicNumComplet() {
    return (String) storedValueForKey(CLIC_NUM_COMPLET_KEY);
  }

  public void setClicNumComplet(String value) {
    takeStoredValueForKey(value, CLIC_NUM_COMPLET_KEY);
  }

  public java.math.BigDecimal lidMontant() {
    return (java.math.BigDecimal) storedValueForKey(LID_MONTANT_KEY);
  }

  public void setLidMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, LID_MONTANT_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommande commande() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
  }

  public void setCommandeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommande oldValue = commande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable depenseControlePlanComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable)storedValueForKey(DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
  }

  public void setDepenseControlePlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODepenseControlePlanComptable oldValue = depenseControlePlanComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_CONTROLE_PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktldepense.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

  public static EOInventaire createFwkCaramboleInventaire(EOEditingContext editingContext, org.cocktail.fwkcktldepense.client.metier.EOCommande commande, org.cocktail.fwkcktldepense.client.metier.EOOrgan organ, org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable, org.cocktail.fwkcktldepense.client.metier.EOTypeCredit typeCredit) {
    EOInventaire eo = (EOInventaire) createAndInsertInstance(editingContext, _EOInventaire.ENTITY_NAME);    
    eo.setCommandeRelationship(commande);
    eo.setOrganRelationship(organ);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOInventaire.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOInventaire.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOInventaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOInventaire)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOInventaire localInstanceIn(EOEditingContext editingContext, EOInventaire eo) {
    EOInventaire localInstance = (eo == null) ? null : (EOInventaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOInventaire#localInstanceIn a la place.
   */
	public static EOInventaire localInstanceOf(EOEditingContext editingContext, EOInventaire eo) {
		return EOInventaire.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOInventaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOInventaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOInventaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
