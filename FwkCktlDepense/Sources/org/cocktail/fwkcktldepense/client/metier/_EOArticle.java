// _EOArticle.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArticle.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOArticle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleArticle";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ARTICLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "artId";

	public static final String ART_LIBELLE_KEY = "artLibelle";
	public static final String ART_PRIX_HT_KEY = "artPrixHt";
	public static final String ART_PRIX_TOTAL_HT_KEY = "artPrixTotalHt";
	public static final String ART_PRIX_TOTAL_TTC_KEY = "artPrixTotalTtc";
	public static final String ART_PRIX_TTC_KEY = "artPrixTtc";
	public static final String ART_QUANTITE_KEY = "artQuantite";
	public static final String ART_REFERENCE_KEY = "artReference";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String ART_ID_PERE_KEY = "artIdPere";
	public static final String ARTC_ID_KEY = "artcId";
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String COMM_ID_KEY = "commId";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String ART_LIBELLE_COLKEY = "ART_LIBELLE";
	public static final String ART_PRIX_HT_COLKEY = "ART_PRIX_HT";
	public static final String ART_PRIX_TOTAL_HT_COLKEY = "ART_PRIX_TOTAL_HT";
	public static final String ART_PRIX_TOTAL_TTC_COLKEY = "ART_PRIX_TOTAL_TTC";
	public static final String ART_PRIX_TTC_COLKEY = "ART_PRIX_TTC";
	public static final String ART_QUANTITE_COLKEY = "ART_QUANTITE";
	public static final String ART_REFERENCE_COLKEY = "ART_REFERENCE";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String ART_ID_PERE_COLKEY = "ART_ID_PERE";
	public static final String ARTC_ID_COLKEY = "ARTC_ID";
	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String ARTICLE_KEY = "article";
	public static final String ARTICLE_CATALOGUE_KEY = "articleCatalogue";
	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String COMMANDE_KEY = "commande";
	public static final String TO_B2B_CXML_ITEMS_KEY = "toB2bCxmlItems";
	public static final String TVA_KEY = "tva";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
  public String artLibelle() {
    return (String) storedValueForKey(ART_LIBELLE_KEY);
  }

  public void setArtLibelle(String value) {
    takeStoredValueForKey(value, ART_LIBELLE_KEY);
  }

  public java.math.BigDecimal artPrixHt() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_HT_KEY);
  }

  public void setArtPrixHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_HT_KEY);
  }

  public java.math.BigDecimal artPrixTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_TOTAL_HT_KEY);
  }

  public void setArtPrixTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal artPrixTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_TOTAL_TTC_KEY);
  }

  public void setArtPrixTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_TOTAL_TTC_KEY);
  }

  public java.math.BigDecimal artPrixTtc() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_TTC_KEY);
  }

  public void setArtPrixTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_TTC_KEY);
  }

  public java.math.BigDecimal artQuantite() {
    return (java.math.BigDecimal) storedValueForKey(ART_QUANTITE_KEY);
  }

  public void setArtQuantite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_QUANTITE_KEY);
  }

  public String artReference() {
    return (String) storedValueForKey(ART_REFERENCE_KEY);
  }

  public void setArtReference(String value) {
    takeStoredValueForKey(value, ART_REFERENCE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOArticle article() {
    return (org.cocktail.fwkcktldepense.client.metier.EOArticle)storedValueForKey(ARTICLE_KEY);
  }

  public void setArticleRelationship(org.cocktail.fwkcktldepense.client.metier.EOArticle value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOArticle oldValue = article();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle articleCatalogue() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle)storedValueForKey(ARTICLE_CATALOGUE_KEY);
  }

  public void setArticleCatalogueRelationship(org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCatalogueArticle oldValue = articleCatalogue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_CATALOGUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_CATALOGUE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOAttribution attribution() {
    return (org.cocktail.fwkcktldepense.client.metier.EOAttribution)storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttributionRelationship(org.cocktail.fwkcktldepense.client.metier.EOAttribution value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOAttribution oldValue = attribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTRIBUTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCommande commande() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
  }

  public void setCommandeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommande oldValue = commande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTva tva() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTva)storedValueForKey(TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.fwkcktldepense.client.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTva oldValue = tva();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
  }

  public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeAchat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeAchat oldValue = typeAchat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
    }
  }
  
  public NSArray toB2bCxmlItems() {
    return (NSArray)storedValueForKey(TO_B2B_CXML_ITEMS_KEY);
  }

  public NSArray toB2bCxmlItems(EOQualifier qualifier) {
    return toB2bCxmlItems(qualifier, null, false);
  }

  public NSArray toB2bCxmlItems(EOQualifier qualifier, boolean fetch) {
    return toB2bCxmlItems(qualifier, null, fetch);
  }

  public NSArray toB2bCxmlItems(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem.TO_ARTICLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toB2bCxmlItems();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToB2bCxmlItemsRelationship(org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ITEMS_KEY);
  }

  public void removeFromToB2bCxmlItemsRelationship(org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ITEMS_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem createToB2bCxmlItemsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleB2bCxmlItem");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_B2B_CXML_ITEMS_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem) eo;
  }

  public void deleteToB2bCxmlItemsRelationship(org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_B2B_CXML_ITEMS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToB2bCxmlItemsRelationships() {
    Enumeration objects = toB2bCxmlItems().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToB2bCxmlItemsRelationship((org.cocktail.fwkcktlb2b.cxml.depense.client.metier.EOB2bCxmlItem)objects.nextElement());
    }
  }


  public static EOArticle createFwkCaramboleArticle(EOEditingContext editingContext, String artLibelle
, java.math.BigDecimal artPrixHt
, java.math.BigDecimal artPrixTotalHt
, java.math.BigDecimal artPrixTotalTtc
, java.math.BigDecimal artPrixTtc
, java.math.BigDecimal artQuantite
, org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer, org.cocktail.fwkcktldepense.client.metier.EOCommande commande, org.cocktail.fwkcktldepense.client.metier.EOTva tva) {
    EOArticle eo = (EOArticle) createAndInsertInstance(editingContext, _EOArticle.ENTITY_NAME);    
		eo.setArtLibelle(artLibelle);
		eo.setArtPrixHt(artPrixHt);
		eo.setArtPrixTotalHt(artPrixTotalHt);
		eo.setArtPrixTotalTtc(artPrixTotalTtc);
		eo.setArtPrixTtc(artPrixTtc);
		eo.setArtQuantite(artQuantite);
    eo.setCodeExerRelationship(codeExer);
    eo.setCommandeRelationship(commande);
    eo.setTvaRelationship(tva);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOArticle.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOArticle.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOArticle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArticle)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOArticle localInstanceIn(EOEditingContext editingContext, EOArticle eo) {
    EOArticle localInstance = (eo == null) ? null : (EOArticle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOArticle#localInstanceIn a la place.
   */
	public static EOArticle localInstanceOf(EOEditingContext editingContext, EOArticle eo) {
		return EOArticle.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOArticle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOArticle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArticle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArticle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOArticle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArticle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArticle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArticle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
