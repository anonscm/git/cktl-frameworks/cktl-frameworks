// _EOOrgan.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrgan.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrgan extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleOrgan";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_ORGAN";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "orgId";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String ORG_CANAL_OBLIGATOIRE_KEY = "orgCanalObligatoire";
	public static final String ORG_CONVENTION_OBLIGATOIRE_KEY = "orgConventionObligatoire";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_DATE_CLOTURE_KEY = "orgDateCloture";
	public static final String ORG_DATE_OUVERTURE_KEY = "orgDateOuverture";
	public static final String ORG_ETAB_KEY = "orgEtab";
	public static final String ORG_LIBELLE_KEY = "orgLibelle";
	public static final String ORG_LUCRATIVITE_KEY = "orgLucrativite";
	public static final String ORG_NIVEAU_KEY = "orgNiveau";
	public static final String ORG_SOUSCR_KEY = "orgSouscr";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String ORG_UNIV_KEY = "orgUniv";

// Attributs non visibles
	public static final String LOG_ORDRE_KEY = "logOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORG_OP_AUTORISEES_KEY = "orgOpAutorisees";
	public static final String ORG_PERE_KEY = "orgPere";
	public static final String TYOR_ID_KEY = "tyorId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String ORG_CANAL_OBLIGATOIRE_COLKEY = "ORG_CANAL_OBLIGATOIRE";
	public static final String ORG_CONVENTION_OBLIGATOIRE_COLKEY = "ORG_CONVENTION_OBLIGATOIRE";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_DATE_CLOTURE_COLKEY = "ORG_DATE_CLOTURE";
	public static final String ORG_DATE_OUVERTURE_COLKEY = "ORG_DATE_OUVERTURE";
	public static final String ORG_ETAB_COLKEY = "ORG_ETAB";
	public static final String ORG_LIBELLE_COLKEY = "ORG_LIB";
	public static final String ORG_LUCRATIVITE_COLKEY = "ORG_LUCRATIVITE";
	public static final String ORG_NIVEAU_COLKEY = "ORG_NIV";
	public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String ORG_UNIV_COLKEY = "ORG_UNIV";

	public static final String LOG_ORDRE_COLKEY = "LOG_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORG_OP_AUTORISEES_COLKEY = "ORG_OP_AUTORISEES";
	public static final String ORG_PERE_COLKEY = "ORG_PERE";
	public static final String TYOR_ID_COLKEY = "TYOR_ID";


	// Relationships
	public static final String ORGAN_EXERCICE_KEY = "organExercice";
	public static final String ORGAN_FILS_KEY = "organFils";
	public static final String ORGAN_PERE_KEY = "organPere";
	public static final String ORGAN_PRORATAS_KEY = "organProratas";
	public static final String ORGAN_SIGNATAIRES_KEY = "organSignataires";
	public static final String STRUCTURE_ULR_KEY = "structureUlr";
	public static final String TYPE_ETAT_CANAL_OBLIGATOIRE_KEY = "typeEtatCanalObligatoire";
	public static final String TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY = "typeEtatConventionObligatoire";
	public static final String TYPE_ETAT_OP_AUTORISEES_KEY = "typeEtatOpAutorisees";
	public static final String TYPE_ORGAN_KEY = "typeOrgan";
	public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public Integer orgCanalObligatoire() {
    return (Integer) storedValueForKey(ORG_CANAL_OBLIGATOIRE_KEY);
  }

  public void setOrgCanalObligatoire(Integer value) {
    takeStoredValueForKey(value, ORG_CANAL_OBLIGATOIRE_KEY);
  }

  public Integer orgConventionObligatoire() {
    return (Integer) storedValueForKey(ORG_CONVENTION_OBLIGATOIRE_KEY);
  }

  public void setOrgConventionObligatoire(Integer value) {
    takeStoredValueForKey(value, ORG_CONVENTION_OBLIGATOIRE_KEY);
  }

  public String orgCr() {
    return (String) storedValueForKey(ORG_CR_KEY);
  }

  public void setOrgCr(String value) {
    takeStoredValueForKey(value, ORG_CR_KEY);
  }

  public NSTimestamp orgDateCloture() {
    return (NSTimestamp) storedValueForKey(ORG_DATE_CLOTURE_KEY);
  }

  public void setOrgDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, ORG_DATE_CLOTURE_KEY);
  }

  public NSTimestamp orgDateOuverture() {
    return (NSTimestamp) storedValueForKey(ORG_DATE_OUVERTURE_KEY);
  }

  public void setOrgDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, ORG_DATE_OUVERTURE_KEY);
  }

  public String orgEtab() {
    return (String) storedValueForKey(ORG_ETAB_KEY);
  }

  public void setOrgEtab(String value) {
    takeStoredValueForKey(value, ORG_ETAB_KEY);
  }

  public String orgLibelle() {
    return (String) storedValueForKey(ORG_LIBELLE_KEY);
  }

  public void setOrgLibelle(String value) {
    takeStoredValueForKey(value, ORG_LIBELLE_KEY);
  }

  public Integer orgLucrativite() {
    return (Integer) storedValueForKey(ORG_LUCRATIVITE_KEY);
  }

  public void setOrgLucrativite(Integer value) {
    takeStoredValueForKey(value, ORG_LUCRATIVITE_KEY);
  }

  public Integer orgNiveau() {
    return (Integer) storedValueForKey(ORG_NIVEAU_KEY);
  }

  public void setOrgNiveau(Integer value) {
    takeStoredValueForKey(value, ORG_NIVEAU_KEY);
  }

  public String orgSouscr() {
    return (String) storedValueForKey(ORG_SOUSCR_KEY);
  }

  public void setOrgSouscr(String value) {
    takeStoredValueForKey(value, ORG_SOUSCR_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public String orgUniv() {
    return (String) storedValueForKey(ORG_UNIV_KEY);
  }

  public void setOrgUniv(String value) {
    takeStoredValueForKey(value, ORG_UNIV_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOOrgan organPere() {
    return (org.cocktail.fwkcktldepense.client.metier.EOOrgan)storedValueForKey(ORGAN_PERE_KEY);
  }

  public void setOrganPereRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOOrgan oldValue = organPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOStructure structureUlr() {
    return (org.cocktail.fwkcktldepense.client.metier.EOStructure)storedValueForKey(STRUCTURE_ULR_KEY);
  }

  public void setStructureUlrRelationship(org.cocktail.fwkcktldepense.client.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOStructure oldValue = structureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_ULR_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtatCanalObligatoire() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_CANAL_OBLIGATOIRE_KEY);
  }

  public void setTypeEtatCanalObligatoireRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeEtat oldValue = typeEtatCanalObligatoire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_CANAL_OBLIGATOIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_CANAL_OBLIGATOIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtatConventionObligatoire() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY);
  }

  public void setTypeEtatConventionObligatoireRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeEtat oldValue = typeEtatConventionObligatoire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_CONVENTION_OBLIGATOIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeEtat typeEtatOpAutorisees() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_OP_AUTORISEES_KEY);
  }

  public void setTypeEtatOpAutoriseesRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeEtat oldValue = typeEtatOpAutorisees();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_OP_AUTORISEES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_OP_AUTORISEES_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeOrgan typeOrgan() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeOrgan)storedValueForKey(TYPE_ORGAN_KEY);
  }

  public void setTypeOrganRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeOrgan oldValue = typeOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORGAN_KEY);
    }
  }
  
  public NSArray organExercice() {
    return (NSArray)storedValueForKey(ORGAN_EXERCICE_KEY);
  }

  public NSArray organExercice(EOQualifier qualifier) {
    return organExercice(qualifier, null, false);
  }

  public NSArray organExercice(EOQualifier qualifier, boolean fetch) {
    return organExercice(qualifier, null, fetch);
  }

  public NSArray organExercice(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOOrganExercice.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOOrganExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organExercice();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
  }

  public void removeFromOrganExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOOrganExercice createOrganExerciceRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleOrganExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_EXERCICE_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOOrganExercice) eo;
  }

  public void deleteOrganExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_EXERCICE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganExerciceRelationships() {
    Enumeration objects = organExercice().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganExerciceRelationship((org.cocktail.fwkcktldepense.client.metier.EOOrganExercice)objects.nextElement());
    }
  }

  public NSArray organFils() {
    return (NSArray)storedValueForKey(ORGAN_FILS_KEY);
  }

  public NSArray organFils(EOQualifier qualifier) {
    return organFils(qualifier, null, false);
  }

  public NSArray organFils(EOQualifier qualifier, boolean fetch) {
    return organFils(qualifier, null, fetch);
  }

  public NSArray organFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOOrgan.ORGAN_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganFilsRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
  }

  public void removeFromOrganFilsRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOOrgan createOrganFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_FILS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOOrgan) eo;
  }

  public void deleteOrganFilsRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganFilsRelationships() {
    Enumeration objects = organFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganFilsRelationship((org.cocktail.fwkcktldepense.client.metier.EOOrgan)objects.nextElement());
    }
  }

  public NSArray organProratas() {
    return (NSArray)storedValueForKey(ORGAN_PRORATAS_KEY);
  }

  public NSArray organProratas(EOQualifier qualifier) {
    return organProratas(qualifier, null, false);
  }

  public NSArray organProratas(EOQualifier qualifier, boolean fetch) {
    return organProratas(qualifier, null, fetch);
  }

  public NSArray organProratas(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOOrganProrata.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOOrganProrata.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organProratas();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganProratasRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganProrata object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
  }

  public void removeFromOrganProratasRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganProrata object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOOrganProrata createOrganProratasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleOrganProrata");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_PRORATAS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOOrganProrata) eo;
  }

  public void deleteOrganProratasRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganProrata object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_PRORATAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganProratasRelationships() {
    Enumeration objects = organProratas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganProratasRelationship((org.cocktail.fwkcktldepense.client.metier.EOOrganProrata)objects.nextElement());
    }
  }

  public NSArray organSignataires() {
    return (NSArray)storedValueForKey(ORGAN_SIGNATAIRES_KEY);
  }

  public NSArray organSignataires(EOQualifier qualifier) {
    return organSignataires(qualifier, null, false);
  }

  public NSArray organSignataires(EOQualifier qualifier, boolean fetch) {
    return organSignataires(qualifier, null, fetch);
  }

  public NSArray organSignataires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organSignataires();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganSignatairesRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
  }

  public void removeFromOrganSignatairesRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire createOrganSignatairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleOrganSignataire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_SIGNATAIRES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire) eo;
  }

  public void deleteOrganSignatairesRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganSignatairesRelationships() {
    Enumeration objects = organSignataires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganSignatairesRelationship((org.cocktail.fwkcktldepense.client.metier.EOOrganSignataire)objects.nextElement());
    }
  }

  public NSArray utilisateurOrgans() {
    return (NSArray)storedValueForKey(UTILISATEUR_ORGANS_KEY);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier) {
    return utilisateurOrgans(qualifier, null, false);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier, boolean fetch) {
    return utilisateurOrgans(qualifier, null, fetch);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurOrgansRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public void removeFromUtilisateurOrgansRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan createUtilisateurOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleUtilisateurOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_ORGANS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan) eo;
  }

  public void deleteUtilisateurOrgansRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurOrgansRelationships() {
    Enumeration objects = utilisateurOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurOrgansRelationship((org.cocktail.fwkcktldepense.client.metier.EOUtilisateurOrgan)objects.nextElement());
    }
  }


  public static EOOrgan createFwkCaramboleOrgan(EOEditingContext editingContext, NSTimestamp orgDateOuverture
, String orgLibelle
, Integer orgLucrativite
, Integer orgNiveau
, String orgUniv
, org.cocktail.fwkcktldepense.client.metier.EOTypeOrgan typeOrgan) {
    EOOrgan eo = (EOOrgan) createAndInsertInstance(editingContext, _EOOrgan.ENTITY_NAME);    
		eo.setOrgDateOuverture(orgDateOuverture);
		eo.setOrgLibelle(orgLibelle);
		eo.setOrgLucrativite(orgLucrativite);
		eo.setOrgNiveau(orgNiveau);
		eo.setOrgUniv(orgUniv);
    eo.setTypeOrganRelationship(typeOrgan);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOOrgan.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOOrgan.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOOrgan localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrgan)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOOrgan localInstanceIn(EOEditingContext editingContext, EOOrgan eo) {
    EOOrgan localInstance = (eo == null) ? null : (EOOrgan)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOOrgan#localInstanceIn a la place.
   */
	public static EOOrgan localInstanceOf(EOEditingContext editingContext, EOOrgan eo) {
		return EOOrgan.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOOrgan fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOOrgan fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrgan)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrgan)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOOrgan fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrgan eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrgan ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrgan fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
