// _EOUtilisateurPreferenceMail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurPreferenceMail.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOUtilisateurPreferenceMail extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleUtilisateurPreferenceMail";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_UTILISATEUR_PREFERENCE_MAIL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "upId";


// Attributs non visibles
	public static final String PREF_ID_KEY = "prefId";
	public static final String UP_ID_KEY = "upId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_ORDRE_MAIL_KEY = "utlOrdreMail";

//Colonnes dans la base de donnees

	public static final String PREF_ID_COLKEY = "PREF_ID";
	public static final String UP_ID_COLKEY = "UP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UTL_ORDRE_MAIL_COLKEY = "UP_VALUE";


	// Relationships
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String UTILISATEUR_MAIL_KEY = "utilisateurMail";



	// Accessors methods
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateurMail() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_MAIL_KEY);
  }

  public void setUtilisateurMailRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateurMail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_MAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_MAIL_KEY);
    }
  }
  

  public static EOUtilisateurPreferenceMail createFwkCaramboleUtilisateurPreferenceMail(EOEditingContext editingContext, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateurMail) {
    EOUtilisateurPreferenceMail eo = (EOUtilisateurPreferenceMail) createAndInsertInstance(editingContext, _EOUtilisateurPreferenceMail.ENTITY_NAME);    
    eo.setUtilisateurRelationship(utilisateur);
    eo.setUtilisateurMailRelationship(utilisateurMail);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOUtilisateurPreferenceMail.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOUtilisateurPreferenceMail.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOUtilisateurPreferenceMail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurPreferenceMail)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOUtilisateurPreferenceMail localInstanceIn(EOEditingContext editingContext, EOUtilisateurPreferenceMail eo) {
    EOUtilisateurPreferenceMail localInstance = (eo == null) ? null : (EOUtilisateurPreferenceMail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOUtilisateurPreferenceMail#localInstanceIn a la place.
   */
	public static EOUtilisateurPreferenceMail localInstanceOf(EOEditingContext editingContext, EOUtilisateurPreferenceMail eo) {
		return EOUtilisateurPreferenceMail.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOUtilisateurPreferenceMail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOUtilisateurPreferenceMail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurPreferenceMail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurPreferenceMail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateurPreferenceMail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateurPreferenceMail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurPreferenceMail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurPreferenceMail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOUtilisateurPreferenceMail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurPreferenceMail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurPreferenceMail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateurPreferenceMail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
