// _EOReimputationAction.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReimputationAction.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReimputationAction extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputationAction";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION_ACTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "reacId";

	public static final String REAC_HT_SAISIE_KEY = "reacHtSaisie";
	public static final String REAC_MONTANT_BUDGETAIRE_KEY = "reacMontantBudgetaire";
	public static final String REAC_TTC_SAISIE_KEY = "reacTtcSaisie";
	public static final String REAC_TVA_SAISIE_KEY = "reacTvaSaisie";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String REAC_ID_KEY = "reacId";
	public static final String REIM_ID_KEY = "reimId";
	public static final String TYAC_ID_KEY = "tyacId";

//Colonnes dans la base de donnees
	public static final String REAC_HT_SAISIE_COLKEY = "REAC_HT_SAISIE";
	public static final String REAC_MONTANT_BUDGETAIRE_COLKEY = "REAC_MONTANT_BUDGETAIRE";
	public static final String REAC_TTC_SAISIE_COLKEY = "REAC_TTC_SAISIE";
	public static final String REAC_TVA_SAISIE_COLKEY = "REAC_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String REAC_ID_COLKEY = "REAC_ID";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String REIMPUTATION_KEY = "reimputation";
	public static final String TYPE_ACTION_KEY = "typeAction";



	// Accessors methods
  public java.math.BigDecimal reacHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REAC_HT_SAISIE_KEY);
  }

  public void setReacHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REAC_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal reacMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(REAC_MONTANT_BUDGETAIRE_KEY);
  }

  public void setReacMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REAC_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal reacTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REAC_TTC_SAISIE_KEY);
  }

  public void setReacTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REAC_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal reacTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REAC_TVA_SAISIE_KEY);
  }

  public void setReacTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REAC_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation() {
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputation)storedValueForKey(REIMPUTATION_KEY);
  }

  public void setReimputationRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputation value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOReimputation oldValue = reimputation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REIMPUTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REIMPUTATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  

  public static EOReimputationAction createFwkCaramboleReimputationAction(EOEditingContext editingContext, java.math.BigDecimal reacHtSaisie
, java.math.BigDecimal reacMontantBudgetaire
, java.math.BigDecimal reacTtcSaisie
, java.math.BigDecimal reacTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOReimputation reimputation, org.cocktail.fwkcktldepense.client.metier.EOTypeAction typeAction) {
    EOReimputationAction eo = (EOReimputationAction) createAndInsertInstance(editingContext, _EOReimputationAction.ENTITY_NAME);    
		eo.setReacHtSaisie(reacHtSaisie);
		eo.setReacMontantBudgetaire(reacMontantBudgetaire);
		eo.setReacTtcSaisie(reacTtcSaisie);
		eo.setReacTvaSaisie(reacTvaSaisie);
    eo.setExerciceRelationship(exercice);
    eo.setReimputationRelationship(reimputation);
    eo.setTypeActionRelationship(typeAction);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOReimputationAction.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOReimputationAction.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOReimputationAction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReimputationAction)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOReimputationAction localInstanceIn(EOEditingContext editingContext, EOReimputationAction eo) {
    EOReimputationAction localInstance = (eo == null) ? null : (EOReimputationAction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOReimputationAction#localInstanceIn a la place.
   */
	public static EOReimputationAction localInstanceOf(EOEditingContext editingContext, EOReimputationAction eo) {
		return EOReimputationAction.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOReimputationAction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOReimputationAction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputationAction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputationAction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputationAction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputationAction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputationAction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputationAction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOReimputationAction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputationAction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputationAction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputationAction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
