// _EOReimputation.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReimputation.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReimputation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleReimputation";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.REIMPUTATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "reimId";

	public static final String REIM_DATE_KEY = "reimDate";
	public static final String REIM_LIBELLE_KEY = "reimLibelle";
	public static final String REIM_NUMERO_KEY = "reimNumero";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String REIM_ID_KEY = "reimId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String REIM_DATE_COLKEY = "REIM_DATE";
	public static final String REIM_LIBELLE_COLKEY = "REIM_LIBELLE";
	public static final String REIM_NUMERO_COLKEY = "REIM_NUMERO";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String REIM_ID_COLKEY = "REIM_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_BUDGET_KEY = "depenseBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String REIMPUTATION_ACTIONS_KEY = "reimputationActions";
	public static final String REIMPUTATION_ANALYTIQUES_KEY = "reimputationAnalytiques";
	public static final String REIMPUTATION_BUDGETS_KEY = "reimputationBudgets";
	public static final String REIMPUTATION_CONVENTIONS_KEY = "reimputationConventions";
	public static final String REIMPUTATION_HORS_MARCHES_KEY = "reimputationHorsMarches";
	public static final String REIMPUTATION_MARCHES_KEY = "reimputationMarches";
	public static final String REIMPUTATION_NEW_ACTIONS_KEY = "reimputationNewActions";
	public static final String REIMPUTATION_NEW_ANALYTIQUES_KEY = "reimputationNewAnalytiques";
	public static final String REIMPUTATION_NEW_BUDGETS_KEY = "reimputationNewBudgets";
	public static final String REIMPUTATION_NEW_CONVENTIONS_KEY = "reimputationNewConventions";
	public static final String REIMPUTATION_NEW_HORS_MARCHES_KEY = "reimputationNewHorsMarches";
	public static final String REIMPUTATION_NEW_MARCHES_KEY = "reimputationNewMarches";
	public static final String REIMPUTATION_NEW_PLANCOS_KEY = "reimputationNewPlancos";
	public static final String REIMPUTATION_PLANCOS_KEY = "reimputationPlancos";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp reimDate() {
    return (NSTimestamp) storedValueForKey(REIM_DATE_KEY);
  }

  public void setReimDate(NSTimestamp value) {
    takeStoredValueForKey(value, REIM_DATE_KEY);
  }

  public String reimLibelle() {
    return (String) storedValueForKey(REIM_LIBELLE_KEY);
  }

  public void setReimLibelle(String value) {
    takeStoredValueForKey(value, REIM_LIBELLE_KEY);
  }

  public Integer reimNumero() {
    return (Integer) storedValueForKey(REIM_NUMERO_KEY);
  }

  public void setReimNumero(Integer value) {
    takeStoredValueForKey(value, REIM_NUMERO_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseBudget depenseBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_KEY);
  }

  public void setDepenseBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODepenseBudget oldValue = depenseBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray reimputationActions() {
    return (NSArray)storedValueForKey(REIMPUTATION_ACTIONS_KEY);
  }

  public NSArray reimputationActions(EOQualifier qualifier) {
    return reimputationActions(qualifier, null, false);
  }

  public NSArray reimputationActions(EOQualifier qualifier, boolean fetch) {
    return reimputationActions(qualifier, null, fetch);
  }

  public NSArray reimputationActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationAction.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_ACTIONS_KEY);
  }

  public void removeFromReimputationActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ACTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationAction createReimputationActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_ACTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationAction) eo;
  }

  public void deleteReimputationActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationActionsRelationships() {
    Enumeration objects = reimputationActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationActionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationAction)objects.nextElement());
    }
  }

  public NSArray reimputationAnalytiques() {
    return (NSArray)storedValueForKey(REIMPUTATION_ANALYTIQUES_KEY);
  }

  public NSArray reimputationAnalytiques(EOQualifier qualifier) {
    return reimputationAnalytiques(qualifier, null, false);
  }

  public NSArray reimputationAnalytiques(EOQualifier qualifier, boolean fetch) {
    return reimputationAnalytiques(qualifier, null, fetch);
  }

  public NSArray reimputationAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_ANALYTIQUES_KEY);
  }

  public void removeFromReimputationAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ANALYTIQUES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique createReimputationAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_ANALYTIQUES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique) eo;
  }

  public void deleteReimputationAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationAnalytiquesRelationships() {
    Enumeration objects = reimputationAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationAnalytiquesRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationAnalytique)objects.nextElement());
    }
  }

  public NSArray reimputationBudgets() {
    return (NSArray)storedValueForKey(REIMPUTATION_BUDGETS_KEY);
  }

  public NSArray reimputationBudgets(EOQualifier qualifier) {
    return reimputationBudgets(qualifier, null, false);
  }

  public NSArray reimputationBudgets(EOQualifier qualifier, boolean fetch) {
    return reimputationBudgets(qualifier, null, fetch);
  }

  public NSArray reimputationBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_BUDGETS_KEY);
  }

  public void removeFromReimputationBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_BUDGETS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget createReimputationBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_BUDGETS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget) eo;
  }

  public void deleteReimputationBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationBudgetsRelationships() {
    Enumeration objects = reimputationBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationBudgetsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationBudget)objects.nextElement());
    }
  }

  public NSArray reimputationConventions() {
    return (NSArray)storedValueForKey(REIMPUTATION_CONVENTIONS_KEY);
  }

  public NSArray reimputationConventions(EOQualifier qualifier) {
    return reimputationConventions(qualifier, null, false);
  }

  public NSArray reimputationConventions(EOQualifier qualifier, boolean fetch) {
    return reimputationConventions(qualifier, null, fetch);
  }

  public NSArray reimputationConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_CONVENTIONS_KEY);
  }

  public void removeFromReimputationConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_CONVENTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention createReimputationConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention) eo;
  }

  public void deleteReimputationConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationConventionsRelationships() {
    Enumeration objects = reimputationConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationConventionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationConvention)objects.nextElement());
    }
  }

  public NSArray reimputationHorsMarches() {
    return (NSArray)storedValueForKey(REIMPUTATION_HORS_MARCHES_KEY);
  }

  public NSArray reimputationHorsMarches(EOQualifier qualifier) {
    return reimputationHorsMarches(qualifier, null, false);
  }

  public NSArray reimputationHorsMarches(EOQualifier qualifier, boolean fetch) {
    return reimputationHorsMarches(qualifier, null, fetch);
  }

  public NSArray reimputationHorsMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationHorsMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_HORS_MARCHES_KEY);
  }

  public void removeFromReimputationHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_HORS_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche createReimputationHorsMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationHorsMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_HORS_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche) eo;
  }

  public void deleteReimputationHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_HORS_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationHorsMarchesRelationships() {
    Enumeration objects = reimputationHorsMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationHorsMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationHorsMarche)objects.nextElement());
    }
  }

  public NSArray reimputationMarches() {
    return (NSArray)storedValueForKey(REIMPUTATION_MARCHES_KEY);
  }

  public NSArray reimputationMarches(EOQualifier qualifier) {
    return reimputationMarches(qualifier, null, false);
  }

  public NSArray reimputationMarches(EOQualifier qualifier, boolean fetch) {
    return reimputationMarches(qualifier, null, fetch);
  }

  public NSArray reimputationMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_MARCHES_KEY);
  }

  public void removeFromReimputationMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche createReimputationMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche) eo;
  }

  public void deleteReimputationMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationMarchesRelationships() {
    Enumeration objects = reimputationMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationMarche)objects.nextElement());
    }
  }

  public NSArray reimputationNewActions() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_ACTIONS_KEY);
  }

  public NSArray reimputationNewActions(EOQualifier qualifier) {
    return reimputationNewActions(qualifier, null, false);
  }

  public NSArray reimputationNewActions(EOQualifier qualifier, boolean fetch) {
    return reimputationNewActions(qualifier, null, fetch);
  }

  public NSArray reimputationNewActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ACTIONS_KEY);
  }

  public void removeFromReimputationNewActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ACTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction createReimputationNewActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_ACTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction) eo;
  }

  public void deleteReimputationNewActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewActionsRelationships() {
    Enumeration objects = reimputationNewActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewActionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAction)objects.nextElement());
    }
  }

  public NSArray reimputationNewAnalytiques() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_ANALYTIQUES_KEY);
  }

  public NSArray reimputationNewAnalytiques(EOQualifier qualifier) {
    return reimputationNewAnalytiques(qualifier, null, false);
  }

  public NSArray reimputationNewAnalytiques(EOQualifier qualifier, boolean fetch) {
    return reimputationNewAnalytiques(qualifier, null, fetch);
  }

  public NSArray reimputationNewAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ANALYTIQUES_KEY);
  }

  public void removeFromReimputationNewAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ANALYTIQUES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique createReimputationNewAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_ANALYTIQUES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique) eo;
  }

  public void deleteReimputationNewAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewAnalytiquesRelationships() {
    Enumeration objects = reimputationNewAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewAnalytiquesRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewAnalytique)objects.nextElement());
    }
  }

  public NSArray reimputationNewBudgets() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_BUDGETS_KEY);
  }

  public NSArray reimputationNewBudgets(EOQualifier qualifier) {
    return reimputationNewBudgets(qualifier, null, false);
  }

  public NSArray reimputationNewBudgets(EOQualifier qualifier, boolean fetch) {
    return reimputationNewBudgets(qualifier, null, fetch);
  }

  public NSArray reimputationNewBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_BUDGETS_KEY);
  }

  public void removeFromReimputationNewBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_BUDGETS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget createReimputationNewBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_BUDGETS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget) eo;
  }

  public void deleteReimputationNewBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewBudgetsRelationships() {
    Enumeration objects = reimputationNewBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewBudgetsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewBudget)objects.nextElement());
    }
  }

  public NSArray reimputationNewConventions() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_CONVENTIONS_KEY);
  }

  public NSArray reimputationNewConventions(EOQualifier qualifier) {
    return reimputationNewConventions(qualifier, null, false);
  }

  public NSArray reimputationNewConventions(EOQualifier qualifier, boolean fetch) {
    return reimputationNewConventions(qualifier, null, fetch);
  }

  public NSArray reimputationNewConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_CONVENTIONS_KEY);
  }

  public void removeFromReimputationNewConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_CONVENTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention createReimputationNewConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention) eo;
  }

  public void deleteReimputationNewConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewConventionsRelationships() {
    Enumeration objects = reimputationNewConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewConventionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewConvention)objects.nextElement());
    }
  }

  public NSArray reimputationNewHorsMarches() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_HORS_MARCHES_KEY);
  }

  public NSArray reimputationNewHorsMarches(EOQualifier qualifier) {
    return reimputationNewHorsMarches(qualifier, null, false);
  }

  public NSArray reimputationNewHorsMarches(EOQualifier qualifier, boolean fetch) {
    return reimputationNewHorsMarches(qualifier, null, fetch);
  }

  public NSArray reimputationNewHorsMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewHorsMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_HORS_MARCHES_KEY);
  }

  public void removeFromReimputationNewHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_HORS_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche createReimputationNewHorsMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewHorsMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_HORS_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche) eo;
  }

  public void deleteReimputationNewHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_HORS_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewHorsMarchesRelationships() {
    Enumeration objects = reimputationNewHorsMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewHorsMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewHorsMarche)objects.nextElement());
    }
  }

  public NSArray reimputationNewMarches() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_MARCHES_KEY);
  }

  public NSArray reimputationNewMarches(EOQualifier qualifier) {
    return reimputationNewMarches(qualifier, null, false);
  }

  public NSArray reimputationNewMarches(EOQualifier qualifier, boolean fetch) {
    return reimputationNewMarches(qualifier, null, fetch);
  }

  public NSArray reimputationNewMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_MARCHES_KEY);
  }

  public void removeFromReimputationNewMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche createReimputationNewMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche) eo;
  }

  public void deleteReimputationNewMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewMarchesRelationships() {
    Enumeration objects = reimputationNewMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewMarche)objects.nextElement());
    }
  }

  public NSArray reimputationNewPlancos() {
    return (NSArray)storedValueForKey(REIMPUTATION_NEW_PLANCOS_KEY);
  }

  public NSArray reimputationNewPlancos(EOQualifier qualifier) {
    return reimputationNewPlancos(qualifier, null, false);
  }

  public NSArray reimputationNewPlancos(EOQualifier qualifier, boolean fetch) {
    return reimputationNewPlancos(qualifier, null, fetch);
  }

  public NSArray reimputationNewPlancos(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationNewPlancos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationNewPlancosRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_PLANCOS_KEY);
  }

  public void removeFromReimputationNewPlancosRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_PLANCOS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco createReimputationNewPlancosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationNewPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_NEW_PLANCOS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco) eo;
  }

  public void deleteReimputationNewPlancosRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_NEW_PLANCOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationNewPlancosRelationships() {
    Enumeration objects = reimputationNewPlancos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationNewPlancosRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationNewPlanco)objects.nextElement());
    }
  }

  public NSArray reimputationPlancos() {
    return (NSArray)storedValueForKey(REIMPUTATION_PLANCOS_KEY);
  }

  public NSArray reimputationPlancos(EOQualifier qualifier) {
    return reimputationPlancos(qualifier, null, false);
  }

  public NSArray reimputationPlancos(EOQualifier qualifier, boolean fetch) {
    return reimputationPlancos(qualifier, null, fetch);
  }

  public NSArray reimputationPlancos(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco.REIMPUTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputationPlancos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationPlancosRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATION_PLANCOS_KEY);
  }

  public void removeFromReimputationPlancosRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_PLANCOS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco createReimputationPlancosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleReimputationPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATION_PLANCOS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco) eo;
  }

  public void deleteReimputationPlancosRelationship(org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATION_PLANCOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationPlancosRelationships() {
    Enumeration objects = reimputationPlancos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationPlancosRelationship((org.cocktail.fwkcktldepense.client.metier.EOReimputationPlanco)objects.nextElement());
    }
  }


  public static EOReimputation createFwkCaramboleReimputation(EOEditingContext editingContext, NSTimestamp reimDate
, Integer reimNumero
, org.cocktail.fwkcktldepense.client.metier.EODepenseBudget depenseBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur) {
    EOReimputation eo = (EOReimputation) createAndInsertInstance(editingContext, _EOReimputation.ENTITY_NAME);    
		eo.setReimDate(reimDate);
		eo.setReimNumero(reimNumero);
    eo.setDepenseBudgetRelationship(depenseBudget);
    eo.setExerciceRelationship(exercice);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOReimputation.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOReimputation.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOReimputation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReimputation)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOReimputation localInstanceIn(EOEditingContext editingContext, EOReimputation eo) {
    EOReimputation localInstance = (eo == null) ? null : (EOReimputation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOReimputation#localInstanceIn a la place.
   */
	public static EOReimputation localInstanceOf(EOEditingContext editingContext, EOReimputation eo) {
		return EOReimputation.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOReimputation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOReimputation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOReimputation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
