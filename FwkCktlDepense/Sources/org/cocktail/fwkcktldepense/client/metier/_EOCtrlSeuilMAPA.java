// _EOCtrlSeuilMAPA.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCtrlSeuilMAPA.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCtrlSeuilMAPA extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCtrlSeuilMAPA";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CTRL_SEUIL_MAPA";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ceOrdre";

	public static final String CM_CODE_KEY = "cmCode";
	public static final String CM_CODE_FAM_KEY = "cmCodeFam";
	public static final String CM_LIB_KEY = "cmLib";
	public static final String CM_LIB_FAM_KEY = "cmLibFam";
	public static final String MONTANT_ENGAGE_HT_KEY = "montantEngageHt";
	public static final String SEUIL_MIN_KEY = "seuilMin";

// Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CM_CODE_COLKEY = "CM_CODE";
	public static final String CM_CODE_FAM_COLKEY = "CM_CODE_FAM";
	public static final String CM_LIB_COLKEY = "CM_LIB";
	public static final String CM_LIB_FAM_COLKEY = "CM_LIB_FAM";
	public static final String MONTANT_ENGAGE_HT_COLKEY = "MONTANT_HT";
	public static final String SEUIL_MIN_COLKEY = "SEUIL_MIN";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public String cmCode() {
    return (String) storedValueForKey(CM_CODE_KEY);
  }

  public void setCmCode(String value) {
    takeStoredValueForKey(value, CM_CODE_KEY);
  }

  public String cmCodeFam() {
    return (String) storedValueForKey(CM_CODE_FAM_KEY);
  }

  public void setCmCodeFam(String value) {
    takeStoredValueForKey(value, CM_CODE_FAM_KEY);
  }

  public String cmLib() {
    return (String) storedValueForKey(CM_LIB_KEY);
  }

  public void setCmLib(String value) {
    takeStoredValueForKey(value, CM_LIB_KEY);
  }

  public String cmLibFam() {
    return (String) storedValueForKey(CM_LIB_FAM_KEY);
  }

  public void setCmLibFam(String value) {
    takeStoredValueForKey(value, CM_LIB_FAM_KEY);
  }

  public java.math.BigDecimal montantEngageHt() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_ENGAGE_HT_KEY);
  }

  public void setMontantEngageHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_ENGAGE_HT_KEY);
  }

  public java.math.BigDecimal seuilMin() {
    return (java.math.BigDecimal) storedValueForKey(SEUIL_MIN_KEY);
  }

  public void setSeuilMin(java.math.BigDecimal value) {
    takeStoredValueForKey(value, SEUIL_MIN_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

  public static EOCtrlSeuilMAPA createFwkCaramboleCtrlSeuilMAPA(EOEditingContext editingContext, String cmCode
, String cmCodeFam
, String cmLib
, String cmLibFam
, java.math.BigDecimal montantEngageHt
, java.math.BigDecimal seuilMin
, org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice) {
    EOCtrlSeuilMAPA eo = (EOCtrlSeuilMAPA) createAndInsertInstance(editingContext, _EOCtrlSeuilMAPA.ENTITY_NAME);    
		eo.setCmCode(cmCode);
		eo.setCmCodeFam(cmCodeFam);
		eo.setCmLib(cmLib);
		eo.setCmLibFam(cmLibFam);
		eo.setMontantEngageHt(montantEngageHt);
		eo.setSeuilMin(seuilMin);
    eo.setCodeExerRelationship(codeExer);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCtrlSeuilMAPA.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCtrlSeuilMAPA.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCtrlSeuilMAPA localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCtrlSeuilMAPA)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCtrlSeuilMAPA localInstanceIn(EOEditingContext editingContext, EOCtrlSeuilMAPA eo) {
    EOCtrlSeuilMAPA localInstance = (eo == null) ? null : (EOCtrlSeuilMAPA)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCtrlSeuilMAPA#localInstanceIn a la place.
   */
	public static EOCtrlSeuilMAPA localInstanceOf(EOEditingContext editingContext, EOCtrlSeuilMAPA eo) {
		return EOCtrlSeuilMAPA.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCtrlSeuilMAPA fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCtrlSeuilMAPA fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCtrlSeuilMAPA eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCtrlSeuilMAPA)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCtrlSeuilMAPA fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCtrlSeuilMAPA fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCtrlSeuilMAPA eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCtrlSeuilMAPA)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCtrlSeuilMAPA fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCtrlSeuilMAPA eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCtrlSeuilMAPA ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCtrlSeuilMAPA fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
