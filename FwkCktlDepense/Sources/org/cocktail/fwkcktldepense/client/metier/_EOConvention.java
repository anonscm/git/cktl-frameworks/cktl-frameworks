// _EOConvention.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOConvention.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOConvention extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleConvention";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CONVENTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "convOrdre";

	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String CONV_REFERENCE_KEY = "convReference";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String CONV_REFERENCE_COLKEY = "CONV_REFERENCE";



	// Relationships
	public static final String CONVENTION_NON_LIMITATIVES_KEY = "conventionNonLimitatives";



	// Accessors methods
  public Integer convOrdre() {
    return (Integer) storedValueForKey(CONV_ORDRE_KEY);
  }

  public void setConvOrdre(Integer value) {
    takeStoredValueForKey(value, CONV_ORDRE_KEY);
  }

  public String convReference() {
    return (String) storedValueForKey(CONV_REFERENCE_KEY);
  }

  public void setConvReference(String value) {
    takeStoredValueForKey(value, CONV_REFERENCE_KEY);
  }

  public NSArray conventionNonLimitatives() {
    return (NSArray)storedValueForKey(CONVENTION_NON_LIMITATIVES_KEY);
  }

  public NSArray conventionNonLimitatives(EOQualifier qualifier) {
    return conventionNonLimitatives(qualifier, null, false);
  }

  public NSArray conventionNonLimitatives(EOQualifier qualifier, boolean fetch) {
    return conventionNonLimitatives(qualifier, null, fetch);
  }

  public NSArray conventionNonLimitatives(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative.CONVENTION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = conventionNonLimitatives();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToConventionNonLimitativesRelationship(org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
  }

  public void removeFromConventionNonLimitativesRelationship(org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative createConventionNonLimitativesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleConventionNonLimitative");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONVENTION_NON_LIMITATIVES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative) eo;
  }

  public void deleteConventionNonLimitativesRelationship(org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONVENTION_NON_LIMITATIVES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllConventionNonLimitativesRelationships() {
    Enumeration objects = conventionNonLimitatives().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteConventionNonLimitativesRelationship((org.cocktail.fwkcktldepense.client.metier.EOConventionNonLimitative)objects.nextElement());
    }
  }


  public static EOConvention createFwkCaramboleConvention(EOEditingContext editingContext, Integer convOrdre
, String convReference
) {
    EOConvention eo = (EOConvention) createAndInsertInstance(editingContext, _EOConvention.ENTITY_NAME);    
		eo.setConvOrdre(convOrdre);
		eo.setConvReference(convReference);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOConvention.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOConvention.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOConvention)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOConvention localInstanceIn(EOEditingContext editingContext, EOConvention eo) {
    EOConvention localInstance = (eo == null) ? null : (EOConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOConvention#localInstanceIn a la place.
   */
	public static EOConvention localInstanceOf(EOEditingContext editingContext, EOConvention eo) {
		return EOConvention.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
