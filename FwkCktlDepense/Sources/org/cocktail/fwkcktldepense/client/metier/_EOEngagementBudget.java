// _EOEngagementBudget.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngagementBudget.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngagementBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleEngagementBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.ENGAGE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "engId";

	public static final String ENG_DATE_SAISIE_KEY = "engDateSaisie";
	public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
	public static final String ENG_LIBELLE_KEY = "engLibelle";
	public static final String ENG_MONTANT_BUDGETAIRE_KEY = "engMontantBudgetaire";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_KEY = "engMontantBudgetaireReste";
	public static final String ENG_NUMERO_KEY = "engNumero";
	public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
	public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";

// Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ENG_DATE_SAISIE_COLKEY = "ENG_DATE_SAISIE";
	public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
	public static final String ENG_LIBELLE_COLKEY = "ENG_LIBELLE";
	public static final String ENG_MONTANT_BUDGETAIRE_COLKEY = "ENG_MONTANT_BUDGETAIRE";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ENG_MONTANT_BUDGETAIRE_RESTE";
	public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
	public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
	public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String COMMANDE_ENGAGEMENTS_KEY = "commandeEngagements";
	public static final String DEPENSE_BUDGETS_KEY = "depenseBudgets";
	public static final String ENGAGEMENT_CONTROLE_ACTIONS_KEY = "engagementControleActions";
	public static final String ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY = "engagementControleAnalytiques";
	public static final String ENGAGEMENT_CONTROLE_CONVENTIONS_KEY = "engagementControleConventions";
	public static final String ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY = "engagementControleHorsMarches";
	public static final String ENGAGEMENT_CONTROLE_MARCHES_KEY = "engagementControleMarches";
	public static final String ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY = "engagementControlePlanComptables";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String ORGAN_KEY = "organ";
	public static final String PRE_DEPENSE_BUDGETS_KEY = "preDepenseBudgets";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp engDateSaisie() {
    return (NSTimestamp) storedValueForKey(ENG_DATE_SAISIE_KEY);
  }

  public void setEngDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ENG_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal engHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_HT_SAISIE_KEY);
  }

  public void setEngHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_HT_SAISIE_KEY);
  }

  public String engLibelle() {
    return (String) storedValueForKey(ENG_LIBELLE_KEY);
  }

  public void setEngLibelle(String value) {
    takeStoredValueForKey(value, ENG_LIBELLE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEngMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEngMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public Integer engNumero() {
    return (Integer) storedValueForKey(ENG_NUMERO_KEY);
  }

  public void setEngNumero(Integer value) {
    takeStoredValueForKey(value, ENG_NUMERO_KEY);
  }

  public java.math.BigDecimal engTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TTC_SAISIE_KEY);
  }

  public void setEngTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal engTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TVA_SAISIE_KEY);
  }

  public void setEngTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.fwkcktldepense.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktldepense.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.client.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray commandeEngagements() {
    return (NSArray)storedValueForKey(COMMANDE_ENGAGEMENTS_KEY);
  }

  public NSArray commandeEngagements(EOQualifier qualifier) {
    return commandeEngagements(qualifier, null, false);
  }

  public NSArray commandeEngagements(EOQualifier qualifier, boolean fetch) {
    return commandeEngagements(qualifier, null, fetch);
  }

  public NSArray commandeEngagements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeEngagements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
  }

  public void removeFromCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement createCommandeEngagementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeEngagement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_ENGAGEMENTS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement) eo;
  }

  public void deleteCommandeEngagementsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_ENGAGEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeEngagementsRelationships() {
    Enumeration objects = commandeEngagements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeEngagementsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeEngagement)objects.nextElement());
    }
  }

  public NSArray depenseBudgets() {
    return (NSArray)storedValueForKey(DEPENSE_BUDGETS_KEY);
  }

  public NSArray depenseBudgets(EOQualifier qualifier) {
    return depenseBudgets(qualifier, null, false);
  }

  public NSArray depenseBudgets(EOQualifier qualifier, boolean fetch) {
    return depenseBudgets(qualifier, null, fetch);
  }

  public NSArray depenseBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EODepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenseBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
  }

  public void removeFromDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseBudget createDepenseBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleDepenseBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSE_BUDGETS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseBudget) eo;
  }

  public void deleteDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSE_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseBudgetsRelationships() {
    Enumeration objects = depenseBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseBudgetsRelationship((org.cocktail.fwkcktldepense.client.metier.EODepenseBudget)objects.nextElement());
    }
  }

  public NSArray engagementControleActions() {
    return (NSArray)storedValueForKey(ENGAGEMENT_CONTROLE_ACTIONS_KEY);
  }

  public NSArray engagementControleActions(EOQualifier qualifier) {
    return engagementControleActions(qualifier, null, false);
  }

  public NSArray engagementControleActions(EOQualifier qualifier, boolean fetch) {
    return engagementControleActions(qualifier, null, fetch);
  }

  public NSArray engagementControleActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagementControleActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
  }

  public void removeFromEngagementControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction createEngagementControleActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleEngagementControleAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction) eo;
  }

  public void deleteEngagementControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControleActionsRelationships() {
    Enumeration objects = engagementControleActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControleActionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAction)objects.nextElement());
    }
  }

  public NSArray engagementControleAnalytiques() {
    return (NSArray)storedValueForKey(ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
  }

  public NSArray engagementControleAnalytiques(EOQualifier qualifier) {
    return engagementControleAnalytiques(qualifier, null, false);
  }

  public NSArray engagementControleAnalytiques(EOQualifier qualifier, boolean fetch) {
    return engagementControleAnalytiques(qualifier, null, fetch);
  }

  public NSArray engagementControleAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagementControleAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
  }

  public void removeFromEngagementControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique createEngagementControleAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleEngagementControleAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique) eo;
  }

  public void deleteEngagementControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControleAnalytiquesRelationships() {
    Enumeration objects = engagementControleAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControleAnalytiquesRelationship((org.cocktail.fwkcktldepense.client.metier.EOEngagementControleAnalytique)objects.nextElement());
    }
  }

  public NSArray engagementControleConventions() {
    return (NSArray)storedValueForKey(ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
  }

  public NSArray engagementControleConventions(EOQualifier qualifier) {
    return engagementControleConventions(qualifier, null, false);
  }

  public NSArray engagementControleConventions(EOQualifier qualifier, boolean fetch) {
    return engagementControleConventions(qualifier, null, fetch);
  }

  public NSArray engagementControleConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagementControleConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
  }

  public void removeFromEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention createEngagementControleConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleEngagementControleConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention) eo;
  }

  public void deleteEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControleConventionsRelationships() {
    Enumeration objects = engagementControleConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControleConventionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOEngagementControleConvention)objects.nextElement());
    }
  }

  public NSArray engagementControleHorsMarches() {
    return (NSArray)storedValueForKey(ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
  }

  public NSArray engagementControleHorsMarches(EOQualifier qualifier) {
    return engagementControleHorsMarches(qualifier, null, false);
  }

  public NSArray engagementControleHorsMarches(EOQualifier qualifier, boolean fetch) {
    return engagementControleHorsMarches(qualifier, null, fetch);
  }

  public NSArray engagementControleHorsMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagementControleHorsMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
  }

  public void removeFromEngagementControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche createEngagementControleHorsMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleEngagementControleHorsMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche) eo;
  }

  public void deleteEngagementControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_HORS_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControleHorsMarchesRelationships() {
    Enumeration objects = engagementControleHorsMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControleHorsMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOEngagementControleHorsMarche)objects.nextElement());
    }
  }

  public NSArray engagementControleMarches() {
    return (NSArray)storedValueForKey(ENGAGEMENT_CONTROLE_MARCHES_KEY);
  }

  public NSArray engagementControleMarches(EOQualifier qualifier) {
    return engagementControleMarches(qualifier, null, false);
  }

  public NSArray engagementControleMarches(EOQualifier qualifier, boolean fetch) {
    return engagementControleMarches(qualifier, null, fetch);
  }

  public NSArray engagementControleMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagementControleMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_MARCHES_KEY);
  }

  public void removeFromEngagementControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche createEngagementControleMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleEngagementControleMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche) eo;
  }

  public void deleteEngagementControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControleMarchesRelationships() {
    Enumeration objects = engagementControleMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControleMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOEngagementControleMarche)objects.nextElement());
    }
  }

  public NSArray engagementControlePlanComptables() {
    return (NSArray)storedValueForKey(ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public NSArray engagementControlePlanComptables(EOQualifier qualifier) {
    return engagementControlePlanComptables(qualifier, null, false);
  }

  public NSArray engagementControlePlanComptables(EOQualifier qualifier, boolean fetch) {
    return engagementControlePlanComptables(qualifier, null, fetch);
  }

  public NSArray engagementControlePlanComptables(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = engagementControlePlanComptables();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEngagementControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public void removeFromEngagementControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable createEngagementControlePlanComptablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleEngagementControlePlanComptable");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable) eo;
  }

  public void deleteEngagementControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGEMENT_CONTROLE_PLAN_COMPTABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControlePlanComptablesRelationships() {
    Enumeration objects = engagementControlePlanComptables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControlePlanComptablesRelationship((org.cocktail.fwkcktldepense.client.metier.EOEngagementControlePlanComptable)objects.nextElement());
    }
  }

  public NSArray preDepenseBudgets() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_BUDGETS_KEY);
  }

  public NSArray preDepenseBudgets(EOQualifier qualifier) {
    return preDepenseBudgets(qualifier, null, false);
  }

  public NSArray preDepenseBudgets(EOQualifier qualifier, boolean fetch) {
    return preDepenseBudgets(qualifier, null, fetch);
  }

  public NSArray preDepenseBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget.ENGAGEMENT_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
  }

  public void removeFromPreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget createPreDepenseBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_BUDGETS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget) eo;
  }

  public void deletePreDepenseBudgetsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseBudgetsRelationships() {
    Enumeration objects = preDepenseBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseBudgetsRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseBudget)objects.nextElement());
    }
  }


  public static EOEngagementBudget createFwkCaramboleEngagementBudget(EOEditingContext editingContext, NSTimestamp engDateSaisie
, java.math.BigDecimal engHtSaisie
, String engLibelle
, java.math.BigDecimal engMontantBudgetaire
, java.math.BigDecimal engMontantBudgetaireReste
, java.math.BigDecimal engTtcSaisie
, java.math.BigDecimal engTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur, org.cocktail.fwkcktldepense.client.metier.EOOrgan organ, org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata, org.cocktail.fwkcktldepense.client.metier.EOTypeApplication typeApplication, org.cocktail.fwkcktldepense.client.metier.EOTypeCredit typeCredit, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur) {
    EOEngagementBudget eo = (EOEngagementBudget) createAndInsertInstance(editingContext, _EOEngagementBudget.ENTITY_NAME);    
		eo.setEngDateSaisie(engDateSaisie);
		eo.setEngHtSaisie(engHtSaisie);
		eo.setEngLibelle(engLibelle);
		eo.setEngMontantBudgetaire(engMontantBudgetaire);
		eo.setEngMontantBudgetaireReste(engMontantBudgetaireReste);
		eo.setEngTtcSaisie(engTtcSaisie);
		eo.setEngTvaSaisie(engTvaSaisie);
    eo.setExerciceRelationship(exercice);
    eo.setFournisseurRelationship(fournisseur);
    eo.setOrganRelationship(organ);
    eo.setTauxProrataRelationship(tauxProrata);
    eo.setTypeApplicationRelationship(typeApplication);
    eo.setTypeCreditRelationship(typeCredit);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngagementBudget.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngagementBudget.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOEngagementBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngagementBudget)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEngagementBudget localInstanceIn(EOEditingContext editingContext, EOEngagementBudget eo) {
    EOEngagementBudget localInstance = (eo == null) ? null : (EOEngagementBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngagementBudget#localInstanceIn a la place.
   */
	public static EOEngagementBudget localInstanceOf(EOEditingContext editingContext, EOEngagementBudget eo) {
		return EOEngagementBudget.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEngagementBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngagementBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngagementBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngagementBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngagementBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngagementBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngagementBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngagementBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngagementBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngagementBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngagementBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngagementBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
