// _EOCommandeBudget.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommandeBudget.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCommandeBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cbudId";

	public static final String CBUD_HT_SAISIE_KEY = "cbudHtSaisie";
	public static final String CBUD_MONTANT_BUDGETAIRE_KEY = "cbudMontantBudgetaire";
	public static final String CBUD_TTC_SAISIE_KEY = "cbudTtcSaisie";
	public static final String CBUD_TVA_SAISIE_KEY = "cbudTvaSaisie";

// Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String COMM_ID_KEY = "commId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CBUD_HT_SAISIE_COLKEY = "CBUD_HT_SAISIE";
	public static final String CBUD_MONTANT_BUDGETAIRE_COLKEY = "CBUD_MONTANT_BUDGETAIRE";
	public static final String CBUD_TTC_SAISIE_COLKEY = "CBUD_TTC_SAISIE";
	public static final String CBUD_TVA_SAISIE_COLKEY = "CBUD_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String COMMANDE_CONTROLE_ACTIONS_KEY = "commandeControleActions";
	public static final String COMMANDE_CONTROLE_ANALYTIQUES_KEY = "commandeControleAnalytiques";
	public static final String COMMANDE_CONTROLE_CONVENTIONS_KEY = "commandeControleConventions";
	public static final String COMMANDE_CONTROLE_HORS_MARCHES_KEY = "commandeControleHorsMarches";
	public static final String COMMANDE_CONTROLE_MARCHES_KEY = "commandeControleMarches";
	public static final String COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY = "commandeControlePlanComptables";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal cbudHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CBUD_HT_SAISIE_KEY);
  }

  public void setCbudHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBUD_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal cbudMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(CBUD_MONTANT_BUDGETAIRE_KEY);
  }

  public void setCbudMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBUD_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal cbudTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CBUD_TTC_SAISIE_KEY);
  }

  public void setCbudTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBUD_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal cbudTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CBUD_TVA_SAISIE_KEY);
  }

  public void setCbudTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBUD_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommande commande() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommande)storedValueForKey(COMMANDE_KEY);
  }

  public void setCommandeRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommande oldValue = commande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktldepense.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktldepense.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.client.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public NSArray commandeControleActions() {
    return (NSArray)storedValueForKey(COMMANDE_CONTROLE_ACTIONS_KEY);
  }

  public NSArray commandeControleActions(EOQualifier qualifier) {
    return commandeControleActions(qualifier, null, false);
  }

  public NSArray commandeControleActions(EOQualifier qualifier, boolean fetch) {
    return commandeControleActions(qualifier, null, fetch);
  }

  public NSArray commandeControleActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeControleActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ACTIONS_KEY);
  }

  public void removeFromCommandeControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ACTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction createCommandeControleActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeControleAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_ACTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction) eo;
  }

  public void deleteCommandeControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeControleActionsRelationships() {
    Enumeration objects = commandeControleActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeControleActionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAction)objects.nextElement());
    }
  }

  public NSArray commandeControleAnalytiques() {
    return (NSArray)storedValueForKey(COMMANDE_CONTROLE_ANALYTIQUES_KEY);
  }

  public NSArray commandeControleAnalytiques(EOQualifier qualifier) {
    return commandeControleAnalytiques(qualifier, null, false);
  }

  public NSArray commandeControleAnalytiques(EOQualifier qualifier, boolean fetch) {
    return commandeControleAnalytiques(qualifier, null, fetch);
  }

  public NSArray commandeControleAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeControleAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
  }

  public void removeFromCommandeControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique createCommandeControleAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeControleAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique) eo;
  }

  public void deleteCommandeControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeControleAnalytiquesRelationships() {
    Enumeration objects = commandeControleAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeControleAnalytiquesRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeControleAnalytique)objects.nextElement());
    }
  }

  public NSArray commandeControleConventions() {
    return (NSArray)storedValueForKey(COMMANDE_CONTROLE_CONVENTIONS_KEY);
  }

  public NSArray commandeControleConventions(EOQualifier qualifier) {
    return commandeControleConventions(qualifier, null, false);
  }

  public NSArray commandeControleConventions(EOQualifier qualifier, boolean fetch) {
    return commandeControleConventions(qualifier, null, fetch);
  }

  public NSArray commandeControleConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeControleConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_CONVENTIONS_KEY);
  }

  public void removeFromCommandeControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_CONVENTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention createCommandeControleConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeControleConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention) eo;
  }

  public void deleteCommandeControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeControleConventionsRelationships() {
    Enumeration objects = commandeControleConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeControleConventionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeControleConvention)objects.nextElement());
    }
  }

  public NSArray commandeControleHorsMarches() {
    return (NSArray)storedValueForKey(COMMANDE_CONTROLE_HORS_MARCHES_KEY);
  }

  public NSArray commandeControleHorsMarches(EOQualifier qualifier) {
    return commandeControleHorsMarches(qualifier, null, false);
  }

  public NSArray commandeControleHorsMarches(EOQualifier qualifier, boolean fetch) {
    return commandeControleHorsMarches(qualifier, null, fetch);
  }

  public NSArray commandeControleHorsMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeControleHorsMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
  }

  public void removeFromCommandeControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche createCommandeControleHorsMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeControleHorsMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche) eo;
  }

  public void deleteCommandeControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_HORS_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeControleHorsMarchesRelationships() {
    Enumeration objects = commandeControleHorsMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeControleHorsMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeControleHorsMarche)objects.nextElement());
    }
  }

  public NSArray commandeControleMarches() {
    return (NSArray)storedValueForKey(COMMANDE_CONTROLE_MARCHES_KEY);
  }

  public NSArray commandeControleMarches(EOQualifier qualifier) {
    return commandeControleMarches(qualifier, null, false);
  }

  public NSArray commandeControleMarches(EOQualifier qualifier, boolean fetch) {
    return commandeControleMarches(qualifier, null, fetch);
  }

  public NSArray commandeControleMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeControleMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_MARCHES_KEY);
  }

  public void removeFromCommandeControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche createCommandeControleMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeControleMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche) eo;
  }

  public void deleteCommandeControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeControleMarchesRelationships() {
    Enumeration objects = commandeControleMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeControleMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeControleMarche)objects.nextElement());
    }
  }

  public NSArray commandeControlePlanComptables() {
    return (NSArray)storedValueForKey(COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public NSArray commandeControlePlanComptables(EOQualifier qualifier) {
    return commandeControlePlanComptables(qualifier, null, false);
  }

  public NSArray commandeControlePlanComptables(EOQualifier qualifier, boolean fetch) {
    return commandeControlePlanComptables(qualifier, null, fetch);
  }

  public NSArray commandeControlePlanComptables(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable.COMMANDE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = commandeControlePlanComptables();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCommandeControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable object) {
    addObjectToBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public void removeFromCommandeControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable createCommandeControlePlanComptablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCommandeControlePlanComptable");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable) eo;
  }

  public void deleteCommandeControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, COMMANDE_CONTROLE_PLAN_COMPTABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCommandeControlePlanComptablesRelationships() {
    Enumeration objects = commandeControlePlanComptables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCommandeControlePlanComptablesRelationship((org.cocktail.fwkcktldepense.client.metier.EOCommandeControlePlanComptable)objects.nextElement());
    }
  }


  public static EOCommandeBudget createFwkCaramboleCommandeBudget(EOEditingContext editingContext, java.math.BigDecimal cbudHtSaisie
, java.math.BigDecimal cbudMontantBudgetaire
, java.math.BigDecimal cbudTtcSaisie
, java.math.BigDecimal cbudTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOCommande commande, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOOrgan organ, org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata, org.cocktail.fwkcktldepense.client.metier.EOTypeCredit typeCredit) {
    EOCommandeBudget eo = (EOCommandeBudget) createAndInsertInstance(editingContext, _EOCommandeBudget.ENTITY_NAME);    
		eo.setCbudHtSaisie(cbudHtSaisie);
		eo.setCbudMontantBudgetaire(cbudMontantBudgetaire);
		eo.setCbudTtcSaisie(cbudTtcSaisie);
		eo.setCbudTvaSaisie(cbudTvaSaisie);
    eo.setCommandeRelationship(commande);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTauxProrataRelationship(tauxProrata);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCommandeBudget.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCommandeBudget.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCommandeBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommandeBudget)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCommandeBudget localInstanceIn(EOEditingContext editingContext, EOCommandeBudget eo) {
    EOCommandeBudget localInstance = (eo == null) ? null : (EOCommandeBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCommandeBudget#localInstanceIn a la place.
   */
	public static EOCommandeBudget localInstanceOf(EOEditingContext editingContext, EOCommandeBudget eo) {
		return EOCommandeBudget.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCommandeBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCommandeBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCommandeBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
