// _EOCommandeControleHorsMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommandeControleHorsMarche.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCommandeControleHorsMarche extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeControleHorsMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_CTRL_HORS_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "chomId";

	public static final String CHOM_HT_SAISIE_KEY = "chomHtSaisie";
	public static final String CHOM_MONTANT_BUDGETAIRE_KEY = "chomMontantBudgetaire";
	public static final String CHOM_POURCENTAGE_KEY = "chomPourcentage";
	public static final String CHOM_TTC_SAISIE_KEY = "chomTtcSaisie";
	public static final String CHOM_TVA_SAISIE_KEY = "chomTvaSaisie";

// Attributs non visibles
	public static final String CBUD_ID_KEY = "cbudId";
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String CHOM_ID_KEY = "chomId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYPA_ID_KEY = "typaId";

//Colonnes dans la base de donnees
	public static final String CHOM_HT_SAISIE_COLKEY = "CHOM_HT_SAISIE";
	public static final String CHOM_MONTANT_BUDGETAIRE_COLKEY = "CHOM_MONTANT_BUDGETAIRE";
	public static final String CHOM_POURCENTAGE_COLKEY = "CHOM_POURCENTAGE";
	public static final String CHOM_TTC_SAISIE_COLKEY = "CHOM_TTC_SAISIE";
	public static final String CHOM_TVA_SAISIE_COLKEY = "CHOM_TVA_SAISIE";

	public static final String CBUD_ID_COLKEY = "CBUD_ID";
	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String CHOM_ID_COLKEY = "CHOM_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String COMMANDE_BUDGET_KEY = "commandeBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ACHAT_KEY = "typeAchat";



	// Accessors methods
  public java.math.BigDecimal chomHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CHOM_HT_SAISIE_KEY);
  }

  public void setChomHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CHOM_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal chomMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(CHOM_MONTANT_BUDGETAIRE_KEY);
  }

  public void setChomMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CHOM_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal chomPourcentage() {
    return (java.math.BigDecimal) storedValueForKey(CHOM_POURCENTAGE_KEY);
  }

  public void setChomPourcentage(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CHOM_POURCENTAGE_KEY);
  }

  public java.math.BigDecimal chomTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CHOM_TTC_SAISIE_KEY);
  }

  public void setChomTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CHOM_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal chomTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(CHOM_TVA_SAISIE_KEY);
  }

  public void setChomTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CHOM_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget commandeBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget)storedValueForKey(COMMANDE_BUDGET_KEY);
  }

  public void setCommandeBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget oldValue = commandeBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTypeAchat)storedValueForKey(TYPE_ACHAT_KEY);
  }

  public void setTypeAchatRelationship(org.cocktail.fwkcktldepense.client.metier.EOTypeAchat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTypeAchat oldValue = typeAchat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACHAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACHAT_KEY);
    }
  }
  

  public static EOCommandeControleHorsMarche createFwkCaramboleCommandeControleHorsMarche(EOEditingContext editingContext, java.math.BigDecimal chomHtSaisie
, java.math.BigDecimal chomMontantBudgetaire
, java.math.BigDecimal chomTtcSaisie
, java.math.BigDecimal chomTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EOCodeExer codeExer, org.cocktail.fwkcktldepense.client.metier.EOCommandeBudget commandeBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOTypeAchat typeAchat) {
    EOCommandeControleHorsMarche eo = (EOCommandeControleHorsMarche) createAndInsertInstance(editingContext, _EOCommandeControleHorsMarche.ENTITY_NAME);    
		eo.setChomHtSaisie(chomHtSaisie);
		eo.setChomMontantBudgetaire(chomMontantBudgetaire);
		eo.setChomTtcSaisie(chomTtcSaisie);
		eo.setChomTvaSaisie(chomTvaSaisie);
    eo.setCodeExerRelationship(codeExer);
    eo.setCommandeBudgetRelationship(commandeBudget);
    eo.setExerciceRelationship(exercice);
    eo.setTypeAchatRelationship(typeAchat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCommandeControleHorsMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCommandeControleHorsMarche.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCommandeControleHorsMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommandeControleHorsMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCommandeControleHorsMarche localInstanceIn(EOEditingContext editingContext, EOCommandeControleHorsMarche eo) {
    EOCommandeControleHorsMarche localInstance = (eo == null) ? null : (EOCommandeControleHorsMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCommandeControleHorsMarche#localInstanceIn a la place.
   */
	public static EOCommandeControleHorsMarche localInstanceOf(EOEditingContext editingContext, EOCommandeControleHorsMarche eo) {
		return EOCommandeControleHorsMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCommandeControleHorsMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCommandeControleHorsMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeControleHorsMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeControleHorsMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeControleHorsMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCommandeControleHorsMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeControleHorsMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeControleHorsMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeControleHorsMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
