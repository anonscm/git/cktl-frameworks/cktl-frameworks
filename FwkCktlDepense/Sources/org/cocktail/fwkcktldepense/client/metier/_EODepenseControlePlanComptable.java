// _EODepenseControlePlanComptable.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepenseControlePlanComptable.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepenseControlePlanComptable extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleDepenseControlePlanComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_CTRL_PLANCO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dpcoId";

	public static final String DPCO_HT_SAISIE_KEY = "dpcoHtSaisie";
	public static final String DPCO_MONTANT_BUDGETAIRE_KEY = "dpcoMontantBudgetaire";
	public static final String DPCO_TTC_SAISIE_KEY = "dpcoTtcSaisie";
	public static final String DPCO_TVA_SAISIE_KEY = "dpcoTvaSaisie";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MAN_ID_KEY = "manId";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String DPCO_HT_SAISIE_COLKEY = "DPCO_HT_SAISIE";
	public static final String DPCO_MONTANT_BUDGETAIRE_COLKEY = "DPCO_MONTANT_BUDGETAIRE";
	public static final String DPCO_TTC_SAISIE_COLKEY = "DPCO_TTC_SAISIE";
	public static final String DPCO_TVA_SAISIE_COLKEY = "DPCO_TVA_SAISIE";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MAN_ID_COLKEY = "MAN_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String DEPENSE_BUDGET_KEY = "depenseBudget";
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String MANDAT_KEY = "mandat";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



	// Accessors methods
  public java.math.BigDecimal dpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_HT_SAISIE_KEY);
  }

  public void setDpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal dpcoMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDpcoMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal dpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TTC_SAISIE_KEY);
  }

  public void setDpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TVA_SAISIE_KEY);
  }

  public void setDpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepenseBudget depenseBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EODepenseBudget)storedValueForKey(DEPENSE_BUDGET_KEY);
  }

  public void setDepenseBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODepenseBudget oldValue = depenseBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail ecritureDetail() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
  }

  public void setEcritureDetailRelationship(org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEcritureDetail oldValue = ecritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOMandat mandat() {
    return (org.cocktail.fwkcktldepense.client.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.fwkcktldepense.client.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.fwkcktldepense.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.fwkcktldepense.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

  public static EODepenseControlePlanComptable createFwkCaramboleDepenseControlePlanComptable(EOEditingContext editingContext, java.math.BigDecimal dpcoHtSaisie
, java.math.BigDecimal dpcoMontantBudgetaire
, java.math.BigDecimal dpcoTtcSaisie
, java.math.BigDecimal dpcoTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EODepenseBudget depenseBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOPlanComptable planComptable) {
    EODepenseControlePlanComptable eo = (EODepenseControlePlanComptable) createAndInsertInstance(editingContext, _EODepenseControlePlanComptable.ENTITY_NAME);    
		eo.setDpcoHtSaisie(dpcoHtSaisie);
		eo.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
		eo.setDpcoTtcSaisie(dpcoTtcSaisie);
		eo.setDpcoTvaSaisie(dpcoTvaSaisie);
    eo.setDepenseBudgetRelationship(depenseBudget);
    eo.setExerciceRelationship(exercice);
    eo.setPlanComptableRelationship(planComptable);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EODepenseControlePlanComptable.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EODepenseControlePlanComptable.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EODepenseControlePlanComptable localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepenseControlePlanComptable)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EODepenseControlePlanComptable localInstanceIn(EOEditingContext editingContext, EODepenseControlePlanComptable eo) {
    EODepenseControlePlanComptable localInstance = (eo == null) ? null : (EODepenseControlePlanComptable)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EODepenseControlePlanComptable#localInstanceIn a la place.
   */
	public static EODepenseControlePlanComptable localInstanceOf(EOEditingContext editingContext, EODepenseControlePlanComptable eo) {
		return EODepenseControlePlanComptable.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EODepenseControlePlanComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODepenseControlePlanComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepenseControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepenseControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepenseControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepenseControlePlanComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepenseControlePlanComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepenseControlePlanComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EODepenseControlePlanComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepenseControlePlanComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepenseControlePlanComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepenseControlePlanComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
