// _EOPreDepenseBudget.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreDepenseBudget.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPreDepenseBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCarambolePreDepenseBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.PDEPENSE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdepId";

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";

// Attributs non visibles
	public static final String DPP_ID_KEY = "dppId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PDEP_ID_KEY = "pdepId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_HT_SAISIE_COLKEY = "PDEP_HT_SAISIE";
	public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "PDEP_MONTANT_BUDGETAIRE";
	public static final String DEP_TTC_SAISIE_COLKEY = "PDEP_TTC_SAISIE";
	public static final String DEP_TVA_SAISIE_COLKEY = "PDEP_TVA_SAISIE";

	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PDEP_ID_COLKEY = "PDEP_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_PAPIER_KEY = "depensePapier";
	public static final String ENGAGEMENT_BUDGET_KEY = "engagementBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PRE_DEPENSE_CONTROLE_ACTIONS_KEY = "preDepenseControleActions";
	public static final String PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY = "preDepenseControleAnalytiques";
	public static final String PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY = "preDepenseControleConventions";
	public static final String PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY = "preDepenseControleHorsMarches";
	public static final String PRE_DEPENSE_CONTROLE_MARCHES_KEY = "preDepenseControleMarches";
	public static final String PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY = "preDepenseControlePlanComptables";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public java.math.BigDecimal depHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_HT_SAISIE_KEY);
  }

  public void setDepHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal depMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDepMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal depTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TTC_SAISIE_KEY);
  }

  public void setDepTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal depTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TVA_SAISIE_KEY);
  }

  public void setDepTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TVA_SAISIE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EODepensePapier depensePapier() {
    return (org.cocktail.fwkcktldepense.client.metier.EODepensePapier)storedValueForKey(DEPENSE_PAPIER_KEY);
  }

  public void setDepensePapierRelationship(org.cocktail.fwkcktldepense.client.metier.EODepensePapier value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EODepensePapier oldValue = depensePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget() {
    return (org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget)storedValueForKey(ENGAGEMENT_BUDGET_KEY);
  }

  public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget oldValue = engagementBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGEMENT_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGEMENT_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktldepense.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktldepense.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.fwkcktldepense.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.fwkcktldepense.client.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray preDepenseControleActions() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
  }

  public NSArray preDepenseControleActions(EOQualifier qualifier) {
    return preDepenseControleActions(qualifier, null, false);
  }

  public NSArray preDepenseControleActions(EOQualifier qualifier, boolean fetch) {
    return preDepenseControleActions(qualifier, null, fetch);
  }

  public NSArray preDepenseControleActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseControleActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
  }

  public void removeFromPreDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction createPreDepenseControleActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseControleAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction) eo;
  }

  public void deletePreDepenseControleActionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseControleActionsRelationships() {
    Enumeration objects = preDepenseControleActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseControleActionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAction)objects.nextElement());
    }
  }

  public NSArray preDepenseControleAnalytiques() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
  }

  public NSArray preDepenseControleAnalytiques(EOQualifier qualifier) {
    return preDepenseControleAnalytiques(qualifier, null, false);
  }

  public NSArray preDepenseControleAnalytiques(EOQualifier qualifier, boolean fetch) {
    return preDepenseControleAnalytiques(qualifier, null, fetch);
  }

  public NSArray preDepenseControleAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseControleAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
  }

  public void removeFromPreDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique createPreDepenseControleAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseControleAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique) eo;
  }

  public void deletePreDepenseControleAnalytiquesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseControleAnalytiquesRelationships() {
    Enumeration objects = preDepenseControleAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseControleAnalytiquesRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleAnalytique)objects.nextElement());
    }
  }

  public NSArray preDepenseControleConventions() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public NSArray preDepenseControleConventions(EOQualifier qualifier) {
    return preDepenseControleConventions(qualifier, null, false);
  }

  public NSArray preDepenseControleConventions(EOQualifier qualifier, boolean fetch) {
    return preDepenseControleConventions(qualifier, null, fetch);
  }

  public NSArray preDepenseControleConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseControleConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public void removeFromPreDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention createPreDepenseControleConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseControleConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention) eo;
  }

  public void deletePreDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseControleConventionsRelationships() {
    Enumeration objects = preDepenseControleConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseControleConventionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleConvention)objects.nextElement());
    }
  }

  public NSArray preDepenseControleHorsMarches() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
  }

  public NSArray preDepenseControleHorsMarches(EOQualifier qualifier) {
    return preDepenseControleHorsMarches(qualifier, null, false);
  }

  public NSArray preDepenseControleHorsMarches(EOQualifier qualifier, boolean fetch) {
    return preDepenseControleHorsMarches(qualifier, null, fetch);
  }

  public NSArray preDepenseControleHorsMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseControleHorsMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
  }

  public void removeFromPreDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche createPreDepenseControleHorsMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseControleHorsMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche) eo;
  }

  public void deletePreDepenseControleHorsMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_HORS_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseControleHorsMarchesRelationships() {
    Enumeration objects = preDepenseControleHorsMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseControleHorsMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleHorsMarche)objects.nextElement());
    }
  }

  public NSArray preDepenseControleMarches() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_CONTROLE_MARCHES_KEY);
  }

  public NSArray preDepenseControleMarches(EOQualifier qualifier) {
    return preDepenseControleMarches(qualifier, null, false);
  }

  public NSArray preDepenseControleMarches(EOQualifier qualifier, boolean fetch) {
    return preDepenseControleMarches(qualifier, null, fetch);
  }

  public NSArray preDepenseControleMarches(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseControleMarches();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
  }

  public void removeFromPreDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche createPreDepenseControleMarchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseControleMarche");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche) eo;
  }

  public void deletePreDepenseControleMarchesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_MARCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseControleMarchesRelationships() {
    Enumeration objects = preDepenseControleMarches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseControleMarchesRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControleMarche)objects.nextElement());
    }
  }

  public NSArray preDepenseControlePlanComptables() {
    return (NSArray)storedValueForKey(PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public NSArray preDepenseControlePlanComptables(EOQualifier qualifier) {
    return preDepenseControlePlanComptables(qualifier, null, false);
  }

  public NSArray preDepenseControlePlanComptables(EOQualifier qualifier, boolean fetch) {
    return preDepenseControlePlanComptables(qualifier, null, fetch);
  }

  public NSArray preDepenseControlePlanComptables(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable.PRE_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = preDepenseControlePlanComptables();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPreDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public void removeFromPreDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable createPreDepenseControlePlanComptablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCarambolePreDepenseControlePlanComptable");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable) eo;
  }

  public void deletePreDepenseControlePlanComptablesRelationship(org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRE_DEPENSE_CONTROLE_PLAN_COMPTABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPreDepenseControlePlanComptablesRelationships() {
    Enumeration objects = preDepenseControlePlanComptables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePreDepenseControlePlanComptablesRelationship((org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptable)objects.nextElement());
    }
  }


  public static EOPreDepenseBudget createFwkCarambolePreDepenseBudget(EOEditingContext editingContext, java.math.BigDecimal depHtSaisie
, java.math.BigDecimal depMontantBudgetaire
, java.math.BigDecimal depTtcSaisie
, java.math.BigDecimal depTvaSaisie
, org.cocktail.fwkcktldepense.client.metier.EODepensePapier depensePapier, org.cocktail.fwkcktldepense.client.metier.EOEngagementBudget engagementBudget, org.cocktail.fwkcktldepense.client.metier.EOExercice exercice, org.cocktail.fwkcktldepense.client.metier.EOTauxProrata tauxProrata, org.cocktail.fwkcktldepense.client.metier.EOUtilisateur utilisateur) {
    EOPreDepenseBudget eo = (EOPreDepenseBudget) createAndInsertInstance(editingContext, _EOPreDepenseBudget.ENTITY_NAME);    
		eo.setDepHtSaisie(depHtSaisie);
		eo.setDepMontantBudgetaire(depMontantBudgetaire);
		eo.setDepTtcSaisie(depTtcSaisie);
		eo.setDepTvaSaisie(depTvaSaisie);
    eo.setDepensePapierRelationship(depensePapier);
    eo.setEngagementBudgetRelationship(engagementBudget);
    eo.setExerciceRelationship(exercice);
    eo.setTauxProrataRelationship(tauxProrata);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPreDepenseBudget.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPreDepenseBudget.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPreDepenseBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreDepenseBudget)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPreDepenseBudget localInstanceIn(EOEditingContext editingContext, EOPreDepenseBudget eo) {
    EOPreDepenseBudget localInstance = (eo == null) ? null : (EOPreDepenseBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPreDepenseBudget#localInstanceIn a la place.
   */
	public static EOPreDepenseBudget localInstanceOf(EOEditingContext editingContext, EOPreDepenseBudget eo) {
		return EOPreDepenseBudget.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPreDepenseBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPreDepenseBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreDepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreDepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreDepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreDepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreDepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreDepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPreDepenseBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreDepenseBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreDepenseBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreDepenseBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
