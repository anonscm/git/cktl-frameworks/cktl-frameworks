// _EOCommandeBascule.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommandeBascule.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCommandeBascule extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCommandeBascule";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.COMMANDE_BASCULE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "combId";


// Attributs non visibles
	public static final String COMB_ID_KEY = "combId";
	public static final String COMM_ID_DESTINATION_KEY = "commIdDestination";
	public static final String COMM_ID_ORIGINE_KEY = "commIdOrigine";

//Colonnes dans la base de donnees

	public static final String COMB_ID_COLKEY = "COMB_ID";
	public static final String COMM_ID_DESTINATION_COLKEY = "COMM_ID_DESTINATION";
	public static final String COMM_ID_ORIGINE_COLKEY = "COMM_ID_ORIGINE";


	// Relationships
	public static final String COMMANDE_DESTINATION_KEY = "commandeDestination";
	public static final String COMMANDE_ORIGINE_KEY = "commandeOrigine";



	// Accessors methods
  public org.cocktail.fwkcktldepense.client.metier.EOCommande commandeDestination() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommande)storedValueForKey(COMMANDE_DESTINATION_KEY);
  }

  public void setCommandeDestinationRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommande oldValue = commandeDestination();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_DESTINATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_DESTINATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOCommande commandeOrigine() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCommande)storedValueForKey(COMMANDE_ORIGINE_KEY);
  }

  public void setCommandeOrigineRelationship(org.cocktail.fwkcktldepense.client.metier.EOCommande value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCommande oldValue = commandeOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_ORIGINE_KEY);
    }
  }
  

  public static EOCommandeBascule createFwkCaramboleCommandeBascule(EOEditingContext editingContext, org.cocktail.fwkcktldepense.client.metier.EOCommande commandeDestination, org.cocktail.fwkcktldepense.client.metier.EOCommande commandeOrigine) {
    EOCommandeBascule eo = (EOCommandeBascule) createAndInsertInstance(editingContext, _EOCommandeBascule.ENTITY_NAME);    
    eo.setCommandeDestinationRelationship(commandeDestination);
    eo.setCommandeOrigineRelationship(commandeOrigine);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCommandeBascule.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCommandeBascule.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCommandeBascule localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommandeBascule)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCommandeBascule localInstanceIn(EOEditingContext editingContext, EOCommandeBascule eo) {
    EOCommandeBascule localInstance = (eo == null) ? null : (EOCommandeBascule)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCommandeBascule#localInstanceIn a la place.
   */
	public static EOCommandeBascule localInstanceOf(EOEditingContext editingContext, EOCommandeBascule eo) {
		return EOCommandeBascule.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCommandeBascule fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCommandeBascule fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommandeBascule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommandeBascule)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommandeBascule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommandeBascule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommandeBascule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommandeBascule)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCommandeBascule fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommandeBascule eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommandeBascule ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommandeBascule fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
