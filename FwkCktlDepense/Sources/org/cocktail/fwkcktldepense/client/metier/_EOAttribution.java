// _EOAttribution.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAttribution.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAttribution extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleAttribution";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_ATTRIBUTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "attOrdre";

	public static final String ATT_DEBUT_KEY = "attDebut";
	public static final String ATT_FIN_KEY = "attFin";
	public static final String ATT_HT_KEY = "attHt";
	public static final String ATT_SUPPR_KEY = "attSuppr";
	public static final String ATT_VALIDE_KEY = "attValide";

// Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TIT_ORDRE_KEY = "titOrdre";

//Colonnes dans la base de donnees
	public static final String ATT_DEBUT_COLKEY = "ATT_DEBUT";
	public static final String ATT_FIN_COLKEY = "ATT_FIN";
	public static final String ATT_HT_COLKEY = "ATT_HT";
	public static final String ATT_SUPPR_COLKEY = "ATT_SUPPR";
	public static final String ATT_VALIDE_COLKEY = "ATT_VALIDE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";


	// Relationships
	public static final String ATTRIBUTION_EXECUTIONS_KEY = "attributionExecutions";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String LOT_KEY = "lot";
	public static final String SOUS_TRAITANTS_KEY = "sousTraitants";
	public static final String TO_RIB_FOURNISSEUR_KEY = "toRibFournisseur";



	// Accessors methods
  public NSTimestamp attDebut() {
    return (NSTimestamp) storedValueForKey(ATT_DEBUT_KEY);
  }

  public void setAttDebut(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_DEBUT_KEY);
  }

  public NSTimestamp attFin() {
    return (NSTimestamp) storedValueForKey(ATT_FIN_KEY);
  }

  public void setAttFin(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_FIN_KEY);
  }

  public java.math.BigDecimal attHt() {
    return (java.math.BigDecimal) storedValueForKey(ATT_HT_KEY);
  }

  public void setAttHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ATT_HT_KEY);
  }

  public String attSuppr() {
    return (String) storedValueForKey(ATT_SUPPR_KEY);
  }

  public void setAttSuppr(String value) {
    takeStoredValueForKey(value, ATT_SUPPR_KEY);
  }

  public String attValide() {
    return (String) storedValueForKey(ATT_VALIDE_KEY);
  }

  public void setAttValide(String value) {
    takeStoredValueForKey(value, ATT_VALIDE_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.fwkcktldepense.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.fwkcktldepense.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EOLot lot() {
    return (org.cocktail.fwkcktldepense.client.metier.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.fwkcktldepense.client.metier.EOLot value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldepense.client.metier.EORibFournisseur toRibFournisseur() {
    return (org.cocktail.fwkcktldepense.client.metier.EORibFournisseur)storedValueForKey(TO_RIB_FOURNISSEUR_KEY);
  }

  public void setToRibFournisseurRelationship(org.cocktail.fwkcktldepense.client.metier.EORibFournisseur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EORibFournisseur oldValue = toRibFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RIB_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RIB_FOURNISSEUR_KEY);
    }
  }
  
  public NSArray attributionExecutions() {
    return (NSArray)storedValueForKey(ATTRIBUTION_EXECUTIONS_KEY);
  }

  public NSArray attributionExecutions(EOQualifier qualifier) {
    return attributionExecutions(qualifier, null, false);
  }

  public NSArray attributionExecutions(EOQualifier qualifier, boolean fetch) {
    return attributionExecutions(qualifier, null, fetch);
  }

  public NSArray attributionExecutions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution.ATTRIBUTION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = attributionExecutions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAttributionExecutionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ATTRIBUTION_EXECUTIONS_KEY);
  }

  public void removeFromAttributionExecutionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ATTRIBUTION_EXECUTIONS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution createAttributionExecutionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleAttributionExecution");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ATTRIBUTION_EXECUTIONS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution) eo;
  }

  public void deleteAttributionExecutionsRelationship(org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ATTRIBUTION_EXECUTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAttributionExecutionsRelationships() {
    Enumeration objects = attributionExecutions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAttributionExecutionsRelationship((org.cocktail.fwkcktldepense.client.metier.EOAttributionExecution)objects.nextElement());
    }
  }

  public NSArray sousTraitants() {
    return (NSArray)storedValueForKey(SOUS_TRAITANTS_KEY);
  }

  public NSArray sousTraitants(EOQualifier qualifier) {
    return sousTraitants(qualifier, null, false);
  }

  public NSArray sousTraitants(EOQualifier qualifier, boolean fetch) {
    return sousTraitants(qualifier, null, fetch);
  }

  public NSArray sousTraitants(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOSousTraitant.ATTRIBUTION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOSousTraitant.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sousTraitants();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSousTraitantsRelationship(org.cocktail.fwkcktldepense.client.metier.EOSousTraitant object) {
    addObjectToBothSidesOfRelationshipWithKey(object, SOUS_TRAITANTS_KEY);
  }

  public void removeFromSousTraitantsRelationship(org.cocktail.fwkcktldepense.client.metier.EOSousTraitant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SOUS_TRAITANTS_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOSousTraitant createSousTraitantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleSousTraitant");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, SOUS_TRAITANTS_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOSousTraitant) eo;
  }

  public void deleteSousTraitantsRelationship(org.cocktail.fwkcktldepense.client.metier.EOSousTraitant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SOUS_TRAITANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSousTraitantsRelationships() {
    Enumeration objects = sousTraitants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSousTraitantsRelationship((org.cocktail.fwkcktldepense.client.metier.EOSousTraitant)objects.nextElement());
    }
  }


  public static EOAttribution createFwkCaramboleAttribution(EOEditingContext editingContext, NSTimestamp attDebut
, NSTimestamp attFin
, java.math.BigDecimal attHt
, String attSuppr
, String attValide
, org.cocktail.fwkcktldepense.client.metier.EOFournisseur fournisseur, org.cocktail.fwkcktldepense.client.metier.EOLot lot) {
    EOAttribution eo = (EOAttribution) createAndInsertInstance(editingContext, _EOAttribution.ENTITY_NAME);    
		eo.setAttDebut(attDebut);
		eo.setAttFin(attFin);
		eo.setAttHt(attHt);
		eo.setAttSuppr(attSuppr);
		eo.setAttValide(attValide);
    eo.setFournisseurRelationship(fournisseur);
    eo.setLotRelationship(lot);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOAttribution.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOAttribution.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOAttribution localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAttribution)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOAttribution localInstanceIn(EOEditingContext editingContext, EOAttribution eo) {
    EOAttribution localInstance = (eo == null) ? null : (EOAttribution)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOAttribution#localInstanceIn a la place.
   */
	public static EOAttribution localInstanceOf(EOEditingContext editingContext, EOAttribution eo) {
		return EOAttribution.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOAttribution fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOAttribution fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOAttribution fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAttribution eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAttribution ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAttribution fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
