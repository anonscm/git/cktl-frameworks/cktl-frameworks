// _EOXMLCommande.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOXMLCommande.java instead.
package org.cocktail.fwkcktldepense.client.metier.xml;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOXMLCommande extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleXMLCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.XML_COMMANDE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "commId";

	public static final String ATTRIBUTION_KEY = "attribution";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String LIBELLE_KEY = "libelle";
	public static final String NUMERO_KEY = "numero";
	public static final String PRESTATION_ID_KEY = "prestationId";
	public static final String REFERENCE_KEY = "reference";
	public static final String TYPE_ACHAT_KEY = "typeAchat";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String ULT_ORDRE_KEY = "ultOrdre";

//Colonnes dans la base de donnees
	public static final String ATTRIBUTION_COLKEY = "ATT_ORDRE";
	public static final String EXERCICE_COLKEY = "EXE_ORDRE";
	public static final String FOURNISSEUR_COLKEY = "FOU_CODE";
	public static final String LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String NUMERO_COLKEY = "COMM_NUMERO";
	public static final String PRESTATION_ID_COLKEY = "PRESTATION_ID";
	public static final String REFERENCE_COLKEY = "COMM_REFERENCE";
	public static final String TYPE_ACHAT_COLKEY = "TYPA_LIBELLE";
	public static final String TYPE_APPLICATION_COLKEY = "TYAP_LIBELLE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String ULT_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ARTICLES_KEY = "articles";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public Integer attribution() {
    return (Integer) storedValueForKey(ATTRIBUTION_KEY);
  }

  public void setAttribution(Integer value) {
    takeStoredValueForKey(value, ATTRIBUTION_KEY);
  }

  public Integer exercice() {
    return (Integer) storedValueForKey(EXERCICE_KEY);
  }

  public void setExercice(Integer value) {
    takeStoredValueForKey(value, EXERCICE_KEY);
  }

  public String fournisseur() {
    return (String) storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseur(String value) {
    takeStoredValueForKey(value, FOURNISSEUR_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public Integer numero() {
    return (Integer) storedValueForKey(NUMERO_KEY);
  }

  public void setNumero(Integer value) {
    takeStoredValueForKey(value, NUMERO_KEY);
  }

  public Integer prestationId() {
    return (Integer) storedValueForKey(PRESTATION_ID_KEY);
  }

  public void setPrestationId(Integer value) {
    takeStoredValueForKey(value, PRESTATION_ID_KEY);
  }

  public String reference() {
    return (String) storedValueForKey(REFERENCE_KEY);
  }

  public void setReference(String value) {
    takeStoredValueForKey(value, REFERENCE_KEY);
  }

  public String typeAchat() {
    return (String) storedValueForKey(TYPE_ACHAT_KEY);
  }

  public void setTypeAchat(String value) {
    takeStoredValueForKey(value, TYPE_ACHAT_KEY);
  }

  public String typeApplication() {
    return (String) storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplication(String value) {
    takeStoredValueForKey(value, TYPE_APPLICATION_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.xml.EOXMLUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldepense.client.metier.xml.EOXMLUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldepense.client.metier.xml.EOXMLUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.xml.EOXMLUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray articles() {
    return (NSArray)storedValueForKey(ARTICLES_KEY);
  }

  public NSArray articles(EOQualifier qualifier) {
    return articles(qualifier, null);
  }

  public NSArray articles(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = articles();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToArticlesRelationship(org.cocktail.fwkcktldepense.client.metier.xml.EOXMLArticle object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
  }

  public void removeFromArticlesRelationship(org.cocktail.fwkcktldepense.client.metier.xml.EOXMLArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.xml.EOXMLArticle createArticlesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleXMLArticle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLES_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.xml.EOXMLArticle) eo;
  }

  public void deleteArticlesRelationship(org.cocktail.fwkcktldepense.client.metier.xml.EOXMLArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllArticlesRelationships() {
    Enumeration objects = articles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteArticlesRelationship((org.cocktail.fwkcktldepense.client.metier.xml.EOXMLArticle)objects.nextElement());
    }
  }


  public static EOXMLCommande createFwkCaramboleXMLCommande(EOEditingContext editingContext, Integer attribution
, Integer exercice
, String fournisseur
, String libelle
, Integer numero
, Integer prestationId
, String reference
, String typeAchat
, String typeApplication
, org.cocktail.fwkcktldepense.client.metier.xml.EOXMLUtilisateur utilisateur) {
    EOXMLCommande eo = (EOXMLCommande) createAndInsertInstance(editingContext, _EOXMLCommande.ENTITY_NAME);    
		eo.setAttribution(attribution);
		eo.setExercice(exercice);
		eo.setFournisseur(fournisseur);
		eo.setLibelle(libelle);
		eo.setNumero(numero);
		eo.setPrestationId(prestationId);
		eo.setReference(reference);
		eo.setTypeAchat(typeAchat);
		eo.setTypeApplication(typeApplication);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOXMLCommande.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOXMLCommande.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOXMLCommande localInstanceIn(EOEditingContext editingContext) {
	  		return (EOXMLCommande)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOXMLCommande localInstanceIn(EOEditingContext editingContext, EOXMLCommande eo) {
    EOXMLCommande localInstance = (eo == null) ? null : (EOXMLCommande)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOXMLCommande#localInstanceIn a la place.
   */
	public static EOXMLCommande localInstanceOf(EOEditingContext editingContext, EOXMLCommande eo) {
		return EOXMLCommande.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOXMLCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOXMLCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOXMLCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOXMLCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOXMLCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOXMLCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOXMLCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOXMLCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOXMLCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOXMLCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOXMLCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOXMLCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
