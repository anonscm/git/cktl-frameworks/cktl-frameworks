// _EOXMLArticle.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOXMLArticle.java instead.
package org.cocktail.fwkcktldepense.client.metier.xml;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOXMLArticle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleXMLArticle";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.XML_ARTICLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "artId";

	public static final String LIBELLE_KEY = "libelle";
	public static final String PRIX_HT_KEY = "prixHt";
	public static final String PRIX_TOTAL_HT_KEY = "prixTotalHt";
	public static final String PRIX_TOTAL_TTC_KEY = "prixTotalTtc";
	public static final String PRIX_TTC_KEY = "prixTtc";
	public static final String QUANTITE_KEY = "quantite";
	public static final String REFERENCE_KEY = "reference";
	public static final String TVA_KEY = "tva";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String COMM_ID_KEY = "commId";

//Colonnes dans la base de donnees
	public static final String LIBELLE_COLKEY = "ART_LIBELLE";
	public static final String PRIX_HT_COLKEY = "ART_PRIX_HT";
	public static final String PRIX_TOTAL_HT_COLKEY = "ART_PRIX_TOTAL_HT";
	public static final String PRIX_TOTAL_TTC_COLKEY = "ART_PRIX_TOTAL_TTC";
	public static final String PRIX_TTC_COLKEY = "ART_PRIX_TTC";
	public static final String QUANTITE_COLKEY = "ART_QUANTITE";
	public static final String REFERENCE_COLKEY = "ART_REFERENCE";
	public static final String TVA_COLKEY = "TVA_TAUX";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String COMM_ID_COLKEY = "COMM_ID";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";



	// Accessors methods
  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public java.math.BigDecimal prixHt() {
    return (java.math.BigDecimal) storedValueForKey(PRIX_HT_KEY);
  }

  public void setPrixHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRIX_HT_KEY);
  }

  public java.math.BigDecimal prixTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(PRIX_TOTAL_HT_KEY);
  }

  public void setPrixTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRIX_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal prixTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(PRIX_TOTAL_TTC_KEY);
  }

  public void setPrixTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRIX_TOTAL_TTC_KEY);
  }

  public java.math.BigDecimal prixTtc() {
    return (java.math.BigDecimal) storedValueForKey(PRIX_TTC_KEY);
  }

  public void setPrixTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRIX_TTC_KEY);
  }

  public java.math.BigDecimal quantite() {
    return (java.math.BigDecimal) storedValueForKey(QUANTITE_KEY);
  }

  public void setQuantite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, QUANTITE_KEY);
  }

  public String reference() {
    return (String) storedValueForKey(REFERENCE_KEY);
  }

  public void setReference(String value) {
    takeStoredValueForKey(value, REFERENCE_KEY);
  }

  public java.math.BigDecimal tva() {
    return (java.math.BigDecimal) storedValueForKey(TVA_KEY);
  }

  public void setTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TVA_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.xml.EOXMLCodeExer codeExer() {
    return (org.cocktail.fwkcktldepense.client.metier.xml.EOXMLCodeExer)storedValueForKey(CODE_EXER_KEY);
  }

  public void setCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.xml.EOXMLCodeExer value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.xml.EOXMLCodeExer oldValue = codeExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_EXER_KEY);
    }
  }
  

  public static EOXMLArticle createFwkCaramboleXMLArticle(EOEditingContext editingContext, String libelle
, java.math.BigDecimal prixHt
, java.math.BigDecimal prixTotalHt
, java.math.BigDecimal prixTotalTtc
, java.math.BigDecimal prixTtc
, java.math.BigDecimal quantite
, String reference
, java.math.BigDecimal tva
, org.cocktail.fwkcktldepense.client.metier.xml.EOXMLCodeExer codeExer) {
    EOXMLArticle eo = (EOXMLArticle) createAndInsertInstance(editingContext, _EOXMLArticle.ENTITY_NAME);    
		eo.setLibelle(libelle);
		eo.setPrixHt(prixHt);
		eo.setPrixTotalHt(prixTotalHt);
		eo.setPrixTotalTtc(prixTotalTtc);
		eo.setPrixTtc(prixTtc);
		eo.setQuantite(quantite);
		eo.setReference(reference);
		eo.setTva(tva);
    eo.setCodeExerRelationship(codeExer);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOXMLArticle.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOXMLArticle.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOXMLArticle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOXMLArticle)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOXMLArticle localInstanceIn(EOEditingContext editingContext, EOXMLArticle eo) {
    EOXMLArticle localInstance = (eo == null) ? null : (EOXMLArticle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOXMLArticle#localInstanceIn a la place.
   */
	public static EOXMLArticle localInstanceOf(EOEditingContext editingContext, EOXMLArticle eo) {
		return EOXMLArticle.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOXMLArticle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOXMLArticle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOXMLArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOXMLArticle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOXMLArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOXMLArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOXMLArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOXMLArticle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOXMLArticle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOXMLArticle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOXMLArticle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOXMLArticle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
