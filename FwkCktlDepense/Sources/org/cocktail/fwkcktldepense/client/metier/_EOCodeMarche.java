// _EOCodeMarche.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeMarche.java instead.
package org.cocktail.fwkcktldepense.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCodeMarche extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCaramboleCodeMarche";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_CODE_MARCHE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cmOrdre";

	public static final String CM_CODE_KEY = "cmCode";
	public static final String CM_LIB_KEY = "cmLib";
	public static final String CM_NIVEAU_KEY = "cmNiveau";
	public static final String CM_SUPPR_KEY = "cmSuppr";

// Attributs non visibles
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String CM_PERE_KEY = "cmPere";

//Colonnes dans la base de donnees
	public static final String CM_CODE_COLKEY = "CM_CODE";
	public static final String CM_LIB_COLKEY = "CM_LIB";
	public static final String CM_NIVEAU_COLKEY = "CM_NIVEAU";
	public static final String CM_SUPPR_COLKEY = "CM_SUPPR";

	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String CM_PERE_COLKEY = "CM_PERE";


	// Relationships
	public static final String CODE_EXER_KEY = "codeExer";
	public static final String CODE_MARCHE_PERE_KEY = "codeMarchePere";



	// Accessors methods
  public String cmCode() {
    return (String) storedValueForKey(CM_CODE_KEY);
  }

  public void setCmCode(String value) {
    takeStoredValueForKey(value, CM_CODE_KEY);
  }

  public String cmLib() {
    return (String) storedValueForKey(CM_LIB_KEY);
  }

  public void setCmLib(String value) {
    takeStoredValueForKey(value, CM_LIB_KEY);
  }

  public Integer cmNiveau() {
    return (Integer) storedValueForKey(CM_NIVEAU_KEY);
  }

  public void setCmNiveau(Integer value) {
    takeStoredValueForKey(value, CM_NIVEAU_KEY);
  }

  public String cmSuppr() {
    return (String) storedValueForKey(CM_SUPPR_KEY);
  }

  public void setCmSuppr(String value) {
    takeStoredValueForKey(value, CM_SUPPR_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeMarche codeMarchePere() {
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeMarche)storedValueForKey(CODE_MARCHE_PERE_KEY);
  }

  public void setCodeMarchePereRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeMarche value) {
    if (value == null) {
    	org.cocktail.fwkcktldepense.client.metier.EOCodeMarche oldValue = codeMarchePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_PERE_KEY);
    }
  }
  
  public NSArray codeExer() {
    return (NSArray)storedValueForKey(CODE_EXER_KEY);
  }

  public NSArray codeExer(EOQualifier qualifier) {
    return codeExer(qualifier, null, false);
  }

  public NSArray codeExer(EOQualifier qualifier, boolean fetch) {
    return codeExer(qualifier, null, fetch);
  }

  public NSArray codeExer(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktldepense.client.metier.EOCodeExer.CODE_MARCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktldepense.client.metier.EOCodeExer.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = codeExer();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_EXER_KEY);
  }

  public void removeFromCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_EXER_KEY);
  }

  public org.cocktail.fwkcktldepense.client.metier.EOCodeExer createCodeExerRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCaramboleCodeExer");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_EXER_KEY);
    return (org.cocktail.fwkcktldepense.client.metier.EOCodeExer) eo;
  }

  public void deleteCodeExerRelationship(org.cocktail.fwkcktldepense.client.metier.EOCodeExer object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_EXER_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeExerRelationships() {
    Enumeration objects = codeExer().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeExerRelationship((org.cocktail.fwkcktldepense.client.metier.EOCodeExer)objects.nextElement());
    }
  }


  public static EOCodeMarche createFwkCaramboleCodeMarche(EOEditingContext editingContext, String cmCode
, String cmLib
) {
    EOCodeMarche eo = (EOCodeMarche) createAndInsertInstance(editingContext, _EOCodeMarche.ENTITY_NAME);    
		eo.setCmCode(cmCode);
		eo.setCmLib(cmLib);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCodeMarche.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCodeMarche.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOCodeMarche localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeMarche)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCodeMarche localInstanceIn(EOEditingContext editingContext, EOCodeMarche eo) {
    EOCodeMarche localInstance = (eo == null) ? null : (EOCodeMarche)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCodeMarche#localInstanceIn a la place.
   */
	public static EOCodeMarche localInstanceOf(EOEditingContext editingContext, EOCodeMarche eo) {
		return EOCodeMarche.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCodeMarche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCodeMarche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeMarche)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeMarche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeMarche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeMarche)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCodeMarche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeMarche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeMarche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeMarche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
