package org.cocktail.fwkcktldepense.server.service;

import static org.junit.Assert.*;

import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.repartitions.AbstractRepartition;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionCodeAnalytique;
import org.cocktail.fwkcktldepense.server.repartitions.RepartitionHorsMarche;
import org.junit.Test;

public class DepenseServiceTest {

	@Test
	public void getFactoryDepenseControle() {
		DepenseService service = new DepenseService();
		assertEquals(FactoryDepenseControleAnalytique.class, 
				service.getFactoryDepenseControle("codeAnalytique").getClass());
		assertEquals(FactoryDepenseControleConvention.class, 
				service.getFactoryDepenseControle("convention").getClass());
	}
	
	@Test(expected=FactoryException.class)
	public void getFactoryDepenseControleCodeInvalide() {
		DepenseService service = new DepenseService(); 
		service.getFactoryDepenseControle("invalide").getClass();
	}

	@Test
	public void getGenerateurRepartition() {
		DepenseService service = new DepenseService();
		assertEquals(RepartitionCodeAnalytique.class, 
				service.getGenerateurRepartition("codeAnalytique").getClass());
		assertEquals(RepartitionHorsMarche.class, 
				service.getGenerateurRepartition("typeAchat").getClass());
	}
	
	@Test(expected=FactoryException.class)
	public void getgetGenerateurRepartitionCodeInvalide() {
		DepenseService service = new DepenseService(); 
		service.getGenerateurRepartition("invalide").getClass();
	}

}
