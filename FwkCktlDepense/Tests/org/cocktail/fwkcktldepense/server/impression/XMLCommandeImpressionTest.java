package org.cocktail.fwkcktldepense.server.impression;

import static org.junit.Assert.*;

import org.junit.Test;

public class XMLCommandeImpressionTest {

	@Test
	public void testEscapeSpecialChars() {
		assertEquals("Test de chaine de caractères avec des caractères spéciaux", XMLCommande.escapeSpecialChars("Test de chaine de caractères avec des caractères spéciaux"));
		assertEquals("Test de chaine de caractères avec des caractères spéciaux: &#x0027;", XMLCommande.escapeSpecialChars("Test de chaine de caractères avec des caractères spéciaux: '"));
		//FIXME a priori il y a toujours des problèmes avec des caractères apostrophes lors du passage du xml à SIX. Peut-etre un caractère left single quote ?
	}

}