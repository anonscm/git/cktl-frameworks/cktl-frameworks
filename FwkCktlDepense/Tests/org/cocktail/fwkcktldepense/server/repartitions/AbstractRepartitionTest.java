package org.cocktail.fwkcktldepense.server.repartitions;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseBudgetException;
import org.cocktail.fwkcktldepense.server.interfaces.IDepenseBudget;
import org.cocktail.fwkcktldepense.server.interfaces.ISourceRepartitionCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class AbstractRepartitionTest {

	@Test
	public void corrigerMontants() {

		IRepartition repartition = new AbstractRepartition() {

			@Override
			protected String getQualifierCondition() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected String getPourcentageKey() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected NSArray getRepartitionDepense(IDepenseBudget depenseBudget) {
				NSArray repartition = new NSArray();
				return null;
			}

			@Override
			protected BigDecimal getRestantHT(IDepenseBudget depenseBudget) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected BigDecimal getRestantTTC(IDepenseBudget depenseBudget) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected String getQualifierReversement() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected NSArray getRepartition(ISourceRepartitionCredit source) {
				// TODO Auto-generated method stub
				return null;
			}

			public void valider(EODepenseBudget depenseBudget)
					throws DepenseBudgetException {
				// TODO Auto-generated method stub
				
			}
		};
		
		
		IDepenseBudget depenseBudget = new IDepenseBudget() {
			
			public BigDecimal restantTtcControlePlanComptable() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantTtcControleMarche() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantTtcControleHorsMarche() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantTtcControleConvention() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantTtcControleAnalytique() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantTtcControleAction() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantHtControlePlanComptable() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantHtControleMarche() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantHtControleHorsMarche() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantHtControleConvention() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantHtControleAnalytique() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal restantHtControleAction() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public Boolean isSurExtourne() {
				// TODO Auto-generated method stub
				return false;
			}
			
			public NSArray depenseControlePlanComptables() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public NSArray depenseControleMarches() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public NSArray depenseControleHorsMarches() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public NSArray depenseControleConventions() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public NSArray depenseControleAnalytiques() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public NSArray depenseControleActions() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal depTtcSaisie() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal depMontantBudgetaire() {
				// TODO Auto-generated method stub
				return null;
			}
			
			public BigDecimal depHtSaisie() {
				// TODO Auto-generated method stub
				return null;
			}

			public IDepenseBudget depenseBudgetReversement() {
				// TODO Auto-generated method stub
				return null;
			}

			public boolean isReversement() {
				// TODO Auto-generated method stub
				return false;
			}
		};
		
		repartition.corrigerMontants(depenseBudget);
		
	}

}
