package org.cocktail.fwkcktldepense.server.finder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFonction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateurFonctionExercice;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = {ZDateUtil.class, FinderUtilisateurFonction.class, FinderUtilisateurFonctionExercice.class })
public class FinderExerciceTest {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
	private static NSTimestamp DATE_OUVERTURE;
	private static NSTimestamp DATE_FERMETURE;

	private static final NSTimestamp DATE_NULL = null;
	private static final EOEditingContext EDC = null;

	static {
		try {
			DATE_OUVERTURE = new NSTimestamp(SDF.parse("02/10/2012"));
			DATE_FERMETURE = new NSTimestamp(SDF.parse("10/10/2012"));
		} catch (Exception e) {
		}
	}

	@Test
	public void testHasUtilisateurDroitAJourBorneOK() throws Exception {
		prepareZDateUtil("05/10/2012");
		assertTrue(FinderExercice.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourHorsBornePosterieur() throws Exception {
		prepareZDateUtil("11/10/2012");
		assertFalse(FinderExercice.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourHorsBorneAnterieur() throws Exception {
		prepareZDateUtil("01/10/2012");
		assertFalse(FinderExercice.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourDateOuvertureNull() throws Exception {
		prepareZDateUtil("11/10/2012");
		assertFalse(FinderExercice.hasUtilisateurDroitAJour(getUtilisateur(DATE_NULL, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourDateFermetureNull() throws Exception {
		prepareZDateUtil("11/10/2012");
		assertTrue(FinderExercice.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_NULL)));
	}

	@Test
	public void testGetExercicesEngageablesPourUtilisateurAvecExercicesEngageables() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_ENGAGE);

		EOExercice exerciceEngageable = getExercice(true, false, false, false);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceEngageable),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesEngageablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(1, exercices.size());
		assertSame(exerciceEngageable, exercices.get(0));
	}


	@Test
	public void testGetExercicesEngageablesPourUtilisateurAvecExercicesEngageablesRestreints() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_ENGAGE_PERIODE_INVENTAIRE);

		EOExercice exerciceEngageableRestreint = getExercice(false, true, false, false);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceEngageableRestreint),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesEngageablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(1, exercices.size());
		assertSame(exerciceEngageableRestreint, exercices.get(0));
	}

	@Test
	public void testGetExercicesEngageablesPourUtilisateurAvecExercicesEngageablesMixtes() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_ENGAGE, EOFonction.FONCTION_ENGAGE_PERIODE_INVENTAIRE);

		EOExercice exerciceEngageable = getExercice(true, false, false, false);
		EOExercice exerciceEngageableRestreint = getExercice(false, true, false, false);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceEngageable),
				getUtilisateurFonctionExercice(exerciceEngageableRestreint),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test 0 exercice engageable et 1 exercice  engageable restreint
		NSArray<EOExercice> exercices = FinderExercice.getExercicesEngageablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(2, exercices.size());
		assertSame(exerciceEngageable, exercices.get(0));
		assertSame(exerciceEngageableRestreint, exercices.get(1));
	}

	@Test
	public void testGetExercicesEngageablesPourUtilisateurSansFonction() throws Exception {
		boolean isNull = true;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(EDC, utilisateurAvecDroit, utilisateurFonction);

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesEngageablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertTrue(exercices.isEmpty());
	}

	@Test
	public void testGetExercicesLiquidablesPourUtilisateurAvecExercicesLiquidables() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_LIQUIDE);

		EOExercice exerciceLiquidable = getExercice(false, false, true, false);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceLiquidable),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesLiquidablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(1, exercices.size());
		assertSame(exerciceLiquidable, exercices.get(0));
	}

	@Test
	public void testGetExercicesLiquidablesPourUtilisateurAvecExercicesLiquidablesRestreints() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_LIQUIDE_PERIODE_INVENTAIRE);

		EOExercice exerciceLiquidableRestreint = getExercice(false, false, false, true);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceLiquidableRestreint),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesLiquidablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(1, exercices.size());
		assertSame(exerciceLiquidableRestreint, exercices.get(0));
	}

	@Test
	public void testGetExercicesLiquidablesPourUtilisateurAvecExercicesLiquidablesMixtes() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_LIQUIDE, EOFonction.FONCTION_LIQUIDE_PERIODE_INVENTAIRE);

		EOExercice exerciceLiquidable = getExercice(false, false, true, false);
		EOExercice exerciceLiquidableRestreint = getExercice(false, false, false, true);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceLiquidable),
				getUtilisateurFonctionExercice(exerciceLiquidableRestreint),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesLiquidablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(2, exercices.size());
		assertSame(exerciceLiquidable, exercices.get(0));
		assertSame(exerciceLiquidableRestreint, exercices.get(1));
	}

	@Test
	public void testGetExercicesLiquidablesPourUtilisateurSansFonction() throws Exception {
		boolean isNull = true;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(EDC, utilisateurAvecDroit, utilisateurFonction);

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesLiquidablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertTrue(exercices.isEmpty());
	}

	@Test
	public void testGetExercicesReimputablesPourUtilisateurAvecExercicesLiquidables() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_REIMPUTER);

		EOExercice exerciceLiquidable = getExercice(false, false, true, false);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceLiquidable),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesReimputablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(1, exercices.size());
		assertSame(exerciceLiquidable, exercices.get(0));
	}

	@Test
	public void testGetExercicesReimputablesPourUtilisateurAvecExercicesLiquidablesRestreints() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_REIMPUTER_PERIODE_INVENTAIRE);

		EOExercice exerciceLiquidableRestreint = getExercice(false, false, false, true);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceLiquidableRestreint),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesReimputablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(1, exercices.size());
		assertSame(exerciceLiquidableRestreint, exercices.get(0));
	}

	@Test
	public void testGetExercicesReimputablesPourUtilisateurAvecExercicesLiquidablesMixtes() throws Exception {
		boolean isNull = false;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(
				EDC, utilisateurAvecDroit, utilisateurFonction, EOFonction.FONCTION_REIMPUTER, EOFonction.FONCTION_REIMPUTER_PERIODE_INVENTAIRE);

		EOExercice exerciceLiquidable = getExercice(false, false, true, false);
		EOExercice exerciceLiquidableRestreint = getExercice(false, false, false, true);
		EOExercice exerciceNeutre = getExercice(false, false, false, false);

		prepareFinderUtilisateurFonctionExercice(
				EDC, utilisateurFonction,
				getUtilisateurFonctionExercice(exerciceLiquidable),
				getUtilisateurFonctionExercice(exerciceLiquidableRestreint),
				getUtilisateurFonctionExercice(exerciceNeutre));

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesReimputablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertEquals(2, exercices.size());
		assertSame(exerciceLiquidable, exercices.get(0));
		assertSame(exerciceLiquidableRestreint, exercices.get(1));
	}

	@Test
	public void testGetExercicesReimputablesPourUtilisateurSansFonction() throws Exception {
		boolean isNull = true;
		prepareZDateUtil("05/10/2012");
		EOUtilisateur utilisateurAvecDroit = getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE);
		EOUtilisateurFonction utilisateurFonction = getFinderUtilisateurFonction(isNull);
		prepareFinderUtilisateurFonction(EDC, utilisateurAvecDroit, utilisateurFonction);

		// test
		NSArray<EOExercice> exercices = FinderExercice.getExercicesReimputablesPourUtilisateur(EDC, utilisateurAvecDroit);
		assertTrue(exercices.isEmpty());
	}

	private EOUtilisateur getUtilisateur(NSTimestamp dateOuverture, NSTimestamp dateFermeture) {
		EOUtilisateur utilisateur = mock(EOUtilisateur.class);
		when(utilisateur.utlOuverture()).thenReturn(dateOuverture);
		when(utilisateur.utlFermeture()).thenReturn(dateFermeture);

		return utilisateur;
	}

	private void prepareZDateUtil(String dateDuJour) throws ParseException {
		PowerMockito.mockStatic(ZDateUtil.class);
		when(ZDateUtil.currentDateNSTimeStamp()).thenReturn(new NSTimestamp(SDF.parse(dateDuJour)));
	}

	private void prepareFinderUtilisateurFonction(EOEditingContext ed, EOUtilisateur utilisateur, EOUtilisateurFonction utilisateurFonction, String... libellesFonction) {
		for (String libelleFonction : libellesFonction) {
			when(FinderUtilisateurFonction.getUtilisateurFonction(ed, utilisateur, libelleFonction))
				.thenReturn(utilisateurFonction);
		}
	}

	private void prepareFinderUtilisateurFonctionExercice(EOEditingContext edc, EOUtilisateurFonction utilisateurFonction, EOUtilisateurFonctionExercice... utilisateurFonctionExercices) {
		PowerMockito.mockStatic(FinderUtilisateurFonctionExercice.class);
		NSMutableDictionary bindings = new NSMutableDictionary();
		bindings.setObjectForKey(utilisateurFonction, "utilisateurFonction");
		when(FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(edc, bindings))
			.thenReturn(new NSArray<EOUtilisateurFonctionExercice>(utilisateurFonctionExercices));
	}


	private EOUtilisateurFonction getFinderUtilisateurFonction(boolean isNull) {
		PowerMockito.mockStatic(FinderUtilisateurFonction.class);
		EOUtilisateurFonction utilisateurFonction = getUtilisateurFonction(isNull);
		return utilisateurFonction;
	}

	private EOUtilisateurFonction getUtilisateurFonction(boolean isNull) {
		EOUtilisateurFonction utilisateurFonction = null;
		if (!isNull) {
			utilisateurFonction = mock(EOUtilisateurFonction.class);
		}
		return utilisateurFonction;
	}

	private EOUtilisateurFonctionExercice getUtilisateurFonctionExercice(EOExercice exercice) {
		EOUtilisateurFonctionExercice utilisateurFnExercice = mock(EOUtilisateurFonctionExercice.class);
		when(utilisateurFnExercice.exercice()).thenReturn(exercice);
		return utilisateurFnExercice;
	}

	private EOExercice getExercice(boolean isEngageable, boolean isEngageableInventaire,
			boolean isLiquidable, boolean isLiquidableInventaire) {
		EOExercice exercice = mock(EOExercice.class);
		when(exercice.estEngageable()).thenReturn(isEngageable);
		when(exercice.estEngageableRestreint()).thenReturn(isEngageableInventaire);
		when(exercice.estLiquidable()).thenReturn(isLiquidable);
		when(exercice.estLiquidableRestreint()).thenReturn(isLiquidableInventaire);
		return exercice;
	}
}
