package org.cocktail.fwkcktldepense.server.metier;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.util.ZDateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.webobjects.foundation.NSTimestamp;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ZDateUtil.class)
public class EOUtilisateurTest {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
	private static NSTimestamp DATE_OUVERTURE;
	private static NSTimestamp DATE_FERMETURE;

	private static final NSTimestamp DATE_NULL = null;

	static {
		try {
			DATE_OUVERTURE = new NSTimestamp(SDF.parse("02/10/2012"));
			DATE_FERMETURE = new NSTimestamp(SDF.parse("10/10/2012"));
		} catch (Exception e) {
		}
	}

	@Test
	public void testHasUtilisateurDroitAJourBorneOK() throws Exception {
		prepareZDateUtil("05/10/2012");
		assertTrue(EOUtilisateur.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourHorsBornePosterieur() throws Exception {
		prepareZDateUtil("11/10/2012");
		assertFalse(EOUtilisateur.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourHorsBorneAnterieur() throws Exception {
		prepareZDateUtil("01/10/2012");
		assertFalse(EOUtilisateur.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourDateOuvertureNull() throws Exception {
		prepareZDateUtil("11/10/2012");
		assertFalse(EOUtilisateur.hasUtilisateurDroitAJour(getUtilisateur(DATE_NULL, DATE_FERMETURE)));
	}

	@Test
	public void testHasUtilisateurDroitAJourDateFermetureNull() throws Exception {
		prepareZDateUtil("11/10/2012");
		assertTrue(EOUtilisateur.hasUtilisateurDroitAJour(getUtilisateur(DATE_OUVERTURE, DATE_NULL)));
	}

	private EOUtilisateur getUtilisateur(NSTimestamp dateOuverture, NSTimestamp dateFermeture) {
		EOUtilisateur utilisateur = mock(EOUtilisateur.class);
		when(utilisateur.utlOuverture()).thenReturn(dateOuverture);
		when(utilisateur.utlFermeture()).thenReturn(dateFermeture);

		return utilisateur;
	}

	private void prepareZDateUtil(String dateDuJour) throws ParseException {
		PowerMockito.mockStatic(ZDateUtil.class);
		when(ZDateUtil.currentDateNSTimeStamp()).thenReturn(new NSTimestamp(SDF.parse(dateDuJour)));
	}

}
