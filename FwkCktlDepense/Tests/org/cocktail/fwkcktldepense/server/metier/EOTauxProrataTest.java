package org.cocktail.fwkcktldepense.server.metier;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata.ETypeMontant;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSDictionary;
import com.wounit.rules.MockEditingContext;

public class EOTauxProrataTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("CaramboleB2bCxml", "FwkCktlB2bCxml", "FwkCktlDroits", "FwkCktlDroitsUtils", "FwkCktlPersonne", "FwkCktlWebApp", "FwkCktlGed", "GB_Courrier", "carambole");    

    @Test
    public void testHtEtTtcAPartirMontantBudgetaireDispoAvecDepassement() {

    	BigDecimal tauxProrata_100 = BigDecimal.valueOf(0.00d).setScale(2);
        BigDecimal mtHT = BigDecimal.valueOf(81.1d).setScale(2);
        BigDecimal mtTTC = BigDecimal.valueOf(97.32d).setScale(2);
        BigDecimal mtDispo = BigDecimal.valueOf(96.57d).setScale(2);
        EOTypeEtat typeEtat = EOTypeEtat.creerInstance(ec);
        
        EOTauxProrata txProrata = EOTauxProrata.createEOTauxProrata(ec, tauxProrata_100, typeEtat);
        
        NSDictionary<ETypeMontant, BigDecimal> results = txProrata.htEtTtcAPartirMontantBudgetaireDispo(mtHT, mtTTC, mtDispo);
        
        assertEquals("80.48", results.objectForKey(ETypeMontant.HT).toString());
        assertEquals("96.57", results.objectForKey(ETypeMontant.TTC).toString());
    }
    
    @Test
    public void testHtEtTtcAPartirMontantBudgetaireDispoSansDepassement() {
    
    	BigDecimal tauxProrata_100 = BigDecimal.valueOf(0.00d).setScale(2);
        BigDecimal mtHT = BigDecimal.valueOf(70.1d).setScale(2);
        BigDecimal mtTTC = BigDecimal.valueOf(84.12d).setScale(2);
        BigDecimal mtDispo = BigDecimal.valueOf(96.57d).setScale(2);
        EOTypeEtat typeEtat = EOTypeEtat.creerInstance(ec);
        EOTauxProrata txProrata = EOTauxProrata.createEOTauxProrata(ec, tauxProrata_100, typeEtat);
        NSDictionary<ETypeMontant, BigDecimal> results = txProrata.htEtTtcAPartirMontantBudgetaireDispo(mtHT, mtTTC, mtDispo);
        
        assertEquals("70.10", results.objectForKey(ETypeMontant.HT).toString());
        assertEquals("84.12", results.objectForKey(ETypeMontant.TTC).toString());
    }
    
}
