package org.cocktail.fwkcktldepense.server.util;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class CalculsTest {

	private final static BigDecimal TTC_ZERO = Calculs.ZERO;
	private final static BigDecimal HT_ZERO = Calculs.ZERO;
	private final static BigDecimal TVA_ZERO = Calculs.ZERO;
	private final static BigDecimal TAUX_ZERO = Calculs.ZERO;
	
	@Test
	public void calcultauxTVA() {
		Calculs calculs = new Calculs();

		assertEquals("0.00",
				calculs.calculTauxTva(TTC_ZERO, HT_ZERO)
						.setScale(2).toString());
		assertEquals("0.00",
				calculs.calculTauxTva(new BigDecimal(100), new BigDecimal(100))
						.setScale(2).toString());
		assertEquals("0.20",
				calculs.calculTauxTva(new BigDecimal(120), new BigDecimal(100))
						.setScale(2).toString());
		assertEquals(
				"0.0548523207",
				calculs.calculTauxTva(new BigDecimal(20), new BigDecimal(18.96))
						.toString());

	}

	@Test
	public void calculMontantTVAaPartirHT() {
		Calculs calculs = new Calculs();

		assertEquals(
				"0.00",
				calculs.calculMontantTVAaPartirHT(TAUX_ZERO,
						HT_ZERO, 2).toString());
		assertEquals(
				"0",
				calculs.calculMontantTVAaPartirHT(TAUX_ZERO,
						HT_ZERO, 0).toString());
		assertEquals(
				"0.00",
				calculs.calculMontantTVAaPartirHT(TAUX_ZERO,
						new BigDecimal(120), 2).toString());
		assertEquals(
				"0.00",
				calculs.calculMontantTVAaPartirHT(new BigDecimal(20),
						HT_ZERO, 2).toString());
		assertEquals(
				"20.00",
				calculs.calculMontantTVAaPartirHT(new BigDecimal(20),
						new BigDecimal(100), 2).toString());
		assertEquals(
				"1.04",
				calculs.calculMontantTVAaPartirHT(new BigDecimal(5.5),
						new BigDecimal(18.96), 2).toString());

	}

	@Test
	public void calculTVAaPartirTTC() {
		Calculs calculs = new Calculs();

		assertEquals(
				"0.00",
				calculs.calculMontantTVAaPartirTTC(TAUX_ZERO,
						TTC_ZERO, 2).toString());
		assertEquals(
				"0",
				calculs.calculMontantTVAaPartirTTC(TAUX_ZERO,
						TTC_ZERO, 0).toString());
		assertEquals(
				"0.00",
				calculs.calculMontantTVAaPartirTTC(TAUX_ZERO,
						new BigDecimal(120), 2).toString());
		assertEquals(
				"0.00",
				calculs.calculMontantTVAaPartirTTC(new BigDecimal(20),
						TTC_ZERO, 2).toString());
		assertEquals(
				"20.00",
				calculs.calculMontantTVAaPartirTTC(new BigDecimal(20.0),
						new BigDecimal(120), 2).toString());
		assertEquals(
				"0.99",
				calculs.calculMontantTVAaPartirTTC(new BigDecimal(5.5),
						new BigDecimal(18.96), 2).toString());

	}

	@Test
	public void calculHTaPartirTTC() {
		Calculs calculs = new Calculs();

		assertEquals(
				"0.00",
				calculs.calculHTaPartirTTC(TAUX_ZERO,
						TTC_ZERO, 2).toString());
		assertEquals(
				"0",
				calculs.calculHTaPartirTTC(TAUX_ZERO,
						TTC_ZERO, 0).toString());
		assertEquals(
				"120.00",
				calculs.calculHTaPartirTTC(TAUX_ZERO,
						new BigDecimal(120), 2).toString());
		assertEquals(
				"0.00",
				calculs.calculHTaPartirTTC(new BigDecimal(20),
						TTC_ZERO, 2).toString());
		assertEquals(
				"100.00",
				calculs.calculHTaPartirTTC(new BigDecimal(20),
						new BigDecimal(120), 2).toString());
		assertEquals(
				"17.97",
				calculs.calculHTaPartirTTC(new BigDecimal(5.5),
						new BigDecimal(18.96), 2).toString());

	}

	@Test
	public void calculTTCaPartirHT() {
		Calculs calculs = new Calculs();

		assertEquals(
				"0.00",
				calculs.calculTTCaPartirHT(TAUX_ZERO,
						HT_ZERO, 2).toString());
		assertEquals(
				"0",
				calculs.calculTTCaPartirHT(TAUX_ZERO,
						HT_ZERO, 0).toString());
		assertEquals(
				"120.00",
				calculs.calculTTCaPartirHT(TAUX_ZERO,
						new BigDecimal("120"), 2).toString());
		assertEquals(
				"0.00",
				calculs.calculTTCaPartirHT(new BigDecimal(20),
						HT_ZERO, 2).toString());
		assertEquals(
				"120.00",
				calculs.calculTTCaPartirHT(new BigDecimal(20),
						new BigDecimal("100"), 2).toString());
		assertEquals(
				"20.00",
				calculs.calculTTCaPartirHT(new BigDecimal(5.5),
						new BigDecimal("18.96"), 2).toString());

	}

	@Test
	public void calculMontantBudgetaire() {
		Calculs calculs = new Calculs();

		assertEquals(Calculs.ZERO, calculs.calculMontantBudgetaire(
				HT_ZERO, TVA_ZERO, TAUX_ZERO));
		assertEquals(Calculs.ZERO, calculs.calculMontantBudgetaire(
				HT_ZERO, TVA_ZERO, Calculs.CENT));
		assertEquals(
				"100.00",
				calculs.calculMontantBudgetaire(new BigDecimal("100.00"),
						TVA_ZERO, TAUX_ZERO).toString());
		assertEquals(
				"120.00",
				calculs.calculMontantBudgetaire(new BigDecimal("100.00"),
						new BigDecimal("20.00"), TAUX_ZERO).toString());
		assertEquals(
				"100.00",
				calculs.calculMontantBudgetaire(new BigDecimal("100.00"),
						new BigDecimal("20.00"), Calculs.CENT).toString());
		assertEquals(
				"116.00",
				calculs.calculMontantBudgetaire(new BigDecimal("100.00"),
						new BigDecimal("20.00"), new BigDecimal("20.00"))
						.toString());
	}

	@Test
	public void testHtEtTtcAPartirMontantBudgetaireDispoAvecDepassement() {
		Calculs calculs = new Calculs();

		BigDecimal tauxProrata_100 = BigDecimal.valueOf(0.00d).setScale(2);
		BigDecimal mtHT = new BigDecimal("81.10");
		BigDecimal mtTTC = new BigDecimal("97.32");
		BigDecimal mtDispo = new BigDecimal("96.57");

		MontantTransaction transaction = calculs
				.calculMontantsProratiseAPartirDisponible(mtHT, mtTTC, mtDispo,
						tauxProrata_100);

		assertEquals("80.475", transaction.getMontantHT().setScale(3).toString());
		assertEquals("96.570", transaction.getMontantTTC().setScale(3).toString());
	}

	@Test
	public void testHtEtTtcAPartirMontantBudgetaireDispoSansDepassement() {
		Calculs calculs = new Calculs();

		BigDecimal tauxProrata_100 = BigDecimal.valueOf(0.00d).setScale(2);
		BigDecimal mtHT = new BigDecimal("70.10");
		BigDecimal mtTTC = new BigDecimal("84.12");
		BigDecimal mtDispo = new BigDecimal("96.57");

		MontantTransaction transaction = calculs
				.calculMontantsProratiseAPartirDisponible(mtHT, mtTTC, mtDispo,
						tauxProrata_100);

		assertEquals("70.100", transaction.getMontantHT().setScale(3).toString());
		assertEquals("84.120", transaction.getMontantTTC().setScale(3).toString());

	}

}
