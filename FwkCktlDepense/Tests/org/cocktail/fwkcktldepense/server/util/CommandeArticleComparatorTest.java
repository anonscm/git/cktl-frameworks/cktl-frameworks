/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktldepense.server.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.util.CommandeArticleComparator;
import org.junit.Test;

public class CommandeArticleComparatorTest {

	private static final String ART_DESC_1 = "01:00 Configuration 2 Ultra-leger  Vostro Notebook 3350";
	private static final String ART_DESC_2 = "01:03 Carte graphique:Carte graphique HD-6490M avec 160-coeurs + memoire GDDR5 ultrarapide";
	private static final String ART_DESC_3 = "01:27 Logiciel de protection Antivirus:Logiciel antivirus non inclus";
	private static final String ART_DESC_4 = "02:00 Configuration 1 Ultra-l?ger  Latitude E6320";
	private static final String ART_DESC_5 = "02:06 Connectivit? sans fil:Mini-carte PCI Dell Wireless?1501 (802.11?b/g/n 1X1) - EMEA";

	@Test
	public void testCompareArticlesNonNull() {
		CommandeArticleComparator comparator = new CommandeArticleComparator();

		EOArticle article1 = buildArticle(ART_DESC_1);
		EOArticle article2 = buildArticle(ART_DESC_2);
		EOArticle article3 = buildArticle(ART_DESC_3);
		EOArticle article4 = buildArticle(ART_DESC_4);
		EOArticle article5 = buildArticle(ART_DESC_5);

		// shuffle list
		List<EOArticle> articles = Arrays.asList(article1, article2, article3, article4, article5);
		Collections.shuffle(articles);

		Collections.sort(articles, comparator);

		Assert.assertEquals(ART_DESC_1, articles.get(0).artLibelle());
		Assert.assertEquals(ART_DESC_2, articles.get(1).artLibelle());
		Assert.assertEquals(ART_DESC_3, articles.get(2).artLibelle());
		Assert.assertEquals(ART_DESC_4, articles.get(3).artLibelle());
		Assert.assertEquals(ART_DESC_5, articles.get(4).artLibelle());
	}

	@Test
	public void testCompareArticlesNull() {
		CommandeArticleComparator comparator = new CommandeArticleComparator();

		EOArticle article1 = null;
		EOArticle article2 = buildArticle(ART_DESC_2);
		EOArticle article3 = buildArticle(ART_DESC_3);
		EOArticle article4 = null;
		EOArticle article5 = buildArticle(ART_DESC_5);

		// shuffle list
		List<EOArticle> articles = Arrays.asList(article1, article2, article3, article4, article5);
		Collections.shuffle(articles);

		Collections.sort(articles, comparator);

		Assert.assertEquals(ART_DESC_2, articles.get(0).artLibelle());
		Assert.assertEquals(ART_DESC_3, articles.get(1).artLibelle());
		Assert.assertEquals(ART_DESC_5, articles.get(2).artLibelle());
		Assert.assertNull(articles.get(3));
		Assert.assertNull(articles.get(4));
	}

	@Test
	public void testCompareDescriptionArticlesNull() {
		CommandeArticleComparator comparator = new CommandeArticleComparator();

		EOArticle article1 = buildArticle(ART_DESC_1);
		EOArticle article2 = buildArticle(null);
		EOArticle article3 = buildArticle(ART_DESC_3);
		EOArticle article4 = buildArticle(null);
		EOArticle article5 = buildArticle(ART_DESC_5);

		// shuffle list
		List<EOArticle> articles = Arrays.asList(article1, article2, article3, article4, article5);
		Collections.shuffle(articles);

		Collections.sort(articles, comparator);

		Assert.assertEquals(ART_DESC_1, articles.get(0).artLibelle());
		Assert.assertEquals(ART_DESC_3, articles.get(1).artLibelle());
		Assert.assertEquals(ART_DESC_5, articles.get(2).artLibelle());
		Assert.assertNull(articles.get(3).artLibelle());
		Assert.assertNull(articles.get(4).artLibelle());
	}

	private EOArticle buildArticle(String description) {
		EOArticle article = mock(EOArticle.class);
		when(article.artLibelle()).thenReturn(description);
		return article;
	}

}
