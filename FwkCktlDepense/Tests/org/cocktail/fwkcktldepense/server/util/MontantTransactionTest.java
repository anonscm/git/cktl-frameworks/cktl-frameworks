package org.cocktail.fwkcktldepense.server.util;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class MontantTransactionTest {

	@Test
	public void multiply() {
		
		MontantTransaction montant = new MontantTransaction(new BigDecimal(100), new BigDecimal(120), new BigDecimal(110));
		MontantTransaction attendu = new MontantTransaction(new BigDecimal(200), new BigDecimal(240), new BigDecimal(220));
		
		assertTrue(attendu.equals(montant.multiply(new BigDecimal(2))));
		
	}

	@Test
	public void divide() {
		
		MontantTransaction attendu = new MontantTransaction(new BigDecimal(100), new BigDecimal(120), new BigDecimal(110));
		MontantTransaction montant = new MontantTransaction(new BigDecimal(200), new BigDecimal(240), new BigDecimal(220));
		
		assertTrue(attendu.equals(montant.divide(new BigDecimal(2))));
		
	}

	@Test
	public void add() {
		
		MontantTransaction attendu = new MontantTransaction(new BigDecimal(12), new BigDecimal(24), new BigDecimal(41));
		MontantTransaction montant = new MontantTransaction(new BigDecimal(5), new BigDecimal(10), new BigDecimal(20));
		MontantTransaction ajout = new MontantTransaction(new BigDecimal(7), new BigDecimal(14), new BigDecimal(21));

		assertTrue(attendu.equals(montant.add(ajout)));
		
	}

	@Test
	public void subtract() {
		
		MontantTransaction attendu = new MontantTransaction(new BigDecimal(2), new BigDecimal(4), new BigDecimal(1));
		MontantTransaction soustrait = new MontantTransaction(new BigDecimal(5), new BigDecimal(10), new BigDecimal(20));
		MontantTransaction montant = new MontantTransaction(new BigDecimal(7), new BigDecimal(14), new BigDecimal(21));

		assertTrue(attendu.equals(montant.subtract(soustrait)));
		
	}

	@Test
	public void plafonner() {
		
		MontantTransaction attendu = new MontantTransaction(new BigDecimal(100), new BigDecimal(240), new BigDecimal(220));
		MontantTransaction montant = new MontantTransaction(new BigDecimal(120), new BigDecimal(240), new BigDecimal(220));
		MontantTransaction roof = new MontantTransaction(new BigDecimal(100), new BigDecimal(600), new BigDecimal(220));

		assertTrue(attendu.equals(montant.plafonner(roof)));
		
	}

}
