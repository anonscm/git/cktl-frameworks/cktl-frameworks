SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.6
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- DT#5304, DT#5289 : lors de la suppression d'une liquidation directe (inclus liquidation définitive de dépassement de crédits d'extourne ou liquidation définitive sur enveloppe d'extourne), l'engagement créé initialement créé avec la dépense est également supprimé.
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2104';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/


