--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2
-- Type : DML
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.2.0
-- Date de publication :  04/01/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Modification du schema pour prise en compte du service facturier, diverses corrections 
-- pour les réimputations.
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_jefydepense_2020;
commit;

drop procedure grhum.inst_patch_jefydepense_2020;
