SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.2.0
-- Date de publication :  04/01/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Modification du schema pour prise en compte du service facturier, diverses corrections 
-- pour les réimputations.
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

 insert into jefy_depense.db_version values (2020,'2020',sysdate,sysdate,null);
 commit;



----------------------------------------------
-- modifications pour le service facturier
----------------------------------------------


-- rendre le champ date_service_fait de depense_papier facultatif
ALTER TABLE JEFY_DEPENSE.DEPENSE_PAPIER MODIFY(DPP_DATE_SERVICE_FAIT  NULL);

-- ajouter les champs necessaires aux infos concernant le service fait
ALTER TABLE JEFY_DEPENSE.DEPENSE_PAPIER ADD (DPP_SF_PERS_ID  NUMBER);
ALTER TABLE JEFY_DEPENSE.DEPENSE_PAPIER ADD (DPP_SF_DATE  DATE);
ALTER TABLE JEFY_DEPENSE.DEPENSE_PAPIER ADD (ECD_ORDRE  NUMBER);

grant references on GRHUM.PERSONNE to jefy_depense;

ALTER TABLE JEFY_DEPENSE.DEPENSE_PAPIER ADD (
  CONSTRAINT FK_DPP_SF_PERSONNE 
 FOREIGN KEY (DPP_SF_PERS_ID) 
 REFERENCES GRHUM.PERSONNE (PERS_ID));

COMMENT ON COLUMN JEFY_DEPENSE.DEPENSE_PAPIER.DPP_SF_PERS_ID IS 'Reference a l''utilisateur qui atteste le service fait';
COMMENT ON COLUMN JEFY_DEPENSE.DEPENSE_PAPIER.DPP_SF_DATE IS 'Date attestation service fait';
COMMENT ON COLUMN JEFY_DEPENSE.DEPENSE_PAPIER.ECD_ORDRE IS 'Reference a un MARACUJA.ECRITURE_DETAIL pour emargement auto (valeur par defaut reprise dans depense_ctrl_planco)';


-- meme modifs table z_depense_papier
ALTER TABLE JEFY_DEPENSE.Z_DEPENSE_PAPIER MODIFY(DPP_DATE_SERVICE_FAIT NULL);

-- ajouter les champs necessaires aux infos concernant le service fait
ALTER TABLE JEFY_DEPENSE.Z_DEPENSE_PAPIER ADD (DPP_SF_PERS_ID  NUMBER);
ALTER TABLE JEFY_DEPENSE.Z_DEPENSE_PAPIER ADD (DPP_SF_DATE  DATE);
ALTER TABLE JEFY_DEPENSE.Z_DEPENSE_PAPIER ADD (ECD_ORDRE  NUMBER);



grant select ON grhum.banque to jefy_depense with grant option;
grant execute on MARACUJA.abricot_impressions to jefy_depense;
grant execute on jefy_admin.API_JASPER_PARAM to jefy_depense;
grant select on maracuja.depense to jefy_depense;




CREATE OR REPLACE FORCE VIEW JEFY_DEPENSE.V_ECRITURE_DETAIL
(ECD_ORDRE, EXE_ORDRE, GES_CODE, ECR_ORDRE, PCO_NUM, 
 ECD_LIBELLE, ECD_SENS, ECR_NUMERO, ECD_DEBIT, ECD_CREDIT)
AS 
select d.ECD_ORDRE, d.EXE_ORDRE, d.GES_CODE, d.ECR_ORDRE, d.PCO_NUM, d.ECD_LIBELLE, d.ECD_SENS, e.ecr_numero, ecd_debit, ecd_credit
from maracuja.ECRITURE_DETAIL d, maracuja.ecriture e
where e.ecr_ordre=d.ecr_ordre;
/




CREATE OR REPLACE FORCE VIEW JEFY_DEPENSE.V_DEPENSE_A_TRANSFERER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 CLE_RIB, DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, 
 PERS_TYPE, PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, 
 ORG_UB, ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, 
 TCD_LIBELLE, DPP_NUMERO_FACTURE, DPP_ID, DPP_HT_SAISIE, DPP_TVA_SAISIE, 
 DPP_TTC_SAISIE, DPP_DATE_FACTURE, DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, 
 DPP_NB_PIECE, UTL_ORDRE, TBO_ORDRE, DEP_ID, PCO_NUM, 
 DPCO_ID, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, ADR_CIVILITE, 
 ADR_NOM, ADR_PRENOM, ADR_ADRESSE1, ADR_ADRESSE2, ADR_VILLE, 
 ADR_CP, UTLNOMPRENOM, SFNOMPRENOM, TYAP_LIBELLE)
AS 
select 
r.C_BANQUE,
r.C_GUICHET,
r.NO_COMPTE,
r.iban,
r.bic,
r.cle_rib,
bq.DOMICILIATION,
mp.MOD_libelle,
mp.MOD_CODE,
mp.MOD_DOM,
p.pers_type,
p.pers_libelle,
p.pers_lc,
e.EXE_EXERCICE,
eb.org_id,
o.ORG_UB,
o.ORG_CR,
o.ORG_SOUSCR,
eb.tcd_ordre,
tc.tcd_code,
tc.tcd_libelle,
dp.dpp_numero_facture,
dp.DPP_ID,
dp.DPP_HT_SAISIE,
dp.DPP_TVA_SAISIE,
dp.DPP_TTC_SAISIE,
dp.DPP_DATE_FACTURE,
dp.DPP_DATE_SAISIE,
dp.DPP_DATE_RECEPTION,
dp.DPP_DATE_SERVICE_FAIT,
dp.DPP_NB_PIECE,
dp.utl_ordre,
dcp.tbo_ordre,
dcp.dep_id dep_id,
dcp.pco_num,
dcp.DPCO_id  DPCO_id ,
dcp.DPCO_HT_SAISIE,
dcp.DPCO_TVA_SAISIE,
dcp.DPCO_TTC_SAISIE,
f.ADR_CIVILITE,
f.ADR_NOM,
f.ADR_PRENOM,
f.ADR_ADRESSE1,
f.ADR_ADRESSE2,
f.ADR_VILLE,
f.ADR_CP,
pu.pers_libelle||' '||pu.pers_lc utlNomPrenom,
pa.pers_libelle||' '||pa.pers_lc sfNomPrenom,
tyap.TYAP_LIBELLE
from
jefy_depense.engage_budget eb,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dp,
jefy_depense.depense_ctrl_planco dcp,
grhum.v_fournis_grhum f,
jefy_admin.EXERCICE e,
jefy_admin.utilisateur u,
grhum.personne p,
grhum.personne pu,
grhum.personne pa,
grhum.ribfour_ulr r,
grhum.banque bq,
maracuja.MODE_PAIEMENT mp,
jefy_admin.type_credit tc,
jefy_admin.organ o,
jefy_Admin.type_application tyap 
where eb.eng_id = db.eng_id
and   db.dpp_id = dp.dpp_id
and   db.dep_id = dcp.dep_id
and   dp.fou_ordre = f.fou_ordre
and   dp.exe_ordre = e.exe_ordre
and   dp.utl_ordre = u.utl_ordre
and   p.pers_id = u.pers_id
and dp.DPP_SF_PERS_ID = pa.pers_id(+)
and   dp.rib_ordre = r.rib_ordre(+)
and   r.C_BANQUE = bq.C_BANQUE(+)
and   r.C_GUICHET = bq.C_GUICHET(+)
and   mp.mod_ordre = dp.mod_ordre
and   eb.tcd_ordre = tc.tcd_ordre
and   o.org_id = eb.org_id
and u.pers_id = pu.pers_id
and   man_id is null
and eb.tyap_id=tyap.tyap_id
and tbo_ordre in (1,2,4);
/


grant select on MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER to jefy_depense;


grant select, references on jefy_inventaire.inventaire_comptable to jefy_depense;
grant select, references on jefy_inventaire.inventaire_comptable_orv to jefy_depense;

grant select, update, references on jefy_inventaire.cle_inventaire_comptable to jefy_depense;
grant select, update, references on jefy_inventaire.amortissement to jefy_depense;

grant select, references on maracuja.plan_comptable_amo to jefy_depense;


--15_tables.PDEPENSEs.sql
CREATE TABLE JEFY_DEPENSE.PDEPENSE_BUDGET
(
  PDEP_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL, 
  DPP_ID NUMBER NOT NULL, 
  ENG_ID NUMBER NOT NULL, 
  PDEP_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDEP_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDEP_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDEP_TTC_SAISIE NUMBER(12,2) NOT NULL, 
  TAP_ID NUMBER NOT NULL, 
  UTL_ORDRE NUMBER NOT NULL
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_BUDGET IS 'Informations utiles a la pre liquidation pour chaque ENGAGE concerne';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.PDEP_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.DPP_ID IS 'Cle etrangere table jefy_depense.depense_papier';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.ENG_ID IS 'Cle etrangere table jefy_depense.engage_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.PDEP_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.PDEP_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.PDEP_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.PDEP_TTC_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.TAP_ID IS 'Cle etrangere table jefy_admin.taux_prorata';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_BUDGET.UTL_ORDRE IS 'Cle etrangere table jefy_admin.utilisateur';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_BUDGET ON JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD (CONSTRAINT PK_PDEPENSE_BUDGET PRIMARY KEY (PDEP_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD (
  CONSTRAINT CH_PDEPENSE_BUDGET_1 CHECK (PDEP_ttc_saisie=PDEP_ht_saisie+PDEP_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD (
  CONSTRAINT CH_PDEPENSE_BUDGET_2 CHECK (abs(PDEP_montant_budgetaire)>=abs(PDEP_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD (
  CONSTRAINT CH_PDEPENSE_BUDGET_3 CHECK (abs(PDEP_montant_budgetaire)<=abs(PDEP_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD ( CONSTRAINT FK_PDEPENSE_BUDGET_TAP_ID FOREIGN KEY (TAP_ID) 
    REFERENCES JEFY_ADMIN.TAUX_PRORATA (TAP_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD ( CONSTRAINT FK_PDEPENSE_BUDGET_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD ( CONSTRAINT FK_PDEPENSE_BUDGET_ENG_ID FOREIGN KEY (ENG_ID) 
    REFERENCES JEFY_DEPENSE.ENGAGE_BUDGET (ENG_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD ( CONSTRAINT FK_PDEPENSE_BUDGET_DPP_ID FOREIGN KEY (DPP_ID) 
    REFERENCES JEFY_DEPENSE.DEPENSE_PAPIER (DPP_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_BUDGET ADD ( CONSTRAINT FK_PDEPENSE_BUDGET_UTL_ORDRE FOREIGN KEY (UTL_ORDRE) 
    REFERENCES JEFY_ADMIN.UTILISATEUR (UTL_ORDRE) DEFERRABLE INITIALLY DEFERRED);
	
create sequence JEFY_DEPENSE.PDEPENSE_BUDGET_SEQ start with 1 nocycle nocache;




CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION
(
  PDACT_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL, 
  PDEP_ID NUMBER NOT NULL, 
  TYAC_ID NUMBER NOT NULL,  
  PDACT_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDACT_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDACT_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDACT_TTC_SAISIE NUMBER(12,2) NOT NULL
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION IS 'Table de repartition des actions pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.PDACT_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.PDEP_ID IS 'Cle etrangere table jefy_depense.pdepense_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.PDACT_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.PDACT_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.PDACT_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.PDACT_TTC_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ACTION.TYAC_ID IS '';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_ACTION ON JEFY_DEPENSE.PDEPENSE_CTRL_ACTION (PDACT_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION ADD (CONSTRAINT PK_PDEPENSE_CTRL_ACTION PRIMARY KEY (PDACT_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_ACTION_1 CHECK (PDACT_ttc_saisie=PDACT_ht_saisie+PDACT_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_ACTION_2 CHECK (abs(PDACT_montant_budgetaire)>=abs(PDACT_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_ACTION_3 CHECK (abs(PDACT_montant_budgetaire)<=abs(PDACT_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION ADD ( CONSTRAINT FK_PDEPENSE_CTRL_ACT_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ACTION ADD ( CONSTRAINT FK_PDEPENSE_CTRL_ACT_PDEP_ID FOREIGN KEY (PDEP_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) DEFERRABLE INITIALLY DEFERRED);

create sequence JEFY_DEPENSE.PDEPENSE_CTRL_ACTION_SEQ start with 1 nocycle nocache;




CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE
(
  PDANA_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL,
  PDEP_ID NUMBER NOT NULL, 
  CAN_ID NUMBER NOT NULL,
  PDANA_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDANA_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDANA_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDANA_TTC_SAISIE NUMBER(12,2) NOT NULL
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE IS 'Table de repartition des codes analytiques pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.PDANA_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.PDEP_ID IS 'Cle etrangere table jefy_depense.pdepense_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.CAN_ID IS 'Cle etrangere table jefy_admin.code_analytique';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.PDANA_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.PDANA_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.PDANA_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE.PDANA_TTC_SAISIE IS '';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_ANALYTIQUE ON JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE (PDANA_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD (CONSTRAINT PK_PDEPENSE_CTRL_ANALYTIQUE PRIMARY KEY (PDANA_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_ANALYTIQUE_1 CHECK (PDANA_ttc_saisie=PDANA_ht_saisie+PDANA_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_ANALYTIQUE_2 CHECK (abs(PDANA_montant_budgetaire)>=abs(PDANA_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_ANALYTIQUE_3 CHECK (abs(PDANA_montant_budgetaire)<=abs(PDANA_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_ANA_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_ANAL_PDEP_ID FOREIGN KEY (PDEP_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_ANAL_CAN_ID FOREIGN KEY (CAN_ID) 
    REFERENCES JEFY_ADMIN.CODE_ANALYTIQUE (CAN_ID) DEFERRABLE INITIALLY DEFERRED);

create sequence JEFY_DEPENSE.PDEPENSE_CTRL_ANALYTIQUE_SEQ start with 1 nocycle nocache;




CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION
(
  PDCON_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL,
  PDEP_ID NUMBER NOT NULL, 
  CONV_ORDRE NUMBER NOT NULL,
  PDCON_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDCON_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDCON_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDCON_TTC_SAISIE NUMBER(12,2) NOT NULL
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION IS 'Table de repartition des conventions pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.PDCON_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.PDEP_ID IS 'Cle etrangere table jefy_depense.pdepense_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.CONV_ORDRE IS 'Cle etrangere table accords.convention_non_limitative';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.PDCON_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.PDCON_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.PDCON_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION.PDCON_TTC_SAISIE IS '';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_CONVENTION ON JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION (PDCON_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION ADD (CONSTRAINT PK_PDEPENSE_CTRL_CONVENTION PRIMARY KEY (PDCON_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_CONVENTION_1 CHECK (PDCON_ttc_saisie=PDCON_ht_saisie+PDCON_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_CONVENTION_2 CHECK (abs(PDCON_montant_budgetaire)>=abs(PDCON_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_CONVENTION_3 CHECK (abs(PDCON_montant_budgetaire)<=abs(PDCON_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION ADD ( CONSTRAINT FK_PDEPENSE_CTRL_CON_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION ADD ( CONSTRAINT FK_PDEPENSE_CTRL_CON_PDEP_ID FOREIGN KEY (PDEP_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) DEFERRABLE INITIALLY DEFERRED);

create sequence JEFY_DEPENSE.PDEPENSE_CTRL_CONVENTION_SEQ start with 1 nocycle nocache;





CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE
(
  PDHOM_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL, 
  PDEP_ID NUMBER NOT NULL, 
  TYPA_ID NUMBER NOT NULL,
  CE_ORDRE NUMBER NOT NULL, 
  PDHOM_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDHOM_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDHOM_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDHOM_TTC_SAISIE NUMBER(12,2) NOT NULL
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE IS 'Table de repartition des codes nomenclature hors marches pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.PDHOM_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.PDEP_ID IS 'Cle etrangere table jefy_depense.pdepense_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.TYPA_ID IS 'Cle etrangere table jefy_depense.type_achat';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.CE_ORDRE IS 'Cle etrangere table jefy_marches.code_exer';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.PDHOM_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.PDHOM_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.PDHOM_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE.PDHOM_TTC_SAISIE IS '';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_HORS_MARCHE ON JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE (PDHOM_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD (CONSTRAINT PK_PDEPENSE_CTRL_HORS_MARCHE PRIMARY KEY (PDHOM_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_HORS_MARCHE_1 CHECK (PDHOM_ttc_saisie=PDHOM_ht_saisie+PDHOM_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_HORS_MARCHE_2 CHECK (abs(PDHOM_montant_budgetaire)>=abs(PDHOM_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_HORS_MARCHE_3 CHECK (abs(PDHOM_montant_budgetaire)<=abs(PDHOM_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_HM_TYPA_ID FOREIGN KEY (TYPA_ID) 
    REFERENCES JEFY_DEPENSE.TYPE_ACHAT (TYPA_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_HM_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_HM_PDEP_ID FOREIGN KEY (PDEP_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_HM_CE_ORDRE FOREIGN KEY (CE_ORDRE) 
    REFERENCES JEFY_MARCHES.CODE_EXER (CE_ORDRE) DEFERRABLE INITIALLY DEFERRED);

create sequence JEFY_DEPENSE.PDEPENSE_CTRL_HORS_MARCHE_SEQ start with 1 nocycle nocache;




CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE
(
  PDMAR_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL,
  PDEP_ID NUMBER NOT NULL, 
  ATT_ORDRE NUMBER NOT NULL, 
  PDMAR_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDMAR_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDMAR_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDMAR_TTC_SAISIE NUMBER(12,2) NOT NULL
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE IS 'Table de repartition des attributions marches pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.PDMAR_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.PDEP_ID IS 'Cle etrangere table jefy_depense.pdepense_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.ATT_ORDRE IS 'Cle etrangere table jefy_marches.attribution';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.PDMAR_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.PDMAR_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.PDMAR_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE.PDMAR_TTC_SAISIE IS '';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_MARCHE ON JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE (PDMAR_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD (CONSTRAINT PK_PDEPENSE_CTRL_MARCHE PRIMARY KEY (PDMAR_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_MARCHE_1 CHECK (PDMAR_ttc_saisie=PDMAR_ht_saisie+PDMAR_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_MARCHE_2 CHECK (abs(PDMAR_montant_budgetaire)>=abs(PDMAR_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_MARCHE_3 CHECK (abs(PDMAR_montant_budgetaire)<=abs(PDMAR_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_MAR_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_MAR_PDEP_ID FOREIGN KEY (PDEP_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_MAR_ATT_ORDRE FOREIGN KEY (ATT_ORDRE) 
    REFERENCES JEFY_MARCHES.ATTRIBUTION (ATT_ORDRE) DEFERRABLE INITIALLY DEFERRED);

create sequence JEFY_DEPENSE.PDEPENSE_CTRL_MARCHE_SEQ start with 1 nocycle nocache;



CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO
(
  PDPCO_ID NUMBER NOT NULL, 
  EXE_ORDRE NUMBER NOT NULL, 
  PDEP_ID NUMBER NOT NULL, 
  PCO_NUM VARCHAR2(20) NOT NULL,
  PDPCO_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL, 
  PDPCO_HT_SAISIE NUMBER(12,2) NOT NULL, 
  PDPCO_TVA_SAISIE NUMBER(12,2) NOT NULL, 
  PDPCO_TTC_SAISIE NUMBER(12,2) NOT NULL,
  ECD_ORDRE NUMBER
)
;

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO IS 'Table de repartition des comptes d''imputation pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PDPCO_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.EXE_ORDRE IS 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PDEP_ID IS 'Cle etrangere table jefy_depense.pdepense_budget';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PCO_NUM IS 'Cle etrangere table maracuja.plan_comptable';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PDPCO_MONTANT_BUDGETAIRE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PDPCO_HT_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PDPCO_TVA_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.PDPCO_TTC_SAISIE IS '';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO.ECD_ORDRE IS 'ecriture a emarger';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_PLANCO ON JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO (PDPCO_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD (CONSTRAINT PK_PDEPENSE_CTRL_PLANCO PRIMARY KEY (PDPCO_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_PLANCO_1 CHECK (PDPCO_ttc_saisie=PDPCO_ht_saisie+PDPCO_tva_saisie));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_PLANCO_2 CHECK (abs(PDPCO_montant_budgetaire)>=abs(PDPCO_ht_saisie)));
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD (
  CONSTRAINT CH_PDEPENSE_CTRL_PLANCO_3 CHECK (abs(PDPCO_montant_budgetaire)<=abs(PDPCO_ttc_saisie)));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD ( CONSTRAINT FK_PDEPENSE_CTRL_PLA_PCO_NUM FOREIGN KEY (PCO_NUM) 
    REFERENCES MARACUJA.PLAN_COMPTABLE (PCO_NUM) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD ( CONSTRAINT FK_PDEPENSE_CTRL_PLA_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO ADD ( CONSTRAINT FK_PDEPENSE_CTRL_PLAN_PDEP_ID FOREIGN KEY (PDEP_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_BUDGET (PDEP_ID) DEFERRABLE INITIALLY DEFERRED);
	
create sequence JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_SEQ start with 1 nocycle nocache;


CREATE TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE
(
  PDPIN_ID NUMBER NOT NULL, 
  PDPCO_ID NUMBER NOT NULL, 
  INVC_ID  NUMBER NOT NULL,
  PDPIN_MONTANT_BUDGETAIRE NUMBER(12,2) NOT NULL
);
/

COMMENT ON TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE IS 'Table de repartition des inventaires comptables pour les pre liquidations';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE.PDPIN_ID IS 'Cle';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE.PDPCO_ID IS 'Cle etrangere table jefy_depense.pdepense_ctrl_planco';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE.INVC_ID IS 'Cle etrangere table jefy_inventaire.inventaire_comptable';
COMMENT ON COLUMN JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE.PDPIN_MONTANT_BUDGETAIRE IS '';


CREATE UNIQUE INDEX JEFY_DEPENSE.PK_PDEPENSE_CTRL_PLANCO_INV ON JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE (PDPIN_ID) ;
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE ADD (CONSTRAINT PK_PDEPENSE_CTRL_PLANCO_INV PRIMARY KEY (PDPIN_ID));

ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_PLINV_PDPCOID FOREIGN KEY (PDPCO_ID) 
    REFERENCES JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO (PDPCO_ID) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE ADD ( CONSTRAINT FK_PDEPENSE_CTRL_PLINV_INVCID FOREIGN KEY (INVC_ID) 
    REFERENCES JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE (INVC_ID) DEFERRABLE INITIALLY DEFERRED);
    
create sequence JEFY_DEPENSE.PDEPENSE_CTRL_PLANCO_INVE_SEQ start with 1 nocycle nocache;



--20_vue.inventaire.pour.reimputation.reversements.sql
grant select on maracuja.plan_comptable_amo to jefy_depense;

grant select, update on jefy_inventaire.cle_inventaire_comptable to jefy_depense;

grant select, update on jefy_inventaire.amortissement to jefy_depense;

grant select on jefy_inventaire.inventaire_comptable_orv to jefy_depense;

CREATE OR REPLACE FORCE VIEW jefy_depense.v_inventaire (invc_id, clic_num_complet, comm_id, org_id,
    tcd_ordre, pco_num, lid_montant, dpco_id, exe_ordre )
AS
   SELECT DISTINCT ic.invc_id, cic.clic_num_complet, l.comm_id, ld.org_id,
                   ld.tcd_ordre, ld.pco_num,
                   ic.invc_montant_acquisition /*sum(ld.lid_montant)*/,
                   ic.dpco_id, ic.exe_ordre
              FROM jefy_inventaire.livraison l,
                   jefy_inventaire.livraison_detail ld,
                   jefy_inventaire.inventaire i,
                   jefy_inventaire.inventaire_comptable ic,
                   jefy_inventaire.cle_inventaire_comptable cic
             WHERE i.invc_id = ic.invc_id
               AND ic.clic_id = cic.clic_id
               AND ld.lid_ordre = i.lid_ordre
               AND l.liv_ordre = ld.liv_ordre
union all
SELECT DISTINCT ic.invc_id, cic.clic_num_complet, l.comm_id, ld.org_id,
                   ld.tcd_ordre, ld.pco_num,
                   icorv.invo_montant_orv /*sum(ld.lid_montant)*/,
                   icorv.dpco_id, ic.exe_ordre
              FROM jefy_inventaire.livraison l,
                   jefy_inventaire.livraison_detail ld,
                   jefy_inventaire.inventaire i,
                   jefy_inventaire.inventaire_comptable ic,
                   jefy_inventaire.inventaire_comptable_orv icorv,
                   jefy_inventaire.cle_inventaire_comptable cic
             WHERE i.invc_id = ic.invc_id and ic.invc_id=icorv.invc_id
               AND ic.clic_id = cic.clic_id
               AND ld.lid_ordre = i.lid_ordre
               AND l.liv_ordre = ld.liv_ordre  ;             
/


-- 30_PRE_LIQUIDER.pks
CREATE OR REPLACE PACKAGE JEFY_DEPENSE.PRE_LIQUIDER IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_pdepense (
      a_pdep_id IN OUT		PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_exe_ordre			PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_dpp_id			    PDEPENSE_BUDGET.dpp_id%TYPE,
	  a_eng_id				PDEPENSE_BUDGET.eng_id%TYPE,
	  a_pdep_ht_saisie		PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
	  a_pdep_ttc_saisie		PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
	  a_tap_id				PDEPENSE_BUDGET.tap_id%TYPE,
	  a_utl_ordre			PDEPENSE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2);

PROCEDURE del_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE del_pdepense_budgets (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE liquider_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE verifier_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE);

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

PROCEDURE ins_pdepense_budget (
      a_pdep_id IN OUT		PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_exe_ordre			PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_dpp_id			    PDEPENSE_BUDGET.dpp_id%TYPE,
	  a_eng_id				PDEPENSE_BUDGET.eng_id%TYPE,
	  a_pdep_ht_saisie		PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
	  a_pdep_ttc_saisie		PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
	  a_tap_id				PDEPENSE_BUDGET.tap_id%TYPE,
	  a_utl_ordre			PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE ins_pdepense_ctrl_action (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_analytique (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_convention (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_hors_marche (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_marche (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_planco (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_planco_inve (
	  a_pdpco_id	        PDEPENSE_CTRL_PLANCO.pdpco_id%TYPE,
	  a_chaine		        VARCHAR2);

FUNCTION get_Chaine_Action ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_analytique ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_convention ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_hors_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
END;
/

-- 35_PRE_LIQUIDER.pkb

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.PRE_LIQUIDER
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE ins_pdepense (
      a_pdep_id IN OUT       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_exe_ordre            PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               PDEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               PDEPENSE_BUDGET.eng_id%TYPE,
      a_pdep_ht_saisie       PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
      a_pdep_ttc_saisie      PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
      a_tap_id               PDEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            PDEPENSE_BUDGET.utl_ordre%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_nb_decimales         NUMBER;
     my_pdep_ht_saisie       PDEPENSE_BUDGET.pdep_ht_saisie%TYPE;
     my_pdep_ttc_saisie      PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE;
   BEGIN
       IF a_pdep_ttc_saisie<>0 THEN
       
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_pdep_ht_saisie:=round(a_pdep_ht_saisie, my_nb_decimales);
        my_pdep_ttc_saisie:=round(a_pdep_ttc_saisie, my_nb_decimales);
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        -- Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

        -- lancement des differentes procedures d'insertion des tables de depense.
        ins_pdepense_budget(a_pdep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_pdep_ht_saisie, my_pdep_ttc_saisie,
            a_tap_id, a_utl_ordre);

        -- repartitions
        ins_pdepense_ctrl_planco(a_exe_ordre, a_pdep_id, a_chaine_planco);
        ins_pdepense_ctrl_action(a_exe_ordre, a_pdep_id, a_chaine_action);
        ins_pdepense_ctrl_analytique(a_exe_ordre, a_pdep_id, a_chaine_analytique);
        ins_pdepense_ctrl_convention(a_exe_ordre, a_pdep_id, a_chaine_convention);
        ins_pdepense_ctrl_hors_marche(a_exe_ordre, a_pdep_id, a_chaine_hors_marche);
        ins_pdepense_ctrl_marche(a_exe_ordre, a_pdep_id, a_chaine_marche);

       END IF;
   END;

   PROCEDURE del_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La pre-liquidation n''existe pas ou est deja annule (pdep_id:'||a_pdep_id||')');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

        DELETE FROM PDEPENSE_CTRL_ACTION WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_ANALYTIQUE WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_CONVENTION WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_HORS_MARCHE WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_MARCHE WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id=a_pdep_id;

        DELETE FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
   END;

   PROCEDURE del_pdepense_budgets (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
   BEGIN
        DELETE FROM PDEPENSE_CTRL_ACTION WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_ANALYTIQUE WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_CONVENTION WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_HORS_MARCHE WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_MARCHE WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);

        DELETE FROM PDEPENSE_BUDGET WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
   END;

   PROCEDURE liquider_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
      my_pdpco_id               pdepense_ctrl_planco.pdpco_id%type;
      my_dep_id                 depense_budget.dep_id%type;
      
      my_pdepense_budget        pdepense_budget%rowtype;
      my_pdepense_ctrl_planco   pdepense_ctrl_planco%rowtype;
      my_pdepense_ctrl_planco_inve  pdepense_ctrl_planco_inve%rowtype;

      my_chaine_action          VARCHAR2(30000);
      my_chaine_analytique      VARCHAR2(30000);
      my_chaine_convention      VARCHAR2(30000);
      my_chaine_hors_marche     VARCHAR2(30000);
      my_chaine_marche          VARCHAR2(30000);
      my_chaine_planco          VARCHAR2(30000);
      my_chaine_inventaire      VARCHAR2(30000);
      
      CURSOR liste IS SELECT * FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id=a_pdep_id;
      CURSOR liste_inventaire IS SELECT * FROM PDEPENSE_CTRL_PLANCO_INVE WHERE pdpco_id=my_pdpco_id;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La pre-liquidation n''existe pas ou est annule (pdep_id:'||a_pdep_id||')');
        END IF;

        SELECT * INTO my_pdepense_budget FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

        -- verification que la pre-liquidation est complete
        verifier_pdepense_budget(a_pdep_id);
        
        -- d?coupe la pr?-liquidation en autant de liquidations qu'il y a de pdepense_ctrl_planco (car 1 liquidation = 1 pco_num)
        OPEN liste();
        LOOP
           FETCH liste INTO my_pdepense_ctrl_planco;
           EXIT WHEN liste%NOTFOUND;

           -- on vide les chaines
           my_chaine_action:='';
           my_chaine_analytique:='';
           my_chaine_convention:='';
           my_chaine_hors_marche:='';
           my_chaine_marche:='';
           my_chaine_planco:='';
           my_chaine_inventaire:='';
           
           -- action
           my_chaine_action:=get_Chaine_Action(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- analytique
           my_chaine_analytique:=get_Chaine_analytique(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- convention
           my_chaine_convention:=get_Chaine_convention(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- hors_marche
           my_chaine_hors_marche:=get_Chaine_hors_marche(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- marche
           my_chaine_marche:=get_Chaine_marche(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- planco
           my_chaine_planco:=my_pdepense_ctrl_planco.pco_num||'$'||my_pdepense_ctrl_planco.pdpco_ht_saisie||'$'||
                my_pdepense_ctrl_planco.pdpco_ttc_saisie||'$'||my_pdepense_ctrl_planco.ecd_ordre||'$';
                      
           -- chaine inventaire si il y en a
           select count(*) into my_nb from pdepense_ctrl_planco_inve where pdpco_id=my_pdepense_ctrl_planco.pdpco_id;
           if my_nb>0 then
              OPEN liste_inventaire();
              LOOP
                 FETCH liste_inventaire INTO my_pdepense_ctrl_planco_inve;
                 EXIT WHEN liste_inventaire%NOTFOUND;

                 my_chaine_inventaire:=my_chaine_inventaire||my_pdepense_ctrl_planco_inve.invc_id||'|'||
                     my_pdepense_ctrl_planco_inve.pdpin_montant_budgetaire||'|';
              END LOOP;
              CLOSE liste_inventaire;
           end if;

           my_chaine_planco:=my_chaine_planco||my_chaine_inventaire||'$$';

           -- on liquide
           my_dep_id:=null;
           
           liquider.ins_depense(my_dep_id, my_pdepense_budget.exe_ordre, my_pdepense_budget.dpp_id, my_pdepense_budget.eng_id, 
               my_pdepense_ctrl_planco.pdpco_ht_saisie, my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_budget.tap_id,
               my_pdepense_budget.utl_ordre, null, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche,
               my_chaine_marche, my_chaine_planco);
        END LOOP;
        CLOSE liste;

        -- liquidation effectuee, on supprime la pre-liquidation
        del_pdepense_budget(a_pdep_id, a_utl_ordre);
   END;

   PROCEDURE verifier_pdepense_budget (
      a_pdep_id             pdepense_budget.pdep_id%type
   ) IS
      my_nb                     integer;
      my_nbbis                  integer;
      my_pdepense_budget        pdepense_budget%rowtype;
      my_depense_papier         depense_papier%rowtype;
      my_mode_paiement          maracuja.mode_paiement%rowtype;
      my_ht                     pdepense_budget.pdep_ht_saisie%type;
      my_ttc                    pdepense_budget.pdep_ttc_saisie%type;
   BEGIN
        select count(*) into my_nb from pdepense_budget where pdep_id=a_pdep_id;
        if my_nb=0 then
           raise_application_error(-20001, 'La pre-liquidation n''existe pas ou est annule (pdep_id:'||a_pdep_id||')');
        end if;

        select * into my_pdepense_budget from pdepense_budget where pdep_id=a_pdep_id;
        select * into my_depense_papier from depense_papier where dpp_id=my_pdepense_budget.dpp_id;
        
        if my_depense_papier.dpp_date_service_fait is null then
           raise_application_error(-20001, 'La depense n''a pas de date de service fait (dpp_id:'||my_pdepense_budget.dpp_id||')');
        end if;
        if my_depense_papier.mod_ordre is null then
           raise_application_error(-20001, 'La depense n''a pas de mode de paiement (dpp_id:'||my_pdepense_budget.dpp_id||')');
        end if;
        
        select * into my_mode_paiement from maracuja.mode_paiement where mod_ordre=my_depense_papier.mod_ordre;
        if my_mode_paiement.mod_dom='VIREMENT' and my_depense_papier.rib_ordre is null then 
           raise_application_error(-20001, 'Pour ce mode de paiement il faut un RIB (dpp_id:'||my_pdepense_budget.dpp_id||')');
        end if;
        

        if my_pdepense_budget.pdep_ht_saisie<0 or my_pdepense_budget.pdep_ttc_saisie<0 then
           raise_application_error(-20001, 'La pre-liquidation doit avoir des montants positifs (pdep_id:'||a_pdep_id||')');
        end if;
         
        -- action
        select count(*) into my_nb from pdepense_ctrl_action where pdep_id=a_pdep_id;
        select nvl(sum(pdact_ht_saisie),0), nvl(sum(pdact_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_action where pdep_id=a_pdep_id;
        if my_nb=0 or my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc then
           raise_application_error(-20001, 'La repartition par actions de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- analytique
        select count(*) into my_nb from pdepense_ctrl_analytique where pdep_id=a_pdep_id;
        select nvl(sum(pdana_ht_saisie),0), nvl(sum(pdana_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_analytique where pdep_id=a_pdep_id;
        if my_pdepense_budget.pdep_ht_saisie<my_ht or my_pdepense_budget.pdep_ttc_saisie<my_ttc then
           raise_application_error(-20001, 'La repartition par codes analytiques de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- convention
        select count(*) into my_nb from pdepense_ctrl_convention where pdep_id=a_pdep_id;
        select nvl(sum(pdcon_ht_saisie),0), nvl(sum(pdcon_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_convention where pdep_id=a_pdep_id;
        if my_pdepense_budget.pdep_ht_saisie<my_ht or my_pdepense_budget.pdep_ttc_saisie<my_ttc then
           raise_application_error(-20001, 'La repartition par conventions de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- hors marche
        select count(*) into my_nb from pdepense_ctrl_hors_marche where pdep_id=a_pdep_id;
        select nvl(sum(pdhom_ht_saisie),0), nvl(sum(pdhom_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_hors_marche where pdep_id=a_pdep_id;
        if my_nb>0 and (my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc) then
           raise_application_error(-20001, 'La repartition par codes de nomenclature de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;
        
        -- marche
        select count(*) into my_nbbis from pdepense_ctrl_marche where pdep_id=a_pdep_id;
        select nvl(sum(pdmar_ht_saisie),0), nvl(sum(pdmar_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_marche where pdep_id=a_pdep_id;
        if my_nbbis>0 and (my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc) then
           raise_application_error(-20001, 'La repartition par marches de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- marche ou hors marche
        if (my_nb=0 and my_nbbis=0) or (my_nb<>0 and my_nbbis<>0) then
           raise_application_error(-20001, 'La pre-liquidation doit etre ''marche'' ou ''hors marche'' (pdep_id:'||a_pdep_id||')');
        end if;
        
        -- planco
        select count(*) into my_nb from pdepense_ctrl_planco where pdep_id=a_pdep_id;
        select nvl(sum(pdpco_ht_saisie),0), nvl(sum(pdpco_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_planco where pdep_id=a_pdep_id;
        if my_nb=0 or my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc then
           raise_application_error(-20001, 'La repartition par imputations de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;
        
   END ;


--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

  
   PROCEDURE ins_pdepense_budget (
      a_pdep_id IN OUT       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_exe_ordre            PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               PDEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               PDEPENSE_BUDGET.eng_id%TYPE,
      a_pdep_ht_saisie       PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
      a_pdep_ttc_saisie      PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
      a_tap_id               PDEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
       my_pdep_ht_saisie     PDEPENSE_BUDGET.pdep_ht_saisie%TYPE;
       my_pdep_ttc_saisie    PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE;
       my_nb_decimales       NUMBER;
       
       my_nb                 INTEGER;
       my_par_value          PARAMETRE.par_value%TYPE;

       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_tcd_ordre          ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;

       my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tva_saisie    PDEPENSE_BUDGET.pdep_tva_saisie%TYPE;

       my_dpp_exe_ordre      DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         PDEPENSE_BUDGET.tap_id%TYPE;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_pdep_ht_saisie:=round(a_pdep_ht_saisie, my_nb_decimales);
        my_pdep_ttc_saisie:=round(a_pdep_ttc_saisie, my_nb_decimales);
      
        IF my_pdep_ht_saisie<0 OR my_pdep_ttc_saisie<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT exe_ordre, org_id, tap_id, tcd_ordre INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT exe_ordre INTO my_dpp_exe_ordre FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La pre-liquidation doit etre sur le meme exercice que la facture papier.');
        END IF;

        -- verification de la coherence du prorata.
        IF Get_Parametre(a_exe_ordre, 'DEPENSE_IDEM_TAP_ID')<>'NON' AND
           my_tap_id<>a_tap_id THEN
             RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la pre-liquidation soit le meme que l''engagement initial.');
        END IF;

        -- on verifie la coherence des montants.
         IF ABS(my_pdep_ht_saisie)>ABS(my_pdep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_pdep_tva_saisie:=Liquider_Outils.get_tva(my_pdep_ht_saisie, my_pdep_ttc_saisie);

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              my_pdep_ht_saisie,my_pdep_ttc_saisie);

        -- insertion dans la table.
        IF a_pdep_id IS NULL THEN
           SELECT pdepense_budget_seq.NEXTVAL INTO a_pdep_id FROM dual;
        END IF;

        -- on liquide.
        INSERT INTO PDEPENSE_BUDGET VALUES (a_pdep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           my_pdep_ht_saisie, my_pdep_tva_saisie, my_pdep_ttc_saisie, a_tap_id, a_utl_ordre);
   END;

   PROCEDURE ins_pdepense_ctrl_action (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdact_id                 PDEPENSE_CTRL_ACTION.pdact_id%TYPE;
       my_tyac_id                  PDEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_pdact_montant_budgetaire PDEPENSE_CTRL_ACTION.pdact_montant_budgetaire%TYPE;
       my_pdact_ht_saisie          PDEPENSE_CTRL_ACTION.pdact_ht_saisie%TYPE;
       my_pdact_tva_saisie         PDEPENSE_CTRL_ACTION.pdact_tva_saisie%TYPE;
       my_pdact_ttc_saisie         PDEPENSE_CTRL_ACTION.pdact_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                PDEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_ACTION.pdact_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdact_ht_saisie:=round(my_pdact_ht_saisie, my_nb_decimales);
            my_pdact_ttc_saisie:=round(my_pdact_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdact_ht_saisie)>ABS(my_pdact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdact_tva_saisie:=Liquider_Outils.get_tva(my_pdact_ht_saisie, my_pdact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_pdact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdact_ht_saisie,my_pdact_ttc_saisie);

            IF my_pdact_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR my_pdep_montant_budgetaire<=my_somme+my_pdact_montant_budgetaire THEN
                my_pdact_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_action_seq.NEXTVAL INTO my_pdact_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_ACTION VALUES (my_pdact_id,
                   a_exe_ordre, a_pdep_id, my_tyac_id, my_pdact_montant_budgetaire,
                   my_pdact_ht_saisie, my_pdact_tva_saisie, my_pdact_ttc_saisie);

            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_pdact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_analytique (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdana_id                 PDEPENSE_CTRL_ANALYTIQUE.pdana_id%TYPE;
       my_can_id                   PDEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_pdana_montant_budgetaire PDEPENSE_CTRL_ANALYTIQUE.pdana_montant_budgetaire%TYPE;
       my_pdana_ht_saisie          PDEPENSE_CTRL_ANALYTIQUE.pdana_ht_saisie%TYPE;
       my_pdana_tva_saisie         PDEPENSE_CTRL_ANALYTIQUE.pdana_tva_saisie%TYPE;
       my_pdana_ttc_saisie         PDEPENSE_CTRL_ANALYTIQUE.pdana_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_ANALYTIQUE.pdana_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdana_ht_saisie:=round(my_pdana_ht_saisie, my_nb_decimales);
            my_pdana_ttc_saisie:=round(my_pdana_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdana_ht_saisie)>ABS(my_pdana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdana_tva_saisie:=Liquider_Outils.get_tva(my_pdana_ht_saisie, my_pdana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdana_ht_saisie,my_pdana_ttc_saisie);

            IF my_pdana_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_pdep_montant_budgetaire<=my_somme+my_pdana_montant_budgetaire THEN
                my_pdana_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_analytique_seq.NEXTVAL INTO my_pdana_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_ANALYTIQUE VALUES (my_pdana_id,
                   a_exe_ordre, a_pdep_id, my_can_id, my_pdana_montant_budgetaire,
                   my_pdana_ht_saisie, my_pdana_tva_saisie, my_pdana_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_pdana_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_convention (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdcon_id                 PDEPENSE_CTRL_CONVENTION.pdcon_id%TYPE;
       my_conv_ordre               PDEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_pdcon_montant_budgetaire PDEPENSE_CTRL_CONVENTION.pdcon_montant_budgetaire%TYPE;
       my_pdcon_ht_saisie          PDEPENSE_CTRL_CONVENTION.pdcon_ht_saisie%TYPE;
       my_pdcon_tva_saisie         PDEPENSE_CTRL_CONVENTION.pdcon_tva_saisie%TYPE;
       my_pdcon_ttc_saisie         PDEPENSE_CTRL_CONVENTION.pdcon_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_CONVENTION.pdcon_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdcon_ht_saisie:=round(my_pdcon_ht_saisie, my_nb_decimales);
            my_pdcon_ttc_saisie:=round(my_pdcon_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdcon_ht_saisie)>ABS(my_pdcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdcon_tva_saisie:=Liquider_Outils.get_tva(my_pdcon_ht_saisie, my_pdcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdcon_ht_saisie,my_pdcon_ttc_saisie);

            IF my_pdcon_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_pdep_montant_budgetaire<=my_somme+my_pdcon_montant_budgetaire THEN
                my_pdcon_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_convention_seq.NEXTVAL INTO my_pdcon_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_CONVENTION VALUES (my_pdcon_id,
                   a_exe_ordre, a_pdep_id, my_conv_ordre, my_pdcon_montant_budgetaire,
                   my_pdcon_ht_saisie, my_pdcon_tva_saisie, my_pdcon_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_pdcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_hors_marche (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdhom_id                 PDEPENSE_CTRL_HORS_MARCHE.pdhom_id%TYPE;
       my_typa_id                  PDEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                 PDEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_pdhom_montant_budgetaire PDEPENSE_CTRL_HORS_MARCHE.pdhom_montant_budgetaire%TYPE;
       my_pdhom_ht_saisie          PDEPENSE_CTRL_HORS_MARCHE.pdhom_ht_saisie%TYPE;
       my_pdhom_tva_saisie         PDEPENSE_CTRL_HORS_MARCHE.pdhom_tva_saisie%TYPE;
       my_pdhom_ttc_saisie         PDEPENSE_CTRL_HORS_MARCHE.pdhom_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_HORS_MARCHE.pdhom_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_?chat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdhom_ht_saisie:=round(my_pdhom_ht_saisie, my_nb_decimales);
            my_pdhom_ttc_saisie:=round(my_pdhom_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdhom_ht_saisie)>ABS(my_pdhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdhom_tva_saisie:=Liquider_Outils.get_tva(my_pdhom_ht_saisie, my_pdhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdhom_ht_saisie,my_pdhom_ttc_saisie);

            IF my_pdhom_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_pdep_montant_budgetaire<=my_somme+my_pdhom_montant_budgetaire THEN
                my_pdhom_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_hors_marche_seq.NEXTVAL INTO my_pdhom_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_HORS_MARCHE VALUES (my_pdhom_id,
                   a_exe_ordre, a_pdep_id, my_typa_id, my_ce_ordre, my_pdhom_montant_budgetaire,
                   my_pdhom_ht_saisie, my_pdhom_tva_saisie, my_pdhom_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_pdhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_marche (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdmar_id                 PDEPENSE_CTRL_MARCHE.pdmar_id%TYPE;
       my_att_ordre                PDEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_pdmar_montant_budgetaire PDEPENSE_CTRL_MARCHE.pdmar_montant_budgetaire%TYPE;
       my_pdmar_ht_saisie          PDEPENSE_CTRL_MARCHE.pdmar_ht_saisie%TYPE;
       my_pdmar_tva_saisie         PDEPENSE_CTRL_MARCHE.pdmar_tva_saisie%TYPE;
       my_pdmar_ttc_saisie         PDEPENSE_CTRL_MARCHE.pdmar_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                PDEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_MARCHE.pdmar_montant_budgetaire%TYPE;
       my_dpp_id                   PDEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                DEPENSE_PAPIER.fou_ordre%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdmar_ht_saisie:=round(my_pdmar_ht_saisie, my_nb_decimales);
            my_pdmar_ttc_saisie:=round(my_pdmar_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdmar_ht_saisie)>ABS(my_pdmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdmar_tva_saisie:=Liquider_Outils.get_tva(my_pdmar_ht_saisie, my_pdmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdmar_ht_saisie,my_pdmar_ttc_saisie);

            IF my_pdmar_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_pdep_montant_budgetaire<=my_somme+my_pdmar_montant_budgetaire THEN
                my_pdmar_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_marche_seq.NEXTVAL INTO my_pdmar_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_MARCHE VALUES (my_pdmar_id,
                   a_exe_ordre, a_pdep_id, my_att_ordre, my_pdmar_montant_budgetaire,
                   my_pdmar_ht_saisie, my_pdmar_tva_saisie, my_pdmar_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_pdmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_planco (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdpco_id                 PDEPENSE_CTRL_PLANCO.pdpco_id%TYPE;
       my_pco_num                  PDEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_pdpco_montant_budgetaire PDEPENSE_CTRL_PLANCO.pdpco_montant_budgetaire%TYPE;
       my_pdpco_ht_saisie          PDEPENSE_CTRL_PLANCO.pdpco_ht_saisie%TYPE;
       my_pdpco_tva_saisie         PDEPENSE_CTRL_PLANCO.pdpco_tva_saisie%TYPE;
       my_pdpco_ttc_saisie         PDEPENSE_CTRL_PLANCO.pdpco_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                PDEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_PLANCO.pdpco_montant_budgetaire%TYPE;
       my_ecd_ordre                PDEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_mod_ordre                DEPENSE_PAPIER.mod_ordre%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_inventaires              VARCHAR2(30000);
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre, my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdpco_ht_saisie:=round(my_pdpco_ht_saisie, my_nb_decimales);
            my_pdpco_ttc_saisie:=round(my_pdpco_ttc_saisie, my_nb_decimales);

            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            IF my_ecd_ordre IS NOT NULL THEN
               SELECT mod_ordre INTO my_mod_ordre
                  FROM PDEPENSE_BUDGET d, DEPENSE_PAPIER p WHERE p.dpp_id=d.dpp_id AND d.pdep_id=a_pdep_id;
               Verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
            END IF;

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdpco_ht_saisie)>ABS(my_pdpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdpco_tva_saisie:=Liquider_Outils.get_tva(my_pdpco_ht_saisie, my_pdpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdpco_ht_saisie,my_pdpco_ttc_saisie);

            IF my_pdpco_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_pdep_montant_budgetaire<=my_somme+my_pdpco_montant_budgetaire THEN
                my_pdpco_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_planco_seq.NEXTVAL INTO my_pdpco_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_PLANCO VALUES (my_pdpco_id,
                   a_exe_ordre, a_pdep_id, my_pco_num, my_pdpco_montant_budgetaire,
                   my_pdpco_ht_saisie, my_pdpco_tva_saisie, my_pdpco_ttc_saisie, my_ecd_ordre);

            -- procedure de verification
            Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
            ins_pdepense_ctrl_planco_inve(my_pdpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_pdpco_montant_budgetaire;
        END LOOP;
   END;
   
PROCEDURE ins_pdepense_ctrl_planco_inve (
      a_pdpco_id            PDEPENSE_CTRL_PLANCO.pdpco_id%TYPE,
      a_chaine                VARCHAR2
   ) IS
          my_invc_id                  integer; --jefy_inventaire.inventaire_comptable.invc%type;
       my_pdpin_id                 pdepense_ctrl_planco_inve.pdpin_id%type;
       my_montant                  pdepense_ctrl_planco_inve.pdpin_montant_budgetaire%type;
       my_somme                    pdepense_ctrl_planco.pdpco_montant_budgetaire%type;
       my_pdpco_montant_budgetaire pdepense_ctrl_planco.pdpco_montant_budgetaire%type;
       my_chaine                   VARCHAR2(30000);
       my_exe_ordre                   pdepense_ctrl_planco.exe_ordre%type;
       my_pco_num                   pdepense_ctrl_planco.pco_num%type;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
   BEGIN
       -- inventaire
       my_chaine:=a_chaine;
       my_somme:=0;
          LOOP
              IF a_chaine IS  NULL OR LENGTH(a_chaine)=0 OR SUBSTR(my_chaine,1,1)='|' THEN EXIT; END IF;

            -- on recupere la cle de l'inventaire.
            SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'|')-1)) INTO my_invc_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'|')+1,LENGTH(my_chaine));
            -- on recupere le montant budgetaire.
            SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'|')-1)) INTO my_montant FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'|')+1,LENGTH(my_chaine));

            my_somme:=my_somme+my_montant;

            select pdepense_ctrl_planco_inve_seq.nextval into my_pdpin_id from dual;
            insert into pdepense_ctrl_planco_inve values (my_pdpin_id, a_pdpco_id, my_invc_id, my_montant);
       END LOOP;

       SELECT dp.pdpco_montant_budgetaire, dp.exe_ordre, dp.pco_num INTO my_pdpco_montant_budgetaire, my_exe_ordre, my_pco_num
           FROM PDEPENSE_CTRL_PLANCO dp WHERE dp.pdpco_id=a_pdpco_id;
           
       SELECT par_value INTO my_par_value FROM PARAMETRE WHERE par_key='INVENTAIRE_OBLIGATOIRE' AND exe_ordre=my_exe_ordre;

       IF my_par_value='OUI' THEN
             SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_inventoriable WHERE pco_num=my_pco_num AND exe_ordre=my_exe_ordre;
          IF my_nb>0 THEN
                IF my_somme<>my_pdpco_montant_budgetaire THEN
                     RAISE_APPLICATION_ERROR(-20001,'le montant budgetaire des inventaires n''est pas egal au montant budgetaire de la pre-liquidation ');
                END IF;
          END IF;

       END IF;
   END;
   
FUNCTION get_Chaine_Action ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_action jefy_depense.pdepense_CTRL_ACTION%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_ACTION WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_ACTION.pdact_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_ACTION.pdact_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_ACTION.pdact_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_ACTION.pdact_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_ACTION WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_action;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_action.pdact_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_action.pdact_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;

           my_chaine:=my_chaine||my_pdepense_ctr_action.tyac_id||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_analytique ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_analytique jefy_depense.pdepense_CTRL_analytique%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_analytique WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_analytique.pdana_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_analytique.pdana_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_analytique.pdana_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_analytique.pdana_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_analytique WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_analytique;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

--              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_analytique.pdana_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_analytique.pdana_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

/*              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;
*/
           my_chaine:=my_chaine||my_pdepense_ctr_analytique.can_id||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_convention ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_convention jefy_depense.pdepense_CTRL_convention%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_convention WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_convention.pdcon_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_convention.pdcon_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_convention.pdcon_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_convention.pdcon_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_convention WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_convention;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

--              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_convention.pdcon_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_convention.pdcon_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

/*              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;
*/
           my_chaine:=my_chaine||my_pdepense_ctr_convention.conv_ordre||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_hors_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_hors_marche jefy_depense.pdepense_CTRL_hors_marche%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_hors_marche WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_hors_marche.pdhom_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_hors_marche.pdhom_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_hors_marche.pdhom_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_hors_marche.pdhom_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_hors_marche WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_hors_marche;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_hors_marche.pdhom_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_hors_marche.pdhom_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;

           my_chaine:=my_chaine||my_pdepense_ctr_hors_marche.typa_id||'$'||my_pdepense_ctr_hors_marche.ce_ordre||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_marche jefy_depense.pdepense_CTRL_marche%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_marche WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_marche.pdmar_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_marche.pdmar_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_marche.pdmar_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_marche.pdmar_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_marche WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_marche;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_marche.pdmar_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_marche.pdmar_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;

           my_chaine:=my_chaine||my_pdepense_ctr_marche.att_ordre||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
END;
/


--40_LIQUIDER.pks
CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Liquider  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE);

PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type);

PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type );

PROCEDURE ins_depense (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2);

PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT          COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE);

PROCEDURE del_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE del_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);


--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


PROCEDURE log_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE log_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);

PROCEDURE ins_depense_budget (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE);

PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

END;
/




CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Liquider
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_depense_papier  depense_papier%rowtype;
      my_pers_id         jefy_admin.utilisateur.pers_id%type;
    BEGIN
      select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 
      
      select * into my_depense_papier from depense_papier where dpp_id=a_dpp_id;
      if my_depense_papier.dpp_date_service_fait is not null then 
         RAISE_APPLICATION_ERROR(-20001, 'La facture a deja une date de service fait ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if;
      
      select pers_id into my_pers_id from jefy_admin.utilisateur where utl_ordre=a_utl_ordre;
      update depense_papier set dpp_date_service_fait=a_dpp_date_service_fait, dpp_sf_pers_id=my_pers_id, dpp_sf_date=sysdate
          where dpp_id=a_dpp_id;      
    END;

   PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_pdep_id         pdepense_budget.pdep_id%type;
      CURSOR liste IS SELECT pdep_id FROM pDEPENSE_budget WHERE dpp_id=a_dpp_id;
    BEGIN
      service_fait(a_dpp_id, a_utl_ordre, a_dpp_date_service_fait);
      
      select count(*) into my_nb from pdepense_budget where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''a pas de pre-liquidation ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 

      OPEN liste();
      LOOP
         FETCH liste INTO my_pdep_id;
         EXIT WHEN liste%NOTFOUND;

         pre_liquider.liquider_pdepense_budget(my_pdep_id, a_utl_ordre);
      END LOOP;
      CLOSE liste;
    END;

   PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
   BEGIN
        ins_depense_papier_avec_IM(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
            a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
            a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, null, null, null);
   end;
   
PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type
   ) IS
   BEGIN
      ins_depense_papier_avec_im_sf(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
          a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception, a_dpp_date_service_fait, 
          a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, a_dpp_im_taux, a_dpp_im_dgp, imtt_id, null, null,null);
   END;
   
   PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type 
    ) IS
      my_dpp_ht_initial       DEPENSE_PAPIER.dpp_ht_initial%TYPE;
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpp_ttc_initial      DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
      my_dpp_sf_pers_id       DEPENSE_PAPIER.dpp_sf_pers_id%type;
      my_dpp_sf_date          DEPENSE_PAPIER.dpp_sf_date%type;
      my_nb_decimales         NUMBER;
      my_nb                   integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);
        Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mod_ordre, a_exe_ordre);

        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dpp_ht_initial:=round(a_dpp_ht_initial, my_nb_decimales);
        my_dpp_ttc_initial:=round(a_dpp_ttc_initial, my_nb_decimales);

        -- si les montants sont negatifs ou si il y a un dpp_id_reversement (c'est un ORV) -> package reverser.
        IF my_dpp_ht_initial<0 OR my_dpp_ttc_initial<0 OR a_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(my_dpp_ht_initial)>ABS(my_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(my_dpp_ht_initial, my_dpp_ttc_initial);

           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        my_dpp_sf_pers_id:=a_dpp_sf_pers_id;
        my_dpp_sf_date:=a_dpp_sf_date;
        if a_dpp_date_service_fait is not null then
           if a_dpp_sf_pers_id is null then
              select pers_id into my_dpp_sf_pers_id from v_utilisateur where utl_ordre=a_utl_ordre;
           end if;
           
           if a_dpp_sf_date is null then
              select sysdate into my_dpp_sf_date from dual;
           end if;  
        end if;
        
        select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
        
        if my_nb=0 then
           INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
              0, 0, a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
              a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
              my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial, a_dpp_im_taux, a_dpp_im_dgp, a_imtt_id,
              my_dpp_sf_pers_id, my_dpp_sf_date,a_ecd_ordre);
        else
           update DEPENSE_PAPIER set dpp_numero_facture=a_dpp_numero_facture, rib_ordre=a_rib_ordre, mod_ordre=a_mod_ordre, dpp_date_facture=a_dpp_date_facture, 
              dpp_date_reception=a_dpp_date_reception, dpp_date_service_fait=a_dpp_date_service_fait, dpp_nb_piece=a_dpp_nb_piece, utl_ordre=a_utl_ordre, 
              dpp_id_reversement=a_dpp_id_reversement, dpp_ht_initial=my_dpp_ht_initial, dpp_tva_initial=my_dpp_tva_initial, dpp_ttc_initial=my_dpp_ttc_initial, 
              dpp_im_taux=a_dpp_im_taux, dpp_im_dgp=a_dpp_im_dgp, imtt_id=a_imtt_id, dpp_sf_pers_id=my_dpp_sf_pers_id, dpp_sf_date=my_dpp_sf_date, dpp_date_saisie=sysdate, ecd_ordre=a_ecd_ordre
             where dpp_id=a_dpp_id;
        end if;
   END;

   PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT         COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE
   ) IS
     my_nb                    INTEGER;
     my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
     my_cde_exe_ordre         COMMANDE.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture est deja utilise pour une commande ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT exe_ordre INTO my_cde_exe_ordre FROM COMMANDE WHERE comm_id=a_comm_id;

        IF my_dpp_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.dep_papier(a_dpp_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        -- si pas de probleme on insere.
        IF a_cdp_id IS NULL THEN
           SELECT commande_dep_papier_seq.NEXTVAL INTO a_cdp_id FROM dual;
        END IF;

        INSERT INTO COMMANDE_DEP_PAPIER VALUES (a_cdp_id, a_comm_id, a_dpp_id);
   END;

   PROCEDURE ins_depense (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id                depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_org_id               engage_budget.org_id%type;
     my_nb_decimales         NUMBER;
     my_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE;
     my_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
   BEGIN
       IF a_dep_ttc_saisie<>0 THEN
       
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);

        select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

        -- lancement des differentes procedures d'insertion des tables de depense.
        ins_depense_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie,
            a_tap_id, a_utl_ordre, a_dep_id_reversement);

        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie+my_dep_ttc_saisie-my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie+my_dep_ttc_saisie
           WHERE dpp_id=a_dpp_id;

           -- on le passe en premier car utilis? par les autres pour les upd_engage_reste_.
        ins_depense_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

        ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
        ins_depense_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
        ins_depense_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
        ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
        ins_depense_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);

        --Corriger.upd_engage_reste(a_eng_id);

        -- on verifie la coherence des montants entre les differents depense_.
        Verifier.verifier_depense_coherence(a_dep_id);
        Verifier.verifier_depense_pap_coherence(a_dpp_id);
        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
      my_eng_id                 ENGAGE_BUDGET.eng_id%TYPE;
      my_zdep_id                Z_DEPENSE_BUDGET.zdep_id%TYPE;
      my_exe_ordre              DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire     DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dep_total_ht           DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_total_ttc          DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dep_total_bud          DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_eng_montant_bud        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_montant_bud_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_dispo_ligne            v_budget_exec_credit.bdxc_disponible%TYPE;
      my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_dep_ht_saisie          DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_eng_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
      my_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE;
      my_comm_id                COMMANDE.comm_id%TYPE;
     my_dpco_id                 DEPENSE_CTRL_PLANCO.dpco_id%TYPE;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La liquidation n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

           SELECT e.org_id, e.tcd_ordre, d.exe_ordre, d.dep_ht_saisie, d.dep_ttc_saisie,
               d.dep_montant_budgetaire, e.eng_id, e.tap_id, e.eng_montant_budgetaire, e.eng_montant_budgetaire_reste, d.dpp_id
          INTO my_org_id, my_tcd_ordre, my_exe_ordre, my_dep_ht_saisie, my_dep_ttc_saisie,
               my_montant_budgetaire, my_eng_id, my_eng_tap_id, my_eng_montant_bud, my_eng_montant_bud_reste, my_dpp_id
        FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e
        WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si ce n'est pas un ORV.
        IF my_montant_budgetaire<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- on recupere les montants des autres factures et ORV.
        SELECT NVL(SUM(Budget.calculer_budgetaire(my_exe_ordre,my_eng_tap_id,my_org_id,dep_ht_saisie, dep_ttc_saisie)),0) 
          INTO my_dep_total_bud
          FROM DEPENSE_BUDGET WHERE dep_id<>a_dep_id AND dep_id IN
             (SELECT dep_id FROM DEPENSE_CTRL_PLANCO
               WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE eng_id=my_eng_id)
               AND (dpco_montant_budgetaire>0 /*OR man_id IN
               (SELECT man_id FROM v_mandat WHERE man_etat IN ('VISE','PAYE'))*/));

        -- on calcule le reste de l'engagement
        my_reste:=my_eng_montant_bud-my_dep_total_bud;
        IF my_reste<0 THEN
           my_reste:=0;
        END IF;

        -- on compare au dispo de la ligne budgetaire ... pour voir si on peut tt reengager.
        my_dispo_ligne:=my_montant_budgetaire+
            Budget.engager_disponible(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>my_dispo_ligne+my_eng_montant_bud_reste  THEN
          my_reste:=my_dispo_ligne+my_eng_montant_bud_reste;
        END IF;

        -- on modifie l'engagement.
        UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=my_reste WHERE eng_id=my_eng_id;

        -- tout est bon ... on supprime la depense.
        log_depense_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        DELETE FROM reimputation_ACTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_ANALYTIQUE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_BUDGET WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_CONVENTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_HORS_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_PLANCO WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation WHERE dep_id=a_dep_id;

        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        Corriger.upd_engage_reste(my_eng_id);

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(my_eng_id);
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

       Apres_Liquide.del_depense_budget(my_eng_id);

   END;


   PROCEDURE del_depense_papier (
      a_dpp_id             DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre          Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE) IS
      my_nb                INTEGER;
      my_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
      my_dpp_ttc_initial   DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_depense_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;



--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------



   PROCEDURE log_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id            Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
       INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_depense_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id                 depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_dep_ht_saisie      DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_nb_decimales       NUMBER;
       
       my_nb                 INTEGER;
       my_par_value          PARAMETRE.par_value%TYPE;

       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_tcd_ordre          ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_eng_budgetaire     ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_eng_reste          ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
       my_eng_init           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_dep_eng_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

       my_dpp_ht_initial     DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial    DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial    DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       --my_sum_ht_saisie         depense_budget.dep_ht_saisie%type;
       --my_sum_tva_saisie     depense_budget.dep_tva_saisie%type;
       --my_sum_ttc_saisie     depense_budget.dep_ttc_saisie%type;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);
      
        IF my_dep_ht_saisie<0 OR my_dep_ttc_saisie<0 OR a_dep_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT exe_ordre, org_id, tap_id, tcd_ordre, eng_montant_budgetaire_reste, eng_montant_budgetaire
          INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre, my_eng_reste, my_eng_init
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

          SELECT exe_ordre, dpp_ht_initial, dpp_tva_initial, dpp_ttc_initial
          INTO my_dpp_exe_ordre, my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        -- verification de la coherence du prorata.
        IF Get_Parametre(a_exe_ordre, 'DEPENSE_IDEM_TAP_ID')<>'NON' AND
           my_tap_id<>a_tap_id THEN
             RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la depense soit le meme que l''engagement initial.');
        END IF;

        -- on verifie la coherence des montants.
         IF ABS(my_dep_ht_saisie)>ABS(my_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(my_dep_ht_saisie, my_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declar? dans la papier.
        /*select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_tva_saisie),0) , nvl(sum(dep_ttc_saisie),0)
               into my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          from depense_budget where dpp_id=a_dpp_id;

        if abs(my_sum_ht_saisie+a_dep_ht_saisie) > abs(my_dpp_ht_initial) or
           abs(my_sum_tva_saisie+my_dep_tva_saisie) > abs(my_dpp_tva_initial) or
           abs(my_sum_ttc_saisie+a_dep_ttc_saisie) > abs(my_dpp_ttc_initial) then
           raise_application_error(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        end if;
        */
        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              my_dep_ht_saisie,my_dep_ttc_saisie);

        -- on calcule pour diminuer le reste de l'engagement du montant liquid? ou celui par rapport au tap_id.
        --    de l'engage (si le tap_id peut etre different de celui de l'engage).
        my_eng_budgetaire:=Liquider_Outils.get_eng_montant_budgetaire(a_exe_ordre, my_tap_id, a_tap_id,
          my_montant_budgetaire, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

        -- si le reste engage est 0, et qu'on passe une liquidation positive -> erreur.
        IF my_eng_reste<=0 AND my_eng_budgetaire>0 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement est deja solde (eng_id='||a_eng_id||')');
        END IF;

        -- on verifie qu'on ne diminue pas plus que ce qu'il y a d'engage.
        IF my_eng_reste<my_eng_budgetaire THEN
           my_eng_budgetaire:=my_eng_reste;
        END IF;

        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

          -- on diminue le reste engage de l'engagement.
        UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=eng_montant_budgetaire_reste-my_eng_budgetaire
           WHERE eng_id=a_eng_id;

        -- on liquide.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre,
           a_dep_id_reversement);

        Budget.maj_budget(a_exe_ordre, my_org_id, my_tcd_ordre);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dact_id                   DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                        DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_dact_ht_saisie             DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie           DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie           DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                   DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dact_ht_saisie:=round(my_dact_ht_saisie, my_nb_decimales);
            my_dact_ttc_saisie:=round(my_dact_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            IF my_dact_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dact_montant_budgetaire THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id,
                   a_exe_ordre, a_dep_id, my_tyac_id, my_dact_montant_budgetaire,
                   my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_action(my_eng_id);

            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dana_id                   DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                        DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_dana_ht_saisie             DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dana_ht_saisie:=round(my_dana_ht_saisie, my_nb_decimales);
            my_dana_ttc_saisie:=round(my_dana_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);

            IF my_dana_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_dep_montant_budgetaire<=my_somme+my_dana_montant_budgetaire THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_analytique(my_eng_id);

            Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                   DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre                    DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_dcon_ht_saisie             DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie           DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie           DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dcon_ht_saisie:=round(my_dcon_ht_saisie, my_nb_decimales);
            my_dcon_ttc_saisie:=round(my_dcon_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);

            IF my_dcon_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_dep_montant_budgetaire<=my_somme+my_dcon_montant_budgetaire THEN
                my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_convention(my_eng_id);

            Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dhom_id                   DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                        DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_dhom_ht_saisie             DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_?chat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dhom_ht_saisie:=round(my_dhom_ht_saisie, my_nb_decimales);
            my_dhom_ttc_saisie:=round(my_dhom_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            IF my_dhom_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dhom_montant_budgetaire THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_hors_marche(my_eng_id);

            Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dmar_ht_saisie:=round(my_dmar_ht_saisie, my_nb_decimales);
            my_dmar_ttc_saisie:=round(my_dmar_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);

            IF my_dmar_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dmar_montant_budgetaire THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

                         dbms_output.put_line('insert depense : '||my_dmar_id);

               -- procedure de verification
                         dbms_output.put_line('upd engage : '||my_eng_id);
            Corriger.upd_engage_reste_marche(my_eng_id);

            Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_mod_ordre                   DEPENSE_PAPIER.mod_ordre%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_inventaires    VARCHAR2(30000);
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            --SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            select ecd_ordre into my_ecd_ordre from depense_papier where dpp_id in (select dpp_id from depense_budget where dep_id=a_dep_id);
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dpco_ht_saisie:=round(my_dpco_ht_saisie, my_nb_decimales);
            my_dpco_ttc_saisie:=round(my_dpco_ttc_saisie, my_nb_decimales);

            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            IF my_ecd_ordre IS NOT NULL THEN
               SELECT mod_ordre INTO my_mod_ordre
                  FROM DEPENSE_BUDGET d, DEPENSE_PAPIER p WHERE p.dpp_id=d.dpp_id AND d.dep_id=a_dep_id;
               Verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
            END IF;

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            IF my_dpco_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dpco_montant_budgetaire THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, my_ecd_ordre);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Corriger.upd_engage_reste_planco(my_eng_id);

            Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dpco_montant_budgetaire;
        END LOOP;
   END;
END;
/


GRANT EXECUTE ON  JEFY_DEPENSE.LIQUIDER TO JEFY_MISSION;

GRANT EXECUTE ON  JEFY_DEPENSE.LIQUIDER TO JEFY_PAF;

GRANT EXECUTE ON  JEFY_DEPENSE.LIQUIDER TO JEFY_PAYE;

GRANT EXECUTE ON  JEFY_DEPENSE.LIQUIDER TO JEFY_RECETTE;


--50_REIMPUTER.sql
CREATE OR REPLACE PACKAGE JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2);

PROCEDURE depense_avec_infos (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type,
      a_eng_id_new            engage_budget.eng_id%type);

PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_eng_id_new            engage_budget.eng_id%type);

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type);
      
FUNCTION subdivise_engage_pour_depense(
      a_dep_id depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type)
return number;

FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type)
return number;

PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type);

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type)
return number;
      
PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type);

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type);
      
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type);

PROCEDURE rempli_reimputation(
      a_dep_id    depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre depense_budget.utl_ordre%type);

PROCEDURE reimputation_maracuja (
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type);

PROCEDURE reimputation_inventaire (
      a_dep_id                depense_ctrl_planco.dep_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_tap_id                depense_budget.tap_id%type);
      
END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2
   ) IS 
   BEGIN
      depense_avec_infos(a_dep_id, a_reim_libelle, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_pco_num, a_chaine_action,
         a_chaine_analytique, a_chaine_convention, a_chaine_hors_marche, a_chaine_marche, 3 /*OUI*/, null);
   END;
   
PROCEDURE depense_avec_infos (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type, /* 3:OUI, 4:NON */
      a_eng_id_new            engage_budget.eng_id%type
   ) IS
      my_depense       depense_budget%rowtype;
      my_engage        engage_budget%rowtype;
      my_nb            integer;
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id;
     select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
     
     -- si la depense a des reversements ou est un reversement, cas non pris en charge pour le moment
     select count(*) into my_nb from depense_budget where dep_id_reversement=a_dep_id;
     if my_nb>0 or my_depense.dep_id_reversement is not null then
        raise_application_error(-20001, 'la depense a des reversements, ce cas n''est pas encore traite'); 
     end if;
     
     if a_tyet_id_reimp_ordo<>4 /*4:NON*/ then
        rempli_reimputation(a_dep_id, a_reim_libelle, a_utl_ordre);
     end if;
   
     if a_org_id is not null and my_engage.org_id<>a_org_id then
       depense_organ(a_dep_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_chaine_analytique, a_chaine_convention, a_eng_id_new);
     else
       if a_chaine_analytique is not null then 
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
       end if;
       if a_chaine_convention is not null then 
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
       end if;
       if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
       end if;
       if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
       end if;
     end if;
     
     if a_chaine_action is not null then 
       depense_action(a_dep_id, a_chaine_action, a_utl_ordre);
     end if;

     if a_chaine_hors_marche is not null then 
       depense_hors_marche(a_dep_id, a_chaine_hors_marche, a_utl_ordre);
     end if;

     if a_chaine_marche is not null then 
       depense_marche(a_dep_id, a_chaine_marche, a_utl_ordre);
     end if;

     if a_pco_num is not null then 
       depense_planco(a_dep_id, a_pco_num, a_utl_ordre, a_tyet_id_reimp_ordo);
     end if;
   END;
   
PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_eng_id_new            engage_budget.eng_id%type
   ) IS
      my_nb        integer;
      my_eng_id    engage_budget.eng_id%type;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_tap_id    engage_budget.tap_id%type;
   BEGIN
--- changement de la ligne budgetaire
--    * depense sans inventaire : si la depense est mandatee, on choisit une ligne budgetaire de la meme UB, sinon toutes les lignes, dans la limite que le disponible le permette.
--    * depense avec inventaire : suivant un parametre a mettre en place, meme limitation que la depense sans inventaire ou restriction au niveau du CR (puisque le code inventaire prend en compte le CR)
--    * le changement de ligne peut impliquer la modification des conventions et des codes analytiques eventuellement associes a cette depense    cf. changement convention
--    * le changement de ligne peut impliquer une creation d'un nouvel engagement, si l'engagement concerne d'autres liquidations (hors ORVs lies a celle qu'on re-impute) par exemple
--    * si la depense re-imputee possede un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..   

      select * into my_depense from depense_budget where dep_id=a_dep_id; 
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;

      -- si la ligne budgetaire a change
      if my_engage.org_id<>a_org_id then
        my_tcd_ordre:=a_tcd_ordre;
        if my_tcd_ordre is null then my_tcd_ordre:=my_engage.tcd_ordre; end if;

        my_tap_id:=a_tap_id;
        if my_tap_id is null then my_tap_id:=my_depense.tap_id; end if;
        
        verifier.verifier_budget(my_depense.exe_ordre, my_tap_id, a_org_id, my_tcd_ordre);
        verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, a_org_id);

        -- si avec inventaire -> CR
        /* select count(*) into my_nb from v_inventaire where dpco_id in (select dpco_id from depense_ctrl_planco where dep_id in (a_dep_id));
        if my_nb>0 then
          select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
            and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub and dest.org_cr=orig.org_cr;
          if my_nb=0 then
            raise_application_error(-20001, 'cette depense comporte des inventaires, on ne peut reimputer que dans le meme CR');
          end if;
        else */
          -- la depense est mandatee -> restriction de la nouvelle ligne a la meme UB que l'ancienne
          select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
          if my_nb>0 then
            select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
              and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub;
            if my_nb=0 then
              raise_application_error(-20001, 'cette depense est mandatee, on ne peut reimputer que dans la meme UB');
            end if;
          end if;
        --end if;
        
        -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
        my_eng_id:=subdivise_engage_pour_depense(a_dep_id, a_org_id, my_tcd_ordre, my_tap_id, a_utl_ordre);
        
      end if;
      
      if a_chaine_analytique is not null then
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
      end if;
      
      if a_chaine_convention is not null then
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
      end if;

      if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
      end if;

      if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
      end if;
   END;

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
   BEGIN
     RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer le type de credit de la depense');
   END;

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_eng_id    engage_budget.eng_id%type;
      my_par_value parametre.par_value%type;
      
      my_dep_id    depense_budget.dep_id%type;
      cursor orvs is select dep_id from depense_budget where dep_id_reversement=a_dep_id;
   BEGIN
      select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
      if my_nb>0 then
        select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and dpco_tva_saisie<>0 and man_id is not null;
        if my_nb>0 then
           RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier le taux de prorata d''une depense mandatee');
        end if;
      end if;
      
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      my_eng_id:=my_engage.eng_id;

      verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, my_engage.org_id);

      -- on ne peut reimputer que les depenses pas les ORvs pour le moment
        -- les orvs rattaches a cette depense seront mis a jour avec les memes infos
      if my_depense.dep_ttc_saisie<0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas reimputer un ORV, il faut reimputer la depense d''origine');
      end if;
            
      -- on verifie si le nouveau taux de prorata est autorise pour cette ligne budgetaire --
      verifier.verifier_budget(my_depense.exe_ordre, a_tap_id, my_engage.org_id, my_engage.tcd_ordre);
      
      select par_value into my_par_value from parametre where par_key='DEPENSE_IDEM_TAP_ID' and exe_ordre=my_depense.exe_ordre;
      if my_par_value='OUI' then
      
         -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
         my_eng_id:=subdivise_engage_pour_depense(a_dep_id, my_engage.org_id, my_engage.tcd_ordre, a_tap_id, a_utl_ordre);
         
      end if;
      
      update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=a_dep_id;
      corrige_depense_budgetaire(a_dep_id);
      
      open orvs();
      loop
         fetch  orvs into my_dep_id;
         exit when orvs%notfound;
         
         update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=my_dep_id;
         corrige_depense_budgetaire(my_dep_id);
      end loop;
      close orvs;
      
      budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
      
   END;

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_action where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_action(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_analytique where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_analytique(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_convention where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_convention(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est rattachee a une attribution on ne peut pas la transformer en hors marche');
       end if;

       select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
       delete from depense_ctrl_hors_marche where dep_id=a_dep_id;
       liquider.ins_depense_ctrl_hors_marche(my_exe_ordre, a_dep_id, a_chaine);
       Verifier.verifier_depense_coherence(a_dep_id);
     end if;     
   END;

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_att_ordre depense_ctrl_marche.att_ordre%type;
      my_old_att_ordre depense_ctrl_marche.att_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est hors marche on ne peut pas la transformer en marche');
       end if;

       SELECT grhum.en_nombre(SUBSTR(a_chaine,1,INSTR(a_chaine,'$')-1)) INTO my_att_ordre FROM dual;
       select nvl(att_ordre,0) into my_old_att_ordre from depense_ctrl_marche where dep_id=a_dep_id;
       
       if my_old_att_ordre<>my_att_ordre then 
          RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer l''attribution de la depense');
       end if;
     end if;
   END;

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type
  ) is
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_org_id    engage_budget.org_id%type;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_pco_num   depense_ctrl_planco.pco_num%type;
      my_dep_id_reversement depense_budget.dep_id_reversement%type;
  begin 
      select nvl(pco_num,'0') into my_pco_num from depense_ctrl_planco where dep_id=a_dep_id;
      if my_pco_num<>a_pco_num then
         select dep_id_reversement into my_dep_id_reversement from depense_budget where dep_id=a_dep_id;
         if my_dep_id_reversement is not null then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''un ordre de reversement');
         end if;
       
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 and a_tyet_id_reimp_ordo=3 /*3:OUI*/ then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 and a_tyet_id_reimp_ordo=3 /*3:OUI*/ then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         
         reimputation_inventaire(a_dep_id, a_pco_num, null);
 
         update depense_ctrl_planco set pco_num=a_pco_num where dep_id=a_dep_id;
      end if;

      select d.exe_ordre, e.org_id, e.tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre 
        from depense_budget d, engage_budget e where d.eng_id=e.eng_id and d.dep_id=a_dep_id;
      if a_utl_ordre is not null then 
         Verifier.verifier_planco(my_exe_ordre, my_org_id, my_tcd_ordre, a_pco_num, a_utl_ordre);
      end if;
      Verifier.verifier_depense_coherence(a_dep_id);
  end;

PROCEDURE reimputation_inventaire (
      a_dep_id                depense_ctrl_planco.dep_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_tap_id                depense_budget.tap_id%type)
   IS
      my_nb        integer;
      my_dpco_id   depense_ctrl_planco.dpco_id%type;
      my_exe_ordre depense_ctrl_planco.exe_ordre%type;
      my_pcoa_num  maracuja.plan_comptable_amo.pcoa_num%type;
      my_pcoa_id   maracuja.plan_comptable_amo.pcoa_id%type;
   BEGIN
         select dpco_id, exe_ordre into my_dpco_id, my_exe_ordre from depense_ctrl_planco where dep_id=a_dep_id;
         select count(*) into my_nb from jefy_inventaire.inventaire_comptable 
           where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id);
         
         if my_nb=0 then return; end if;

         if a_pco_num is not null then 
            select count(*) into my_nb from jefy_inventaire.inventaire_comptable where clic_id in (         
                 select clic_id from jefy_inventaire.inventaire_comptable 
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id))
               and dpco_id<>my_dpco_id and invc_id not in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)
               and dpco_id is not null;
         
            if my_nb>0 then
               RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas effectuer cette r�imputation car elle concerne des inventaires comptables relies a d''autres depenses');
            end if;
            
			select count(*) into my_nb from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
			if my_nb<>1 then
               RAISE_APPLICATION_ERROR(-20001, 'il y a un probleme de configuration pour le compte d''amortissement de cette imputation '||a_pco_num||' sur l''exercice '||my_exe_ordre);
			end if;

			select pcoa_id into my_pcoa_id from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
			
            update jefy_inventaire.cle_inventaire_comptable set pco_num=a_pco_num where clic_id in (         
                 select clic_id from jefy_inventaire.inventaire_comptable 
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id));

				 
            select pcoa_num into my_pcoa_num from maracuja.plan_comptable_amo 
               where pcoa_id in (select pcoa_id from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre);
               
            update jefy_inventaire.amortissement set pco_num=my_pcoa_num where invc_id in (select invc_id from jefy_inventaire.inventaire_comptable 
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)); 
         end if;
         
         if a_tap_id is not null then
            raise_application_error(-20001, 'changement de prorata en presence d''inventaire, cas pas encore pris en compte');
         end if;
   END;
FUNCTION subdivise_engage_pour_depense(
      a_dep_id       depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
      my_nb          integer;
      bool_creation  integer;
      my_comm_id     commande.comm_id%type;
      my_cbud_id     commande_budget.cbud_id%type;
      my_old_commande_budget commande_budget%rowtype;
      my_commande_budget commande_budget%rowtype;
      my_ht          engage_budget.eng_ht_saisie%type;
      my_tva         engage_budget.eng_tva_saisie%type;
      my_ttc         engage_budget.eng_ttc_saisie%type;
      my_bud         engage_budget.eng_montant_budgetaire%type;
      my_depense     depense_budget%rowtype;
      my_engage      engage_budget%rowtype;
      my_new_engage  engage_budget%rowtype;
      my_eng_id      engage_budget.eng_id%type;
   BEGIN
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      my_eng_id:=my_depense.eng_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      
      -- si il n'y a pas d'autre depense sur cette engagement on n'en cr�e pas de nouveau 
      select count(*) into bool_creation from depense_budget where eng_id=my_depense.eng_id and dep_id<>a_dep_id and dep_ttc_saisie>0;
      
      -- on regarde si l'engagement appartient a une commande
      my_comm_id:=null;
      select count(*) into my_nb from commande_engagement where eng_id=my_depense.eng_id;
      if my_nb>0 then
         select min(comm_id) into my_comm_id from commande_engagement where eng_id=my_depense.eng_id;
      end if;
   
      if bool_creation=0 then
         -- on modifie juste l'engagement et enventuellement les infos de la commande
   
         if my_comm_id is not null then 
            select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
            select count(*) into my_nb from commande_budget 
              where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
            if my_nb>0 then  
               select min(cbud_id) into my_cbud_id from commande_budget 
                 where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
               update commande_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id where cbud_id=my_cbud_id;
            
               --si le taux de prorata a change, on met a jour les montants budgetaires  
               if my_engage.tap_id<>a_tap_id then
                  update commande_budget set cbud_montant_budgetaire=Budget.calculer_budgetaire(my_engage.exe_ordre,a_tap_id,a_org_id, cbud_ht_saisie, cbud_ttc_saisie)
                    where cbud_id=my_cbud_id;
                  corriger.corriger_commande_ctrl(my_cbud_id);
                  verifier.verifier_cde_budget_coherence(my_cbud_id);
               end if;
            end if;
         end if;
         
         -- on log l'ancien engagement
         engager.log_engage(my_eng_id, a_utl_ordre);
                  
         -- modif engagement
         update engage_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id, utl_ordre=a_utl_ordre where eng_id=my_eng_id;

         if my_engage.tap_id<>a_tap_id then
            -- modification des montants budgetaires   
            corrige_engage_budgetaire(my_eng_id);         
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre then 
            -- si on a chang� de ligne budgetaire on met a jour l'"ancien" budget
            budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre or my_engage.tap_id<>a_tap_id then
            -- si le taux de prorata ou la ligne budgetaire a change on met a jour le budget
            budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);
         end if;
         
      else

        my_nb:=0;
      
        -- si l'engagement appartient a une commande on regarde si un engagement avec les nouvelles infos existe deja 
        if my_comm_id is not null then
           select count(*) into my_nb from commande_engagement ce, engage_budget e
             where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
           if my_nb>0 then
             select min(e.eng_id) into my_eng_id from commande_engagement ce, engage_budget e
               where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
             
             -- on augmente les montants de l'engagement avec les montants de la depense
             augmenter_engage(a_dep_id, my_eng_id);
             
             -- on augmente la commande_bugdet correspondante (normalement elle existe)
             select * into my_new_engage from engage_budget where eng_id=my_eng_id;
             select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
             if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
                update commande_budget set cbud_montant_budgetaire=my_new_engage.eng_montant_budgetaire, cbud_ht_saisie=my_new_engage.eng_ht_saisie,
                   cbud_tva_saisie=my_new_engage.eng_tva_saisie, cbud_ttc_saisie=my_new_engage.eng_ttc_saisie where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
             end if;

           else
             -- on cree le nouvel engagement avec les infos de la depense
             my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);
             insert into commande_engagement select commande_engagement_seq.nextval, my_comm_id, my_eng_id from dual;
             
             -- on cree la commande_budget correspondante
             my_cbud_id:=creer_commande_budget(my_eng_id, my_comm_id);
           end if;
           
           -- on diminue l'ancienne commande_budget
           select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
           if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id 
                   and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
                   
                select * into my_new_engage from engage_budget where eng_id=my_eng_id;
                select * into my_commande_budget from commande_budget where cbud_id=my_cbud_id;
                
                my_ht :=my_commande_budget.cbud_ht_saisie -my_new_engage.eng_ht_saisie;
                if my_ht<0 then my_ht:=0; end if;
                my_tva:=my_commande_budget.cbud_tva_saisie-my_new_engage.eng_tva_saisie;
                if my_tva<0 then my_tva:=0; end if;
                my_ttc:=my_commande_budget.cbud_ttc_saisie-my_new_engage.eng_ttc_saisie;
                if my_ttc<0 then my_ttc:=0; end if;
                my_bud:=my_commande_budget.cbud_montant_budgetaire-my_new_engage.eng_montant_budgetaire;
                if my_bud<0 then my_bud:=0; end if;
                
                update commande_budget set cbud_montant_budgetaire=my_bud, cbud_ht_saisie=my_ht,
                   cbud_tva_saisie=my_tva, cbud_ttc_saisie=my_ttc where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
           end if; 

        else
           my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);       
        end if;

        -- on diminue l'ancien engagement du montant de la depense a reimputer
        -- ATTENTION : on ne diminue pas le budgetaire reste car on enleve la liquidation de cet engagement, et donc le reste ne change pas 
        select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
        my_ht :=my_engage.eng_ht_saisie -my_depense.dep_ht_saisie;
        if my_ht<0 then my_ht:=0; end if;
        my_tva:=my_engage.eng_tva_saisie-my_depense.dep_tva_saisie;
        if my_tva<0 then my_tva:=0; end if;
        my_ttc:=my_engage.eng_ttc_saisie-my_depense.dep_ttc_saisie;
        if my_ttc<0 then my_ttc:=0; end if;
        my_bud:=my_engage.eng_montant_budgetaire-my_depense.dep_montant_budgetaire;
        if my_bud<0 then my_bud:=0; end if;
        if my_bud<my_engage.eng_montant_budgetaire_reste then my_bud:=my_engage.eng_montant_budgetaire_reste; end if;
  
        update engage_budget set eng_ht_saisie=my_ht, eng_tva_saisie=my_tva, eng_ttc_saisie=my_ttc, eng_montant_budgetaire=my_bud
          where eng_id=my_engage.eng_id;
        corrige_engage_repartition(my_engage.eng_id);
        
        -- on associe la depense au nouvel engagement
        update depense_budget set eng_id=my_eng_id where dep_id=a_dep_id;
                     
        -- on lance la correction du budget
        budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
        budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);

      end if;
      
      return my_eng_id;
   END;
   
FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
     my_eng_id      engage_budget.eng_id%type;
     my_depense     depense_budget%rowtype;      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
   
     select engage_budget_seq.nextval into my_eng_id from dual;
     insert into engage_budget values (my_eng_id, my_depense.exe_ordre, Get_Numerotation(my_depense.exe_ordre, NULL, null,'ENGAGE_BUDGET'),
       a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, 0, 0, 0, 0, 0, sysdate, a_tyap_id, a_utl_ordre);
     
     augmenter_engage(a_dep_id,my_eng_id);
          
     return my_eng_id;
   END;
      
PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type)
   IS
     my_nb          integer;
     my_id          engage_ctrl_action.eact_id%type;
     my_engage      engage_budget%rowtype;
     my_depense     depense_budget%rowtype;
      
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     update engage_budget set eng_montant_budgetaire=eng_montant_budgetaire+my_depense.dep_montant_budgetaire, eng_ht_saisie=eng_ht_saisie+my_depense.dep_ht_saisie, 
       eng_tva_saisie=eng_tva_saisie+my_depense.dep_tva_saisie, eng_ttc_saisie=eng_ttc_saisie+my_depense.dep_ttc_saisie where eng_id=a_eng_id;
     
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
          select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id; 
          if my_nb=0 then 
             insert into engage_ctrl_action select engage_ctrl_action_seq.nextval, row_action.exe_ordre, a_eng_id, row_action.tyac_id,
                row_action.dact_montant_budgetaire, 0, row_action.dact_ht_saisie, row_action.dact_tva_saisie, row_action.dact_ttc_saisie, sysdate from dual;
          else
             select min(eact_id) into my_id from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id;
             update engage_ctrl_action set eact_montant_budgetaire=eact_montant_budgetaire+row_action.dact_montant_budgetaire,
               eact_ht_saisie=eact_ht_saisie+row_action.dact_ht_saisie, eact_tva_saisie=eact_tva_saisie+row_action.dact_tva_saisie,
               eact_ttc_saisie=eact_ttc_saisie+row_action.dact_ttc_saisie where eact_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
          select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id; 
          if my_nb=0 then 
            insert into engage_ctrl_analytique select engage_ctrl_analytique_seq.nextval, row_analytique.exe_ordre, a_eng_id, row_analytique.can_id,
               row_analytique.dana_montant_budgetaire, 0, row_analytique.dana_ht_saisie, row_analytique.dana_tva_saisie, row_analytique.dana_ttc_saisie, 
               sysdate from dual;
          else
             select min(eana_id) into my_id from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id;
             update engage_ctrl_analytique set eana_montant_budgetaire=eana_montant_budgetaire+row_analytique.dana_montant_budgetaire,
               eana_ht_saisie=eana_ht_saisie+row_analytique.dana_ht_saisie, eana_tva_saisie=eana_tva_saisie+row_analytique.dana_tva_saisie,
               eana_ttc_saisie=eana_ttc_saisie+row_analytique.dana_ttc_saisie where eana_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
          select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_convention select engage_ctrl_convention_seq.nextval, row_convention.exe_ordre, a_eng_id, row_convention.conv_ordre,
                row_convention.dcon_montant_budgetaire, 0, row_convention.dcon_ht_saisie, row_convention.dcon_tva_saisie, row_convention.dcon_ttc_saisie, 
                sysdate from dual;
          else
             select min(econ_id) into my_id from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre;
             update engage_ctrl_convention set econ_montant_budgetaire=econ_montant_budgetaire+row_convention.dcon_montant_budgetaire,
               econ_ht_saisie=econ_ht_saisie+row_convention.dcon_ht_saisie, econ_tva_saisie=econ_tva_saisie+row_convention.dcon_tva_saisie,
               econ_ttc_saisie=econ_ttc_saisie+row_convention.dcon_ttc_saisie where econ_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
          select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id; 
          if my_nb=0 then 
             insert into engage_ctrl_hors_marche select engage_ctrl_hors_marche_seq.nextval, row_hors_marche.exe_ordre, a_eng_id, row_hors_marche.typa_id, 
                row_hors_marche.ce_ordre, row_hors_marche.dhom_montant_budgetaire, 0,0, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_tva_saisie, 
                row_hors_marche.dhom_ttc_saisie, sysdate from dual;
          else
             select min(ehom_id) into my_id from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id;
             update engage_ctrl_hors_marche set ehom_montant_budgetaire=ehom_montant_budgetaire+row_hors_marche.dhom_montant_budgetaire,
               ehom_ht_saisie=ehom_ht_saisie+row_hors_marche.dhom_ht_saisie, ehom_tva_saisie=ehom_tva_saisie+row_hors_marche.dhom_tva_saisie,
               ehom_ttc_saisie=ehom_ttc_saisie+row_hors_marche.dhom_ttc_saisie where ehom_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
          select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_marche select engage_ctrl_marche_seq.nextval, row_marche.exe_ordre, a_eng_id, row_marche.att_ordre,
                row_marche.dmar_montant_budgetaire, 0, 0, row_marche.dmar_ht_saisie, row_marche.dmar_tva_saisie, row_marche.dmar_ttc_saisie, sysdate from dual;
          else
             select min(emar_id) into my_id from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre;
             update engage_ctrl_marche set emar_montant_budgetaire=emar_montant_budgetaire+row_marche.dmar_montant_budgetaire,
               emar_ht_saisie=emar_ht_saisie+row_marche.dmar_ht_saisie, emar_tva_saisie=emar_tva_saisie+row_marche.dmar_tva_saisie,
               emar_ttc_saisie=emar_ttc_saisie+row_marche.dmar_ttc_saisie where emar_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then
       for row_planco in planco
       loop
          select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num; 
          if my_nb=0 then 
             insert into engage_ctrl_planco select engage_ctrl_planco_seq.nextval, row_planco.exe_ordre, a_eng_id, row_planco.pco_num,
                row_planco.dpco_montant_budgetaire, 0, row_planco.dpco_ht_saisie, row_planco.dpco_tva_saisie, row_planco.dpco_ttc_saisie, sysdate from dual;
          else
             select min(epco_id) into my_id from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num;
             update engage_ctrl_planco set epco_montant_budgetaire=epco_montant_budgetaire+row_planco.dpco_montant_budgetaire,
               epco_ht_saisie=epco_ht_saisie+row_planco.dpco_ht_saisie, epco_tva_saisie=epco_tva_saisie+row_planco.dpco_tva_saisie,
               epco_ttc_saisie=epco_ttc_saisie+row_planco.dpco_ttc_saisie where epco_id=my_id;
          end if;
       end loop;
     end if;

     verifier.VERIFIER_ENGAGE_COHERENCE(a_eng_id);
   END;

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type) return number
   IS
     my_cbud_id       commande_budget.cbud_id%type;
     my_engage        engage_budget%rowtype;
     
     my_nb integer;
     my_pourcentage number(15,5);
     my_total_pourcentage number(15,5);
     
     my_chaine_action varchar2(3000);
     my_chaine_analytique varchar2(3000);
     my_chaine_convention varchar2(3000);
     my_chaine_hors_marche varchar2(3000);
     my_chaine_marche varchar2(3000);
     my_chaine_planco varchar2(3000);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;

   BEGIN
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     -- on definit les infos, notamment les pourcentages
     my_chaine_action:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_action.eact_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_action:=my_chaine_action||row_action.tyac_id||'$'||row_action.eact_ht_saisie||'$'||row_action.eact_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_action:=my_chaine_action||'$';

     my_chaine_analytique:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         my_pourcentage:=round(100*row_analytique.eana_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_analytique:=my_chaine_analytique||row_analytique.can_id||'$'||row_analytique.eana_ht_saisie||'$'
               ||row_analytique.eana_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_analytique:=my_chaine_analytique||'$';

     my_chaine_convention:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         my_pourcentage:=round(100*row_convention.econ_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_convention:=my_chaine_convention||row_convention.conv_ordre||'$'||row_convention.econ_ht_saisie||'$'
              ||row_convention.econ_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_convention:=my_chaine_convention||'$';

     my_chaine_hors_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
           my_chaine_hors_marche:=my_chaine_hors_marche||row_hors_marche.typa_id||'$'||row_hors_marche.ce_ordre||'$'
              ||row_hors_marche.ehom_ht_saisie||'$'||row_hors_marche.ehom_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_hors_marche:=my_chaine_hors_marche||'$';

     my_chaine_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
           my_chaine_marche:=my_chaine_marche||row_marche.att_ordre||'$'||row_marche.emar_ht_saisie||'$'||row_marche.emar_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_marche:=my_chaine_marche||'$';

     my_chaine_planco:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_planco.epco_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_planco:=my_chaine_planco||row_planco.pco_num||'$'||row_planco.epco_ht_saisie||'$'||row_planco.epco_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_planco:=my_chaine_planco||'$';
     
     -- on cree
     select commande_budget_seq.nextval into my_cbud_id from dual;
     commander.ins_commande_budget (my_cbud_id, a_comm_id, my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre, my_engage.tap_id, my_engage.eng_ht_saisie,
       my_engage.eng_ttc_saisie, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche, my_chaine_marche, my_chaine_planco);
               
     -- on corrige pour calculer les montants des repartitions
     corriger.corriger_commande_ctrl(my_cbud_id);
     verifier.verifier_cde_budget_coherence(my_cbud_id);
     
     
     return my_cbud_id;
   END;

PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;
     my_eng_montant_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_reste engage_budget.eng_montant_budgetaire%type;
     my_ctrl_bud engage_budget.eng_montant_budgetaire%type;
     my_dep_budgetaire depense_budget.dep_montant_budgetaire%type;
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant engage_budget
     select * into my_engage from engage_budget where eng_id=a_eng_id;
    
     my_eng_montant_budgetaire:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id,
         my_engage.org_id, my_engage.eng_ht_saisie, my_engage.eng_ttc_saisie);
     
     select nvl(SUM(Budget.calculer_budgetaire(exe_ordre, my_engage.tap_id, my_engage.org_id, dep_ht_saisie, dep_ttc_saisie)),0) 
        into my_dep_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
     my_reste:=my_eng_montant_budgetaire-my_dep_budgetaire;
     if my_reste<0 then my_reste:=0; end if;
     
     update engage_budget set eng_montant_budgetaire=my_eng_montant_budgetaire, eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;

     -- maj montant engage_ctrl_action
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update engage_ctrl_action set eact_montant_budgetaire=my_reste, eact_montant_budgetaire_reste=my_reste  where eact_id=row_action.eact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_action.eact_ht_saisie, row_action.eact_ttc_saisie);

            update engage_ctrl_action set eact_montant_budgetaire=my_ctrl_bud, eact_montant_budgetaire_reste=my_ctrl_bud where eact_id=row_action.eact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_analytique
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update engage_ctrl_analytique set eana_montant_budgetaire=my_reste, eana_montant_budgetaire_reste=my_reste  where eana_id=row_analytique.eana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_analytique.eana_ht_saisie, row_analytique.eana_ttc_saisie);

            update engage_ctrl_analytique set eana_montant_budgetaire=my_ctrl_bud, eana_montant_budgetaire_reste=my_ctrl_bud where eana_id=row_analytique.eana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(a_eng_id);
     end if;

     -- maj montant engage_ctrl_convention
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update engage_ctrl_convention set econ_montant_budgetaire=my_reste, econ_montant_budgetaire_reste=my_reste  where econ_id=row_convention.econ_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_convention.econ_ht_saisie, row_convention.econ_ttc_saisie);

            update engage_ctrl_convention set econ_montant_budgetaire=my_ctrl_bud, econ_montant_budgetaire_reste=my_ctrl_bud where econ_id=row_convention.econ_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(a_eng_id);
     end if;

     -- maj montant engage_ctrl_hors_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste, ehom_montant_budgetaire_reste=my_reste  where ehom_id=row_hors_marche.ehom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_hors_marche.ehom_ht_saisie, row_hors_marche.ehom_ttc_saisie);

            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_ctrl_bud, ehom_montant_budgetaire_reste=my_ctrl_bud where ehom_id=row_hors_marche.ehom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(a_eng_id);
     end if;

     -- maj montant engage_ctrl_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update engage_ctrl_marche set emar_montant_budgetaire=my_reste, emar_montant_budgetaire_reste=my_reste  where emar_id=row_marche.emar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_marche.emar_ht_saisie, row_marche.emar_ttc_saisie);

            update engage_ctrl_marche set emar_montant_budgetaire=my_ctrl_bud, emar_montant_budgetaire_reste=my_ctrl_bud where emar_id=row_marche.emar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update engage_ctrl_planco set epco_montant_budgetaire=my_reste, epco_montant_budgetaire_reste=my_reste  where epco_id=row_planco.epco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_planco.epco_ht_saisie, row_planco.epco_ttc_saisie);

            update engage_ctrl_planco set epco_montant_budgetaire=my_ctrl_bud, epco_montant_budgetaire_reste=my_ctrl_bud where epco_id=row_planco.epco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(a_eng_id);
     end if;

     verifier.verifier_engage_coherence(a_eng_id);
   END;

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;

     my_montant_ht_total engage_budget.eng_ht_saisie%type;
     my_montant_ttc_total engage_budget.eng_ttc_saisie%type;
     my_montant_budgetaire_total engage_budget.eng_montant_budgetaire%type;
     
     my_ht engage_budget.eng_ht_saisie%type;
     my_ttc engage_budget.eng_ttc_saisie%type;
     my_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_pourcentage number (12,8);
          
     my_reste_ht engage_budget.eng_ht_saisie%type;
     my_reste_ttc engage_budget.eng_ttc_saisie%type;
     my_reste_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_total_pourcentage number (12,8);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
     my_nb_decimales        NUMBER;
   BEGIN

       -- infos references
       select * into my_engage from engage_budget where eng_id=a_eng_id;
       my_nb_decimales:=liquider_outils.get_nb_decimales(my_engage.exe_ordre);
        
       select sum(eact_ht_saisie), sum(eact_montant_budgetaire), sum(eact_ttc_saisie)
          into my_montant_ht_total, my_montant_budgetaire_total, my_montant_ttc_total from engage_ctrl_action where eng_id=a_eng_id;
   
   
       -- maj montant engage_ctrl_action       
       select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_action in action
         loop
           if action%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_action set eact_montant_budgetaire=my_reste_budgetaire, eact_ht_saisie=my_reste_ht,
                 eact_ttc_saisie=my_reste_ttc,  eact_tva_saisie=my_reste_ttc-my_reste_ht where eact_id=row_action.eact_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_action.eact_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_action.eact_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_action.eact_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_action set eact_montant_budgetaire=my_budgetaire, eact_ht_saisie=my_ht,
                 eact_ttc_saisie=my_ttc, eact_tva_saisie=my_ttc-my_ht where eact_id=row_action.eact_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_analytique       
       select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(eana_ht_saisie) into my_total_pourcentage from engage_ctrl_analytique where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_analytique in analytique
         loop
           if analytique%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_analytique set eana_montant_budgetaire=my_reste_budgetaire, eana_ht_saisie=my_reste_ht,
                 eana_ttc_saisie=my_reste_ttc,  eana_tva_saisie=my_reste_ttc-my_reste_ht where eana_id=row_analytique.eana_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_analytique.eana_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_analytique.eana_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_analytique.eana_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_analytique set eana_montant_budgetaire=my_budgetaire, eana_ht_saisie=my_ht,
                 eana_ttc_saisie=my_ttc, eana_tva_saisie=my_ttc-my_ht where eana_id=row_analytique.eana_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_convention
       select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(econ_ht_saisie) into my_total_pourcentage from engage_ctrl_convention where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_convention in convention
         loop
           if convention%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_convention set econ_montant_budgetaire=my_reste_budgetaire, econ_ht_saisie=my_reste_ht,
                 econ_ttc_saisie=my_reste_ttc,  econ_tva_saisie=my_reste_ttc-my_reste_ht where econ_id=row_convention.econ_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_convention.econ_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_convention.econ_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_convention.econ_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_convention set econ_montant_budgetaire=my_budgetaire, econ_ht_saisie=my_ht,
                 econ_ttc_saisie=my_ttc, econ_tva_saisie=my_ttc-my_ht where econ_id=row_convention.econ_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_hors_marche       
       select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;

         for row_hors_marche in hors_marche
         loop
           if hors_marche%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste_budgetaire, ehom_ht_saisie=my_reste_ht,
                 ehom_ttc_saisie=my_reste_ttc,  ehom_tva_saisie=my_reste_ttc-my_reste_ht where ehom_id=row_hors_marche.ehom_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_hors_marche.ehom_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_hors_marche.ehom_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_hors_marche.ehom_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_budgetaire, ehom_ht_saisie=my_ht,
                 ehom_ttc_saisie=my_ttc, ehom_tva_saisie=my_ttc-my_ht where ehom_id=row_hors_marche.ehom_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_marche       
       select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;

         update engage_ctrl_marche set emar_montant_budgetaire=my_reste_budgetaire, emar_ht_saisie=my_reste_ht,
              emar_ttc_saisie=my_reste_ttc,  emar_tva_saisie=my_reste_ttc-my_reste_ht where eng_id=a_eng_id;
       end if;   

       -- maj montant engage_ctrl_planco
       select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_planco in planco
         loop
           if planco%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_planco set epco_montant_budgetaire=my_reste_budgetaire, epco_ht_saisie=my_reste_ht,
                 epco_ttc_saisie=my_reste_ttc,  epco_tva_saisie=my_reste_ttc-my_reste_ht where epco_id=row_planco.epco_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_planco.epco_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_planco.epco_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_planco.epco_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_planco set epco_montant_budgetaire=my_budgetaire, epco_ht_saisie=my_ht,
                 epco_ttc_saisie=my_ttc, epco_tva_saisie=my_ttc-my_ht where epco_id=row_planco.epco_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       --corrige_engage_budgetaire();
       verifier.verifier_engage_coherence(a_eng_id);
   END;
   
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type
   ) 
   IS
     my_depense depense_budget%rowtype;
     my_dep_montant_budgetaire depense_budget.dep_montant_budgetaire%type;
     my_reste depense_budget.dep_montant_budgetaire%type;
     my_ctrl_bud depense_budget.dep_montant_budgetaire%type;
     
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant depense_budget
     select * into my_depense from depense_budget where dep_id=a_dep_id;
    
     my_dep_montant_budgetaire:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, my_depense.dep_ht_saisie, my_depense.dep_ttc_saisie);
     update depense_budget set dep_montant_budgetaire=my_dep_montant_budgetaire where dep_id=a_dep_id;

     -- maj montant depense_ctrl_action
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update depense_ctrl_action set dact_montant_budgetaire=my_reste where dact_id=row_action.dact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_action.dact_ht_saisie, row_action.dact_ttc_saisie);

            update depense_ctrl_action set dact_montant_budgetaire=my_ctrl_bud where dact_id=row_action.dact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(my_depense.eng_id);
     end if;
     
     -- maj montant depense_ctrl_analytique
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update depense_ctrl_analytique set dana_montant_budgetaire=my_reste where dana_id=row_analytique.dana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_analytique.dana_ht_saisie, row_analytique.dana_ttc_saisie);

            update depense_ctrl_analytique set dana_montant_budgetaire=my_ctrl_bud where dana_id=row_analytique.dana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_convention
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update depense_ctrl_convention set dcon_montant_budgetaire=my_reste where dcon_id=row_convention.dcon_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_convention.dcon_ht_saisie, row_convention.dcon_ttc_saisie);

            update depense_ctrl_convention set dcon_montant_budgetaire=my_ctrl_bud where dcon_id=row_convention.dcon_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_hors_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_reste where dhom_id=row_hors_marche.dhom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_ttc_saisie);

            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_ctrl_bud where dhom_id=row_hors_marche.dhom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update depense_ctrl_marche set dmar_montant_budgetaire=my_reste where dmar_id=row_marche.dmar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_marche.dmar_ht_saisie, row_marche.dmar_ttc_saisie);

            update depense_ctrl_marche set dmar_montant_budgetaire=my_ctrl_bud where dmar_id=row_marche.dmar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(my_depense.eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update depense_ctrl_planco set dpco_montant_budgetaire=my_reste where dpco_id=row_planco.dpco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_planco.dpco_ht_saisie, row_planco.dpco_ttc_saisie);

            update depense_ctrl_planco set dpco_montant_budgetaire=my_ctrl_bud where dpco_id=row_planco.dpco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(my_depense.eng_id);
     end if;

     verifier.verifier_depense_coherence(a_dep_id);
   END;
   
PROCEDURE rempli_reimputation(
      a_dep_id       depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre    depense_budget.utl_ordre%type
   )
   IS
     my_nb          integer;
     my_reim_id     reimputation.reim_id%type;
     my_exe_ordre   depense_budget.exe_ordre%type;
     my_reim_numero reimputation.reim_numero%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     my_reim_numero := Get_Numerotation(my_exe_ordre, NULL, null, 'REIMPUTATION');

     select reimputation_seq.nextval into my_reim_id from dual;
     insert into reimputation values (my_reim_id, my_exe_ordre, my_reim_numero, a_dep_id, a_reim_libelle, a_utl_ordre, sysdate);
     
     insert into reimputation_action select reimputation_action_seq.nextval, my_reim_id, tyac_id, exe_ordre, dact_montant_budgetaire, dact_ht_saisie,
         dact_tva_saisie, dact_ttc_saisie from depense_ctrl_action where dep_id=a_dep_id;
         
     insert into reimputation_analytique select reimputation_analytique_seq.nextval, my_reim_id, can_id, dana_montant_budgetaire, dana_ht_saisie,
         dana_tva_saisie, dana_ttc_saisie from depense_ctrl_analytique where dep_id=a_dep_id;
         
     insert into reimputation_budget select reimputation_budget_seq.nextval, my_reim_id, d.eng_id, e.org_id, e.tcd_ordre, d.tap_id
        from depense_budget d, engage_budget e where e.eng_id=d.eng_id and dep_id=a_dep_id;
     
     insert into reimputation_convention select reimputation_convention_seq.nextval, my_reim_id, conv_ordre, dcon_montant_budgetaire, dcon_ht_saisie,
         dcon_tva_saisie, dcon_ttc_saisie from depense_ctrl_convention where dep_id=a_dep_id;
         
     insert into reimputation_hors_marche select reimputation_hors_marche_seq.nextval, my_reim_id, typa_id, ce_ordre, dhom_montant_budgetaire, dhom_ht_saisie,
         dhom_tva_saisie, dhom_ttc_saisie from depense_ctrl_hors_marche where dep_id=a_dep_id;
         
     insert into reimputation_marche select reimputation_marche_seq.nextval, my_reim_id, att_ordre, dmar_montant_budgetaire, dmar_ht_saisie,
         dmar_tva_saisie, dmar_ttc_saisie from depense_ctrl_marche where dep_id=a_dep_id;
         
     insert into reimputation_planco select reimputation_planco_seq.nextval, my_reim_id, pco_num, exe_ordre, dpco_montant_budgetaire, dpco_ht_saisie,
         dpco_tva_saisie, dpco_ttc_saisie from depense_ctrl_planco where dep_id=a_dep_id;
   END;
   
PROCEDURE reimputation_maracuja (
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type
   )
   IS
      my_dep_id       depense_budget.dep_id%type;
      cursor depenses is select dep_id from depense_ctrl_planco where man_id=a_man_id;
   BEGIN
      if a_man_id is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer un man_id');
      end if;
      if a_pco_num is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer la nouvelle imputation');
      end if;
        
      open depenses();
      loop
         fetch depenses into my_dep_id;
         exit when depenses%notfound;
         
         depense_avec_infos(my_dep_id, 'reimputation comptable', null, null, null, null, a_pco_num, null,
            null, null, null, null, 4 /*NON*/, null);
      end loop;
      close depenses;
   END;

END;
/


--60_REMPLACER_PRORATA.prc
CREATE OR REPLACE PROCEDURE JEFY_DEPENSE.remplacer_prorata(
   a_exe_ordre     jefy_admin.exercice.exe_ordre%type,
   a_old_tap_id    jefy_admin.taux_prorata.tap_id%type,
   a_new_tap_id    jefy_admin.taux_prorata.tap_id%type)
IS
           CURSOR liste IS SELECT * FROM  ENGAGE_BUDGET WHERE exe_ordre=a_exe_ordre and eng_montant_budgetaire_reste>0 AND tap_id=a_old_tap_id;
		   engage ENGAGE_BUDGET%ROWTYPE;
		   bud ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

           CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id;
		   depense DEPENSE_BUDGET%ROWTYPE;
		   depbud DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

      	   CURSOR listeaction(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		   action COMMANDE_CTRL_ACTION%ROWTYPE;
      	   CURSOR listeanalytique(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		   ANALYTIQUE COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;
      	   CURSOR listeconvention(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		   convention COMMANDE_CTRL_CONVENTION%ROWTYPE;
      	   CURSOR listehors_marche(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		   hors_marche COMMANDE_CTRL_HORS_MARCHE%ROWTYPE;
      	   CURSOR listemarche(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		   marche COMMANDE_CTRL_MARCHE%ROWTYPE;
      	   CURSOR listeplanco(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
		   planco COMMANDE_CTRL_PLANCO%ROWTYPE;
		   
		   my_nb INTEGER;
		   cdebudget COMMANDE_BUDGET%ROWTYPE;
		   chaine_action  VARCHAR2(3000);
		   chaine_analytique  VARCHAR2(3000);
		   chaine_convention  VARCHAR2(3000);
		   chaine_hors_marche  VARCHAR2(3000);
		   chaine_marche  VARCHAR2(3000);
		   chaine_planco  VARCHAR2(3000);
BEGIN
           OPEN liste();
 		   LOOP
		      FETCH  liste INTO engage;
		      EXIT WHEN liste%NOTFOUND;
			  
              -- si l''engagement n''a pas de TVA, le changement de prorata ne change pas le budgetaire
              if engage.eng_tva_saisie<>0 then
			     -- correction de l'engagement
			     bud:=Budget.calculer_budgetaire(engage.exe_ordre, a_new_tap_id, engage.org_id, engage.eng_ht_saisie, engage.eng_ttc_saisie);

			     depbud:=0;
                 OPEN listedepense(engage.eng_id);
 	             LOOP
		            FETCH  listedepense INTO depense;
		            EXIT WHEN listedepense%NOTFOUND;
				   
				    depbud:=depbud+Budget.calculer_budgetaire(depense.exe_ordre, a_new_tap_id, engage.org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
                 END LOOP;
			     CLOSE listedepense;
			  
			     UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=bud, eng_montant_budgetaire_reste=bud-depbud, tap_id=a_new_tap_id WHERE eng_id=engage.eng_id;
			     Corriger.upd_engage_reste(engage.eng_id);
			     Budget.maj_budget(engage.exe_ordre, engage.org_id, engage.tcd_ordre);
             	  
  			     -- correction du commande_budget associe ... il doit normalement y en avoir un seul
			     SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id;
			  
			     IF my_nb>0 THEN
			   
			        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			            AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				        AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
				 
				    IF my_nb=1 THEN
				      SELECT * INTO cdebudget FROM COMMANDE_BUDGET WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			                 AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				             AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
					  
					  chaine_action:='';
 	 	              OPEN listeaction(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeaction INTO action;
		      		       EXIT WHEN listeaction%NOTFOUND;
					  
					  	   chaine_action:=chaine_action||action.tyac_id||'$'||action.cact_ht_saisie||'$'||action.cact_ttc_saisie||'$'||action.cact_pourcentage||'$';
					  END LOOP;
					  CLOSE listeaction;
					  chaine_action:=chaine_action||'$';

					  chaine_analytique:='';
 	 	              OPEN listeanalytique(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeanalytique INTO analytique;
		      		       EXIT WHEN listeanalytique%NOTFOUND;
					  
					  	   chaine_analytique:=chaine_analytique||analytique.can_id||'$'||analytique.cana_ht_saisie||'$'||analytique.cana_ttc_saisie||'$'||analytique.cana_pourcentage||'$';
					  END LOOP;
					  CLOSE listeanalytique;
					  chaine_analytique:=chaine_analytique||'$';

					  chaine_convention:='';
 	 	              OPEN listeconvention(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeconvention INTO convention;
		      		       EXIT WHEN listeconvention%NOTFOUND;
					  
					  	   chaine_convention:=chaine_convention||convention.conv_ordre||'$'||convention.ccon_ht_saisie||'$'||convention.ccon_ttc_saisie||'$'||convention.ccon_pourcentage||'$';
					  END LOOP;
					  CLOSE listeconvention;
					  chaine_convention:=chaine_convention||'$';

					  chaine_hors_marche:='';
 	 	              OPEN listehors_marche(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listehors_marche INTO hors_marche;
		      		       EXIT WHEN listehors_marche%NOTFOUND;
					  
					  	   chaine_hors_marche:=chaine_hors_marche||hors_marche.typa_id||'$'||hors_marche.ce_ordre||'$'||hors_marche.chom_ht_saisie||'$'||hors_marche.chom_ttc_saisie||'$';
					  END LOOP;
					  CLOSE listehors_marche;
					  chaine_hors_marche:=chaine_hors_marche||'$';

					  chaine_marche:='';
 	 	              OPEN listemarche(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listemarche INTO marche;
		      		       EXIT WHEN listemarche%NOTFOUND;
					  
					  	   chaine_marche:=chaine_marche||marche.att_ordre||'$'||marche.cmar_ht_saisie||'$'||marche.cmar_ttc_saisie||'$';
					  END LOOP;
					  CLOSE listemarche;
					  chaine_marche:=chaine_marche||'$';

					  chaine_planco:='';
 	 	              OPEN listeplanco(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeplanco INTO planco;
		      		       EXIT WHEN listeplanco%NOTFOUND;
					  
					  	   chaine_planco:=chaine_planco||planco.pco_num||'$'||planco.cpco_ht_saisie||'$'||planco.cpco_ttc_saisie||'$'||planco.cpco_pourcentage||'$';
					  END LOOP;
					  CLOSE listeplanco;
					  chaine_planco:=chaine_planco||'$';
					  
					     Commander.upd_commande_budget(cdebudget.cbud_id, cdebudget.cbud_ht_saisie, cdebudget.cbud_ttc_saisie,
					    	  a_new_tap_id, chaine_action, chaine_analytique, chaine_convention, chaine_hors_marche, chaine_marche, chaine_planco);
				    END IF;
				 	 
                 END IF;
              else
                 update COMMANDE_BUDGET SET tap_id=a_new_tap_id WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			            AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				        AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
                 UPDATE ENGAGE_BUDGET SET tap_id=a_new_tap_id WHERE eng_id=engage.eng_id;
              end if;		
			      
		  END LOOP;
		  CLOSE liste;
END;
/

--70_REVERSER.pkb

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Reverser
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE ins_reverse_papier (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpco_ht_saisie          DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
      my_dpco_ttc_saisie         DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
      my_sum_rev_ht              DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
      my_sum_rev_ttc          DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
      my_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE;
      my_dpp_id_reversement   DEPENSE_PAPIER.dpp_id_reversement%TYPE;
      my_nb       INTEGER;
      my_nb_verif    integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);

        -- pour un ORV il faut des montants negatifs.
           IF a_dpp_ht_initial>0 OR a_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        IF a_dpp_id_reversement IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture initiale n''existe pas (dpp_id='||a_dpp_id_reversement||')');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(a_dpp_ht_initial)>ABS(a_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(a_dpp_ht_initial, a_dpp_ttc_initial);

        -- on verifie que la facture referencee n'est pas un ORV (pas d'ORV sur un ORV);
         SELECT dpp_id_reversement, mod_ordre INTO my_dpp_id_reversement, my_mod_ordre
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;

        IF my_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ordre de reversement a partir d''un autre ORV');
        END IF;

        -- verifier que les montants du total des ORV ne depassent pas ceux mandates et vises de la facture d'origine.
        SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dpco_ht_saisie, my_dpco_ttc_saisie
           FROM DEPENSE_CTRL_PLANCO WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET
              WHERE dpp_id=a_dpp_id_reversement) AND dpco_ttc_saisie>0 AND man_id IN
               (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

        SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
           FROM DEPENSE_CTRL_PLANCO WHERE dpco_ttc_saisie<0 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE
             dpp_id IN (SELECT dpp_id FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id_reversement));

        select count(*) into my_nb_verif from depense_ctrl_planco 
           where dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id_reversement)
             and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
        IF my_nb_verif=0 and (my_dpco_ht_saisie<ABS(my_sum_rev_ht)+ABS(a_dpp_ht_initial) OR
           my_dpco_ttc_saisie<ABS(my_sum_rev_ttc)+ABS(a_dpp_ttc_initial)) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui vise de la facture initiale');
        END IF;
        
           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
           0, 0, a_fou_ordre, a_rib_ordre, my_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
           a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
           a_dpp_ht_initial, my_dpp_tva_initial, a_dpp_ttc_initial,null,null,null,null,null,null);
   END;

   PROCEDURE ins_reverse (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
      my_org_id              engage_budget.org_id%type;
   BEGIN
          IF a_dep_ttc_saisie<>0 THEN
             select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
               
             -- verifier qu'on a le droit de liquider sur cet exercice.
             Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

             -- lancement des differentes procedures d'insertion des tables de depense.
             ins_reverse_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, a_dep_ht_saisie, a_dep_ttc_saisie,
                a_tap_id, a_utl_ordre, a_dep_id_reversement);

              UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+a_dep_ht_saisie,
                  dpp_tva_saisie=dpp_tva_saisie+a_dep_ttc_saisie-a_dep_ht_saisie,
                  dpp_ttc_saisie=dpp_ttc_saisie+a_dep_ttc_saisie
                 WHERE dpp_id=a_dpp_id;

             ins_reverse_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
             ins_reverse_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
             ins_reverse_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
             ins_reverse_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
             ins_reverse_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);
             ins_reverse_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Corriger.upd_engage_reste(a_eng_id);

             -- on verifie la coherence des montants entre les differents depense_.
             Verifier.verifier_depense_coherence(a_dep_id);
             Verifier.verifier_depense_pap_coherence(a_dpp_id);
             -- on verifie si la coherence des montants budgetaires restant est  conservee.
             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_reverse (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                            INTEGER;
      my_exe_ordre                    DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire            DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dpp_id                        DEPENSE_BUDGET.dpp_id%TYPE;
      my_dep_ht_saisie                DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie                DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dpco_id                    DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
      my_org_id              engage_budget.org_id%type;
      my_eng_id              engage_budget.eng_id%type;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
   BEGIN

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ORV n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

           SELECT  d.exe_ordre, d.dep_montant_budgetaire, d.dpp_id, d.dep_ht_saisie, d.dep_ttc_saisie, d.eng_id
           INTO my_exe_ordre, my_montant_budgetaire, my_dpp_id, my_dep_ht_saisie, my_dep_ttc_saisie, my_eng_id
           FROM DEPENSE_BUDGET d WHERE d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        select org_id into my_org_id from engage_budget where eng_id=my_eng_id;
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si c'est un ORV.
           IF my_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- tout est bon ... on supprime la depense.
        log_reverse_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

        --Apres_Liquide.del_depense_budget(my_eng_id);
   END;

      PROCEDURE del_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE)
      IS
       my_nb         INTEGER;
       my_exe_ordre     DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dpp_ttc_initial  DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_reverse_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

      PROCEDURE log_reverse_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id                    Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
           INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_reverse_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                 DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                 DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement     DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_nb                 INTEGER;
       my_nb_verif    integer;

       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;
       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_exe_ordre       ENGAGE_BUDGET.exe_ordre%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

       my_dpp_ht_initial         DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial     DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial     DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dpp_ttc_saisie     DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
       my_dpp_id_reversement DEPENSE_PAPIER.dpp_id_reversement%TYPE;
       my_dpp_id_origine DEPENSE_BUDGET.dpp_id%TYPE;

       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN

           -- pour un ORV il faut des montants negatifs.
           IF a_dep_ht_saisie>0 OR a_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        IF a_dep_id_reversement IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture initiale n''existe pas (dep_id='||a_dep_id_reversement||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT org_id, exe_ordre INTO my_org_id, my_exe_ordre FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
          SELECT exe_ordre, dpp_id_reversement INTO my_dpp_exe_ordre, my_dpp_id_reversement
         FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ORV doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        SELECT exe_ordre, eng_id, tap_id, dep_id_reversement, dpp_id
          INTO my_dep_exe_ordre, my_eng_id, my_dep_tap_id, my_dep_id_reversement, my_dpp_id_origine
          FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;

        IF my_dpp_id_reversement<>my_dpp_id_origine THEN
           RAISE_APPLICATION_ERROR(-20001,'la facture papier d''origine est differente de celle de la depense budget d''origine');
        END IF;

        IF a_eng_id<>my_eng_id THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ordre de reversement doit etre sur le meme engagement que la facture initiale');
        END IF;

        IF my_dep_exe_ordre<>my_dep_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit etre sur le meme exercice que la facture initiale');
        END IF;

        IF a_tap_id<>my_dep_tap_id THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit avoir le meme prorata que la facture initiale.');
        END IF;

        IF my_dep_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ORV a partir d''un autre ORV.');
        END IF;

        -- verification des coherences entre les sommes (si elles ne depassent pas).
        SELECT dpp_ttc_saisie
               INTO my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0) , NVL(SUM(dep_ttc_saisie),0)
               INTO my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        -- on verifie la coherence des montants.
         IF ABS(a_dep_ht_saisie)>ABS(a_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste si le ttc de la depense papier est du meme signe que celui de depense_budget.
        IF my_dpp_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Incoherence de signe entre le montant de la facture et le montant budgetaire');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(a_dep_ht_saisie, a_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declaré dans la papier.
        /*IF ABS(my_sum_ht_saisie+a_dep_ht_saisie) > ABS(my_dpp_ht_saisie) OR
           ABS(my_sum_tva_saisie+my_dep_tva_saisie) > ABS(my_dpp_tva_saisie) OR
           ABS(my_sum_ttc_saisie+a_dep_ttc_saisie) > ABS(my_dpp_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        END IF;*/

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              a_dep_ht_saisie,a_dep_ttc_saisie);

        -- on verifie que la somme des ORV ne depasse pas le montant de la facture initiale ... mandate et vise !!.
          SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dep_ht_init, my_dep_ttc_init
           FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id_reversement AND dpco_montant_budgetaire>=0 AND man_id IN
               (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
          FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id_reversement;

        select count(*) into my_nb_verif from depense_ctrl_planco 
           where dep_id=a_dep_id_reversement
             and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
        IF my_nb_verif=0 and (my_dep_ht_init < ABS(my_sum_rev_ht) + ABS(a_dep_ht_saisie) OR
           my_dep_ttc_init < ABS(my_sum_rev_ttc) + ABS(a_dep_ttc_saisie)) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui de la facture initiale');
        END IF;

        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

        -- on reverse pour liberer les credits.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           a_dep_ht_saisie, my_dep_tva_saisie, a_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_reverse_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dact_id                   DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                        DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       sum_dact_montant_budgetaire DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_dact_ht_saisie             DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie           DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie           DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                   DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            -- Pour les O.R.
            IF my_dact_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un ORV.');
            END IF;

            -- verifier que l'action qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR pour cette action et que l'action existe bien au depart.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ACTION
               WHERE tyac_id=my_tyac_id
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet ORV et cette action n''est pas autorisee (tyac_id='||my_tyac_id||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dact_montant_budgetaire) THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dact_montant_budgetaire),0) INTO sum_dact_montant_budgetaire
              FROM DEPENSE_CTRL_ACTION WHERE tyac_id=my_tyac_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dact_montant_budgetaire+my_dact_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette action (tyac_id='||my_tyac_id||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id, a_exe_ordre, a_dep_id, my_tyac_id,
                my_dact_montant_budgetaire, my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dana_id                   DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                        DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       sum_dana_montant_budgetaire DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_dana_ht_saisie             DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);

            -- Pour les O.R.
            IF my_dana_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ANALYTIQUE
               WHERE can_id=my_can_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code analytique n''est pas autorise pour cette depense (can_id='||my_can_id||')');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dana_montant_budgetaire) THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dana_montant_budgetaire),0) INTO sum_dana_montant_budgetaire
               FROM DEPENSE_CTRL_ANALYTIQUE WHERE can_id=my_can_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
               OR dep_id_reversement=my_dep_id_reversement);
               
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dana_montant_budgetaire+my_dana_montant_budgetaire<0) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code analytique (can_id='||my_can_id||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

      PROCEDURE ins_reverse_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                   DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre                    DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       sum_dcon_montant_budgetaire DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_dcon_ht_saisie             DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie           DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie           DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere la convention.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);

            -- Pour les O.R.
            IF my_dcon_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_CONVENTION
               WHERE conv_ordre=my_conv_ordre 
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette convention n''est pas autorisee pour cette depense (conv_ordre='||my_conv_ordre||')');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dcon_montant_budgetaire) THEN
                my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dcon_montant_budgetaire),0) INTO sum_dcon_montant_budgetaire
               FROM DEPENSE_CTRL_CONVENTION WHERE conv_ordre=my_conv_ordre 
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
               OR dep_id_reversement=my_dep_id_reversement);
               
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dcon_montant_budgetaire+my_dcon_montant_budgetaire<0) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cet engagement pour cette convention (conv_ordre='||my_conv_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dhom_id                   DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                        DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       sum_dhom_montant_budgetaire DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_dhom_ht_saisie             DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_çchat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            -- Pour les O.R.
            IF my_dhom_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE
               WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code nomenclature n''est pas autorise pour cette depense (ce_ordre='||my_ce_ordre||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dhom_montant_budgetaire) THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dhom_montant_budgetaire),0) INTO sum_dhom_montant_budgetaire
              FROM DEPENSE_CTRL_HORS_MARCHE WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dhom_montant_budgetaire+my_dhom_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code nomenclature (ce_ordre='||my_ce_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       sum_dmar_montant_budgetaire DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);

            -- Pour les O.R.
            IF my_dmar_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE
               WHERE att_ordre=my_att_ordre
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                  OR dep_id_reversement=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette attribution n''est pas autorisee pour cette depense (att_ordre='||my_att_ordre||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dmar_montant_budgetaire) THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dmar_montant_budgetaire),0) INTO sum_dmar_montant_budgetaire
              FROM DEPENSE_CTRL_MARCHE WHERE att_ordre=my_att_ordre
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);
                
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dmar_montant_budgetaire+my_dmar_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette attribution (att_ordre='||my_att_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       sum_dpco_montant_budgetaire DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_inventaires    VARCHAR2(30000);
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre,my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            -- Pour les O.R.
            IF my_dpco_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO
               WHERE pco_num=my_pco_num
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette imputation il n''existe pas d''equivalent pour cette depense (pco_num='||my_pco_num||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dpco_montant_budgetaire) THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dpco_montant_budgetaire),0) INTO sum_dpco_montant_budgetaire
              FROM DEPENSE_CTRL_PLANCO WHERE pco_num=my_pco_num
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);
                
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dpco_montant_budgetaire+my_dpco_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette imputation (pco_num='||my_pco_num||')');
            END IF;


            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;


            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, NULL);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_dpco_montant_budgetaire;
		END LOOP;
   END;

END;
/

create procedure grhum.inst_patch_jefydepense_2020 is
begin
	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 214, 'SFBDLIQ', 'SF', 'Créer des dossiers de liquidation', 'Créer des dossiers de liquidation', 'N', 'N', 3 );

	INSERT INTO jefy_admin.PARAMETRE (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) 
   (SELECT jefy_admin.parametre_seq.NEXTVAL, e.exe_ordre, 'SERVICE_FACTURIER_DISPO', 'NON', 'Indique si un service facturier est disponible (OUI/NON). Si OUI, les fonctionnalités de pré-liquidations sont actives et le mandatement classique n''est plus obligatoire. ',1  
                         FROM jefy_admin.exercice e
                      WHERE e.exe_ordre>=to_number(to_char(sysdate,'YYYY'))
                      and not exists (select * from jefy_admin.parametre where par_key='SERVICE_FACTURIER_DISPO' and e.exe_ordre >=to_char(sysdate,'YYYY'))
                      );
                      
   update jefy_depense.db_version set DB_INSTALL_DATE=SYSDATE WHERE DB_VERSION_LIBELLE='2.0.2.0';              
 
end;
/






