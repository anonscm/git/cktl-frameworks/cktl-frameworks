set define off;
-- drop table "GRHUM"."FOURNIS_B2B_CXML_PARAM"  cascade constraints;
-- drop table "JEFY_DEPENSE"."B2B_CXML_ITEM"  cascade constraints;
-- drop table "JEFY_DEPENSE"."B2B_CXML_PUNCHOUTMSG"   cascade constraints;
-- drop table "JEFY_DEPENSE"."B2B_CXML_ORDERREQUEST"    cascade constraints;


  CREATE TABLE "GRHUM"."FOURNIS_B2B_CXML_PARAM" 
   ( "FBCP_ID" NUMBER, 
    "FOU_ORDRE" NUMBER NOT NULL, 
    "FBCP_NAME" VARCHAR2(100) NOT NULL, 
    "FBCP_SUPPLIER_SETUP_URL" VARCHAR2(1000) NOT NULL,
    "FBCP_SUPPLIER_CRED_DOMAIN" VARCHAR2(50) NOT NULL,
    "FBCP_SUPPLIER_CRED_ID" VARCHAR2(50) NOT NULL,
    "FBCP_IDENTITY" VARCHAR2(50) NOT NULL, 
    "FBCP_SHARED_SECRET" VARCHAR2(50) NOT NULL, 
    "FBCP_DEPLOYMENT_MODE" VARCHAR2(50), 
    "FBCP_COMMENTAIRES" VARCHAR2(1000),
    FBCP_DUREE_VALID_PANIER    NUMBER(3,0)   DEFAULT 30 NOT NULL,
    tva_id number not null,
    tyet_id number,
    pers_id_modification number not null, -- reference a l'utilisateur
    date_modification date not null, -- date de creation de l'enregistrement    
     CONSTRAINT "PK_FOURNIS_B2B_CXML_PARAM" PRIMARY KEY ("FBCP_ID")
  USING INDEX TABLESPACE "INDX_GRHUM" 
     
    )  TABLESPACE "DATA_GRHUM" ;
    
  grant select, references on GRHUM."FOURNIS_B2B_CXML_PARAM"  to jefy_depense;

   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_ID" IS 'Identifiant (utilisez la sequence GRHUM.FOURNIS_B2B_CXML_PARAM_SEQ)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FOU_ORDRE" IS 'Reference au fournisseur (table grhum.fournis)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_NAME" IS 'Nom du parametre (sera affiché)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_SUPPLIER_SETUP_URL" IS 'URL d''acces pour la connexion Cxml (fourni par le marchand)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_IDENTITY" IS 'Identifiant sur le site cxml du fournisseur (fourni par le marchand)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_SHARED_SECRET" IS 'Mot de passe sur le site cxml du fournisseur (fourni par le marchand)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_DEPLOYMENT_MODE" IS '(test ou production) suivant si la connexion est une connexion de test (pour tester des commandes)';
 
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_COMMENTAIRES" IS 'Commentaires sur la connexion';
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM"."FBCP_DUREE_VALID_PANIER" IS 'Duree de validite du panier sur le site marchand';
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM".TVA_ID IS 'reference a la tva a appliquer pour les commandes effectuees (jefy_Admin.tva)';
   COMMENT ON COLUMN "GRHUM"."FOURNIS_B2B_CXML_PARAM".TYET_ID IS 'Etat de l''enregistrement (valide etc.) (jefy_Admin.type_etat)';
 
   COMMENT ON TABLE "GRHUM"."FOURNIS_B2B_CXML_PARAM"  IS 'Parametres de connexion aux plateformes e-commerce pour le B2B en CXML';
 

   alter table "GRHUM"."FOURNIS_B2B_CXML_PARAM"  add CONSTRAINT "FK_FBCP_FOU_ORDRE" FOREIGN KEY ("FOU_ORDRE")
      REFERENCES "GRHUM"."FOURNIS_ULR" ("FOU_ORDRE") deferrable initially deferred;

       --grant select, references on jefy_admin.tva to grhum;  
    -- a passer sur jefy_admin si la ligne suivante declenche une erreur...
   alter table "GRHUM"."FOURNIS_B2B_CXML_PARAM"  add CONSTRAINT "FK_FBCP_TVA_ID" FOREIGN KEY ("TVA_ID")
      REFERENCES "JEFY_ADMIN"."TVA" ("TVA_ID") deferrable initially deferred;
   
   
    CREATE TABLE "JEFY_DEPENSE"."B2B_CXML_ITEM" 
   (BCI_ID NUMBER not null, 
    FBCP_ID NUMBER not null, -- reference au GRHUM.FOURNIS_B2B_CXML_PARAM
    ART_ID NUMBER not null, -- reference a JEFY_DEPENSE.ARTICLE
    unit_price number(15,2) not null,
    description varchar2(2000) not null,
    Supplier_Part_ID VARCHAR2(500) NOT NULL,     
    Supplier_Part_Auxiliary_ID VARCHAR2(500) null, --
    Unit_Of_Measure varchar2(50) not null,
    Classification_dom varchar2(50) not null,
    Classification varchar2(50) not null,
    manufacturer_name varchar2(100) null,
    Manufacturer_Part_ID varchar2(500) null,
    url varchar2(2000) null,
    comments varchar2(2000) null,
    pers_id_creation number not null, -- reference a l'utilisateur
    date_creation date not null, -- date de creation de l'enregistrement    
        CONSTRAINT "PK_B2B_CXML_ITEM" PRIMARY KEY ("BCI_ID") USING INDEX TABLESPACE "GFC_INDX"
    
    )  TABLESPACE "GFC" ;
 
    alter table "JEFY_DEPENSE"."B2B_CXML_ITEM" add CONSTRAINT "FK_BCI_ART_ID" FOREIGN KEY ("ART_ID")
      REFERENCES "JEFY_DEPENSE"."ARTICLE" ("ART_ID") deferrable initially deferred;
    
    alter table "JEFY_DEPENSE"."B2B_CXML_ITEM" add CONSTRAINT "FK_BCI_FBCP_ID" FOREIGN KEY ("FBCP_ID")
      REFERENCES GRHUM."FOURNIS_B2B_CXML_PARAM" ("FBCP_ID") deferrable initially deferred;          

      ---------
      
    CREATE TABLE "JEFY_DEPENSE"."B2B_CXML_PUNCHOUTMSG" 
   (BCPO_ID NUMBER not null, 
    FBCP_ID NUMBER not null, -- reference au GRHUM.FOURNIS_B2B_CXML_PARAM
    COMM_ID NUMBER not null, -- reference a JEFY_DEPENSE.COMMANDE
    pers_id_creation number not null, -- reference a l'utilisateur
    date_creation date not null, -- date de creation de l'enregistrement    
    cxml clob not null, -- contenu du msg cxml recu
    
        CONSTRAINT "PK_B2B_CXML_PUNCHOUTMSG" PRIMARY KEY ("BCPO_ID") USING INDEX TABLESPACE "GFC_INDX"
    
    )  TABLESPACE "GFC" ;
    
    
---------
    

    CREATE TABLE "JEFY_DEPENSE"."B2B_CXML_ORDERREQUEST" 
   (BCOR_ID NUMBER not null, 
    FBCP_ID NUMBER not null, -- reference au GRHUM.FOURNIS_B2B_CXML_PARAM
    COMM_ID NUMBER not null, -- reference a JEFY_DEPENSE.COMMANDE
    pers_id_creation number not null, -- reference a l'utilisateur
    date_creation date not null, -- date de creation de l'enregistrement    
    cxml clob not null, -- contenu de la request cxml envoyee
    CXML_REPONSE clob null,
    
        CONSTRAINT "PK_B2B_CXML_ORDERREQUEST" PRIMARY KEY ("BCOR_ID") USING INDEX TABLESPACE "GFC_INDX"
    
    )  TABLESPACE "GFC" ;
 
    alter table "JEFY_DEPENSE"."B2B_CXML_ORDERREQUEST" add CONSTRAINT "FK_BCOR_COMM_ID" FOREIGN KEY ("COMM_ID")
      REFERENCES "JEFY_DEPENSE"."COMMANDE" ("COMM_ID") deferrable initially deferred;      
 
    alter table "JEFY_DEPENSE"."B2B_CXML_ORDERREQUEST" add CONSTRAINT "FK_BCOR_FBCP_ID" FOREIGN KEY ("FBCP_ID")
      REFERENCES GRHUM."FOURNIS_B2B_CXML_PARAM" ("FBCP_ID") deferrable initially deferred;      
      
      ---------
      
CREATE SEQUENCE  "GRHUM"."FOURNIS_B2B_CXML_PARAM_SEQ"  MINVALUE 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  "JEFY_DEPENSE"."B2B_CXML_ITEM_SEQ"  MINVALUE 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  "JEFY_DEPENSE"."B2B_CXML_PUNCHOUTMSG_SEQ"  MINVALUE 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  "JEFY_DEPENSE"."B2B_CXML_ORDERREQUEST_SEQ"  MINVALUE 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;      
      
   
    
    
    