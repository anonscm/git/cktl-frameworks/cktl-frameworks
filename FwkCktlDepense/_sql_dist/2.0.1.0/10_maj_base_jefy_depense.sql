connect grhum/<mot de passe>;

begin
 JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 212, 'B2BCAT', 'B2B', 'Accès au site web marchand pour créer un panier et le rapatrier dans Carambole', 'Accéder au site web marchand pour créer un panier', 'N', 'N', 3 );
 JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 213, 'B2BCOM', 'B2B', 'Envoyer la commande directement au marchand via B2B à partir de Carambole', 'Envoyer la commande vers le site web marchand', 'N', 'N', 3 );
 commit;
end;




insert into jefy_depense.db_version values (2010,'2010',to_date('07/04/2010','dd/mm/yyyy'),sysdate,null);

commit;


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2
   ) IS
      my_depense       depense_budget%rowtype;
      my_engage        engage_budget%rowtype;
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id;
     select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
    
     if my_engage.tyap_id<>get_type_application then
       raise_application_error(-20001, 'on ne peut reimputer que des liquidations reliees a une commande');
     end if;
     
     rempli_reimputation(a_dep_id, a_reim_libelle, a_utl_ordre);
   
     if a_org_id is not null and my_engage.org_id<>a_org_id then
       depense_organ(a_dep_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_chaine_analytique, a_chaine_convention);
     else
       if a_chaine_analytique is not null then 
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
       end if;
       if a_chaine_convention is not null then 
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
       end if;
       if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
       end if;
       if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
       end if;
     end if;
     
     if a_chaine_action is not null then 
       depense_action(a_dep_id, a_chaine_action, a_utl_ordre);
     end if;

     if a_chaine_hors_marche is not null then 
       depense_hors_marche(a_dep_id, a_chaine_hors_marche, a_utl_ordre);
     end if;

     if a_chaine_marche is not null then 
       depense_marche(a_dep_id, a_chaine_marche, a_utl_ordre);
     end if;

     if a_pco_num is not null then 
       depense_planco(a_dep_id, a_pco_num, a_utl_ordre);
     end if;
   END;
   
PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2
   ) IS
      my_nb        integer;
      my_eng_id    engage_budget.eng_id%type;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_tap_id    engage_budget.tap_id%type;
   BEGIN
--- changement de la ligne budgétaire
--    * dépense sans inventaire : si la dépense est mandatée, on choisit une ligne budgétaire de la même UB, sinon toutes les lignes, dans la limite que le disponible le permette.
--    * dépense avec inventaire : suivant un paramètre a mettre en place, même limitation que la dépense sans inventaire ou restriction au niveau du CR (puisque le code inventaire prend en compte le CR)
--    * le changement de ligne peut impliquer la modification des conventions et des codes analytiques éventuellement associés à cette dépense    cf. changement convention
--    * le changement de ligne peut impliquer une création d'un nouvel engagement, si l'engagement concerne d'autres liquidations (hors ORVs liés a celle qu'on ré-impute) par exemple
--    * si la dépense ré-imputée possède un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..   

      select * into my_depense from depense_budget where dep_id=a_dep_id; 
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;

      -- si la ligne budgetaire a change
      if my_engage.org_id<>a_org_id then
        my_tcd_ordre:=a_tcd_ordre;
        if my_tcd_ordre is null then my_tcd_ordre:=my_engage.tcd_ordre; end if;

        my_tap_id:=a_tap_id;
        if my_tap_id is null then my_tap_id:=my_depense.tap_id; end if;
        
        verifier.verifier_budget(my_depense.exe_ordre, my_tap_id, a_org_id, my_tcd_ordre);
        verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, a_org_id);

        -- si avec inventaire -> CR
        select count(*) into my_nb from v_inventaire where dpco_id in (select dpco_id from depense_ctrl_planco where dep_id in (a_dep_id));
        if my_nb>0 then
          select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
            and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub and dest.org_cr=orig.org_cr;
          if my_nb=0 then
            raise_application_error(-20001, 'cette depense comporte des inventaires, on ne peut reimputer que dans le meme CR');
          end if;
        else
          -- la depense est mandatee -> restriction de la nouvelle ligne a la meme UB que l'ancienne
          select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
          if my_nb>0 then
            select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
              and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub;
            if my_nb=0 then
              raise_application_error(-20001, 'cette depense est mandatee, on ne peut reimputer que dans la meme UB');
            end if;
          end if;
        end if;
        
        -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
        my_eng_id:=subdivise_engage_pour_depense(a_dep_id, a_org_id, my_tcd_ordre, my_tap_id, a_utl_ordre);
        
      end if;
      
      if a_chaine_analytique is not null then
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
      end if;
      
      if a_chaine_convention is not null then
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
      end if;

      if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
      end if;

      if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
      end if;
   END;

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
   BEGIN
     RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer le type de credit de la depense');
   END;

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_eng_id    engage_budget.eng_id%type;
      my_par_value parametre.par_value%type;
      
      my_dep_id    depense_budget.dep_id%type;
      cursor orvs is select dep_id from depense_budget where dep_id_reversement=a_dep_id;
   BEGIN
      select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
      if my_nb>0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier le taux de prorata d''une depense mandatee');
      end if;
      
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      my_eng_id:=my_engage.eng_id;

      verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, my_engage.org_id);

      -- on ne peut reimputer que les depenses pas les ORvs pour le moment
        -- les orvs rattaches a cette depense seront mis a jour avec les memes infos
      if my_depense.dep_ttc_saisie<0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas reimputer un ORV, il faut reimputer la depense d''origine');
      end if;
            
      -- on verifie si le nouveau taux de prorata est autorise pour cette ligne budgetaire --
      verifier.verifier_budget(my_depense.exe_ordre, a_tap_id, my_engage.org_id, my_engage.tcd_ordre);
      
      select par_value into my_par_value from parametre where par_key='DEPENSE_IDEM_TAP_ID' and exe_ordre=my_depense.exe_ordre;
      if my_par_value='OUI' then
      
         -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
         my_eng_id:=subdivise_engage_pour_depense(a_dep_id, my_engage.org_id, my_engage.tcd_ordre, a_tap_id, a_utl_ordre);
         
      end if;
      
      update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=a_dep_id;
      corrige_depense_budgetaire(a_dep_id);
      
      open orvs();
      loop
         fetch  orvs into my_dep_id;
         exit when orvs%notfound;
         
         update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=my_dep_id;
         corrige_depense_budgetaire(my_dep_id);
      end loop;
      close orvs;
      
      budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
      
   END;

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_action where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_action(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_analytique where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_analytique(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_convention where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_convention(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est rattachee a une attribution on ne peut pas la transformer en hors marche');
       end if;

       select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
       delete from depense_ctrl_hors_marche where dep_id=a_dep_id;
       liquider.ins_depense_ctrl_hors_marche(my_exe_ordre, a_dep_id, a_chaine);
       Verifier.verifier_depense_coherence(a_dep_id);
     end if;     
   END;

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_att_ordre depense_ctrl_marche.att_ordre%type;
      my_old_att_ordre depense_ctrl_marche.att_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est hors marche on ne peut pas la transformer en marche');
       end if;

       SELECT grhum.en_nombre(SUBSTR(a_chaine,1,INSTR(a_chaine,'$')-1)) INTO my_att_ordre FROM dual;
       select nvl(att_ordre,0) into my_old_att_ordre from depense_ctrl_marche where dep_id=a_dep_id;
       
       if my_old_att_ordre<>my_att_ordre then 
          RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer l''attribution de la depense');
       end if;
     end if;
   END;

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type
  ) is
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_org_id    engage_budget.org_id%type;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_pco_num   depense_ctrl_planco.pco_num%type;
  begin 
      select nvl(pco_num,'0') into my_pco_num from depense_ctrl_planco where dep_id=a_dep_id;
      if my_pco_num<>a_pco_num then 
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         update depense_ctrl_planco set pco_num=a_pco_num where dep_id=a_dep_id;
      end if;

      select d.exe_ordre, e.org_id, e.tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre 
        from depense_budget d, engage_budget e where d.eng_id=e.eng_id and d.dep_id=a_dep_id;
      Verifier.verifier_planco(my_exe_ordre, my_org_id, my_tcd_ordre, a_pco_num, a_utl_ordre);
      Verifier.verifier_depense_coherence(a_dep_id);
  end;

FUNCTION subdivise_engage_pour_depense(
      a_dep_id       depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
      my_nb          integer;
      bool_creation  integer;
      my_comm_id     commande.comm_id%type;
      my_cbud_id     commande_budget.cbud_id%type;
      my_old_commande_budget commande_budget%rowtype;
      my_commande_budget commande_budget%rowtype;
      my_ht          engage_budget.eng_ht_saisie%type;
      my_tva         engage_budget.eng_tva_saisie%type;
      my_ttc         engage_budget.eng_ttc_saisie%type;
      my_bud         engage_budget.eng_montant_budgetaire%type;
      my_depense     depense_budget%rowtype;
      my_engage      engage_budget%rowtype;
      my_new_engage  engage_budget%rowtype;
      my_eng_id      engage_budget.eng_id%type;
   BEGIN
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      my_eng_id:=my_depense.eng_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      
      -- si il n'y a pas d'autre depense sur cette engagement on n'en crée pas de nouveau 
      select count(*) into bool_creation from depense_budget where eng_id=my_depense.eng_id and dep_id<>a_dep_id and dep_ttc_saisie>0;
      
      -- on regarde si l'engagement appartient a une commande
      my_comm_id:=null;
      select count(*) into my_nb from commande_engagement where eng_id=my_depense.eng_id;
      if my_nb>0 then
         select min(comm_id) into my_comm_id from commande_engagement where eng_id=my_depense.eng_id;
      end if;
   
      if bool_creation=0 then
         -- on modifie juste l'engagement et enventuellement les infos de la commande
   
         if my_comm_id is not null then 
            select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
            select count(*) into my_nb from commande_budget 
              where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
            if my_nb>0 then  
               select min(cbud_id) into my_cbud_id from commande_budget 
                 where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
               update commande_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id where cbud_id=my_cbud_id;
            
               --si le taux de prorata a change, on met a jour les montants budgetaires  
               if my_engage.tap_id<>a_tap_id then
                  update commande_budget set cbud_montant_budgetaire=Budget.calculer_budgetaire(my_engage.exe_ordre,a_tap_id,a_org_id, cbud_ht_saisie, cbud_ttc_saisie)
                    where cbud_id=my_cbud_id;
                  corriger.corriger_commande_ctrl(my_cbud_id);
                  verifier.verifier_cde_budget_coherence(my_cbud_id);
               end if;
            end if;
         end if;
         
         -- on log l'ancien engagement
         engager.log_engage(my_eng_id, a_utl_ordre);
                  
         -- modif engagement
         update engage_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id, utl_ordre=a_utl_ordre where eng_id=my_eng_id;

         if my_engage.tap_id<>a_tap_id then
            -- modification des montants budgetaires   
            corrige_engage_budgetaire(my_eng_id);         
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre then 
            -- si on a changé de ligne budgetaire on met a jour l'"ancien" budget
            budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre or my_engage.tap_id<>a_tap_id then
            -- si le taux de prorata ou la ligne budgetaire a change on met a jour le budget
            budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);
         end if;
         
      else

        my_nb:=0;
      
        -- si l'engagement appartient a une commande on regarde si un engagement avec les nouvelles infos existe deja 
        if my_comm_id is not null then
           select count(*) into my_nb from commande_engagement ce, engage_budget e
             where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
           if my_nb>0 then
             select min(e.eng_id) into my_eng_id from commande_engagement ce, engage_budget e
               where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
             
             -- on augmente les montants de l'engagement avec les montants de la depense
             augmenter_engage(a_dep_id, my_eng_id);
             
             -- on augmente la commande_bugdet correspondante (normalement elle existe)
             select * into my_new_engage from engage_budget where eng_id=my_eng_id;
             select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
             if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
                update commande_budget set cbud_montant_budgetaire=my_new_engage.eng_montant_budgetaire, cbud_ht_saisie=my_new_engage.eng_ht_saisie,
                   cbud_tva_saisie=my_new_engage.eng_tva_saisie, cbud_ttc_saisie=my_new_engage.eng_ttc_saisie where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
             end if;

           else
             -- on cree le nouvel engagement avec les infos de la depense
             my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);
             insert into commande_engagement select commande_engagement_seq.nextval, my_comm_id, my_eng_id from dual;
             
             -- on cree la commande_budget correspondante
             my_cbud_id:=creer_commande_budget(my_eng_id, my_comm_id);
           end if;
           
           -- on diminue l'ancienne commande_budget
           select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
           if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id 
                   and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
                   
                select * into my_new_engage from engage_budget where eng_id=my_eng_id;
                select * into my_commande_budget from commande_budget where cbud_id=my_cbud_id;
                
                my_ht :=my_commande_budget.cbud_ht_saisie -my_new_engage.eng_ht_saisie;
                if my_ht<0 then my_ht:=0; end if;
                my_tva:=my_commande_budget.cbud_tva_saisie-my_new_engage.eng_tva_saisie;
                if my_tva<0 then my_tva:=0; end if;
                my_ttc:=my_commande_budget.cbud_ttc_saisie-my_new_engage.eng_ttc_saisie;
                if my_ttc<0 then my_ttc:=0; end if;
                my_bud:=my_commande_budget.cbud_montant_budgetaire-my_new_engage.eng_montant_budgetaire;
                if my_bud<0 then my_bud:=0; end if;
                
                update commande_budget set cbud_montant_budgetaire=my_bud, cbud_ht_saisie=my_ht,
                   cbud_tva_saisie=my_tva, cbud_ttc_saisie=my_ttc where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
           end if; 

        else
           my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);       
        end if;

        -- on diminue l'ancien engagement du montant de la depense a reimputer
        -- ATTENTION : on ne diminue pas le budgetaire reste car on enleve la liquidation de cet engagement, et donc le reste ne change pas 
        select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
        my_ht :=my_engage.eng_ht_saisie -my_depense.dep_ht_saisie;
        if my_ht<0 then my_ht:=0; end if;
        my_tva:=my_engage.eng_tva_saisie-my_depense.dep_tva_saisie;
        if my_tva<0 then my_tva:=0; end if;
        my_ttc:=my_engage.eng_ttc_saisie-my_depense.dep_ttc_saisie;
        if my_ttc<0 then my_ttc:=0; end if;
        my_bud:=my_engage.eng_montant_budgetaire-my_depense.dep_montant_budgetaire;
        if my_bud<0 then my_bud:=0; end if;
        if my_bud<my_engage.eng_montant_budgetaire_reste then my_bud:=my_engage.eng_montant_budgetaire_reste; end if;
  
        update engage_budget set eng_ht_saisie=my_ht, eng_tva_saisie=my_tva, eng_ttc_saisie=my_ttc, eng_montant_budgetaire=my_bud
          where eng_id=my_engage.eng_id;
        corrige_engage_repartition(my_engage.eng_id);
        
        -- on associe la depense au nouvel engagement
        update depense_budget set eng_id=my_eng_id where dep_id=a_dep_id;
                     
        -- on lance la correction du budget
        budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
        budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);

      end if;
      
      return my_eng_id;
   END;
   
FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
     my_eng_id      engage_budget.eng_id%type;
     my_depense     depense_budget%rowtype;      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
   
     select engage_budget_seq.nextval into my_eng_id from dual;
     insert into engage_budget values (my_eng_id, my_depense.exe_ordre, Get_Numerotation(my_depense.exe_ordre, NULL, null,'ENGAGE_BUDGET'),
       a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, 0, 0, 0, 0, 0, sysdate, a_tyap_id, a_utl_ordre);
     
     augmenter_engage(a_dep_id,my_eng_id);
          
     return my_eng_id;
   END;
      
PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type)
   IS
     my_nb          integer;
     my_id          engage_ctrl_action.eact_id%type;
     my_engage      engage_budget%rowtype;
     my_depense     depense_budget%rowtype;
      
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     update engage_budget set eng_montant_budgetaire=eng_montant_budgetaire+my_depense.dep_montant_budgetaire, eng_ht_saisie=eng_ht_saisie+my_depense.dep_ht_saisie, 
       eng_tva_saisie=eng_tva_saisie+my_depense.dep_tva_saisie, eng_ttc_saisie=eng_ttc_saisie+my_depense.dep_ttc_saisie where eng_id=a_eng_id;
     
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
          select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id; 
          if my_nb=0 then 
             insert into engage_ctrl_action select engage_ctrl_action_seq.nextval, row_action.exe_ordre, a_eng_id, row_action.tyac_id,
                row_action.dact_montant_budgetaire, 0, row_action.dact_ht_saisie, row_action.dact_tva_saisie, row_action.dact_ttc_saisie, sysdate from dual;
          else
             select min(eact_id) into my_id from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id;
             update engage_ctrl_action set eact_montant_budgetaire=eact_montant_budgetaire+row_action.dact_montant_budgetaire,
               eact_ht_saisie=eact_ht_saisie+row_action.dact_ht_saisie, eact_tva_saisie=eact_tva_saisie+row_action.dact_tva_saisie,
               eact_ttc_saisie=eact_ttc_saisie+row_action.dact_ttc_saisie where eact_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
          select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id; 
          if my_nb=0 then 
            insert into engage_ctrl_analytique select engage_ctrl_analytique_seq.nextval, row_analytique.exe_ordre, a_eng_id, row_analytique.can_id,
               row_analytique.dana_montant_budgetaire, 0, row_analytique.dana_ht_saisie, row_analytique.dana_tva_saisie, row_analytique.dana_ttc_saisie, 
               sysdate from dual;
          else
             select min(eana_id) into my_id from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id;
             update engage_ctrl_analytique set eana_montant_budgetaire=eana_montant_budgetaire+row_analytique.dana_montant_budgetaire,
               eana_ht_saisie=eana_ht_saisie+row_analytique.dana_ht_saisie, eana_tva_saisie=eana_tva_saisie+row_analytique.dana_tva_saisie,
               eana_ttc_saisie=eana_ttc_saisie+row_analytique.dana_ttc_saisie where eana_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
          select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_convention select engage_ctrl_convention_seq.nextval, row_convention.exe_ordre, a_eng_id, row_convention.conv_ordre,
                row_convention.dcon_montant_budgetaire, 0, row_convention.dcon_ht_saisie, row_convention.dcon_tva_saisie, row_convention.dcon_ttc_saisie, 
                sysdate from dual;
          else
             select min(econ_id) into my_id from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre;
             update engage_ctrl_convention set econ_montant_budgetaire=econ_montant_budgetaire+row_convention.dcon_montant_budgetaire,
               econ_ht_saisie=econ_ht_saisie+row_convention.dcon_ht_saisie, econ_tva_saisie=econ_tva_saisie+row_convention.dcon_tva_saisie,
               econ_ttc_saisie=econ_ttc_saisie+row_convention.dcon_ttc_saisie where econ_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
          select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id; 
          if my_nb=0 then 
             insert into engage_ctrl_hors_marche select engage_ctrl_hors_marche_seq.nextval, row_hors_marche.exe_ordre, a_eng_id, row_hors_marche.typa_id, 
                row_hors_marche.ce_ordre, row_hors_marche.dhom_montant_budgetaire, 0,0, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_tva_saisie, 
                row_hors_marche.dhom_ttc_saisie, sysdate from dual;
          else
             select min(ehom_id) into my_id from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id;
             update engage_ctrl_hors_marche set ehom_montant_budgetaire=ehom_montant_budgetaire+row_hors_marche.dhom_montant_budgetaire,
               ehom_ht_saisie=ehom_ht_saisie+row_hors_marche.dhom_ht_saisie, ehom_tva_saisie=ehom_tva_saisie+row_hors_marche.dhom_tva_saisie,
               ehom_ttc_saisie=ehom_ttc_saisie+row_hors_marche.dhom_ttc_saisie where ehom_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
          select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_marche select engage_ctrl_marche_seq.nextval, row_marche.exe_ordre, a_eng_id, row_marche.att_ordre,
                row_marche.dmar_montant_budgetaire, 0, 0, row_marche.dmar_ht_saisie, row_marche.dmar_tva_saisie, row_marche.dmar_ttc_saisie, sysdate from dual;
          else
             select min(emar_id) into my_id from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre;
             update engage_ctrl_marche set emar_montant_budgetaire=emar_montant_budgetaire+row_marche.dmar_montant_budgetaire,
               emar_ht_saisie=emar_ht_saisie+row_marche.dmar_ht_saisie, emar_tva_saisie=emar_tva_saisie+row_marche.dmar_tva_saisie,
               emar_ttc_saisie=emar_ttc_saisie+row_marche.dmar_ttc_saisie where emar_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then
       for row_planco in planco
       loop
          select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num; 
          if my_nb=0 then 
             insert into engage_ctrl_planco select engage_ctrl_planco_seq.nextval, row_planco.exe_ordre, a_eng_id, row_planco.pco_num,
                row_planco.dpco_montant_budgetaire, 0, row_planco.dpco_ht_saisie, row_planco.dpco_tva_saisie, row_planco.dpco_ttc_saisie, sysdate from dual;
          else
             select min(epco_id) into my_id from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num;
             update engage_ctrl_planco set epco_montant_budgetaire=epco_montant_budgetaire+row_planco.dpco_montant_budgetaire,
               epco_ht_saisie=epco_ht_saisie+row_planco.dpco_ht_saisie, epco_tva_saisie=epco_tva_saisie+row_planco.dpco_tva_saisie,
               epco_ttc_saisie=epco_ttc_saisie+row_planco.dpco_ttc_saisie where epco_id=my_id;
          end if;
       end loop;
     end if;

     verifier.VERIFIER_ENGAGE_COHERENCE(a_eng_id);
   END;

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type) return number
   IS
     my_cbud_id       commande_budget.cbud_id%type;
     my_engage        engage_budget%rowtype;
     
     my_nb integer;
     my_pourcentage number(15,5);
     my_total_pourcentage number(15,5);
     
     my_chaine_action varchar2(3000);
     my_chaine_analytique varchar2(3000);
     my_chaine_convention varchar2(3000);
     my_chaine_hors_marche varchar2(3000);
     my_chaine_marche varchar2(3000);
     my_chaine_planco varchar2(3000);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;

   BEGIN
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     -- on definit les infos, notamment les pourcentages
     my_chaine_action:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_action.eact_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_action:=my_chaine_action||row_action.tyac_id||'$'||row_action.eact_ht_saisie||'$'||row_action.eact_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_action:=my_chaine_action||'$';

     my_chaine_analytique:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         my_pourcentage:=round(100*row_analytique.eana_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_analytique:=my_chaine_analytique||row_analytique.can_id||'$'||row_analytique.eana_ht_saisie||'$'
               ||row_analytique.eana_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_analytique:=my_chaine_analytique||'$';

     my_chaine_convention:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         my_pourcentage:=round(100*row_convention.econ_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_convention:=my_chaine_convention||row_convention.conv_ordre||'$'||row_convention.econ_ht_saisie||'$'
              ||row_convention.econ_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_convention:=my_chaine_convention||'$';

     my_chaine_hors_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
           my_chaine_hors_marche:=my_chaine_hors_marche||row_hors_marche.typa_id||'$'||row_hors_marche.ce_ordre||'$'
              ||row_hors_marche.ehom_ht_saisie||'$'||row_hors_marche.ehom_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_hors_marche:=my_chaine_hors_marche||'$';

     my_chaine_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
           my_chaine_marche:=my_chaine_marche||row_marche.att_ordre||'$'||row_marche.emar_ht_saisie||'$'||row_marche.emar_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_marche:=my_chaine_marche||'$';

     my_chaine_planco:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_planco.epco_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_planco:=my_chaine_planco||row_planco.pco_num||'$'||row_planco.epco_ht_saisie||'$'||row_planco.epco_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_planco:=my_chaine_planco||'$';
     
     -- on cree
     select commande_budget_seq.nextval into my_cbud_id from dual;
     commander.ins_commande_budget (my_cbud_id, a_comm_id, my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre, my_engage.tap_id, my_engage.eng_ht_saisie,
       my_engage.eng_ttc_saisie, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche, my_chaine_marche, my_chaine_planco);
               
     -- on corrige pour calculer les montants des repartitions
     corriger.corriger_commande_ctrl(my_cbud_id);
     verifier.verifier_cde_budget_coherence(my_cbud_id);
     
     
     return my_cbud_id;
   END;

PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;
     my_eng_montant_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_reste engage_budget.eng_montant_budgetaire%type;
     my_ctrl_bud engage_budget.eng_montant_budgetaire%type;
     my_dep_budgetaire depense_budget.dep_montant_budgetaire%type;
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant engage_budget
     select * into my_engage from engage_budget where eng_id=a_eng_id;
    
     my_eng_montant_budgetaire:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id,
         my_engage.org_id, my_engage.eng_ht_saisie, my_engage.eng_ttc_saisie);
     
     select nvl(SUM(Budget.calculer_budgetaire(exe_ordre, my_engage.tap_id, my_engage.org_id, dep_ht_saisie, dep_ttc_saisie)),0) 
        into my_dep_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
     my_reste:=my_eng_montant_budgetaire-my_dep_budgetaire;
     if my_reste<0 then my_reste:=0; end if;
     
     update engage_budget set eng_montant_budgetaire=my_eng_montant_budgetaire, eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;

     -- maj montant engage_ctrl_action
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update engage_ctrl_action set eact_montant_budgetaire=my_reste, eact_montant_budgetaire_reste=my_reste  where eact_id=row_action.eact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_action.eact_ht_saisie, row_action.eact_ttc_saisie);

            update engage_ctrl_action set eact_montant_budgetaire=my_ctrl_bud, eact_montant_budgetaire_reste=my_ctrl_bud where eact_id=row_action.eact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_analytique
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update engage_ctrl_analytique set eana_montant_budgetaire=my_reste, eana_montant_budgetaire_reste=my_reste  where eana_id=row_analytique.eana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_analytique.eana_ht_saisie, row_analytique.eana_ttc_saisie);

            update engage_ctrl_analytique set eana_montant_budgetaire=my_ctrl_bud, eana_montant_budgetaire_reste=my_ctrl_bud where eana_id=row_analytique.eana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(a_eng_id);
     end if;

     -- maj montant engage_ctrl_convention
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update engage_ctrl_convention set econ_montant_budgetaire=my_reste, econ_montant_budgetaire_reste=my_reste  where econ_id=row_convention.econ_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_convention.econ_ht_saisie, row_convention.econ_ttc_saisie);

            update engage_ctrl_convention set econ_montant_budgetaire=my_ctrl_bud, econ_montant_budgetaire_reste=my_ctrl_bud where econ_id=row_convention.econ_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(a_eng_id);
     end if;

     -- maj montant engage_ctrl_hors_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste, ehom_montant_budgetaire_reste=my_reste  where ehom_id=row_hors_marche.ehom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_hors_marche.ehom_ht_saisie, row_hors_marche.ehom_ttc_saisie);

            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_ctrl_bud, ehom_montant_budgetaire_reste=my_ctrl_bud where ehom_id=row_hors_marche.ehom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(a_eng_id);
     end if;

     -- maj montant engage_ctrl_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update engage_ctrl_marche set emar_montant_budgetaire=my_reste, emar_montant_budgetaire_reste=my_reste  where emar_id=row_marche.emar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_marche.emar_ht_saisie, row_marche.emar_ttc_saisie);

            update engage_ctrl_marche set emar_montant_budgetaire=my_ctrl_bud, emar_montant_budgetaire_reste=my_ctrl_bud where emar_id=row_marche.emar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update engage_ctrl_planco set epco_montant_budgetaire=my_reste, epco_montant_budgetaire_reste=my_reste  where epco_id=row_planco.epco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_planco.epco_ht_saisie, row_planco.epco_ttc_saisie);

            update engage_ctrl_planco set epco_montant_budgetaire=my_ctrl_bud, epco_montant_budgetaire_reste=my_ctrl_bud where epco_id=row_planco.epco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(a_eng_id);
     end if;

     verifier.verifier_engage_coherence(a_eng_id);
   END;

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;

     my_montant_ht_total engage_budget.eng_ht_saisie%type;
     my_montant_ttc_total engage_budget.eng_ttc_saisie%type;
     my_montant_budgetaire_total engage_budget.eng_montant_budgetaire%type;
     
     my_ht engage_budget.eng_ht_saisie%type;
     my_ttc engage_budget.eng_ttc_saisie%type;
     my_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_pourcentage number (12,8);
          
     my_reste_ht engage_budget.eng_ht_saisie%type;
     my_reste_ttc engage_budget.eng_ttc_saisie%type;
     my_reste_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_total_pourcentage number (12,8);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
     my_nb_decimales        NUMBER;
   BEGIN

       -- infos references
       select * into my_engage from engage_budget where eng_id=a_eng_id;
       my_nb_decimales:=liquider_outils.get_nb_decimales(my_engage.exe_ordre);
        
       select sum(eact_ht_saisie), sum(eact_montant_budgetaire), sum(eact_ttc_saisie)
          into my_montant_ht_total, my_montant_budgetaire_total, my_montant_ttc_total from engage_ctrl_action where eng_id=a_eng_id;
   
   
       -- maj montant engage_ctrl_action       
       select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_action in action
         loop
           if action%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_action set eact_montant_budgetaire=my_reste_budgetaire, eact_ht_saisie=my_reste_ht,
                 eact_ttc_saisie=my_reste_ttc,  eact_tva_saisie=my_reste_ttc-my_reste_ht where eact_id=row_action.eact_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_action.eact_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_action.eact_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_action.eact_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_action set eact_montant_budgetaire=my_budgetaire, eact_ht_saisie=my_ht,
                 eact_ttc_saisie=my_ttc, eact_tva_saisie=my_ttc-my_ht where eact_id=row_action.eact_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_analytique       
       select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(eana_ht_saisie) into my_total_pourcentage from engage_ctrl_analytique where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_analytique in analytique
         loop
           if analytique%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_analytique set eana_montant_budgetaire=my_reste_budgetaire, eana_ht_saisie=my_reste_ht,
                 eana_ttc_saisie=my_reste_ttc,  eana_tva_saisie=my_reste_ttc-my_reste_ht where eana_id=row_analytique.eana_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_analytique.eana_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_analytique.eana_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_analytique.eana_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_analytique set eana_montant_budgetaire=my_budgetaire, eana_ht_saisie=my_ht,
                 eana_ttc_saisie=my_ttc, eana_tva_saisie=my_ttc-my_ht where eana_id=row_analytique.eana_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_convention
       select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(econ_ht_saisie) into my_total_pourcentage from engage_ctrl_convention where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_convention in convention
         loop
           if convention%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_convention set econ_montant_budgetaire=my_reste_budgetaire, econ_ht_saisie=my_reste_ht,
                 econ_ttc_saisie=my_reste_ttc,  econ_tva_saisie=my_reste_ttc-my_reste_ht where econ_id=row_convention.econ_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_convention.econ_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_convention.econ_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_convention.econ_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_convention set econ_montant_budgetaire=my_budgetaire, econ_ht_saisie=my_ht,
                 econ_ttc_saisie=my_ttc, econ_tva_saisie=my_ttc-my_ht where econ_id=row_convention.econ_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_hors_marche       
       select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;

         for row_hors_marche in hors_marche
         loop
           if hors_marche%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste_budgetaire, ehom_ht_saisie=my_reste_ht,
                 ehom_ttc_saisie=my_reste_ttc,  ehom_tva_saisie=my_reste_ttc-my_reste_ht where ehom_id=row_hors_marche.ehom_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_hors_marche.ehom_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_hors_marche.ehom_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_hors_marche.ehom_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_budgetaire, ehom_ht_saisie=my_ht,
                 ehom_ttc_saisie=my_ttc, ehom_tva_saisie=my_ttc-my_ht where ehom_id=row_hors_marche.ehom_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_marche       
       select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;

         update engage_ctrl_marche set emar_montant_budgetaire=my_reste_budgetaire, emar_ht_saisie=my_reste_ht,
              emar_ttc_saisie=my_reste_ttc,  emar_tva_saisie=my_reste_ttc-my_reste_ht where eng_id=a_eng_id;
       end if;   

       -- maj montant engage_ctrl_planco
       select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_planco in planco
         loop
           if planco%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_planco set epco_montant_budgetaire=my_reste_budgetaire, epco_ht_saisie=my_reste_ht,
                 epco_ttc_saisie=my_reste_ttc,  epco_tva_saisie=my_reste_ttc-my_reste_ht where epco_id=row_planco.epco_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_planco.epco_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_planco.epco_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_planco.epco_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_planco set epco_montant_budgetaire=my_budgetaire, epco_ht_saisie=my_ht,
                 epco_ttc_saisie=my_ttc, epco_tva_saisie=my_ttc-my_ht where epco_id=row_planco.epco_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       --corrige_engage_budgetaire();
       verifier.verifier_engage_coherence(a_eng_id);
   END;
   
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type
   ) 
   IS
     my_depense depense_budget%rowtype;
     my_dep_montant_budgetaire depense_budget.dep_montant_budgetaire%type;
     my_reste depense_budget.dep_montant_budgetaire%type;
     my_ctrl_bud depense_budget.dep_montant_budgetaire%type;
     
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant depense_budget
     select * into my_depense from depense_budget where dep_id=a_dep_id;
    
     my_dep_montant_budgetaire:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, my_depense.dep_ht_saisie, my_depense.dep_ttc_saisie);
     update depense_budget set dep_montant_budgetaire=my_dep_montant_budgetaire where dep_id=a_dep_id;

     -- maj montant depense_ctrl_action
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update depense_ctrl_action set dact_montant_budgetaire=my_reste where dact_id=row_action.dact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_action.dact_ht_saisie, row_action.dact_ttc_saisie);

            update depense_ctrl_action set dact_montant_budgetaire=my_ctrl_bud where dact_id=row_action.dact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(my_depense.eng_id);
     end if;
     
     -- maj montant depense_ctrl_analytique
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update depense_ctrl_analytique set dana_montant_budgetaire=my_reste where dana_id=row_analytique.dana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_analytique.dana_ht_saisie, row_analytique.dana_ttc_saisie);

            update depense_ctrl_analytique set dana_montant_budgetaire=my_ctrl_bud where dana_id=row_analytique.dana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_convention
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update depense_ctrl_convention set dcon_montant_budgetaire=my_reste where dcon_id=row_convention.dcon_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_convention.dcon_ht_saisie, row_convention.dcon_ttc_saisie);

            update depense_ctrl_convention set dcon_montant_budgetaire=my_ctrl_bud where dcon_id=row_convention.dcon_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_hors_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_reste where dhom_id=row_hors_marche.dhom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_ttc_saisie);

            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_ctrl_bud where dhom_id=row_hors_marche.dhom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update depense_ctrl_marche set dmar_montant_budgetaire=my_reste where dmar_id=row_marche.dmar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_marche.dmar_ht_saisie, row_marche.dmar_ttc_saisie);

            update depense_ctrl_marche set dmar_montant_budgetaire=my_ctrl_bud where dmar_id=row_marche.dmar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(my_depense.eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update depense_ctrl_planco set dpco_montant_budgetaire=my_reste where dpco_id=row_planco.dpco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_planco.dpco_ht_saisie, row_planco.dpco_ttc_saisie);

            update depense_ctrl_planco set dpco_montant_budgetaire=my_ctrl_bud where dpco_id=row_planco.dpco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(my_depense.eng_id);
     end if;

     verifier.verifier_depense_coherence(a_dep_id);
   END;
   
PROCEDURE rempli_reimputation(
      a_dep_id       depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre    depense_budget.utl_ordre%type
   )
   IS
     my_nb          integer;
     my_reim_id     reimputation.reim_id%type;
     my_exe_ordre   depense_budget.exe_ordre%type;
     my_reim_numero reimputation.reim_numero%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     my_reim_numero := Get_Numerotation(my_exe_ordre, NULL, null, 'REIMPUTATION');

     select reimputation_seq.nextval into my_reim_id from dual;
     insert into reimputation values (my_reim_id, my_exe_ordre, my_reim_numero, a_dep_id, a_reim_libelle, a_utl_ordre, sysdate);
     
     insert into reimputation_action select reimputation_action_seq.nextval, my_reim_id, tyac_id, exe_ordre, dact_montant_budgetaire, dact_ht_saisie,
         dact_tva_saisie, dact_ttc_saisie from depense_ctrl_action where dep_id=a_dep_id;
         
     insert into reimputation_analytique select reimputation_analytique_seq.nextval, my_reim_id, can_id, dana_montant_budgetaire, dana_ht_saisie,
         dana_tva_saisie, dana_ttc_saisie from depense_ctrl_analytique where dep_id=a_dep_id;
         
     insert into reimputation_budget select reimputation_budget_seq.nextval, my_reim_id, d.eng_id, e.org_id, e.tcd_ordre, d.tap_id
        from depense_budget d, engage_budget e where e.eng_id=d.eng_id and dep_id=a_dep_id;
     
     insert into reimputation_convention select reimputation_convention_seq.nextval, my_reim_id, conv_ordre, dcon_montant_budgetaire, dcon_ht_saisie,
         dcon_tva_saisie, dcon_ttc_saisie from depense_ctrl_convention where dep_id=a_dep_id;
         
     insert into reimputation_hors_marche select reimputation_hors_marche_seq.nextval, my_reim_id, typa_id, ce_ordre, dhom_montant_budgetaire, dhom_ht_saisie,
         dhom_tva_saisie, dhom_ttc_saisie from depense_ctrl_hors_marche where dep_id=a_dep_id;
         
     insert into reimputation_marche select reimputation_marche_seq.nextval, my_reim_id, att_ordre, dmar_montant_budgetaire, dmar_ht_saisie,
         dmar_tva_saisie, dmar_ttc_saisie from depense_ctrl_marche where dep_id=a_dep_id;
         
     insert into reimputation_planco select reimputation_planco_seq.nextval, my_reim_id, pco_num, exe_ordre, dpco_montant_budgetaire, dpco_ht_saisie,
         dpco_tva_saisie, dpco_ttc_saisie from depense_ctrl_planco where dep_id=a_dep_id;
   END;
   
END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Commander
IS

   PROCEDURE annuler_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   del_commande_engagement(a_comm_id, my_eng_id);
		   Engager.del_engage(my_eng_id, a_utl_ordre);
        END LOOP;
		CLOSE engagements;

		del_commande(a_comm_id);
   END;

   PROCEDURE basculer_commande (
      a_comm_id_origine     commande.comm_id%type,
	  a_comm_id_destination	commande.comm_id%type
   ) IS
     my_nb integer;
   begin
       select count(*) into my_nb from commande_bascule where comm_id_origine=a_comm_id_origine;
       if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande a deja ete basculee');
       end if;
       
       insert into commande_bascule select commande_bascule_seq.nextval, a_comm_id_origine, a_comm_id_destination from dual;
       
       insert into commande_utilisateur select commande_utilisateur_seq.nextval, a_comm_id_destination, c.utl_ordre from 
           (select utl_ordre from commande where comm_id=a_comm_id_origine
              union select utl_ordre from commande_utilisateur where comm_id=a_comm_id_origine) c;
   end;

   PROCEDURE solder_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   Engager.solder_engage(my_eng_id, a_utl_ordre);
        END LOOP;
		CLOSE engagements;

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE solder_commande_sansdroit (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   Engager.solder_engage_sansdroit(my_eng_id,null);
        END LOOP;
		CLOSE engagements;

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande (
      a_comm_id IN OUT		COMMANDE.comm_id%TYPE,
	  a_exe_ordre			COMMANDE.exe_ordre%TYPE,
	  a_comm_numero	IN OUT  COMMANDE.comm_numero%TYPE,
	  a_tyet_id				COMMANDE.tyet_id%TYPE,
	  a_fou_ordre			COMMANDE.fou_ordre%TYPE,
	  a_comm_reference		COMMANDE.comm_reference%TYPE,
	  a_comm_libelle		COMMANDE.comm_libelle%TYPE,
	  a_tyet_id_imprimable  COMMANDE.tyet_id_imprimable%TYPE,
	  a_dev_id				COMMANDE.dev_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      my_comm_reference		COMMANDE.comm_reference%TYPE;
   BEGIN
	   	-- enregistrement dans la table.
		IF a_comm_numero IS NULL THEN
   		   a_comm_numero := Get_Numerotation(a_exe_ordre, NULL, null, 'COMMANDE');
   		END IF;

		IF a_comm_reference IS NULL THEN
		   my_comm_reference:=TO_CHAR(a_comm_numero);
		ELSE
		   my_comm_reference:=a_comm_reference;
		END IF;

	    IF a_comm_id IS NULL THEN
	       SELECT commande_seq.NEXTVAL INTO a_comm_id FROM dual;
	    END IF;

		-- insertion de la commande.
		INSERT INTO COMMANDE VALUES(a_comm_id, a_exe_ordre,a_comm_numero, a_tyet_id, a_fou_ordre,
		   my_comm_reference, a_comm_libelle, a_tyet_id_imprimable, a_dev_id, a_utl_ordre, SYSDATE, SYSDATE);

		-- verification et correction de l'etat de la commande.
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE upd_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_fou_ordre			COMMANDE.fou_ordre%TYPE,
	  a_comm_reference		COMMANDE.comm_reference%TYPE,
	  a_comm_libelle		COMMANDE.comm_libelle%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
     my_nb                  INTEGER;
     my_commande            COMMANDE%ROWTYPE;
      my_comm_reference		COMMANDE.comm_reference%TYPE;
   BEGIN
   		SELECT * INTO my_commande FROM COMMANDE WHERE comm_id=a_comm_id;

		IF a_comm_reference IS NULL THEN
		   my_comm_reference:=TO_CHAR(my_commande.comm_numero);
		ELSE
		   my_comm_reference:=a_comm_reference;
		END IF;

		IF my_commande.tyet_id=Etats.get_etat_annulee THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La commande est annulee on ne peut rien modifier');
		END IF;

		IF my_commande.tyet_id=Etats.get_etat_precommande THEN
		   UPDATE COMMANDE SET comm_reference=my_comm_reference,
		      comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre WHERE comm_id=a_comm_id;

		   -- si c'est une precommande et que ce n'est pas un marche on peut changer le fournisseur.
           SELECT COUNT(*) INTO my_nb FROM ARTICLE WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;
		   IF my_nb=0 AND my_commande.fou_ordre<>a_fou_ordre THEN
		      UPDATE COMMANDE SET fou_ordre=a_fou_ordre WHERE comm_id=a_comm_id;
		   END IF;

		   -- pour le moment on bloque le fournisseur d'une commande sur marche... on pourrait chercher.
		   -- si le nouveau fournisseur est le titulaire ou un sous_traitant ... a voir plus tard.
		   IF my_nb>0 AND my_commande.fou_ordre<>a_fou_ordre THEN
		      RAISE_APPLICATION_ERROR(-20001, 'La commande est sur marche on ne peut pas modifier le fournisseur');
		   END IF;
		ELSE
		   IF my_commande.fou_ordre<>a_fou_ordre THEN
		      RAISE_APPLICATION_ERROR(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier le fournisseur');
		   END IF;

		   --if my_commande.comm_reference<>a_comm_reference then
		   --   raise_application_error(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier la reference');
		   --end if;

           UPDATE COMMANDE SET fou_ordre=a_fou_ordre, comm_reference=my_comm_reference,
		      comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre, comm_date=SYSDATE WHERE comm_id=a_comm_id;
		END IF;

        update engage_budget set eng_libelle=a_comm_libelle where eng_libelle<>a_comm_libelle
           and eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
           
		jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande_engagement (
	  a_come_id	IN OUT	    COMMANDE_ENGAGEMENT.come_id%TYPE,
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_nb                       INTEGER;
	 my_tyap_id                  ENGAGE_BUDGET.tyap_id%TYPE;
	 my_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	 my_montant_budgetaire_reste ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
	 my_eng_exe_ordre			 ENGAGE_BUDGET.exe_ordre%TYPE;
	 my_eng_fou_ordre			 ENGAGE_BUDGET.fou_ordre%TYPE;
	 my_cde_exe_ordre			 COMMANDE.exe_ordre%TYPE;
	 my_cde_fou_ordre			 COMMANDE.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
		IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
		IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement est deja utilise pour une commande ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		SELECT tyap_id, eng_montant_budgetaire, eng_montant_budgetaire_reste, exe_ordre, fou_ordre
		  INTO my_tyap_id, my_montant_budgetaire, my_montant_budgetaire_reste,my_eng_exe_ordre,
		       my_eng_fou_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

		SELECT exe_ordre, fou_ordre INTO my_cde_exe_ordre, my_cde_fou_ordre
		  FROM COMMANDE WHERE comm_id=a_comm_id;

		IF my_eng_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_eng_fou_ordre<>my_cde_fou_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas pour le meme fournisseur ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_montant_budgetaire>my_montant_budgetaire_reste THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement est deja partiellement solde ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		IF my_tyap_id<>Get_Type_Application THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement n''est pas genere par l''application Carambole ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- si pas de probleme on insere.
        IF a_come_id IS NULL THEN
	       SELECT commande_engagement_seq.NEXTVAL INTO a_come_id FROM dual;
	    END IF;

		INSERT INTO COMMANDE_ENGAGEMENT VALUES (a_come_id, a_comm_id, a_eng_id);

		-- verification et correction de l'etat de la commande.
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande_engagement (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
   BEGIN
   		DELETE FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id AND eng_id=a_eng_id;
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
   BEGIN
        Verifier.verifier_util_commande(a_comm_id);

		UPDATE COMMANDE SET tyet_id=Etats.get_etat_annulee,
		     tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
   END;

   PROCEDURE del_commande_budget (
      a_cbud_id             COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_comm_id             COMMANDE_BUDGET.comm_id%TYPE;
   BEGIN
        Verifier.verifier_util_commande_budget(a_cbud_id);

		SELECT comm_id INTO my_comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

		DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_article (
      a_art_id IN OUT		ARTICLE.art_id%TYPE,
	  a_comm_id				ARTICLE.comm_id%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_quantite		ARTICLE.art_quantite%TYPE,
	  a_art_prix_total_ht	ARTICLE.art_prix_total_ht%TYPE,
	  a_art_prix_total_ttc	ARTICLE.art_prix_total_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_artc_id				ARTICLE.artc_id%TYPE,
	  a_att_ordre			ARTICLE.att_ordre%TYPE,
	  a_ce_ordre			ARTICLE.ce_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE,
	  a_art_id_pere			ARTICLE.art_id_pere%TYPE,
	  a_typa_id				ARTICLE.typa_id%TYPE
   ) IS
      my_cm_niveau			v_code_marche.cm_niveau%TYPE;
      my_exe_ordre			v_code_exer.exe_ordre%TYPE;
      my_article_cm_niveau	NUMBER;
   BEGIN

   	   	-- verification des montants.
		IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF a_art_quantite<=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
		END IF;

		-- verification de l'attribution et type achat.
		IF a_att_ordre IS NULL AND a_typa_id IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
		END IF;

		IF a_att_ordre IS NOT NULL AND a_typa_id IS NOT NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
		END IF;

		-- verification du niveau du code_exer de l'article.
		SELECT cm_niveau, exe_ordre INTO my_cm_niveau, my_exe_ordre FROM v_code_exer e, v_code_marche c
		WHERE e.cm_ordre=c.cm_ordre AND e.ce_ordre=a_ce_ordre;

		my_article_cm_niveau:=grhum.en_nombre(Get_Parametre(my_exe_ordre, 'ARTICLE_CM_NIVEAU'));
		IF my_article_cm_niveau<>my_cm_niveau THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''article doit avoir un code de nomenclature de niveau'||
		     my_article_cm_niveau);
		END IF;

	   	-- enregistrement dans la table.
	    IF a_art_id IS NULL THEN
	       SELECT article_seq.NEXTVAL INTO a_art_id FROM dual;
	    END IF;

		INSERT INTO ARTICLE VALUES (a_art_id, a_comm_id, a_art_libelle, a_art_prix_ht, a_art_prix_ttc,
		   a_art_quantite, a_art_prix_total_ht, a_art_prix_total_ttc, a_art_reference, a_artc_id,
		   a_att_ordre, a_ce_ordre, a_tva_id, a_art_id_pere, a_typa_id);

		-- verification et correction de l'etat de la commande.
		jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_article_catalogue (
      a_caar_id IN OUT		jefy_catalogue.catalogue_article.caar_id%TYPE,
	  a_fou_ordre			jefy_catalogue.catalogue.fou_ordre%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_cm_ordre			jefy_catalogue.ARTICLE.cm_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE
   ) IS
   	 my_nb					INTEGER;
	 my_cat_id				jefy_catalogue.catalogue.cat_id%TYPE;
	 my_art_id				jefy_catalogue.ARTICLE.art_id%TYPE;
   BEGIN
      	-- recherche du catalogue depense.
		SELECT COUNT(*) INTO my_nb FROM jefy_catalogue.catalogue
		   WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
		IF my_nb=0 THEN
		   jefy_catalogue.gestion_catalogue.ins_catalogue(my_cat_id, 'catalogue depense', a_fou_ordre, Get_Type_Application,
      	   	  Get_Type_Etat('VALIDE'), SYSDATE, NULL, 'cree par carambole');
		ELSE
		   SELECT MAX(cat_id) INTO my_cat_id FROM jefy_catalogue.catalogue
		      WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
		END IF;

		-- on insere l'article.
		jefy_catalogue.gestion_catalogue.ins_article(my_art_id, a_art_libelle, NULL, a_cm_ordre,
		Get_Type_Article('ARTICLE'));

		-- on le rattache au catalogue.
		jefy_catalogue.gestion_catalogue.ins_catalogue_article(a_caar_id, my_cat_id, my_art_id,
           a_art_reference, a_art_prix_ht, a_art_prix_ttc, a_tva_id, Get_Type_Etat('VALIDE'),
      	   NULL, NULL);
   END;

   PROCEDURE upd_article (
      a_art_id      		ARTICLE.art_id%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_quantite		ARTICLE.art_quantite%TYPE,
	  a_art_prix_total_ht	ARTICLE.art_prix_total_ht%TYPE,
	  a_art_prix_total_ttc	ARTICLE.art_prix_total_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_ce_ordre			ARTICLE.ce_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE
   ) IS
        my_comm_id			ARTICLE.comm_id%TYPE;
   BEGIN
   	   	-- verification des montants.
		IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF a_art_quantite<=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
		END IF;

		SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;

		-- enregistrement des modifications.
		UPDATE ARTICLE SET art_libelle=a_art_libelle, art_prix_ht=a_art_prix_ht, art_prix_ttc=a_art_prix_ttc,
		   art_quantite=a_art_quantite, art_prix_total_ht=a_art_prix_total_ht,
		   art_prix_total_ttc=a_art_prix_total_ttc, art_reference=a_art_reference, ce_ordre=a_ce_ordre,
		   tva_id=a_tva_id WHERE art_id=a_art_id;

		-- verification et correction de l'etat de la commande.
		jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE del_article (
      a_art_id 		 	ARTICLE.art_id%TYPE)
   IS
     my_comm_id         ARTICLE.comm_id%TYPE;
   BEGIN
        b2bcxml.DEL_B2BCXMLITEM(a_art_id);
   
   		SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;
        DELETE FROM ARTICLE WHERE art_id=a_art_id;
		jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_commande_budget (
      a_cbud_id IN OUT		COMMANDE_BUDGET.cbud_id%TYPE,
      a_comm_id	   			COMMANDE_BUDGET.comm_id%TYPE,
	  a_exe_ordre			COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_org_id				COMMANDE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			COMMANDE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				COMMANDE_BUDGET.tap_id%TYPE,
	  a_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
	  a_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

		IF my_cbud_ht_saisie<0 OR my_cbud_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
		END IF;

   		my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

		IF my_cbud_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      my_cbud_ht_saisie,my_cbud_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_cbud_id IS NULL THEN
	       SELECT commande_budget_seq.NEXTVAL INTO a_cbud_id FROM dual;
	    END IF;

	    INSERT INTO COMMANDE_BUDGET VALUES (a_cbud_id, a_comm_id, a_exe_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, my_montant_budgetaire, my_cbud_ht_saisie, my_cbud_tva_saisie,
		  my_cbud_ttc_saisie);

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_commande_ctrl_action(a_exe_ordre,a_cbud_id,a_chaine_action);
		ins_commande_ctrl_analytique(a_exe_ordre,a_cbud_id,a_chaine_analytique);
		ins_commande_ctrl_convention(a_exe_ordre,a_cbud_id,a_chaine_convention);
		ins_commande_ctrl_hors_marche(a_exe_ordre,a_cbud_id,a_chaine_hors_marche);
		ins_commande_ctrl_marche(a_exe_ordre,a_cbud_id,a_chaine_marche);
		ins_commande_ctrl_planco(a_exe_ordre,a_cbud_id,a_chaine_planco);

		Verifier.verifier_cde_budget_coherence(a_cbud_id);
		--verifier.verifier_engage_coherence(a_eng_id);
		--apres_engage.engage(a_eng_id);
   END;

   PROCEDURE upd_commande_budget (
      a_cbud_id     		COMMANDE_BUDGET.cbud_id%TYPE,
	  a_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
	  a_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
	  a_tap_id				COMMANDE_BUDGET.tap_id%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_org_id     			COMMANDE_BUDGET.org_id%TYPE;
     my_exe_ordre  			COMMANDE_BUDGET.exe_ordre%TYPE;
     my_tcd_ordre  			COMMANDE_BUDGET.tcd_ordre%TYPE;
     my_nb_decimales        NUMBER;
     my_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
   BEGIN

		IF a_cbud_ht_saisie<0 OR a_cbud_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
		END IF;

		SELECT org_id, exe_ordre, tcd_ordre INTO my_org_id, my_exe_ordre, my_tcd_ordre
		  FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

   		my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

		IF my_cbud_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;


  		Verifier.verifier_budget(my_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,a_tap_id,my_org_id,
		      my_cbud_ht_saisie,my_cbud_ttc_saisie);

	    UPDATE COMMANDE_BUDGET SET cbud_montant_budgetaire=my_montant_budgetaire,
		    cbud_ht_saisie=my_cbud_ht_saisie, cbud_tva_saisie=my_cbud_tva_saisie,
		    cbud_ttc_saisie=my_cbud_ttc_saisie, tap_id=a_tap_id WHERE cbud_id=a_cbud_id;

		DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_commande_ctrl_action(my_exe_ordre,a_cbud_id,a_chaine_action);
		ins_commande_ctrl_analytique(my_exe_ordre,a_cbud_id,a_chaine_analytique);
		ins_commande_ctrl_convention(my_exe_ordre,a_cbud_id,a_chaine_convention);
		ins_commande_ctrl_hors_marche(my_exe_ordre,a_cbud_id,a_chaine_hors_marche);
		ins_commande_ctrl_marche(my_exe_ordre,a_cbud_id,a_chaine_marche);
		ins_commande_ctrl_planco(my_exe_ordre,a_cbud_id,a_chaine_planco);

		Verifier.verifier_cde_budget_coherence(a_cbud_id);
		--verifier.verifier_engage_coherence(a_eng_id);
		--apres_engage.engage(a_eng_id);
   END;

   PROCEDURE ins_commande_ctrl_action (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cact_id	               COMMANDE_CTRL_ACTION.cact_id%TYPE;
       my_tyac_id	  	   		   COMMANDE_CTRL_ACTION.tyac_id%TYPE;
       my_cact_montant_budgetaire  COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_cact_ht_saisie	  	   COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
       my_cact_tva_saisie		   COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
       my_cact_ttc_saisie		   COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
       my_cact_pourcentage		   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cact_ht_saisie:=round(my_cact_ht_saisie, my_nb_decimales);
            my_cact_ttc_saisie:=round(my_cact_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aact_ht_saisie<0 or my_aact_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cact_tva_saisie := my_cact_ttc_saisie - my_cact_ht_saisie;
			IF my_cact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cact_ht_saisie, my_cact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cact_montant_budgetaire THEN
			    my_cact_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_action_seq.NEXTVAL INTO my_cact_id FROM dual;

			INSERT INTO COMMANDE_CTRL_ACTION VALUES (my_cact_id,
			       a_exe_ordre, a_cbud_id, my_tyac_id, my_cact_montant_budgetaire, my_cact_pourcentage,
				   my_cact_ht_saisie, my_cact_tva_saisie, my_cact_ttc_saisie);

	        --verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id);
			--apres_engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cact_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_analytique (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cana_id	               COMMANDE_CTRL_ANALYTIQUE.cana_id%TYPE;
       my_can_id	  	   		   COMMANDE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_cana_montant_budgetaire  COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_cana_ht_saisie	  	   COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
       my_cana_tva_saisie		   COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
       my_cana_ttc_saisie		   COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
       my_cana_pourcentage		   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cana_ht_saisie:=round(my_cana_ht_saisie, my_nb_decimales);
            my_cana_ttc_saisie:=round(my_cana_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cana_tva_saisie := my_cana_ttc_saisie - my_cana_ht_saisie;
			IF my_cana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cana_ht_saisie,my_cana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_cbud_montant_budgetaire<=my_somme+my_cana_montant_budgetaire THEN
			    my_cana_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_analytique_seq.NEXTVAL INTO my_cana_id FROM dual;

			INSERT INTO COMMANDE_CTRL_ANALYTIQUE VALUES (my_cana_id,
			       a_exe_ordre, a_cbud_id, my_can_id, my_cana_montant_budgetaire, my_cana_pourcentage,
				   my_cana_ht_saisie, my_cana_tva_saisie, my_cana_ttc_saisie);

			--verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	--apres_engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_convention (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ccon_id	               COMMANDE_CTRL_CONVENTION.ccon_id%TYPE;
       my_conv_ordre  	   		   COMMANDE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_ccon_montant_budgetaire  COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_ccon_ht_saisie	  	   COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
       my_ccon_tva_saisie		   COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
       my_ccon_ttc_saisie		   COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
       my_ccon_pourcentage		   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ccon_ht_saisie:=round(my_ccon_ht_saisie, my_nb_decimales);
            my_ccon_ttc_saisie:=round(my_ccon_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_ccon_tva_saisie := my_ccon_ttc_saisie - my_ccon_ht_saisie;
			IF my_ccon_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_ccon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ccon_ht_saisie,my_ccon_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_cbud_montant_budgetaire<=my_somme+my_ccon_montant_budgetaire THEN
			    my_ccon_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_convention_seq.NEXTVAL INTO my_ccon_id FROM dual;

			INSERT INTO COMMANDE_CTRL_CONVENTION VALUES (my_ccon_id,
			       a_exe_ordre, a_cbud_id, my_conv_ordre, my_ccon_montant_budgetaire, my_ccon_pourcentage,
				   my_ccon_ht_saisie, my_ccon_tva_saisie, my_ccon_ttc_saisie);

			--verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	--apres_engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ccon_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_hors_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_chom_id	               COMMANDE_CTRL_HORS_MARCHE.chom_id%TYPE;
       my_ce_ordre	  	   		   COMMANDE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_typa_id	  	   		   COMMANDE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_chom_montant_budgetaire  COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_chom_ht_saisie	  	   COMMANDE_CTRL_HORS_MARCHE.chom_ht_saisie%TYPE;
       my_chom_tva_saisie		   COMMANDE_CTRL_HORS_MARCHE.chom_tva_saisie%TYPE;
       my_chom_ttc_saisie		   COMMANDE_CTRL_HORS_MARCHE.chom_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code nomenclature.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_chom_ht_saisie:=round(my_chom_ht_saisie, my_nb_decimales);
            my_chom_ttc_saisie:=round(my_chom_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_chom_tva_saisie := my_chom_ttc_saisie - my_chom_ht_saisie;
			IF my_chom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_chom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_chom_ht_saisie,my_chom_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_chom_montant_budgetaire THEN
			    my_chom_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_hors_marche_seq.NEXTVAL INTO my_chom_id FROM dual;

			INSERT INTO COMMANDE_CTRL_HORS_MARCHE VALUES (my_chom_id,
			       a_exe_ordre, a_cbud_id, my_typa_id, my_ce_ordre, my_chom_montant_budgetaire,
				   NULL, my_chom_ht_saisie, my_chom_tva_saisie, my_chom_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_chom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cmar_id	               COMMANDE_CTRL_MARCHE.cmar_id%TYPE;
       my_att_ordre	  	   		   COMMANDE_CTRL_MARCHE.att_ordre%TYPE;
       my_cmar_montant_budgetaire  COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_cmar_ht_saisie	  	   COMMANDE_CTRL_MARCHE.cmar_ht_saisie%TYPE;
       my_cmar_tva_saisie		   COMMANDE_CTRL_MARCHE.cmar_tva_saisie%TYPE;
       my_cmar_ttc_saisie		   COMMANDE_CTRL_MARCHE.cmar_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cmar_ht_saisie:=round(my_cmar_ht_saisie, my_nb_decimales);
            my_cmar_ttc_saisie:=round(my_cmar_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cmar_tva_saisie := my_cmar_ttc_saisie - my_cmar_ht_saisie;
			IF my_cmar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cmar_ht_saisie,my_cmar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cmar_montant_budgetaire THEN
			    my_cmar_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour une commande');
			END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_marche_seq.NEXTVAL INTO my_cmar_id FROM dual;

			INSERT INTO COMMANDE_CTRL_MARCHE VALUES (my_cmar_id,
			       a_exe_ordre, a_cbud_id, my_att_ordre, my_cmar_montant_budgetaire, NULL,
				   my_cmar_ht_saisie, my_cmar_tva_saisie, my_cmar_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cmar_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_planco (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cpco_id	               COMMANDE_CTRL_PLANCO.cpco_id%TYPE;
       my_pco_num	  	   		   COMMANDE_CTRL_PLANCO.pco_num%TYPE;
       my_cpco_montant_budgetaire  COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_cpco_ht_saisie	  	   COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
       my_cpco_tva_saisie		   COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
       my_cpco_ttc_saisie		   COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
       my_cpco_pourcentage		   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cpco_ht_saisie:=round(my_cpco_ht_saisie, my_nb_decimales);
            my_cpco_ttc_saisie:=round(my_cpco_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cpco_tva_saisie := my_cpco_ttc_saisie - my_cpco_ht_saisie;
			IF my_cpco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cpco_ht_saisie,my_cpco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cpco_montant_budgetaire THEN
			    my_cpco_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_planco_seq.NEXTVAL INTO my_cpco_id FROM dual;

			INSERT INTO COMMANDE_CTRL_PLANCO VALUES (my_cpco_id,
			       a_exe_ordre, a_cbud_id, my_pco_num, my_cpco_montant_budgetaire, my_cpco_pourcentage,
				   my_cpco_ht_saisie, my_cpco_tva_saisie, my_cpco_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cpco_montant_budgetaire;
		END LOOP;
   END;

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Corriger
IS

   PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE)
   IS
     my_nb INTEGER;
     my_comm_reference     commande.comm_reference%type;
     my_comm_numero        commande.comm_numero%type;
     my_exe_ordre          commande.exe_ordre%type;
     my_eng_id             engage_budget.eng_id%type;
     my_org_ub             v_organ.org_ub%type;
     my_org_cr             v_organ.org_cr%type;
     my_eng_ht_saisie      ENGAGE_BUDGET.eng_ht_saisie%TYPE;
     my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_eng_montant        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_montant_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_art_prix_total_ht  ARTICLE.art_prix_total_ht%TYPE;
     my_art_prix_total_ttc ARTICLE.art_prix_total_ttc%TYPE;
     my_tyet_id_imprimable COMMANDE.tyet_id_imprimable%TYPE;
   BEGIN
      SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;

      -- si il n'y a pas d'engagement c'est une precommande.
      IF my_nb=0 THEN
         UPDATE COMMANDE SET tyet_id=Etats.get_etat_precommande,
            tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
      ELSE
         SELECT SUM(eng_ht_saisie), SUM(eng_ttc_saisie), SUM(eng_montant_budgetaire),
             SUM(eng_montant_budgetaire_reste)
           INTO my_eng_ht_saisie, my_eng_ttc_saisie, my_eng_montant, my_eng_montant_reste
           FROM ENGAGE_BUDGET e, COMMANDE_ENGAGEMENT ce
           WHERE ce.comm_id=a_comm_id AND ce.eng_id=e.eng_id;

         SELECT SUM(art_prix_total_ht), SUM(art_prix_total_ttc) INTO my_art_prix_total_ht, my_art_prix_total_ttc
           FROM ARTICLE WHERE comm_id=a_comm_id;

         -- si le montant engage est inferieur au montant commande c'est partiellement engage.
         IF my_eng_ht_saisie<my_art_prix_total_ht OR my_eng_ttc_saisie<my_art_prix_total_ttc THEN
             UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_engagee,
               tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
         ELSE
            -- si le reste engage est egal au montant engage c'est une commande engagee.
            IF my_eng_montant=my_eng_montant_reste THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_engagee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le montant engage est superieur au reste c'est partiellement solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste>0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_soldee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le reste est a 0 c'est solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste=0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_soldee,
                 tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
            END IF;

             -- on verifie dans les services validations pour savoir si la commande est imprimable.
            SELECT tyet_id_imprimable INTO my_tyet_id_imprimable FROM COMMANDE WHERE comm_id=a_comm_id;
            IF my_tyet_id_imprimable=Etats.get_etat_imprimable THEN

                SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande WHERE vlco_comid=a_comm_id
                     AND vlco_valide ='OUI';
                IF my_nb>0 THEN

                   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande
                      WHERE vlco_comid=a_comm_id AND vlco_etat<>'ACCEPTEE' AND vlco_valide ='OUI'
                      -- Ajout du vlco_id max --
                      AND vlco_id = (SELECT MAX(vlco_id) FROM jefy_marches.sa_validation_commande
                            WHERE vlco_comid=a_comm_id AND vlco_valide = 'OUI');

                   IF my_nb>0 THEN
                        UPDATE COMMANDE SET tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
                   END IF;

                END IF;
            END IF;

         END IF;
         
         -- reference automatique.
         SELECT comm_numero, comm_reference, exe_ordre INTO my_comm_numero, my_comm_reference, my_exe_ordre
            FROM COMMANDE WHERE comm_id=a_comm_id;
         if to_char(my_comm_numero)=my_comm_reference then
            select min(eng_id) into my_eng_id from commande_engagement where comm_id=a_comm_id;
            select o.org_ub, o.org_cr into my_org_ub, my_org_cr from v_organ o, engage_budget e
              where o.org_id=e.org_id and e.eng_id=my_eng_id;
            update commande set comm_reference=my_org_ub||'/'||my_org_cr||'/'||Get_Numerotation(my_exe_ordre,my_org_ub,my_org_cr,'COMMANDE_REF')
              where comm_id=a_comm_id;
         end if;
      END IF;
   END;

   PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE   ) IS
     CURSOR liste IS SELECT * FROM COMMANDE_BUDGET WHERE comm_id=a_comm_id;

     my_commande_budget                COMMANDE_BUDGET%ROWTYPE;
   BEGIN
        OPEN liste();
          LOOP
           FETCH  liste INTO my_commande_budget;
           EXIT WHEN liste%NOTFOUND;

           corriger_commande_ctrl(my_commande_budget.cbud_id);
        END LOOP;
        CLOSE liste;
   END;

   PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_reste         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_dep_bud       depense_budget.dep_montant_budgetaire%type;
     my_eng_bud       engage_budget.eng_montant_budgetaire%type;
     my_exe_ordre     engage_budget.exe_ordre%type;
     my_org_id        engage_budget.org_id%type;
     my_tcd_ordre     engage_budget.tcd_ordre%type;
   BEGIN
        select nvl(sum(eng_montant_budgetaire_reste),0) into my_reste from engage_budget where eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_dep_bud from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
--        select nvl(sum(eng_montant_budgetaire),0) into my_eng_bud from engage_budget where eng_id=a_eng_id;
--        my_reste:=my_eng_bud-my_dep_bud;
--        if my_reste<0 then my_reste:=0; end if;

--        select exe_ordre, org_id, tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre from engage_budget where eng_id=a_eng_id; 
--        update engage_budget set eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;
--        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>0 THEN
        --dbms_output.put_line ('debut '||a_eng_id||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_action(a_eng_id);
        --dbms_output.put_line (TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_analytique(a_eng_id);
             upd_engage_reste_convention(a_eng_id);
             upd_engage_reste_hors_marche(a_eng_id);
             upd_engage_reste_marche(a_eng_id);
             upd_engage_reste_planco(a_eng_id);
        ELSE
          UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        END IF;
   END;


   /*********************************************************************************/
   PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT tyac_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie))
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY tyac_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
        AND eact_montant_budgetaire_reste>0;

     my_depense_tyac_id               DEPENSE_CTRL_ACTION.tyac_id%TYPE;
     my_engage_ctrl_action            ENGAGE_CTRL_ACTION%ROWTYPE;

     my_eact_montant_bud              ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
     my_eact_montant_bud_reste        ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_eact_id                       ENGAGE_CTRL_ACTION.eact_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     --my_org_id                      ENGAGE_BUDGET.org_id%TYPE;
     --my_tcd_ordre                   ENGAGE_BUDGET.tcd_ordre%TYPE;
     --my_tap_id                      ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eact_reste                    engage_ctrl_action.eact_montant_budgetaire_reste%type;
     my_dact_bud                      depense_ctrl_action.dact_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre INTO my_exe_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eact_montant_budgetaire_reste) into my_eact_reste
          from engage_ctrl_action where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie)),0)
          into my_dact_bud
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eact_reste>my_reste+my_dact_bud then
           my_somme:=my_eact_reste-my_reste-my_dact_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_action;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_action.eact_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_action 
                      set eact_montant_budgetaire_reste=my_engage_ctrl_action.eact_montant_budgetaire_reste-my_somme
                      where eact_id=my_engage_ctrl_action.eact_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_action.eact_montant_budgetaire_reste;
                    update engage_ctrl_action set eact_montant_budgetaire_reste=0 where eact_id=my_engage_ctrl_action.eact_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eact_montant_budgetaire_reste) into my_eact_reste
             from engage_ctrl_action where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_tyac_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION
              WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eact_id, eact_montant_budgetaire_reste, eact_montant_budgetaire
                INTO my_eact_id, my_eact_montant_bud_reste, my_eact_montant_bud
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

              -- cas de la liquidation.
              IF my_eact_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ACTION
                   SET eact_montant_budgetaire_reste=my_eact_montant_bud_reste-my_montant_budgetaire
                   WHERE eact_id=my_eact_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eact_id=my_eact_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eact_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0)
                INTO my_eact_montant_bud, my_eact_montant_bud_reste
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND eact_montant_budgetaire_reste>0;

              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>=my_eact_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
                         AND eact_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_action;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant := upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_action.eact_montant_budgetaire_reste, my_eact_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire_reste-my_montant
                          WHERE eact_id=my_engage_ctrl_action.eact_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;


   /****************************************************************************/
   PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      CURSOR liste  IS SELECT can_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie))
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id
        AND eana_montant_budgetaire_reste>0;

     my_depense_can_id                DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
     my_engage_ctrl_analytique        ENGAGE_CTRL_ANALYTIQUE%ROWTYPE;

     my_eana_montant_bud              ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
     my_eana_montant_bud_reste        ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eana_id                       ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                        ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                     ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                        ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste                 ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eana_reste                    engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste%type;
     my_dana_bud                      depense_ctrl_ANALYTIQUE.dana_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id /*AND eana_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eana_montant_budgetaire_reste) into my_eana_reste
          from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eana_reste>my_reste+my_dana_bud then
           my_somme:=my_eana_reste-my_reste-my_dana_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_ANALYTIQUE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_ANALYTIQUE 
                      set eana_montant_budgetaire_reste=my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste-my_somme
                      where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste;
                    update engage_ctrl_ANALYTIQUE set eana_montant_budgetaire_reste=0 where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eana_montant_budgetaire_reste) into my_eana_reste
             from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_can_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE
              WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eana_id, eana_montant_budgetaire_reste, eana_montant_budgetaire
                INTO my_eana_id, my_eana_montant_bud_reste, my_eana_montant_bud
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

              -- cas de la liquidation.
              IF my_eana_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE
                   SET eana_montant_budgetaire_reste=my_eana_montant_bud_reste-my_montant_budgetaire
                   WHERE eana_id=my_eana_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eana_id=my_eana_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eana_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0)
                INTO my_eana_montant_bud, my_eana_montant_bud_reste
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>=my_eana_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_analytique;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                          WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(eana_montant_budgetaire_reste),0) INTO my_eana_montant_bud_reste
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        IF my_eana_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_eana_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id  AND eana_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_analytique;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                    WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT conv_ordre, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie))
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY conv_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id
        AND econ_montant_budgetaire_reste>0;

     my_depense_conv_ordre       DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
     my_engage_ctrl_convention   ENGAGE_CTRL_CONVENTION%ROWTYPE;

     my_econ_montant_bud         ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
     my_econ_montant_bud_reste   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_econ_id                  ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste            ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                  NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_econ_reste               engage_ctrl_CONVENTION.econ_montant_budgetaire_reste%type;
     my_dcon_bud                 depense_ctrl_CONVENTION.dcon_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
   BEGIN
        select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id /*AND econ_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(econ_montant_budgetaire_reste) into my_econ_reste
          from engage_ctrl_CONVENTION where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie)),0)
          into my_dcon_bud
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_econ_reste>my_reste+my_dcon_bud then
           my_somme:=my_econ_reste-my_reste-my_dcon_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_CONVENTION;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_CONVENTION
                      set econ_montant_budgetaire_reste=my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste-my_somme
                      where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste;
                    update engage_ctrl_CONVENTION set econ_montant_budgetaire_reste=0 where econ_id=my_engage_ctrl_CONVENTION.econ_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(econ_montant_budgetaire_reste) into my_econ_reste
             from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_conv_ordre, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION
              WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT econ_id, econ_montant_budgetaire_reste, econ_montant_budgetaire
                INTO my_econ_id, my_econ_montant_bud_reste, my_econ_montant_bud
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

              -- cas de la liquidation.
              IF my_econ_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_CONVENTION
                   SET econ_montant_budgetaire_reste=my_econ_montant_bud_reste-my_montant_budgetaire
                   WHERE econ_id=my_econ_id;
              ELSE
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE econ_id=my_econ_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_econ_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0)
                INTO my_econ_montant_bud, my_econ_montant_bud_reste
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;

              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>=my_econ_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_convention;
                        EXIT WHEN liste2%NOTFOUND;

                        IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                          WHERE econ_id=my_engage_ctrl_convention.econ_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(econ_montant_budgetaire_reste),0) INTO my_econ_montant_bud_reste
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_econ_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_econ_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id  AND econ_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_convention;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                    WHERE econ_id=my_engage_ctrl_convention.econ_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT ce_ordre, typa_id, SUM(dc.dhom_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie))
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY ce_ordre, typa_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id
        AND ehom_montant_budgetaire_reste>0;

     my_depense_ce_ordre         DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
     my_depense_typa_id          DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
     my_engage_ctrl_hors_marche  ENGAGE_CTRL_HORS_MARCHE%ROWTYPE;

     my_ehom_montant_bud         ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
     my_ehom_montant_bud_reste   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_ehom_ht_reste            ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_ehom_id                  ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_ehom_reste               engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste%type;
     my_dhom_bud                 depense_ctrl_HORS_MARCHE.dhom_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_HORS_MARCHE
          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire, ehom_ht_reste=ehom_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
          from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie)),0)
          into my_dhom_bud
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_ehom_reste>my_reste+my_dhom_bud then
           my_somme:=my_ehom_reste-my_reste-my_dhom_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_HORS_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_HORS_MARCHE 
                      set ehom_montant_budgetaire_reste=my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste-my_somme
                      where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste;
                    update engage_ctrl_HORS_MARCHE set ehom_montant_budgetaire_reste=0 where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
             from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_ce_ordre, my_depense_typa_id, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE eng_id=a_eng_id AND typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT ehom_id, ehom_montant_budgetaire_reste, ehom_montant_budgetaire, ehom_ht_reste
                INTO my_ehom_id, my_ehom_montant_bud_reste, my_ehom_montant_bud, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND
                typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

              -- cas de la liquidation.
              IF my_ehom_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE
                   SET ehom_montant_budgetaire_reste=my_ehom_montant_bud_reste-my_montant_budgetaire,
                       ehom_ht_reste=decode(ehom_ht_reste-my_montant_budht, abs(ehom_ht_reste-my_montant_budht),
                          ehom_ht_reste-my_montant_budht,0)
                   WHERE ehom_id=my_ehom_id;
              ELSE
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                   WHERE ehom_id=my_ehom_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_ehom_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_ehom_ht_reste;
              
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0), NVL(SUM(ehom_ht_reste),0)
                INTO my_ehom_montant_bud, my_ehom_montant_bud_reste, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND ehom_montant_budgetaire_reste>0;

              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>=my_ehom_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                    WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id  AND ehom_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_hors_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_hors_marche.ehom_montant_budgetaire_reste, my_ehom_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_hors_marche.ehom_ht_reste, my_ehom_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_HORS_MARCHE
                          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire_reste-my_montant,
                              ehom_ht_reste=decode(ehom_ht_reste-my_montant_ht, abs(ehom_ht_reste-my_montant_ht),
                                  ehom_ht_reste-my_montant_ht,0)
                          WHERE ehom_id=my_engage_ctrl_hors_marche.ehom_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT att_ordre, SUM(dc.dmar_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie))
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY att_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id
        AND emar_montant_budgetaire_reste>0;

     my_depense_att_ordre        DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
     my_engage_ctrl_marche       ENGAGE_CTRL_MARCHE%ROWTYPE;

     my_emar_montant_bud         ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
     my_emar_montant_bud_reste   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_emar_ht_reste            ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_emar_id                  ENGAGE_CTRL_MARCHE.emar_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_emar_reste               engage_ctrl_MARCHE.emar_montant_budgetaire_reste%type;
     my_dmar_bud                 depense_ctrl_MARCHE.dmar_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_MARCHE
          SET emar_montant_budgetaire_reste=emar_montant_budgetaire, emar_ht_reste=emar_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(emar_montant_budgetaire_reste) into my_emar_reste
          from engage_ctrl_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie)),0)
          into my_dmar_bud
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_emar_reste>my_reste+my_dmar_bud then
           my_somme:=my_emar_reste-my_reste-my_dmar_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_MARCHE 
                      set emar_montant_budgetaire_reste=my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste-my_somme
                      where emar_id=my_engage_ctrl_MARCHE.emar_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste;
                    update engage_ctrl_MARCHE set emar_montant_budgetaire_reste=0 where emar_id=my_engage_ctrl_MARCHE.emar_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(emar_montant_budgetaire_reste) into my_emar_reste
             from engage_ctrl_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_att_ordre, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE
              WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT emar_id, emar_montant_budgetaire_reste, emar_montant_budgetaire, emar_ht_reste
                INTO my_emar_id, my_emar_montant_bud_reste, my_emar_montant_bud, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

              -- cas de la liquidation.
              IF my_emar_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_MARCHE
                   SET emar_montant_budgetaire_reste=my_emar_montant_bud_reste-my_montant_budgetaire,
                   emar_ht_reste=decode(emar_ht_reste-my_montant_budht,abs(emar_ht_reste-my_montant_budht),
                       emar_ht_reste-my_montant_budht,0)
                   WHERE emar_id=my_emar_id;
              ELSE
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE emar_id=my_emar_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_emar_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_emar_ht_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0), NVL(SUM(emar_ht_reste),0)
                INTO my_emar_montant_bud, my_emar_montant_bud_reste, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND emar_montant_budgetaire_reste>0;

              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>=my_emar_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id  AND emar_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_marche.emar_montant_budgetaire_reste, my_emar_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_marche.emar_ht_reste, my_emar_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_MARCHE
                          SET emar_montant_budgetaire_reste=emar_montant_budgetaire_reste-my_montant,
                              emar_ht_reste=decode(emar_ht_reste-my_montant_ht, abs(emar_ht_reste-my_montant_ht),
                                 emar_ht_reste-my_montant_ht,0)
                          WHERE emar_id=my_engage_ctrl_marche.emar_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT pco_num, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie))
        FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY pco_num;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id
        AND epco_montant_budgetaire_reste>0;

     my_depense_pco_num        DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_engage_ctrl_planco     ENGAGE_CTRL_PLANCO%ROWTYPE;

     my_epco_montant_bud       ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
     my_epco_montant_bud_reste ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_epco_id                ENGAGE_CTRL_PLANCO.epco_id%TYPE;
     my_exe_ordre              ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                 ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire     DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
     my_nb                     INTEGER;
     my_somme                  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_montant                NUMBER;
     my_dernier                BOOLEAN;
     my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_epco_reste             engage_ctrl_PLANCO.epco_montant_budgetaire_reste%type;
     my_dpco_bud               depense_ctrl_PLANCO.dpco_montant_budgetaire%type;
     my_eng                    ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(epco_montant_budgetaire_reste) into my_epco_reste
          from engage_ctrl_PLANCO where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie)),0)
          into my_dpco_bud
         FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_epco_reste>my_reste+my_dpco_bud then
           my_somme:=my_epco_reste-my_reste-my_dpco_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_PLANCO;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_PLANCO 
                      set epco_montant_budgetaire_reste=my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste-my_somme
                      where epco_id=my_engage_ctrl_PLANCO.epco_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste;
                    update engage_ctrl_PLANCO set epco_montant_budgetaire_reste=0 where epco_id=my_engage_ctrl_PLANCO.epco_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(epco_montant_budgetaire_reste) into my_epco_reste
             from engage_ctrl_PLANCO where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_pco_num, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO
              WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT epco_id, epco_montant_budgetaire_reste, epco_montant_budgetaire
                INTO my_epco_id, my_epco_montant_bud_reste, my_epco_montant_bud
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

              -- cas de la liquidation.
              IF my_epco_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_PLANCO
                   SET epco_montant_budgetaire_reste=my_epco_montant_bud_reste-my_montant_budgetaire
                   WHERE epco_id=my_epco_id;
              ELSE
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE epco_id=my_epco_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_epco_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0)
                INTO my_epco_montant_bud, my_epco_montant_bud_reste
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND epco_montant_budgetaire_reste>0;

              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>=my_epco_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id  AND epco_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_planco;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_planco.epco_montant_budgetaire_reste, my_epco_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire_reste-my_montant
                          WHERE epco_id=my_engage_ctrl_planco.epco_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_dernier             BOOLEAN,
      a_exe_ordre           ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE
   IS
     my_nb                 INTEGER;
     my_par_value          jefy_admin.PARAMETRE.par_value%TYPE;
     my_dev                jefy_admin.devise.dev_nb_decimales%TYPE;
     my_montant            NUMBER;
     my_pourcentage        NUMBER;
     my_nb_decimales       NUMBER;
   BEGIN
        -- nb decimales.
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        -- calcul
        if a_total_reste=0 then
           my_pourcentage:=0;
        else
           my_pourcentage:=a_reste / a_total_reste;
        end if;
        
        my_montant:=a_montant_budgetaire*my_pourcentage;

        -- arrondir.
        my_montant:= ROUND(my_montant,my_nb_decimales);

        -- on teste avec +1 si les arrondis ont decalé un peu par rapport au total budgetaire
            IF a_dernier THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;
            IF my_montant>a_reste OR my_montant>a_montant_budgetaire - a_somme THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;

        --if a_dernier or my_montant>a_reste then
        --   my_montant:=a_reste;
        --end if;

        RETURN my_montant;
   END;


   PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE) IS
   BEGIN
           corriger_commande_action(a_cbud_id);
           corriger_commande_analytique(a_cbud_id);
           corriger_commande_convention(a_cbud_id);
           corriger_commande_planco(a_cbud_id);

           corriger_commande_hors_marche(a_cbud_id);
           corriger_commande_marche(a_cbud_id);
   END;

   PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ACTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;

           my_somme_pourcentage:=0;
           
           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cact_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ht, my_reste);
              --my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
              --    my_commande_ctrl.cact_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ACTION SET cact_ht_saisie=my_ht, cact_tva_saisie=my_ttc-my_ht,--my_tva,
                     cact_ttc_saisie=my_ttc, cact_montant_budgetaire=my_budgetaire
               WHERE cact_id=my_commande_ctrl.cact_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cana_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ANALYTIQUE SET cana_ht_saisie=my_ht, cana_tva_saisie=my_tva,
                     cana_ttc_saisie=my_ttc, cana_montant_budgetaire=my_budgetaire
               WHERE cana_id=my_commande_ctrl.cana_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_CONVENTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.ccon_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_CONVENTION SET ccon_ht_saisie=my_ht, ccon_tva_saisie=my_tva,
                     ccon_ttc_saisie=my_ttc, ccon_montant_budgetaire=my_budgetaire
               WHERE ccon_id=my_commande_ctrl.ccon_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;
     my_reste              BOOLEAN;

     my_ht_reste           COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste          COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste          COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste   COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_somme_ttc          COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;

     my_ht                 COMMANDE_CTRL_hors_marche.chom_ht_saisie%TYPE;
     my_tva                COMMANDE_CTRL_hors_marche.chom_tva_saisie%TYPE;
     my_ttc                COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;
     my_budgetaire         COMMANDE_CTRL_hors_marche.chom_montant_budgetaire%TYPE;
     my_somme_pourcentage  COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_pourcentage        COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_commande_ctrl      COMMANDE_CTRL_hors_marche%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           select sum(chom_ttc_saisie) into my_somme_ttc from commande_ctrl_hors_marche where cbud_id=a_cbud_id;
            
           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              if my_somme_ttc=0 then 
                 my_pourcentage:=0; 
              else
                 my_pourcentage:=my_commande_ctrl.chom_ttc_saisie*100.0/my_somme_ttc;
              end if;
              my_somme_pourcentage:=my_somme_pourcentage+my_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste, my_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste, my_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste, my_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste, my_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_hors_marche SET chom_ht_saisie=my_ht, chom_tva_saisie=my_tva,
                     chom_ttc_saisie=my_ttc, chom_montant_budgetaire=my_budgetaire
               WHERE chom_id=my_commande_ctrl.chom_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           UPDATE COMMANDE_CTRL_marche SET cmar_ht_saisie=my_cde_ht, cmar_tva_saisie=my_cde_tva,
                  cmar_ttc_saisie=my_cde_ttc, cmar_montant_budgetaire=my_cde_budgetaire
               WHERE cbud_id=a_cbud_id;

        END IF;
   END;

   PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_PLANCO%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cpco_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_PLANCO SET cpco_ht_saisie=my_ht, cpco_tva_saisie=my_tva,
                     cpco_ttc_saisie=my_ttc, cpco_montant_budgetaire=my_budgetaire
               WHERE cpco_id=my_commande_ctrl.cpco_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage          COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant              COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN
   )
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE
   IS
        my_calcul                NUMBER;
   BEGIN
           IF a_somme_pourcentage>=100.0 AND a_reste=TRUE THEN
           RETURN a_montant_reste;
        END IF;

        my_calcul:=ROUND(a_pourcentage*a_montant/100, 2);

        IF my_calcul>a_montant_reste THEN
           RETURN a_montant_reste;
        END IF;

        RETURN my_calcul;
   END;

END;
/

CREATE OR REPLACE PACKAGE JEFY_DEPENSE.B2BCXML  IS


PROCEDURE ins_B2bCxmlItem (
      a_bciid IN OUT            b2b_cxml_item.bci_id%TYPE,
      a_fbcpid                  b2b_cxml_item.fbcp_id%TYPE,
      a_artid                   b2b_cxml_item.art_id%TYPE,
      a_classification          b2b_cxml_item.CLASSIFICATION%type,
      a_classificationdom       b2b_cxml_item.CLASSIFICATION_DOM%type,
      a_datecreation            b2b_cxml_item.DATE_CREATION%type,
      a_description             b2b_cxml_item.DESCRIPTION%type,
      a_manufacturername        b2b_cxml_item.MANUFACTURER_NAME%type,
      a_manufacturerpartid      b2b_cxml_item.MANUFACTURER_PART_ID%type,
      a_persidcreation          b2b_cxml_item.PERS_ID_CREATION%type,
      a_supplierpartid          b2b_cxml_item.SUPPLIER_PART_ID%type,
      a_supplierPartAuxiliaryId b2b_cxml_item.SUPPLIER_PART_AUXILIARY_ID%type,
      a_unitofmeasure           b2b_cxml_item.UNIT_OF_MEASURE%type,
      a_unitprice               b2b_cxml_item.UNIT_PRICE%type,
      a_url                     b2b_cxml_item.URL%type,
       a_comments                b2b_cxml_item.COMMENTS%type
      );


PROCEDURE ins_B2bCxmlPunchoutmsg (
         a_bcpoid IN OUT        B2B_CXML_PUNCHOUTMSG.bcpo_id%TYPE,
      a_fbcpid                  B2B_CXML_PUNCHOUTMSG.fbcp_id%TYPE,
      a_commid                  B2B_CXML_PUNCHOUTMSG.comm_id%TYPE,
      a_datecreation            B2B_CXML_PUNCHOUTMSG.DATE_CREATION%type,
      a_persidcreation          B2B_CXML_PUNCHOUTMSG.PERS_ID_CREATION%type,
      a_cxml                    B2B_CXML_PUNCHOUTMSG.cxml%type
      );


PROCEDURE ins_B2bCxmlOrderRequest (
      a_bcorid IN OUT           B2B_CXML_ORDERREQUEST.bcor_id%TYPE,
      a_fbcpid                  B2B_CXML_ORDERREQUEST.fbcp_id%TYPE,
      a_commid                  B2B_CXML_ORDERREQUEST.comm_id%TYPE,
      a_datecreation            B2B_CXML_ORDERREQUEST.DATE_CREATION%type,
      a_persidcreation          B2B_CXML_ORDERREQUEST.PERS_ID_CREATION%type,
      a_cxml                    B2B_CXML_ORDERREQUEST.cxml%type,
      a_cxmlreponse            B2B_CXML_ORDERREQUEST.CXML_REPONSE%type
   );
   
PROCEDURE del_B2bCxmlItem (a_artid b2b_cxml_item.art_id%TYPE);   
   
end;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.B2BCXML
IS
   PROCEDURE ins_B2bCxmlItem (
      a_bciid IN OUT           b2b_cxml_item.bci_id%TYPE,
      a_fbcpid                 b2b_cxml_item.fbcp_id%TYPE,
      a_artid                  b2b_cxml_item.art_id%TYPE,
      a_classification          b2b_cxml_item.CLASSIFICATION%type,
      a_classificationdom       b2b_cxml_item.CLASSIFICATION_DOM%type,
      a_datecreation            b2b_cxml_item.DATE_CREATION%type,
      a_description             b2b_cxml_item.DESCRIPTION%type,
      a_manufacturername        b2b_cxml_item.MANUFACTURER_NAME%type,
      a_manufacturerpartid      b2b_cxml_item.MANUFACTURER_PART_ID%type,
      a_persidcreation          b2b_cxml_item.PERS_ID_CREATION%type,
      a_supplierpartid          b2b_cxml_item.SUPPLIER_PART_ID%type,
      a_supplierPartAuxiliaryId b2b_cxml_item.SUPPLIER_PART_AUXILIARY_ID%type,
      a_unitofmeasure           b2b_cxml_item.UNIT_OF_MEASURE%type,
      a_unitprice               b2b_cxml_item.UNIT_PRICE%type,
      a_url                     b2b_cxml_item.URL%type,
      a_comments                b2b_cxml_item.COMMENTS%type
   ) IS
     
   BEGIN
       
        IF a_bciid IS NULL THEN
           SELECT b2b_cxml_item_seq.NEXTVAL INTO a_bciid FROM dual;
        END IF;

        -- insertion .
        INSERT INTO JEFY_DEPENSE.B2B_CXML_ITEM (
                BCI_ID, 
                FBCP_ID, 
                ART_ID, 
                UNIT_PRICE, 
                DESCRIPTION, 
                SUPPLIER_PART_ID, 
                SUPPLIER_PART_AUXILIARY_ID, 
                UNIT_OF_MEASURE, 
                CLASSIFICATION_DOM, 
                CLASSIFICATION, 
                MANUFACTURER_NAME, 
                MANUFACTURER_PART_ID, 
                URL, 
                COMMENTS,
                PERS_ID_CREATION, 
                DATE_CREATION) 
                VALUES ( 
                        a_bciid,                   --BCI_ID, 
                        a_fbcpid,                  --FBCP_ID, 
                        a_artid,                   --ART_ID, 
                        a_unitprice,                --UNIT_PRICE, 
                        a_description,              --DESCRIPTION, 
                        a_supplierpartid,           --SUPPLIER_PART_ID, 
                        a_supplierpartauxiliaryid,  --SUPPLIER_PART_AUXILIARY_ID, 
                        a_unitofmeasure,            --UNIT_OF_MEASURE, 
                        a_classificationdom,        --CLASSIFICATION_DOM, 
                        a_classification,           --CLASSIFICATION, 
                        a_manufacturername,        --MANUFACTURER_NAME, 
                        a_manufacturerpartid,       --MANUFACTURER_PART_ID, 
                        a_url,                      --URL, 
                        a_comments,                 --COMMENTS 
                        a_persidcreation,         --PERS_ID_CREATION, 
                        a_datecreation             --DATE_CREATION 
                );        
        
        
       

   END;



 PROCEDURE ins_B2bCxmlPunchoutmsg (
      a_bcpoid IN OUT           B2B_CXML_PUNCHOUTMSG.bcpo_id%TYPE,
      a_fbcpid                  B2B_CXML_PUNCHOUTMSG.fbcp_id%TYPE,
      a_commid                  B2B_CXML_PUNCHOUTMSG.comm_id%TYPE,
      a_datecreation            B2B_CXML_PUNCHOUTMSG.DATE_CREATION%type,
      a_persidcreation          B2B_CXML_PUNCHOUTMSG.PERS_ID_CREATION%type,
      a_cxml                    B2B_CXML_PUNCHOUTMSG.cxml%type
   ) IS
     
   BEGIN
       
        IF a_bcpoid IS NULL THEN
           SELECT B2B_CXML_PUNCHOUTMSG_seq.NEXTVAL INTO a_bcpoid FROM dual;
        END IF;
        
        INSERT INTO JEFY_DEPENSE.B2B_CXML_PUNCHOUTMSG (
                BCPO_ID, 
                FBCP_ID, 
                COMM_ID, 
                PERS_ID_CREATION, 
                DATE_CREATION, 
                CXML) 
            VALUES (
                a_bcpoid, -- BCPO_ID, 
                a_fbcpid, -- FBCP_ID, 
                a_commid, --COMM_ID, 
                a_persidcreation, --PERS_ID_CREATION, 
                a_datecreation, -- DATE_CREATION, 
                a_cxml --CXML
                );
        
        
       

   END;
   
   
PROCEDURE ins_B2bCxmlOrderRequest (
      a_bcorid IN OUT           B2B_CXML_ORDERREQUEST.bcor_id%TYPE,
      a_fbcpid                  B2B_CXML_ORDERREQUEST.fbcp_id%TYPE,
      a_commid                  B2B_CXML_ORDERREQUEST.comm_id%TYPE,
      a_datecreation            B2B_CXML_ORDERREQUEST.DATE_CREATION%type,
      a_persidcreation          B2B_CXML_ORDERREQUEST.PERS_ID_CREATION%type,
      a_cxml                    B2B_CXML_ORDERREQUEST.cxml%type,
      a_cxmlreponse            B2B_CXML_ORDERREQUEST.CXML_REPONSE%type
   ) IS
     
   BEGIN
       
        IF a_bcorid IS NULL THEN
           SELECT B2B_CXML_ORDERREQUEST_seq.NEXTVAL INTO a_bcorid FROM dual;
        END IF;
        
        INSERT INTO JEFY_DEPENSE.B2B_CXML_ORDERREQUEST (
                BCOR_ID, 
                FBCP_ID, 
                COMM_ID, 
                PERS_ID_CREATION, 
                DATE_CREATION, 
                CXML,
                CXML_REPONSE) 
            VALUES (
                a_bcorid, -- BCOR_ID, 
                a_fbcpid, -- FBCP_ID, 
                a_commid, --COMM_ID, 
                a_persidcreation, --PERS_ID_CREATION, 
                a_datecreation, -- DATE_CREATION, 
                a_cxml, --CXML
                a_cxmlreponse
                );
  END;
  
  
PROCEDURE del_B2bCxmlItem (a_artid b2b_cxml_item.art_id%TYPE)
is
  BEGIN  
    delete from jefy_depense.b2b_cxml_item where art_id = a_artid;
  end;
     
end;
/