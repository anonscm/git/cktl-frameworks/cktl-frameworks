SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.7
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Suppression du controle portant sur les dépassements de HT ou TTC par rapport à la liquidation initiale lors d'une liquidation sur extourne
-- (provoquait des blocages inutiles en cas de changement de taux de TVA ou de prorata entre la liquidation initiale et la liquidation définitive).
  
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2104';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/


