SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.7
-- Date de publication : 23/01/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Suppression du controle portant sur les dépassements de HT ou TTC par rapport à la liquidation initiale lors d'une liquidation sur extourne
-- (provoquait des blocages inutiles en cas de changement de taux de TVA ou de prorata entre la liquidation initiale et la liquidation définitive).
  
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2106';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/




CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.liquider
is
--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------
   procedure service_fait (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre depense_papier.utl_ordre%type, a_dpp_date_service_fait depense_papier.dpp_date_service_fait%type)
   is
      my_nb               integer;
      my_depense_papier   depense_papier%rowtype;
      my_pers_id          jefy_admin.utilisateur.pers_id%type;
   begin
      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture n''existe pas (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select *
      into   my_depense_papier
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_depense_papier.dpp_date_service_fait is not null then
         raise_application_error (-20001, 'La facture a deja une date de service fait (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select pers_id
      into   my_pers_id
      from   jefy_admin.utilisateur
      where  utl_ordre = a_utl_ordre;

      update depense_papier
         set dpp_date_service_fait = a_dpp_date_service_fait,
             dpp_sf_pers_id = my_pers_id,
             dpp_sf_date = sysdate
       where dpp_id = a_dpp_id;
   end;

   procedure service_fait_et_liquide (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre depense_papier.utl_ordre%type, a_dpp_date_service_fait depense_papier.dpp_date_service_fait%type)
   is
      my_nb        integer;
      my_pdep_id   pdepense_budget.pdep_id%type;

      cursor liste
      is
         select pdep_id
         from   pdepense_budget
         where  dpp_id = a_dpp_id;
   begin
      service_fait (a_dpp_id, a_utl_ordre, a_dpp_date_service_fait);

      select count (*)
      into   my_nb
      from   pdepense_budget
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture n''a pas de pre-liquidation (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      open liste ();

      loop
         fetch liste
         into  my_pdep_id;

         exit when liste%notfound;
         pre_liquider.liquider_pdepense_budget (my_pdep_id, a_utl_ordre);
      end loop;

      close liste;
   end;

   procedure ins_depense_papier (
      a_dpp_id                  in out   depense_papier.dpp_id%type,
      a_exe_ordre                        depense_papier.exe_ordre%type,
      a_dpp_numero_facture               depense_papier.dpp_numero_facture%type,
      a_dpp_ht_initial                   depense_papier.dpp_ht_initial%type,
      a_dpp_ttc_initial                  depense_papier.dpp_ttc_initial%type,
      a_fou_ordre                        depense_papier.fou_ordre%type,
      a_rib_ordre                        depense_papier.rib_ordre%type,
      a_mod_ordre                        depense_papier.mod_ordre%type,
      a_dpp_date_facture                 depense_papier.dpp_date_facture%type,
      a_dpp_date_saisie                  depense_papier.dpp_date_saisie%type,
      a_dpp_date_reception               depense_papier.dpp_date_reception%type,
      a_dpp_date_service_fait            depense_papier.dpp_date_service_fait%type,
      a_dpp_nb_piece                     depense_papier.dpp_nb_piece%type,
      a_utl_ordre                        depense_papier.utl_ordre%type,
      a_dpp_id_reversement               depense_papier.dpp_id_reversement%type
   )
   is
   begin
      ins_depense_papier_avec_im (a_dpp_id,
                                  a_exe_ordre,
                                  a_dpp_numero_facture,
                                  a_dpp_ht_initial,
                                  a_dpp_ttc_initial,
                                  a_fou_ordre,
                                  a_rib_ordre,
                                  a_mod_ordre,
                                  a_dpp_date_facture,
                                  a_dpp_date_saisie,
                                  a_dpp_date_reception,
                                  a_dpp_date_service_fait,
                                  a_dpp_nb_piece,
                                  a_utl_ordre,
                                  a_dpp_id_reversement,
                                  null,
                                  null,
                                  null
                                 );
   end;

   procedure ins_depense_papier_avec_im (
      a_dpp_id                  in out   depense_papier.dpp_id%type,
      a_exe_ordre                        depense_papier.exe_ordre%type,
      a_dpp_numero_facture               depense_papier.dpp_numero_facture%type,
      a_dpp_ht_initial                   depense_papier.dpp_ht_initial%type,
      a_dpp_ttc_initial                  depense_papier.dpp_ttc_initial%type,
      a_fou_ordre                        depense_papier.fou_ordre%type,
      a_rib_ordre                        depense_papier.rib_ordre%type,
      a_mod_ordre                        depense_papier.mod_ordre%type,
      a_dpp_date_facture                 depense_papier.dpp_date_facture%type,
      a_dpp_date_saisie                  depense_papier.dpp_date_saisie%type,
      a_dpp_date_reception               depense_papier.dpp_date_reception%type,
      a_dpp_date_service_fait            depense_papier.dpp_date_service_fait%type,
      a_dpp_nb_piece                     depense_papier.dpp_nb_piece%type,
      a_utl_ordre                        depense_papier.utl_ordre%type,
      a_dpp_id_reversement               depense_papier.dpp_id_reversement%type,
      a_dpp_im_taux                      depense_papier.dpp_im_taux%type,
      a_dpp_im_dgp                       depense_papier.dpp_im_dgp%type,
      imtt_id                            depense_papier.imtt_id%type
   )
   is
   begin
      ins_depense_papier_avec_im_sf (a_dpp_id,
                                     a_exe_ordre,
                                     a_dpp_numero_facture,
                                     a_dpp_ht_initial,
                                     a_dpp_ttc_initial,
                                     a_fou_ordre,
                                     a_rib_ordre,
                                     a_mod_ordre,
                                     a_dpp_date_facture,
                                     a_dpp_date_saisie,
                                     a_dpp_date_reception,
                                     a_dpp_date_service_fait,
                                     a_dpp_nb_piece,
                                     a_utl_ordre,
                                     a_dpp_id_reversement,
                                     a_dpp_im_taux,
                                     a_dpp_im_dgp,
                                     imtt_id,
                                     null,
                                     null,
                                     null
                                    );
   end;

   procedure ins_depense_papier_avec_im_sf (
      a_dpp_id                  in out   depense_papier.dpp_id%type,
      a_exe_ordre                        depense_papier.exe_ordre%type,
      a_dpp_numero_facture               depense_papier.dpp_numero_facture%type,
      a_dpp_ht_initial                   depense_papier.dpp_ht_initial%type,
      a_dpp_ttc_initial                  depense_papier.dpp_ttc_initial%type,
      a_fou_ordre                        depense_papier.fou_ordre%type,
      a_rib_ordre                        depense_papier.rib_ordre%type,
      a_mod_ordre                        depense_papier.mod_ordre%type,
      a_dpp_date_facture                 depense_papier.dpp_date_facture%type,
      a_dpp_date_saisie                  depense_papier.dpp_date_saisie%type,
      a_dpp_date_reception               depense_papier.dpp_date_reception%type,
      a_dpp_date_service_fait            depense_papier.dpp_date_service_fait%type,
      a_dpp_nb_piece                     depense_papier.dpp_nb_piece%type,
      a_utl_ordre                        depense_papier.utl_ordre%type,
      a_dpp_id_reversement               depense_papier.dpp_id_reversement%type,
      a_dpp_im_taux                      depense_papier.dpp_im_taux%type,
      a_dpp_im_dgp                       depense_papier.dpp_im_dgp%type,
      a_imtt_id                          depense_papier.imtt_id%type,
      a_dpp_sf_pers_id                   depense_papier.dpp_sf_pers_id%type,
      a_dpp_sf_date                      depense_papier.dpp_sf_date%type,
      a_ecd_ordre                        depense_papier.ecd_ordre%type
   )
   is
      my_dpp_ht_initial    depense_papier.dpp_ht_initial%type;
      my_dpp_tva_initial   depense_papier.dpp_tva_initial%type;
      my_dpp_ttc_initial   depense_papier.dpp_ttc_initial%type;
      my_dpp_sf_pers_id    depense_papier.dpp_sf_pers_id%type;
      my_dpp_sf_date       depense_papier.dpp_sf_date%type;
      my_nb_decimales      number;
      my_nb                integer;
   begin
      -- verifier qu'on a le droit de liquider sur cet exercice.
      --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);
      verifier.verifier_rib (a_fou_ordre, a_rib_ordre, a_mod_ordre, a_exe_ordre);
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
      my_dpp_ht_initial := round (a_dpp_ht_initial, my_nb_decimales);
      my_dpp_ttc_initial := round (a_dpp_ttc_initial, my_nb_decimales);

      -- si les montants sont negatifs ou si il y a un dpp_id_reversement (c'est un ORV) -> package reverser.
      if my_dpp_ht_initial < 0 or my_dpp_ttc_initial < 0 or a_dpp_id_reversement is not null then
         raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
      end if;

      -- on verifie la coherence des montants.
      if abs (my_dpp_ht_initial) > abs (my_dpp_ttc_initial) then
         raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
      end if;

      -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
      my_dpp_tva_initial := liquider_outils.get_tva (my_dpp_ht_initial, my_dpp_ttc_initial);

      -- enregistrement dans la table.
      if a_dpp_id is null then
         select depense_papier_seq.nextval
         into   a_dpp_id
         from   dual;
      end if;

      my_dpp_sf_pers_id := a_dpp_sf_pers_id;
      my_dpp_sf_date := a_dpp_sf_date;

      if a_dpp_date_service_fait is not null then
         if a_dpp_sf_pers_id is null then
            select pers_id
            into   my_dpp_sf_pers_id
            from   v_utilisateur
            where  utl_ordre = a_utl_ordre;
         end if;

         if a_dpp_sf_date is null then
            select sysdate
            into   my_dpp_sf_date
            from   dual;
         end if;
      end if;

      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         insert into depense_papier
         values      (a_dpp_id,
                      a_exe_ordre,
                      a_dpp_numero_facture,
                      0,
                      0,
                      0,
                      a_fou_ordre,
                      a_rib_ordre,
                      a_mod_ordre,
                      a_dpp_date_facture,
                      a_dpp_date_saisie,
                      a_dpp_date_reception,
                      a_dpp_date_service_fait,
                      a_dpp_nb_piece,
                      a_utl_ordre,
                      a_dpp_id_reversement,
                      my_dpp_ht_initial,
                      my_dpp_tva_initial,
                      my_dpp_ttc_initial,
                      a_dpp_im_taux,
                      a_dpp_im_dgp,
                      a_imtt_id,
                      my_dpp_sf_pers_id,
                      my_dpp_sf_date,
                      a_ecd_ordre
                     );
      else
         update depense_papier
            set dpp_numero_facture = a_dpp_numero_facture,
                rib_ordre = a_rib_ordre,
                mod_ordre = a_mod_ordre,
                dpp_date_facture = a_dpp_date_facture,
                dpp_date_reception = a_dpp_date_reception,
                dpp_date_service_fait = a_dpp_date_service_fait,
                dpp_nb_piece = a_dpp_nb_piece,
                utl_ordre = a_utl_ordre,
                dpp_id_reversement = a_dpp_id_reversement,
                dpp_ht_initial = my_dpp_ht_initial,
                dpp_tva_initial = my_dpp_tva_initial,
                dpp_ttc_initial = my_dpp_ttc_initial,
                dpp_im_taux = a_dpp_im_taux,
                dpp_im_dgp = a_dpp_im_dgp,
                imtt_id = a_imtt_id,
                dpp_sf_pers_id = my_dpp_sf_pers_id,
                dpp_sf_date = my_dpp_sf_date,
                dpp_date_saisie = sysdate,
                ecd_ordre = a_ecd_ordre
          where dpp_id = a_dpp_id;
      end if;
   end;

   procedure ins_commande_dep_papier (a_cdp_id in out commande_dep_papier.cdp_id%type, a_comm_id commande_dep_papier.comm_id%type, a_dpp_id commande_dep_papier.dpp_id%type)
   is
      my_nb              integer;
      my_dpp_exe_ordre   depense_papier.exe_ordre%type;
      my_cde_exe_ordre   commande.exe_ordre%type;
   begin
      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture n''existe pas (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select count (*)
      into   my_nb
      from   commande
      where  comm_id = a_comm_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La commande n''existe pas (' || indication_erreur.commande (a_comm_id) || ')');
      end if;

      select count (*)
      into   my_nb
      from   commande_dep_papier
      where  dpp_id = a_dpp_id;

      if my_nb > 0 then
         raise_application_error (-20001, 'La facture est deja utilise pour une commande (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select exe_ordre
      into   my_cde_exe_ordre
      from   commande
      where  comm_id = a_comm_id;

      if my_dpp_exe_ordre <> my_cde_exe_ordre then
         raise_application_error (-20001, 'La facture et la commande ne sont pas sur le meme exercice (' || indication_erreur.dep_papier (a_dpp_id) || ', ' || indication_erreur.commande (a_comm_id) || ')');
      end if;

      -- si pas de probleme on insere.
      if a_cdp_id is null then
         select commande_dep_papier_seq.nextval
         into   a_cdp_id
         from   dual;
      end if;

      insert into commande_dep_papier
      values      (a_cdp_id,
                   a_comm_id,
                   a_dpp_id
                  );
   end;

   procedure ins_depense (
      a_dep_id               in out   depense_budget.dep_id%type,
      a_exe_ordre                     depense_budget.exe_ordre%type,
      a_dpp_id                        depense_budget.dpp_id%type,
      a_eng_id                        depense_budget.eng_id%type,
      a_dep_ht_saisie                 depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie                depense_budget.dep_ttc_saisie%type,
      a_tap_id                        depense_budget.tap_id%type,
      a_utl_ordre                     depense_budget.utl_ordre%type,
      a_dep_id_reversement            depense_budget.dep_id_reversement%type,
      a_chaine_action                 varchar2,
      a_chaine_analytique             varchar2,
      a_chaine_convention             varchar2,
      a_chaine_hors_marche            varchar2,
      a_chaine_marche                 varchar2,
      a_chaine_planco                 varchar2
   )
   is
      my_org_id           engage_budget.org_id%type;
      my_nb_decimales     number;
      my_dep_ht_saisie    depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie   depense_budget.dep_ttc_saisie%type;
   begin
      if a_dep_ttc_saisie <> 0 then
         my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
         my_dep_ht_saisie := round (a_dep_ht_saisie, my_nb_decimales);
         my_dep_ttc_saisie := round (a_dep_ttc_saisie, my_nb_decimales);

         select org_id
         into   my_org_id
         from   engage_budget
         where  eng_id = a_eng_id;

         -- verifier qu'on a le droit de liquider sur cet exercice.
         verifier.verifier_depense_exercice (a_exe_ordre, a_utl_ordre, my_org_id);
         -- lancement des differentes procedures d'insertion des tables de depense.
         ins_depense_budget (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

         update depense_papier
            set dpp_ht_saisie = dpp_ht_saisie + my_dep_ht_saisie,
                dpp_tva_saisie = dpp_tva_saisie + my_dep_ttc_saisie - my_dep_ht_saisie,
                dpp_ttc_saisie = dpp_ttc_saisie + my_dep_ttc_saisie
          where dpp_id = a_dpp_id;

         -- on le passe en premier car utilis? par les autres pour les upd_engage_reste_.
         ins_depense_ctrl_planco (a_exe_ordre, a_dep_id, a_chaine_planco);
         ins_depense_ctrl_action (a_exe_ordre, a_dep_id, a_chaine_action);
         ins_depense_ctrl_analytique (a_exe_ordre, a_dep_id, a_chaine_analytique);
         ins_depense_ctrl_convention (a_exe_ordre, a_dep_id, a_chaine_convention);
         ins_depense_ctrl_hors_marche (a_exe_ordre, a_dep_id, a_chaine_hors_marche);
         ins_depense_ctrl_marche (a_exe_ordre, a_dep_id, a_chaine_marche);
         --Corriger.upd_engage_reste(a_eng_id);

         -- on verifie la coherence des montants entre les differents depense_.
         verifier.verifier_depense_coherence (a_dep_id);
         verifier.verifier_depense_pap_coherence (a_dpp_id);
         -- on verifie si la coherence des montants budgetaires restant est  conservee.
         verifier.verifier_engage_coherence (a_eng_id);
      end if;
   end;

   procedure ins_depense_extourne (
      a_dep_id              in out   depense_budget.dep_id%type,
      a_exe_ordre                    depense_budget.exe_ordre%type,
      a_dpp_id                       depense_budget.dpp_id%type,
      a_eng_id              in out   depense_budget.eng_id%type,
      a_dep_ht_saisie                depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie               depense_budget.dep_ttc_saisie%type,
      a_fou_ordre                    engage_budget.fou_ordre%type,
      a_org_id                       engage_budget.org_id%type,
      a_tcd_ordre                    engage_budget.tcd_ordre%type,
      a_tap_id                       depense_budget.tap_id%type,
      a_eng_libelle                  engage_budget.eng_libelle%type,
      a_tyap_id                      engage_budget.tyap_id%type,
      a_utl_ordre                    depense_budget.utl_ordre%type,
      --a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      --a_chaine_action        VARCHAR2,
      a_chaine_analytique            varchar2,
      a_chaine_convention            varchar2,
      --a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche                varchar2,
      a_chaine_planco                varchar2
   )
   is
      my_eng_numero        engage_budget.eng_numero%type;
      my_org_id            engage_budget.org_id%type;
      my_nb_decimales      number;
      my_dep_ht_saisie     depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie    depense_budget.dep_ttc_saisie%type;
      my_eng_ht_saisie     engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie    engage_budget.eng_ttc_saisie%type;
      my_dep_id_initiale   depense_budget.dep_id%type;
      my_eld_id            extourne_liq_def.eld_id%type;
      my_el_id             extourne_liq.el_id%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
      my_dep_ht_saisie := round (a_dep_ht_saisie, my_nb_decimales);
      my_dep_ttc_saisie := round (a_dep_ttc_saisie, my_nb_decimales);

      -- si pas d''engagement prealable c'est qu'on consomme sur poche, donc on en cree un
      if a_eng_id is null then
         if a_fou_ordre is null or a_org_id is null or a_tcd_ordre is null or a_tap_id is null then
            raise_application_error (-20001, 'il faut un fournisseur, une ligne budgetaire, un type de credit et un taux de prorata');
         end if;

         -- quid exercice du tcd_ordre et dates ouverture org_id ????
         -- autre probleme, dans la procedure de creation d'engagement si montant>0 alors engagement normal donc creer procedure engagement_extourne_poche
         engager.ins_engage_extourne_poche (a_eng_id, a_exe_ordre, my_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, a_dep_ht_saisie, a_dep_ttc_saisie, a_tyap_id, a_utl_ordre);
      end if;

      select eng_ht_saisie,
             eng_ttc_saisie,
             org_id
      into   my_eng_ht_saisie,
             my_eng_ttc_saisie,
             my_org_id
      from   engage_budget
      where  eng_id = a_eng_id;

    -- on ne controle pas les montants HT et TTC de la liquidation definitive par rapport à ceux de la liquidation d'extourne
    -- car dans le cas d'un changement de taux de tva ou de taux de prorata
    -- entre la liquidation initiale et la liquidation definitive
    -- les HT et TTC sont recalculés a partir du montant budgetaire
    -- et peuvent depasser ceux de la liquidation d'extourne
--      if my_eng_ht_saisie < 0 or my_eng_ttc_saisie < 0 then
--         if abs (my_eng_ht_saisie) < a_dep_ht_saisie then
--            raise_application_error (-20001, 'Le montant HT de la depense (' || to_char (a_dep_ht_saisie) || ') depasse celui du credit d''extourne associe (' || to_char (abs (my_eng_ht_saisie)) || ')');
--         end if;

--         if abs (my_eng_ttc_saisie) < a_dep_ttc_saisie then
--            raise_application_error (-20001, 'Le montant TTC de la depense (' || to_char (a_dep_ttc_saisie) || ') depasse celui du credit d''extourne associe (' || to_char (abs (my_eng_ttc_saisie)) || ')');
--         end if;
--      end if;

      -- verifier qu'on a le droit de liquider sur cet exercice.
      -- Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

      -- c'est une liquidation sur credit extourne fleche
      if my_eng_ttc_saisie < 0 then
         -- on memorise la reference a la liquidation d''extourne avant d'inserer la nouvelle depensebudget
         select max (dep_id)
         into   my_dep_id_initiale
         from   depense_budget
         where  eng_id = a_eng_id and dep_ttc_saisie < 0;

         select max (el_id)
         into   my_el_id
         from   extourne_liq
         where  dep_id_n1 = my_dep_id_initiale;
      end if;

      -- lancement des differentes procedures d'insertion des tables de depense.
      ins_depense_budget (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre, null);

      update depense_papier
         set dpp_ht_saisie = dpp_ht_saisie + my_dep_ht_saisie,
             dpp_tva_saisie = dpp_tva_saisie + my_dep_ttc_saisie - my_dep_ht_saisie,
             dpp_ttc_saisie = dpp_ttc_saisie + my_dep_ttc_saisie
       where dpp_id = a_dpp_id;

      -- on le passe en premier car utilise par les autres pour les upd_engage_reste_.
      ins_depense_ctrl_planco (a_exe_ordre, a_dep_id, a_chaine_planco);
      --ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
      ins_depense_ctrl_analytique (a_exe_ordre, a_dep_id, a_chaine_analytique);
      ins_depense_ctrl_convention (a_exe_ordre, a_dep_id, a_chaine_convention);
      --ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
      ins_depense_ctrl_marche (a_exe_ordre, a_dep_id, a_chaine_marche);

      ------ on met a jour les tables d'extourne
      select extourne_liq_def_seq.nextval
      into   my_eld_id
      from   dual;

      insert into extourne_liq_def
         select my_eld_id,
                a_dep_id,
                get_type_etat ('sur extourne')
         from   dual;

      -- c'est une liquidation sur credit extourne fleche
      if my_eng_ttc_saisie < 0 then
--        select max(dep_id) into my_dep_id_initiale from depense_budget where eng_id=a_eng_id;
--        select max(el_id) into my_el_id from extourne_liq where dep_id_n1=my_dep_id_initiale;
         insert into extourne_liq_repart
            select extourne_liq_repart_seq.nextval,
                   my_el_id,
                   my_eld_id
            from   dual;
      end if;

      -- on verifie la coherence des montants entre les differents depense_.
      verifier.verifier_depense_coherence (a_dep_id);
      verifier.verifier_depense_pap_coherence (a_dpp_id);
     -- on verifie si la coherence des montants budgetaires restant est  conservee.
     -- verif inutile pour extourne
--     Verifier.verifier_engage_coherence(a_eng_id);
   end;

   procedure ins_depense_directe (
      a_dep_id               in out   depense_budget.dep_id%type,
      a_exe_ordre                     depense_budget.exe_ordre%type,
      a_dpp_id                        depense_budget.dpp_id%type,
      a_dep_ht_saisie                 depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie                depense_budget.dep_ttc_saisie%type,
      a_fou_ordre                     engage_budget.fou_ordre%type,
      a_org_id                        engage_budget.org_id%type,
      a_tcd_ordre                     engage_budget.tcd_ordre%type,
      a_tap_id                        depense_budget.tap_id%type,
      a_eng_libelle                   engage_budget.eng_libelle%type,
      a_tyap_id                       engage_budget.tyap_id%type,
      a_utl_ordre                     depense_budget.utl_ordre%type,
      a_dep_id_reversement            depense_budget.dep_id_reversement%type,
      a_chaine_action                 varchar2,
      a_chaine_analytique             varchar2,
      a_chaine_convention             varchar2,
      a_chaine_hors_marche            varchar2,
      a_chaine_marche                 varchar2,
      a_chaine_planco                 varchar2
   )
   is
      my_eng_id            engage_budget.eng_id%type;
      my_eng_numero        engage_budget.eng_numero%type;
      my_chaine_planco     varchar2 (30000);
      my_chaine            varchar2 (30000);
      my_pco_num           depense_ctrl_planco.pco_num%type;
      my_dpco_ht_saisie    depense_ctrl_planco.dpco_ht_saisie%type;
      my_dpco_ttc_saisie   depense_ctrl_planco.dpco_ttc_saisie%type;
   begin
      my_eng_id := null;
      my_eng_numero := null;
      -- on retravaille la chaine_planco, car c'est la seule qui differencie pour les parametres d'ins_engage
      my_chaine_planco := '';
      my_chaine := a_chaine_planco;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'imputation.
         select substr (my_chaine, 1, instr (my_chaine, '$') - 1)
         into   my_pco_num
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         -- on enleve l'ecriture.
         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         -- on enleve la chaine des inventaires.
         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_chaine_planco := my_chaine_planco || my_pco_num || '$' || my_dpco_ht_saisie || '$' || my_dpco_ttc_saisie || '$';
      end loop;

      my_chaine_planco := my_chaine_planco || '$';
      -- on cree l'engagement
      engager.ins_engage (my_eng_id,
                          a_exe_ordre,
                          my_eng_numero,
                          a_fou_ordre,
                          a_org_id,
                          a_tcd_ordre,
                          a_tap_id,
                          a_eng_libelle,
                          a_dep_ht_saisie,
                          a_dep_ttc_saisie,
                          a_tyap_id,
                          a_utl_ordre,
                          a_chaine_action,
                          a_chaine_analytique,
                          a_chaine_convention,
                          a_chaine_hors_marche,
                          a_chaine_marche,
                          my_chaine_planco
                         );

      if a_dep_ttc_saisie < 0 then
         reverser.ins_reverse (a_dep_id,
                               a_exe_ordre,
                               a_dpp_id,
                               my_eng_id,
                               a_dep_ht_saisie,
                               a_dep_ttc_saisie,
                               a_tap_id,
                               a_utl_ordre,
                               a_dep_id_reversement,
                               a_chaine_action,
                               a_chaine_analytique,
                               a_chaine_convention,
                               a_chaine_hors_marche,
                               a_chaine_marche,
                               a_chaine_planco
                              );
      else
         -- on cree la depense
         ins_depense (a_dep_id,
                      a_exe_ordre,
                      a_dpp_id,
                      my_eng_id,
                      a_dep_ht_saisie,
                      a_dep_ttc_saisie,
                      a_tap_id,
                      a_utl_ordre,
                      a_dep_id_reversement,
                      a_chaine_action,
                      a_chaine_analytique,
                      a_chaine_convention,
                      a_chaine_hors_marche,
                      a_chaine_marche,
                      a_chaine_planco
                     );
      end if;
   end;

   procedure del_depense_budget (a_dep_id depense_budget.dep_id%type, a_utl_ordre z_depense_budget.zdep_utl_ordre%type)
   is
      my_nb                        integer;
      my_reim_id                   integer;
      my_eng_id                    engage_budget.eng_id%type;
      my_zdep_id                   z_depense_budget.zdep_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_montant_budgetaire        depense_budget.dep_montant_budgetaire%type;
      my_dep_total_ht              depense_budget.dep_ht_saisie%type;
      my_dep_total_ttc             depense_budget.dep_ttc_saisie%type;
      my_dep_total_bud             depense_budget.dep_montant_budgetaire%type;
      my_eng_montant_bud           engage_budget.eng_montant_budgetaire%type;
      my_eng_montant_bud_reste     engage_budget.eng_montant_budgetaire_reste%type;
      my_reste                     engage_budget.eng_montant_budgetaire_reste%type;
      my_dispo_ligne               v_budget_exec_credit.bdxc_disponible%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_dep_ht_saisie             depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie            depense_budget.dep_ttc_saisie%type;
      my_eng_tap_id                engage_budget.tap_id%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_dpp_id                    depense_budget.dpp_id%type;
      my_comm_id                   commande.comm_id%type;
      my_dpco_id                   depense_ctrl_planco.dpco_id%type;
      my_eng_ht_saisie             engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie            engage_budget.eng_ttc_saisie%type;
      my_nb_extourne               integer;
      my_taux_tva                  number (5, 3);
      my_tap_taux                  jefy_admin.taux_prorata.tap_taux%type;
      my_new_ht                    engage_budget.eng_ht_saisie%type;
      my_new_ttc                   engage_budget.eng_ttc_saisie%type;

      cursor liste
      is
         select dpco_id
         from   depense_ctrl_planco
         where  dep_id = a_dep_id;

      isliquidationfromcarambole   smallint;
      isliquidationdirecte         smallint;
      isliqsurextourneflechee      smallint;
      wantsupprimerengagement      smallint;
   begin
      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La liquidation n''existe pas ou est deja annulee (dep_id:' || a_dep_id || ')');
      end if;

      -- on memorise les differents montants avant suppression
      select e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.dep_ht_saisie,
             d.dep_ttc_saisie,
             e.eng_ht_saisie,
             e.eng_ttc_saisie,
             d.dep_montant_budgetaire,
             e.eng_id,
             e.tap_id,
             e.eng_montant_budgetaire,
             e.eng_montant_budgetaire_reste,
             d.dpp_id,
             d.tap_id
      into   my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_dep_ht_saisie,
             my_dep_ttc_saisie,
             my_eng_ht_saisie,
             my_eng_ttc_saisie,
             my_montant_budgetaire,
             my_eng_id,
             my_eng_tap_id,
             my_eng_montant_bud,
             my_eng_montant_bud_reste,
             my_dpp_id,
             my_dep_tap_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      dbms_output.put_line ('a_dep_id=' || a_dep_id);
      dbms_output.put_line ('my_dep_ht_saisie=' || my_dep_ht_saisie);
      dbms_output.put_line ('my_dep_ttc_saisie=' || my_dep_ttc_saisie);
      dbms_output.put_line ('my_montant_budgetaire=' || my_montant_budgetaire);
      dbms_output.put_line ('my_eng_montant_bud=' || my_eng_montant_bud);
      dbms_output.put_line ('my_eng_montant_bud_reste=' || my_eng_montant_bud_reste);
      dbms_output.put_line ('my_eng_ht_saisie=' || my_eng_ht_saisie);
      dbms_output.put_line ('my_eng_ttc_saisie=' || my_eng_ttc_saisie);

      -- on teste si ce n'est pas un ORV.
      if my_montant_budgetaire < 0 then
         raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
      end if;

      -- verifier qu'on a le droit de liquider sur cet exercice.
      verifier.verifier_depense_exercice (my_exe_ordre, a_utl_ordre, my_org_id);
      verifier.verifier_util_depense_budget (a_dep_id);

      -- on met a jour les montants de la depense papier (en enlevant ceux de la depense_budget qu'on supprime)
      update depense_papier
         set dpp_ht_saisie = dpp_ht_saisie - my_dep_ht_saisie,
             dpp_tva_saisie = dpp_tva_saisie - my_dep_ttc_saisie + my_dep_ht_saisie,
             dpp_ttc_saisie = dpp_ttc_saisie - my_dep_ttc_saisie
       where dpp_id = my_dpp_id;

      -- est-ce qu'on est sur une liquidation passee sur un engagement d'extourne N+1
      select count (*)
      into   my_nb_extourne
      from   extourne_liq
      where  dep_id_n1 in (select dep_id
                           from   depense_budget
                           where  eng_id = my_eng_id);

      isliquidationfromcarambole := 0;
      isliquidationdirecte := is_dep_directe (a_dep_id);

      if (get_app_dep (a_dep_id) = 'DEPENSE') then
         isliquidationfromcarambole := 1;
      end if;

      -- si liquidation sur extourne flechee on ne met a jour que les montants HT et TTC
      isliqsurextourneflechee := is_dep_sur_extourne_flechee (a_dep_id);

      -- si liquidation directe de carambole (extourne ou pas), alors on supprime l'engagement
      if (isliquidationdirecte > 0 and isliquidationfromcarambole > 0) then
         dbms_output.put_line ('CAS liquidation directe carambole (extourne ou non)');
         wantsupprimerengagement := 1;
      -- on ne cherche pas à mettre à jour les montants de l'engagement
      else
         wantsupprimerengagement := 0;

          -- si on est sur une liquidation passee sur un engagement d'extourne N+1
          -- on recalcule les montants HT et TTC de l'engagement à partir de ce qui reste liquidé
          -- dans le cas de l'extourne les montants budgétaire sont toujours à 0 car ils n'impactent pas le budget
         -- if my_eng_ttc_saisie<0 or my_nb_extourne>0 then
         if (isliqsurextourneflechee > 0) then
            dbms_output.put_line ('CAS extourne flechee');

            -- on calcule le montant budgetaire de l'engagement sans la depense qu'on supprime
            select nvl (sum (budget.calculer_budgetaire (my_exe_ordre, my_eng_tap_id, my_org_id, dep_ht_saisie, dep_ttc_saisie)), 0)
            into   my_dep_total_bud
            from   depense_budget
            where  dep_id <> a_dep_id and eng_id = my_eng_id;

            -- si le taux de prorata est le meme entre la depense et l'engagement et que
            -- le ttc et le HT de l'engagement sont à zero
            -- (ROD : donc on est forcement dans le cas my_nb_extourne>0)
            -- ROD : dans quels cas on peut avoir my_eng_ttc_saisie=0 and my_eng_ht_saisie=0 ? )
            if my_eng_tap_id = my_dep_tap_id and my_eng_ttc_saisie = 0 and my_eng_ht_saisie = 0 then
               dbms_output.put_line ('on ne devrait pas passer par la ?');

               update engage_budget
                  set eng_ht_saisie = -my_dep_ht_saisie,
                      eng_tva_saisie = -my_dep_ttc_saisie + my_dep_ht_saisie,
                      eng_ttc_saisie = -my_dep_ttc_saisie
                where eng_id = my_eng_id;
            else
               --- VERIFIER SI IL N'Y A PAS UN PB DE CENTIME (arrondi, prorata ...)
               select tap_taux
               into   my_tap_taux
               from   jefy_admin.taux_prorata
               where  tap_id = my_eng_tap_id;

               dbms_output.put_line (' my_tap_taux=' || my_tap_taux);

               if my_eng_ttc_saisie = 0 and my_eng_ht_saisie = 0 then
                  my_taux_tva := my_dep_ttc_saisie / my_dep_ht_saisie - 1;
               else
                  my_taux_tva := my_eng_ttc_saisie / my_eng_ht_saisie - 1;
               end if;

               dbms_output.put_line ('my_taux_tva=' || my_taux_tva);
               my_new_ht := round ((my_dep_total_bud) / (1 + (my_taux_tva * (1 - (my_tap_taux / 100)))), liquider_outils.get_nb_decimales (my_exe_ordre));
               --setScale(2, BigDecimal.ROUND_HALF_DOWN);
               my_new_ttc := round (my_new_ht * (1 + my_taux_tva), liquider_outils.get_nb_decimales (my_exe_ordre));   --.setScale(2, BigDecimal.ROUND_HALF_DOWN);
               dbms_output.put_line ('my_new_ht=' || my_new_ht);
               dbms_output.put_line ('my_dep_total_bud=' || my_dep_total_bud);
               dbms_output.put_line ('my_new_ttc=' || my_new_ttc);

               -- on ne touche pas au motant budgétaire qui doit rester à 0
               update engage_budget
                  set eng_ht_saisie = my_new_ht,
                      eng_ttc_saisie = my_new_ttc,
                      eng_tva_saisie = my_new_ttc - my_new_ht
                where eng_id = my_eng_id;
            end if;
         else
            -- si on est sur une liquidation passee sur un engagement NON d'extourne N+1 (ou ORV ???)
            -- on met a jour le reste engagé (on ne touche pas aux montants HT et TTC)
            dbms_output.put_line ('Cas NON extourne');

            -- on recupere les montants des autres factures et ORV.
            select nvl (sum (budget.calculer_budgetaire (my_exe_ordre, my_eng_tap_id, my_org_id, dep_ht_saisie, dep_ttc_saisie)), 0)
            into   my_dep_total_bud
            from   depense_budget
            where  dep_id <> a_dep_id and dep_id in (select dep_id
                                                     from   depense_ctrl_planco
                                                     where  dep_id in (select dep_id
                                                                       from   depense_budget
                                                                       where  eng_id = my_eng_id) and (dpco_montant_budgetaire > 0                                /*OR man_id IN
                                                                                                                                   (SELECT man_id FROM v_mandat WHERE man_etat IN ('VISE','PAYE'))*/));

            -- on calcule le reste de l'engagement
            my_reste := my_eng_montant_bud - my_dep_total_bud;

            if my_reste < 0 then
               my_reste := 0;
            end if;

            dbms_output.put_line ('my_reste (budgetaire) = ' || my_reste);
            -- on compare au dispo de la ligne budgetaire ... pour voir si on peut tt reengager.
            my_dispo_ligne := my_montant_budgetaire + budget.engager_disponible (my_exe_ordre, my_org_id, my_tcd_ordre);
            dbms_output.put_line (' my_dispo_ligne = ' || my_dispo_ligne);

            if my_reste > my_dispo_ligne + my_eng_montant_bud_reste then
               my_reste := my_dispo_ligne + my_eng_montant_bud_reste;
            end if;

            dbms_output.put_line ('my_reste (budgetaire) = ' || my_reste);

            -- on modifie l'engagement.
            update engage_budget
               set eng_montant_budgetaire_reste = my_reste
             where eng_id = my_eng_id;
         end if;
      end if;

      -- tout est bon ... on supprime la depense.
      log_depense_budget (a_dep_id, a_utl_ordre);

      delete from depense_ctrl_action
            where dep_id = a_dep_id;

      delete from depense_ctrl_analytique
            where dep_id = a_dep_id;

      delete from depense_ctrl_convention
            where dep_id = a_dep_id;

      delete from depense_ctrl_hors_marche
            where dep_id = a_dep_id;

      delete from depense_ctrl_marche
            where dep_id = a_dep_id;

      open liste ();

      loop
         fetch liste
         into  my_dpco_id;

         exit when liste%notfound;
         jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);

         delete from depense_ctrl_planco
               where dpco_id = my_dpco_id;
      end loop;

      close liste;

      -- Suppression des reimputations associees (Dont celles effectuees dans Kiwi)
      select count (*)
      into   my_nb
      from   reimputation
      where  dep_id = a_dep_id;

      if my_nb > 0 then
         jefy_mission.kiwi_paiement.del_reimputation (a_dep_id);

         delete from reimputation_action
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_analytique
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_budget
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_convention
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_hors_marche
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_marche
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_planco
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation
               where dep_id = a_dep_id;
      end if;

      delete from extourne_liq_repart
            where eld_id in (select eld_id
                             from   extourne_liq_def
                             where  dep_id = a_dep_id);

      delete from extourne_liq_def
            where dep_id = a_dep_id;

      delete from depense_budget
            where dep_id = a_dep_id;

      if (wantsupprimerengagement > 0) then
         -- on supprime l'engagement (le budget est mis a jour dans la procedure)
         dbms_output.put_line ('Suppression de l''engagement eng_id='||my_eng_id);
         engager.del_engage (my_eng_id, a_utl_ordre);
      else
         if my_eng_ttc_saisie >= 0 then
            dbms_output.put_line ('Mise a jour du budget');
            budget.maj_budget (my_exe_ordre, my_org_id, my_tcd_ordre);
            dbms_output.put_line ('Correction reste engage');
            corriger.upd_engage_reste (my_eng_id);
         end if;

         -- on verifie si la coherence des montants budgetaires restant est  conservee.
         dbms_output.put_line ('Verification si coherence des montants budgetaires restant est conservee');
         verifier.verifier_engage_coherence (my_eng_id);
         verifier.verifier_depense_pap_coherence (my_dpp_id);
      end if;

      apres_liquide.del_depense_budget (my_eng_id, a_dep_id);
   end;

   procedure del_depense_papier (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre z_depense_papier.zdpp_utl_ordre%type)
   is
      my_nb                integer;
      my_exe_ordre         depense_papier.exe_ordre%type;
      my_dpp_ttc_initial   depense_papier.dpp_ttc_initial%type;
   begin
      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:' || a_dpp_id || ')');
      end if;

      select exe_ordre,
             dpp_ttc_initial
      into   my_exe_ordre,
             my_dpp_ttc_initial
      from   depense_papier
      where  dpp_id = a_dpp_id;

      -- on teste si ce n'est pas un ORV.
      if my_dpp_ttc_initial < 0 then
         raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
      end if;

      -- verifier qu'on a le droit de liquider sur cet exercice.
      --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);
      verifier.verifier_util_depense_papier (a_dpp_id);
      log_depense_papier (a_dpp_id, a_utl_ordre);

      delete from commande_dep_papier
            where dpp_id = a_dpp_id;

      delete from depense_papier
            where dpp_id = a_dpp_id;

      apres_liquide.del_depense_papier (a_dpp_id);
   end;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------
   procedure log_depense_budget (a_dep_id depense_budget.dep_id%type, a_utl_ordre z_depense_budget.zdep_utl_ordre%type)
   is
      my_zdep_id   z_depense_budget.zdep_id%type;
   begin
      select z_depense_budget_seq.nextval
      into   my_zdep_id
      from   dual;

      insert into z_depense_budget
         select my_zdep_id,
                sysdate,
                a_utl_ordre,
                e.*
         from   depense_budget e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_action
         select z_depense_ctrl_action_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_action e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_analytique
         select z_depense_ctrl_analytique_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_analytique e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_convention
         select z_depense_ctrl_convention_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_convention e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_hors_marche
         select z_depense_ctrl_hors_marche_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_hors_marche e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_marche
         select z_depense_ctrl_marche_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_marche e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_planco
         select z_depense_ctrl_planco_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_planco e
         where  dep_id = a_dep_id;
   end;

   procedure log_depense_papier (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre z_depense_papier.zdpp_utl_ordre%type)
   is
   begin
      insert into z_depense_papier
         select z_depense_papier_seq.nextval,
                sysdate,
                a_utl_ordre,
                e.*
         from   depense_papier e
         where  dpp_id = a_dpp_id;
   end;

   procedure ins_depense_budget (
      a_dep_id               in out   depense_budget.dep_id%type,
      a_exe_ordre                     depense_budget.exe_ordre%type,
      a_dpp_id                        depense_budget.dpp_id%type,
      a_eng_id                        depense_budget.eng_id%type,
      a_dep_ht_saisie                 depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie                depense_budget.dep_ttc_saisie%type,
      a_tap_id                        depense_budget.tap_id%type,
      a_utl_ordre                     depense_budget.utl_ordre%type,
      --a_der_id             depense_budget.der_id%type,
      a_dep_id_reversement            depense_budget.dep_id_reversement%type
   )
   is
      my_dep_ht_saisie             depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie            depense_budget.dep_ttc_saisie%type;
      my_nb_decimales              number;
      my_nb                        integer;
      my_par_value                 parametre.par_value%type;
      my_org_id                    engage_budget.org_id%type;
      my_tap_id                    engage_budget.tap_id%type;
      my_exe_ordre                 engage_budget.exe_ordre%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_eng_id                    engage_budget.eng_id%type;
      my_montant_budgetaire        depense_budget.dep_montant_budgetaire%type;
      my_eng_budgetaire            engage_budget.eng_montant_budgetaire%type;
      my_eng_reste                 engage_budget.eng_montant_budgetaire_reste%type;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
      my_eng_ht                    engage_budget.eng_ht_saisie%type;
      my_eng_ttc                   engage_budget.eng_ttc_saisie%type;
      my_dep_eng_budgetaire        engage_budget.eng_montant_budgetaire%type;
      my_dpp_ht_initial            depense_papier.dpp_ht_initial%type;
      my_dpp_tva_initial           depense_papier.dpp_tva_initial%type;
      my_dpp_ttc_initial           depense_papier.dpp_ttc_initial%type;
      my_dep_tva_saisie            depense_budget.dep_tva_saisie%type;
      --my_sum_ht_saisie    depense_budget.dep_ht_saisie%type;
      --my_sum_tva_saisie   depense_budget.dep_tva_saisie%type;
      --my_sum_ttc_saisie   depense_budget.dep_ttc_saisie%type;
      my_sum_rev_ht                depense_budget.dep_ht_saisie%type;
      my_sum_rev_ttc               depense_budget.dep_ttc_saisie%type;
      my_dep_id_reversement        depense_budget.dep_id_reversement%type;
      my_dep_ht_init               depense_budget.dep_ht_saisie%type;
      my_dep_ttc_init              depense_budget.dep_ttc_saisie%type;
      my_dpp_exe_ordre             depense_papier.exe_ordre%type;
      my_dep_exe_ordre             depense_budget.exe_ordre%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_eng_negatif_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_extourne_budgetaire   depense_budget.dep_montant_budgetaire%type;
      my_taux_tva                  number (5, 3);
      my_tap_taux                  jefy_admin.taux_prorata.tap_taux%type;
      my_new_ht                    engage_budget.eng_ht_saisie%type;
      my_new_ttc                   engage_budget.eng_ttc_saisie%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
      my_dep_ht_saisie := round (a_dep_ht_saisie, my_nb_decimales);
      my_dep_ttc_saisie := round (a_dep_ttc_saisie, my_nb_decimales);

      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La facture n''existe pas (dpp_id=' || a_dpp_id || ')');
      end if;

      select count (*)
      into   my_nb
      from   engage_budget
      where  eng_id = a_eng_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'L''engagement n''existe pas (eng_id=' || a_eng_id || ')');
      end if;

      select exe_ordre,
             org_id,
             tap_id,
             tcd_ordre,
             eng_montant_budgetaire_reste,
             eng_montant_budgetaire,
             eng_ht_saisie,
             eng_ttc_saisie
      into   my_exe_ordre,
             my_org_id,
             my_tap_id,
             my_tcd_ordre,
             my_eng_reste,
             my_eng_init,
             my_eng_ht,
             my_eng_ttc
      from   engage_budget
      where  eng_id = a_eng_id;

      --extourne
      if (my_eng_ht < 0 or my_eng_ttc < 0) and (my_dep_ht_saisie >= 0 and my_dep_ttc_saisie >= 0) then
         -- on compare les montants budgetaires
         my_eng_negatif_budgetaire := budget.calculer_budgetaire (my_exe_ordre, my_tap_id, my_org_id, my_eng_ht, my_eng_ttc);
         my_dep_extourne_budgetaire := budget.calculer_budgetaire (a_exe_ordre, a_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

         if abs (my_eng_negatif_budgetaire) < my_dep_extourne_budgetaire then
            raise_application_error (-20001, 'Pour une liquidation sur mandat d''extourne on ne peut pas depasser le montant des credits extournes');
         end if;
      else
         if my_dep_ht_saisie < 0 or my_dep_ttc_saisie < 0 or a_dep_id_reversement is not null or my_eng_ttc < 0 then
            raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
         end if;
      end if;

      select exe_ordre,
             dpp_ht_initial,
             dpp_tva_initial,
             dpp_ttc_initial
      into   my_dpp_exe_ordre,
             my_dpp_ht_initial,
             my_dpp_tva_initial,
             my_dpp_ttc_initial
      from   depense_papier
      where  dpp_id = a_dpp_id;

      --    RAISE_APPLICATION_ERROR(-20001,'On ne peut pas liquider sur un engagement d''extourne');

      -- verification de la coherence de l'exercice.
      if a_exe_ordre <> my_exe_ordre then
         raise_application_error (-20001, 'La facture doit etre sur le meme exercice que l''engagement');
      end if;

      if a_exe_ordre <> my_dpp_exe_ordre then
         raise_application_error (-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
      end if;

      -- verification de la coherence du prorata.
      if get_parametre (a_exe_ordre, 'DEPENSE_IDEM_TAP_ID') <> 'NON' and my_tap_id <> a_tap_id then
         raise_application_error (-20001, 'il faut que le taux de prorata de la depense soit le meme que l''engagement initial.');
      end if;

      -- on verifie la coherence des montants.
      if abs (my_dep_ht_saisie) > abs (my_dep_ttc_saisie) then
         raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
      end if;

      -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
      my_dep_tva_saisie := liquider_outils.get_tva (my_dep_ht_saisie, my_dep_ttc_saisie);

      -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declar? dans la papier.
      /*select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_tva_saisie),0) , nvl(sum(dep_ttc_saisie),0)
             into my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
        from depense_budget where dpp_id=a_dpp_id;

      if abs(my_sum_ht_saisie+a_dep_ht_saisie) > abs(my_dpp_ht_initial) or
         abs(my_sum_tva_saisie+my_dep_tva_saisie) > abs(my_dpp_tva_initial) or
         abs(my_sum_ttc_saisie+a_dep_ttc_saisie) > abs(my_dpp_ttc_initial) then
         raise_application_error(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
      end if;
      */
      select count (*)
      into   my_nb
      from   engage_ctrl_action
      where  eng_id = a_eng_id;

      if my_eng_init = 0 and my_nb = 0 then
         my_montant_budgetaire := 0;
         my_eng_budgetaire := 0;
      else
         verifier.verifier_budget (a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);
         -- calcul du montant budgetaire.
         my_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, a_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);
         -- on calcule pour diminuer le reste de l'engagement du montant liquid? ou celui par rapport au tap_id.
         --    de l'engage (si le tap_id peut etre different de celui de l'engage).
         my_eng_budgetaire := liquider_outils.get_eng_montant_budgetaire (a_exe_ordre, my_tap_id, a_tap_id, my_montant_budgetaire, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

         -- si le reste engage est 0, et qu'on passe une liquidation positive -> erreur.

         --> ON ENLEVE TEMPORAIREMENT POUR L'EXTOURNE, A CONTOURNER PLUS TARD
            --IF my_eng_reste<=0 AND my_eng_budgetaire>0 THEN
               --   RAISE_APPLICATION_ERROR(-20001,'L''engagement est deja solde ('||indication_erreur.engagement(a_eng_id)||')');
            --END IF;
         --<

         -- on verifie qu'on ne diminue pas plus que ce qu'il y a d'engage.
         if my_eng_reste < my_eng_budgetaire then
            my_eng_budgetaire := my_eng_reste;
         end if;
      end if;

      -- insertion dans la table.
      if a_dep_id is null then
         select depense_budget_seq.nextval
         into   a_dep_id
         from   dual;
      end if;

      -- on diminue le reste engage de l'engagement.
      update engage_budget
         set eng_montant_budgetaire_reste = eng_montant_budgetaire_reste - my_eng_budgetaire
       where eng_id = a_eng_id;

      if (my_eng_ht < 0 or my_eng_ttc < 0) and (my_dep_ht_saisie >= 0 and my_dep_ttc_saisie >= 0) then
         if abs (my_eng_negatif_budgetaire) < my_dep_extourne_budgetaire then
            update engage_budget
               set eng_ht_saisie = 0,
                   eng_tva_saisie = 0,
                   eng_ttc_saisie = 0
             where eng_id = a_eng_id;
         else
            --- VERIFIER SI IL N'Y A PAS UN PB DE CENTIME (arrondi, prorata ...)
            select tap_taux
            into   my_tap_taux
            from   jefy_admin.taux_prorata
            where  tap_id = my_tap_id;

            my_taux_tva := my_eng_ttc / my_eng_ht - 1;
            my_new_ht := round ((my_eng_negatif_budgetaire + my_dep_extourne_budgetaire) / (1 + (my_taux_tva * (1 - (my_tap_taux / 100)))), liquider_outils.get_nb_decimales (a_exe_ordre));
            --setScale(2, BigDecimal.ROUND_HALF_DOWN);
            my_new_ttc := round (my_new_ht * (1 + my_taux_tva), liquider_outils.get_nb_decimales (a_exe_ordre));   --.setScale(2, BigDecimal.ROUND_HALF_DOWN);

            update engage_budget
               set eng_ht_saisie = my_new_ht,
                   eng_ttc_saisie = my_new_ttc,
                   eng_tva_saisie = my_new_ttc - my_new_ht
             where eng_id = a_eng_id;
         end if;
      end if;

      -- on liquide.
      insert into depense_budget
      values      (a_dep_id,
                   a_exe_ordre,
                   a_dpp_id,
                   a_eng_id,
                   my_montant_budgetaire,
                   my_dep_ht_saisie,
                   my_dep_tva_saisie,
                   my_dep_ttc_saisie,
                   a_tap_id,
                   a_utl_ordre,
                   a_dep_id_reversement
                  );

      if my_montant_budgetaire <> 0 then
         budget.maj_budget (a_exe_ordre, my_org_id, my_tcd_ordre);
      end if;

      -- procedure de verification
      apres_liquide.budget (a_dep_id);
   end;

   procedure ins_depense_ctrl_action (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dact_id                   depense_ctrl_action.dact_id%type;
      my_tyac_id                   depense_ctrl_action.tyac_id%type;
      my_dact_montant_budgetaire   depense_ctrl_action.dact_montant_budgetaire%type;
      my_dact_ht_saisie            depense_ctrl_action.dact_ht_saisie%type;
      my_dact_tva_saisie           depense_ctrl_action.dact_tva_saisie%type;
      my_dact_ttc_saisie           depense_ctrl_action.dact_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_utl_ordre                 depense_budget.utl_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_action.dact_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.utl_ordre,
             d.eng_id
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_utl_ordre,
             my_eng_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'action.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_tyac_id
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dact_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dact_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dact_ht_saisie := round (my_dact_ht_saisie, my_nb_decimales);
         my_dact_ttc_saisie := round (my_dact_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dact_ht_saisie) > abs (my_dact_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dact_tva_saisie := liquider_outils.get_tva (my_dact_ht_saisie, my_dact_ttc_saisie);
         -- on calcule le montant budgetaire.
         my_dact_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dact_ht_saisie, my_dact_ttc_saisie);

         if my_dact_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dact_montant_budgetaire then
            my_dact_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_action_seq.nextval
         into   my_dact_id
         from   dual;

         insert into depense_ctrl_action
         values      (my_dact_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_tyac_id,
                      my_dact_montant_budgetaire,
                      my_dact_ht_saisie,
                      my_dact_tva_saisie,
                      my_dact_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_action (my_eng_id);
         verifier.verifier_action (a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
         apres_liquide.action (my_dact_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dact_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_analytique (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dana_id                   depense_ctrl_analytique.dana_id%type;
      my_can_id                    depense_ctrl_analytique.can_id%type;
      my_dana_montant_budgetaire   depense_ctrl_analytique.dana_montant_budgetaire%type;
      my_dana_ht_saisie            depense_ctrl_analytique.dana_ht_saisie%type;
      my_dana_tva_saisie           depense_ctrl_analytique.dana_tva_saisie%type;
      my_dana_ttc_saisie           depense_ctrl_analytique.dana_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_analytique.dana_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.eng_id,
             e.eng_montant_budgetaire
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_eng_id,
             my_eng_init
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere le code analytique.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_can_id
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dana_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dana_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dana_ht_saisie := round (my_dana_ht_saisie, my_nb_decimales);
         my_dana_ttc_saisie := round (my_dana_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dana_ht_saisie) > abs (my_dana_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dana_tva_saisie := liquider_outils.get_tva (my_dana_ht_saisie, my_dana_ttc_saisie);

         select count (*)
         into   my_nb
         from   engage_ctrl_action
         where  eng_id = my_eng_id;

         /*if my_eng_init=0 and my_nb=0 then
            my_dana_montant_budgetaire:=0;
         else*/-- on calcule le montant budgetaire.
         my_dana_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dana_ht_saisie, my_dana_ttc_saisie);

         --end if;
         if my_dana_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         if my_nb > 0 then
            -- on teste si il n'y a pas assez de dispo.
            if my_dep_montant_budgetaire <= my_somme + my_dana_montant_budgetaire then
               my_dana_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
            end if;
         end if;

         -- insertion dans la base.
         select depense_ctrl_analytique_seq.nextval
         into   my_dana_id
         from   dual;

         insert into depense_ctrl_analytique
         values      (my_dana_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_can_id,
                      my_dana_montant_budgetaire,
                      my_dana_ht_saisie,
                      my_dana_tva_saisie,
                      my_dana_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_analytique (my_eng_id);
         verifier.verifier_analytique (a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
         apres_liquide.analytique (my_dana_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dana_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_convention (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dcon_id                   depense_ctrl_convention.dcon_id%type;
      my_conv_ordre                depense_ctrl_convention.conv_ordre%type;
      my_dcon_montant_budgetaire   depense_ctrl_convention.dcon_montant_budgetaire%type;
      my_dcon_ht_saisie            depense_ctrl_convention.dcon_ht_saisie%type;
      my_dcon_tva_saisie           depense_ctrl_convention.dcon_tva_saisie%type;
      my_dcon_ttc_saisie           depense_ctrl_convention.dcon_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_convention.dcon_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.eng_id,
             e.eng_montant_budgetaire
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_eng_id,
             my_eng_init
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere le code analytique.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_conv_ordre
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dcon_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dcon_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dcon_ht_saisie := round (my_dcon_ht_saisie, my_nb_decimales);
         my_dcon_ttc_saisie := round (my_dcon_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dcon_ht_saisie) > abs (my_dcon_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dcon_tva_saisie := liquider_outils.get_tva (my_dcon_ht_saisie, my_dcon_ttc_saisie);

         select count (*)
         into   my_nb
         from   engage_ctrl_action
         where  eng_id = my_eng_id;

         /*if my_eng_init=0 and my_nb=0 then
            my_dcon_montant_budgetaire:=0;
         else*/-- on calcule le montant budgetaire.
         my_dcon_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dcon_ht_saisie, my_dcon_ttc_saisie);

         --end if;
         if my_dcon_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si il n'y a pas assez de dispo.
         if my_nb > 0 then
            if my_dep_montant_budgetaire <= my_somme + my_dcon_montant_budgetaire then
               my_dcon_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
            end if;
         end if;

         -- insertion dans la base.
         select depense_ctrl_convention_seq.nextval
         into   my_dcon_id
         from   dual;

         insert into depense_ctrl_convention
         values      (my_dcon_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_conv_ordre,
                      my_dcon_montant_budgetaire,
                      my_dcon_ht_saisie,
                      my_dcon_tva_saisie,
                      my_dcon_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_convention (my_eng_id);
         verifier.verifier_convention (a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
         apres_liquide.convention (my_dcon_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dcon_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_hors_marche (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dhom_id                   depense_ctrl_hors_marche.dhom_id%type;
      my_typa_id                   depense_ctrl_hors_marche.typa_id%type;
      my_ce_ordre                  depense_ctrl_hors_marche.ce_ordre%type;
      my_dhom_montant_budgetaire   depense_ctrl_hors_marche.dhom_montant_budgetaire%type;
      my_dhom_ht_saisie            depense_ctrl_hors_marche.dhom_ht_saisie%type;
      my_dhom_tva_saisie           depense_ctrl_hors_marche.dhom_tva_saisie%type;
      my_dhom_ttc_saisie           depense_ctrl_hors_marche.dhom_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_fou_ordre                 engage_budget.fou_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_hors_marche.dhom_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             e.fou_ordre,
             d.eng_id
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_fou_ordre,
             my_eng_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere le type_?chat.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_typa_id
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le code nomenclature.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_ce_ordre
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dhom_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dhom_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dhom_ht_saisie := round (my_dhom_ht_saisie, my_nb_decimales);
         my_dhom_ttc_saisie := round (my_dhom_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dhom_ht_saisie) > abs (my_dhom_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dhom_tva_saisie := liquider_outils.get_tva (my_dhom_ht_saisie, my_dhom_ttc_saisie);
         -- on calcule le montant budgetaire.
         my_dhom_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dhom_ht_saisie, my_dhom_ttc_saisie);

         if my_dhom_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dhom_montant_budgetaire then
            my_dhom_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_hors_marche_seq.nextval
         into   my_dhom_id
         from   dual;

         insert into depense_ctrl_hors_marche
         values      (my_dhom_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_typa_id,
                      my_ce_ordre,
                      my_dhom_montant_budgetaire,
                      my_dhom_ht_saisie,
                      my_dhom_tva_saisie,
                      my_dhom_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_hors_marche (my_eng_id);
         verifier.verifier_hors_marche (a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
         apres_liquide.hors_marche (my_dhom_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dhom_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_marche (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dmar_id                   depense_ctrl_marche.dmar_id%type;
      my_att_ordre                 depense_ctrl_marche.att_ordre%type;
      my_dmar_montant_budgetaire   depense_ctrl_marche.dmar_montant_budgetaire%type;
      my_dmar_ht_saisie            depense_ctrl_marche.dmar_ht_saisie%type;
      my_dmar_tva_saisie           depense_ctrl_marche.dmar_tva_saisie%type;
      my_dmar_ttc_saisie           depense_ctrl_marche.dmar_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_utl_ordre                 depense_budget.utl_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_marche.dmar_montant_budgetaire%type;
      my_dpp_id                    depense_budget.dpp_id%type;
      my_fou_ordre                 depense_papier.fou_ordre%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.dpp_id,
             d.exe_ordre,
             d.utl_ordre,
             d.eng_id,
             e.eng_montant_budgetaire
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_dpp_id,
             my_exe_ordre,
             my_utl_ordre,
             my_eng_id,
             my_eng_init
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      select fou_ordre
      into   my_fou_ordre
      from   depense_papier
      where  dpp_id = my_dpp_id;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'attribution.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_att_ordre
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dmar_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dmar_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dmar_ht_saisie := round (my_dmar_ht_saisie, my_nb_decimales);
         my_dmar_ttc_saisie := round (my_dmar_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dmar_ht_saisie) > abs (my_dmar_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dmar_tva_saisie := liquider_outils.get_tva (my_dmar_ht_saisie, my_dmar_ttc_saisie);

         select count (*)
         into   my_nb
         from   engage_ctrl_action
         where  eng_id = my_eng_id;

         if my_eng_init = 0 and my_nb = 0 then
            my_dmar_montant_budgetaire := 0;
         else
            -- on calcule le montant budgetaire.
            my_dmar_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dmar_ht_saisie, my_dmar_ttc_saisie);
         end if;

         if my_dmar_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dmar_montant_budgetaire then
            my_dmar_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_marche_seq.nextval
         into   my_dmar_id
         from   dual;

         insert into depense_ctrl_marche
         values      (my_dmar_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_att_ordre,
                      my_dmar_montant_budgetaire,
                      my_dmar_ht_saisie,
                      my_dmar_tva_saisie,
                      my_dmar_ttc_saisie
                     );

         dbms_output.put_line ('insert depense : ' || my_dmar_id);
         -- procedure de verification
         dbms_output.put_line ('upd engage : ' || my_eng_id);
         corriger.upd_engage_reste_marche (my_eng_id);
         verifier.verifier_marche (a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
         apres_liquide.marche (my_dmar_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dmar_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_planco (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dpco_id                   depense_ctrl_planco.dpco_id%type;
      my_pco_num                   depense_ctrl_planco.pco_num%type;
      my_dpco_montant_budgetaire   depense_ctrl_planco.dpco_montant_budgetaire%type;
      my_dpco_ht_saisie            depense_ctrl_planco.dpco_ht_saisie%type;
      my_dpco_tva_saisie           depense_ctrl_planco.dpco_tva_saisie%type;
      my_dpco_ttc_saisie           depense_ctrl_planco.dpco_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_utl_ordre                 depense_budget.utl_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_planco.dpco_montant_budgetaire%type;
      my_ecd_ordre                 depense_ctrl_planco.ecd_ordre%type;
      my_mod_ordre                 depense_papier.mod_ordre%type;
      my_tbo_ordre                 depense_ctrl_planco.tbo_ordre%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_inventaires               varchar2 (30000);
      my_nb_decimales              number;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.utl_ordre,
             d.eng_id
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_utl_ordre,
             my_eng_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'imputation.
         select substr (my_chaine, 1, instr (my_chaine, '$') - 1)
         into   my_pco_num
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere l'ecriture.
         --SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
         select ecd_ordre
         into   my_ecd_ordre
         from   depense_papier
         where  dpp_id in (select dpp_id
                           from   depense_budget
                           where  dep_id = a_dep_id);

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dpco_ht_saisie := round (my_dpco_ht_saisie, my_nb_decimales);
         my_dpco_ttc_saisie := round (my_dpco_ttc_saisie, my_nb_decimales);

         -- on recupere la chaine des inventaires.
         select substr (my_chaine, 1, instr (my_chaine, '$') - 1)
         into   my_inventaires
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         if my_ecd_ordre is not null then
            select mod_ordre
            into   my_mod_ordre
            from   depense_budget d, depense_papier p
            where  p.dpp_id = d.dpp_id and d.dep_id = a_dep_id;

            verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
         end if;

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dpco_ht_saisie) > abs (my_dpco_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dpco_tva_saisie := liquider_outils.get_tva (my_dpco_ht_saisie, my_dpco_ttc_saisie);
         -- on calcule le montant budgetaire.
         my_dpco_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dpco_ht_saisie, my_dpco_ttc_saisie);

         if my_dpco_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dpco_montant_budgetaire then
            my_dpco_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
         select count (*)
         into   my_nb
         from   depense_ctrl_planco
         where  dep_id = a_dep_id;

         if my_nb > 0 then
            raise_application_error (-20001, 'On ne peut mettre qu''une imputation comptable pour une depense ');
         end if;

         -- insertion dans la base.
         select depense_ctrl_planco_seq.nextval
         into   my_dpco_id
         from   dual;

         insert into depense_ctrl_planco
         values      (my_dpco_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_pco_num,
                      null,
                      my_dpco_montant_budgetaire,
                      my_dpco_ht_saisie,
                      my_dpco_tva_saisie,
                      my_dpco_ttc_saisie,
                      1,
                      my_ecd_ordre
                     );

         my_tbo_ordre := get_tbo_ordre (my_dpco_id);

         update depense_ctrl_planco
            set tbo_ordre = my_tbo_ordre
          where dpco_id = my_dpco_id;

         -- procedure de verification
         corriger.upd_engage_reste_planco (my_eng_id);
         verifier.verifier_planco (a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
         apres_liquide.planco (my_dpco_id, my_inventaires);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dpco_montant_budgetaire;
      end loop;
   end;

-- renvoie >0 s'il s'agit d'une depense directe (sans engagement prealable) generee a partir de carambole
-- inclue les liquidation d'extourne, et liquidations sur enveloppe d'extourne
-- inclue egalement les liquidations issues de kiwi ou kaki ou autres
   function is_dep_directe (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      db                  depense_budget%rowtype;
      engid               engage_budget.eng_id%type;
      unseulengagement    smallint;
      montantidentiques   smallint;
      pasdecommande       smallint;
      flag                integer;
   begin
      -- depense directe =
      -- + un seul engagement par depense_budget
      -- + montants identiques entre depense_budget et engage_budget et reste = 0
      -- + pas de commande associee a l'engagement
      unseulengagement := 0;
      montantidentiques := 0;
      pasdecommande := 0;

      select eng_id
      into   engid
      from   depense_budget
      where  dep_id = a_dep_id;

      select count (*)
      into   flag
      from   depense_budget
      where  eng_id = engid;

      if (flag = 1) then
         unseulengagement := 1;
      end if;

--    select count(*) into flag from engage_budget eb inner join jefy_admin.type_application ta on eb.tyap_id=ta.tyap_id
--    where eng_id=engid and ta.TYAP_STRID='DEPENSE';
--    if (flag=1) then
--        creepardepense := 1;
--    end if;
      select count (*)
      into   flag
      from   depense_budget db inner join engage_budget eb on db.eng_id = eb.eng_id
      where  db.eng_id = engid and db.dep_montant_budgetaire = eb.eng_montant_budgetaire and db.dep_ht_saisie = eb.eng_ht_saisie and db.dep_tva_saisie = eb.eng_tva_saisie and db.dep_ttc_saisie = eb.eng_ttc_saisie and eb.eng_montant_budgetaire_reste = 0;

      if (flag = 1) then
         montantidentiques := 1;
      end if;

      select count (*)
      into   flag
      from   depense_budget db inner join engage_budget eb on db.eng_id = eb.eng_id
             inner join commande_engagement ce on ce.eng_id = eb.eng_id
      where  db.eng_id = engid;

      if (flag = 0) then
         pasdecommande := 1;
      end if;

      return unseulengagement * montantidentiques * pasdecommande;
   end;

-- renvoie >0 si la depense est une liquidation definitive sur enveloppe d'extourne
   function is_dep_sur_extourne_env (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      isdepensedirecte      smallint;
      issurcreditextourne   integer;
      flag                  integer;
   begin
      -- depense sur enveloppe d'extourne =
      -- + depense directe
      -- + depense sur credit extourne
      isdepensedirecte := 0;
      issurcreditextourne := is_dep_sur_extourne (a_dep_id);

      if (issurcreditextourne > 0) then
         isdepensedirecte := is_dep_directe (a_dep_id);
      end if;

      return isdepensedirecte * issurcreditextourne;
   end;

-- renvoie >0 si la depense est une liquidation sur credit d'extourne fleche
   function is_dep_sur_extourne_flechee (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      issurcreditextourne   integer;
      isfleche              integer;
   begin
      -- liquidation sur credit d'extourne fleche =
      --  + depense sur credit extourne
      --  + lien existant entre liquidation definitive et liquidation d'extourne
      issurcreditextourne := is_dep_sur_extourne (a_dep_id);
      isfleche := 0;

      if (issurcreditextourne > 0) then
         select count (*)
         into   isfleche
         from   extourne_liq_def eld inner join extourne_liq_repart elr on eld.eld_id = elr.eld_id
                inner join extourne_liq el on elr.el_id = el.el_id
         where  eld.dep_id = a_dep_id and eld.tyet_id = 220;
      end if;

      return issurcreditextourne * isfleche;
   end;

-- renvoie >0 si la depense est une liquidation sur credit d'extourne
   function is_dep_sur_extourne (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      isdansextourneliqdef   integer;
   begin
      --liquidation sur credit d'extourne = refernce dans la table  extourne_liq_def
      select count (*)
      into   isdansextourneliqdef
      from   extourne_liq_def
      where  dep_id = a_dep_id and tyet_id = 220;

      return isdansextourneliqdef;
   end;

-- renvoie >0 si la depense est une liquidation d'extourne
   function is_dep_extourne (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      isliqextourne   integer;
   begin
      --liquidation d'extourne =
          -- + presence dans la table extourne_liq en dep_idN1
      select count (*)
      into   isliqextourne
      from   extourne_liq
      where  dep_id_n1 = a_dep_id;

      return isliqextourne;
   end;

-- renvoie l'application a l'origine de la depense (via l'engagement)
   function get_app_dep (a_dep_id depense_budget.dep_id%type)
      return jefy_admin.type_application.tyap_strid%type
   as
      res   jefy_admin.type_application.tyap_strid%type;
   begin
      select ta.tyap_strid
      into   res
      from   jefy_admin.type_application ta inner join engage_budget eb on ta.tyap_id = eb.tyap_id
             inner join depense_budget db on db.eng_id = eb.eng_id
      where  db.dep_id = a_dep_id;

      return res;
   end;
end;
/



create or replace procedure grhum.inst_patch_jefy_depense_2107 is
begin

    insert into jefy_depense.db_version values (2107,'2107',sysdate,sysdate,null);
    commit;
end;

