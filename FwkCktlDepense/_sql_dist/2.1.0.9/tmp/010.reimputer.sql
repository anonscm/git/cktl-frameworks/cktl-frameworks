CREATE OR REPLACE PACKAGE JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2);

PROCEDURE depense_avec_infos (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type,
      a_eng_id_new            engage_budget.eng_id%type);

PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_eng_id_new            engage_budget.eng_id%type);

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type);
      
FUNCTION subdivise_engage_pour_depense(
      a_dep_id depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type)
return number;

FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type)
return number;

PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type);

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type)
return number;
      
PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type);

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type);
      
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type);

PROCEDURE rempli_reimputation(
      a_dep_id    depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre depense_budget.utl_ordre%type);

PROCEDURE reimputation_maracuja (
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type); 

PROCEDURE reimputation_inventaire (
      a_dep_id                depense_ctrl_planco.dep_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_tap_id                depense_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type);
      
procedure check_reimp_maracuja_possible(
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type);      
      
      
END;
/


GRANT EXECUTE ON JEFY_DEPENSE.REIMPUTER TO JEFY_MISSION;

GRANT EXECUTE ON JEFY_DEPENSE.REIMPUTER TO MARACUJA;


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2
   ) IS 
   BEGIN
      depense_avec_infos(a_dep_id, a_reim_libelle, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_pco_num, a_chaine_action,
         a_chaine_analytique, a_chaine_convention, a_chaine_hors_marche, a_chaine_marche, 3 /*OUI*/, null);
   END;
   
PROCEDURE depense_avec_infos (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type, /* 3:OUI, 4:NON */
      a_eng_id_new            engage_budget.eng_id%type
   ) IS
      my_depense       depense_budget%rowtype;
      my_engage        engage_budget%rowtype;
      my_nb            integer;
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id;
     select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
     
     -- si la depense a des reversements ou est un reversement, cas non pris en charge pour le moment
     select count(*) into my_nb from depense_budget where dep_id_reversement=a_dep_id;
     if my_nb>0 or my_depense.dep_id_reversement is not null then
        raise_application_error(-20001, 'la depense a des reversements, ce cas n''est pas encore traite'); 
     end if;
     
     if a_tyet_id_reimp_ordo<>4 /*4:NON*/ then
        rempli_reimputation(a_dep_id, a_reim_libelle, a_utl_ordre);
     end if;
   
     if a_org_id is not null and my_engage.org_id<>a_org_id then
       depense_organ(a_dep_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_chaine_analytique, a_chaine_convention, a_eng_id_new);
     else
       if a_chaine_analytique is not null then 
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
       end if;
       if a_chaine_convention is not null then 
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
       end if;
       if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
       end if;
       if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
       end if;
     end if;
     
     if a_chaine_action is not null then 
       depense_action(a_dep_id, a_chaine_action, a_utl_ordre);
     end if;

     if a_chaine_hors_marche is not null then 
       depense_hors_marche(a_dep_id, a_chaine_hors_marche, a_utl_ordre);
     end if;

     if a_chaine_marche is not null then 
       depense_marche(a_dep_id, a_chaine_marche, a_utl_ordre);
     end if;

     if a_pco_num is not null then 
       depense_planco(a_dep_id, a_pco_num, a_utl_ordre, a_tyet_id_reimp_ordo);
     end if;
   END;
   
PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_eng_id_new            engage_budget.eng_id%type
   ) IS
      my_nb        integer;
      my_eng_id    engage_budget.eng_id%type;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_tap_id    engage_budget.tap_id%type;
   BEGIN
--- changement de la ligne budgetaire
--    * depense sans inventaire : si la depense est mandatee, on choisit une ligne budgetaire de la meme UB, sinon toutes les lignes, dans la limite que le disponible le permette.
--    * depense avec inventaire : suivant un parametre a mettre en place, meme limitation que la depense sans inventaire ou restriction au niveau du CR (puisque le code inventaire prend en compte le CR)
--    * le changement de ligne peut impliquer la modification des conventions et des codes analytiques eventuellement associes a cette depense    cf. changement convention
--    * le changement de ligne peut impliquer une creation d'un nouvel engagement, si l'engagement concerne d'autres liquidations (hors ORVs lies a celle qu'on re-impute) par exemple
--    * si la depense re-imputee possede un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..   

      select * into my_depense from depense_budget where dep_id=a_dep_id; 
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;

      -- si la ligne budgetaire a change
      if my_engage.org_id<>a_org_id then
        my_tcd_ordre:=a_tcd_ordre;
        if my_tcd_ordre is null then my_tcd_ordre:=my_engage.tcd_ordre; end if;

        my_tap_id:=a_tap_id;
        if my_tap_id is null then my_tap_id:=my_depense.tap_id; end if;
        
        verifier.verifier_budget(my_depense.exe_ordre, my_tap_id, a_org_id, my_tcd_ordre);
        verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, a_org_id);

        -- si avec inventaire -> CR
        /* select count(*) into my_nb from v_inventaire where dpco_id in (select dpco_id from depense_ctrl_planco where dep_id in (a_dep_id));
        if my_nb>0 then
          select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
            and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub and dest.org_cr=orig.org_cr;
          if my_nb=0 then
            raise_application_error(-20001, 'cette depense comporte des inventaires, on ne peut reimputer que dans le meme CR');
          end if;
        else */
          -- la depense est mandatee -> restriction de la nouvelle ligne a la meme UB que l'ancienne
          select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
          if my_nb>0 then
            select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
              and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub;
            if my_nb=0 then
              raise_application_error(-20001, 'cette depense est mandatee, on ne peut reimputer que dans la meme UB');
            end if;
          end if;
        --end if;
        
        -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
        my_eng_id:=subdivise_engage_pour_depense(a_dep_id, a_org_id, my_tcd_ordre, my_tap_id, a_utl_ordre);
        
      end if;
      
      if a_chaine_analytique is not null then
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
      end if;
      
      if a_chaine_convention is not null then
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
      end if;

      if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
      end if;

      if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
      end if;
   END;

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
   BEGIN
     RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer le type de credit de la depense');
   END;

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_eng_id    engage_budget.eng_id%type;
      my_par_value parametre.par_value%type;
      
      my_dep_id    depense_budget.dep_id%type;
      cursor orvs is select dep_id from depense_budget where dep_id_reversement=a_dep_id;
   BEGIN
      select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
      if my_nb>0 then
        select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and dpco_tva_saisie<>0 and man_id is not null;
        if my_nb>0 then
           RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier le taux de prorata d''une depense mandatee');
        end if;
      end if;
      
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      my_eng_id:=my_engage.eng_id;

      verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, my_engage.org_id);

      -- on ne peut reimputer que les depenses pas les ORvs pour le moment
        -- les orvs rattaches a cette depense seront mis a jour avec les memes infos
      if my_depense.dep_ttc_saisie<0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas reimputer un ORV, il faut reimputer la depense d''origine');
      end if;
            
      -- on verifie si le nouveau taux de prorata est autorise pour cette ligne budgetaire --
      verifier.verifier_budget(my_depense.exe_ordre, a_tap_id, my_engage.org_id, my_engage.tcd_ordre);
      
      select par_value into my_par_value from parametre where par_key='DEPENSE_IDEM_TAP_ID' and exe_ordre=my_depense.exe_ordre;
      if my_par_value='OUI' then
      
         -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
         my_eng_id:=subdivise_engage_pour_depense(a_dep_id, my_engage.org_id, my_engage.tcd_ordre, a_tap_id, a_utl_ordre);
         
      end if;
      
      update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=a_dep_id;
      corrige_depense_budgetaire(a_dep_id);
      
      open orvs();
      loop
         fetch  orvs into my_dep_id;
         exit when orvs%notfound;
         
         update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=my_dep_id;
         corrige_depense_budgetaire(my_dep_id);
      end loop;
      close orvs;
      
      budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
      
   END;

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_action where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_action(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_analytique where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_analytique(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_convention where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_convention(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est rattachee a une attribution on ne peut pas la transformer en hors marche');
       end if;

       select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
       delete from depense_ctrl_hors_marche where dep_id=a_dep_id;
       liquider.ins_depense_ctrl_hors_marche(my_exe_ordre, a_dep_id, a_chaine);
       Verifier.verifier_depense_coherence(a_dep_id);
     end if;     
   END;

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_att_ordre depense_ctrl_marche.att_ordre%type;
      my_old_att_ordre depense_ctrl_marche.att_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est hors marche on ne peut pas la transformer en marche');
       end if;

       SELECT grhum.en_nombre(SUBSTR(a_chaine,1,INSTR(a_chaine,'$')-1)) INTO my_att_ordre FROM dual;
       select nvl(att_ordre,0) into my_old_att_ordre from depense_ctrl_marche where dep_id=a_dep_id;
       
       if my_old_att_ordre<>my_att_ordre then 
          RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer l''attribution de la depense');
       end if;
     end if;
   END;

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type
  ) is
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_org_id    engage_budget.org_id%type;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_pco_num   depense_ctrl_planco.pco_num%type;
      my_dep_id_reversement depense_budget.dep_id_reversement%type;
  begin 
      select nvl(pco_num,'0') into my_pco_num from depense_ctrl_planco where dep_id=a_dep_id;
      if my_pco_num<>a_pco_num then
         select dep_id_reversement into my_dep_id_reversement from depense_budget where dep_id=a_dep_id;
         if my_dep_id_reversement is not null then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''un ordre de reversement');
         end if;
       
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 and a_tyet_id_reimp_ordo=3 /*3:OUI*/ then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 and a_tyet_id_reimp_ordo=3 /*3:OUI*/ then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         
         reimputation_inventaire(a_dep_id, a_pco_num, null, a_utl_ordre);
 
         update depense_ctrl_planco set pco_num=a_pco_num where dep_id=a_dep_id;
      end if;

      select d.exe_ordre, e.org_id, e.tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre 
        from depense_budget d, engage_budget e where d.eng_id=e.eng_id and d.dep_id=a_dep_id;
      if a_utl_ordre is not null then 
         Verifier.verifier_planco(my_exe_ordre, my_org_id, my_tcd_ordre, a_pco_num, a_utl_ordre);
      end if;
      Verifier.verifier_depense_coherence(a_dep_id);
  end;

PROCEDURE reimputation_inventaire (
      a_dep_id                depense_ctrl_planco.dep_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_tap_id                depense_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type)
   IS
      my_nb        integer;
      my_dpco_id   depense_ctrl_planco.dpco_id%type;
      my_exe_ordre depense_ctrl_planco.exe_ordre%type;
      my_pcoa_num  maracuja.plan_comptable_amo.pcoa_num%type;
      my_pca_duree maracuja.planco_amortissement.pca_duree%type;
      my_pcoa_id   maracuja.plan_comptable_amo.pcoa_id%type;
      my_invc_id   jefy_inventaire.inventaire_comptable.invc_id%type;
      my_clic_id   jefy_inventaire.inventaire_comptable.clic_id%type;
      my_clic_duree_amort jefy_inventaire.cle_inventaire_comptable.clic_duree_amort%type;
      cursor c1 is select i.invc_id, i.clic_id, c.clic_duree_amort from jefy_inventaire.inventaire_comptable i, jefy_inventaire.cle_inventaire_comptable c
        where i.clic_id=c.clic_id and c.clic_duree_amort<>my_pca_duree and i.invc_id in (
                 select invc_id from jefy_inventaire.inventaire_comptable
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id));
   BEGIN
         select dpco_id, exe_ordre into my_dpco_id, my_exe_ordre from depense_ctrl_planco where dep_id=a_dep_id;
         select count(*) into my_nb from jefy_inventaire.inventaire_comptable
           where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id);

         if my_nb=0 then return; end if;

         if a_pco_num is not null then
            select count(*) into my_nb from jefy_inventaire.inventaire_comptable where clic_id in (
                 select clic_id from jefy_inventaire.inventaire_comptable
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id))
               and dpco_id<>my_dpco_id and invc_id not in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)
               and dpco_id is not null;

            if my_nb>0 then
               RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas effectuer cette reimputation car elle concerne des inventaires comptables relies a d''autres depenses');
            end if;

			select count(*) into my_nb from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
			if my_nb<>1 then
               RAISE_APPLICATION_ERROR(-20001, 'il y a un probleme de configuration pour le compte d''amortissement de cette imputation '||a_pco_num||' sur l''exercice '||my_exe_ordre);
			end if;

			select pcoa_id, pca_duree into my_pcoa_id, my_pca_duree from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
      
      /*
      -- Cette partie n'est plus utilisée
      -- On souhaite conserver le numéro de compte de livraisons semblables à ceux de l'engagement
      -- Changement des comptes des détails de livraisons
      update jefy_inventaire.livraison_detail set pco_num=a_pco_num where lid_ordre in   
                  (
                  -- cherche le détail de livraison correspondant à l'inventaire comptable lié à la dépense
                   select livd.lid_ordre 
                   from   JEFY_INVENTAIRE.livraison_detail livd,
                          JEFY_INVENTAIRE.inventaire inv, 
                          JEFY_INVENTAIRE.inventaire_comptable invc
                   where  livd.lid_ordre=inv.lid_ordre
                          and inv.invc_id=invc.invc_id
                          and invc.dpco_id=my_dpco_id );   
          */                
          
            -- Mise à jour du pco_num et du pcoa_id
            update jefy_inventaire.cle_inventaire_comptable set pco_num=a_pco_num, pcoa_id=my_pcoa_id where clic_id in (
                 select clic_id from jefy_inventaire.inventaire_comptable
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id));

            select pcoa_num into my_pcoa_num from maracuja.plan_comptable_amo
               where pcoa_id in (select pcoa_id from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre);

            update jefy_inventaire.amortissement set pco_num=my_pcoa_num where invc_id in (select invc_id from jefy_inventaire.inventaire_comptable
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id));

            OPEN c1();
            LOOP
                FETCH  c1 INTO my_invc_id, my_clic_id, my_clic_duree_amort;
                EXIT WHEN c1%NOTFOUND;

                insert into jefy_inventaire.cle_inventaire_compt_modif
                    select jefy_inventaire.cle_inventaire_compt_modif_seq.nextval, my_clic_id, a_utl_ordre, my_clic_duree_amort, my_pca_duree, sysdate from dual;
                update jefy_inventaire.cle_inventaire_comptable set clic_duree_amort=my_pca_duree where clic_id=my_clic_id;
                jefy_inventaire.api_corossol.amortissement(my_invc_id);
            END LOOP;
            CLOSE c1;

         end if;

         if a_tap_id is not null then
            raise_application_error(-20001, 'changement de prorata en presence d''inventaire, cas pas encore pris en compte');
         end if;
   END;
FUNCTION subdivise_engage_pour_depense(
      a_dep_id       depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
      my_nb          integer;
      bool_creation  integer;
      my_comm_id     commande.comm_id%type;
      my_cbud_id     commande_budget.cbud_id%type;
      my_old_commande_budget commande_budget%rowtype;
      my_commande_budget commande_budget%rowtype;
      my_ht          engage_budget.eng_ht_saisie%type;
      my_tva         engage_budget.eng_tva_saisie%type;
      my_ttc         engage_budget.eng_ttc_saisie%type;
      my_bud         engage_budget.eng_montant_budgetaire%type;
      my_depense     depense_budget%rowtype;
      my_engage      engage_budget%rowtype;
      my_new_engage  engage_budget%rowtype;
      my_eng_id      engage_budget.eng_id%type;
   BEGIN
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      my_eng_id:=my_depense.eng_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      
      -- si il n'y a pas d'autre depense sur cette engagement on n'en cr¿e pas de nouveau 
      select count(*) into bool_creation from depense_budget where eng_id=my_depense.eng_id and dep_id<>a_dep_id and dep_ttc_saisie>0;
      
      -- on regarde si l'engagement appartient a une commande
      my_comm_id:=null;
      select count(*) into my_nb from commande_engagement where eng_id=my_depense.eng_id;
      if my_nb>0 then
         select min(comm_id) into my_comm_id from commande_engagement where eng_id=my_depense.eng_id;
      end if;
   
      if bool_creation=0 then
         -- on modifie juste l'engagement et enventuellement les infos de la commande
   
         if my_comm_id is not null then 
            select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
            select count(*) into my_nb from commande_budget 
              where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
            if my_nb>0 then  
               select min(cbud_id) into my_cbud_id from commande_budget 
                 where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
               update commande_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id where cbud_id=my_cbud_id;
            
               --si le taux de prorata a change, on met a jour les montants budgetaires  
               if my_engage.tap_id<>a_tap_id then
                  update commande_budget set cbud_montant_budgetaire=Budget.calculer_budgetaire(my_engage.exe_ordre,a_tap_id,a_org_id, cbud_ht_saisie, cbud_ttc_saisie)
                    where cbud_id=my_cbud_id;
                  corriger.corriger_commande_ctrl(my_cbud_id);
                  verifier.verifier_cde_budget_coherence(my_cbud_id);
               end if;
            end if;
         end if;
         
         -- on log l'ancien engagement
         engager.log_engage(my_eng_id, a_utl_ordre);
                  
         -- modif engagement
         update engage_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id, utl_ordre=a_utl_ordre where eng_id=my_eng_id;

         if my_engage.tap_id<>a_tap_id then
            -- modification des montants budgetaires   
            corrige_engage_budgetaire(my_eng_id);         
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre then 
            -- si on a chang¿ de ligne budgetaire on met a jour l'"ancien" budget
            budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre or my_engage.tap_id<>a_tap_id then
            -- si le taux de prorata ou la ligne budgetaire a change on met a jour le budget
            budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);
         end if;
         
      else

        my_nb:=0;
      
        -- si l'engagement appartient a une commande on regarde si un engagement avec les nouvelles infos existe deja 
        if my_comm_id is not null then
           select count(*) into my_nb from commande_engagement ce, engage_budget e
             where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
           if my_nb>0 then
             select min(e.eng_id) into my_eng_id from commande_engagement ce, engage_budget e
               where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
             
             -- on augmente les montants de l'engagement avec les montants de la depense
             augmenter_engage(a_dep_id, my_eng_id);
             
             -- on augmente la commande_bugdet correspondante (normalement elle existe)
             select * into my_new_engage from engage_budget where eng_id=my_eng_id;
             select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
             if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
                update commande_budget set cbud_montant_budgetaire=my_new_engage.eng_montant_budgetaire, cbud_ht_saisie=my_new_engage.eng_ht_saisie,
                   cbud_tva_saisie=my_new_engage.eng_tva_saisie, cbud_ttc_saisie=my_new_engage.eng_ttc_saisie where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
             end if;

           else
             -- on cree le nouvel engagement avec les infos de la depense
             my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);
             insert into commande_engagement select commande_engagement_seq.nextval, my_comm_id, my_eng_id from dual;
             
             -- on cree la commande_budget correspondante
             my_cbud_id:=creer_commande_budget(my_eng_id, my_comm_id);
           end if;
           
           -- on diminue l'ancienne commande_budget
           select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
           if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id 
                   and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
                   
                select * into my_new_engage from engage_budget where eng_id=my_eng_id;
                select * into my_commande_budget from commande_budget where cbud_id=my_cbud_id;
                
                my_ht :=my_commande_budget.cbud_ht_saisie -my_new_engage.eng_ht_saisie;
                if my_ht<0 then my_ht:=0; end if;
                my_ttc:=my_commande_budget.cbud_ttc_saisie-my_new_engage.eng_ttc_saisie;
                if my_ttc<0 then my_ttc:=0; end if;
                my_bud:=my_commande_budget.cbud_montant_budgetaire-my_new_engage.eng_montant_budgetaire;
                if my_bud<0 then my_bud:=0; end if;
                
				my_tva:=my_commande_budget.cbud_tva_saisie-my_new_engage.eng_tva_saisie;
                if my_tva<0 then
                   if my_ht+my_tva>0 then
                      my_ht:=my_ht+my_tva; 
                      my_bud:=Budget.calculer_budgetaire(my_commande_budget.exe_ordre,my_commande_budget.tap_id,my_commande_budget.org_id, my_ht, my_ttc);
                   else
                      my_ht:=my_ttc; 
                      my_bud:=my_ttc;
                   end if;
                   my_tva:=0;
                end if;

                update commande_budget set cbud_montant_budgetaire=my_bud, cbud_ht_saisie=my_ht,
                   cbud_tva_saisie=my_ttc-my_ht, cbud_ttc_saisie=my_ttc where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
           end if; 

        else
           my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);       
        end if;

        -- on diminue l'ancien engagement du montant de la depense a reimputer
        -- ATTENTION : on ne diminue pas le budgetaire reste car on enleve la liquidation de cet engagement, et donc le reste ne change pas 
        select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
        my_ht :=my_engage.eng_ht_saisie -my_depense.dep_ht_saisie;
        if my_ht<0 then my_ht:=0; end if;
        my_ttc:=my_engage.eng_ttc_saisie-my_depense.dep_ttc_saisie;
        if my_ttc<0 then my_ttc:=0; end if;
        my_bud:=my_engage.eng_montant_budgetaire-my_depense.dep_montant_budgetaire;
        if my_bud<0 then my_bud:=0; end if;
        if my_bud<my_engage.eng_montant_budgetaire_reste then my_bud:=my_engage.eng_montant_budgetaire_reste; end if;
  
        my_tva:=my_engage.eng_tva_saisie-my_depense.dep_tva_saisie;
        if my_tva<0 then
           if my_ht+my_tva>0 then
              my_ht:=my_ht+my_tva; 
              my_bud:=Budget.calculer_budgetaire(my_engage.exe_ordre,my_engage.tap_id,my_engage.org_id, my_ht, my_ttc);
           else
              my_ht:=my_ttc; 
              my_bud:=my_ttc;
           end if;

           my_tva:=0;
        end if;

        update engage_budget set eng_ht_saisie=my_ht, eng_tva_saisie=my_tva, eng_ttc_saisie=my_ttc, eng_montant_budgetaire=my_bud
          where eng_id=my_engage.eng_id;
        corrige_engage_repartition(my_engage.eng_id);
        
        -- on associe la depense au nouvel engagement
        update depense_budget set eng_id=my_eng_id where dep_id=a_dep_id;
                     
        -- on lance la correction du budget
        budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
        budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);

      end if;
      
      return my_eng_id;
   END;
   
FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
     my_eng_id      engage_budget.eng_id%type;
     my_depense     depense_budget%rowtype;      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
   
     select engage_budget_seq.nextval into my_eng_id from dual;
     insert into engage_budget values (my_eng_id, my_depense.exe_ordre, Get_Numerotation(my_depense.exe_ordre, NULL, null,'ENGAGE_BUDGET'),
       a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, 0, 0, 0, 0, 0, sysdate, a_tyap_id, a_utl_ordre);
     
     augmenter_engage(a_dep_id,my_eng_id);
          
     return my_eng_id;
   END;
      
PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type)
   IS
     my_nb          integer;
     my_id          engage_ctrl_action.eact_id%type;
     my_engage      engage_budget%rowtype;
     my_depense     depense_budget%rowtype;
      
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     update engage_budget set eng_montant_budgetaire=eng_montant_budgetaire+my_depense.dep_montant_budgetaire, eng_ht_saisie=eng_ht_saisie+my_depense.dep_ht_saisie, 
       eng_tva_saisie=eng_tva_saisie+my_depense.dep_tva_saisie, eng_ttc_saisie=eng_ttc_saisie+my_depense.dep_ttc_saisie where eng_id=a_eng_id;
     
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
          select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id; 
          if my_nb=0 then 
             insert into engage_ctrl_action select engage_ctrl_action_seq.nextval, row_action.exe_ordre, a_eng_id, row_action.tyac_id,
                row_action.dact_montant_budgetaire, 0, row_action.dact_ht_saisie, row_action.dact_tva_saisie, row_action.dact_ttc_saisie, sysdate from dual;
          else
             select min(eact_id) into my_id from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id;
             update engage_ctrl_action set eact_montant_budgetaire=eact_montant_budgetaire+row_action.dact_montant_budgetaire,
               eact_ht_saisie=eact_ht_saisie+row_action.dact_ht_saisie, eact_tva_saisie=eact_tva_saisie+row_action.dact_tva_saisie,
               eact_ttc_saisie=eact_ttc_saisie+row_action.dact_ttc_saisie where eact_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
          select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id; 
          if my_nb=0 then 
            insert into engage_ctrl_analytique select engage_ctrl_analytique_seq.nextval, row_analytique.exe_ordre, a_eng_id, row_analytique.can_id,
               row_analytique.dana_montant_budgetaire, 0, row_analytique.dana_ht_saisie, row_analytique.dana_tva_saisie, row_analytique.dana_ttc_saisie, 
               sysdate from dual;
          else
             select min(eana_id) into my_id from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id;
             update engage_ctrl_analytique set eana_montant_budgetaire=eana_montant_budgetaire+row_analytique.dana_montant_budgetaire,
               eana_ht_saisie=eana_ht_saisie+row_analytique.dana_ht_saisie, eana_tva_saisie=eana_tva_saisie+row_analytique.dana_tva_saisie,
               eana_ttc_saisie=eana_ttc_saisie+row_analytique.dana_ttc_saisie where eana_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
          select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_convention select engage_ctrl_convention_seq.nextval, row_convention.exe_ordre, a_eng_id, row_convention.conv_ordre,
                row_convention.dcon_montant_budgetaire, 0, row_convention.dcon_ht_saisie, row_convention.dcon_tva_saisie, row_convention.dcon_ttc_saisie, 
                sysdate from dual;
          else
             select min(econ_id) into my_id from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre;
             update engage_ctrl_convention set econ_montant_budgetaire=econ_montant_budgetaire+row_convention.dcon_montant_budgetaire,
               econ_ht_saisie=econ_ht_saisie+row_convention.dcon_ht_saisie, econ_tva_saisie=econ_tva_saisie+row_convention.dcon_tva_saisie,
               econ_ttc_saisie=econ_ttc_saisie+row_convention.dcon_ttc_saisie where econ_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
          select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id; 
          if my_nb=0 then 
             insert into engage_ctrl_hors_marche select engage_ctrl_hors_marche_seq.nextval, row_hors_marche.exe_ordre, a_eng_id, row_hors_marche.typa_id, 
                row_hors_marche.ce_ordre, row_hors_marche.dhom_montant_budgetaire, 0,0, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_tva_saisie, 
                row_hors_marche.dhom_ttc_saisie, sysdate from dual;
          else
             select min(ehom_id) into my_id from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id;
             update engage_ctrl_hors_marche set ehom_montant_budgetaire=ehom_montant_budgetaire+row_hors_marche.dhom_montant_budgetaire,
               ehom_ht_saisie=ehom_ht_saisie+row_hors_marche.dhom_ht_saisie, ehom_tva_saisie=ehom_tva_saisie+row_hors_marche.dhom_tva_saisie,
               ehom_ttc_saisie=ehom_ttc_saisie+row_hors_marche.dhom_ttc_saisie where ehom_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
          select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_marche select engage_ctrl_marche_seq.nextval, row_marche.exe_ordre, a_eng_id, row_marche.att_ordre,
                row_marche.dmar_montant_budgetaire, 0, 0, row_marche.dmar_ht_saisie, row_marche.dmar_tva_saisie, row_marche.dmar_ttc_saisie, sysdate from dual;
          else
             select min(emar_id) into my_id from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre;
             update engage_ctrl_marche set emar_montant_budgetaire=emar_montant_budgetaire+row_marche.dmar_montant_budgetaire,
               emar_ht_saisie=emar_ht_saisie+row_marche.dmar_ht_saisie, emar_tva_saisie=emar_tva_saisie+row_marche.dmar_tva_saisie,
               emar_ttc_saisie=emar_ttc_saisie+row_marche.dmar_ttc_saisie where emar_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then
       for row_planco in planco
       loop
          select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num; 
          if my_nb=0 then 
             insert into engage_ctrl_planco select engage_ctrl_planco_seq.nextval, row_planco.exe_ordre, a_eng_id, row_planco.pco_num,
                row_planco.dpco_montant_budgetaire, 0, row_planco.dpco_ht_saisie, row_planco.dpco_tva_saisie, row_planco.dpco_ttc_saisie, sysdate from dual;
          else
             select min(epco_id) into my_id from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num;
             update engage_ctrl_planco set epco_montant_budgetaire=epco_montant_budgetaire+row_planco.dpco_montant_budgetaire,
               epco_ht_saisie=epco_ht_saisie+row_planco.dpco_ht_saisie, epco_tva_saisie=epco_tva_saisie+row_planco.dpco_tva_saisie,
               epco_ttc_saisie=epco_ttc_saisie+row_planco.dpco_ttc_saisie where epco_id=my_id;
          end if;
       end loop;
     end if;

     verifier.VERIFIER_ENGAGE_COHERENCE(a_eng_id);
   END;

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type) return number
   IS
     my_cbud_id       commande_budget.cbud_id%type;
     my_engage        engage_budget%rowtype;
     
     my_nb integer;
     my_pourcentage number(15,5);
     my_total_pourcentage number(15,5);
     
     my_chaine_action varchar2(3000);
     my_chaine_analytique varchar2(3000);
     my_chaine_convention varchar2(3000);
     my_chaine_hors_marche varchar2(3000);
     my_chaine_marche varchar2(3000);
     my_chaine_planco varchar2(3000);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;

   BEGIN
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     -- on definit les infos, notamment les pourcentages
     my_chaine_action:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_action.eact_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_action:=my_chaine_action||row_action.tyac_id||'$'||row_action.eact_ht_saisie||'$'||row_action.eact_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_action:=my_chaine_action||'$';

     my_chaine_analytique:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         my_pourcentage:=round(100*row_analytique.eana_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_analytique:=my_chaine_analytique||row_analytique.can_id||'$'||row_analytique.eana_ht_saisie||'$'
               ||row_analytique.eana_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_analytique:=my_chaine_analytique||'$';

     my_chaine_convention:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         my_pourcentage:=round(100*row_convention.econ_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_convention:=my_chaine_convention||row_convention.conv_ordre||'$'||row_convention.econ_ht_saisie||'$'
              ||row_convention.econ_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_convention:=my_chaine_convention||'$';

     my_chaine_hors_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
           my_chaine_hors_marche:=my_chaine_hors_marche||row_hors_marche.typa_id||'$'||row_hors_marche.ce_ordre||'$'
              ||row_hors_marche.ehom_ht_saisie||'$'||row_hors_marche.ehom_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_hors_marche:=my_chaine_hors_marche||'$';

     my_chaine_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
           my_chaine_marche:=my_chaine_marche||row_marche.att_ordre||'$'||row_marche.emar_ht_saisie||'$'||row_marche.emar_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_marche:=my_chaine_marche||'$';

     my_chaine_planco:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_planco.epco_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_planco:=my_chaine_planco||row_planco.pco_num||'$'||row_planco.epco_ht_saisie||'$'||row_planco.epco_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_planco:=my_chaine_planco||'$';
     
     -- on cree
     select commande_budget_seq.nextval into my_cbud_id from dual;
     commander.ins_commande_budget (my_cbud_id, a_comm_id, my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre, my_engage.tap_id, my_engage.eng_ht_saisie,
       my_engage.eng_ttc_saisie, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche, my_chaine_marche, my_chaine_planco);
               
     -- on corrige pour calculer les montants des repartitions
     corriger.corriger_commande_ctrl(my_cbud_id);
     verifier.verifier_cde_budget_coherence(my_cbud_id);
     
     
     return my_cbud_id;
   END;

PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;
     my_eng_montant_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_reste engage_budget.eng_montant_budgetaire%type;
     my_ctrl_bud engage_budget.eng_montant_budgetaire%type;
     my_dep_budgetaire depense_budget.dep_montant_budgetaire%type;
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant engage_budget
     select * into my_engage from engage_budget where eng_id=a_eng_id;
    
     my_eng_montant_budgetaire:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id,
         my_engage.org_id, my_engage.eng_ht_saisie, my_engage.eng_ttc_saisie);
     
     select nvl(SUM(Budget.calculer_budgetaire(exe_ordre, my_engage.tap_id, my_engage.org_id, dep_ht_saisie, dep_ttc_saisie)),0) 
        into my_dep_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
     my_reste:=my_eng_montant_budgetaire-my_dep_budgetaire;
     if my_reste<0 then my_reste:=0; end if;
     
     update engage_budget set eng_montant_budgetaire=my_eng_montant_budgetaire, eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;

     -- maj montant engage_ctrl_action
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update engage_ctrl_action set eact_montant_budgetaire=my_reste, eact_montant_budgetaire_reste=my_reste  where eact_id=row_action.eact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_action.eact_ht_saisie, row_action.eact_ttc_saisie);

            update engage_ctrl_action set eact_montant_budgetaire=my_ctrl_bud, eact_montant_budgetaire_reste=my_ctrl_bud where eact_id=row_action.eact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_analytique
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update engage_ctrl_analytique set eana_montant_budgetaire=my_reste, eana_montant_budgetaire_reste=my_reste  where eana_id=row_analytique.eana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_analytique.eana_ht_saisie, row_analytique.eana_ttc_saisie);

            update engage_ctrl_analytique set eana_montant_budgetaire=my_ctrl_bud, eana_montant_budgetaire_reste=my_ctrl_bud where eana_id=row_analytique.eana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(a_eng_id);
     end if;

     -- maj montant engage_ctrl_convention
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update engage_ctrl_convention set econ_montant_budgetaire=my_reste, econ_montant_budgetaire_reste=my_reste  where econ_id=row_convention.econ_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_convention.econ_ht_saisie, row_convention.econ_ttc_saisie);

            update engage_ctrl_convention set econ_montant_budgetaire=my_ctrl_bud, econ_montant_budgetaire_reste=my_ctrl_bud where econ_id=row_convention.econ_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(a_eng_id);
     end if;

     -- maj montant engage_ctrl_hors_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste, ehom_montant_budgetaire_reste=my_reste  where ehom_id=row_hors_marche.ehom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_hors_marche.ehom_ht_saisie, row_hors_marche.ehom_ttc_saisie);

            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_ctrl_bud, ehom_montant_budgetaire_reste=my_ctrl_bud where ehom_id=row_hors_marche.ehom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(a_eng_id);
     end if;

     -- maj montant engage_ctrl_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update engage_ctrl_marche set emar_montant_budgetaire=my_reste, emar_montant_budgetaire_reste=my_reste  where emar_id=row_marche.emar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_marche.emar_ht_saisie, row_marche.emar_ttc_saisie);

            update engage_ctrl_marche set emar_montant_budgetaire=my_ctrl_bud, emar_montant_budgetaire_reste=my_ctrl_bud where emar_id=row_marche.emar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update engage_ctrl_planco set epco_montant_budgetaire=my_reste, epco_montant_budgetaire_reste=my_reste  where epco_id=row_planco.epco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_planco.epco_ht_saisie, row_planco.epco_ttc_saisie);

            update engage_ctrl_planco set epco_montant_budgetaire=my_ctrl_bud, epco_montant_budgetaire_reste=my_ctrl_bud where epco_id=row_planco.epco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(a_eng_id);
     end if;

     verifier.verifier_engage_coherence(a_eng_id);
   END;

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;

     my_montant_ht_total engage_budget.eng_ht_saisie%type;
     my_montant_ttc_total engage_budget.eng_ttc_saisie%type;
     my_montant_budgetaire_total engage_budget.eng_montant_budgetaire%type;
     
     my_ht engage_budget.eng_ht_saisie%type;
     my_ttc engage_budget.eng_ttc_saisie%type;
     my_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_pourcentage number (18,8);
          
     my_reste_ht engage_budget.eng_ht_saisie%type;
     my_reste_ttc engage_budget.eng_ttc_saisie%type;
     my_reste_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_total_pourcentage number (18,8);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
     my_nb_decimales        NUMBER;
   BEGIN

       -- infos references
       select * into my_engage from engage_budget where eng_id=a_eng_id;
       my_nb_decimales:=liquider_outils.get_nb_decimales(my_engage.exe_ordre);
        
       select sum(eact_ht_saisie), sum(eact_montant_budgetaire), sum(eact_ttc_saisie)
          into my_montant_ht_total, my_montant_budgetaire_total, my_montant_ttc_total from engage_ctrl_action where eng_id=a_eng_id;
   
   
       -- maj montant engage_ctrl_action       
       select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_action in action
         loop
           if action%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_action set eact_montant_budgetaire=my_reste_budgetaire, eact_ht_saisie=my_reste_ht,
                 eact_ttc_saisie=my_reste_ttc,  eact_tva_saisie=my_reste_ttc-my_reste_ht where eact_id=row_action.eact_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_action.eact_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_action.eact_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_action.eact_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_action set eact_montant_budgetaire=my_budgetaire, eact_ht_saisie=my_ht,
                 eact_ttc_saisie=my_ttc, eact_tva_saisie=my_ttc-my_ht where eact_id=row_action.eact_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_analytique       
       select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(eana_ht_saisie) into my_total_pourcentage from engage_ctrl_analytique where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_analytique in analytique
         loop
           if analytique%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_analytique set eana_montant_budgetaire=my_reste_budgetaire, eana_ht_saisie=my_reste_ht,
                 eana_ttc_saisie=my_reste_ttc,  eana_tva_saisie=my_reste_ttc-my_reste_ht where eana_id=row_analytique.eana_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_analytique.eana_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_analytique.eana_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_analytique.eana_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_analytique set eana_montant_budgetaire=my_budgetaire, eana_ht_saisie=my_ht,
                 eana_ttc_saisie=my_ttc, eana_tva_saisie=my_ttc-my_ht where eana_id=row_analytique.eana_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_convention
       select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(econ_ht_saisie) into my_total_pourcentage from engage_ctrl_convention where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_convention in convention
         loop
           if convention%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_convention set econ_montant_budgetaire=my_reste_budgetaire, econ_ht_saisie=my_reste_ht,
                 econ_ttc_saisie=my_reste_ttc,  econ_tva_saisie=my_reste_ttc-my_reste_ht where econ_id=row_convention.econ_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_convention.econ_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_convention.econ_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_convention.econ_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_convention set econ_montant_budgetaire=my_budgetaire, econ_ht_saisie=my_ht,
                 econ_ttc_saisie=my_ttc, econ_tva_saisie=my_ttc-my_ht where econ_id=row_convention.econ_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_hors_marche       
       select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;

         for row_hors_marche in hors_marche
         loop
           if hors_marche%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste_budgetaire, ehom_ht_saisie=my_reste_ht,
                 ehom_ttc_saisie=my_reste_ttc,  ehom_tva_saisie=my_reste_ttc-my_reste_ht where ehom_id=row_hors_marche.ehom_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_hors_marche.ehom_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_hors_marche.ehom_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_hors_marche.ehom_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_budgetaire, ehom_ht_saisie=my_ht,
                 ehom_ttc_saisie=my_ttc, ehom_tva_saisie=my_ttc-my_ht where ehom_id=row_hors_marche.ehom_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_marche       
       select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;

         update engage_ctrl_marche set emar_montant_budgetaire=my_reste_budgetaire, emar_ht_saisie=my_reste_ht,
              emar_ttc_saisie=my_reste_ttc,  emar_tva_saisie=my_reste_ttc-my_reste_ht where eng_id=a_eng_id;
       end if;   

       -- maj montant engage_ctrl_planco
       select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_planco in planco
         loop
           if planco%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_planco set epco_montant_budgetaire=my_reste_budgetaire, epco_ht_saisie=my_reste_ht,
                 epco_ttc_saisie=my_reste_ttc,  epco_tva_saisie=my_reste_ttc-my_reste_ht where epco_id=row_planco.epco_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_planco.epco_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_planco.epco_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_planco.epco_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_planco set epco_montant_budgetaire=my_budgetaire, epco_ht_saisie=my_ht,
                 epco_ttc_saisie=my_ttc, epco_tva_saisie=my_ttc-my_ht where epco_id=row_planco.epco_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       --corrige_engage_budgetaire();
       verifier.verifier_engage_coherence(a_eng_id);
   END;
   
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type
   ) 
   IS
     my_depense depense_budget%rowtype;
     my_dep_montant_budgetaire depense_budget.dep_montant_budgetaire%type;
     my_reste depense_budget.dep_montant_budgetaire%type;
     my_ctrl_bud depense_budget.dep_montant_budgetaire%type;
     
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant depense_budget
     select * into my_depense from depense_budget where dep_id=a_dep_id;
    
     my_dep_montant_budgetaire:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, my_depense.dep_ht_saisie, my_depense.dep_ttc_saisie);
     update depense_budget set dep_montant_budgetaire=my_dep_montant_budgetaire where dep_id=a_dep_id;

     -- maj montant depense_ctrl_action
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update depense_ctrl_action set dact_montant_budgetaire=my_reste where dact_id=row_action.dact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_action.dact_ht_saisie, row_action.dact_ttc_saisie);

            update depense_ctrl_action set dact_montant_budgetaire=my_ctrl_bud where dact_id=row_action.dact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(my_depense.eng_id);
     end if;
     
     -- maj montant depense_ctrl_analytique
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update depense_ctrl_analytique set dana_montant_budgetaire=my_reste where dana_id=row_analytique.dana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_analytique.dana_ht_saisie, row_analytique.dana_ttc_saisie);

            update depense_ctrl_analytique set dana_montant_budgetaire=my_ctrl_bud where dana_id=row_analytique.dana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_convention
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update depense_ctrl_convention set dcon_montant_budgetaire=my_reste where dcon_id=row_convention.dcon_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_convention.dcon_ht_saisie, row_convention.dcon_ttc_saisie);

            update depense_ctrl_convention set dcon_montant_budgetaire=my_ctrl_bud where dcon_id=row_convention.dcon_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_hors_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_reste where dhom_id=row_hors_marche.dhom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_ttc_saisie);

            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_ctrl_bud where dhom_id=row_hors_marche.dhom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update depense_ctrl_marche set dmar_montant_budgetaire=my_reste where dmar_id=row_marche.dmar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_marche.dmar_ht_saisie, row_marche.dmar_ttc_saisie);

            update depense_ctrl_marche set dmar_montant_budgetaire=my_ctrl_bud where dmar_id=row_marche.dmar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(my_depense.eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update depense_ctrl_planco set dpco_montant_budgetaire=my_reste where dpco_id=row_planco.dpco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_planco.dpco_ht_saisie, row_planco.dpco_ttc_saisie);

            update depense_ctrl_planco set dpco_montant_budgetaire=my_ctrl_bud where dpco_id=row_planco.dpco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(my_depense.eng_id);
     end if;

     verifier.verifier_depense_coherence(a_dep_id);
   END;
   
PROCEDURE rempli_reimputation(
      a_dep_id       depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre    depense_budget.utl_ordre%type
   )
   IS
     my_nb          integer;
     my_reim_id     reimputation.reim_id%type;
     my_exe_ordre   depense_budget.exe_ordre%type;
     my_reim_numero reimputation.reim_numero%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     my_reim_numero := Get_Numerotation(my_exe_ordre, NULL, null, 'REIMPUTATION');

     select reimputation_seq.nextval into my_reim_id from dual;
     insert into reimputation values (my_reim_id, my_exe_ordre, my_reim_numero, a_dep_id, a_reim_libelle, a_utl_ordre, sysdate);
     
     insert into reimputation_action select reimputation_action_seq.nextval, my_reim_id, tyac_id, exe_ordre, dact_montant_budgetaire, dact_ht_saisie,
         dact_tva_saisie, dact_ttc_saisie from depense_ctrl_action where dep_id=a_dep_id;
         
     insert into reimputation_analytique select reimputation_analytique_seq.nextval, my_reim_id, can_id, dana_montant_budgetaire, dana_ht_saisie,
         dana_tva_saisie, dana_ttc_saisie from depense_ctrl_analytique where dep_id=a_dep_id;
         
     insert into reimputation_budget select reimputation_budget_seq.nextval, my_reim_id, d.eng_id, e.org_id, e.tcd_ordre, d.tap_id
        from depense_budget d, engage_budget e where e.eng_id=d.eng_id and dep_id=a_dep_id;
     
     insert into reimputation_convention select reimputation_convention_seq.nextval, my_reim_id, conv_ordre, dcon_montant_budgetaire, dcon_ht_saisie,
         dcon_tva_saisie, dcon_ttc_saisie from depense_ctrl_convention where dep_id=a_dep_id;
         
     insert into reimputation_hors_marche select reimputation_hors_marche_seq.nextval, my_reim_id, typa_id, ce_ordre, dhom_montant_budgetaire, dhom_ht_saisie,
         dhom_tva_saisie, dhom_ttc_saisie from depense_ctrl_hors_marche where dep_id=a_dep_id;
         
     insert into reimputation_marche select reimputation_marche_seq.nextval, my_reim_id, att_ordre, dmar_montant_budgetaire, dmar_ht_saisie,
         dmar_tva_saisie, dmar_ttc_saisie from depense_ctrl_marche where dep_id=a_dep_id;
         
     insert into reimputation_planco select reimputation_planco_seq.nextval, my_reim_id, pco_num, exe_ordre, dpco_montant_budgetaire, dpco_ht_saisie,
         dpco_tva_saisie, dpco_ttc_saisie from depense_ctrl_planco where dep_id=a_dep_id;
   END;
   
PROCEDURE reimputation_maracuja (
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   )
   IS
      my_dep_id       depense_budget.dep_id%type;
      cursor depenses is select dep_id from depense_ctrl_planco where man_id=a_man_id;
   BEGIN
      if a_man_id is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer un man_id');
      end if;
      if a_pco_num is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer la nouvelle imputation');
      end if;
      if a_utl_ordre is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer un numero d''utilisateur');
      end if;
      
      open depenses();
      loop
         fetch depenses into my_dep_id;
         exit when depenses%notfound;

         depense_avec_infos(my_dep_id, 'reimputation comptable', null, null, null, a_utl_ordre, a_pco_num, null,
            null, null, null, null, 4 /*NON*/, null);
      end loop;
      close depenses;
   END;

   
   
procedure check_reimp_maracuja_possible(
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type
   )
   IS    
      my_nb        integer;       
      my_dep_id       depense_budget.dep_id%type;
      my_dpco_id   depense_ctrl_planco.dpco_id%type;
      my_exe_ordre depense_ctrl_planco.exe_ordre%type;
      cursor depenses is select dep_id from depense_ctrl_planco where man_id=a_man_id;
    begin
       --- on reprend les controles effectues dans "reimputation_inventaire"
       
        
      open depenses();
      loop
         fetch depenses into my_dep_id;
         exit when depenses%notfound;
         
               select dpco_id, exe_ordre into my_dpco_id, my_exe_ordre from depense_ctrl_planco where dep_id=my_dep_id;
               select count(*) into my_nb from jefy_inventaire.inventaire_comptable 
                   where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id);
                 
                 if my_nb=0 then return; end if;

                if a_pco_num is not null then 
                    select count(*) into my_nb from jefy_inventaire.inventaire_comptable where clic_id in (         
                         select clic_id from jefy_inventaire.inventaire_comptable 
                         where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id))
                       and dpco_id<>my_dpco_id and invc_id not in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)
                       and dpco_id is not null;
                 
                    if my_nb>0 then
                       RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas effectuer cette reimputation car elle concerne des inventaires comptables relies a d''autres depenses');
                    end if;
                    
                    select count(*) into my_nb from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
                    if my_nb<>1 then
                       RAISE_APPLICATION_ERROR(-20001, 'il y a un probleme de configuration pour le compte d''amortissement de cette imputation '||a_pco_num||' sur l''exercice '||my_exe_ordre);
                    end if;
                 end if;
         
      end loop;
      close depenses;
    
    end;
END;
/


