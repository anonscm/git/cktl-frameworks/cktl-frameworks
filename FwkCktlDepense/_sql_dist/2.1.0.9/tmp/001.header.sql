SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.9
-- Date de publication : 08/04/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- DT4756/DT5025 (SQL maracuja, jefy_depense) Correction de l'imputation comptable. Les paramètres de l'imputation n'étaient pas passé correctement entre maracuja et jefy_depense pour les comptes de classe 2.
-- Ce patch doit etre passe juste avant le patch 1932 de maracuja
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2108';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/

grant update on jefy_inventaire.livraison_detail to jefy_depense;



