
CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Verifier
IS

   PROCEDURE verifier_engage_exercice (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     ENGAGE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit          INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        verifier_organ_utilisateur(a_utl_ordre, a_org_id);

        -- verifier que l'on peux engager liquider et/ou mandater sur cet exercice.

        my_droit:=0;

        -- on verifie si l"utilisateur a le droit d'engager hors periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DEENG');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DEENG pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- on verifie si l"utilisateur a le droit d'engager pendant la periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DEENGINV');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DEENGINV pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='R';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- si droit=0 alors pas de droit trouver.
        IF my_droit=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit d''engager pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
               INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_extourne_exercice (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     ENGAGE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit       INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        my_droit:=0;

        -- on verifie si l"utilisateur a le droit d'extourne sur l'exercice
        my_fon_ordre:=Get_Fonction('DELIQEXT');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQEXT pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng in ('O','R');

        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit d''extourner pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
               INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_depense_exercice (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     DEPENSE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit          INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        verifier_organ_utilisateur(a_utl_ordre, a_org_id);
        
        -- verifier que l'on peux engager liquider et/ou mandater sur cet exercice.

        my_droit:=0;

        -- on verifie si l"utilisateur a le droit de liquider hors periode d'inventaire.
        my_fon_ordre:=Get_Fonction('DELIQ');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQ pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_liq='O';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- on verifie si l"utilisateur a le droit de liquider pendant la periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DELIQINV');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQINV pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_liq='R';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- si droit=0 alors pas de droit trouver.
        IF my_droit=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de liquider pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
              INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_organ_utilisateur (
         a_utl_ordre          engage_budget.utl_ordre%type,
      a_org_id          engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
   BEGIN
        select count(*) into my_nb from v_utilisateur_organ where utl_ordre=a_utl_ordre and org_id=a_org_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''a pas le droit sur cette ligne budgetaire ('||INDICATION_ERREUR.utilisateur(a_utl_ordre)||', '||
              INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;
   END;
   
   PROCEDURE verifier_budget (
       a_exe_ordre    ENGAGE_BUDGET.exe_ordre%TYPE,
       a_tap_id       ENGAGE_BUDGET.tap_id%TYPE,
       a_org_id       ENGAGE_BUDGET.org_id%TYPE,
       a_tcd_ordre    ENGAGE_BUDGET.tcd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_organ_prorata WHERE tap_id=a_tap_id AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce taux de prorata n''est pas autorise pour cette ligne budgetaire et cet exercice ('||
              INDICATION_ERREUR.exercice(a_exe_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.taux_prorata(a_tap_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM v_budget_exec_credit WHERE tcd_ordre=a_tcd_ordre AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il n''y a pas de credit ouvert pour cette ligne budgetaire, ce type de credit et cet exercice ('
              ||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_action (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tyac_id                 ENGAGE_CTRL_ACTION.tyac_id%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb           INTEGER;
      my_par_value    PARAMETRE.par_value%TYPE;
      my_fon_ordre      v_fonction.fon_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_type_action WHERE exe_ordre=a_exe_ordre AND tyac_id=a_tyac_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorise pour cet exercice ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
              INDICATION_ERREUR.action(a_tyac_id)||')');
        END IF;

        -- si parametre tester suivant les destinations du budget de gestion.
        my_par_value:=Get_Parametre(a_exe_ordre, 'CTRL_ORGAN_DEST');
        IF my_par_value = 'OUI' THEN

           my_fon_ordre:=Get_Fonction('DEAUTACT');
           IF my_fon_ordre IS NULL THEN
               RAISE_APPLICATION_ERROR(-20001, 'La fonction DEAUTACT pour le type d''application DEPENSE n''existe pas.');
           END IF;

           SELECT COUNT(*) INTO my_nb  FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe
             WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre;

           IF my_nb=0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_organ_action WHERE tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
              IF my_nb>0 THEN
                 SELECT COUNT(*) INTO my_nb FROM v_organ_action WHERE tyac_id=a_tyac_id AND tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
                 IF my_nb=0 THEN
                    RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorisee pour cette ligne budgetaire et ce type de credit ('||
                       INDICATION_ERREUR.action(a_tyac_id)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
                 END IF;
              END IF;
           END IF;
        END IF;
   END;

   PROCEDURE verifier_analytique (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_can_id                  ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE
   ) IS
       my_nb      INTEGER;
   BEGIN

        -- verifier si ce code analytique est utilisable.
        SELECT COUNT(*) INTO my_nb FROM v_code_analytique c, v_code_analytique_exercice e
           WHERE c.can_id=a_can_id AND can_utilisable=Etats.get_etat_canal_utilisable
             AND tyet_id=Etats.get_etat_canal_valide AND e.can_id=c.can_id AND e.exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas utilisable  ('||INDICATION_ERREUR.analytique(a_can_id)||')');
        END IF;

        -- verifier si droit utilisation code analytique (suivant organ).
        SELECT COUNT(*) INTO my_nb FROM v_code_analytique c, v_code_analytique_exercice e
           WHERE c.can_id=a_can_id AND can_utilisable=Etats.get_etat_canal_utilisable
             AND tyet_id=Etats.get_etat_canal_valide AND e.can_id=c.can_id AND e.exe_ordre=a_exe_ordre
           AND can_public=Etats.get_etat_canal_public;
        IF my_nb=0 THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_analytique_organ WHERE
              can_id=a_can_id AND (org_id=Get_Composante(a_org_id) OR org_id=get_cr(a_org_id) OR org_id=a_org_id);
           IF my_nb=0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique ne peut etre utilise avec cette ligne budgetaire ('||
                  INDICATION_ERREUR.analytique(a_can_id)||', '||INDICATION_ERREUR.organ(a_org_id)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_convention (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_conv_ordre              ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE
   ) IS
       my_nb      INTEGER;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_convention_non_limitative
           WHERE exe_ordre=a_exe_ordre AND org_id=a_org_id AND tcd_ordre=a_tcd_ordre
             AND conv_ordre=a_conv_ordre;

        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette convention ne peut etre utilisee avec cette ligne budgetaire et ce type de credit ('||
              INDICATION_ERREUR.convention(a_conv_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_hors_marche (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_typa_id                 ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE,
      a_ce_ordre                ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE,
      a_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE
   ) IS
       my_nb         INTEGER;
       my_cm_niveau  v_code_marche.cm_niveau%TYPE;
       my_cm_suppr   v_code_marche.cm_suppr%TYPE;
       my_ce_suppr   v_code_exer.ce_suppr%TYPE;
   BEGIN
           -- les seuils sont en HT.

        -- verifier que le ce_ordre est bien sur l'année choisie.
        SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND exe_ordre=a_exe_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce code nomenclature n''existe pas sur cet exercice ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '
             ||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        -- que le cm_code est un niveau 2.
        SELECT m.cm_niveau, m.cm_suppr, e.ce_suppr INTO my_cm_niveau, my_cm_suppr, my_ce_suppr
           FROM v_code_marche m, v_code_exer e
           WHERE m.cm_ordre=e.cm_ordre AND e.ce_ordre=a_ce_ordre;
        IF my_cm_niveau<>2 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il faut mettre un code nomenclature de niveau 2 ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        IF my_ce_suppr='O' OR my_cm_suppr='O' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le code nomenclature n''est plus utilisable ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        -- verifier que ce type achat est utilisable pour cet exercice.
        --select count(*) into my_nb from type_achat_exercice where typa_id=a_typa_id and exe_ordre=a_exe_ordre;
        --if my_nb<>1 then
        --   raise_application_error(-20001, 'Ce type achat n''est pas autorise sur cet exercice (typa_id:'||a_typa_id||', exercice:'||a_exe_ordre||')');
        --end if;


        -- inclure le verifs concernant eventuellement service_achat.


        -- verifier si fournisseur est mono ou 3CMP (mettre tag dans ces types).
        IF a_typa_id=Get_Type_Achat('3CMP') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_3cmp=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "3CMP" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;

           SELECT COUNT(*) INTO my_nb FROM v_code_marche_four
              WHERE fou_ordre=a_fou_ordre AND ce_ordre=a_ce_ordre;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur n''est pas autorise pour ce code nomenclature ('||
                INDICATION_ERREUR.code_exer(a_ce_ordre)||', '||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
           END IF;
        END IF;

        IF a_typa_id=Get_Type_Achat('MONOPOLE') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_monopole=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "MONOPOLE" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;

           SELECT COUNT(*) INTO my_nb FROM v_code_marche_four
              WHERE fou_ordre=a_fou_ordre AND ce_ordre=a_ce_ordre;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur n''est pas autorise pour ce code nomenclature ('||
                INDICATION_ERREUR.code_exer(a_ce_ordre)||', '||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
           END IF;
        END IF;

        IF a_typa_id=Get_Type_Achat('SANS ACHAT') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_autres=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "SANS ACHAT" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_marche (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_fou_ordre               ENGAGE_BUDGET.fou_ordre%TYPE,
      a_att_ordre               ENGAGE_CTRL_MARCHE.att_ordre%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
       my_nb             INTEGER;
       my_lot_ordre      v_attribution.lot_ordre%TYPE;
       my_att_execution  v_attribution_execution.aee_execution%TYPE;
       my_reversement    DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_reste_engage   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
   BEGIN
        -- verifier que le fournisseur est bon.
        --    que c'est celui de l'attribution ou un sous-traitant.
        SELECT COUNT(*) INTO my_nb FROM v_attribution WHERE fou_ordre=a_fou_ordre AND att_ordre=a_att_ordre;
        IF my_nb=0 THEN
           SELECT COUNT(*) INTO my_nb FROM v_sous_traitant WHERE fou_ordre=a_fou_ordre AND att_ordre=a_att_ordre;
           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas celui de l''attribution ni un sous traitant ('||
                 INDICATION_ERREUR.fournisseur(a_fou_ordre)||', '||INDICATION_ERREUR.attribution(a_att_ordre)||')');
           END IF;
        END IF;

        -- verif sur lot_organ et lot_agent ... mais il faut changer les clefs.
        SELECT lot_ordre INTO my_lot_ordre FROM v_attribution WHERE att_ordre=a_att_ordre;

        SELECT COUNT(*) INTO my_nb FROM v_lot_organ WHERE lot_ordre=my_lot_ordre;
        IF my_nb>0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_lot_organ WHERE lot_ordre=my_lot_ordre AND org_id=a_org_id;
              IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande sur ce lot n''est pas autorisee pour cette ligne budgetaire ('||
                 INDICATION_ERREUR.lot(my_lot_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||')');
             END IF;
        END IF;

        SELECT COUNT(*) INTO my_nb FROM v_lot_utilisateur WHERE lot_ordre=my_lot_ordre;
        IF my_nb>0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_lot_utilisateur WHERE lot_ordre=my_lot_ordre AND utl_ordre=a_utl_ordre;
           
              IF my_nb=0 THEN      
              SELECT COUNT(*) INTO my_nb FROM v_utilisateur_fonct WHERE utl_ordre=a_utl_ordre AND fon_ordre=Get_Fonction_jefyadmin('TOUTORG');
              
              IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'La commande sur ce lot n''est pas autorisee pour cet utilisateur ('||
                      INDICATION_ERREUR.lot(my_lot_ordre)||', '||INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
              END IF;
             END IF;
        END IF;
   END;

   PROCEDURE verifier_planco (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
         a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_pco_num                 ENGAGE_CTRL_PLANCO.pco_num%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
          my_nb              INTEGER;
       my_pco_validite    v_plan_comptable.pco_validite%TYPE;
       my_fon_ordre          jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
        -- verifier la validite du compte.
        SELECT COUNT(*) INTO my_nb FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''existe pas pour cet exercice ('||
              INDICATION_ERREUR.imputation(a_pco_num)||INDICATION_ERREUR.exercice(a_exe_ordre)||')');
        END IF;

        SELECT pco_validite INTO my_pco_validite FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre=a_exe_ordre;
        IF my_pco_validite<>'VALIDE' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas valide pour cet exercice ('||INDICATION_ERREUR.imputation(a_pco_num)
              ||INDICATION_ERREUR.exercice(a_exe_ordre)||')');
        END IF;

        -- verifier si correct avec le type de credit.
        SELECT COUNT(*) INTO my_nb FROM v_planco_credit
         WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pla_quoi='D' and exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas associe a ce type de credit ('||
              INDICATION_ERREUR.imputation(a_pco_num)||', type de credit:'||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;

        -- on verifie si l"utilisateur a le droit d'utiliser des comptes autres que de classe 2 ou 6.
        IF SUBSTR(a_pco_num,1,1)<>'2' AND SUBSTR(a_pco_num,1,1)<>'6' THEN
              my_fon_ordre:=Get_Fonction('DEAUTIMP');
           IF my_fon_ordre IS NULL THEN
              RAISE_APPLICATION_ERROR(-20001, 'La fonction DEAUTIMP pour le type d''application DEPENSE n''existe pas.');
           END IF;

           SELECT COUNT(*) INTO my_nb
             FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe
             WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre;

           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation ('||
                 INDICATION_ERREUR.imputation(a_pco_num)||', '||INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_emargement (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
      a_mod_ordre                DEPENSE_PAPIER.mod_ordre%TYPE,
      a_ecd_ordre               DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_ecriture_detail_a_emarger
          WHERE exe_ordre=a_exe_ordre AND mod_ordre=a_mod_ordre AND ecd_ordre=a_ecd_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette ecriture n''est pas autorisee pour ce mode de paiement ('
              ||INDICATION_ERREUR.ecriture(a_ecd_ordre)||', '||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_inventaire IS
       i INTEGER;
   BEGIN
        -- a completer --
        i:=1;
   END;

   PROCEDURE verifier_monnaie IS
       i INTEGER;
   BEGIN
        -- a completer --
        i:=1;
   END;

   PROCEDURE verifier_fournisseur (
         a_fou_ordre        v_fournisseur.fou_ordre%TYPE
   ) IS
          my_nb            INTEGER;
       my_fou_valide    v_fournisseur.fou_valide%TYPE;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_fournisseur WHERE fou_ordre=a_fou_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''existe pas ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
        END IF;

        SELECT fou_valide INTO my_fou_valide FROM v_fournisseur WHERE fou_ordre=a_fou_ordre;
        IF my_fou_valide<>'O' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas valide ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_rib (
         a_fou_ordre        v_rib_fournisseur.fou_ordre%TYPE,
         a_rib_ordre        v_rib_fournisseur.rib_ordre%TYPE,
         a_mod_ordre        DEPENSE_PAPIER.mod_ordre%TYPE,
         a_exe_ordre        DEPENSE_PAPIER.exe_ordre%TYPE
   ) IS
          my_nb               INTEGER;
       my_rib_valide    v_rib_fournisseur.rib_valide%TYPE;
       my_mod_code        v_rib_fournisseur.mod_code%TYPE;
       my_mod_dom        v_mode_paiement.mod_dom%TYPE;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_mode_paiement WHERE mod_ordre=a_mod_ordre AND exe_ordre=a_exe_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le mode de paiement n''existe pas pour cet exercice  ('||INDICATION_ERREUR.exercice(a_exe_ordre)||
              ', '||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
        END IF;

         SELECT mod_code, mod_dom INTO my_mod_code, my_mod_dom FROM v_mode_paiement WHERE mod_ordre=a_mod_ordre;

        IF a_rib_ordre IS NOT NULL THEN

           IF my_mod_dom<>'VIREMENT' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de paiement il ne faut pas de rib ('||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
           END IF;

              SELECT COUNT(*) INTO my_nb FROM v_rib_fournisseur WHERE fou_ordre=a_fou_ordre AND rib_ordre=a_rib_ordre;

           IF my_nb<>1 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le rib n''existe pas pour ce fournisseur ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||', '
                 ||INDICATION_ERREUR.rib(a_rib_ordre)||')');
           END IF;

           SELECT rib_valide INTO my_rib_valide FROM v_rib_fournisseur WHERE rib_ordre=a_rib_ordre;
           IF my_rib_valide<>'O' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le rib n''est pas valide ('||INDICATION_ERREUR.rib(a_rib_ordre)||')');
           END IF;
        ELSE
           IF my_mod_dom='VIREMENT' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de paiement il faut un rib ('||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_engage_coherence (a_eng_id ENGAGE_BUDGET.eng_id%TYPE) IS
        my_eng_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
        my_eng_montant_budgetaire_rest  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
        my_eng_ht_saisie                ENGAGE_BUDGET.eng_ht_saisie%TYPE;
        my_eng_tva_saisie               ENGAGE_BUDGET.eng_tva_saisie%TYPE;
        my_eng_ttc_saisie               ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
        my_org_id                       engage_budget.org_id%type;
        my_org_canal_obligatoire        jefy_admin.organ.org_canal_obligatoire%type;
        
        my_montant_budgetaire           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
        my_montant_budgetaire_reste     ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
        my_ht_saisie                    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
        my_tva_saisie                   ENGAGE_BUDGET.eng_tva_saisie%TYPE;
        my_ttc_saisie                   ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
        my_tyap_id                      ENGAGE_BUDGET.tyap_id%TYPE;

        my_tag_marche                   INTEGER;
        my_nb                           INTEGER;
   BEGIN
        my_tag_marche:=0;
        my_nb:=0;

           -- recupere les montants de engage_budget.
        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste, eng_ht_saisie, eng_tva_saisie, eng_ttc_saisie, org_id
          INTO my_eng_montant_budgetaire, my_eng_montant_budgetaire_rest, my_eng_ht_saisie, my_eng_tva_saisie, my_eng_ttc_saisie, my_org_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        -- si c'est un engagement d'extourne on ne veut pas de repartition
     if my_eng_ttc_saisie<0 then 
           select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par action'); end if;
           select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par code analytique'); end if;
           select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par convention'); end if;
           select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par code de nomenclature'); end if;
           select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par marche'); end if;
           select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par plan comptable'); end if;
     else
        select org_canal_obligatoire into my_org_canal_obligatoire from jefy_admin.organ where org_id=my_org_id;
        
        -- recupere et compare les montants de engage_ctrl_action.
        SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0),
               NVL(SUM(eact_ht_saisie),0), NVL(SUM(eact_tva_saisie),0), NVL(SUM(eact_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
        IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
           my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
           my_eng_ttc_saisie<>my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_action ('||INDICATION_ERREUR.engagement(a_eng_id)||')'||
              my_eng_montant_budgetaire||','||my_montant_budgetaire||','||
              my_eng_montant_budgetaire_rest||','||my_montant_budgetaire_reste);
        end if;

        -- recupere et compare les montants de engage_ctrl_analytique.

        SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0),
               NVL(SUM(eana_ht_saisie),0), NVL(SUM(eana_tva_saisie),0), NVL(SUM(eana_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        if my_org_canal_obligatoire is not null and (my_org_canal_obligatoire=3 or my_org_canal_obligatoire=21) then
           IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
              my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
              my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
              my_eng_ttc_saisie<>my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_analytique ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
           END IF;
        else
        -- on ne verifie pas la coherence exacte de engage_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de l'engagement.
           IF my_eng_montant_budgetaire<my_montant_budgetaire OR
              my_eng_montant_budgetaire_rest<my_montant_budgetaire_reste OR
              my_eng_ht_saisie<my_ht_saisie OR my_eng_tva_saisie<my_tva_saisie OR
              my_eng_ttc_saisie<my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_analytique ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
           END IF;
        end if;
        -- recupere et compare les montants de engage_ctrl_convention.

        -- on ne verifie pas la coherence exacte de engage_ctrl_convention.
        --   puisque les conventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de l'engagement.

        SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0),
               NVL(SUM(econ_ht_saisie),0), NVL(SUM(econ_tva_saisie),0), NVL(SUM(econ_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<my_montant_budgetaire_reste OR
           my_eng_ht_saisie<my_ht_saisie OR my_eng_tva_saisie<my_tva_saisie OR
           my_eng_ttc_saisie<my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_convention ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- recupere et compare les montants de engage_ctrl_planco.
        SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0),
               NVL(SUM(epco_ht_saisie),0), NVL(SUM(epco_tva_saisie),0), NVL(SUM(epco_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
           my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
           my_eng_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_planco ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        IF my_tyap_id<>Get_Tyap_Id('PRESTATION INTERNE') THEN
           -- recupere et compare les montants de engage_ctrl_hors_marche.
           -- on teste si c'est un engagement hors marche.
           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;

           -- si c'est hors marche on teste.
           IF my_nb>0 THEN
              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0),
                  NVL(SUM(ehom_ht_saisie),0), NVL(SUM(ehom_tva_saisie),0), NVL(SUM(ehom_ttc_saisie),0)
                INTO my_montant_budgetaire, my_montant_budgetaire_reste,
                  my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;

                 IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
                 my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
                   my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
                   my_eng_ttc_saisie<>my_ttc_saisie THEN
                   RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_hors_marche ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           END IF;

           -- recupere et compare les montants de engage_ctrl_marche.
           IF my_nb>0 THEN
              -- si on a trouve un engagement hors marche, on teste qu'il n'y a pas d'engagement marche.
              SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
              IF my_nb>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit etre MARCHE ou HORS MARCHE ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           ELSE
              -- si pas d'engagement hors marche on teste qu'il y a un engagement marche.
                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
                 IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit etre MARCHE ou HORS MARCHE ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0),
                  NVL(SUM(emar_ht_saisie),0), NVL(SUM(emar_tva_saisie),0), NVL(SUM(emar_ttc_saisie),0)
                INTO my_montant_budgetaire, my_montant_budgetaire_reste,
                  my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

              IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
                 my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
                 my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
                 my_eng_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_marche ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           END IF;
        END IF;
     end if;

   END;

   PROCEDURE verifier_commande_coherence(a_comm_id COMMANDE.comm_id%TYPE)
   IS
        my_nb_art_marche      INTEGER;
        my_nb_art_hors_marche INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb_art_hors_marche FROM ARTICLE
          WHERE comm_id=a_comm_id AND att_ordre IS NULL;

        SELECT COUNT(*) INTO my_nb_art_marche FROM ARTICLE
          WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;

        IF my_nb_art_hors_marche>0 AND my_nb_art_marche>0 THEN
          RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre marche ou hors marche');
        END IF;
   END;

   PROCEDURE verifier_cde_budget_coherence(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE)
   IS
        my_exe_ordre                    COMMANDE.exe_ordre%TYPE;

        my_ht                           COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
        my_tva                          COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
        my_ttc                          COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
        my_budgetaire                   COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
        my_pourcentage                  COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;

        my_budget_ht                    COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
        my_budget_tva                   COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
        my_budget_ttc                   COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
        my_budget_montant_budget        COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
        my_tag_marche                   INTEGER;
        my_nb                           INTEGER;
   BEGIN
        my_tag_marche:=0;
        my_nb:=0;

        SELECT exe_ordre INTO my_exe_ordre FROM COMMANDE
           WHERE comm_id IN (SELECT comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id);

        SELECT NVL(cbud_montant_budgetaire,0),
                NVL(cbud_ht_saisie,0), NVL(cbud_tva_saisie,0), NVL(cbud_ttc_saisie,0)
           INTO my_budget_montant_budget,
                my_budget_ht, my_budget_tva, my_budget_ttc
           FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        -- recupere et compare les montants de commande_ctrl_action.
        SELECT NVL(SUM(cact_montant_budgetaire),0), NVL(SUM(cact_pourcentage),0),
            NVL(SUM(cact_ht_saisie),0), NVL(SUM(cact_tva_saisie),0), NVL(SUM(cact_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
           FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_budget_montant_budget<>my_budgetaire OR
              100.0<my_pourcentage OR
              my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
              my_budget_ttc<>my_ttc THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_action (commande_budget:'||a_cbud_id||')');
        END IF;

        -- recupere et compare les montants de commande_ctrl_analytique.

        -- on ne verifie pas la coherence exacte de commande_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de la commande_budget.

        SELECT NVL(SUM(cana_montant_budgetaire),0), NVL(SUM(cana_pourcentage),0),
               NVL(SUM(cana_ht_saisie),0), NVL(SUM(cana_tva_saisie),0), NVL(SUM(cana_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
         FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

         IF my_budget_montant_budget<my_budgetaire OR
                100.0<my_pourcentage OR
               my_budget_ht<my_ht OR my_budget_tva<my_tva OR
               my_budget_ttc<my_ttc THEN
               RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_analytique (commande_budget:'||a_cbud_id||')');
         END IF;

        -- recupere et compare les montants de commande_ctrl_convention.

        -- on ne verifie pas la coherence exacte de commande_ctrl_convention.
        --   puisque les coventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de la commande_budget.

        SELECT NVL(SUM(ccon_montant_budgetaire),0), NVL(SUM(ccon_pourcentage),0),
               NVL(SUM(ccon_ht_saisie),0), NVL(SUM(ccon_tva_saisie),0), NVL(SUM(ccon_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
         FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

         IF my_budget_montant_budget<my_budgetaire OR
                100.0<my_pourcentage OR
               my_budget_ht<my_ht OR my_budget_tva<my_tva OR
               my_budget_ttc<my_ttc THEN
               RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_convention (commande_budget:'||a_cbud_id||')');
         END IF;

         -- recupere et compare les montants de commande_ctrl_planco.
         SELECT NVL(SUM(cpco_montant_budgetaire),0), NVL(SUM(cpco_pourcentage),0),
                   NVL(SUM(cpco_ht_saisie),0), NVL(SUM(cpco_tva_saisie),0), NVL(SUM(cpco_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
           FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_budget_montant_budget<>my_budgetaire OR
               100.0<my_pourcentage OR
              my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
              my_budget_ttc<>my_ttc THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_planco (commande_budget:'||a_cbud_id||')');
        END IF;

        -- recupere et compare les montants de commande_ctrl_hors_marche.
           -- on teste si c'est une commande hors marche.
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;

           -- si c'est hors marche on teste.
        IF my_nb>0 THEN
           SELECT NVL(SUM(chom_montant_budgetaire),0),
                  NVL(SUM(chom_ht_saisie),0), NVL(SUM(chom_tva_saisie),0), NVL(SUM(chom_ttc_saisie),0)
             INTO my_budgetaire, my_ht, my_tva, my_ttc
             FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;

           IF my_budget_montant_budget<>my_budgetaire OR
                 my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
                 my_budget_ttc<>my_ttc THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_hors_marche (commande_budget:'||a_cbud_id||')');
           END IF;
        END IF;

        -- recupere et compare les montants de commande_ctrl_marche.
        IF my_nb>0 THEN
           -- si on a trouve une commande hors marche, on teste qu'il n'y a pas de commande marche.
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
           IF my_nb>0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre MARCHE ou HORS MARCHE (commande_budget:'||a_cbud_id||')');
           END IF;
        ELSE
           -- si pas de commande hors marche on teste qu'il y a une commande marche.
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
           IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre MARCHE ou HORS MARCHE (commande_budget:'||a_cbud_id||')');
           END IF;

           SELECT NVL(SUM(cmar_montant_budgetaire),0),
                  NVL(SUM(cmar_ht_saisie),0), NVL(SUM(cmar_tva_saisie),0), NVL(SUM(cmar_ttc_saisie),0)
             INTO my_budgetaire, my_ht, my_tva, my_ttc
             FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

           IF my_budget_montant_budget<>my_budgetaire OR
                 my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
                 my_budget_ttc<>my_ttc THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_marche (commande_budget:'||a_cbud_id||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_depense_pap_coherence(a_dpp_id DEPENSE_PAPIER.dpp_id%TYPE)
   IS
        my_dpp_ht_saisie                DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
        my_dpp_tva_saisie                DEPENSE_PAPIER.dpp_tva_saisie%TYPE;
        my_dpp_ttc_saisie                DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;

        my_ht_saisie                    DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
        my_tva_saisie                    DEPENSE_PAPIER.dpp_tva_saisie%TYPE;
        my_ttc_saisie                    DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
   BEGIN

           -- recupere les montants de depense_papier.
        SELECT dpp_ht_saisie, dpp_tva_saisie, dpp_ttc_saisie
          INTO my_dpp_ht_saisie, my_dpp_tva_saisie, my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- recupere et compare les montants de depense_budget.
        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0), NVL(SUM(dep_ttc_saisie),0)
          INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        IF my_dpp_ht_saisie<>my_ht_saisie OR my_dpp_tva_saisie<>my_tva_saisie OR
           my_dpp_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_budget ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;
   END;

   PROCEDURE verifier_depense_coherence(a_dep_id DEPENSE_BUDGET.dep_id%TYPE)
   IS
           my_dep_montant_budgetaire       DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
        my_dep_ht_saisie                DEPENSE_BUDGET.dep_ht_saisie%TYPE;
        my_dep_tva_saisie                DEPENSE_BUDGET.dep_tva_saisie%TYPE;
        my_dep_ttc_saisie                DEPENSE_BUDGET.dep_ttc_saisie%TYPE;

        my_montant_budgetaire            DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
        my_ht_saisie                    DEPENSE_BUDGET.dep_ht_saisie%TYPE;
        my_tva_saisie                    DEPENSE_BUDGET.dep_tva_saisie%TYPE;
        my_ttc_saisie                    DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
        my_tyap_id                        ENGAGE_BUDGET.tyap_id%TYPE;

        my_org_id                       engage_budget.org_id%type;
        my_org_canal_obligatoire        jefy_admin.organ.org_canal_obligatoire%type;
        my_eng_id                       engage_budget.eng_id%type;

        my_tag_marche                    INTEGER;
        my_nb                            INTEGER;
        my_nb_eng                            INTEGER;
   BEGIN
           my_tag_marche:=0;
        my_nb:=0;

        -- recupere les montants de depense_budget.
        SELECT dep_montant_budgetaire, dep_ht_saisie, dep_tva_saisie, dep_ttc_saisie, tyap_id, org_id, e.eng_id
          INTO my_dep_montant_budgetaire, my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, my_tyap_id, my_org_id, my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dep_id=a_dep_id AND d.eng_id=e.eng_id;
    
    
        select count(*) into my_nb_eng from engage_ctrl_action where eng_id=my_eng_id;
        -- my_nb_eng = 0  ==> engagement d'extourne
        
        select org_canal_obligatoire into my_org_canal_obligatoire from jefy_admin.organ where org_id=my_org_id;

        if (/*my_dep_ttc_saisie>0 and*/ my_nb_eng>0) then 
        -- recupere et compare les montants de depense_ctrl_action.
        SELECT NVL(SUM(dact_montant_budgetaire),0), NVL(SUM(dact_ht_saisie),0), NVL(SUM(dact_tva_saisie),0),
           NVL(SUM(dact_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;

        IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
           my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
           my_dep_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_action (depense:'||a_dep_id||')');
        END IF;
        end if;
        -- recupere et compare les montants de depense_ctrl_analytique.

        -- on ne verifie pas la coherence exacte de depense_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de la depense.

        SELECT NVL(SUM(dana_montant_budgetaire),0), NVL(SUM(dana_ht_saisie),0), NVL(SUM(dana_tva_saisie),0),
          NVL(SUM(dana_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;

        if my_org_canal_obligatoire is not null and (my_org_canal_obligatoire=3 or my_org_canal_obligatoire=21) then
           IF ABS(my_dep_ht_saisie)<>ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<>ABS(my_tva_saisie) OR
              ABS(my_dep_ttc_saisie)<>ABS(my_ttc_saisie) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           END IF;
           
           if (my_nb_eng>0 and ABS(my_dep_montant_budgetaire)<>ABS(my_montant_budgetaire)) then
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           end if;
        else
           IF ABS(my_dep_ht_saisie)<ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<ABS(my_tva_saisie) OR
              ABS(my_dep_ttc_saisie)<ABS(my_ttc_saisie) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           END IF;

           if (my_nb_eng>0 and ABS(my_dep_montant_budgetaire)<ABS(my_montant_budgetaire)) then
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           end if;
        end if;
        -- recupere et compare les montants de depense_ctrl_convention.

        -- on ne verifie pas la coherence exacte de depense_ctrl_convention.
        --   puisque les conventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de la depense.

        SELECT NVL(SUM(dcon_montant_budgetaire),0), NVL(SUM(dcon_ht_saisie),0), NVL(SUM(dcon_tva_saisie),0),
          NVL(SUM(dcon_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;

        IF ABS(my_dep_ht_saisie)<ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<ABS(my_tva_saisie) OR
           ABS(my_dep_ttc_saisie)<ABS(my_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_convention (depense:'||a_dep_id||')');
        END IF;
           if (my_nb_eng>0 and ABS(my_dep_montant_budgetaire)<ABS(my_montant_budgetaire)) then
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_convention (depense:'||a_dep_id||')');
           end if;

        -- recupere et compare les montants de depense_ctrl_planco.

          -- pour les problemes de rejet d'une partie d'une depense_budget on limite pour le moment.
          --  a un seul compte d'imputation.
        if my_dep_ttc_saisie>0 then 
       
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'On limite a une seule imputation pour les depenses (depense:'||a_dep_id||')');
        END IF;

        SELECT NVL(SUM(dpco_montant_budgetaire),0), NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_tva_saisie),0),
          NVL(SUM(dpco_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

        IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
           my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
           my_dep_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_planco (depense:'||a_dep_id||')');
        END IF;
        end if;
        
       if my_dep_ttc_saisie>0  and my_nb_eng>0 then  
        
        IF my_tyap_id<>Get_Tyap_Id('PRESTATION INTERNE') THEN
           -- recupere et compare les montants de depense_ctrl_hors_marche.
           -- on teste si c'est une depense hors marche.
           SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;

           -- si c'est hors marche on teste.
           IF my_nb>0 THEN
              SELECT NVL(SUM(dhom_montant_budgetaire),0), NVL(SUM(dhom_ht_saisie),0), NVL(SUM(dhom_tva_saisie),0),
                NVL(SUM(dhom_ttc_saisie),0)
                INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;

              IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
                 my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
                 my_dep_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_hors_marche (depense:'||a_dep_id||')');
              END IF;
           END IF;

           -- recupere et compare les montants de depense_ctrl_marche.
           IF my_nb>0 THEN
              -- si on a trouve une depense hors marche, on teste qu'il n'y a pas de depense marche.
              SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;
              IF my_nb>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La depense doit etre MARCHE ou HORS MARCHE (depense:'||a_dep_id||')');
              END IF;
           ELSE
              -- si pas de depense hors marche on teste qu'il y a une depense marche.
              SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;
              IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La depense doit etre MARCHE ou HORS MARCHE (depense:'||a_dep_id||')');
              END IF;

              SELECT NVL(SUM(dmar_montant_budgetaire),0), NVL(SUM(dmar_ht_saisie),0), NVL(SUM(dmar_tva_saisie),0),
                NVL(SUM(dmar_ttc_saisie),0)
                INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

              IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
                 my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
                 my_dep_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_marche (depense:'||a_dep_id||')');
              END IF;
           END IF;
        END IF;
       end if; 
        
   END;

   PROCEDURE verifier_organ(
        a_org_id       ENGAGE_BUDGET.org_id%TYPE,
     a_tcd_ordre   ENGAGE_BUDGET.tcd_ordre%TYPE
   ) IS
       my_org_niv        jefy_admin.organ.org_niv%TYPE;
       my_nb            INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.organ WHERE org_id=a_org_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La ligne budgetaire n''existe pas ('||INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;

           SELECT org_niv INTO my_org_niv FROM jefy_admin.organ WHERE org_id=a_org_id;

        IF my_org_niv<3 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il faut engager sur un CR ou une convention ('||INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;
   END;

   PROCEDURE verifier_util_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des liquidations sur cet engagement ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des commandes sur cet engagement ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- a rajouter.

        -- missions.
        /*select count(*) into my_nb from jefy_mission.mission_paiement_engage where eng_id=a_eng_id;
        if my_nb>0 then
           raise_application_error(-20001, 'Il y a des missions sur cet engagement (eng_id='||a_eng_id||')');
        end if;
        */
        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des reversements sur cette liquidation (dep_id='||a_dep_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id AND man_id IS NOT NULL;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des mandats sur cette liquidation (dep_id='||a_dep_id||')');
        END IF;

        -- a rajouter.

        -- missions.
        --select count(*) into my_nb from jefy_mission.mission_paiement_liquide where dep_id=a_dep_id;
        --if my_nb>0 then
        --   raise_application_error(-20001, 'Il y a des missions sur cette liquidation (dep_id='||a_dep_id||')');
        --end if;

        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des liquidations sur cette facture ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des reversements sur cette facture ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;


        -- a rajouter.

        -- missions.
        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_commande (
      a_comm_id             COMMANDE.comm_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des engagements sur cette commande ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;
   END;

   PROCEDURE verifier_util_commande_budget (
      a_cbud_id              COMMANDE_BUDGET.cbud_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
           my_nb:=0;
   END;

END;
/


