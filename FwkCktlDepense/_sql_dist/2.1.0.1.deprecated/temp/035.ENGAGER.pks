CREATE OR REPLACE PACKAGE JEFY_DEPENSE.engager  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_engage (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

PROCEDURE ins_engage_extourne_poche (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type);

PROCEDURE del_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           z_engage_budget.zeng_utl_ordre%type);

PROCEDURE solder_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           engage_budget.utl_ordre%type);

PROCEDURE solder_engage_sansdroit (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE);

PROCEDURE upd_engage (
      a_eng_id 		        engage_budget.eng_id%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_utl_ordre			engage_budget.utl_ordre%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

PROCEDURE log_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           z_engage_budget.zeng_utl_ordre%type);

PROCEDURE ins_engage_budget (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type);

PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

END;
/
