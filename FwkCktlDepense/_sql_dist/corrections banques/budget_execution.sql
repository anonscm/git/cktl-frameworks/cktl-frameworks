CREATE OR REPLACE PACKAGE JEFY_BUDGET.BUDGET_EXECUTION AS

/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic
 -- Version du : 01/02/2007

  -- package de maj a jour du budget executoire
  -- ces procedures sont a utiliser avant le commit de la
  -- transaction et apres tous les inserts, updates ou deletes.

  -- maj du budget DEPENSE
  PROCEDURE maj_budget_depense(exeordre INTEGER,   orgid INTEGER);
  PROCEDURE maj_budget_depense_tcd(exeordre INTEGER,   orgid INTEGER, tcdordre integer);
  PROCEDURE maj_budget_depense_nature(exeordre INTEGER,   orgid INTEGER);
  PROCEDURE maj_budget_depense_gestion(exeordre INTEGER,   orgid INTEGER);

  -- maj du budget RECETTE
  PROCEDURE maj_budget_recette(exeordre INTEGER,   orgid INTEGER);
  PROCEDURE maj_budget_recette_nature(exeordre INTEGER,   orgid INTEGER);
  PROCEDURE maj_budget_recette_gestion(exeordre INTEGER,   orgid INTEGER);


  -- controles de l execution budgetaire.
  -- verification limitativite
  PROCEDURE verif_depense(exeordre INTEGER,   orgid INTEGER,   tcdordre INTEGER,   montant NUMBER,   mess OUT VARCHAR);
  PROCEDURE verif_depense_nature(exeordre INTEGER,   orgid INTEGER,   pconum INTEGER,   montant NUMBER,   mess OUT VARCHAR);
  PROCEDURE verif_depense_gestion(exeordre INTEGER,   orgid INTEGER,   tyacid INTEGER,   montant NUMBER,   mess OUT VARCHAR);

  PROCEDURE verif_recette(exeordre INTEGER,   orgid INTEGER,   tcdordre INTEGER,   montant NUMBER,   mess OUT VARCHAR);
  PROCEDURE verif_recette_nature(exeordre INTEGER,   orgid INTEGER,   pconum INTEGER,   montant NUMBER,   mess OUT VARCHAR);
  PROCEDURE verif_recette_gestion(exeordre INTEGER,   orgid INTEGER,   tyacid INTEGER,   montant NUMBER,   mess OUT VARCHAR);


  -- fonctions , procedures outils
  PROCEDURE maj_budget(exeordre INTEGER,   orgid INTEGER);
  PROCEDURE maj_budget_tcd(exeordre INTEGER,   orgid INTEGER,  tcdordre integer);
  PROCEDURE maj_budget_conv(exeordre INTEGER,   orgid INTEGER);

END;
/


GRANT EXECUTE ON JEFY_BUDGET.BUDGET_EXECUTION TO JEFY_DEPENSE;


CREATE OR REPLACE PACKAGE BODY JEFY_BUDGET.BUDGET_EXECUTION AS
  -- l ensemble de ce package est base sur la view
  -- jefy_depense.bb_budget_depense_ctrl
  -- jefy_depense.bb_budget_recette_ctrl

  -- maj du budget DEPENSE
  PROCEDURE maj_budget_depense(exeordre INTEGER,   orgid INTEGER) IS
  cpt INTEGER;
  BEGIN

Budget_Moteur.lock_table_budget_execution;
cpt :=Budget_Moteur.conv_ra(orgid,exeordre);
  IF (cpt = 0) THEN
      jefy_budget.Budget_Execution.maj_budget(exeordre,orgid);
    ELSE
      jefy_budget.Budget_Execution.maj_budget_conv(exeordre,orgid);
    END IF;
  jefy_budget.Budget_Execution.maj_budget_depense_nature(exeordre,orgid );
  jefy_budget.Budget_Execution.maj_budget_depense_gestion(exeordre,orgid );
  END;

  PROCEDURE maj_budget_depense_tcd(exeordre INTEGER,   orgid INTEGER, tcdordre integer) IS
  cpt INTEGER;
  BEGIN

--Budget_Moteur.lock_table_budget_execution;
--cpt :=Budget_Moteur.conv_ra(orgid,exeordre);
--  IF (cpt = 0) THEN
      jefy_budget.Budget_Execution.maj_budget_tcd(exeordre,orgid,tcdordre);
/*    ELSE
      jefy_budget.Budget_Execution.maj_budget_conv(exeordre,orgid);
    END IF;
  jefy_budget.Budget_Execution.maj_budget_depense_nature(exeordre,orgid );
  jefy_budget.Budget_Execution.maj_budget_depense_gestion(exeordre,orgid );
  */END;

  PROCEDURE maj_budget_depense_nature(exeordre INTEGER,   orgid INTEGER) IS
  cpt INTEGER;
  BEGIN
    SELECT 1
    INTO cpt
    FROM dual;
  END;

  PROCEDURE maj_budget_depense_gestion(exeordre INTEGER,   orgid INTEGER) IS
  cpt INTEGER;
  BEGIN
    SELECT 1
    INTO cpt
    FROM dual;
  END;



   -- maj du budget RECETTE
  PROCEDURE maj_budget_recette(exeordre INTEGER,   orgid INTEGER) IS
  cpt INTEGER;
  BEGIN
  Budget_Moteur.lock_table_budget_execution;

    SELECT 1
    INTO cpt
    FROM dual;

  jefy_budget.Budget_Execution.maj_budget_recette_nature(exeordre,orgid );
  jefy_budget.Budget_Execution.maj_budget_depense_gestion(exeordre,orgid );
  END;

  PROCEDURE maj_budget_recette_nature(exeordre INTEGER,   orgid INTEGER )IS
  cpt INTEGER;
  BEGIN
    SELECT 1
    INTO cpt
    FROM dual;
  END;

  PROCEDURE maj_budget_recette_gestion(exeordre INTEGER,   orgid INTEGER) IS
  cpt INTEGER;
  BEGIN
    SELECT 1
    INTO cpt
    FROM dual;
  END;





  -- controles de l execution budgetaire.
  -- controles de l execution budgetaire.
PROCEDURE maj_budget(exeordre INTEGER,   orgid INTEGER) IS

  cpt INTEGER;
  disponible NUMBER;
  currentOrgan jefy_admin.organ%ROWTYPE;

  CURSOR c_ctrl_depense IS
  SELECT *
  FROM jefy_depense.bb_budget_depense_ctrl_credits
  WHERE org_id = orgid
   AND exe_ordre = exeordre;

  current_bb jefy_depense.bb_budget_depense_ctrl_credits % ROWTYPE;
  BEGIN

Budget_Moteur.lock_table_budget_execution;

SELECT * INTO currentOrgan FROM jefy_admin.organ WHERE org_id = orgid;

    OPEN c_ctrl_depense;
    LOOP
      FETCH c_ctrl_depense
      INTO current_bb;
      EXIT
    WHEN c_ctrl_depense % NOTFOUND;

    UPDATE jefy_budget.BUDGET_EXEC_CREDIT
    SET bdxc_engagements = current_bb.eng,
      bdxc_mandats = current_bb.dep + current_bb.dep_orv,
      bdxc_disponible = bdxc_ouverts -current_bb.eng -current_bb.dep - current_bb.dep_orv
    WHERE exe_ordre = current_bb.exe_ordre
     AND org_id = current_bb.org_id
     AND tcd_ordre = current_bb.tcd_ordre;

  END LOOP;


  CLOSE c_ctrl_depense;

SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_EXEC_CREDIT
WHERE bdxc_disponible <0;

IF cpt != 0 THEN
 RAISE_APPLICATION_ERROR (-20001,'DISPONIBLE INSUFFISANT SUR LA LIGNE '||currentOrgan.org_Ub||'/'||currentOrgan.org_cr||'/'||currentOrgan.org_Souscr);
END IF;


END;


PROCEDURE maj_budget_tcd(exeordre INTEGER,   orgid INTEGER, tcdordre integer) IS

  cpt INTEGER;
  disponible NUMBER;
  currentOrgan jefy_admin.organ%ROWTYPE;
 
  eng number;
  dep number;
  dep_orv number;
  BEGIN

--Budget_Moteur.lock_table_budget_execution;


  SELECT nvl(SUM(eng_montant_budgetaire_reste),0) into eng FROM jefy_depense.engage_budget WHERE exe_ordre=exeordre and org_id=orgid and tcd_ordre=tcdordre;
   
  SELECT nvl(SUM(dep_montant_budgetaire),0) into dep FROM jefy_depense.depense_budget d, jefy_depense.engage_budget e
    WHERE dep_montant_budgetaire>=0 AND d.eng_id = e.eng_id and d.exe_ordre=exeordre and org_id=orgid and tcd_ordre=tcdordre;

  SELECT nvl(SUM(dep_montant_budgetaire),0) into dep_orv FROM jefy_depense.depense_budget d, jefy_depense.engage_budget e, jefy_depense.depense_ctrl_planco dpco, maracuja.mandat m
    WHERE dep_montant_budgetaire < 0 AND d.eng_id = e.eng_id AND d.dep_id = dpco.dep_id AND dpco.man_id = m.man_id AND m.man_etat = 'VISE' and
    d.exe_ordre=exeordre and org_id=orgid and tcd_ordre=tcdordre;

  UPDATE jefy_budget.BUDGET_EXEC_CREDIT
    SET bdxc_engagements = eng,   bdxc_mandats = dep + dep_orv,    bdxc_disponible = bdxc_ouverts - eng - dep - dep_orv
       WHERE exe_ordre = exeordre AND org_id = orgid  AND tcd_ordre = tcdordre;


  SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_EXEC_CREDIT WHERE bdxc_disponible <0;

  IF cpt != 0 THEN
     SELECT * INTO currentOrgan FROM jefy_admin.organ WHERE org_id = orgid;
     RAISE_APPLICATION_ERROR (-20001,'DISPONIBLE INSUFFISANT SUR LA LIGNE '||currentOrgan.org_Ub||'/'||currentOrgan.org_cr||'/'||currentOrgan.org_Souscr);
  END IF;

END;

/*

PROCEDURE maj_budget_tcd(exeordre INTEGER,   orgid INTEGER, tcdordre integer) IS

  cpt INTEGER;
  disponible NUMBER;
  currentOrgan jefy_admin.organ%ROWTYPE;

  CURSOR c_ctrl_depense IS
  SELECT *
  FROM jefy_depense.bb_budget_depense_ctrl_credits
  WHERE org_id = orgid AND exe_ordre = exeordre and tcd_ordre=tcdordre;

  current_bb jefy_depense.bb_budget_depense_ctrl_credits % ROWTYPE;
  BEGIN

Budget_Moteur.lock_table_budget_execution;

SELECT * INTO currentOrgan FROM jefy_admin.organ WHERE org_id = orgid;

    OPEN c_ctrl_depense;
    LOOP
      FETCH c_ctrl_depense
      INTO current_bb;
      EXIT
    WHEN c_ctrl_depense % NOTFOUND;

    UPDATE jefy_budget.BUDGET_EXEC_CREDIT
    SET bdxc_engagements = current_bb.eng,
      bdxc_mandats = current_bb.dep + current_bb.dep_orv,
      bdxc_disponible = bdxc_ouverts -current_bb.eng -current_bb.dep - current_bb.dep_orv
    WHERE exe_ordre = current_bb.exe_ordre
     AND org_id = current_bb.org_id
     AND tcd_ordre = current_bb.tcd_ordre;

  END LOOP;


  CLOSE c_ctrl_depense;

SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_EXEC_CREDIT
WHERE bdxc_disponible <0;

IF cpt != 0 THEN
 RAISE_APPLICATION_ERROR (-20001,'DISPONIBLE INSUFFISANT SUR LA LIGNE '||currentOrgan.org_Ub||'/'||currentOrgan.org_cr||'/'||currentOrgan.org_Souscr);
END IF;


END;
*/

PROCEDURE maj_budget_conv(exeordre INTEGER,   orgid INTEGER) IS
cpt INTEGER;

CURSOR c_ctrl_depense IS
SELECT *
FROM jefy_depense.bb_budget_depense_ctrl_credits
WHERE org_id = orgid
 AND exe_ordre = exeordre;

current_bb jefy_depense.bb_budget_depense_ctrl_credits % ROWTYPE;
BEGIN
Budget_Moteur.lock_table_budget_execution;

  OPEN c_ctrl_depense;
  LOOP
    FETCH c_ctrl_depense
    INTO current_bb;
    EXIT
  WHEN c_ctrl_depense % NOTFOUND;

  UPDATE jefy_budget.BUDGET_EXEC_CREDIT_CONV
  SET becc_engagements = current_bb.eng,
    becc_mandats = current_bb.dep + current_bb.dep_orv,
    becc_disponible = becc_ouverts -current_bb.eng -current_bb.dep -current_bb.dep_orv
  WHERE exe_ordre = current_bb.exe_ordre
   AND org_id = current_bb.org_id
   AND tcd_ordre = current_bb.tcd_ordre;

END LOOP;

CLOSE c_ctrl_depense;

SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_EXEC_CREDIT
WHERE bdxc_disponible <0;

IF cpt != 0 THEN
 RAISE_APPLICATION_ERROR (-20001,'DISPONIBLE INSUFFISANT');
END IF;

END;

-- verification limitativite par masse
PROCEDURE verif_depense(exeordre INTEGER,orgid INTEGER,   tcdordre INTEGER,   montant NUMBER ,mess OUT VARCHAR ) IS
cpt INTEGER;
BEGIN

SELECT 1
INTO cpt
FROM dual;

END;

-- verification limitativite par chapitre budgetaire
-- si on appel cette apres l'insertion de lenregistrement dans engage_ctrla_planco ou depense_ctrl_planco
-- montant = 0
-- sinon montant = montant_budgetaire de la depense OU
-- montant = montant_budgetaire_reste de l'engagement
PROCEDURE verif_depense_nature(exeordre INTEGER,orgid INTEGER,   pconum INTEGER,   montant NUMBER,mess OUT VARCHAR ) IS
cpt INTEGER;

chapitre_vote maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant_vote jefy_budget.BUDGET_VOTE_NATURE.BDVN_VOTES%TYPE;
montant_exec jefy_budget.BUDGET_VOTE_NATURE.BDVN_VOTES%TYPE;

BEGIN

-- recup du compte budgetaire de lexercice
SELECT JEFY_BUDGET.Get_Chap_Vote_Exec (exeordre,pconum) INTO chapitre_vote FROM dual;

-- verifier le compte budgetaire si limitatif pour lexercice
SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_VOTE_CHAPITRE_CTRL
WHERE pco_num = chapitre_vote
AND exe_ordre = exeordre;

IF cpt = 1 THEN

-- maj de la table JEFY_BUDGET.BUDGET_CHAPITRE_CTRL_EXECs i necessaire
-- cette table permet de connaitre l'appartenance d un compte a un chapitre vot¿
--SELECT COUNT(*) INTO cpt FROM JEFY_BUDGET.BUDGET_CHAPITRE_CTRL_EXEC
--WHERE pco_num = pconum
--AND exe_ordre = exeordre;

--IF cpt = 0 THEN
-- INSERT INTO JEFY_BUDGET.BUDGET_CHAPITRE_CTRL_EXEC
--  SELECT exeordre,pconum,JEFY_BUDGET.Get_Chap_Vote_Exec (exeordre,pconum) FROM dual;
--END IF;

-- recup du montant vote etablissement  de lexercice
 SELECT SUM(BDVN_VOTES) INTO montant_vote
 FROM jefy_budget.BUDGET_VOTE_NATURE
 WHERE exe_ordre = exeordre
 AND pco_num = chapitre_vote;

-- recup du montant des engagement_reste budgetaire pour ce compte budgetaire  de lexercice
-- recup du montant des depense_budgetaire pour ce compte budgetaire  de lexercice
-- SELECT SUM(montant_budgetaire) INTO montant_exec FROM (
-- SELECT dcp.dpco_montant_budgetaire montant_budgetaire
-- FROM jefy_depense.depense_ctrl_planco dcp , JEFY_BUDGET.BUDGET_CHAPITRE_CTRL_EXEC bcce
-- WHERE bcce.chapitre = chapitre_vote
-- AND dcp.pco_num = bcce.pco_num
-- AND dcp.exe_ordre = exeordre
-- UNION ALL
-- SELECT epco_montant_budgetaire_reste montant_budgetaire
-- FROM jefy_depense.engage_ctrl_planco ecp ,JEFY_BUDGET.BUDGET_CHAPITRE_CTRL_EXEC bcce
-- WHERE bcce.chapitre = chapitre_vote
-- AND ecp.pco_num = bcce.pco_num
-- AND ecp.exe_ordre = exeordre
-- );

 SELECT SUM(montant_budgetaire) INTO montant_exec FROM (
 SELECT dcp.dpco_montant_budgetaire montant_budgetaire
 FROM jefy_depense.depense_ctrl_planco dcp 
 WHERE dcp.pco_num like chapitre_vote||'%'
 AND dcp.exe_ordre = exeordre
 UNION ALL
 SELECT epco_montant_budgetaire_reste montant_budgetaire
 FROM jefy_depense.engage_ctrl_planco ecp
 WHERE ecp.pco_num like chapitre_vote||'%'
 AND ecp.exe_ordre = exeordre
 );


 -- si eng+ dep  > VOTE alors raise sinon rien  de lexercice
 IF montant_exec+montant > montant_vote THEN
 if (montant=0) then
  mess :='Montant du chapitre limitatif  vote ('||chapitre_vote||') = '||montant_vote||'  < au montant des executions '||montant_exec;
  else
  mess :='Montant du chapitre limitatif  vote ('||chapitre_vote||') = '||montant_vote||'  < au montant des executions '||montant_exec ||' + '|| montant;
  end if;
  RAISE_APPLICATION_ERROR (-20001,mess);
 ELSE
  mess :='OK';
 END IF;

ELSE
 mess :='OK';
END IF;

END;

-- verification limitativite des actions
PROCEDURE verif_depense_gestion(exeordre INTEGER,orgid INTEGER,   tyacid INTEGER,   montant NUMBER,mess OUT VARCHAR ) IS
cpt INTEGER;
BEGIN

SELECT 1
INTO cpt
FROM dual;

END;



-- verification limitativite par masse
PROCEDURE verif_recette(exeordre INTEGER,orgid INTEGER,   tcdordre INTEGER,   montant NUMBER ,mess OUT VARCHAR ) IS
cpt INTEGER;
BEGIN

SELECT 1
INTO cpt
FROM dual;

END;

-- verification limitativite par chapitre budgetaire
PROCEDURE verif_recette_nature(exeordre INTEGER,orgid INTEGER,   pconum INTEGER,   montant NUMBER,mess OUT VARCHAR ) IS
cpt INTEGER;
BEGIN

SELECT 1
INTO cpt
FROM dual;

END;

-- verification limitativite des actions
PROCEDURE verif_recette_gestion(exeordre INTEGER,orgid INTEGER,   tyacid INTEGER,   montant NUMBER,mess OUT VARCHAR ) IS
cpt INTEGER;
BEGIN

SELECT 1
INTO cpt
FROM dual;

END;

END;
/


GRANT EXECUTE ON JEFY_BUDGET.BUDGET_EXECUTION TO JEFY_DEPENSE;

