CREATE OR REPLACE PACKAGE JEFY_RECETTE.Reduire IS

--
-- procedures appelees par le package api
--

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE ins_reduction_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer);

PROCEDURE ins_reduction_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer);

PROCEDURE ins_reduction_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer);

PROCEDURE ins_reduction_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer);

PROCEDURE ins_reduction_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer);

PROCEDURE ins_reduction_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer);

END;
/



CREATE OR REPLACE PACKAGE BODY JEFY_RECETTE.Reduire
IS

PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
       my_nb                 INTEGER;

	   my_org_id			 FACTURE.org_id%TYPE;
	   my_fac_tap_id		 FACTURE.tap_id%TYPE;
	   my_exe_ordre			 FACTURE.exe_ordre%TYPE;
	   my_tcd_ordre			 FACTURE.tcd_ordre%TYPE;
	   my_fac_id			 FACTURE.fac_id%TYPE;

	   my_montant_budgetaire RECETTE.rec_montant_budgetaire%TYPE;
	   my_fac_budgetaire     FACTURE.fac_montant_budgetaire%TYPE;
	   my_fac_reste          FACTURE.fac_montant_budgetaire_reste%TYPE;
	   my_fac_init           FACTURE.fac_montant_budgetaire%TYPE;
	   my_rec_fac_budgetaire FACTURE.fac_montant_budgetaire%TYPE;

	   my_rec_ht_saisie		 RECETTE.rec_ht_saisie%TYPE;
	   my_rec_ttc_saisie	 RECETTE.rec_ttc_saisie%TYPE;
	   my_rec_tva_saisie	 RECETTE.rec_tva_saisie%TYPE;
	   my_sum_ht_saisie		 RECETTE.rec_ht_saisie%TYPE;
	   my_sum_tva_saisie	 RECETTE.rec_tva_saisie%TYPE;
	   my_sum_ttc_saisie	 RECETTE.rec_ttc_saisie%TYPE;
	   my_sum_red_ht		 RECETTE.rec_ht_saisie%TYPE;
	   my_sum_red_ttc		 RECETTE.rec_ttc_saisie%TYPE;
	   my_rec_id_reduction	 RECETTE.rec_id_reduction%TYPE;
	   my_rpp_id			 RECETTE.rpp_id%TYPE;
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;
	   my_tit_id			 RECETTE_CTRL_PLANCO.tit_id%TYPE;

	   my_pers_id			 RECETTE_PAPIER.pers_id%TYPE;
	   my_fou_ordre			 RECETTE_PAPIER.fou_ordre%TYPE;
	   my_rib_ordre			 RECETTE_PAPIER.rib_ordre%TYPE;
	   my_mor_ordre			 RECETTE_PAPIER.mor_ordre%TYPE;

	   my_tit_etat			 v_titre.tit_etat%TYPE;

	   my_rec_ht_init		 RECETTE.rec_ht_saisie%TYPE;
	   my_rec_ttc_init		 RECETTE.rec_ttc_saisie%TYPE;
	   my_rec_exe_ordre      RECETTE.exe_ordre%TYPE;
	   my_rec_tap_id         RECETTE.tap_id%TYPE;
        my_pco_num   	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE; 
       verifierMontant integer;      
   BEGIN
   		-- pour une reduction il faut des montants negatifs.
		my_rec_ht_saisie := a_rec_ht_saisie;
		my_rec_ttc_saisie := a_rec_ttc_saisie;
   	    IF my_rec_ht_saisie>0 OR my_rec_ttc_saisie>0 THEN
	       --RAISE_APPLICATION_ERROR(-20001, 'Les montants d''une reduction doivent etre negatifs (ins_reduction)');
		   IF my_rec_ht_saisie>0 THEN
		   	  my_rec_ht_saisie := -my_rec_ht_saisie;
		   END IF;
		   IF my_rec_ttc_saisie>0 THEN
		   	  my_rec_ttc_saisie := -my_rec_ttc_saisie;
		   END IF;
	    END IF;

		-- il faut un rec_id_reduction
		IF a_rec_id_reduction IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pour une reduction il faut la reference de la recette initiale (ins_reduction)');
		END IF;

   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id_reduction;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id_reduction||') (ins_reduction)');
		END IF;

		-- verifs par rapport a la recette a reduire
		SELECT exe_ordre, fac_id, tap_id, rec_id_reduction, rec_ht_saisie, rec_ttc_saisie, rpp_id
		  INTO my_rec_exe_ordre, my_fac_id, my_rec_tap_id, my_rec_id_reduction , my_rec_ht_init, my_rec_ttc_init, my_rpp_id
		  FROM RECETTE WHERE rec_id = a_rec_id_reduction;

		-- verification de la coherence de l'exercice.
		IF a_exe_ordre <> my_rec_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La reduction doit etre sur le meme exercice que la recette initiale (ins_reduction)');
		END IF;

		IF my_rec_id_reduction IS NOT NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire une reduction a partir d''une autre reduction (ins_reduction)');
		END IF;

		-- verif recette deja visée
		SELECT tit_id INTO my_tit_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id_reduction AND ROWNUM = 1;
		IF my_tit_id IS NULL
		THEN
		   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire une reduction sur une recette non titrée (ins_reduction)');
		END IF;
		SELECT tit_etat INTO my_tit_etat FROM v_titre WHERE tit_id = my_tit_id AND ROWNUM = 1;
		IF my_tit_etat NOT IN ('VISE')
		THEN
		   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire une reduction sur un titre non vise (ins_reduction)');
		END IF;

		-- verifs par rapport a la facture
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = my_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||my_fac_id||') (ins_reduction)');
		END IF;

		SELECT exe_ordre, org_id, tap_id, tcd_ordre, fac_montant_budgetaire_reste, fac_montant_budgetaire
		  INTO my_exe_ordre, my_org_id, my_fac_tap_id, my_tcd_ordre, my_fac_reste, my_fac_init
		  FROM FACTURE WHERE fac_id = my_fac_id;

		Verifier.verifier_budget(a_exe_ordre, my_rec_tap_id, my_org_id, my_tcd_ordre);

		my_rec_tva_saisie := Recetter_Outils.get_tva(my_rec_ht_saisie, my_rec_ttc_saisie);

		-- calcul du montant budgetaire.
		my_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		      my_rec_ht_saisie,my_rec_ttc_saisie);

		my_fac_budgetaire := Recetter_Outils.get_fac_montant_budgetaire(a_exe_ordre, my_fac_tap_id, my_rec_tap_id,
	      my_montant_budgetaire, my_org_id, my_rec_ht_saisie, my_rec_ttc_saisie);

		-- on verifie que la somme des reductions ne depasse pas le montant de la recette initiale.
		SELECT NVL(SUM(rec_ht_saisie),0), NVL(SUM(rec_ttc_saisie),0) INTO my_sum_red_ht, my_sum_red_ttc
		  FROM RECETTE WHERE rec_id_reduction = a_rec_id_reduction;
    
         SELECT SUBSTR(a_chaine_planco,1,INSTR(a_chaine_planco,'$')-1) INTO my_pco_num FROM dual;
         verifierMontant := 1;
         if (my_pco_num like '713%') then
            verifierMontant := 0;
         end if;
         if (verifierMontant>0) then  
            IF my_rec_ht_init < ABS(my_sum_red_ht) + ABS(my_rec_ht_saisie) OR
               my_rec_ttc_init < ABS(my_sum_red_ttc) + ABS(my_rec_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Le total des reductions ne peut pas depasser celui de la recette initiale (ins_reduction)');
            END IF;
        end if;
		SELECT pers_id, fou_ordre, rib_ordre, mor_ordre
		  INTO my_pers_id, my_fou_ordre, my_rib_ordre, my_mor_ordre
		  FROM RECETTE_PAPIER WHERE rpp_id = my_rpp_id;

		SELECT recette_papier_seq.NEXTVAL INTO my_rpp_id FROM dual;
  		Recetter.ins_recette_papier(my_rpp_id, a_exe_ordre, my_rpp_numero, my_rec_ht_saisie, my_rec_ttc_saisie,
	   								my_pers_id, my_fou_ordre, my_rib_ordre, my_mor_ordre, SYSDATE, SYSDATE,
	   								SYSDATE, a_rpp_nb_piece, a_utl_ordre, 'N');

		-- insertion dans la table.
	    IF a_rec_id IS NULL THEN
	       SELECT recette_seq.NEXTVAL INTO a_rec_id FROM dual;
	    END IF;
		IF a_rec_numero IS NULL THEN
   		   a_rec_numero := Get_Numerotation(a_exe_ordre, NULL, 'RECETTE');
   		END IF;

		-- on reduit
		INSERT INTO RECETTE (
			   REC_ID, EXE_ORDRE, RPP_ID, REC_NUMERO, FAC_ID, REC_LIB,
   			   REC_DATE_SAISIE, REC_MONTANT_BUDGETAIRE, REC_HT_SAISIE, REC_TVA_SAISIE, REC_TTC_SAISIE,
			   TAP_ID, TYET_ID, UTL_ORDRE, REC_ID_REDUCTION
		)
		VALUES (a_rec_id, a_exe_ordre, my_rpp_id, a_rec_numero, my_fac_id, a_rec_lib,
			   SYSDATE, my_montant_budgetaire, my_rec_ht_saisie, my_rec_tva_saisie, my_rec_ttc_saisie,
		   	   my_rec_tap_id, Type_Etat.get_etat_valide, a_utl_ordre, a_rec_id_reduction);

		ins_reduction_ctrl_action(a_exe_ordre, a_rec_id, a_chaine_action, verifierMontant);
		ins_reduction_ctrl_analytique(a_exe_ordre, a_rec_id, a_chaine_analytique, verifierMontant);
		ins_reduction_ctrl_convention(a_exe_ordre, a_rec_id, a_chaine_convention, verifierMontant);
		ins_reduction_ctrl_planco(a_exe_ordre, a_rec_id, a_chaine_planco, verifierMontant);
		ins_reduction_ctrl_planco_tva(a_exe_ordre, a_rec_id, a_chaine_planco_tva, verifierMontant);
		ins_reduction_ctrl_planco_ctp(a_exe_ordre, a_rec_id, a_chaine_planco_ctp, verifierMontant);

		-- on remonte le reste de la facture...
		--my_fac_reste := my_fac_reste - my_fac_budgetaire;
		--IF my_fac_reste > my_fac_init THEN
		--   my_fac_reste := my_fac_init;
		--END IF;
		--UPDATE FACTURE SET fac_montant_budgetaire_reste = my_fac_reste WHERE fac_id = my_fac_id;

		-- mise a jour des restes des ctrl de la facture
		--Corriger.maj_restes_ctrl(my_fac_id);

		Budget.ins_recette(a_exe_ordre, my_org_id, my_tcd_ordre, my_montant_budgetaire);
   END;

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
      my_fac_id						RECETTE.fac_id%TYPE;
	  my_fac_tap_id					FACTURE.tap_id%TYPE;
   	  my_rec_tap_id					RECETTE.tap_id%TYPE;
   	  my_exe_ordre					RECETTE.exe_ordre%TYPE;
   	  my_org_id						FACTURE.org_id%TYPE;
	  my_tcd_ordre					FACTURE.tcd_ordre%TYPE;
	  my_rec_montant_budgetaire		RECETTE.rec_montant_budgetaire%TYPE;
	  my_rec_id_reduction		    RECETTE.rec_id_reduction%TYPE;
	  my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
	  my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;
	  my_fac_budgetaire				FACTURE.fac_montant_budgetaire%TYPE;
	  my_nb							INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb <> 1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (del_reduction)');
		END IF;
		SELECT fac_id, exe_ordre, tap_id, rec_montant_budgetaire, rec_ht_saisie, rec_ttc_saisie, rec_id_reduction
		  INTO my_fac_id, my_exe_ordre, my_rec_tap_id, my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_ttc_saisie, my_rec_id_reduction
		  FROM RECETTE WHERE rec_id = a_rec_id;

		-- on verifie que c'est une reduction
		IF my_rec_id_reduction IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette n''est pas une reduction, utiliser le package RECETTER (del_reduction)');
		END IF;

		Recetter.log_recette(a_rec_id, a_utl_ordre);

		DELETE FROM RECETTE_CTRL_ACTION WHERE rec_id = a_rec_id;
		DELETE FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id = a_rec_id;
		DELETE FROM RECETTE_CTRL_CONVENTION WHERE rec_id = a_rec_id;
	    DELETE FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id);
	    DELETE FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id);
		DELETE FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;

		-- delete...
	    DELETE FROM RECETTE WHERE rec_id = a_rec_id;

		-- mise a jour de la facture
		--SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = my_fac_id;
		--IF my_nb<>1 THEN
		--   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||my_fac_id||') (del_reduction)');
		--END IF;
		--SELECT org_id, tap_id, tcd_ordre INTO my_org_id, my_fac_tap_id, my_tcd_ordre FROM FACTURE WHERE fac_id = my_fac_id;

	    --my_fac_budgetaire := Recetter_Outils.get_fac_montant_budgetaire(my_exe_ordre, my_fac_tap_id, my_rec_tap_id,
	    --      my_rec_montant_budgetaire, my_org_id, my_rec_ht_saisie, my_rec_ttc_saisie);

		--UPDATE FACTURE SET fac_montant_budgetaire_reste = fac_montant_budgetaire_reste - my_fac_budgetaire WHERE fac_id = my_fac_id;

		-- mise a jour des restes des ctrl de la facture
		--Corriger.maj_restes_ctrl(my_fac_id);

		Budget.del_recette(my_exe_ordre, my_org_id, my_tcd_ordre, my_rec_montant_budgetaire);
   END;

PROCEDURE ins_reduction_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
      a_verifierMontant     integer
   ) IS
       my_ract_id	               RECETTE_CTRL_ACTION.ract_id%TYPE;
       my_lolf_id	  	   		   RECETTE_CTRL_ACTION.lolf_id%TYPE;
       my_ract_montant_budgetaire  RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
	   sum_ract_montant_budgetaire RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
       my_ract_ht_saisie	  	   RECETTE_CTRL_ACTION.ract_ht_saisie%TYPE;
       my_ract_tva_saisie		   RECETTE_CTRL_ACTION.ract_tva_saisie%TYPE;
       my_ract_ttc_saisie		   RECETTE_CTRL_ACTION.ract_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre				   RECETTE.exe_ordre%TYPE;
	   my_rec_id_reduction		   RECETTE.rec_id_reduction%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_reduction_ctrl_action)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

  		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_reduction_ctrl_action)');
		END IF;

		-- on verifie que la recette a aussi un montant negatif.
		IF my_rec_montant_budgetaire >= 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La recette correspondante n''est pas une reduction (ins_reduction_ctrl_action)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_lolf_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ract_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ract_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			-- si les montants sont positifs, on inverse...
			IF my_ract_ht_saisie > 0 THEN
			   my_ract_ht_saisie := -my_ract_ht_saisie;
			END IF;
			IF my_ract_ttc_saisie > 0 THEN
			   my_ract_ttc_saisie := -my_ract_ttc_saisie;
			END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_ract_ht_saisie)>ABS(my_ract_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_reduction_ctrl_action)');
		    END IF;

		    my_ract_tva_saisie := Recetter_Outils.get_tva(my_ract_ht_saisie, my_ract_ttc_saisie);

			-- on calcule le montant budgetaire.
			my_ract_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_ract_ht_saisie,my_ract_ttc_saisie);

			-- Pour les reductions
			IF my_ract_montant_budgetaire > 0 THEN
   	   	   	   RAISE_APPLICATION_ERROR(-20001, 'C''est une reduction le montant doit etre negatif (ins_reduction_ctrl_action)');
			END IF;

			-- verifier que l'action qu'on enleve ne fait pas passer en negatif la montant de la recette de depart
			--     moins la somme des reductions pour cette action et que l'action existe bien au depart.
			-- et que la facture correspondante est le meme facture_ctrl_action.
			SELECT rec_id_reduction INTO my_rec_id_reduction FROM RECETTE WHERE rec_id = a_rec_id;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_ACTION
			   WHERE lolf_id = my_lolf_id
				 AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
				  OR rec_id_reduction = my_rec_id_reduction);

			IF my_nb=0 THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Pour cette reduction et cette action il n''existe pas d''equivalent pour cette recette (ins_reduction_ctrl_action)');
			END IF;

            if (a_verifierMontant>1) then
                SELECT NVL(SUM(ract_montant_budgetaire),0) INTO sum_ract_montant_budgetaire
                  FROM RECETTE_CTRL_ACTION WHERE lolf_id = my_lolf_id
                   AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
                    OR rec_id_reduction = my_rec_id_reduction);
                IF sum_ract_montant_budgetaire + my_ract_montant_budgetaire < 0 THEN
                   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette recette pour cette action (ins_reduction_ctrl_action)');
                END IF;
            end if;
            
			-- on teste si c'est le dernier.
      		IF SUBSTR(my_chaine,1,1) = '$' OR
			  ABS(my_rec_montant_budgetaire) <= ABS(my_somme + my_ract_montant_budgetaire) THEN
			    my_ract_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_action_seq.NEXTVAL INTO my_ract_id FROM dual;

			INSERT INTO RECETTE_CTRL_ACTION VALUES (my_ract_id,
			       a_exe_ordre, a_rec_id, my_lolf_id, my_ract_montant_budgetaire,
				   my_ract_ht_saisie, my_ract_tva_saisie, my_ract_ttc_saisie, SYSDATE);

	   		-- procedure de verification
            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_lolf_id);
			Apres_Recette.ins_recette_ctrl_action(my_ract_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_ract_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_reduction_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer
   ) IS
       my_rana_id	               RECETTE_CTRL_ANALYTIQUE.rana_id%TYPE;
       my_can_id	  	   		   RECETTE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_rana_montant_budgetaire  RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
	   sum_rana_montant_budgetaire RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
       my_rana_ht_saisie	  	   RECETTE_CTRL_ANALYTIQUE.rana_ht_saisie%TYPE;
       my_rana_tva_saisie		   RECETTE_CTRL_ANALYTIQUE.rana_tva_saisie%TYPE;
       my_rana_ttc_saisie		   RECETTE_CTRL_ANALYTIQUE.rana_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_rec_id_reduction		   RECETTE.rec_id_reduction%TYPE;
	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_reduction_ctrl_analytique)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_reduction_ctrl_analytique)');
		END IF;

		-- on verifie que la recette a aussi un montant negatif.
		IF my_rec_montant_budgetaire >= 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Recette correspondante n''est pas une reduction (ins_reduction_ctrl_analytique)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			-- si les montants sont positifs, on inverse...
			IF my_rana_ht_saisie > 0 THEN
			   my_rana_ht_saisie := -my_rana_ht_saisie;
			END IF;
			IF my_rana_ttc_saisie > 0 THEN
			   my_rana_ttc_saisie := -my_rana_ttc_saisie;
			END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
 		    IF ABS(my_rana_ht_saisie)>ABS(my_rana_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_reduction_ctrl_analytique)');
		    END IF;

		    my_rana_tva_saisie := Recetter_Outils.get_tva(my_rana_ht_saisie, my_rana_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_rana_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_rana_ht_saisie,my_rana_ttc_saisie);

			-- Pour les reductions
			IF my_rana_montant_budgetaire > 0 THEN
   	   	   	   RAISE_APPLICATION_ERROR(-20001, 'C''est une reduction le montant doit etre negatif (ins_reduction_ctrl_analytique)');
			END IF;

			-- verifier que le code analytique qu'on enleve ne fait pas passer en negatif la montant de la recette de depart
			--     moins la somme des reductions pour ce code analytique et que le code existe bien au depart.
			-- et que la facture correspondante est le meme facture_ctrl_analytique.
			SELECT rec_id_reduction INTO my_rec_id_reduction FROM RECETTE WHERE rec_id = a_rec_id;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_ANALYTIQUE
			   WHERE can_id = my_can_id
				 AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
				  OR rec_id_reduction = my_rec_id_reduction);

			IF my_nb=0 THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Pour cette reduction et ce code analytique il n''existe pas d''equivalent pour cette recette (ins_reduction_ctrl_analytique)');
			END IF;

            if (a_verifierMontant>1) then
                SELECT NVL(SUM(rana_montant_budgetaire),0) INTO sum_rana_montant_budgetaire
                  FROM RECETTE_CTRL_ANALYTIQUE WHERE can_id = my_can_id
                   AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
                    OR rec_id_reduction = my_rec_id_reduction);
                IF sum_rana_montant_budgetaire + my_rana_montant_budgetaire < 0 THEN
                   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette recette pour ce code analytique (ins_reduction_ctrl_analytique)');
                END IF;
            end if;
			-- on teste si il n'y a pas assez.
      		IF ABS(my_rec_montant_budgetaire) <= ABS(my_somme + my_rana_montant_budgetaire) THEN
			    my_rana_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a un seul code analytique
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		       --RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''un seul code analytique pour une recette (ins_recette_ctrl_analytique)');
			--END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_analytique_seq.NEXTVAL INTO my_rana_id FROM dual;

			INSERT INTO RECETTE_CTRL_ANALYTIQUE VALUES (my_rana_id,
			       a_exe_ordre, a_rec_id, my_can_id, my_rana_montant_budgetaire,
				   my_rana_ht_saisie, my_rana_tva_saisie, my_rana_ttc_saisie, SYSDATE);

	   		-- procedure de verification
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
			Apres_Recette.ins_recette_ctrl_analytique(my_rana_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_rana_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_reduction_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer
   ) IS
       my_rcon_id	               RECETTE_CTRL_CONVENTION.rcon_id%TYPE;
       my_con_ordre	  	   		   RECETTE_CTRL_CONVENTION.con_ordre%TYPE;
       my_rcon_montant_budgetaire  RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
	   sum_rcon_montant_budgetaire RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
       my_rcon_ht_saisie	  	   RECETTE_CTRL_CONVENTION.rcon_ht_saisie%TYPE;
       my_rcon_tva_saisie		   RECETTE_CTRL_CONVENTION.rcon_tva_saisie%TYPE;
       my_rcon_ttc_saisie		   RECETTE_CTRL_CONVENTION.rcon_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_rec_id_reduction		   RECETTE.rec_id_reduction%TYPE;
	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_reduction_ctrl_convention)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_reduction_ctrl_convention)');
		END IF;

		-- on verifie que la recette a aussi un montant negatif.
		IF my_rec_montant_budgetaire >= 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Recette correspondante n''est pas une reduction (ins_reduction_ctrl_convention)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_con_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rcon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rcon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			-- si les montants sont positifs, on inverse...
			IF my_rcon_ht_saisie > 0 THEN
			   my_rcon_ht_saisie := -my_rcon_ht_saisie;
			END IF;
			IF my_rcon_ttc_saisie > 0 THEN
			   my_rcon_ttc_saisie := -my_rcon_ttc_saisie;
			END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
 		    IF ABS(my_rcon_ht_saisie)>ABS(my_rcon_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_reduction_ctrl_convention)');
		    END IF;

		    my_rcon_tva_saisie := Recetter_Outils.get_tva(my_rcon_ht_saisie, my_rcon_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_rcon_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_rcon_ht_saisie,my_rcon_ttc_saisie);

			-- Pour les reductions
			IF my_rcon_montant_budgetaire > 0 THEN
   	   	   	   RAISE_APPLICATION_ERROR(-20001, 'C''est une reduction le montant doit etre negatif (ins_reduction_ctrl_convention)');
			END IF;

			-- verifier que la convention qu'on enleve ne fait pas passer en negatif la montant de la recette de depart
			--     moins la somme des reductions pour cette convention et que la convention existe bien au depart.
			-- et que la facture correspondante est le meme facture_ctrl_convention.
			SELECT rec_id_reduction INTO my_rec_id_reduction FROM RECETTE WHERE rec_id = a_rec_id;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_CONVENTION
			   WHERE con_ordre = my_con_ordre
				 AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
				  OR rec_id_reduction = my_rec_id_reduction);

			IF my_nb=0 THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Pour cette reduction et cette convention il n''existe pas d''equivalent pour cette recette (ins_reduction_ctrl_convention)');
			END IF;

            if (a_verifierMontant>1) then
                SELECT NVL(SUM(rcon_montant_budgetaire),0) INTO sum_rcon_montant_budgetaire
                  FROM RECETTE_CTRL_CONVENTION WHERE con_ordre = my_con_ordre
                   AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
                    OR rec_id_reduction = my_rec_id_reduction);
                IF sum_rcon_montant_budgetaire + my_rcon_montant_budgetaire < 0 THEN
                   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette recette pour cette convention (ins_reduction_ctrl_convention)');
                END IF;
            end if;
            
			-- on teste si il n'y a pas assez.
      		IF ABS(my_rec_montant_budgetaire) <= ABS(my_somme + my_rcon_montant_budgetaire) THEN
			    my_rcon_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule convention
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_CONVENTION WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		       --RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une seule convention pour une recette (ins_recette_ctrl_convention)');
			--END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_convention_seq.NEXTVAL INTO my_rcon_id FROM dual;

			INSERT INTO RECETTE_CTRL_CONVENTION VALUES (my_rcon_id,
			       a_exe_ordre, a_rec_id, my_con_ordre, my_rcon_montant_budgetaire,
				   my_rcon_ht_saisie, my_rcon_tva_saisie, my_rcon_ttc_saisie, SYSDATE);

	   		-- procedure de verification
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_con_ordre);
			Apres_Recette.ins_recette_ctrl_convention(my_rcon_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_rcon_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_reduction_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer
   ) IS
	   my_sum_rpco_ht_saisie	   RECETTE_CTRL_PLANCO.rpco_ht_saisie%TYPE;
       my_rpco_id	               RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_rpco_ht_saisie	  	   RECETTE_CTRL_PLANCO.rpco_ht_saisie%TYPE;
       my_rpco_tva_saisie	  	   RECETTE_CTRL_PLANCO.rpco_tva_saisie%TYPE;
       my_rpco_ttc_saisie	  	   RECETTE_CTRL_PLANCO.rpco_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_rec_id_reduction		   RECETTE.rec_id_reduction%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_fac_ht_reste             FACTURE_CTRL_PLANCO.fpco_ht_reste%TYPE;
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_reduction_ctrl_planco)');
		END IF;

		SELECT r.rec_montant_budgetaire, f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_reduction_ctrl_planco)');
		END IF;

		-- on verifie que la recette a aussi un montant negatif.
		IF my_rec_montant_budgetaire >= 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Recette correspondante n''est pas une reduction (ins_reduction_ctrl_planco)');
		END IF;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			-- si les montants sont positifs, on inverse...
			IF my_rpco_ht_saisie > 0 THEN
			   my_rpco_ht_saisie := -my_rpco_ht_saisie;
			END IF;
			IF my_rpco_ttc_saisie > 0 THEN
			   my_rpco_ttc_saisie := -my_rpco_ttc_saisie;
			END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_rpco_ht_saisie) > ABS(my_rpco_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_reduction_ctrl_planco)');
		    END IF;

		    my_rpco_tva_saisie := Recetter_Outils.get_tva(my_rpco_ht_saisie, my_rpco_ttc_saisie);

			-- verifier que le planco qu'on enleve ne fait pas passer en negatif la montant de la recette de depart
			--     moins la somme des reductions pour ce planco et que le planco existe bien au depart.
			-- et que la facture correspondante est le meme facture_ctrl_planco.
			SELECT rec_id_reduction INTO my_rec_id_reduction FROM RECETTE WHERE rec_id = a_rec_id;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO
			   WHERE pco_num = my_pco_num
				 AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
				  OR rec_id_reduction = my_rec_id_reduction);

			IF my_nb=0 THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Pour cette reduction et ce planco il n''existe pas d''equivalent pour cette recette (ins_reduction_ctrl_planco)');
			END IF;

            if (a_verifierMontant>1) then
                SELECT NVL(SUM(rpco_ht_saisie),0) INTO my_sum_rpco_ht_saisie
                  FROM RECETTE_CTRL_PLANCO WHERE pco_num = my_pco_num
                   AND rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
                    OR rec_id_reduction = my_rec_id_reduction);
                IF my_sum_rpco_ht_saisie + my_rpco_ht_saisie < 0 THEN
                   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette recette pour ce planco (ins_reduction_ctrl_planco)');
                END IF;
           end if;

			-- on bloque a une seule imputation
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une recette (ins_reduction_ctrl_planco)');
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_seq.NEXTVAL INTO my_rpco_id FROM dual;
			INSERT INTO RECETTE_CTRL_PLANCO VALUES (my_rpco_id,
			       a_exe_ordre, a_rec_id, my_pco_num, NULL, my_rpco_ht_saisie, my_rpco_tva_saisie, my_rpco_ttc_saisie, SYSDATE, Get_Tbo_Ordre(a_rec_id));

   	   		-- procedure de verification
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco(my_rpco_id);
 		END LOOP;
   END;

PROCEDURE ins_reduction_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer
   ) IS
       my_rpco_id	           	   RECETTE_CTRL_PLANCO.rpco_id%TYPE;
	   my_sum_rpcotva_tva_saisie   RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
       my_rpcotva_id	           RECETTE_CTRL_PLANCO_TVA.rpcotva_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO_TVA.pco_num%TYPE;
       my_rpcotva_tva_saisie	   RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_default_ges_code		   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_rec_id_reduction		   RECETTE.rec_id_reduction%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_fac_tva_reste            FACTURE_CTRL_PLANCO.fpco_tva_reste%TYPE;
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_reduction_ctrl_planco_tva)');
		END IF;

		SELECT r.rec_montant_budgetaire, f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_reduction_ctrl_planco_tva)');
		END IF;

		-- on verifie que la recette a aussi un montant negatif.
		IF my_rec_montant_budgetaire >= 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Recette correspondante n''est pas une reduction (ins_reduction_ctrl_planco_tva)');
		END IF;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcotva_tva_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			-- si les montants sont positifs, on inverse...
			IF my_rpcotva_tva_saisie > 0 THEN
			   my_rpcotva_tva_saisie := -my_rpcotva_tva_saisie;
			END IF;

			-- verifier que le planco qu'on enleve ne fait pas passer en negatif la montant de la recette de depart
			--     moins la somme des reductions pour ce planco et que le planco existe bien au depart.
			-- et que la facture correspondante est le meme facture_ctrl_planco.
			SELECT rec_id_reduction INTO my_rec_id_reduction FROM RECETTE WHERE rec_id = a_rec_id;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO_TVA
			   WHERE pco_num = my_pco_num
				 AND rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN
				 	 (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
				  	 		 OR rec_id_reduction = my_rec_id_reduction));

			IF my_nb=0 THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Pour cette reduction et ce planco il n''existe pas d''equivalent pour cette recette (ins_reduction_ctrl_planco_tva)');
			END IF;
            
            if (a_verifierMontant>1) then
                SELECT NVL(SUM(rpcotva_tva_saisie),0) INTO my_sum_rpcotva_tva_saisie
                  FROM RECETTE_CTRL_PLANCO_TVA WHERE pco_num = my_pco_num
                   AND rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
                    OR rec_id_reduction = my_rec_id_reduction));
                IF my_sum_rpcotva_tva_saisie + my_rpcotva_tva_saisie < 0 THEN
                   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette recette pour ce planco tva (ins_reduction_ctrl_planco_tva)');
                END IF;
            end if;
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation tva doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = ''
			THEN
				my_ges_code := my_default_ges_code;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_tva_seq.NEXTVAL INTO my_rpcotva_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO_TVA VALUES (my_rpcotva_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcotva_tva_saisie, SYSDATE, my_ges_code);

   	   		-- procedure de verification
			Verifier.verifier_planco_tva(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco_tva(my_rpcotva_id);
		END LOOP;
   END;

PROCEDURE ins_reduction_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2,
       a_verifierMontant     integer
   ) IS
       my_rpco_id	           	   RECETTE_CTRL_PLANCO.rpco_id%TYPE;
	   my_sum_rpcoctp_ttc_saisie   RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;
       my_rpcoctp_id	           RECETTE_CTRL_PLANCO_CTP.rpcoctp_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO_CTP.pco_num%TYPE;
       my_rpcoctp_ttc_saisie	   RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_default_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_par_value				   maracuja.parametre.par_value%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_rec_id_reduction		   RECETTE.rec_id_reduction%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_reduction_ctrl_planco_ctp)');
		END IF;

		SELECT r.rec_montant_budgetaire, f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_reduction_ctrl_planco_ctp)');
		END IF;

		-- on verifie que la recette a aussi un montant negatif.
		IF my_rec_montant_budgetaire >= 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Recette correspondante n''est pas une reduction (ins_reduction_ctrl_planco_ctp)');
		END IF;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;
		SELECT par_value INTO my_par_value
           FROM maracuja.PARAMETRE
           WHERE UPPER(par_key) = 'CONTRE PARTIE VISA RECETTE'
           AND exe_ordre = my_exe_ordre;

		IF UPPER(my_par_value) <> 'COMPOSANTE' THEN
		   SELECT c.ges_code INTO my_default_ges_code
			FROM maracuja.GESTION g, maracuja.COMPTABILITE c, maracuja.GESTION_EXERCICE ge
			WHERE g.ges_code = my_default_ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=my_exe_ordre;
		END IF;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcoctp_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			-- si le montant est positif, on inverse...
			IF my_rpcoctp_ttc_saisie > 0 THEN
			   my_rpcoctp_ttc_saisie := -my_rpcoctp_ttc_saisie;
			END IF;

			-- verifier que le planco qu'on enleve ne fait pas passer en negatif la montant de la recette de depart
			--     moins la somme des reductions pour ce planco et que le planco existe bien au depart.
			-- et que la facture correspondante est le meme facture_ctrl_planco.
			SELECT rec_id_reduction INTO my_rec_id_reduction FROM RECETTE WHERE rec_id = a_rec_id;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO_CTP
			   WHERE pco_num = my_pco_num
				 AND rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN
				 	 (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
				  	 		 OR rec_id_reduction = my_rec_id_reduction));

			IF my_nb=0 THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Pour cette reduction et ce planco il n''existe pas d''equivalent pour cette recette (ins_reduction_ctrl_planco_ctp)');
			END IF;

            if (a_verifierMontant>1) then
                SELECT NVL(SUM(rpcoctp_ttc_saisie),0) INTO my_sum_rpcoctp_ttc_saisie
                  FROM RECETTE_CTRL_PLANCO_CTP WHERE pco_num = my_pco_num
                   AND rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE rec_id = my_rec_id_reduction
                    OR rec_id_reduction = my_rec_id_reduction));
                IF my_sum_rpcoctp_ttc_saisie + my_rpcoctp_ttc_saisie < 0 THEN
                   RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette recette pour ce planco contrepartie (ins_reduction_ctrl_planco_ctp)');
                END IF;
            end if;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation tva doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = ''
			THEN
				my_ges_code := my_default_ges_code;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_ctp_seq.NEXTVAL INTO my_rpcoctp_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO_CTP VALUES (my_rpcoctp_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcoctp_ttc_saisie, SYSDATE, my_ges_code);

   	   		-- procedure de verification
			Verifier.verifier_planco_ctp(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco_ctp(my_rpcoctp_id);
		END LOOP;
   END;

END;
/


