SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.2 BETA
-- Date de publication : 23/10/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Modifications pour intégration de l'extourne
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2055';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/

create or replace procedure grhum.drop_constraint (ownerconstraint varchar2, tableconstraint varchar2, nameconstraint varchar2)
is
    -- Suppression d'une contrainte si elle existe
    
   flag   smallint;
begin
   flag := 0;

   select count (constraint_name)
   into   flag
   from   all_constraints
   where  upper (owner) = upper (ownerconstraint) and upper (table_name) = upper (tableconstraint) and upper (constraint_name) = upper (nameconstraint);

   if flag > 0 then
      execute immediate 'ALTER TABLE ' || ownerconstraint || '.' || tableconstraint || ' DROP CONSTRAINT ' || nameconstraint;
   end if;
end;
/




-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_depense','extourne_liq_repart','table');
execute grhum.drop_object('jefy_depense','extourne_liq_def','table');
execute grhum.drop_object('jefy_depense','extourne_liq','table');

execute grhum.drop_object('jefy_depense','extourne_liq_seq','sequence');
execute grhum.drop_object('jefy_depense','extourne_liq_def_seq','sequence');
execute grhum.drop_object('jefy_depense','extourne_liq_repart_seq','sequence');



create   sequence jefy_depense.extourne_liq_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
create sequence jefy_depense.extourne_liq_def_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
create sequence jefy_depense.extourne_liq_repart_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_depense.extourne_liq (
  el_id            number not null,
  dep_id_n         number null,
  dep_id_n1        number not null
)
tablespace gfc;

comment on table jefy_depense.extourne_liq is 'Memorise une liquidation d''extourne sur N+1 et sa liquidation initiale correspondante sur N';
comment on column jefy_depense.extourne_liq.dep_id_n is 'Reference a la liquidation initiale sur N (null dans le cas de demarrage d''un exercice)';
comment on column jefy_depense.extourne_liq.dep_id_n1 is 'Reference a la liquidation d''extourne sur N+1';

alter table jefy_depense.extourne_liq add (primary key (el_id) using index tablespace gfc_indx);
alter table jefy_depense.extourne_liq add (constraint fk_extourne_liq_depidn  foreign key (dep_id_n)  references jefy_depense.depense_budget (dep_id));
alter table jefy_depense.extourne_liq add (constraint fk_extourne_liq_depidn1  foreign key (dep_id_n1)  references jefy_depense.depense_budget (dep_id));
alter table jefy_depense.extourne_liq add (constraint uk_extourne_liq  unique (dep_id_n, dep_id_n1) using index tablespace gfc_indx);

-----------

create table jefy_depense.extourne_liq_def (
  eld_id            number not null,
  dep_id        number not null,
  tyet_id           number not null
)
tablespace gfc;

comment on table jefy_depense.extourne_liq_def is 'Memorise une liquidation définitive avec flag pour savoir si c’est une liquidation sur extourne ou sur budget';
comment on column jefy_depense.extourne_liq_def.dep_id is 'Reference a la liquidation definitive';
COMMENT ON COLUMN JEFY_DEPENSE.EXTOURNE_LIQ_DEF.TYET_ID IS 'Reference a la constante indiquant si liquidation sur extourne (220) ou sur budget (221)';



alter table jefy_depense.extourne_liq_def add (primary key (eld_id) using index tablespace gfc_indx);
alter table jefy_depense.extourne_liq_def add (constraint fk_extourne_liq_def_depid  foreign key (dep_id)  references jefy_depense.depense_budget (dep_id));
alter table jefy_depense.extourne_liq_def add (constraint fk_extourne_liq_def_tyetid  foreign key (tyet_id)  references jefy_admin.type_etat (tyet_id));
alter table jefy_depense.extourne_liq_def add (constraint uk_extourne_liq_def  unique (dep_id) using index tablespace gfc_indx);
alter table jefy_depense.extourne_liq_def add (constraint chk_extourne_liq_tyet_id  check (tyet_id in (220,221)));



-----------

create table jefy_depense.extourne_liq_repart (
  elr_id        number not null,
  el_id            number not null,
  eld_id        number not null
)
tablespace gfc;

comment on table jefy_depense.extourne_liq_repart is 'Lien entre la liquidation definitive et les liquidations d’extourne correspondantes. Si liquidation definitive sur poche, pas d’enregistrement';
comment on column jefy_depense.extourne_liq_repart.el_id is 'Reference a la liquidation d''extourne';
comment on column jefy_depense.extourne_liq_repart.eld_id is 'Reference a la liquidation definitive';

alter table jefy_depense.extourne_liq_repart add (primary key (elr_id) using index tablespace gfc_indx );
alter table jefy_depense.extourne_liq_repart add (constraint fk_extourne_liq_repart_elid  foreign key (el_id)  references jefy_depense.extourne_liq (el_id) deferrable initially deferred  );
alter table jefy_depense.extourne_liq_repart add (constraint fk_extourne_liq_repart_eldid  foreign key (eld_id)  references jefy_depense.extourne_liq_def (eld_id) deferrable initially deferred);
alter table jefy_depense.extourne_liq_repart add (constraint uk_extourne_liq_repart  unique (el_id,eld_id) using index tablespace gfc_indx);

execute grhum.drop_constraint('jefy_depense','engage_ctrl_action','ch_engage_ctrl_action_2');
alter table jefy_depense.engage_ctrl_action add (constraint ch_engage_ctrl_action_2 check ((eact_ht_saisie>=0 and eact_montant_budgetaire>=eact_ht_saisie) or (eact_ht_saisie<0 and eact_montant_budgetaire=0)));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_action','ch_engage_ctrl_action_3');
alter table jefy_depense.engage_ctrl_action add (constraint ch_engage_ctrl_action_3 check ((eact_ttc_saisie>0 and eact_montant_budgetaire>=0 and eact_montant_budgetaire<=eact_ttc_saisie) or (eact_ttc_saisie<=0 and eact_montant_budgetaire=0)));


execute grhum.drop_constraint('jefy_depense','engage_ctrl_hors_marche','ch_engage_ctrl_hors_marche_2');
alter table jefy_depense.engage_ctrl_hors_marche add (constraint ch_engage_ctrl_hors_marche_2 check ((ehom_ht_saisie>=0 and ehom_montant_budgetaire>=ehom_ht_saisie) or (ehom_ht_saisie<0 and ehom_montant_budgetaire=0)));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_hors_marche','ch_engage_ctrl_hors_marche_3');
alter table jefy_depense.engage_ctrl_hors_marche add (constraint ch_engage_ctrl_hors_marche_3 check ((ehom_ttc_saisie>0 and ehom_montant_budgetaire>=0 and ehom_montant_budgetaire<=ehom_ttc_saisie) or (ehom_ttc_saisie<=0 and ehom_montant_budgetaire=0)));

execute grhum.drop_constraint('jefy_depense','engage_budget','ch_engage_budget_2');
alter table jefy_depense.engage_budget add (constraint ch_engage_budget_2 check ((eng_ht_saisie>=0 and eng_montant_budgetaire>=eng_ht_saisie) or (eng_ht_saisie<0 and eng_montant_budgetaire=0) or (eng_ht_saisie>0 and eng_montant_budgetaire=0)));
execute grhum.drop_constraint('jefy_depense','engage_budget','ch_engage_budget_3');
alter table jefy_depense.engage_budget add ( constraint ch_engage_budget_3 check ((eng_ttc_saisie>0 and eng_montant_budgetaire>=0 and eng_montant_budgetaire<=eng_ttc_saisie) or    (eng_ttc_saisie<=0 and eng_montant_budgetaire=0)     or (eng_ttc_saisie>0 and eng_montant_budgetaire=0)  ) );
  
execute grhum.drop_constraint('jefy_depense','depense_budget','ch_depense_budget_2');
alter table jefy_depense.depense_budget add (  constraint ch_depense_budget_2 check (dep_montant_budgetaire=0 or abs(dep_montant_budgetaire)>=abs(dep_ht_saisie))); 
 
execute grhum.drop_constraint('jefy_depense','depense_ctrl_analytique','ch_depense_ctrl_analytique_2');
alter table jefy_depense.depense_ctrl_analytique add (constraint ch_depense_ctrl_analytique_2 check ( (dana_montant_budgetaire=0) or  abs(dana_montant_budgetaire)>=abs(dana_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','depense_ctrl_convention','ch_depense_ctrl_convention_2');
alter table jefy_depense.depense_ctrl_convention add (constraint ch_depense_ctrl_convention_2 check ((dcon_montant_budgetaire=0) or abs(dcon_montant_budgetaire)>=abs(dcon_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','depense_ctrl_marche','ch_depense_ctrl_marche_2');
alter table jefy_depense.depense_ctrl_marche add (constraint ch_depense_ctrl_marche_2 check ((dmar_montant_budgetaire=0) or abs(dmar_montant_budgetaire)>=abs(dmar_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','depense_ctrl_planco','ch_depense_ctrl_planco_2');
alter table jefy_depense.depense_ctrl_planco add (  constraint ch_depense_ctrl_planco_2 check ( (dpco_montant_budgetaire=0) or (abs(dpco_montant_budgetaire)>=abs(dpco_ht_saisie))));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_analytique','ch_pdepense_ctrl_analytique_2');
alter table jefy_depense.pdepense_ctrl_analytique add (constraint ch_pdepense_ctrl_analytique_2 check ( (pdana_montant_budgetaire=0) or  abs(pdana_montant_budgetaire)>=abs(pdana_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_convention','ch_pdepense_ctrl_convention_2');
alter table jefy_depense.pdepense_ctrl_convention add (constraint ch_pdepense_ctrl_convention_2 check ((pdcon_montant_budgetaire=0) or abs(pdcon_montant_budgetaire)>=abs(pdcon_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_marche','ch_pdepense_ctrl_marche_2');
alter table jefy_depense.pdepense_ctrl_marche add (constraint ch_pdepense_ctrl_marche_2 check ((pdmar_montant_budgetaire=0) or abs(pdmar_montant_budgetaire)>=abs(pdmar_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_planco','ch_pdepense_ctrl_planco_2');
alter table jefy_depense.pdepense_ctrl_planco add (  constraint ch_pdepense_ctrl_planco_2 check ( (pdpco_montant_budgetaire=0) or (abs(pdpco_montant_budgetaire)>=abs(pdpco_ht_saisie))));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_analytique','ch_engage_ctrl_analytique_2');
alter table jefy_depense.engage_ctrl_analytique add (constraint ch_engage_ctrl_analytique_2 check ((eana_montant_budgetaire=0) or (eana_ht_saisie>=0 and eana_montant_budgetaire>=eana_ht_saisie) ));
execute grhum.drop_constraint('jefy_depense','engage_ctrl_analytique','ch_engage_ctrl_analytique_3');
alter table jefy_depense.engage_ctrl_analytique add (constraint ch_engage_ctrl_analytique_3 check ((eana_montant_budgetaire=0) or (eana_ttc_saisie>0 and eana_montant_budgetaire>=0 and eana_montant_budgetaire<=eana_ttc_saisie) ));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_convention','ch_engage_ctrl_convention_2');
alter table jefy_depense.engage_ctrl_convention add (constraint ch_engage_ctrl_convention_2 check ( (econ_montant_budgetaire=0)  or (econ_ht_saisie>=0 and econ_montant_budgetaire>=econ_ht_saisie)      ));
execute grhum.drop_constraint('jefy_depense','engage_ctrl_convention','ch_engage_ctrl_convention_3');
alter table jefy_depense.engage_ctrl_convention add (constraint ch_engage_ctrl_convention_3 check ( (econ_montant_budgetaire=0) or (econ_ttc_saisie>0 and econ_montant_budgetaire>=0 and econ_montant_budgetaire<=econ_ttc_saisie) ));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_marche','ch_engage_ctrl_marche_2');
alter table jefy_depense.engage_ctrl_marche add (  constraint ch_engage_ctrl_marche_2 check (( emar_montant_budgetaire=0) or  (emar_ht_saisie>=0 and emar_montant_budgetaire>=emar_ht_saisie) ));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_marche','ch_engage_ctrl_marche_3');
alter table jefy_depense.engage_ctrl_marche add (  constraint ch_engage_ctrl_marche_3 check ((emar_montant_budgetaire=0) or (emar_ttc_saisie>0 and emar_montant_budgetaire>=0 and emar_montant_budgetaire<=emar_ttc_saisie)));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_planco','ch_engage_ctrl_planco_2');
alter table jefy_depense.engage_ctrl_planco add (constraint ch_engage_ctrl_planco_2 check ((epco_montant_budgetaire=0) or   (epco_montant_budgetaire>0 and epco_montant_budgetaire>=epco_ht_saisie)  ));
execute grhum.drop_constraint('jefy_depense','engage_ctrl_planco','ch_engage_ctrl_planco_3');
alter table jefy_depense.engage_ctrl_planco add (constraint ch_engage_ctrl_planco_3 check ( (epco_montant_budgetaire=0) or (epco_ttc_saisie>0 and epco_montant_budgetaire>=0 and epco_montant_budgetaire<=epco_ttc_saisie) ));

  
  
CREATE OR REPLACE FORCE VIEW JEFY_ADMIN.V_UB_FOR_ORGAN
(ORG_ID, ORG_ID_UB)
AS 
select     org_id,
                 connect_by_root org_id as org_id_ub
      from       jefy_admin.organ
      where      level > 0
      start with org_niv = 2
      connect by prior org_id = org_pere;
/

CREATE OR REPLACE FORCE VIEW JEFY_ADMIN.V_CR_FOR_ORGAN
(ORG_ID, ORG_ID_CR)
AS 
select     org_id,
                 connect_by_root org_id as org_id_cr
      from       jefy_admin.organ
      where      level > 0
      start with org_niv = 3
      connect by prior org_id = org_pere;
/
grant select on jefy_admin.v_cr_for_organ to jefy_depense with grant option;
grant select on jefy_admin.v_ub_for_organ to jefy_depense with grant option;

create or replace force view jefy_depense.v_extourne_liq_suivi (dep_id_ext, dpco_id_ext, dep_id_def, dpco_id_def)
as
   select el.dep_id_n1 as dep_id_ext,
          dpcoext.dpco_id dpco_id_ext,
          eld.dep_id as dep_id_def,
          dpcodef.dpco_id as dpco_id_def
   from   jefy_depense.extourne_liq el inner join jefy_depense.depense_ctrl_planco dpcoext on (dpcoext.dep_id = el.dep_id_n1)
          left outer join jefy_depense.extourne_liq_repart elr on (elr.el_id = el.el_id)
          left outer join jefy_depense.extourne_liq_def eld on (eld.eld_id = elr.eld_id)
          left outer join jefy_depense.depense_ctrl_planco dpcodef on (dpcodef.dep_id = eld.dep_id);
grant select on jefy_depense.v_extourne_liq_suivi to maracuja with grant option;
grant select on jefy_depense.v_extourne_liq_suivi to comptefi with grant option;






create or replace force view jefy_depense.v_extourne_credits_ub (exe_ordre, org_id_ub, tcd_ordre, vec_montant_ht, vec_montant_ttc, vec_montant_bud_initial, vec_montant_bud_consomme, vec_montant_bud_disponible, vec_montant_bud_dispo_reel)
as
   select   exe_ordre,
            org_id_ub,
            tcd_ordre,
            sum (vec_montant_ht) vec_montant_ht,
            sum (vec_montant_ttc) vec_montant_ttc,
            sum (vec_montant_bud_initial) vec_montant_bud_initial,
            sum (vec_montant_bud_consomme) vec_montant_bud_consomme,
            (sum (vec_montant_bud_initial) + sum (vec_montant_bud_consomme)) as vec_montant_bud_disponible,
            (sum (vec_montant_bud_initial) + sum (vec_montant_bud_consomme)) as vec_montant_bud_dispo_reel
   from     (
-- engagements des liquidations d'extourne (négatifs)
             select   eb.exe_ordre,
                      eb.org_id,
                      ub.org_id_ub,
                      eb.tcd_ordre,
                      sum (db.dep_ht_saisie) vec_montant_ht,
                      sum (db.dep_ttc_saisie) vec_montant_ttc,
                      sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_initial,
                      0 as vec_montant_bud_consomme
             from     depense_budget db inner join extourne_liq el on (db.dep_id = dep_id_n1)
                      inner join engage_budget eb on (db.eng_id = eb.eng_id)
                      inner join jefy_admin.v_ub_for_organ ub on (ub.org_id = eb.org_id)
             where    eb.eng_ttc_saisie <> 0
             group by eb.exe_ordre, eb.org_id, ub.org_id_ub, eb.tcd_ordre
             union all
-- engagements crees via liquidation définitive sur poche (positifs)
             select   eb.exe_ordre,
                      eb.org_id,
                      ub.org_id_ub,
                      eb.tcd_ordre,
                      sum (db.dep_ht_saisie) vec_montant_ht,
                      sum (db.dep_ttc_saisie) vec_montant_ttc,
                      0 as vec_montant_bud_initial,
                      sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_consomme
             from     depense_budget db
                      inner join
                      (select eld.dep_id
                       from   extourne_liq_def eld left join extourne_liq_repart elr on (eld.eld_id = elr.eld_id)
                       where  elr.eld_id is null and eld.tyet_id = 220) x on (db.dep_id = x.dep_id)
                      inner join engage_budget eb on (db.eng_id = eb.eng_id)
                      inner join jefy_admin.v_ub_for_organ ub on (ub.org_id = eb.org_id)
             group by eb.exe_ordre, eb.org_id, ub.org_id_ub, eb.tcd_ordre)
   group by exe_ordre, org_id_ub, tcd_ordre;
/




--Montant de la poche de crédit d’extourne : Somme des montant budgétaires recalculés (ex. TTC- tva proratisée) 
--des engagements correspondant aux liquidations d'extourne (N+1) 
--et des engagements créés lors de la liquidation définitive sur poche
-- (présent dans la table extourne_liq_def mais non présents dans la table extourne_liq_repart).
create or replace force view jefy_depense.v_extourne_credits_cr (exe_ordre,
                                                                 org_id_cr,
                                                                 tcd_ordre,
                                                                 vec_montant_ht,
                                                                 vec_montant_ttc,
                                                                 vec_montant_bud_initial,
                                                                 vec_montant_bud_consomme,
                                                                 vec_montant_bud_disponible,
                                                                 vec_montant_bud_dispo_ub,
                                                                 vec_montant_bud_dispo_reel
                                                                )
as
   select cr.exe_ordre,
          cr.org_id_cr,
          cr.tcd_ordre,
          cr.vec_montant_ht,
          cr.vec_montant_ttc,
          cr.vec_montant_bud_initial,
          cr.vec_montant_bud_consomme,
          cr.vec_montant_bud_disponible,
          ub.vec_montant_bud_disponible as vec_montant_bud_dispo_ub,
          decode (sign (cr.vec_montant_bud_disponible - ub.vec_montant_bud_disponible), -1, ub.vec_montant_bud_disponible, cr.vec_montant_bud_disponible) as vec_montant_bud_dispo_reel
   from   (select   exe_ordre,
                    org_id_cr,
                    tcd_ordre,
                    sum (vec_montant_ht) vec_montant_ht,
                    sum (vec_montant_ttc) vec_montant_ttc,
                    sum (vec_montant_bud_initial) vec_montant_bud_initial,
                    sum (vec_montant_bud_consomme) vec_montant_bud_consomme,
                    (sum (vec_montant_bud_initial) + sum (vec_montant_bud_consomme)) as vec_montant_bud_disponible
           from     (
-- engagements des liquidations d'extourne (négatifs)
                     select   eb.exe_ordre,
                              eb.org_id,
                              ub.org_id_cr,
                              eb.tcd_ordre,
                              sum (db.dep_ht_saisie) vec_montant_ht,
                              sum (db.dep_ttc_saisie) vec_montant_ttc,
                              sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_initial,
                              0 as vec_montant_bud_consomme
                     from     depense_budget db inner join extourne_liq el on (db.dep_id = dep_id_n1)
                              inner join engage_budget eb on (db.eng_id = eb.eng_id)
                              inner join jefy_admin.v_cr_for_organ ub on (ub.org_id = eb.org_id)
                     where    eb.eng_ttc_saisie <> 0
                     group by eb.exe_ordre, eb.org_id, ub.org_id_cr, eb.tcd_ordre
                     union all
-- engagements crees via liquidation définitive sur poche (positifs) pour le CR
                     select   eb.exe_ordre,
                              eb.org_id,
                              ub.org_id_cr,
                              eb.tcd_ordre,
                              sum (db.dep_ht_saisie) vec_montant_ht,
                              sum (db.dep_ttc_saisie) vec_montant_ttc,
                              0 as vec_montant_bud_initial,
                              sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_consomme
                     from     depense_budget db
                              inner join
                              (select eld.dep_id
                               from   extourne_liq_def eld left join extourne_liq_repart elr on (eld.eld_id = elr.eld_id)
                               where  elr.eld_id is null and eld.tyet_id = 220) x on (db.dep_id = x.dep_id)
                              inner join engage_budget eb on (db.eng_id = eb.eng_id)
                              inner join jefy_admin.v_cr_for_organ ub on (ub.org_id = eb.org_id)
                     group by eb.exe_ordre, eb.org_id, ub.org_id_cr, eb.tcd_ordre)
           group by exe_ordre, org_id_cr, tcd_ordre) cr
          inner join
          jefy_admin.v_ub_for_organ ubo on (ubo.org_id = cr.org_id_cr)
          inner join v_extourne_credits_ub ub on (ub.exe_ordre = cr.exe_ordre and ub.tcd_ordre = cr.tcd_ordre and ubo.org_id_ub = ub.org_id_ub)
          ;
/

 


CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Liquider  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE);

PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type);

PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type );

PROCEDURE ins_depense (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_chaine_planco         VARCHAR2);

PROCEDURE ins_depense_extourne (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id IN OUT         DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
	  a_fou_ordre			  ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				  ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			  ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			  ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_tyap_id				  ENGAGE_BUDGET.tyap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      --a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      --a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_chaine_planco         VARCHAR2);

PROCEDURE ins_depense_directe (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
	  a_fou_ordre			  ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				  ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			  ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			  ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_tyap_id				  ENGAGE_BUDGET.tyap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_chaine_planco         VARCHAR2);     

PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT         COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE);

PROCEDURE del_depense_budget (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre             Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE del_depense_papier (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);


--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


PROCEDURE log_depense_budget (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre             Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE log_depense_papier (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);

PROCEDURE ins_depense_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE);

PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

END;
/
CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Corriger  IS

-- procedure de correction entre les commandes et ses engagements.
-- peut etre a prevoir dans interface ??

-- correction entre engage_budget et les engage_ctrl_...

PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE);

PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE);

PROCEDURE corriger_engage_extourne (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
        a_dernier                BOOLEAN,
      a_exe_ordre            ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;

PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage            COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant                COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN)
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;

END;
/
CREATE OR REPLACE PACKAGE JEFY_DEPENSE.verifier  IS

PROCEDURE verifier_engage_exercice (
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_utl_ordre      			engage_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_extourne_exercice (
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_utl_ordre      			engage_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_depense_exercice (
      a_exe_ordre               depense_budget.exe_ordre%type,
   	  a_utl_ordre      			depense_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_organ_utilisateur (
   	  a_utl_ordre      			engage_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_budget (
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_tap_id                  engage_budget.tap_id%type,
	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type);

PROCEDURE verifier_action(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_tyac_id                 engage_ctrl_action.tyac_id%type,
	  a_utl_ordre               engage_budget.utl_ordre%type);

PROCEDURE verifier_analytique(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_can_id                  engage_ctrl_analytique.can_id%type);

PROCEDURE verifier_convention(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_conv_ordre              engage_ctrl_convention.conv_ordre%type);

PROCEDURE verifier_hors_marche(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_typa_id                 engage_ctrl_hors_marche.typa_id%type,
	  a_ce_ordre                engage_ctrl_hors_marche.ce_ordre%type,
	  a_fou_ordre				engage_budget.fou_ordre%type);

PROCEDURE verifier_marche(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_fou_ordre               engage_budget.fou_ordre%type,
	  a_att_ordre               engage_ctrl_marche.att_ordre%type,
	  a_utl_ordre               engage_budget.utl_ordre%type);

PROCEDURE verifier_planco(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
   	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_pco_num                 engage_ctrl_planco.pco_num%type,
	  a_utl_ordre               engage_budget.utl_ordre%type);

PROCEDURE verifier_emargement (
      a_exe_ordre               engage_budget.exe_ordre%type,
	  a_mod_ordre				depense_papier.mod_ordre%type,
	  a_ecd_ordre               depense_ctrl_planco.ecd_ordre%type);


	  -- ne sert pas pour le moment --
PROCEDURE verifier_inventaire;

	  -- ne sert pas pour le moment --
PROCEDURE verifier_monnaie;

PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		v_fournisseur.fou_ordre%type);

PROCEDURE verifier_rib (
   	  a_fou_ordre		v_rib_fournisseur.fou_ordre%type,
   	  a_rib_ordre		v_rib_fournisseur.rib_ordre%type,
   	  a_mod_ordre		depense_papier.mod_ordre%type,
   	  a_exe_ordre		depense_papier.exe_ordre%type);

	  -- ne sert pas pour le moment --
PROCEDURE verifier_organ(
   	  a_org_id			engage_budget.org_id%type,
	  a_tcd_ordre		engage_budget.tcd_ordre%type);

PROCEDURE verifier_engage_coherence(a_eng_id engage_budget.eng_id%type);

PROCEDURE verifier_commande_coherence(a_comm_id commande.comm_id%type);

PROCEDURE verifier_cde_budget_coherence(a_cbud_id commande_budget.cbud_id%type);

PROCEDURE verifier_depense_pap_coherence(a_dpp_id depense_papier.dpp_id%type);

PROCEDURE verifier_depense_coherence(a_dep_id depense_budget.dep_id%type);

PROCEDURE verifier_util_engage (
      a_eng_id              engage_budget.eng_id%type);

PROCEDURE verifier_util_depense_budget (
      a_dep_id              depense_budget.dep_id%type);

PROCEDURE verifier_util_depense_papier (
      a_dpp_id              depense_papier.dpp_id%type);

PROCEDURE verifier_util_commande (
      a_comm_id              commande.comm_id%type);

PROCEDURE verifier_util_commande_budget (
      a_cbud_id              commande_budget.cbud_id%type);
END;
/
CREATE OR REPLACE PACKAGE JEFY_DEPENSE.engager  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_engage (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

PROCEDURE ins_engage_extourne_poche (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type);

PROCEDURE del_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           z_engage_budget.zeng_utl_ordre%type);

PROCEDURE solder_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           engage_budget.utl_ordre%type);

PROCEDURE solder_engage_sansdroit (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE);

PROCEDURE upd_engage (
      a_eng_id 		        engage_budget.eng_id%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_utl_ordre			engage_budget.utl_ordre%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

PROCEDURE log_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           z_engage_budget.zeng_utl_ordre%type);

PROCEDURE ins_engage_budget (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type);

PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Liquider
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_depense_papier  depense_papier%rowtype;
      my_pers_id         jefy_admin.utilisateur.pers_id%type;
    BEGIN
      select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 
      
      select * into my_depense_papier from depense_papier where dpp_id=a_dpp_id;
      if my_depense_papier.dpp_date_service_fait is not null then 
         RAISE_APPLICATION_ERROR(-20001, 'La facture a deja une date de service fait ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if;
      
      select pers_id into my_pers_id from jefy_admin.utilisateur where utl_ordre=a_utl_ordre;
      update depense_papier set dpp_date_service_fait=a_dpp_date_service_fait, dpp_sf_pers_id=my_pers_id, dpp_sf_date=sysdate
          where dpp_id=a_dpp_id;      
    END;

   PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_pdep_id         pdepense_budget.pdep_id%type;
      CURSOR liste IS SELECT pdep_id FROM pDEPENSE_budget WHERE dpp_id=a_dpp_id;
    BEGIN
      service_fait(a_dpp_id, a_utl_ordre, a_dpp_date_service_fait);
      
      select count(*) into my_nb from pdepense_budget where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''a pas de pre-liquidation ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 

      OPEN liste();
      LOOP
         FETCH liste INTO my_pdep_id;
         EXIT WHEN liste%NOTFOUND;

         pre_liquider.liquider_pdepense_budget(my_pdep_id, a_utl_ordre);
      END LOOP;
      CLOSE liste;
    END;

   PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
   BEGIN
        ins_depense_papier_avec_IM(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
            a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
            a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, null, null, null);
   end;
   
PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type
   ) IS
   BEGIN
      ins_depense_papier_avec_im_sf(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
          a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception, a_dpp_date_service_fait, 
          a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, a_dpp_im_taux, a_dpp_im_dgp, imtt_id, null, null,null);
   END;
   
   PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type 
    ) IS
      my_dpp_ht_initial       DEPENSE_PAPIER.dpp_ht_initial%TYPE;
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpp_ttc_initial      DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
      my_dpp_sf_pers_id       DEPENSE_PAPIER.dpp_sf_pers_id%type;
      my_dpp_sf_date          DEPENSE_PAPIER.dpp_sf_date%type;
      my_nb_decimales         NUMBER;
      my_nb                   integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);
        Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mod_ordre, a_exe_ordre);

        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dpp_ht_initial:=round(a_dpp_ht_initial, my_nb_decimales);
        my_dpp_ttc_initial:=round(a_dpp_ttc_initial, my_nb_decimales);

        -- si les montants sont negatifs ou si il y a un dpp_id_reversement (c'est un ORV) -> package reverser.
        IF my_dpp_ht_initial<0 OR my_dpp_ttc_initial<0 OR a_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(my_dpp_ht_initial)>ABS(my_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(my_dpp_ht_initial, my_dpp_ttc_initial);

           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        my_dpp_sf_pers_id:=a_dpp_sf_pers_id;
        my_dpp_sf_date:=a_dpp_sf_date;
        if a_dpp_date_service_fait is not null then
           if a_dpp_sf_pers_id is null then
              select pers_id into my_dpp_sf_pers_id from v_utilisateur where utl_ordre=a_utl_ordre;
           end if;
           
           if a_dpp_sf_date is null then
              select sysdate into my_dpp_sf_date from dual;
           end if;  
        end if;
        
        select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
        
        if my_nb=0 then
           INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
              0, 0, a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
              a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
              my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial, a_dpp_im_taux, a_dpp_im_dgp, a_imtt_id,
              my_dpp_sf_pers_id, my_dpp_sf_date,a_ecd_ordre);
        else
           update DEPENSE_PAPIER set dpp_numero_facture=a_dpp_numero_facture, rib_ordre=a_rib_ordre, mod_ordre=a_mod_ordre, dpp_date_facture=a_dpp_date_facture, 
              dpp_date_reception=a_dpp_date_reception, dpp_date_service_fait=a_dpp_date_service_fait, dpp_nb_piece=a_dpp_nb_piece, utl_ordre=a_utl_ordre, 
              dpp_id_reversement=a_dpp_id_reversement, dpp_ht_initial=my_dpp_ht_initial, dpp_tva_initial=my_dpp_tva_initial, dpp_ttc_initial=my_dpp_ttc_initial, 
              dpp_im_taux=a_dpp_im_taux, dpp_im_dgp=a_dpp_im_dgp, imtt_id=a_imtt_id, dpp_sf_pers_id=my_dpp_sf_pers_id, dpp_sf_date=my_dpp_sf_date, dpp_date_saisie=sysdate, ecd_ordre=a_ecd_ordre
             where dpp_id=a_dpp_id;
        end if;
   END;

   PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT         COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE
   ) IS
     my_nb                    INTEGER;
     my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
     my_cde_exe_ordre         COMMANDE.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture est deja utilise pour une commande ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT exe_ordre INTO my_cde_exe_ordre FROM COMMANDE WHERE comm_id=a_comm_id;

        IF my_dpp_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.dep_papier(a_dpp_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        -- si pas de probleme on insere.
        IF a_cdp_id IS NULL THEN
           SELECT commande_dep_papier_seq.NEXTVAL INTO a_cdp_id FROM dual;
        END IF;

        INSERT INTO COMMANDE_DEP_PAPIER VALUES (a_cdp_id, a_comm_id, a_dpp_id);
   END;

   PROCEDURE ins_depense (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_org_id               engage_budget.org_id%type;
     my_nb_decimales         NUMBER;
     my_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE;
     my_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
   BEGIN
       IF a_dep_ttc_saisie<>0 THEN
       
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);

        select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

        -- lancement des differentes procedures d'insertion des tables de depense.
        ins_depense_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie,
            a_tap_id, a_utl_ordre, a_dep_id_reversement);

        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie+my_dep_ttc_saisie-my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie+my_dep_ttc_saisie
           WHERE dpp_id=a_dpp_id;

           -- on le passe en premier car utilis? par les autres pour les upd_engage_reste_.
        ins_depense_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

        ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
        ins_depense_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
        ins_depense_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
        ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
        ins_depense_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);

        --Corriger.upd_engage_reste(a_eng_id);

        -- on verifie la coherence des montants entre les differents depense_.
        Verifier.verifier_depense_coherence(a_dep_id);
        Verifier.verifier_depense_pap_coherence(a_dpp_id);
        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE ins_depense_extourne (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id IN OUT        DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
	  a_fou_ordre			 ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				 ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			 ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			 ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_tyap_id				 ENGAGE_BUDGET.tyap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      --a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      --a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_eng_numero           engage_budget.eng_numero%type;
     my_org_id               engage_budget.org_id%type;
     my_nb_decimales         number;
     my_dep_ht_saisie        depense_budget.dep_ht_saisie%type;
     my_dep_ttc_saisie       depense_budget.dep_ttc_saisie%type;
     my_eng_ht_saisie        engage_budget.eng_ht_saisie%type;
     my_eng_ttc_saisie       engage_budget.eng_ttc_saisie%type;
     my_dep_id_initiale      depense_budget.dep_id%type;
     my_eld_id               extourne_liq_def.eld_id%type;
     my_el_id                extourne_liq.el_id%type;
   BEGIN
      my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
      my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
      my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);

     -- si pas d''engagement prealable c'est qu'on consomme sur poche, donc on en cree un
     if a_eng_id is null then
        if a_fou_ordre is null or a_org_id is null or a_tcd_ordre is null or a_tap_id is null then
             raise_application_error(-20001, 'il faut un fournisseur, une ligne budgetaire, un type de credit et un taux de prorata');
        end if;
        
        -- quid exercice du tcd_ordre et dates ouverture org_id ????
        -- autre probleme, dans la procedure de creation d'engagement si montant>0 alors engagement normal donc creer procedure engagement_extourne_poche
        
        engager.ins_engage_extourne_poche(a_eng_id, a_exe_ordre, my_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle,
	       a_dep_ht_saisie, a_dep_ttc_saisie, a_tyap_id, a_utl_ordre);
     end if;
     
     select eng_ht_saisie, eng_ttc_saisie, org_id into my_eng_ht_saisie, my_eng_ttc_saisie, my_org_id from engage_budget where eng_id=a_eng_id;
     if my_eng_ht_saisie<0 or my_eng_ttc_saisie<0 then
        if abs(my_eng_ht_saisie)<a_dep_ht_saisie then
             raise_application_error(-20001, 'Le montant HT de la depense ('|| to_char(a_dep_ht_saisie) ||') depasse celui du credit d''extourne associe ('|| to_char(abs(my_eng_ht_saisie)) ||')');     
        end if; 
        if abs(my_eng_ttc_saisie)<a_dep_ttc_saisie then
             raise_application_error(-20001, 'Le montant TTC de la depense ('|| to_char(a_dep_ttc_saisie) ||') depasse celui du credit d''extourne associe ('|| to_char(abs(my_eng_ttc_saisie)) ||')');     
        end if; 
     end if;
     
     -- verifier qu'on a le droit de liquider sur cet exercice.
     -- Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

-- c'est une liquidation sur credit extourne fleche
     if my_eng_ttc_saisie<0 then
     -- on memorise la reference a la liquidation d''extourne avant d'inserer la nouvelle depensebudget
        select max(dep_id) into my_dep_id_initiale from depense_budget where eng_id=a_eng_id and dep_ttc_saisie<0;
        select max(el_id) into my_el_id from extourne_liq where dep_id_n1=my_dep_id_initiale;
     end if;

     -- lancement des differentes procedures d'insertion des tables de depense.
     ins_depense_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre, null);

     UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+my_dep_ht_saisie,
            dpp_tva_saisie=dpp_tva_saisie+my_dep_ttc_saisie-my_dep_ht_saisie,
            dpp_ttc_saisie=dpp_ttc_saisie+my_dep_ttc_saisie
        WHERE dpp_id=a_dpp_id;

     -- on le passe en premier car utilise par les autres pour les upd_engage_reste_.
     ins_depense_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

     --ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
     ins_depense_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
     ins_depense_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
     --ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
     ins_depense_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);

     ------ on met a jour les tables d'extourne
     select extourne_liq_def_seq.nextval into my_eld_id from dual;
     insert into extourne_liq_def select my_eld_id, a_dep_id, get_type_etat('sur extourne') from dual;
     
     -- c'est une liquidation sur credit extourne fleche
     if my_eng_ttc_saisie<0 then
--        select max(dep_id) into my_dep_id_initiale from depense_budget where eng_id=a_eng_id;
--        select max(el_id) into my_el_id from extourne_liq where dep_id_n1=my_dep_id_initiale;
        insert into extourne_liq_repart select extourne_liq_repart_seq.nextval, my_el_id, my_eld_id from dual;
     end if;
     
     -- on verifie la coherence des montants entre les differents depense_.
     Verifier.verifier_depense_coherence(a_dep_id);
     Verifier.verifier_depense_pap_coherence(a_dpp_id);
     -- on verifie si la coherence des montants budgetaires restant est  conservee.
     -- verif inutile pour extourne
--     Verifier.verifier_engage_coherence(a_eng_id);
   END;

PROCEDURE ins_depense_directe (
      a_dep_id IN OUT       DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id              DEPENSE_BUDGET.dpp_id%TYPE,
      a_dep_ht_saisie       DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie      DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tap_id              DEPENSE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
      a_utl_ordre           DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement  DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action       VARCHAR2,
      a_chaine_analytique   VARCHAR2,
      a_chaine_convention   VARCHAR2,
      a_chaine_hors_marche  VARCHAR2,
      a_chaine_marche       VARCHAR2,
      a_chaine_planco       VARCHAR2
   ) IS
     my_eng_id              engage_budget.eng_id%type;
     my_eng_numero          engage_budget.eng_numero%type;
     my_chaine_planco       VARCHAR2(30000);
     my_chaine              VARCHAR2(30000);

     my_pco_num             DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_dpco_ht_saisie      DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
     my_dpco_ttc_saisie     DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;     
  begin
    my_eng_id:=null; 
    my_eng_numero:=null; 
    
    -- on retravaille la chaine_planco, car c'est la seule qui differencie pour les parametres d'ins_engage
    my_chaine_planco:='';
    my_chaine:=a_chaine_planco;
    
    LOOP
        IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

        -- on recupere l'imputation.
        SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
        my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
        -- on recupere le ht.
        SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
        my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
        -- on recupere le ttc.
        SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
        my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
        -- on enleve l'ecriture.
        my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
        -- on enleve la chaine des inventaires.
        my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

        my_chaine_planco:=my_chaine_planco||my_pco_num||'$'||my_dpco_ht_saisie||'$'||my_dpco_ttc_saisie||'$';
    END LOOP;
    my_chaine_planco:=my_chaine_planco||'$';
    
    -- on cree l'engagement
    engager.ins_engage(my_eng_id, a_exe_ordre, my_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, a_dep_ht_saisie, a_dep_ttc_saisie,
	  a_tyap_id, a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_hors_marche, a_chaine_marche, my_chaine_planco);
      
    if a_dep_ttc_saisie<0 then
       reverser.ins_reverse(a_dep_id, a_exe_ordre, a_dpp_id, my_eng_id, a_dep_ht_saisie, a_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement,
         a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_hors_marche, a_chaine_marche, a_chaine_planco);
    else
      -- on cree la depense
      ins_depense(a_dep_id, a_exe_ordre, a_dpp_id, my_eng_id, a_dep_ht_saisie, a_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement,
        a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_hors_marche, a_chaine_marche, a_chaine_planco);
    end if;
  end;    

   PROCEDURE del_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
      my_reim_id                INTEGER;
      my_eng_id                 ENGAGE_BUDGET.eng_id%TYPE;
      my_zdep_id                Z_DEPENSE_BUDGET.zdep_id%TYPE;
      my_exe_ordre              DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire     DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dep_total_ht           DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_total_ttc          DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dep_total_bud          DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_eng_montant_bud        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_montant_bud_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_dispo_ligne            v_budget_exec_credit.bdxc_disponible%TYPE;
      my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_dep_ht_saisie          DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_eng_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
      my_dep_tap_id             depense_budget.tap_id%type;
      my_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE;
      my_comm_id                COMMANDE.comm_id%TYPE;
      my_dpco_id                DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
      my_eng_ht_saisie          engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie         engage_budget.eng_ttc_saisie%type;
      my_nb_extourne            integer;
      my_taux_tva                number(5,3);
      my_tap_taux                jefy_admin.taux_prorata.tap_taux%type;
      my_new_ht                  engage_budget.eng_ht_saisie%type;
      my_new_ttc                 engage_budget.eng_ttc_saisie%type;
      
      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La liquidation n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

        SELECT e.org_id, e.tcd_ordre, d.exe_ordre, d.dep_ht_saisie, d.dep_ttc_saisie, e.eng_ht_saisie, e.eng_ttc_saisie,
               d.dep_montant_budgetaire, e.eng_id, e.tap_id, e.eng_montant_budgetaire, e.eng_montant_budgetaire_reste, d.dpp_id, d.tap_id
          INTO my_org_id, my_tcd_ordre, my_exe_ordre, my_dep_ht_saisie, my_dep_ttc_saisie, my_eng_ht_saisie, my_eng_ttc_saisie,
               my_montant_budgetaire, my_eng_id, my_eng_tap_id, my_eng_montant_bud, my_eng_montant_bud_reste, my_dpp_id, my_dep_tap_id
        FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e
        WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        select count(*) into my_nb_extourne from extourne_liq where dep_id_n1 in (select dep_id from depense_budget where eng_id=my_eng_id);
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si ce n'est pas un ORV.
        IF my_montant_budgetaire<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- on recupere les montants des autres factures et ORV.
        SELECT NVL(SUM(Budget.calculer_budgetaire(my_exe_ordre,my_eng_tap_id,my_org_id,dep_ht_saisie, dep_ttc_saisie)),0) 
          INTO my_dep_total_bud
          FROM DEPENSE_BUDGET WHERE dep_id<>a_dep_id AND dep_id IN
             (SELECT dep_id FROM DEPENSE_CTRL_PLANCO
               WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE eng_id=my_eng_id)
               AND (dpco_montant_budgetaire>0 /*OR man_id IN
               (SELECT man_id FROM v_mandat WHERE man_etat IN ('VISE','PAYE'))*/));

        if my_eng_ttc_saisie<0 or my_nb_extourne>0 then

          SELECT NVL(SUM(Budget.calculer_budgetaire(my_exe_ordre,my_eng_tap_id,my_org_id,dep_ht_saisie, dep_ttc_saisie)),0) 
          INTO my_dep_total_bud
          FROM DEPENSE_BUDGET WHERE dep_id<>a_dep_id and eng_id=my_eng_id;

           if my_eng_tap_id=my_dep_tap_id and my_eng_ttc_saisie=0 and my_eng_ht_saisie=0 then
              update engage_budget set eng_ht_saisie=-my_dep_ht_saisie, eng_tva_saisie=-my_dep_ttc_saisie+my_dep_ht_saisie, 
                 eng_ttc_saisie=-my_dep_ttc_saisie where eng_id=my_eng_id;
           else
              --- VERIFIER SI IL N'Y A PAS UN PB DE CENTIME (arrondi, prorata ...)
              select tap_taux into my_tap_taux from jefy_admin.taux_prorata where tap_id=my_eng_tap_id; 
              if my_eng_ttc_saisie=0 and my_eng_ht_saisie=0 then
                 my_taux_tva:=my_dep_ttc_saisie/my_dep_ht_saisie-1;
              else
                 my_taux_tva:=my_eng_ttc_saisie/my_eng_ht_saisie-1;
              end if;
              
              my_new_ht:=round((my_dep_total_bud)/(1+(my_taux_tva*(1-(my_tap_taux/100)))), liquider_outils.get_nb_decimales(my_exe_ordre));
                  --setScale(2, BigDecimal.ROUND_HALF_DOWN);
              my_new_ttc:=round(my_new_ht*(1+my_taux_tva),liquider_outils.get_nb_decimales(my_exe_ordre)); --.setScale(2, BigDecimal.ROUND_HALF_DOWN);

--                dbms_output.put_line(' my_new_ht='|| my_new_ht);
--                 dbms_output.put_line(' my_dep_total_bud='|| my_dep_total_bud);
--                  dbms_output.put_line('  my_new_ttc='||  my_new_ttc);
                  


              update engage_budget set eng_ht_saisie=my_new_ht, eng_ttc_saisie=my_new_ttc, eng_tva_saisie=my_new_ttc-my_new_ht
              where eng_id=my_eng_id;
           end if;


        else
           -- on calcule le reste de l'engagement
           my_reste:=my_eng_montant_bud-my_dep_total_bud;
           IF my_reste<0 THEN
              my_reste:=0;
           END IF;
           
           -- on compare au dispo de la ligne budgetaire ... pour voir si on peut tt reengager.
           my_dispo_ligne:=my_montant_budgetaire+
              Budget.engager_disponible(my_exe_ordre, my_org_id, my_tcd_ordre);

           IF my_reste>my_dispo_ligne+my_eng_montant_bud_reste  THEN
             my_reste:=my_dispo_ligne+my_eng_montant_bud_reste;
           END IF;

           -- on modifie l'engagement.
           UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=my_reste WHERE eng_id=my_eng_id;
        end if;



        -- tout est bon ... on supprime la depense.
        log_depense_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
        LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
        
           jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
           DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        -- Suppression des reimputations associees (Dont celles effectuees dans Kiwi)
        select count(*) into my_nb from reimputation where dep_id = a_dep_id;
        if my_nb>0 then
            jefy_mission.kiwi_paiement.del_reimputation(a_dep_id);

            DELETE FROM reimputation_ACTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_ANALYTIQUE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_BUDGET WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_CONVENTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_HORS_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_PLANCO WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            
            DELETE FROM reimputation WHERE dep_id=a_dep_id;
        end if;

        delete from extourne_liq_repart where eld_id in (select eld_id from extourne_liq_def where dep_id=a_dep_id);
        delete from extourne_liq_def where dep_id=a_dep_id;
        
        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        if my_eng_ttc_saisie>=0 then
           Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
           Corriger.upd_engage_reste(my_eng_id);
        end if;

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(my_eng_id);
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

        Apres_Liquide.del_depense_budget(my_eng_id, a_dep_id);
   END;


   PROCEDURE del_depense_papier (
      a_dpp_id             DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre          Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE) IS
      my_nb                INTEGER;
      my_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
      my_dpp_ttc_initial   DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_depense_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;



--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------



   PROCEDURE log_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id            Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
       INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_depense_budget (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id             depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_dep_ht_saisie      DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_nb_decimales       NUMBER;
       
       my_nb                 INTEGER;
       my_par_value          PARAMETRE.par_value%TYPE;

       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_tcd_ordre          ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_eng_budgetaire     ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_eng_reste          ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
       my_eng_init           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_eng_ht             ENGAGE_BUDGET.eng_ht_saisie%TYPE;
       my_eng_ttc            ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_dep_eng_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

       my_dpp_ht_initial     DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial    DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial    DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       --my_sum_ht_saisie    depense_budget.dep_ht_saisie%type;
       --my_sum_tva_saisie   depense_budget.dep_tva_saisie%type;
       --my_sum_ttc_saisie   depense_budget.dep_ttc_saisie%type;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc        DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init        DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init       DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre      DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
       
       my_eng_negatif_budgetaire  depense_budget.dep_montant_budgetaire%type;
       my_dep_extourne_budgetaire depense_budget.dep_montant_budgetaire%type;
       my_taux_tva                number(5,3);
       my_tap_taux                jefy_admin.taux_prorata.tap_taux%type;
       my_new_ht                  engage_budget.eng_ht_saisie%type;
       my_new_ttc                 engage_budget.eng_ttc_saisie%type;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT exe_ordre, org_id, tap_id, tcd_ordre, eng_montant_budgetaire_reste, eng_montant_budgetaire, eng_ht_saisie, eng_ttc_saisie
          INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre, my_eng_reste, my_eng_init, my_eng_ht, my_eng_ttc
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

      
        --extourne
        if (my_eng_ht<0 or my_eng_ttc<0) and (my_dep_ht_saisie>=0 and my_dep_ttc_saisie>=0) then
        
           -- on compare les montants budgetaires
           my_eng_negatif_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, my_eng_ht, my_eng_ttc);
           my_dep_extourne_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre, a_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

           if abs(my_eng_negatif_budgetaire)<my_dep_extourne_budgetaire then
               RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation sur mandat d''extourne on ne peut pas depasser le montant des credits extournes');
           end if;
        else
          IF my_dep_ht_saisie<0 OR my_dep_ttc_saisie<0 OR a_dep_id_reversement IS NOT NULL or my_eng_ttc<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
          END IF;
        end if;
        
        

        
        
        SELECT exe_ordre, dpp_ht_initial, dpp_tva_initial, dpp_ttc_initial
          INTO my_dpp_exe_ordre, my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

       --    RAISE_APPLICATION_ERROR(-20001,'On ne peut pas liquider sur un engagement d''extourne');
      


        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        -- verification de la coherence du prorata.
        IF Get_Parametre(a_exe_ordre, 'DEPENSE_IDEM_TAP_ID')<>'NON' AND
           my_tap_id<>a_tap_id THEN
             RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la depense soit le meme que l''engagement initial.');
        END IF;

        -- on verifie la coherence des montants.
         IF ABS(my_dep_ht_saisie)>ABS(my_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(my_dep_ht_saisie, my_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declar? dans la papier.
        /*select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_tva_saisie),0) , nvl(sum(dep_ttc_saisie),0)
               into my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          from depense_budget where dpp_id=a_dpp_id;

        if abs(my_sum_ht_saisie+a_dep_ht_saisie) > abs(my_dpp_ht_initial) or
           abs(my_sum_tva_saisie+my_dep_tva_saisie) > abs(my_dpp_tva_initial) or
           abs(my_sum_ttc_saisie+a_dep_ttc_saisie) > abs(my_dpp_ttc_initial) then
           raise_application_error(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        end if;
        */
        
        select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
        
        if my_eng_init=0 and my_nb=0 then
           my_montant_budgetaire:=0;
           my_eng_budgetaire:=0;
        else
           Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);
                
           -- calcul du montant budgetaire.
           my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id, my_dep_ht_saisie,my_dep_ttc_saisie);

           -- on calcule pour diminuer le reste de l'engagement du montant liquid? ou celui par rapport au tap_id.
           --    de l'engage (si le tap_id peut etre different de celui de l'engage).
           my_eng_budgetaire:=Liquider_Outils.get_eng_montant_budgetaire(a_exe_ordre, my_tap_id, a_tap_id,
               my_montant_budgetaire, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

           -- si le reste engage est 0, et qu'on passe une liquidation positive -> erreur.

           --> ON ENLEVE TEMPORAIREMENT POUR L'EXTOURNE, A CONTOURNER PLUS TARD
              --IF my_eng_reste<=0 AND my_eng_budgetaire>0 THEN
                 --   RAISE_APPLICATION_ERROR(-20001,'L''engagement est deja solde ('||indication_erreur.engagement(a_eng_id)||')');
              --END IF;
           --<

           -- on verifie qu'on ne diminue pas plus que ce qu'il y a d'engage.
           if my_eng_reste<my_eng_budgetaire then
              my_eng_budgetaire:=my_eng_reste;
           end if;
        end if;
        
        -- insertion dans la table.
        if a_dep_id is null then
           select depense_budget_seq.nextval into a_dep_id from dual;
        end if;

        -- on diminue le reste engage de l'engagement.
        update engage_budget set eng_montant_budgetaire_reste=eng_montant_budgetaire_reste-my_eng_budgetaire
           where eng_id=a_eng_id;

        if (my_eng_ht<0 or my_eng_ttc<0) and (my_dep_ht_saisie>=0 and my_dep_ttc_saisie>=0) then
        
           if abs(my_eng_negatif_budgetaire) < my_dep_extourne_budgetaire then
              update engage_budget set eng_ht_saisie=0, eng_tva_saisie=0, eng_ttc_saisie=0 where eng_id=a_eng_id;
           else
              --- VERIFIER SI IL N'Y A PAS UN PB DE CENTIME (arrondi, prorata ...)
              select tap_taux into my_tap_taux from jefy_admin.taux_prorata where tap_id=my_tap_id; 
              my_taux_tva:=my_eng_ttc/my_eng_ht-1;
              my_new_ht:=round((my_eng_negatif_budgetaire+my_dep_extourne_budgetaire)/(1+(my_taux_tva*(1-(my_tap_taux/100)))), 
                   liquider_outils.get_nb_decimales(a_exe_ordre));
                  --setScale(2, BigDecimal.ROUND_HALF_DOWN);
              my_new_ttc:=round(my_new_ht*(1+my_taux_tva),liquider_outils.get_nb_decimales(a_exe_ordre)); --.setScale(2, BigDecimal.ROUND_HALF_DOWN);

              update engage_budget set eng_ht_saisie=my_new_ht, eng_ttc_saisie=my_new_ttc, eng_tva_saisie=my_new_ttc-my_new_ht
              where eng_id=a_eng_id;
           end if;

        end if;

        -- on liquide.
        insert into depense_budget values (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

        if my_montant_budgetaire<>0 then 
           budget.maj_budget(a_exe_ordre, my_org_id, my_tcd_ordre);
        end if;

        -- procedure de verification
        apres_liquide.budget(a_dep_id);
   END;

   PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dact_id                  DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                  DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_dact_ht_saisie           DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie          DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie          DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                DEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dact_ht_saisie:=round(my_dact_ht_saisie, my_nb_decimales);
            my_dact_ttc_saisie:=round(my_dact_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
            IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            IF my_dact_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dact_montant_budgetaire THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id,
                   a_exe_ordre, a_dep_id, my_tyac_id, my_dact_montant_budgetaire,
                   my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_action(my_eng_id);

            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dana_id                   DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                        DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_dana_ht_saisie             DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
       my_eng_init                 engage_budget.eng_montant_budgetaire%type;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id, e.eng_montant_budgetaire
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id,my_eng_init
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dana_ht_saisie:=round(my_dana_ht_saisie, my_nb_decimales);
            my_dana_ttc_saisie:=round(my_dana_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            select count(*) into my_nb from engage_ctrl_action where eng_id=my_eng_id;
        
            /*if my_eng_init=0 and my_nb=0 then
               my_dana_montant_budgetaire:=0;
            else*/
               -- on calcule le montant budgetaire.
                my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);
            --end if;
            
            IF my_dana_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            if my_nb>0 then
               -- on teste si il n'y a pas assez de dispo.  
               IF my_dep_montant_budgetaire<=my_somme+my_dana_montant_budgetaire THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
               END IF;
            end if;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_analytique(my_eng_id);

            Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                   DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre                    DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_dcon_ht_saisie             DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie           DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie           DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
       my_eng_init                 engage_budget.eng_montant_budgetaire%type;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id, e.eng_montant_budgetaire
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id,my_eng_init
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dcon_ht_saisie:=round(my_dcon_ht_saisie, my_nb_decimales);
            my_dcon_ttc_saisie:=round(my_dcon_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            select count(*) into my_nb from engage_ctrl_action where eng_id=my_eng_id;
        
            /*if my_eng_init=0 and my_nb=0 then
               my_dcon_montant_budgetaire:=0;
            else*/
               -- on calcule le montant budgetaire.
                 my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);
            --end if;
            
            IF my_dcon_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
            if my_nb>0 then
               IF my_dep_montant_budgetaire<=my_somme+my_dcon_montant_budgetaire THEN
                 my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
               END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_convention(my_eng_id);

            Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dhom_id                   DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                        DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_dhom_ht_saisie             DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_?chat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dhom_ht_saisie:=round(my_dhom_ht_saisie, my_nb_decimales);
            my_dhom_ttc_saisie:=round(my_dhom_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            IF my_dhom_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dhom_montant_budgetaire THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_hors_marche(my_eng_id);

            Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
       my_eng_init                 engage_budget.eng_montant_budgetaire%type;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, d.eng_id, e.eng_montant_budgetaire
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre,my_eng_id,my_eng_init
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dmar_ht_saisie:=round(my_dmar_ht_saisie, my_nb_decimales);
            my_dmar_ttc_saisie:=round(my_dmar_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            select count(*) into my_nb from engage_ctrl_action where eng_id=my_eng_id;
        
            if my_eng_init=0 and my_nb=0 then
               my_dmar_montant_budgetaire:=0;
            else
               -- on calcule le montant budgetaire.
               my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);
            end if;
            
            IF my_dmar_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dmar_montant_budgetaire THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

                         dbms_output.put_line('insert depense : '||my_dmar_id);

               -- procedure de verification
                         dbms_output.put_line('upd engage : '||my_eng_id);
            Corriger.upd_engage_reste_marche(my_eng_id);

            Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_mod_ordre                   DEPENSE_PAPIER.mod_ordre%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_inventaires    VARCHAR2(30000);
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            --SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            select ecd_ordre into my_ecd_ordre from depense_papier where dpp_id in (select dpp_id from depense_budget where dep_id=a_dep_id);
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dpco_ht_saisie:=round(my_dpco_ht_saisie, my_nb_decimales);
            my_dpco_ttc_saisie:=round(my_dpco_ttc_saisie, my_nb_decimales);

            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            IF my_ecd_ordre IS NOT NULL THEN
               SELECT mod_ordre INTO my_mod_ordre
                  FROM DEPENSE_BUDGET d, DEPENSE_PAPIER p WHERE p.dpp_id=d.dpp_id AND d.dep_id=a_dep_id;
               Verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
            END IF;

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            IF my_dpco_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dpco_montant_budgetaire THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, my_ecd_ordre);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Corriger.upd_engage_reste_planco(my_eng_id);

            Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dpco_montant_budgetaire;
        END LOOP;
   END;
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Engager
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------


   PROCEDURE ins_engage (
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN
		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
        if a_eng_ttc_saisie>=0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
		   Verifier.verifier_engage_exercice(a_exe_ordre, a_utl_ordre, a_org_id);
        --else
		--   Verifier.verifier_extourne_exercice(a_exe_ordre-1, a_utl_ordre, a_org_id);      
        end if;
        
		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_engage_budget(a_eng_id,a_exe_ordre,a_eng_numero,a_fou_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_eng_libelle, a_eng_ht_saisie,a_eng_ttc_saisie,a_tyap_id,a_utl_ordre);

        if a_eng_ttc_saisie>=0 then 
		   ins_engage_ctrl_action(a_exe_ordre,a_eng_id,a_chaine_action);
		   ins_engage_ctrl_analytique(a_exe_ordre,a_eng_id,a_chaine_analytique);
		   ins_engage_ctrl_convention(a_exe_ordre,a_eng_id,a_chaine_convention);
		   ins_engage_ctrl_hors_marche(a_exe_ordre,a_eng_id,a_chaine_hors_marche);
		   ins_engage_ctrl_marche(a_exe_ordre,a_eng_id,a_chaine_marche);
		   ins_engage_ctrl_planco(a_exe_ordre,a_eng_id,a_chaine_planco);
        end if;
        
		Verifier.verifier_engage_coherence(a_eng_id);
		Apres_Engage.engage(a_eng_id);
   END;

PROCEDURE ins_engage_extourne_poche (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type
   ) is
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   begin
		IF a_eng_ttc_saisie<0 or a_eng_ht_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : pour une consommation sur poche d''extourne les montants doivent etre positifs');
		END IF;

        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

--        verifier.verifier_organ(a_org_id, a_tcd_ordre);
	    verifier.verifier_fournisseur(a_fou_ordre);

		IF abs(my_eng_ttc_saisie)-abs(my_eng_ht_saisie)<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		-- calcul du montant budgetaire.
        my_montant_budgetaire:=0;

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);
   end;
   
   PROCEDURE del_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_montant_reste      ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est deja annule (eng_id:'||a_eng_id||')');
	    END IF;

   		SELECT exe_ordre, eng_montant_budgetaire, eng_montant_budgetaire_reste, org_id, tcd_ordre
		  INTO my_exe_ordre, my_montant_budgetaire, my_montant_reste, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

   		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on traite le cas d'un engagement dont la somme des liquidations.
		-- est egale a la somme des ordres de reversement ... ce qui fait que l'engagement est reengage.
		--  donc on ne le supprime pas mais on le solde.
		SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb>0 AND my_montant_budgetaire=my_montant_reste THEN
		   solder_engage(a_eng_id, a_utl_ordre);
		ELSE
           Verifier.verifier_util_engage(a_eng_id);

		   log_engage(a_eng_id,a_utl_ordre);

	       DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        END IF;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.del_engage(a_eng_id);
   END;

   PROCEDURE solder_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
      my_eng_ht_saisie      engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie     engage_budget.eng_ttc_saisie%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste, eng_ht_saisie, eng_ttc_saisie 
          INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste, my_eng_ht_saisie, my_eng_ttc_saisie
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
              
        if my_eng_ttc_saisie>=0 and my_eng_montant_bud_reste=0 then return; end if;

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		solder_engage_sansdroit(a_eng_id, a_utl_ordre);
   END;

   PROCEDURE solder_engage_sansdroit (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
      my_eng_ht_saisie      engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie     engage_budget.eng_ttc_saisie%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste, eng_ht_saisie, eng_ttc_saisie 
          INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste, my_eng_ht_saisie, my_eng_ttc_saisie
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if my_eng_ttc_saisie>=0 and my_eng_montant_bud_reste=0 then return; end if;
        
		-- on solde l'engagement et ses controles.
        if my_eng_ttc_saisie>=0 then
		   UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        else
		   UPDATE ENGAGE_BUDGET SET eng_ht_saisie=0, eng_tva_saisie=0, eng_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ACTION SET eact_ht_saisie=0, eact_tva_saisie=0, eact_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_ht_saisie=0, eana_tva_saisie=0, eana_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_CONVENTION SET econ_ht_saisie=0, econ_tva_saisie=0, econ_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_ht_saisie=0, ehom_tva_saisie=0, ehom_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_MARCHE SET emar_ht_saisie=0, emar_tva_saisie=0, emar_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_PLANCO SET epco_ht_saisie=0, epco_tva_saisie=0, epco_ttc_saisie=0 WHERE eng_id=a_eng_id;
        end if;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE upd_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
	  my_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	  my_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_tap_id			    ENGAGE_BUDGET.tap_id%TYPE;
      my_tyap_id            ENGAGE_BUDGET.tyap_id%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_tva_saisie     ENGAGE_BUDGET.eng_tva_saisie%TYPE;
      my_budgetaire         ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_nb_decimales       NUMBER;
      
      CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id and dep_montant_budgetaire>0;
      depense DEPENSE_BUDGET%ROWTYPE;
	  my_dep_ht_saisie		DEPENSE_BUDGET.dep_ht_saisie%TYPE;
	  my_dep_ttc_saisie		DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      
   BEGIN
   		SELECT exe_ordre, tap_id, org_id, tcd_ordre, tyap_id INTO my_exe_ordre, my_tap_id, my_org_id, my_tcd_ordre, my_tyap_id
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_ttc_saisie),0) into my_dep_ht_saisie, my_dep_ttc_saisie
            from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
        my_budgetaire:=0;
        if my_dep_ht_saisie<>0 or my_dep_ttc_saisie<>0 then
           if my_dep_ht_saisie=my_dep_ttc_saisie then
               my_budgetaire:=my_dep_ttc_saisie;
           else
               OPEN listedepense(a_eng_id);
 	           LOOP
		           FETCH  listedepense INTO depense;
		           EXIT WHEN listedepense%NOTFOUND;
				   
				    my_budgetaire:=my_budgetaire+Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
                 END LOOP;
			     CLOSE listedepense;
           end if;
        end if;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);
        my_budgetaire:=round(my_budgetaire, my_nb_decimales);

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		log_engage(a_eng_id,a_utl_ordre);

	    -- on verifie la coherence des montants.
        /*if my_tyap_id=get_type_application_EXTOURNE then
            IF my_eng_ht_saisie>0 OR my_eng_ttc_saisie>0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		    END IF;
        else
            IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;
        end if;*/

		IF abs(my_eng_ttc_saisie)-abs(my_eng_ht_saisie)<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		-- calcul du montant budgetaire.
        my_montant_budgetaire:=0;
        if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
           my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id, my_eng_ht_saisie,my_eng_ttc_saisie);
        end if;

	    IF my_budgetaire> my_montant_budgetaire THEN
          RAISE_APPLICATION_ERROR(-20001, 'Le montant modifie est inferieur au montant deja liquide ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- on modifie les montants.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=my_montant_budgetaire,
		   eng_montant_budgetaire_reste=my_montant_budgetaire-my_budgetaire,
		   eng_ht_saisie=my_eng_ht_saisie, eng_tva_saisie=my_eng_tva_saisie,
		   eng_ttc_saisie=my_eng_ttc_saisie, utl_ordre=a_utl_ordre
		   WHERE eng_id=a_eng_id;

		-- on supprime les anciens.
		DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

		-- on insere les nouveaux.
		ins_engage_ctrl_action(my_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(my_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(my_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(my_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(my_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(my_exe_ordre,a_eng_id,a_chaine_planco);

		-- on met a jour les restes engages des controleurs suivant les depenses.
		Corriger.upd_engage_reste(a_eng_id);

		-- on verifie.
		Verifier.verifier_engage_coherence(a_eng_id);
        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.engage(a_eng_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


   PROCEDURE log_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_zeng_id			Z_ENGAGE_BUDGET.zeng_id%TYPE;
   BEGIN
		SELECT z_engage_budget_seq.NEXTVAL INTO my_zeng_id FROM dual;

		-- engage_budget.
		INSERT INTO Z_ENGAGE_BUDGET SELECT my_zeng_id, SYSDATE, a_utl_ordre, e.*
		  FROM ENGAGE_BUDGET e WHERE eng_id=a_eng_id;

		-- engage_ctrl_action.
		INSERT INTO Z_ENGAGE_CTRL_ACTION SELECT z_engage_ctrl_action_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ACTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_analytique.
		INSERT INTO Z_ENGAGE_CTRL_ANALYTIQUE SELECT z_engage_ctrl_analytique_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ANALYTIQUE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_convention.
		INSERT INTO Z_ENGAGE_CTRL_CONVENTION SELECT z_engage_ctrl_convention_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_CONVENTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_hors_marche.
		INSERT INTO Z_ENGAGE_CTRL_HORS_MARCHE SELECT z_engage_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_HORS_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_marche.
		INSERT INTO Z_ENGAGE_CTRL_MARCHE SELECT z_engage_ctrl_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_planco.
		INSERT INTO Z_ENGAGE_CTRL_PLANCO SELECT z_engage_ctrl_planco_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_PLANCO e WHERE eng_id=a_eng_id;
   END;

   PROCEDURE ins_engage_budget(
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

   		-- lucrativite ??.
		--		UPDATE COMMANDE SET cde_lucrativite=(select org_lucrativite from organ where org_ordre=orgordre) WHERE cde_ordre=cdeordre;

        verifier.verifier_organ(a_org_id, a_tcd_ordre);
        if my_eng_ttc_saisie>0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
		   Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
        end if;
	    Verifier.verifier_fournisseur(a_fou_ordre);

        /*if a_tyap_id=get_type_application_EXTOURNE then
		   IF my_eng_ht_saisie>0 OR my_eng_ttc_saisie>0 THEN
		      RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		   END IF;
        else
		   IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		      RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		   END IF;
        end if;*/
        
		IF abs(my_eng_ttc_saisie)-abs(my_eng_ht_saisie)<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		-- calcul du montant budgetaire.
        my_montant_budgetaire:=0;
        if my_eng_ttc_saisie>0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
		   my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id, my_eng_ht_saisie,my_eng_ttc_saisie);
        end if;

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);

        Budget.maj_budget(a_exe_ordre, a_org_id, a_tcd_ordre);
	    Apres_Engage.Budget(a_eng_id);
   END;

   PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eact_id	               ENGAGE_CTRL_ACTION.eact_id%TYPE;
       my_tyac_id	  	   		   ENGAGE_CTRL_ACTION.tyac_id%TYPE;
       my_eact_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_eact_ht_saisie	  	   ENGAGE_CTRL_ACTION.eact_ht_saisie%TYPE;
       my_eact_tva_saisie		   ENGAGE_CTRL_ACTION.eact_tva_saisie%TYPE;
       my_eact_ttc_saisie		   ENGAGE_CTRL_ACTION.eact_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
	   my_utl_ordre                ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eact_ht_saisie:=round(my_eact_ht_saisie, my_nb_decimales);
            my_eact_ttc_saisie:=round(my_eact_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_tyac_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette action pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_eact_ht_saisie>0 OR my_eact_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_eact_ht_saisie<0 OR my_eact_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_eact_ttc_saisie)-abs(my_eact_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_eact_tva_saisie:=my_eact_ttc_saisie-my_eact_ht_saisie;

            my_eact_montant_budgetaire:=0;
            if my_eact_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_eact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_eact_ht_saisie,my_eact_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR my_eng_montant_budgetaire<=my_somme+my_eact_montant_budgetaire THEN
			    my_eact_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_action_seq.NEXTVAL INTO my_eact_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ACTION VALUES (my_eact_id, a_exe_ordre, a_eng_id, my_tyac_id, my_eact_montant_budgetaire, 
                my_eact_montant_budgetaire, my_eact_ht_saisie, my_eact_tva_saisie, my_eact_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_action(a_eng_id);
            
	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
			Apres_Engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eact_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eana_id	               ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
       my_can_id	  	   		   ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_eana_montant_budgetaire  ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_eana_ht_saisie	  	   ENGAGE_CTRL_ANALYTIQUE.eana_ht_saisie%TYPE;
       my_eana_tva_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_tva_saisie%TYPE;
       my_eana_ttc_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eana_ht_saisie:=round(my_eana_ht_saisie, my_nb_decimales);
            my_eana_ttc_saisie:=round(my_eana_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_can_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code analytique pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_eana_ht_saisie>0 OR my_eana_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_eana_ht_saisie<0 OR my_eana_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/
            
			-- on calcule la tva et le montant budgetaire.
			IF abs(my_eana_ttc_saisie)-abs(my_eana_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_eana_tva_saisie := my_eana_ttc_saisie - my_eana_ht_saisie;

            my_eana_montant_budgetaire:=0;
            if my_eana_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_eana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_eana_ht_saisie,my_eana_ttc_saisie);
            end if;
            
			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_eana_montant_budgetaire THEN
			    my_eana_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_analytique_seq.NEXTVAL INTO my_eana_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ANALYTIQUE VALUES (my_eana_id, a_exe_ordre, a_eng_id, my_can_id, my_eana_montant_budgetaire, 
                   my_eana_montant_budgetaire, my_eana_ht_saisie, my_eana_tva_saisie, my_eana_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_analytique(a_eng_id);
            
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	Apres_Engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_econ_id	               ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
       my_conv_ordre 	   		   ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_econ_montant_budgetaire  ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_econ_ht_saisie	  	   ENGAGE_CTRL_CONVENTION.econ_ht_saisie%TYPE;
       my_econ_tva_saisie		   ENGAGE_CTRL_CONVENTION.econ_tva_saisie%TYPE;
       my_econ_ttc_saisie		   ENGAGE_CTRL_CONVENTION.econ_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_econ_ht_saisie:=round(my_econ_ht_saisie, my_nb_decimales);
            my_econ_ttc_saisie:=round(my_econ_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_conv_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette convention pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_econ_ht_saisie>0 OR my_econ_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_econ_ht_saisie<0 OR my_econ_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_econ_ttc_saisie)-abs(my_econ_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_econ_tva_saisie := my_econ_ttc_saisie - my_econ_ht_saisie;

            my_econ_montant_budgetaire:=0;
            if my_econ_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_econ_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_econ_ht_saisie,my_econ_ttc_saisie);
            end if;

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_econ_montant_budgetaire THEN
			    my_econ_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_convention_seq.NEXTVAL INTO my_econ_id FROM dual;

			INSERT INTO ENGAGE_CTRL_CONVENTION VALUES (my_econ_id,
			       a_exe_ordre, a_eng_id, my_conv_ordre, my_econ_montant_budgetaire, my_econ_montant_budgetaire,
				   my_econ_ht_saisie, my_econ_tva_saisie, my_econ_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_convention(a_eng_id);
            
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
	    	Apres_Engage.convention(my_econ_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_econ_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ehom_id	               ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
       my_typa_id	  	   		   ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre	  	   		   ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_ehom_montant_budgetaire  ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_ehom_ht_saisie	  	   ENGAGE_CTRL_HORS_MARCHE.ehom_ht_saisie%TYPE;
       my_ehom_tva_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_tva_saisie%TYPE;
       my_ehom_ttc_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code de nomenclature.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ehom_ht_saisie:=round(my_ehom_ht_saisie, my_nb_decimales);
            my_ehom_ttc_saisie:=round(my_ehom_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
			     WHERE eng_id=a_eng_id AND ce_ordre=my_ce_ordre AND typa_id=my_typa_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code nomenclature pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_ehom_ht_saisie>0 OR my_ehom_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_ehom_ht_saisie<0 OR my_ehom_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_ehom_ttc_saisie)-abs(my_ehom_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_ehom_tva_saisie := my_ehom_ttc_saisie - my_ehom_ht_saisie;

            my_ehom_montant_budgetaire:=0;
            if my_ehom_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_ehom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ehom_ht_saisie,my_ehom_ttc_saisie);
            end if;
            
			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_ehom_montant_budgetaire THEN
			    my_ehom_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_hors_marche_seq.NEXTVAL INTO my_ehom_id FROM dual;

			INSERT INTO ENGAGE_CTRL_HORS_MARCHE VALUES (my_ehom_id,
			       a_exe_ordre, a_eng_id, my_typa_id, my_ce_ordre, my_ehom_montant_budgetaire, my_ehom_montant_budgetaire,
				   my_ehom_ht_saisie, my_ehom_ht_saisie, my_ehom_tva_saisie, my_ehom_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_hors_marche(a_eng_id);
            
			Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
	    	Apres_Engage.hors_marche(my_ehom_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ehom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_emar_id	               ENGAGE_CTRL_MARCHE.emar_id%TYPE;
       my_att_ordre	  	   		   ENGAGE_CTRL_MARCHE.att_ordre%TYPE;
       my_emar_montant_budgetaire  ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_emar_ht_saisie	  	   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
       my_emar_tva_saisie		   ENGAGE_CTRL_MARCHE.emar_tva_saisie%TYPE;
       my_emar_ttc_saisie		   ENGAGE_CTRL_MARCHE.emar_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
   	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
   	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, utl_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_utl_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'attribution.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_emar_ht_saisie:=round(my_emar_ht_saisie, my_nb_decimales);
            my_emar_ttc_saisie:=round(my_emar_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_att_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette attribution pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_emar_ht_saisie>0 OR my_emar_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_emar_ht_saisie<0 OR my_emar_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_emar_ttc_saisie)-abs(my_emar_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_emar_tva_saisie := my_emar_ttc_saisie - my_emar_ht_saisie;

            my_emar_montant_budgetaire:=0;
            if my_emar_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			    my_emar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_emar_ht_saisie,my_emar_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR my_eng_montant_budgetaire<=my_somme+my_emar_montant_budgetaire THEN
			    my_emar_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour un engagement');
			END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_marche_seq.NEXTVAL INTO my_emar_id FROM dual;

			INSERT INTO ENGAGE_CTRL_MARCHE VALUES (my_emar_id,
			       a_exe_ordre, a_eng_id, my_att_ordre, my_emar_montant_budgetaire, my_emar_montant_budgetaire,
				   my_emar_ht_saisie,my_emar_ht_saisie, my_emar_tva_saisie, my_emar_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_marche(a_eng_id);
            
			Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
	    	Apres_Engage.marche(my_emar_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_emar_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_epco_id	               ENGAGE_CTRL_PLANCO.epco_id%TYPE;
       my_pco_num	  	   		   ENGAGE_CTRL_PLANCO.pco_num%TYPE;
	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_epco_montant_budgetaire  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_epco_ht_saisie	  	   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
       my_epco_tva_saisie		   ENGAGE_CTRL_PLANCO.epco_tva_saisie%TYPE;
       my_epco_ttc_saisie		   ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre				   ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_epco_ht_saisie:=round(my_epco_ht_saisie, my_nb_decimales);
            my_epco_ttc_saisie:=round(my_epco_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_pco_num;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette imputation pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_epco_ht_saisie>0 OR my_epco_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_epco_ht_saisie<0 OR my_epco_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_epco_ttc_saisie)-abs(my_epco_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_epco_tva_saisie := my_epco_ttc_saisie - my_epco_ht_saisie;
            
            my_epco_montant_budgetaire:=0;
            if my_epco_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_epco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_epco_ht_saisie,my_epco_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR my_eng_montant_budgetaire<=my_somme+my_epco_montant_budgetaire THEN
			    my_epco_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_planco_seq.NEXTVAL INTO my_epco_id FROM dual;

			INSERT INTO ENGAGE_CTRL_PLANCO VALUES (my_epco_id,
			       a_exe_ordre, a_eng_id, my_pco_num, my_epco_montant_budgetaire, my_epco_montant_budgetaire,
				   my_epco_ht_saisie, my_epco_tva_saisie, my_epco_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_planco(a_eng_id);
            
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_epco_montant_budgetaire;
		END LOOP;
   END;

END;
/


GRANT EXECUTE ON JEFY_DEPENSE.ENGAGER TO JEFY_MISSION;

GRANT EXECUTE ON JEFY_DEPENSE.ENGAGER TO JEFY_PAF;

GRANT EXECUTE ON JEFY_DEPENSE.ENGAGER TO JEFY_PAYE;

GRANT EXECUTE ON JEFY_DEPENSE.ENGAGER TO JEFY_RECETTE;

GRANT EXECUTE ON JEFY_DEPENSE.ENGAGER TO SAPHARI;


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Reverser
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE ins_reverse_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpco_ht_saisie       DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
      my_dpco_ttc_saisie      DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
      my_sum_rev_ht           DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
      my_sum_rev_ttc          DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
      my_mod_ordre            DEPENSE_PAPIER.mod_ordre%TYPE;
      my_dpp_id_reversement   DEPENSE_PAPIER.dpp_id_reversement%TYPE;
      my_nb       INTEGER;
      my_nb_verif    integer;
   BEGIN
        -- pour un ORV il faut des montants negatifs.
        IF a_dpp_ht_initial>0 OR a_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        --IF a_dpp_id_reversement IS NULL THEN
        --   RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        --END IF;

        -- on verifie la coherence des montants.
        IF ABS(a_dpp_ht_initial)>ABS(a_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(a_dpp_ht_initial, a_dpp_ttc_initial);

        my_mod_ordre:=a_mod_ordre;
        
        IF a_dpp_id_reversement IS NOT NULL THEN
             -- verifier qu'on n'est pas sur une prestation interne
             select count(*) into my_nb from engage_budget eb, depense_budget db, jefy_admin.type_application ta
                 where dpp_id=a_dpp_id_reversement and eb.eng_id=db.eng_id and eb.tyap_id=ta.tyap_id and ta.TYAP_STRID='PRESTATION INTERNE';
             if my_nb>0 then
                 RAISE_APPLICATION_ERROR(-20001, 'Impossible de créer un ordre de reversement sur une dépense rattachée à une prestation interne.');  
             end if;

             SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;
             IF my_nb<>1 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La facture initiale n''existe pas (dpp_id='||a_dpp_id_reversement||')');
             END IF;
        
             -- on verifie que la facture referencee n'est pas un ORV (pas d'ORV sur un ORV);
             SELECT dpp_id_reversement, mod_ordre INTO my_dpp_id_reversement, my_mod_ordre FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;
             IF my_dpp_id_reversement IS NOT NULL THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ordre de reversement a partir d''un autre ORV');
             END IF;

             -- verifier que les montants du total des ORV ne depassent pas ceux mandates et vises de la facture d'origine.
             SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dpco_ht_saisie, my_dpco_ttc_saisie
                 FROM DEPENSE_CTRL_PLANCO WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET
                 WHERE dpp_id=a_dpp_id_reversement) AND dpco_ttc_saisie>0 AND man_id IN
                    (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

             SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
                 FROM DEPENSE_CTRL_PLANCO WHERE dpco_ttc_saisie<0 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE
                     dpp_id IN (SELECT dpp_id FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id_reversement));

             select count(*) into my_nb_verif from depense_ctrl_planco 
                 where dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id_reversement)
                   and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
             IF my_nb_verif=0 and (my_dpco_ht_saisie<ABS(my_sum_rev_ht)+ABS(a_dpp_ht_initial) OR
                     my_dpco_ttc_saisie<ABS(my_sum_rev_ttc)+ABS(a_dpp_ttc_initial)) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui vise de la facture initiale');
             END IF;
        end if;
   
           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
           0, 0, a_fou_ordre, a_rib_ordre, my_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
           a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
           a_dpp_ht_initial, my_dpp_tva_initial, a_dpp_ttc_initial,null,null,null,null,null,null);
   END;

   PROCEDURE ins_reverse (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
      my_org_id              engage_budget.org_id%type;
      my_eng_ttc_saisie      engage_budget.eng_ttc_saisie%type;
   BEGIN
        IF a_dep_ttc_saisie<>0 THEN
             select org_id, eng_ttc_saisie into my_org_id, my_eng_ttc_saisie from engage_budget where eng_id=a_eng_id;
               
             -- verifier qu'on a le droit de liquider sur cet exercice.
             if my_eng_ttc_saisie>=0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
                 Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);
             else
                 Verifier.verifier_extourne_exercice(a_exe_ordre-1, a_utl_ordre, my_org_id);      
             end if;

             -- lancement des differentes procedures d'insertion des tables de depense.
             ins_reverse_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, a_dep_ht_saisie, a_dep_ttc_saisie,
                a_tap_id, a_utl_ordre, a_dep_id_reversement);

             UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+a_dep_ht_saisie,
                  dpp_tva_saisie=dpp_tva_saisie+a_dep_ttc_saisie-a_dep_ht_saisie,
                  dpp_ttc_saisie=dpp_ttc_saisie+a_dep_ttc_saisie
                 WHERE dpp_id=a_dpp_id;

             ins_reverse_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
             ins_reverse_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
             ins_reverse_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
             ins_reverse_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
             ins_reverse_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);
             ins_reverse_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Corriger.upd_engage_reste(a_eng_id);

             -- on verifie la coherence des montants entre les differents depense_.
--             Verifier.verifier_depense_coherence(a_dep_id);
             Verifier.verifier_depense_pap_coherence(a_dpp_id);
             -- on verifie si la coherence des montants budgetaires restant est  conservee.
             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_reverse (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                 INTEGER;
      my_exe_ordre          DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dpp_id             DEPENSE_BUDGET.dpp_id%TYPE;
      my_dep_ht_saisie      DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dpco_id            DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
      my_org_id             engage_budget.org_id%type;
      my_eng_id             engage_budget.eng_id%type;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
   BEGIN

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ORV n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

        SELECT  d.exe_ordre, d.dep_montant_budgetaire, d.dpp_id, d.dep_ht_saisie, d.dep_ttc_saisie, d.eng_id
           INTO my_exe_ordre, my_montant_budgetaire, my_dpp_id, my_dep_ht_saisie, my_dep_ttc_saisie, my_eng_id
           FROM DEPENSE_BUDGET d WHERE d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        select org_id into my_org_id from engage_budget where eng_id=my_eng_id;
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si c'est un ORV.
           IF my_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- tout est bon ... on supprime la depense.
        log_reverse_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        select count(*) into my_nb from reimputation where dep_id=a_dep_id;
        if my_nb>0 then
            DELETE FROM reimputation_ACTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_ANALYTIQUE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_BUDGET WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_CONVENTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_HORS_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_PLANCO WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            
            DELETE FROM reimputation WHERE dep_id=a_dep_id;
        end if;

        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

        Apres_Liquide.del_depense_budget(my_eng_id, a_dep_id);
   END;

      PROCEDURE del_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE)
      IS
       my_nb         INTEGER;
       my_exe_ordre     DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dpp_ttc_initial  DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_reverse_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

      PROCEDURE log_reverse_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id                    Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
           INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_reverse_budget (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_nb                 INTEGER;
       my_nb_verif           integer;

       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;
       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

       my_dpp_ht_initial     DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial    DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial    DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dpp_ttc_saisie     DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
       my_dpp_id_reversement DEPENSE_PAPIER.dpp_id_reversement%TYPE;
       my_dpp_id_origine     DEPENSE_BUDGET.dpp_id%TYPE;

       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ht_saisie      DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc        DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init        DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init       DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre      DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN

        -- pour un ORV il faut des montants negatifs.
        IF a_dep_ht_saisie>0 OR a_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        --IF a_dep_id_reversement IS NULL THEN
        --   RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        --END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        IF a_dep_id_reversement IS NOT NULL THEN
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;
            IF my_nb<>1 THEN
                RAISE_APPLICATION_ERROR(-20001,'La facture initiale n''existe pas (dep_id='||a_dep_id_reversement||')');
            END IF;
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT org_id, exe_ordre, eng_ttc_saisie INTO my_org_id, my_exe_ordre, my_eng_ttc_saisie FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        SELECT exe_ordre, dpp_id_reversement INTO my_dpp_exe_ordre, my_dpp_id_reversement
         FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ORV doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

       
        IF a_dep_id_reversement IS NOT NULL THEN
            SELECT exe_ordre, eng_id, tap_id, dep_id_reversement, dpp_id
                INTO my_dep_exe_ordre, my_eng_id, my_dep_tap_id, my_dep_id_reversement, my_dpp_id_origine
                FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;

            IF my_dpp_id_reversement<>my_dpp_id_origine THEN
               RAISE_APPLICATION_ERROR(-20001,'la facture papier d''origine est differente de celle de la depense budget d''origine');
            END IF;

            IF a_eng_id<>my_eng_id THEN
               RAISE_APPLICATION_ERROR(-20001,'L''ordre de reversement doit etre sur le meme engagement que la facture initiale');
            END IF;

            IF a_exe_ordre<>my_dep_exe_ordre THEN
                RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit etre sur le meme exercice que la facture initiale');
            END IF;

            IF a_tap_id<>my_dep_tap_id THEN
                RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit avoir le meme prorata que la facture initiale.');
            END IF;

            IF my_dep_id_reversement IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ORV a partir d''un autre ORV.');
            END IF;
        END IF;

        -- verification des coherences entre les sommes (si elles ne depassent pas).
        SELECT dpp_ttc_saisie INTO my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0) , NVL(SUM(dep_ttc_saisie),0)
               INTO my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        -- on verifie la coherence des montants.
         IF ABS(a_dep_ht_saisie)>ABS(a_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste si le ttc de la depense papier est du meme signe que celui de depense_budget.
        IF my_dpp_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Incoherence de signe entre le montant de la facture et le montant budgetaire');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(a_dep_ht_saisie, a_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declaré dans la papier.
        /*IF ABS(my_sum_ht_saisie+a_dep_ht_saisie) > ABS(my_dpp_ht_saisie) OR
           ABS(my_sum_tva_saisie+my_dep_tva_saisie) > ABS(my_dpp_tva_saisie) OR
           ABS(my_sum_ttc_saisie+a_dep_ttc_saisie) > ABS(my_dpp_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        END IF;*/

        -- calcul du montant budgetaire.
        if (my_eng_ttc_saisie<0) then
            my_montant_budgetaire:=0;
        else
            my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              a_dep_ht_saisie,a_dep_ttc_saisie);
        end if;
        if a_dep_id_reversement is not null then 
            -- on verifie que la somme des ORV ne depasse pas le montant de la facture initiale ... mandate et vise !!.
            SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dep_ht_init, my_dep_ttc_init
               FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id_reversement AND dpco_montant_budgetaire>=0 AND man_id IN
                  (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

             SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
               FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id_reversement;

            select count(*) into my_nb_verif from depense_ctrl_planco 
               where dep_id=a_dep_id_reversement
                 and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb_verif=0 and (my_dep_ht_init < ABS(my_sum_rev_ht) + ABS(a_dep_ht_saisie) OR
                my_dep_ttc_init < ABS(my_sum_rev_ttc) + ABS(a_dep_ttc_saisie)) THEN
                RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui de la facture initiale');
            END IF;
        end if;
        
        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

        -- on reverse pour liberer les credits.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           a_dep_ht_saisie, my_dep_tva_saisie, a_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_reverse_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id        DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_dact_id                  DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                  DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       sum_dact_montant_budgetaire DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       sum_dact_ht_saisie          DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       sum_dact_ttc_saisie         DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dact_ht_saisie           DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie          DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie          DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                DEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_ttc_saisie           ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, e.eng_ttc_saisie
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre, my_eng_ttc_saisie
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=0;
            if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id, my_dact_ht_saisie,my_dact_ttc_saisie);
            end if;

            -- Pour les O.R.
            IF my_dact_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un ORV.');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
            IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dact_montant_budgetaire) THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- verifier que l'action qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR pour cette action et que l'action existe bien au depart.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            if my_dep_id_reversement is not null then 
                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ACTION
                    WHERE tyac_id=my_tyac_id
                      AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

                IF my_nb=0 THEN
                    RAISE_APPLICATION_ERROR(-20001, 'Pour cet ORV et cette action n''est pas autorisee (tyac_id='||my_tyac_id||')');
                END IF;

                SELECT NVL(SUM(dact_montant_budgetaire),0), NVL(SUM(dact_ht_saisie),0), NVL(SUM(dact_ttc_saisie),0)
                    INTO sum_dact_montant_budgetaire, sum_dact_ht_saisie, sum_dact_ttc_saisie
                   FROM DEPENSE_CTRL_ACTION WHERE tyac_id=my_tyac_id
                    AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                     OR dep_id_reversement=my_dep_id_reversement);

                if my_dact_ht_saisie=-sum_dact_ht_saisie and my_dact_ttc_saisie=-sum_dact_ttc_saisie and my_dact_montant_budgetaire<>-sum_dact_montant_budgetaire then
                    my_dact_montant_budgetaire:=-sum_dact_montant_budgetaire;
                end if;

                select count(*) into my_nb from depense_ctrl_planco 
                   where dep_id=my_dep_id_reversement
                     and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
                IF my_nb=0 and (sum_dact_montant_budgetaire+my_dact_montant_budgetaire<0) THEN
                    RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette action (tyac_id='||my_tyac_id||')');
                END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id, a_exe_ordre, a_dep_id, my_tyac_id,
                my_dact_montant_budgetaire, my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

            -- procedure de verification
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id        DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_dana_id                  DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                   DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       sum_dana_montant_budgetaire DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       sum_dana_ht_saisie          DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       sum_dana_ttc_saisie         DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dana_ht_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie          DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie          DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_ttc_saisie           ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.eng_ttc_saisie
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_eng_ttc_saisie
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
            IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
            --my_dana_montant_budgetaire:=0;
            --if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id, my_dana_ht_saisie,my_dana_ttc_saisie);
            --end if;

            -- Pour les O.R.
            IF my_dana_montant_budgetaire>0 THEN
                RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            if my_dep_id_reversement is not null then 

                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ANALYTIQUE
                  WHERE can_id=my_can_id
                    AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

                IF my_nb=0 THEN
                    RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code analytique n''est pas autorise pour cette depense (can_id='||my_can_id||')');
                END IF;

                -- on teste si il n'y a pas assez de dispo.
                IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dana_montant_budgetaire) THEN
                    my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
                END IF;

                SELECT NVL(SUM(dana_montant_budgetaire),0), NVL(SUM(dana_ht_saisie),0), NVL(SUM(dana_ttc_saisie),0)
                      INTO sum_dana_montant_budgetaire, sum_dana_ht_saisie, sum_dana_ttc_saisie
                  FROM DEPENSE_CTRL_ANALYTIQUE WHERE can_id=my_can_id
                    AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                     OR dep_id_reversement=my_dep_id_reversement);
               
                if my_dana_ht_saisie=-sum_dana_ht_saisie and my_dana_ttc_saisie=-sum_dana_ttc_saisie and my_dana_montant_budgetaire<>-sum_dana_montant_budgetaire then
                    my_dana_montant_budgetaire:=-sum_dana_montant_budgetaire;
                end if;

                select count(*) into my_nb from depense_ctrl_planco 
                    where dep_id=my_dep_id_reversement and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
                IF my_nb=0 and (sum_dana_montant_budgetaire+my_dana_montant_budgetaire<0) THEN
                    RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code analytique (can_id='||my_can_id||')');
                END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

      PROCEDURE ins_reverse_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                  DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre               DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       sum_dcon_montant_budgetaire DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       sum_dcon_ht_saisie          DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       sum_dcon_ttc_saisie         DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dcon_ht_saisie           DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie          DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie          DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_ttc_saisie           ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.eng_ttc_saisie
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_eng_ttc_saisie
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere la convention.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
            --my_dcon_montant_budgetaire:=0;
            --if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id, my_dcon_ht_saisie,my_dcon_ttc_saisie);
            --end if;

            -- Pour les O.R.
            IF my_dcon_montant_budgetaire>0 THEN
                RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            if my_dep_id_reversement is not null then
                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_CONVENTION
                   WHERE conv_ordre=my_conv_ordre 
                   AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

                IF my_nb=0 THEN
                     RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette convention n''est pas autorisee pour cette depense (conv_ordre='||my_conv_ordre||')');
                END IF;

                -- on teste si il n'y a pas assez de dispo.
                IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dcon_montant_budgetaire) THEN
                    my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
                END IF;

                SELECT NVL(SUM(dcon_montant_budgetaire),0), NVL(SUM(dcon_ht_saisie),0), NVL(SUM(dcon_ttc_saisie),0)
                      INTO sum_dcon_montant_budgetaire, sum_dcon_ht_saisie, sum_dcon_ttc_saisie
                   FROM DEPENSE_CTRL_CONVENTION WHERE conv_ordre=my_conv_ordre 
                   AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                   OR dep_id_reversement=my_dep_id_reversement);

                if my_dcon_ht_saisie=-sum_dcon_ht_saisie and my_dcon_ttc_saisie=-sum_dcon_ttc_saisie and my_dcon_montant_budgetaire<>-sum_dcon_montant_budgetaire then
                    my_dcon_montant_budgetaire:=-sum_dcon_montant_budgetaire;
                end if;

                select count(*) into my_nb from depense_ctrl_planco 
                  where dep_id=my_dep_id_reversement
                    and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
                IF my_nb=0 and (sum_dcon_montant_budgetaire+my_dcon_montant_budgetaire<0) THEN
                     RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cet engagement pour cette convention (conv_ordre='||my_conv_ordre||')');
                END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id        DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_dhom_id                  DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                  DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                 DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       sum_dhom_montant_budgetaire DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       sum_dhom_ht_saisie          DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       sum_dhom_ttc_saisie         DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dhom_ht_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie          DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie          DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_ttc_saisie           ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, e.eng_ttc_saisie
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre, my_eng_ttc_saisie
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_çchat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dhom_montant_budgetaire:=0;
            if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id, my_dhom_ht_saisie,my_dhom_ttc_saisie);
            end if;

            -- Pour les O.R.
            IF my_dhom_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
            IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dhom_montant_budgetaire) THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            if my_dep_id_reversement is not null then
                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE
                   WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
                     AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

                IF my_nb=0 THEN
                      RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code nomenclature n''est pas autorise pour cette depense (ce_ordre='||my_ce_ordre||')');
                END IF;

                SELECT NVL(SUM(dhom_montant_budgetaire),0), NVL(SUM(dhom_ht_saisie),0), NVL(SUM(dhom_ttc_saisie),0)
                      INTO sum_dhom_montant_budgetaire, sum_dhom_ht_saisie, sum_dhom_ttc_saisie
                  FROM DEPENSE_CTRL_HORS_MARCHE WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
                   AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                    OR dep_id_reversement=my_dep_id_reversement);

                if my_dhom_ht_saisie=-sum_dhom_ht_saisie and my_dhom_ttc_saisie=-sum_dhom_ttc_saisie and my_dhom_montant_budgetaire<>-sum_dhom_montant_budgetaire then
                    my_dhom_montant_budgetaire:=-sum_dhom_montant_budgetaire;
                end if;
            
                select count(*) into my_nb from depense_ctrl_planco 
                  where dep_id=my_dep_id_reversement
                    and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
                IF my_nb=0 and (sum_dhom_montant_budgetaire+my_dhom_montant_budgetaire<0) THEN
                      RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code nomenclature (ce_ordre='||my_ce_ordre||')');
                END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       sum_dmar_montant_budgetaire DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_ttc_saisie           ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, e.eng_ttc_saisie
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre, my_eng_ttc_saisie
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
            --my_dmar_montant_budgetaire:=0;
            --if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id, my_dmar_ht_saisie,my_dmar_ttc_saisie);
            --end if;

            -- Pour les O.R.
            IF my_dmar_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dmar_montant_budgetaire) THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            if my_dep_id_reversement is not null then 
                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE
                   WHERE att_ordre=my_att_ordre
                     AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                      OR dep_id_reversement=my_dep_id_reversement);

                IF my_nb=0 THEN
                      RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette attribution n''est pas autorisee pour cette depense (att_ordre='||my_att_ordre||')');
                END IF;

                SELECT NVL(SUM(dmar_montant_budgetaire),0) INTO sum_dmar_montant_budgetaire
                  FROM DEPENSE_CTRL_MARCHE WHERE att_ordre=my_att_ordre
                   AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                    OR dep_id_reversement=my_dep_id_reversement);
                
                select count(*) into my_nb from depense_ctrl_planco 
                  where dep_id=my_dep_id_reversement
                    and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
                IF my_nb=0 and (sum_dmar_montant_budgetaire+my_dmar_montant_budgetaire<0) THEN
                      RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette attribution (att_ordre='||my_att_ordre||')');
                END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       sum_dpco_montant_budgetaire DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_ttc_saisie           ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_inventaires    VARCHAR2(30000);
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, e.eng_ttc_saisie
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre,my_utl_ordre, my_eng_ttc_saisie
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
            --my_dpco_montant_budgetaire:=0;
            --if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id, my_dpco_ht_saisie,my_dpco_ttc_saisie);
            --end if;

            -- Pour les O.R.
            IF my_dpco_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dpco_montant_budgetaire) THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            if my_dep_id_reversement is not null then
                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO
                   WHERE pco_num=my_pco_num
                     AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

                IF my_nb=0 THEN
                      RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette imputation il n''existe pas d''equivalent pour cette depense (pco_num='||my_pco_num||')');
                END IF;

                SELECT NVL(SUM(dpco_montant_budgetaire),0) INTO sum_dpco_montant_budgetaire
                  FROM DEPENSE_CTRL_PLANCO WHERE pco_num=my_pco_num
                   AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                    OR dep_id_reversement=my_dep_id_reversement);
                
                select count(*) into my_nb from depense_ctrl_planco 
                  where dep_id=my_dep_id_reversement
                    and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
                IF my_nb=0 and (sum_dpco_montant_budgetaire+my_dpco_montant_budgetaire<0) THEN
                      RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette imputation (pco_num='||my_pco_num||')');
                END IF;

                -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
                SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
                IF my_nb>0 THEN
                   RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
                END IF;
            end if;
            
            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, NULL);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dpco_montant_budgetaire;
        END LOOP;
   END;

END;
/



CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Verifier
IS

   PROCEDURE verifier_engage_exercice (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     ENGAGE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit          INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        verifier_organ_utilisateur(a_utl_ordre, a_org_id);

        -- verifier que l'on peux engager liquider et/ou mandater sur cet exercice.

        my_droit:=0;

        -- on verifie si l"utilisateur a le droit d'engager hors periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DEENG');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DEENG pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- on verifie si l"utilisateur a le droit d'engager pendant la periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DEENGINV');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DEENGINV pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='R';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- si droit=0 alors pas de droit trouver.
        IF my_droit=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit d''engager pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
               INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_extourne_exercice (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     ENGAGE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit       INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        my_droit:=0;

        -- on verifie si l"utilisateur a le droit d'extourne sur l'exercice
        my_fon_ordre:=Get_Fonction('DELIQEXT');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQEXT pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng in ('O','R');

        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit d''extourner pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
               INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_depense_exercice (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     DEPENSE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit          INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        verifier_organ_utilisateur(a_utl_ordre, a_org_id);
        
        -- verifier que l'on peux engager liquider et/ou mandater sur cet exercice.

        my_droit:=0;

        -- on verifie si l"utilisateur a le droit de liquider hors periode d'inventaire.
        my_fon_ordre:=Get_Fonction('DELIQ');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQ pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_liq='O';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- on verifie si l"utilisateur a le droit de liquider pendant la periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DELIQINV');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQINV pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_liq='R';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- si droit=0 alors pas de droit trouver.
        IF my_droit=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de liquider pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
              INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_organ_utilisateur (
         a_utl_ordre          engage_budget.utl_ordre%type,
      a_org_id          engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
   BEGIN
        select count(*) into my_nb from v_utilisateur_organ where utl_ordre=a_utl_ordre and org_id=a_org_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''a pas le droit sur cette ligne budgetaire ('||INDICATION_ERREUR.utilisateur(a_utl_ordre)||', '||
              INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;
   END;
   
   PROCEDURE verifier_budget (
       a_exe_ordre    ENGAGE_BUDGET.exe_ordre%TYPE,
       a_tap_id       ENGAGE_BUDGET.tap_id%TYPE,
       a_org_id       ENGAGE_BUDGET.org_id%TYPE,
       a_tcd_ordre    ENGAGE_BUDGET.tcd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_organ_prorata WHERE tap_id=a_tap_id AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce taux de prorata n''est pas autorise pour cette ligne budgetaire et cet exercice ('||
              INDICATION_ERREUR.exercice(a_exe_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.taux_prorata(a_tap_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM v_budget_exec_credit WHERE tcd_ordre=a_tcd_ordre AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il n''y a pas de credit ouvert pour cette ligne budgetaire, ce type de credit et cet exercice ('
              ||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_action (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tyac_id                 ENGAGE_CTRL_ACTION.tyac_id%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb           INTEGER;
      my_par_value    PARAMETRE.par_value%TYPE;
      my_fon_ordre      v_fonction.fon_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_type_action WHERE exe_ordre=a_exe_ordre AND tyac_id=a_tyac_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorise pour cet exercice ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
              INDICATION_ERREUR.action(a_tyac_id)||')');
        END IF;

        -- si parametre tester suivant les destinations du budget de gestion.
        my_par_value:=Get_Parametre(a_exe_ordre, 'CTRL_ORGAN_DEST');
        IF my_par_value = 'OUI' THEN

           my_fon_ordre:=Get_Fonction('DEAUTACT');
           IF my_fon_ordre IS NULL THEN
               RAISE_APPLICATION_ERROR(-20001, 'La fonction DEAUTACT pour le type d''application DEPENSE n''existe pas.');
           END IF;

           SELECT COUNT(*) INTO my_nb  FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe
             WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre;

           IF my_nb=0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_organ_action WHERE tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
              IF my_nb>0 THEN
                 SELECT COUNT(*) INTO my_nb FROM v_organ_action WHERE tyac_id=a_tyac_id AND tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
                 IF my_nb=0 THEN
                    RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorisee pour cette ligne budgetaire et ce type de credit ('||
                       INDICATION_ERREUR.action(a_tyac_id)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
                 END IF;
              END IF;
           END IF;
        END IF;
   END;

   PROCEDURE verifier_analytique (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_can_id                  ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE
   ) IS
       my_nb      INTEGER;
   BEGIN

        -- verifier si ce code analytique est utilisable.
        SELECT COUNT(*) INTO my_nb FROM v_code_analytique c, v_code_analytique_exercice e
           WHERE c.can_id=a_can_id AND can_utilisable=Etats.get_etat_canal_utilisable
             AND tyet_id=Etats.get_etat_canal_valide AND e.can_id=c.can_id AND e.exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas utilisable  ('||INDICATION_ERREUR.analytique(a_can_id)||')');
        END IF;

        -- verifier si droit utilisation code analytique (suivant organ).
        SELECT COUNT(*) INTO my_nb FROM v_code_analytique c, v_code_analytique_exercice e
           WHERE c.can_id=a_can_id AND can_utilisable=Etats.get_etat_canal_utilisable
             AND tyet_id=Etats.get_etat_canal_valide AND e.can_id=c.can_id AND e.exe_ordre=a_exe_ordre
           AND can_public=Etats.get_etat_canal_public;
        IF my_nb=0 THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_analytique_organ WHERE
              can_id=a_can_id AND (org_id=Get_Composante(a_org_id) OR org_id=get_cr(a_org_id) OR org_id=a_org_id);
           IF my_nb=0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique ne peut etre utilise avec cette ligne budgetaire ('||
                  INDICATION_ERREUR.analytique(a_can_id)||', '||INDICATION_ERREUR.organ(a_org_id)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_convention (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_conv_ordre              ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE
   ) IS
       my_nb      INTEGER;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_convention_non_limitative
           WHERE exe_ordre=a_exe_ordre AND org_id=a_org_id AND tcd_ordre=a_tcd_ordre
             AND conv_ordre=a_conv_ordre;

        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette convention ne peut etre utilisee avec cette ligne budgetaire et ce type de credit ('||
              INDICATION_ERREUR.convention(a_conv_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_hors_marche (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_typa_id                 ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE,
      a_ce_ordre                ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE,
      a_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE
   ) IS
       my_nb         INTEGER;
       my_cm_niveau  v_code_marche.cm_niveau%TYPE;
       my_cm_suppr   v_code_marche.cm_suppr%TYPE;
       my_ce_suppr   v_code_exer.ce_suppr%TYPE;
   BEGIN
           -- les seuils sont en HT.

        -- verifier que le ce_ordre est bien sur l'année choisie.
        SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND exe_ordre=a_exe_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce code nomenclature n''existe pas sur cet exercice ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '
             ||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        -- que le cm_code est un niveau 2.
        SELECT m.cm_niveau, m.cm_suppr, e.ce_suppr INTO my_cm_niveau, my_cm_suppr, my_ce_suppr
           FROM v_code_marche m, v_code_exer e
           WHERE m.cm_ordre=e.cm_ordre AND e.ce_ordre=a_ce_ordre;
        IF my_cm_niveau<>2 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il faut mettre un code nomenclature de niveau 2 ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        IF my_ce_suppr='O' OR my_cm_suppr='O' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le code nomenclature n''est plus utilisable ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        -- verifier que ce type achat est utilisable pour cet exercice.
        --select count(*) into my_nb from type_achat_exercice where typa_id=a_typa_id and exe_ordre=a_exe_ordre;
        --if my_nb<>1 then
        --   raise_application_error(-20001, 'Ce type achat n''est pas autorise sur cet exercice (typa_id:'||a_typa_id||', exercice:'||a_exe_ordre||')');
        --end if;


        -- inclure le verifs concernant eventuellement service_achat.


        -- verifier si fournisseur est mono ou 3CMP (mettre tag dans ces types).
        IF a_typa_id=Get_Type_Achat('3CMP') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_3cmp=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "3CMP" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;

           SELECT COUNT(*) INTO my_nb FROM v_code_marche_four
              WHERE fou_ordre=a_fou_ordre AND ce_ordre=a_ce_ordre;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur n''est pas autorise pour ce code nomenclature ('||
                INDICATION_ERREUR.code_exer(a_ce_ordre)||', '||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
           END IF;
        END IF;

        IF a_typa_id=Get_Type_Achat('MONOPOLE') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_monopole=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "MONOPOLE" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;

           SELECT COUNT(*) INTO my_nb FROM v_code_marche_four
              WHERE fou_ordre=a_fou_ordre AND ce_ordre=a_ce_ordre;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur n''est pas autorise pour ce code nomenclature ('||
                INDICATION_ERREUR.code_exer(a_ce_ordre)||', '||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
           END IF;
        END IF;

        IF a_typa_id=Get_Type_Achat('SANS ACHAT') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_autres=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "SANS ACHAT" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_marche (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_fou_ordre               ENGAGE_BUDGET.fou_ordre%TYPE,
      a_att_ordre               ENGAGE_CTRL_MARCHE.att_ordre%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
       my_nb             INTEGER;
       my_lot_ordre      v_attribution.lot_ordre%TYPE;
       my_att_execution  v_attribution_execution.aee_execution%TYPE;
       my_reversement    DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_reste_engage   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
   BEGIN
        -- verifier que le fournisseur est bon.
        --    que c'est celui de l'attribution ou un sous-traitant.
        SELECT COUNT(*) INTO my_nb FROM v_attribution WHERE fou_ordre=a_fou_ordre AND att_ordre=a_att_ordre;
        IF my_nb=0 THEN
           SELECT COUNT(*) INTO my_nb FROM v_sous_traitant WHERE fou_ordre=a_fou_ordre AND att_ordre=a_att_ordre;
           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas celui de l''attribution ni un sous traitant ('||
                 INDICATION_ERREUR.fournisseur(a_fou_ordre)||', '||INDICATION_ERREUR.attribution(a_att_ordre)||')');
           END IF;
        END IF;

        -- verif sur lot_organ et lot_agent ... mais il faut changer les clefs.
        SELECT lot_ordre INTO my_lot_ordre FROM v_attribution WHERE att_ordre=a_att_ordre;

        SELECT COUNT(*) INTO my_nb FROM v_lot_organ WHERE lot_ordre=my_lot_ordre;
        IF my_nb>0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_lot_organ WHERE lot_ordre=my_lot_ordre AND org_id=a_org_id;
              IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande sur ce lot n''est pas autorisee pour cette ligne budgetaire ('||
                 INDICATION_ERREUR.lot(my_lot_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||')');
             END IF;
        END IF;

        SELECT COUNT(*) INTO my_nb FROM v_lot_utilisateur WHERE lot_ordre=my_lot_ordre;
        IF my_nb>0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_lot_utilisateur WHERE lot_ordre=my_lot_ordre AND utl_ordre=a_utl_ordre;
           
              IF my_nb=0 THEN      
              SELECT COUNT(*) INTO my_nb FROM v_utilisateur_fonct WHERE utl_ordre=a_utl_ordre AND fon_ordre=Get_Fonction_jefyadmin('TOUTORG');
              
              IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'La commande sur ce lot n''est pas autorisee pour cet utilisateur ('||
                      INDICATION_ERREUR.lot(my_lot_ordre)||', '||INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
              END IF;
             END IF;
        END IF;
   END;

   PROCEDURE verifier_planco (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
         a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_pco_num                 ENGAGE_CTRL_PLANCO.pco_num%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
          my_nb              INTEGER;
       my_pco_validite    v_plan_comptable.pco_validite%TYPE;
       my_fon_ordre          jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
        -- verifier la validite du compte.
        SELECT COUNT(*) INTO my_nb FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''existe pas pour cet exercice ('||
              INDICATION_ERREUR.imputation(a_pco_num)||INDICATION_ERREUR.exercice(a_exe_ordre)||')');
        END IF;

        SELECT pco_validite INTO my_pco_validite FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre=a_exe_ordre;
        IF my_pco_validite<>'VALIDE' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas valide pour cet exercice ('||INDICATION_ERREUR.imputation(a_pco_num)
              ||INDICATION_ERREUR.exercice(a_exe_ordre)||')');
        END IF;

        -- verifier si correct avec le type de credit.
        SELECT COUNT(*) INTO my_nb FROM v_planco_credit
         WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pla_quoi='D' and exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas associe a ce type de credit ('||
              INDICATION_ERREUR.imputation(a_pco_num)||', type de credit:'||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;

        -- on verifie si l"utilisateur a le droit d'utiliser des comptes autres que de classe 2 ou 6.
        IF SUBSTR(a_pco_num,1,1)<>'2' AND SUBSTR(a_pco_num,1,1)<>'6' THEN
              my_fon_ordre:=Get_Fonction('DEAUTIMP');
           IF my_fon_ordre IS NULL THEN
              RAISE_APPLICATION_ERROR(-20001, 'La fonction DEAUTIMP pour le type d''application DEPENSE n''existe pas.');
           END IF;

           SELECT COUNT(*) INTO my_nb
             FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe
             WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre;

           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation ('||
                 INDICATION_ERREUR.imputation(a_pco_num)||', '||INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_emargement (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
      a_mod_ordre                DEPENSE_PAPIER.mod_ordre%TYPE,
      a_ecd_ordre               DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_ecriture_detail_a_emarger
          WHERE exe_ordre=a_exe_ordre AND mod_ordre=a_mod_ordre AND ecd_ordre=a_ecd_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette ecriture n''est pas autorisee pour ce mode de paiement ('
              ||INDICATION_ERREUR.ecriture(a_ecd_ordre)||', '||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_inventaire IS
       i INTEGER;
   BEGIN
        -- a completer --
        i:=1;
   END;

   PROCEDURE verifier_monnaie IS
       i INTEGER;
   BEGIN
        -- a completer --
        i:=1;
   END;

   PROCEDURE verifier_fournisseur (
         a_fou_ordre        v_fournisseur.fou_ordre%TYPE
   ) IS
          my_nb            INTEGER;
       my_fou_valide    v_fournisseur.fou_valide%TYPE;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_fournisseur WHERE fou_ordre=a_fou_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''existe pas ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
        END IF;

        SELECT fou_valide INTO my_fou_valide FROM v_fournisseur WHERE fou_ordre=a_fou_ordre;
        IF my_fou_valide<>'O' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas valide ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_rib (
         a_fou_ordre        v_rib_fournisseur.fou_ordre%TYPE,
         a_rib_ordre        v_rib_fournisseur.rib_ordre%TYPE,
         a_mod_ordre        DEPENSE_PAPIER.mod_ordre%TYPE,
         a_exe_ordre        DEPENSE_PAPIER.exe_ordre%TYPE
   ) IS
          my_nb               INTEGER;
       my_rib_valide    v_rib_fournisseur.rib_valide%TYPE;
       my_mod_code        v_rib_fournisseur.mod_code%TYPE;
       my_mod_dom        v_mode_paiement.mod_dom%TYPE;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_mode_paiement WHERE mod_ordre=a_mod_ordre AND exe_ordre=a_exe_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le mode de paiement n''existe pas pour cet exercice  ('||INDICATION_ERREUR.exercice(a_exe_ordre)||
              ', '||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
        END IF;

         SELECT mod_code, mod_dom INTO my_mod_code, my_mod_dom FROM v_mode_paiement WHERE mod_ordre=a_mod_ordre;

        IF a_rib_ordre IS NOT NULL THEN

           IF my_mod_dom<>'VIREMENT' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de paiement il ne faut pas de rib ('||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
           END IF;

              SELECT COUNT(*) INTO my_nb FROM v_rib_fournisseur WHERE fou_ordre=a_fou_ordre AND rib_ordre=a_rib_ordre;

           IF my_nb<>1 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le rib n''existe pas pour ce fournisseur ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||', '
                 ||INDICATION_ERREUR.rib(a_rib_ordre)||')');
           END IF;

           SELECT rib_valide INTO my_rib_valide FROM v_rib_fournisseur WHERE rib_ordre=a_rib_ordre;
           IF my_rib_valide<>'O' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le rib n''est pas valide ('||INDICATION_ERREUR.rib(a_rib_ordre)||')');
           END IF;
        ELSE
           IF my_mod_dom='VIREMENT' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de paiement il faut un rib ('||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_engage_coherence (a_eng_id ENGAGE_BUDGET.eng_id%TYPE) IS
        my_eng_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
        my_eng_montant_budgetaire_rest  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
        my_eng_ht_saisie                ENGAGE_BUDGET.eng_ht_saisie%TYPE;
        my_eng_tva_saisie               ENGAGE_BUDGET.eng_tva_saisie%TYPE;
        my_eng_ttc_saisie               ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
        my_org_id                       engage_budget.org_id%type;
        my_org_canal_obligatoire        jefy_admin.organ.org_canal_obligatoire%type;
        
        my_montant_budgetaire           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
        my_montant_budgetaire_reste     ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
        my_ht_saisie                    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
        my_tva_saisie                   ENGAGE_BUDGET.eng_tva_saisie%TYPE;
        my_ttc_saisie                   ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
        my_tyap_id                      ENGAGE_BUDGET.tyap_id%TYPE;

        my_tag_marche                   INTEGER;
        my_nb                           INTEGER;
   BEGIN
        my_tag_marche:=0;
        my_nb:=0;

           -- recupere les montants de engage_budget.
        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste, eng_ht_saisie, eng_tva_saisie, eng_ttc_saisie, org_id
          INTO my_eng_montant_budgetaire, my_eng_montant_budgetaire_rest, my_eng_ht_saisie, my_eng_tva_saisie, my_eng_ttc_saisie, my_org_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        -- si c'est un engagement d'extourne on ne veut pas de repartition
     if my_eng_ttc_saisie<0 then 
           select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par action'); end if;
           select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par code analytique'); end if;
           select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par convention'); end if;
           select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par code de nomenclature'); end if;
           select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par marche'); end if;
           select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
           if my_nb>0 then raise_application_error(-20001, 'Un engagement d''extourne ne doit pas comporter de repartition par plan comptable'); end if;
     else
        select org_canal_obligatoire into my_org_canal_obligatoire from jefy_admin.organ where org_id=my_org_id;
        
        -- recupere et compare les montants de engage_ctrl_action.
        SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0),
               NVL(SUM(eact_ht_saisie),0), NVL(SUM(eact_tva_saisie),0), NVL(SUM(eact_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
        IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
           my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
           my_eng_ttc_saisie<>my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_action ('||INDICATION_ERREUR.engagement(a_eng_id)||')'||
              my_eng_montant_budgetaire||','||my_montant_budgetaire||','||
              my_eng_montant_budgetaire_rest||','||my_montant_budgetaire_reste);
        end if;

        -- recupere et compare les montants de engage_ctrl_analytique.

        SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0),
               NVL(SUM(eana_ht_saisie),0), NVL(SUM(eana_tva_saisie),0), NVL(SUM(eana_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        if my_org_canal_obligatoire is not null and (my_org_canal_obligatoire=3 or my_org_canal_obligatoire=21) then
           IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
              my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
              my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
              my_eng_ttc_saisie<>my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_analytique ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
           END IF;
        else
        -- on ne verifie pas la coherence exacte de engage_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de l'engagement.
           IF my_eng_montant_budgetaire<my_montant_budgetaire OR
              my_eng_montant_budgetaire_rest<my_montant_budgetaire_reste OR
              my_eng_ht_saisie<my_ht_saisie OR my_eng_tva_saisie<my_tva_saisie OR
              my_eng_ttc_saisie<my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_analytique ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
           END IF;
        end if;
        -- recupere et compare les montants de engage_ctrl_convention.

        -- on ne verifie pas la coherence exacte de engage_ctrl_convention.
        --   puisque les conventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de l'engagement.

        SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0),
               NVL(SUM(econ_ht_saisie),0), NVL(SUM(econ_tva_saisie),0), NVL(SUM(econ_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<my_montant_budgetaire_reste OR
           my_eng_ht_saisie<my_ht_saisie OR my_eng_tva_saisie<my_tva_saisie OR
           my_eng_ttc_saisie<my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_convention ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- recupere et compare les montants de engage_ctrl_planco.
        SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0),
               NVL(SUM(epco_ht_saisie),0), NVL(SUM(epco_tva_saisie),0), NVL(SUM(epco_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
           my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
           my_eng_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_planco ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        IF my_tyap_id<>Get_Tyap_Id('PRESTATION INTERNE') THEN
           -- recupere et compare les montants de engage_ctrl_hors_marche.
           -- on teste si c'est un engagement hors marche.
           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;

           -- si c'est hors marche on teste.
           IF my_nb>0 THEN
              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0),
                  NVL(SUM(ehom_ht_saisie),0), NVL(SUM(ehom_tva_saisie),0), NVL(SUM(ehom_ttc_saisie),0)
                INTO my_montant_budgetaire, my_montant_budgetaire_reste,
                  my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;

                 IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
                 my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
                   my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
                   my_eng_ttc_saisie<>my_ttc_saisie THEN
                   RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_hors_marche ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           END IF;

           -- recupere et compare les montants de engage_ctrl_marche.
           IF my_nb>0 THEN
              -- si on a trouve un engagement hors marche, on teste qu'il n'y a pas d'engagement marche.
              SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
              IF my_nb>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit etre MARCHE ou HORS MARCHE ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           ELSE
              -- si pas d'engagement hors marche on teste qu'il y a un engagement marche.
                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
                 IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit etre MARCHE ou HORS MARCHE ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0),
                  NVL(SUM(emar_ht_saisie),0), NVL(SUM(emar_tva_saisie),0), NVL(SUM(emar_ttc_saisie),0)
                INTO my_montant_budgetaire, my_montant_budgetaire_reste,
                  my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

              IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
                 my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
                 my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
                 my_eng_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_marche ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           END IF;
        END IF;
     end if;

   END;

   PROCEDURE verifier_commande_coherence(a_comm_id COMMANDE.comm_id%TYPE)
   IS
        my_nb_art_marche      INTEGER;
        my_nb_art_hors_marche INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb_art_hors_marche FROM ARTICLE
          WHERE comm_id=a_comm_id AND att_ordre IS NULL;

        SELECT COUNT(*) INTO my_nb_art_marche FROM ARTICLE
          WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;

        IF my_nb_art_hors_marche>0 AND my_nb_art_marche>0 THEN
          RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre marche ou hors marche');
        END IF;
   END;

   PROCEDURE verifier_cde_budget_coherence(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE)
   IS
        my_exe_ordre                    COMMANDE.exe_ordre%TYPE;

        my_ht                           COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
        my_tva                          COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
        my_ttc                          COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
        my_budgetaire                   COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
        my_pourcentage                  COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;

        my_budget_ht                    COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
        my_budget_tva                   COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
        my_budget_ttc                   COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
        my_budget_montant_budget        COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
        my_tag_marche                   INTEGER;
        my_nb                           INTEGER;
   BEGIN
        my_tag_marche:=0;
        my_nb:=0;

        SELECT exe_ordre INTO my_exe_ordre FROM COMMANDE
           WHERE comm_id IN (SELECT comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id);

        SELECT NVL(cbud_montant_budgetaire,0),
                NVL(cbud_ht_saisie,0), NVL(cbud_tva_saisie,0), NVL(cbud_ttc_saisie,0)
           INTO my_budget_montant_budget,
                my_budget_ht, my_budget_tva, my_budget_ttc
           FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        -- recupere et compare les montants de commande_ctrl_action.
        SELECT NVL(SUM(cact_montant_budgetaire),0), NVL(SUM(cact_pourcentage),0),
            NVL(SUM(cact_ht_saisie),0), NVL(SUM(cact_tva_saisie),0), NVL(SUM(cact_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
           FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_budget_montant_budget<>my_budgetaire OR
              100.0<my_pourcentage OR
              my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
              my_budget_ttc<>my_ttc THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_action (commande_budget:'||a_cbud_id||')');
        END IF;

        -- recupere et compare les montants de commande_ctrl_analytique.

        -- on ne verifie pas la coherence exacte de commande_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de la commande_budget.

        SELECT NVL(SUM(cana_montant_budgetaire),0), NVL(SUM(cana_pourcentage),0),
               NVL(SUM(cana_ht_saisie),0), NVL(SUM(cana_tva_saisie),0), NVL(SUM(cana_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
         FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

         IF my_budget_montant_budget<my_budgetaire OR
                100.0<my_pourcentage OR
               my_budget_ht<my_ht OR my_budget_tva<my_tva OR
               my_budget_ttc<my_ttc THEN
               RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_analytique (commande_budget:'||a_cbud_id||')');
         END IF;

        -- recupere et compare les montants de commande_ctrl_convention.

        -- on ne verifie pas la coherence exacte de commande_ctrl_convention.
        --   puisque les coventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de la commande_budget.

        SELECT NVL(SUM(ccon_montant_budgetaire),0), NVL(SUM(ccon_pourcentage),0),
               NVL(SUM(ccon_ht_saisie),0), NVL(SUM(ccon_tva_saisie),0), NVL(SUM(ccon_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
         FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

         IF my_budget_montant_budget<my_budgetaire OR
                100.0<my_pourcentage OR
               my_budget_ht<my_ht OR my_budget_tva<my_tva OR
               my_budget_ttc<my_ttc THEN
               RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_convention (commande_budget:'||a_cbud_id||')');
         END IF;

         -- recupere et compare les montants de commande_ctrl_planco.
         SELECT NVL(SUM(cpco_montant_budgetaire),0), NVL(SUM(cpco_pourcentage),0),
                   NVL(SUM(cpco_ht_saisie),0), NVL(SUM(cpco_tva_saisie),0), NVL(SUM(cpco_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
           FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_budget_montant_budget<>my_budgetaire OR
               100.0<my_pourcentage OR
              my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
              my_budget_ttc<>my_ttc THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_planco (commande_budget:'||a_cbud_id||')');
        END IF;

        -- recupere et compare les montants de commande_ctrl_hors_marche.
           -- on teste si c'est une commande hors marche.
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;

           -- si c'est hors marche on teste.
        IF my_nb>0 THEN
           SELECT NVL(SUM(chom_montant_budgetaire),0),
                  NVL(SUM(chom_ht_saisie),0), NVL(SUM(chom_tva_saisie),0), NVL(SUM(chom_ttc_saisie),0)
             INTO my_budgetaire, my_ht, my_tva, my_ttc
             FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;

           IF my_budget_montant_budget<>my_budgetaire OR
                 my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
                 my_budget_ttc<>my_ttc THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_hors_marche (commande_budget:'||a_cbud_id||')');
           END IF;
        END IF;

        -- recupere et compare les montants de commande_ctrl_marche.
        IF my_nb>0 THEN
           -- si on a trouve une commande hors marche, on teste qu'il n'y a pas de commande marche.
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
           IF my_nb>0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre MARCHE ou HORS MARCHE (commande_budget:'||a_cbud_id||')');
           END IF;
        ELSE
           -- si pas de commande hors marche on teste qu'il y a une commande marche.
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
           IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre MARCHE ou HORS MARCHE (commande_budget:'||a_cbud_id||')');
           END IF;

           SELECT NVL(SUM(cmar_montant_budgetaire),0),
                  NVL(SUM(cmar_ht_saisie),0), NVL(SUM(cmar_tva_saisie),0), NVL(SUM(cmar_ttc_saisie),0)
             INTO my_budgetaire, my_ht, my_tva, my_ttc
             FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

           IF my_budget_montant_budget<>my_budgetaire OR
                 my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
                 my_budget_ttc<>my_ttc THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_marche (commande_budget:'||a_cbud_id||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_depense_pap_coherence(a_dpp_id DEPENSE_PAPIER.dpp_id%TYPE)
   IS
        my_dpp_ht_saisie                DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
        my_dpp_tva_saisie                DEPENSE_PAPIER.dpp_tva_saisie%TYPE;
        my_dpp_ttc_saisie                DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;

        my_ht_saisie                    DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
        my_tva_saisie                    DEPENSE_PAPIER.dpp_tva_saisie%TYPE;
        my_ttc_saisie                    DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
   BEGIN

           -- recupere les montants de depense_papier.
        SELECT dpp_ht_saisie, dpp_tva_saisie, dpp_ttc_saisie
          INTO my_dpp_ht_saisie, my_dpp_tva_saisie, my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- recupere et compare les montants de depense_budget.
        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0), NVL(SUM(dep_ttc_saisie),0)
          INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        IF my_dpp_ht_saisie<>my_ht_saisie OR my_dpp_tva_saisie<>my_tva_saisie OR
           my_dpp_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_budget ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;
   END;

   PROCEDURE verifier_depense_coherence(a_dep_id DEPENSE_BUDGET.dep_id%TYPE)
   IS
           my_dep_montant_budgetaire       DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
        my_dep_ht_saisie                DEPENSE_BUDGET.dep_ht_saisie%TYPE;
        my_dep_tva_saisie                DEPENSE_BUDGET.dep_tva_saisie%TYPE;
        my_dep_ttc_saisie                DEPENSE_BUDGET.dep_ttc_saisie%TYPE;

        my_montant_budgetaire            DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
        my_ht_saisie                    DEPENSE_BUDGET.dep_ht_saisie%TYPE;
        my_tva_saisie                    DEPENSE_BUDGET.dep_tva_saisie%TYPE;
        my_ttc_saisie                    DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
        my_tyap_id                        ENGAGE_BUDGET.tyap_id%TYPE;

        my_org_id                       engage_budget.org_id%type;
        my_org_canal_obligatoire        jefy_admin.organ.org_canal_obligatoire%type;
        my_eng_id                       engage_budget.eng_id%type;

        my_tag_marche                    INTEGER;
        my_nb                            INTEGER;
        my_nb_eng                            INTEGER;
   BEGIN
           my_tag_marche:=0;
        my_nb:=0;

        -- recupere les montants de depense_budget.
        SELECT dep_montant_budgetaire, dep_ht_saisie, dep_tva_saisie, dep_ttc_saisie, tyap_id, org_id, e.eng_id
          INTO my_dep_montant_budgetaire, my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, my_tyap_id, my_org_id, my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dep_id=a_dep_id AND d.eng_id=e.eng_id;
    
    
        select count(*) into my_nb_eng from engage_ctrl_action where eng_id=my_eng_id;
        -- my_nb_eng = 0  ==> engagement d'extourne
        
        select org_canal_obligatoire into my_org_canal_obligatoire from jefy_admin.organ where org_id=my_org_id;

        if (/*my_dep_ttc_saisie>0 and*/ my_nb_eng>0) then 
        -- recupere et compare les montants de depense_ctrl_action.
        SELECT NVL(SUM(dact_montant_budgetaire),0), NVL(SUM(dact_ht_saisie),0), NVL(SUM(dact_tva_saisie),0),
           NVL(SUM(dact_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;

        IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
           my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
           my_dep_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_action (depense:'||a_dep_id||')');
        END IF;
        end if;
        -- recupere et compare les montants de depense_ctrl_analytique.

        -- on ne verifie pas la coherence exacte de depense_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de la depense.

        SELECT NVL(SUM(dana_montant_budgetaire),0), NVL(SUM(dana_ht_saisie),0), NVL(SUM(dana_tva_saisie),0),
          NVL(SUM(dana_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;

        if my_org_canal_obligatoire is not null and (my_org_canal_obligatoire=3 or my_org_canal_obligatoire=21) then
           IF ABS(my_dep_ht_saisie)<>ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<>ABS(my_tva_saisie) OR
              ABS(my_dep_ttc_saisie)<>ABS(my_ttc_saisie) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           END IF;
           
           if (my_nb_eng>0 and ABS(my_dep_montant_budgetaire)<>ABS(my_montant_budgetaire)) then
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           end if;
        else
           IF ABS(my_dep_ht_saisie)<ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<ABS(my_tva_saisie) OR
              ABS(my_dep_ttc_saisie)<ABS(my_ttc_saisie) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           END IF;

           if (my_nb_eng>0 and ABS(my_dep_montant_budgetaire)<ABS(my_montant_budgetaire)) then
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           end if;
        end if;
        -- recupere et compare les montants de depense_ctrl_convention.

        -- on ne verifie pas la coherence exacte de depense_ctrl_convention.
        --   puisque les conventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de la depense.

        SELECT NVL(SUM(dcon_montant_budgetaire),0), NVL(SUM(dcon_ht_saisie),0), NVL(SUM(dcon_tva_saisie),0),
          NVL(SUM(dcon_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;

        IF ABS(my_dep_ht_saisie)<ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<ABS(my_tva_saisie) OR
           ABS(my_dep_ttc_saisie)<ABS(my_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_convention (depense:'||a_dep_id||')');
        END IF;
           if (my_nb_eng>0 and ABS(my_dep_montant_budgetaire)<ABS(my_montant_budgetaire)) then
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_convention (depense:'||a_dep_id||')');
           end if;

        -- recupere et compare les montants de depense_ctrl_planco.

          -- pour les problemes de rejet d'une partie d'une depense_budget on limite pour le moment.
          --  a un seul compte d'imputation.
        if my_dep_ttc_saisie>0 then 
       
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'On limite a une seule imputation pour les depenses (depense:'||a_dep_id||')');
        END IF;

        SELECT NVL(SUM(dpco_montant_budgetaire),0), NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_tva_saisie),0),
          NVL(SUM(dpco_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

        IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
           my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
           my_dep_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_planco (depense:'||a_dep_id||')');
        END IF;
        end if;
        
       if my_dep_ttc_saisie>0  and my_nb_eng>0 then  
        
        IF my_tyap_id<>Get_Tyap_Id('PRESTATION INTERNE') THEN
           -- recupere et compare les montants de depense_ctrl_hors_marche.
           -- on teste si c'est une depense hors marche.
           SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;

           -- si c'est hors marche on teste.
           IF my_nb>0 THEN
              SELECT NVL(SUM(dhom_montant_budgetaire),0), NVL(SUM(dhom_ht_saisie),0), NVL(SUM(dhom_tva_saisie),0),
                NVL(SUM(dhom_ttc_saisie),0)
                INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;

              IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
                 my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
                 my_dep_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_hors_marche (depense:'||a_dep_id||')');
              END IF;
           END IF;

           -- recupere et compare les montants de depense_ctrl_marche.
           IF my_nb>0 THEN
              -- si on a trouve une depense hors marche, on teste qu'il n'y a pas de depense marche.
              SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;
              IF my_nb>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La depense doit etre MARCHE ou HORS MARCHE (depense:'||a_dep_id||')');
              END IF;
           ELSE
              -- si pas de depense hors marche on teste qu'il y a une depense marche.
              SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;
              IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La depense doit etre MARCHE ou HORS MARCHE (depense:'||a_dep_id||')');
              END IF;

              SELECT NVL(SUM(dmar_montant_budgetaire),0), NVL(SUM(dmar_ht_saisie),0), NVL(SUM(dmar_tva_saisie),0),
                NVL(SUM(dmar_ttc_saisie),0)
                INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

              IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
                 my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
                 my_dep_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_marche (depense:'||a_dep_id||')');
              END IF;
           END IF;
        END IF;
       end if; 
        
   END;

   PROCEDURE verifier_organ(
        a_org_id       ENGAGE_BUDGET.org_id%TYPE,
     a_tcd_ordre   ENGAGE_BUDGET.tcd_ordre%TYPE
   ) IS
       my_org_niv        jefy_admin.organ.org_niv%TYPE;
       my_nb            INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.organ WHERE org_id=a_org_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La ligne budgetaire n''existe pas ('||INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;

           SELECT org_niv INTO my_org_niv FROM jefy_admin.organ WHERE org_id=a_org_id;

        IF my_org_niv<3 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il faut engager sur un CR ou une convention ('||INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;
   END;

   PROCEDURE verifier_util_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des liquidations sur cet engagement ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des commandes sur cet engagement ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- a rajouter.

        -- missions.
        /*select count(*) into my_nb from jefy_mission.mission_paiement_engage where eng_id=a_eng_id;
        if my_nb>0 then
           raise_application_error(-20001, 'Il y a des missions sur cet engagement (eng_id='||a_eng_id||')');
        end if;
        */
        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des reversements sur cette liquidation (dep_id='||a_dep_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id AND man_id IS NOT NULL;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des mandats sur cette liquidation (dep_id='||a_dep_id||')');
        END IF;

        -- a rajouter.

        -- missions.
        --select count(*) into my_nb from jefy_mission.mission_paiement_liquide where dep_id=a_dep_id;
        --if my_nb>0 then
        --   raise_application_error(-20001, 'Il y a des missions sur cette liquidation (dep_id='||a_dep_id||')');
        --end if;

        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des liquidations sur cette facture ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des reversements sur cette facture ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;


        -- a rajouter.

        -- missions.
        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_commande (
      a_comm_id             COMMANDE.comm_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des engagements sur cette commande ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;
   END;

   PROCEDURE verifier_util_commande_budget (
      a_cbud_id              COMMANDE_BUDGET.cbud_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
           my_nb:=0;
   END;

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Corriger
IS

   PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE)
   IS
     my_nb INTEGER;
     my_comm_reference     commande.comm_reference%type;
     my_comm_numero        commande.comm_numero%type;
     my_exe_ordre          commande.exe_ordre%type;
     my_eng_id             engage_budget.eng_id%type;
     my_org_ub             v_organ.org_ub%type;
     my_org_cr             v_organ.org_cr%type;
     my_eng_ht_saisie      ENGAGE_BUDGET.eng_ht_saisie%TYPE;
     my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_eng_montant        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_montant_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_art_prix_total_ht  ARTICLE.art_prix_total_ht%TYPE;
     my_art_prix_total_ttc ARTICLE.art_prix_total_ttc%TYPE;
     my_tyet_id_imprimable COMMANDE.tyet_id_imprimable%TYPE;
     newCommRef             varchar2(500);
   BEGIN
      SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;

      -- si il n'y a pas d'engagement c'est une precommande.
      IF my_nb=0 THEN
         UPDATE COMMANDE SET tyet_id=Etats.get_etat_precommande,
            tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
      ELSE
         SELECT SUM(eng_ht_saisie), SUM(eng_ttc_saisie), SUM(eng_montant_budgetaire),
             SUM(eng_montant_budgetaire_reste)
           INTO my_eng_ht_saisie, my_eng_ttc_saisie, my_eng_montant, my_eng_montant_reste
           FROM ENGAGE_BUDGET e, COMMANDE_ENGAGEMENT ce
           WHERE ce.comm_id=a_comm_id AND ce.eng_id=e.eng_id;

         SELECT SUM(art_prix_total_ht), SUM(art_prix_total_ttc) INTO my_art_prix_total_ht, my_art_prix_total_ttc
           FROM ARTICLE WHERE comm_id=a_comm_id;

         -- si le montant engage est inferieur au montant commande c'est partiellement engage.
         IF my_eng_ht_saisie<my_art_prix_total_ht OR my_eng_ttc_saisie<my_art_prix_total_ttc THEN
             UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_engagee,
               tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
         ELSE
            -- si le reste engage est egal au montant engage c'est une commande engagee.
            IF my_eng_montant=my_eng_montant_reste THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_engagee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le montant engage est superieur au reste c'est partiellement solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste>0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_soldee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le reste est a 0 c'est solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste=0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_soldee,
                 tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
            END IF;

             -- on verifie dans les services validations pour savoir si la commande est imprimable.
            SELECT tyet_id_imprimable INTO my_tyet_id_imprimable FROM COMMANDE WHERE comm_id=a_comm_id;
            IF my_tyet_id_imprimable=Etats.get_etat_imprimable THEN

                SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande WHERE vlco_comid=a_comm_id
                     AND vlco_valide ='OUI';
                IF my_nb>0 THEN

                   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande
                      WHERE vlco_comid=a_comm_id AND vlco_etat<>'ACCEPTEE' AND vlco_valide ='OUI';
                      -- Ajout du vlco_id max --
                      --AND vlco_id = (SELECT MAX(vlco_id) FROM jefy_marches.sa_validation_commande
                        --    WHERE vlco_comid=a_comm_id AND vlco_valide = 'OUI');

                   IF my_nb>0 THEN
                        UPDATE COMMANDE SET tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
                   END IF;

                END IF;
            END IF;

         END IF;
         
         -- reference automatique.
         SELECT comm_numero, comm_reference, exe_ordre INTO my_comm_numero, my_comm_reference, my_exe_ordre
            FROM COMMANDE WHERE comm_id=a_comm_id;
         if to_char(my_comm_numero)=my_comm_reference then
          
            select min(eng_id) into my_eng_id from commande_engagement where comm_id=a_comm_id;
            select o.org_ub, o.org_cr into my_org_ub, my_org_cr from v_organ o, engage_budget e
              where o.org_id=e.org_id and e.eng_id=my_eng_id;
              newCommRef := my_org_ub||'/'||my_org_cr;
              if (length(newCommRef)>=44) then
                newCommRef := substr(newCommRef,1,44);
              end if;
              newCommRef := newCommRef ||'/'||Get_Numerotation(my_exe_ordre,my_org_ub,my_org_cr,'COMMANDE_REF');
              
              
            update commande set comm_reference=newCommRef
              where comm_id=a_comm_id;
         end if;
      END IF;
   END;

   PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE   ) IS
     CURSOR liste IS SELECT * FROM COMMANDE_BUDGET WHERE comm_id=a_comm_id;

     my_commande_budget                COMMANDE_BUDGET%ROWTYPE;
   BEGIN
        OPEN liste();
          LOOP
           FETCH  liste INTO my_commande_budget;
           EXIT WHEN liste%NOTFOUND;

           corriger_commande_ctrl(my_commande_budget.cbud_id);
        END LOOP;
        CLOSE liste;
   END;

   PROCEDURE corriger_engage_extourne (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      my_nb integer;
   begin
      my_nb:=0;
   end;

   PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_reste         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_dep_bud       depense_budget.dep_montant_budgetaire%type;
     my_eng_bud       engage_budget.eng_montant_budgetaire%type;
     my_exe_ordre     engage_budget.exe_ordre%type;
     my_org_id        engage_budget.org_id%type;
     my_tcd_ordre     engage_budget.tcd_ordre%type;
   BEGIN
        select nvl(sum(eng_montant_budgetaire_reste),0) into my_reste from engage_budget where eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_dep_bud from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
--        select nvl(sum(eng_montant_budgetaire),0) into my_eng_bud from engage_budget where eng_id=a_eng_id;
--        my_reste:=my_eng_bud-my_dep_bud;
--        if my_reste<0 then my_reste:=0; end if;

--        select exe_ordre, org_id, tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre from engage_budget where eng_id=a_eng_id; 
--        update engage_budget set eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;
--        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>0 THEN
        --dbms_output.put_line ('debut '||a_eng_id||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_action(a_eng_id);
        --dbms_output.put_line (TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_analytique(a_eng_id);
             upd_engage_reste_convention(a_eng_id);
             upd_engage_reste_hors_marche(a_eng_id);
             upd_engage_reste_marche(a_eng_id);
             upd_engage_reste_planco(a_eng_id);
        ELSE
          UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        END IF;
   END;


   /*********************************************************************************/
   PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT tyac_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie))
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY tyac_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
        AND eact_montant_budgetaire_reste>0;

     my_depense_tyac_id               DEPENSE_CTRL_ACTION.tyac_id%TYPE;
     my_engage_ctrl_action            ENGAGE_CTRL_ACTION%ROWTYPE;

     my_eact_montant_bud              ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
     my_eact_montant_bud_reste        ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_eact_id                       ENGAGE_CTRL_ACTION.eact_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     --my_org_id                      ENGAGE_BUDGET.org_id%TYPE;
     --my_tcd_ordre                   ENGAGE_BUDGET.tcd_ordre%TYPE;
     --my_tap_id                      ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eact_reste                    engage_ctrl_action.eact_montant_budgetaire_reste%type;
     my_dact_bud                      depense_ctrl_action.dact_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre INTO my_exe_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eact_montant_budgetaire_reste) into my_eact_reste
          from engage_ctrl_action where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie)),0)
          into my_dact_bud
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eact_reste>my_reste+my_dact_bud then
           my_somme:=my_eact_reste-my_reste-my_dact_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_action;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_action.eact_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_action 
                      set eact_montant_budgetaire_reste=my_engage_ctrl_action.eact_montant_budgetaire_reste-my_somme
                      where eact_id=my_engage_ctrl_action.eact_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_action.eact_montant_budgetaire_reste;
                    update engage_ctrl_action set eact_montant_budgetaire_reste=0 where eact_id=my_engage_ctrl_action.eact_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eact_montant_budgetaire_reste) into my_eact_reste
             from engage_ctrl_action where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_tyac_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION
              WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eact_id, eact_montant_budgetaire_reste, eact_montant_budgetaire
                INTO my_eact_id, my_eact_montant_bud_reste, my_eact_montant_bud
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

              -- cas de la liquidation.
              IF my_eact_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ACTION
                   SET eact_montant_budgetaire_reste=my_eact_montant_bud_reste-my_montant_budgetaire
                   WHERE eact_id=my_eact_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eact_id=my_eact_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eact_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0)
                INTO my_eact_montant_bud, my_eact_montant_bud_reste
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND eact_montant_budgetaire_reste>0;

              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>=my_eact_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
                         AND eact_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_action;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant := upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_action.eact_montant_budgetaire_reste, my_eact_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire_reste-my_montant
                          WHERE eact_id=my_engage_ctrl_action.eact_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;


   /****************************************************************************/
   PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      CURSOR liste  IS SELECT can_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie))
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id
        AND eana_montant_budgetaire_reste>0;

     my_depense_can_id                DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
     my_engage_ctrl_analytique        ENGAGE_CTRL_ANALYTIQUE%ROWTYPE;

     my_eana_montant_bud              ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
     my_eana_montant_bud_reste        ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eana_id                       ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                        ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                     ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                        ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste                 ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eana_reste                    engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste%type;
     my_dana_bud                      depense_ctrl_ANALYTIQUE.dana_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id /*AND eana_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eana_montant_budgetaire_reste) into my_eana_reste
          from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eana_reste>my_reste+my_dana_bud then
           my_somme:=my_eana_reste-my_reste-my_dana_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_ANALYTIQUE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_ANALYTIQUE 
                      set eana_montant_budgetaire_reste=my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste-my_somme
                      where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste;
                    update engage_ctrl_ANALYTIQUE set eana_montant_budgetaire_reste=0 where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eana_montant_budgetaire_reste) into my_eana_reste
             from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_can_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE
              WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eana_id, eana_montant_budgetaire_reste, eana_montant_budgetaire
                INTO my_eana_id, my_eana_montant_bud_reste, my_eana_montant_bud
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

              -- cas de la liquidation.
              IF my_eana_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE
                   SET eana_montant_budgetaire_reste=my_eana_montant_bud_reste-my_montant_budgetaire
                   WHERE eana_id=my_eana_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eana_id=my_eana_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eana_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0)
                INTO my_eana_montant_bud, my_eana_montant_bud_reste
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>=my_eana_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_analytique;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                          WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(eana_montant_budgetaire_reste),0) INTO my_eana_montant_bud_reste
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        IF my_eana_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_eana_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id  AND eana_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_analytique;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                    WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT conv_ordre, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie))
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY conv_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id
        AND econ_montant_budgetaire_reste>0;

     my_depense_conv_ordre       DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
     my_engage_ctrl_convention   ENGAGE_CTRL_CONVENTION%ROWTYPE;

     my_econ_montant_bud         ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
     my_econ_montant_bud_reste   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_econ_id                  ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste            ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                  NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_econ_reste               engage_ctrl_CONVENTION.econ_montant_budgetaire_reste%type;
     my_dcon_bud                 depense_ctrl_CONVENTION.dcon_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
   BEGIN
        select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id /*AND econ_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(econ_montant_budgetaire_reste) into my_econ_reste
          from engage_ctrl_CONVENTION where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie)),0)
          into my_dcon_bud
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_econ_reste>my_reste+my_dcon_bud then
           my_somme:=my_econ_reste-my_reste-my_dcon_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_CONVENTION;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_CONVENTION
                      set econ_montant_budgetaire_reste=my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste-my_somme
                      where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste;
                    update engage_ctrl_CONVENTION set econ_montant_budgetaire_reste=0 where econ_id=my_engage_ctrl_CONVENTION.econ_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(econ_montant_budgetaire_reste) into my_econ_reste
             from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_conv_ordre, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION
              WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT econ_id, econ_montant_budgetaire_reste, econ_montant_budgetaire
                INTO my_econ_id, my_econ_montant_bud_reste, my_econ_montant_bud
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

              -- cas de la liquidation.
              IF my_econ_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_CONVENTION
                   SET econ_montant_budgetaire_reste=my_econ_montant_bud_reste-my_montant_budgetaire
                   WHERE econ_id=my_econ_id;
              ELSE
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE econ_id=my_econ_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_econ_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0)
                INTO my_econ_montant_bud, my_econ_montant_bud_reste
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;

              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>=my_econ_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_convention;
                        EXIT WHEN liste2%NOTFOUND;

                        IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                          WHERE econ_id=my_engage_ctrl_convention.econ_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(econ_montant_budgetaire_reste),0) INTO my_econ_montant_bud_reste
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_econ_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_econ_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id  AND econ_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_convention;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                    WHERE econ_id=my_engage_ctrl_convention.econ_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT ce_ordre, typa_id, SUM(dc.dhom_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie))
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY ce_ordre, typa_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id
        AND ehom_montant_budgetaire_reste>0;

     my_depense_ce_ordre         DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
     my_depense_typa_id          DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
     my_engage_ctrl_hors_marche  ENGAGE_CTRL_HORS_MARCHE%ROWTYPE;

     my_ehom_montant_bud         ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
     my_ehom_montant_bud_reste   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_ehom_ht_reste            ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_ehom_id                  ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_ehom_reste               engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste%type;
     my_dhom_bud                 depense_ctrl_HORS_MARCHE.dhom_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_HORS_MARCHE
          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire, ehom_ht_reste=ehom_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
          from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie)),0)
          into my_dhom_bud
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_ehom_reste>my_reste+my_dhom_bud then
           my_somme:=my_ehom_reste-my_reste-my_dhom_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_HORS_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_HORS_MARCHE 
                      set ehom_montant_budgetaire_reste=my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste-my_somme
                      where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste;
                    update engage_ctrl_HORS_MARCHE set ehom_montant_budgetaire_reste=0 where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
             from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_ce_ordre, my_depense_typa_id, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE eng_id=a_eng_id AND typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT ehom_id, ehom_montant_budgetaire_reste, ehom_montant_budgetaire, ehom_ht_reste
                INTO my_ehom_id, my_ehom_montant_bud_reste, my_ehom_montant_bud, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND
                typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

              -- cas de la liquidation.
              IF my_ehom_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE
                   SET ehom_montant_budgetaire_reste=my_ehom_montant_bud_reste-my_montant_budgetaire,
                       ehom_ht_reste=decode(ehom_ht_reste-my_montant_budht, abs(ehom_ht_reste-my_montant_budht),
                          ehom_ht_reste-my_montant_budht,0)
                   WHERE ehom_id=my_ehom_id;
              ELSE
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                   WHERE ehom_id=my_ehom_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_ehom_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_ehom_ht_reste;
              
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0), NVL(SUM(ehom_ht_reste),0)
                INTO my_ehom_montant_bud, my_ehom_montant_bud_reste, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND ehom_montant_budgetaire_reste>0;

              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>=my_ehom_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                    WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id  AND ehom_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_hors_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_hors_marche.ehom_montant_budgetaire_reste, my_ehom_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_hors_marche.ehom_ht_reste, my_ehom_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_HORS_MARCHE
                          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire_reste-my_montant,
                              ehom_ht_reste=decode(ehom_ht_reste-my_montant_ht, abs(ehom_ht_reste-my_montant_ht),
                                  ehom_ht_reste-my_montant_ht,0)
                          WHERE ehom_id=my_engage_ctrl_hors_marche.ehom_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT att_ordre, SUM(dc.dmar_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie))
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY att_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id
        AND emar_montant_budgetaire_reste>0;

     my_depense_att_ordre        DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
     my_engage_ctrl_marche       ENGAGE_CTRL_MARCHE%ROWTYPE;

     my_emar_montant_bud         ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
     my_emar_montant_bud_reste   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_emar_ht_reste            ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_emar_id                  ENGAGE_CTRL_MARCHE.emar_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_emar_reste               engage_ctrl_MARCHE.emar_montant_budgetaire_reste%type;
     my_dmar_bud                 depense_ctrl_MARCHE.dmar_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_MARCHE
          SET emar_montant_budgetaire_reste=emar_montant_budgetaire, emar_ht_reste=emar_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(emar_montant_budgetaire_reste) into my_emar_reste
          from engage_ctrl_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie)),0)
          into my_dmar_bud
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_emar_reste>my_reste+my_dmar_bud then
           my_somme:=my_emar_reste-my_reste-my_dmar_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_MARCHE 
                      set emar_montant_budgetaire_reste=my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste-my_somme
                      where emar_id=my_engage_ctrl_MARCHE.emar_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste;
                    update engage_ctrl_MARCHE set emar_montant_budgetaire_reste=0 where emar_id=my_engage_ctrl_MARCHE.emar_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(emar_montant_budgetaire_reste) into my_emar_reste
             from engage_ctrl_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_att_ordre, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE
              WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT emar_id, emar_montant_budgetaire_reste, emar_montant_budgetaire, emar_ht_reste
                INTO my_emar_id, my_emar_montant_bud_reste, my_emar_montant_bud, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

              -- cas de la liquidation.
              IF my_emar_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_MARCHE
                   SET emar_montant_budgetaire_reste=my_emar_montant_bud_reste-my_montant_budgetaire,
                   emar_ht_reste=decode(emar_ht_reste-my_montant_budht,abs(emar_ht_reste-my_montant_budht),
                       emar_ht_reste-my_montant_budht,0)
                   WHERE emar_id=my_emar_id;
              ELSE
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE emar_id=my_emar_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_emar_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_emar_ht_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0), NVL(SUM(emar_ht_reste),0)
                INTO my_emar_montant_bud, my_emar_montant_bud_reste, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND emar_montant_budgetaire_reste>0;

              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>=my_emar_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id  AND emar_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_marche.emar_montant_budgetaire_reste, my_emar_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_marche.emar_ht_reste, my_emar_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_MARCHE
                          SET emar_montant_budgetaire_reste=emar_montant_budgetaire_reste-my_montant,
                              emar_ht_reste=decode(emar_ht_reste-my_montant_ht, abs(emar_ht_reste-my_montant_ht),
                                 emar_ht_reste-my_montant_ht,0)
                          WHERE emar_id=my_engage_ctrl_marche.emar_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT pco_num, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie))
        FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY pco_num;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id
        AND epco_montant_budgetaire_reste>0;

     my_depense_pco_num        DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_engage_ctrl_planco     ENGAGE_CTRL_PLANCO%ROWTYPE;

     my_epco_montant_bud       ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
     my_epco_montant_bud_reste ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_epco_id                ENGAGE_CTRL_PLANCO.epco_id%TYPE;
     my_exe_ordre              ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                 ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire     DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
     my_nb                     INTEGER;
     my_somme                  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_montant                NUMBER;
     my_dernier                BOOLEAN;
     my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_epco_reste             engage_ctrl_PLANCO.epco_montant_budgetaire_reste%type;
     my_dpco_bud               depense_ctrl_PLANCO.dpco_montant_budgetaire%type;
     my_eng                    ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(epco_montant_budgetaire_reste) into my_epco_reste
          from engage_ctrl_PLANCO where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie)),0)
          into my_dpco_bud
         FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_epco_reste>my_reste+my_dpco_bud then
           my_somme:=my_epco_reste-my_reste-my_dpco_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_PLANCO;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_PLANCO 
                      set epco_montant_budgetaire_reste=my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste-my_somme
                      where epco_id=my_engage_ctrl_PLANCO.epco_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste;
                    update engage_ctrl_PLANCO set epco_montant_budgetaire_reste=0 where epco_id=my_engage_ctrl_PLANCO.epco_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(epco_montant_budgetaire_reste) into my_epco_reste
             from engage_ctrl_PLANCO where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_pco_num, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO
              WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT epco_id, epco_montant_budgetaire_reste, epco_montant_budgetaire
                INTO my_epco_id, my_epco_montant_bud_reste, my_epco_montant_bud
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

              -- cas de la liquidation.
              IF my_epco_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_PLANCO
                   SET epco_montant_budgetaire_reste=my_epco_montant_bud_reste-my_montant_budgetaire
                   WHERE epco_id=my_epco_id;
              ELSE
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE epco_id=my_epco_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_epco_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0)
                INTO my_epco_montant_bud, my_epco_montant_bud_reste
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND epco_montant_budgetaire_reste>0;

              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>=my_epco_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id  AND epco_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_planco;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_planco.epco_montant_budgetaire_reste, my_epco_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire_reste-my_montant
                          WHERE epco_id=my_engage_ctrl_planco.epco_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_dernier             BOOLEAN,
      a_exe_ordre           ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE
   IS
     my_nb                 INTEGER;
     my_par_value          jefy_admin.PARAMETRE.par_value%TYPE;
     my_dev                jefy_admin.devise.dev_nb_decimales%TYPE;
     my_montant            NUMBER;
     my_pourcentage        NUMBER;
     my_nb_decimales       NUMBER;
   BEGIN
        -- nb decimales.
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        -- calcul
        if a_total_reste=0 then
           my_pourcentage:=0;
        else
           my_pourcentage:=a_reste / a_total_reste;
        end if;
        
        my_montant:=a_montant_budgetaire*my_pourcentage;

        -- arrondir.
        my_montant:= ROUND(my_montant,my_nb_decimales);

        -- on teste avec +1 si les arrondis ont decal� un peu par rapport au total budgetaire
            IF a_dernier THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;
            IF my_montant>a_reste OR my_montant>a_montant_budgetaire - a_somme THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;

        --if a_dernier or my_montant>a_reste then
        --   my_montant:=a_reste;
        --end if;

        RETURN my_montant;
   END;


   PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE) IS
   BEGIN
           corriger_commande_action(a_cbud_id);
           corriger_commande_analytique(a_cbud_id);
           corriger_commande_convention(a_cbud_id);
           corriger_commande_planco(a_cbud_id);

           corriger_commande_hors_marche(a_cbud_id);
           corriger_commande_marche(a_cbud_id);
   END;

   PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ACTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;

           my_somme_pourcentage:=0;
           
           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cact_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ht, my_reste);
              --my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
              --    my_commande_ctrl.cact_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ACTION SET cact_ht_saisie=my_ht, cact_tva_saisie=my_ttc-my_ht,--my_tva,
                     cact_ttc_saisie=my_ttc, cact_montant_budgetaire=my_budgetaire
               WHERE cact_id=my_commande_ctrl.cact_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cana_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ANALYTIQUE SET cana_ht_saisie=my_ht, cana_tva_saisie=my_tva,
                     cana_ttc_saisie=my_ttc, cana_montant_budgetaire=my_budgetaire
               WHERE cana_id=my_commande_ctrl.cana_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_CONVENTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.ccon_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_CONVENTION SET ccon_ht_saisie=my_ht, ccon_tva_saisie=my_tva,
                     ccon_ttc_saisie=my_ttc, ccon_montant_budgetaire=my_budgetaire
               WHERE ccon_id=my_commande_ctrl.ccon_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;
     my_reste              BOOLEAN;

     my_ht_reste           COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste          COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste          COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste   COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_somme_ttc          COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;

     my_ht                 COMMANDE_CTRL_hors_marche.chom_ht_saisie%TYPE;
     my_tva                COMMANDE_CTRL_hors_marche.chom_tva_saisie%TYPE;
     my_ttc                COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;
     my_budgetaire         COMMANDE_CTRL_hors_marche.chom_montant_budgetaire%TYPE;
     my_somme_pourcentage  COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_pourcentage        COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_commande_ctrl      COMMANDE_CTRL_hors_marche%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           select sum(chom_ttc_saisie) into my_somme_ttc from commande_ctrl_hors_marche where cbud_id=a_cbud_id;
            
           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              if my_somme_ttc=0 then 
                 my_pourcentage:=0; 
              else
                 my_pourcentage:=my_commande_ctrl.chom_ttc_saisie*100.0/my_somme_ttc;
              end if;
              my_somme_pourcentage:=my_somme_pourcentage+my_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste, my_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste, my_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste, my_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste, my_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_hors_marche SET chom_ht_saisie=my_ht, chom_tva_saisie=my_tva,
                     chom_ttc_saisie=my_ttc, chom_montant_budgetaire=my_budgetaire
               WHERE chom_id=my_commande_ctrl.chom_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           UPDATE COMMANDE_CTRL_marche SET cmar_ht_saisie=my_cde_ht, cmar_tva_saisie=my_cde_tva,
                  cmar_ttc_saisie=my_cde_ttc, cmar_montant_budgetaire=my_cde_budgetaire
               WHERE cbud_id=a_cbud_id;

        END IF;
   END;

   PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_PLANCO%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cpco_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_PLANCO SET cpco_ht_saisie=my_ht, cpco_tva_saisie=my_tva,
                     cpco_ttc_saisie=my_ttc, cpco_montant_budgetaire=my_budgetaire
               WHERE cpco_id=my_commande_ctrl.cpco_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage          COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant              COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN
   )
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE
   IS
        my_calcul                NUMBER;
   BEGIN
           IF a_somme_pourcentage>=100.0 AND a_reste=TRUE THEN
           RETURN a_montant_reste;
        END IF;

        my_calcul:=ROUND(a_pourcentage*a_montant/100, 2);

        IF my_calcul>a_montant_reste THEN
           RETURN a_montant_reste;
        END IF;

        RETURN my_calcul;
   END;

END;
/

create or replace procedure grhum.inst_patch_jefy_depense_2102 is
begin

   jefy_admin.api_application.creerfonction (250, 'DELIQEXT', 'Depense', null, 'Liquider avec mode de paiement "A extourner"', 'N', 'O', 3);
   jefy_admin.api_application.creerfonction (251, 'LIQLIEXT', 'Depense', null, 'Mandat définitif dépenses courantes', 'N', 'O', 3);
   jefy_admin.api_application.creerfonction (252, 'LIQENEXT', 'Depense', null, 'Mandat définitif masse salariale', 'N', 'O', 3);
   jefy_admin.api_application.creerfonction (253, 'LIQSENG', 'Depense', null, 'Liquider sans engagement', 'N', 'O', 3);

	commit;
    INSERT INTO JEFY_ADMIN.TYPE_ETAT (TYET_ID, TYET_LIBELLE) select 220, 'sur extourne'  from dual where not exists (select * from jefy_admin.type_etat where tyet_id=220);
    INSERT INTO JEFY_ADMIN.TYPE_ETAT (TYET_ID, TYET_LIBELLE) select  221, 'sur budget exercice' from dual  where not exists (select * from jefy_admin.type_etat where tyet_id=221);
	commit;


    insert into jefy_depense.db_version values (2102,'2102',sysdate,sysdate,null);
    commit;
end;

