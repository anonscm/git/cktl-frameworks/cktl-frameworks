CREATE OR REPLACE FORCE VIEW JEFY_ADMIN.V_UB_FOR_ORGAN
(ORG_ID, ORG_ID_UB)
AS 
select     org_id,
                 to_number(replace(sys_connect_by_path(decode(level, 1, org_id), '~'), '~')) as org_id_ub
      from       jefy_admin.organ
      where      level > 0
      start with org_niv = 2
      connect by prior org_id = org_pere;
/

CREATE OR REPLACE FORCE VIEW JEFY_ADMIN.V_CR_FOR_ORGAN
(ORG_ID, ORG_ID_CR)
AS 
select     org_id,
                 to_number(replace(sys_connect_by_path(decode(level, 1, org_id), '~'), '~')) as org_id_cr
      from       jefy_admin.organ
      where      level > 0
      start with org_niv = 3
      connect by prior org_id = org_pere;
/
grant select on jefy_admin.v_cr_for_organ to jefy_depense with grant option;
grant select on jefy_admin.v_ub_for_organ to jefy_depense with grant option;

create or replace force view jefy_depense.v_extourne_liq_suivi (dep_id_ext, dpco_id_ext, dep_id_def, dpco_id_def)
as
   select el.dep_id_n1 as dep_id_ext,
          dpcoext.dpco_id dpco_id_ext,
          eld.dep_id as dep_id_def,
          dpcodef.dpco_id as dpco_id_def
   from   jefy_depense.extourne_liq el inner join jefy_depense.depense_ctrl_planco dpcoext on (dpcoext.dep_id = el.dep_id_n1)
          left outer join jefy_depense.extourne_liq_repart elr on (elr.el_id = el.el_id)
          left outer join jefy_depense.extourne_liq_def eld on (eld.eld_id = elr.eld_id)
          left outer join jefy_depense.depense_ctrl_planco dpcodef on (dpcodef.dep_id = eld.dep_id);
grant select on jefy_depense.v_extourne_liq_suivi to maracuja with grant option;
grant select on jefy_depense.v_extourne_liq_suivi to comptefi with grant option;






create or replace force view jefy_depense.v_extourne_credits_ub (exe_ordre, org_id_ub, tcd_ordre, vec_montant_ht, vec_montant_ttc, vec_montant_bud_initial, vec_montant_bud_consomme, vec_montant_bud_disponible, vec_montant_bud_dispo_reel)
as
   select   exe_ordre,
            org_id_ub,
            tcd_ordre,
            sum (vec_montant_ht) vec_montant_ht,
            sum (vec_montant_ttc) vec_montant_ttc,
            sum (vec_montant_bud_initial) vec_montant_bud_initial,
            sum (vec_montant_bud_consomme) vec_montant_bud_consomme,
            (sum (vec_montant_bud_initial) + sum (vec_montant_bud_consomme)) as vec_montant_bud_disponible,
            (sum (vec_montant_bud_initial) + sum (vec_montant_bud_consomme)) as vec_montant_bud_dispo_reel
   from     (
-- engagements des liquidations d'extourne (négatifs)
             select   eb.exe_ordre,
                      eb.org_id,
                      ub.org_id_ub,
                      eb.tcd_ordre,
                      sum (db.dep_ht_saisie) vec_montant_ht,
                      sum (db.dep_ttc_saisie) vec_montant_ttc,
                      sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_initial,
                      0 as vec_montant_bud_consomme
             from     depense_budget db inner join extourne_liq el on (db.dep_id = dep_id_n1)
                      inner join engage_budget eb on (db.eng_id = eb.eng_id)
                      inner join jefy_admin.v_ub_for_organ ub on (ub.org_id = eb.org_id)
             where    eb.eng_ttc_saisie <> 0
             group by eb.exe_ordre, eb.org_id, ub.org_id_ub, eb.tcd_ordre
             union all
-- engagements crees via liquidation définitive sur poche (positifs)
             select   eb.exe_ordre,
                      eb.org_id,
                      ub.org_id_ub,
                      eb.tcd_ordre,
                      sum (db.dep_ht_saisie) vec_montant_ht,
                      sum (db.dep_ttc_saisie) vec_montant_ttc,
                      0 as vec_montant_bud_initial,
                      sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_consomme
             from     depense_budget db
                      inner join
                      (select eld.dep_id
                       from   extourne_liq_def eld left join extourne_liq_repart elr on (eld.eld_id = elr.eld_id)
                       where  elr.eld_id is null and eld.tyet_id = 220) x on (db.dep_id = x.dep_id)
                      inner join engage_budget eb on (db.eng_id = eb.eng_id)
                      inner join jefy_admin.v_ub_for_organ ub on (ub.org_id = eb.org_id)
             group by eb.exe_ordre, eb.org_id, ub.org_id_ub, eb.tcd_ordre)
   group by exe_ordre, org_id_ub, tcd_ordre;
/




--Montant de la poche de crédit d’extourne : Somme des montant budgétaires recalculés (ex. TTC- tva proratisée) 
--des engagements correspondant aux liquidations d'extourne (N+1) 
--et des engagements créés lors de la liquidation définitive sur poche
-- (présent dans la table extourne_liq_def mais non présents dans la table extourne_liq_repart).
create or replace force view jefy_depense.v_extourne_credits_cr (exe_ordre,
                                                                 org_id_cr,
                                                                 tcd_ordre,
                                                                 vec_montant_ht,
                                                                 vec_montant_ttc,
                                                                 vec_montant_bud_initial,
                                                                 vec_montant_bud_consomme,
                                                                 vec_montant_bud_disponible,
                                                                 vec_montant_bud_dispo_ub,
                                                                 vec_montant_bud_dispo_reel
                                                                )
as
   select cr.exe_ordre,
          cr.org_id_cr,
          cr.tcd_ordre,
          cr.vec_montant_ht,
          cr.vec_montant_ttc,
          cr.vec_montant_bud_initial,
          cr.vec_montant_bud_consomme,
          cr.vec_montant_bud_disponible,
          ub.vec_montant_bud_disponible as vec_montant_bud_dispo_ub,
          decode (sign (cr.vec_montant_bud_disponible - ub.vec_montant_bud_disponible), -1, ub.vec_montant_bud_disponible, cr.vec_montant_bud_disponible) as vec_montant_bud_dispo_reel
   from   (select   exe_ordre,
                    org_id_cr,
                    tcd_ordre,
                    sum (vec_montant_ht) vec_montant_ht,
                    sum (vec_montant_ttc) vec_montant_ttc,
                    sum (vec_montant_bud_initial) vec_montant_bud_initial,
                    sum (vec_montant_bud_consomme) vec_montant_bud_consomme,
                    (sum (vec_montant_bud_initial) + sum (vec_montant_bud_consomme)) as vec_montant_bud_disponible
           from     (
-- engagements des liquidations d'extourne (négatifs)
                     select   eb.exe_ordre,
                              eb.org_id,
                              ub.org_id_cr,
                              eb.tcd_ordre,
                              sum (db.dep_ht_saisie) vec_montant_ht,
                              sum (db.dep_ttc_saisie) vec_montant_ttc,
                              sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_initial,
                              0 as vec_montant_bud_consomme
                     from     depense_budget db inner join extourne_liq el on (db.dep_id = dep_id_n1)
                              inner join engage_budget eb on (db.eng_id = eb.eng_id)
                              inner join jefy_admin.v_cr_for_organ ub on (ub.org_id = eb.org_id)
                     where    eb.eng_ttc_saisie <> 0
                     group by eb.exe_ordre, eb.org_id, ub.org_id_cr, eb.tcd_ordre
                     union all
-- engagements crees via liquidation définitive sur poche (positifs) pour le CR
                     select   eb.exe_ordre,
                              eb.org_id,
                              ub.org_id_cr,
                              eb.tcd_ordre,
                              sum (db.dep_ht_saisie) vec_montant_ht,
                              sum (db.dep_ttc_saisie) vec_montant_ttc,
                              0 as vec_montant_bud_initial,
                              sum (jefy_depense.budget.calculer_budgetaire (eb.exe_ordre, eb.tap_id, eb.org_id, eb.eng_ht_saisie, eb.eng_ttc_saisie)) as vec_montant_bud_consomme
                     from     depense_budget db
                              inner join
                              (select eld.dep_id
                               from   extourne_liq_def eld left join extourne_liq_repart elr on (eld.eld_id = elr.eld_id)
                               where  elr.eld_id is null and eld.tyet_id = 220) x on (db.dep_id = x.dep_id)
                              inner join engage_budget eb on (db.eng_id = eb.eng_id)
                              inner join jefy_admin.v_cr_for_organ ub on (ub.org_id = eb.org_id)
                     group by eb.exe_ordre, eb.org_id, ub.org_id_cr, eb.tcd_ordre)
           group by exe_ordre, org_id_cr, tcd_ordre) cr
          inner join
          jefy_admin.v_ub_for_organ ubo on (ubo.org_id = cr.org_id_cr)
          inner join v_extourne_credits_ub ub on (ub.exe_ordre = cr.exe_ordre and ub.tcd_ordre = cr.tcd_ordre and ubo.org_id_ub = ub.org_id_ub)
          ;
/

 


