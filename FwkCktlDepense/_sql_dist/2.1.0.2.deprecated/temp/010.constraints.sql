
execute grhum.drop_constraint('jefy_depense','engage_ctrl_action','ch_engage_ctrl_action_2');
alter table jefy_depense.engage_ctrl_action add (constraint ch_engage_ctrl_action_2 check ((eact_ht_saisie>=0 and eact_montant_budgetaire>=eact_ht_saisie) or (eact_ht_saisie<0 and eact_montant_budgetaire=0)));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_action','ch_engage_ctrl_action_3');
alter table jefy_depense.engage_ctrl_action add (constraint ch_engage_ctrl_action_3 check ((eact_ttc_saisie>0 and eact_montant_budgetaire>=0 and eact_montant_budgetaire<=eact_ttc_saisie) or (eact_ttc_saisie<=0 and eact_montant_budgetaire=0)));


execute grhum.drop_constraint('jefy_depense','engage_ctrl_hors_marche','ch_engage_ctrl_hors_marche_2');
alter table jefy_depense.engage_ctrl_hors_marche add (constraint ch_engage_ctrl_hors_marche_2 check ((ehom_ht_saisie>=0 and ehom_montant_budgetaire>=ehom_ht_saisie) or (ehom_ht_saisie<0 and ehom_montant_budgetaire=0)));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_hors_marche','ch_engage_ctrl_hors_marche_3');
alter table jefy_depense.engage_ctrl_hors_marche add (constraint ch_engage_ctrl_hors_marche_3 check ((ehom_ttc_saisie>0 and ehom_montant_budgetaire>=0 and ehom_montant_budgetaire<=ehom_ttc_saisie) or (ehom_ttc_saisie<=0 and ehom_montant_budgetaire=0)));

execute grhum.drop_constraint('jefy_depense','engage_budget','ch_engage_budget_2');
alter table jefy_depense.engage_budget add (constraint ch_engage_budget_2 check ((eng_ht_saisie>=0 and eng_montant_budgetaire>=eng_ht_saisie) or (eng_ht_saisie<0 and eng_montant_budgetaire=0) or (eng_ht_saisie>0 and eng_montant_budgetaire=0)));
execute grhum.drop_constraint('jefy_depense','engage_budget','ch_engage_budget_3');
alter table jefy_depense.engage_budget add ( constraint ch_engage_budget_3 check ((eng_ttc_saisie>0 and eng_montant_budgetaire>=0 and eng_montant_budgetaire<=eng_ttc_saisie) or    (eng_ttc_saisie<=0 and eng_montant_budgetaire=0)     or (eng_ttc_saisie>0 and eng_montant_budgetaire=0)  ) );
  
execute grhum.drop_constraint('jefy_depense','depense_budget','ch_depense_budget_2');
alter table jefy_depense.depense_budget add (  constraint ch_depense_budget_2 check (dep_montant_budgetaire=0 or abs(dep_montant_budgetaire)>=abs(dep_ht_saisie))); 
 
execute grhum.drop_constraint('jefy_depense','depense_ctrl_analytique','ch_depense_ctrl_analytique_2');
alter table jefy_depense.depense_ctrl_analytique add (constraint ch_depense_ctrl_analytique_2 check ( (dana_montant_budgetaire=0) or  abs(dana_montant_budgetaire)>=abs(dana_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','depense_ctrl_convention','ch_depense_ctrl_convention_2');
alter table jefy_depense.depense_ctrl_convention add (constraint ch_depense_ctrl_convention_2 check ((dcon_montant_budgetaire=0) or abs(dcon_montant_budgetaire)>=abs(dcon_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','depense_ctrl_marche','ch_depense_ctrl_marche_2');
alter table jefy_depense.depense_ctrl_marche add (constraint ch_depense_ctrl_marche_2 check ((dmar_montant_budgetaire=0) or abs(dmar_montant_budgetaire)>=abs(dmar_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','depense_ctrl_planco','ch_depense_ctrl_planco_2');
alter table jefy_depense.depense_ctrl_planco add (  constraint ch_depense_ctrl_planco_2 check ( (dpco_montant_budgetaire=0) or (abs(dpco_montant_budgetaire)>=abs(dpco_ht_saisie))));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_analytique','ch_pdepense_ctrl_analytique_2');
alter table jefy_depense.pdepense_ctrl_analytique add (constraint ch_pdepense_ctrl_analytique_2 check ( (pdana_montant_budgetaire=0) or  abs(pdana_montant_budgetaire)>=abs(pdana_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_convention','ch_pdepense_ctrl_convention_2');
alter table jefy_depense.pdepense_ctrl_convention add (constraint ch_pdepense_ctrl_convention_2 check ((pdcon_montant_budgetaire=0) or abs(pdcon_montant_budgetaire)>=abs(pdcon_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_marche','ch_pdepense_ctrl_marche_2');
alter table jefy_depense.pdepense_ctrl_marche add (constraint ch_pdepense_ctrl_marche_2 check ((pdmar_montant_budgetaire=0) or abs(pdmar_montant_budgetaire)>=abs(pdmar_ht_saisie)));

execute grhum.drop_constraint('jefy_depense','pdepense_ctrl_planco','ch_pdepense_ctrl_planco_2');
alter table jefy_depense.pdepense_ctrl_planco add (  constraint ch_pdepense_ctrl_planco_2 check ( (pdpco_montant_budgetaire=0) or (abs(pdpco_montant_budgetaire)>=abs(pdpco_ht_saisie))));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_analytique','ch_engage_ctrl_analytique_2');
alter table jefy_depense.engage_ctrl_analytique add (constraint ch_engage_ctrl_analytique_2 check ((eana_montant_budgetaire=0) or (eana_ht_saisie>=0 and eana_montant_budgetaire>=eana_ht_saisie) ));
execute grhum.drop_constraint('jefy_depense','engage_ctrl_analytique','ch_engage_ctrl_analytique_3');
alter table jefy_depense.engage_ctrl_analytique add (constraint ch_engage_ctrl_analytique_3 check ((eana_montant_budgetaire=0) or (eana_ttc_saisie>0 and eana_montant_budgetaire>=0 and eana_montant_budgetaire<=eana_ttc_saisie) ));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_convention','ch_engage_ctrl_convention_2');
alter table jefy_depense.engage_ctrl_convention add (constraint ch_engage_ctrl_convention_2 check ( (econ_montant_budgetaire=0)  or (econ_ht_saisie>=0 and econ_montant_budgetaire>=econ_ht_saisie)      ));
execute grhum.drop_constraint('jefy_depense','engage_ctrl_convention','ch_engage_ctrl_convention_3');
alter table jefy_depense.engage_ctrl_convention add (constraint ch_engage_ctrl_convention_3 check ( (econ_montant_budgetaire=0) or (econ_ttc_saisie>0 and econ_montant_budgetaire>=0 and econ_montant_budgetaire<=econ_ttc_saisie) ));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_marche','ch_engage_ctrl_marche_2');
alter table jefy_depense.engage_ctrl_marche add (  constraint ch_engage_ctrl_marche_2 check (( emar_montant_budgetaire=0) or  (emar_ht_saisie>=0 and emar_montant_budgetaire>=emar_ht_saisie) ));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_marche','ch_engage_ctrl_marche_3');
alter table jefy_depense.engage_ctrl_marche add (  constraint ch_engage_ctrl_marche_3 check ((emar_montant_budgetaire=0) or (emar_ttc_saisie>0 and emar_montant_budgetaire>=0 and emar_montant_budgetaire<=emar_ttc_saisie)));

execute grhum.drop_constraint('jefy_depense','engage_ctrl_planco','ch_engage_ctrl_planco_2');
alter table jefy_depense.engage_ctrl_planco add (constraint ch_engage_ctrl_planco_2 check ((epco_montant_budgetaire=0) or   (epco_montant_budgetaire>0 and epco_montant_budgetaire>=epco_ht_saisie)  ));
execute grhum.drop_constraint('jefy_depense','engage_ctrl_planco','ch_engage_ctrl_planco_3');
alter table jefy_depense.engage_ctrl_planco add (constraint ch_engage_ctrl_planco_3 check ( (epco_montant_budgetaire=0) or (epco_ttc_saisie>0 and epco_montant_budgetaire>=0 and epco_montant_budgetaire<=epco_ttc_saisie) ));

  
  
