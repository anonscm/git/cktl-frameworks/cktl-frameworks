SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.4.0
-- Date de publication :  21/03/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Corrections diverses, notamment pour le service facturier
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;





CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Apres_Commun IS

   PROCEDURE hors_marche (
      a_typa_id         ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE,
      a_ce_ordre        ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE,
      a_fou_ordre       ENGAGE_BUDGET.fou_ordre%TYPE
   ) IS
      my_exe_ordre      ENGAGE_CTRL_HORS_MARCHE.exe_ordre%TYPE;
      my_ehom_ht_reste  ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
      my_dhom_ht_saisie DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
      my_ce_controle    jefy_marches.code_exer.ce_controle%TYPE;
      my_cm_code        jefy_marches.code_marche.cm_code%TYPE;
   BEGIN
        SELECT NVL(ce_controle,0), m.cm_code, e.exe_ordre INTO my_ce_controle, my_cm_code, my_exe_ordre
          FROM jefy_marches.code_exer e, jefy_marches.code_marche m WHERE ce_ordre=a_ce_ordre AND e.cm_ordre=m.cm_ordre;

          -- ajout sans achat
        IF a_typa_id<>Get_Type_Achat('MONOPOLE') AND a_typa_id<>Get_Type_Achat('3CMP')
            AND a_typa_id<>Get_Type_Achat('SANS ACHAT') THEN
            jefy_marches.creerCodeMarcheFour(a_fou_ordre, a_ce_ordre, 'jefy_depense');

              SELECT NVL(SUM(ehom_ht_reste),0) INTO my_ehom_ht_reste FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE ce_ordre=a_ce_ordre AND typa_id=a_typa_id;
              SELECT NVL(SUM(dhom_ht_saisie),0) INTO my_dhom_ht_saisie FROM DEPENSE_CTRL_HORS_MARCHE
              WHERE ce_ordre=a_ce_ordre  AND typa_id=a_typa_id AND (dhom_ht_saisie>0 OR dep_id IN
                (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')));

            IF my_ce_controle=0 THEN
              my_ce_controle:=150000;
           END IF;

           IF my_ehom_ht_reste+my_dhom_ht_saisie>my_ce_controle THEN
              RAISE_APPLICATION_ERROR(-20001,'Le seuil '||my_ce_controle||' ne peut etre depasse pour le code '||my_cm_code);
           END IF;
        END IF;
   END;

   PROCEDURE marche (
       a_att_ordre      ENGAGE_CTRL_MARCHE.att_ordre%TYPE,
       a_fou_ordre        ENGAGE_BUDGET.fou_ordre%TYPE,
       a_exe_ordre        ENGAGE_CTRL_MARCHE.exe_ordre%TYPE
   ) IS
       my_nb               INTEGER;
       my_att_execution    v_attribution_execution.aee_execution%TYPE;
       my_emar_ht_reste    ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_dmar_ht_saisie   DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_montant_consomme ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_att_ht           jefy_marches.attribution.att_ht%TYPE;
       my_lot_ordre           jefy_marches.lot.lot_ordre%TYPE;
       my_st_montant       jefy_marches.sous_traitant.st_montant%TYPE;
       my_montant           ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
   BEGIN
        select count(*) into my_nb from jefy_marches.attribution where att_ordre=a_att_ordre;
        if my_nb<>1 then
           RAISE_APPLICATION_ERROR(-20001,'L''attribution n''existe pas (att_ordre='||a_att_ordre||')');
        end if;

-- DEBUT MODIF THIERRY 17/07/2009
        -- on verifie le total de la conso du lot
        select lot_ordre into my_lot_ordre from jefy_marches.attribution where att_ordre=a_att_ordre;
        
        SELECT NVL(SUM(aee_execution),0) INTO my_att_execution FROM v_attribution_execution
           WHERE att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND exe_ordre<a_exe_ordre;

        SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

        SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
           FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND (m.dmar_ht_saisie>0 OR m.dep_id IN
           (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
           AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;
        
        SELECT NVL(lot_ht,0) INTO my_att_ht FROM jefy_marches.lot
		 WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

        if my_att_ht<my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie then
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     to_char(my_att_ht)||' et avec cette commande la consommation serait de '||to_char(my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie)); 
            /*RAISE_APPLICATION_ERROR(-20001,'Le montant du lot est de '||
			     to_char(my_att_ht)||', et on a deja consomme '||to_char(my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie));*/
        end if;

-- FIN MODIF THIERRY 17/07/2009

         
        -- verifier le dispo pour ce lot/attribution.
        -- recuperation de l'execution de l'attribution pour les annees anterieures --
        SELECT NVL(SUM(aee_execution),0) INTO my_att_execution
           FROM v_attribution_execution
           -- EGE 07/02/2007 where att_ordre=a_att_ordre and fou_ordre=a_fou_ordre and exe_ordre<=a_exe_ordre;
           WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre AND exe_ordre<a_exe_ordre;

           SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
           WHERE m.att_ordre=a_att_ordre AND e.fou_ordre=a_fou_ordre AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

           SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
           FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
           WHERE m.att_ordre=a_att_ordre AND (m.dmar_ht_saisie>0 OR m.dep_id IN
           (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
           AND e.fou_ordre=a_fou_ordre AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;


        my_montant_consomme:=my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie;

        SELECT COUNT(*) INTO my_nb FROM jefy_marches.attribution WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;
        IF (my_nb>0) THEN
	 	   SELECT NVL(att_ht,0), lot_ordre INTO my_att_ht, my_lot_ordre
		       FROM jefy_marches.attribution WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

		   IF my_att_ht=0 THEN
		      SELECT COUNT(*) INTO my_nb FROM jefy_marches.lot
			     WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

		      IF my_nb=1 THEN
		      	 SELECT NVL(lot_ht,0) INTO my_att_ht FROM jefy_marches.lot
			        WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;
                    
                 -- on recherche les executions de ttes les attributions du lot
                 SELECT NVL(SUM(aee_execution),0) INTO my_att_execution
                    FROM v_attribution_execution
		            WHERE att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND exe_ordre<a_exe_ordre;

   		         SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
		            WHERE m.att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

   		         SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
		            FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
		            WHERE m.att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND (m.dmar_ht_saisie>0 OR m.dep_id IN
		              (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
		              AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;

		         my_montant_consomme:=my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie;                    
                    
			  END IF;

		   END IF;

	       -- enlevons la somme allou�e au sous-traitants
		   SELECT NVL(SUM(st_montant),0) INTO my_st_montant FROM jefy_marches.sous_traitant WHERE att_ordre=a_att_ordre;

		   my_montant:=grhum.En_Nombre(my_att_ht)-grhum.En_Nombre(my_st_montant);
		   IF my_montant<my_montant_consomme THEN
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     my_montant||' et avec cette commande la consommation serait de '||my_montant_consomme); 
		    /*RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     my_montant||', et on a deja consomme '||my_montant_consomme);*/
		   END IF;
	    ELSE
	 	   -- le fournisseur est un sous-traitant, ou alors l'attribution n'existe pas
	 	   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sous_traitant WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

		   IF (my_nb>0) THEN
		 	  SELECT st_montant INTO my_st_montant FROM jefy_marches.sous_traitant
			     WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

			  IF (my_st_montant<my_montant_consomme) THEN
                 RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			        my_st_montant||' et avec cette commande la consommation serait de '||my_montant_consomme); 
  		         /*RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			        my_st_montant||', et on a deja consomme '||my_montant_consomme);*/
		      END IF;
           else
  		         RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur ne peut etre utilise pour cette attribution');           
	       END IF;
        END IF;
     END;

    procedure analytique (
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_can_id 		ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE
    ) is
      my_code_analytique jefy_admin.code_analytique%rowtype;
      my_nb              integer;
      my_montant         V_CODE_ANALYTIQUE_SUIVI.montant_budgetaire%type;
    begin
      select count(*) into my_nb from jefy_admin.code_analytique where can_id=a_can_id;
      if my_nb!=1 then
         RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas utilisable  ('||INDICATION_ERREUR.analytique(a_can_id)||')');
      end if;
      
      select * into my_code_analytique from jefy_admin.code_analytique where can_id=a_can_id;
      if my_code_analytique.can_montant is not null and my_code_analytique.can_montant_depassement=15 then
         select nvl(montant_budgetaire,0) into my_montant from V_CODE_ANALYTIQUE_SUIVI where can_id=a_can_id;
         if my_montant>my_code_analytique.can_montant then
            RAISE_APPLICATION_ERROR(-20001, 'Le montant max pour ce code analytique est atteint  ('||INDICATION_ERREUR.analytique(a_can_id)||
              ', '||my_montant||'>'||my_code_analytique.can_montant||')');
         end if;
      end if;
    end;

	PROCEDURE convention(
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_conv_ordre		ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE,
	   a_tcd_ordre      ENGAGE_BUDGET.tcd_ordre%TYPE,
	   a_org_id      ENGAGE_BUDGET.org_id%TYPE
    )
	 IS
	 BEGIN
	 	  accords.suivi_exec_depense.verif_disponible(a_exe_ordre, a_conv_ordre, a_tcd_ordre, a_org_id);
	 END;
	 
	 PROCEDURE planco (
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_pco_num		ENGAGE_CTRL_PLANCO.pco_num%TYPE,
	   a_org_id      ENGAGE_BUDGET.org_id%TYPE
	 )
	 IS
	   my_message   VARCHAR2(1000);
	 BEGIN
	     jefy_budget.budget_execution.verif_depense_nature(a_exe_ordre, a_org_id, a_pco_num, 0, my_message);
	 END;

 END;
/

CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Apres_Liquide  IS

-- procedure de traitement a faire apres les insert dans les depense.

PROCEDURE Budget       (a_dep_id      DEPENSE_BUDGET.dep_id%TYPE);
PROCEDURE action       (a_dact_id     DEPENSE_CTRL_ACTION.dact_id%TYPE);
PROCEDURE analytique   (a_dana_id     DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE);
PROCEDURE convention   (a_dcon_id     DEPENSE_CTRL_CONVENTION.dcon_id%TYPE);
PROCEDURE hors_marche  (a_dhom_id     DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE);
PROCEDURE marche       (a_dmar_id     DEPENSE_CTRL_MARCHE.dmar_id%TYPE);
PROCEDURE planco       (a_dpco_id     DEPENSE_CTRL_PLANCO.dpco_id%TYPE,  a_chaine VARCHAR2);

PROCEDURE del_depense_budget   (a_eng_id     ENGAGE_BUDGET.eng_id%TYPE, a_dep_id depense_budget.dep_id%TYPE);
PROCEDURE del_depense_papier   (a_dpp_id     DEPENSE_PAPIER.dpp_id%TYPE);

END;
/



CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Apres_Liquide
IS

   PROCEDURE Budget (a_dep_id      DEPENSE_BUDGET.dep_id%TYPE) IS
       my_nb      INTEGER;
	   my_comm_id COMMANDE.comm_id%TYPE;
   BEGIN
		-- verification et correction de l'etat de la commande.
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT ce, DEPENSE_BUDGET d
		  WHERE ce.eng_id=d.eng_id AND d.dep_id=a_dep_id;

		IF my_nb=1 THEN
          SELECT comm_id INTO my_comm_id FROM COMMANDE_ENGAGEMENT ce, DEPENSE_BUDGET d
		    WHERE ce.eng_id=d.eng_id AND d.dep_id=a_dep_id;
		  Corriger.corriger_etats_commande(my_comm_id);
		END IF;
   END;

   PROCEDURE action (a_dact_id     DEPENSE_CTRL_ACTION.dact_id%TYPE) IS
       i INTEGER;
   BEGIN
        -- a completer --
		i:=1;
   END;

   PROCEDURE analytique (a_dana_id     DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE) IS
       my_exe_ordre    DEPENSE_ctrl_analytique.exe_ordre%TYPE;
       my_can_id       DEPENSE_ctrl_analytique.can_id%TYPE;
   BEGIN
        select exe_ordre, can_id into my_exe_ordre, my_can_id from DEPENSE_ctrl_analytique where dana_id=a_dana_id;
        apres_commun.analytique(my_exe_ordre, my_can_id);
   END;

   PROCEDURE convention (a_dcon_id     DEPENSE_CTRL_CONVENTION.dcon_id%TYPE) IS
       my_exe_ordre    ENGAGE_BUDGET.exe_ordre%TYPE;
       my_org_id       ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre    ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_conv_ordre   ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE;
   BEGIN
		SELECT e.exe_ordre, e.org_id, e.tcd_ordre, c.conv_ordre INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre
		   FROM ENGAGE_BUDGET e, DEPENSE_BUDGET d, DEPENSE_CTRL_CONVENTION c
		   WHERE d.eng_id=e.eng_id AND d.dep_id=c.dep_id AND dcon_id=a_dcon_id;
		Apres_Commun.convention(my_exe_ordre, my_conv_ordre, my_tcd_ordre,my_org_id);
   END;

   PROCEDURE hors_marche (a_dhom_id     DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE) IS
      my_typa_id        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
      my_ce_ordre       DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
      my_fou_ordre      ENGAGE_BUDGET.fou_ordre%TYPE;
   BEGIN
   		SELECT h.typa_id, h.ce_ordre, e.fou_ordre INTO my_typa_id, my_ce_ordre, my_fou_ordre
		FROM DEPENSE_CTRL_HORS_MARCHE h, DEPENSE_BUDGET d, ENGAGE_BUDGET e
		WHERE h.dhom_id=a_dhom_id AND h.dep_id=d.dep_id AND d.eng_id=e.eng_id;

		Apres_Commun.hors_marche (my_typa_id, my_ce_ordre, my_fou_ordre);
   END;

   PROCEDURE marche (a_dmar_id     DEPENSE_CTRL_MARCHE.dmar_id%TYPE) IS
      my_att_ordre      DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
      my_exe_ordre      DEPENSE_CTRL_MARCHE.exe_ordre%TYPE;
      my_fou_ordre      ENGAGE_BUDGET.fou_ordre%TYPE;
   BEGIN
   		SELECT m.att_ordre, m.exe_ordre, e.fou_ordre INTO my_att_ordre, my_exe_ordre, my_fou_ordre
		FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
		WHERE m.dmar_id=a_dmar_id AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;

		Apres_Commun.marche(my_att_ordre, my_fou_ordre, my_exe_ordre);
   END;

   PROCEDURE planco (a_dpco_id     DEPENSE_CTRL_PLANCO.dpco_id%TYPE,  a_chaine VARCHAR2
   ) IS
   	   my_invc_id INTEGER;
	   my_montant NUMBER(12,2);
	   my_somme NUMBER(12,2);
	   my_dpco_montant_budgetaire DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_chaine	  VARCHAR2(30000);
	   my_exe_ordre	DEPENSE_CTRL_PLANCO.exe_ordre%TYPE;
	   my_pco_num	DEPENSE_CTRL_PLANCO.pco_num%TYPE;
	         my_org_id      ENGAGE_BUDGET.org_id%TYPE;
	   my_par_value  PARAMETRE.par_value%TYPE;
	   my_nb INTEGER;
   BEGIN
           -- inventaire
		   
           my_chaine:=a_chaine;
		   my_somme:=0;
   		LOOP
      		IF a_chaine IS  NULL OR LENGTH(a_chaine)=0 OR SUBSTR(my_chaine,1,1)='|' THEN EXIT; END IF;

			    -- on recupere la cle de l'inventaire.
				SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'|')-1)) INTO my_invc_id FROM dual;
				my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'|')+1,LENGTH(my_chaine));
				-- on recupere le montant ht.
				SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'|')-1)) INTO my_montant FROM dual;
				my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'|')+1,LENGTH(my_chaine));

				--SELECT dpco_montant_budgetaire INTO my_montant FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=a_dpco_id;

				my_somme:=my_somme+my_montant;

				jefy_inventaire.api_corossol.creer_lien_inv_dep (a_dpco_id, my_invc_id, my_montant) ;
	   END LOOP;

	   SELECT dp.dpco_montant_budgetaire, dp.exe_ordre, dp.pco_num, e.org_id 
	       INTO my_dpco_montant_budgetaire, my_exe_ordre, my_pco_num, my_org_id
	       FROM DEPENSE_CTRL_PLANCO dp, ENGAGE_BUDGET e, DEPENSE_BUDGET d
		   WHERE dp.dpco_id=a_dpco_id AND dp.dep_id=d.dep_id AND d.eng_id=e.eng_id;
		   
	   SELECT par_value INTO my_par_value FROM PARAMETRE WHERE par_key='INVENTAIRE_OBLIGATOIRE' AND exe_ordre=my_exe_ordre;

	   IF my_par_value='OUI' THEN
	   	  SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_inventoriable WHERE pco_num=my_pco_num AND exe_ordre=my_exe_ordre;
		  IF my_nb>0 THEN
		        IF my_somme<>my_dpco_montant_budgetaire THEN
  		           RAISE_APPLICATION_ERROR(-20001,'le montant budgetaire des inventaires n''est pas egal au montant budgetaire de la depense '||
                      'my_somme='||my_somme||', my_dpco_montant_budgetaire='||my_dpco_montant_budgetaire||', chaine='||a_chaine);
				END IF;
		  END IF;

	   END IF;
	   
	   -- partie commune avec l'engagement
		Apres_Commun.planco(my_exe_ordre, my_pco_num, my_org_id);
   END;

   PROCEDURE del_depense_budget (a_eng_id     ENGAGE_BUDGET.eng_id%TYPE, a_dep_id depense_budget.dep_id%TYPE) IS
       my_nb      INTEGER;
	   my_comm_id COMMANDE.comm_id%TYPE;
	   my_eng_id ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
		-- verification et correction de l'etat de la commande.
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT ce  WHERE ce.eng_id=a_eng_id;

		IF my_nb=1 THEN
          SELECT comm_id INTO my_comm_id FROM COMMANDE_ENGAGEMENT ce WHERE ce.eng_id=a_eng_id;
		  Corriger.corriger_etats_commande(my_comm_id);
		END IF;

		-- informe jefy_mission.
		jefy_mission.kiwi_paiement.del_liquidation(a_eng_id, a_dep_id);
        
   END;

   PROCEDURE del_depense_papier (a_dpp_id     DEPENSE_PAPIER.dpp_id%TYPE) IS
       i INTEGER;
   BEGIN
        -- a completer --
		i:=1;
   END;
END;
/


CREATE OR REPLACE PACKAGE JEFY_DEPENSE.PRE_LIQUIDER IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_pdepense (
      a_pdep_id IN OUT		PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_exe_ordre			PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_dpp_id			    PDEPENSE_BUDGET.dpp_id%TYPE,
	  a_eng_id				PDEPENSE_BUDGET.eng_id%TYPE,
	  a_pdep_ht_saisie		PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
	  a_pdep_ttc_saisie		PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
	  a_tap_id				PDEPENSE_BUDGET.tap_id%TYPE,
	  a_utl_ordre			PDEPENSE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2);

PROCEDURE del_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE del_pdepense_budgets (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE liquider_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE verifier_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE);

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

PROCEDURE ins_pdepense_budget (
      a_pdep_id IN OUT		PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_exe_ordre			PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_dpp_id			    PDEPENSE_BUDGET.dpp_id%TYPE,
	  a_eng_id				PDEPENSE_BUDGET.eng_id%TYPE,
	  a_pdep_ht_saisie		PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
	  a_pdep_ttc_saisie		PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
	  a_tap_id				PDEPENSE_BUDGET.tap_id%TYPE,
	  a_utl_ordre			PDEPENSE_BUDGET.utl_ordre%TYPE);

PROCEDURE ins_pdepense_ctrl_action (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_analytique (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_convention (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_hors_marche (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_marche (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_planco (
      a_exe_ordre           PDEPENSE_BUDGET.exe_ordre%TYPE,
	  a_pdep_id		        PDEPENSE_BUDGET.pdep_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_pdepense_ctrl_planco_inve (
	  a_pdpco_id	        PDEPENSE_CTRL_PLANCO.pdpco_id%TYPE,
	  a_chaine		        VARCHAR2);

FUNCTION get_Chaine_Action ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_analytique ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_convention ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_hors_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
FUNCTION get_Chaine_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2;
   
END;
/



CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.PRE_LIQUIDER
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE ins_pdepense (
      a_pdep_id IN OUT       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_exe_ordre            PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               PDEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               PDEPENSE_BUDGET.eng_id%TYPE,
      a_pdep_ht_saisie       PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
      a_pdep_ttc_saisie      PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
      a_tap_id               PDEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            PDEPENSE_BUDGET.utl_ordre%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_nb_decimales         NUMBER;
     my_pdep_ht_saisie       PDEPENSE_BUDGET.pdep_ht_saisie%TYPE;
     my_pdep_ttc_saisie      PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE;
   BEGIN
       IF a_pdep_ttc_saisie<>0 THEN
       
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_pdep_ht_saisie:=round(a_pdep_ht_saisie, my_nb_decimales);
        my_pdep_ttc_saisie:=round(a_pdep_ttc_saisie, my_nb_decimales);
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        -- Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

        -- lancement des differentes procedures d'insertion des tables de depense.
        ins_pdepense_budget(a_pdep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_pdep_ht_saisie, my_pdep_ttc_saisie,
            a_tap_id, a_utl_ordre);

        -- repartitions
        ins_pdepense_ctrl_planco(a_exe_ordre, a_pdep_id, a_chaine_planco);
        ins_pdepense_ctrl_action(a_exe_ordre, a_pdep_id, a_chaine_action);
        ins_pdepense_ctrl_analytique(a_exe_ordre, a_pdep_id, a_chaine_analytique);
        ins_pdepense_ctrl_convention(a_exe_ordre, a_pdep_id, a_chaine_convention);
        ins_pdepense_ctrl_hors_marche(a_exe_ordre, a_pdep_id, a_chaine_hors_marche);
        ins_pdepense_ctrl_marche(a_exe_ordre, a_pdep_id, a_chaine_marche);

       END IF;
   END;

   PROCEDURE del_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La pre-liquidation n''existe pas ou est deja annule (pdep_id:'||a_pdep_id||')');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

        DELETE FROM PDEPENSE_CTRL_ACTION WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_ANALYTIQUE WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_CONVENTION WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_HORS_MARCHE WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_MARCHE WHERE pdep_id=a_pdep_id;
        DELETE FROM PDEPENSE_CTRL_PLANCO_inve WHERE pdpco_id in (select pdpco_id FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id=a_pdep_id);
        DELETE FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id=a_pdep_id;

        DELETE FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
   END;

   PROCEDURE del_pdepense_budgets (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
   BEGIN
        DELETE FROM PDEPENSE_CTRL_ACTION WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_ANALYTIQUE WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_CONVENTION WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_HORS_MARCHE WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_MARCHE WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
        DELETE FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);

        DELETE FROM PDEPENSE_BUDGET WHERE pdep_id in (select pdep_id from pdepense_budget where dpp_id=a_dpp_id);
   END;

   PROCEDURE liquider_pdepense_budget (
      a_pdep_id             PDEPENSE_BUDGET.pdep_id%TYPE,
      a_utl_ordre           PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
      my_pdpco_id               pdepense_ctrl_planco.pdpco_id%type;
      my_dep_id                 depense_budget.dep_id%type;
      
      my_pdepense_budget        pdepense_budget%rowtype;
      my_pdepense_ctrl_planco   pdepense_ctrl_planco%rowtype;
      my_pdepense_ctrl_planco_inve  pdepense_ctrl_planco_inve%rowtype;

      my_chaine_action          VARCHAR2(30000);
      my_chaine_analytique      VARCHAR2(30000);
      my_chaine_convention      VARCHAR2(30000);
      my_chaine_hors_marche     VARCHAR2(30000);
      my_chaine_marche          VARCHAR2(30000);
      my_chaine_planco          VARCHAR2(30000);
      my_chaine_inventaire      VARCHAR2(30000);
      
      CURSOR liste IS SELECT * FROM PDEPENSE_CTRL_PLANCO WHERE pdep_id=a_pdep_id;
      CURSOR liste_inventaire IS SELECT * FROM PDEPENSE_CTRL_PLANCO_INVE WHERE pdpco_id=my_pdpco_id;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La pre-liquidation n''existe pas ou est annule (pdep_id:'||a_pdep_id||')');
        END IF;

        SELECT * INTO my_pdepense_budget FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

        -- verification que la pre-liquidation est complete
        verifier_pdepense_budget(a_pdep_id);
        
        -- d?coupe la pr?-liquidation en autant de liquidations qu'il y a de pdepense_ctrl_planco (car 1 liquidation = 1 pco_num)
        OPEN liste();
        LOOP
           FETCH liste INTO my_pdepense_ctrl_planco;
           EXIT WHEN liste%NOTFOUND;

           -- on vide les chaines
           my_chaine_action:='';
           my_chaine_analytique:='';
           my_chaine_convention:='';
           my_chaine_hors_marche:='';
           my_chaine_marche:='';
           my_chaine_planco:='';
           my_chaine_inventaire:='';
           
           -- action
           my_chaine_action:=get_Chaine_Action(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- analytique
           my_chaine_analytique:=get_Chaine_analytique(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- convention
           my_chaine_convention:=get_Chaine_convention(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- hors_marche
           my_chaine_hors_marche:=get_Chaine_hors_marche(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- marche
           my_chaine_marche:=get_Chaine_marche(my_pdepense_ctrl_planco.pdpco_ht_saisie,
              my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_ctrl_planco.pdep_id);

           -- planco
           my_chaine_planco:=my_pdepense_ctrl_planco.pco_num||'$'||my_pdepense_ctrl_planco.pdpco_ht_saisie||'$'||
                my_pdepense_ctrl_planco.pdpco_ttc_saisie||'$'||my_pdepense_ctrl_planco.ecd_ordre||'$';

           my_pdpco_id:=my_pdepense_ctrl_planco.pdpco_id;
           
           -- chaine inventaire si il y en a
           select count(*) into my_nb from pdepense_ctrl_planco_inve where pdpco_id=my_pdepense_ctrl_planco.pdpco_id;
           if my_nb>0 then
              OPEN liste_inventaire();
              LOOP
                 FETCH liste_inventaire INTO my_pdepense_ctrl_planco_inve;
                 EXIT WHEN liste_inventaire%NOTFOUND;

                 my_chaine_inventaire:=my_chaine_inventaire||my_pdepense_ctrl_planco_inve.invc_id||'|'||
                     my_pdepense_ctrl_planco_inve.pdpin_montant_budgetaire||'|';
              END LOOP;
              CLOSE liste_inventaire;

              my_chaine_inventaire:=my_chaine_inventaire||'|';

           end if;

           my_chaine_planco:=my_chaine_planco||my_chaine_inventaire||'$$';

           -- on liquide
           my_dep_id:=null;
           
           liquider.ins_depense(my_dep_id, my_pdepense_budget.exe_ordre, my_pdepense_budget.dpp_id, my_pdepense_budget.eng_id, 
               my_pdepense_ctrl_planco.pdpco_ht_saisie, my_pdepense_ctrl_planco.pdpco_ttc_saisie, my_pdepense_budget.tap_id,
               my_pdepense_budget.utl_ordre, null, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche,
               my_chaine_marche, my_chaine_planco);
        END LOOP;
        CLOSE liste;

        -- liquidation effectuee, on supprime la pre-liquidation
        del_pdepense_budget(a_pdep_id, a_utl_ordre);
   END;

   PROCEDURE verifier_pdepense_budget (
      a_pdep_id             pdepense_budget.pdep_id%type
   ) IS
      my_nb                     integer;
      my_nbbis                  integer;
      my_pdepense_budget        pdepense_budget%rowtype;
      my_depense_papier         depense_papier%rowtype;
      my_mode_paiement          maracuja.mode_paiement%rowtype;
      my_ht                     pdepense_budget.pdep_ht_saisie%type;
      my_ttc                    pdepense_budget.pdep_ttc_saisie%type;
   BEGIN
        select count(*) into my_nb from pdepense_budget where pdep_id=a_pdep_id;
        if my_nb=0 then
           raise_application_error(-20001, 'La pre-liquidation n''existe pas ou est annule (pdep_id:'||a_pdep_id||')');
        end if;

        select * into my_pdepense_budget from pdepense_budget where pdep_id=a_pdep_id;
        select * into my_depense_papier from depense_papier where dpp_id=my_pdepense_budget.dpp_id;
        
        if my_depense_papier.dpp_date_service_fait is null then
           raise_application_error(-20001, 'La depense n''a pas de date de service fait (dpp_id:'||my_pdepense_budget.dpp_id||')');
        end if;
        if my_depense_papier.mod_ordre is null then
           raise_application_error(-20001, 'La depense n''a pas de mode de paiement (dpp_id:'||my_pdepense_budget.dpp_id||')');
        end if;
        
        select * into my_mode_paiement from maracuja.mode_paiement where mod_ordre=my_depense_papier.mod_ordre;
        if my_mode_paiement.mod_dom='VIREMENT' and my_depense_papier.rib_ordre is null then 
           raise_application_error(-20001, 'Pour ce mode de paiement il faut un RIB (dpp_id:'||my_pdepense_budget.dpp_id||')');
        end if;
        

        if my_pdepense_budget.pdep_ht_saisie<0 or my_pdepense_budget.pdep_ttc_saisie<0 then
           raise_application_error(-20001, 'La pre-liquidation doit avoir des montants positifs (pdep_id:'||a_pdep_id||')');
        end if;
         
        -- action
        select count(*) into my_nb from pdepense_ctrl_action where pdep_id=a_pdep_id;
        select nvl(sum(pdact_ht_saisie),0), nvl(sum(pdact_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_action where pdep_id=a_pdep_id;
        if my_nb=0 or my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc then
           raise_application_error(-20001, 'La repartition par actions de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- analytique
        select count(*) into my_nb from pdepense_ctrl_analytique where pdep_id=a_pdep_id;
        select nvl(sum(pdana_ht_saisie),0), nvl(sum(pdana_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_analytique where pdep_id=a_pdep_id;
        if my_pdepense_budget.pdep_ht_saisie<my_ht or my_pdepense_budget.pdep_ttc_saisie<my_ttc then
           raise_application_error(-20001, 'La repartition par codes analytiques de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- convention
        select count(*) into my_nb from pdepense_ctrl_convention where pdep_id=a_pdep_id;
        select nvl(sum(pdcon_ht_saisie),0), nvl(sum(pdcon_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_convention where pdep_id=a_pdep_id;
        if my_pdepense_budget.pdep_ht_saisie<my_ht or my_pdepense_budget.pdep_ttc_saisie<my_ttc then
           raise_application_error(-20001, 'La repartition par conventions de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- hors marche
        select count(*) into my_nb from pdepense_ctrl_hors_marche where pdep_id=a_pdep_id;
        select nvl(sum(pdhom_ht_saisie),0), nvl(sum(pdhom_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_hors_marche where pdep_id=a_pdep_id;
        if my_nb>0 and (my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc) then
           raise_application_error(-20001, 'La repartition par codes de nomenclature de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;
        
        -- marche
        select count(*) into my_nbbis from pdepense_ctrl_marche where pdep_id=a_pdep_id;
        select nvl(sum(pdmar_ht_saisie),0), nvl(sum(pdmar_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_marche where pdep_id=a_pdep_id;
        if my_nbbis>0 and (my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc) then
           raise_application_error(-20001, 'La repartition par marches de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;

        -- marche ou hors marche
        if (my_nb=0 and my_nbbis=0) or (my_nb<>0 and my_nbbis<>0) then
           raise_application_error(-20001, 'La pre-liquidation doit etre ''marche'' ou ''hors marche'' (pdep_id:'||a_pdep_id||')');
        end if;
        
        -- planco
        select count(*) into my_nb from pdepense_ctrl_planco where pdep_id=a_pdep_id;
        select nvl(sum(pdpco_ht_saisie),0), nvl(sum(pdpco_ttc_saisie),0) into my_ht, my_ttc from pdepense_ctrl_planco where pdep_id=a_pdep_id;
        if my_nb=0 or my_pdepense_budget.pdep_ht_saisie<>my_ht or my_pdepense_budget.pdep_ttc_saisie<>my_ttc then
           raise_application_error(-20001, 'La repartition par imputations de la pre-liquidation n''est pas complete (pdep_id:'||a_pdep_id||')');
        end if;
        
   END ;


--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

  
   PROCEDURE ins_pdepense_budget (
      a_pdep_id IN OUT       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_exe_ordre            PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               PDEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               PDEPENSE_BUDGET.eng_id%TYPE,
      a_pdep_ht_saisie       PDEPENSE_BUDGET.pdep_ht_saisie%TYPE,
      a_pdep_ttc_saisie      PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE,
      a_tap_id               PDEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            PDEPENSE_BUDGET.utl_ordre%TYPE
   ) IS
       my_pdep_ht_saisie     PDEPENSE_BUDGET.pdep_ht_saisie%TYPE;
       my_pdep_ttc_saisie    PDEPENSE_BUDGET.pdep_ttc_saisie%TYPE;
       my_nb_decimales       NUMBER;
       
       my_nb                 INTEGER;
       my_par_value          PARAMETRE.par_value%TYPE;

       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_tcd_ordre          ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;

       my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tva_saisie    PDEPENSE_BUDGET.pdep_tva_saisie%TYPE;

       my_dpp_exe_ordre      DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         PDEPENSE_BUDGET.tap_id%TYPE;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_pdep_ht_saisie:=round(a_pdep_ht_saisie, my_nb_decimales);
        my_pdep_ttc_saisie:=round(a_pdep_ttc_saisie, my_nb_decimales);
      
        IF my_pdep_ht_saisie<0 OR my_pdep_ttc_saisie<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT exe_ordre, org_id, tap_id, tcd_ordre INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT exe_ordre INTO my_dpp_exe_ordre FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La pre-liquidation doit etre sur le meme exercice que la facture papier.');
        END IF;

        -- verification de la coherence du prorata.
        IF Get_Parametre(a_exe_ordre, 'DEPENSE_IDEM_TAP_ID')<>'NON' AND
           my_tap_id<>a_tap_id THEN
             RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la pre-liquidation soit le meme que l''engagement initial.');
        END IF;

        -- on verifie la coherence des montants.
         IF ABS(my_pdep_ht_saisie)>ABS(my_pdep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_pdep_tva_saisie:=Liquider_Outils.get_tva(my_pdep_ht_saisie, my_pdep_ttc_saisie);

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              my_pdep_ht_saisie,my_pdep_ttc_saisie);

        -- insertion dans la table.
        IF a_pdep_id IS NULL THEN
           SELECT pdepense_budget_seq.NEXTVAL INTO a_pdep_id FROM dual;
        END IF;

        -- on liquide.
        INSERT INTO PDEPENSE_BUDGET VALUES (a_pdep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           my_pdep_ht_saisie, my_pdep_tva_saisie, my_pdep_ttc_saisie, a_tap_id, a_utl_ordre);
   END;

   PROCEDURE ins_pdepense_ctrl_action (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdact_id                 PDEPENSE_CTRL_ACTION.pdact_id%TYPE;
       my_tyac_id                  PDEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_pdact_montant_budgetaire PDEPENSE_CTRL_ACTION.pdact_montant_budgetaire%TYPE;
       my_pdact_ht_saisie          PDEPENSE_CTRL_ACTION.pdact_ht_saisie%TYPE;
       my_pdact_tva_saisie         PDEPENSE_CTRL_ACTION.pdact_tva_saisie%TYPE;
       my_pdact_ttc_saisie         PDEPENSE_CTRL_ACTION.pdact_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                PDEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_ACTION.pdact_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdact_ht_saisie:=round(my_pdact_ht_saisie, my_nb_decimales);
            my_pdact_ttc_saisie:=round(my_pdact_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdact_ht_saisie)>ABS(my_pdact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdact_tva_saisie:=Liquider_Outils.get_tva(my_pdact_ht_saisie, my_pdact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_pdact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdact_ht_saisie,my_pdact_ttc_saisie);

            IF my_pdact_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR my_pdep_montant_budgetaire<=my_somme+my_pdact_montant_budgetaire THEN
                my_pdact_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_action_seq.NEXTVAL INTO my_pdact_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_ACTION VALUES (my_pdact_id,
                   a_exe_ordre, a_pdep_id, my_tyac_id, my_pdact_montant_budgetaire,
                   my_pdact_ht_saisie, my_pdact_tva_saisie, my_pdact_ttc_saisie);

            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_pdact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_analytique (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdana_id                 PDEPENSE_CTRL_ANALYTIQUE.pdana_id%TYPE;
       my_can_id                   PDEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_pdana_montant_budgetaire PDEPENSE_CTRL_ANALYTIQUE.pdana_montant_budgetaire%TYPE;
       my_pdana_ht_saisie          PDEPENSE_CTRL_ANALYTIQUE.pdana_ht_saisie%TYPE;
       my_pdana_tva_saisie         PDEPENSE_CTRL_ANALYTIQUE.pdana_tva_saisie%TYPE;
       my_pdana_ttc_saisie         PDEPENSE_CTRL_ANALYTIQUE.pdana_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_ANALYTIQUE.pdana_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdana_ht_saisie:=round(my_pdana_ht_saisie, my_nb_decimales);
            my_pdana_ttc_saisie:=round(my_pdana_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdana_ht_saisie)>ABS(my_pdana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdana_tva_saisie:=Liquider_Outils.get_tva(my_pdana_ht_saisie, my_pdana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdana_ht_saisie,my_pdana_ttc_saisie);

            IF my_pdana_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_pdep_montant_budgetaire<=my_somme+my_pdana_montant_budgetaire THEN
                my_pdana_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_analytique_seq.NEXTVAL INTO my_pdana_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_ANALYTIQUE VALUES (my_pdana_id,
                   a_exe_ordre, a_pdep_id, my_can_id, my_pdana_montant_budgetaire,
                   my_pdana_ht_saisie, my_pdana_tva_saisie, my_pdana_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_pdana_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_convention (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdcon_id                 PDEPENSE_CTRL_CONVENTION.pdcon_id%TYPE;
       my_conv_ordre               PDEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_pdcon_montant_budgetaire PDEPENSE_CTRL_CONVENTION.pdcon_montant_budgetaire%TYPE;
       my_pdcon_ht_saisie          PDEPENSE_CTRL_CONVENTION.pdcon_ht_saisie%TYPE;
       my_pdcon_tva_saisie         PDEPENSE_CTRL_CONVENTION.pdcon_tva_saisie%TYPE;
       my_pdcon_ttc_saisie         PDEPENSE_CTRL_CONVENTION.pdcon_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_CONVENTION.pdcon_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdcon_ht_saisie:=round(my_pdcon_ht_saisie, my_nb_decimales);
            my_pdcon_ttc_saisie:=round(my_pdcon_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdcon_ht_saisie)>ABS(my_pdcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdcon_tva_saisie:=Liquider_Outils.get_tva(my_pdcon_ht_saisie, my_pdcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdcon_ht_saisie,my_pdcon_ttc_saisie);

            IF my_pdcon_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_pdep_montant_budgetaire<=my_somme+my_pdcon_montant_budgetaire THEN
                my_pdcon_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_convention_seq.NEXTVAL INTO my_pdcon_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_CONVENTION VALUES (my_pdcon_id,
                   a_exe_ordre, a_pdep_id, my_conv_ordre, my_pdcon_montant_budgetaire,
                   my_pdcon_ht_saisie, my_pdcon_tva_saisie, my_pdcon_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_pdcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_hors_marche (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdhom_id                 PDEPENSE_CTRL_HORS_MARCHE.pdhom_id%TYPE;
       my_typa_id                  PDEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                 PDEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_pdhom_montant_budgetaire PDEPENSE_CTRL_HORS_MARCHE.pdhom_montant_budgetaire%TYPE;
       my_pdhom_ht_saisie          PDEPENSE_CTRL_HORS_MARCHE.pdhom_ht_saisie%TYPE;
       my_pdhom_tva_saisie         PDEPENSE_CTRL_HORS_MARCHE.pdhom_tva_saisie%TYPE;
       my_pdhom_ttc_saisie         PDEPENSE_CTRL_HORS_MARCHE.pdhom_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_HORS_MARCHE.pdhom_montant_budgetaire%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_?chat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdhom_ht_saisie:=round(my_pdhom_ht_saisie, my_nb_decimales);
            my_pdhom_ttc_saisie:=round(my_pdhom_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdhom_ht_saisie)>ABS(my_pdhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdhom_tva_saisie:=Liquider_Outils.get_tva(my_pdhom_ht_saisie, my_pdhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdhom_ht_saisie,my_pdhom_ttc_saisie);

            IF my_pdhom_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_pdep_montant_budgetaire<=my_somme+my_pdhom_montant_budgetaire THEN
                my_pdhom_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_hors_marche_seq.NEXTVAL INTO my_pdhom_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_HORS_MARCHE VALUES (my_pdhom_id,
                   a_exe_ordre, a_pdep_id, my_typa_id, my_ce_ordre, my_pdhom_montant_budgetaire,
                   my_pdhom_ht_saisie, my_pdhom_tva_saisie, my_pdhom_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_pdhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_marche (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdmar_id                 PDEPENSE_CTRL_MARCHE.pdmar_id%TYPE;
       my_att_ordre                PDEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_pdmar_montant_budgetaire PDEPENSE_CTRL_MARCHE.pdmar_montant_budgetaire%TYPE;
       my_pdmar_ht_saisie          PDEPENSE_CTRL_MARCHE.pdmar_ht_saisie%TYPE;
       my_pdmar_tva_saisie         PDEPENSE_CTRL_MARCHE.pdmar_tva_saisie%TYPE;
       my_pdmar_ttc_saisie         PDEPENSE_CTRL_MARCHE.pdmar_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                PDEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_MARCHE.pdmar_montant_budgetaire%TYPE;
       my_dpp_id                   PDEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                DEPENSE_PAPIER.fou_ordre%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdmar_ht_saisie:=round(my_pdmar_ht_saisie, my_nb_decimales);
            my_pdmar_ttc_saisie:=round(my_pdmar_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdmar_ht_saisie)>ABS(my_pdmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdmar_tva_saisie:=Liquider_Outils.get_tva(my_pdmar_ht_saisie, my_pdmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdmar_ht_saisie,my_pdmar_ttc_saisie);

            IF my_pdmar_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_pdep_montant_budgetaire<=my_somme+my_pdmar_montant_budgetaire THEN
                my_pdmar_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_marche_seq.NEXTVAL INTO my_pdmar_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_MARCHE VALUES (my_pdmar_id,
                   a_exe_ordre, a_pdep_id, my_att_ordre, my_pdmar_montant_budgetaire,
                   my_pdmar_ht_saisie, my_pdmar_tva_saisie, my_pdmar_ttc_saisie);

            -- procedure de verification
            Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_pdmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_pdepense_ctrl_planco (
      a_exe_ordre     PDEPENSE_BUDGET.exe_ordre%TYPE,
      a_pdep_id       PDEPENSE_BUDGET.pdep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_pdpco_id                 PDEPENSE_CTRL_PLANCO.pdpco_id%TYPE;
       my_pco_num                  PDEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_pdpco_montant_budgetaire PDEPENSE_CTRL_PLANCO.pdpco_montant_budgetaire%TYPE;
       my_pdpco_ht_saisie          PDEPENSE_CTRL_PLANCO.pdpco_ht_saisie%TYPE;
       my_pdpco_tva_saisie         PDEPENSE_CTRL_PLANCO.pdpco_tva_saisie%TYPE;
       my_pdpco_ttc_saisie         PDEPENSE_CTRL_PLANCO.pdpco_ttc_saisie%TYPE;
       my_pdep_montant_budgetaire  PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;
       my_pdep_tap_id              PDEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                PDEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                PDEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    PDEPENSE_CTRL_PLANCO.pdpco_montant_budgetaire%TYPE;
       my_ecd_ordre                PDEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_mod_ordre                DEPENSE_PAPIER.mod_ordre%TYPE;
       my_eng_id                   PDEPENSE_BUDGET.eng_id%TYPE;
       my_inventaires              VARCHAR2(30000);
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La pre-liquidation n''existe pas (pdep_id='||a_pdep_id||')');
        END IF;

        SELECT d.pdep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, d.eng_id
               INTO my_pdep_montant_budgetaire, my_pdep_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre, my_eng_id
          FROM PDEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.pdep_id=a_pdep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_pdpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_pdpco_ht_saisie:=round(my_pdpco_ht_saisie, my_nb_decimales);
            my_pdpco_ttc_saisie:=round(my_pdpco_ttc_saisie, my_nb_decimales);

            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            IF my_ecd_ordre IS NOT NULL THEN
               SELECT mod_ordre INTO my_mod_ordre
                  FROM PDEPENSE_BUDGET d, DEPENSE_PAPIER p WHERE p.dpp_id=d.dpp_id AND d.pdep_id=a_pdep_id;
               Verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
            END IF;

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_pdpco_ht_saisie)>ABS(my_pdpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_pdpco_tva_saisie:=Liquider_Outils.get_tva(my_pdpco_ht_saisie, my_pdpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_pdpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_pdep_tap_id,my_org_id,
                   my_pdpco_ht_saisie,my_pdpco_ttc_saisie);

            IF my_pdpco_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une pre-liquidation il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_pdep_montant_budgetaire<=my_somme+my_pdpco_montant_budgetaire THEN
                my_pdpco_montant_budgetaire:=my_pdep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT pdepense_ctrl_planco_seq.NEXTVAL INTO my_pdpco_id FROM dual;

            INSERT INTO PDEPENSE_CTRL_PLANCO VALUES (my_pdpco_id,
                   a_exe_ordre, a_pdep_id, my_pco_num, my_pdpco_montant_budgetaire,
                   my_pdpco_ht_saisie, my_pdpco_tva_saisie, my_pdpco_ttc_saisie, my_ecd_ordre);

            -- procedure de verification
            Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
            ins_pdepense_ctrl_planco_inve(my_pdpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
            my_somme:=my_somme + my_pdpco_montant_budgetaire;
        END LOOP;
   END;
   
PROCEDURE ins_pdepense_ctrl_planco_inve (
      a_pdpco_id            PDEPENSE_CTRL_PLANCO.pdpco_id%TYPE,
      a_chaine                VARCHAR2
   ) IS
          my_invc_id                  integer; --jefy_inventaire.inventaire_comptable.invc%type;
       my_pdpin_id                 pdepense_ctrl_planco_inve.pdpin_id%type;
       my_montant                  pdepense_ctrl_planco_inve.pdpin_montant_budgetaire%type;
       my_somme                    pdepense_ctrl_planco.pdpco_montant_budgetaire%type;
       my_pdpco_montant_budgetaire pdepense_ctrl_planco.pdpco_montant_budgetaire%type;
       my_chaine                   VARCHAR2(30000);
       my_exe_ordre                   pdepense_ctrl_planco.exe_ordre%type;
       my_pco_num                   pdepense_ctrl_planco.pco_num%type;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                       INTEGER;
   BEGIN
       -- inventaire
       my_chaine:=a_chaine;
       my_somme:=0;
          LOOP
              IF a_chaine IS  NULL OR LENGTH(a_chaine)=0 OR SUBSTR(my_chaine,1,1)='|' THEN EXIT; END IF;

            -- on recupere la cle de l'inventaire.
            SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'|')-1)) INTO my_invc_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'|')+1,LENGTH(my_chaine));
            -- on recupere le montant budgetaire.
            SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'|')-1)) INTO my_montant FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'|')+1,LENGTH(my_chaine));

            my_somme:=my_somme+my_montant;

            select pdepense_ctrl_planco_inve_seq.nextval into my_pdpin_id from dual;
            insert into pdepense_ctrl_planco_inve values (my_pdpin_id, a_pdpco_id, my_invc_id, my_montant);
       END LOOP;

       SELECT dp.pdpco_montant_budgetaire, dp.exe_ordre, dp.pco_num INTO my_pdpco_montant_budgetaire, my_exe_ordre, my_pco_num
           FROM PDEPENSE_CTRL_PLANCO dp WHERE dp.pdpco_id=a_pdpco_id;
           
       SELECT par_value INTO my_par_value FROM PARAMETRE WHERE par_key='INVENTAIRE_OBLIGATOIRE' AND exe_ordre=my_exe_ordre;

       IF my_par_value='OUI' THEN
             SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_inventoriable WHERE pco_num=my_pco_num AND exe_ordre=my_exe_ordre;
          IF my_nb>0 THEN
                IF my_somme<>my_pdpco_montant_budgetaire THEN
                     RAISE_APPLICATION_ERROR(-20001,'le montant budgetaire des inventaires n''est pas egal au montant budgetaire de la pre-liquidation ');
                END IF;
          END IF;

       END IF;
   END;
   
FUNCTION get_Chaine_Action ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_action jefy_depense.pdepense_CTRL_ACTION%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_ACTION WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_ACTION.pdact_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_ACTION.pdact_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_ACTION.pdact_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_ACTION.pdact_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_ACTION WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_action;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_action.pdact_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_action.pdact_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;

           my_chaine:=my_chaine||my_pdepense_ctr_action.tyac_id||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_analytique ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_analytique jefy_depense.pdepense_CTRL_analytique%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_analytique WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_analytique.pdana_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_analytique.pdana_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_analytique.pdana_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_analytique.pdana_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_analytique WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_analytique;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

--              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_analytique.pdana_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_analytique.pdana_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

/*              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;
*/
           my_chaine:=my_chaine||my_pdepense_ctr_analytique.can_id||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_convention ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_convention jefy_depense.pdepense_CTRL_convention%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_convention WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_convention.pdcon_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_convention.pdcon_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_convention.pdcon_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_convention.pdcon_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_convention WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_convention;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

--              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_convention.pdcon_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_convention.pdcon_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

/*              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;
*/
           my_chaine:=my_chaine||my_pdepense_ctr_convention.conv_ordre||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_hors_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_hors_marche jefy_depense.pdepense_CTRL_hors_marche%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_hors_marche WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_hors_marche.pdhom_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_hors_marche.pdhom_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_hors_marche.pdhom_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_hors_marche.pdhom_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_hors_marche WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_hors_marche;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_hors_marche.pdhom_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_hors_marche.pdhom_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;

           my_chaine:=my_chaine||my_pdepense_ctr_hors_marche.typa_id||'$'||my_pdepense_ctr_hors_marche.ce_ordre||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
   
FUNCTION get_Chaine_marche ( 
      a_ht      pdepense_ctrl_planco.pdpco_ht_saisie%type,
      a_ttc     pdepense_ctrl_planco.pdpco_ttc_saisie%type, 
      a_pdep_id pdepense_ctrl_planco.pdep_id%type
   ) RETURN VARCHAR2 IS
         my_pdepense_ctr_marche jefy_depense.pdepense_CTRL_marche%ROWTYPE;
       CURSOR my_cursor IS SELECT * FROM jefy_depense.pdepense_CTRL_marche WHERE pdep_id=a_pdep_id;

       my_chaine VARCHAR2(200);

       my_montant_ht     PDEPENSE_CTRL_marche.pdmar_ht_saisie%TYPE;
         my_montant_ttc    PDEPENSE_CTRL_marche.pdmar_ttc_saisie%TYPE;
         my_temp_ht        PDEPENSE_CTRL_marche.pdmar_ht_saisie%TYPE;
         my_temp_ttc       PDEPENSE_CTRL_marche.pdmar_ttc_saisie%TYPE;
         my_montant_budgetaire PDEPENSE_BUDGET.pdep_montant_budgetaire%TYPE;

         my_nb             INTEGER;
         my_cpt            INTEGER;
       my_nb_decimales     NUMBER;
       my_exe_ordre         PDEPENSE_BUDGET.exe_ordre%TYPE;
   BEGIN
       my_chaine:='';
       my_cpt:=0;
       my_montant_ht:=0;
       my_montant_ttc:=0;
       my_temp_ht:=0;
       my_temp_ttc:=0;

       SELECT pdep_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
       FROM jefy_depense.PDEPENSE_BUDGET WHERE pdep_id=a_pdep_id;

           -- nb decimales.
        my_nb_decimales:=get_nb_decimales(my_exe_ordre);
       
       SELECT COUNT(*) INTO my_nb FROM jefy_depense.PDEPENSE_CTRL_marche WHERE pdep_id = a_pdep_id;

       OPEN my_cursor;
       LOOP
              FETCH my_cursor INTO my_pdepense_ctr_marche;
              EXIT WHEN my_cursor %NOTFOUND;

              my_cpt:=my_cpt + 1;

              IF (my_cpt < my_nb) THEN
                    my_montant_ht:=ROUND( a_ht * my_pdepense_ctr_marche.pdmar_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
                    my_montant_ttc:= ROUND( a_ttc * my_pdepense_ctr_marche.pdmar_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

                    IF my_montant_ht>a_ht-my_temp_ht THEN 
                         my_montant_ht:=a_ht-my_temp_ht;
                    END IF;
                    IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
                         my_montant_ttc:=a_ttc-my_temp_ttc;
                    END IF;

                    my_temp_ht:=my_temp_ht + my_montant_ht;
                    my_temp_ttc:=my_temp_ttc + my_montant_ttc;

              ELSE
                     my_montant_ht:=a_ht - my_temp_ht;
                     my_montant_ttc :=a_ttc - my_temp_ttc;
              END IF;

           my_chaine:=my_chaine||my_pdepense_ctr_marche.att_ordre||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

       END LOOP;
       CLOSE my_cursor;

       RETURN my_chaine||'$';
   END;
END;
/





insert into jefy_depense.db_version values (2040,'2040',sysdate,sysdate,null);
 commit;









