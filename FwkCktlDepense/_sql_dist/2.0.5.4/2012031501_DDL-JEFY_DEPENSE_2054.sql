SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.5.4
-- Date de publication : 15/03/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- correction package liquider
-- correction vues intégrant des ribs
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
	select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.1.5';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
    
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2050';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/




CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Liquider  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE);

PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type);

PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type );

PROCEDURE ins_depense (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2);

PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT          COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE);

PROCEDURE del_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE del_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);


--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


PROCEDURE log_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE log_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);

PROCEDURE ins_depense_budget (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE);

PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre           DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

END;
/


GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_MISSION;

GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_PAF;

GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_PAYE;

GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_RECETTE;


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Liquider
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_depense_papier  depense_papier%rowtype;
      my_pers_id         jefy_admin.utilisateur.pers_id%type;
    BEGIN
      select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 
      
      select * into my_depense_papier from depense_papier where dpp_id=a_dpp_id;
      if my_depense_papier.dpp_date_service_fait is not null then 
         RAISE_APPLICATION_ERROR(-20001, 'La facture a deja une date de service fait ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if;
      
      select pers_id into my_pers_id from jefy_admin.utilisateur where utl_ordre=a_utl_ordre;
      update depense_papier set dpp_date_service_fait=a_dpp_date_service_fait, dpp_sf_pers_id=my_pers_id, dpp_sf_date=sysdate
          where dpp_id=a_dpp_id;      
    END;

   PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_pdep_id         pdepense_budget.pdep_id%type;
      CURSOR liste IS SELECT pdep_id FROM pDEPENSE_budget WHERE dpp_id=a_dpp_id;
    BEGIN
      service_fait(a_dpp_id, a_utl_ordre, a_dpp_date_service_fait);
      
      select count(*) into my_nb from pdepense_budget where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''a pas de pre-liquidation ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 

      OPEN liste();
      LOOP
         FETCH liste INTO my_pdep_id;
         EXIT WHEN liste%NOTFOUND;

         pre_liquider.liquider_pdepense_budget(my_pdep_id, a_utl_ordre);
      END LOOP;
      CLOSE liste;
    END;

   PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
   BEGIN
        ins_depense_papier_avec_IM(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
            a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
            a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, null, null, null);
   end;
   
PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type
   ) IS
   BEGIN
      ins_depense_papier_avec_im_sf(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
          a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception, a_dpp_date_service_fait, 
          a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, a_dpp_im_taux, a_dpp_im_dgp, imtt_id, null, null,null);
   END;
   
   PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type 
    ) IS
      my_dpp_ht_initial       DEPENSE_PAPIER.dpp_ht_initial%TYPE;
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpp_ttc_initial      DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
      my_dpp_sf_pers_id       DEPENSE_PAPIER.dpp_sf_pers_id%type;
      my_dpp_sf_date          DEPENSE_PAPIER.dpp_sf_date%type;
      my_nb_decimales         NUMBER;
      my_nb                   integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);
        Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mod_ordre, a_exe_ordre);

        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dpp_ht_initial:=round(a_dpp_ht_initial, my_nb_decimales);
        my_dpp_ttc_initial:=round(a_dpp_ttc_initial, my_nb_decimales);

        -- si les montants sont negatifs ou si il y a un dpp_id_reversement (c'est un ORV) -> package reverser.
        IF my_dpp_ht_initial<0 OR my_dpp_ttc_initial<0 OR a_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(my_dpp_ht_initial)>ABS(my_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(my_dpp_ht_initial, my_dpp_ttc_initial);

           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        my_dpp_sf_pers_id:=a_dpp_sf_pers_id;
        my_dpp_sf_date:=a_dpp_sf_date;
        if a_dpp_date_service_fait is not null then
           if a_dpp_sf_pers_id is null then
              select pers_id into my_dpp_sf_pers_id from v_utilisateur where utl_ordre=a_utl_ordre;
           end if;
           
           if a_dpp_sf_date is null then
              select sysdate into my_dpp_sf_date from dual;
           end if;  
        end if;
        
        select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
        
        if my_nb=0 then
           INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
              0, 0, a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
              a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
              my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial, a_dpp_im_taux, a_dpp_im_dgp, a_imtt_id,
              my_dpp_sf_pers_id, my_dpp_sf_date,a_ecd_ordre);
        else
           update DEPENSE_PAPIER set dpp_numero_facture=a_dpp_numero_facture, rib_ordre=a_rib_ordre, mod_ordre=a_mod_ordre, dpp_date_facture=a_dpp_date_facture, 
              dpp_date_reception=a_dpp_date_reception, dpp_date_service_fait=a_dpp_date_service_fait, dpp_nb_piece=a_dpp_nb_piece, utl_ordre=a_utl_ordre, 
              dpp_id_reversement=a_dpp_id_reversement, dpp_ht_initial=my_dpp_ht_initial, dpp_tva_initial=my_dpp_tva_initial, dpp_ttc_initial=my_dpp_ttc_initial, 
              dpp_im_taux=a_dpp_im_taux, dpp_im_dgp=a_dpp_im_dgp, imtt_id=a_imtt_id, dpp_sf_pers_id=my_dpp_sf_pers_id, dpp_sf_date=my_dpp_sf_date, dpp_date_saisie=sysdate, ecd_ordre=a_ecd_ordre
             where dpp_id=a_dpp_id;
        end if;
   END;

   PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT         COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE
   ) IS
     my_nb                    INTEGER;
     my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
     my_cde_exe_ordre         COMMANDE.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture est deja utilise pour une commande ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT exe_ordre INTO my_cde_exe_ordre FROM COMMANDE WHERE comm_id=a_comm_id;

        IF my_dpp_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.dep_papier(a_dpp_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        -- si pas de probleme on insere.
        IF a_cdp_id IS NULL THEN
           SELECT commande_dep_papier_seq.NEXTVAL INTO a_cdp_id FROM dual;
        END IF;

        INSERT INTO COMMANDE_DEP_PAPIER VALUES (a_cdp_id, a_comm_id, a_dpp_id);
   END;

   PROCEDURE ins_depense (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id                depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_org_id               engage_budget.org_id%type;
     my_nb_decimales         NUMBER;
     my_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE;
     my_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
   BEGIN
       IF a_dep_ttc_saisie<>0 THEN
       
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);

        select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

        -- lancement des differentes procedures d'insertion des tables de depense.
        ins_depense_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie,
            a_tap_id, a_utl_ordre, a_dep_id_reversement);

        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie+my_dep_ttc_saisie-my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie+my_dep_ttc_saisie
           WHERE dpp_id=a_dpp_id;

           -- on le passe en premier car utilis? par les autres pour les upd_engage_reste_.
        ins_depense_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

        ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
        ins_depense_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
        ins_depense_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
        ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
        ins_depense_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);

        --Corriger.upd_engage_reste(a_eng_id);

        -- on verifie la coherence des montants entre les differents depense_.
        Verifier.verifier_depense_coherence(a_dep_id);
        Verifier.verifier_depense_pap_coherence(a_dpp_id);
        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
      my_reim_id                INTEGER;
      cpt                       INTEGER;
      my_eng_id                 ENGAGE_BUDGET.eng_id%TYPE;
      my_zdep_id                Z_DEPENSE_BUDGET.zdep_id%TYPE;
      my_exe_ordre              DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire     DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dep_total_ht           DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_total_ttc          DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dep_total_bud          DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_eng_montant_bud        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_montant_bud_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_dispo_ligne            v_budget_exec_credit.bdxc_disponible%TYPE;
      my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_dep_ht_saisie          DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_eng_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
      my_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE;
      my_comm_id                COMMANDE.comm_id%TYPE;
      my_dpco_id                 DEPENSE_CTRL_PLANCO.dpco_id%TYPE;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La liquidation n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

           SELECT e.org_id, e.tcd_ordre, d.exe_ordre, d.dep_ht_saisie, d.dep_ttc_saisie,
               d.dep_montant_budgetaire, e.eng_id, e.tap_id, e.eng_montant_budgetaire, e.eng_montant_budgetaire_reste, d.dpp_id
          INTO my_org_id, my_tcd_ordre, my_exe_ordre, my_dep_ht_saisie, my_dep_ttc_saisie,
               my_montant_budgetaire, my_eng_id, my_eng_tap_id, my_eng_montant_bud, my_eng_montant_bud_reste, my_dpp_id
        FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e
        WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si ce n'est pas un ORV.
        IF my_montant_budgetaire<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- on recupere les montants des autres factures et ORV.
        SELECT NVL(SUM(Budget.calculer_budgetaire(my_exe_ordre,my_eng_tap_id,my_org_id,dep_ht_saisie, dep_ttc_saisie)),0) 
          INTO my_dep_total_bud
          FROM DEPENSE_BUDGET WHERE dep_id<>a_dep_id AND dep_id IN
             (SELECT dep_id FROM DEPENSE_CTRL_PLANCO
               WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE eng_id=my_eng_id)
               AND (dpco_montant_budgetaire>0 /*OR man_id IN
               (SELECT man_id FROM v_mandat WHERE man_etat IN ('VISE','PAYE'))*/));

        -- on calcule le reste de l'engagement
        my_reste:=my_eng_montant_bud-my_dep_total_bud;
        IF my_reste<0 THEN
           my_reste:=0;
        END IF;

        -- on compare au dispo de la ligne budgetaire ... pour voir si on peut tt reengager.
        my_dispo_ligne:=my_montant_budgetaire+
            Budget.engager_disponible(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>my_dispo_ligne+my_eng_montant_bud_reste  THEN
          my_reste:=my_dispo_ligne+my_eng_montant_bud_reste;
        END IF;

        -- on modifie l'engagement.
        UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=my_reste WHERE eng_id=my_eng_id;

        -- tout est bon ... on supprime la depense.
        log_depense_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        -- Suppression des reimputations associees (Dont celles effectuees dans Kiwi)
        select count(*) into cpt from reimputation where dep_id = a_dep_id;
        if (cpt > 0)
        then
        
            jefy_mission.kiwi_paiement.del_reimputation(a_dep_id);

            DELETE FROM reimputation_ACTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_ANALYTIQUE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_BUDGET WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_CONVENTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_HORS_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            DELETE FROM reimputation_PLANCO WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
            
            DELETE FROM reimputation WHERE dep_id=a_dep_id;

        end if;


        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        Corriger.upd_engage_reste(my_eng_id);

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(my_eng_id);
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

       Apres_Liquide.del_depense_budget(my_eng_id, a_dep_id);

   END;


   PROCEDURE del_depense_papier (
      a_dpp_id             DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre          Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE) IS
      my_nb                INTEGER;
      my_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
      my_dpp_ttc_initial   DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_depense_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;



--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------



   PROCEDURE log_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id            Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
       INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_depense_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id                 depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_dep_ht_saisie      DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_nb_decimales       NUMBER;
       
       my_nb                 INTEGER;
       my_par_value          PARAMETRE.par_value%TYPE;

       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_tcd_ordre          ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_eng_budgetaire     ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_eng_reste          ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
       my_eng_init           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_dep_eng_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

       my_dpp_ht_initial     DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial    DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial    DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       --my_sum_ht_saisie         depense_budget.dep_ht_saisie%type;
       --my_sum_tva_saisie     depense_budget.dep_tva_saisie%type;
       --my_sum_ttc_saisie     depense_budget.dep_ttc_saisie%type;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);
      
        IF my_dep_ht_saisie<0 OR my_dep_ttc_saisie<0 OR a_dep_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT exe_ordre, org_id, tap_id, tcd_ordre, eng_montant_budgetaire_reste, eng_montant_budgetaire
          INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre, my_eng_reste, my_eng_init
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

          SELECT exe_ordre, dpp_ht_initial, dpp_tva_initial, dpp_ttc_initial
          INTO my_dpp_exe_ordre, my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        -- verification de la coherence du prorata.
        IF Get_Parametre(a_exe_ordre, 'DEPENSE_IDEM_TAP_ID')<>'NON' AND
           my_tap_id<>a_tap_id THEN
             RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la depense soit le meme que l''engagement initial.');
        END IF;

        -- on verifie la coherence des montants.
         IF ABS(my_dep_ht_saisie)>ABS(my_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(my_dep_ht_saisie, my_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declar? dans la papier.
        /*select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_tva_saisie),0) , nvl(sum(dep_ttc_saisie),0)
               into my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          from depense_budget where dpp_id=a_dpp_id;

        if abs(my_sum_ht_saisie+a_dep_ht_saisie) > abs(my_dpp_ht_initial) or
           abs(my_sum_tva_saisie+my_dep_tva_saisie) > abs(my_dpp_tva_initial) or
           abs(my_sum_ttc_saisie+a_dep_ttc_saisie) > abs(my_dpp_ttc_initial) then
           raise_application_error(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        end if;
        */
        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              my_dep_ht_saisie,my_dep_ttc_saisie);

        -- on calcule pour diminuer le reste de l'engagement du montant liquid? ou celui par rapport au tap_id.
        --    de l'engage (si le tap_id peut etre different de celui de l'engage).
        my_eng_budgetaire:=Liquider_Outils.get_eng_montant_budgetaire(a_exe_ordre, my_tap_id, a_tap_id,
          my_montant_budgetaire, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

        -- si le reste engage est 0, et qu'on passe une liquidation positive -> erreur.
        IF my_eng_reste<=0 AND my_eng_budgetaire>0 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement est deja solde ('||indication_erreur.engagement(a_eng_id)||')');
        END IF;

        -- on verifie qu'on ne diminue pas plus que ce qu'il y a d'engage.
        IF my_eng_reste<my_eng_budgetaire THEN
           my_eng_budgetaire:=my_eng_reste;
        END IF;

        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

          -- on diminue le reste engage de l'engagement.
        UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=eng_montant_budgetaire_reste-my_eng_budgetaire
           WHERE eng_id=a_eng_id;

        -- on liquide.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre,
           a_dep_id_reversement);

        Budget.maj_budget(a_exe_ordre, my_org_id, my_tcd_ordre);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dact_id                   DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                        DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_dact_ht_saisie             DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie           DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie           DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                   DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dact_ht_saisie:=round(my_dact_ht_saisie, my_nb_decimales);
            my_dact_ttc_saisie:=round(my_dact_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            IF my_dact_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dact_montant_budgetaire THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id,
                   a_exe_ordre, a_dep_id, my_tyac_id, my_dact_montant_budgetaire,
                   my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_action(my_eng_id);

            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dana_id                   DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                        DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_dana_ht_saisie             DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dana_ht_saisie:=round(my_dana_ht_saisie, my_nb_decimales);
            my_dana_ttc_saisie:=round(my_dana_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);

            IF my_dana_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_dep_montant_budgetaire<=my_somme+my_dana_montant_budgetaire THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_analytique(my_eng_id);

            Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                   DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre                    DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_dcon_ht_saisie             DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie           DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie           DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dcon_ht_saisie:=round(my_dcon_ht_saisie, my_nb_decimales);
            my_dcon_ttc_saisie:=round(my_dcon_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);

            IF my_dcon_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_dep_montant_budgetaire<=my_somme+my_dcon_montant_budgetaire THEN
                my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_convention(my_eng_id);

            Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dhom_id                   DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                        DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_dhom_ht_saisie             DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_?chat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dhom_ht_saisie:=round(my_dhom_ht_saisie, my_nb_decimales);
            my_dhom_ttc_saisie:=round(my_dhom_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            IF my_dhom_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dhom_montant_budgetaire THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_hors_marche(my_eng_id);

            Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dmar_ht_saisie:=round(my_dmar_ht_saisie, my_nb_decimales);
            my_dmar_ttc_saisie:=round(my_dmar_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);

            IF my_dmar_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dmar_montant_budgetaire THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

                         dbms_output.put_line('insert depense : '||my_dmar_id);

               -- procedure de verification
                         dbms_output.put_line('upd engage : '||my_eng_id);
            Corriger.upd_engage_reste_marche(my_eng_id);

            Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_mod_ordre                   DEPENSE_PAPIER.mod_ordre%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_inventaires    VARCHAR2(30000);
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            --SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            select ecd_ordre into my_ecd_ordre from depense_papier where dpp_id in (select dpp_id from depense_budget where dep_id=a_dep_id);
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dpco_ht_saisie:=round(my_dpco_ht_saisie, my_nb_decimales);
            my_dpco_ttc_saisie:=round(my_dpco_ttc_saisie, my_nb_decimales);

            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            IF my_ecd_ordre IS NOT NULL THEN
               SELECT mod_ordre INTO my_mod_ordre
                  FROM DEPENSE_BUDGET d, DEPENSE_PAPIER p WHERE p.dpp_id=d.dpp_id AND d.dep_id=a_dep_id;
               Verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
            END IF;

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            IF my_dpco_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dpco_montant_budgetaire THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, my_ecd_ordre);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Corriger.upd_engage_reste_planco(my_eng_id);

            Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dpco_montant_budgetaire;
        END LOOP;
   END;
END;
/


GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_MISSION;

GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_PAF;

GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_PAYE;

GRANT EXECUTE ON JEFY_DEPENSE.LIQUIDER TO JEFY_RECETTE;


create or replace force view jefy_depense.v_rib_fournisseur (rib_ordre,
                                                             fou_ordre,
                                                             c_banque,
                                                             c_guichet,
                                                             no_compte,
                                                             cle_rib,
                                                             rib_titco,
                                                             mod_code,
                                                             rib_valide,
                                                             d_creation,
                                                             d_modification,
                                                             tem_paye_util,
                                                             iban,
                                                             bic,
                                                             domiciliation,
                                                             pers_id_creation,
                                                             pers_id_modification
                                                            )
as
   select rib_ordre,
          fou_ordre,
          r.c_banque,
          r.c_guichet,
          no_compte,
          cle_rib,
          rib_titco,
          mod_code,
          rib_valide,
          r.d_creation,
          r.d_modification,
          tem_paye_util,
          iban,
          b.bic,
          b.domiciliation,
          r.pers_id_creation,
          r.pers_id_modification
   from   grhum.ribfour_ulr r, grhum.banque b
   where  r.banq_ordre = b.banq_ordre;
/




create procedure grhum.inst_patch_jefy_depense_2054 is
begin
    insert into jefy_depense.db_version values (2054,'2054',sysdate,sysdate,null);           
end;



 









