CREATE OR REPLACE PACKAGE JEFY_DEPENSE.verifier  IS

PROCEDURE verifier_engage_exercice (
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_utl_ordre      			engage_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_extourne_exercice (
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_utl_ordre      			engage_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_depense_exercice (
      a_exe_ordre               depense_budget.exe_ordre%type,
   	  a_utl_ordre      			depense_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_organ_utilisateur (
   	  a_utl_ordre      			engage_budget.utl_ordre%type,
      a_org_id                  engage_budget.org_id%type);

PROCEDURE verifier_budget (
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_tap_id                  engage_budget.tap_id%type,
	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type);

PROCEDURE verifier_action(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_tyac_id                 engage_ctrl_action.tyac_id%type,
	  a_utl_ordre               engage_budget.utl_ordre%type);

PROCEDURE verifier_analytique(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_can_id                  engage_ctrl_analytique.can_id%type);

PROCEDURE verifier_convention(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_conv_ordre              engage_ctrl_convention.conv_ordre%type);

PROCEDURE verifier_hors_marche(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_typa_id                 engage_ctrl_hors_marche.typa_id%type,
	  a_ce_ordre                engage_ctrl_hors_marche.ce_ordre%type,
	  a_fou_ordre				engage_budget.fou_ordre%type);

PROCEDURE verifier_marche(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
	  a_fou_ordre               engage_budget.fou_ordre%type,
	  a_att_ordre               engage_ctrl_marche.att_ordre%type,
	  a_utl_ordre               engage_budget.utl_ordre%type);

PROCEDURE verifier_planco(
      a_exe_ordre               engage_budget.exe_ordre%type,
   	  a_org_id                  engage_budget.org_id%type,
   	  a_tcd_ordre               engage_budget.tcd_ordre%type,
	  a_pco_num                 engage_ctrl_planco.pco_num%type,
	  a_utl_ordre               engage_budget.utl_ordre%type);

PROCEDURE verifier_emargement (
      a_exe_ordre               engage_budget.exe_ordre%type,
	  a_mod_ordre				depense_papier.mod_ordre%type,
	  a_ecd_ordre               depense_ctrl_planco.ecd_ordre%type);


	  -- ne sert pas pour le moment --
PROCEDURE verifier_inventaire;

	  -- ne sert pas pour le moment --
PROCEDURE verifier_monnaie;

PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		v_fournisseur.fou_ordre%type);

PROCEDURE verifier_rib (
   	  a_fou_ordre		v_rib_fournisseur.fou_ordre%type,
   	  a_rib_ordre		v_rib_fournisseur.rib_ordre%type,
   	  a_mod_ordre		depense_papier.mod_ordre%type,
   	  a_exe_ordre		depense_papier.exe_ordre%type);

	  -- ne sert pas pour le moment --
PROCEDURE verifier_organ(
   	  a_org_id			engage_budget.org_id%type,
	  a_tcd_ordre		engage_budget.tcd_ordre%type);

PROCEDURE verifier_engage_coherence(a_eng_id engage_budget.eng_id%type);

PROCEDURE verifier_commande_coherence(a_comm_id commande.comm_id%type);

PROCEDURE verifier_cde_budget_coherence(a_cbud_id commande_budget.cbud_id%type);

PROCEDURE verifier_depense_pap_coherence(a_dpp_id depense_papier.dpp_id%type);

PROCEDURE verifier_depense_coherence(a_dep_id depense_budget.dep_id%type);

PROCEDURE verifier_util_engage (
      a_eng_id              engage_budget.eng_id%type);

PROCEDURE verifier_util_depense_budget (
      a_dep_id              depense_budget.dep_id%type);

PROCEDURE verifier_util_depense_papier (
      a_dpp_id              depense_papier.dpp_id%type);

PROCEDURE verifier_util_commande (
      a_comm_id              commande.comm_id%type);

PROCEDURE verifier_util_commande_budget (
      a_cbud_id              commande_budget.cbud_id%type);
END;
/
