CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Liquider  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE);

PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type);

PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type );

PROCEDURE ins_depense (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_chaine_planco         VARCHAR2);

PROCEDURE ins_depense_extourne (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id IN OUT         DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
	  a_fou_ordre			  ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				  ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			  ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			  ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_tyap_id				  ENGAGE_BUDGET.tyap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      --a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      --a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_chaine_planco         VARCHAR2);

PROCEDURE ins_depense_directe (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
	  a_fou_ordre			  ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				  ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			  ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			  ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_tyap_id				  ENGAGE_BUDGET.tyap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_chaine_planco         VARCHAR2);     

PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE);

PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT         COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE);

PROCEDURE del_depense_budget (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre             Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE del_depense_papier (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);


--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


PROCEDURE log_depense_budget (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre             Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE);

PROCEDURE log_depense_papier (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE);

PROCEDURE ins_depense_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE);

PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2);

END;
/
